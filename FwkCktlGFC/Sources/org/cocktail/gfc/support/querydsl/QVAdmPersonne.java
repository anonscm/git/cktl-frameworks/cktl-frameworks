package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmPersonne is a Querydsl query type for QVAdmPersonne
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmPersonne extends com.mysema.query.sql.RelationalPathBase<QVAdmPersonne> {

    private static final long serialVersionUID = -822373439;

    public static final QVAdmPersonne vAdmPersonne = new QVAdmPersonne("V_ADM_PERSONNE");

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final StringPath persLc = createString("persLc");

    public final StringPath persLibelle = createString("persLibelle");

    public final StringPath persNomptr = createString("persNomptr");

    public final NumberPath<Long> persOrdre = createNumber("persOrdre", Long.class);

    public final StringPath persType = createString("persType");

    public QVAdmPersonne(String variable) {
        super(QVAdmPersonne.class, forVariable(variable), "GFC", "V_ADM_PERSONNE");
        addMetadata();
    }

    public QVAdmPersonne(String variable, String schema, String table) {
        super(QVAdmPersonne.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmPersonne(Path<? extends QVAdmPersonne> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_PERSONNE");
        addMetadata();
    }

    public QVAdmPersonne(PathMetadata<?> metadata) {
        super(QVAdmPersonne.class, metadata, "GFC", "V_ADM_PERSONNE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persLc, ColumnMetadata.named("PERS_LC").withIndex(5).ofType(Types.VARCHAR).withSize(40));
        addMetadata(persLibelle, ColumnMetadata.named("PERS_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(persNomptr, ColumnMetadata.named("PERS_NOMPTR").withIndex(6).ofType(Types.VARCHAR).withSize(80));
        addMetadata(persOrdre, ColumnMetadata.named("PERS_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persType, ColumnMetadata.named("PERS_TYPE").withIndex(2).ofType(Types.VARCHAR).withSize(5).notNull());
    }

}

