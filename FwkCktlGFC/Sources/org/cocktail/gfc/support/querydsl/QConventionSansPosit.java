package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QConventionSansPosit is a Querydsl query type for QConventionSansPosit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QConventionSansPosit extends com.mysema.query.sql.RelationalPathBase<QConventionSansPosit> {

    private static final long serialVersionUID = 1382811290;

    public static final QConventionSansPosit conventionSansPosit = new QConventionSansPosit("CONVENTION_SANS_POSIT");

    public final SimplePath<Object> cspConReference = createSimple("cspConReference", Object.class);

    public final SimplePath<Object> cspModeGestion = createSimple("cspModeGestion", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public QConventionSansPosit(String variable) {
        super(QConventionSansPosit.class, forVariable(variable), "GFC", "CONVENTION_SANS_POSIT");
        addMetadata();
    }

    public QConventionSansPosit(String variable, String schema, String table) {
        super(QConventionSansPosit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QConventionSansPosit(Path<? extends QConventionSansPosit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CONVENTION_SANS_POSIT");
        addMetadata();
    }

    public QConventionSansPosit(PathMetadata<?> metadata) {
        super(QConventionSansPosit.class, metadata, "GFC", "CONVENTION_SANS_POSIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cspConReference, ColumnMetadata.named("CSP_CON_REFERENCE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(cspModeGestion, ColumnMetadata.named("CSP_MODE_GESTION").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
    }

}

