package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEngageCtrlAction is a Querydsl query type for QEngageCtrlAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEngageCtrlAction extends com.mysema.query.sql.RelationalPathBase<QEngageCtrlAction> {

    private static final long serialVersionUID = 819360195;

    public static final QEngageCtrlAction engageCtrlAction = new QEngageCtrlAction("ENGAGE_CTRL_ACTION");

    public final DateTimePath<java.sql.Timestamp> eactDateSaisie = createDateTime("eactDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> eactHtSaisie = createNumber("eactHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> eactId = createNumber("eactId", Long.class);

    public final NumberPath<java.math.BigDecimal> eactMontantBudgetaire = createNumber("eactMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eactMontantBudgetaireReste = createNumber("eactMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eactTtcSaisie = createNumber("eactTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eactTvaSaisie = createNumber("eactTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QEngageCtrlAction> engageCtrlActionPk = createPrimaryKey(eactId);

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> engageCtrlActionEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> engageCtrlActiExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QEngageCtrlAction(String variable) {
        super(QEngageCtrlAction.class, forVariable(variable), "GFC", "ENGAGE_CTRL_ACTION");
        addMetadata();
    }

    public QEngageCtrlAction(String variable, String schema, String table) {
        super(QEngageCtrlAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEngageCtrlAction(Path<? extends QEngageCtrlAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ENGAGE_CTRL_ACTION");
        addMetadata();
    }

    public QEngageCtrlAction(PathMetadata<?> metadata) {
        super(QEngageCtrlAction.class, metadata, "GFC", "ENGAGE_CTRL_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(eactDateSaisie, ColumnMetadata.named("EACT_DATE_SAISIE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(eactHtSaisie, ColumnMetadata.named("EACT_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactId, ColumnMetadata.named("EACT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(eactMontantBudgetaire, ColumnMetadata.named("EACT_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactMontantBudgetaireReste, ColumnMetadata.named("EACT_MONTANT_BUDGETAIRE_RESTE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactTtcSaisie, ColumnMetadata.named("EACT_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactTvaSaisie, ColumnMetadata.named("EACT_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

