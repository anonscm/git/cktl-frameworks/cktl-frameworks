package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeOperationBrouillard is a Querydsl query type for QOpeOperationBrouillard
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeOperationBrouillard extends com.mysema.query.sql.RelationalPathBase<QOpeOperationBrouillard> {

    private static final long serialVersionUID = -867700348;

    public static final QOpeOperationBrouillard opeOperationBrouillard = new QOpeOperationBrouillard("OPE_OPERATION_BROUILLARD");

    public final NumberPath<Long> broId = createNumber("broId", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QOpeOperationBrouillard> opeOperationBrouillardPk = createPrimaryKey(broId, idOpeOperation);

    public QOpeOperationBrouillard(String variable) {
        super(QOpeOperationBrouillard.class, forVariable(variable), "GFC", "OPE_OPERATION_BROUILLARD");
        addMetadata();
    }

    public QOpeOperationBrouillard(String variable, String schema, String table) {
        super(QOpeOperationBrouillard.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeOperationBrouillard(Path<? extends QOpeOperationBrouillard> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_OPERATION_BROUILLARD");
        addMetadata();
    }

    public QOpeOperationBrouillard(PathMetadata<?> metadata) {
        super(QOpeOperationBrouillard.class, metadata, "GFC", "OPE_OPERATION_BROUILLARD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(broId, ColumnMetadata.named("BRO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

