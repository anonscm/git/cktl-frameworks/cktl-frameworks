package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDbVersionObsolete is a Querydsl query type for QDbVersionObsolete
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDbVersionObsolete extends com.mysema.query.sql.RelationalPathBase<QDbVersionObsolete> {

    private static final long serialVersionUID = 1834639186;

    public static final QDbVersionObsolete dbVersionObsolete = new QDbVersionObsolete("DB_VERSION_OBSOLETE");

    public final StringPath dbvComment = createString("dbvComment");

    public final DateTimePath<java.sql.Timestamp> dbvDate = createDateTime("dbvDate", java.sql.Timestamp.class);

    public final NumberPath<Integer> dbvId = createNumber("dbvId", Integer.class);

    public final DateTimePath<java.sql.Timestamp> dbvInstall = createDateTime("dbvInstall", java.sql.Timestamp.class);

    public final StringPath dbvLibelle = createString("dbvLibelle");

    public final com.mysema.query.sql.PrimaryKey<QDbVersionObsolete> dbVersionPk = createPrimaryKey(dbvId);

    public QDbVersionObsolete(String variable) {
        super(QDbVersionObsolete.class, forVariable(variable), "GFC", "DB_VERSION_OBSOLETE");
        addMetadata();
    }

    public QDbVersionObsolete(String variable, String schema, String table) {
        super(QDbVersionObsolete.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDbVersionObsolete(Path<? extends QDbVersionObsolete> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DB_VERSION_OBSOLETE");
        addMetadata();
    }

    public QDbVersionObsolete(PathMetadata<?> metadata) {
        super(QDbVersionObsolete.class, metadata, "GFC", "DB_VERSION_OBSOLETE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dbvComment, ColumnMetadata.named("DBV_COMMENT").withIndex(5).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(dbvDate, ColumnMetadata.named("DBV_DATE").withIndex(3).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dbvId, ColumnMetadata.named("DBV_ID").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(dbvInstall, ColumnMetadata.named("DBV_INSTALL").withIndex(4).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dbvLibelle, ColumnMetadata.named("DBV_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(15).notNull());
    }

}

