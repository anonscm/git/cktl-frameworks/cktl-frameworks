package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QLotUtilisateur is a Querydsl query type for QLotUtilisateur
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QLotUtilisateur extends com.mysema.query.sql.RelationalPathBase<QLotUtilisateur> {

    private static final long serialVersionUID = -1104165197;

    public static final QLotUtilisateur lotUtilisateur = new QLotUtilisateur("LOT_UTILISATEUR");

    public final NumberPath<Long> lotOrdre = createNumber("lotOrdre", Long.class);

    public final NumberPath<Long> luId = createNumber("luId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QLotUtilisateur> lotUtilisateurPk = createPrimaryKey(lotOrdre, utlOrdre);

    public QLotUtilisateur(String variable) {
        super(QLotUtilisateur.class, forVariable(variable), "GFC", "LOT_UTILISATEUR");
        addMetadata();
    }

    public QLotUtilisateur(String variable, String schema, String table) {
        super(QLotUtilisateur.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QLotUtilisateur(Path<? extends QLotUtilisateur> path) {
        super(path.getType(), path.getMetadata(), "GFC", "LOT_UTILISATEUR");
        addMetadata();
    }

    public QLotUtilisateur(PathMetadata<?> metadata) {
        super(QLotUtilisateur.class, metadata, "GFC", "LOT_UTILISATEUR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(lotOrdre, ColumnMetadata.named("LOT_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(22).notNull());
        addMetadata(luId, ColumnMetadata.named("LU_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(22).notNull());
    }

}

