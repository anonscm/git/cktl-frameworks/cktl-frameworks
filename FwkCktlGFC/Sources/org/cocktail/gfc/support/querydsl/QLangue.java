package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QLangue is a Querydsl query type for QLangue
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QLangue extends com.mysema.query.sql.RelationalPathBase<QLangue> {

    private static final long serialVersionUID = -1712207553;

    public static final QLangue langue = new QLangue("LANGUE");

    public final StringPath bibliographicCode = createString("bibliographicCode");

    public final StringPath cLangue = createString("cLangue");

    public final StringPath cPays = createString("cPays");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final StringPath llLangue = createString("llLangue");

    public final StringPath llLangueEn = createString("llLangueEn");

    public final StringPath temLocal = createString("temLocal");

    public final com.mysema.query.sql.PrimaryKey<QLangue> sysC0019014 = createPrimaryKey(cLangue);

    public QLangue(String variable) {
        super(QLangue.class, forVariable(variable), "GRHUM", "LANGUE");
        addMetadata();
    }

    public QLangue(String variable, String schema, String table) {
        super(QLangue.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QLangue(Path<? extends QLangue> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "LANGUE");
        addMetadata();
    }

    public QLangue(PathMetadata<?> metadata) {
        super(QLangue.class, metadata, "GRHUM", "LANGUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bibliographicCode, ColumnMetadata.named("BIBLIOGRAPHIC_CODE").withIndex(6).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(cLangue, ColumnMetadata.named("C_LANGUE").withIndex(1).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(cPays, ColumnMetadata.named("C_PAYS").withIndex(5).ofType(Types.VARCHAR).withSize(3));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(3).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(llLangue, ColumnMetadata.named("LL_LANGUE").withIndex(2).ofType(Types.VARCHAR).withSize(60));
        addMetadata(llLangueEn, ColumnMetadata.named("LL_LANGUE_EN").withIndex(7).ofType(Types.VARCHAR).withSize(60));
        addMetadata(temLocal, ColumnMetadata.named("TEM_LOCAL").withIndex(8).ofType(Types.CHAR).withSize(1));
    }

}

