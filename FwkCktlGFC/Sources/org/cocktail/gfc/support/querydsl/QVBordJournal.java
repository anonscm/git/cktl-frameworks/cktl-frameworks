package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVBordJournal is a Querydsl query type for QVBordJournal
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVBordJournal extends com.mysema.query.sql.RelationalPathBase<QVBordJournal> {

    private static final long serialVersionUID = 1286377507;

    public static final QVBordJournal vBordJournal = new QVBordJournal("V_BORD_JOURNAL");

    public final SimplePath<Object> borDate = createSimple("borDate", Object.class);

    public final SimplePath<Object> borHt = createSimple("borHt", Object.class);

    public final SimplePath<Object> borId = createSimple("borId", Object.class);

    public final SimplePath<Object> borNum = createSimple("borNum", Object.class);

    public final SimplePath<Object> borTtc = createSimple("borTtc", Object.class);

    public final SimplePath<Object> borTva = createSimple("borTva", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> gesCode = createSimple("gesCode", Object.class);

    public final SimplePath<Object> tboOrdre = createSimple("tboOrdre", Object.class);

    public QVBordJournal(String variable) {
        super(QVBordJournal.class, forVariable(variable), "GFC", "V_BORD_JOURNAL");
        addMetadata();
    }

    public QVBordJournal(String variable, String schema, String table) {
        super(QVBordJournal.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVBordJournal(Path<? extends QVBordJournal> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_BORD_JOURNAL");
        addMetadata();
    }

    public QVBordJournal(PathMetadata<?> metadata) {
        super(QVBordJournal.class, metadata, "GFC", "V_BORD_JOURNAL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borDate, ColumnMetadata.named("BOR_DATE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(borHt, ColumnMetadata.named("BOR_HT").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(borNum, ColumnMetadata.named("BOR_NUM").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(borTtc, ColumnMetadata.named("BOR_TTC").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(borTva, ColumnMetadata.named("BOR_TVA").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

