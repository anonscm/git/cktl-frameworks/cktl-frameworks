package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOrdreDePaiement is a Querydsl query type for QOrdreDePaiement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOrdreDePaiement extends com.mysema.query.sql.RelationalPathBase<QOrdreDePaiement> {

    private static final long serialVersionUID = -878026209;

    public static final QOrdreDePaiement ordreDePaiement = new QOrdreDePaiement("ORDRE_DE_PAIEMENT");

    public final NumberPath<Long> comOrdre = createNumber("comOrdre", Long.class);

    public final NumberPath<Long> ecrOrdre = createNumber("ecrOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> modOrdre = createNumber("modOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> odpDate = createDateTime("odpDate", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> odpDateSaisie = createDateTime("odpDateSaisie", java.sql.Timestamp.class);

    public final StringPath odpEtat = createString("odpEtat");

    public final NumberPath<java.math.BigDecimal> odpHt = createNumber("odpHt", java.math.BigDecimal.class);

    public final StringPath odpLibelle = createString("odpLibelle");

    public final StringPath odpLibelleFournisseur = createString("odpLibelleFournisseur");

    public final NumberPath<java.math.BigDecimal> odpMontantPaiement = createNumber("odpMontantPaiement", java.math.BigDecimal.class);

    public final NumberPath<Long> odpNumero = createNumber("odpNumero", Long.class);

    public final NumberPath<Long> odpOrdre = createNumber("odpOrdre", Long.class);

    public final StringPath odpReferencePaiement = createString("odpReferencePaiement");

    public final NumberPath<java.math.BigDecimal> odpTtc = createNumber("odpTtc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> odpTva = createNumber("odpTva", java.math.BigDecimal.class);

    public final NumberPath<Long> orgOrdre = createNumber("orgOrdre", Long.class);

    public final NumberPath<Long> oriOrdre = createNumber("oriOrdre", Long.class);

    public final NumberPath<Long> paiOrdre = createNumber("paiOrdre", Long.class);

    public final NumberPath<Long> ribOrdre = createNumber("ribOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QOrdreDePaiement> ordreDePaiementPk = createPrimaryKey(odpOrdre);

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> odpFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcriture> ordreDePaiementEcrOrdreFk = createForeignKey(ecrOrdre, "ECR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QComptabilite> ordreDePaiementComOrdreFk = createForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPaiement> ordreDePaiementPaiOrdreFk = createForeignKey(paiOrdre, "PAI_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> ordreDePaiementExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QModePaiement> ordreDePaiementModOrdreFk = createForeignKey(modOrdre, "MOD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRibfourUlr> odpRibOrdreFk = createForeignKey(ribOrdre, "RIB_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QOdpaiemDetailEcriture> _odpaiemDetailEcritureOdpFk = createInvForeignKey(odpOrdre, "ODP_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QVisaAvMission> _visaAvMissionOdpOrdreFk = createInvForeignKey(odpOrdre, "ODP_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QOdpaiementBrouillard> _odpaiementBrouillardOdpFk = createInvForeignKey(odpOrdre, "ODP_ORDRE");

    public QOrdreDePaiement(String variable) {
        super(QOrdreDePaiement.class, forVariable(variable), "GFC", "ORDRE_DE_PAIEMENT");
        addMetadata();
    }

    public QOrdreDePaiement(String variable, String schema, String table) {
        super(QOrdreDePaiement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOrdreDePaiement(Path<? extends QOrdreDePaiement> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ORDRE_DE_PAIEMENT");
        addMetadata();
    }

    public QOrdreDePaiement(PathMetadata<?> metadata) {
        super(QOrdreDePaiement.class, metadata, "GFC", "ORDRE_DE_PAIEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comOrdre, ColumnMetadata.named("COM_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ecrOrdre, ColumnMetadata.named("ECR_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(modOrdre, ColumnMetadata.named("MOD_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(odpDate, ColumnMetadata.named("ODP_DATE").withIndex(19).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(odpDateSaisie, ColumnMetadata.named("ODP_DATE_SAISIE").withIndex(20).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(odpEtat, ColumnMetadata.named("ODP_ETAT").withIndex(17).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(odpHt, ColumnMetadata.named("ODP_HT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(odpLibelle, ColumnMetadata.named("ODP_LIBELLE").withIndex(8).ofType(Types.VARCHAR).withSize(2000).notNull());
        addMetadata(odpLibelleFournisseur, ColumnMetadata.named("ODP_LIBELLE_FOURNISSEUR").withIndex(9).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(odpMontantPaiement, ColumnMetadata.named("ODP_MONTANT_PAIEMENT").withIndex(21).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(odpNumero, ColumnMetadata.named("ODP_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(odpOrdre, ColumnMetadata.named("ODP_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(odpReferencePaiement, ColumnMetadata.named("ODP_REFERENCE_PAIEMENT").withIndex(22).ofType(Types.VARCHAR).withSize(50));
        addMetadata(odpTtc, ColumnMetadata.named("ODP_TTC").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(odpTva, ColumnMetadata.named("ODP_TVA").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(orgOrdre, ColumnMetadata.named("ORG_ORDRE").withIndex(16).ofType(Types.DECIMAL).withSize(38));
        addMetadata(oriOrdre, ColumnMetadata.named("ORI_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(paiOrdre, ColumnMetadata.named("PAI_ORDRE").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ribOrdre, ColumnMetadata.named("RIB_ORDRE").withIndex(13).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(18).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

