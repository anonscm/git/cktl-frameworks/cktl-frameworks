package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVUtilisateurPreferenceOrgan is a Querydsl query type for QVUtilisateurPreferenceOrgan
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVUtilisateurPreferenceOrgan extends com.mysema.query.sql.RelationalPathBase<QVUtilisateurPreferenceOrgan> {

    private static final long serialVersionUID = -1503088792;

    public static final QVUtilisateurPreferenceOrgan vUtilisateurPreferenceOrgan = new QVUtilisateurPreferenceOrgan("V_UTILISATEUR_PREFERENCE_ORGAN");

    public final SimplePath<Object> prefId = createSimple("prefId", Object.class);

    public final SimplePath<Object> upId = createSimple("upId", Object.class);

    public final SimplePath<Object> upValue = createSimple("upValue", Object.class);

    public final SimplePath<Object> utlOrdre = createSimple("utlOrdre", Object.class);

    public QVUtilisateurPreferenceOrgan(String variable) {
        super(QVUtilisateurPreferenceOrgan.class, forVariable(variable), "GFC", "V_UTILISATEUR_PREFERENCE_ORGAN");
        addMetadata();
    }

    public QVUtilisateurPreferenceOrgan(String variable, String schema, String table) {
        super(QVUtilisateurPreferenceOrgan.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVUtilisateurPreferenceOrgan(Path<? extends QVUtilisateurPreferenceOrgan> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_UTILISATEUR_PREFERENCE_ORGAN");
        addMetadata();
    }

    public QVUtilisateurPreferenceOrgan(PathMetadata<?> metadata) {
        super(QVUtilisateurPreferenceOrgan.class, metadata, "GFC", "V_UTILISATEUR_PREFERENCE_ORGAN");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(prefId, ColumnMetadata.named("PREF_ID").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(upId, ColumnMetadata.named("UP_ID").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(upValue, ColumnMetadata.named("UP_VALUE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

