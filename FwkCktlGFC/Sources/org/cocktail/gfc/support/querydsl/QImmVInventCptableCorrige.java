package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVInventCptableCorrige is a Querydsl query type for QImmVInventCptableCorrige
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVInventCptableCorrige extends com.mysema.query.sql.RelationalPathBase<QImmVInventCptableCorrige> {

    private static final long serialVersionUID = 1896374882;

    public static final QImmVInventCptableCorrige immVInventCptableCorrige = new QImmVInventCptableCorrige("IMM_V_INVENT_CPTABLE_CORRIGE");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> invcAcquisition = createNumber("invcAcquisition", java.math.BigDecimal.class);

    public final NumberPath<Long> invcAcquisitionCorrige = createNumber("invcAcquisitionCorrige", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> invcMontantAmortissable = createNumber("invcMontantAmortissable", Long.class);

    public final NumberPath<Long> invcMontantOrvs = createNumber("invcMontantOrvs", Long.class);

    public final NumberPath<java.math.BigDecimal> invcMontantResiduel = createNumber("invcMontantResiduel", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> invcMtBudgetaire = createNumber("invcMtBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> invcResiduelCorrige = createNumber("invcResiduelCorrige", java.math.BigDecimal.class);

    public QImmVInventCptableCorrige(String variable) {
        super(QImmVInventCptableCorrige.class, forVariable(variable), "GFC", "IMM_V_INVENT_CPTABLE_CORRIGE");
        addMetadata();
    }

    public QImmVInventCptableCorrige(String variable, String schema, String table) {
        super(QImmVInventCptableCorrige.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVInventCptableCorrige(Path<? extends QImmVInventCptableCorrige> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_INVENT_CPTABLE_CORRIGE");
        addMetadata();
    }

    public QImmVInventCptableCorrige(PathMetadata<?> metadata) {
        super(QImmVInventCptableCorrige.class, metadata, "GFC", "IMM_V_INVENT_CPTABLE_CORRIGE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcAcquisition, ColumnMetadata.named("INVC_ACQUISITION").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invcAcquisitionCorrige, ColumnMetadata.named("INVC_ACQUISITION_CORRIGE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invcMontantAmortissable, ColumnMetadata.named("INVC_MONTANT_AMORTISSABLE").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcMontantOrvs, ColumnMetadata.named("INVC_MONTANT_ORVS").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcMontantResiduel, ColumnMetadata.named("INVC_MONTANT_RESIDUEL").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invcMtBudgetaire, ColumnMetadata.named("INVC_MT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invcResiduelCorrige, ColumnMetadata.named("INVC_RESIDUEL_CORRIGE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2));
    }

}

