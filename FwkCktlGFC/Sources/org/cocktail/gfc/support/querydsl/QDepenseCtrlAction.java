package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepenseCtrlAction is a Querydsl query type for QDepenseCtrlAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepenseCtrlAction extends com.mysema.query.sql.RelationalPathBase<QDepenseCtrlAction> {

    private static final long serialVersionUID = -1870347638;

    public static final QDepenseCtrlAction depenseCtrlAction = new QDepenseCtrlAction("DEPENSE_CTRL_ACTION");

    public final NumberPath<java.math.BigDecimal> dactHtSaisie = createNumber("dactHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dactId = createNumber("dactId", Long.class);

    public final NumberPath<java.math.BigDecimal> dactMontantBudgetaire = createNumber("dactMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dactTtcSaisie = createNumber("dactTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dactTvaSaisie = createNumber("dactTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDepenseCtrlAction> depenseCtrlActionPk = createPrimaryKey(dactId);

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> depenseCtrlActDepIdFk = createForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depenseCtrlActExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QDepenseCtrlAction(String variable) {
        super(QDepenseCtrlAction.class, forVariable(variable), "GFC", "DEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public QDepenseCtrlAction(String variable, String schema, String table) {
        super(QDepenseCtrlAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepenseCtrlAction(Path<? extends QDepenseCtrlAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public QDepenseCtrlAction(PathMetadata<?> metadata) {
        super(QDepenseCtrlAction.class, metadata, "GFC", "DEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dactHtSaisie, ColumnMetadata.named("DACT_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dactId, ColumnMetadata.named("DACT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dactMontantBudgetaire, ColumnMetadata.named("DACT_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dactTtcSaisie, ColumnMetadata.named("DACT_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dactTvaSaisie, ColumnMetadata.named("DACT_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

