package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVMarchesOptionCatalogue is a Querydsl query type for QVMarchesOptionCatalogue
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVMarchesOptionCatalogue extends com.mysema.query.sql.RelationalPathBase<QVMarchesOptionCatalogue> {

    private static final long serialVersionUID = 528415480;

    public static final QVMarchesOptionCatalogue vMarchesOptionCatalogue = new QVMarchesOptionCatalogue("V_MARCHES_OPTION_CATALOGUE");

    public final SimplePath<Object> catOrdre = createSimple("catOrdre", Object.class);

    public final SimplePath<Object> optDesc = createSimple("optDesc", Object.class);

    public final SimplePath<Object> optHtControle = createSimple("optHtControle", Object.class);

    public final SimplePath<Object> optHtDispo = createSimple("optHtDispo", Object.class);

    public final SimplePath<Object> optOrdre = createSimple("optOrdre", Object.class);

    public final SimplePath<Object> optPrixHt = createSimple("optPrixHt", Object.class);

    public final SimplePath<Object> optQteControle = createSimple("optQteControle", Object.class);

    public final SimplePath<Object> optQteDispo = createSimple("optQteDispo", Object.class);

    public final SimplePath<Object> optRef = createSimple("optRef", Object.class);

    public final SimplePath<Object> tvaId = createSimple("tvaId", Object.class);

    public QVMarchesOptionCatalogue(String variable) {
        super(QVMarchesOptionCatalogue.class, forVariable(variable), "GFC", "V_MARCHES_OPTION_CATALOGUE");
        addMetadata();
    }

    public QVMarchesOptionCatalogue(String variable, String schema, String table) {
        super(QVMarchesOptionCatalogue.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVMarchesOptionCatalogue(Path<? extends QVMarchesOptionCatalogue> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_MARCHES_OPTION_CATALOGUE");
        addMetadata();
    }

    public QVMarchesOptionCatalogue(PathMetadata<?> metadata) {
        super(QVMarchesOptionCatalogue.class, metadata, "GFC", "V_MARCHES_OPTION_CATALOGUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(catOrdre, ColumnMetadata.named("CAT_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(optDesc, ColumnMetadata.named("OPT_DESC").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(optHtControle, ColumnMetadata.named("OPT_HT_CONTROLE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(optHtDispo, ColumnMetadata.named("OPT_HT_DISPO").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(optOrdre, ColumnMetadata.named("OPT_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(optPrixHt, ColumnMetadata.named("OPT_PRIX_HT").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(optQteControle, ColumnMetadata.named("OPT_QTE_CONTROLE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(optQteDispo, ColumnMetadata.named("OPT_QTE_DISPO").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(optRef, ColumnMetadata.named("OPT_REF").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(10).ofType(Types.OTHER).withSize(0));
    }

}

