package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVEngageCtrlConvention is a Querydsl query type for QVEngageCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVEngageCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QVEngageCtrlConvention> {

    private static final long serialVersionUID = -1608310478;

    public static final QVEngageCtrlConvention vEngageCtrlConvention = new QVEngageCtrlConvention("V_ENGAGE_CTRL_CONVENTION");

    public final SimplePath<Object> convOrdre = createSimple("convOrdre", Object.class);

    public final SimplePath<Object> econDateSaisie = createSimple("econDateSaisie", Object.class);

    public final SimplePath<Object> econHtSaisie = createSimple("econHtSaisie", Object.class);

    public final SimplePath<Object> econId = createSimple("econId", Object.class);

    public final SimplePath<Object> econMontantBudgetaire = createSimple("econMontantBudgetaire", Object.class);

    public final SimplePath<Object> econMontantBudgetaireReste = createSimple("econMontantBudgetaireReste", Object.class);

    public final SimplePath<Object> econTtcSaisie = createSimple("econTtcSaisie", Object.class);

    public final SimplePath<Object> econTvaSaisie = createSimple("econTvaSaisie", Object.class);

    public final SimplePath<Object> engId = createSimple("engId", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public QVEngageCtrlConvention(String variable) {
        super(QVEngageCtrlConvention.class, forVariable(variable), "GFC", "V_ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public QVEngageCtrlConvention(String variable, String schema, String table) {
        super(QVEngageCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVEngageCtrlConvention(Path<? extends QVEngageCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public QVEngageCtrlConvention(PathMetadata<?> metadata) {
        super(QVEngageCtrlConvention.class, metadata, "GFC", "V_ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(econDateSaisie, ColumnMetadata.named("ECON_DATE_SAISIE").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(econHtSaisie, ColumnMetadata.named("ECON_HT_SAISIE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(econId, ColumnMetadata.named("ECON_ID").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaire, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaireReste, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE_RESTE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(econTtcSaisie, ColumnMetadata.named("ECON_TTC_SAISIE").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(econTvaSaisie, ColumnMetadata.named("ECON_TVA_SAISIE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

