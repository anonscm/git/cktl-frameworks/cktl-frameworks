package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmDegressif is a Querydsl query type for QImmDegressif
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmDegressif extends com.mysema.query.sql.RelationalPathBase<QImmDegressif> {

    private static final long serialVersionUID = -932849298;

    public static final QImmDegressif immDegressif = new QImmDegressif("IMM_DEGRESSIF");

    public final DateTimePath<java.sql.Timestamp> dgrfDateDebut = createDateTime("dgrfDateDebut", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dgrfDateFin = createDateTime("dgrfDateFin", java.sql.Timestamp.class);

    public final NumberPath<Long> dgrfId = createNumber("dgrfId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmDegressif> immDegressifPk = createPrimaryKey(dgrfId);

    public final com.mysema.query.sql.ForeignKey<QImmDegressifCoef> _degressifDgrfIdFk = createInvForeignKey(dgrfId, "DGRF_ID");

    public QImmDegressif(String variable) {
        super(QImmDegressif.class, forVariable(variable), "GFC", "IMM_DEGRESSIF");
        addMetadata();
    }

    public QImmDegressif(String variable, String schema, String table) {
        super(QImmDegressif.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmDegressif(Path<? extends QImmDegressif> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_DEGRESSIF");
        addMetadata();
    }

    public QImmDegressif(PathMetadata<?> metadata) {
        super(QImmDegressif.class, metadata, "GFC", "IMM_DEGRESSIF");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dgrfDateDebut, ColumnMetadata.named("DGRF_DATE_DEBUT").withIndex(2).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dgrfDateFin, ColumnMetadata.named("DGRF_DATE_FIN").withIndex(3).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dgrfId, ColumnMetadata.named("DGRF_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

