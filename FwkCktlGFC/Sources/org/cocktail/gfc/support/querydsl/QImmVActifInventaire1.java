package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVActifInventaire1 is a Querydsl query type for QImmVActifInventaire1
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVActifInventaire1 extends com.mysema.query.sql.RelationalPathBase<QImmVActifInventaire1> {

    private static final long serialVersionUID = 253170107;

    public static final QImmVActifInventaire1 immVActifInventaire1 = new QImmVActifInventaire1("IMM_V_ACTIF_INVENTAIRE_1");

    public final NumberPath<java.math.BigDecimal> amoAcquisition = createNumber("amoAcquisition", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amoAnnuite = createNumber("amoAnnuite", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amoCumul = createNumber("amoCumul", java.math.BigDecimal.class);

    public final NumberPath<Long> amoExer = createNumber("amoExer", Long.class);

    public final NumberPath<java.math.BigDecimal> amoResiduel = createNumber("amoResiduel", java.math.BigDecimal.class);

    public final NumberPath<Long> exercice = createNumber("exercice", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> invcAcquisitionCorrige = createNumber("invcAcquisitionCorrige", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final StringPath numero = createString("numero");

    public final StringPath pcoNum = createString("pcoNum");

    public QImmVActifInventaire1(String variable) {
        super(QImmVActifInventaire1.class, forVariable(variable), "GFC", "IMM_V_ACTIF_INVENTAIRE_1");
        addMetadata();
    }

    public QImmVActifInventaire1(String variable, String schema, String table) {
        super(QImmVActifInventaire1.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVActifInventaire1(Path<? extends QImmVActifInventaire1> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_ACTIF_INVENTAIRE_1");
        addMetadata();
    }

    public QImmVActifInventaire1(PathMetadata<?> metadata) {
        super(QImmVActifInventaire1.class, metadata, "GFC", "IMM_V_ACTIF_INVENTAIRE_1");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(amoAcquisition, ColumnMetadata.named("AMO_ACQUISITION").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(amoAnnuite, ColumnMetadata.named("AMO_ANNUITE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(amoCumul, ColumnMetadata.named("AMO_CUMUL").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(amoExer, ColumnMetadata.named("AMO_EXER").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(amoResiduel, ColumnMetadata.named("AMO_RESIDUEL").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(invcAcquisitionCorrige, ColumnMetadata.named("INVC_ACQUISITION_CORRIGE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(11).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(numero, ColumnMetadata.named("NUMERO").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(9).ofType(Types.VARCHAR).withSize(20));
    }

}

