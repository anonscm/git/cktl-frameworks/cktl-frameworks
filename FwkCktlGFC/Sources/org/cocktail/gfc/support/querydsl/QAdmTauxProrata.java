package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTauxProrata is a Querydsl query type for QAdmTauxProrata
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTauxProrata extends com.mysema.query.sql.RelationalPathBase<QAdmTauxProrata> {

    private static final long serialVersionUID = -2077608624;

    public static final QAdmTauxProrata admTauxProrata = new QAdmTauxProrata("ADM_TAUX_PRORATA");

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<java.math.BigDecimal> tapTaux = createNumber("tapTaux", java.math.BigDecimal.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmTauxProrata> sysC0076586 = createPrimaryKey(tapId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admTauxProrataTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QRecRecette> _recRecetteTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEbProrata> _admEbProrataTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> _commandeBudgetTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> _pdepenseBudgetTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> _engageBudgetTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationBudget> _reimpBudgetTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestationBudgetClient> _pbcTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QFacture> _factureTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFactureTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> _depenseBudgetTapIdFk = createInvForeignKey(tapId, "TAP_ID");

    public QAdmTauxProrata(String variable) {
        super(QAdmTauxProrata.class, forVariable(variable), "GFC", "ADM_TAUX_PRORATA");
        addMetadata();
    }

    public QAdmTauxProrata(String variable, String schema, String table) {
        super(QAdmTauxProrata.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTauxProrata(Path<? extends QAdmTauxProrata> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TAUX_PRORATA");
        addMetadata();
    }

    public QAdmTauxProrata(PathMetadata<?> metadata) {
        super(QAdmTauxProrata.class, metadata, "GFC", "ADM_TAUX_PRORATA");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapTaux, ColumnMetadata.named("TAP_TAUX").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

