package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEngageCtrlHorsMarche is a Querydsl query type for QEngageCtrlHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEngageCtrlHorsMarche extends com.mysema.query.sql.RelationalPathBase<QEngageCtrlHorsMarche> {

    private static final long serialVersionUID = -1059162313;

    public static final QEngageCtrlHorsMarche engageCtrlHorsMarche = new QEngageCtrlHorsMarche("ENGAGE_CTRL_HORS_MARCHE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> ehomDateSaisie = createDateTime("ehomDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> ehomHtReste = createNumber("ehomHtReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomHtSaisie = createNumber("ehomHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> ehomId = createNumber("ehomId", Long.class);

    public final NumberPath<java.math.BigDecimal> ehomMontantBudgetaire = createNumber("ehomMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomMontantBudgetaireReste = createNumber("ehomMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomTtcSaisie = createNumber("ehomTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomTvaSaisie = createNumber("ehomTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QEngageCtrlHorsMarche> engageCtrlHorsMarchePk = createPrimaryKey(ehomId);

    public final com.mysema.query.sql.ForeignKey<QCodeExer> engageCtrlHmCeOrdreFk = createForeignKey(ceOrdre, "CE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypeAchat> engageCtrlHmTypaIdFk = createForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> engageCtrlHmEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> engageCtrlHmExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QEngageCtrlHorsMarche(String variable) {
        super(QEngageCtrlHorsMarche.class, forVariable(variable), "GFC", "ENGAGE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QEngageCtrlHorsMarche(String variable, String schema, String table) {
        super(QEngageCtrlHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEngageCtrlHorsMarche(Path<? extends QEngageCtrlHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ENGAGE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QEngageCtrlHorsMarche(PathMetadata<?> metadata) {
        super(QEngageCtrlHorsMarche.class, metadata, "GFC", "ENGAGE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ehomDateSaisie, ColumnMetadata.named("EHOM_DATE_SAISIE").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(ehomHtReste, ColumnMetadata.named("EHOM_HT_RESTE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomHtSaisie, ColumnMetadata.named("EHOM_HT_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomId, ColumnMetadata.named("EHOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ehomMontantBudgetaire, ColumnMetadata.named("EHOM_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomMontantBudgetaireReste, ColumnMetadata.named("EHOM_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomTtcSaisie, ColumnMetadata.named("EHOM_TTC_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomTvaSaisie, ColumnMetadata.named("EHOM_TVA_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

