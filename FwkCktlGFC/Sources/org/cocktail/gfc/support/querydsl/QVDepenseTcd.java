package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepenseTcd is a Querydsl query type for QVDepenseTcd
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepenseTcd extends com.mysema.query.sql.RelationalPathBase<QVDepenseTcd> {

    private static final long serialVersionUID = -1159682144;

    public static final QVDepenseTcd vDepenseTcd = new QVDepenseTcd("V_DEPENSE_TCD");

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final StringPath tcdCode = createString("tcdCode");

    public final StringPath tcdSect = createString("tcdSect");

    public QVDepenseTcd(String variable) {
        super(QVDepenseTcd.class, forVariable(variable), "GFC", "V_DEPENSE_TCD");
        addMetadata();
    }

    public QVDepenseTcd(String variable, String schema, String table) {
        super(QVDepenseTcd.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepenseTcd(Path<? extends QVDepenseTcd> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSE_TCD");
        addMetadata();
    }

    public QVDepenseTcd(PathMetadata<?> metadata) {
        super(QVDepenseTcd.class, metadata, "GFC", "V_DEPENSE_TCD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdCode, ColumnMetadata.named("TCD_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(tcdSect, ColumnMetadata.named("TCD_SECT").withIndex(3).ofType(Types.CHAR).withSize(1).notNull());
    }

}

