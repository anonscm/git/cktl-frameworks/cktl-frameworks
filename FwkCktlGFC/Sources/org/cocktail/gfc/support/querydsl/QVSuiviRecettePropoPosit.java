package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviRecettePropoPosit is a Querydsl query type for QVSuiviRecettePropoPosit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviRecettePropoPosit extends com.mysema.query.sql.RelationalPathBase<QVSuiviRecettePropoPosit> {

    private static final long serialVersionUID = 819683934;

    public static final QVSuiviRecettePropoPosit vSuiviRecettePropoPosit = new QVSuiviRecettePropoPosit("V_SUIVI_RECETTE_PROPO_POSIT");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> orgIdPosit = createSimple("orgIdPosit", Object.class);

    public final SimplePath<Object> orgIdPositPere = createSimple("orgIdPositPere", Object.class);

    public final SimplePath<Object> orgIdPropo = createSimple("orgIdPropo", Object.class);

    public final SimplePath<Object> tcdOrdrePosit = createSimple("tcdOrdrePosit", Object.class);

    public final SimplePath<Object> totalPosit = createSimple("totalPosit", Object.class);

    public final SimplePath<Object> totalPropo = createSimple("totalPropo", Object.class);

    public QVSuiviRecettePropoPosit(String variable) {
        super(QVSuiviRecettePropoPosit.class, forVariable(variable), "GFC", "V_SUIVI_RECETTE_PROPO_POSIT");
        addMetadata();
    }

    public QVSuiviRecettePropoPosit(String variable, String schema, String table) {
        super(QVSuiviRecettePropoPosit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviRecettePropoPosit(Path<? extends QVSuiviRecettePropoPosit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_RECETTE_PROPO_POSIT");
        addMetadata();
    }

    public QVSuiviRecettePropoPosit(PathMetadata<?> metadata) {
        super(QVSuiviRecettePropoPosit.class, metadata, "GFC", "V_SUIVI_RECETTE_PROPO_POSIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPosit, ColumnMetadata.named("ORG_ID_POSIT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPositPere, ColumnMetadata.named("ORG_ID_POSIT_PERE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPropo, ColumnMetadata.named("ORG_ID_PROPO").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdrePosit, ColumnMetadata.named("TCD_ORDRE_POSIT").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPosit, ColumnMetadata.named("TOTAL_POSIT").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPropo, ColumnMetadata.named("TOTAL_PROPO").withIndex(7).ofType(Types.OTHER).withSize(0));
    }

}

