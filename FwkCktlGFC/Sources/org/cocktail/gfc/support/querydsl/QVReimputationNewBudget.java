package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVReimputationNewBudget is a Querydsl query type for QVReimputationNewBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVReimputationNewBudget extends com.mysema.query.sql.RelationalPathBase<QVReimputationNewBudget> {

    private static final long serialVersionUID = 2075887635;

    public static final QVReimputationNewBudget vReimputationNewBudget = new QVReimputationNewBudget("V_REIMPUTATION_NEW_BUDGET");

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> rebuId = createNumber("rebuId", Long.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QVReimputationNewBudget(String variable) {
        super(QVReimputationNewBudget.class, forVariable(variable), "GFC", "V_REIMPUTATION_NEW_BUDGET");
        addMetadata();
    }

    public QVReimputationNewBudget(String variable, String schema, String table) {
        super(QVReimputationNewBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVReimputationNewBudget(Path<? extends QVReimputationNewBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_REIMPUTATION_NEW_BUDGET");
        addMetadata();
    }

    public QVReimputationNewBudget(PathMetadata<?> metadata) {
        super(QVReimputationNewBudget.class, metadata, "GFC", "V_REIMPUTATION_NEW_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(rebuId, ColumnMetadata.named("REBU_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
    }

}

