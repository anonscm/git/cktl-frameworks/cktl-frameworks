package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEngageCtrlMarche is a Querydsl query type for QEngageCtrlMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEngageCtrlMarche extends com.mysema.query.sql.RelationalPathBase<QEngageCtrlMarche> {

    private static final long serialVersionUID = 1160997391;

    public static final QEngageCtrlMarche engageCtrlMarche = new QEngageCtrlMarche("ENGAGE_CTRL_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> emarDateSaisie = createDateTime("emarDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> emarHtReste = createNumber("emarHtReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarHtSaisie = createNumber("emarHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> emarId = createNumber("emarId", Long.class);

    public final NumberPath<java.math.BigDecimal> emarMontantBudgetaire = createNumber("emarMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarMontantBudgetaireReste = createNumber("emarMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarTtcSaisie = createNumber("emarTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarTvaSaisie = createNumber("emarTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QEngageCtrlMarche> engageCtrlMarchePk = createPrimaryKey(emarId);

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> engageCtrlMarcEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QAttribution> engageCtrlMarcAttOrdreFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> engageCtrlMarcExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QEngageCtrlMarche(String variable) {
        super(QEngageCtrlMarche.class, forVariable(variable), "GFC", "ENGAGE_CTRL_MARCHE");
        addMetadata();
    }

    public QEngageCtrlMarche(String variable, String schema, String table) {
        super(QEngageCtrlMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEngageCtrlMarche(Path<? extends QEngageCtrlMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ENGAGE_CTRL_MARCHE");
        addMetadata();
    }

    public QEngageCtrlMarche(PathMetadata<?> metadata) {
        super(QEngageCtrlMarche.class, metadata, "GFC", "ENGAGE_CTRL_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(emarDateSaisie, ColumnMetadata.named("EMAR_DATE_SAISIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(emarHtReste, ColumnMetadata.named("EMAR_HT_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarHtSaisie, ColumnMetadata.named("EMAR_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarId, ColumnMetadata.named("EMAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(emarMontantBudgetaire, ColumnMetadata.named("EMAR_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarMontantBudgetaireReste, ColumnMetadata.named("EMAR_MONTANT_BUDGETAIRE_RESTE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarTtcSaisie, ColumnMetadata.named("EMAR_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarTvaSaisie, ColumnMetadata.named("EMAR_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

