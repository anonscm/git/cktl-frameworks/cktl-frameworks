package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVCommande is a Querydsl query type for QImmVCommande
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVCommande extends com.mysema.query.sql.RelationalPathBase<QImmVCommande> {

    private static final long serialVersionUID = 140495560;

    public static final QImmVCommande immVCommande = new QImmVCommande("IMM_V_COMMANDE");

    public final DateTimePath<java.sql.Timestamp> commDate = createDateTime("commDate", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> commDateCreation = createDateTime("commDateCreation", java.sql.Timestamp.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final StringPath commLibelle = createString("commLibelle");

    public final NumberPath<Long> commNumero = createNumber("commNumero", Long.class);

    public final StringPath commReference = createString("commReference");

    public final NumberPath<Long> devId = createNumber("devId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final NumberPath<Long> tyetIdImprimable = createNumber("tyetIdImprimable", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QImmVCommande(String variable) {
        super(QImmVCommande.class, forVariable(variable), "GFC", "IMM_V_COMMANDE");
        addMetadata();
    }

    public QImmVCommande(String variable, String schema, String table) {
        super(QImmVCommande.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVCommande(Path<? extends QImmVCommande> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_COMMANDE");
        addMetadata();
    }

    public QImmVCommande(PathMetadata<?> metadata) {
        super(QImmVCommande.class, metadata, "GFC", "IMM_V_COMMANDE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commDate, ColumnMetadata.named("COMM_DATE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(commDateCreation, ColumnMetadata.named("COMM_DATE_CREATION").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commLibelle, ColumnMetadata.named("COMM_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(commNumero, ColumnMetadata.named("COMM_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commReference, ColumnMetadata.named("COMM_REFERENCE").withIndex(6).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(devId, ColumnMetadata.named("DEV_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(13).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetIdImprimable, ColumnMetadata.named("TYET_ID_IMPRIMABLE").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

