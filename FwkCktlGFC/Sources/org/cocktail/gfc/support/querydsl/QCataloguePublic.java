package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCataloguePublic is a Querydsl query type for QCataloguePublic
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCataloguePublic extends com.mysema.query.sql.RelationalPathBase<QCataloguePublic> {

    private static final long serialVersionUID = -343735023;

    public static final QCataloguePublic cataloguePublic = new QCataloguePublic("CATALOGUE_PUBLIC");

    public final NumberPath<Long> capId = createNumber("capId", Long.class);

    public final NumberPath<java.math.BigDecimal> capPourcentage = createNumber("capPourcentage", java.math.BigDecimal.class);

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final NumberPath<Long> typuId = createNumber("typuId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCataloguePublic> cataloguePublicPk = createPrimaryKey(capId);

    public final com.mysema.query.sql.ForeignKey<QCataloguePrestation> capCatIdFk = createForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QTypePublic> capTypuIdFk = createForeignKey(typuId, "TYPU_ID");

    public QCataloguePublic(String variable) {
        super(QCataloguePublic.class, forVariable(variable), "GFC", "CATALOGUE_PUBLIC");
        addMetadata();
    }

    public QCataloguePublic(String variable, String schema, String table) {
        super(QCataloguePublic.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCataloguePublic(Path<? extends QCataloguePublic> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CATALOGUE_PUBLIC");
        addMetadata();
    }

    public QCataloguePublic(PathMetadata<?> metadata) {
        super(QCataloguePublic.class, metadata, "GFC", "CATALOGUE_PUBLIC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(capId, ColumnMetadata.named("CAP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(capPourcentage, ColumnMetadata.named("CAP_POURCENTAGE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(typuId, ColumnMetadata.named("TYPU_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

