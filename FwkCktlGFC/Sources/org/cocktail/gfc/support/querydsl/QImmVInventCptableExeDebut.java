package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVInventCptableExeDebut is a Querydsl query type for QImmVInventCptableExeDebut
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVInventCptableExeDebut extends com.mysema.query.sql.RelationalPathBase<QImmVInventCptableExeDebut> {

    private static final long serialVersionUID = 1126514119;

    public static final QImmVInventCptableExeDebut immVInventCptableExeDebut = new QImmVInventCptableExeDebut("IMM_V_INVENT_CPTABLE_EXE_DEBUT");

    public final NumberPath<Long> exeOrdre = createNumber("exeOrdre", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public QImmVInventCptableExeDebut(String variable) {
        super(QImmVInventCptableExeDebut.class, forVariable(variable), "GFC", "IMM_V_INVENT_CPTABLE_EXE_DEBUT");
        addMetadata();
    }

    public QImmVInventCptableExeDebut(String variable, String schema, String table) {
        super(QImmVInventCptableExeDebut.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVInventCptableExeDebut(Path<? extends QImmVInventCptableExeDebut> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_INVENT_CPTABLE_EXE_DEBUT");
        addMetadata();
    }

    public QImmVInventCptableExeDebut(PathMetadata<?> metadata) {
        super(QImmVInventCptableExeDebut.class, metadata, "GFC", "IMM_V_INVENT_CPTABLE_EXE_DEBUT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
    }

}

