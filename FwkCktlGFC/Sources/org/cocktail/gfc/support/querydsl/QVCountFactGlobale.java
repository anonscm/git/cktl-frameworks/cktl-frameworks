package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCountFactGlobale is a Querydsl query type for QVCountFactGlobale
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCountFactGlobale extends com.mysema.query.sql.RelationalPathBase<QVCountFactGlobale> {

    private static final long serialVersionUID = 1201992252;

    public static final QVCountFactGlobale vCountFactGlobale = new QVCountFactGlobale("V_COUNT_FACT_GLOBALE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> cnt = createNumber("cnt", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public QVCountFactGlobale(String variable) {
        super(QVCountFactGlobale.class, forVariable(variable), "GFC", "V_COUNT_FACT_GLOBALE");
        addMetadata();
    }

    public QVCountFactGlobale(String variable, String schema, String table) {
        super(QVCountFactGlobale.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCountFactGlobale(Path<? extends QVCountFactGlobale> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_COUNT_FACT_GLOBALE");
        addMetadata();
    }

    public QVCountFactGlobale(PathMetadata<?> metadata) {
        super(QVCountFactGlobale.class, metadata, "GFC", "V_COUNT_FACT_GLOBALE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cnt, ColumnMetadata.named("CNT").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

