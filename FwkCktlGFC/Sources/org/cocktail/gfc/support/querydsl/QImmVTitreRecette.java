package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVTitreRecette is a Querydsl query type for QImmVTitreRecette
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVTitreRecette extends com.mysema.query.sql.RelationalPathBase<QImmVTitreRecette> {

    private static final long serialVersionUID = 1532215756;

    public static final QImmVTitreRecette immVTitreRecette = new QImmVTitreRecette("IMM_V_TITRE_RECETTE");

    public final NumberPath<Long> borId = createNumber("borId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> montantDisponible = createNumber("montantDisponible", Long.class);

    public final NumberPath<Long> montantReduction = createNumber("montantReduction", Long.class);

    public final NumberPath<Long> montantUtilise = createNumber("montantUtilise", Long.class);

    public final NumberPath<Long> orgOrdre = createNumber("orgOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<java.math.BigDecimal> titHt = createNumber("titHt", java.math.BigDecimal.class);

    public final NumberPath<Long> titId = createNumber("titId", Long.class);

    public final StringPath titLibelle = createString("titLibelle");

    public final NumberPath<Long> titNumero = createNumber("titNumero", Long.class);

    public final NumberPath<java.math.BigDecimal> titTtc = createNumber("titTtc", java.math.BigDecimal.class);

    public QImmVTitreRecette(String variable) {
        super(QImmVTitreRecette.class, forVariable(variable), "GFC", "IMM_V_TITRE_RECETTE");
        addMetadata();
    }

    public QImmVTitreRecette(String variable, String schema, String table) {
        super(QImmVTitreRecette.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVTitreRecette(Path<? extends QImmVTitreRecette> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_TITRE_RECETTE");
        addMetadata();
    }

    public QImmVTitreRecette(PathMetadata<?> metadata) {
        super(QImmVTitreRecette.class, metadata, "GFC", "IMM_V_TITRE_RECETTE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(38));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(4).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(montantDisponible, ColumnMetadata.named("MONTANT_DISPONIBLE").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(montantReduction, ColumnMetadata.named("MONTANT_REDUCTION").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(montantUtilise, ColumnMetadata.named("MONTANT_UTILISE").withIndex(13).ofType(Types.DECIMAL).withSize(0));
        addMetadata(orgOrdre, ColumnMetadata.named("ORG_ORDRE").withIndex(11).ofType(Types.DECIMAL).withSize(38));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(2).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(titHt, ColumnMetadata.named("TIT_HT").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titLibelle, ColumnMetadata.named("TIT_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(titNumero, ColumnMetadata.named("TIT_NUMERO").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titTtc, ColumnMetadata.named("TIT_TTC").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
    }

}

