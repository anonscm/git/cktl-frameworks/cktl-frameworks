package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmDbVersionVirt is a Querydsl query type for QVAdmDbVersionVirt
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmDbVersionVirt extends com.mysema.query.sql.RelationalPathBase<QVAdmDbVersionVirt> {

    private static final long serialVersionUID = 1616504570;

    public static final QVAdmDbVersionVirt vAdmDbVersionVirt = new QVAdmDbVersionVirt("V_ADM_DB_VERSION_VIRT");

    public final StringPath tyavComment = createString("tyavComment");

    public final DateTimePath<java.sql.Timestamp> tyavDate = createDateTime("tyavDate", java.sql.Timestamp.class);

    public final NumberPath<Long> tyavId = createNumber("tyavId", Long.class);

    public final StringPath tyavVersion = createString("tyavVersion");

    public QVAdmDbVersionVirt(String variable) {
        super(QVAdmDbVersionVirt.class, forVariable(variable), "GFC", "V_ADM_DB_VERSION_VIRT");
        addMetadata();
    }

    public QVAdmDbVersionVirt(String variable, String schema, String table) {
        super(QVAdmDbVersionVirt.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmDbVersionVirt(Path<? extends QVAdmDbVersionVirt> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_DB_VERSION_VIRT");
        addMetadata();
    }

    public QVAdmDbVersionVirt(PathMetadata<?> metadata) {
        super(QVAdmDbVersionVirt.class, metadata, "GFC", "V_ADM_DB_VERSION_VIRT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tyavComment, ColumnMetadata.named("TYAV_COMMENT").withIndex(3).ofType(Types.VARCHAR).withSize(100));
        addMetadata(tyavDate, ColumnMetadata.named("TYAV_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(tyavId, ColumnMetadata.named("TYAV_ID").withIndex(4).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(tyavVersion, ColumnMetadata.named("TYAV_VERSION").withIndex(1).ofType(Types.VARCHAR).withSize(10).notNull());
    }

}

