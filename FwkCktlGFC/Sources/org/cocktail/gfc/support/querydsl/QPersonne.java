package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPersonne is a Querydsl query type for QPersonne
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPersonne extends com.mysema.query.sql.RelationalPathBase<QPersonne> {

    private static final long serialVersionUID = 1597769677;

    public static final QPersonne personne = new QPersonne("PERSONNE");

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final StringPath persLc = createString("persLc");

    public final StringPath persLibelle = createString("persLibelle");

    public final StringPath persNomptr = createString("persNomptr");

    public final NumberPath<Long> persOrdre = createNumber("persOrdre", Long.class);

    public final StringPath persType = createString("persType");

    public final com.mysema.query.sql.PrimaryKey<QPersonne> personnePk = createPrimaryKey(persId);

    public QPersonne(String variable) {
        super(QPersonne.class, forVariable(variable), "GRHUM", "PERSONNE");
        addMetadata();
    }

    public QPersonne(String variable, String schema, String table) {
        super(QPersonne.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPersonne(Path<? extends QPersonne> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "PERSONNE");
        addMetadata();
    }

    public QPersonne(PathMetadata<?> metadata) {
        super(QPersonne.class, metadata, "GRHUM", "PERSONNE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persLc, ColumnMetadata.named("PERS_LC").withIndex(5).ofType(Types.VARCHAR).withSize(40));
        addMetadata(persLibelle, ColumnMetadata.named("PERS_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(persNomptr, ColumnMetadata.named("PERS_NOMPTR").withIndex(6).ofType(Types.VARCHAR).withSize(80));
        addMetadata(persOrdre, ColumnMetadata.named("PERS_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persType, ColumnMetadata.named("PERS_TYPE").withIndex(2).ofType(Types.VARCHAR).withSize(5).notNull());
    }

}

