package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBdfCalendrier is a Querydsl query type for QBdfCalendrier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBdfCalendrier extends com.mysema.query.sql.RelationalPathBase<QBdfCalendrier> {

    private static final long serialVersionUID = -2011042540;

    public static final QBdfCalendrier bdfCalendrier = new QBdfCalendrier("BDF_CALENDRIER");

    public final StringPath bdfcalDateBdf = createString("bdfcalDateBdf");

    public final StringPath bdfcalDateIso = createString("bdfcalDateIso");

    public final com.mysema.query.sql.PrimaryKey<QBdfCalendrier> bdfCalendrierPk = createPrimaryKey(bdfcalDateIso);

    public QBdfCalendrier(String variable) {
        super(QBdfCalendrier.class, forVariable(variable), "GFC", "BDF_CALENDRIER");
        addMetadata();
    }

    public QBdfCalendrier(String variable, String schema, String table) {
        super(QBdfCalendrier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBdfCalendrier(Path<? extends QBdfCalendrier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BDF_CALENDRIER");
        addMetadata();
    }

    public QBdfCalendrier(PathMetadata<?> metadata) {
        super(QBdfCalendrier.class, metadata, "GFC", "BDF_CALENDRIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bdfcalDateBdf, ColumnMetadata.named("BDFCAL_DATE_BDF").withIndex(2).ofType(Types.VARCHAR).withSize(6).notNull());
        addMetadata(bdfcalDateIso, ColumnMetadata.named("BDFCAL_DATE_ISO").withIndex(1).ofType(Types.VARCHAR).withSize(8).notNull());
    }

}

