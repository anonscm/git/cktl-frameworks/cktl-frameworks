package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypeCredit is a Querydsl query type for QAdmTypeCredit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypeCredit extends com.mysema.query.sql.RelationalPathBase<QAdmTypeCredit> {

    private static final long serialVersionUID = 571364284;

    public static final QAdmTypeCredit admTypeCredit = new QAdmTypeCredit("ADM_TYPE_CREDIT");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath tcdAbrege = createString("tcdAbrege");

    public final StringPath tcdBudget = createString("tcdBudget");

    public final StringPath tcdCode = createString("tcdCode");

    public final StringPath tcdLibelle = createString("tcdLibelle");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> tcdPresident = createNumber("tcdPresident", java.math.BigDecimal.class);

    public final StringPath tcdSect = createString("tcdSect");

    public final StringPath tcdType = createString("tcdType");

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmTypeCredit> sysC0076742 = createPrimaryKey(tcdOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admTypeCreditTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> admTypeCreditExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFactureTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputationBudget> _reimpBudgetTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QSbDepense> _depenseTcFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QSbRecette> _recetteTcFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPrestationBudgetClient> _pbcTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlancoCredit> _plancoCreditTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> _engageBudgetTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacture> _factureTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCptDepense> _depenseTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> _commandeBudgetTcdOrdreFk = createInvForeignKey(tcdOrdre, "TCD_ORDRE");

    public QAdmTypeCredit(String variable) {
        super(QAdmTypeCredit.class, forVariable(variable), "GFC", "ADM_TYPE_CREDIT");
        addMetadata();
    }

    public QAdmTypeCredit(String variable, String schema, String table) {
        super(QAdmTypeCredit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypeCredit(Path<? extends QAdmTypeCredit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_CREDIT");
        addMetadata();
    }

    public QAdmTypeCredit(PathMetadata<?> metadata) {
        super(QAdmTypeCredit.class, metadata, "GFC", "ADM_TYPE_CREDIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(tcdAbrege, ColumnMetadata.named("TCD_ABREGE").withIndex(5).ofType(Types.VARCHAR).withSize(12).notNull());
        addMetadata(tcdBudget, ColumnMetadata.named("TCD_BUDGET").withIndex(9).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tcdCode, ColumnMetadata.named("TCD_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(tcdLibelle, ColumnMetadata.named("TCD_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(40).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdPresident, ColumnMetadata.named("TCD_PRESIDENT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(tcdSect, ColumnMetadata.named("TCD_SECT").withIndex(6).ofType(Types.CHAR).withSize(1).notNull());
        addMetadata(tcdType, ColumnMetadata.named("TCD_TYPE").withIndex(8).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

