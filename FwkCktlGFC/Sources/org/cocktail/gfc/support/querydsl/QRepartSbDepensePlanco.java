package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRepartSbDepensePlanco is a Querydsl query type for QRepartSbDepensePlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRepartSbDepensePlanco extends com.mysema.query.sql.RelationalPathBase<QRepartSbDepensePlanco> {

    private static final long serialVersionUID = -1365528247;

    public static final QRepartSbDepensePlanco repartSbDepensePlanco = new QRepartSbDepensePlanco("REPART_SB_DEPENSE_PLANCO");

    public final NumberPath<java.math.BigDecimal> montantHt = createNumber("montantHt", java.math.BigDecimal.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> sdOrdre = createNumber("sdOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRepartSbDepensePlanco> repartSbDepensePlancoPk = createPrimaryKey(pcoNum, sdOrdre);

    public QRepartSbDepensePlanco(String variable) {
        super(QRepartSbDepensePlanco.class, forVariable(variable), "GFC", "REPART_SB_DEPENSE_PLANCO");
        addMetadata();
    }

    public QRepartSbDepensePlanco(String variable, String schema, String table) {
        super(QRepartSbDepensePlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRepartSbDepensePlanco(Path<? extends QRepartSbDepensePlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REPART_SB_DEPENSE_PLANCO");
        addMetadata();
    }

    public QRepartSbDepensePlanco(PathMetadata<?> metadata) {
        super(QRepartSbDepensePlanco.class, metadata, "GFC", "REPART_SB_DEPENSE_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(montantHt, ColumnMetadata.named("MONTANT_HT").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(sdOrdre, ColumnMetadata.named("SD_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

