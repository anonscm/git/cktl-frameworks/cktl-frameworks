package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVConsoCn is a Querydsl query type for QVConsoCn
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVConsoCn extends com.mysema.query.sql.RelationalPathBase<QVConsoCn> {

    private static final long serialVersionUID = -1031010540;

    public static final QVConsoCn vConsoCn = new QVConsoCn("V_CONSO_CN");

    public final StringPath code = createString("code");

    public final NumberPath<Integer> exercice = createNumber("exercice", Integer.class);

    public final StringPath fouCode = createString("fouCode");

    public final StringPath fournisseur = createString("fournisseur");

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Long> liquide = createNumber("liquide", Long.class);

    public final StringPath lot = createString("lot");

    public final NumberPath<Long> mandate = createNumber("mandate", Long.class);

    public final NumberPath<Long> resteEng = createNumber("resteEng", Long.class);

    public final NumberPath<Long> total = createNumber("total", Long.class);

    public final StringPath typeAchat = createString("typeAchat");

    public QVConsoCn(String variable) {
        super(QVConsoCn.class, forVariable(variable), "GFC", "V_CONSO_CN");
        addMetadata();
    }

    public QVConsoCn(String variable, String schema, String table) {
        super(QVConsoCn.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVConsoCn(Path<? extends QVConsoCn> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CONSO_CN");
        addMetadata();
    }

    public QVConsoCn(PathMetadata<?> metadata) {
        super(QVConsoCn.class, metadata, "GFC", "V_CONSO_CN");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouCode, ColumnMetadata.named("FOU_CODE").withIndex(6).ofType(Types.VARCHAR).withSize(10));
        addMetadata(fournisseur, ColumnMetadata.named("FOURNISSEUR").withIndex(7).ofType(Types.VARCHAR).withSize(161));
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(150));
        addMetadata(liquide, ColumnMetadata.named("LIQUIDE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lot, ColumnMetadata.named("LOT").withIndex(4).ofType(Types.VARCHAR).withSize(512));
        addMetadata(mandate, ColumnMetadata.named("MANDATE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(resteEng, ColumnMetadata.named("RESTE_ENG").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(total, ColumnMetadata.named("TOTAL").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(typeAchat, ColumnMetadata.named("TYPE_ACHAT").withIndex(5).ofType(Types.VARCHAR).withSize(50));
    }

}

