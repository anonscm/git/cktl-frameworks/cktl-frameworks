package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecVPlancoCreditDep is a Querydsl query type for QRecVPlancoCreditDep
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecVPlancoCreditDep extends com.mysema.query.sql.RelationalPathBase<QRecVPlancoCreditDep> {

    private static final long serialVersionUID = -14725318;

    public static final QRecVPlancoCreditDep recVPlancoCreditDep = new QRecVPlancoCreditDep("REC_V_PLANCO_CREDIT_DEP");

    public final StringPath pccEtat = createString("pccEtat");

    public final NumberPath<Long> pccOrdre = createNumber("pccOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath pcoValidite = createString("pcoValidite");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QRecVPlancoCreditDep(String variable) {
        super(QRecVPlancoCreditDep.class, forVariable(variable), "GFC", "REC_V_PLANCO_CREDIT_DEP");
        addMetadata();
    }

    public QRecVPlancoCreditDep(String variable, String schema, String table) {
        super(QRecVPlancoCreditDep.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecVPlancoCreditDep(Path<? extends QRecVPlancoCreditDep> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REC_V_PLANCO_CREDIT_DEP");
        addMetadata();
    }

    public QRecVPlancoCreditDep(PathMetadata<?> metadata) {
        super(QRecVPlancoCreditDep.class, metadata, "GFC", "REC_V_PLANCO_CREDIT_DEP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pccEtat, ColumnMetadata.named("PCC_ETAT").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pccOrdre, ColumnMetadata.named("PCC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pcoValidite, ColumnMetadata.named("PCO_VALIDITE").withIndex(5).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

