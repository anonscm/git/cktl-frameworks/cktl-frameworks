package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmRepartStructure is a Querydsl query type for QVAdmRepartStructure
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmRepartStructure extends com.mysema.query.sql.RelationalPathBase<QVAdmRepartStructure> {

    private static final long serialVersionUID = -1583960616;

    public static final QVAdmRepartStructure vAdmRepartStructure = new QVAdmRepartStructure("V_ADM_REPART_STRUCTURE");

    public final StringPath cStructure = createString("cStructure");

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public QVAdmRepartStructure(String variable) {
        super(QVAdmRepartStructure.class, forVariable(variable), "GFC", "V_ADM_REPART_STRUCTURE");
        addMetadata();
    }

    public QVAdmRepartStructure(String variable, String schema, String table) {
        super(QVAdmRepartStructure.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmRepartStructure(Path<? extends QVAdmRepartStructure> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_REPART_STRUCTURE");
        addMetadata();
    }

    public QVAdmRepartStructure(PathMetadata<?> metadata) {
        super(QVAdmRepartStructure.class, metadata, "GFC", "V_ADM_REPART_STRUCTURE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cStructure, ColumnMetadata.named("C_STRUCTURE").withIndex(2).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

