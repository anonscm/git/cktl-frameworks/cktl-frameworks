package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMandat is a Querydsl query type for QMandat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QMandat extends com.mysema.query.sql.RelationalPathBase<QMandat> {

    private static final long serialVersionUID = -1683581890;

    public static final QMandat mandat = new QMandat("MANDAT");

    public final NumberPath<Long> borId = createNumber("borId", Long.class);

    public final NumberPath<Long> brjOrdre = createNumber("brjOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final DateTimePath<java.sql.Timestamp> manAttenteDate = createDateTime("manAttenteDate", java.sql.Timestamp.class);

    public final StringPath manAttenteObjet = createString("manAttenteObjet");

    public final NumberPath<Long> manAttentePaiement = createNumber("manAttentePaiement", Long.class);

    public final DateTimePath<java.sql.Timestamp> manDateRemise = createDateTime("manDateRemise", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> manDateVisaPrinc = createDateTime("manDateVisaPrinc", java.sql.Timestamp.class);

    public final StringPath manEtat = createString("manEtat");

    public final StringPath manEtatRemise = createString("manEtatRemise");

    public final NumberPath<java.math.BigDecimal> manHt = createNumber("manHt", java.math.BigDecimal.class);

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final StringPath manMotifRejet = createString("manMotifRejet");

    public final NumberPath<Long> manNbPiece = createNumber("manNbPiece", Long.class);

    public final NumberPath<Long> manNumero = createNumber("manNumero", Long.class);

    public final NumberPath<Long> manNumeroRejet = createNumber("manNumeroRejet", Long.class);

    public final NumberPath<Long> manOrdre = createNumber("manOrdre", Long.class);

    public final NumberPath<Long> manOrgineKey = createNumber("manOrgineKey", Long.class);

    public final StringPath manOrigineLib = createString("manOrigineLib");

    public final NumberPath<java.math.BigDecimal> manTtc = createNumber("manTtc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> manTva = createNumber("manTva", java.math.BigDecimal.class);

    public final NumberPath<Long> modOrdre = createNumber("modOrdre", Long.class);

    public final NumberPath<Long> orgOrdre = createNumber("orgOrdre", Long.class);

    public final NumberPath<Long> oriOrdre = createNumber("oriOrdre", Long.class);

    public final NumberPath<Long> paiOrdre = createNumber("paiOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> prestId = createNumber("prestId", Long.class);

    public final NumberPath<Long> ribOrdreComptable = createNumber("ribOrdreComptable", Long.class);

    public final NumberPath<Long> ribOrdreOrdonnateur = createNumber("ribOrdreOrdonnateur", Long.class);

    public final NumberPath<Long> torOrdre = createNumber("torOrdre", Long.class);

    public final NumberPath<Long> utlOrdreAttente = createNumber("utlOrdreAttente", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QMandat> mandatPk = createPrimaryKey(manId);

    public final com.mysema.query.sql.ForeignKey<QRibfourUlr> manRibOrdreOrdFk = createForeignKey(ribOrdreOrdonnateur, "RIB_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRibfourUlr> manRibOrdreComFk = createForeignKey(ribOrdreComptable, "RIB_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereau> mandatBorIdFk = createForeignKey(borId, "BOR_ID");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> manFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> mandatManAttentePaiementFk = createForeignKey(manAttentePaiement, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QPaiement> mandatPaiOrdreFk = createForeignKey(paiOrdre, "PAI_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> mandatExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypeOrigineBordereau> mandatTorOrdreFk = createForeignKey(torOrdre, "TOR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QModePaiement> mandatModOrdreFk = createForeignKey(modOrdre, "MOD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereauRejet> mandatBrjOrdreFk = createForeignKey(brjOrdre, "BRJ_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QGestion> mandatGesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QMandatBrouillard> _mandatBrouillardManIdFk = createInvForeignKey(manId, "MAN_ID");

    public final com.mysema.query.sql.ForeignKey<QCptReimputation> _cptReimputationManIdFk = createInvForeignKey(manId, "MAN_ID");

    public final com.mysema.query.sql.ForeignKey<QExtourneMandat> _extourneMandatManidnFk = createInvForeignKey(manId, "MAN_ID_N");

    public final com.mysema.query.sql.ForeignKey<QMandatDetailEcriture> _mandatDetailEcritureMan_fk = createInvForeignKey(manId, "MAN_ID");

    public final com.mysema.query.sql.ForeignKey<QExtourneMandat> _extourneMandatManidn1Fk = createInvForeignKey(manId, "MAN_ID_N1");

    public final com.mysema.query.sql.ForeignKey<QCptDepense> _depenseManIdFk = createInvForeignKey(manId, "MAN_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlPlanco> _depenseCtrlPlanManIdFk = createInvForeignKey(manId, "MAN_ID");

    public QMandat(String variable) {
        super(QMandat.class, forVariable(variable), "GFC", "MANDAT");
        addMetadata();
    }

    public QMandat(String variable, String schema, String table) {
        super(QMandat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMandat(Path<? extends QMandat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "MANDAT");
        addMetadata();
    }

    public QMandat(PathMetadata<?> metadata) {
        super(QMandat.class, metadata, "GFC", "MANDAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(brjOrdre, ColumnMetadata.named("BRJ_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(6).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(manAttenteDate, ColumnMetadata.named("MAN_ATTENTE_DATE").withIndex(31).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(manAttenteObjet, ColumnMetadata.named("MAN_ATTENTE_OBJET").withIndex(32).ofType(Types.VARCHAR).withSize(500));
        addMetadata(manAttentePaiement, ColumnMetadata.named("MAN_ATTENTE_PAIEMENT").withIndex(30).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manDateRemise, ColumnMetadata.named("MAN_DATE_REMISE").withIndex(7).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(manDateVisaPrinc, ColumnMetadata.named("MAN_DATE_VISA_PRINC").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(manEtat, ColumnMetadata.named("MAN_ETAT").withIndex(9).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(manEtatRemise, ColumnMetadata.named("MAN_ETAT_REMISE").withIndex(10).ofType(Types.VARCHAR).withSize(10));
        addMetadata(manHt, ColumnMetadata.named("MAN_HT").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manMotifRejet, ColumnMetadata.named("MAN_MOTIF_REJET").withIndex(12).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(manNbPiece, ColumnMetadata.named("MAN_NB_PIECE").withIndex(13).ofType(Types.DECIMAL).withSize(38));
        addMetadata(manNumero, ColumnMetadata.named("MAN_NUMERO").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manNumeroRejet, ColumnMetadata.named("MAN_NUMERO_REJET").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(manOrdre, ColumnMetadata.named("MAN_ORDRE").withIndex(16).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manOrgineKey, ColumnMetadata.named("MAN_ORGINE_KEY").withIndex(17).ofType(Types.DECIMAL).withSize(0));
        addMetadata(manOrigineLib, ColumnMetadata.named("MAN_ORIGINE_LIB").withIndex(18).ofType(Types.VARCHAR).withSize(200));
        addMetadata(manTtc, ColumnMetadata.named("MAN_TTC").withIndex(19).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(manTva, ColumnMetadata.named("MAN_TVA").withIndex(20).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(modOrdre, ColumnMetadata.named("MOD_ORDRE").withIndex(21).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(orgOrdre, ColumnMetadata.named("ORG_ORDRE").withIndex(27).ofType(Types.DECIMAL).withSize(38));
        addMetadata(oriOrdre, ColumnMetadata.named("ORI_ORDRE").withIndex(22).ofType(Types.DECIMAL).withSize(0));
        addMetadata(paiOrdre, ColumnMetadata.named("PAI_ORDRE").withIndex(26).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(23).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(prestId, ColumnMetadata.named("PREST_ID").withIndex(24).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ribOrdreComptable, ColumnMetadata.named("RIB_ORDRE_COMPTABLE").withIndex(29).ofType(Types.DECIMAL).withSize(38));
        addMetadata(ribOrdreOrdonnateur, ColumnMetadata.named("RIB_ORDRE_ORDONNATEUR").withIndex(28).ofType(Types.DECIMAL).withSize(38));
        addMetadata(torOrdre, ColumnMetadata.named("TOR_ORDRE").withIndex(25).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdreAttente, ColumnMetadata.named("UTL_ORDRE_ATTENTE").withIndex(33).ofType(Types.DECIMAL).withSize(0));
    }

}

