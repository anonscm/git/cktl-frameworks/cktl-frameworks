package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRepartIndividuEvt is a Querydsl query type for QRepartIndividuEvt
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRepartIndividuEvt extends com.mysema.query.sql.RelationalPathBase<QRepartIndividuEvt> {

    private static final long serialVersionUID = 1568972206;

    public static final QRepartIndividuEvt repartIndividuEvt = new QRepartIndividuEvt("REPART_INDIVIDU_EVT");

    public final NumberPath<Long> evtOrdre = createNumber("evtOrdre", Long.class);

    public final NumberPath<Long> noIndividu = createNumber("noIndividu", Long.class);

    public final NumberPath<Long> rieOrdre = createNumber("rieOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRepartIndividuEvt> repartIndividuEvtPk = createPrimaryKey(rieOrdre);

    public QRepartIndividuEvt(String variable) {
        super(QRepartIndividuEvt.class, forVariable(variable), "GFC", "REPART_INDIVIDU_EVT");
        addMetadata();
    }

    public QRepartIndividuEvt(String variable, String schema, String table) {
        super(QRepartIndividuEvt.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRepartIndividuEvt(Path<? extends QRepartIndividuEvt> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REPART_INDIVIDU_EVT");
        addMetadata();
    }

    public QRepartIndividuEvt(PathMetadata<?> metadata) {
        super(QRepartIndividuEvt.class, metadata, "GFC", "REPART_INDIVIDU_EVT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(evtOrdre, ColumnMetadata.named("EVT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(noIndividu, ColumnMetadata.named("NO_INDIVIDU").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rieOrdre, ColumnMetadata.named("RIE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

