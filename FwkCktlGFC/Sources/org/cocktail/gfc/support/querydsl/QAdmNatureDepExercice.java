package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmNatureDepExercice is a Querydsl query type for QAdmNatureDepExercice
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmNatureDepExercice extends com.mysema.query.sql.RelationalPathBase<QAdmNatureDepExercice> {

    private static final long serialVersionUID = 2077674727;

    public static final QAdmNatureDepExercice admNatureDepExercice = new QAdmNatureDepExercice("ADM_NATURE_DEP_EXERCICE");

    public final StringPath active = createString("active");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmNatureDep = createNumber("idAdmNatureDep", Long.class);

    public final NumberPath<Long> idAdmNatureDepExercice = createNumber("idAdmNatureDepExercice", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmNatureDepExercice> admNatureDepExercicePk = createPrimaryKey(idAdmNatureDepExercice);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> budNatdepAdmExerciceFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureDep> admNatureDepFk = createForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public QAdmNatureDepExercice(String variable) {
        super(QAdmNatureDepExercice.class, forVariable(variable), "GFC", "ADM_NATURE_DEP_EXERCICE");
        addMetadata();
    }

    public QAdmNatureDepExercice(String variable, String schema, String table) {
        super(QAdmNatureDepExercice.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmNatureDepExercice(Path<? extends QAdmNatureDepExercice> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_NATURE_DEP_EXERCICE");
        addMetadata();
    }

    public QAdmNatureDepExercice(PathMetadata<?> metadata) {
        super(QAdmNatureDepExercice.class, metadata, "GFC", "ADM_NATURE_DEP_EXERCICE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(active, ColumnMetadata.named("ACTIVE").withIndex(4).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(idAdmNatureDep, ColumnMetadata.named("ID_ADM_NATURE_DEP").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmNatureDepExercice, ColumnMetadata.named("ID_ADM_NATURE_DEP_EXERCICE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

