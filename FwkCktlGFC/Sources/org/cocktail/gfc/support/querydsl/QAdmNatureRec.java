package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmNatureRec is a Querydsl query type for QAdmNatureRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmNatureRec extends com.mysema.query.sql.RelationalPathBase<QAdmNatureRec> {

    private static final long serialVersionUID = 796257376;

    public static final QAdmNatureRec admNatureRec = new QAdmNatureRec("ADM_NATURE_REC");

    public final StringPath code = createString("code");

    public final NumberPath<Long> idAdmNatureRec = createNumber("idAdmNatureRec", Long.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmNatureRec> admNatureRecPk = createPrimaryKey(idAdmNatureRec);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admNatureRecTypeEtatFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCptbudEcriture> _cptbudEcrNatrFk = createInvForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudRec> _opeTrancheBudRecNatFk = createInvForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureRecExercice> _admNatureRecNatRecExerFk = createInvForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public final com.mysema.query.sql.ForeignKey<QBudPrevHorsOpRec> _budPrevHorsOpRecNatFk = createInvForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public final com.mysema.query.sql.ForeignKey<QBudBudgetRec> _budBudgetRecNatFk = createInvForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraRec> _budTraOpeRecNatFk = createInvForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public QAdmNatureRec(String variable) {
        super(QAdmNatureRec.class, forVariable(variable), "GFC", "ADM_NATURE_REC");
        addMetadata();
    }

    public QAdmNatureRec(String variable, String schema, String table) {
        super(QAdmNatureRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmNatureRec(Path<? extends QAdmNatureRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_NATURE_REC");
        addMetadata();
    }

    public QAdmNatureRec(PathMetadata<?> metadata) {
        super(QAdmNatureRec.class, metadata, "GFC", "ADM_NATURE_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(3).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(idAdmNatureRec, ColumnMetadata.named("ID_ADM_NATURE_REC").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

