package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCleInventaireMontant is a Querydsl query type for QVCleInventaireMontant
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCleInventaireMontant extends com.mysema.query.sql.RelationalPathBase<QVCleInventaireMontant> {

    private static final long serialVersionUID = 1106630403;

    public static final QVCleInventaireMontant vCleInventaireMontant = new QVCleInventaireMontant("V_CLE_INVENTAIRE_MONTANT");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final NumberPath<Long> montant = createNumber("montant", Long.class);

    public QVCleInventaireMontant(String variable) {
        super(QVCleInventaireMontant.class, forVariable(variable), "GFC", "V_CLE_INVENTAIRE_MONTANT");
        addMetadata();
    }

    public QVCleInventaireMontant(String variable, String schema, String table) {
        super(QVCleInventaireMontant.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCleInventaireMontant(Path<? extends QVCleInventaireMontant> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CLE_INVENTAIRE_MONTANT");
        addMetadata();
    }

    public QVCleInventaireMontant(PathMetadata<?> metadata) {
        super(QVCleInventaireMontant.class, metadata, "GFC", "V_CLE_INVENTAIRE_MONTANT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

