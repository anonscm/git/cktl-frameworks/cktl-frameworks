package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputationAnalytique is a Querydsl query type for QReimputationAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputationAnalytique extends com.mysema.query.sql.RelationalPathBase<QReimputationAnalytique> {

    private static final long serialVersionUID = -1484755607;

    public static final QReimputationAnalytique reimputationAnalytique = new QReimputationAnalytique("REIMPUTATION_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<java.math.BigDecimal> reanHtSaisie = createNumber("reanHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reanId = createNumber("reanId", Long.class);

    public final NumberPath<java.math.BigDecimal> reanMontantBudgetaire = createNumber("reanMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reanTtcSaisie = createNumber("reanTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reanTvaSaisie = createNumber("reanTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputationAnalytique> reimputationAnalytiquePk = createPrimaryKey(reanId);

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> reimpAnalytiqueCanIdFk = createForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputation> reimpAnalytiqueReimIdFk = createForeignKey(reimId, "REIM_ID");

    public QReimputationAnalytique(String variable) {
        super(QReimputationAnalytique.class, forVariable(variable), "GFC", "REIMPUTATION_ANALYTIQUE");
        addMetadata();
    }

    public QReimputationAnalytique(String variable, String schema, String table) {
        super(QReimputationAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputationAnalytique(Path<? extends QReimputationAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION_ANALYTIQUE");
        addMetadata();
    }

    public QReimputationAnalytique(PathMetadata<?> metadata) {
        super(QReimputationAnalytique.class, metadata, "GFC", "REIMPUTATION_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(reanHtSaisie, ColumnMetadata.named("REAN_HT_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reanId, ColumnMetadata.named("REAN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(reanMontantBudgetaire, ColumnMetadata.named("REAN_MONTANT_BUDGETAIRE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reanTtcSaisie, ColumnMetadata.named("REAN_TTC_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reanTvaSaisie, ColumnMetadata.named("REAN_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

