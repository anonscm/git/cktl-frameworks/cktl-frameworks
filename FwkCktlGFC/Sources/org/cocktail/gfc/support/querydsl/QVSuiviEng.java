package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviEng is a Querydsl query type for QVSuiviEng
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviEng extends com.mysema.query.sql.RelationalPathBase<QVSuiviEng> {

    private static final long serialVersionUID = 1108108793;

    public static final QVSuiviEng vSuiviEng = new QVSuiviEng("V_SUIVI_ENG");

    public final SimplePath<Object> econDateSaisie = createSimple("econDateSaisie", Object.class);

    public final SimplePath<Object> econMontantBudgetaire = createSimple("econMontantBudgetaire", Object.class);

    public final SimplePath<Object> econMontantBudgetaireReste = createSimple("econMontantBudgetaireReste", Object.class);

    public final SimplePath<Object> engId = createSimple("engId", Object.class);

    public final SimplePath<Object> engNumero = createSimple("engNumero", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QVSuiviEng(String variable) {
        super(QVSuiviEng.class, forVariable(variable), "GFC", "V_SUIVI_ENG");
        addMetadata();
    }

    public QVSuiviEng(String variable, String schema, String table) {
        super(QVSuiviEng.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviEng(Path<? extends QVSuiviEng> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_ENG");
        addMetadata();
    }

    public QVSuiviEng(PathMetadata<?> metadata) {
        super(QVSuiviEng.class, metadata, "GFC", "V_SUIVI_ENG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(econDateSaisie, ColumnMetadata.named("ECON_DATE_SAISIE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaire, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaireReste, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(9).ofType(Types.OTHER).withSize(0));
    }

}

