package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeCtrlConvention is a Querydsl query type for QCommandeCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QCommandeCtrlConvention> {

    private static final long serialVersionUID = 446133143;

    public static final QCommandeCtrlConvention commandeCtrlConvention = new QCommandeCtrlConvention("COMMANDE_CTRL_CONVENTION");

    public final NumberPath<Long> cbudId = createNumber("cbudId", Long.class);

    public final NumberPath<java.math.BigDecimal> cconHtSaisie = createNumber("cconHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> cconId = createNumber("cconId", Long.class);

    public final NumberPath<java.math.BigDecimal> cconMontantBudgetaire = createNumber("cconMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<Double> cconPourcentage = createNumber("cconPourcentage", Double.class);

    public final NumberPath<java.math.BigDecimal> cconTtcSaisie = createNumber("cconTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> cconTvaSaisie = createNumber("cconTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeCtrlConvention> commandeCtrlConventionPk = createPrimaryKey(cconId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeCtrlCoExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> commandeCtrlCoCbudIdFk = createForeignKey(cbudId, "CBUD_ID");

    public QCommandeCtrlConvention(String variable) {
        super(QCommandeCtrlConvention.class, forVariable(variable), "GFC", "COMMANDE_CTRL_CONVENTION");
        addMetadata();
    }

    public QCommandeCtrlConvention(String variable, String schema, String table) {
        super(QCommandeCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeCtrlConvention(Path<? extends QCommandeCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_CTRL_CONVENTION");
        addMetadata();
    }

    public QCommandeCtrlConvention(PathMetadata<?> metadata) {
        super(QCommandeCtrlConvention.class, metadata, "GFC", "COMMANDE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cbudId, ColumnMetadata.named("CBUD_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cconHtSaisie, ColumnMetadata.named("CCON_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cconId, ColumnMetadata.named("CCON_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cconMontantBudgetaire, ColumnMetadata.named("CCON_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cconPourcentage, ColumnMetadata.named("CCON_POURCENTAGE").withIndex(6).ofType(Types.DECIMAL).withSize(15).withDigits(5).notNull());
        addMetadata(cconTtcSaisie, ColumnMetadata.named("CCON_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cconTvaSaisie, ColumnMetadata.named("CCON_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

