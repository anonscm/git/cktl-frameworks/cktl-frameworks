package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZEngageCtrlConvention is a Querydsl query type for QZEngageCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZEngageCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QZEngageCtrlConvention> {

    private static final long serialVersionUID = -762906826;

    public static final QZEngageCtrlConvention zEngageCtrlConvention = new QZEngageCtrlConvention("Z_ENGAGE_CTRL_CONVENTION");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> econDateSaisie = createDateTime("econDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> econHtSaisie = createNumber("econHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> econId = createNumber("econId", Long.class);

    public final NumberPath<java.math.BigDecimal> econMontantBudgetaire = createNumber("econMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> econMontantBudgetaireReste = createNumber("econMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> econTtcSaisie = createNumber("econTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> econTvaSaisie = createNumber("econTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> zeconId = createNumber("zeconId", Long.class);

    public final NumberPath<Long> zengId = createNumber("zengId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZEngageCtrlConvention> sysC0075710 = createPrimaryKey(zeconId);

    public final com.mysema.query.sql.ForeignKey<QZEngageBudget> zEngageCtrlConZengIdFk = createForeignKey(zengId, "ZENG_ID");

    public QZEngageCtrlConvention(String variable) {
        super(QZEngageCtrlConvention.class, forVariable(variable), "GFC", "Z_ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public QZEngageCtrlConvention(String variable, String schema, String table) {
        super(QZEngageCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZEngageCtrlConvention(Path<? extends QZEngageCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public QZEngageCtrlConvention(PathMetadata<?> metadata) {
        super(QZEngageCtrlConvention.class, metadata, "GFC", "Z_ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(econDateSaisie, ColumnMetadata.named("ECON_DATE_SAISIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(econHtSaisie, ColumnMetadata.named("ECON_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econId, ColumnMetadata.named("ECON_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(econMontantBudgetaire, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econMontantBudgetaireReste, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econTtcSaisie, ColumnMetadata.named("ECON_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econTvaSaisie, ColumnMetadata.named("ECON_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(zeconId, ColumnMetadata.named("ZECON_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zengId, ColumnMetadata.named("ZENG_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

