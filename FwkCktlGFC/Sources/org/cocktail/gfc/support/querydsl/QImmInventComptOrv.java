package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventComptOrv is a Querydsl query type for QImmInventComptOrv
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventComptOrv extends com.mysema.query.sql.RelationalPathBase<QImmInventComptOrv> {

    private static final long serialVersionUID = 1713581104;

    public static final QImmInventComptOrv immInventComptOrv = new QImmInventComptOrv("IMM_INVENT_COMPT_ORV");

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> invoId = createNumber("invoId", Long.class);

    public final NumberPath<java.math.BigDecimal> invoMontantOrv = createNumber("invoMontantOrv", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInventComptOrv> immInventComptOrvPk = createPrimaryKey(invoId);

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> inventaireComptableFk2 = createForeignKey(invcId, "INVC_ID");

    public QImmInventComptOrv(String variable) {
        super(QImmInventComptOrv.class, forVariable(variable), "GFC", "IMM_INVENT_COMPT_ORV");
        addMetadata();
    }

    public QImmInventComptOrv(String variable, String schema, String table) {
        super(QImmInventComptOrv.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventComptOrv(Path<? extends QImmInventComptOrv> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_COMPT_ORV");
        addMetadata();
    }

    public QImmInventComptOrv(PathMetadata<?> metadata) {
        super(QImmInventComptOrv.class, metadata, "GFC", "IMM_INVENT_COMPT_ORV");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invoId, ColumnMetadata.named("INVO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invoMontantOrv, ColumnMetadata.named("INVO_MONTANT_ORV").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
    }

}

