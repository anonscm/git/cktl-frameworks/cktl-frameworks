package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAbricotRecettePi is a Querydsl query type for QVAbricotRecettePi
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAbricotRecettePi extends com.mysema.query.sql.RelationalPathBase<QVAbricotRecettePi> {

    private static final long serialVersionUID = -330654418;

    public static final QVAbricotRecettePi vAbricotRecettePi = new QVAbricotRecettePi("V_ABRICOT_RECETTE_PI");

    public final StringPath adrAdresse1 = createString("adrAdresse1");

    public final StringPath adrAdresse2 = createString("adrAdresse2");

    public final StringPath adrCivilite = createString("adrCivilite");

    public final StringPath adrCp = createString("adrCp");

    public final StringPath adrNom = createString("adrNom");

    public final StringPath adrPrenom = createString("adrPrenom");

    public final StringPath adrVille = createString("adrVille");

    public final StringPath bic = createString("bic");

    public final StringPath cBanque = createString("cBanque");

    public final StringPath cGuichet = createString("cGuichet");

    public final StringPath domiciliation = createString("domiciliation");

    public final NumberPath<Integer> exeExercice = createNumber("exeExercice", Integer.class);

    public final DateTimePath<java.sql.Timestamp> facDateSaisie = createDateTime("facDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<Long> facNumero = createNumber("facNumero", Long.class);

    public final StringPath iban = createString("iban");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath modCode = createString("modCode");

    public final StringPath modDom = createString("modDom");

    public final StringPath modLibelle = createString("modLibelle");

    public final StringPath noCompte = createString("noCompte");

    public final StringPath orgCr = createString("orgCr");

    public final StringPath orgSouscr = createString("orgSouscr");

    public final StringPath orgUb = createString("orgUb");

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath persLc = createString("persLc");

    public final StringPath persLibelle = createString("persLibelle");

    public final StringPath persType = createString("persType");

    public final NumberPath<java.math.BigDecimal> recHtSaisie = createNumber("recHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final NumberPath<Long> recNbPiece = createNumber("recNbPiece", Long.class);

    public final NumberPath<java.math.BigDecimal> recTtcSaisie = createNumber("recTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> recTvaSaisie = createNumber("recTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> rpcoHtSaisie = createNumber("rpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> rpcoId = createNumber("rpcoId", Long.class);

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public final StringPath tcdCode = createString("tcdCode");

    public final StringPath tcdLibelle = createString("tcdLibelle");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> ttc = createNumber("ttc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> tva = createNumber("tva", java.math.BigDecimal.class);

    public final StringPath utlnomprenom = createString("utlnomprenom");

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QVAbricotRecettePi(String variable) {
        super(QVAbricotRecettePi.class, forVariable(variable), "GFC", "V_ABRICOT_RECETTE_PI");
        addMetadata();
    }

    public QVAbricotRecettePi(String variable, String schema, String table) {
        super(QVAbricotRecettePi.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAbricotRecettePi(Path<? extends QVAbricotRecettePi> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ABRICOT_RECETTE_PI");
        addMetadata();
    }

    public QVAbricotRecettePi(PathMetadata<?> metadata) {
        super(QVAbricotRecettePi.class, metadata, "GFC", "V_ABRICOT_RECETTE_PI");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrAdresse1, ColumnMetadata.named("ADR_ADRESSE1").withIndex(38).ofType(Types.VARCHAR).withSize(100));
        addMetadata(adrAdresse2, ColumnMetadata.named("ADR_ADRESSE2").withIndex(39).ofType(Types.VARCHAR).withSize(300));
        addMetadata(adrCivilite, ColumnMetadata.named("ADR_CIVILITE").withIndex(35).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(adrCp, ColumnMetadata.named("ADR_CP").withIndex(41).ofType(Types.VARCHAR).withSize(10));
        addMetadata(adrNom, ColumnMetadata.named("ADR_NOM").withIndex(36).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(adrPrenom, ColumnMetadata.named("ADR_PRENOM").withIndex(37).ofType(Types.VARCHAR).withSize(40));
        addMetadata(adrVille, ColumnMetadata.named("ADR_VILLE").withIndex(40).ofType(Types.VARCHAR).withSize(60));
        addMetadata(bic, ColumnMetadata.named("BIC").withIndex(5).ofType(Types.VARCHAR).withSize(11));
        addMetadata(cBanque, ColumnMetadata.named("C_BANQUE").withIndex(1).ofType(Types.VARCHAR).withSize(5));
        addMetadata(cGuichet, ColumnMetadata.named("C_GUICHET").withIndex(2).ofType(Types.VARCHAR).withSize(5));
        addMetadata(domiciliation, ColumnMetadata.named("DOMICILIATION").withIndex(6).ofType(Types.VARCHAR).withSize(200));
        addMetadata(exeExercice, ColumnMetadata.named("EXE_EXERCICE").withIndex(13).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(facDateSaisie, ColumnMetadata.named("FAC_DATE_SAISIE").withIndex(26).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(facNumero, ColumnMetadata.named("FAC_NUMERO").withIndex(21).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(iban, ColumnMetadata.named("IBAN").withIndex(4).ofType(Types.VARCHAR).withSize(34));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(modCode, ColumnMetadata.named("MOD_CODE").withIndex(8).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(modDom, ColumnMetadata.named("MOD_DOM").withIndex(9).ofType(Types.VARCHAR).withSize(25));
        addMetadata(modLibelle, ColumnMetadata.named("MOD_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(noCompte, ColumnMetadata.named("NO_COMPTE").withIndex(3).ofType(Types.VARCHAR).withSize(11));
        addMetadata(orgCr, ColumnMetadata.named("ORG_CR").withIndex(16).ofType(Types.VARCHAR).withSize(50));
        addMetadata(orgSouscr, ColumnMetadata.named("ORG_SOUSCR").withIndex(17).ofType(Types.VARCHAR).withSize(50));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(15).ofType(Types.VARCHAR).withSize(10));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(30).ofType(Types.VARCHAR).withSize(20));
        addMetadata(persLc, ColumnMetadata.named("PERS_LC").withIndex(12).ofType(Types.VARCHAR).withSize(40));
        addMetadata(persLibelle, ColumnMetadata.named("PERS_LIBELLE").withIndex(11).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(persType, ColumnMetadata.named("PERS_TYPE").withIndex(10).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(recHtSaisie, ColumnMetadata.named("REC_HT_SAISIE").withIndex(23).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(22).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recNbPiece, ColumnMetadata.named("REC_NB_PIECE").withIndex(27).ofType(Types.DECIMAL).withSize(0));
        addMetadata(recTtcSaisie, ColumnMetadata.named("REC_TTC_SAISIE").withIndex(25).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recTvaSaisie, ColumnMetadata.named("REC_TVA_SAISIE").withIndex(24).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(rpcoHtSaisie, ColumnMetadata.named("RPCO_HT_SAISIE").withIndex(31).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(rpcoId, ColumnMetadata.named("RPCO_ID").withIndex(32).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(29).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdCode, ColumnMetadata.named("TCD_CODE").withIndex(19).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(tcdLibelle, ColumnMetadata.named("TCD_LIBELLE").withIndex(20).ofType(Types.VARCHAR).withSize(40).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(18).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ttc, ColumnMetadata.named("TTC").withIndex(34).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(tva, ColumnMetadata.named("TVA").withIndex(33).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(utlnomprenom, ColumnMetadata.named("UTLNOMPRENOM").withIndex(42).ofType(Types.VARCHAR).withSize(161));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(28).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

