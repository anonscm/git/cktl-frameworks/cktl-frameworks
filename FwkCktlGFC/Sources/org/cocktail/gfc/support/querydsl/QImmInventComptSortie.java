package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventComptSortie is a Querydsl query type for QImmInventComptSortie
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventComptSortie extends com.mysema.query.sql.RelationalPathBase<QImmInventComptSortie> {

    private static final long serialVersionUID = -574869283;

    public static final QImmInventComptSortie immInventComptSortie = new QImmInventComptSortie("IMM_INVENT_COMPT_SORTIE");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final DateTimePath<java.sql.Timestamp> invcsDate = createDateTime("invcsDate", java.sql.Timestamp.class);

    public final NumberPath<Long> invcsId = createNumber("invcsId", Long.class);

    public final NumberPath<java.math.BigDecimal> invcsMontantAcqSortie = createNumber("invcsMontantAcqSortie", java.math.BigDecimal.class);

    public final StringPath invcsMotif = createString("invcsMotif");

    public final NumberPath<java.math.BigDecimal> invcsValeurCession = createNumber("invcsValeurCession", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> invcsVnc = createNumber("invcsVnc", java.math.BigDecimal.class);

    public final NumberPath<Long> tysoId = createNumber("tysoId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInventComptSortie> immInventComptSortiePk = createPrimaryKey(invcsId);

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> comptSortieInvcIdFk = createForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmTypeSortie> comptSortieTysoIdFk = createForeignKey(tysoId, "TYSO_ID");

    public QImmInventComptSortie(String variable) {
        super(QImmInventComptSortie.class, forVariable(variable), "GFC", "IMM_INVENT_COMPT_SORTIE");
        addMetadata();
    }

    public QImmInventComptSortie(String variable, String schema, String table) {
        super(QImmInventComptSortie.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventComptSortie(Path<? extends QImmInventComptSortie> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_COMPT_SORTIE");
        addMetadata();
    }

    public QImmInventComptSortie(PathMetadata<?> metadata) {
        super(QImmInventComptSortie.class, metadata, "GFC", "IMM_INVENT_COMPT_SORTIE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invcsDate, ColumnMetadata.named("INVCS_DATE").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(invcsId, ColumnMetadata.named("INVCS_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invcsMontantAcqSortie, ColumnMetadata.named("INVCS_MONTANT_ACQ_SORTIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(invcsMotif, ColumnMetadata.named("INVCS_MOTIF").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(invcsValeurCession, ColumnMetadata.named("INVCS_VALEUR_CESSION").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(invcsVnc, ColumnMetadata.named("INVCS_VNC").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(tysoId, ColumnMetadata.named("TYSO_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

