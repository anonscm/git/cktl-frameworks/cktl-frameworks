package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPlanComptableRefExt is a Querydsl query type for QPlanComptableRefExt
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPlanComptableRefExt extends com.mysema.query.sql.RelationalPathBase<QPlanComptableRefExt> {

    private static final long serialVersionUID = -2007662973;

    public static final QPlanComptableRefExt planComptableRefExt = new QPlanComptableRefExt("PLAN_COMPTABLE_REF_EXT");

    public final StringPath refPcoBudgetaire = createString("refPcoBudgetaire");

    public final DateTimePath<java.sql.Timestamp> refPcoDateDebut = createDateTime("refPcoDateDebut", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> refPcoDateFin = createDateTime("refPcoDateFin", java.sql.Timestamp.class);

    public final NumberPath<Long> refPcoId = createNumber("refPcoId", Long.class);

    public final StringPath refPcoImportId = createString("refPcoImportId");

    public final StringPath refPcoLibelle = createString("refPcoLibelle");

    public final StringPath refPcoNomenclatureRef = createString("refPcoNomenclatureRef");

    public final StringPath refPcoNum = createString("refPcoNum");

    public final StringPath refPcoNumOld = createString("refPcoNumOld");

    public final StringPath refPcoRemarques = createString("refPcoRemarques");

    public final StringPath refPcoSubdivisable = createString("refPcoSubdivisable");

    public final StringPath refPcoUtilisation = createString("refPcoUtilisation");

    public final com.mysema.query.sql.PrimaryKey<QPlanComptableRefExt> planComptableRefExtPk = createPrimaryKey(refPcoId);

    public QPlanComptableRefExt(String variable) {
        super(QPlanComptableRefExt.class, forVariable(variable), "GFC", "PLAN_COMPTABLE_REF_EXT");
        addMetadata();
    }

    public QPlanComptableRefExt(String variable, String schema, String table) {
        super(QPlanComptableRefExt.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPlanComptableRefExt(Path<? extends QPlanComptableRefExt> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PLAN_COMPTABLE_REF_EXT");
        addMetadata();
    }

    public QPlanComptableRefExt(PathMetadata<?> metadata) {
        super(QPlanComptableRefExt.class, metadata, "GFC", "PLAN_COMPTABLE_REF_EXT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(refPcoBudgetaire, ColumnMetadata.named("REF_PCO_BUDGETAIRE").withIndex(7).ofType(Types.VARCHAR).withSize(1));
        addMetadata(refPcoDateDebut, ColumnMetadata.named("REF_PCO_DATE_DEBUT").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(refPcoDateFin, ColumnMetadata.named("REF_PCO_DATE_FIN").withIndex(11).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(refPcoId, ColumnMetadata.named("REF_PCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(refPcoImportId, ColumnMetadata.named("REF_PCO_IMPORT_ID").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(refPcoLibelle, ColumnMetadata.named("REF_PCO_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(refPcoNomenclatureRef, ColumnMetadata.named("REF_PCO_NOMENCLATURE_REF").withIndex(12).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(refPcoNum, ColumnMetadata.named("REF_PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(refPcoNumOld, ColumnMetadata.named("REF_PCO_NUM_OLD").withIndex(6).ofType(Types.VARCHAR).withSize(30));
        addMetadata(refPcoRemarques, ColumnMetadata.named("REF_PCO_REMARQUES").withIndex(5).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(refPcoSubdivisable, ColumnMetadata.named("REF_PCO_SUBDIVISABLE").withIndex(9).ofType(Types.VARCHAR).withSize(1));
        addMetadata(refPcoUtilisation, ColumnMetadata.named("REF_PCO_UTILISATION").withIndex(8).ofType(Types.VARCHAR).withSize(200));
    }

}

