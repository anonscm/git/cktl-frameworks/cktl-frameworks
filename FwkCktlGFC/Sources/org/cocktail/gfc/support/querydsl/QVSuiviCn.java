package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviCn is a Querydsl query type for QVSuiviCn
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviCn extends com.mysema.query.sql.RelationalPathBase<QVSuiviCn> {

    private static final long serialVersionUID = 451387376;

    public static final QVSuiviCn vSuiviCn = new QVSuiviCn("V_SUIVI_CN");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> liqPasMandate = createNumber("liqPasMandate", Long.class);

    public final NumberPath<Long> mandate = createNumber("mandate", Long.class);

    public final NumberPath<Long> resteEngage = createNumber("resteEngage", Long.class);

    public final NumberPath<Long> total = createNumber("total", Long.class);

    public QVSuiviCn(String variable) {
        super(QVSuiviCn.class, forVariable(variable), "GFC", "V_SUIVI_CN");
        addMetadata();
    }

    public QVSuiviCn(String variable, String schema, String table) {
        super(QVSuiviCn.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviCn(Path<? extends QVSuiviCn> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_CN");
        addMetadata();
    }

    public QVSuiviCn(PathMetadata<?> metadata) {
        super(QVSuiviCn.class, metadata, "GFC", "V_SUIVI_CN");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(liqPasMandate, ColumnMetadata.named("LIQ_PAS_MANDATE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(mandate, ColumnMetadata.named("MANDATE").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(resteEngage, ColumnMetadata.named("RESTE_ENGAGE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(total, ColumnMetadata.named("TOTAL").withIndex(8).ofType(Types.DECIMAL).withSize(0));
    }

}

