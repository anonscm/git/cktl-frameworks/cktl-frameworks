package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVReimputationNewAnalytique is a Querydsl query type for QVReimputationNewAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVReimputationNewAnalytique extends com.mysema.query.sql.RelationalPathBase<QVReimputationNewAnalytique> {

    private static final long serialVersionUID = -1798779847;

    public static final QVReimputationNewAnalytique vReimputationNewAnalytique = new QVReimputationNewAnalytique("V_REIMPUTATION_NEW_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<java.math.BigDecimal> reanHtSaisie = createNumber("reanHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reanId = createNumber("reanId", Long.class);

    public final NumberPath<java.math.BigDecimal> reanMontantBudgetaire = createNumber("reanMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reanTtcSaisie = createNumber("reanTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reanTvaSaisie = createNumber("reanTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public QVReimputationNewAnalytique(String variable) {
        super(QVReimputationNewAnalytique.class, forVariable(variable), "GFC", "V_REIMPUTATION_NEW_ANALYTIQUE");
        addMetadata();
    }

    public QVReimputationNewAnalytique(String variable, String schema, String table) {
        super(QVReimputationNewAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVReimputationNewAnalytique(Path<? extends QVReimputationNewAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_REIMPUTATION_NEW_ANALYTIQUE");
        addMetadata();
    }

    public QVReimputationNewAnalytique(PathMetadata<?> metadata) {
        super(QVReimputationNewAnalytique.class, metadata, "GFC", "V_REIMPUTATION_NEW_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(reanHtSaisie, ColumnMetadata.named("REAN_HT_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reanId, ColumnMetadata.named("REAN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(reanMontantBudgetaire, ColumnMetadata.named("REAN_MONTANT_BUDGETAIRE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reanTtcSaisie, ColumnMetadata.named("REAN_TTC_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reanTvaSaisie, ColumnMetadata.named("REAN_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

