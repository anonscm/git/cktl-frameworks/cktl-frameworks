package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCptParametre is a Querydsl query type for QCptParametre
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCptParametre extends com.mysema.query.sql.RelationalPathBase<QCptParametre> {

    private static final long serialVersionUID = -2081868087;

    public static final QCptParametre cptParametre = new QCptParametre("CPT_PARAMETRE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath parDescription = createString("parDescription");

    public final StringPath parKey = createString("parKey");

    public final NumberPath<Long> parOrdre = createNumber("parOrdre", Long.class);

    public final StringPath parValue = createString("parValue");

    public final com.mysema.query.sql.PrimaryKey<QCptParametre> cptParametrePk = createPrimaryKey(parOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> cptParametreExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QCptParametre(String variable) {
        super(QCptParametre.class, forVariable(variable), "GFC", "CPT_PARAMETRE");
        addMetadata();
    }

    public QCptParametre(String variable, String schema, String table) {
        super(QCptParametre.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCptParametre(Path<? extends QCptParametre> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CPT_PARAMETRE");
        addMetadata();
    }

    public QCptParametre(PathMetadata<?> metadata) {
        super(QCptParametre.class, metadata, "GFC", "CPT_PARAMETRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(4));
        addMetadata(parDescription, ColumnMetadata.named("PAR_DESCRIPTION").withIndex(1).ofType(Types.VARCHAR).withSize(300));
        addMetadata(parKey, ColumnMetadata.named("PAR_KEY").withIndex(2).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(parOrdre, ColumnMetadata.named("PAR_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(parValue, ColumnMetadata.named("PAR_VALUE").withIndex(4).ofType(Types.VARCHAR).withSize(500));
    }

}

