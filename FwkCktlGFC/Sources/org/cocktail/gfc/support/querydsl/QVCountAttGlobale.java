package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCountAttGlobale is a Querydsl query type for QVCountAttGlobale
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCountAttGlobale extends com.mysema.query.sql.RelationalPathBase<QVCountAttGlobale> {

    private static final long serialVersionUID = 1073887579;

    public static final QVCountAttGlobale vCountAttGlobale = new QVCountAttGlobale("V_COUNT_ATT_GLOBALE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> cnt = createNumber("cnt", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public QVCountAttGlobale(String variable) {
        super(QVCountAttGlobale.class, forVariable(variable), "GFC", "V_COUNT_ATT_GLOBALE");
        addMetadata();
    }

    public QVCountAttGlobale(String variable, String schema, String table) {
        super(QVCountAttGlobale.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCountAttGlobale(Path<? extends QVCountAttGlobale> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_COUNT_ATT_GLOBALE");
        addMetadata();
    }

    public QVCountAttGlobale(PathMetadata<?> metadata) {
        super(QVCountAttGlobale.class, metadata, "GFC", "V_COUNT_ATT_GLOBALE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cnt, ColumnMetadata.named("CNT").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

