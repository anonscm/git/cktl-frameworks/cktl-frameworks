package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QConventionLimitative is a Querydsl query type for QConventionLimitative
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QConventionLimitative extends com.mysema.query.sql.RelationalPathBase<QConventionLimitative> {

    private static final long serialVersionUID = -1291824708;

    public static final QConventionLimitative conventionLimitative = new QConventionLimitative("CONVENTION_LIMITATIVE");

    public final SimplePath<Object> clConReference = createSimple("clConReference", Object.class);

    public final SimplePath<Object> clMontant = createSimple("clMontant", Object.class);

    public final SimplePath<Object> clTypeMontant = createSimple("clTypeMontant", Object.class);

    public final SimplePath<Object> conObjetCourt = createSimple("conObjetCourt", Object.class);

    public final SimplePath<Object> conReferenceExterne = createSimple("conReferenceExterne", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QConventionLimitative(String variable) {
        super(QConventionLimitative.class, forVariable(variable), "GFC", "CONVENTION_LIMITATIVE");
        addMetadata();
    }

    public QConventionLimitative(String variable, String schema, String table) {
        super(QConventionLimitative.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QConventionLimitative(Path<? extends QConventionLimitative> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CONVENTION_LIMITATIVE");
        addMetadata();
    }

    public QConventionLimitative(PathMetadata<?> metadata) {
        super(QConventionLimitative.class, metadata, "GFC", "CONVENTION_LIMITATIVE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clConReference, ColumnMetadata.named("CL_CON_REFERENCE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(clMontant, ColumnMetadata.named("CL_MONTANT").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(clTypeMontant, ColumnMetadata.named("CL_TYPE_MONTANT").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(conObjetCourt, ColumnMetadata.named("CON_OBJET_COURT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(conReferenceExterne, ColumnMetadata.named("CON_REFERENCE_EXTERNE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(6).ofType(Types.OTHER).withSize(0));
    }

}

