package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCommandesPourPi is a Querydsl query type for QVCommandesPourPi
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCommandesPourPi extends com.mysema.query.sql.RelationalPathBase<QVCommandesPourPi> {

    private static final long serialVersionUID = 897763897;

    public static final QVCommandesPourPi vCommandesPourPi = new QVCommandesPourPi("V_COMMANDES_POUR_PI");

    public final DateTimePath<java.sql.Timestamp> commDateCreation = createDateTime("commDateCreation", java.sql.Timestamp.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final StringPath commLibelle = createString("commLibelle");

    public final NumberPath<Long> commNumero = createNumber("commNumero", Long.class);

    public final StringPath commReference = createString("commReference");

    public final NumberPath<java.math.BigDecimal> engHtSaisie = createNumber("engHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Long> engNumero = createNumber("engNumero", Long.class);

    public final NumberPath<java.math.BigDecimal> engTtcSaisie = createNumber("engTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> engTvaSaisie = createNumber("engTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QVCommandesPourPi(String variable) {
        super(QVCommandesPourPi.class, forVariable(variable), "GFC", "V_COMMANDES_POUR_PI");
        addMetadata();
    }

    public QVCommandesPourPi(String variable, String schema, String table) {
        super(QVCommandesPourPi.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCommandesPourPi(Path<? extends QVCommandesPourPi> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_COMMANDES_POUR_PI");
        addMetadata();
    }

    public QVCommandesPourPi(PathMetadata<?> metadata) {
        super(QVCommandesPourPi.class, metadata, "GFC", "V_COMMANDES_POUR_PI");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commDateCreation, ColumnMetadata.named("COMM_DATE_CREATION").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commLibelle, ColumnMetadata.named("COMM_LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(commNumero, ColumnMetadata.named("COMM_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commReference, ColumnMetadata.named("COMM_REFERENCE").withIndex(4).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(engHtSaisie, ColumnMetadata.named("ENG_HT_SAISIE").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engTtcSaisie, ColumnMetadata.named("ENG_TTC_SAISIE").withIndex(14).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engTvaSaisie, ColumnMetadata.named("ENG_TVA_SAISIE").withIndex(13).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(11).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(15).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

