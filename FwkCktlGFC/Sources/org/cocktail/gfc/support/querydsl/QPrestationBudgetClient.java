package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPrestationBudgetClient is a Querydsl query type for QPrestationBudgetClient
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPrestationBudgetClient extends com.mysema.query.sql.RelationalPathBase<QPrestationBudgetClient> {

    private static final long serialVersionUID = 1058686338;

    public static final QPrestationBudgetClient prestationBudgetClient = new QPrestationBudgetClient("PRESTATION_BUDGET_CLIENT");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final NumberPath<Long> lolfId = createNumber("lolfId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> prestId = createNumber("prestId", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPrestationBudgetClient> prestationBudgetClientPk = createPrimaryKey(prestId);

    public final com.mysema.query.sql.ForeignKey<QPrestation> pbcPrestIdFk = createForeignKey(prestId, "PREST_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> pbcOrgIdFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> pbcEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> pbcPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> pbcCanIdFk = createForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> pbcTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> pbcTcdOrdreFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepense> pbcLolfIdFk = createForeignKey(lolfId, "ID_ADM_DESTINATION_DEPENSE");

    public QPrestationBudgetClient(String variable) {
        super(QPrestationBudgetClient.class, forVariable(variable), "GFC", "PRESTATION_BUDGET_CLIENT");
        addMetadata();
    }

    public QPrestationBudgetClient(String variable, String schema, String table) {
        super(QPrestationBudgetClient.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPrestationBudgetClient(Path<? extends QPrestationBudgetClient> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PRESTATION_BUDGET_CLIENT");
        addMetadata();
    }

    public QPrestationBudgetClient(PathMetadata<?> metadata) {
        super(QPrestationBudgetClient.class, metadata, "GFC", "PRESTATION_BUDGET_CLIENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lolfId, ColumnMetadata.named("LOLF_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(6).ofType(Types.VARCHAR).withSize(20));
        addMetadata(prestId, ColumnMetadata.named("PREST_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
    }

}

