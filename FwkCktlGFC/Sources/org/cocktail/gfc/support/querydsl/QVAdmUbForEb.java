package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmUbForEb is a Querydsl query type for QVAdmUbForEb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmUbForEb extends com.mysema.query.sql.RelationalPathBase<QVAdmUbForEb> {

    private static final long serialVersionUID = -1396090332;

    public static final QVAdmUbForEb vAdmUbForEb = new QVAdmUbForEb("V_ADM_UB_FOR_EB");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idAdmEbUb = createNumber("idAdmEbUb", Long.class);

    public QVAdmUbForEb(String variable) {
        super(QVAdmUbForEb.class, forVariable(variable), "GFC", "V_ADM_UB_FOR_EB");
        addMetadata();
    }

    public QVAdmUbForEb(String variable, String schema, String table) {
        super(QVAdmUbForEb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmUbForEb(Path<? extends QVAdmUbForEb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_UB_FOR_EB");
        addMetadata();
    }

    public QVAdmUbForEb(PathMetadata<?> metadata) {
        super(QVAdmUbForEb.class, metadata, "GFC", "V_ADM_UB_FOR_EB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmEbUb, ColumnMetadata.named("ID_ADM_EB_UB").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

