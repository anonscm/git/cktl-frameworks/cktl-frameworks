package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmLivraisonDetail is a Querydsl query type for QImmLivraisonDetail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmLivraisonDetail extends com.mysema.query.sql.RelationalPathBase<QImmLivraisonDetail> {

    private static final long serialVersionUID = 228793978;

    public static final QImmLivraisonDetail immLivraisonDetail = new QImmLivraisonDetail("IMM_LIVRAISON_DETAIL");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath lidLibelle = createString("lidLibelle");

    public final NumberPath<java.math.BigDecimal> lidMontant = createNumber("lidMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> lidOrdre = createNumber("lidOrdre", Long.class);

    public final NumberPath<Long> lidQuantite = createNumber("lidQuantite", Long.class);

    public final NumberPath<Long> lidReste = createNumber("lidReste", Long.class);

    public final NumberPath<Long> livOrdre = createNumber("livOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmLivraisonDetail> immLivraisonDetailPk = createPrimaryKey(lidOrdre);

    public final com.mysema.query.sql.ForeignKey<QImmLivraison> livraisonFk = createForeignKey(livOrdre, "LIV_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QImmInvent> _livraisondetailFk = createInvForeignKey(lidOrdre, "LID_ORDRE");

    public QImmLivraisonDetail(String variable) {
        super(QImmLivraisonDetail.class, forVariable(variable), "GFC", "IMM_LIVRAISON_DETAIL");
        addMetadata();
    }

    public QImmLivraisonDetail(String variable, String schema, String table) {
        super(QImmLivraisonDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmLivraisonDetail(Path<? extends QImmLivraisonDetail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_LIVRAISON_DETAIL");
        addMetadata();
    }

    public QImmLivraisonDetail(PathMetadata<?> metadata) {
        super(QImmLivraisonDetail.class, metadata, "GFC", "IMM_LIVRAISON_DETAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(lidLibelle, ColumnMetadata.named("LID_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(400));
        addMetadata(lidMontant, ColumnMetadata.named("LID_MONTANT").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(lidOrdre, ColumnMetadata.named("LID_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(lidQuantite, ColumnMetadata.named("LID_QUANTITE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(lidReste, ColumnMetadata.named("LID_RESTE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(livOrdre, ColumnMetadata.named("LIV_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(8).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

