package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBudVersionBudget is a Querydsl query type for QBudVersionBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBudVersionBudget extends com.mysema.query.sql.RelationalPathBase<QBudVersionBudget> {

    private static final long serialVersionUID = 1070102093;

    public static final QBudVersionBudget budVersionBudget = new QBudVersionBudget("BUD_VERSION_BUDGET");

    public final StringPath code = createString("code");

    public final NumberPath<Long> idBudVersionBudget = createNumber("idBudVersionBudget", Long.class);

    public final StringPath libelle = createString("libelle");

    public final com.mysema.query.sql.PrimaryKey<QBudVersionBudget> budVersionBudgetPk = createPrimaryKey(idBudVersionBudget);

    public final com.mysema.query.sql.ForeignKey<QBudBudget> _budBudgetVersionFk = createInvForeignKey(idBudVersionBudget, "ID_BUD_VERSION_BUDGET");

    public QBudVersionBudget(String variable) {
        super(QBudVersionBudget.class, forVariable(variable), "GFC", "BUD_VERSION_BUDGET");
        addMetadata();
    }

    public QBudVersionBudget(String variable, String schema, String table) {
        super(QBudVersionBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBudVersionBudget(Path<? extends QBudVersionBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BUD_VERSION_BUDGET");
        addMetadata();
    }

    public QBudVersionBudget(PathMetadata<?> metadata) {
        super(QBudVersionBudget.class, metadata, "GFC", "BUD_VERSION_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(2).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(idBudVersionBudget, ColumnMetadata.named("ID_BUD_VERSION_BUDGET").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
    }

}

