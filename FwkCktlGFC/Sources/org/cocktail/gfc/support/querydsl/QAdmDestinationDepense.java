package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmDestinationDepense is a Querydsl query type for QAdmDestinationDepense
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmDestinationDepense extends com.mysema.query.sql.RelationalPathBase<QAdmDestinationDepense> {

    private static final long serialVersionUID = 304400709;

    public static final QAdmDestinationDepense admDestinationDepense = new QAdmDestinationDepense("ADM_DESTINATION_DEPENSE");

    public final StringPath abreviation = createString("abreviation");

    public final StringPath code = createString("code");

    public final DateTimePath<java.sql.Timestamp> fermeture = createDateTime("fermeture", java.sql.Timestamp.class);

    public final NumberPath<Long> idAdmDestinationDepense = createNumber("idAdmDestinationDepense", Long.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Long> niveau = createNumber("niveau", Long.class);

    public final NumberPath<Integer> ordreAffichage = createNumber("ordreAffichage", Integer.class);

    public final DateTimePath<java.sql.Timestamp> ouverture = createDateTime("ouverture", java.sql.Timestamp.class);

    public final NumberPath<Long> pere = createNumber("pere", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final StringPath type = createString("type");

    public final com.mysema.query.sql.PrimaryKey<QAdmDestinationDepense> admDestinationDepensePk = createPrimaryKey(idAdmDestinationDepense);

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepenseType> typeFk = createForeignKey(type, "TYPE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admLolfdTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepAe> _budPrevOpeAeDestFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepCp> _budPrevOpeCpDestFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevHorsOpDep> _budPrevHorsOpDepDestFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QBudBudgetDep> _budBudgetDepDestFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudDepAe> _opeTrancheBudDepAeDestFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QPrestationBudgetClient> _pbcLolfIdFk = createInvForeignKey(idAdmDestinationDepense, "LOLF_ID");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudDepCp> _opeTrancheBudDepCpDestFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QCptbudEcriture> _cptbudEcrDestFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepExer> _admDestDepExeDestDepFk = createInvForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QSbDepense> _depLolfFk = createInvForeignKey(idAdmDestinationDepense, "LOLF_ID");

    public QAdmDestinationDepense(String variable) {
        super(QAdmDestinationDepense.class, forVariable(variable), "GFC", "ADM_DESTINATION_DEPENSE");
        addMetadata();
    }

    public QAdmDestinationDepense(String variable, String schema, String table) {
        super(QAdmDestinationDepense.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmDestinationDepense(Path<? extends QAdmDestinationDepense> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_DESTINATION_DEPENSE");
        addMetadata();
    }

    public QAdmDestinationDepense(PathMetadata<?> metadata) {
        super(QAdmDestinationDepense.class, metadata, "GFC", "ADM_DESTINATION_DEPENSE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(abreviation, ColumnMetadata.named("ABREVIATION").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(2).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(fermeture, ColumnMetadata.named("FERMETURE").withIndex(9).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(idAdmDestinationDepense, ColumnMetadata.named("ID_ADM_DESTINATION_DEPENSE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(niveau, ColumnMetadata.named("NIVEAU").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ordreAffichage, ColumnMetadata.named("ORDRE_AFFICHAGE").withIndex(10).ofType(Types.DECIMAL).withSize(3));
        addMetadata(ouverture, ColumnMetadata.named("OUVERTURE").withIndex(8).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(pere, ColumnMetadata.named("PERE").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(type, ColumnMetadata.named("TYPE").withIndex(6).ofType(Types.VARCHAR).withSize(5).notNull());
    }

}

