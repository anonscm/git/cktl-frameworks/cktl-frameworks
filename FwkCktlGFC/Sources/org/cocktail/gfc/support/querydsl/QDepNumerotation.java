package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepNumerotation is a Querydsl query type for QDepNumerotation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepNumerotation extends com.mysema.query.sql.RelationalPathBase<QDepNumerotation> {

    private static final long serialVersionUID = -1052750677;

    public static final QDepNumerotation depNumerotation = new QDepNumerotation("DEP_NUMEROTATION");

    public final StringPath cr = createString("cr");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> numId = createNumber("numId", Long.class);

    public final NumberPath<Long> numNumero = createNumber("numNumero", Long.class);

    public final NumberPath<Long> tnuId = createNumber("tnuId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDepNumerotation> sysC0076175 = createPrimaryKey(numId);

    public final com.mysema.query.sql.ForeignKey<QGestion> depNumerotationGesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depNumerotationExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepTypeNumerotation> depNumerotationTnuIdFk = createForeignKey(tnuId, "TNU_ID");

    public QDepNumerotation(String variable) {
        super(QDepNumerotation.class, forVariable(variable), "GFC", "DEP_NUMEROTATION");
        addMetadata();
    }

    public QDepNumerotation(String variable, String schema, String table) {
        super(QDepNumerotation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepNumerotation(Path<? extends QDepNumerotation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEP_NUMEROTATION");
        addMetadata();
    }

    public QDepNumerotation(PathMetadata<?> metadata) {
        super(QDepNumerotation.class, metadata, "GFC", "DEP_NUMEROTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cr, ColumnMetadata.named("CR").withIndex(5).ofType(Types.VARCHAR).withSize(50));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(numId, ColumnMetadata.named("NUM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(numNumero, ColumnMetadata.named("NUM_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnuId, ColumnMetadata.named("TNU_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

