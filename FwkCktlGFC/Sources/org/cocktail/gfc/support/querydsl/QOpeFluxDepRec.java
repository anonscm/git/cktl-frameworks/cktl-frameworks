package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeFluxDepRec is a Querydsl query type for QOpeFluxDepRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeFluxDepRec extends com.mysema.query.sql.RelationalPathBase<QOpeFluxDepRec> {

    private static final long serialVersionUID = -194531411;

    public static final QOpeFluxDepRec opeFluxDepRec = new QOpeFluxDepRec("OPE_FLUX_DEP_REC");

    public final StringPath codeFluxDepRec = createString("codeFluxDepRec");

    public final NumberPath<Long> idOpeFluxDepRec = createNumber("idOpeFluxDepRec", Long.class);

    public final StringPath llFluxDepRec = createString("llFluxDepRec");

    public final com.mysema.query.sql.PrimaryKey<QOpeFluxDepRec> idOpeFluxDepRecPk = createPrimaryKey(idOpeFluxDepRec);

    public final com.mysema.query.sql.ForeignKey<QOpeOperation> _opeFluxDepRecOpeFk = createInvForeignKey(idOpeFluxDepRec, "ID_OPE_FLUX_DEP_REC");

    public QOpeFluxDepRec(String variable) {
        super(QOpeFluxDepRec.class, forVariable(variable), "GFC", "OPE_FLUX_DEP_REC");
        addMetadata();
    }

    public QOpeFluxDepRec(String variable, String schema, String table) {
        super(QOpeFluxDepRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeFluxDepRec(Path<? extends QOpeFluxDepRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_FLUX_DEP_REC");
        addMetadata();
    }

    public QOpeFluxDepRec(PathMetadata<?> metadata) {
        super(QOpeFluxDepRec.class, metadata, "GFC", "OPE_FLUX_DEP_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(codeFluxDepRec, ColumnMetadata.named("CODE_FLUX_DEP_REC").withIndex(2).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(idOpeFluxDepRec, ColumnMetadata.named("ID_OPE_FLUX_DEP_REC").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(llFluxDepRec, ColumnMetadata.named("LL_FLUX_DEP_REC").withIndex(3).ofType(Types.VARCHAR).withSize(100).notNull());
    }

}

