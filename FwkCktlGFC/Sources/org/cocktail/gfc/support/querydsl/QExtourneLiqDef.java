package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QExtourneLiqDef is a Querydsl query type for QExtourneLiqDef
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QExtourneLiqDef extends com.mysema.query.sql.RelationalPathBase<QExtourneLiqDef> {

    private static final long serialVersionUID = -139375820;

    public static final QExtourneLiqDef extourneLiqDef = new QExtourneLiqDef("EXTOURNE_LIQ_DEF");

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Long> eldId = createNumber("eldId", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QExtourneLiqDef> sysC0077908 = createPrimaryKey(eldId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> extourneLiqDefTyetidFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> extourneLiqDefDepidFk = createForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QExtourneLiqRepart> _extourneLiqRepartEldidFk = createInvForeignKey(eldId, "ELD_ID");

    public QExtourneLiqDef(String variable) {
        super(QExtourneLiqDef.class, forVariable(variable), "GFC", "EXTOURNE_LIQ_DEF");
        addMetadata();
    }

    public QExtourneLiqDef(String variable, String schema, String table) {
        super(QExtourneLiqDef.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QExtourneLiqDef(Path<? extends QExtourneLiqDef> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EXTOURNE_LIQ_DEF");
        addMetadata();
    }

    public QExtourneLiqDef(PathMetadata<?> metadata) {
        super(QExtourneLiqDef.class, metadata, "GFC", "EXTOURNE_LIQ_DEF");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(eldId, ColumnMetadata.named("ELD_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

