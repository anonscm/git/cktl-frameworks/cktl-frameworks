package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviFact is a Querydsl query type for QVSuiviFact
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviFact extends com.mysema.query.sql.RelationalPathBase<QVSuiviFact> {

    private static final long serialVersionUID = -8348495;

    public static final QVSuiviFact vSuiviFact = new QVSuiviFact("V_SUIVI_FACT");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> facId = createNumber("facId", Long.class);

    public final NumberPath<Long> facNumero = createNumber("facNumero", Long.class);

    public final DateTimePath<java.sql.Timestamp> fconDateSaisie = createDateTime("fconDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> fconMontantBudgetaire = createNumber("fconMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> fconMontantBudgetaireReste = createNumber("fconMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QVSuiviFact(String variable) {
        super(QVSuiviFact.class, forVariable(variable), "GFC", "V_SUIVI_FACT");
        addMetadata();
    }

    public QVSuiviFact(String variable, String schema, String table) {
        super(QVSuiviFact.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviFact(Path<? extends QVSuiviFact> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_FACT");
        addMetadata();
    }

    public QVSuiviFact(PathMetadata<?> metadata) {
        super(QVSuiviFact.class, metadata, "GFC", "V_SUIVI_FACT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(facId, ColumnMetadata.named("FAC_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(facNumero, ColumnMetadata.named("FAC_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(fconDateSaisie, ColumnMetadata.named("FCON_DATE_SAISIE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(fconMontantBudgetaire, ColumnMetadata.named("FCON_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(fconMontantBudgetaireReste, ColumnMetadata.named("FCON_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

