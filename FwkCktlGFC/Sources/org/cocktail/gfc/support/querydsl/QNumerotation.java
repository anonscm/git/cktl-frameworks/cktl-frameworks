package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QNumerotation is a Querydsl query type for QNumerotation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QNumerotation extends com.mysema.query.sql.RelationalPathBase<QNumerotation> {

    private static final long serialVersionUID = -1549222498;

    public static final QNumerotation numerotation = new QNumerotation("NUMEROTATION");

    public final NumberPath<Long> comOrdre = createNumber("comOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> numNumero = createNumber("numNumero", Long.class);

    public final NumberPath<Long> numOrdre = createNumber("numOrdre", Long.class);

    public final NumberPath<Long> tnuOrdre = createNumber("tnuOrdre", Long.class);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> numerotationExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QGestion> numerotationGesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QComptabilite> numerotationComOrdreFk = createForeignKey(comOrdre, "COM_ORDRE");

    public QNumerotation(String variable) {
        super(QNumerotation.class, forVariable(variable), "GFC", "NUMEROTATION");
        addMetadata();
    }

    public QNumerotation(String variable, String schema, String table) {
        super(QNumerotation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QNumerotation(Path<? extends QNumerotation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "NUMEROTATION");
        addMetadata();
    }

    public QNumerotation(PathMetadata<?> metadata) {
        super(QNumerotation.class, metadata, "GFC", "NUMEROTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comOrdre, ColumnMetadata.named("COM_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(4).ofType(Types.VARCHAR).withSize(10));
        addMetadata(numNumero, ColumnMetadata.named("NUM_NUMERO").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(numOrdre, ColumnMetadata.named("NUM_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnuOrdre, ColumnMetadata.named("TNU_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

