package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepParametre is a Querydsl query type for QDepParametre
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepParametre extends com.mysema.query.sql.RelationalPathBase<QDepParametre> {

    private static final long serialVersionUID = 1419864417;

    public static final QDepParametre depParametre = new QDepParametre("DEP_PARAMETRE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath parDescription = createString("parDescription");

    public final StringPath parKey = createString("parKey");

    public final StringPath parValue = createString("parValue");

    public QDepParametre(String variable) {
        super(QDepParametre.class, forVariable(variable), "GFC", "DEP_PARAMETRE");
        addMetadata();
    }

    public QDepParametre(String variable, String schema, String table) {
        super(QDepParametre.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepParametre(Path<? extends QDepParametre> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEP_PARAMETRE");
        addMetadata();
    }

    public QDepParametre(PathMetadata<?> metadata) {
        super(QDepParametre.class, metadata, "GFC", "DEP_PARAMETRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(parDescription, ColumnMetadata.named("PAR_DESCRIPTION").withIndex(4).ofType(Types.VARCHAR).withSize(200));
        addMetadata(parKey, ColumnMetadata.named("PAR_KEY").withIndex(2).ofType(Types.VARCHAR).withSize(2000).notNull());
        addMetadata(parValue, ColumnMetadata.named("PAR_VALUE").withIndex(3).ofType(Types.VARCHAR).withSize(2000));
    }

}

