package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPlancoAmortissement is a Querydsl query type for QPlancoAmortissement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPlancoAmortissement extends com.mysema.query.sql.RelationalPathBase<QPlancoAmortissement> {

    private static final long serialVersionUID = 1353241769;

    public static final QPlancoAmortissement plancoAmortissement = new QPlancoAmortissement("PLANCO_AMORTISSEMENT");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> pcaDuree = createNumber("pcaDuree", Long.class);

    public final NumberPath<Long> pcaId = createNumber("pcaId", Long.class);

    public final NumberPath<Long> pcoaId = createNumber("pcoaId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final com.mysema.query.sql.PrimaryKey<QPlancoAmortissement> plancoAmortissementPk = createPrimaryKey(pcaId);

    public final com.mysema.query.sql.ForeignKey<QPlanComptableAmo> pcaPcoaIdFk = createForeignKey(pcoaId, "PCOA_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> plancoAmortissementExeFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> pcaPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public QPlancoAmortissement(String variable) {
        super(QPlancoAmortissement.class, forVariable(variable), "GFC", "PLANCO_AMORTISSEMENT");
        addMetadata();
    }

    public QPlancoAmortissement(String variable, String schema, String table) {
        super(QPlancoAmortissement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPlancoAmortissement(Path<? extends QPlancoAmortissement> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PLANCO_AMORTISSEMENT");
        addMetadata();
    }

    public QPlancoAmortissement(PathMetadata<?> metadata) {
        super(QPlancoAmortissement.class, metadata, "GFC", "PLANCO_AMORTISSEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pcaDuree, ColumnMetadata.named("PCA_DUREE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcaId, ColumnMetadata.named("PCA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoaId, ColumnMetadata.named("PCOA_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

