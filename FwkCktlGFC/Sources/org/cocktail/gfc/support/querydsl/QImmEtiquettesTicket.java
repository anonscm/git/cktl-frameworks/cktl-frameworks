package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmEtiquettesTicket is a Querydsl query type for QImmEtiquettesTicket
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmEtiquettesTicket extends com.mysema.query.sql.RelationalPathBase<QImmEtiquettesTicket> {

    private static final long serialVersionUID = 51806537;

    public static final QImmEtiquettesTicket immEtiquettesTicket = new QImmEtiquettesTicket("IMM_ETIQUETTES_TICKET");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final NumberPath<Long> ticket = createNumber("ticket", Long.class);

    public QImmEtiquettesTicket(String variable) {
        super(QImmEtiquettesTicket.class, forVariable(variable), "GFC", "IMM_ETIQUETTES_TICKET");
        addMetadata();
    }

    public QImmEtiquettesTicket(String variable, String schema, String table) {
        super(QImmEtiquettesTicket.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmEtiquettesTicket(Path<? extends QImmEtiquettesTicket> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_ETIQUETTES_TICKET");
        addMetadata();
    }

    public QImmEtiquettesTicket(PathMetadata<?> metadata) {
        super(QImmEtiquettesTicket.class, metadata, "GFC", "IMM_ETIQUETTES_TICKET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ticket, ColumnMetadata.named("TICKET").withIndex(1).ofType(Types.DECIMAL).withSize(0));
    }

}

