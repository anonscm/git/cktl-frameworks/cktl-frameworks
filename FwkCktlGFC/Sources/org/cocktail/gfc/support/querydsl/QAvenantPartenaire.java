package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAvenantPartenaire is a Querydsl query type for QAvenantPartenaire
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAvenantPartenaire extends com.mysema.query.sql.RelationalPathBase<QAvenantPartenaire> {

    private static final long serialVersionUID = 1874167231;

    public static final QAvenantPartenaire avenantPartenaire = new QAvenantPartenaire("AVENANT_PARTENAIRE");

    public final DateTimePath<java.sql.Timestamp> apDateSignature = createDateTime("apDateSignature", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> apMontant = createNumber("apMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> apOrdre = createNumber("apOrdre", Long.class);

    public final StringPath apPrincipal = createString("apPrincipal");

    public final StringPath apRefExterneParten = createString("apRefExterneParten");

    public final NumberPath<Long> avtOrdre = createNumber("avtOrdre", Long.class);

    public final StringPath cStructure = createString("cStructure");

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final NumberPath<Long> typePartOrdre = createNumber("typePartOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAvenantPartenaire> avenantPartenairePk = createPrimaryKey(apOrdre);

    public QAvenantPartenaire(String variable) {
        super(QAvenantPartenaire.class, forVariable(variable), "GFC", "AVENANT_PARTENAIRE");
        addMetadata();
    }

    public QAvenantPartenaire(String variable, String schema, String table) {
        super(QAvenantPartenaire.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAvenantPartenaire(Path<? extends QAvenantPartenaire> path) {
        super(path.getType(), path.getMetadata(), "GFC", "AVENANT_PARTENAIRE");
        addMetadata();
    }

    public QAvenantPartenaire(PathMetadata<?> metadata) {
        super(QAvenantPartenaire.class, metadata, "GFC", "AVENANT_PARTENAIRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(apDateSignature, ColumnMetadata.named("AP_DATE_SIGNATURE").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(apMontant, ColumnMetadata.named("AP_MONTANT").withIndex(5).ofType(Types.DECIMAL).withSize(20).withDigits(2));
        addMetadata(apOrdre, ColumnMetadata.named("AP_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(apPrincipal, ColumnMetadata.named("AP_PRINCIPAL").withIndex(6).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(apRefExterneParten, ColumnMetadata.named("AP_REF_EXTERNE_PARTEN").withIndex(9).ofType(Types.VARCHAR).withSize(100));
        addMetadata(avtOrdre, ColumnMetadata.named("AVT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(cStructure, ColumnMetadata.named("C_STRUCTURE").withIndex(4).ofType(Types.VARCHAR).withSize(10));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(typePartOrdre, ColumnMetadata.named("TYPE_PART_ORDRE").withIndex(7).ofType(Types.DECIMAL).withSize(38));
    }

}

