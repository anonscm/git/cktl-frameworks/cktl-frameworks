package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVPlanComptableNonImprime is a Querydsl query type for QVPlanComptableNonImprime
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVPlanComptableNonImprime extends com.mysema.query.sql.RelationalPathBase<QVPlanComptableNonImprime> {

    private static final long serialVersionUID = -1149658481;

    public static final QVPlanComptableNonImprime vPlanComptableNonImprime = new QVPlanComptableNonImprime("V_PLAN_COMPTABLE_NON_IMPRIME");

    public final StringPath pcoNum = createString("pcoNum");

    public QVPlanComptableNonImprime(String variable) {
        super(QVPlanComptableNonImprime.class, forVariable(variable), "GFC", "V_PLAN_COMPTABLE_NON_IMPRIME");
        addMetadata();
    }

    public QVPlanComptableNonImprime(String variable, String schema, String table) {
        super(QVPlanComptableNonImprime.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVPlanComptableNonImprime(Path<? extends QVPlanComptableNonImprime> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_PLAN_COMPTABLE_NON_IMPRIME");
        addMetadata();
    }

    public QVPlanComptableNonImprime(PathMetadata<?> metadata) {
        super(QVPlanComptableNonImprime.class, metadata, "GFC", "V_PLAN_COMPTABLE_NON_IMPRIME");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(1).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

