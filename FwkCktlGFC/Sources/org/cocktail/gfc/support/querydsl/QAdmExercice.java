package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmExercice is a Querydsl query type for QAdmExercice
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmExercice extends com.mysema.query.sql.RelationalPathBase<QAdmExercice> {

    private static final long serialVersionUID = 441001745;

    public static final QAdmExercice admExercice = new QAdmExercice("ADM_EXERCICE");

    public final DateTimePath<java.sql.Timestamp> exeCloture = createDateTime("exeCloture", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeExercice = createNumber("exeExercice", Integer.class);

    public final DateTimePath<java.sql.Timestamp> exeInventaire = createDateTime("exeInventaire", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> exeOuverture = createDateTime("exeOuverture", java.sql.Timestamp.class);

    public final StringPath exeStat = createString("exeStat");

    public final StringPath exeStatEng = createString("exeStatEng");

    public final StringPath exeStatFac = createString("exeStatFac");

    public final StringPath exeStatLiq = createString("exeStatLiq");

    public final StringPath exeStatRec = createString("exeStatRec");

    public final StringPath exeType = createString("exeType");

    public final com.mysema.query.sql.PrimaryKey<QAdmExercice> admExercicePk = createPrimaryKey(exeOrdre);

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlConvention> _engageCtrlConExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBrouillard> _brouillardExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepExer> _admDestDepExeExerciceFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBilanPoste> _bilanPosteExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepNumerotation> _depNumerotationExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCptDepense> _depenseExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlAction> _depenseCtrlActExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlPlanco> _pdepenseCtrlPlaExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlHorsMarche> _engageCtrlHmExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputationPlanco> _reimpPlancoExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QModePaiement> _modePaiementExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPaiement> _paiementExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputationAction> _reimpActionExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlanComptableExer> _plancoExerExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlAction> _pdepenseCtrlActExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QNumerotation> _numerotationExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlMarche> _engageCtrlMarcExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlPlanco> _depenseCtrlPlanExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlMarche> _commandeCtrlMarExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QChequeDetailEcriture> _chequeDetailEcritureExe_fk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcriture> _ecritureExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> _engageBudgetExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereau> _bordereauExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlConvention> _commandeCtrlCoExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlAnalytique> _commandeCtrlAnExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputation> _reimputationExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmNomenclatureEtatCred> _admNomenEtatCreditExeOFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurFonctExer> _admUfeExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlAnalytique> _fanaExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepensePapier> _depensePapierExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmEbExer> _admExerciceEbExerFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QMandatBrouillard> _mandatBrouillardExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlancoVisa> _plancoVisaExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlAnalytique> _depenseCtrlAnalExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlancoCtp> _rpcoctpExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRetenue> _retenueExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> _commandeBudgetExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QMandat> _mandatExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlMarche> _pdepenseCtrlMarExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlancoAmortissement> _plancoAmortissementExeFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlAnalytique> _ranaExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QGestionExercice> _gestionExerciceExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlConvention> _rconExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEmargement> _emargementExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTitreBrouillard> _titreBrouillardExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacture> _factureExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCptParametre> _cptParametreExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecetteExer> _admOrigRecExeExerciceFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlMarche> _depenseCtrlMarExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlPlanco> _commandeCtrlPlExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlAnalytique> _engageCtrlAnalExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlHorsMarche> _pdepenseCtrlHmExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlConvention> _pdepenseCtrlConExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommande> _commandeExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> _admTypeCreditExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereauRejet> _bordereauRejetExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBudEnvExerNatDep> _admExerciceEnvExerNdFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTitre> _titreExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTitreDetailEcriture> _titreDetailEcritureExeOFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecParametres> _parametresExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBudBudget> _budBudgetExerFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlHorsMarche> _depenseCtrlHmExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCptRecette> _recetteExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlHorsMarche> _commandeCtrlHomExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereauBrouillard> _bordereauBrouillardExeOrFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEmargementDetail> _emargementDetailExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcheancier> _echeancierExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmNomenclatureLolfDest> _admNldrExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlAction> _ractExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmNomenclaturePrevRec> _admNomenPrevRecExeOrdrFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlConvention> _depenseCtrlConExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QModeRecouvrement> _modeRecouvrementExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureRecExercice> _admExerciceNatRecExerFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlAnalytique> _pdepenseCtrlAnaExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecettePapier> _recettePapierExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> _pdepenseBudgetExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCptReimputation> _cptReimputationExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QChequeBrouillard> _chequeBrouillardExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlAction> _commandeCtrlActExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QVisaAvMission> _visaAvMissionExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlPlanco> _engageCtrlPlanExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmEbSignataire> _admEbSignataireExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QOrdreDePaiement> _ordreDePaiementExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecetteRelance> _recetteRelanceExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> _depenseBudgetExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlPlanco> _fpcoExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> _ecritureDetailExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmEbProrata> _admEbProrataExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecRecette> _recRecetteExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecouvrement> _recouvrementExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlConvention> _fconExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlanco> _rpcoExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAbricotBordSelection> _abricotBordSelectionExe_fk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlAction> _factExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlAction> _engageCtrlActiExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QImmParametre> _immParametreExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecNumerotation> _exeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFactureExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QMandatDetailEcriture> _mandatDetailEcritureExe_fk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureDepExercice> _budNatdepAdmExerciceFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlancoTva> _rpcotvaExeOrdreFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public QAdmExercice(String variable) {
        super(QAdmExercice.class, forVariable(variable), "GFC", "ADM_EXERCICE");
        addMetadata();
    }

    public QAdmExercice(String variable, String schema, String table) {
        super(QAdmExercice.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmExercice(Path<? extends QAdmExercice> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_EXERCICE");
        addMetadata();
    }

    public QAdmExercice(PathMetadata<?> metadata) {
        super(QAdmExercice.class, metadata, "GFC", "ADM_EXERCICE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeCloture, ColumnMetadata.named("EXE_CLOTURE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeExercice, ColumnMetadata.named("EXE_EXERCICE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(exeInventaire, ColumnMetadata.named("EXE_INVENTAIRE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(exeOuverture, ColumnMetadata.named("EXE_OUVERTURE").withIndex(9).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeStat, ColumnMetadata.named("EXE_STAT").withIndex(4).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(exeStatEng, ColumnMetadata.named("EXE_STAT_ENG").withIndex(5).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(exeStatFac, ColumnMetadata.named("EXE_STAT_FAC").withIndex(6).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(exeStatLiq, ColumnMetadata.named("EXE_STAT_LIQ").withIndex(7).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(exeStatRec, ColumnMetadata.named("EXE_STAT_REC").withIndex(8).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(exeType, ColumnMetadata.named("EXE_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1).notNull());
    }

}

