package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviRecettePosit is a Querydsl query type for QVSuiviRecettePosit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviRecettePosit extends com.mysema.query.sql.RelationalPathBase<QVSuiviRecettePosit> {

    private static final long serialVersionUID = 1906939316;

    public static final QVSuiviRecettePosit vSuiviRecettePosit = new QVSuiviRecettePosit("V_SUIVI_RECETTE_POSIT");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> orgPere = createSimple("orgPere", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> totalPosit = createSimple("totalPosit", Object.class);

    public QVSuiviRecettePosit(String variable) {
        super(QVSuiviRecettePosit.class, forVariable(variable), "GFC", "V_SUIVI_RECETTE_POSIT");
        addMetadata();
    }

    public QVSuiviRecettePosit(String variable, String schema, String table) {
        super(QVSuiviRecettePosit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviRecettePosit(Path<? extends QVSuiviRecettePosit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_RECETTE_POSIT");
        addMetadata();
    }

    public QVSuiviRecettePosit(PathMetadata<?> metadata) {
        super(QVSuiviRecettePosit.class, metadata, "GFC", "V_SUIVI_RECETTE_POSIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(orgPere, ColumnMetadata.named("ORG_PERE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPosit, ColumnMetadata.named("TOTAL_POSIT").withIndex(6).ofType(Types.OTHER).withSize(0));
    }

}

