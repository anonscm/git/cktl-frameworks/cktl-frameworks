package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeDocument is a Querydsl query type for QTypeDocument
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeDocument extends com.mysema.query.sql.RelationalPathBase<QTypeDocument> {

    private static final long serialVersionUID = 1004528118;

    public static final QTypeDocument typeDocument = new QTypeDocument("TYPE_DOCUMENT");

    public final NumberPath<Long> tcomId = createNumber("tcomId", Long.class);

    public final StringPath tcomLibelle = createString("tcomLibelle");

    public final com.mysema.query.sql.PrimaryKey<QTypeDocument> typeDocumentPk = createPrimaryKey(tcomId);

    public QTypeDocument(String variable) {
        super(QTypeDocument.class, forVariable(variable), "GFC", "TYPE_DOCUMENT");
        addMetadata();
    }

    public QTypeDocument(String variable, String schema, String table) {
        super(QTypeDocument.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeDocument(Path<? extends QTypeDocument> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_DOCUMENT");
        addMetadata();
    }

    public QTypeDocument(PathMetadata<?> metadata) {
        super(QTypeDocument.class, metadata, "GFC", "TYPE_DOCUMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tcomId, ColumnMetadata.named("TCOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcomLibelle, ColumnMetadata.named("TCOM_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

