package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVExtourneCreditsCr is a Querydsl query type for QVExtourneCreditsCr
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVExtourneCreditsCr extends com.mysema.query.sql.RelationalPathBase<QVExtourneCreditsCr> {

    private static final long serialVersionUID = 180314418;

    public static final QVExtourneCreditsCr vExtourneCreditsCr = new QVExtourneCreditsCr("V_EXTOURNE_CREDITS_CR");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> orgIdCr = createSimple("orgIdCr", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> vecMontantBudConsomme = createSimple("vecMontantBudConsomme", Object.class);

    public final SimplePath<Object> vecMontantBudDisponible = createSimple("vecMontantBudDisponible", Object.class);

    public final SimplePath<Object> vecMontantBudDispoReel = createSimple("vecMontantBudDispoReel", Object.class);

    public final SimplePath<Object> vecMontantBudDispoUb = createSimple("vecMontantBudDispoUb", Object.class);

    public final SimplePath<Object> vecMontantBudInitial = createSimple("vecMontantBudInitial", Object.class);

    public final SimplePath<Object> vecMontantHt = createSimple("vecMontantHt", Object.class);

    public final SimplePath<Object> vecMontantTtc = createSimple("vecMontantTtc", Object.class);

    public QVExtourneCreditsCr(String variable) {
        super(QVExtourneCreditsCr.class, forVariable(variable), "GFC", "V_EXTOURNE_CREDITS_CR");
        addMetadata();
    }

    public QVExtourneCreditsCr(String variable, String schema, String table) {
        super(QVExtourneCreditsCr.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVExtourneCreditsCr(Path<? extends QVExtourneCreditsCr> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EXTOURNE_CREDITS_CR");
        addMetadata();
    }

    public QVExtourneCreditsCr(PathMetadata<?> metadata) {
        super(QVExtourneCreditsCr.class, metadata, "GFC", "V_EXTOURNE_CREDITS_CR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdCr, ColumnMetadata.named("ORG_ID_CR").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudConsomme, ColumnMetadata.named("VEC_MONTANT_BUD_CONSOMME").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudDisponible, ColumnMetadata.named("VEC_MONTANT_BUD_DISPONIBLE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudDispoReel, ColumnMetadata.named("VEC_MONTANT_BUD_DISPO_REEL").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudDispoUb, ColumnMetadata.named("VEC_MONTANT_BUD_DISPO_UB").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudInitial, ColumnMetadata.named("VEC_MONTANT_BUD_INITIAL").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantHt, ColumnMetadata.named("VEC_MONTANT_HT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantTtc, ColumnMetadata.named("VEC_MONTANT_TTC").withIndex(5).ofType(Types.OTHER).withSize(0));
    }

}

