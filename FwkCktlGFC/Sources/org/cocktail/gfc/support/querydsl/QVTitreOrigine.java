package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVTitreOrigine is a Querydsl query type for QVTitreOrigine
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVTitreOrigine extends com.mysema.query.sql.RelationalPathBase<QVTitreOrigine> {

    private static final long serialVersionUID = -1397768510;

    public static final QVTitreOrigine vTitreOrigine = new QVTitreOrigine("V_TITRE_ORIGINE");

    public final NumberPath<Long> annId = createNumber("annId", Long.class);

    public final NumberPath<Long> borNumero = createNumber("borNumero", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> recIdOrig = createNumber("recIdOrig", Long.class);

    public final NumberPath<Long> titId = createNumber("titId", Long.class);

    public final NumberPath<Long> titNumero = createNumber("titNumero", Long.class);

    public QVTitreOrigine(String variable) {
        super(QVTitreOrigine.class, forVariable(variable), "GFC", "V_TITRE_ORIGINE");
        addMetadata();
    }

    public QVTitreOrigine(String variable, String schema, String table) {
        super(QVTitreOrigine.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVTitreOrigine(Path<? extends QVTitreOrigine> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_TITRE_ORIGINE");
        addMetadata();
    }

    public QVTitreOrigine(PathMetadata<?> metadata) {
        super(QVTitreOrigine.class, metadata, "GFC", "V_TITRE_ORIGINE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(annId, ColumnMetadata.named("ANN_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(borNumero, ColumnMetadata.named("BOR_NUMERO").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(recIdOrig, ColumnMetadata.named("REC_ID_ORIG").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(titNumero, ColumnMetadata.named("TIT_NUMERO").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

