package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepenseCtrlMarche is a Querydsl query type for QDepenseCtrlMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepenseCtrlMarche extends com.mysema.query.sql.RelationalPathBase<QDepenseCtrlMarche> {

    private static final long serialVersionUID = -1528710442;

    public static final QDepenseCtrlMarche depenseCtrlMarche = new QDepenseCtrlMarche("DEPENSE_CTRL_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<java.math.BigDecimal> dmarHtSaisie = createNumber("dmarHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dmarId = createNumber("dmarId", Long.class);

    public final NumberPath<java.math.BigDecimal> dmarMontantBudgetaire = createNumber("dmarMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dmarTtcSaisie = createNumber("dmarTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dmarTvaSaisie = createNumber("dmarTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QDepenseCtrlMarche> depenseCtrlMarchePk = createPrimaryKey(dmarId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depenseCtrlMarExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAttribution> depenseCtrlMarAttOrdreFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> depenseCtrlMarDepIdFk = createForeignKey(depId, "DEP_ID");

    public QDepenseCtrlMarche(String variable) {
        super(QDepenseCtrlMarche.class, forVariable(variable), "GFC", "DEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public QDepenseCtrlMarche(String variable, String schema, String table) {
        super(QDepenseCtrlMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepenseCtrlMarche(Path<? extends QDepenseCtrlMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public QDepenseCtrlMarche(PathMetadata<?> metadata) {
        super(QDepenseCtrlMarche.class, metadata, "GFC", "DEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dmarHtSaisie, ColumnMetadata.named("DMAR_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dmarId, ColumnMetadata.named("DMAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dmarMontantBudgetaire, ColumnMetadata.named("DMAR_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dmarTtcSaisie, ColumnMetadata.named("DMAR_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dmarTvaSaisie, ColumnMetadata.named("DMAR_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

