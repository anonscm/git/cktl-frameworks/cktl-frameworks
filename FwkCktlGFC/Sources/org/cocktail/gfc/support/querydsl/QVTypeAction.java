package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVTypeAction is a Querydsl query type for QVTypeAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVTypeAction extends com.mysema.query.sql.RelationalPathBase<QVTypeAction> {

    private static final long serialVersionUID = 1194064773;

    public static final QVTypeAction vTypeAction = new QVTypeAction("V_TYPE_ACTION");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> tyacCode = createSimple("tyacCode", Object.class);

    public final SimplePath<Object> tyacId = createSimple("tyacId", Object.class);

    public final SimplePath<Object> tyacLibelle = createSimple("tyacLibelle", Object.class);

    public final SimplePath<Object> tyacNiveau = createSimple("tyacNiveau", Object.class);

    public final SimplePath<Object> tyacProgramme = createSimple("tyacProgramme", Object.class);

    public final SimplePath<Object> tyacType = createSimple("tyacType", Object.class);

    public final SimplePath<Object> tyetId = createSimple("tyetId", Object.class);

    public QVTypeAction(String variable) {
        super(QVTypeAction.class, forVariable(variable), "GFC", "V_TYPE_ACTION");
        addMetadata();
    }

    public QVTypeAction(String variable, String schema, String table) {
        super(QVTypeAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVTypeAction(Path<? extends QVTypeAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_TYPE_ACTION");
        addMetadata();
    }

    public QVTypeAction(PathMetadata<?> metadata) {
        super(QVTypeAction.class, metadata, "GFC", "V_TYPE_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(tyacCode, ColumnMetadata.named("TYAC_CODE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tyacLibelle, ColumnMetadata.named("TYAC_LIBELLE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(tyacNiveau, ColumnMetadata.named("TYAC_NIVEAU").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(tyacProgramme, ColumnMetadata.named("TYAC_PROGRAMME").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(tyacType, ColumnMetadata.named("TYAC_TYPE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(8).ofType(Types.OTHER).withSize(0));
    }

}

