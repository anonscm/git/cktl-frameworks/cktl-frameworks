package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEngageCtrlConvention is a Querydsl query type for QEngageCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEngageCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QEngageCtrlConvention> {

    private static final long serialVersionUID = 608813182;

    public static final QEngageCtrlConvention engageCtrlConvention = new QEngageCtrlConvention("ENGAGE_CTRL_CONVENTION");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> econDateSaisie = createDateTime("econDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> econHtSaisie = createNumber("econHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> econId = createNumber("econId", Long.class);

    public final NumberPath<java.math.BigDecimal> econMontantBudgetaire = createNumber("econMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> econMontantBudgetaireReste = createNumber("econMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> econTtcSaisie = createNumber("econTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> econTvaSaisie = createNumber("econTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QEngageCtrlConvention> engageCtrlConventionPk = createPrimaryKey(econId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> engageCtrlConExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> engageCtrlConEngIdFk = createForeignKey(engId, "ENG_ID");

    public QEngageCtrlConvention(String variable) {
        super(QEngageCtrlConvention.class, forVariable(variable), "GFC", "ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public QEngageCtrlConvention(String variable, String schema, String table) {
        super(QEngageCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEngageCtrlConvention(Path<? extends QEngageCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public QEngageCtrlConvention(PathMetadata<?> metadata) {
        super(QEngageCtrlConvention.class, metadata, "GFC", "ENGAGE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(econDateSaisie, ColumnMetadata.named("ECON_DATE_SAISIE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(econHtSaisie, ColumnMetadata.named("ECON_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econId, ColumnMetadata.named("ECON_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(econMontantBudgetaire, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econMontantBudgetaireReste, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE_RESTE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econTtcSaisie, ColumnMetadata.named("ECON_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(econTvaSaisie, ColumnMetadata.named("ECON_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

