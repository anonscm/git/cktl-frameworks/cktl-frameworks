package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepenseCtrlConvention is a Querydsl query type for QVDepenseCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepenseCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QVDepenseCtrlConvention> {

    private static final long serialVersionUID = 1855392657;

    public static final QVDepenseCtrlConvention vDepenseCtrlConvention = new QVDepenseCtrlConvention("V_DEPENSE_CTRL_CONVENTION");

    public final SimplePath<Object> convOrdre = createSimple("convOrdre", Object.class);

    public final SimplePath<Object> dconHtSaisie = createSimple("dconHtSaisie", Object.class);

    public final SimplePath<Object> dconId = createSimple("dconId", Object.class);

    public final SimplePath<Object> dconMontantBudgetaire = createSimple("dconMontantBudgetaire", Object.class);

    public final SimplePath<Object> dconTtcSaisie = createSimple("dconTtcSaisie", Object.class);

    public final SimplePath<Object> dconTvaSaisie = createSimple("dconTvaSaisie", Object.class);

    public final SimplePath<Object> depId = createSimple("depId", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public QVDepenseCtrlConvention(String variable) {
        super(QVDepenseCtrlConvention.class, forVariable(variable), "GFC", "V_DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QVDepenseCtrlConvention(String variable, String schema, String table) {
        super(QVDepenseCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepenseCtrlConvention(Path<? extends QVDepenseCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QVDepenseCtrlConvention(PathMetadata<?> metadata) {
        super(QVDepenseCtrlConvention.class, metadata, "GFC", "V_DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(dconHtSaisie, ColumnMetadata.named("DCON_HT_SAISIE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(dconId, ColumnMetadata.named("DCON_ID").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(dconMontantBudgetaire, ColumnMetadata.named("DCON_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(dconTtcSaisie, ColumnMetadata.named("DCON_TTC_SAISIE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(dconTvaSaisie, ColumnMetadata.named("DCON_TVA_SAISIE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

