package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecRecette is a Querydsl query type for QRecRecette
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecRecette extends com.mysema.query.sql.RelationalPathBase<QRecRecette> {

    private static final long serialVersionUID = 1868406017;

    public static final QRecRecette recRecette = new QRecRecette("REC_RECETTE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> facId = createNumber("facId", Long.class);

    public final DateTimePath<java.sql.Timestamp> recDateSaisie = createDateTime("recDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> recHtSaisie = createNumber("recHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final NumberPath<Long> recIdReduction = createNumber("recIdReduction", Long.class);

    public final StringPath recLib = createString("recLib");

    public final NumberPath<java.math.BigDecimal> recMontantBudgetaire = createNumber("recMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<Long> recNumero = createNumber("recNumero", Long.class);

    public final NumberPath<java.math.BigDecimal> recTtcSaisie = createNumber("recTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> recTvaSaisie = createNumber("recTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> rppId = createNumber("rppId", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRecRecette> recRecettePk = createPrimaryKey(recId);

    public final com.mysema.query.sql.ForeignKey<QFacture> recRecetteFacIdFk = createForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QRecettePapier> recRecetteRppIdFk = createForeignKey(rppId, "RPP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> recRecetteTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> recRecetteUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> recRecetteExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecRecette> recRecetteRecIdReducFk = createForeignKey(recIdReduction, "REC_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> recRecetteTypetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QPiDepRec> _pdrRecIdFk = createInvForeignKey(recId, "REC_ID");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlAnalytique> _ranaRecIdFk = createInvForeignKey(recId, "REC_ID");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlAction> _ractRecIdFk = createInvForeignKey(recId, "REC_ID");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlanco> _rpcoRecIdFk = createInvForeignKey(recId, "REC_ID");

    public final com.mysema.query.sql.ForeignKey<QRecRecette> _recRecetteRecIdReducFk = createInvForeignKey(recId, "REC_ID_REDUCTION");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlConvention> _rconRecIdFk = createInvForeignKey(recId, "REC_ID");

    public QRecRecette(String variable) {
        super(QRecRecette.class, forVariable(variable), "GFC", "REC_RECETTE");
        addMetadata();
    }

    public QRecRecette(String variable, String schema, String table) {
        super(QRecRecette.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecRecette(Path<? extends QRecRecette> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REC_RECETTE");
        addMetadata();
    }

    public QRecRecette(PathMetadata<?> metadata) {
        super(QRecRecette.class, metadata, "GFC", "REC_RECETTE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(facId, ColumnMetadata.named("FAC_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recDateSaisie, ColumnMetadata.named("REC_DATE_SAISIE").withIndex(7).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(recHtSaisie, ColumnMetadata.named("REC_HT_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recIdReduction, ColumnMetadata.named("REC_ID_REDUCTION").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(recLib, ColumnMetadata.named("REC_LIB").withIndex(6).ofType(Types.VARCHAR).withSize(500));
        addMetadata(recMontantBudgetaire, ColumnMetadata.named("REC_MONTANT_BUDGETAIRE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recNumero, ColumnMetadata.named("REC_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recTtcSaisie, ColumnMetadata.named("REC_TTC_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recTvaSaisie, ColumnMetadata.named("REC_TVA_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(rppId, ColumnMetadata.named("RPP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

