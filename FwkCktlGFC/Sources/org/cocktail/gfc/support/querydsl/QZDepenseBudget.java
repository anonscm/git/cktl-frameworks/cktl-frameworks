package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZDepenseBudget is a Querydsl query type for QZDepenseBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZDepenseBudget extends com.mysema.query.sql.RelationalPathBase<QZDepenseBudget> {

    private static final long serialVersionUID = -1894656490;

    public static final QZDepenseBudget zDepenseBudget = new QZDepenseBudget("Z_DEPENSE_BUDGET");

    public final NumberPath<java.math.BigDecimal> depHtSaisie = createNumber("depHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Long> depIdReversement = createNumber("depIdReversement", Long.class);

    public final NumberPath<java.math.BigDecimal> depMontantBudgetaire = createNumber("depMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> depTtcSaisie = createNumber("depTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> depTvaSaisie = createNumber("depTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> zdepDate = createDateTime("zdepDate", java.sql.Timestamp.class);

    public final NumberPath<Long> zdepId = createNumber("zdepId", Long.class);

    public final NumberPath<Long> zdepUtlOrdre = createNumber("zdepUtlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZDepenseBudget> sysC0075889 = createPrimaryKey(zdepId);

    public final com.mysema.query.sql.ForeignKey<QZDepenseCtrlHorsMarche> _zDepenseCtrlHmZdepIdFk = createInvForeignKey(zdepId, "ZDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QZDepenseCtrlMarche> _zDepenseCtrlMarcZdepIdFk = createInvForeignKey(zdepId, "ZDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QZDepenseCtrlConvention> _zDepenseCtrlConZdepIdFk = createInvForeignKey(zdepId, "ZDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QZDepenseCtrlAnalytique> _zDepenseCtrlAnalZdepIdFk = createInvForeignKey(zdepId, "ZDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QZDepenseCtrlAction> _zDepenseCtrlActiZdepIdFk = createInvForeignKey(zdepId, "ZDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QZDepenseCtrlPlanco> _zDepenseCtrlPlanZdepIdFk = createInvForeignKey(zdepId, "ZDEP_ID");

    public QZDepenseBudget(String variable) {
        super(QZDepenseBudget.class, forVariable(variable), "GFC", "Z_DEPENSE_BUDGET");
        addMetadata();
    }

    public QZDepenseBudget(String variable, String schema, String table) {
        super(QZDepenseBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZDepenseBudget(Path<? extends QZDepenseBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_DEPENSE_BUDGET");
        addMetadata();
    }

    public QZDepenseBudget(PathMetadata<?> metadata) {
        super(QZDepenseBudget.class, metadata, "GFC", "Z_DEPENSE_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depHtSaisie, ColumnMetadata.named("DEP_HT_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depIdReversement, ColumnMetadata.named("DEP_ID_REVERSEMENT").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depMontantBudgetaire, ColumnMetadata.named("DEP_MONTANT_BUDGETAIRE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depTtcSaisie, ColumnMetadata.named("DEP_TTC_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depTvaSaisie, ColumnMetadata.named("DEP_TVA_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdepDate, ColumnMetadata.named("ZDEP_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(zdepId, ColumnMetadata.named("ZDEP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdepUtlOrdre, ColumnMetadata.named("ZDEP_UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

