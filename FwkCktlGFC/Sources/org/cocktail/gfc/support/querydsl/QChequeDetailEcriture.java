package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QChequeDetailEcriture is a Querydsl query type for QChequeDetailEcriture
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QChequeDetailEcriture extends com.mysema.query.sql.RelationalPathBase<QChequeDetailEcriture> {

    private static final long serialVersionUID = -132666436;

    public static final QChequeDetailEcriture chequeDetailEcriture = new QChequeDetailEcriture("CHEQUE_DETAIL_ECRITURE");

    public final DateTimePath<java.sql.Timestamp> cdeDate = createDateTime("cdeDate", java.sql.Timestamp.class);

    public final NumberPath<Long> cdeOrdre = createNumber("cdeOrdre", Long.class);

    public final NumberPath<Long> cheOrdre = createNumber("cheOrdre", Long.class);

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QChequeDetailEcriture> chequeDetailEcriturePk = createPrimaryKey(cdeOrdre);

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> chequeDetailEcritureEcd_fk = createForeignKey(ecdOrdre, "ECD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> chequeDetailEcritureExe_fk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QChequeDetailEcriture(String variable) {
        super(QChequeDetailEcriture.class, forVariable(variable), "GFC", "CHEQUE_DETAIL_ECRITURE");
        addMetadata();
    }

    public QChequeDetailEcriture(String variable, String schema, String table) {
        super(QChequeDetailEcriture.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QChequeDetailEcriture(Path<? extends QChequeDetailEcriture> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CHEQUE_DETAIL_ECRITURE");
        addMetadata();
    }

    public QChequeDetailEcriture(PathMetadata<?> metadata) {
        super(QChequeDetailEcriture.class, metadata, "GFC", "CHEQUE_DETAIL_ECRITURE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cdeDate, ColumnMetadata.named("CDE_DATE").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(cdeOrdre, ColumnMetadata.named("CDE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cheOrdre, ColumnMetadata.named("CHE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(4));
    }

}

