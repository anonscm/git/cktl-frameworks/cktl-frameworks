package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmCleInventComptModif is a Querydsl query type for QImmCleInventComptModif
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmCleInventComptModif extends com.mysema.query.sql.RelationalPathBase<QImmCleInventComptModif> {

    private static final long serialVersionUID = 1704175432;

    public static final QImmCleInventComptModif immCleInventComptModif = new QImmCleInventComptModif("IMM_CLE_INVENT_COMPT_MODIF");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final DateTimePath<java.sql.Timestamp> clicmDate = createDateTime("clicmDate", java.sql.Timestamp.class);

    public final NumberPath<Long> clicmDureeNew = createNumber("clicmDureeNew", Long.class);

    public final NumberPath<Long> clicmDureeOld = createNumber("clicmDureeOld", Long.class);

    public final NumberPath<Long> clicmId = createNumber("clicmId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmCleInventComptModif> immCleInventComptModifPk = createPrimaryKey(clicmId);

    public final com.mysema.query.sql.ForeignKey<QImmCleInventCompt> cleInvComptMClicIdFk = createForeignKey(clicId, "CLIC_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> cleInvComptMUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public QImmCleInventComptModif(String variable) {
        super(QImmCleInventComptModif.class, forVariable(variable), "GFC", "IMM_CLE_INVENT_COMPT_MODIF");
        addMetadata();
    }

    public QImmCleInventComptModif(String variable, String schema, String table) {
        super(QImmCleInventComptModif.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmCleInventComptModif(Path<? extends QImmCleInventComptModif> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_CLE_INVENT_COMPT_MODIF");
        addMetadata();
    }

    public QImmCleInventComptModif(PathMetadata<?> metadata) {
        super(QImmCleInventComptModif.class, metadata, "GFC", "IMM_CLE_INVENT_COMPT_MODIF");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(clicmDate, ColumnMetadata.named("CLICM_DATE").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(clicmDureeNew, ColumnMetadata.named("CLICM_DUREE_NEW").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clicmDureeOld, ColumnMetadata.named("CLICM_DUREE_OLD").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clicmId, ColumnMetadata.named("CLICM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

