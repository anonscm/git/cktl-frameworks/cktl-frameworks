package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QProjet is a Querydsl query type for QProjet
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QProjet extends com.mysema.query.sql.RelationalPathBase<QProjet> {

    private static final long serialVersionUID = -1581958899;

    public static final QProjet projet = new QProjet("PROJET");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> persIdResponsable = createNumber("persIdResponsable", Long.class);

    public final DateTimePath<java.sql.Timestamp> pjtDateCreation = createDateTime("pjtDateCreation", java.sql.Timestamp.class);

    public final NumberPath<Long> pjtId = createNumber("pjtId", Long.class);

    public final NumberPath<Long> pjtIdPere = createNumber("pjtIdPere", Long.class);

    public final StringPath pjtLibelle = createString("pjtLibelle");

    public final NumberPath<Long> tpjtId = createNumber("tpjtId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QProjet> projetPk = createPrimaryKey(pjtId);

    public QProjet(String variable) {
        super(QProjet.class, forVariable(variable), "GFC", "PROJET");
        addMetadata();
    }

    public QProjet(String variable, String schema, String table) {
        super(QProjet.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QProjet(Path<? extends QProjet> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PROJET");
        addMetadata();
    }

    public QProjet(PathMetadata<?> metadata) {
        super(QProjet.class, metadata, "GFC", "PROJET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(persIdResponsable, ColumnMetadata.named("PERS_ID_RESPONSABLE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pjtDateCreation, ColumnMetadata.named("PJT_DATE_CREATION").withIndex(7).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(pjtId, ColumnMetadata.named("PJT_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pjtIdPere, ColumnMetadata.named("PJT_ID_PERE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pjtLibelle, ColumnMetadata.named("PJT_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(250));
        addMetadata(tpjtId, ColumnMetadata.named("TPJT_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
    }

}

