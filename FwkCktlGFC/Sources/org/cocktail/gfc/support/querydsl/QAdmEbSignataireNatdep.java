package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmEbSignataireNatdep is a Querydsl query type for QAdmEbSignataireNatdep
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmEbSignataireNatdep extends com.mysema.query.sql.RelationalPathBase<QAdmEbSignataireNatdep> {

    private static final long serialVersionUID = 1066678495;

    public static final QAdmEbSignataireNatdep admEbSignataireNatdep = new QAdmEbSignataireNatdep("ADM_EB_SIGNATAIRE_NATDEP");

    public final NumberPath<Long> idAdmEbSignataireNatdep = createNumber("idAdmEbSignataireNatdep", Long.class);

    public final NumberPath<Long> idAdmNatureDep = createNumber("idAdmNatureDep", Long.class);

    public final NumberPath<java.math.BigDecimal> maxMontantTtc = createNumber("maxMontantTtc", java.math.BigDecimal.class);

    public final NumberPath<Long> orsiId = createNumber("orsiId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmEbSignataireNatdep> admEbSignataireNatdepPk = createPrimaryKey(idAdmEbSignataireNatdep);

    public final com.mysema.query.sql.ForeignKey<QAdmNatureDep> ebSignataireNdNatdepFk = createForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QAdmEbSignataire> ebSignatNdEbSignataireFk = createForeignKey(orsiId, "ORSI_ID");

    public QAdmEbSignataireNatdep(String variable) {
        super(QAdmEbSignataireNatdep.class, forVariable(variable), "GFC", "ADM_EB_SIGNATAIRE_NATDEP");
        addMetadata();
    }

    public QAdmEbSignataireNatdep(String variable, String schema, String table) {
        super(QAdmEbSignataireNatdep.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmEbSignataireNatdep(Path<? extends QAdmEbSignataireNatdep> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_EB_SIGNATAIRE_NATDEP");
        addMetadata();
    }

    public QAdmEbSignataireNatdep(PathMetadata<?> metadata) {
        super(QAdmEbSignataireNatdep.class, metadata, "GFC", "ADM_EB_SIGNATAIRE_NATDEP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idAdmEbSignataireNatdep, ColumnMetadata.named("ID_ADM_EB_SIGNATAIRE_NATDEP").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmNatureDep, ColumnMetadata.named("ID_ADM_NATURE_DEP").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(maxMontantTtc, ColumnMetadata.named("MAX_MONTANT_TTC").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(orsiId, ColumnMetadata.named("ORSI_ID").withIndex(3).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

