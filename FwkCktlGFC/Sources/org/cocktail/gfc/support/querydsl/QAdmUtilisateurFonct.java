package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmUtilisateurFonct is a Querydsl query type for QAdmUtilisateurFonct
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmUtilisateurFonct extends com.mysema.query.sql.RelationalPathBase<QAdmUtilisateurFonct> {

    private static final long serialVersionUID = 1593966684;

    public static final QAdmUtilisateurFonct admUtilisateurFonct = new QAdmUtilisateurFonct("ADM_UTILISATEUR_FONCT");

    public final NumberPath<Long> fonOrdre = createNumber("fonOrdre", Long.class);

    public final NumberPath<Long> ufOrdre = createNumber("ufOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmUtilisateurFonct> sysC0075426 = createPrimaryKey(ufOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmFonction> admUfFonOrdreFk = createForeignKey(fonOrdre, "FON_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> admUfUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurFonctExer> _admUfeUfOrdreFk = createInvForeignKey(ufOrdre, "UF_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurFonctGest> _admUfgUfOrdreFk = createInvForeignKey(ufOrdre, "UF_ORDRE");

    public QAdmUtilisateurFonct(String variable) {
        super(QAdmUtilisateurFonct.class, forVariable(variable), "GFC", "ADM_UTILISATEUR_FONCT");
        addMetadata();
    }

    public QAdmUtilisateurFonct(String variable, String schema, String table) {
        super(QAdmUtilisateurFonct.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmUtilisateurFonct(Path<? extends QAdmUtilisateurFonct> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_UTILISATEUR_FONCT");
        addMetadata();
    }

    public QAdmUtilisateurFonct(PathMetadata<?> metadata) {
        super(QAdmUtilisateurFonct.class, metadata, "GFC", "ADM_UTILISATEUR_FONCT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(fonOrdre, ColumnMetadata.named("FON_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ufOrdre, ColumnMetadata.named("UF_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

