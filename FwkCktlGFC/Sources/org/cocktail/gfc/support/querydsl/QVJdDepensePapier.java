package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVJdDepensePapier is a Querydsl query type for QVJdDepensePapier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVJdDepensePapier extends com.mysema.query.sql.RelationalPathBase<QVJdDepensePapier> {

    private static final long serialVersionUID = 1004613714;

    public static final QVJdDepensePapier vJdDepensePapier = new QVJdDepensePapier("V_JD_DEPENSE_PAPIER");

    public final DateTimePath<java.sql.Timestamp> dateDebutDgp = createDateTime("dateDebutDgp", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateFacture = createDateTime("dppDateFacture", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateReception = createDateTime("dppDateReception", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateSaisie = createDateTime("dppDateSaisie", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateServiceFait = createDateTime("dppDateServiceFait", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> dppHtInitial = createNumber("dppHtInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppHtSaisie = createNumber("dppHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> dppIdReversement = createNumber("dppIdReversement", Long.class);

    public final NumberPath<Long> dppImDgp = createNumber("dppImDgp", Long.class);

    public final NumberPath<Long> dppNbPiece = createNumber("dppNbPiece", Long.class);

    public final StringPath dppNumeroFacture = createString("dppNumeroFacture");

    public final NumberPath<java.math.BigDecimal> dppTtcInitial = createNumber("dppTtcInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTtcSaisie = createNumber("dppTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTvaInitial = createNumber("dppTvaInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTvaSaisie = createNumber("dppTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> modOrdre = createNumber("modOrdre", Long.class);

    public final NumberPath<Long> ribOrdre = createNumber("ribOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QVJdDepensePapier(String variable) {
        super(QVJdDepensePapier.class, forVariable(variable), "GFC", "V_JD_DEPENSE_PAPIER");
        addMetadata();
    }

    public QVJdDepensePapier(String variable, String schema, String table) {
        super(QVJdDepensePapier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVJdDepensePapier(Path<? extends QVJdDepensePapier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_JD_DEPENSE_PAPIER");
        addMetadata();
    }

    public QVJdDepensePapier(PathMetadata<?> metadata) {
        super(QVJdDepensePapier.class, metadata, "GFC", "V_JD_DEPENSE_PAPIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dateDebutDgp, ColumnMetadata.named("DATE_DEBUT_DGP").withIndex(21).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppDateFacture, ColumnMetadata.named("DPP_DATE_FACTURE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateReception, ColumnMetadata.named("DPP_DATE_RECEPTION").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateSaisie, ColumnMetadata.named("DPP_DATE_SAISIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateServiceFait, ColumnMetadata.named("DPP_DATE_SERVICE_FAIT").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppHtInitial, ColumnMetadata.named("DPP_HT_INITIAL").withIndex(17).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppHtSaisie, ColumnMetadata.named("DPP_HT_SAISIE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppIdReversement, ColumnMetadata.named("DPP_ID_REVERSEMENT").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dppImDgp, ColumnMetadata.named("DPP_IM_DGP").withIndex(20).ofType(Types.DECIMAL).withSize(10));
        addMetadata(dppNbPiece, ColumnMetadata.named("DPP_NB_PIECE").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(dppTtcInitial, ColumnMetadata.named("DPP_TTC_INITIAL").withIndex(19).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTtcSaisie, ColumnMetadata.named("DPP_TTC_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTvaInitial, ColumnMetadata.named("DPP_TVA_INITIAL").withIndex(18).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTvaSaisie, ColumnMetadata.named("DPP_TVA_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(modOrdre, ColumnMetadata.named("MOD_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ribOrdre, ColumnMetadata.named("RIB_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

