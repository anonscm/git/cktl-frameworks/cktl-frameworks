package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmOrigineRecetteExer is a Querydsl query type for QAdmOrigineRecetteExer
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmOrigineRecetteExer extends com.mysema.query.sql.RelationalPathBase<QAdmOrigineRecetteExer> {

    private static final long serialVersionUID = 778986778;

    public static final QAdmOrigineRecetteExer admOrigineRecetteExer = new QAdmOrigineRecetteExer("ADM_ORIGINE_RECETTE_EXER");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmOrigineRecette = createNumber("idAdmOrigineRecette", Long.class);

    public final NumberPath<Long> idAdmOrigineRecetteExer = createNumber("idAdmOrigineRecetteExer", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmOrigineRecetteExer> admOrigRecExePk = createPrimaryKey(idAdmOrigineRecetteExer);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> admOrigRecExeExerciceFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecette> admOrigRecExeOriFk = createForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QAdmOriNatRecPcptExer> _admOriNatRecPcptExOriFk = createInvForeignKey(idAdmOrigineRecetteExer, "ID_ADM_ORIGINE_RECETTE_EXER");

    public QAdmOrigineRecetteExer(String variable) {
        super(QAdmOrigineRecetteExer.class, forVariable(variable), "GFC", "ADM_ORIGINE_RECETTE_EXER");
        addMetadata();
    }

    public QAdmOrigineRecetteExer(String variable, String schema, String table) {
        super(QAdmOrigineRecetteExer.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmOrigineRecetteExer(Path<? extends QAdmOrigineRecetteExer> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_ORIGINE_RECETTE_EXER");
        addMetadata();
    }

    public QAdmOrigineRecetteExer(PathMetadata<?> metadata) {
        super(QAdmOrigineRecetteExer.class, metadata, "GFC", "ADM_ORIGINE_RECETTE_EXER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(idAdmOrigineRecette, ColumnMetadata.named("ID_ADM_ORIGINE_RECETTE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmOrigineRecetteExer, ColumnMetadata.named("ID_ADM_ORIGINE_RECETTE_EXER").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

