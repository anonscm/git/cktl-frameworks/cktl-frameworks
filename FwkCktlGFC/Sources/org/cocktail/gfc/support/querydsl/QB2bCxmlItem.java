package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QB2bCxmlItem is a Querydsl query type for QB2bCxmlItem
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QB2bCxmlItem extends com.mysema.query.sql.RelationalPathBase<QB2bCxmlItem> {

    private static final long serialVersionUID = -680971560;

    public static final QB2bCxmlItem b2bCxmlItem = new QB2bCxmlItem("B2B_CXML_ITEM");

    public final NumberPath<Long> artId = createNumber("artId", Long.class);

    public final NumberPath<Long> bciId = createNumber("bciId", Long.class);

    public final StringPath classification = createString("classification");

    public final StringPath classificationDom = createString("classificationDom");

    public final StringPath comments = createString("comments");

    public final DateTimePath<java.sql.Timestamp> dateCreation = createDateTime("dateCreation", java.sql.Timestamp.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> fbcpId = createNumber("fbcpId", Long.class);

    public final StringPath manufacturerName = createString("manufacturerName");

    public final StringPath manufacturerPartId = createString("manufacturerPartId");

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final StringPath supplierPartAuxiliaryId = createString("supplierPartAuxiliaryId");

    public final StringPath supplierPartId = createString("supplierPartId");

    public final StringPath unitOfMeasure = createString("unitOfMeasure");

    public final NumberPath<Double> unitPrice = createNumber("unitPrice", Double.class);

    public final StringPath url = createString("url");

    public final com.mysema.query.sql.PrimaryKey<QB2bCxmlItem> b2bCxmlItemPk = createPrimaryKey(bciId);

    public final com.mysema.query.sql.ForeignKey<QDepArticle> bciArtIdFk = createForeignKey(artId, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QFournisB2bCxmlParam> bciFbcpIdFk = createForeignKey(fbcpId, "FBCP_ID");

    public QB2bCxmlItem(String variable) {
        super(QB2bCxmlItem.class, forVariable(variable), "GFC", "B2B_CXML_ITEM");
        addMetadata();
    }

    public QB2bCxmlItem(String variable, String schema, String table) {
        super(QB2bCxmlItem.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QB2bCxmlItem(Path<? extends QB2bCxmlItem> path) {
        super(path.getType(), path.getMetadata(), "GFC", "B2B_CXML_ITEM");
        addMetadata();
    }

    public QB2bCxmlItem(PathMetadata<?> metadata) {
        super(QB2bCxmlItem.class, metadata, "GFC", "B2B_CXML_ITEM");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(artId, ColumnMetadata.named("ART_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(bciId, ColumnMetadata.named("BCI_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(classification, ColumnMetadata.named("CLASSIFICATION").withIndex(10).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(classificationDom, ColumnMetadata.named("CLASSIFICATION_DOM").withIndex(9).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(comments, ColumnMetadata.named("COMMENTS").withIndex(14).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(dateCreation, ColumnMetadata.named("DATE_CREATION").withIndex(16).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(description, ColumnMetadata.named("DESCRIPTION").withIndex(5).ofType(Types.VARCHAR).withSize(2000).notNull());
        addMetadata(fbcpId, ColumnMetadata.named("FBCP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manufacturerName, ColumnMetadata.named("MANUFACTURER_NAME").withIndex(11).ofType(Types.VARCHAR).withSize(100));
        addMetadata(manufacturerPartId, ColumnMetadata.named("MANUFACTURER_PART_ID").withIndex(12).ofType(Types.VARCHAR).withSize(500));
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(supplierPartAuxiliaryId, ColumnMetadata.named("SUPPLIER_PART_AUXILIARY_ID").withIndex(7).ofType(Types.VARCHAR).withSize(500));
        addMetadata(supplierPartId, ColumnMetadata.named("SUPPLIER_PART_ID").withIndex(6).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(unitOfMeasure, ColumnMetadata.named("UNIT_OF_MEASURE").withIndex(8).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(unitPrice, ColumnMetadata.named("UNIT_PRICE").withIndex(4).ofType(Types.DECIMAL).withSize(15).withDigits(2).notNull());
        addMetadata(url, ColumnMetadata.named("URL").withIndex(13).ofType(Types.VARCHAR).withSize(2000));
    }

}

