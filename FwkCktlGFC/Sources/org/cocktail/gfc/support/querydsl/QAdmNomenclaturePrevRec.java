package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmNomenclaturePrevRec is a Querydsl query type for QAdmNomenclaturePrevRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmNomenclaturePrevRec extends com.mysema.query.sql.RelationalPathBase<QAdmNomenclaturePrevRec> {

    private static final long serialVersionUID = 145363741;

    public static final QAdmNomenclaturePrevRec admNomenclaturePrevRec = new QAdmNomenclaturePrevRec("ADM_NOMENCLATURE_PREV_REC");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath nprCode = createString("nprCode");

    public final StringPath nprDetails = createString("nprDetails");

    public final NumberPath<Long> nprId = createNumber("nprId", Long.class);

    public final StringPath nprLibelle = createString("nprLibelle");

    public final com.mysema.query.sql.PrimaryKey<QAdmNomenclaturePrevRec> sysC0075589 = createPrimaryKey(nprId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> admNomenPrevRecExeOrdrFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QAdmNomenclaturePrevRec(String variable) {
        super(QAdmNomenclaturePrevRec.class, forVariable(variable), "GFC", "ADM_NOMENCLATURE_PREV_REC");
        addMetadata();
    }

    public QAdmNomenclaturePrevRec(String variable, String schema, String table) {
        super(QAdmNomenclaturePrevRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmNomenclaturePrevRec(Path<? extends QAdmNomenclaturePrevRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_NOMENCLATURE_PREV_REC");
        addMetadata();
    }

    public QAdmNomenclaturePrevRec(PathMetadata<?> metadata) {
        super(QAdmNomenclaturePrevRec.class, metadata, "GFC", "ADM_NOMENCLATURE_PREV_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(nprCode, ColumnMetadata.named("NPR_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(8).notNull());
        addMetadata(nprDetails, ColumnMetadata.named("NPR_DETAILS").withIndex(5).ofType(Types.VARCHAR).withSize(200));
        addMetadata(nprId, ColumnMetadata.named("NPR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(nprLibelle, ColumnMetadata.named("NPR_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(200).notNull());
    }

}

