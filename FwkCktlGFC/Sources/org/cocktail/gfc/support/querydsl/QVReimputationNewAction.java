package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVReimputationNewAction is a Querydsl query type for QVReimputationNewAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVReimputationNewAction extends com.mysema.query.sql.RelationalPathBase<QVReimputationNewAction> {

    private static final long serialVersionUID = 2031113988;

    public static final QVReimputationNewAction vReimputationNewAction = new QVReimputationNewAction("V_REIMPUTATION_NEW_ACTION");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<java.math.BigDecimal> reacHtSaisie = createNumber("reacHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reacId = createNumber("reacId", Long.class);

    public final NumberPath<java.math.BigDecimal> reacMontantBudgetaire = createNumber("reacMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reacTtcSaisie = createNumber("reacTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reacTvaSaisie = createNumber("reacTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public QVReimputationNewAction(String variable) {
        super(QVReimputationNewAction.class, forVariable(variable), "GFC", "V_REIMPUTATION_NEW_ACTION");
        addMetadata();
    }

    public QVReimputationNewAction(String variable, String schema, String table) {
        super(QVReimputationNewAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVReimputationNewAction(Path<? extends QVReimputationNewAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_REIMPUTATION_NEW_ACTION");
        addMetadata();
    }

    public QVReimputationNewAction(PathMetadata<?> metadata) {
        super(QVReimputationNewAction.class, metadata, "GFC", "V_REIMPUTATION_NEW_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(4));
        addMetadata(reacHtSaisie, ColumnMetadata.named("REAC_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reacId, ColumnMetadata.named("REAC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(reacMontantBudgetaire, ColumnMetadata.named("REAC_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reacTtcSaisie, ColumnMetadata.named("REAC_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reacTvaSaisie, ColumnMetadata.named("REAC_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

