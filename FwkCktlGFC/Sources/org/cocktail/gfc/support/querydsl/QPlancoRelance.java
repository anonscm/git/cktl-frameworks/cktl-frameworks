package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPlancoRelance is a Querydsl query type for QPlancoRelance
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPlancoRelance extends com.mysema.query.sql.RelationalPathBase<QPlancoRelance> {

    private static final long serialVersionUID = 1423665906;

    public static final QPlancoRelance plancoRelance = new QPlancoRelance("PLANCO_RELANCE");

    public final StringPath pcoNum = createString("pcoNum");

    public final DateTimePath<java.sql.Timestamp> plrDateCreation = createDateTime("plrDateCreation", java.sql.Timestamp.class);

    public final StringPath plrEtat = createString("plrEtat");

    public final NumberPath<Long> plrOrdre = createNumber("plrOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPlancoRelance> plancoRelancePk = createPrimaryKey(plrOrdre);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> plancoRelanceFk1 = createForeignKey(pcoNum, "PCO_NUM");

    public QPlancoRelance(String variable) {
        super(QPlancoRelance.class, forVariable(variable), "GFC", "PLANCO_RELANCE");
        addMetadata();
    }

    public QPlancoRelance(String variable, String schema, String table) {
        super(QPlancoRelance.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPlancoRelance(Path<? extends QPlancoRelance> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PLANCO_RELANCE");
        addMetadata();
    }

    public QPlancoRelance(PathMetadata<?> metadata) {
        super(QPlancoRelance.class, metadata, "GFC", "PLANCO_RELANCE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(2).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(plrDateCreation, ColumnMetadata.named("PLR_DATE_CREATION").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(plrEtat, ColumnMetadata.named("PLR_ETAT").withIndex(5).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(plrOrdre, ColumnMetadata.named("PLR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(12).notNull());
    }

}

