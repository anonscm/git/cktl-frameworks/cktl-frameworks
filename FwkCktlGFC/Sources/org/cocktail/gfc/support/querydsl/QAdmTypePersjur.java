package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypePersjur is a Querydsl query type for QAdmTypePersjur
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypePersjur extends com.mysema.query.sql.RelationalPathBase<QAdmTypePersjur> {

    private static final long serialVersionUID = -1174655634;

    public static final QAdmTypePersjur admTypePersjur = new QAdmTypePersjur("ADM_TYPE_PERSJUR");

    public final NumberPath<Long> tpjId = createNumber("tpjId", Long.class);

    public final StringPath tpjLibelle = createString("tpjLibelle");

    public final NumberPath<Long> tpjNbNivMax = createNumber("tpjNbNivMax", Long.class);

    public final NumberPath<Long> tpjPere = createNumber("tpjPere", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmTypePersjur> typePersjurPk = createPrimaryKey(tpjId);

    public final com.mysema.query.sql.ForeignKey<QAdmPersjur> _admPersjurTpjIdFk = createInvForeignKey(tpjId, "TPJ_ID");

    public QAdmTypePersjur(String variable) {
        super(QAdmTypePersjur.class, forVariable(variable), "GFC", "ADM_TYPE_PERSJUR");
        addMetadata();
    }

    public QAdmTypePersjur(String variable, String schema, String table) {
        super(QAdmTypePersjur.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypePersjur(Path<? extends QAdmTypePersjur> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_PERSJUR");
        addMetadata();
    }

    public QAdmTypePersjur(PathMetadata<?> metadata) {
        super(QAdmTypePersjur.class, metadata, "GFC", "ADM_TYPE_PERSJUR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tpjId, ColumnMetadata.named("TPJ_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tpjLibelle, ColumnMetadata.named("TPJ_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(tpjNbNivMax, ColumnMetadata.named("TPJ_NB_NIV_MAX").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tpjPere, ColumnMetadata.named("TPJ_PERE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

