package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeBudget is a Querydsl query type for QCommandeBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeBudget extends com.mysema.query.sql.RelationalPathBase<QCommandeBudget> {

    private static final long serialVersionUID = -340886112;

    public static final QCommandeBudget commandeBudget = new QCommandeBudget("COMMANDE_BUDGET");

    public final NumberPath<java.math.BigDecimal> cbudHtSaisie = createNumber("cbudHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> cbudId = createNumber("cbudId", Long.class);

    public final NumberPath<java.math.BigDecimal> cbudMontantBudgetaire = createNumber("cbudMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> cbudTtcSaisie = createNumber("cbudTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> cbudTvaSaisie = createNumber("cbudTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeBudget> commandeBudgetPk = createPrimaryKey(cbudId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeBudgetExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeBudgetCommIdFk = createForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> commandeBudgetTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> commandeBudgetOrgIdFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> commandeBudgetTcdOrdreFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlAnalytique> _commandeCtrlAnCbudIdFk = createInvForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlPlanco> _commandeCtrlPlCbudIdFk = createInvForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlHorsMarche> _commandeCtrlHomCbudIdFk = createInvForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlMarche> _commandeCtrlMarCbudIdFk = createInvForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlConvention> _commandeCtrlCoCbudIdFk = createInvForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlAction> _commandeCtrlActCbudIdFk = createInvForeignKey(cbudId, "CBUD_ID");

    public QCommandeBudget(String variable) {
        super(QCommandeBudget.class, forVariable(variable), "GFC", "COMMANDE_BUDGET");
        addMetadata();
    }

    public QCommandeBudget(String variable, String schema, String table) {
        super(QCommandeBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeBudget(Path<? extends QCommandeBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_BUDGET");
        addMetadata();
    }

    public QCommandeBudget(PathMetadata<?> metadata) {
        super(QCommandeBudget.class, metadata, "GFC", "COMMANDE_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cbudHtSaisie, ColumnMetadata.named("CBUD_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cbudId, ColumnMetadata.named("CBUD_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cbudMontantBudgetaire, ColumnMetadata.named("CBUD_MONTANT_BUDGETAIRE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cbudTtcSaisie, ColumnMetadata.named("CBUD_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cbudTvaSaisie, ColumnMetadata.named("CBUD_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

