package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepenseImTauxRef is a Querydsl query type for QVDepenseImTauxRef
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepenseImTauxRef extends com.mysema.query.sql.RelationalPathBase<QVDepenseImTauxRef> {

    private static final long serialVersionUID = 1628591722;

    public static final QVDepenseImTauxRef vDepenseImTauxRef = new QVDepenseImTauxRef("V_DEPENSE_IM_TAUX_REF");

    public final DateTimePath<java.sql.Timestamp> dateDebutDgp = createDateTime("dateDebutDgp", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateDeterminationTaux = createDateTime("dateDeterminationTaux", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> debutSuspension = createDateTime("debutSuspension", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateReception = createDateTime("dppDateReception", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateServiceFait = createDateTime("dppDateServiceFait", java.sql.Timestamp.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> dureeSuspension = createNumber("dureeSuspension", Long.class);

    public final DateTimePath<java.sql.Timestamp> finDgpReelle = createDateTime("finDgpReelle", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> finDgpTheorique = createDateTime("finDgpTheorique", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> finSuspension = createDateTime("finSuspension", java.sql.Timestamp.class);

    public final NumberPath<Long> imdgDgp = createNumber("imdgDgp", Long.class);

    public final NumberPath<Long> imtaId = createNumber("imtaId", Long.class);

    public final NumberPath<java.math.BigDecimal> imtaTaux = createNumber("imtaTaux", java.math.BigDecimal.class);

    public final StringPath imttCode = createString("imttCode");

    public final NumberPath<Long> imttId = createNumber("imttId", Long.class);

    public QVDepenseImTauxRef(String variable) {
        super(QVDepenseImTauxRef.class, forVariable(variable), "GFC", "V_DEPENSE_IM_TAUX_REF");
        addMetadata();
    }

    public QVDepenseImTauxRef(String variable, String schema, String table) {
        super(QVDepenseImTauxRef.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepenseImTauxRef(Path<? extends QVDepenseImTauxRef> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSE_IM_TAUX_REF");
        addMetadata();
    }

    public QVDepenseImTauxRef(PathMetadata<?> metadata) {
        super(QVDepenseImTauxRef.class, metadata, "GFC", "V_DEPENSE_IM_TAUX_REF");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dateDebutDgp, ColumnMetadata.named("DATE_DEBUT_DGP").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateDeterminationTaux, ColumnMetadata.named("DATE_DETERMINATION_TAUX").withIndex(15).ofType(Types.TIMESTAMP).withSize(8));
        addMetadata(debutSuspension, ColumnMetadata.named("DEBUT_SUSPENSION").withIndex(10).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppDateReception, ColumnMetadata.named("DPP_DATE_RECEPTION").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateServiceFait, ColumnMetadata.named("DPP_DATE_SERVICE_FAIT").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dureeSuspension, ColumnMetadata.named("DUREE_SUSPENSION").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(finDgpReelle, ColumnMetadata.named("FIN_DGP_REELLE").withIndex(14).ofType(Types.TIMESTAMP).withSize(8));
        addMetadata(finDgpTheorique, ColumnMetadata.named("FIN_DGP_THEORIQUE").withIndex(13).ofType(Types.TIMESTAMP).withSize(8));
        addMetadata(finSuspension, ColumnMetadata.named("FIN_SUSPENSION").withIndex(11).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(imdgDgp, ColumnMetadata.named("IMDG_DGP").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(imtaId, ColumnMetadata.named("IMTA_ID").withIndex(4).ofType(Types.DECIMAL).withSize(10).notNull());
        addMetadata(imtaTaux, ColumnMetadata.named("IMTA_TAUX").withIndex(7).ofType(Types.DECIMAL).withSize(19).withDigits(2));
        addMetadata(imttCode, ColumnMetadata.named("IMTT_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(imttId, ColumnMetadata.named("IMTT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(10).notNull());
    }

}

