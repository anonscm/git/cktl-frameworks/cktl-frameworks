package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeProjet is a Querydsl query type for QTypeProjet
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeProjet extends com.mysema.query.sql.RelationalPathBase<QTypeProjet> {

    private static final long serialVersionUID = -36644761;

    public static final QTypeProjet typeProjet = new QTypeProjet("TYPE_PROJET");

    public final StringPath tpjtCode = createString("tpjtCode");

    public final NumberPath<Long> tpjtId = createNumber("tpjtId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeProjet> typeProjetPk = createPrimaryKey(tpjtId);

    public QTypeProjet(String variable) {
        super(QTypeProjet.class, forVariable(variable), "GFC", "TYPE_PROJET");
        addMetadata();
    }

    public QTypeProjet(String variable, String schema, String table) {
        super(QTypeProjet.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeProjet(Path<? extends QTypeProjet> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_PROJET");
        addMetadata();
    }

    public QTypeProjet(PathMetadata<?> metadata) {
        super(QTypeProjet.class, metadata, "GFC", "TYPE_PROJET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tpjtCode, ColumnMetadata.named("TPJT_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(tpjtId, ColumnMetadata.named("TPJT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

