package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputationBudget is a Querydsl query type for QReimputationBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputationBudget extends com.mysema.query.sql.RelationalPathBase<QReimputationBudget> {

    private static final long serialVersionUID = -2142027453;

    public static final QReimputationBudget reimputationBudget = new QReimputationBudget("REIMPUTATION_BUDGET");

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> rebuId = createNumber("rebuId", Long.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputationBudget> reimputationBudgetPk = createPrimaryKey(rebuId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> reimpBudgetTcdOrdreFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> reimpBudgetTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> reimpBudgetOrgIdFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QReimputation> reimpBudgetReimIdFk = createForeignKey(reimId, "REIM_ID");

    public QReimputationBudget(String variable) {
        super(QReimputationBudget.class, forVariable(variable), "GFC", "REIMPUTATION_BUDGET");
        addMetadata();
    }

    public QReimputationBudget(String variable, String schema, String table) {
        super(QReimputationBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputationBudget(Path<? extends QReimputationBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION_BUDGET");
        addMetadata();
    }

    public QReimputationBudget(PathMetadata<?> metadata) {
        super(QReimputationBudget.class, metadata, "GFC", "REIMPUTATION_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rebuId, ColumnMetadata.named("REBU_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

