package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import java.util.*;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QGestionExercice is a Querydsl query type for QGestionExercice
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QGestionExercice extends com.mysema.query.sql.RelationalPathBase<QGestionExercice> {

    private static final long serialVersionUID = 1674504144;

    public static final QGestionExercice gestionExercice = new QGestionExercice("GESTION_EXERCICE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final StringPath gesLibelle = createString("gesLibelle");

    public final StringPath pcoNum185 = createString("pcoNum185");

    public final StringPath pcoNum185CtpSacd = createString("pcoNum185CtpSacd");

    public final com.mysema.query.sql.PrimaryKey<QGestionExercice> gestionExercicePk = createPrimaryKey(exeOrdre, gesCode);

    public final com.mysema.query.sql.ForeignKey<QGestion> gestionGesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> gestionExerciceExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QGestionAgregatRepart> _garGesCodeFk = createInvForeignKey(Arrays.asList(exeOrdre, gesCode), Arrays.asList("EXE_ORDRE", "GES_CODE"));

    public QGestionExercice(String variable) {
        super(QGestionExercice.class, forVariable(variable), "GFC", "GESTION_EXERCICE");
        addMetadata();
    }

    public QGestionExercice(String variable, String schema, String table) {
        super(QGestionExercice.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QGestionExercice(Path<? extends QGestionExercice> path) {
        super(path.getType(), path.getMetadata(), "GFC", "GESTION_EXERCICE");
        addMetadata();
    }

    public QGestionExercice(PathMetadata<?> metadata) {
        super(QGestionExercice.class, metadata, "GFC", "GESTION_EXERCICE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(1).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(gesLibelle, ColumnMetadata.named("GES_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(pcoNum185, ColumnMetadata.named("PCO_NUM_185").withIndex(4).ofType(Types.VARCHAR).withSize(20));
        addMetadata(pcoNum185CtpSacd, ColumnMetadata.named("PCO_NUM_185_CTP_SACD").withIndex(5).ofType(Types.VARCHAR).withSize(20));
    }

}

