package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypapVersion is a Querydsl query type for QAdmTypapVersion
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypapVersion extends com.mysema.query.sql.RelationalPathBase<QAdmTypapVersion> {

    private static final long serialVersionUID = 15120935;

    public static final QAdmTypapVersion admTypapVersion = new QAdmTypapVersion("ADM_TYPAP_VERSION");

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final StringPath tyavComment = createString("tyavComment");

    public final DateTimePath<java.sql.Timestamp> tyavDate = createDateTime("tyavDate", java.sql.Timestamp.class);

    public final NumberPath<Long> tyavId = createNumber("tyavId", Long.class);

    public final StringPath tyavTypeVersion = createString("tyavTypeVersion");

    public final StringPath tyavVersion = createString("tyavVersion");

    public final com.mysema.query.sql.PrimaryKey<QAdmTypapVersion> typapVersionPk = createPrimaryKey(tyavId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplication> admTyavTyapFk = createForeignKey(tyapId, "TYAP_ID");

    public QAdmTypapVersion(String variable) {
        super(QAdmTypapVersion.class, forVariable(variable), "GFC", "ADM_TYPAP_VERSION");
        addMetadata();
    }

    public QAdmTypapVersion(String variable, String schema, String table) {
        super(QAdmTypapVersion.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypapVersion(Path<? extends QAdmTypapVersion> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPAP_VERSION");
        addMetadata();
    }

    public QAdmTypapVersion(PathMetadata<?> metadata) {
        super(QAdmTypapVersion.class, metadata, "GFC", "ADM_TYPAP_VERSION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(tyavComment, ColumnMetadata.named("TYAV_COMMENT").withIndex(6).ofType(Types.VARCHAR).withSize(100));
        addMetadata(tyavDate, ColumnMetadata.named("TYAV_DATE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(tyavId, ColumnMetadata.named("TYAV_ID").withIndex(1).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(tyavTypeVersion, ColumnMetadata.named("TYAV_TYPE_VERSION").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tyavVersion, ColumnMetadata.named("TYAV_VERSION").withIndex(4).ofType(Types.VARCHAR).withSize(10).notNull());
    }

}

