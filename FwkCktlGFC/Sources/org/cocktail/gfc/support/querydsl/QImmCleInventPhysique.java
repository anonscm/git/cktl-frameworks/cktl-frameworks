package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmCleInventPhysique is a Querydsl query type for QImmCleInventPhysique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmCleInventPhysique extends com.mysema.query.sql.RelationalPathBase<QImmCleInventPhysique> {

    private static final long serialVersionUID = 1144899384;

    public static final QImmCleInventPhysique immCleInventPhysique = new QImmCleInventPhysique("IMM_CLE_INVENT_PHYSIQUE");

    public final StringPath clipEtat = createString("clipEtat");

    public final NumberPath<Long> clipId = createNumber("clipId", Long.class);

    public final NumberPath<Long> clipNbEtiquette = createNumber("clipNbEtiquette", Long.class);

    public final StringPath clipNumero = createString("clipNumero");

    public final StringPath exeOrdre = createString("exeOrdre");

    public final com.mysema.query.sql.PrimaryKey<QImmCleInventPhysique> immCleInventPhysiquePk = createPrimaryKey(clipId);

    public final com.mysema.query.sql.ForeignKey<QImmInvent> _cleinventairephysiqueFk = createInvForeignKey(clipId, "CLIP_ID");

    public QImmCleInventPhysique(String variable) {
        super(QImmCleInventPhysique.class, forVariable(variable), "GFC", "IMM_CLE_INVENT_PHYSIQUE");
        addMetadata();
    }

    public QImmCleInventPhysique(String variable, String schema, String table) {
        super(QImmCleInventPhysique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmCleInventPhysique(Path<? extends QImmCleInventPhysique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_CLE_INVENT_PHYSIQUE");
        addMetadata();
    }

    public QImmCleInventPhysique(PathMetadata<?> metadata) {
        super(QImmCleInventPhysique.class, metadata, "GFC", "IMM_CLE_INVENT_PHYSIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clipEtat, ColumnMetadata.named("CLIP_ETAT").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(clipId, ColumnMetadata.named("CLIP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(clipNbEtiquette, ColumnMetadata.named("CLIP_NB_ETIQUETTE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clipNumero, ColumnMetadata.named("CLIP_NUMERO").withIndex(4).ofType(Types.VARCHAR).withSize(30));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.VARCHAR).withSize(4));
    }

}

