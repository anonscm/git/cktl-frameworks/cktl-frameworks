package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeImpression is a Querydsl query type for QCommandeImpression
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeImpression extends com.mysema.query.sql.RelationalPathBase<QCommandeImpression> {

    private static final long serialVersionUID = 2138028772;

    public static final QCommandeImpression commandeImpression = new QCommandeImpression("COMMANDE_IMPRESSION");

    public final DateTimePath<java.sql.Timestamp> cimpDate = createDateTime("cimpDate", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> cimpDateLivraison = createDateTime("cimpDateLivraison", java.sql.Timestamp.class);

    public final NumberPath<Long> cimpFournisseurAdrordre = createNumber("cimpFournisseurAdrordre", Long.class);

    public final NumberPath<Long> cimpId = createNumber("cimpId", Long.class);

    public final StringPath cimpInfosImpression = createString("cimpInfosImpression");

    public final NumberPath<Long> cimpLivraisonAdrordre = createNumber("cimpLivraisonAdrordre", Long.class);

    public final StringPath cimpLivraisonCstructure = createString("cimpLivraisonCstructure");

    public final StringPath cimpLivraisonFax = createString("cimpLivraisonFax");

    public final StringPath cimpLivraisonTelephone = createString("cimpLivraisonTelephone");

    public final NumberPath<Long> cimpServiceAdrordre = createNumber("cimpServiceAdrordre", Long.class);

    public final StringPath cimpServiceCstructure = createString("cimpServiceCstructure");

    public final StringPath cimpServiceFax = createString("cimpServiceFax");

    public final StringPath cimpServiceTelephone = createString("cimpServiceTelephone");

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeImpression> commandeImpressionPk = createPrimaryKey(cimpId);

    public final com.mysema.query.sql.ForeignKey<QAdresse> commandeImpresLAdrFk = createForeignKey(cimpLivraisonAdrordre, "ADR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QStructureUlr> commandeImpresSStructFk = createForeignKey(cimpServiceCstructure, "C_STRUCTURE");

    public final com.mysema.query.sql.ForeignKey<QAdresse> commandeImpresFAdrFk = createForeignKey(cimpFournisseurAdrordre, "ADR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QStructureUlr> commandeImpresLStructFk = createForeignKey(cimpLivraisonCstructure, "C_STRUCTURE");

    public final com.mysema.query.sql.ForeignKey<QAdresse> commandeImpresSAdrFk = createForeignKey(cimpServiceAdrordre, "ADR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeImpresCommIdFk = createForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> commandeImpresUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeDocument> _commandeDocumentCimpIdFk = createInvForeignKey(cimpId, "CIMP_ID");

    public QCommandeImpression(String variable) {
        super(QCommandeImpression.class, forVariable(variable), "GFC", "COMMANDE_IMPRESSION");
        addMetadata();
    }

    public QCommandeImpression(String variable, String schema, String table) {
        super(QCommandeImpression.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeImpression(Path<? extends QCommandeImpression> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_IMPRESSION");
        addMetadata();
    }

    public QCommandeImpression(PathMetadata<?> metadata) {
        super(QCommandeImpression.class, metadata, "GFC", "COMMANDE_IMPRESSION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cimpDate, ColumnMetadata.named("CIMP_DATE").withIndex(3).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(cimpDateLivraison, ColumnMetadata.named("CIMP_DATE_LIVRAISON").withIndex(14).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(cimpFournisseurAdrordre, ColumnMetadata.named("CIMP_FOURNISSEUR_ADRORDRE").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cimpId, ColumnMetadata.named("CIMP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cimpInfosImpression, ColumnMetadata.named("CIMP_INFOS_IMPRESSION").withIndex(4).ofType(Types.VARCHAR).withSize(500));
        addMetadata(cimpLivraisonAdrordre, ColumnMetadata.named("CIMP_LIVRAISON_ADRORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cimpLivraisonCstructure, ColumnMetadata.named("CIMP_LIVRAISON_CSTRUCTURE").withIndex(9).ofType(Types.VARCHAR).withSize(10));
        addMetadata(cimpLivraisonFax, ColumnMetadata.named("CIMP_LIVRAISON_FAX").withIndex(12).ofType(Types.VARCHAR).withSize(14));
        addMetadata(cimpLivraisonTelephone, ColumnMetadata.named("CIMP_LIVRAISON_TELEPHONE").withIndex(11).ofType(Types.VARCHAR).withSize(14));
        addMetadata(cimpServiceAdrordre, ColumnMetadata.named("CIMP_SERVICE_ADRORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cimpServiceCstructure, ColumnMetadata.named("CIMP_SERVICE_CSTRUCTURE").withIndex(5).ofType(Types.VARCHAR).withSize(10));
        addMetadata(cimpServiceFax, ColumnMetadata.named("CIMP_SERVICE_FAX").withIndex(8).ofType(Types.VARCHAR).withSize(14));
        addMetadata(cimpServiceTelephone, ColumnMetadata.named("CIMP_SERVICE_TELEPHONE").withIndex(7).ofType(Types.VARCHAR).withSize(14));
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

