package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSepaSddEcheancierEcd is a Querydsl query type for QSepaSddEcheancierEcd
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSepaSddEcheancierEcd extends com.mysema.query.sql.RelationalPathBase<QSepaSddEcheancierEcd> {

    private static final long serialVersionUID = 2144549212;

    public static final QSepaSddEcheancierEcd sepaSddEcheancierEcd = new QSepaSddEcheancierEcd("SEPA_SDD_ECHEANCIER_ECD");

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Long> idSepaSddEcheancier = createNumber("idSepaSddEcheancier", Long.class);

    public final NumberPath<Long> idSepaSddEcheancierEcd = createNumber("idSepaSddEcheancierEcd", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QSepaSddEcheancierEcd> sepaSddEcheancierEcdPk = createPrimaryKey(idSepaSddEcheancierEcd);

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> sepaSddEcheancierEcdEcdFk = createForeignKey(ecdOrdre, "ECD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QSepaSddEcheancier> sepaSddEcheancierEcdEchFk = createForeignKey(idSepaSddEcheancier, "ID_SEPA_SDD_ECHEANCIER");

    public QSepaSddEcheancierEcd(String variable) {
        super(QSepaSddEcheancierEcd.class, forVariable(variable), "GFC", "SEPA_SDD_ECHEANCIER_ECD");
        addMetadata();
    }

    public QSepaSddEcheancierEcd(String variable, String schema, String table) {
        super(QSepaSddEcheancierEcd.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSepaSddEcheancierEcd(Path<? extends QSepaSddEcheancierEcd> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SEPA_SDD_ECHEANCIER_ECD");
        addMetadata();
    }

    public QSepaSddEcheancierEcd(PathMetadata<?> metadata) {
        super(QSepaSddEcheancierEcd.class, metadata, "GFC", "SEPA_SDD_ECHEANCIER_ECD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idSepaSddEcheancier, ColumnMetadata.named("ID_SEPA_SDD_ECHEANCIER").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idSepaSddEcheancierEcd, ColumnMetadata.named("ID_SEPA_SDD_ECHEANCIER_ECD").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

