package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypeSignature is a Querydsl query type for QAdmTypeSignature
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypeSignature extends com.mysema.query.sql.RelationalPathBase<QAdmTypeSignature> {

    private static final long serialVersionUID = 957125909;

    public static final QAdmTypeSignature admTypeSignature = new QAdmTypeSignature("ADM_TYPE_SIGNATURE");

    public final NumberPath<Long> tysiId = createNumber("tysiId", Long.class);

    public final StringPath tysiLibelle = createString("tysiLibelle");

    public final com.mysema.query.sql.PrimaryKey<QAdmTypeSignature> sysC0077263 = createPrimaryKey(tysiId);

    public final com.mysema.query.sql.ForeignKey<QAdmEbSignataire> _admOsTysiIdFk = createInvForeignKey(tysiId, "TYSI_ID");

    public QAdmTypeSignature(String variable) {
        super(QAdmTypeSignature.class, forVariable(variable), "GFC", "ADM_TYPE_SIGNATURE");
        addMetadata();
    }

    public QAdmTypeSignature(String variable, String schema, String table) {
        super(QAdmTypeSignature.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypeSignature(Path<? extends QAdmTypeSignature> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_SIGNATURE");
        addMetadata();
    }

    public QAdmTypeSignature(PathMetadata<?> metadata) {
        super(QAdmTypeSignature.class, metadata, "GFC", "ADM_TYPE_SIGNATURE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tysiId, ColumnMetadata.named("TYSI_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tysiLibelle, ColumnMetadata.named("TYSI_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

