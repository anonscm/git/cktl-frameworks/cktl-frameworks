package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVPlancoCreditDep is a Querydsl query type for QVPlancoCreditDep
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVPlancoCreditDep extends com.mysema.query.sql.RelationalPathBase<QVPlancoCreditDep> {

    private static final long serialVersionUID = 2122251308;

    public static final QVPlancoCreditDep vPlancoCreditDep = new QVPlancoCreditDep("V_PLANCO_CREDIT_DEP");

    public final StringPath pccEtat = createString("pccEtat");

    public final NumberPath<Long> pccOrdre = createNumber("pccOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QVPlancoCreditDep(String variable) {
        super(QVPlancoCreditDep.class, forVariable(variable), "GFC", "V_PLANCO_CREDIT_DEP");
        addMetadata();
    }

    public QVPlancoCreditDep(String variable, String schema, String table) {
        super(QVPlancoCreditDep.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVPlancoCreditDep(Path<? extends QVPlancoCreditDep> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_PLANCO_CREDIT_DEP");
        addMetadata();
    }

    public QVPlancoCreditDep(PathMetadata<?> metadata) {
        super(QVPlancoCreditDep.class, metadata, "GFC", "V_PLANCO_CREDIT_DEP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pccEtat, ColumnMetadata.named("PCC_ETAT").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pccOrdre, ColumnMetadata.named("PCC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

