package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVExtourneCreditsUb is a Querydsl query type for QVExtourneCreditsUb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVExtourneCreditsUb extends com.mysema.query.sql.RelationalPathBase<QVExtourneCreditsUb> {

    private static final long serialVersionUID = 180314960;

    public static final QVExtourneCreditsUb vExtourneCreditsUb = new QVExtourneCreditsUb("V_EXTOURNE_CREDITS_UB");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> orgIdUb = createSimple("orgIdUb", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> vecMontantBudConsomme = createSimple("vecMontantBudConsomme", Object.class);

    public final SimplePath<Object> vecMontantBudDisponible = createSimple("vecMontantBudDisponible", Object.class);

    public final SimplePath<Object> vecMontantBudDispoReel = createSimple("vecMontantBudDispoReel", Object.class);

    public final SimplePath<Object> vecMontantBudInitial = createSimple("vecMontantBudInitial", Object.class);

    public final SimplePath<Object> vecMontantHt = createSimple("vecMontantHt", Object.class);

    public final SimplePath<Object> vecMontantTtc = createSimple("vecMontantTtc", Object.class);

    public QVExtourneCreditsUb(String variable) {
        super(QVExtourneCreditsUb.class, forVariable(variable), "GFC", "V_EXTOURNE_CREDITS_UB");
        addMetadata();
    }

    public QVExtourneCreditsUb(String variable, String schema, String table) {
        super(QVExtourneCreditsUb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVExtourneCreditsUb(Path<? extends QVExtourneCreditsUb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EXTOURNE_CREDITS_UB");
        addMetadata();
    }

    public QVExtourneCreditsUb(PathMetadata<?> metadata) {
        super(QVExtourneCreditsUb.class, metadata, "GFC", "V_EXTOURNE_CREDITS_UB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdUb, ColumnMetadata.named("ORG_ID_UB").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudConsomme, ColumnMetadata.named("VEC_MONTANT_BUD_CONSOMME").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudDisponible, ColumnMetadata.named("VEC_MONTANT_BUD_DISPONIBLE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudDispoReel, ColumnMetadata.named("VEC_MONTANT_BUD_DISPO_REEL").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantBudInitial, ColumnMetadata.named("VEC_MONTANT_BUD_INITIAL").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantHt, ColumnMetadata.named("VEC_MONTANT_HT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(vecMontantTtc, ColumnMetadata.named("VEC_MONTANT_TTC").withIndex(5).ofType(Types.OTHER).withSize(0));
    }

}

