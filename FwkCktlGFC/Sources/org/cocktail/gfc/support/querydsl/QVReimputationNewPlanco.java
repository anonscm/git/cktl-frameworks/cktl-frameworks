package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVReimputationNewPlanco is a Querydsl query type for QVReimputationNewPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVReimputationNewPlanco extends com.mysema.query.sql.RelationalPathBase<QVReimputationNewPlanco> {

    private static final long serialVersionUID = -1826665949;

    public static final QVReimputationNewPlanco vReimputationNewPlanco = new QVReimputationNewPlanco("V_REIMPUTATION_NEW_PLANCO");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<java.math.BigDecimal> repcHtSaisie = createNumber("repcHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> repcId = createNumber("repcId", Long.class);

    public final NumberPath<java.math.BigDecimal> repcMontantBudgetaire = createNumber("repcMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> repcTtcSaisie = createNumber("repcTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> repcTvaSaisie = createNumber("repcTvaSaisie", java.math.BigDecimal.class);

    public QVReimputationNewPlanco(String variable) {
        super(QVReimputationNewPlanco.class, forVariable(variable), "GFC", "V_REIMPUTATION_NEW_PLANCO");
        addMetadata();
    }

    public QVReimputationNewPlanco(String variable, String schema, String table) {
        super(QVReimputationNewPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVReimputationNewPlanco(Path<? extends QVReimputationNewPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_REIMPUTATION_NEW_PLANCO");
        addMetadata();
    }

    public QVReimputationNewPlanco(PathMetadata<?> metadata) {
        super(QVReimputationNewPlanco.class, metadata, "GFC", "V_REIMPUTATION_NEW_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20));
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(repcHtSaisie, ColumnMetadata.named("REPC_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(repcId, ColumnMetadata.named("REPC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(repcMontantBudgetaire, ColumnMetadata.named("REPC_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(repcTtcSaisie, ColumnMetadata.named("REPC_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(repcTvaSaisie, ColumnMetadata.named("REPC_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
    }

}

