package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZEngageCtrlHorsMarche is a Querydsl query type for QZEngageCtrlHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZEngageCtrlHorsMarche extends com.mysema.query.sql.RelationalPathBase<QZEngageCtrlHorsMarche> {

    private static final long serialVersionUID = 1864084975;

    public static final QZEngageCtrlHorsMarche zEngageCtrlHorsMarche = new QZEngageCtrlHorsMarche("Z_ENGAGE_CTRL_HORS_MARCHE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> ehomDateSaisie = createDateTime("ehomDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> ehomHtReste = createNumber("ehomHtReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomHtSaisie = createNumber("ehomHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> ehomId = createNumber("ehomId", Long.class);

    public final NumberPath<java.math.BigDecimal> ehomMontantBudgetaire = createNumber("ehomMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomMontantBudgetaireReste = createNumber("ehomMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomTtcSaisie = createNumber("ehomTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ehomTvaSaisie = createNumber("ehomTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final NumberPath<Long> zehomId = createNumber("zehomId", Long.class);

    public final NumberPath<Long> zengId = createNumber("zengId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZEngageCtrlHorsMarche> sysC0077386 = createPrimaryKey(zehomId);

    public final com.mysema.query.sql.ForeignKey<QZEngageBudget> zEngageCtrlHmZengIdFk = createForeignKey(zengId, "ZENG_ID");

    public QZEngageCtrlHorsMarche(String variable) {
        super(QZEngageCtrlHorsMarche.class, forVariable(variable), "GFC", "Z_ENGAGE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QZEngageCtrlHorsMarche(String variable, String schema, String table) {
        super(QZEngageCtrlHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZEngageCtrlHorsMarche(Path<? extends QZEngageCtrlHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_ENGAGE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QZEngageCtrlHorsMarche(PathMetadata<?> metadata) {
        super(QZEngageCtrlHorsMarche.class, metadata, "GFC", "Z_ENGAGE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ehomDateSaisie, ColumnMetadata.named("EHOM_DATE_SAISIE").withIndex(13).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(ehomHtReste, ColumnMetadata.named("EHOM_HT_RESTE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomHtSaisie, ColumnMetadata.named("EHOM_HT_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomId, ColumnMetadata.named("EHOM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ehomMontantBudgetaire, ColumnMetadata.named("EHOM_MONTANT_BUDGETAIRE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomMontantBudgetaireReste, ColumnMetadata.named("EHOM_MONTANT_BUDGETAIRE_RESTE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomTtcSaisie, ColumnMetadata.named("EHOM_TTC_SAISIE").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ehomTvaSaisie, ColumnMetadata.named("EHOM_TVA_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zehomId, ColumnMetadata.named("ZEHOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zengId, ColumnMetadata.named("ZENG_ID").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

