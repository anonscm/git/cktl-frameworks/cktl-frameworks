package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVEditionTitreOrigine is a Querydsl query type for QVEditionTitreOrigine
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVEditionTitreOrigine extends com.mysema.query.sql.RelationalPathBase<QVEditionTitreOrigine> {

    private static final long serialVersionUID = 494701974;

    public static final QVEditionTitreOrigine vEditionTitreOrigine = new QVEditionTitreOrigine("V_EDITION_TITRE_ORIGINE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final NumberPath<Long> recIdOrigine = createNumber("recIdOrigine", Long.class);

    public final NumberPath<java.math.BigDecimal> recMontantBudgetaire = createNumber("recMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> recMontantBudgOrigine = createNumber("recMontantBudgOrigine", java.math.BigDecimal.class);

    public final NumberPath<Long> recOrdre = createNumber("recOrdre", Long.class);

    public final NumberPath<Long> recOrdreOrigine = createNumber("recOrdreOrigine", Long.class);

    public final NumberPath<Long> titBordOrigine = createNumber("titBordOrigine", Long.class);

    public final NumberPath<Long> titId = createNumber("titId", Long.class);

    public final NumberPath<Long> titIdOrigine = createNumber("titIdOrigine", Long.class);

    public final NumberPath<Long> titNumero = createNumber("titNumero", Long.class);

    public final NumberPath<Long> titNumeroOrigine = createNumber("titNumeroOrigine", Long.class);

    public QVEditionTitreOrigine(String variable) {
        super(QVEditionTitreOrigine.class, forVariable(variable), "GFC", "V_EDITION_TITRE_ORIGINE");
        addMetadata();
    }

    public QVEditionTitreOrigine(String variable, String schema, String table) {
        super(QVEditionTitreOrigine.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVEditionTitreOrigine(Path<? extends QVEditionTitreOrigine> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EDITION_TITRE_ORIGINE");
        addMetadata();
    }

    public QVEditionTitreOrigine(PathMetadata<?> metadata) {
        super(QVEditionTitreOrigine.class, metadata, "GFC", "V_EDITION_TITRE_ORIGINE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recIdOrigine, ColumnMetadata.named("REC_ID_ORIGINE").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recMontantBudgetaire, ColumnMetadata.named("REC_MONTANT_BUDGETAIRE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recMontantBudgOrigine, ColumnMetadata.named("REC_MONTANT_BUDG_ORIGINE").withIndex(13).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recOrdre, ColumnMetadata.named("REC_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recOrdreOrigine, ColumnMetadata.named("REC_ORDRE_ORIGINE").withIndex(11).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titBordOrigine, ColumnMetadata.named("TIT_BORD_ORIGINE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titIdOrigine, ColumnMetadata.named("TIT_ID_ORIGINE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titNumero, ColumnMetadata.named("TIT_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titNumeroOrigine, ColumnMetadata.named("TIT_NUMERO_ORIGINE").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

