package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCatImportDevise is a Querydsl query type for QCatImportDevise
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCatImportDevise extends com.mysema.query.sql.RelationalPathBase<QCatImportDevise> {

    private static final long serialVersionUID = 716472352;

    public static final QCatImportDevise catImportDevise = new QCatImportDevise("CAT_IMPORT_DEVISE");

    public final StringPath catMonnaie = createString("catMonnaie");

    public final NumberPath<Long> devId = createNumber("devId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCatImportDevise> catImportDevisePk = createPrimaryKey(catMonnaie);

    public final com.mysema.query.sql.ForeignKey<QAdmDevise> importDeviseDevIdFk = createForeignKey(devId, "DEV_ID");

    public QCatImportDevise(String variable) {
        super(QCatImportDevise.class, forVariable(variable), "GFC", "CAT_IMPORT_DEVISE");
        addMetadata();
    }

    public QCatImportDevise(String variable, String schema, String table) {
        super(QCatImportDevise.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCatImportDevise(Path<? extends QCatImportDevise> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CAT_IMPORT_DEVISE");
        addMetadata();
    }

    public QCatImportDevise(PathMetadata<?> metadata) {
        super(QCatImportDevise.class, metadata, "GFC", "CAT_IMPORT_DEVISE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(catMonnaie, ColumnMetadata.named("CAT_MONNAIE").withIndex(1).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(devId, ColumnMetadata.named("DEV_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

