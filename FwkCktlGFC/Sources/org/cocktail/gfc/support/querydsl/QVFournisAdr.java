package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVFournisAdr is a Querydsl query type for QVFournisAdr
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVFournisAdr extends com.mysema.query.sql.RelationalPathBase<QVFournisAdr> {

    private static final long serialVersionUID = 615509394;

    public static final QVFournisAdr vFournisAdr = new QVFournisAdr("V_FOURNIS_ADR");

    public final NumberPath<Long> adrOrdre = createNumber("adrOrdre", Long.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public QVFournisAdr(String variable) {
        super(QVFournisAdr.class, forVariable(variable), "GFC", "V_FOURNIS_ADR");
        addMetadata();
    }

    public QVFournisAdr(String variable, String schema, String table) {
        super(QVFournisAdr.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVFournisAdr(Path<? extends QVFournisAdr> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_FOURNIS_ADR");
        addMetadata();
    }

    public QVFournisAdr(PathMetadata<?> metadata) {
        super(QVFournisAdr.class, metadata, "GFC", "V_FOURNIS_ADR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrOrdre, ColumnMetadata.named("ADR_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

