package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOdpaiementBrouillard is a Querydsl query type for QOdpaiementBrouillard
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOdpaiementBrouillard extends com.mysema.query.sql.RelationalPathBase<QOdpaiementBrouillard> {

    private static final long serialVersionUID = 1937764577;

    public static final QOdpaiementBrouillard odpaiementBrouillard = new QOdpaiementBrouillard("ODPAIEMENT_BROUILLARD");

    public final StringPath gesCode = createString("gesCode");

    public final StringPath odbLibelle = createString("odbLibelle");

    public final NumberPath<java.math.BigDecimal> odbMontant = createNumber("odbMontant", java.math.BigDecimal.class);

    public final StringPath odbSens = createString("odbSens");

    public final NumberPath<Long> odpOrdre = createNumber("odpOrdre", Long.class);

    public final NumberPath<Long> opbOrdre = createNumber("opbOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final com.mysema.query.sql.PrimaryKey<QOdpaiementBrouillard> odpaiementBrouillardPk = createPrimaryKey(opbOrdre);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> odpaiementBrouillardPcoNFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QGestion> odpaiementBrouillardGesCFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QOrdreDePaiement> odpaiementBrouillardOdpFk = createForeignKey(odpOrdre, "ODP_ORDRE");

    public QOdpaiementBrouillard(String variable) {
        super(QOdpaiementBrouillard.class, forVariable(variable), "GFC", "ODPAIEMENT_BROUILLARD");
        addMetadata();
    }

    public QOdpaiementBrouillard(String variable, String schema, String table) {
        super(QOdpaiementBrouillard.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOdpaiementBrouillard(Path<? extends QOdpaiementBrouillard> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ODPAIEMENT_BROUILLARD");
        addMetadata();
    }

    public QOdpaiementBrouillard(PathMetadata<?> metadata) {
        super(QOdpaiementBrouillard.class, metadata, "GFC", "ODPAIEMENT_BROUILLARD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(odbLibelle, ColumnMetadata.named("ODB_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(200));
        addMetadata(odbMontant, ColumnMetadata.named("ODB_MONTANT").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(odbSens, ColumnMetadata.named("ODB_SENS").withIndex(4).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(odpOrdre, ColumnMetadata.named("ODP_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(opbOrdre, ColumnMetadata.named("OPB_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

