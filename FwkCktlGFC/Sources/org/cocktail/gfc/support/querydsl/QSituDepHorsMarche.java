package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSituDepHorsMarche is a Querydsl query type for QSituDepHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSituDepHorsMarche extends com.mysema.query.sql.RelationalPathBase<QSituDepHorsMarche> {

    private static final long serialVersionUID = -1788235103;

    public static final QSituDepHorsMarche situDepHorsMarche = new QSituDepHorsMarche("SITU_DEP_HORS_MARCHE");

    public final StringPath cmCode = createString("cmCode");

    public final StringPath cmLib = createString("cmLib");

    public final NumberPath<Long> cmOrdre = createNumber("cmOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> contr = createNumber("contr", java.math.BigDecimal.class);

    public final NumberPath<Integer> exer = createNumber("exer", Integer.class);

    public final NumberPath<Long> ht = createNumber("ht", Long.class);

    public final NumberPath<Long> pcontr = createNumber("pcontr", Long.class);

    public QSituDepHorsMarche(String variable) {
        super(QSituDepHorsMarche.class, forVariable(variable), "GFC", "SITU_DEP_HORS_MARCHE");
        addMetadata();
    }

    public QSituDepHorsMarche(String variable, String schema, String table) {
        super(QSituDepHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSituDepHorsMarche(Path<? extends QSituDepHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SITU_DEP_HORS_MARCHE");
        addMetadata();
    }

    public QSituDepHorsMarche(PathMetadata<?> metadata) {
        super(QSituDepHorsMarche.class, metadata, "GFC", "SITU_DEP_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cmCode, ColumnMetadata.named("CM_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(cmLib, ColumnMetadata.named("CM_LIB").withIndex(4).ofType(Types.VARCHAR).withSize(150).notNull());
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(contr, ColumnMetadata.named("CONTR").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(exer, ColumnMetadata.named("EXER").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(ht, ColumnMetadata.named("HT").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcontr, ColumnMetadata.named("PCONTR").withIndex(6).ofType(Types.DECIMAL).withSize(0));
    }

}

