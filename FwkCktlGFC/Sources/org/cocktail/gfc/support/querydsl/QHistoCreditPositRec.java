package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QHistoCreditPositRec is a Querydsl query type for QHistoCreditPositRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QHistoCreditPositRec extends com.mysema.query.sql.RelationalPathBase<QHistoCreditPositRec> {

    private static final long serialVersionUID = 1676321782;

    public static final QHistoCreditPositRec histoCreditPositRec = new QHistoCreditPositRec("HISTO_CREDIT_POSIT_REC");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> hcprDate = createDateTime("hcprDate", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> hcprMontant = createNumber("hcprMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> hcprOrdre = createNumber("hcprOrdre", Long.class);

    public final StringPath hcprSuppr = createString("hcprSuppr");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> traOrdre = createNumber("traOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QHistoCreditPositRec> histoCreditPositRecPk = createPrimaryKey(hcprOrdre);

    public QHistoCreditPositRec(String variable) {
        super(QHistoCreditPositRec.class, forVariable(variable), "GFC", "HISTO_CREDIT_POSIT_REC");
        addMetadata();
    }

    public QHistoCreditPositRec(String variable, String schema, String table) {
        super(QHistoCreditPositRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QHistoCreditPositRec(Path<? extends QHistoCreditPositRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "HISTO_CREDIT_POSIT_REC");
        addMetadata();
    }

    public QHistoCreditPositRec(PathMetadata<?> metadata) {
        super(QHistoCreditPositRec.class, metadata, "GFC", "HISTO_CREDIT_POSIT_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(hcprDate, ColumnMetadata.named("HCPR_DATE").withIndex(9).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(hcprMontant, ColumnMetadata.named("HCPR_MONTANT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(hcprOrdre, ColumnMetadata.named("HCPR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(hcprSuppr, ColumnMetadata.named("HCPR_SUPPR").withIndex(10).ofType(Types.VARCHAR).withSize(1));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(10));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
    }

}

