package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZDepenseCtrlHorsMarche is a Querydsl query type for QZDepenseCtrlHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZDepenseCtrlHorsMarche extends com.mysema.query.sql.RelationalPathBase<QZDepenseCtrlHorsMarche> {

    private static final long serialVersionUID = 625126598;

    public static final QZDepenseCtrlHorsMarche zDepenseCtrlHorsMarche = new QZDepenseCtrlHorsMarche("Z_DEPENSE_CTRL_HORS_MARCHE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<java.math.BigDecimal> dhomHtSaisie = createNumber("dhomHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dhomId = createNumber("dhomId", Long.class);

    public final NumberPath<java.math.BigDecimal> dhomMontantBudgetaire = createNumber("dhomMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dhomTtcSaisie = createNumber("dhomTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dhomTvaSaisie = createNumber("dhomTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final NumberPath<Long> zdepId = createNumber("zdepId", Long.class);

    public final NumberPath<Long> zdhomId = createNumber("zdhomId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZDepenseCtrlHorsMarche> sysC0077574 = createPrimaryKey(zdhomId);

    public final com.mysema.query.sql.ForeignKey<QZDepenseBudget> zDepenseCtrlHmZdepIdFk = createForeignKey(zdepId, "ZDEP_ID");

    public QZDepenseCtrlHorsMarche(String variable) {
        super(QZDepenseCtrlHorsMarche.class, forVariable(variable), "GFC", "Z_DEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QZDepenseCtrlHorsMarche(String variable, String schema, String table) {
        super(QZDepenseCtrlHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZDepenseCtrlHorsMarche(Path<? extends QZDepenseCtrlHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_DEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QZDepenseCtrlHorsMarche(PathMetadata<?> metadata) {
        super(QZDepenseCtrlHorsMarche.class, metadata, "GFC", "Z_DEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dhomHtSaisie, ColumnMetadata.named("DHOM_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dhomId, ColumnMetadata.named("DHOM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dhomMontantBudgetaire, ColumnMetadata.named("DHOM_MONTANT_BUDGETAIRE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dhomTtcSaisie, ColumnMetadata.named("DHOM_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dhomTvaSaisie, ColumnMetadata.named("DHOM_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdepId, ColumnMetadata.named("ZDEP_ID").withIndex(11).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdhomId, ColumnMetadata.named("ZDHOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

