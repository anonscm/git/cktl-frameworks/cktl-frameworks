package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputationAction is a Querydsl query type for QReimputationAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputationAction extends com.mysema.query.sql.RelationalPathBase<QReimputationAction> {

    private static final long serialVersionUID = 2108166196;

    public static final QReimputationAction reimputationAction = new QReimputationAction("REIMPUTATION_ACTION");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<java.math.BigDecimal> reacHtSaisie = createNumber("reacHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reacId = createNumber("reacId", Long.class);

    public final NumberPath<java.math.BigDecimal> reacMontantBudgetaire = createNumber("reacMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reacTtcSaisie = createNumber("reacTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> reacTvaSaisie = createNumber("reacTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputationAction> reimputationActionPk = createPrimaryKey(reacId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> reimpActionExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputation> reimpActionReimIdFk = createForeignKey(reimId, "REIM_ID");

    public QReimputationAction(String variable) {
        super(QReimputationAction.class, forVariable(variable), "GFC", "REIMPUTATION_ACTION");
        addMetadata();
    }

    public QReimputationAction(String variable, String schema, String table) {
        super(QReimputationAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputationAction(Path<? extends QReimputationAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION_ACTION");
        addMetadata();
    }

    public QReimputationAction(PathMetadata<?> metadata) {
        super(QReimputationAction.class, metadata, "GFC", "REIMPUTATION_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(reacHtSaisie, ColumnMetadata.named("REAC_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reacId, ColumnMetadata.named("REAC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(reacMontantBudgetaire, ColumnMetadata.named("REAC_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reacTtcSaisie, ColumnMetadata.named("REAC_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reacTvaSaisie, ColumnMetadata.named("REAC_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

