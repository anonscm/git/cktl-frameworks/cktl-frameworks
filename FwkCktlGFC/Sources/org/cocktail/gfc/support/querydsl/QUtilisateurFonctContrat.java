package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QUtilisateurFonctContrat is a Querydsl query type for QUtilisateurFonctContrat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QUtilisateurFonctContrat extends com.mysema.query.sql.RelationalPathBase<QUtilisateurFonctContrat> {

    private static final long serialVersionUID = -1678993441;

    public static final QUtilisateurFonctContrat utilisateurFonctContrat = new QUtilisateurFonctContrat("UTILISATEUR_FONCT_CONTRAT");

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final NumberPath<Long> ufcId = createNumber("ufcId", Long.class);

    public final NumberPath<Long> ufOrdre = createNumber("ufOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QUtilisateurFonctContrat> utilisateurFonctContratPk = createPrimaryKey(ufcId);

    public QUtilisateurFonctContrat(String variable) {
        super(QUtilisateurFonctContrat.class, forVariable(variable), "GFC", "UTILISATEUR_FONCT_CONTRAT");
        addMetadata();
    }

    public QUtilisateurFonctContrat(String variable, String schema, String table) {
        super(QUtilisateurFonctContrat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QUtilisateurFonctContrat(Path<? extends QUtilisateurFonctContrat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "UTILISATEUR_FONCT_CONTRAT");
        addMetadata();
    }

    public QUtilisateurFonctContrat(PathMetadata<?> metadata) {
        super(QUtilisateurFonctContrat.class, metadata, "GFC", "UTILISATEUR_FONCT_CONTRAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(3).ofType(Types.DECIMAL).withSize(22).notNull());
        addMetadata(ufcId, ColumnMetadata.named("UFC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(22).notNull());
        addMetadata(ufOrdre, ColumnMetadata.named("UF_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(22).notNull());
    }

}

