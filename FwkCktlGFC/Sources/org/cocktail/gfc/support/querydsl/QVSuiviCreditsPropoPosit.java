package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviCreditsPropoPosit is a Querydsl query type for QVSuiviCreditsPropoPosit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviCreditsPropoPosit extends com.mysema.query.sql.RelationalPathBase<QVSuiviCreditsPropoPosit> {

    private static final long serialVersionUID = -1576456568;

    public static final QVSuiviCreditsPropoPosit vSuiviCreditsPropoPosit = new QVSuiviCreditsPropoPosit("V_SUIVI_CREDITS_PROPO_POSIT");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> positionne = createSimple("positionne", Object.class);

    public final SimplePath<Object> propose = createSimple("propose", Object.class);

    public final SimplePath<Object> resteAPosit = createSimple("resteAPosit", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> traOrdre = createSimple("traOrdre", Object.class);

    public QVSuiviCreditsPropoPosit(String variable) {
        super(QVSuiviCreditsPropoPosit.class, forVariable(variable), "GFC", "V_SUIVI_CREDITS_PROPO_POSIT");
        addMetadata();
    }

    public QVSuiviCreditsPropoPosit(String variable, String schema, String table) {
        super(QVSuiviCreditsPropoPosit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviCreditsPropoPosit(Path<? extends QVSuiviCreditsPropoPosit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_CREDITS_PROPO_POSIT");
        addMetadata();
    }

    public QVSuiviCreditsPropoPosit(PathMetadata<?> metadata) {
        super(QVSuiviCreditsPropoPosit.class, metadata, "GFC", "V_SUIVI_CREDITS_PROPO_POSIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(positionne, ColumnMetadata.named("POSITIONNE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(propose, ColumnMetadata.named("PROPOSE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(resteAPosit, ColumnMetadata.named("RESTE_A_POSIT").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

