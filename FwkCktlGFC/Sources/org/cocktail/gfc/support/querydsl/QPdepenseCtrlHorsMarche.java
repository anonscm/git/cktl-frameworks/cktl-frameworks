package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseCtrlHorsMarche is a Querydsl query type for QPdepenseCtrlHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseCtrlHorsMarche extends com.mysema.query.sql.RelationalPathBase<QPdepenseCtrlHorsMarche> {

    private static final long serialVersionUID = 1999114928;

    public static final QPdepenseCtrlHorsMarche pdepenseCtrlHorsMarche = new QPdepenseCtrlHorsMarche("PDEPENSE_CTRL_HORS_MARCHE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> pdepId = createNumber("pdepId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdhomHtSaisie = createNumber("pdhomHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdhomId = createNumber("pdhomId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdhomMontantBudgetaire = createNumber("pdhomMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdhomTtcSaisie = createNumber("pdhomTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdhomTvaSaisie = createNumber("pdhomTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseCtrlHorsMarche> pdepenseCtrlHorsMarchePk = createPrimaryKey(pdhomId);

    public final com.mysema.query.sql.ForeignKey<QCodeExer> pdepenseCtrlHmCeOrdreFk = createForeignKey(ceOrdre, "CE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> pdepenseCtrlHmPdepIdFk = createForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> pdepenseCtrlHmExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypeAchat> pdepenseCtrlHmTypaIdFk = createForeignKey(typaId, "TYPA_ID");

    public QPdepenseCtrlHorsMarche(String variable) {
        super(QPdepenseCtrlHorsMarche.class, forVariable(variable), "GFC", "PDEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QPdepenseCtrlHorsMarche(String variable, String schema, String table) {
        super(QPdepenseCtrlHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseCtrlHorsMarche(Path<? extends QPdepenseCtrlHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QPdepenseCtrlHorsMarche(PathMetadata<?> metadata) {
        super(QPdepenseCtrlHorsMarche.class, metadata, "GFC", "PDEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pdepId, ColumnMetadata.named("PDEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdhomHtSaisie, ColumnMetadata.named("PDHOM_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdhomId, ColumnMetadata.named("PDHOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdhomMontantBudgetaire, ColumnMetadata.named("PDHOM_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdhomTtcSaisie, ColumnMetadata.named("PDHOM_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdhomTvaSaisie, ColumnMetadata.named("PDHOM_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

