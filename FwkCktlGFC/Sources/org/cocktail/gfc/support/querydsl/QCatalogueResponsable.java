package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCatalogueResponsable is a Querydsl query type for QCatalogueResponsable
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCatalogueResponsable extends com.mysema.query.sql.RelationalPathBase<QCatalogueResponsable> {

    private static final long serialVersionUID = 663337558;

    public static final QCatalogueResponsable catalogueResponsable = new QCatalogueResponsable("CATALOGUE_RESPONSABLE");

    public final NumberPath<Long> carId = createNumber("carId", Long.class);

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCatalogueResponsable> catalogueResponsablePk = createPrimaryKey(carId);

    public final com.mysema.query.sql.ForeignKey<QCataloguePrestation> carCatIdFk = createForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> carUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public QCatalogueResponsable(String variable) {
        super(QCatalogueResponsable.class, forVariable(variable), "GFC", "CATALOGUE_RESPONSABLE");
        addMetadata();
    }

    public QCatalogueResponsable(String variable, String schema, String table) {
        super(QCatalogueResponsable.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCatalogueResponsable(Path<? extends QCatalogueResponsable> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CATALOGUE_RESPONSABLE");
        addMetadata();
    }

    public QCatalogueResponsable(PathMetadata<?> metadata) {
        super(QCatalogueResponsable.class, metadata, "GFC", "CATALOGUE_RESPONSABLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(carId, ColumnMetadata.named("CAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

