package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecTypeNumerotation is a Querydsl query type for QRecTypeNumerotation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecTypeNumerotation extends com.mysema.query.sql.RelationalPathBase<QRecTypeNumerotation> {

    private static final long serialVersionUID = 1474446470;

    public static final QRecTypeNumerotation recTypeNumerotation = new QRecTypeNumerotation("REC_TYPE_NUMEROTATION");

    public final StringPath tnuEntite = createString("tnuEntite");

    public final NumberPath<Long> tnuId = createNumber("tnuId", Long.class);

    public final StringPath tnuLibelle = createString("tnuLibelle");

    public final com.mysema.query.sql.PrimaryKey<QRecTypeNumerotation> recTypeNumerotationPk = createPrimaryKey(tnuId);

    public final com.mysema.query.sql.ForeignKey<QRecNumerotation> _tnuIdFk = createInvForeignKey(tnuId, "TNU_ID");

    public QRecTypeNumerotation(String variable) {
        super(QRecTypeNumerotation.class, forVariable(variable), "GFC", "REC_TYPE_NUMEROTATION");
        addMetadata();
    }

    public QRecTypeNumerotation(String variable, String schema, String table) {
        super(QRecTypeNumerotation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecTypeNumerotation(Path<? extends QRecTypeNumerotation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REC_TYPE_NUMEROTATION");
        addMetadata();
    }

    public QRecTypeNumerotation(PathMetadata<?> metadata) {
        super(QRecTypeNumerotation.class, metadata, "GFC", "REC_TYPE_NUMEROTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tnuEntite, ColumnMetadata.named("TNU_ENTITE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tnuId, ColumnMetadata.named("TNU_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnuLibelle, ColumnMetadata.named("TNU_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
    }

}

