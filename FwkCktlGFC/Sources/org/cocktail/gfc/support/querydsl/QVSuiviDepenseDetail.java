package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviDepenseDetail is a Querydsl query type for QVSuiviDepenseDetail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviDepenseDetail extends com.mysema.query.sql.RelationalPathBase<QVSuiviDepenseDetail> {

    private static final long serialVersionUID = 1366906262;

    public static final QVSuiviDepenseDetail vSuiviDepenseDetail = new QVSuiviDepenseDetail("V_SUIVI_DEPENSE_DETAIL");

    public final SimplePath<Object> commId = createSimple("commId", Object.class);

    public final SimplePath<Object> commNumero = createSimple("commNumero", Object.class);

    public final SimplePath<Object> dconHtSaisie = createSimple("dconHtSaisie", Object.class);

    public final SimplePath<Object> dconTtcSaisie = createSimple("dconTtcSaisie", Object.class);

    public final SimplePath<Object> depId = createSimple("depId", Object.class);

    public final SimplePath<Object> dppDateSaisie = createSimple("dppDateSaisie", Object.class);

    public final SimplePath<Object> dppId = createSimple("dppId", Object.class);

    public final SimplePath<Object> dppNumeroFacture = createSimple("dppNumeroFacture", Object.class);

    public final SimplePath<Object> econDateSaisie = createSimple("econDateSaisie", Object.class);

    public final SimplePath<Object> econMontantBudgetaire = createSimple("econMontantBudgetaire", Object.class);

    public final SimplePath<Object> econMontantBudgetaireReste = createSimple("econMontantBudgetaireReste", Object.class);

    public final SimplePath<Object> engId = createSimple("engId", Object.class);

    public final SimplePath<Object> engNumero = createSimple("engNumero", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> tapId = createSimple("tapId", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QVSuiviDepenseDetail(String variable) {
        super(QVSuiviDepenseDetail.class, forVariable(variable), "GFC", "V_SUIVI_DEPENSE_DETAIL");
        addMetadata();
    }

    public QVSuiviDepenseDetail(String variable, String schema, String table) {
        super(QVSuiviDepenseDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviDepenseDetail(Path<? extends QVSuiviDepenseDetail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_DEPENSE_DETAIL");
        addMetadata();
    }

    public QVSuiviDepenseDetail(PathMetadata<?> metadata) {
        super(QVSuiviDepenseDetail.class, metadata, "GFC", "V_SUIVI_DEPENSE_DETAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(commNumero, ColumnMetadata.named("COMM_NUMERO").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(dconHtSaisie, ColumnMetadata.named("DCON_HT_SAISIE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(dconTtcSaisie, ColumnMetadata.named("DCON_TTC_SAISIE").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(dppDateSaisie, ColumnMetadata.named("DPP_DATE_SAISIE").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(econDateSaisie, ColumnMetadata.named("ECON_DATE_SAISIE").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaire, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaireReste, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE_RESTE").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

