package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviCreditsPositionnes is a Querydsl query type for QVSuiviCreditsPositionnes
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviCreditsPositionnes extends com.mysema.query.sql.RelationalPathBase<QVSuiviCreditsPositionnes> {

    private static final long serialVersionUID = 1867508830;

    public static final QVSuiviCreditsPositionnes vSuiviCreditsPositionnes = new QVSuiviCreditsPositionnes("V_SUIVI_CREDITS_POSITIONNES");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> positionne = createSimple("positionne", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> traOrdre = createSimple("traOrdre", Object.class);

    public QVSuiviCreditsPositionnes(String variable) {
        super(QVSuiviCreditsPositionnes.class, forVariable(variable), "GFC", "V_SUIVI_CREDITS_POSITIONNES");
        addMetadata();
    }

    public QVSuiviCreditsPositionnes(String variable, String schema, String table) {
        super(QVSuiviCreditsPositionnes.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviCreditsPositionnes(Path<? extends QVSuiviCreditsPositionnes> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_CREDITS_POSITIONNES");
        addMetadata();
    }

    public QVSuiviCreditsPositionnes(PathMetadata<?> metadata) {
        super(QVSuiviCreditsPositionnes.class, metadata, "GFC", "V_SUIVI_CREDITS_POSITIONNES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(positionne, ColumnMetadata.named("POSITIONNE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

