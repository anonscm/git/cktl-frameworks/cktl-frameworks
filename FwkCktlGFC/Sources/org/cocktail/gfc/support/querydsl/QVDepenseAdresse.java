package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepenseAdresse is a Querydsl query type for QVDepenseAdresse
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepenseAdresse extends com.mysema.query.sql.RelationalPathBase<QVDepenseAdresse> {

    private static final long serialVersionUID = -1701364998;

    public static final QVDepenseAdresse vDepenseAdresse = new QVDepenseAdresse("V_DEPENSE_ADRESSE");

    public final StringPath adrAdresse1 = createString("adrAdresse1");

    public final StringPath adrAdresse2 = createString("adrAdresse2");

    public final StringPath adresse = createString("adresse");

    public final NumberPath<Long> adrOrdre = createNumber("adrOrdre", Long.class);

    public final StringPath civilite = createString("civilite");

    public final StringPath codePostal = createString("codePostal");

    public final StringPath cPays = createString("cPays");

    public final StringPath cpEtranger = createString("cpEtranger");

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final StringPath nom = createString("nom");

    public final StringPath prenom = createString("prenom");

    public final StringPath ville = createString("ville");

    public QVDepenseAdresse(String variable) {
        super(QVDepenseAdresse.class, forVariable(variable), "GFC", "V_DEPENSE_ADRESSE");
        addMetadata();
    }

    public QVDepenseAdresse(String variable, String schema, String table) {
        super(QVDepenseAdresse.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepenseAdresse(Path<? extends QVDepenseAdresse> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSE_ADRESSE");
        addMetadata();
    }

    public QVDepenseAdresse(PathMetadata<?> metadata) {
        super(QVDepenseAdresse.class, metadata, "GFC", "V_DEPENSE_ADRESSE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrAdresse1, ColumnMetadata.named("ADR_ADRESSE1").withIndex(9).ofType(Types.VARCHAR).withSize(100));
        addMetadata(adrAdresse2, ColumnMetadata.named("ADR_ADRESSE2").withIndex(10).ofType(Types.VARCHAR).withSize(300));
        addMetadata(adresse, ColumnMetadata.named("ADRESSE").withIndex(14).ofType(Types.VARCHAR).withSize(510));
        addMetadata(adrOrdre, ColumnMetadata.named("ADR_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(civilite, ColumnMetadata.named("CIVILITE").withIndex(5).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(codePostal, ColumnMetadata.named("CODE_POSTAL").withIndex(11).ofType(Types.VARCHAR).withSize(10));
        addMetadata(cPays, ColumnMetadata.named("C_PAYS").withIndex(15).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(cpEtranger, ColumnMetadata.named("CP_ETRANGER").withIndex(13).ofType(Types.VARCHAR).withSize(10));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(nom, ColumnMetadata.named("NOM").withIndex(6).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(prenom, ColumnMetadata.named("PRENOM").withIndex(7).ofType(Types.VARCHAR).withSize(40));
        addMetadata(ville, ColumnMetadata.named("VILLE").withIndex(12).ofType(Types.VARCHAR).withSize(60));
    }

}

