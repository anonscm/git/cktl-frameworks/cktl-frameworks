package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSepaSddOrigine is a Querydsl query type for QSepaSddOrigine
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSepaSddOrigine extends com.mysema.query.sql.RelationalPathBase<QSepaSddOrigine> {

    private static final long serialVersionUID = 1938862512;

    public static final QSepaSddOrigine sepaSddOrigine = new QSepaSddOrigine("SEPA_SDD_ORIGINE");

    public final NumberPath<Long> idSepaSddOrigine = createNumber("idSepaSddOrigine", Long.class);

    public final NumberPath<Long> idSepaSddOrigineType = createNumber("idSepaSddOrigineType", Long.class);

    public final NumberPath<Long> origineId = createNumber("origineId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QSepaSddOrigine> sepaSddOriginePk = createPrimaryKey(idSepaSddOrigine);

    public final com.mysema.query.sql.ForeignKey<QSepaSddOrigineType> sepaSddOrigineTypFk = createForeignKey(idSepaSddOrigineType, "ID_SEPA_SDD_ORIGINE_TYPE");

    public final com.mysema.query.sql.ForeignKey<QSepaSddEcheancier> _sepaSddEcheancierOrigineFk = createInvForeignKey(idSepaSddOrigine, "ID_SEPA_SDD_ORIGINE");

    public QSepaSddOrigine(String variable) {
        super(QSepaSddOrigine.class, forVariable(variable), "GFC", "SEPA_SDD_ORIGINE");
        addMetadata();
    }

    public QSepaSddOrigine(String variable, String schema, String table) {
        super(QSepaSddOrigine.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSepaSddOrigine(Path<? extends QSepaSddOrigine> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SEPA_SDD_ORIGINE");
        addMetadata();
    }

    public QSepaSddOrigine(PathMetadata<?> metadata) {
        super(QSepaSddOrigine.class, metadata, "GFC", "SEPA_SDD_ORIGINE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idSepaSddOrigine, ColumnMetadata.named("ID_SEPA_SDD_ORIGINE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idSepaSddOrigineType, ColumnMetadata.named("ID_SEPA_SDD_ORIGINE_TYPE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(origineId, ColumnMetadata.named("ORIGINE_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

