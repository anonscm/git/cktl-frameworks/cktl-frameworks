package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QXlabConvention is a Querydsl query type for QXlabConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QXlabConvention extends com.mysema.query.sql.RelationalPathBase<QXlabConvention> {

    private static final long serialVersionUID = -1504925017;

    public static final QXlabConvention xlabConvention = new QXlabConvention("XLAB_CONVENTION");

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final StringPath xconCode = createString("xconCode");

    public final NumberPath<Long> xconId = createNumber("xconId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QXlabConvention> xlabConventionPk = createPrimaryKey(xconId);

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeXconIdFk = createInvForeignKey(xconId, "XCON_ID");

    public QXlabConvention(String variable) {
        super(QXlabConvention.class, forVariable(variable), "GFC", "XLAB_CONVENTION");
        addMetadata();
    }

    public QXlabConvention(String variable, String schema, String table) {
        super(QXlabConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QXlabConvention(Path<? extends QXlabConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "XLAB_CONVENTION");
        addMetadata();
    }

    public QXlabConvention(PathMetadata<?> metadata) {
        super(QXlabConvention.class, metadata, "GFC", "XLAB_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(xconCode, ColumnMetadata.named("XCON_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(4).notNull());
        addMetadata(xconId, ColumnMetadata.named("XCON_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

