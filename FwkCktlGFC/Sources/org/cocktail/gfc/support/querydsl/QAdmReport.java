package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmReport is a Querydsl query type for QAdmReport
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmReport extends com.mysema.query.sql.RelationalPathBase<QAdmReport> {

    private static final long serialVersionUID = 788935325;

    public static final QAdmReport admReport = new QAdmReport("ADM_REPORT");

    public final StringPath repId = createString("repId");

    public final StringPath repLocation = createString("repLocation");

    public final StringPath repNom = createString("repNom");

    public final com.mysema.query.sql.PrimaryKey<QAdmReport> reportPk = createPrimaryKey(repId);

    public QAdmReport(String variable) {
        super(QAdmReport.class, forVariable(variable), "GFC", "ADM_REPORT");
        addMetadata();
    }

    public QAdmReport(String variable, String schema, String table) {
        super(QAdmReport.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmReport(Path<? extends QAdmReport> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_REPORT");
        addMetadata();
    }

    public QAdmReport(PathMetadata<?> metadata) {
        super(QAdmReport.class, metadata, "GFC", "ADM_REPORT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(repId, ColumnMetadata.named("REP_ID").withIndex(1).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(repLocation, ColumnMetadata.named("REP_LOCATION").withIndex(3).ofType(Types.VARCHAR).withSize(500));
        addMetadata(repNom, ColumnMetadata.named("REP_NOM").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

