package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAttributionExecutionFouExer is a Querydsl query type for QAttributionExecutionFouExer
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAttributionExecutionFouExer extends com.mysema.query.sql.RelationalPathBase<QAttributionExecutionFouExer> {

    private static final long serialVersionUID = -317341102;

    public static final QAttributionExecutionFouExer attributionExecutionFouExer = new QAttributionExecutionFouExer("ATTRIBUTION_EXECUTION_FOU_EXER");

    public final NumberPath<Long> aeeEngHt = createNumber("aeeEngHt", Long.class);

    public final NumberPath<Long> aeeExecution = createNumber("aeeExecution", Long.class);

    public final NumberPath<Long> aeeLiqHt = createNumber("aeeLiqHt", Long.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public QAttributionExecutionFouExer(String variable) {
        super(QAttributionExecutionFouExer.class, forVariable(variable), "GFC", "ATTRIBUTION_EXECUTION_FOU_EXER");
        addMetadata();
    }

    public QAttributionExecutionFouExer(String variable, String schema, String table) {
        super(QAttributionExecutionFouExer.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAttributionExecutionFouExer(Path<? extends QAttributionExecutionFouExer> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ATTRIBUTION_EXECUTION_FOU_EXER");
        addMetadata();
    }

    public QAttributionExecutionFouExer(PathMetadata<?> metadata) {
        super(QAttributionExecutionFouExer.class, metadata, "GFC", "ATTRIBUTION_EXECUTION_FOU_EXER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeeEngHt, ColumnMetadata.named("AEE_ENG_HT").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeExecution, ColumnMetadata.named("AEE_EXECUTION").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeLiqHt, ColumnMetadata.named("AEE_LIQ_HT").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

