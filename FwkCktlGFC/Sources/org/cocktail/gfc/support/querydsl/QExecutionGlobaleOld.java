package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QExecutionGlobaleOld is a Querydsl query type for QExecutionGlobaleOld
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QExecutionGlobaleOld extends com.mysema.query.sql.RelationalPathBase<QExecutionGlobaleOld> {

    private static final long serialVersionUID = -545961924;

    public static final QExecutionGlobaleOld executionGlobaleOld = new QExecutionGlobaleOld("EXECUTION_GLOBALE_OLD");

    public final NumberPath<java.math.BigDecimal> aeeEngHt = createNumber("aeeEngHt", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> aeeExecution = createNumber("aeeExecution", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> aeeLiqHt = createNumber("aeeLiqHt", java.math.BigDecimal.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QExecutionGlobaleOld> executionGlobaleOldPk = createPrimaryKey(attOrdre, exeOrdre);

    public QExecutionGlobaleOld(String variable) {
        super(QExecutionGlobaleOld.class, forVariable(variable), "GFC", "EXECUTION_GLOBALE_OLD");
        addMetadata();
    }

    public QExecutionGlobaleOld(String variable, String schema, String table) {
        super(QExecutionGlobaleOld.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QExecutionGlobaleOld(Path<? extends QExecutionGlobaleOld> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EXECUTION_GLOBALE_OLD");
        addMetadata();
    }

    public QExecutionGlobaleOld(PathMetadata<?> metadata) {
        super(QExecutionGlobaleOld.class, metadata, "GFC", "EXECUTION_GLOBALE_OLD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeeEngHt, ColumnMetadata.named("AEE_ENG_HT").withIndex(3).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(aeeExecution, ColumnMetadata.named("AEE_EXECUTION").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(aeeLiqHt, ColumnMetadata.named("AEE_LIQ_HT").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
    }

}

