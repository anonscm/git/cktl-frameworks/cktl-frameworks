package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviPrevuRecetteDetail is a Querydsl query type for QVSuiviPrevuRecetteDetail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviPrevuRecetteDetail extends com.mysema.query.sql.RelationalPathBase<QVSuiviPrevuRecetteDetail> {

    private static final long serialVersionUID = 1833859172;

    public static final QVSuiviPrevuRecetteDetail vSuiviPrevuRecetteDetail = new QVSuiviPrevuRecetteDetail("V_SUIVI_PREVU_RECETTE_DETAIL");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> facId = createSimple("facId", Object.class);

    public final SimplePath<Object> facNumero = createSimple("facNumero", Object.class);

    public final SimplePath<Object> fconDateSaisie = createSimple("fconDateSaisie", Object.class);

    public final SimplePath<Object> fconMontantBudgetaire = createSimple("fconMontantBudgetaire", Object.class);

    public final SimplePath<Object> fconMontantBudgetaireReste = createSimple("fconMontantBudgetaireReste", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> orgIdPosit = createSimple("orgIdPosit", Object.class);

    public final SimplePath<Object> orgIdPropo = createSimple("orgIdPropo", Object.class);

    public final SimplePath<Object> rconHtSaisie = createSimple("rconHtSaisie", Object.class);

    public final SimplePath<Object> rconTtcSaisie = createSimple("rconTtcSaisie", Object.class);

    public final SimplePath<Object> recId = createSimple("recId", Object.class);

    public final SimplePath<Object> rppDateSaisie = createSimple("rppDateSaisie", Object.class);

    public final SimplePath<Object> rppId = createSimple("rppId", Object.class);

    public final SimplePath<Object> rppNumero = createSimple("rppNumero", Object.class);

    public final SimplePath<Object> tapId = createSimple("tapId", Object.class);

    public final SimplePath<Object> tcdOrdrePosit = createSimple("tcdOrdrePosit", Object.class);

    public final SimplePath<Object> totalPosit = createSimple("totalPosit", Object.class);

    public final SimplePath<Object> totalPropo = createSimple("totalPropo", Object.class);

    public QVSuiviPrevuRecetteDetail(String variable) {
        super(QVSuiviPrevuRecetteDetail.class, forVariable(variable), "GFC", "V_SUIVI_PREVU_RECETTE_DETAIL");
        addMetadata();
    }

    public QVSuiviPrevuRecetteDetail(String variable, String schema, String table) {
        super(QVSuiviPrevuRecetteDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviPrevuRecetteDetail(Path<? extends QVSuiviPrevuRecetteDetail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_PREVU_RECETTE_DETAIL");
        addMetadata();
    }

    public QVSuiviPrevuRecetteDetail(PathMetadata<?> metadata) {
        super(QVSuiviPrevuRecetteDetail.class, metadata, "GFC", "V_SUIVI_PREVU_RECETTE_DETAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(facId, ColumnMetadata.named("FAC_ID").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(facNumero, ColumnMetadata.named("FAC_NUMERO").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(fconDateSaisie, ColumnMetadata.named("FCON_DATE_SAISIE").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(fconMontantBudgetaire, ColumnMetadata.named("FCON_MONTANT_BUDGETAIRE").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(fconMontantBudgetaireReste, ColumnMetadata.named("FCON_MONTANT_BUDGETAIRE_RESTE").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPosit, ColumnMetadata.named("ORG_ID_POSIT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPropo, ColumnMetadata.named("ORG_ID_PROPO").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(rconHtSaisie, ColumnMetadata.named("RCON_HT_SAISIE").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(rconTtcSaisie, ColumnMetadata.named("RCON_TTC_SAISIE").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(rppDateSaisie, ColumnMetadata.named("RPP_DATE_SAISIE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(rppId, ColumnMetadata.named("RPP_ID").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(rppNumero, ColumnMetadata.named("RPP_NUMERO").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdrePosit, ColumnMetadata.named("TCD_ORDRE_POSIT").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPosit, ColumnMetadata.named("TOTAL_POSIT").withIndex(19).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPropo, ColumnMetadata.named("TOTAL_PROPO").withIndex(18).ofType(Types.OTHER).withSize(0));
    }

}

