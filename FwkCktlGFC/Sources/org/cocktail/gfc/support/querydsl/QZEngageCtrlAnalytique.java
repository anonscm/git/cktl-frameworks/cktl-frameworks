package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZEngageCtrlAnalytique is a Querydsl query type for QZEngageCtrlAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZEngageCtrlAnalytique extends com.mysema.query.sql.RelationalPathBase<QZEngageCtrlAnalytique> {

    private static final long serialVersionUID = 548578864;

    public static final QZEngageCtrlAnalytique zEngageCtrlAnalytique = new QZEngageCtrlAnalytique("Z_ENGAGE_CTRL_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final DateTimePath<java.sql.Timestamp> eanaDateSaisie = createDateTime("eanaDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> eanaHtSaisie = createNumber("eanaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> eanaId = createNumber("eanaId", Long.class);

    public final NumberPath<java.math.BigDecimal> eanaMontantBudgetaire = createNumber("eanaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eanaMontantBudgetaireReste = createNumber("eanaMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eanaTtcSaisie = createNumber("eanaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eanaTvaSaisie = createNumber("eanaTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> zeanaId = createNumber("zeanaId", Long.class);

    public final NumberPath<Long> zengId = createNumber("zengId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZEngageCtrlAnalytique> sysC0077756 = createPrimaryKey(zeanaId);

    public final com.mysema.query.sql.ForeignKey<QZEngageBudget> zEngageCtrlAnalZengIdFk = createForeignKey(zengId, "ZENG_ID");

    public QZEngageCtrlAnalytique(String variable) {
        super(QZEngageCtrlAnalytique.class, forVariable(variable), "GFC", "Z_ENGAGE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QZEngageCtrlAnalytique(String variable, String schema, String table) {
        super(QZEngageCtrlAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZEngageCtrlAnalytique(Path<? extends QZEngageCtrlAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_ENGAGE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QZEngageCtrlAnalytique(PathMetadata<?> metadata) {
        super(QZEngageCtrlAnalytique.class, metadata, "GFC", "Z_ENGAGE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(eanaDateSaisie, ColumnMetadata.named("EANA_DATE_SAISIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(eanaHtSaisie, ColumnMetadata.named("EANA_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaId, ColumnMetadata.named("EANA_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(eanaMontantBudgetaire, ColumnMetadata.named("EANA_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaMontantBudgetaireReste, ColumnMetadata.named("EANA_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaTtcSaisie, ColumnMetadata.named("EANA_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaTvaSaisie, ColumnMetadata.named("EANA_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(zeanaId, ColumnMetadata.named("ZEANA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zengId, ColumnMetadata.named("ZENG_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

