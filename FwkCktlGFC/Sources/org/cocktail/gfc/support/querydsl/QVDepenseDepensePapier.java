package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepenseDepensePapier is a Querydsl query type for QVDepenseDepensePapier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepenseDepensePapier extends com.mysema.query.sql.RelationalPathBase<QVDepenseDepensePapier> {

    private static final long serialVersionUID = 834918860;

    public static final QVDepenseDepensePapier vDepenseDepensePapier = new QVDepenseDepensePapier("V_DEPENSE_DEPENSE_PAPIER");

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> jdDepId = createNumber("jdDepId", Long.class);

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final NumberPath<Long> maraDepId = createNumber("maraDepId", Long.class);

    public QVDepenseDepensePapier(String variable) {
        super(QVDepenseDepensePapier.class, forVariable(variable), "GFC", "V_DEPENSE_DEPENSE_PAPIER");
        addMetadata();
    }

    public QVDepenseDepensePapier(String variable, String schema, String table) {
        super(QVDepenseDepensePapier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepenseDepensePapier(Path<? extends QVDepenseDepensePapier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSE_DEPENSE_PAPIER");
        addMetadata();
    }

    public QVDepenseDepensePapier(PathMetadata<?> metadata) {
        super(QVDepenseDepensePapier.class, metadata, "GFC", "V_DEPENSE_DEPENSE_PAPIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(jdDepId, ColumnMetadata.named("JD_DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(maraDepId, ColumnMetadata.named("MARA_DEP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

