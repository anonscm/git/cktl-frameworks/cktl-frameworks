package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZDepenseCtrlConvention is a Querydsl query type for QZDepenseCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZDepenseCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QZDepenseCtrlConvention> {

    private static final long serialVersionUID = -2001865203;

    public static final QZDepenseCtrlConvention zDepenseCtrlConvention = new QZDepenseCtrlConvention("Z_DEPENSE_CTRL_CONVENTION");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> dconHtSaisie = createNumber("dconHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dconId = createNumber("dconId", Long.class);

    public final NumberPath<java.math.BigDecimal> dconMontantBudgetaire = createNumber("dconMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dconTtcSaisie = createNumber("dconTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dconTvaSaisie = createNumber("dconTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> zdconId = createNumber("zdconId", Long.class);

    public final NumberPath<Long> zdepId = createNumber("zdepId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZDepenseCtrlConvention> sysC0076303 = createPrimaryKey(zdconId);

    public final com.mysema.query.sql.ForeignKey<QZDepenseBudget> zDepenseCtrlConZdepIdFk = createForeignKey(zdepId, "ZDEP_ID");

    public QZDepenseCtrlConvention(String variable) {
        super(QZDepenseCtrlConvention.class, forVariable(variable), "GFC", "Z_DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QZDepenseCtrlConvention(String variable, String schema, String table) {
        super(QZDepenseCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZDepenseCtrlConvention(Path<? extends QZDepenseCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QZDepenseCtrlConvention(PathMetadata<?> metadata) {
        super(QZDepenseCtrlConvention.class, metadata, "GFC", "Z_DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dconHtSaisie, ColumnMetadata.named("DCON_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dconId, ColumnMetadata.named("DCON_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dconMontantBudgetaire, ColumnMetadata.named("DCON_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dconTtcSaisie, ColumnMetadata.named("DCON_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dconTvaSaisie, ColumnMetadata.named("DCON_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(zdconId, ColumnMetadata.named("ZDCON_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdepId, ColumnMetadata.named("ZDEP_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

