package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZDepenseCtrlAction is a Querydsl query type for QZDepenseCtrlAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZDepenseCtrlAction extends com.mysema.query.sql.RelationalPathBase<QZDepenseCtrlAction> {

    private static final long serialVersionUID = 1075431122;

    public static final QZDepenseCtrlAction zDepenseCtrlAction = new QZDepenseCtrlAction("Z_DEPENSE_CTRL_ACTION");

    public final NumberPath<java.math.BigDecimal> dactHtSaisie = createNumber("dactHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dactId = createNumber("dactId", Long.class);

    public final NumberPath<java.math.BigDecimal> dactMontantBudgetaire = createNumber("dactMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dactTtcSaisie = createNumber("dactTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dactTvaSaisie = createNumber("dactTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final NumberPath<Long> zdactId = createNumber("zdactId", Long.class);

    public final NumberPath<Long> zdepId = createNumber("zdepId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZDepenseCtrlAction> sysC0077434 = createPrimaryKey(zdactId);

    public final com.mysema.query.sql.ForeignKey<QZDepenseBudget> zDepenseCtrlActiZdepIdFk = createForeignKey(zdepId, "ZDEP_ID");

    public QZDepenseCtrlAction(String variable) {
        super(QZDepenseCtrlAction.class, forVariable(variable), "GFC", "Z_DEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public QZDepenseCtrlAction(String variable, String schema, String table) {
        super(QZDepenseCtrlAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZDepenseCtrlAction(Path<? extends QZDepenseCtrlAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_DEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public QZDepenseCtrlAction(PathMetadata<?> metadata) {
        super(QZDepenseCtrlAction.class, metadata, "GFC", "Z_DEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dactHtSaisie, ColumnMetadata.named("DACT_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dactId, ColumnMetadata.named("DACT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dactMontantBudgetaire, ColumnMetadata.named("DACT_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dactTtcSaisie, ColumnMetadata.named("DACT_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dactTvaSaisie, ColumnMetadata.named("DACT_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdactId, ColumnMetadata.named("ZDACT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdepId, ColumnMetadata.named("ZDEP_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

