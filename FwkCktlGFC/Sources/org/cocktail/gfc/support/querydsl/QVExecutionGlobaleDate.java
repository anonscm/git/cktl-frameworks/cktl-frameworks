package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVExecutionGlobaleDate is a Querydsl query type for QVExecutionGlobaleDate
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVExecutionGlobaleDate extends com.mysema.query.sql.RelationalPathBase<QVExecutionGlobaleDate> {

    private static final long serialVersionUID = -1962411795;

    public static final QVExecutionGlobaleDate vExecutionGlobaleDate = new QVExecutionGlobaleDate("V_EXECUTION_GLOBALE_DATE");

    public final NumberPath<Long> aeeEngHt = createNumber("aeeEngHt", Long.class);

    public final NumberPath<Long> aeeExecution = createNumber("aeeExecution", Long.class);

    public final NumberPath<Long> aeeLiqHt = createNumber("aeeLiqHt", Long.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateEng = createDateTime("dateEng", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateExec = createDateTime("dateExec", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateLiqu = createDateTime("dateLiqu", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public QVExecutionGlobaleDate(String variable) {
        super(QVExecutionGlobaleDate.class, forVariable(variable), "GFC", "V_EXECUTION_GLOBALE_DATE");
        addMetadata();
    }

    public QVExecutionGlobaleDate(String variable, String schema, String table) {
        super(QVExecutionGlobaleDate.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVExecutionGlobaleDate(Path<? extends QVExecutionGlobaleDate> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EXECUTION_GLOBALE_DATE");
        addMetadata();
    }

    public QVExecutionGlobaleDate(PathMetadata<?> metadata) {
        super(QVExecutionGlobaleDate.class, metadata, "GFC", "V_EXECUTION_GLOBALE_DATE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeeEngHt, ColumnMetadata.named("AEE_ENG_HT").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeExecution, ColumnMetadata.named("AEE_EXECUTION").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeLiqHt, ColumnMetadata.named("AEE_LIQ_HT").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dateEng, ColumnMetadata.named("DATE_ENG").withIndex(6).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateExec, ColumnMetadata.named("DATE_EXEC").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateLiqu, ColumnMetadata.named("DATE_LIQU").withIndex(7).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

