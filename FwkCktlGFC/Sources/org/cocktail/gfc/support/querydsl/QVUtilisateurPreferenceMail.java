package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVUtilisateurPreferenceMail is a Querydsl query type for QVUtilisateurPreferenceMail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVUtilisateurPreferenceMail extends com.mysema.query.sql.RelationalPathBase<QVUtilisateurPreferenceMail> {

    private static final long serialVersionUID = 782721408;

    public static final QVUtilisateurPreferenceMail vUtilisateurPreferenceMail = new QVUtilisateurPreferenceMail("V_UTILISATEUR_PREFERENCE_MAIL");

    public final SimplePath<Object> prefId = createSimple("prefId", Object.class);

    public final SimplePath<Object> upId = createSimple("upId", Object.class);

    public final SimplePath<Object> upValue = createSimple("upValue", Object.class);

    public final SimplePath<Object> utlOrdre = createSimple("utlOrdre", Object.class);

    public QVUtilisateurPreferenceMail(String variable) {
        super(QVUtilisateurPreferenceMail.class, forVariable(variable), "GFC", "V_UTILISATEUR_PREFERENCE_MAIL");
        addMetadata();
    }

    public QVUtilisateurPreferenceMail(String variable, String schema, String table) {
        super(QVUtilisateurPreferenceMail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVUtilisateurPreferenceMail(Path<? extends QVUtilisateurPreferenceMail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_UTILISATEUR_PREFERENCE_MAIL");
        addMetadata();
    }

    public QVUtilisateurPreferenceMail(PathMetadata<?> metadata) {
        super(QVUtilisateurPreferenceMail.class, metadata, "GFC", "V_UTILISATEUR_PREFERENCE_MAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(prefId, ColumnMetadata.named("PREF_ID").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(upId, ColumnMetadata.named("UP_ID").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(upValue, ColumnMetadata.named("UP_VALUE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

