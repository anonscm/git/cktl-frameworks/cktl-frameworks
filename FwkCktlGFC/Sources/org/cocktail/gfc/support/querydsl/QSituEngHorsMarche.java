package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSituEngHorsMarche is a Querydsl query type for QSituEngHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSituEngHorsMarche extends com.mysema.query.sql.RelationalPathBase<QSituEngHorsMarche> {

    private static final long serialVersionUID = -1927200592;

    public static final QSituEngHorsMarche situEngHorsMarche = new QSituEngHorsMarche("SITU_ENG_HORS_MARCHE");

    public final StringPath cmCode = createString("cmCode");

    public final StringPath cmLib = createString("cmLib");

    public final NumberPath<Long> cmOrdre = createNumber("cmOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> contr = createNumber("contr", java.math.BigDecimal.class);

    public final NumberPath<Integer> exer = createNumber("exer", Integer.class);

    public final NumberPath<Long> ht = createNumber("ht", Long.class);

    public final NumberPath<Long> pcontr = createNumber("pcontr", Long.class);

    public QSituEngHorsMarche(String variable) {
        super(QSituEngHorsMarche.class, forVariable(variable), "GFC", "SITU_ENG_HORS_MARCHE");
        addMetadata();
    }

    public QSituEngHorsMarche(String variable, String schema, String table) {
        super(QSituEngHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSituEngHorsMarche(Path<? extends QSituEngHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SITU_ENG_HORS_MARCHE");
        addMetadata();
    }

    public QSituEngHorsMarche(PathMetadata<?> metadata) {
        super(QSituEngHorsMarche.class, metadata, "GFC", "SITU_ENG_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cmCode, ColumnMetadata.named("CM_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(cmLib, ColumnMetadata.named("CM_LIB").withIndex(4).ofType(Types.VARCHAR).withSize(150).notNull());
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(contr, ColumnMetadata.named("CONTR").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(exer, ColumnMetadata.named("EXER").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(ht, ColumnMetadata.named("HT").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcontr, ColumnMetadata.named("PCONTR").withIndex(6).ofType(Types.DECIMAL).withSize(0));
    }

}

