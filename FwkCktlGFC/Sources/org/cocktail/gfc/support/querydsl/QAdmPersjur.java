package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmPersjur is a Querydsl query type for QAdmPersjur
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmPersjur extends com.mysema.query.sql.RelationalPathBase<QAdmPersjur> {

    private static final long serialVersionUID = 1209109896;

    public static final QAdmPersjur admPersjur = new QAdmPersjur("ADM_PERSJUR");

    public final StringPath pjCommentaire = createString("pjCommentaire");

    public final DateTimePath<java.sql.Timestamp> pjDateDebut = createDateTime("pjDateDebut", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> pjDateFin = createDateTime("pjDateFin", java.sql.Timestamp.class);

    public final NumberPath<Long> pjId = createNumber("pjId", Long.class);

    public final StringPath pjLibelle = createString("pjLibelle");

    public final NumberPath<Long> pjNiveau = createNumber("pjNiveau", Long.class);

    public final NumberPath<Long> pjPere = createNumber("pjPere", Long.class);

    public final NumberPath<Long> tpjId = createNumber("tpjId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmPersjur> persjurPk = createPrimaryKey(pjId);

    public final com.mysema.query.sql.ForeignKey<QAdmPersjur> admPersjurPjPereFk = createForeignKey(pjPere, "PJ_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypePersjur> admPersjurTpjIdFk = createForeignKey(tpjId, "TPJ_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmPersjurPersonne> _admPersjurPersonnePjIdFk = createInvForeignKey(pjId, "PJ_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmPersjur> _admPersjurPjPereFk = createInvForeignKey(pjId, "PJ_PERE");

    public final com.mysema.query.sql.ForeignKey<QAdmPrmEb> _admPrmEbPjIdFk = createInvForeignKey(pjId, "PJ_ID");

    public QAdmPersjur(String variable) {
        super(QAdmPersjur.class, forVariable(variable), "GFC", "ADM_PERSJUR");
        addMetadata();
    }

    public QAdmPersjur(String variable, String schema, String table) {
        super(QAdmPersjur.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmPersjur(Path<? extends QAdmPersjur> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_PERSJUR");
        addMetadata();
    }

    public QAdmPersjur(PathMetadata<?> metadata) {
        super(QAdmPersjur.class, metadata, "GFC", "ADM_PERSJUR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pjCommentaire, ColumnMetadata.named("PJ_COMMENTAIRE").withIndex(7).ofType(Types.VARCHAR).withSize(500));
        addMetadata(pjDateDebut, ColumnMetadata.named("PJ_DATE_DEBUT").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(pjDateFin, ColumnMetadata.named("PJ_DATE_FIN").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(pjId, ColumnMetadata.named("PJ_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pjLibelle, ColumnMetadata.named("PJ_LIBELLE").withIndex(6).ofType(Types.VARCHAR).withSize(50));
        addMetadata(pjNiveau, ColumnMetadata.named("PJ_NIVEAU").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pjPere, ColumnMetadata.named("PJ_PERE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tpjId, ColumnMetadata.named("TPJ_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

