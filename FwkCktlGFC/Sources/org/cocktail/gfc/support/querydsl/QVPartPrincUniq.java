package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVPartPrincUniq is a Querydsl query type for QVPartPrincUniq
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVPartPrincUniq extends com.mysema.query.sql.RelationalPathBase<QVPartPrincUniq> {

    private static final long serialVersionUID = -422376971;

    public static final QVPartPrincUniq vPartPrincUniq = new QVPartPrincUniq("V_PART_PRINC_UNIQ");

    public final NumberPath<Long> apOrdre = createNumber("apOrdre", Long.class);

    public final NumberPath<Long> avtOrdre = createNumber("avtOrdre", Long.class);

    public QVPartPrincUniq(String variable) {
        super(QVPartPrincUniq.class, forVariable(variable), "GFC", "V_PART_PRINC_UNIQ");
        addMetadata();
    }

    public QVPartPrincUniq(String variable, String schema, String table) {
        super(QVPartPrincUniq.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVPartPrincUniq(Path<? extends QVPartPrincUniq> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_PART_PRINC_UNIQ");
        addMetadata();
    }

    public QVPartPrincUniq(PathMetadata<?> metadata) {
        super(QVPartPrincUniq.class, metadata, "GFC", "V_PART_PRINC_UNIQ");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(apOrdre, ColumnMetadata.named("AP_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(avtOrdre, ColumnMetadata.named("AVT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

