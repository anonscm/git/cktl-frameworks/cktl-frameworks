package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecettePapierAdrClient is a Querydsl query type for QRecettePapierAdrClient
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecettePapierAdrClient extends com.mysema.query.sql.RelationalPathBase<QRecettePapierAdrClient> {

    private static final long serialVersionUID = -2126088844;

    public static final QRecettePapierAdrClient recettePapierAdrClient = new QRecettePapierAdrClient("RECETTE_PAPIER_ADR_CLIENT");

    public final NumberPath<Long> adrOrdre = createNumber("adrOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateCreation = createDateTime("dateCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateFin = createDateTime("dateFin", java.sql.Timestamp.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> rppadcId = createNumber("rppadcId", Long.class);

    public final NumberPath<Long> rppId = createNumber("rppId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRecettePapierAdrClient> recettePapierAdrClientPk = createPrimaryKey(rppadcId);

    public final com.mysema.query.sql.ForeignKey<QRecettePapier> rppAdrClientRppIdFk = createForeignKey(rppId, "RPP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdresse> rppAdrClientAdrOrdreFk = createForeignKey(adrOrdre, "ADR_ORDRE");

    public QRecettePapierAdrClient(String variable) {
        super(QRecettePapierAdrClient.class, forVariable(variable), "GFC", "RECETTE_PAPIER_ADR_CLIENT");
        addMetadata();
    }

    public QRecettePapierAdrClient(String variable, String schema, String table) {
        super(QRecettePapierAdrClient.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecettePapierAdrClient(Path<? extends QRecettePapierAdrClient> path) {
        super(path.getType(), path.getMetadata(), "GFC", "RECETTE_PAPIER_ADR_CLIENT");
        addMetadata();
    }

    public QRecettePapierAdrClient(PathMetadata<?> metadata) {
        super(QRecettePapierAdrClient.class, metadata, "GFC", "RECETTE_PAPIER_ADR_CLIENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrOrdre, ColumnMetadata.named("ADR_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dateCreation, ColumnMetadata.named("DATE_CREATION").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dateFin, ColumnMetadata.named("DATE_FIN").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rppadcId, ColumnMetadata.named("RPPADC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rppId, ColumnMetadata.named("RPP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

