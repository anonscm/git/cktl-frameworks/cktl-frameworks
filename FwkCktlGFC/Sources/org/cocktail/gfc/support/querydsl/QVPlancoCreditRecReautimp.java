package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVPlancoCreditRecReautimp is a Querydsl query type for QVPlancoCreditRecReautimp
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVPlancoCreditRecReautimp extends com.mysema.query.sql.RelationalPathBase<QVPlancoCreditRecReautimp> {

    private static final long serialVersionUID = 1739659468;

    public static final QVPlancoCreditRecReautimp vPlancoCreditRecReautimp = new QVPlancoCreditRecReautimp("V_PLANCO_CREDIT_REC_REAUTIMP");

    public final StringPath pccEtat = createString("pccEtat");

    public final NumberPath<Long> pccOrdre = createNumber("pccOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath pcoValidite = createString("pcoValidite");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QVPlancoCreditRecReautimp(String variable) {
        super(QVPlancoCreditRecReautimp.class, forVariable(variable), "GFC", "V_PLANCO_CREDIT_REC_REAUTIMP");
        addMetadata();
    }

    public QVPlancoCreditRecReautimp(String variable, String schema, String table) {
        super(QVPlancoCreditRecReautimp.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVPlancoCreditRecReautimp(Path<? extends QVPlancoCreditRecReautimp> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_PLANCO_CREDIT_REC_REAUTIMP");
        addMetadata();
    }

    public QVPlancoCreditRecReautimp(PathMetadata<?> metadata) {
        super(QVPlancoCreditRecReautimp.class, metadata, "GFC", "V_PLANCO_CREDIT_REC_REAUTIMP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pccEtat, ColumnMetadata.named("PCC_ETAT").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pccOrdre, ColumnMetadata.named("PCC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pcoValidite, ColumnMetadata.named("PCO_VALIDITE").withIndex(5).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

