package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeCtrlMarche is a Querydsl query type for QCommandeCtrlMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeCtrlMarche extends com.mysema.query.sql.RelationalPathBase<QCommandeCtrlMarche> {

    private static final long serialVersionUID = 1006670248;

    public static final QCommandeCtrlMarche commandeCtrlMarche = new QCommandeCtrlMarche("COMMANDE_CTRL_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> cbudId = createNumber("cbudId", Long.class);

    public final NumberPath<java.math.BigDecimal> cmarHtSaisie = createNumber("cmarHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> cmarId = createNumber("cmarId", Long.class);

    public final NumberPath<java.math.BigDecimal> cmarMontantBudgetaire = createNumber("cmarMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<Double> cmarPourcentage = createNumber("cmarPourcentage", Double.class);

    public final NumberPath<java.math.BigDecimal> cmarTtcSaisie = createNumber("cmarTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> cmarTvaSaisie = createNumber("cmarTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeCtrlMarche> commandeCtrlMarchePk = createPrimaryKey(cmarId);

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> commandeCtrlMarCbudIdFk = createForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QAttribution> commandeCtrlMarAttOrdreFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeCtrlMarExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QCommandeCtrlMarche(String variable) {
        super(QCommandeCtrlMarche.class, forVariable(variable), "GFC", "COMMANDE_CTRL_MARCHE");
        addMetadata();
    }

    public QCommandeCtrlMarche(String variable, String schema, String table) {
        super(QCommandeCtrlMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeCtrlMarche(Path<? extends QCommandeCtrlMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_CTRL_MARCHE");
        addMetadata();
    }

    public QCommandeCtrlMarche(PathMetadata<?> metadata) {
        super(QCommandeCtrlMarche.class, metadata, "GFC", "COMMANDE_CTRL_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cbudId, ColumnMetadata.named("CBUD_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cmarHtSaisie, ColumnMetadata.named("CMAR_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cmarId, ColumnMetadata.named("CMAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cmarMontantBudgetaire, ColumnMetadata.named("CMAR_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cmarPourcentage, ColumnMetadata.named("CMAR_POURCENTAGE").withIndex(6).ofType(Types.DECIMAL).withSize(15).withDigits(5));
        addMetadata(cmarTtcSaisie, ColumnMetadata.named("CMAR_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cmarTvaSaisie, ColumnMetadata.named("CMAR_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

