package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QLogUtils is a Querydsl query type for QLogUtils
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QLogUtils extends com.mysema.query.sql.RelationalPathBase<QLogUtils> {

    private static final long serialVersionUID = -1135884338;

    public static final QLogUtils logUtils = new QLogUtils("LOG_UTILS");

    public final DateTimePath<java.sql.Timestamp> logDate = createDateTime("logDate", java.sql.Timestamp.class);

    public final StringPath logDesc = createString("logDesc");

    public final NumberPath<Long> logId = createNumber("logId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QLogUtils> logUtilsPk = createPrimaryKey(logId);

    public QLogUtils(String variable) {
        super(QLogUtils.class, forVariable(variable), "GFC", "LOG_UTILS");
        addMetadata();
    }

    public QLogUtils(String variable, String schema, String table) {
        super(QLogUtils.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QLogUtils(Path<? extends QLogUtils> path) {
        super(path.getType(), path.getMetadata(), "GFC", "LOG_UTILS");
        addMetadata();
    }

    public QLogUtils(PathMetadata<?> metadata) {
        super(QLogUtils.class, metadata, "GFC", "LOG_UTILS");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(logDate, ColumnMetadata.named("LOG_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(logDesc, ColumnMetadata.named("LOG_DESC").withIndex(3).ofType(Types.CLOB).withSize(4000));
        addMetadata(logId, ColumnMetadata.named("LOG_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

