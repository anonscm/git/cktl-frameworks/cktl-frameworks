package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBbBudgetDepense is a Querydsl query type for QBbBudgetDepense
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBbBudgetDepense extends com.mysema.query.sql.RelationalPathBase<QBbBudgetDepense> {

    private static final long serialVersionUID = -271125820;

    public static final QBbBudgetDepense bbBudgetDepense = new QBbBudgetDepense("BB_BUDGET_DEPENSE");

    public final StringPath budget = createString("budget");

    public final NumberPath<Long> dep = createNumber("dep", Long.class);

    public final NumberPath<Long> eng = createNumber("eng", Long.class);

    public final NumberPath<Long> orv = createNumber("orv", Long.class);

    public QBbBudgetDepense(String variable) {
        super(QBbBudgetDepense.class, forVariable(variable), "GFC", "BB_BUDGET_DEPENSE");
        addMetadata();
    }

    public QBbBudgetDepense(String variable, String schema, String table) {
        super(QBbBudgetDepense.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBbBudgetDepense(Path<? extends QBbBudgetDepense> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BB_BUDGET_DEPENSE");
        addMetadata();
    }

    public QBbBudgetDepense(PathMetadata<?> metadata) {
        super(QBbBudgetDepense.class, metadata, "GFC", "BB_BUDGET_DEPENSE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(budget, ColumnMetadata.named("BUDGET").withIndex(1).ofType(Types.VARCHAR).withSize(11));
        addMetadata(dep, ColumnMetadata.named("DEP").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(eng, ColumnMetadata.named("ENG").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(orv, ColumnMetadata.named("ORV").withIndex(4).ofType(Types.DECIMAL).withSize(0));
    }

}

