package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCataloguePrestation is a Querydsl query type for QCataloguePrestation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCataloguePrestation extends com.mysema.query.sql.RelationalPathBase<QCataloguePrestation> {

    private static final long serialVersionUID = -1184276519;

    public static final QCataloguePrestation cataloguePrestation = new QCataloguePrestation("CATALOGUE_PRESTATION");

    public final DateTimePath<java.sql.Timestamp> catDateVote = createDateTime("catDateVote", java.sql.Timestamp.class);

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final NumberPath<Long> catNumero = createNumber("catNumero", Long.class);

    public final StringPath catPublieWeb = createString("catPublieWeb");

    public final NumberPath<Long> lolfIdRecette = createNumber("lolfIdRecette", Long.class);

    public final NumberPath<Long> orgIdRecette = createNumber("orgIdRecette", Long.class);

    public final StringPath pcoNumDepense = createString("pcoNumDepense");

    public final StringPath pcoNumRecette = createString("pcoNumRecette");

    public final com.mysema.query.sql.PrimaryKey<QCataloguePrestation> cataloguePrestationPk = createPrimaryKey(catId);

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecette> catalogueLolfIdRecetteFk = createForeignKey(lolfIdRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> catalogueOrgIdRecetteFk = createForeignKey(orgIdRecette, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogue> catalogueCatIdFk = createForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> cataloguePcoNumDepenseFk = createForeignKey(pcoNumDepense, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> cataloguePcoNumRecetteFk = createForeignKey(pcoNumRecette, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationCatIdFk = createInvForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QCataloguePublic> _capCatIdFk = createInvForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QCatalogueResponsable> _carCatIdFk = createInvForeignKey(catId, "CAT_ID");

    public QCataloguePrestation(String variable) {
        super(QCataloguePrestation.class, forVariable(variable), "GFC", "CATALOGUE_PRESTATION");
        addMetadata();
    }

    public QCataloguePrestation(String variable, String schema, String table) {
        super(QCataloguePrestation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCataloguePrestation(Path<? extends QCataloguePrestation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CATALOGUE_PRESTATION");
        addMetadata();
    }

    public QCataloguePrestation(PathMetadata<?> metadata) {
        super(QCataloguePrestation.class, metadata, "GFC", "CATALOGUE_PRESTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(catDateVote, ColumnMetadata.named("CAT_DATE_VOTE").withIndex(3).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catNumero, ColumnMetadata.named("CAT_NUMERO").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catPublieWeb, ColumnMetadata.named("CAT_PUBLIE_WEB").withIndex(4).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(lolfIdRecette, ColumnMetadata.named("LOLF_ID_RECETTE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(orgIdRecette, ColumnMetadata.named("ORG_ID_RECETTE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNumDepense, ColumnMetadata.named("PCO_NUM_DEPENSE").withIndex(7).ofType(Types.VARCHAR).withSize(20));
        addMetadata(pcoNumRecette, ColumnMetadata.named("PCO_NUM_RECETTE").withIndex(8).ofType(Types.VARCHAR).withSize(20));
    }

}

