package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVOrganActionDep is a Querydsl query type for QVOrganActionDep
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVOrganActionDep extends com.mysema.query.sql.RelationalPathBase<QVOrganActionDep> {

    private static final long serialVersionUID = -1609318947;

    public static final QVOrganActionDep vOrganActionDep = new QVOrganActionDep("V_ORGAN_ACTION_DEP");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> lolfId = createSimple("lolfId", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QVOrganActionDep(String variable) {
        super(QVOrganActionDep.class, forVariable(variable), "GFC", "V_ORGAN_ACTION_DEP");
        addMetadata();
    }

    public QVOrganActionDep(String variable, String schema, String table) {
        super(QVOrganActionDep.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVOrganActionDep(Path<? extends QVOrganActionDep> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ORGAN_ACTION_DEP");
        addMetadata();
    }

    public QVOrganActionDep(PathMetadata<?> metadata) {
        super(QVOrganActionDep.class, metadata, "GFC", "V_ORGAN_ACTION_DEP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(lolfId, ColumnMetadata.named("LOLF_ID").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
    }

}

