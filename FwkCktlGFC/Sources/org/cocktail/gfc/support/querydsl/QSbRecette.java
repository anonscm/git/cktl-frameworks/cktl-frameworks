package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSbRecette is a Querydsl query type for QSbRecette
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSbRecette extends com.mysema.query.sql.RelationalPathBase<QSbRecette> {

    private static final long serialVersionUID = 1477861056;

    public static final QSbRecette sbRecette = new QSbRecette("SB_RECETTE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> clientPersid = createNumber("clientPersid", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> exeOrdreSb = createNumber("exeOrdreSb", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> lolfId = createNumber("lolfId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> pvbuId = createNumber("pvbuId", Long.class);

    public final StringPath srBlocage = createString("srBlocage");

    public final DateTimePath<java.sql.Timestamp> srDateBloc = createDateTime("srDateBloc", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> srDatePrev = createDateTime("srDatePrev", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> srDateReal = createDateTime("srDateReal", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> srDateValid = createDateTime("srDateValid", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> srDateValidPart = createDateTime("srDateValidPart", java.sql.Timestamp.class);

    public final StringPath srLibelle = createString("srLibelle");

    public final NumberPath<java.math.BigDecimal> srMntValidHt = createNumber("srMntValidHt", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> srMontantHt = createNumber("srMontantHt", java.math.BigDecimal.class);

    public final StringPath srMotifBloc = createString("srMotifBloc");

    public final NumberPath<Long> srOrdre = createNumber("srOrdre", Long.class);

    public final NumberPath<Double> srPctContrib = createNumber("srPctContrib", Double.class);

    public final NumberPath<Double> srPctImpots = createNumber("srPctImpots", Double.class);

    public final NumberPath<java.math.BigDecimal> srPctValidHt = createNumber("srPctValidHt", java.math.BigDecimal.class);

    public final StringPath srValid = createString("srValid");

    public final StringPath srValidPart = createString("srValidPart");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> traOrdre = createNumber("traOrdre", Long.class);

    public final NumberPath<Long> tvaId = createNumber("tvaId", Long.class);

    public final NumberPath<Long> utlOrdreBloc = createNumber("utlOrdreBloc", Long.class);

    public final NumberPath<Long> utlOrdreValid = createNumber("utlOrdreValid", Long.class);

    public final NumberPath<Long> utlOrdreValidPart = createNumber("utlOrdreValidPart", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QSbRecette> sbRecettePk = createPrimaryKey(srOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecette> recLolfFk = createForeignKey(lolfId, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QAdmTva> sbRecTvaFk = createForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> recetteTcFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPersonne> recPersidClientFk = createForeignKey(clientPersid, "PERS_ID");

    public QSbRecette(String variable) {
        super(QSbRecette.class, forVariable(variable), "GFC", "SB_RECETTE");
        addMetadata();
    }

    public QSbRecette(String variable, String schema, String table) {
        super(QSbRecette.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSbRecette(Path<? extends QSbRecette> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SB_RECETTE");
        addMetadata();
    }

    public QSbRecette(PathMetadata<?> metadata) {
        super(QSbRecette.class, metadata, "GFC", "SB_RECETTE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(27).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clientPersid, ColumnMetadata.named("CLIENT_PERSID").withIndex(29).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(exeOrdreSb, ColumnMetadata.named("EXE_ORDRE_SB").withIndex(25).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lolfId, ColumnMetadata.named("LOLF_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(32));
        addMetadata(pvbuId, ColumnMetadata.named("PVBU_ID").withIndex(30).ofType(Types.DECIMAL).withSize(0));
        addMetadata(srBlocage, ColumnMetadata.named("SR_BLOCAGE").withIndex(10).ofType(Types.VARCHAR).withSize(1));
        addMetadata(srDateBloc, ColumnMetadata.named("SR_DATE_BLOC").withIndex(12).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(srDatePrev, ColumnMetadata.named("SR_DATE_PREV").withIndex(9).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(srDateReal, ColumnMetadata.named("SR_DATE_REAL").withIndex(21).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(srDateValid, ColumnMetadata.named("SR_DATE_VALID").withIndex(17).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(srDateValidPart, ColumnMetadata.named("SR_DATE_VALID_PART").withIndex(20).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(srLibelle, ColumnMetadata.named("SR_LIBELLE").withIndex(8).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(srMntValidHt, ColumnMetadata.named("SR_MNT_VALID_HT").withIndex(15).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(srMontantHt, ColumnMetadata.named("SR_MONTANT_HT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(srMotifBloc, ColumnMetadata.named("SR_MOTIF_BLOC").withIndex(13).ofType(Types.VARCHAR).withSize(256));
        addMetadata(srOrdre, ColumnMetadata.named("SR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(srPctContrib, ColumnMetadata.named("SR_PCT_CONTRIB").withIndex(22).ofType(Types.DECIMAL).withSize(5).withDigits(2));
        addMetadata(srPctImpots, ColumnMetadata.named("SR_PCT_IMPOTS").withIndex(23).ofType(Types.DECIMAL).withSize(5).withDigits(2));
        addMetadata(srPctValidHt, ColumnMetadata.named("SR_PCT_VALID_HT").withIndex(24).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(srValid, ColumnMetadata.named("SR_VALID").withIndex(14).ofType(Types.VARCHAR).withSize(1));
        addMetadata(srValidPart, ColumnMetadata.named("SR_VALID_PART").withIndex(18).ofType(Types.VARCHAR).withSize(1));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(26).ofType(Types.DECIMAL).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(28).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdreBloc, ColumnMetadata.named("UTL_ORDRE_BLOC").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdreValid, ColumnMetadata.named("UTL_ORDRE_VALID").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdreValidPart, ColumnMetadata.named("UTL_ORDRE_VALID_PART").withIndex(19).ofType(Types.DECIMAL).withSize(0));
    }

}

