package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCourrier is a Querydsl query type for QCourrier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCourrier extends com.mysema.query.sql.RelationalPathBase<QCourrier> {

    private static final long serialVersionUID = 786222254;

    public static final QCourrier courrier = new QCourrier("COURRIER");

    public final StringPath agtLogin = createString("agtLogin");

    public final StringPath couCommentaire = createString("couCommentaire");

    public final DateTimePath<java.sql.Timestamp> couDateCourrier = createDateTime("couDateCourrier", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> couDateEnreg = createDateTime("couDateEnreg", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> couDateReponseAttendue = createDateTime("couDateReponseAttendue", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> couDateReponseEffectuee = createDateTime("couDateReponseEffectuee", java.sql.Timestamp.class);

    public final StringPath couDepartArrivee = createString("couDepartArrivee");

    public final StringPath couExpediteur = createString("couExpediteur");

    public final StringPath couExpEtablissement = createString("couExpEtablissement");

    public final StringPath couExpVia = createString("couExpVia");

    public final NumberPath<Long> couIdent = createNumber("couIdent", Long.class);

    public final StringPath couLocalisation = createString("couLocalisation");

    public final StringPath couMotsClefs = createString("couMotsClefs");

    public final NumberPath<Long> couNombreRelance = createNumber("couNombreRelance", Long.class);

    public final NumberPath<Long> couNumero = createNumber("couNumero", Long.class);

    public final NumberPath<Long> couNumeroPere = createNumber("couNumeroPere", Long.class);

    public final StringPath couObjet = createString("couObjet");

    public final StringPath couReference = createString("couReference");

    public final NumberPath<Long> couTypeConsultation = createNumber("couTypeConsultation", Long.class);

    public final StringPath cStructure = createString("cStructure");

    public final NumberPath<Integer> persId = createNumber("persId", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QCourrier> courrier1Pk = createPrimaryKey(couNumero);

    public QCourrier(String variable) {
        super(QCourrier.class, forVariable(variable), "COURRIER", "COURRIER");
        addMetadata();
    }

    public QCourrier(String variable, String schema, String table) {
        super(QCourrier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCourrier(Path<? extends QCourrier> path) {
        super(path.getType(), path.getMetadata(), "COURRIER", "COURRIER");
        addMetadata();
    }

    public QCourrier(PathMetadata<?> metadata) {
        super(QCourrier.class, metadata, "COURRIER", "COURRIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(agtLogin, ColumnMetadata.named("AGT_LOGIN").withIndex(2).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(couCommentaire, ColumnMetadata.named("COU_COMMENTAIRE").withIndex(16).ofType(Types.VARCHAR).withSize(4000));
        addMetadata(couDateCourrier, ColumnMetadata.named("COU_DATE_COURRIER").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(couDateEnreg, ColumnMetadata.named("COU_DATE_ENREG").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(couDateReponseAttendue, ColumnMetadata.named("COU_DATE_REPONSE_ATTENDUE").withIndex(7).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(couDateReponseEffectuee, ColumnMetadata.named("COU_DATE_REPONSE_EFFECTUEE").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(couDepartArrivee, ColumnMetadata.named("COU_DEPART_ARRIVEE").withIndex(10).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(couExpediteur, ColumnMetadata.named("COU_EXPEDITEUR").withIndex(12).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(couExpEtablissement, ColumnMetadata.named("COU_EXP_ETABLISSEMENT").withIndex(14).ofType(Types.VARCHAR).withSize(120));
        addMetadata(couExpVia, ColumnMetadata.named("COU_EXP_VIA").withIndex(13).ofType(Types.VARCHAR).withSize(120));
        addMetadata(couIdent, ColumnMetadata.named("COU_IDENT").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(couLocalisation, ColumnMetadata.named("COU_LOCALISATION").withIndex(19).ofType(Types.VARCHAR).withSize(512));
        addMetadata(couMotsClefs, ColumnMetadata.named("COU_MOTS_CLEFS").withIndex(17).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(couNombreRelance, ColumnMetadata.named("COU_NOMBRE_RELANCE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(couNumero, ColumnMetadata.named("COU_NUMERO").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(couNumeroPere, ColumnMetadata.named("COU_NUMERO_PERE").withIndex(20).ofType(Types.DECIMAL).withSize(0));
        addMetadata(couObjet, ColumnMetadata.named("COU_OBJET").withIndex(15).ofType(Types.VARCHAR).withSize(256).notNull());
        addMetadata(couReference, ColumnMetadata.named("COU_REFERENCE").withIndex(11).ofType(Types.VARCHAR).withSize(30));
        addMetadata(couTypeConsultation, ColumnMetadata.named("COU_TYPE_CONSULTATION").withIndex(18).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cStructure, ColumnMetadata.named("C_STRUCTURE").withIndex(3).ofType(Types.VARCHAR).withSize(10));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(21).ofType(Types.DECIMAL).withSize(8));
    }

}

