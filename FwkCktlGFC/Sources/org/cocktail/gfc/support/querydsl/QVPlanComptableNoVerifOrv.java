package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVPlanComptableNoVerifOrv is a Querydsl query type for QVPlanComptableNoVerifOrv
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVPlanComptableNoVerifOrv extends com.mysema.query.sql.RelationalPathBase<QVPlanComptableNoVerifOrv> {

    private static final long serialVersionUID = -810835467;

    public static final QVPlanComptableNoVerifOrv vPlanComptableNoVerifOrv = new QVPlanComptableNoVerifOrv("V_PLAN_COMPTABLE_NO_VERIF_ORV");

    public final StringPath pcoNum = createString("pcoNum");

    public QVPlanComptableNoVerifOrv(String variable) {
        super(QVPlanComptableNoVerifOrv.class, forVariable(variable), "GFC", "V_PLAN_COMPTABLE_NO_VERIF_ORV");
        addMetadata();
    }

    public QVPlanComptableNoVerifOrv(String variable, String schema, String table) {
        super(QVPlanComptableNoVerifOrv.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVPlanComptableNoVerifOrv(Path<? extends QVPlanComptableNoVerifOrv> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_PLAN_COMPTABLE_NO_VERIF_ORV");
        addMetadata();
    }

    public QVPlanComptableNoVerifOrv(PathMetadata<?> metadata) {
        super(QVPlanComptableNoVerifOrv.class, metadata, "GFC", "V_PLAN_COMPTABLE_NO_VERIF_ORV");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(1).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

