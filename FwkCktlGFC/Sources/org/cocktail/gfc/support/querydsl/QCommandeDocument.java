package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeDocument is a Querydsl query type for QCommandeDocument
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeDocument extends com.mysema.query.sql.RelationalPathBase<QCommandeDocument> {

    private static final long serialVersionUID = 1266054326;

    public static final QCommandeDocument commandeDocument = new QCommandeDocument("COMMANDE_DOCUMENT");

    public final NumberPath<Long> cimpId = createNumber("cimpId", Long.class);

    public final DateTimePath<java.sql.Timestamp> comdDate = createDateTime("comdDate", java.sql.Timestamp.class);

    public final NumberPath<Long> comdId = createNumber("comdId", Long.class);

    public final StringPath comdUrl = createString("comdUrl");

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> couNumero = createNumber("couNumero", Long.class);

    public final NumberPath<Long> tcomId = createNumber("tcomId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeDocument> commandeDocumentPk = createPrimaryKey(comdId);

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeDocumentCommIdFk = createForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeImpression> commandeDocumentCimpIdFk = createForeignKey(cimpId, "CIMP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> commandeDocumentUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCourrier> commandeDocumentCouNumFk = createForeignKey(couNumero, "COU_NUMERO");

    public QCommandeDocument(String variable) {
        super(QCommandeDocument.class, forVariable(variable), "GFC", "COMMANDE_DOCUMENT");
        addMetadata();
    }

    public QCommandeDocument(String variable, String schema, String table) {
        super(QCommandeDocument.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeDocument(Path<? extends QCommandeDocument> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_DOCUMENT");
        addMetadata();
    }

    public QCommandeDocument(PathMetadata<?> metadata) {
        super(QCommandeDocument.class, metadata, "GFC", "COMMANDE_DOCUMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cimpId, ColumnMetadata.named("CIMP_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(comdDate, ColumnMetadata.named("COMD_DATE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(comdId, ColumnMetadata.named("COMD_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(comdUrl, ColumnMetadata.named("COMD_URL").withIndex(8).ofType(Types.VARCHAR).withSize(200));
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(couNumero, ColumnMetadata.named("COU_NUMERO").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tcomId, ColumnMetadata.named("TCOM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

