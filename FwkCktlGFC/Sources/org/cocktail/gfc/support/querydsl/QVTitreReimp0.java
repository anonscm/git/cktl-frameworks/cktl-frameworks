package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVTitreReimp0 is a Querydsl query type for QVTitreReimp0
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVTitreReimp0 extends com.mysema.query.sql.RelationalPathBase<QVTitreReimp0> {

    private static final long serialVersionUID = 1137176948;

    public static final QVTitreReimp0 vTitreReimp0 = new QVTitreReimp0("V_TITRE_REIMP_0");

    public final SimplePath<Object> borId = createSimple("borId", Object.class);

    public final SimplePath<Object> ecdMontant = createSimple("ecdMontant", Object.class);

    public final SimplePath<Object> ecrDate = createSimple("ecrDate", Object.class);

    public final SimplePath<Object> ecrDateSaisie = createSimple("ecrDateSaisie", Object.class);

    public final SimplePath<Object> ecrOrdre = createSimple("ecrOrdre", Object.class);

    public final SimplePath<Object> ecrSacd = createSimple("ecrSacd", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> fouOrdre = createSimple("fouOrdre", Object.class);

    public final SimplePath<Object> gesCode = createSimple("gesCode", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> recDebiteur = createSimple("recDebiteur", Object.class);

    public final SimplePath<Object> recInterne = createSimple("recInterne", Object.class);

    public final SimplePath<Object> tboOrdre = createSimple("tboOrdre", Object.class);

    public final SimplePath<Object> tdeOrigine = createSimple("tdeOrigine", Object.class);

    public final SimplePath<Object> titEtat = createSimple("titEtat", Object.class);

    public final SimplePath<Object> titId = createSimple("titId", Object.class);

    public final SimplePath<Object> titLib = createSimple("titLib", Object.class);

    public final SimplePath<Object> titMont = createSimple("titMont", Object.class);

    public final SimplePath<Object> titNum = createSimple("titNum", Object.class);

    public final SimplePath<Object> titTtc = createSimple("titTtc", Object.class);

    public final SimplePath<Object> titTva = createSimple("titTva", Object.class);

    public QVTitreReimp0(String variable) {
        super(QVTitreReimp0.class, forVariable(variable), "GFC", "V_TITRE_REIMP_0");
        addMetadata();
    }

    public QVTitreReimp0(String variable, String schema, String table) {
        super(QVTitreReimp0.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVTitreReimp0(Path<? extends QVTitreReimp0> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_TITRE_REIMP_0");
        addMetadata();
    }

    public QVTitreReimp0(PathMetadata<?> metadata) {
        super(QVTitreReimp0.class, metadata, "GFC", "V_TITRE_REIMP_0");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(ecdMontant, ColumnMetadata.named("ECD_MONTANT").withIndex(21).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrDate, ColumnMetadata.named("ECR_DATE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrDateSaisie, ColumnMetadata.named("ECR_DATE_SAISIE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrOrdre, ColumnMetadata.named("ECR_ORDRE").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrSacd, ColumnMetadata.named("ECR_SACD").withIndex(19).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(recDebiteur, ColumnMetadata.named("REC_DEBITEUR").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(recInterne, ColumnMetadata.named("REC_INTERNE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(tdeOrigine, ColumnMetadata.named("TDE_ORIGINE").withIndex(20).ofType(Types.OTHER).withSize(0));
        addMetadata(titEtat, ColumnMetadata.named("TIT_ETAT").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(titLib, ColumnMetadata.named("TIT_LIB").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(titMont, ColumnMetadata.named("TIT_MONT").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(titNum, ColumnMetadata.named("TIT_NUM").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(titTtc, ColumnMetadata.named("TIT_TTC").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(titTva, ColumnMetadata.named("TIT_TVA").withIndex(12).ofType(Types.OTHER).withSize(0));
    }

}

