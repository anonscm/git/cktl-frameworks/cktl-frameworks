package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmTypeSortie is a Querydsl query type for QImmTypeSortie
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmTypeSortie extends com.mysema.query.sql.RelationalPathBase<QImmTypeSortie> {

    private static final long serialVersionUID = -158416804;

    public static final QImmTypeSortie immTypeSortie = new QImmTypeSortie("IMM_TYPE_SORTIE");

    public final NumberPath<Long> tysoId = createNumber("tysoId", Long.class);

    public final StringPath tysoLibelle = createString("tysoLibelle");

    public final com.mysema.query.sql.PrimaryKey<QImmTypeSortie> immTypeSortiePk = createPrimaryKey(tysoId);

    public final com.mysema.query.sql.ForeignKey<QImmInventComptSortie> _comptSortieTysoIdFk = createInvForeignKey(tysoId, "TYSO_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventSortie> _sortieTysoIdFk = createInvForeignKey(tysoId, "TYSO_ID");

    public QImmTypeSortie(String variable) {
        super(QImmTypeSortie.class, forVariable(variable), "GFC", "IMM_TYPE_SORTIE");
        addMetadata();
    }

    public QImmTypeSortie(String variable, String schema, String table) {
        super(QImmTypeSortie.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmTypeSortie(Path<? extends QImmTypeSortie> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_TYPE_SORTIE");
        addMetadata();
    }

    public QImmTypeSortie(PathMetadata<?> metadata) {
        super(QImmTypeSortie.class, metadata, "GFC", "IMM_TYPE_SORTIE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tysoId, ColumnMetadata.named("TYSO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tysoLibelle, ColumnMetadata.named("TYSO_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

