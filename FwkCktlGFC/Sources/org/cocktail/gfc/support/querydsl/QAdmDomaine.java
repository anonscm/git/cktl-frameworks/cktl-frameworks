package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmDomaine is a Querydsl query type for QAdmDomaine
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmDomaine extends com.mysema.query.sql.RelationalPathBase<QAdmDomaine> {

    private static final long serialVersionUID = -569863208;

    public static final QAdmDomaine admDomaine = new QAdmDomaine("ADM_DOMAINE");

    public final NumberPath<Long> domId = createNumber("domId", Long.class);

    public final StringPath domLibelle = createString("domLibelle");

    public final com.mysema.query.sql.PrimaryKey<QAdmDomaine> domainePk = createPrimaryKey(domId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplication> _admTyapDomIdFk = createInvForeignKey(domId, "DOM_ID");

    public QAdmDomaine(String variable) {
        super(QAdmDomaine.class, forVariable(variable), "GFC", "ADM_DOMAINE");
        addMetadata();
    }

    public QAdmDomaine(String variable, String schema, String table) {
        super(QAdmDomaine.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmDomaine(Path<? extends QAdmDomaine> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_DOMAINE");
        addMetadata();
    }

    public QAdmDomaine(PathMetadata<?> metadata) {
        super(QAdmDomaine.class, metadata, "GFC", "ADM_DOMAINE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(domId, ColumnMetadata.named("DOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(domLibelle, ColumnMetadata.named("DOM_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

