package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypeEb is a Querydsl query type for QAdmTypeEb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypeEb extends com.mysema.query.sql.RelationalPathBase<QAdmTypeEb> {

    private static final long serialVersionUID = 864653024;

    public static final QAdmTypeEb admTypeEb = new QAdmTypeEb("ADM_TYPE_EB");

    public final NumberPath<Long> tyorId = createNumber("tyorId", Long.class);

    public final StringPath tyorLibelle = createString("tyorLibelle");

    public final com.mysema.query.sql.PrimaryKey<QAdmTypeEb> sysC0077219 = createPrimaryKey(tyorId);

    public final com.mysema.query.sql.ForeignKey<QAdmEb> _admEbTyorIdFk = createInvForeignKey(tyorId, "TYOR_ID");

    public QAdmTypeEb(String variable) {
        super(QAdmTypeEb.class, forVariable(variable), "GFC", "ADM_TYPE_EB");
        addMetadata();
    }

    public QAdmTypeEb(String variable, String schema, String table) {
        super(QAdmTypeEb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypeEb(Path<? extends QAdmTypeEb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_EB");
        addMetadata();
    }

    public QAdmTypeEb(PathMetadata<?> metadata) {
        super(QAdmTypeEb.class, metadata, "GFC", "ADM_TYPE_EB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tyorId, ColumnMetadata.named("TYOR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyorLibelle, ColumnMetadata.named("TYOR_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

