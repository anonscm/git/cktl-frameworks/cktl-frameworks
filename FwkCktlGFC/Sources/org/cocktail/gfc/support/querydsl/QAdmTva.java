package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTva is a Querydsl query type for QAdmTva
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTva extends com.mysema.query.sql.RelationalPathBase<QAdmTva> {

    private static final long serialVersionUID = -2024405674;

    public static final QAdmTva admTva = new QAdmTva("ADM_TVA");

    public final NumberPath<Long> tvaId = createNumber("tvaId", Long.class);

    public final NumberPath<java.math.BigDecimal> tvaTaux = createNumber("tvaTaux", java.math.BigDecimal.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmTva> sysC0075730 = createPrimaryKey(tvaId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admTvaTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapierLigne> _fligTvaIdInitialFk = createInvForeignKey(tvaId, "TVA_ID_INITIAL");

    public final com.mysema.query.sql.ForeignKey<QPrestationLigne> _prligTvaIdFk = createInvForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QDepArticle> _depArticleTvaIdFk = createInvForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QSbDepense> _sbDepTvaFk = createInvForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogueArticle> _catCatalogueArticleTvaFk = createInvForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QSbRecette> _sbRecTvaFk = createInvForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapierLigne> _fligTvaIdFk = createInvForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestationLigne> _prligTvaIdInitialFk = createInvForeignKey(tvaId, "TVA_ID_INITIAL");

    public QAdmTva(String variable) {
        super(QAdmTva.class, forVariable(variable), "GFC", "ADM_TVA");
        addMetadata();
    }

    public QAdmTva(String variable, String schema, String table) {
        super(QAdmTva.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTva(Path<? extends QAdmTva> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TVA");
        addMetadata();
    }

    public QAdmTva(PathMetadata<?> metadata) {
        super(QAdmTva.class, metadata, "GFC", "ADM_TVA");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tvaTaux, ColumnMetadata.named("TVA_TAUX").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

