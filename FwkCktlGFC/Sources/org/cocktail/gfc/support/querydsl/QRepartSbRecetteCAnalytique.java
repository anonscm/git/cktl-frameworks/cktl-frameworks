package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRepartSbRecetteCAnalytique is a Querydsl query type for QRepartSbRecetteCAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRepartSbRecetteCAnalytique extends com.mysema.query.sql.RelationalPathBase<QRepartSbRecetteCAnalytique> {

    private static final long serialVersionUID = -491226796;

    public static final QRepartSbRecetteCAnalytique repartSbRecetteCAnalytique = new QRepartSbRecetteCAnalytique("REPART_SB_RECETTE_C_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<java.math.BigDecimal> montantHt = createNumber("montantHt", java.math.BigDecimal.class);

    public final NumberPath<Long> srOrdre = createNumber("srOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRepartSbRecetteCAnalytique> repartSbRecetteCAnalytiPk = createPrimaryKey(canId, srOrdre);

    public QRepartSbRecetteCAnalytique(String variable) {
        super(QRepartSbRecetteCAnalytique.class, forVariable(variable), "GFC", "REPART_SB_RECETTE_C_ANALYTIQUE");
        addMetadata();
    }

    public QRepartSbRecetteCAnalytique(String variable, String schema, String table) {
        super(QRepartSbRecetteCAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRepartSbRecetteCAnalytique(Path<? extends QRepartSbRecetteCAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REPART_SB_RECETTE_C_ANALYTIQUE");
        addMetadata();
    }

    public QRepartSbRecetteCAnalytique(PathMetadata<?> metadata) {
        super(QRepartSbRecetteCAnalytique.class, metadata, "GFC", "REPART_SB_RECETTE_C_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(montantHt, ColumnMetadata.named("MONTANT_HT").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(srOrdre, ColumnMetadata.named("SR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

