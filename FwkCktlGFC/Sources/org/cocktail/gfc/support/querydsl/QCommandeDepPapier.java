package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeDepPapier is a Querydsl query type for QCommandeDepPapier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeDepPapier extends com.mysema.query.sql.RelationalPathBase<QCommandeDepPapier> {

    private static final long serialVersionUID = -2072648053;

    public static final QCommandeDepPapier commandeDepPapier = new QCommandeDepPapier("COMMANDE_DEP_PAPIER");

    public final NumberPath<Long> cdpId = createNumber("cdpId", Long.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeDepPapier> commandeDepPapierPk = createPrimaryKey(cdpId);

    public final com.mysema.query.sql.ForeignKey<QDepensePapier> commandeDepPapierDppIdFk = createForeignKey(dppId, "DPP_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeDepPapierCommIdFk = createForeignKey(commId, "COMM_ID");

    public QCommandeDepPapier(String variable) {
        super(QCommandeDepPapier.class, forVariable(variable), "GFC", "COMMANDE_DEP_PAPIER");
        addMetadata();
    }

    public QCommandeDepPapier(String variable, String schema, String table) {
        super(QCommandeDepPapier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeDepPapier(Path<? extends QCommandeDepPapier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_DEP_PAPIER");
        addMetadata();
    }

    public QCommandeDepPapier(PathMetadata<?> metadata) {
        super(QCommandeDepPapier.class, metadata, "GFC", "COMMANDE_DEP_PAPIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cdpId, ColumnMetadata.named("CDP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

