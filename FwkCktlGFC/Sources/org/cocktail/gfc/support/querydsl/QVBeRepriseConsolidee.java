package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVBeRepriseConsolidee is a Querydsl query type for QVBeRepriseConsolidee
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVBeRepriseConsolidee extends com.mysema.query.sql.RelationalPathBase<QVBeRepriseConsolidee> {

    private static final long serialVersionUID = -1565114761;

    public static final QVBeRepriseConsolidee vBeRepriseConsolidee = new QVBeRepriseConsolidee("V_BE_REPRISE_CONSOLIDEE");

    public final NumberPath<Long> comOrdre = createNumber("comOrdre", Long.class);

    public final StringPath compteBe = createString("compteBe");

    public final StringPath compteBeLibelle = createString("compteBeLibelle");

    public final NumberPath<Long> credits = createNumber("credits", Long.class);

    public final NumberPath<Long> debits = createNumber("debits", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final StringPath pcoLibelle = createString("pcoLibelle");

    public final StringPath pcoNum = createString("pcoNum");

    public QVBeRepriseConsolidee(String variable) {
        super(QVBeRepriseConsolidee.class, forVariable(variable), "GFC", "V_BE_REPRISE_CONSOLIDEE");
        addMetadata();
    }

    public QVBeRepriseConsolidee(String variable, String schema, String table) {
        super(QVBeRepriseConsolidee.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVBeRepriseConsolidee(Path<? extends QVBeRepriseConsolidee> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_BE_REPRISE_CONSOLIDEE");
        addMetadata();
    }

    public QVBeRepriseConsolidee(PathMetadata<?> metadata) {
        super(QVBeRepriseConsolidee.class, metadata, "GFC", "V_BE_REPRISE_CONSOLIDEE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comOrdre, ColumnMetadata.named("COM_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(compteBe, ColumnMetadata.named("COMPTE_BE").withIndex(6).ofType(Types.VARCHAR).withSize(20));
        addMetadata(compteBeLibelle, ColumnMetadata.named("COMPTE_BE_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(200));
        addMetadata(credits, ColumnMetadata.named("CREDITS").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(debits, ColumnMetadata.named("DEBITS").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(pcoLibelle, ColumnMetadata.named("PCO_LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

