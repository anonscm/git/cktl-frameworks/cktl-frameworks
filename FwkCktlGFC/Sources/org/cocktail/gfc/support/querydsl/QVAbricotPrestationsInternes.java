package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAbricotPrestationsInternes is a Querydsl query type for QVAbricotPrestationsInternes
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAbricotPrestationsInternes extends com.mysema.query.sql.RelationalPathBase<QVAbricotPrestationsInternes> {

    private static final long serialVersionUID = 502757287;

    public static final QVAbricotPrestationsInternes vAbricotPrestationsInternes = new QVAbricotPrestationsInternes("V_ABRICOT_PRESTATIONS_INTERNES");

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Long> facId = createNumber("facId", Long.class);

    public final NumberPath<Long> pdrId = createNumber("pdrId", Long.class);

    public final NumberPath<Long> pefId = createNumber("pefId", Long.class);

    public final NumberPath<Long> prestId = createNumber("prestId", Long.class);

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public QVAbricotPrestationsInternes(String variable) {
        super(QVAbricotPrestationsInternes.class, forVariable(variable), "GFC", "V_ABRICOT_PRESTATIONS_INTERNES");
        addMetadata();
    }

    public QVAbricotPrestationsInternes(String variable, String schema, String table) {
        super(QVAbricotPrestationsInternes.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAbricotPrestationsInternes(Path<? extends QVAbricotPrestationsInternes> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ABRICOT_PRESTATIONS_INTERNES");
        addMetadata();
    }

    public QVAbricotPrestationsInternes(PathMetadata<?> metadata) {
        super(QVAbricotPrestationsInternes.class, metadata, "GFC", "V_ABRICOT_PRESTATIONS_INTERNES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(facId, ColumnMetadata.named("FAC_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdrId, ColumnMetadata.named("PDR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pefId, ColumnMetadata.named("PEF_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prestId, ColumnMetadata.named("PREST_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
    }

}

