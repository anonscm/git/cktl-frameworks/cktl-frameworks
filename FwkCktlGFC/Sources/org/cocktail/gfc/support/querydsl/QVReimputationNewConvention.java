package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVReimputationNewConvention is a Querydsl query type for QVReimputationNewConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVReimputationNewConvention extends com.mysema.query.sql.RelationalPathBase<QVReimputationNewConvention> {

    private static final long serialVersionUID = 1184701759;

    public static final QVReimputationNewConvention vReimputationNewConvention = new QVReimputationNewConvention("V_REIMPUTATION_NEW_CONVENTION");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> recoHtSaisie = createNumber("recoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> recoId = createNumber("recoId", Long.class);

    public final NumberPath<java.math.BigDecimal> recoMontantBudgetaire = createNumber("recoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> recoTtcSaisie = createNumber("recoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> recoTvaSaisie = createNumber("recoTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public QVReimputationNewConvention(String variable) {
        super(QVReimputationNewConvention.class, forVariable(variable), "GFC", "V_REIMPUTATION_NEW_CONVENTION");
        addMetadata();
    }

    public QVReimputationNewConvention(String variable, String schema, String table) {
        super(QVReimputationNewConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVReimputationNewConvention(Path<? extends QVReimputationNewConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_REIMPUTATION_NEW_CONVENTION");
        addMetadata();
    }

    public QVReimputationNewConvention(PathMetadata<?> metadata) {
        super(QVReimputationNewConvention.class, metadata, "GFC", "V_REIMPUTATION_NEW_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(recoHtSaisie, ColumnMetadata.named("RECO_HT_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(recoId, ColumnMetadata.named("RECO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(recoMontantBudgetaire, ColumnMetadata.named("RECO_MONTANT_BUDGETAIRE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(recoTtcSaisie, ColumnMetadata.named("RECO_TTC_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(recoTvaSaisie, ColumnMetadata.named("RECO_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

