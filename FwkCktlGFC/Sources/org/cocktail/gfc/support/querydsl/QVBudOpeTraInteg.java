package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVBudOpeTraInteg is a Querydsl query type for QVBudOpeTraInteg
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVBudOpeTraInteg extends com.mysema.query.sql.RelationalPathBase<QVBudOpeTraInteg> {

    private static final long serialVersionUID = -1767445642;

    public static final QVBudOpeTraInteg vBudOpeTraInteg = new QVBudOpeTraInteg("V_BUD_OPE_TRA_INTEG");

    public final NumberPath<Long> idBudBudget = createNumber("idBudBudget", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final NumberPath<Long> idOpeTrancheBud = createNumber("idOpeTrancheBud", Long.class);

    public QVBudOpeTraInteg(String variable) {
        super(QVBudOpeTraInteg.class, forVariable(variable), "GFC", "V_BUD_OPE_TRA_INTEG");
        addMetadata();
    }

    public QVBudOpeTraInteg(String variable, String schema, String table) {
        super(QVBudOpeTraInteg.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVBudOpeTraInteg(Path<? extends QVBudOpeTraInteg> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_BUD_OPE_TRA_INTEG");
        addMetadata();
    }

    public QVBudOpeTraInteg(PathMetadata<?> metadata) {
        super(QVBudOpeTraInteg.class, metadata, "GFC", "V_BUD_OPE_TRA_INTEG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idBudBudget, ColumnMetadata.named("ID_BUD_BUDGET").withIndex(3).ofType(Types.DECIMAL).withSize(38));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.DECIMAL).withSize(38));
        addMetadata(idOpeTrancheBud, ColumnMetadata.named("ID_OPE_TRANCHE_BUD").withIndex(1).ofType(Types.DECIMAL).withSize(38));
    }

}

