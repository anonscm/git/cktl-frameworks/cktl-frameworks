package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVTrancheGpConvention is a Querydsl query type for QVTrancheGpConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVTrancheGpConvention extends com.mysema.query.sql.RelationalPathBase<QVTrancheGpConvention> {

    private static final long serialVersionUID = 1080215962;

    public static final QVTrancheGpConvention vTrancheGpConvention = new QVTrancheGpConvention("V_TRANCHE_GP_CONVENTION");

    public final SimplePath<Object> convIndex = createSimple("convIndex", Object.class);

    public final SimplePath<Object> convOrdre = createSimple("convOrdre", Object.class);

    public final SimplePath<Object> convPremierExercice = createSimple("convPremierExercice", Object.class);

    public final SimplePath<Object> convReferenceExterne = createSimple("convReferenceExterne", Object.class);

    public final SimplePath<Object> cStructureCr = createSimple("cStructureCr", Object.class);

    public final SimplePath<Object> cStructurePartPrinc = createSimple("cStructurePartPrinc", Object.class);

    public final SimplePath<Object> exercice = createSimple("exercice", Object.class);

    public final SimplePath<Object> groupe = createSimple("groupe", Object.class);

    public final SimplePath<Object> libelle = createSimple("libelle", Object.class);

    public final SimplePath<Object> libelleCourt = createSimple("libelleCourt", Object.class);

    public final SimplePath<Object> noIndividuRespAdmin = createSimple("noIndividuRespAdmin", Object.class);

    public final SimplePath<Object> valide = createSimple("valide", Object.class);

    public QVTrancheGpConvention(String variable) {
        super(QVTrancheGpConvention.class, forVariable(variable), "GFC", "V_TRANCHE_GP_CONVENTION");
        addMetadata();
    }

    public QVTrancheGpConvention(String variable, String schema, String table) {
        super(QVTrancheGpConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVTrancheGpConvention(Path<? extends QVTrancheGpConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_TRANCHE_GP_CONVENTION");
        addMetadata();
    }

    public QVTrancheGpConvention(PathMetadata<?> metadata) {
        super(QVTrancheGpConvention.class, metadata, "GFC", "V_TRANCHE_GP_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convIndex, ColumnMetadata.named("CONV_INDEX").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(convPremierExercice, ColumnMetadata.named("CONV_PREMIER_EXERCICE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(convReferenceExterne, ColumnMetadata.named("CONV_REFERENCE_EXTERNE").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(cStructureCr, ColumnMetadata.named("C_STRUCTURE_CR").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(cStructurePartPrinc, ColumnMetadata.named("C_STRUCTURE_PART_PRINC").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(groupe, ColumnMetadata.named("GROUPE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(libelleCourt, ColumnMetadata.named("LIBELLE_COURT").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(noIndividuRespAdmin, ColumnMetadata.named("NO_INDIVIDU_RESP_ADMIN").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(valide, ColumnMetadata.named("VALIDE").withIndex(5).ofType(Types.OTHER).withSize(0));
    }

}

