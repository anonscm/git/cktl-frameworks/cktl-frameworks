package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeTypeOperation is a Querydsl query type for QOpeTypeOperation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeTypeOperation extends com.mysema.query.sql.RelationalPathBase<QOpeTypeOperation> {

    private static final long serialVersionUID = -646012406;

    public static final QOpeTypeOperation opeTypeOperation = new QOpeTypeOperation("OPE_TYPE_OPERATION");

    public final StringPath codeTypeOperation = createString("codeTypeOperation");

    public final NumberPath<Long> idOpeCategOperation = createNumber("idOpeCategOperation", Long.class);

    public final NumberPath<Long> idOpeTypeOperation = createNumber("idOpeTypeOperation", Long.class);

    public final NumberPath<Integer> isInclusTabxBudgetaires = createNumber("isInclusTabxBudgetaires", Integer.class);

    public final StringPath llTypeOperation = createString("llTypeOperation");

    public final com.mysema.query.sql.PrimaryKey<QOpeTypeOperation> opeTypeOperationPk = createPrimaryKey(idOpeTypeOperation);

    public final com.mysema.query.sql.ForeignKey<QOpeCategOperation> categOperationTypeOpeFk = createForeignKey(idOpeCategOperation, "ID_OPE_CATEG_OPERATION");

    public final com.mysema.query.sql.ForeignKey<QOpeOperation> _opeTypeOperationOpeFk = createInvForeignKey(idOpeTypeOperation, "ID_OPE_TYPE_OPERATION");

    public QOpeTypeOperation(String variable) {
        super(QOpeTypeOperation.class, forVariable(variable), "GFC", "OPE_TYPE_OPERATION");
        addMetadata();
    }

    public QOpeTypeOperation(String variable, String schema, String table) {
        super(QOpeTypeOperation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeTypeOperation(Path<? extends QOpeTypeOperation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_TYPE_OPERATION");
        addMetadata();
    }

    public QOpeTypeOperation(PathMetadata<?> metadata) {
        super(QOpeTypeOperation.class, metadata, "GFC", "OPE_TYPE_OPERATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(codeTypeOperation, ColumnMetadata.named("CODE_TYPE_OPERATION").withIndex(2).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(idOpeCategOperation, ColumnMetadata.named("ID_OPE_CATEG_OPERATION").withIndex(4).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeTypeOperation, ColumnMetadata.named("ID_OPE_TYPE_OPERATION").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(isInclusTabxBudgetaires, ColumnMetadata.named("IS_INCLUS_TABX_BUDGETAIRES").withIndex(5).ofType(Types.DECIMAL).withSize(1));
        addMetadata(llTypeOperation, ColumnMetadata.named("LL_TYPE_OPERATION").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
    }

}

