package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCatArticle is a Querydsl query type for QCatArticle
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCatArticle extends com.mysema.query.sql.RelationalPathBase<QCatArticle> {

    private static final long serialVersionUID = 1204317185;

    public static final QCatArticle catArticle = new QCatArticle("CAT_ARTICLE");

    public final NumberPath<Long> artId = createNumber("artId", Long.class);

    public final NumberPath<Long> artIdPere = createNumber("artIdPere", Long.class);

    public final StringPath artLibelle = createString("artLibelle");

    public final NumberPath<Long> cmOrdre = createNumber("cmOrdre", Long.class);

    public final NumberPath<Long> tyarId = createNumber("tyarId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCatArticle> catArticlePk = createPrimaryKey(artId);

    public final com.mysema.query.sql.ForeignKey<QCatArticle> catArticleArtIdFk = createForeignKey(artIdPere, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QCodeMarche> catArticleCmOrdreFk = createForeignKey(cmOrdre, "CM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCatTypeArticle> catArticleTyarIdFk = createForeignKey(tyarId, "TYAR_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogueArticle> _catCatalogueArticleArtFk = createInvForeignKey(artId, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QCatArticleMarche> _catArticleMarcheArtIdFk = createInvForeignKey(artId, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QCatArticleLocalise> _catArticleLocaliseArticlFk = createInvForeignKey(artId, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QCatArticle> _catArticleArtIdFk = createInvForeignKey(artId, "ART_ID_PERE");

    public final com.mysema.query.sql.ForeignKey<QArticlePrestation> _articleArtIdFk = createInvForeignKey(artId, "ART_ID");

    public QCatArticle(String variable) {
        super(QCatArticle.class, forVariable(variable), "GFC", "CAT_ARTICLE");
        addMetadata();
    }

    public QCatArticle(String variable, String schema, String table) {
        super(QCatArticle.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCatArticle(Path<? extends QCatArticle> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CAT_ARTICLE");
        addMetadata();
    }

    public QCatArticle(PathMetadata<?> metadata) {
        super(QCatArticle.class, metadata, "GFC", "CAT_ARTICLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(artId, ColumnMetadata.named("ART_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(artIdPere, ColumnMetadata.named("ART_ID_PERE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(artLibelle, ColumnMetadata.named("ART_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tyarId, ColumnMetadata.named("TYAR_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

