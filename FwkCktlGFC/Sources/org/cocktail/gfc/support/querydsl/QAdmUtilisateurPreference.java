package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmUtilisateurPreference is a Querydsl query type for QAdmUtilisateurPreference
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmUtilisateurPreference extends com.mysema.query.sql.RelationalPathBase<QAdmUtilisateurPreference> {

    private static final long serialVersionUID = 2072840245;

    public static final QAdmUtilisateurPreference admUtilisateurPreference = new QAdmUtilisateurPreference("ADM_UTILISATEUR_PREFERENCE");

    public final NumberPath<Long> prefId = createNumber("prefId", Long.class);

    public final NumberPath<Long> upId = createNumber("upId", Long.class);

    public final StringPath upValue = createString("upValue");

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmUtilisateurPreference> sysC0077598 = createPrimaryKey(upId);

    public final com.mysema.query.sql.ForeignKey<QAdmPreference> admUpPrefIdFk = createForeignKey(prefId, "PREF_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> admUpUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public QAdmUtilisateurPreference(String variable) {
        super(QAdmUtilisateurPreference.class, forVariable(variable), "GFC", "ADM_UTILISATEUR_PREFERENCE");
        addMetadata();
    }

    public QAdmUtilisateurPreference(String variable, String schema, String table) {
        super(QAdmUtilisateurPreference.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmUtilisateurPreference(Path<? extends QAdmUtilisateurPreference> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_UTILISATEUR_PREFERENCE");
        addMetadata();
    }

    public QAdmUtilisateurPreference(PathMetadata<?> metadata) {
        super(QAdmUtilisateurPreference.class, metadata, "GFC", "ADM_UTILISATEUR_PREFERENCE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(prefId, ColumnMetadata.named("PREF_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(upId, ColumnMetadata.named("UP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(upValue, ColumnMetadata.named("UP_VALUE").withIndex(4).ofType(Types.VARCHAR).withSize(500));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

