package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVRecetteResteRecouvrer is a Querydsl query type for QVRecetteResteRecouvrer
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVRecetteResteRecouvrer extends com.mysema.query.sql.RelationalPathBase<QVRecetteResteRecouvrer> {

    private static final long serialVersionUID = -2022253751;

    public static final QVRecetteResteRecouvrer vRecetteResteRecouvrer = new QVRecetteResteRecouvrer("V_RECETTE_RESTE_RECOUVRER");

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final NumberPath<Long> resteRecouvrer = createNumber("resteRecouvrer", Long.class);

    public QVRecetteResteRecouvrer(String variable) {
        super(QVRecetteResteRecouvrer.class, forVariable(variable), "GFC", "V_RECETTE_RESTE_RECOUVRER");
        addMetadata();
    }

    public QVRecetteResteRecouvrer(String variable, String schema, String table) {
        super(QVRecetteResteRecouvrer.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVRecetteResteRecouvrer(Path<? extends QVRecetteResteRecouvrer> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_RECETTE_RESTE_RECOUVRER");
        addMetadata();
    }

    public QVRecetteResteRecouvrer(PathMetadata<?> metadata) {
        super(QVRecetteResteRecouvrer.class, metadata, "GFC", "V_RECETTE_RESTE_RECOUVRER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(resteRecouvrer, ColumnMetadata.named("RESTE_RECOUVRER").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

