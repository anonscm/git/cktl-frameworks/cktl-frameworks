package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmParametre is a Querydsl query type for QAdmParametre
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmParametre extends com.mysema.query.sql.RelationalPathBase<QAdmParametre> {

    private static final long serialVersionUID = -875859162;

    public static final QAdmParametre admParametre = new QAdmParametre("ADM_PARAMETRE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath parDescription = createString("parDescription");

    public final StringPath parKey = createString("parKey");

    public final NumberPath<Long> parNiveauModif = createNumber("parNiveauModif", Long.class);

    public final NumberPath<Long> parOrdre = createNumber("parOrdre", Long.class);

    public final StringPath parValue = createString("parValue");

    public final com.mysema.query.sql.PrimaryKey<QAdmParametre> sysC0077728 = createPrimaryKey(parOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admParNiveauModifFk = createForeignKey(parNiveauModif, "TYET_ID");

    public QAdmParametre(String variable) {
        super(QAdmParametre.class, forVariable(variable), "GFC", "ADM_PARAMETRE");
        addMetadata();
    }

    public QAdmParametre(String variable, String schema, String table) {
        super(QAdmParametre.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmParametre(Path<? extends QAdmParametre> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_PARAMETRE");
        addMetadata();
    }

    public QAdmParametre(PathMetadata<?> metadata) {
        super(QAdmParametre.class, metadata, "GFC", "ADM_PARAMETRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(parDescription, ColumnMetadata.named("PAR_DESCRIPTION").withIndex(5).ofType(Types.VARCHAR).withSize(300));
        addMetadata(parKey, ColumnMetadata.named("PAR_KEY").withIndex(3).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(parNiveauModif, ColumnMetadata.named("PAR_NIVEAU_MODIF").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(parOrdre, ColumnMetadata.named("PAR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(parValue, ColumnMetadata.named("PAR_VALUE").withIndex(4).ofType(Types.VARCHAR).withSize(200));
    }

}

