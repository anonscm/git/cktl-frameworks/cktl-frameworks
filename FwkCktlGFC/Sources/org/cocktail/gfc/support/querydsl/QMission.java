package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMission is a Querydsl query type for QMission
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QMission extends com.mysema.query.sql.RelationalPathBase<QMission> {

    private static final long serialVersionUID = -417325717;

    public static final QMission mission = new QMission("MISSION");

    public final StringPath cCorps = createString("cCorps");

    public final StringPath codPayeur = createString("codPayeur");

    public final NumberPath<Long> denQuotientRemb = createNumber("denQuotientRemb", Long.class);

    public final NumberPath<Long> exeOrdre = createNumber("exeOrdre", Long.class);

    public final NumberPath<Long> exeOrdreOrigine = createNumber("exeOrdreOrigine", Long.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> grmOrdre = createNumber("grmOrdre", Long.class);

    public final StringPath misCorps = createString("misCorps");

    public final DateTimePath<java.sql.Timestamp> misCreation = createDateTime("misCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> misDebut = createDateTime("misDebut", java.sql.Timestamp.class);

    public final StringPath misEtat = createString("misEtat");

    public final DateTimePath<java.sql.Timestamp> misFin = createDateTime("misFin", java.sql.Timestamp.class);

    public final StringPath misMotif = createString("misMotif");

    public final NumberPath<Long> misNumero = createNumber("misNumero", Long.class);

    public final StringPath misObservation = createString("misObservation");

    public final NumberPath<Long> misOrdre = createNumber("misOrdre", Long.class);

    public final StringPath misPayeur = createString("misPayeur");

    public final StringPath misResidence = createString("misResidence");

    public final NumberPath<Long> numQuotientRemb = createNumber("numQuotientRemb", Long.class);

    public final NumberPath<Long> titOrdre = createNumber("titOrdre", Long.class);

    public final NumberPath<Long> utlOrdreCreation = createNumber("utlOrdreCreation", Long.class);

    public final NumberPath<Long> utlOrdreModification = createNumber("utlOrdreModification", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QMission> sysC0020809 = createPrimaryKey(misOrdre);

    public QMission(String variable) {
        super(QMission.class, forVariable(variable), "JEFY_MISSION", "MISSION");
        addMetadata();
    }

    public QMission(String variable, String schema, String table) {
        super(QMission.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMission(Path<? extends QMission> path) {
        super(path.getType(), path.getMetadata(), "JEFY_MISSION", "MISSION");
        addMetadata();
    }

    public QMission(PathMetadata<?> metadata) {
        super(QMission.class, metadata, "JEFY_MISSION", "MISSION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cCorps, ColumnMetadata.named("C_CORPS").withIndex(10).ofType(Types.VARCHAR).withSize(4));
        addMetadata(codPayeur, ColumnMetadata.named("COD_PAYEUR").withIndex(15).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(denQuotientRemb, ColumnMetadata.named("DEN_QUOTIENT_REMB").withIndex(19).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdreOrigine, ColumnMetadata.named("EXE_ORDRE_ORIGINE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(grmOrdre, ColumnMetadata.named("GRM_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(misCorps, ColumnMetadata.named("MIS_CORPS").withIndex(11).ofType(Types.VARCHAR).withSize(200));
        addMetadata(misCreation, ColumnMetadata.named("MIS_CREATION").withIndex(22).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(misDebut, ColumnMetadata.named("MIS_DEBUT").withIndex(7).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(misEtat, ColumnMetadata.named("MIS_ETAT").withIndex(9).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(misFin, ColumnMetadata.named("MIS_FIN").withIndex(8).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(misMotif, ColumnMetadata.named("MIS_MOTIF").withIndex(13).ofType(Types.VARCHAR).withSize(400).notNull());
        addMetadata(misNumero, ColumnMetadata.named("MIS_NUMERO").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(misObservation, ColumnMetadata.named("MIS_OBSERVATION").withIndex(14).ofType(Types.VARCHAR).withSize(200));
        addMetadata(misOrdre, ColumnMetadata.named("MIS_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(misPayeur, ColumnMetadata.named("MIS_PAYEUR").withIndex(16).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(misResidence, ColumnMetadata.named("MIS_RESIDENCE").withIndex(17).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(numQuotientRemb, ColumnMetadata.named("NUM_QUOTIENT_REMB").withIndex(18).ofType(Types.DECIMAL).withSize(0));
        addMetadata(titOrdre, ColumnMetadata.named("TIT_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdreCreation, ColumnMetadata.named("UTL_ORDRE_CREATION").withIndex(20).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdreModification, ColumnMetadata.named("UTL_ORDRE_MODIFICATION").withIndex(21).ofType(Types.DECIMAL).withSize(0));
    }

}

