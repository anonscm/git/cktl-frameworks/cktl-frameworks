package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnRecBud1 is a Querydsl query type for QEpnRecBud1
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnRecBud1 extends com.mysema.query.sql.RelationalPathBase<QEpnRecBud1> {

    private static final long serialVersionUID = -584013394;

    public static final QEpnRecBud1 epnRecBud1 = new QEpnRecBud1("EPN_REC_BUD_1");

    public final StringPath epnrb1CodBud = createString("epnrb1CodBud");

    public final StringPath epnrb1CodNomen = createString("epnrb1CodNomen");

    public final StringPath epnrb1Date = createString("epnrb1Date");

    public final NumberPath<Integer> epnrb1Exercice = createNumber("epnrb1Exercice", Integer.class);

    public final StringPath epnrb1GesCode = createString("epnrb1GesCode");

    public final NumberPath<Long> epnrb1Identifiant = createNumber("epnrb1Identifiant", Long.class);

    public final NumberPath<Integer> epnrb1Nbenreg = createNumber("epnrb1Nbenreg", Integer.class);

    public final NumberPath<Integer> epnrb1Numero = createNumber("epnrb1Numero", Integer.class);

    public final StringPath epnrb1Rang = createString("epnrb1Rang");

    public final NumberPath<Integer> epnrb1Siren = createNumber("epnrb1Siren", Integer.class);

    public final NumberPath<Long> epnrb1Siret = createNumber("epnrb1Siret", Long.class);

    public final StringPath epnrb1Type = createString("epnrb1Type");

    public final StringPath epnrb1TypeDoc = createString("epnrb1TypeDoc");

    public final StringPath epnrbOrdre = createString("epnrbOrdre");

    public QEpnRecBud1(String variable) {
        super(QEpnRecBud1.class, forVariable(variable), "GFC", "EPN_REC_BUD_1");
        addMetadata();
    }

    public QEpnRecBud1(String variable, String schema, String table) {
        super(QEpnRecBud1.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnRecBud1(Path<? extends QEpnRecBud1> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_REC_BUD_1");
        addMetadata();
    }

    public QEpnRecBud1(PathMetadata<?> metadata) {
        super(QEpnRecBud1.class, metadata, "GFC", "EPN_REC_BUD_1");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epnrb1CodBud, ColumnMetadata.named("EPNRB1_COD_BUD").withIndex(8).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnrb1CodNomen, ColumnMetadata.named("EPNRB1_COD_NOMEN").withIndex(7).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnrb1Date, ColumnMetadata.named("EPNRB1_DATE").withIndex(11).ofType(Types.VARCHAR).withSize(8));
        addMetadata(epnrb1Exercice, ColumnMetadata.named("EPNRB1_EXERCICE").withIndex(9).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epnrb1GesCode, ColumnMetadata.named("EPNRB1_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epnrb1Identifiant, ColumnMetadata.named("EPNRB1_IDENTIFIANT").withIndex(5).ofType(Types.DECIMAL).withSize(10));
        addMetadata(epnrb1Nbenreg, ColumnMetadata.named("EPNRB1_NBENREG").withIndex(14).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnrb1Numero, ColumnMetadata.named("EPNRB1_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnrb1Rang, ColumnMetadata.named("EPNRB1_RANG").withIndex(10).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnrb1Siren, ColumnMetadata.named("EPNRB1_SIREN").withIndex(12).ofType(Types.DECIMAL).withSize(9));
        addMetadata(epnrb1Siret, ColumnMetadata.named("EPNRB1_SIRET").withIndex(13).ofType(Types.DECIMAL).withSize(14));
        addMetadata(epnrb1Type, ColumnMetadata.named("EPNRB1_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epnrb1TypeDoc, ColumnMetadata.named("EPNRB1_TYPE_DOC").withIndex(6).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnrbOrdre, ColumnMetadata.named("EPNRB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

