package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVHistCredPodEngBudget is a Querydsl query type for QVHistCredPodEngBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVHistCredPodEngBudget extends com.mysema.query.sql.RelationalPathBase<QVHistCredPodEngBudget> {

    private static final long serialVersionUID = 58990563;

    public static final QVHistCredPodEngBudget vHistCredPodEngBudget = new QVHistCredPodEngBudget("V_HIST_CRED_POD_ENG_BUDGET");

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Long> hcpOrdre = createNumber("hcpOrdre", Long.class);

    public QVHistCredPodEngBudget(String variable) {
        super(QVHistCredPodEngBudget.class, forVariable(variable), "GFC", "V_HIST_CRED_POD_ENG_BUDGET");
        addMetadata();
    }

    public QVHistCredPodEngBudget(String variable, String schema, String table) {
        super(QVHistCredPodEngBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVHistCredPodEngBudget(Path<? extends QVHistCredPodEngBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_HIST_CRED_POD_ENG_BUDGET");
        addMetadata();
    }

    public QVHistCredPodEngBudget(PathMetadata<?> metadata) {
        super(QVHistCredPodEngBudget.class, metadata, "GFC", "V_HIST_CRED_POD_ENG_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(hcpOrdre, ColumnMetadata.named("HCP_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

