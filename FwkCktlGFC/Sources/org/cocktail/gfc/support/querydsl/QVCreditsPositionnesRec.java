package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCreditsPositionnesRec is a Querydsl query type for QVCreditsPositionnesRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCreditsPositionnesRec extends com.mysema.query.sql.RelationalPathBase<QVCreditsPositionnesRec> {

    private static final long serialVersionUID = -213302686;

    public static final QVCreditsPositionnesRec vCreditsPositionnesRec = new QVCreditsPositionnesRec("V_CREDITS_POSITIONNES_REC");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> montant = createSimple("montant", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> traOrdre = createSimple("traOrdre", Object.class);

    public QVCreditsPositionnesRec(String variable) {
        super(QVCreditsPositionnesRec.class, forVariable(variable), "GFC", "V_CREDITS_POSITIONNES_REC");
        addMetadata();
    }

    public QVCreditsPositionnesRec(String variable, String schema, String table) {
        super(QVCreditsPositionnesRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCreditsPositionnesRec(Path<? extends QVCreditsPositionnesRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CREDITS_POSITIONNES_REC");
        addMetadata();
    }

    public QVCreditsPositionnesRec(PathMetadata<?> metadata) {
        super(QVCreditsPositionnesRec.class, metadata, "GFC", "V_CREDITS_POSITIONNES_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

