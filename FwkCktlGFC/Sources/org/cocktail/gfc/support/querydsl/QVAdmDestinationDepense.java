package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmDestinationDepense is a Querydsl query type for QVAdmDestinationDepense
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmDestinationDepense extends com.mysema.query.sql.RelationalPathBase<QVAdmDestinationDepense> {

    private static final long serialVersionUID = 293043985;

    public static final QVAdmDestinationDepense vAdmDestinationDepense = new QVAdmDestinationDepense("V_ADM_DESTINATION_DEPENSE");

    public final StringPath abreviation = createString("abreviation");

    public final StringPath code = createString("code");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> fermeture = createDateTime("fermeture", java.sql.Timestamp.class);

    public final NumberPath<Long> idAdmDestinationDepense = createNumber("idAdmDestinationDepense", Long.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Long> niveau = createNumber("niveau", Long.class);

    public final NumberPath<Integer> ordreAffichage = createNumber("ordreAffichage", Integer.class);

    public final DateTimePath<java.sql.Timestamp> ouverture = createDateTime("ouverture", java.sql.Timestamp.class);

    public final NumberPath<Long> pere = createNumber("pere", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final StringPath type = createString("type");

    public QVAdmDestinationDepense(String variable) {
        super(QVAdmDestinationDepense.class, forVariable(variable), "GFC", "V_ADM_DESTINATION_DEPENSE");
        addMetadata();
    }

    public QVAdmDestinationDepense(String variable, String schema, String table) {
        super(QVAdmDestinationDepense.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmDestinationDepense(Path<? extends QVAdmDestinationDepense> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_DESTINATION_DEPENSE");
        addMetadata();
    }

    public QVAdmDestinationDepense(PathMetadata<?> metadata) {
        super(QVAdmDestinationDepense.class, metadata, "GFC", "V_ADM_DESTINATION_DEPENSE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(abreviation, ColumnMetadata.named("ABREVIATION").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fermeture, ColumnMetadata.named("FERMETURE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(idAdmDestinationDepense, ColumnMetadata.named("ID_ADM_DESTINATION_DEPENSE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(niveau, ColumnMetadata.named("NIVEAU").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ordreAffichage, ColumnMetadata.named("ORDRE_AFFICHAGE").withIndex(11).ofType(Types.DECIMAL).withSize(3));
        addMetadata(ouverture, ColumnMetadata.named("OUVERTURE").withIndex(9).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(pere, ColumnMetadata.named("PERE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(type, ColumnMetadata.named("TYPE").withIndex(7).ofType(Types.VARCHAR).withSize(5).notNull());
    }

}

