package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeBascule is a Querydsl query type for QCommandeBascule
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeBascule extends com.mysema.query.sql.RelationalPathBase<QCommandeBascule> {

    private static final long serialVersionUID = 1758598276;

    public static final QCommandeBascule commandeBascule = new QCommandeBascule("COMMANDE_BASCULE");

    public final NumberPath<Long> combId = createNumber("combId", Long.class);

    public final NumberPath<Long> commIdDestination = createNumber("commIdDestination", Long.class);

    public final NumberPath<Long> commIdOrigine = createNumber("commIdOrigine", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeBascule> commandeBasculePk = createPrimaryKey(combId);

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeBascOrigFk = createForeignKey(commIdOrigine, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeBascDestFk = createForeignKey(commIdDestination, "COMM_ID");

    public QCommandeBascule(String variable) {
        super(QCommandeBascule.class, forVariable(variable), "GFC", "COMMANDE_BASCULE");
        addMetadata();
    }

    public QCommandeBascule(String variable, String schema, String table) {
        super(QCommandeBascule.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeBascule(Path<? extends QCommandeBascule> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_BASCULE");
        addMetadata();
    }

    public QCommandeBascule(PathMetadata<?> metadata) {
        super(QCommandeBascule.class, metadata, "GFC", "COMMANDE_BASCULE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(combId, ColumnMetadata.named("COMB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commIdDestination, ColumnMetadata.named("COMM_ID_DESTINATION").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commIdOrigine, ColumnMetadata.named("COMM_ID_ORIGINE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

