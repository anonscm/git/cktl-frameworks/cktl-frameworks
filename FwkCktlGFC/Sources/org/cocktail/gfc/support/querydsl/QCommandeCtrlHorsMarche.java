package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeCtrlHorsMarche is a Querydsl query type for QCommandeCtrlHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeCtrlHorsMarche extends com.mysema.query.sql.RelationalPathBase<QCommandeCtrlHorsMarche> {

    private static final long serialVersionUID = -1221842352;

    public static final QCommandeCtrlHorsMarche commandeCtrlHorsMarche = new QCommandeCtrlHorsMarche("COMMANDE_CTRL_HORS_MARCHE");

    public final NumberPath<Long> cbudId = createNumber("cbudId", Long.class);

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> chomHtSaisie = createNumber("chomHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> chomId = createNumber("chomId", Long.class);

    public final NumberPath<java.math.BigDecimal> chomMontantBudgetaire = createNumber("chomMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<Double> chomPourcentage = createNumber("chomPourcentage", Double.class);

    public final NumberPath<java.math.BigDecimal> chomTtcSaisie = createNumber("chomTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> chomTvaSaisie = createNumber("chomTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeCtrlHorsMarche> commandeCtrlHorsMarchePk = createPrimaryKey(chomId);

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> commandeCtrlHomCbudIdFk = createForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QCodeExer> commandeCtrlHomCeOrdreFk = createForeignKey(ceOrdre, "CE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypeAchat> commandeCtrlHomTypaIdFk = createForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeCtrlHomExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QCommandeCtrlHorsMarche(String variable) {
        super(QCommandeCtrlHorsMarche.class, forVariable(variable), "GFC", "COMMANDE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QCommandeCtrlHorsMarche(String variable, String schema, String table) {
        super(QCommandeCtrlHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeCtrlHorsMarche(Path<? extends QCommandeCtrlHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QCommandeCtrlHorsMarche(PathMetadata<?> metadata) {
        super(QCommandeCtrlHorsMarche.class, metadata, "GFC", "COMMANDE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cbudId, ColumnMetadata.named("CBUD_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(chomHtSaisie, ColumnMetadata.named("CHOM_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(chomId, ColumnMetadata.named("CHOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(chomMontantBudgetaire, ColumnMetadata.named("CHOM_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(chomPourcentage, ColumnMetadata.named("CHOM_POURCENTAGE").withIndex(7).ofType(Types.DECIMAL).withSize(15).withDigits(5));
        addMetadata(chomTtcSaisie, ColumnMetadata.named("CHOM_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(chomTvaSaisie, ColumnMetadata.named("CHOM_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

