package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVDepenseExport is a Querydsl query type for QImmVDepenseExport
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVDepenseExport extends com.mysema.query.sql.RelationalPathBase<QImmVDepenseExport> {

    private static final long serialVersionUID = -1255728624;

    public static final QImmVDepenseExport immVDepenseExport = new QImmVDepenseExport("IMM_V_DEPENSE_EXPORT");

    public final NumberPath<Long> borNum = createNumber("borNum", Long.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoHtSaisie = createNumber("dpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoMontantBudgetaire = createNumber("dpcoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoTtcSaisie = createNumber("dpcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoTvaSaisie = createNumber("dpcoTvaSaisie", java.math.BigDecimal.class);

    public final DateTimePath<java.sql.Timestamp> dppDateFacture = createDateTime("dppDateFacture", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateReception = createDateTime("dppDateReception", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateSaisie = createDateTime("dppDateSaisie", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateServiceFait = createDateTime("dppDateServiceFait", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> dppHtInitial = createNumber("dppHtInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppHtSaisie = createNumber("dppHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> dppIdReversement = createNumber("dppIdReversement", Long.class);

    public final NumberPath<Long> dppNbPiece = createNumber("dppNbPiece", Long.class);

    public final StringPath dppNumeroFacture = createString("dppNumeroFacture");

    public final NumberPath<java.math.BigDecimal> dppTtcInitial = createNumber("dppTtcInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTtcSaisie = createNumber("dppTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTvaInitial = createNumber("dppTvaInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTvaSaisie = createNumber("dppTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> manNumero = createNumber("manNumero", Long.class);

    public final NumberPath<Long> modOrdre = createNumber("modOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> ribOrdre = createNumber("ribOrdre", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QImmVDepenseExport(String variable) {
        super(QImmVDepenseExport.class, forVariable(variable), "GFC", "IMM_V_DEPENSE_EXPORT");
        addMetadata();
    }

    public QImmVDepenseExport(String variable, String schema, String table) {
        super(QImmVDepenseExport.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVDepenseExport(Path<? extends QImmVDepenseExport> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_DEPENSE_EXPORT");
        addMetadata();
    }

    public QImmVDepenseExport(PathMetadata<?> metadata) {
        super(QImmVDepenseExport.class, metadata, "GFC", "IMM_V_DEPENSE_EXPORT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borNum, ColumnMetadata.named("BOR_NUM").withIndex(28).ofType(Types.DECIMAL).withSize(0));
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoHtSaisie, ColumnMetadata.named("DPCO_HT_SAISIE").withIndex(24).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(21).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoMontantBudgetaire, ColumnMetadata.named("DPCO_MONTANT_BUDGETAIRE").withIndex(23).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoTtcSaisie, ColumnMetadata.named("DPCO_TTC_SAISIE").withIndex(26).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoTvaSaisie, ColumnMetadata.named("DPCO_TVA_SAISIE").withIndex(25).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppDateFacture, ColumnMetadata.named("DPP_DATE_FACTURE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateReception, ColumnMetadata.named("DPP_DATE_RECEPTION").withIndex(13).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateSaisie, ColumnMetadata.named("DPP_DATE_SAISIE").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateServiceFait, ColumnMetadata.named("DPP_DATE_SERVICE_FAIT").withIndex(14).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppHtInitial, ColumnMetadata.named("DPP_HT_INITIAL").withIndex(18).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppHtSaisie, ColumnMetadata.named("DPP_HT_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppIdReversement, ColumnMetadata.named("DPP_ID_REVERSEMENT").withIndex(17).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dppNbPiece, ColumnMetadata.named("DPP_NB_PIECE").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(4).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(dppTtcInitial, ColumnMetadata.named("DPP_TTC_INITIAL").withIndex(20).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTtcSaisie, ColumnMetadata.named("DPP_TTC_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTvaInitial, ColumnMetadata.named("DPP_TVA_INITIAL").withIndex(19).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTvaSaisie, ColumnMetadata.named("DPP_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(29).ofType(Types.VARCHAR).withSize(10));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(30).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manNumero, ColumnMetadata.named("MAN_NUMERO").withIndex(27).ofType(Types.DECIMAL).withSize(0));
        addMetadata(modOrdre, ColumnMetadata.named("MOD_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(22).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(ribOrdre, ColumnMetadata.named("RIB_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(31).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(16).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

