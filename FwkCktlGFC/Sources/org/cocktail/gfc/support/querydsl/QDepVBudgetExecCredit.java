package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepVBudgetExecCredit is a Querydsl query type for QDepVBudgetExecCredit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepVBudgetExecCredit extends com.mysema.query.sql.RelationalPathBase<QDepVBudgetExecCredit> {

    private static final long serialVersionUID = 207016663;

    public static final QDepVBudgetExecCredit depVBudgetExecCredit = new QDepVBudgetExecCredit("DEP_V_BUDGET_EXEC_CREDIT");

    public final SimplePath<Object> bdxcConventions = createSimple("bdxcConventions", Object.class);

    public final SimplePath<Object> bdxcCredits = createSimple("bdxcCredits", Object.class);

    public final SimplePath<Object> bdxcDbms = createSimple("bdxcDbms", Object.class);

    public final SimplePath<Object> bdxcDebits = createSimple("bdxcDebits", Object.class);

    public final SimplePath<Object> bdxcDisponible = createSimple("bdxcDisponible", Object.class);

    public final SimplePath<Object> bdxcDispoReserve = createSimple("bdxcDispoReserve", Object.class);

    public final SimplePath<Object> bdxcEngagements = createSimple("bdxcEngagements", Object.class);

    public final SimplePath<Object> bdxcId = createSimple("bdxcId", Object.class);

    public final SimplePath<Object> bdxcMandats = createSimple("bdxcMandats", Object.class);

    public final SimplePath<Object> bdxcOuverts = createSimple("bdxcOuverts", Object.class);

    public final SimplePath<Object> bdxcPrimitifs = createSimple("bdxcPrimitifs", Object.class);

    public final SimplePath<Object> bdxcProvisoires = createSimple("bdxcProvisoires", Object.class);

    public final SimplePath<Object> bdxcReliquats = createSimple("bdxcReliquats", Object.class);

    public final SimplePath<Object> bdxcReserve = createSimple("bdxcReserve", Object.class);

    public final SimplePath<Object> bdxcVotes = createSimple("bdxcVotes", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QDepVBudgetExecCredit(String variable) {
        super(QDepVBudgetExecCredit.class, forVariable(variable), "GFC", "DEP_V_BUDGET_EXEC_CREDIT");
        addMetadata();
    }

    public QDepVBudgetExecCredit(String variable, String schema, String table) {
        super(QDepVBudgetExecCredit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepVBudgetExecCredit(Path<? extends QDepVBudgetExecCredit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEP_V_BUDGET_EXEC_CREDIT");
        addMetadata();
    }

    public QDepVBudgetExecCredit(PathMetadata<?> metadata) {
        super(QDepVBudgetExecCredit.class, metadata, "GFC", "DEP_V_BUDGET_EXEC_CREDIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bdxcConventions, ColumnMetadata.named("BDXC_CONVENTIONS").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcCredits, ColumnMetadata.named("BDXC_CREDITS").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcDbms, ColumnMetadata.named("BDXC_DBMS").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcDebits, ColumnMetadata.named("BDXC_DEBITS").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcDisponible, ColumnMetadata.named("BDXC_DISPONIBLE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcDispoReserve, ColumnMetadata.named("BDXC_DISPO_RESERVE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcEngagements, ColumnMetadata.named("BDXC_ENGAGEMENTS").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcId, ColumnMetadata.named("BDXC_ID").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcMandats, ColumnMetadata.named("BDXC_MANDATS").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcOuverts, ColumnMetadata.named("BDXC_OUVERTS").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcPrimitifs, ColumnMetadata.named("BDXC_PRIMITIFS").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcProvisoires, ColumnMetadata.named("BDXC_PROVISOIRES").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcReliquats, ColumnMetadata.named("BDXC_RELIQUATS").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcReserve, ColumnMetadata.named("BDXC_RESERVE").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(bdxcVotes, ColumnMetadata.named("BDXC_VOTES").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
    }

}

