package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAbricotPapayeAttente is a Querydsl query type for QVAbricotPapayeAttente
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAbricotPapayeAttente extends com.mysema.query.sql.RelationalPathBase<QVAbricotPapayeAttente> {

    private static final long serialVersionUID = -1109113454;

    public static final QVAbricotPapayeAttente vAbricotPapayeAttente = new QVAbricotPapayeAttente("V_ABRICOT_PAPAYE_ATTENTE");

    public final NumberPath<Integer> exercice = createNumber("exercice", Integer.class);

    public final StringPath mois = createString("mois");

    public final NumberPath<Long> montant = createNumber("montant", Long.class);

    public final NumberPath<Long> nbAttente = createNumber("nbAttente", Long.class);

    public final StringPath orgUb = createString("orgUb");

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public QVAbricotPapayeAttente(String variable) {
        super(QVAbricotPapayeAttente.class, forVariable(variable), "GFC", "V_ABRICOT_PAPAYE_ATTENTE");
        addMetadata();
    }

    public QVAbricotPapayeAttente(String variable, String schema, String table) {
        super(QVAbricotPapayeAttente.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAbricotPapayeAttente(Path<? extends QVAbricotPapayeAttente> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ABRICOT_PAPAYE_ATTENTE");
        addMetadata();
    }

    public QVAbricotPapayeAttente(PathMetadata<?> metadata) {
        super(QVAbricotPapayeAttente.class, metadata, "GFC", "V_ABRICOT_PAPAYE_ATTENTE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(mois, ColumnMetadata.named("MOIS").withIndex(2).ofType(Types.VARCHAR).withSize(500));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(nbAttente, ColumnMetadata.named("NB_ATTENTE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(3).ofType(Types.VARCHAR).withSize(10));
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
    }

}

