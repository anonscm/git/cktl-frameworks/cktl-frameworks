package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCatCatalogue is a Querydsl query type for QCatCatalogue
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCatCatalogue extends com.mysema.query.sql.RelationalPathBase<QCatCatalogue> {

    private static final long serialVersionUID = -1395927884;

    public static final QCatCatalogue catCatalogue = new QCatCatalogue("CAT_CATALOGUE");

    public final StringPath catCommentaire = createString("catCommentaire");

    public final DateTimePath<java.sql.Timestamp> catDateDebut = createDateTime("catDateDebut", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> catDateFin = createDateTime("catDateFin", java.sql.Timestamp.class);

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final StringPath catLibelle = createString("catLibelle");

    public final NumberPath<Long> cmOrdre = createNumber("cmOrdre", Long.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCatCatalogue> catCataloguePk = createPrimaryKey(catId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> catCatalogueTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCodeMarche> catCatalogueCmOrdreFk = createForeignKey(cmOrdre, "CM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> catCatalogueFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplication> catCatalogueTyapIdFk = createForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogueLocalise> _catCatalogueLocaliseCatFk = createInvForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QCataloguePrestation> _catalogueCatIdFk = createInvForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogueArticle> _catCatalogueArticleCatFk = createInvForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QBoutiqueCatalogue> _boutiqueCatalogueCataloFk1 = createInvForeignKey(catId, "CAT_ID");

    public QCatCatalogue(String variable) {
        super(QCatCatalogue.class, forVariable(variable), "GFC", "CAT_CATALOGUE");
        addMetadata();
    }

    public QCatCatalogue(String variable, String schema, String table) {
        super(QCatCatalogue.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCatCatalogue(Path<? extends QCatCatalogue> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CAT_CATALOGUE");
        addMetadata();
    }

    public QCatCatalogue(PathMetadata<?> metadata) {
        super(QCatCatalogue.class, metadata, "GFC", "CAT_CATALOGUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(catCommentaire, ColumnMetadata.named("CAT_COMMENTAIRE").withIndex(8).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(catDateDebut, ColumnMetadata.named("CAT_DATE_DEBUT").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(catDateFin, ColumnMetadata.named("CAT_DATE_FIN").withIndex(7).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catLibelle, ColumnMetadata.named("CAT_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

