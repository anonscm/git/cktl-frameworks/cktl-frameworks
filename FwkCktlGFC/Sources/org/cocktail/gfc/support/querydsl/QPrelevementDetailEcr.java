package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPrelevementDetailEcr is a Querydsl query type for QPrelevementDetailEcr
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPrelevementDetailEcr extends com.mysema.query.sql.RelationalPathBase<QPrelevementDetailEcr> {

    private static final long serialVersionUID = 1943872283;

    public static final QPrelevementDetailEcr prelevementDetailEcr = new QPrelevementDetailEcr("PRELEVEMENT_DETAIL_ECR");

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Long> pdeId = createNumber("pdeId", Long.class);

    public final NumberPath<Long> prelPrelevOrdre = createNumber("prelPrelevOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPrelevementDetailEcr> prelevementDetailEcrPk = createPrimaryKey(pdeId);

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> prelevementDetailEcrEcdFk = createForeignKey(ecdOrdre, "ECD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPrelevement> prelevementDetailEcrEchFk = createForeignKey(prelPrelevOrdre, "PREL_PRELEV_ORDRE");

    public QPrelevementDetailEcr(String variable) {
        super(QPrelevementDetailEcr.class, forVariable(variable), "GFC", "PRELEVEMENT_DETAIL_ECR");
        addMetadata();
    }

    public QPrelevementDetailEcr(String variable, String schema, String table) {
        super(QPrelevementDetailEcr.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPrelevementDetailEcr(Path<? extends QPrelevementDetailEcr> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PRELEVEMENT_DETAIL_ECR");
        addMetadata();
    }

    public QPrelevementDetailEcr(PathMetadata<?> metadata) {
        super(QPrelevementDetailEcr.class, metadata, "GFC", "PRELEVEMENT_DETAIL_ECR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdeId, ColumnMetadata.named("PDE_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prelPrelevOrdre, ColumnMetadata.named("PREL_PRELEV_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

