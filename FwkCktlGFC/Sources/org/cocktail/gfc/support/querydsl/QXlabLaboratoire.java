package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QXlabLaboratoire is a Querydsl query type for QXlabLaboratoire
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QXlabLaboratoire extends com.mysema.query.sql.RelationalPathBase<QXlabLaboratoire> {

    private static final long serialVersionUID = -764019718;

    public static final QXlabLaboratoire xlabLaboratoire = new QXlabLaboratoire("XLAB_LABORATOIRE");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath xlabCr = createString("xlabCr");

    public final StringPath xlabEtab = createString("xlabEtab");

    public final NumberPath<Long> xlabId = createNumber("xlabId", Long.class);

    public final StringPath xlabUb = createString("xlabUb");

    public final StringPath xlabUnit = createString("xlabUnit");

    public final com.mysema.query.sql.PrimaryKey<QXlabLaboratoire> xlabLaboratoirePk = createPrimaryKey(xlabId);

    public final com.mysema.query.sql.ForeignKey<QAdmEb> xlabLaboratoireOrgIdFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFactureXlabIdFk = createInvForeignKey(xlabId, "XLAB_ID");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeXlabIdFk = createInvForeignKey(xlabId, "XLAB_ID");

    public QXlabLaboratoire(String variable) {
        super(QXlabLaboratoire.class, forVariable(variable), "GFC", "XLAB_LABORATOIRE");
        addMetadata();
    }

    public QXlabLaboratoire(String variable, String schema, String table) {
        super(QXlabLaboratoire.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QXlabLaboratoire(Path<? extends QXlabLaboratoire> path) {
        super(path.getType(), path.getMetadata(), "GFC", "XLAB_LABORATOIRE");
        addMetadata();
    }

    public QXlabLaboratoire(PathMetadata<?> metadata) {
        super(QXlabLaboratoire.class, metadata, "GFC", "XLAB_LABORATOIRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(xlabCr, ColumnMetadata.named("XLAB_CR").withIndex(5).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(xlabEtab, ColumnMetadata.named("XLAB_ETAB").withIndex(3).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(xlabId, ColumnMetadata.named("XLAB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(xlabUb, ColumnMetadata.named("XLAB_UB").withIndex(4).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(xlabUnit, ColumnMetadata.named("XLAB_UNIT").withIndex(6).ofType(Types.VARCHAR).withSize(6).notNull());
    }

}

