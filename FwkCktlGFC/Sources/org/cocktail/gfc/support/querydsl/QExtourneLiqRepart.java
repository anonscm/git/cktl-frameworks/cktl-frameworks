package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QExtourneLiqRepart is a Querydsl query type for QExtourneLiqRepart
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QExtourneLiqRepart extends com.mysema.query.sql.RelationalPathBase<QExtourneLiqRepart> {

    private static final long serialVersionUID = 1489524503;

    public static final QExtourneLiqRepart extourneLiqRepart = new QExtourneLiqRepart("EXTOURNE_LIQ_REPART");

    public final NumberPath<Long> eldId = createNumber("eldId", Long.class);

    public final NumberPath<Long> elId = createNumber("elId", Long.class);

    public final NumberPath<Long> elrId = createNumber("elrId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QExtourneLiqRepart> sysC0076880 = createPrimaryKey(elrId);

    public final com.mysema.query.sql.ForeignKey<QExtourneLiqDef> extourneLiqRepartEldidFk = createForeignKey(eldId, "ELD_ID");

    public final com.mysema.query.sql.ForeignKey<QExtourneLiq> extourneLiqRepartElidFk = createForeignKey(elId, "EL_ID");

    public QExtourneLiqRepart(String variable) {
        super(QExtourneLiqRepart.class, forVariable(variable), "GFC", "EXTOURNE_LIQ_REPART");
        addMetadata();
    }

    public QExtourneLiqRepart(String variable, String schema, String table) {
        super(QExtourneLiqRepart.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QExtourneLiqRepart(Path<? extends QExtourneLiqRepart> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EXTOURNE_LIQ_REPART");
        addMetadata();
    }

    public QExtourneLiqRepart(PathMetadata<?> metadata) {
        super(QExtourneLiqRepart.class, metadata, "GFC", "EXTOURNE_LIQ_REPART");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(eldId, ColumnMetadata.named("ELD_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(elId, ColumnMetadata.named("EL_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(elrId, ColumnMetadata.named("ELR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

