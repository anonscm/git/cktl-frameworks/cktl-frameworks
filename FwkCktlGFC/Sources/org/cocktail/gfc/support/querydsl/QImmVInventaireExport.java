package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVInventaireExport is a Querydsl query type for QImmVInventaireExport
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVInventaireExport extends com.mysema.query.sql.RelationalPathBase<QImmVInventaireExport> {

    private static final long serialVersionUID = 1844480279;

    public static final QImmVInventaireExport immVInventaireExport = new QImmVInventaireExport("IMM_V_INVENTAIRE_EXPORT");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final StringPath clicNumComplet = createString("clicNumComplet");

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<java.math.BigDecimal> invcMontantAcquisition = createNumber("invcMontantAcquisition", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> invcMontantResiduel = createNumber("invcMontantResiduel", java.math.BigDecimal.class);

    public final NumberPath<Long> invId = createNumber("invId", Long.class);

    public final NumberPath<Long> lidMontant = createNumber("lidMontant", Long.class);

    public final NumberPath<Long> lidOrdre = createNumber("lidOrdre", Long.class);

    public final NumberPath<Long> livOrdre = createNumber("livOrdre", Long.class);

    public final NumberPath<Long> orgfId = createNumber("orgfId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QImmVInventaireExport(String variable) {
        super(QImmVInventaireExport.class, forVariable(variable), "GFC", "IMM_V_INVENTAIRE_EXPORT");
        addMetadata();
    }

    public QImmVInventaireExport(String variable, String schema, String table) {
        super(QImmVInventaireExport.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVInventaireExport(Path<? extends QImmVInventaireExport> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_INVENTAIRE_EXPORT");
        addMetadata();
    }

    public QImmVInventaireExport(PathMetadata<?> metadata) {
        super(QImmVInventaireExport.class, metadata, "GFC", "IMM_V_INVENTAIRE_EXPORT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clicNumComplet, ColumnMetadata.named("CLIC_NUM_COMPLET").withIndex(14).ofType(Types.VARCHAR).withSize(200));
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcMontantAcquisition, ColumnMetadata.named("INVC_MONTANT_ACQUISITION").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invcMontantResiduel, ColumnMetadata.named("INVC_MONTANT_RESIDUEL").withIndex(3).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invId, ColumnMetadata.named("INV_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lidMontant, ColumnMetadata.named("LID_MONTANT").withIndex(13).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lidOrdre, ColumnMetadata.named("LID_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(livOrdre, ColumnMetadata.named("LIV_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(orgfId, ColumnMetadata.named("ORGF_ID").withIndex(1).ofType(Types.DECIMAL).withSize(38));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(12).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(11).ofType(Types.DECIMAL).withSize(0));
    }

}

