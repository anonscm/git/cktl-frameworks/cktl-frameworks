package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVEditionMandatOrigine is a Querydsl query type for QVEditionMandatOrigine
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVEditionMandatOrigine extends com.mysema.query.sql.RelationalPathBase<QVEditionMandatOrigine> {

    private static final long serialVersionUID = 2080957785;

    public static final QVEditionMandatOrigine vEditionMandatOrigine = new QVEditionMandatOrigine("V_EDITION_MANDAT_ORIGINE");

    public final NumberPath<Long> borNumOrigine = createNumber("borNumOrigine", Long.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Long> depIdOrigine = createNumber("depIdOrigine", Long.class);

    public final NumberPath<java.math.BigDecimal> depMontantBudgetaire = createNumber("depMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> depMontantBudgOrigine = createNumber("depMontantBudgOrigine", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final NumberPath<Long> manIdOrigine = createNumber("manIdOrigine", Long.class);

    public final NumberPath<Long> manNumero = createNumber("manNumero", Long.class);

    public final NumberPath<Long> manNumOrigine = createNumber("manNumOrigine", Long.class);

    public QVEditionMandatOrigine(String variable) {
        super(QVEditionMandatOrigine.class, forVariable(variable), "GFC", "V_EDITION_MANDAT_ORIGINE");
        addMetadata();
    }

    public QVEditionMandatOrigine(String variable, String schema, String table) {
        super(QVEditionMandatOrigine.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVEditionMandatOrigine(Path<? extends QVEditionMandatOrigine> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EDITION_MANDAT_ORIGINE");
        addMetadata();
    }

    public QVEditionMandatOrigine(PathMetadata<?> metadata) {
        super(QVEditionMandatOrigine.class, metadata, "GFC", "V_EDITION_MANDAT_ORIGINE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borNumOrigine, ColumnMetadata.named("BOR_NUM_ORIGINE").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depIdOrigine, ColumnMetadata.named("DEP_ID_ORIGINE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depMontantBudgetaire, ColumnMetadata.named("DEP_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depMontantBudgOrigine, ColumnMetadata.named("DEP_MONTANT_BUDG_ORIGINE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manIdOrigine, ColumnMetadata.named("MAN_ID_ORIGINE").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manNumero, ColumnMetadata.named("MAN_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manNumOrigine, ColumnMetadata.named("MAN_NUM_ORIGINE").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

