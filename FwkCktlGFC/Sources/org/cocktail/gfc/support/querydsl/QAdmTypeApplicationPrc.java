package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypeApplicationPrc is a Querydsl query type for QAdmTypeApplicationPrc
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypeApplicationPrc extends com.mysema.query.sql.RelationalPathBase<QAdmTypeApplicationPrc> {

    private static final long serialVersionUID = -689395468;

    public static final QAdmTypeApplicationPrc admTypeApplicationPrc = new QAdmTypeApplicationPrc("ADM_TYPE_APPLICATION_PRC");

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final NumberPath<Long> tyappId = createNumber("tyappId", Long.class);

    public final StringPath tyappProc = createString("tyappProc");

    public final NumberPath<Long> tyappSort = createNumber("tyappSort", Long.class);

    public final StringPath tyappType = createString("tyappType");

    public final com.mysema.query.sql.PrimaryKey<QAdmTypeApplicationPrc> sysC0076608 = createPrimaryKey(tyappId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplication> admTyapTyapIdFk = createForeignKey(tyapId, "TYAP_ID");

    public QAdmTypeApplicationPrc(String variable) {
        super(QAdmTypeApplicationPrc.class, forVariable(variable), "GFC", "ADM_TYPE_APPLICATION_PRC");
        addMetadata();
    }

    public QAdmTypeApplicationPrc(String variable, String schema, String table) {
        super(QAdmTypeApplicationPrc.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypeApplicationPrc(Path<? extends QAdmTypeApplicationPrc> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_APPLICATION_PRC");
        addMetadata();
    }

    public QAdmTypeApplicationPrc(PathMetadata<?> metadata) {
        super(QAdmTypeApplicationPrc.class, metadata, "GFC", "ADM_TYPE_APPLICATION_PRC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(tyappId, ColumnMetadata.named("TYAPP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(tyappProc, ColumnMetadata.named("TYAPP_PROC").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tyappSort, ColumnMetadata.named("TYAPP_SORT").withIndex(4).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(tyappType, ColumnMetadata.named("TYAPP_TYPE").withIndex(5).ofType(Types.VARCHAR).withSize(200).notNull());
    }

}

