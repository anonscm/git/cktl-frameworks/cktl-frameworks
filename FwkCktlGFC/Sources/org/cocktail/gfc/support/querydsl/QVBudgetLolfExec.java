package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVBudgetLolfExec is a Querydsl query type for QVBudgetLolfExec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVBudgetLolfExec extends com.mysema.query.sql.RelationalPathBase<QVBudgetLolfExec> {

    private static final long serialVersionUID = -209609272;

    public static final QVBudgetLolfExec vBudgetLolfExec = new QVBudgetLolfExec("V_BUDGET_LOLF_EXEC");

    public final SimplePath<Object> budget = createSimple("budget", Object.class);

    public final SimplePath<Object> codeLolf = createSimple("codeLolf", Object.class);

    public final SimplePath<Object> exercice = createSimple("exercice", Object.class);

    public final SimplePath<Object> libelleLolf = createSimple("libelleLolf", Object.class);

    public final SimplePath<Object> lolfCodePere = createSimple("lolfCodePere", Object.class);

    public final SimplePath<Object> lolfLibellePere = createSimple("lolfLibellePere", Object.class);

    public final SimplePath<Object> masse = createSimple("masse", Object.class);

    public final SimplePath<Object> montant = createSimple("montant", Object.class);

    public final SimplePath<Object> nature = createSimple("nature", Object.class);

    public final SimplePath<Object> tcdLibelle = createSimple("tcdLibelle", Object.class);

    public final SimplePath<Object> typeCredit = createSimple("typeCredit", Object.class);

    public final SimplePath<Object> ub = createSimple("ub", Object.class);

    public QVBudgetLolfExec(String variable) {
        super(QVBudgetLolfExec.class, forVariable(variable), "GFC", "V_BUDGET_LOLF_EXEC");
        addMetadata();
    }

    public QVBudgetLolfExec(String variable, String schema, String table) {
        super(QVBudgetLolfExec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVBudgetLolfExec(Path<? extends QVBudgetLolfExec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_BUDGET_LOLF_EXEC");
        addMetadata();
    }

    public QVBudgetLolfExec(PathMetadata<?> metadata) {
        super(QVBudgetLolfExec.class, metadata, "GFC", "V_BUDGET_LOLF_EXEC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(budget, ColumnMetadata.named("BUDGET").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(codeLolf, ColumnMetadata.named("CODE_LOLF").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(libelleLolf, ColumnMetadata.named("LIBELLE_LOLF").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(lolfCodePere, ColumnMetadata.named("LOLF_CODE_PERE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(lolfLibellePere, ColumnMetadata.named("LOLF_LIBELLE_PERE").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(masse, ColumnMetadata.named("MASSE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(nature, ColumnMetadata.named("NATURE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdLibelle, ColumnMetadata.named("TCD_LIBELLE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(typeCredit, ColumnMetadata.named("TYPE_CREDIT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(ub, ColumnMetadata.named("UB").withIndex(7).ofType(Types.OTHER).withSize(0));
    }

}

