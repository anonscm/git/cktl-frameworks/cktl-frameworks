package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBudEnvExerNatDep is a Querydsl query type for QBudEnvExerNatDep
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBudEnvExerNatDep extends com.mysema.query.sql.RelationalPathBase<QBudEnvExerNatDep> {

    private static final long serialVersionUID = -1528098613;

    public static final QBudEnvExerNatDep budEnvExerNatDep = new QBudEnvExerNatDep("BUD_ENV_EXER_NAT_DEP");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmNatureDep = createNumber("idAdmNatureDep", Long.class);

    public final NumberPath<Long> idBudEnveloppe = createNumber("idBudEnveloppe", Long.class);

    public final NumberPath<Long> idBudEnvExerNatDep = createNumber("idBudEnvExerNatDep", Long.class);

    public final NumberPath<Integer> isActive = createNumber("isActive", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QBudEnvExerNatDep> budEnvExerNatDepPk = createPrimaryKey(idBudEnvExerNatDep);

    public final com.mysema.query.sql.ForeignKey<QBudEnveloppe> budEnveloppeEnvExerNdFk = createForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> admExerciceEnvExerNdFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QBudEnvExerNatDep(String variable) {
        super(QBudEnvExerNatDep.class, forVariable(variable), "GFC", "BUD_ENV_EXER_NAT_DEP");
        addMetadata();
    }

    public QBudEnvExerNatDep(String variable, String schema, String table) {
        super(QBudEnvExerNatDep.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBudEnvExerNatDep(Path<? extends QBudEnvExerNatDep> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BUD_ENV_EXER_NAT_DEP");
        addMetadata();
    }

    public QBudEnvExerNatDep(PathMetadata<?> metadata) {
        super(QBudEnvExerNatDep.class, metadata, "GFC", "BUD_ENV_EXER_NAT_DEP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(idAdmNatureDep, ColumnMetadata.named("ID_ADM_NATURE_DEP").withIndex(4).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idBudEnveloppe, ColumnMetadata.named("ID_BUD_ENVELOPPE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idBudEnvExerNatDep, ColumnMetadata.named("ID_BUD_ENV_EXER_NAT_DEP").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(isActive, ColumnMetadata.named("IS_ACTIVE").withIndex(5).ofType(Types.DECIMAL).withSize(1));
    }

}

