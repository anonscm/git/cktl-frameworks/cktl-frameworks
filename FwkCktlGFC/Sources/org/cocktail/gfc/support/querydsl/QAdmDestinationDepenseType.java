package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmDestinationDepenseType is a Querydsl query type for QAdmDestinationDepenseType
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmDestinationDepenseType extends com.mysema.query.sql.RelationalPathBase<QAdmDestinationDepenseType> {

    private static final long serialVersionUID = 1955373599;

    public static final QAdmDestinationDepenseType admDestinationDepenseType = new QAdmDestinationDepenseType("ADM_DESTINATION_DEPENSE_TYPE");

    public final StringPath libelle = createString("libelle");

    public final StringPath type = createString("type");

    public final com.mysema.query.sql.PrimaryKey<QAdmDestinationDepenseType> admDestinationDepenseTypPk = createPrimaryKey(type);

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepense> _typeFk = createInvForeignKey(type, "TYPE");

    public QAdmDestinationDepenseType(String variable) {
        super(QAdmDestinationDepenseType.class, forVariable(variable), "GFC", "ADM_DESTINATION_DEPENSE_TYPE");
        addMetadata();
    }

    public QAdmDestinationDepenseType(String variable, String schema, String table) {
        super(QAdmDestinationDepenseType.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmDestinationDepenseType(Path<? extends QAdmDestinationDepenseType> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_DESTINATION_DEPENSE_TYPE");
        addMetadata();
    }

    public QAdmDestinationDepenseType(PathMetadata<?> metadata) {
        super(QAdmDestinationDepenseType.class, metadata, "GFC", "ADM_DESTINATION_DEPENSE_TYPE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(60).notNull());
        addMetadata(type, ColumnMetadata.named("TYPE").withIndex(1).ofType(Types.VARCHAR).withSize(5).notNull());
    }

}

