package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventCb is a Querydsl query type for QImmInventCb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventCb extends com.mysema.query.sql.RelationalPathBase<QImmInventCb> {

    private static final long serialVersionUID = 47870113;

    public static final QImmInventCb immInventCb = new QImmInventCb("IMM_INVENT_CB");

    public final StringPath incbCode = createString("incbCode");

    public final StringPath incbCommentaire = createString("incbCommentaire");

    public final NumberPath<Long> incbId = createNumber("incbId", Long.class);

    public final NumberPath<Long> invId = createNumber("invId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInventCb> immInventCbPk = createPrimaryKey(incbId);

    public final com.mysema.query.sql.ForeignKey<QImmInvent> inventaireInvIdFk = createForeignKey(invId, "INV_ID");

    public QImmInventCb(String variable) {
        super(QImmInventCb.class, forVariable(variable), "GFC", "IMM_INVENT_CB");
        addMetadata();
    }

    public QImmInventCb(String variable, String schema, String table) {
        super(QImmInventCb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventCb(Path<? extends QImmInventCb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_CB");
        addMetadata();
    }

    public QImmInventCb(PathMetadata<?> metadata) {
        super(QImmInventCb.class, metadata, "GFC", "IMM_INVENT_CB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(incbCode, ColumnMetadata.named("INCB_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(incbCommentaire, ColumnMetadata.named("INCB_COMMENTAIRE").withIndex(4).ofType(Types.VARCHAR).withSize(200));
        addMetadata(incbId, ColumnMetadata.named("INCB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invId, ColumnMetadata.named("INV_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

