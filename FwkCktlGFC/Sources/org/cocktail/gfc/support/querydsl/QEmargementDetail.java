package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEmargementDetail is a Querydsl query type for QEmargementDetail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEmargementDetail extends com.mysema.query.sql.RelationalPathBase<QEmargementDetail> {

    private static final long serialVersionUID = -747795705;

    public static final QEmargementDetail emargementDetail = new QEmargementDetail("EMARGEMENT_DETAIL");

    public final NumberPath<Long> ecdOrdreDestination = createNumber("ecdOrdreDestination", Long.class);

    public final NumberPath<Long> ecdOrdreSource = createNumber("ecdOrdreSource", Long.class);

    public final NumberPath<Long> emaOrdre = createNumber("emaOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> emdMontant = createNumber("emdMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> emdOrdre = createNumber("emdOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QEmargementDetail> emargementDetailPk = createPrimaryKey(emdOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> emargementDetailExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> emargementDetail3Fk = createForeignKey(ecdOrdreDestination, "ECD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> emargementDetail2Fk = createForeignKey(ecdOrdreSource, "ECD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEmargement> emargementDetail1Fk = createForeignKey(emaOrdre, "EMA_ORDRE");

    public QEmargementDetail(String variable) {
        super(QEmargementDetail.class, forVariable(variable), "GFC", "EMARGEMENT_DETAIL");
        addMetadata();
    }

    public QEmargementDetail(String variable, String schema, String table) {
        super(QEmargementDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEmargementDetail(Path<? extends QEmargementDetail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EMARGEMENT_DETAIL");
        addMetadata();
    }

    public QEmargementDetail(PathMetadata<?> metadata) {
        super(QEmargementDetail.class, metadata, "GFC", "EMARGEMENT_DETAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ecdOrdreDestination, ColumnMetadata.named("ECD_ORDRE_DESTINATION").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ecdOrdreSource, ColumnMetadata.named("ECD_ORDRE_SOURCE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(emaOrdre, ColumnMetadata.named("EMA_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(emdMontant, ColumnMetadata.named("EMD_MONTANT").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emdOrdre, ColumnMetadata.named("EMD_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4));
    }

}

