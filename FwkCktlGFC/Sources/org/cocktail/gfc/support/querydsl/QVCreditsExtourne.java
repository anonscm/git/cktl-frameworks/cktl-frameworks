package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCreditsExtourne is a Querydsl query type for QVCreditsExtourne
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCreditsExtourne extends com.mysema.query.sql.RelationalPathBase<QVCreditsExtourne> {

    private static final long serialVersionUID = 2087577319;

    public static final QVCreditsExtourne vCreditsExtourne = new QVCreditsExtourne("V_CREDITS_EXTOURNE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> montant = createNumber("montant", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath quoi = createString("quoi");

    public final StringPath section = createString("section");

    public QVCreditsExtourne(String variable) {
        super(QVCreditsExtourne.class, forVariable(variable), "GFC", "V_CREDITS_EXTOURNE");
        addMetadata();
    }

    public QVCreditsExtourne(String variable, String schema, String table) {
        super(QVCreditsExtourne.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCreditsExtourne(Path<? extends QVCreditsExtourne> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CREDITS_EXTOURNE");
        addMetadata();
    }

    public QVCreditsExtourne(PathMetadata<?> metadata) {
        super(QVCreditsExtourne.class, metadata, "GFC", "V_CREDITS_EXTOURNE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20));
        addMetadata(quoi, ColumnMetadata.named("QUOI").withIndex(4).ofType(Types.VARCHAR).withSize(1));
        addMetadata(section, ColumnMetadata.named("SECTION").withIndex(5).ofType(Types.CHAR).withSize(1));
    }

}

