package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVActifInventaire2 is a Querydsl query type for QImmVActifInventaire2
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVActifInventaire2 extends com.mysema.query.sql.RelationalPathBase<QImmVActifInventaire2> {

    private static final long serialVersionUID = 253170108;

    public static final QImmVActifInventaire2 immVActifInventaire2 = new QImmVActifInventaire2("IMM_V_ACTIF_INVENTAIRE_2");

    public final NumberPath<Long> amoAcquisition = createNumber("amoAcquisition", Long.class);

    public final NumberPath<Long> amoAnnuite = createNumber("amoAnnuite", Long.class);

    public final NumberPath<Long> amoCumul = createNumber("amoCumul", Long.class);

    public final NumberPath<Integer> amoExer = createNumber("amoExer", Integer.class);

    public final NumberPath<Long> amoResiduel = createNumber("amoResiduel", Long.class);

    public final NumberPath<Long> exercice = createNumber("exercice", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> invcAcquisitionCorrige = createNumber("invcAcquisitionCorrige", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final StringPath numero = createString("numero");

    public final StringPath pcoNum = createString("pcoNum");

    public QImmVActifInventaire2(String variable) {
        super(QImmVActifInventaire2.class, forVariable(variable), "GFC", "IMM_V_ACTIF_INVENTAIRE_2");
        addMetadata();
    }

    public QImmVActifInventaire2(String variable, String schema, String table) {
        super(QImmVActifInventaire2.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVActifInventaire2(Path<? extends QImmVActifInventaire2> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_ACTIF_INVENTAIRE_2");
        addMetadata();
    }

    public QImmVActifInventaire2(PathMetadata<?> metadata) {
        super(QImmVActifInventaire2.class, metadata, "GFC", "IMM_V_ACTIF_INVENTAIRE_2");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(amoAcquisition, ColumnMetadata.named("AMO_ACQUISITION").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(amoAnnuite, ColumnMetadata.named("AMO_ANNUITE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(amoCumul, ColumnMetadata.named("AMO_CUMUL").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(amoExer, ColumnMetadata.named("AMO_EXER").withIndex(4).ofType(Types.DECIMAL).withSize(4));
        addMetadata(amoResiduel, ColumnMetadata.named("AMO_RESIDUEL").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(invcAcquisitionCorrige, ColumnMetadata.named("INVC_ACQUISITION_CORRIGE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(11).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(numero, ColumnMetadata.named("NUMERO").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(9).ofType(Types.VARCHAR).withSize(20));
    }

}

