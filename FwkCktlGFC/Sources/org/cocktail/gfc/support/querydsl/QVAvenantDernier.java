package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAvenantDernier is a Querydsl query type for QVAvenantDernier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAvenantDernier extends com.mysema.query.sql.RelationalPathBase<QVAvenantDernier> {

    private static final long serialVersionUID = 480619621;

    public static final QVAvenantDernier vAvenantDernier = new QVAvenantDernier("V_AVENANT_DERNIER");

    public final NumberPath<Long> avtOrdre = createNumber("avtOrdre", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public QVAvenantDernier(String variable) {
        super(QVAvenantDernier.class, forVariable(variable), "GFC", "V_AVENANT_DERNIER");
        addMetadata();
    }

    public QVAvenantDernier(String variable, String schema, String table) {
        super(QVAvenantDernier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAvenantDernier(Path<? extends QVAvenantDernier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_AVENANT_DERNIER");
        addMetadata();
    }

    public QVAvenantDernier(PathMetadata<?> metadata) {
        super(QVAvenantDernier.class, metadata, "GFC", "V_AVENANT_DERNIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(avtOrdre, ColumnMetadata.named("AVT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

