package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVCommEngageCtrlPlanco is a Querydsl query type for QImmVCommEngageCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVCommEngageCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QImmVCommEngageCtrlPlanco> {

    private static final long serialVersionUID = -2114966437;

    public static final QImmVCommEngageCtrlPlanco immVCommEngageCtrlPlanco = new QImmVCommEngageCtrlPlanco("IMM_V_COMM_ENGAGE_CTRL_PLANCO");

    public final DateTimePath<java.sql.Timestamp> commDate = createDateTime("commDate", java.sql.Timestamp.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final StringPath commLibelle = createString("commLibelle");

    public final NumberPath<Long> commNumero = createNumber("commNumero", Long.class);

    public final NumberPath<Long> devId = createNumber("devId", Long.class);

    public final NumberPath<Long> epcoId = createNumber("epcoId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<java.math.BigDecimal> tapTaux = createNumber("tapTaux", java.math.BigDecimal.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QImmVCommEngageCtrlPlanco(String variable) {
        super(QImmVCommEngageCtrlPlanco.class, forVariable(variable), "GFC", "IMM_V_COMM_ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public QImmVCommEngageCtrlPlanco(String variable, String schema, String table) {
        super(QImmVCommEngageCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVCommEngageCtrlPlanco(Path<? extends QImmVCommEngageCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_COMM_ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public QImmVCommEngageCtrlPlanco(PathMetadata<?> metadata) {
        super(QImmVCommEngageCtrlPlanco.class, metadata, "GFC", "IMM_V_COMM_ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commDate, ColumnMetadata.named("COMM_DATE").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commLibelle, ColumnMetadata.named("COMM_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(commNumero, ColumnMetadata.named("COMM_NUMERO").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(devId, ColumnMetadata.named("DEV_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(epcoId, ColumnMetadata.named("EPCO_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(6).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tapTaux, ColumnMetadata.named("TAP_TAUX").withIndex(13).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(11).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

