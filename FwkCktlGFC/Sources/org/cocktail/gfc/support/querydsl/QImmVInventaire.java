package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVInventaire is a Querydsl query type for QImmVInventaire
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVInventaire extends com.mysema.query.sql.RelationalPathBase<QImmVInventaire> {

    private static final long serialVersionUID = -1421379709;

    public static final QImmVInventaire immVInventaire = new QImmVInventaire("IMM_V_INVENTAIRE");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final StringPath clicNumComplet = createString("clicNumComplet");

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> invId = createNumber("invId", Long.class);

    public final NumberPath<java.math.BigDecimal> lidMontant = createNumber("lidMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> lidOrdre = createNumber("lidOrdre", Long.class);

    public final NumberPath<Long> livOrdre = createNumber("livOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QImmVInventaire(String variable) {
        super(QImmVInventaire.class, forVariable(variable), "GFC", "IMM_V_INVENTAIRE");
        addMetadata();
    }

    public QImmVInventaire(String variable, String schema, String table) {
        super(QImmVInventaire.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVInventaire(Path<? extends QImmVInventaire> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_INVENTAIRE");
        addMetadata();
    }

    public QImmVInventaire(PathMetadata<?> metadata) {
        super(QImmVInventaire.class, metadata, "GFC", "IMM_V_INVENTAIRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clicNumComplet, ColumnMetadata.named("CLIC_NUM_COMPLET").withIndex(11).ofType(Types.VARCHAR).withSize(200));
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invId, ColumnMetadata.named("INV_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lidMontant, ColumnMetadata.named("LID_MONTANT").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(lidOrdre, ColumnMetadata.named("LID_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(livOrdre, ColumnMetadata.named("LIV_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(9).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
    }

}

