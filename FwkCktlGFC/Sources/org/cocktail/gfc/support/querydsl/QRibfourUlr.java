package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRibfourUlr is a Querydsl query type for QRibfourUlr
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRibfourUlr extends com.mysema.query.sql.RelationalPathBase<QRibfourUlr> {

    private static final long serialVersionUID = -1935778293;

    public static final QRibfourUlr ribfourUlr = new QRibfourUlr("RIBFOUR_ULR");

    public final NumberPath<Long> banqOrdre = createNumber("banqOrdre", Long.class);

    public final StringPath bic = createString("bic");

    public final StringPath cBanque = createString("cBanque");

    public final StringPath cGuichet = createString("cGuichet");

    public final StringPath cleRib = createString("cleRib");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath iban = createString("iban");

    public final StringPath modCode = createString("modCode");

    public final StringPath noCompte = createString("noCompte");

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final NumberPath<Long> ribOrdre = createNumber("ribOrdre", Long.class);

    public final StringPath ribTitco = createString("ribTitco");

    public final StringPath ribValide = createString("ribValide");

    public final StringPath temPayeUtil = createString("temPayeUtil");

    public final com.mysema.query.sql.PrimaryKey<QRibfourUlr> ribfourUlrPk = createPrimaryKey(ribOrdre);

    public QRibfourUlr(String variable) {
        super(QRibfourUlr.class, forVariable(variable), "GRHUM", "RIBFOUR_ULR");
        addMetadata();
    }

    public QRibfourUlr(String variable, String schema, String table) {
        super(QRibfourUlr.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRibfourUlr(Path<? extends QRibfourUlr> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "RIBFOUR_ULR");
        addMetadata();
    }

    public QRibfourUlr(PathMetadata<?> metadata) {
        super(QRibfourUlr.class, metadata, "GRHUM", "RIBFOUR_ULR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(banqOrdre, ColumnMetadata.named("BANQ_ORDRE").withIndex(17).ofType(Types.DECIMAL).withSize(0));
        addMetadata(bic, ColumnMetadata.named("BIC").withIndex(13).ofType(Types.VARCHAR).withSize(11));
        addMetadata(cBanque, ColumnMetadata.named("C_BANQUE").withIndex(3).ofType(Types.VARCHAR).withSize(5));
        addMetadata(cGuichet, ColumnMetadata.named("C_GUICHET").withIndex(4).ofType(Types.VARCHAR).withSize(5));
        addMetadata(cleRib, ColumnMetadata.named("CLE_RIB").withIndex(6).ofType(Types.VARCHAR).withSize(2));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(iban, ColumnMetadata.named("IBAN").withIndex(14).ofType(Types.VARCHAR).withSize(34));
        addMetadata(modCode, ColumnMetadata.named("MOD_CODE").withIndex(8).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(noCompte, ColumnMetadata.named("NO_COMPTE").withIndex(5).ofType(Types.VARCHAR).withSize(11));
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ribOrdre, ColumnMetadata.named("RIB_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(ribTitco, ColumnMetadata.named("RIB_TITCO").withIndex(7).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(ribValide, ColumnMetadata.named("RIB_VALIDE").withIndex(9).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(temPayeUtil, ColumnMetadata.named("TEM_PAYE_UTIL").withIndex(12).ofType(Types.VARCHAR).withSize(1).notNull());
    }

}

