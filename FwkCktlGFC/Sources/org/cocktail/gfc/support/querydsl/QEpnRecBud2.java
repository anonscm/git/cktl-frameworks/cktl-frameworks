package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnRecBud2 is a Querydsl query type for QEpnRecBud2
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnRecBud2 extends com.mysema.query.sql.RelationalPathBase<QEpnRecBud2> {

    private static final long serialVersionUID = -584013393;

    public static final QEpnRecBud2 epnRecBud2 = new QEpnRecBud2("EPN_REC_BUD_2");

    public final StringPath epnrb2CodBud = createString("epnrb2CodBud");

    public final StringPath epnrb2Compte = createString("epnrb2Compte");

    public final NumberPath<Integer> epnrb2Exercice = createNumber("epnrb2Exercice", Integer.class);

    public final StringPath epnrb2GesCode = createString("epnrb2GesCode");

    public final NumberPath<Double> epnrb2Mntannul = createNumber("epnrb2Mntannul", Double.class);

    public final NumberPath<Integer> epnrb2Numero = createNumber("epnrb2Numero", Integer.class);

    public final NumberPath<Double> epnrb2Recbrut = createNumber("epnrb2Recbrut", Double.class);

    public final NumberPath<Double> epnrb2RecExt = createNumber("epnrb2RecExt", Double.class);

    public final NumberPath<Double> epnrb2Recnet = createNumber("epnrb2Recnet", Double.class);

    public final NumberPath<Double> epnrb2RecPreext = createNumber("epnrb2RecPreext", Double.class);

    public final StringPath epnrb2Type = createString("epnrb2Type");

    public final NumberPath<Integer> epnrb2TypeCpt = createNumber("epnrb2TypeCpt", Integer.class);

    public final StringPath epnrb2TypeDoc = createString("epnrb2TypeDoc");

    public final StringPath epnrbOrdre = createString("epnrbOrdre");

    public QEpnRecBud2(String variable) {
        super(QEpnRecBud2.class, forVariable(variable), "GFC", "EPN_REC_BUD_2");
        addMetadata();
    }

    public QEpnRecBud2(String variable, String schema, String table) {
        super(QEpnRecBud2.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnRecBud2(Path<? extends QEpnRecBud2> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_REC_BUD_2");
        addMetadata();
    }

    public QEpnRecBud2(PathMetadata<?> metadata) {
        super(QEpnRecBud2.class, metadata, "GFC", "EPN_REC_BUD_2");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epnrb2CodBud, ColumnMetadata.named("EPNRB2_COD_BUD").withIndex(13).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnrb2Compte, ColumnMetadata.named("EPNRB2_COMPTE").withIndex(6).ofType(Types.VARCHAR).withSize(15));
        addMetadata(epnrb2Exercice, ColumnMetadata.named("EPNRB2_EXERCICE").withIndex(12).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epnrb2GesCode, ColumnMetadata.named("EPNRB2_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epnrb2Mntannul, ColumnMetadata.named("EPNRB2_MNTANNUL").withIndex(9).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnrb2Numero, ColumnMetadata.named("EPNRB2_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnrb2Recbrut, ColumnMetadata.named("EPNRB2_RECBRUT").withIndex(7).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnrb2RecExt, ColumnMetadata.named("EPNRB2_REC_EXT").withIndex(10).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnrb2Recnet, ColumnMetadata.named("EPNRB2_RECNET").withIndex(11).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnrb2RecPreext, ColumnMetadata.named("EPNRB2_REC_PREEXT").withIndex(8).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnrb2Type, ColumnMetadata.named("EPNRB2_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epnrb2TypeCpt, ColumnMetadata.named("EPNRB2_TYPE_CPT").withIndex(5).ofType(Types.DECIMAL).withSize(1));
        addMetadata(epnrb2TypeDoc, ColumnMetadata.named("EPNRB2_TYPE_DOC").withIndex(14).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnrbOrdre, ColumnMetadata.named("EPNRB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

