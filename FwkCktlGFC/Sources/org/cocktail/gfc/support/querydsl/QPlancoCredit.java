package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPlancoCredit is a Querydsl query type for QPlancoCredit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPlancoCredit extends com.mysema.query.sql.RelationalPathBase<QPlancoCredit> {

    private static final long serialVersionUID = -787354577;

    public static final QPlancoCredit plancoCredit = new QPlancoCredit("PLANCO_CREDIT");

    public final StringPath pccEtat = createString("pccEtat");

    public final NumberPath<Long> pccOrdre = createNumber("pccOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath plaQuoi = createString("plaQuoi");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPlancoCredit> primPlcr = createPrimaryKey(pccOrdre);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> plancoCreditPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> plancoCreditTcdOrdreFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public QPlancoCredit(String variable) {
        super(QPlancoCredit.class, forVariable(variable), "GFC", "PLANCO_CREDIT");
        addMetadata();
    }

    public QPlancoCredit(String variable, String schema, String table) {
        super(QPlancoCredit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPlancoCredit(Path<? extends QPlancoCredit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PLANCO_CREDIT");
        addMetadata();
    }

    public QPlancoCredit(PathMetadata<?> metadata) {
        super(QPlancoCredit.class, metadata, "GFC", "PLANCO_CREDIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pccEtat, ColumnMetadata.named("PCC_ETAT").withIndex(5).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pccOrdre, ColumnMetadata.named("PCC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(plaQuoi, ColumnMetadata.named("PLA_QUOI").withIndex(4).ofType(Types.CHAR).withSize(1));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

