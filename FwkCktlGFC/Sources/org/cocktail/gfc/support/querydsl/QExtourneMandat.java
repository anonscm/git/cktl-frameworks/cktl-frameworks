package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QExtourneMandat is a Querydsl query type for QExtourneMandat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QExtourneMandat extends com.mysema.query.sql.RelationalPathBase<QExtourneMandat> {

    private static final long serialVersionUID = -118193568;

    public static final QExtourneMandat extourneMandat = new QExtourneMandat("EXTOURNE_MANDAT");

    public final NumberPath<Long> emId = createNumber("emId", Long.class);

    public final NumberPath<Long> manIdN = createNumber("manIdN", Long.class);

    public final NumberPath<Long> manIdN1 = createNumber("manIdN1", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QExtourneMandat> sysC0076474 = createPrimaryKey(emId);

    public final com.mysema.query.sql.ForeignKey<QMandat> extourneMandatManidnFk = createForeignKey(manIdN, "MAN_ID");

    public final com.mysema.query.sql.ForeignKey<QMandat> extourneMandatManidn1Fk = createForeignKey(manIdN1, "MAN_ID");

    public QExtourneMandat(String variable) {
        super(QExtourneMandat.class, forVariable(variable), "GFC", "EXTOURNE_MANDAT");
        addMetadata();
    }

    public QExtourneMandat(String variable, String schema, String table) {
        super(QExtourneMandat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QExtourneMandat(Path<? extends QExtourneMandat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EXTOURNE_MANDAT");
        addMetadata();
    }

    public QExtourneMandat(PathMetadata<?> metadata) {
        super(QExtourneMandat.class, metadata, "GFC", "EXTOURNE_MANDAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(emId, ColumnMetadata.named("EM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manIdN, ColumnMetadata.named("MAN_ID_N").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(manIdN1, ColumnMetadata.named("MAN_ID_N1").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

