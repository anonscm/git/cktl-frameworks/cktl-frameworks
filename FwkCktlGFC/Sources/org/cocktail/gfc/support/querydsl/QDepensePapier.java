package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepensePapier is a Querydsl query type for QDepensePapier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepensePapier extends com.mysema.query.sql.RelationalPathBase<QDepensePapier> {

    private static final long serialVersionUID = -623973280;

    public static final QDepensePapier depensePapier = new QDepensePapier("DEPENSE_PAPIER");

    public final DateTimePath<java.sql.Timestamp> dppDateFacture = createDateTime("dppDateFacture", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateReception = createDateTime("dppDateReception", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateSaisie = createDateTime("dppDateSaisie", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateServiceFait = createDateTime("dppDateServiceFait", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> dppHtInitial = createNumber("dppHtInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppHtSaisie = createNumber("dppHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> dppIdReversement = createNumber("dppIdReversement", Long.class);

    public final NumberPath<Long> dppImDgp = createNumber("dppImDgp", Long.class);

    public final NumberPath<java.math.BigDecimal> dppImTaux = createNumber("dppImTaux", java.math.BigDecimal.class);

    public final NumberPath<Long> dppNbPiece = createNumber("dppNbPiece", Long.class);

    public final StringPath dppNumeroFacture = createString("dppNumeroFacture");

    public final DateTimePath<java.sql.Timestamp> dppSfDate = createDateTime("dppSfDate", java.sql.Timestamp.class);

    public final NumberPath<Long> dppSfPersId = createNumber("dppSfPersId", Long.class);

    public final NumberPath<java.math.BigDecimal> dppTtcInitial = createNumber("dppTtcInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTtcSaisie = createNumber("dppTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTvaInitial = createNumber("dppTvaInitial", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTvaSaisie = createNumber("dppTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> imttId = createNumber("imttId", Long.class);

    public final NumberPath<Long> modOrdre = createNumber("modOrdre", Long.class);

    public final NumberPath<Long> ribOrdre = createNumber("ribOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDepensePapier> depensePapierPk = createPrimaryKey(dppId);

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> depensePapierUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QModePaiement> depensePapierModOrdreFk = createForeignKey(modOrdre, "MOD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepensePapier> depensePapierDppIdRevFk = createForeignKey(dppIdReversement, "DPP_ID");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> depensePapierFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPersonne> dppSfPersonneFk = createForeignKey(dppSfPersId, "PERS_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmImTypeTaux> depensePapierImttIdFk = createForeignKey(imttId, "IMTT_ID");

    public final com.mysema.query.sql.ForeignKey<QRibfourUlr> depensePapierRibOrdreFk = createForeignKey(ribOrdre, "RIB_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depensePapierExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> _pdepenseBudgetDppIdFk = createInvForeignKey(dppId, "DPP_ID");

    public final com.mysema.query.sql.ForeignKey<QImSuspension> _imSuspensDppIdFk = createInvForeignKey(dppId, "DPP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepensePapier> _depensePapierDppIdRevFk = createInvForeignKey(dppId, "DPP_ID_REVERSEMENT");

    public final com.mysema.query.sql.ForeignKey<QCommandeDepPapier> _commandeDepPapierDppIdFk = createInvForeignKey(dppId, "DPP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> _depenseBudgetDppIdFk = createInvForeignKey(dppId, "DPP_ID");

    public QDepensePapier(String variable) {
        super(QDepensePapier.class, forVariable(variable), "GFC", "DEPENSE_PAPIER");
        addMetadata();
    }

    public QDepensePapier(String variable, String schema, String table) {
        super(QDepensePapier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepensePapier(Path<? extends QDepensePapier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_PAPIER");
        addMetadata();
    }

    public QDepensePapier(PathMetadata<?> metadata) {
        super(QDepensePapier.class, metadata, "GFC", "DEPENSE_PAPIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dppDateFacture, ColumnMetadata.named("DPP_DATE_FACTURE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateReception, ColumnMetadata.named("DPP_DATE_RECEPTION").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateSaisie, ColumnMetadata.named("DPP_DATE_SAISIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateServiceFait, ColumnMetadata.named("DPP_DATE_SERVICE_FAIT").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppHtInitial, ColumnMetadata.named("DPP_HT_INITIAL").withIndex(17).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppHtSaisie, ColumnMetadata.named("DPP_HT_SAISIE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppIdReversement, ColumnMetadata.named("DPP_ID_REVERSEMENT").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dppImDgp, ColumnMetadata.named("DPP_IM_DGP").withIndex(21).ofType(Types.DECIMAL).withSize(10));
        addMetadata(dppImTaux, ColumnMetadata.named("DPP_IM_TAUX").withIndex(20).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(dppNbPiece, ColumnMetadata.named("DPP_NB_PIECE").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(dppSfDate, ColumnMetadata.named("DPP_SF_DATE").withIndex(24).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppSfPersId, ColumnMetadata.named("DPP_SF_PERS_ID").withIndex(23).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dppTtcInitial, ColumnMetadata.named("DPP_TTC_INITIAL").withIndex(19).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTtcSaisie, ColumnMetadata.named("DPP_TTC_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTvaInitial, ColumnMetadata.named("DPP_TVA_INITIAL").withIndex(18).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTvaSaisie, ColumnMetadata.named("DPP_TVA_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(25).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(imttId, ColumnMetadata.named("IMTT_ID").withIndex(22).ofType(Types.DECIMAL).withSize(10));
        addMetadata(modOrdre, ColumnMetadata.named("MOD_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ribOrdre, ColumnMetadata.named("RIB_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

