package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeRetenue is a Querydsl query type for QTypeRetenue
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeRetenue extends com.mysema.query.sql.RelationalPathBase<QTypeRetenue> {

    private static final long serialVersionUID = 271318239;

    public static final QTypeRetenue typeRetenue = new QTypeRetenue("TYPE_RETENUE");

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath treLibelle = createString("treLibelle");

    public final NumberPath<Long> treOrdre = createNumber("treOrdre", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeRetenue> typeRetenuePk = createPrimaryKey(treOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> typeRetenueTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> typeRetenuePcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QRetenue> _retenueTreOrdreFk = createInvForeignKey(treOrdre, "TRE_ORDRE");

    public QTypeRetenue(String variable) {
        super(QTypeRetenue.class, forVariable(variable), "GFC", "TYPE_RETENUE");
        addMetadata();
    }

    public QTypeRetenue(String variable, String schema, String table) {
        super(QTypeRetenue.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeRetenue(Path<? extends QTypeRetenue> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_RETENUE");
        addMetadata();
    }

    public QTypeRetenue(PathMetadata<?> metadata) {
        super(QTypeRetenue.class, metadata, "GFC", "TYPE_RETENUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(treLibelle, ColumnMetadata.named("TRE_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(treOrdre, ColumnMetadata.named("TRE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

