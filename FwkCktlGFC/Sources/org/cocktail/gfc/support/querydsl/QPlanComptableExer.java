package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPlanComptableExer is a Querydsl query type for QPlanComptableExer
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPlanComptableExer extends com.mysema.query.sql.RelationalPathBase<QPlanComptableExer> {

    private static final long serialVersionUID = -333184043;

    public static final QPlanComptableExer planComptableExer = new QPlanComptableExer("PLAN_COMPTABLE_EXER");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath pcoBudgetaire = createString("pcoBudgetaire");

    public final StringPath pcoCompteBe = createString("pcoCompteBe");

    public final NumberPath<Long> pcoeId = createNumber("pcoeId", Long.class);

    public final StringPath pcoEmargement = createString("pcoEmargement");

    public final StringPath pcoJBe = createString("pcoJBe");

    public final StringPath pcoJExercice = createString("pcoJExercice");

    public final StringPath pcoJFinExercice = createString("pcoJFinExercice");

    public final StringPath pcoLibelle = createString("pcoLibelle");

    public final StringPath pcoNature = createString("pcoNature");

    public final NumberPath<Long> pcoNiveau = createNumber("pcoNiveau", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath pcoSensEmargement = createString("pcoSensEmargement");

    public final StringPath pcoSensSolde = createString("pcoSensSolde");

    public final StringPath pcoValidite = createString("pcoValidite");

    public final com.mysema.query.sql.PrimaryKey<QPlanComptableExer> planComptableExerPk = createPrimaryKey(pcoeId);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> plancoExerPcoCompteBeFk = createForeignKey(pcoCompteBe, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> plancoExerExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> plancoExerPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QAdmOriNatRecPcptExer> _admOriNatRecPcptExPceFk = createInvForeignKey(pcoeId, "PCOE_ID");

    public QPlanComptableExer(String variable) {
        super(QPlanComptableExer.class, forVariable(variable), "GFC", "PLAN_COMPTABLE_EXER");
        addMetadata();
    }

    public QPlanComptableExer(String variable, String schema, String table) {
        super(QPlanComptableExer.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPlanComptableExer(Path<? extends QPlanComptableExer> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PLAN_COMPTABLE_EXER");
        addMetadata();
    }

    public QPlanComptableExer(PathMetadata<?> metadata) {
        super(QPlanComptableExer.class, metadata, "GFC", "PLAN_COMPTABLE_EXER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(pcoBudgetaire, ColumnMetadata.named("PCO_BUDGETAIRE").withIndex(5).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(pcoCompteBe, ColumnMetadata.named("PCO_COMPTE_BE").withIndex(14).ofType(Types.VARCHAR).withSize(20));
        addMetadata(pcoeId, ColumnMetadata.named("PCOE_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoEmargement, ColumnMetadata.named("PCO_EMARGEMENT").withIndex(6).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(pcoJBe, ColumnMetadata.named("PCO_J_BE").withIndex(13).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoJExercice, ColumnMetadata.named("PCO_J_EXERCICE").withIndex(11).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoJFinExercice, ColumnMetadata.named("PCO_J_FIN_EXERCICE").withIndex(12).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoLibelle, ColumnMetadata.named("PCO_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(pcoNature, ColumnMetadata.named("PCO_NATURE").withIndex(8).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(pcoNiveau, ColumnMetadata.named("PCO_NIVEAU").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pcoSensEmargement, ColumnMetadata.named("PCO_SENS_EMARGEMENT").withIndex(9).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoSensSolde, ColumnMetadata.named("PCO_SENS_SOLDE").withIndex(15).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(pcoValidite, ColumnMetadata.named("PCO_VALIDITE").withIndex(10).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

