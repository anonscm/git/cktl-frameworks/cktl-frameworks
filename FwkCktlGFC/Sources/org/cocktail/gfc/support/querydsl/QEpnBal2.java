package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnBal2 is a Querydsl query type for QEpnBal2
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnBal2 extends com.mysema.query.sql.RelationalPathBase<QEpnBal2> {

    private static final long serialVersionUID = 1266898279;

    public static final QEpnBal2 epnBal2 = new QEpnBal2("EPN_BAL_2");

    public final NumberPath<Double> epnb2Bscre = createNumber("epnb2Bscre", Double.class);

    public final NumberPath<Double> epnb2Bsdeb = createNumber("epnb2Bsdeb", Double.class);

    public final StringPath epnb2CodBud = createString("epnb2CodBud");

    public final StringPath epnb2Compte = createString("epnb2Compte");

    public final NumberPath<Double> epnb2Crebe = createNumber("epnb2Crebe", Double.class);

    public final NumberPath<Double> epnb2Crecum = createNumber("epnb2Crecum", Double.class);

    public final NumberPath<Double> epnb2Cretot = createNumber("epnb2Cretot", Double.class);

    public final NumberPath<Double> epnb2Debbe = createNumber("epnb2Debbe", Double.class);

    public final NumberPath<Double> epnb2Debcum = createNumber("epnb2Debcum", Double.class);

    public final NumberPath<Double> epnb2Debtot = createNumber("epnb2Debtot", Double.class);

    public final NumberPath<Integer> epnb2Exercice = createNumber("epnb2Exercice", Integer.class);

    public final StringPath epnb2GesCode = createString("epnb2GesCode");

    public final NumberPath<Integer> epnb2Numero = createNumber("epnb2Numero", Integer.class);

    public final StringPath epnb2Type = createString("epnb2Type");

    public final NumberPath<Integer> epnb2TypeCpt = createNumber("epnb2TypeCpt", Integer.class);

    public final StringPath epnb2TypeDoc = createString("epnb2TypeDoc");

    public final StringPath epnbOrdre = createString("epnbOrdre");

    public QEpnBal2(String variable) {
        super(QEpnBal2.class, forVariable(variable), "GFC", "EPN_BAL_2");
        addMetadata();
    }

    public QEpnBal2(String variable, String schema, String table) {
        super(QEpnBal2.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnBal2(Path<? extends QEpnBal2> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_BAL_2");
        addMetadata();
    }

    public QEpnBal2(PathMetadata<?> metadata) {
        super(QEpnBal2.class, metadata, "GFC", "EPN_BAL_2");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epnb2Bscre, ColumnMetadata.named("EPNB2_BSCRE").withIndex(14).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2Bsdeb, ColumnMetadata.named("EPNB2_BSDEB").withIndex(13).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2CodBud, ColumnMetadata.named("EPNB2_COD_BUD").withIndex(16).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnb2Compte, ColumnMetadata.named("EPNB2_COMPTE").withIndex(6).ofType(Types.VARCHAR).withSize(15));
        addMetadata(epnb2Crebe, ColumnMetadata.named("EPNB2_CREBE").withIndex(10).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2Crecum, ColumnMetadata.named("EPNB2_CRECUM").withIndex(11).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2Cretot, ColumnMetadata.named("EPNB2_CRETOT").withIndex(12).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2Debbe, ColumnMetadata.named("EPNB2_DEBBE").withIndex(7).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2Debcum, ColumnMetadata.named("EPNB2_DEBCUM").withIndex(8).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2Debtot, ColumnMetadata.named("EPNB2_DEBTOT").withIndex(9).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnb2Exercice, ColumnMetadata.named("EPNB2_EXERCICE").withIndex(15).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epnb2GesCode, ColumnMetadata.named("EPNB2_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epnb2Numero, ColumnMetadata.named("EPNB2_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnb2Type, ColumnMetadata.named("EPNB2_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epnb2TypeCpt, ColumnMetadata.named("EPNB2_TYPE_CPT").withIndex(5).ofType(Types.DECIMAL).withSize(1));
        addMetadata(epnb2TypeDoc, ColumnMetadata.named("EPNB2_TYPE_DOC").withIndex(17).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnbOrdre, ColumnMetadata.named("EPNB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

