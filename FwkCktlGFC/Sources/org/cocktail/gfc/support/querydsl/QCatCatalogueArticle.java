package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCatCatalogueArticle is a Querydsl query type for QCatCatalogueArticle
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCatCatalogueArticle extends com.mysema.query.sql.RelationalPathBase<QCatCatalogueArticle> {

    private static final long serialVersionUID = 602054050;

    public static final QCatCatalogueArticle catCatalogueArticle = new QCatCatalogueArticle("CAT_CATALOGUE_ARTICLE");

    public final NumberPath<Long> artId = createNumber("artId", Long.class);

    public final NumberPath<Long> caarDocId = createNumber("caarDocId", Long.class);

    public final StringPath caarDocReference = createString("caarDocReference");

    public final NumberPath<Long> caarId = createNumber("caarId", Long.class);

    public final NumberPath<Double> caarPrixHt = createNumber("caarPrixHt", Double.class);

    public final NumberPath<Double> caarPrixTtc = createNumber("caarPrixTtc", Double.class);

    public final StringPath caarReference = createString("caarReference");

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final NumberPath<Long> tvaId = createNumber("tvaId", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCatCatalogueArticle> catCatalogueArticlePk = createPrimaryKey(caarId);

    public final com.mysema.query.sql.ForeignKey<QCatArticle> catCatalogueArticleArtFk = createForeignKey(artId, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTva> catCatalogueArticleTvaFk = createForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> catCatalogueArticleTyetFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogue> catCatalogueArticleCatFk = createForeignKey(catId, "CAT_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestationLigne> _prligCaarIdFk = createInvForeignKey(caarId, "CAAR_ID");

    public final com.mysema.query.sql.ForeignKey<QDepArticle> _depArticleArtcIdFk = createInvForeignKey(caarId, "ARTC_ID");

    public QCatCatalogueArticle(String variable) {
        super(QCatCatalogueArticle.class, forVariable(variable), "GFC", "CAT_CATALOGUE_ARTICLE");
        addMetadata();
    }

    public QCatCatalogueArticle(String variable, String schema, String table) {
        super(QCatCatalogueArticle.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCatCatalogueArticle(Path<? extends QCatCatalogueArticle> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CAT_CATALOGUE_ARTICLE");
        addMetadata();
    }

    public QCatCatalogueArticle(PathMetadata<?> metadata) {
        super(QCatCatalogueArticle.class, metadata, "GFC", "CAT_CATALOGUE_ARTICLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(artId, ColumnMetadata.named("ART_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(caarDocId, ColumnMetadata.named("CAAR_DOC_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(caarDocReference, ColumnMetadata.named("CAAR_DOC_REFERENCE").withIndex(10).ofType(Types.VARCHAR).withSize(500));
        addMetadata(caarId, ColumnMetadata.named("CAAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(caarPrixHt, ColumnMetadata.named("CAAR_PRIX_HT").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(3).notNull());
        addMetadata(caarPrixTtc, ColumnMetadata.named("CAAR_PRIX_TTC").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(3).notNull());
        addMetadata(caarReference, ColumnMetadata.named("CAAR_REFERENCE").withIndex(4).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

