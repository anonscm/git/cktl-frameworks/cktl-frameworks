package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCataloguePrestationWeb is a Querydsl query type for QCataloguePrestationWeb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCataloguePrestationWeb extends com.mysema.query.sql.RelationalPathBase<QCataloguePrestationWeb> {

    private static final long serialVersionUID = -1920321349;

    public static final QCataloguePrestationWeb cataloguePrestationWeb = new QCataloguePrestationWeb("CATALOGUE_PRESTATION_WEB");

    public final StringPath catCle = createString("catCle");

    public final StringPath catCommantaire = createString("catCommantaire");

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final NumberPath<Long> catPwebId = createNumber("catPwebId", Long.class);

    public final StringPath catValeur = createString("catValeur");

    public final com.mysema.query.sql.PrimaryKey<QCataloguePrestationWeb> cataloguePrestationWebPk = createPrimaryKey(catPwebId);

    public QCataloguePrestationWeb(String variable) {
        super(QCataloguePrestationWeb.class, forVariable(variable), "GFC", "CATALOGUE_PRESTATION_WEB");
        addMetadata();
    }

    public QCataloguePrestationWeb(String variable, String schema, String table) {
        super(QCataloguePrestationWeb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCataloguePrestationWeb(Path<? extends QCataloguePrestationWeb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CATALOGUE_PRESTATION_WEB");
        addMetadata();
    }

    public QCataloguePrestationWeb(PathMetadata<?> metadata) {
        super(QCataloguePrestationWeb.class, metadata, "GFC", "CATALOGUE_PRESTATION_WEB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(catCle, ColumnMetadata.named("CAT_CLE").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(catCommantaire, ColumnMetadata.named("CAT_COMMANTAIRE").withIndex(5).ofType(Types.VARCHAR).withSize(4000));
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catPwebId, ColumnMetadata.named("CAT_PWEB_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catValeur, ColumnMetadata.named("CAT_VALEUR").withIndex(4).ofType(Types.VARCHAR).withSize(4000));
    }

}

