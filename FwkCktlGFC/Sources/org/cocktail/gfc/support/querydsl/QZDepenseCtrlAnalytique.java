package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZDepenseCtrlAnalytique is a Querydsl query type for QZDepenseCtrlAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZDepenseCtrlAnalytique extends com.mysema.query.sql.RelationalPathBase<QZDepenseCtrlAnalytique> {

    private static final long serialVersionUID = -690379513;

    public static final QZDepenseCtrlAnalytique zDepenseCtrlAnalytique = new QZDepenseCtrlAnalytique("Z_DEPENSE_CTRL_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<java.math.BigDecimal> danaHtSaisie = createNumber("danaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> danaId = createNumber("danaId", Long.class);

    public final NumberPath<java.math.BigDecimal> danaMontantBudgetaire = createNumber("danaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> danaTtcSaisie = createNumber("danaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> danaTvaSaisie = createNumber("danaTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> zdanaId = createNumber("zdanaId", Long.class);

    public final NumberPath<Long> zdepId = createNumber("zdepId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZDepenseCtrlAnalytique> sysC0076275 = createPrimaryKey(zdanaId);

    public final com.mysema.query.sql.ForeignKey<QZDepenseBudget> zDepenseCtrlAnalZdepIdFk = createForeignKey(zdepId, "ZDEP_ID");

    public QZDepenseCtrlAnalytique(String variable) {
        super(QZDepenseCtrlAnalytique.class, forVariable(variable), "GFC", "Z_DEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QZDepenseCtrlAnalytique(String variable, String schema, String table) {
        super(QZDepenseCtrlAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZDepenseCtrlAnalytique(Path<? extends QZDepenseCtrlAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_DEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QZDepenseCtrlAnalytique(PathMetadata<?> metadata) {
        super(QZDepenseCtrlAnalytique.class, metadata, "GFC", "Z_DEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(danaHtSaisie, ColumnMetadata.named("DANA_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(danaId, ColumnMetadata.named("DANA_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(danaMontantBudgetaire, ColumnMetadata.named("DANA_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(danaTtcSaisie, ColumnMetadata.named("DANA_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(danaTvaSaisie, ColumnMetadata.named("DANA_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(zdanaId, ColumnMetadata.named("ZDANA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdepId, ColumnMetadata.named("ZDEP_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

