package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVConventionPartenaire is a Querydsl query type for QVConventionPartenaire
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVConventionPartenaire extends com.mysema.query.sql.RelationalPathBase<QVConventionPartenaire> {

    private static final long serialVersionUID = -1841543235;

    public static final QVConventionPartenaire vConventionPartenaire = new QVConventionPartenaire("V_CONVENTION_PARTENAIRE");

    public final SimplePath<Object> avisDefavorable = createSimple("avisDefavorable", Object.class);

    public final SimplePath<Object> avisFavorable = createSimple("avisFavorable", Object.class);

    public final SimplePath<Object> cNaf = createSimple("cNaf", Object.class);

    public final SimplePath<Object> conCr = createSimple("conCr", Object.class);

    public final SimplePath<Object> conDateApurement = createSimple("conDateApurement", Object.class);

    public final SimplePath<Object> conDateCloture = createSimple("conDateCloture", Object.class);

    public final SimplePath<Object> conDateCreation = createSimple("conDateCreation", Object.class);

    public final SimplePath<Object> conDateFinPaiement = createSimple("conDateFinPaiement", Object.class);

    public final SimplePath<Object> conDateModif = createSimple("conDateModif", Object.class);

    public final SimplePath<Object> conDateValidAdm = createSimple("conDateValidAdm", Object.class);

    public final SimplePath<Object> conDuree = createSimple("conDuree", Object.class);

    public final SimplePath<Object> conDureeMois = createSimple("conDureeMois", Object.class);

    public final SimplePath<Object> conEtablissement = createSimple("conEtablissement", Object.class);

    public final SimplePath<Object> conGroupeBud = createSimple("conGroupeBud", Object.class);

    public final SimplePath<Object> conGroupePartenaire = createSimple("conGroupePartenaire", Object.class);

    public final SimplePath<Object> conIndex = createSimple("conIndex", Object.class);

    public final SimplePath<Object> conNature = createSimple("conNature", Object.class);

    public final SimplePath<Object> conObjet = createSimple("conObjet", Object.class);

    public final SimplePath<Object> conObjetCourt = createSimple("conObjetCourt", Object.class);

    public final SimplePath<Object> conObservations = createSimple("conObservations", Object.class);

    public final SimplePath<Object> conReferenceExterne = createSimple("conReferenceExterne", Object.class);

    public final SimplePath<Object> conSuppr = createSimple("conSuppr", Object.class);

    public final SimplePath<Object> contexte = createSimple("contexte", Object.class);

    public final SimplePath<Object> dateMigration = createSimple("dateMigration", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> llPartenaire = createSimple("llPartenaire", Object.class);

    public final SimplePath<Object> motifsAvis = createSimple("motifsAvis", Object.class);

    public final SimplePath<Object> orgIdComposante = createSimple("orgIdComposante", Object.class);

    public final SimplePath<Object> persIdPartenaire = createSimple("persIdPartenaire", Object.class);

    public final SimplePath<Object> persIdService = createSimple("persIdService", Object.class);

    public final SimplePath<Object> remarques = createSimple("remarques", Object.class);

    public final SimplePath<Object> tccId = createSimple("tccId", Object.class);

    public final SimplePath<Object> trOrdre = createSimple("trOrdre", Object.class);

    public final SimplePath<Object> utlOrdreCreation = createSimple("utlOrdreCreation", Object.class);

    public final SimplePath<Object> utlOrdreModif = createSimple("utlOrdreModif", Object.class);

    public final SimplePath<Object> utlOrdreValidAdm = createSimple("utlOrdreValidAdm", Object.class);

    public QVConventionPartenaire(String variable) {
        super(QVConventionPartenaire.class, forVariable(variable), "GFC", "V_CONVENTION_PARTENAIRE");
        addMetadata();
    }

    public QVConventionPartenaire(String variable, String schema, String table) {
        super(QVConventionPartenaire.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVConventionPartenaire(Path<? extends QVConventionPartenaire> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CONVENTION_PARTENAIRE");
        addMetadata();
    }

    public QVConventionPartenaire(PathMetadata<?> metadata) {
        super(QVConventionPartenaire.class, metadata, "GFC", "V_CONVENTION_PARTENAIRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(avisDefavorable, ColumnMetadata.named("AVIS_DEFAVORABLE").withIndex(28).ofType(Types.OTHER).withSize(0));
        addMetadata(avisFavorable, ColumnMetadata.named("AVIS_FAVORABLE").withIndex(27).ofType(Types.OTHER).withSize(0));
        addMetadata(cNaf, ColumnMetadata.named("C_NAF").withIndex(32).ofType(Types.OTHER).withSize(0));
        addMetadata(conCr, ColumnMetadata.named("CON_CR").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(conDateApurement, ColumnMetadata.named("CON_DATE_APUREMENT").withIndex(22).ofType(Types.OTHER).withSize(0));
        addMetadata(conDateCloture, ColumnMetadata.named("CON_DATE_CLOTURE").withIndex(21).ofType(Types.OTHER).withSize(0));
        addMetadata(conDateCreation, ColumnMetadata.named("CON_DATE_CREATION").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(conDateFinPaiement, ColumnMetadata.named("CON_DATE_FIN_PAIEMENT").withIndex(37).ofType(Types.OTHER).withSize(0));
        addMetadata(conDateModif, ColumnMetadata.named("CON_DATE_MODIF").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(conDateValidAdm, ColumnMetadata.named("CON_DATE_VALID_ADM").withIndex(20).ofType(Types.OTHER).withSize(0));
        addMetadata(conDuree, ColumnMetadata.named("CON_DUREE").withIndex(34).ofType(Types.OTHER).withSize(0));
        addMetadata(conDureeMois, ColumnMetadata.named("CON_DUREE_MOIS").withIndex(36).ofType(Types.OTHER).withSize(0));
        addMetadata(conEtablissement, ColumnMetadata.named("CON_ETABLISSEMENT").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(conGroupeBud, ColumnMetadata.named("CON_GROUPE_BUD").withIndex(23).ofType(Types.OTHER).withSize(0));
        addMetadata(conGroupePartenaire, ColumnMetadata.named("CON_GROUPE_PARTENAIRE").withIndex(33).ofType(Types.OTHER).withSize(0));
        addMetadata(conIndex, ColumnMetadata.named("CON_INDEX").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(conNature, ColumnMetadata.named("CON_NATURE").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(conObjet, ColumnMetadata.named("CON_OBJET").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(conObjetCourt, ColumnMetadata.named("CON_OBJET_COURT").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(conObservations, ColumnMetadata.named("CON_OBSERVATIONS").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(conReferenceExterne, ColumnMetadata.named("CON_REFERENCE_EXTERNE").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(conSuppr, ColumnMetadata.named("CON_SUPPR").withIndex(24).ofType(Types.OTHER).withSize(0));
        addMetadata(contexte, ColumnMetadata.named("CONTEXTE").withIndex(29).ofType(Types.OTHER).withSize(0));
        addMetadata(dateMigration, ColumnMetadata.named("DATE_MIGRATION").withIndex(35).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(llPartenaire, ColumnMetadata.named("LL_PARTENAIRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(motifsAvis, ColumnMetadata.named("MOTIFS_AVIS").withIndex(31).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdComposante, ColumnMetadata.named("ORG_ID_COMPOSANTE").withIndex(25).ofType(Types.OTHER).withSize(0));
        addMetadata(persIdPartenaire, ColumnMetadata.named("PERS_ID_PARTENAIRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(persIdService, ColumnMetadata.named("PERS_ID_SERVICE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(remarques, ColumnMetadata.named("REMARQUES").withIndex(30).ofType(Types.OTHER).withSize(0));
        addMetadata(tccId, ColumnMetadata.named("TCC_ID").withIndex(26).ofType(Types.OTHER).withSize(0));
        addMetadata(trOrdre, ColumnMetadata.named("TR_ORDRE").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(utlOrdreCreation, ColumnMetadata.named("UTL_ORDRE_CREATION").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(utlOrdreModif, ColumnMetadata.named("UTL_ORDRE_MODIF").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(utlOrdreValidAdm, ColumnMetadata.named("UTL_ORDRE_VALID_ADM").withIndex(19).ofType(Types.OTHER).withSize(0));
    }

}

