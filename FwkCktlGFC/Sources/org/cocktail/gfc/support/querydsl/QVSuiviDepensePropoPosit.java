package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviDepensePropoPosit is a Querydsl query type for QVSuiviDepensePropoPosit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviDepensePropoPosit extends com.mysema.query.sql.RelationalPathBase<QVSuiviDepensePropoPosit> {

    private static final long serialVersionUID = -1192110280;

    public static final QVSuiviDepensePropoPosit vSuiviDepensePropoPosit = new QVSuiviDepensePropoPosit("V_SUIVI_DEPENSE_PROPO_POSIT");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> orgIdPosit = createSimple("orgIdPosit", Object.class);

    public final SimplePath<Object> orgIdPositPere = createSimple("orgIdPositPere", Object.class);

    public final SimplePath<Object> orgIdPropo = createSimple("orgIdPropo", Object.class);

    public final SimplePath<Object> tcdOrdrePosit = createSimple("tcdOrdrePosit", Object.class);

    public final SimplePath<Object> totalPosit = createSimple("totalPosit", Object.class);

    public final SimplePath<Object> totalPropo = createSimple("totalPropo", Object.class);

    public QVSuiviDepensePropoPosit(String variable) {
        super(QVSuiviDepensePropoPosit.class, forVariable(variable), "GFC", "V_SUIVI_DEPENSE_PROPO_POSIT");
        addMetadata();
    }

    public QVSuiviDepensePropoPosit(String variable, String schema, String table) {
        super(QVSuiviDepensePropoPosit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviDepensePropoPosit(Path<? extends QVSuiviDepensePropoPosit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_DEPENSE_PROPO_POSIT");
        addMetadata();
    }

    public QVSuiviDepensePropoPosit(PathMetadata<?> metadata) {
        super(QVSuiviDepensePropoPosit.class, metadata, "GFC", "V_SUIVI_DEPENSE_PROPO_POSIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPosit, ColumnMetadata.named("ORG_ID_POSIT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPositPere, ColumnMetadata.named("ORG_ID_POSIT_PERE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPropo, ColumnMetadata.named("ORG_ID_PROPO").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdrePosit, ColumnMetadata.named("TCD_ORDRE_POSIT").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPosit, ColumnMetadata.named("TOTAL_POSIT").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPropo, ColumnMetadata.named("TOTAL_PROPO").withIndex(7).ofType(Types.OTHER).withSize(0));
    }

}

