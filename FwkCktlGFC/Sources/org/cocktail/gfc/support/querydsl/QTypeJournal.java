package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeJournal is a Querydsl query type for QTypeJournal
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeJournal extends com.mysema.query.sql.RelationalPathBase<QTypeJournal> {

    private static final long serialVersionUID = 2048825084;

    public static final QTypeJournal typeJournal = new QTypeJournal("TYPE_JOURNAL");

    public final StringPath tjoLibelle = createString("tjoLibelle");

    public final NumberPath<Long> tjoOrdre = createNumber("tjoOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeJournal> typeJournalPk = createPrimaryKey(tjoOrdre);

    public final com.mysema.query.sql.ForeignKey<QBrouillard> _brouillardTjoOrdreFk = createInvForeignKey(tjoOrdre, "TJO_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcriture> _ecritureTjoOrdreFk = createInvForeignKey(tjoOrdre, "TJO_ORDRE");

    public QTypeJournal(String variable) {
        super(QTypeJournal.class, forVariable(variable), "GFC", "TYPE_JOURNAL");
        addMetadata();
    }

    public QTypeJournal(String variable, String schema, String table) {
        super(QTypeJournal.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeJournal(Path<? extends QTypeJournal> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_JOURNAL");
        addMetadata();
    }

    public QTypeJournal(PathMetadata<?> metadata) {
        super(QTypeJournal.class, metadata, "GFC", "TYPE_JOURNAL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tjoLibelle, ColumnMetadata.named("TJO_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tjoOrdre, ColumnMetadata.named("TJO_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

