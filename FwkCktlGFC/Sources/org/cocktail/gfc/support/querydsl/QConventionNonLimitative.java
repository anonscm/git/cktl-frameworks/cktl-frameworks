package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QConventionNonLimitative is a Querydsl query type for QConventionNonLimitative
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QConventionNonLimitative extends com.mysema.query.sql.RelationalPathBase<QConventionNonLimitative> {

    private static final long serialVersionUID = -783750779;

    public static final QConventionNonLimitative conventionNonLimitative = new QConventionNonLimitative("CONVENTION_NON_LIMITATIVE");

    public final SimplePath<Object> cnlConReference = createSimple("cnlConReference", Object.class);

    public final SimplePath<Object> cnlLimitatif = createSimple("cnlLimitatif", Object.class);

    public final SimplePath<Object> cnlModeGestion = createSimple("cnlModeGestion", Object.class);

    public final SimplePath<Object> cnlMontant = createSimple("cnlMontant", Object.class);

    public final SimplePath<Object> cnlTypeMontant = createSimple("cnlTypeMontant", Object.class);

    public final SimplePath<Object> conObjetCourt = createSimple("conObjetCourt", Object.class);

    public final SimplePath<Object> conReferenceExterne = createSimple("conReferenceExterne", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QConventionNonLimitative(String variable) {
        super(QConventionNonLimitative.class, forVariable(variable), "GFC", "CONVENTION_NON_LIMITATIVE");
        addMetadata();
    }

    public QConventionNonLimitative(String variable, String schema, String table) {
        super(QConventionNonLimitative.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QConventionNonLimitative(Path<? extends QConventionNonLimitative> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CONVENTION_NON_LIMITATIVE");
        addMetadata();
    }

    public QConventionNonLimitative(PathMetadata<?> metadata) {
        super(QConventionNonLimitative.class, metadata, "GFC", "CONVENTION_NON_LIMITATIVE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cnlConReference, ColumnMetadata.named("CNL_CON_REFERENCE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(cnlLimitatif, ColumnMetadata.named("CNL_LIMITATIF").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(cnlModeGestion, ColumnMetadata.named("CNL_MODE_GESTION").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(cnlMontant, ColumnMetadata.named("CNL_MONTANT").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(cnlTypeMontant, ColumnMetadata.named("CNL_TYPE_MONTANT").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(conObjetCourt, ColumnMetadata.named("CON_OBJET_COURT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(conReferenceExterne, ColumnMetadata.named("CON_REFERENCE_EXTERNE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(8).ofType(Types.OTHER).withSize(0));
    }

}

