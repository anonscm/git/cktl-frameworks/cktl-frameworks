package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSbDepense is a Querydsl query type for QSbDepense
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSbDepense extends com.mysema.query.sql.RelationalPathBase<QSbDepense> {

    private static final long serialVersionUID = 1949711386;

    public static final QSbDepense sbDepense = new QSbDepense("SB_DEPENSE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> exeOrdreSb = createNumber("exeOrdreSb", Long.class);

    public final NumberPath<Long> fournisPersid = createNumber("fournisPersid", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> lolfId = createNumber("lolfId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> pvbuId = createNumber("pvbuId", Long.class);

    public final StringPath sdAuto = createString("sdAuto");

    public final DateTimePath<java.sql.Timestamp> sdDatePrev = createDateTime("sdDatePrev", java.sql.Timestamp.class);

    public final NumberPath<Integer> sdEtat = createNumber("sdEtat", Integer.class);

    public final StringPath sdLibelle = createString("sdLibelle");

    public final NumberPath<java.math.BigDecimal> sdMontantHt = createNumber("sdMontantHt", java.math.BigDecimal.class);

    public final NumberPath<Long> sdOrdre = createNumber("sdOrdre", Long.class);

    public final NumberPath<Long> sdtOrdre = createNumber("sdtOrdre", Long.class);

    public final NumberPath<Long> srOrdreOwner = createNumber("srOrdreOwner", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> traOrdre = createNumber("traOrdre", Long.class);

    public final NumberPath<Long> tvaId = createNumber("tvaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QSbDepense> sbDepensePk = createPrimaryKey(sdOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> depenseTcFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTva> sbDepTvaFk = createForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QPersonne> depPersidFournisFk = createForeignKey(fournisPersid, "PERS_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepense> depLolfFk = createForeignKey(lolfId, "ID_ADM_DESTINATION_DEPENSE");

    public QSbDepense(String variable) {
        super(QSbDepense.class, forVariable(variable), "GFC", "SB_DEPENSE");
        addMetadata();
    }

    public QSbDepense(String variable, String schema, String table) {
        super(QSbDepense.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSbDepense(Path<? extends QSbDepense> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SB_DEPENSE");
        addMetadata();
    }

    public QSbDepense(PathMetadata<?> metadata) {
        super(QSbDepense.class, metadata, "GFC", "SB_DEPENSE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(17).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(exeOrdreSb, ColumnMetadata.named("EXE_ORDRE_SB").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fournisPersid, ColumnMetadata.named("FOURNIS_PERSID").withIndex(19).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lolfId, ColumnMetadata.named("LOLF_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(32));
        addMetadata(pvbuId, ColumnMetadata.named("PVBU_ID").withIndex(20).ofType(Types.DECIMAL).withSize(0));
        addMetadata(sdAuto, ColumnMetadata.named("SD_AUTO").withIndex(11).ofType(Types.VARCHAR).withSize(30));
        addMetadata(sdDatePrev, ColumnMetadata.named("SD_DATE_PREV").withIndex(9).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(sdEtat, ColumnMetadata.named("SD_ETAT").withIndex(10).ofType(Types.DECIMAL).withSize(1));
        addMetadata(sdLibelle, ColumnMetadata.named("SD_LIBELLE").withIndex(8).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(sdMontantHt, ColumnMetadata.named("SD_MONTANT_HT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(sdOrdre, ColumnMetadata.named("SD_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(sdtOrdre, ColumnMetadata.named("SDT_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(srOrdreOwner, ColumnMetadata.named("SR_ORDRE_OWNER").withIndex(13).ofType(Types.DECIMAL).withSize(22));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(18).ofType(Types.DECIMAL).withSize(0));
    }

}

