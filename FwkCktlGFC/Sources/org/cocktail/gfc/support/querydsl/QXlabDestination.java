package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QXlabDestination is a Querydsl query type for QXlabDestination
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QXlabDestination extends com.mysema.query.sql.RelationalPathBase<QXlabDestination> {

    private static final long serialVersionUID = 359979928;

    public static final QXlabDestination xlabDestination = new QXlabDestination("XLAB_DESTINATION");

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final StringPath xdesCode = createString("xdesCode");

    public final NumberPath<Long> xdesExer = createNumber("xdesExer", Long.class);

    public final NumberPath<Long> xdesId = createNumber("xdesId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QXlabDestination> xlabDestinationPk = createPrimaryKey(xdesId);

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeXdesIdFk = createInvForeignKey(xdesId, "XDES_ID");

    public QXlabDestination(String variable) {
        super(QXlabDestination.class, forVariable(variable), "GFC", "XLAB_DESTINATION");
        addMetadata();
    }

    public QXlabDestination(String variable, String schema, String table) {
        super(QXlabDestination.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QXlabDestination(Path<? extends QXlabDestination> path) {
        super(path.getType(), path.getMetadata(), "GFC", "XLAB_DESTINATION");
        addMetadata();
    }

    public QXlabDestination(PathMetadata<?> metadata) {
        super(QXlabDestination.class, metadata, "GFC", "XLAB_DESTINATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(xdesCode, ColumnMetadata.named("XDES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(xdesExer, ColumnMetadata.named("XDES_EXER").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(xdesId, ColumnMetadata.named("XDES_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

