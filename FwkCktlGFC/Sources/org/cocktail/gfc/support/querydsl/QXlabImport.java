package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QXlabImport is a Querydsl query type for QXlabImport
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QXlabImport extends com.mysema.query.sql.RelationalPathBase<QXlabImport> {

    private static final long serialVersionUID = -2040476197;

    public static final QXlabImport xlabImport = new QXlabImport("XLAB_IMPORT");

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> ximpDate = createDateTime("ximpDate", java.sql.Timestamp.class);

    public final StringPath ximpFilename = createString("ximpFilename");

    public final NumberPath<Long> ximpId = createNumber("ximpId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QXlabImport> xlabImportPk = createPrimaryKey(ximpId);

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> xlabImportUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFactureXimpIdFk = createInvForeignKey(ximpId, "XIMP_ID");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeXimpIdFk = createInvForeignKey(ximpId, "XIMP_ID");

    public QXlabImport(String variable) {
        super(QXlabImport.class, forVariable(variable), "GFC", "XLAB_IMPORT");
        addMetadata();
    }

    public QXlabImport(String variable, String schema, String table) {
        super(QXlabImport.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QXlabImport(Path<? extends QXlabImport> path) {
        super(path.getType(), path.getMetadata(), "GFC", "XLAB_IMPORT");
        addMetadata();
    }

    public QXlabImport(PathMetadata<?> metadata) {
        super(QXlabImport.class, metadata, "GFC", "XLAB_IMPORT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ximpDate, ColumnMetadata.named("XIMP_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(ximpFilename, ColumnMetadata.named("XIMP_FILENAME").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(ximpId, ColumnMetadata.named("XIMP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

