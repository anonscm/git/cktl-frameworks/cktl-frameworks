package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRepartIndicateursContrat is a Querydsl query type for QRepartIndicateursContrat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRepartIndicateursContrat extends com.mysema.query.sql.RelationalPathBase<QRepartIndicateursContrat> {

    private static final long serialVersionUID = 2140178973;

    public static final QRepartIndicateursContrat repartIndicateursContrat = new QRepartIndicateursContrat("REPART_INDICATEURS_CONTRAT");

    public final NumberPath<Long> icId = createNumber("icId", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final NumberPath<Long> ricId = createNumber("ricId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRepartIndicateursContrat> repartIndicateursContratPk = createPrimaryKey(ricId);

    public QRepartIndicateursContrat(String variable) {
        super(QRepartIndicateursContrat.class, forVariable(variable), "GFC", "REPART_INDICATEURS_CONTRAT");
        addMetadata();
    }

    public QRepartIndicateursContrat(String variable, String schema, String table) {
        super(QRepartIndicateursContrat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRepartIndicateursContrat(Path<? extends QRepartIndicateursContrat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REPART_INDICATEURS_CONTRAT");
        addMetadata();
    }

    public QRepartIndicateursContrat(PathMetadata<?> metadata) {
        super(QRepartIndicateursContrat.class, metadata, "GFC", "REPART_INDICATEURS_CONTRAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(icId, ColumnMetadata.named("IC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ricId, ColumnMetadata.named("RIC_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

