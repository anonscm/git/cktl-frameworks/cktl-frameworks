package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmAmortissement is a Querydsl query type for QImmAmortissement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmAmortissement extends com.mysema.query.sql.RelationalPathBase<QImmAmortissement> {

    private static final long serialVersionUID = -780839849;

    public static final QImmAmortissement immAmortissement = new QImmAmortissement("IMM_AMORTISSEMENT");

    public final NumberPath<java.math.BigDecimal> amoAcquisition = createNumber("amoAcquisition", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amoAnnuite = createNumber("amoAnnuite", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amoCumul = createNumber("amoCumul", java.math.BigDecimal.class);

    public final DateTimePath<java.sql.Timestamp> amoDate = createDateTime("amoDate", java.sql.Timestamp.class);

    public final NumberPath<Long> amoId = createNumber("amoId", Long.class);

    public final NumberPath<java.math.BigDecimal> amoResiduel = createNumber("amoResiduel", java.math.BigDecimal.class);

    public final StringPath exeOrdre = createString("exeOrdre");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final com.mysema.query.sql.PrimaryKey<QImmAmortissement> immAmortissementPk = createPrimaryKey(amoId);

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> inventairecomptableFk = createForeignKey(invcId, "INVC_ID");

    public QImmAmortissement(String variable) {
        super(QImmAmortissement.class, forVariable(variable), "GFC", "IMM_AMORTISSEMENT");
        addMetadata();
    }

    public QImmAmortissement(String variable, String schema, String table) {
        super(QImmAmortissement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmAmortissement(Path<? extends QImmAmortissement> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_AMORTISSEMENT");
        addMetadata();
    }

    public QImmAmortissement(PathMetadata<?> metadata) {
        super(QImmAmortissement.class, metadata, "GFC", "IMM_AMORTISSEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(amoAcquisition, ColumnMetadata.named("AMO_ACQUISITION").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(amoAnnuite, ColumnMetadata.named("AMO_ANNUITE").withIndex(3).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(amoCumul, ColumnMetadata.named("AMO_CUMUL").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(amoDate, ColumnMetadata.named("AMO_DATE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(amoId, ColumnMetadata.named("AMO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(amoResiduel, ColumnMetadata.named("AMO_RESIDUEL").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(7).ofType(Types.VARCHAR).withSize(4));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(9).ofType(Types.VARCHAR).withSize(20));
    }

}

