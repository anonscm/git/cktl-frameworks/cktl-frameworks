package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCreditsPositionnes is a Querydsl query type for QVCreditsPositionnes
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCreditsPositionnes extends com.mysema.query.sql.RelationalPathBase<QVCreditsPositionnes> {

    private static final long serialVersionUID = -2131848370;

    public static final QVCreditsPositionnes vCreditsPositionnes = new QVCreditsPositionnes("V_CREDITS_POSITIONNES");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> montant = createSimple("montant", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> traOrdre = createSimple("traOrdre", Object.class);

    public QVCreditsPositionnes(String variable) {
        super(QVCreditsPositionnes.class, forVariable(variable), "GFC", "V_CREDITS_POSITIONNES");
        addMetadata();
    }

    public QVCreditsPositionnes(String variable, String schema, String table) {
        super(QVCreditsPositionnes.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCreditsPositionnes(Path<? extends QVCreditsPositionnes> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CREDITS_POSITIONNES");
        addMetadata();
    }

    public QVCreditsPositionnes(PathMetadata<?> metadata) {
        super(QVCreditsPositionnes.class, metadata, "GFC", "V_CREDITS_POSITIONNES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

