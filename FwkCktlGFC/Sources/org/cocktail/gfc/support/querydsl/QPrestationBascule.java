package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPrestationBascule is a Querydsl query type for QPrestationBascule
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPrestationBascule extends com.mysema.query.sql.RelationalPathBase<QPrestationBascule> {

    private static final long serialVersionUID = 1481446957;

    public static final QPrestationBascule prestationBascule = new QPrestationBascule("PRESTATION_BASCULE");

    public final NumberPath<Long> presbId = createNumber("presbId", Long.class);

    public final NumberPath<Long> prestIdDestination = createNumber("prestIdDestination", Long.class);

    public final NumberPath<Long> prestIdOrigine = createNumber("prestIdOrigine", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPrestationBascule> prestationBasculePk = createPrimaryKey(presbId);

    public final com.mysema.query.sql.ForeignKey<QPrestation> prestationBascOrigFk = createForeignKey(prestIdOrigine, "PREST_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestation> prestationBascDestFk = createForeignKey(prestIdDestination, "PREST_ID");

    public QPrestationBascule(String variable) {
        super(QPrestationBascule.class, forVariable(variable), "GFC", "PRESTATION_BASCULE");
        addMetadata();
    }

    public QPrestationBascule(String variable, String schema, String table) {
        super(QPrestationBascule.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPrestationBascule(Path<? extends QPrestationBascule> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PRESTATION_BASCULE");
        addMetadata();
    }

    public QPrestationBascule(PathMetadata<?> metadata) {
        super(QPrestationBascule.class, metadata, "GFC", "PRESTATION_BASCULE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(presbId, ColumnMetadata.named("PRESB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prestIdDestination, ColumnMetadata.named("PREST_ID_DESTINATION").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prestIdOrigine, ColumnMetadata.named("PREST_ID_ORIGINE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

