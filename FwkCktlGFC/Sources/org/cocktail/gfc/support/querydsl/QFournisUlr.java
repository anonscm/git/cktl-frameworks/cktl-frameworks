package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QFournisUlr is a Querydsl query type for QFournisUlr
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QFournisUlr extends com.mysema.query.sql.RelationalPathBase<QFournisUlr> {

    private static final long serialVersionUID = -1058322454;

    public static final QFournisUlr fournisUlr = new QFournisUlr("FOURNIS_ULR");

    public final NumberPath<Long> adrOrdre = createNumber("adrOrdre", Long.class);

    public final NumberPath<Long> agtOrdre = createNumber("agtOrdre", Long.class);

    public final NumberPath<Long> cptOrdre = createNumber("cptOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final StringPath fouCode = createString("fouCode");

    public final DateTimePath<java.sql.Timestamp> fouDate = createDateTime("fouDate", java.sql.Timestamp.class);

    public final StringPath fouEtranger = createString("fouEtranger");

    public final StringPath fouMarche = createString("fouMarche");

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath fouType = createString("fouType");

    public final StringPath fouValide = createString("fouValide");

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QFournisUlr> fournisUlrPk = createPrimaryKey(fouOrdre);

    public QFournisUlr(String variable) {
        super(QFournisUlr.class, forVariable(variable), "GRHUM", "FOURNIS_ULR");
        addMetadata();
    }

    public QFournisUlr(String variable, String schema, String table) {
        super(QFournisUlr.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QFournisUlr(Path<? extends QFournisUlr> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "FOURNIS_ULR");
        addMetadata();
    }

    public QFournisUlr(PathMetadata<?> metadata) {
        super(QFournisUlr.class, metadata, "GRHUM", "FOURNIS_ULR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrOrdre, ColumnMetadata.named("ADR_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(agtOrdre, ColumnMetadata.named("AGT_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cptOrdre, ColumnMetadata.named("CPT_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(fouCode, ColumnMetadata.named("FOU_CODE").withIndex(4).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(fouDate, ColumnMetadata.named("FOU_DATE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(fouEtranger, ColumnMetadata.named("FOU_ETRANGER").withIndex(13).ofType(Types.CHAR).withSize(1).notNull());
        addMetadata(fouMarche, ColumnMetadata.named("FOU_MARCHE").withIndex(6).ofType(Types.CHAR).withSize(1).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(fouType, ColumnMetadata.named("FOU_TYPE").withIndex(9).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(fouValide, ColumnMetadata.named("FOU_VALIDE").withIndex(7).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

