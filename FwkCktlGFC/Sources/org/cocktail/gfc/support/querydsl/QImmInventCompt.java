package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventCompt is a Querydsl query type for QImmInventCompt
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventCompt extends com.mysema.query.sql.RelationalPathBase<QImmInventCompt> {

    private static final long serialVersionUID = 169889731;

    public static final QImmInventCompt immInventCompt = new QImmInventCompt("IMM_INVENT_COMPT");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final NumberPath<Long> dgcoId = createNumber("dgcoId", Long.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> invcDateAcquisition = createDateTime("invcDateAcquisition", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> invcDateSortie = createDateTime("invcDateSortie", java.sql.Timestamp.class);

    public final StringPath invcEtat = createString("invcEtat");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<java.math.BigDecimal> invcMontantAcquisition = createNumber("invcMontantAcquisition", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> invcMontantAmortissable = createNumber("invcMontantAmortissable", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> invcMontantResiduel = createNumber("invcMontantResiduel", java.math.BigDecimal.class);

    public final NumberPath<Long> orgfId = createNumber("orgfId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInventCompt> immInventComptPk = createPrimaryKey(invcId);

    public final com.mysema.query.sql.ForeignKey<QImmOrigineFinancement> originefinancementFk = createForeignKey(orgfId, "ORGF_ID");

    public final com.mysema.query.sql.ForeignKey<QImmDegressifCoef> invcDgcoIdFk = createForeignKey(dgcoId, "DGCO_ID");

    public final com.mysema.query.sql.ForeignKey<QImmCleInventCompt> cleinventairecomptableFk = createForeignKey(clicId, "CLIC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventComptSortie> _comptSortieInvcIdFk = createInvForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInvent> _inventairecomptableFk2 = createInvForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlPlancoInve> _pdepenseCtrlPlinvInvcidFk = createInvForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventComptOrv> _inventaireComptableFk2 = createInvForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmAmortissement> _inventairecomptableFk = createInvForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventComptOrig> _invComptOInvcIdFk = createInvForeignKey(invcId, "INVC_ID");

    public QImmInventCompt(String variable) {
        super(QImmInventCompt.class, forVariable(variable), "GFC", "IMM_INVENT_COMPT");
        addMetadata();
    }

    public QImmInventCompt(String variable, String schema, String table) {
        super(QImmInventCompt.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventCompt(Path<? extends QImmInventCompt> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_COMPT");
        addMetadata();
    }

    public QImmInventCompt(PathMetadata<?> metadata) {
        super(QImmInventCompt.class, metadata, "GFC", "IMM_INVENT_COMPT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dgcoId, ColumnMetadata.named("DGCO_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(invcDateAcquisition, ColumnMetadata.named("INVC_DATE_ACQUISITION").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(invcDateSortie, ColumnMetadata.named("INVC_DATE_SORTIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(invcEtat, ColumnMetadata.named("INVC_ETAT").withIndex(6).ofType(Types.VARCHAR).withSize(20));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invcMontantAcquisition, ColumnMetadata.named("INVC_MONTANT_ACQUISITION").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invcMontantAmortissable, ColumnMetadata.named("INVC_MONTANT_AMORTISSABLE").withIndex(13).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invcMontantResiduel, ColumnMetadata.named("INVC_MONTANT_RESIDUEL").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(orgfId, ColumnMetadata.named("ORGF_ID").withIndex(9).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
    }

}

