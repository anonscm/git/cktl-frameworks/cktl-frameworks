package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVExtractGeneralites is a Querydsl query type for QVExtractGeneralites
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVExtractGeneralites extends com.mysema.query.sql.RelationalPathBase<QVExtractGeneralites> {

    private static final long serialVersionUID = 1850081589;

    public static final QVExtractGeneralites vExtractGeneralites = new QVExtractGeneralites("V_EXTRACT_GENERALITES");

    public final SimplePath<Object> composante = createSimple("composante", Object.class);

    public final SimplePath<Object> creation = createSimple("creation", Object.class);

    public final SimplePath<Object> dateCloture = createSimple("dateCloture", Object.class);

    public final SimplePath<Object> dateCreation = createSimple("dateCreation", Object.class);

    public final SimplePath<Object> dateDebut = createSimple("dateDebut", Object.class);

    public final SimplePath<Object> dateDebutExecution = createSimple("dateDebutExecution", Object.class);

    public final SimplePath<Object> dateFin = createSimple("dateFin", Object.class);

    public final SimplePath<Object> dateFinExecution = createSimple("dateFinExecution", Object.class);

    public final SimplePath<Object> dateModif = createSimple("dateModif", Object.class);

    public final SimplePath<Object> dateSignature = createSimple("dateSignature", Object.class);

    public final SimplePath<Object> dateSignatureDefinitive = createSimple("dateSignatureDefinitive", Object.class);

    public final SimplePath<Object> dateSoumissionCa = createSimple("dateSoumissionCa", Object.class);

    public final SimplePath<Object> dateValidAdm = createSimple("dateValidAdm", Object.class);

    public final SimplePath<Object> depensePositHtPrevu = createSimple("depensePositHtPrevu", Object.class);

    public final SimplePath<Object> depensePropoHtPrevu = createSimple("depensePropoHtPrevu", Object.class);

    public final SimplePath<Object> etabGestionnaire = createSimple("etabGestionnaire", Object.class);

    public final SimplePath<Object> exerOuvert = createSimple("exerOuvert", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> localisationOuCentreGestion = createSimple("localisationOuCentreGestion", Object.class);

    public final SimplePath<Object> lucrative = createSimple("lucrative", Object.class);

    public final SimplePath<Object> modeGestion = createSimple("modeGestion", Object.class);

    public final SimplePath<Object> modif = createSimple("modif", Object.class);

    public final SimplePath<Object> numeroConv = createSimple("numeroConv", Object.class);

    public final SimplePath<Object> objet = createSimple("objet", Object.class);

    public final SimplePath<Object> observations = createSimple("observations", Object.class);

    public final SimplePath<Object> partenairesAutres = createSimple("partenairesAutres", Object.class);

    public final SimplePath<Object> partenairesPrinc = createSimple("partenairesPrinc", Object.class);

    public final SimplePath<Object> recettePositHtPrevu = createSimple("recettePositHtPrevu", Object.class);

    public final SimplePath<Object> recettePropoHtPrevu = createSimple("recettePropoHtPrevu", Object.class);

    public final SimplePath<Object> refConv = createSimple("refConv", Object.class);

    public final SimplePath<Object> responsablesAdmin = createSimple("responsablesAdmin", Object.class);

    public final SimplePath<Object> responsablesScient = createSimple("responsablesScient", Object.class);

    public final SimplePath<Object> totalHtPrevu = createSimple("totalHtPrevu", Object.class);

    public final SimplePath<Object> totalTtcPrevu = createSimple("totalTtcPrevu", Object.class);

    public final SimplePath<Object> trancheHtExerOuvert = createSimple("trancheHtExerOuvert", Object.class);

    public final SimplePath<Object> tvaCollectee = createSimple("tvaCollectee", Object.class);

    public final SimplePath<Object> typeConvention = createSimple("typeConvention", Object.class);

    public final SimplePath<Object> typeOptionnel = createSimple("typeOptionnel", Object.class);

    public final SimplePath<Object> validAdm = createSimple("validAdm", Object.class);

    public QVExtractGeneralites(String variable) {
        super(QVExtractGeneralites.class, forVariable(variable), "GFC", "V_EXTRACT_GENERALITES");
        addMetadata();
    }

    public QVExtractGeneralites(String variable, String schema, String table) {
        super(QVExtractGeneralites.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVExtractGeneralites(Path<? extends QVExtractGeneralites> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EXTRACT_GENERALITES");
        addMetadata();
    }

    public QVExtractGeneralites(PathMetadata<?> metadata) {
        super(QVExtractGeneralites.class, metadata, "GFC", "V_EXTRACT_GENERALITES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(composante, ColumnMetadata.named("COMPOSANTE").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(creation, ColumnMetadata.named("CREATION").withIndex(33).ofType(Types.OTHER).withSize(0));
        addMetadata(dateCloture, ColumnMetadata.named("DATE_CLOTURE").withIndex(28).ofType(Types.OTHER).withSize(0));
        addMetadata(dateCreation, ColumnMetadata.named("DATE_CREATION").withIndex(34).ofType(Types.OTHER).withSize(0));
        addMetadata(dateDebut, ColumnMetadata.named("DATE_DEBUT").withIndex(21).ofType(Types.OTHER).withSize(0));
        addMetadata(dateDebutExecution, ColumnMetadata.named("DATE_DEBUT_EXECUTION").withIndex(23).ofType(Types.OTHER).withSize(0));
        addMetadata(dateFin, ColumnMetadata.named("DATE_FIN").withIndex(22).ofType(Types.OTHER).withSize(0));
        addMetadata(dateFinExecution, ColumnMetadata.named("DATE_FIN_EXECUTION").withIndex(24).ofType(Types.OTHER).withSize(0));
        addMetadata(dateModif, ColumnMetadata.named("DATE_MODIF").withIndex(36).ofType(Types.OTHER).withSize(0));
        addMetadata(dateSignature, ColumnMetadata.named("DATE_SIGNATURE").withIndex(26).ofType(Types.OTHER).withSize(0));
        addMetadata(dateSignatureDefinitive, ColumnMetadata.named("DATE_SIGNATURE_DEFINITIVE").withIndex(27).ofType(Types.OTHER).withSize(0));
        addMetadata(dateSoumissionCa, ColumnMetadata.named("DATE_SOUMISSION_CA").withIndex(25).ofType(Types.OTHER).withSize(0));
        addMetadata(dateValidAdm, ColumnMetadata.named("DATE_VALID_ADM").withIndex(38).ofType(Types.OTHER).withSize(0));
        addMetadata(depensePositHtPrevu, ColumnMetadata.named("DEPENSE_POSIT_HT_PREVU").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(depensePropoHtPrevu, ColumnMetadata.named("DEPENSE_PROPO_HT_PREVU").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(etabGestionnaire, ColumnMetadata.named("ETAB_GESTIONNAIRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(exerOuvert, ColumnMetadata.named("EXER_OUVERT").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(39).ofType(Types.OTHER).withSize(0));
        addMetadata(localisationOuCentreGestion, ColumnMetadata.named("LOCALISATION_OU_CENTRE_GESTION").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(lucrative, ColumnMetadata.named("LUCRATIVE").withIndex(19).ofType(Types.OTHER).withSize(0));
        addMetadata(modeGestion, ColumnMetadata.named("MODE_GESTION").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(modif, ColumnMetadata.named("MODIF").withIndex(35).ofType(Types.OTHER).withSize(0));
        addMetadata(numeroConv, ColumnMetadata.named("NUMERO_CONV").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(objet, ColumnMetadata.named("OBJET").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(observations, ColumnMetadata.named("OBSERVATIONS").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(partenairesAutres, ColumnMetadata.named("PARTENAIRES_AUTRES").withIndex(30).ofType(Types.OTHER).withSize(0));
        addMetadata(partenairesPrinc, ColumnMetadata.named("PARTENAIRES_PRINC").withIndex(29).ofType(Types.OTHER).withSize(0));
        addMetadata(recettePositHtPrevu, ColumnMetadata.named("RECETTE_POSIT_HT_PREVU").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(recettePropoHtPrevu, ColumnMetadata.named("RECETTE_PROPO_HT_PREVU").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(refConv, ColumnMetadata.named("REF_CONV").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(responsablesAdmin, ColumnMetadata.named("RESPONSABLES_ADMIN").withIndex(31).ofType(Types.OTHER).withSize(0));
        addMetadata(responsablesScient, ColumnMetadata.named("RESPONSABLES_SCIENT").withIndex(32).ofType(Types.OTHER).withSize(0));
        addMetadata(totalHtPrevu, ColumnMetadata.named("TOTAL_HT_PREVU").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(totalTtcPrevu, ColumnMetadata.named("TOTAL_TTC_PREVU").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(trancheHtExerOuvert, ColumnMetadata.named("TRANCHE_HT_EXER_OUVERT").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(tvaCollectee, ColumnMetadata.named("TVA_COLLECTEE").withIndex(20).ofType(Types.OTHER).withSize(0));
        addMetadata(typeConvention, ColumnMetadata.named("TYPE_CONVENTION").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(typeOptionnel, ColumnMetadata.named("TYPE_OPTIONNEL").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(validAdm, ColumnMetadata.named("VALID_ADM").withIndex(37).ofType(Types.OTHER).withSize(0));
    }

}

