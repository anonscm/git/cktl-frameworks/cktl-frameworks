package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVBordereauRejet is a Querydsl query type for QVBordereauRejet
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVBordereauRejet extends com.mysema.query.sql.RelationalPathBase<QVBordereauRejet> {

    private static final long serialVersionUID = 799395534;

    public static final QVBordereauRejet vBordereauRejet = new QVBordereauRejet("V_BORDEREAU_REJET");

    public final StringPath bordereauRejetEtat = createString("bordereauRejetEtat");

    public final NumberPath<Long> bordereauRejetNumero = createNumber("bordereauRejetNumero", Long.class);

    public final NumberPath<Long> borNumeroOrigine = createNumber("borNumeroOrigine", Long.class);

    public final StringPath btType = createString("btType");

    public final DateTimePath<java.sql.Timestamp> dateRejet = createDateTime("dateRejet", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<java.math.BigDecimal> montantHt = createNumber("montantHt", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> montantTtc = createNumber("montantTtc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> montantTva = createNumber("montantTva", java.math.BigDecimal.class);

    public final StringPath motifRejet = createString("motifRejet");

    public final NumberPath<Long> numeroObjet = createNumber("numeroObjet", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath rejetePar = createString("rejetePar");

    public final StringPath typeBordereauOrigine = createString("typeBordereauOrigine");

    public QVBordereauRejet(String variable) {
        super(QVBordereauRejet.class, forVariable(variable), "GFC", "V_BORDEREAU_REJET");
        addMetadata();
    }

    public QVBordereauRejet(String variable, String schema, String table) {
        super(QVBordereauRejet.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVBordereauRejet(Path<? extends QVBordereauRejet> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_BORDEREAU_REJET");
        addMetadata();
    }

    public QVBordereauRejet(PathMetadata<?> metadata) {
        super(QVBordereauRejet.class, metadata, "GFC", "V_BORDEREAU_REJET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bordereauRejetEtat, ColumnMetadata.named("BORDEREAU_REJET_ETAT").withIndex(15).ofType(Types.VARCHAR).withSize(20));
        addMetadata(bordereauRejetNumero, ColumnMetadata.named("BORDEREAU_REJET_NUMERO").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(borNumeroOrigine, ColumnMetadata.named("BOR_NUMERO_ORIGINE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(btType, ColumnMetadata.named("BT_TYPE").withIndex(2).ofType(Types.VARCHAR).withSize(60));
        addMetadata(dateRejet, ColumnMetadata.named("DATE_REJET").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(4).ofType(Types.VARCHAR).withSize(10));
        addMetadata(montantHt, ColumnMetadata.named("MONTANT_HT").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(montantTtc, ColumnMetadata.named("MONTANT_TTC").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(montantTva, ColumnMetadata.named("MONTANT_TVA").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(motifRejet, ColumnMetadata.named("MOTIF_REJET").withIndex(12).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(numeroObjet, ColumnMetadata.named("NUMERO_OBJET").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(8).ofType(Types.VARCHAR).withSize(20));
        addMetadata(rejetePar, ColumnMetadata.named("REJETE_PAR").withIndex(14).ofType(Types.VARCHAR).withSize(121));
        addMetadata(typeBordereauOrigine, ColumnMetadata.named("TYPE_BORDEREAU_ORIGINE").withIndex(3).ofType(Types.VARCHAR).withSize(60));
    }

}

