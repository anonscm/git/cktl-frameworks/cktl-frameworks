package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnDepBud1 is a Querydsl query type for QEpnDepBud1
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnDepBud1 extends com.mysema.query.sql.RelationalPathBase<QEpnDepBud1> {

    private static final long serialVersionUID = -112157267;

    public static final QEpnDepBud1 epnDepBud1 = new QEpnDepBud1("EPN_DEP_BUD_1");

    public final StringPath epndb1CodBud = createString("epndb1CodBud");

    public final StringPath epndb1CodNomen = createString("epndb1CodNomen");

    public final StringPath epndb1Date = createString("epndb1Date");

    public final NumberPath<Integer> epndb1Exercice = createNumber("epndb1Exercice", Integer.class);

    public final StringPath epndb1GesCode = createString("epndb1GesCode");

    public final NumberPath<Long> epndb1Identifiant = createNumber("epndb1Identifiant", Long.class);

    public final NumberPath<Integer> epndb1Nbenreg = createNumber("epndb1Nbenreg", Integer.class);

    public final NumberPath<Integer> epndb1Numero = createNumber("epndb1Numero", Integer.class);

    public final StringPath epndb1Rang = createString("epndb1Rang");

    public final NumberPath<Integer> epndb1Siren = createNumber("epndb1Siren", Integer.class);

    public final NumberPath<Long> epndb1Siret = createNumber("epndb1Siret", Long.class);

    public final StringPath epndb1Type = createString("epndb1Type");

    public final StringPath epndb1TypeDoc = createString("epndb1TypeDoc");

    public final StringPath epndbOrdre = createString("epndbOrdre");

    public QEpnDepBud1(String variable) {
        super(QEpnDepBud1.class, forVariable(variable), "GFC", "EPN_DEP_BUD_1");
        addMetadata();
    }

    public QEpnDepBud1(String variable, String schema, String table) {
        super(QEpnDepBud1.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnDepBud1(Path<? extends QEpnDepBud1> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_DEP_BUD_1");
        addMetadata();
    }

    public QEpnDepBud1(PathMetadata<?> metadata) {
        super(QEpnDepBud1.class, metadata, "GFC", "EPN_DEP_BUD_1");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epndb1CodBud, ColumnMetadata.named("EPNDB1_COD_BUD").withIndex(8).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epndb1CodNomen, ColumnMetadata.named("EPNDB1_COD_NOMEN").withIndex(7).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epndb1Date, ColumnMetadata.named("EPNDB1_DATE").withIndex(11).ofType(Types.VARCHAR).withSize(8));
        addMetadata(epndb1Exercice, ColumnMetadata.named("EPNDB1_EXERCICE").withIndex(9).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epndb1GesCode, ColumnMetadata.named("EPNDB1_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epndb1Identifiant, ColumnMetadata.named("EPNDB1_IDENTIFIANT").withIndex(5).ofType(Types.DECIMAL).withSize(10));
        addMetadata(epndb1Nbenreg, ColumnMetadata.named("EPNDB1_NBENREG").withIndex(14).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epndb1Numero, ColumnMetadata.named("EPNDB1_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epndb1Rang, ColumnMetadata.named("EPNDB1_RANG").withIndex(10).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epndb1Siren, ColumnMetadata.named("EPNDB1_SIREN").withIndex(12).ofType(Types.DECIMAL).withSize(9));
        addMetadata(epndb1Siret, ColumnMetadata.named("EPNDB1_SIRET").withIndex(13).ofType(Types.DECIMAL).withSize(14));
        addMetadata(epndb1Type, ColumnMetadata.named("EPNDB1_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epndb1TypeDoc, ColumnMetadata.named("EPNDB1_TYPE_DOC").withIndex(6).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epndbOrdre, ColumnMetadata.named("EPNDB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

