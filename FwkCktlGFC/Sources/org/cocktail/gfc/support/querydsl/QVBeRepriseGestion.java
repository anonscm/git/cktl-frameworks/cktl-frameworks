package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVBeRepriseGestion is a Querydsl query type for QVBeRepriseGestion
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVBeRepriseGestion extends com.mysema.query.sql.RelationalPathBase<QVBeRepriseGestion> {

    private static final long serialVersionUID = -590684933;

    public static final QVBeRepriseGestion vBeRepriseGestion = new QVBeRepriseGestion("V_BE_REPRISE_GESTION");

    public final NumberPath<Long> comOrdre = createNumber("comOrdre", Long.class);

    public final StringPath compteBe = createString("compteBe");

    public final StringPath compteBeLibelle = createString("compteBeLibelle");

    public final NumberPath<Long> credits = createNumber("credits", Long.class);

    public final NumberPath<Long> debits = createNumber("debits", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final StringPath pcoLibelle = createString("pcoLibelle");

    public final StringPath pcoNum = createString("pcoNum");

    public QVBeRepriseGestion(String variable) {
        super(QVBeRepriseGestion.class, forVariable(variable), "GFC", "V_BE_REPRISE_GESTION");
        addMetadata();
    }

    public QVBeRepriseGestion(String variable, String schema, String table) {
        super(QVBeRepriseGestion.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVBeRepriseGestion(Path<? extends QVBeRepriseGestion> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_BE_REPRISE_GESTION");
        addMetadata();
    }

    public QVBeRepriseGestion(PathMetadata<?> metadata) {
        super(QVBeRepriseGestion.class, metadata, "GFC", "V_BE_REPRISE_GESTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comOrdre, ColumnMetadata.named("COM_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(compteBe, ColumnMetadata.named("COMPTE_BE").withIndex(6).ofType(Types.VARCHAR).withSize(20));
        addMetadata(compteBeLibelle, ColumnMetadata.named("COMPTE_BE_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(200));
        addMetadata(credits, ColumnMetadata.named("CREDITS").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(debits, ColumnMetadata.named("DEBITS").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(pcoLibelle, ColumnMetadata.named("PCO_LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

