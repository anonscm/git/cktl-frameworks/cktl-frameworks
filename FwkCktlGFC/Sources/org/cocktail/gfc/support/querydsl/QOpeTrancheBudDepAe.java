package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeTrancheBudDepAe is a Querydsl query type for QOpeTrancheBudDepAe
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeTrancheBudDepAe extends com.mysema.query.sql.RelationalPathBase<QOpeTrancheBudDepAe> {

    private static final long serialVersionUID = 484076756;

    public static final QOpeTrancheBudDepAe opeTrancheBudDepAe = new QOpeTrancheBudDepAe("OPE_TRANCHE_BUD_DEP_AE");

    public final StringPath commentaire = createString("commentaire");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final NumberPath<Long> idAdmDestinationDepense = createNumber("idAdmDestinationDepense", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idAdmNatureDep = createNumber("idAdmNatureDep", Long.class);

    public final NumberPath<Long> idBudEnveloppe = createNumber("idBudEnveloppe", Long.class);

    public final NumberPath<Long> idOpeTrancheBud = createNumber("idOpeTrancheBud", Long.class);

    public final NumberPath<Long> idOpeTrancheBudDepAe = createNumber("idOpeTrancheBudDepAe", Long.class);

    public final NumberPath<java.math.BigDecimal> montantAe = createNumber("montantAe", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> montantCp = createNumber("montantCp", java.math.BigDecimal.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final StringPath tbdSuppr = createString("tbdSuppr");

    public final com.mysema.query.sql.PrimaryKey<QOpeTrancheBudDepAe> opeTrancheBudDepAePk = createPrimaryKey(idOpeTrancheBudDepAe);

    public final com.mysema.query.sql.ForeignKey<QBudEnveloppe> opeTrancheBudDepAeEnvFk = createForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureDep> opeTrancheBudDepAeNatFk = createForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBud> opeTrancheBudDepAeTraFk = createForeignKey(idOpeTrancheBud, "ID_OPE_TRANCHE_BUD");

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepense> opeTrancheBudDepAeDestFk = createForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> opeTrancheBudDepAeEbFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepAe> _budPrevOpeAeOpeTraAeFk = createInvForeignKey(idOpeTrancheBudDepAe, "ID_OPE_TRANCHE_BUD_DEP_AE");

    public QOpeTrancheBudDepAe(String variable) {
        super(QOpeTrancheBudDepAe.class, forVariable(variable), "GFC", "OPE_TRANCHE_BUD_DEP_AE");
        addMetadata();
    }

    public QOpeTrancheBudDepAe(String variable, String schema, String table) {
        super(QOpeTrancheBudDepAe.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeTrancheBudDepAe(Path<? extends QOpeTrancheBudDepAe> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_TRANCHE_BUD_DEP_AE");
        addMetadata();
    }

    public QOpeTrancheBudDepAe(PathMetadata<?> metadata) {
        super(QOpeTrancheBudDepAe.class, metadata, "GFC", "OPE_TRANCHE_BUD_DEP_AE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commentaire, ColumnMetadata.named("COMMENTAIRE").withIndex(9).ofType(Types.VARCHAR).withSize(4000));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(idAdmDestinationDepense, ColumnMetadata.named("ID_ADM_DESTINATION_DEPENSE").withIndex(8).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmNatureDep, ColumnMetadata.named("ID_ADM_NATURE_DEP").withIndex(7).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idBudEnveloppe, ColumnMetadata.named("ID_BUD_ENVELOPPE").withIndex(6).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeTrancheBud, ColumnMetadata.named("ID_OPE_TRANCHE_BUD").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeTrancheBudDepAe, ColumnMetadata.named("ID_OPE_TRANCHE_BUD_DEP_AE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(montantAe, ColumnMetadata.named("MONTANT_AE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(montantCp, ColumnMetadata.named("MONTANT_CP").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(10).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(12).ofType(Types.DECIMAL).withSize(38));
        addMetadata(tbdSuppr, ColumnMetadata.named("TBD_SUPPR").withIndex(14).ofType(Types.VARCHAR).withSize(1));
    }

}

