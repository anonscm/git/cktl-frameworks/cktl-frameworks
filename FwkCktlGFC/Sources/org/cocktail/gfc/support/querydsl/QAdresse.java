package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdresse is a Querydsl query type for QAdresse
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdresse extends com.mysema.query.sql.RelationalPathBase<QAdresse> {

    private static final long serialVersionUID = 1673055374;

    public static final QAdresse adresse = new QAdresse("ADRESSE");

    public final StringPath adrAdresse1 = createString("adrAdresse1");

    public final StringPath adrAdresse2 = createString("adrAdresse2");

    public final StringPath adrBp = createString("adrBp");

    public final NumberPath<Double> adrGpsLatitude = createNumber("adrGpsLatitude", Double.class);

    public final NumberPath<Double> adrGpsLongitude = createNumber("adrGpsLongitude", Double.class);

    public final StringPath adrListeRouge = createString("adrListeRouge");

    public final NumberPath<Long> adrOrdre = createNumber("adrOrdre", Long.class);

    public final StringPath adrUrlPere = createString("adrUrlPere");

    public final StringPath adrUrlRelative = createString("adrUrlRelative");

    public final StringPath adrUrlTemplate = createString("adrUrlTemplate");

    public final StringPath bisTer = createString("bisTer");

    public final StringPath cImplantation = createString("cImplantation");

    public final StringPath codePostal = createString("codePostal");

    public final StringPath cPays = createString("cPays");

    public final StringPath cpEtranger = createString("cpEtranger");

    public final StringPath cpltLocalisationEtranger = createString("cpltLocalisationEtranger");

    public final StringPath cVoie = createString("cVoie");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dDebVal = createDateTime("dDebVal", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dFinVal = createDateTime("dFinVal", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final StringPath habitantChez = createString("habitantChez");

    public final StringPath lieuDit = createString("lieuDit");

    public final StringPath localite = createString("localite");

    public final StringPath nomVoie = createString("nomVoie");

    public final StringPath noVoie = createString("noVoie");

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final StringPath temPayeUtil = createString("temPayeUtil");

    public final StringPath ville = createString("ville");

    public final com.mysema.query.sql.PrimaryKey<QAdresse> adressePk = createPrimaryKey(adrOrdre);

    public QAdresse(String variable) {
        super(QAdresse.class, forVariable(variable), "GRHUM", "ADRESSE");
        addMetadata();
    }

    public QAdresse(String variable, String schema, String table) {
        super(QAdresse.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdresse(Path<? extends QAdresse> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "ADRESSE");
        addMetadata();
    }

    public QAdresse(PathMetadata<?> metadata) {
        super(QAdresse.class, metadata, "GRHUM", "ADRESSE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrAdresse1, ColumnMetadata.named("ADR_ADRESSE1").withIndex(15).ofType(Types.VARCHAR).withSize(100));
        addMetadata(adrAdresse2, ColumnMetadata.named("ADR_ADRESSE2").withIndex(16).ofType(Types.VARCHAR).withSize(300));
        addMetadata(adrBp, ColumnMetadata.named("ADR_BP").withIndex(17).ofType(Types.VARCHAR).withSize(7));
        addMetadata(adrGpsLatitude, ColumnMetadata.named("ADR_GPS_LATITUDE").withIndex(25).ofType(Types.DECIMAL).withSize(5).withDigits(2));
        addMetadata(adrGpsLongitude, ColumnMetadata.named("ADR_GPS_LONGITUDE").withIndex(26).ofType(Types.DECIMAL).withSize(5).withDigits(2));
        addMetadata(adrListeRouge, ColumnMetadata.named("ADR_LISTE_ROUGE").withIndex(21).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(adrOrdre, ColumnMetadata.named("ADR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(adrUrlPere, ColumnMetadata.named("ADR_URL_PERE").withIndex(18).ofType(Types.VARCHAR).withSize(100));
        addMetadata(adrUrlRelative, ColumnMetadata.named("ADR_URL_RELATIVE").withIndex(19).ofType(Types.VARCHAR).withSize(100));
        addMetadata(adrUrlTemplate, ColumnMetadata.named("ADR_URL_TEMPLATE").withIndex(20).ofType(Types.VARCHAR).withSize(100));
        addMetadata(bisTer, ColumnMetadata.named("BIS_TER").withIndex(4).ofType(Types.VARCHAR).withSize(1));
        addMetadata(cImplantation, ColumnMetadata.named("C_IMPLANTATION").withIndex(12).ofType(Types.VARCHAR).withSize(2));
        addMetadata(codePostal, ColumnMetadata.named("CODE_POSTAL").withIndex(8).ofType(Types.VARCHAR).withSize(10));
        addMetadata(cPays, ColumnMetadata.named("C_PAYS").withIndex(10).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(cpEtranger, ColumnMetadata.named("CP_ETRANGER").withIndex(11).ofType(Types.VARCHAR).withSize(10));
        addMetadata(cpltLocalisationEtranger, ColumnMetadata.named("CPLT_LOCALISATION_ETRANGER").withIndex(30).ofType(Types.VARCHAR).withSize(300));
        addMetadata(cVoie, ColumnMetadata.named("C_VOIE").withIndex(5).ofType(Types.VARCHAR).withSize(4));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(22).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dDebVal, ColumnMetadata.named("D_DEB_VAL").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dFinVal, ColumnMetadata.named("D_FIN_VAL").withIndex(14).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(23).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(habitantChez, ColumnMetadata.named("HABITANT_CHEZ").withIndex(2).ofType(Types.VARCHAR).withSize(32));
        addMetadata(lieuDit, ColumnMetadata.named("LIEU_DIT").withIndex(29).ofType(Types.VARCHAR).withSize(60));
        addMetadata(localite, ColumnMetadata.named("LOCALITE").withIndex(7).ofType(Types.VARCHAR).withSize(60));
        addMetadata(nomVoie, ColumnMetadata.named("NOM_VOIE").withIndex(6).ofType(Types.VARCHAR).withSize(22));
        addMetadata(noVoie, ColumnMetadata.named("NO_VOIE").withIndex(3).ofType(Types.VARCHAR).withSize(4));
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(27).ofType(Types.DECIMAL).withSize(0));
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(28).ofType(Types.DECIMAL).withSize(0));
        addMetadata(temPayeUtil, ColumnMetadata.named("TEM_PAYE_UTIL").withIndex(24).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(ville, ColumnMetadata.named("VILLE").withIndex(9).ofType(Types.VARCHAR).withSize(60));
    }

}

