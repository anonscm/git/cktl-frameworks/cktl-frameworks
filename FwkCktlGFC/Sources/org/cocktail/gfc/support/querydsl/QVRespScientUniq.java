package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVRespScientUniq is a Querydsl query type for QVRespScientUniq
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVRespScientUniq extends com.mysema.query.sql.RelationalPathBase<QVRespScientUniq> {

    private static final long serialVersionUID = -1968876104;

    public static final QVRespScientUniq vRespScientUniq = new QVRespScientUniq("V_RESP_SCIENT_UNIQ");

    public final NumberPath<Long> apcOrdre = createNumber("apcOrdre", Long.class);

    public final NumberPath<Long> avtOrdre = createNumber("avtOrdre", Long.class);

    public QVRespScientUniq(String variable) {
        super(QVRespScientUniq.class, forVariable(variable), "GFC", "V_RESP_SCIENT_UNIQ");
        addMetadata();
    }

    public QVRespScientUniq(String variable, String schema, String table) {
        super(QVRespScientUniq.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVRespScientUniq(Path<? extends QVRespScientUniq> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_RESP_SCIENT_UNIQ");
        addMetadata();
    }

    public QVRespScientUniq(PathMetadata<?> metadata) {
        super(QVRespScientUniq.class, metadata, "GFC", "V_RESP_SCIENT_UNIQ");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(apcOrdre, ColumnMetadata.named("APC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(avtOrdre, ColumnMetadata.named("AVT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

