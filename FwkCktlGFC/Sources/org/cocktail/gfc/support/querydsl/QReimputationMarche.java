package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputationMarche is a Querydsl query type for QReimputationMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputationMarche extends com.mysema.query.sql.RelationalPathBase<QReimputationMarche> {

    private static final long serialVersionUID = -1845163904;

    public static final QReimputationMarche reimputationMarche = new QReimputationMarche("REIMPUTATION_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<java.math.BigDecimal> remaHtSaisie = createNumber("remaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> remaId = createNumber("remaId", Long.class);

    public final NumberPath<java.math.BigDecimal> remaMontantBudgetaire = createNumber("remaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> remaTtcSaisie = createNumber("remaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> remaTvaSaisie = createNumber("remaTvaSaisie", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputationMarche> reimputationMarchePk = createPrimaryKey(remaId);

    public final com.mysema.query.sql.ForeignKey<QAttribution> reimpMarcheAttOrdreFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputation> reimpMarcheReimIdFk = createForeignKey(reimId, "REIM_ID");

    public QReimputationMarche(String variable) {
        super(QReimputationMarche.class, forVariable(variable), "GFC", "REIMPUTATION_MARCHE");
        addMetadata();
    }

    public QReimputationMarche(String variable, String schema, String table) {
        super(QReimputationMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputationMarche(Path<? extends QReimputationMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION_MARCHE");
        addMetadata();
    }

    public QReimputationMarche(PathMetadata<?> metadata) {
        super(QReimputationMarche.class, metadata, "GFC", "REIMPUTATION_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(remaHtSaisie, ColumnMetadata.named("REMA_HT_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(remaId, ColumnMetadata.named("REMA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(remaMontantBudgetaire, ColumnMetadata.named("REMA_MONTANT_BUDGETAIRE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(remaTtcSaisie, ColumnMetadata.named("REMA_TTC_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(remaTvaSaisie, ColumnMetadata.named("REMA_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
    }

}

