package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDetailAttGlobale is a Querydsl query type for QVDetailAttGlobale
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDetailAttGlobale extends com.mysema.query.sql.RelationalPathBase<QVDetailAttGlobale> {

    private static final long serialVersionUID = -1127402457;

    public static final QVDetailAttGlobale vDetailAttGlobale = new QVDetailAttGlobale("V_DETAIL_ATT_GLOBALE");

    public final NumberPath<java.math.BigDecimal> aeeEngHt = createNumber("aeeEngHt", java.math.BigDecimal.class);

    public final NumberPath<Long> aeeExecution = createNumber("aeeExecution", Long.class);

    public final NumberPath<Long> aeeLiqHt = createNumber("aeeLiqHt", Long.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final StringPath cdeLib = createString("cdeLib");

    public final NumberPath<Long> cdeOrdre = createNumber("cdeOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateEng = createDateTime("dateEng", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public QVDetailAttGlobale(String variable) {
        super(QVDetailAttGlobale.class, forVariable(variable), "GFC", "V_DETAIL_ATT_GLOBALE");
        addMetadata();
    }

    public QVDetailAttGlobale(String variable, String schema, String table) {
        super(QVDetailAttGlobale.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDetailAttGlobale(Path<? extends QVDetailAttGlobale> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DETAIL_ATT_GLOBALE");
        addMetadata();
    }

    public QVDetailAttGlobale(PathMetadata<?> metadata) {
        super(QVDetailAttGlobale.class, metadata, "GFC", "V_DETAIL_ATT_GLOBALE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeeEngHt, ColumnMetadata.named("AEE_ENG_HT").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(aeeExecution, ColumnMetadata.named("AEE_EXECUTION").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeLiqHt, ColumnMetadata.named("AEE_LIQ_HT").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cdeLib, ColumnMetadata.named("CDE_LIB").withIndex(4).ofType(Types.VARCHAR).withSize(500));
        addMetadata(cdeOrdre, ColumnMetadata.named("CDE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dateEng, ColumnMetadata.named("DATE_ENG").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4));
    }

}

