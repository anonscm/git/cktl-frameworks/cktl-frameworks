package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeTrancheBudDepCp is a Querydsl query type for QOpeTrancheBudDepCp
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeTrancheBudDepCp extends com.mysema.query.sql.RelationalPathBase<QOpeTrancheBudDepCp> {

    private static final long serialVersionUID = 484076829;

    public static final QOpeTrancheBudDepCp opeTrancheBudDepCp = new QOpeTrancheBudDepCp("OPE_TRANCHE_BUD_DEP_CP");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final NumberPath<Long> idAdmDestinationDepense = createNumber("idAdmDestinationDepense", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idAdmNatureDep = createNumber("idAdmNatureDep", Long.class);

    public final NumberPath<Long> idBudEnveloppe = createNumber("idBudEnveloppe", Long.class);

    public final NumberPath<Long> idOpeTrancheBud = createNumber("idOpeTrancheBud", Long.class);

    public final NumberPath<Long> idOpeTrancheBudDepCp = createNumber("idOpeTrancheBudDepCp", Long.class);

    public final NumberPath<java.math.BigDecimal> montantCp = createNumber("montantCp", java.math.BigDecimal.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QOpeTrancheBudDepCp> opeTrancheBudDepCpPk = createPrimaryKey(idOpeTrancheBudDepCp);

    public final com.mysema.query.sql.ForeignKey<QAdmNatureDep> opeTrancheBudDepCpNatFk = createForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBud> opeTrancheBudDepCpTraFk = createForeignKey(idOpeTrancheBud, "ID_OPE_TRANCHE_BUD");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> opeTrancheBudDepCpEbFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepense> opeTrancheBudDepCpDestFk = createForeignKey(idAdmDestinationDepense, "ID_ADM_DESTINATION_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QBudEnveloppe> opeTrancheBudDepCpEnvFk = createForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepCp> _budPrevOpeCpOpeTraCpFk = createInvForeignKey(idOpeTrancheBudDepCp, "ID_OPE_TRANCHE_BUD_DEP_CP");

    public QOpeTrancheBudDepCp(String variable) {
        super(QOpeTrancheBudDepCp.class, forVariable(variable), "GFC", "OPE_TRANCHE_BUD_DEP_CP");
        addMetadata();
    }

    public QOpeTrancheBudDepCp(String variable, String schema, String table) {
        super(QOpeTrancheBudDepCp.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeTrancheBudDepCp(Path<? extends QOpeTrancheBudDepCp> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_TRANCHE_BUD_DEP_CP");
        addMetadata();
    }

    public QOpeTrancheBudDepCp(PathMetadata<?> metadata) {
        super(QOpeTrancheBudDepCp.class, metadata, "GFC", "OPE_TRANCHE_BUD_DEP_CP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(9).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(11).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(idAdmDestinationDepense, ColumnMetadata.named("ID_ADM_DESTINATION_DEPENSE").withIndex(7).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmNatureDep, ColumnMetadata.named("ID_ADM_NATURE_DEP").withIndex(6).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idBudEnveloppe, ColumnMetadata.named("ID_BUD_ENVELOPPE").withIndex(5).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeTrancheBud, ColumnMetadata.named("ID_OPE_TRANCHE_BUD").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeTrancheBudDepCp, ColumnMetadata.named("ID_OPE_TRANCHE_BUD_DEP_CP").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(montantCp, ColumnMetadata.named("MONTANT_CP").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(8).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(10).ofType(Types.DECIMAL).withSize(38));
    }

}

