package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAttributionExercice is a Querydsl query type for QVAttributionExercice
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAttributionExercice extends com.mysema.query.sql.RelationalPathBase<QVAttributionExercice> {

    private static final long serialVersionUID = 942113714;

    public static final QVAttributionExercice vAttributionExercice = new QVAttributionExercice("V_ATTRIBUTION_EXERCICE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public QVAttributionExercice(String variable) {
        super(QVAttributionExercice.class, forVariable(variable), "GFC", "V_ATTRIBUTION_EXERCICE");
        addMetadata();
    }

    public QVAttributionExercice(String variable, String schema, String table) {
        super(QVAttributionExercice.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAttributionExercice(Path<? extends QVAttributionExercice> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ATTRIBUTION_EXERCICE");
        addMetadata();
    }

    public QVAttributionExercice(PathMetadata<?> metadata) {
        super(QVAttributionExercice.class, metadata, "GFC", "V_ATTRIBUTION_EXERCICE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
    }

}

