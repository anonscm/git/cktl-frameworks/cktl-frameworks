package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecNumerotation is a Querydsl query type for QRecNumerotation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecNumerotation extends com.mysema.query.sql.RelationalPathBase<QRecNumerotation> {

    private static final long serialVersionUID = -1902590036;

    public static final QRecNumerotation recNumerotation = new QRecNumerotation("REC_NUMEROTATION");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> numId = createNumber("numId", Long.class);

    public final NumberPath<Long> numNumero = createNumber("numNumero", Long.class);

    public final NumberPath<Long> tnuId = createNumber("tnuId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRecNumerotation> numerotationPk = createPrimaryKey(numId);

    public final com.mysema.query.sql.ForeignKey<QRecTypeNumerotation> tnuIdFk = createForeignKey(tnuId, "TNU_ID");

    public final com.mysema.query.sql.ForeignKey<QGestion> gesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> exeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QRecNumerotation(String variable) {
        super(QRecNumerotation.class, forVariable(variable), "GFC", "REC_NUMEROTATION");
        addMetadata();
    }

    public QRecNumerotation(String variable, String schema, String table) {
        super(QRecNumerotation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecNumerotation(Path<? extends QRecNumerotation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REC_NUMEROTATION");
        addMetadata();
    }

    public QRecNumerotation(PathMetadata<?> metadata) {
        super(QRecNumerotation.class, metadata, "GFC", "REC_NUMEROTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10));
        addMetadata(numId, ColumnMetadata.named("NUM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(numNumero, ColumnMetadata.named("NUM_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnuId, ColumnMetadata.named("TNU_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

