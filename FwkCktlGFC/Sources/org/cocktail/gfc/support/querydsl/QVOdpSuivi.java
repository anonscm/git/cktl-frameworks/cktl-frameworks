package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVOdpSuivi is a Querydsl query type for QVOdpSuivi
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVOdpSuivi extends com.mysema.query.sql.RelationalPathBase<QVOdpSuivi> {

    private static final long serialVersionUID = 692614516;

    public static final QVOdpSuivi vOdpSuivi = new QVOdpSuivi("V_ODP_SUIVI");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath modCode = createString("modCode");

    public final StringPath modDom = createString("modDom");

    public final StringPath modLibelle = createString("modLibelle");

    public final DateTimePath<java.sql.Timestamp> odpDateSaisie = createDateTime("odpDateSaisie", java.sql.Timestamp.class);

    public final StringPath odpEtat = createString("odpEtat");

    public final NumberPath<java.math.BigDecimal> odpHt = createNumber("odpHt", java.math.BigDecimal.class);

    public final NumberPath<Long> odpNumero = createNumber("odpNumero", Long.class);

    public final NumberPath<Long> odpOrdre = createNumber("odpOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> odpTtc = createNumber("odpTtc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> odpTva = createNumber("odpTva", java.math.BigDecimal.class);

    public final NumberPath<Long> orgOrdre = createNumber("orgOrdre", Long.class);

    public final NumberPath<Long> oriOrdre = createNumber("oriOrdre", Long.class);

    public final NumberPath<Long> paiNumero = createNumber("paiNumero", Long.class);

    public final NumberPath<Long> paiOrdre = createNumber("paiOrdre", Long.class);

    public final NumberPath<Long> ribOrdre = createNumber("ribOrdre", Long.class);

    public final StringPath tviLibelle = createString("tviLibelle");

    public final DateTimePath<java.sql.Timestamp> virDateCreation = createDateTime("virDateCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> virDateValeur = createDateTime("virDateValeur", java.sql.Timestamp.class);

    public QVOdpSuivi(String variable) {
        super(QVOdpSuivi.class, forVariable(variable), "GFC", "V_ODP_SUIVI");
        addMetadata();
    }

    public QVOdpSuivi(String variable, String schema, String table) {
        super(QVOdpSuivi.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVOdpSuivi(Path<? extends QVOdpSuivi> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ODP_SUIVI");
        addMetadata();
    }

    public QVOdpSuivi(PathMetadata<?> metadata) {
        super(QVOdpSuivi.class, metadata, "GFC", "V_ODP_SUIVI");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(modCode, ColumnMetadata.named("MOD_CODE").withIndex(14).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(modDom, ColumnMetadata.named("MOD_DOM").withIndex(13).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(modLibelle, ColumnMetadata.named("MOD_LIBELLE").withIndex(15).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(odpDateSaisie, ColumnMetadata.named("ODP_DATE_SAISIE").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(odpEtat, ColumnMetadata.named("ODP_ETAT").withIndex(5).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(odpHt, ColumnMetadata.named("ODP_HT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(odpNumero, ColumnMetadata.named("ODP_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(odpOrdre, ColumnMetadata.named("ODP_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(odpTtc, ColumnMetadata.named("ODP_TTC").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(odpTva, ColumnMetadata.named("ODP_TVA").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(orgOrdre, ColumnMetadata.named("ORG_ORDRE").withIndex(18).ofType(Types.DECIMAL).withSize(38));
        addMetadata(oriOrdre, ColumnMetadata.named("ORI_ORDRE").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(paiNumero, ColumnMetadata.named("PAI_NUMERO").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(paiOrdre, ColumnMetadata.named("PAI_ORDRE").withIndex(17).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ribOrdre, ColumnMetadata.named("RIB_ORDRE").withIndex(19).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tviLibelle, ColumnMetadata.named("TVI_LIBELLE").withIndex(11).ofType(Types.VARCHAR).withSize(20));
        addMetadata(virDateCreation, ColumnMetadata.named("VIR_DATE_CREATION").withIndex(20).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(virDateValeur, ColumnMetadata.named("VIR_DATE_VALEUR").withIndex(12).ofType(Types.TIMESTAMP).withSize(7));
    }

}

