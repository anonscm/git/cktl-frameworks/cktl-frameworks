package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmOrigineRecette is a Querydsl query type for QVAdmOrigineRecette
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmOrigineRecette extends com.mysema.query.sql.RelationalPathBase<QVAdmOrigineRecette> {

    private static final long serialVersionUID = -613879514;

    public static final QVAdmOrigineRecette vAdmOrigineRecette = new QVAdmOrigineRecette("V_ADM_ORIGINE_RECETTE");

    public final StringPath abreviation = createString("abreviation");

    public final StringPath code = createString("code");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> fermeture = createDateTime("fermeture", java.sql.Timestamp.class);

    public final NumberPath<Long> idAdmOrigineRecette = createNumber("idAdmOrigineRecette", Long.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Integer> ordreAffichage = createNumber("ordreAffichage", Integer.class);

    public final DateTimePath<java.sql.Timestamp> ouverture = createDateTime("ouverture", java.sql.Timestamp.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public QVAdmOrigineRecette(String variable) {
        super(QVAdmOrigineRecette.class, forVariable(variable), "GFC", "V_ADM_ORIGINE_RECETTE");
        addMetadata();
    }

    public QVAdmOrigineRecette(String variable, String schema, String table) {
        super(QVAdmOrigineRecette.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmOrigineRecette(Path<? extends QVAdmOrigineRecette> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_ORIGINE_RECETTE");
        addMetadata();
    }

    public QVAdmOrigineRecette(PathMetadata<?> metadata) {
        super(QVAdmOrigineRecette.class, metadata, "GFC", "V_ADM_ORIGINE_RECETTE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(abreviation, ColumnMetadata.named("ABREVIATION").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(3).ofType(Types.VARCHAR).withSize(7).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fermeture, ColumnMetadata.named("FERMETURE").withIndex(7).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(idAdmOrigineRecette, ColumnMetadata.named("ID_ADM_ORIGINE_RECETTE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(ordreAffichage, ColumnMetadata.named("ORDRE_AFFICHAGE").withIndex(8).ofType(Types.DECIMAL).withSize(3));
        addMetadata(ouverture, ColumnMetadata.named("OUVERTURE").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0));
    }

}

