package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZEngageCtrlAction is a Querydsl query type for QZEngageCtrlAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZEngageCtrlAction extends com.mysema.query.sql.RelationalPathBase<QZEngageCtrlAction> {

    private static final long serialVersionUID = -471088005;

    public static final QZEngageCtrlAction zEngageCtrlAction = new QZEngageCtrlAction("Z_ENGAGE_CTRL_ACTION");

    public final DateTimePath<java.sql.Timestamp> eactDateSaisie = createDateTime("eactDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> eactHtSaisie = createNumber("eactHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> eactId = createNumber("eactId", Long.class);

    public final NumberPath<java.math.BigDecimal> eactMontantBudgetaire = createNumber("eactMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eactMontantBudgetaireReste = createNumber("eactMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eactTtcSaisie = createNumber("eactTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eactTvaSaisie = createNumber("eactTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final NumberPath<Long> zeactId = createNumber("zeactId", Long.class);

    public final NumberPath<Long> zengId = createNumber("zengId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZEngageCtrlAction> sysC0077616 = createPrimaryKey(zeactId);

    public final com.mysema.query.sql.ForeignKey<QZEngageBudget> zEngageCtrlActiZengIdFk = createForeignKey(zengId, "ZENG_ID");

    public QZEngageCtrlAction(String variable) {
        super(QZEngageCtrlAction.class, forVariable(variable), "GFC", "Z_ENGAGE_CTRL_ACTION");
        addMetadata();
    }

    public QZEngageCtrlAction(String variable, String schema, String table) {
        super(QZEngageCtrlAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZEngageCtrlAction(Path<? extends QZEngageCtrlAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_ENGAGE_CTRL_ACTION");
        addMetadata();
    }

    public QZEngageCtrlAction(PathMetadata<?> metadata) {
        super(QZEngageCtrlAction.class, metadata, "GFC", "Z_ENGAGE_CTRL_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(eactDateSaisie, ColumnMetadata.named("EACT_DATE_SAISIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(eactHtSaisie, ColumnMetadata.named("EACT_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactId, ColumnMetadata.named("EACT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(eactMontantBudgetaire, ColumnMetadata.named("EACT_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactMontantBudgetaireReste, ColumnMetadata.named("EACT_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactTtcSaisie, ColumnMetadata.named("EACT_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eactTvaSaisie, ColumnMetadata.named("EACT_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zeactId, ColumnMetadata.named("ZEACT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zengId, ColumnMetadata.named("ZENG_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

