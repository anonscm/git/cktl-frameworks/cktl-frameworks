package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepenseCtrlConvention is a Querydsl query type for QDepenseCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepenseCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QDepenseCtrlConvention> {

    private static final long serialVersionUID = 1866749381;

    public static final QDepenseCtrlConvention depenseCtrlConvention = new QDepenseCtrlConvention("DEPENSE_CTRL_CONVENTION");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> dconHtSaisie = createNumber("dconHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dconId = createNumber("dconId", Long.class);

    public final NumberPath<java.math.BigDecimal> dconMontantBudgetaire = createNumber("dconMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dconTtcSaisie = createNumber("dconTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dconTvaSaisie = createNumber("dconTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QDepenseCtrlConvention> depenseCtrlConventionPk = createPrimaryKey(dconId);

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> depenseCtrlConDepIdFk = createForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depenseCtrlConExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QDepenseCtrlConvention(String variable) {
        super(QDepenseCtrlConvention.class, forVariable(variable), "GFC", "DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QDepenseCtrlConvention(String variable, String schema, String table) {
        super(QDepenseCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepenseCtrlConvention(Path<? extends QDepenseCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QDepenseCtrlConvention(PathMetadata<?> metadata) {
        super(QDepenseCtrlConvention.class, metadata, "GFC", "DEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dconHtSaisie, ColumnMetadata.named("DCON_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dconId, ColumnMetadata.named("DCON_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dconMontantBudgetaire, ColumnMetadata.named("DCON_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dconTtcSaisie, ColumnMetadata.named("DCON_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dconTvaSaisie, ColumnMetadata.named("DCON_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

