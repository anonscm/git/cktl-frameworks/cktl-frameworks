package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmUtilisateurEb is a Querydsl query type for QAdmUtilisateurEb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmUtilisateurEb extends com.mysema.query.sql.RelationalPathBase<QAdmUtilisateurEb> {

    private static final long serialVersionUID = 915821047;

    public static final QAdmUtilisateurEb admUtilisateurEb = new QAdmUtilisateurEb("ADM_UTILISATEUR_EB");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idAdmUtilisateurEb = createNumber("idAdmUtilisateurEb", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmUtilisateurEb> admUtilisateurEbPk = createPrimaryKey(idAdmUtilisateurEb);

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> admUtilisateurEbUtlFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public QAdmUtilisateurEb(String variable) {
        super(QAdmUtilisateurEb.class, forVariable(variable), "GFC", "ADM_UTILISATEUR_EB");
        addMetadata();
    }

    public QAdmUtilisateurEb(String variable, String schema, String table) {
        super(QAdmUtilisateurEb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmUtilisateurEb(Path<? extends QAdmUtilisateurEb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_UTILISATEUR_EB");
        addMetadata();
    }

    public QAdmUtilisateurEb(PathMetadata<?> metadata) {
        super(QAdmUtilisateurEb.class, metadata, "GFC", "ADM_UTILISATEUR_EB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmUtilisateurEb, ColumnMetadata.named("ID_ADM_UTILISATEUR_EB").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

