package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmOrigineFinancement is a Querydsl query type for QImmOrigineFinancement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmOrigineFinancement extends com.mysema.query.sql.RelationalPathBase<QImmOrigineFinancement> {

    private static final long serialVersionUID = -211144191;

    public static final QImmOrigineFinancement immOrigineFinancement = new QImmOrigineFinancement("IMM_ORIGINE_FINANCEMENT");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> orgfId = createNumber("orgfId", Long.class);

    public final StringPath orgfLibelle = createString("orgfLibelle");

    public final StringPath pcoNum = createString("pcoNum");

    public final com.mysema.query.sql.PrimaryKey<QImmOrigineFinancement> immOrigineFinancementPk = createPrimaryKey(orgfId);

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> _originefinancementFk = createInvForeignKey(orgfId, "ORGF_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventComptOrig> _invComptOOrgfIdFk = createInvForeignKey(orgfId, "ORGF_ID");

    public QImmOrigineFinancement(String variable) {
        super(QImmOrigineFinancement.class, forVariable(variable), "GFC", "IMM_ORIGINE_FINANCEMENT");
        addMetadata();
    }

    public QImmOrigineFinancement(String variable, String schema, String table) {
        super(QImmOrigineFinancement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmOrigineFinancement(Path<? extends QImmOrigineFinancement> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_ORIGINE_FINANCEMENT");
        addMetadata();
    }

    public QImmOrigineFinancement(PathMetadata<?> metadata) {
        super(QImmOrigineFinancement.class, metadata, "GFC", "IMM_ORIGINE_FINANCEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(orgfId, ColumnMetadata.named("ORGF_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(orgfLibelle, ColumnMetadata.named("ORGF_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(200));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20));
    }

}

