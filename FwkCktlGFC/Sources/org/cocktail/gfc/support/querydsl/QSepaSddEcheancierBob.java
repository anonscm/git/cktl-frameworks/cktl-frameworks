package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSepaSddEcheancierBob is a Querydsl query type for QSepaSddEcheancierBob
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSepaSddEcheancierBob extends com.mysema.query.sql.RelationalPathBase<QSepaSddEcheancierBob> {

    private static final long serialVersionUID = 2144546699;

    public static final QSepaSddEcheancierBob sepaSddEcheancierBob = new QSepaSddEcheancierBob("SEPA_SDD_ECHEANCIER_BOB");

    public final NumberPath<Long> bobOrdre = createNumber("bobOrdre", Long.class);

    public final NumberPath<Long> idSepaSddEcheancier = createNumber("idSepaSddEcheancier", Long.class);

    public final NumberPath<Long> sebId = createNumber("sebId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QSepaSddEcheancierBob> sepaSddEcheancierBobPk = createPrimaryKey(sebId);

    public final com.mysema.query.sql.ForeignKey<QBordereauBrouillard> sepaSddEcheancierBobBobFk = createForeignKey(bobOrdre, "BOB_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QSepaSddEcheancier> sepaSddEcheancierBobEchFk = createForeignKey(idSepaSddEcheancier, "ID_SEPA_SDD_ECHEANCIER");

    public QSepaSddEcheancierBob(String variable) {
        super(QSepaSddEcheancierBob.class, forVariable(variable), "GFC", "SEPA_SDD_ECHEANCIER_BOB");
        addMetadata();
    }

    public QSepaSddEcheancierBob(String variable, String schema, String table) {
        super(QSepaSddEcheancierBob.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSepaSddEcheancierBob(Path<? extends QSepaSddEcheancierBob> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SEPA_SDD_ECHEANCIER_BOB");
        addMetadata();
    }

    public QSepaSddEcheancierBob(PathMetadata<?> metadata) {
        super(QSepaSddEcheancierBob.class, metadata, "GFC", "SEPA_SDD_ECHEANCIER_BOB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bobOrdre, ColumnMetadata.named("BOB_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idSepaSddEcheancier, ColumnMetadata.named("ID_SEPA_SDD_ECHEANCIER").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(sebId, ColumnMetadata.named("SEB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

