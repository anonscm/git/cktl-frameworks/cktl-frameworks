package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmOrigineRecette is a Querydsl query type for QAdmOrigineRecette
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmOrigineRecette extends com.mysema.query.sql.RelationalPathBase<QAdmOrigineRecette> {

    private static final long serialVersionUID = 1041986906;

    public static final QAdmOrigineRecette admOrigineRecette = new QAdmOrigineRecette("ADM_ORIGINE_RECETTE");

    public final StringPath abreviation = createString("abreviation");

    public final StringPath code = createString("code");

    public final DateTimePath<java.sql.Timestamp> fermeture = createDateTime("fermeture", java.sql.Timestamp.class);

    public final NumberPath<Long> idAdmOrigineRecette = createNumber("idAdmOrigineRecette", Long.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Integer> ordreAffichage = createNumber("ordreAffichage", Integer.class);

    public final DateTimePath<java.sql.Timestamp> ouverture = createDateTime("ouverture", java.sql.Timestamp.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmOrigineRecette> admOrigineRecettePk = createPrimaryKey(idAdmOrigineRecette);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCataloguePrestation> _catalogueLolfIdRecetteFk = createInvForeignKey(idAdmOrigineRecette, "LOLF_ID_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraRec> _budTraOpeRecOrigFk = createInvForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QBudBudgetRec> _budBudgetRecOrigFk = createInvForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlAction> _ractLolfIdFk = createInvForeignKey(idAdmOrigineRecette, "LOLF_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecetteExer> _admOrigRecExeOriFk = createInvForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationLolfIdFk = createInvForeignKey(idAdmOrigineRecette, "LOLF_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapLolfIdFk = createInvForeignKey(idAdmOrigineRecette, "LOLF_ID");

    public final com.mysema.query.sql.ForeignKey<QSbRecette> _recLolfFk = createInvForeignKey(idAdmOrigineRecette, "LOLF_ID");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudRec> _opeTrancheBudRecOrigFk = createInvForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevHorsOpRec> _budPrevHorsOpRecOrigFk = createInvForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QCptbudEcriture> _cptbudEcrOrigFk = createInvForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlAction> _factLolfIdFk = createInvForeignKey(idAdmOrigineRecette, "LOLF_ID");

    public QAdmOrigineRecette(String variable) {
        super(QAdmOrigineRecette.class, forVariable(variable), "GFC", "ADM_ORIGINE_RECETTE");
        addMetadata();
    }

    public QAdmOrigineRecette(String variable, String schema, String table) {
        super(QAdmOrigineRecette.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmOrigineRecette(Path<? extends QAdmOrigineRecette> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_ORIGINE_RECETTE");
        addMetadata();
    }

    public QAdmOrigineRecette(PathMetadata<?> metadata) {
        super(QAdmOrigineRecette.class, metadata, "GFC", "ADM_ORIGINE_RECETTE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(abreviation, ColumnMetadata.named("ABREVIATION").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(2).ofType(Types.VARCHAR).withSize(7).notNull());
        addMetadata(fermeture, ColumnMetadata.named("FERMETURE").withIndex(6).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(idAdmOrigineRecette, ColumnMetadata.named("ID_ADM_ORIGINE_RECETTE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(ordreAffichage, ColumnMetadata.named("ORDRE_AFFICHAGE").withIndex(7).ofType(Types.DECIMAL).withSize(3));
        addMetadata(ouverture, ColumnMetadata.named("OUVERTURE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0));
    }

}

