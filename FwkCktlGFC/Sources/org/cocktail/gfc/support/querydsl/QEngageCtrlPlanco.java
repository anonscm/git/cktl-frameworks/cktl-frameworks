package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEngageCtrlPlanco is a Querydsl query type for QEngageCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEngageCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QEngageCtrlPlanco> {

    private static final long serialVersionUID = 1256547554;

    public static final QEngageCtrlPlanco engageCtrlPlanco = new QEngageCtrlPlanco("ENGAGE_CTRL_PLANCO");

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final DateTimePath<java.sql.Timestamp> epcoDateSaisie = createDateTime("epcoDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> epcoHtSaisie = createNumber("epcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> epcoId = createNumber("epcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> epcoMontantBudgetaire = createNumber("epcoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> epcoMontantBudgetaireReste = createNumber("epcoMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> epcoTtcSaisie = createNumber("epcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> epcoTvaSaisie = createNumber("epcoTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final com.mysema.query.sql.PrimaryKey<QEngageCtrlPlanco> engageCtrlPlancoPk = createPrimaryKey(epcoId);

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> engageCtrlPlanEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> engageCtrlPlanExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QEngageCtrlPlanco(String variable) {
        super(QEngageCtrlPlanco.class, forVariable(variable), "GFC", "ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public QEngageCtrlPlanco(String variable, String schema, String table) {
        super(QEngageCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEngageCtrlPlanco(Path<? extends QEngageCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public QEngageCtrlPlanco(PathMetadata<?> metadata) {
        super(QEngageCtrlPlanco.class, metadata, "GFC", "ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(epcoDateSaisie, ColumnMetadata.named("EPCO_DATE_SAISIE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(epcoHtSaisie, ColumnMetadata.named("EPCO_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoId, ColumnMetadata.named("EPCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(epcoMontantBudgetaire, ColumnMetadata.named("EPCO_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoMontantBudgetaireReste, ColumnMetadata.named("EPCO_MONTANT_BUDGETAIRE_RESTE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoTtcSaisie, ColumnMetadata.named("EPCO_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoTvaSaisie, ColumnMetadata.named("EPCO_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

