package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVExecDateTmp is a Querydsl query type for QVExecDateTmp
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVExecDateTmp extends com.mysema.query.sql.RelationalPathBase<QVExecDateTmp> {

    private static final long serialVersionUID = 1563899363;

    public static final QVExecDateTmp vExecDateTmp = new QVExecDateTmp("V_EXEC_DATE_TMP");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateEng = createDateTime("dateEng", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateExec = createDateTime("dateExec", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateLiqu = createDateTime("dateLiqu", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public QVExecDateTmp(String variable) {
        super(QVExecDateTmp.class, forVariable(variable), "GFC", "V_EXEC_DATE_TMP");
        addMetadata();
    }

    public QVExecDateTmp(String variable, String schema, String table) {
        super(QVExecDateTmp.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVExecDateTmp(Path<? extends QVExecDateTmp> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EXEC_DATE_TMP");
        addMetadata();
    }

    public QVExecDateTmp(PathMetadata<?> metadata) {
        super(QVExecDateTmp.class, metadata, "GFC", "V_EXEC_DATE_TMP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dateEng, ColumnMetadata.named("DATE_ENG").withIndex(3).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateExec, ColumnMetadata.named("DATE_EXEC").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateLiqu, ColumnMetadata.named("DATE_LIQU").withIndex(4).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

