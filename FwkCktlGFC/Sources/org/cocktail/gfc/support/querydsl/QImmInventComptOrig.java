package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventComptOrig is a Querydsl query type for QImmInventComptOrig
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventComptOrig extends com.mysema.query.sql.RelationalPathBase<QImmInventComptOrig> {

    private static final long serialVersionUID = 1581406372;

    public static final QImmInventComptOrig immInventComptOrig = new QImmInventComptOrig("IMM_INVENT_COMPT_ORIG");

    public final NumberPath<Long> icorId = createNumber("icorId", Long.class);

    public final NumberPath<java.math.BigDecimal> icorMontant = createNumber("icorMontant", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> icorPourcentage = createNumber("icorPourcentage", java.math.BigDecimal.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> orgfId = createNumber("orgfId", Long.class);

    public final NumberPath<Long> titId = createNumber("titId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInventComptOrig> immInventComptOrigPk = createPrimaryKey(icorId);

    public final com.mysema.query.sql.ForeignKey<QTitre> invComptOTitIdFk = createForeignKey(titId, "TIT_ID");

    public final com.mysema.query.sql.ForeignKey<QImmOrigineFinancement> invComptOOrgfIdFk = createForeignKey(orgfId, "ORGF_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> invComptOInvcIdFk = createForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmAmortissementOrigine> _amooInvComptableOrigFk = createInvForeignKey(icorId, "ICOR_ID");

    public QImmInventComptOrig(String variable) {
        super(QImmInventComptOrig.class, forVariable(variable), "GFC", "IMM_INVENT_COMPT_ORIG");
        addMetadata();
    }

    public QImmInventComptOrig(String variable, String schema, String table) {
        super(QImmInventComptOrig.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventComptOrig(Path<? extends QImmInventComptOrig> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_COMPT_ORIG");
        addMetadata();
    }

    public QImmInventComptOrig(PathMetadata<?> metadata) {
        super(QImmInventComptOrig.class, metadata, "GFC", "IMM_INVENT_COMPT_ORIG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(icorId, ColumnMetadata.named("ICOR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(icorMontant, ColumnMetadata.named("ICOR_MONTANT").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(icorPourcentage, ColumnMetadata.named("ICOR_POURCENTAGE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(orgfId, ColumnMetadata.named("ORGF_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
    }

}

