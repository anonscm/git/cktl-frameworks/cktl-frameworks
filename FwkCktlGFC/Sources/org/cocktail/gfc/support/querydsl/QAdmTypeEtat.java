package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypeEtat is a Querydsl query type for QAdmTypeEtat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypeEtat extends com.mysema.query.sql.RelationalPathBase<QAdmTypeEtat> {

    private static final long serialVersionUID = 2002888357;

    public static final QAdmTypeEtat admTypeEtat = new QAdmTypeEtat("ADM_TYPE_ETAT");

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final StringPath tyetLibelle = createString("tyetLibelle");

    public final com.mysema.query.sql.PrimaryKey<QAdmTypeEtat> typeEtatPk = createPrimaryKey(tyetId);

    public final com.mysema.query.sql.ForeignKey<QCatCatalogue> _catCatalogueTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> _admCoanTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureRec> _admNatureRecTypeEtatFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QPlanComptableAmo> _pcoaTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmParametre> _admParNiveauModifFk = createInvForeignKey(tyetId, "PAR_NIVEAU_MODIF");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecette> _admTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> _commandeTyetIdImprimableFk = createInvForeignKey(tyetId, "TYET_ID_IMPRIMABLE");

    public final com.mysema.query.sql.ForeignKey<QSepaSddMandat> _sepaSddMandatTypeEtatFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEbExer> _admEbExerOpeObligFk = createInvForeignKey(tyetId, "OPERATION_OBLIGATOIRE");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> _admCodeAnalytiqueCmdepasFk = createInvForeignKey(tyetId, "CAN_MONTANT_DEPASSEMENT");

    public final com.mysema.query.sql.ForeignKey<QVisaAvMission> _visaAvMissionTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QRecRecette> _recRecetteTypetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEbExer> _admEbExerCanalObligFk = createInvForeignKey(tyetId, "CANAL_OBLIGATOIRE");

    public final com.mysema.query.sql.ForeignKey<QExtourneLiqDef> _extourneLiqDefTyetidFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QTypeRetenue> _typeRetenueTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QSepaSddMandatHisto> _sepaSddMandatHistoTyetFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QMandat> _mandatManAttentePaiementFk = createInvForeignKey(tyetId, "MAN_ATTENTE_PAIEMENT");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> _canPublicFk = createInvForeignKey(tyetId, "CAN_PUBLIC");

    public final com.mysema.query.sql.ForeignKey<QAdmImTypeTaux> _admImTypeTauxTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> _canUtilisableFk = createInvForeignKey(tyetId, "CAN_UTILISABLE");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> _admTauxProrataTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeNatureBudget> _admTypeNatureBudgetTyetFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QIm> _imTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> _utilisateurTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QFacture> _factureTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QBudBudgetEbEtat> _budBudgetEbEtatTyetFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> _commandeTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEbExer> _admEbExerOpAutoriseesFk = createInvForeignKey(tyetId, "OP_AUTORISEES");

    public final com.mysema.query.sql.ForeignKey<QAdmTva> _admTvaTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBud> _opeTrancheBudEtatFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogueArticle> _catCatalogueArticleTyetFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> _admTypeCreditTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmPreference> _admPrefPersonnalisable_fk = createInvForeignKey(tyetId, "PREF_PERSONNALISABLE");

    public final com.mysema.query.sql.ForeignKey<QAdmDestinationDepense> _admLolfdTyetIdFk = createInvForeignKey(tyetId, "TYET_ID");

    public QAdmTypeEtat(String variable) {
        super(QAdmTypeEtat.class, forVariable(variable), "GFC", "ADM_TYPE_ETAT");
        addMetadata();
    }

    public QAdmTypeEtat(String variable, String schema, String table) {
        super(QAdmTypeEtat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypeEtat(Path<? extends QAdmTypeEtat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_ETAT");
        addMetadata();
    }

    public QAdmTypeEtat(PathMetadata<?> metadata) {
        super(QAdmTypeEtat.class, metadata, "GFC", "ADM_TYPE_ETAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetLibelle, ColumnMetadata.named("TYET_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

