package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVUtilisateurFonctContrat is a Querydsl query type for QVUtilisateurFonctContrat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVUtilisateurFonctContrat extends com.mysema.query.sql.RelationalPathBase<QVUtilisateurFonctContrat> {

    private static final long serialVersionUID = 292096683;

    public static final QVUtilisateurFonctContrat vUtilisateurFonctContrat = new QVUtilisateurFonctContrat("V_UTILISATEUR_FONCT_CONTRAT");

    public final SimplePath<Object> conObjet = createSimple("conObjet", Object.class);

    public final SimplePath<Object> conReferenceExterne = createSimple("conReferenceExterne", Object.class);

    public final SimplePath<Object> convention = createSimple("convention", Object.class);

    public final SimplePath<Object> fonCategorie = createSimple("fonCategorie", Object.class);

    public final SimplePath<Object> fonIdInterne = createSimple("fonIdInterne", Object.class);

    public final SimplePath<Object> fonLibelle = createSimple("fonLibelle", Object.class);

    public final SimplePath<Object> fonOrdre = createSimple("fonOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> noIndividu = createSimple("noIndividu", Object.class);

    public final SimplePath<Object> nom = createSimple("nom", Object.class);

    public final SimplePath<Object> persId = createSimple("persId", Object.class);

    public final SimplePath<Object> ufcId = createSimple("ufcId", Object.class);

    public final SimplePath<Object> ufOrdre = createSimple("ufOrdre", Object.class);

    public final SimplePath<Object> utlOrdre = createSimple("utlOrdre", Object.class);

    public QVUtilisateurFonctContrat(String variable) {
        super(QVUtilisateurFonctContrat.class, forVariable(variable), "GFC", "V_UTILISATEUR_FONCT_CONTRAT");
        addMetadata();
    }

    public QVUtilisateurFonctContrat(String variable, String schema, String table) {
        super(QVUtilisateurFonctContrat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVUtilisateurFonctContrat(Path<? extends QVUtilisateurFonctContrat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_UTILISATEUR_FONCT_CONTRAT");
        addMetadata();
    }

    public QVUtilisateurFonctContrat(PathMetadata<?> metadata) {
        super(QVUtilisateurFonctContrat.class, metadata, "GFC", "V_UTILISATEUR_FONCT_CONTRAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(conObjet, ColumnMetadata.named("CON_OBJET").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(conReferenceExterne, ColumnMetadata.named("CON_REFERENCE_EXTERNE").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(convention, ColumnMetadata.named("CONVENTION").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(fonCategorie, ColumnMetadata.named("FON_CATEGORIE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(fonIdInterne, ColumnMetadata.named("FON_ID_INTERNE").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(fonLibelle, ColumnMetadata.named("FON_LIBELLE").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(fonOrdre, ColumnMetadata.named("FON_ORDRE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(noIndividu, ColumnMetadata.named("NO_INDIVIDU").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(nom, ColumnMetadata.named("NOM").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(ufcId, ColumnMetadata.named("UFC_ID").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(ufOrdre, ColumnMetadata.named("UF_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

