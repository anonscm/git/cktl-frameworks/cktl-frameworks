package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVMarchesCatalogue is a Querydsl query type for QVMarchesCatalogue
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVMarchesCatalogue extends com.mysema.query.sql.RelationalPathBase<QVMarchesCatalogue> {

    private static final long serialVersionUID = 468689645;

    public static final QVMarchesCatalogue vMarchesCatalogue = new QVMarchesCatalogue("V_MARCHES_CATALOGUE");

    public final SimplePath<Object> attOrdre = createSimple("attOrdre", Object.class);

    public final SimplePath<Object> catDesc = createSimple("catDesc", Object.class);

    public final SimplePath<Object> catOrdre = createSimple("catOrdre", Object.class);

    public final SimplePath<Object> catOrdrePere = createSimple("catOrdrePere", Object.class);

    public final SimplePath<Object> catPrixHt = createSimple("catPrixHt", Object.class);

    public final SimplePath<Object> catPrixTtc = createSimple("catPrixTtc", Object.class);

    public final SimplePath<Object> catRef = createSimple("catRef", Object.class);

    public final SimplePath<Object> cmOrdre = createSimple("cmOrdre", Object.class);

    public final SimplePath<Object> tvaId = createSimple("tvaId", Object.class);

    public final SimplePath<Object> tyarId = createSimple("tyarId", Object.class);

    public final SimplePath<Object> tyetId = createSimple("tyetId", Object.class);

    public QVMarchesCatalogue(String variable) {
        super(QVMarchesCatalogue.class, forVariable(variable), "GFC", "V_MARCHES_CATALOGUE");
        addMetadata();
    }

    public QVMarchesCatalogue(String variable, String schema, String table) {
        super(QVMarchesCatalogue.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVMarchesCatalogue(Path<? extends QVMarchesCatalogue> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_MARCHES_CATALOGUE");
        addMetadata();
    }

    public QVMarchesCatalogue(PathMetadata<?> metadata) {
        super(QVMarchesCatalogue.class, metadata, "GFC", "V_MARCHES_CATALOGUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(catDesc, ColumnMetadata.named("CAT_DESC").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(catOrdre, ColumnMetadata.named("CAT_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(catOrdrePere, ColumnMetadata.named("CAT_ORDRE_PERE").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(catPrixHt, ColumnMetadata.named("CAT_PRIX_HT").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(catPrixTtc, ColumnMetadata.named("CAT_PRIX_TTC").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(catRef, ColumnMetadata.named("CAT_REF").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(tyarId, ColumnMetadata.named("TYAR_ID").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(9).ofType(Types.OTHER).withSize(0));
    }

}

