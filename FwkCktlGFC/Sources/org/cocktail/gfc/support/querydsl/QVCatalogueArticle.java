package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCatalogueArticle is a Querydsl query type for QVCatalogueArticle
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCatalogueArticle extends com.mysema.query.sql.RelationalPathBase<QVCatalogueArticle> {

    private static final long serialVersionUID = -676615678;

    public static final QVCatalogueArticle vCatalogueArticle = new QVCatalogueArticle("V_CATALOGUE_ARTICLE");

    public final SimplePath<Object> attOrdre = createSimple("attOrdre", Object.class);

    public final SimplePath<Object> caarId = createSimple("caarId", Object.class);

    public final SimplePath<Object> caarIdPere = createSimple("caarIdPere", Object.class);

    public final SimplePath<Object> caarLibelle = createSimple("caarLibelle", Object.class);

    public final SimplePath<Object> caarPrixHt = createSimple("caarPrixHt", Object.class);

    public final SimplePath<Object> caarPrixTtc = createSimple("caarPrixTtc", Object.class);

    public final SimplePath<Object> caarReference = createSimple("caarReference", Object.class);

    public final SimplePath<Object> catDateDebut = createSimple("catDateDebut", Object.class);

    public final SimplePath<Object> catDateFin = createSimple("catDateFin", Object.class);

    public final SimplePath<Object> catId = createSimple("catId", Object.class);

    public final SimplePath<Object> catLibelle = createSimple("catLibelle", Object.class);

    public final SimplePath<Object> cmOrdre = createSimple("cmOrdre", Object.class);

    public final SimplePath<Object> fouOrdre = createSimple("fouOrdre", Object.class);

    public final SimplePath<Object> tvaId = createSimple("tvaId", Object.class);

    public final SimplePath<Object> tyapId = createSimple("tyapId", Object.class);

    public final SimplePath<Object> tyarId = createSimple("tyarId", Object.class);

    public final SimplePath<Object> tyetId = createSimple("tyetId", Object.class);

    public QVCatalogueArticle(String variable) {
        super(QVCatalogueArticle.class, forVariable(variable), "GFC", "V_CATALOGUE_ARTICLE");
        addMetadata();
    }

    public QVCatalogueArticle(String variable, String schema, String table) {
        super(QVCatalogueArticle.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCatalogueArticle(Path<? extends QVCatalogueArticle> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CATALOGUE_ARTICLE");
        addMetadata();
    }

    public QVCatalogueArticle(PathMetadata<?> metadata) {
        super(QVCatalogueArticle.class, metadata, "GFC", "V_CATALOGUE_ARTICLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(caarId, ColumnMetadata.named("CAAR_ID").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(caarIdPere, ColumnMetadata.named("CAAR_ID_PERE").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(caarLibelle, ColumnMetadata.named("CAAR_LIBELLE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(caarPrixHt, ColumnMetadata.named("CAAR_PRIX_HT").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(caarPrixTtc, ColumnMetadata.named("CAAR_PRIX_TTC").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(caarReference, ColumnMetadata.named("CAAR_REFERENCE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(catDateDebut, ColumnMetadata.named("CAT_DATE_DEBUT").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(catDateFin, ColumnMetadata.named("CAT_DATE_FIN").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(catLibelle, ColumnMetadata.named("CAT_LIBELLE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(tyarId, ColumnMetadata.named("TYAR_ID").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(11).ofType(Types.OTHER).withSize(0));
    }

}

