package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmPreference is a Querydsl query type for QAdmPreference
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmPreference extends com.mysema.query.sql.RelationalPathBase<QAdmPreference> {

    private static final long serialVersionUID = 994557380;

    public static final QAdmPreference admPreference = new QAdmPreference("ADM_PREFERENCE");

    public final StringPath prefDefaultValue = createString("prefDefaultValue");

    public final StringPath prefDescription = createString("prefDescription");

    public final NumberPath<Long> prefId = createNumber("prefId", Long.class);

    public final StringPath prefKey = createString("prefKey");

    public final NumberPath<Long> prefPersonnalisable = createNumber("prefPersonnalisable", Long.class);

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmPreference> sysC0077904 = createPrimaryKey(prefId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplication> admPreferenceTyapIdFk = createForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admPrefPersonnalisable_fk = createForeignKey(prefPersonnalisable, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurPreference> _admUpPrefIdFk = createInvForeignKey(prefId, "PREF_ID");

    public QAdmPreference(String variable) {
        super(QAdmPreference.class, forVariable(variable), "GFC", "ADM_PREFERENCE");
        addMetadata();
    }

    public QAdmPreference(String variable, String schema, String table) {
        super(QAdmPreference.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmPreference(Path<? extends QAdmPreference> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_PREFERENCE");
        addMetadata();
    }

    public QAdmPreference(PathMetadata<?> metadata) {
        super(QAdmPreference.class, metadata, "GFC", "ADM_PREFERENCE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(prefDefaultValue, ColumnMetadata.named("PREF_DEFAULT_VALUE").withIndex(3).ofType(Types.VARCHAR).withSize(500));
        addMetadata(prefDescription, ColumnMetadata.named("PREF_DESCRIPTION").withIndex(4).ofType(Types.VARCHAR).withSize(500));
        addMetadata(prefId, ColumnMetadata.named("PREF_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prefKey, ColumnMetadata.named("PREF_KEY").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(prefPersonnalisable, ColumnMetadata.named("PREF_PERSONNALISABLE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

