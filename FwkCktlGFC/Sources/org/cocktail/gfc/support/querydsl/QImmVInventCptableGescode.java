package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVInventCptableGescode is a Querydsl query type for QImmVInventCptableGescode
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVInventCptableGescode extends com.mysema.query.sql.RelationalPathBase<QImmVInventCptableGescode> {

    private static final long serialVersionUID = 865613129;

    public static final QImmVInventCptableGescode immVInventCptableGescode = new QImmVInventCptableGescode("IMM_V_INVENT_CPTABLE_GESCODE");

    public final StringPath gesCode = createString("gesCode");

    public final StringPath gesCodeAlt = createString("gesCodeAlt");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public QImmVInventCptableGescode(String variable) {
        super(QImmVInventCptableGescode.class, forVariable(variable), "GFC", "IMM_V_INVENT_CPTABLE_GESCODE");
        addMetadata();
    }

    public QImmVInventCptableGescode(String variable, String schema, String table) {
        super(QImmVInventCptableGescode.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVInventCptableGescode(Path<? extends QImmVInventCptableGescode> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_INVENT_CPTABLE_GESCODE");
        addMetadata();
    }

    public QImmVInventCptableGescode(PathMetadata<?> metadata) {
        super(QImmVInventCptableGescode.class, metadata, "GFC", "IMM_V_INVENT_CPTABLE_GESCODE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(gesCodeAlt, ColumnMetadata.named("GES_CODE_ALT").withIndex(3).ofType(Types.VARCHAR).withSize(10));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

