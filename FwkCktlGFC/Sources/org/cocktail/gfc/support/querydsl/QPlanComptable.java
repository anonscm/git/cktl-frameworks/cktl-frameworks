package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPlanComptable is a Querydsl query type for QPlanComptable
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPlanComptable extends com.mysema.query.sql.RelationalPathBase<QPlanComptable> {

    private static final long serialVersionUID = 1942727061;

    public static final QPlanComptable planComptable = new QPlanComptable("PLAN_COMPTABLE");

    public final StringPath pcoBudgetaire = createString("pcoBudgetaire");

    public final StringPath pcoCompteBe = createString("pcoCompteBe");

    public final StringPath pcoEmargement = createString("pcoEmargement");

    public final StringPath pcoJBe = createString("pcoJBe");

    public final StringPath pcoJExercice = createString("pcoJExercice");

    public final StringPath pcoJFinExercice = createString("pcoJFinExercice");

    public final StringPath pcoLibelle = createString("pcoLibelle");

    public final StringPath pcoNature = createString("pcoNature");

    public final NumberPath<Long> pcoNiveau = createNumber("pcoNiveau", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath pcoSensEmargement = createString("pcoSensEmargement");

    public final StringPath pcoSensSolde = createString("pcoSensSolde");

    public final StringPath pcoValidite = createString("pcoValidite");

    public final com.mysema.query.sql.PrimaryKey<QPlanComptable> planComptablePk = createPrimaryKey(pcoNum);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> sysC0078788 = createForeignKey(pcoCompteBe, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapPcoNumTvaFk = createInvForeignKey(pcoNum, "PCO_NUM_TVA");

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> _ecrDetailFk2 = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QOdpaiementBrouillard> _odpaiementBrouillardPcoNFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlanco> _rpcoPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPlancoAmortissement> _pcaPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapPcoNumCtpFk = createInvForeignKey(pcoNum, "PCO_NUM_CTP");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlancoCtp> _rpcoctpPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlPlanco> _pdepenseCtrlPlaPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlPlanco> _fpcoPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandePcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPlancoRelance> _plancoRelanceFk1 = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QReimputationPlanco> _reimpPlancoPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QBordereauInfo> _bordereauInfoPcoNumVisaFk = createInvForeignKey(pcoNum, "PCO_NUM_VISA");

    public final com.mysema.query.sql.ForeignKey<QPlanComptableExer> _plancoExerPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPlanComptableExer> _plancoExerPcoCompteBeFk = createInvForeignKey(pcoNum, "PCO_COMPTE_BE");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> _sysC0078788 = createInvForeignKey(pcoNum, "PCO_COMPTE_BE");

    public final com.mysema.query.sql.ForeignKey<QCataloguePrestation> _cataloguePcoNumDepenseFk = createInvForeignKey(pcoNum, "PCO_NUM_DEPENSE");

    public final com.mysema.query.sql.ForeignKey<QCataloguePrestation> _cataloguePcoNumRecetteFk = createInvForeignKey(pcoNum, "PCO_NUM_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QArticlePrestation> _articlePcoNumRecetteFk = createInvForeignKey(pcoNum, "PCO_NUM_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFacturePcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPrestationLigne> _prligPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QTitre> _titrePcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlancoTva> _rpcotvaPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPrestationBudgetClient> _pbcPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPlancoCredit> _plancoCreditPcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QTypeRetenue> _typeRetenuePcoNumFk = createInvForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QArticlePrestation> _articlePcoNumDepenseFk = createInvForeignKey(pcoNum, "PCO_NUM_DEPENSE");

    public QPlanComptable(String variable) {
        super(QPlanComptable.class, forVariable(variable), "GFC", "PLAN_COMPTABLE");
        addMetadata();
    }

    public QPlanComptable(String variable, String schema, String table) {
        super(QPlanComptable.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPlanComptable(Path<? extends QPlanComptable> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PLAN_COMPTABLE");
        addMetadata();
    }

    public QPlanComptable(PathMetadata<?> metadata) {
        super(QPlanComptable.class, metadata, "GFC", "PLAN_COMPTABLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pcoBudgetaire, ColumnMetadata.named("PCO_BUDGETAIRE").withIndex(3).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(pcoCompteBe, ColumnMetadata.named("PCO_COMPTE_BE").withIndex(12).ofType(Types.VARCHAR).withSize(20));
        addMetadata(pcoEmargement, ColumnMetadata.named("PCO_EMARGEMENT").withIndex(4).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(pcoJBe, ColumnMetadata.named("PCO_J_BE").withIndex(11).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoJExercice, ColumnMetadata.named("PCO_J_EXERCICE").withIndex(9).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoJFinExercice, ColumnMetadata.named("PCO_J_FIN_EXERCICE").withIndex(10).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoLibelle, ColumnMetadata.named("PCO_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(pcoNature, ColumnMetadata.named("PCO_NATURE").withIndex(5).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(pcoNiveau, ColumnMetadata.named("PCO_NIVEAU").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(1).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pcoSensEmargement, ColumnMetadata.named("PCO_SENS_EMARGEMENT").withIndex(7).ofType(Types.VARCHAR).withSize(1));
        addMetadata(pcoSensSolde, ColumnMetadata.named("PCO_SENS_SOLDE").withIndex(13).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(pcoValidite, ColumnMetadata.named("PCO_VALIDITE").withIndex(8).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

