package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVExtourneLiqSuivi is a Querydsl query type for QVExtourneLiqSuivi
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVExtourneLiqSuivi extends com.mysema.query.sql.RelationalPathBase<QVExtourneLiqSuivi> {

    private static final long serialVersionUID = 1104406877;

    public static final QVExtourneLiqSuivi vExtourneLiqSuivi = new QVExtourneLiqSuivi("V_EXTOURNE_LIQ_SUIVI");

    public final NumberPath<Long> depIdDef = createNumber("depIdDef", Long.class);

    public final NumberPath<Long> depIdExt = createNumber("depIdExt", Long.class);

    public final NumberPath<Long> dpcoIdDef = createNumber("dpcoIdDef", Long.class);

    public final NumberPath<Long> dpcoIdExt = createNumber("dpcoIdExt", Long.class);

    public QVExtourneLiqSuivi(String variable) {
        super(QVExtourneLiqSuivi.class, forVariable(variable), "GFC", "V_EXTOURNE_LIQ_SUIVI");
        addMetadata();
    }

    public QVExtourneLiqSuivi(String variable, String schema, String table) {
        super(QVExtourneLiqSuivi.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVExtourneLiqSuivi(Path<? extends QVExtourneLiqSuivi> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EXTOURNE_LIQ_SUIVI");
        addMetadata();
    }

    public QVExtourneLiqSuivi(PathMetadata<?> metadata) {
        super(QVExtourneLiqSuivi.class, metadata, "GFC", "V_EXTOURNE_LIQ_SUIVI");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depIdDef, ColumnMetadata.named("DEP_ID_DEF").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depIdExt, ColumnMetadata.named("DEP_ID_EXT").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoIdDef, ColumnMetadata.named("DPCO_ID_DEF").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoIdExt, ColumnMetadata.named("DPCO_ID_EXT").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

