package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAmandeExecHm is a Querydsl query type for QVAmandeExecHm
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAmandeExecHm extends com.mysema.query.sql.RelationalPathBase<QVAmandeExecHm> {

    private static final long serialVersionUID = -746695195;

    public static final QVAmandeExecHm vAmandeExecHm = new QVAmandeExecHm("V_AMANDE_EXEC_HM");

    public final StringPath code = createString("code");

    public final NumberPath<Long> depenseHt = createNumber("depenseHt", Long.class);

    public final NumberPath<Long> dispo = createNumber("dispo", Long.class);

    public final NumberPath<Long> engageHt = createNumber("engageHt", Long.class);

    public final NumberPath<Integer> exercice = createNumber("exercice", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<java.math.BigDecimal> seuil = createNumber("seuil", java.math.BigDecimal.class);

    public final NumberPath<Long> totalHt = createNumber("totalHt", Long.class);

    public QVAmandeExecHm(String variable) {
        super(QVAmandeExecHm.class, forVariable(variable), "GFC", "V_AMANDE_EXEC_HM");
        addMetadata();
    }

    public QVAmandeExecHm(String variable, String schema, String table) {
        super(QVAmandeExecHm.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAmandeExecHm(Path<? extends QVAmandeExecHm> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_AMANDE_EXEC_HM");
        addMetadata();
    }

    public QVAmandeExecHm(PathMetadata<?> metadata) {
        super(QVAmandeExecHm.class, metadata, "GFC", "V_AMANDE_EXEC_HM");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(depenseHt, ColumnMetadata.named("DEPENSE_HT").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dispo, ColumnMetadata.named("DISPO").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(engageHt, ColumnMetadata.named("ENGAGE_HT").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(5).ofType(Types.DECIMAL).withSize(4));
        addMetadata(id, ColumnMetadata.named("ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(150));
        addMetadata(seuil, ColumnMetadata.named("SEUIL").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(totalHt, ColumnMetadata.named("TOTAL_HT").withIndex(8).ofType(Types.DECIMAL).withSize(0));
    }

}

