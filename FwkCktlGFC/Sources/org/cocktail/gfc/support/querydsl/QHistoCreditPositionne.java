package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QHistoCreditPositionne is a Querydsl query type for QHistoCreditPositionne
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QHistoCreditPositionne extends com.mysema.query.sql.RelationalPathBase<QHistoCreditPositionne> {

    private static final long serialVersionUID = 354049477;

    public static final QHistoCreditPositionne histoCreditPositionne = new QHistoCreditPositionne("HISTO_CREDIT_POSITIONNE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> hcpDate = createDateTime("hcpDate", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> hcpMontant = createNumber("hcpMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> hcpOrdre = createNumber("hcpOrdre", Long.class);

    public final StringPath hcpSuppr = createString("hcpSuppr");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> traOrdre = createNumber("traOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QHistoCreditPositionne> histoCreditPositionnePk = createPrimaryKey(hcpOrdre);

    public QHistoCreditPositionne(String variable) {
        super(QHistoCreditPositionne.class, forVariable(variable), "GFC", "HISTO_CREDIT_POSITIONNE");
        addMetadata();
    }

    public QHistoCreditPositionne(String variable, String schema, String table) {
        super(QHistoCreditPositionne.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QHistoCreditPositionne(Path<? extends QHistoCreditPositionne> path) {
        super(path.getType(), path.getMetadata(), "GFC", "HISTO_CREDIT_POSITIONNE");
        addMetadata();
    }

    public QHistoCreditPositionne(PathMetadata<?> metadata) {
        super(QHistoCreditPositionne.class, metadata, "GFC", "HISTO_CREDIT_POSITIONNE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(hcpDate, ColumnMetadata.named("HCP_DATE").withIndex(9).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(hcpMontant, ColumnMetadata.named("HCP_MONTANT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(hcpOrdre, ColumnMetadata.named("HCP_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(hcpSuppr, ColumnMetadata.named("HCP_SUPPR").withIndex(10).ofType(Types.VARCHAR).withSize(1));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(10));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
    }

}

