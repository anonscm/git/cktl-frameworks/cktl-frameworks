package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviDepensePropo is a Querydsl query type for QVSuiviDepensePropo
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviDepensePropo extends com.mysema.query.sql.RelationalPathBase<QVSuiviDepensePropo> {

    private static final long serialVersionUID = -914272377;

    public static final QVSuiviDepensePropo vSuiviDepensePropo = new QVSuiviDepensePropo("V_SUIVI_DEPENSE_PROPO");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> totalPropo = createSimple("totalPropo", Object.class);

    public QVSuiviDepensePropo(String variable) {
        super(QVSuiviDepensePropo.class, forVariable(variable), "GFC", "V_SUIVI_DEPENSE_PROPO");
        addMetadata();
    }

    public QVSuiviDepensePropo(String variable, String schema, String table) {
        super(QVSuiviDepensePropo.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviDepensePropo(Path<? extends QVSuiviDepensePropo> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_DEPENSE_PROPO");
        addMetadata();
    }

    public QVSuiviDepensePropo(PathMetadata<?> metadata) {
        super(QVSuiviDepensePropo.class, metadata, "GFC", "V_SUIVI_DEPENSE_PROPO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPropo, ColumnMetadata.named("TOTAL_PROPO").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

