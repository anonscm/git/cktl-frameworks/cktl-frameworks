package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVSortieMontantAnnuel is a Querydsl query type for QImmVSortieMontantAnnuel
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVSortieMontantAnnuel extends com.mysema.query.sql.RelationalPathBase<QImmVSortieMontantAnnuel> {

    private static final long serialVersionUID = -982456558;

    public static final QImmVSortieMontantAnnuel immVSortieMontantAnnuel = new QImmVSortieMontantAnnuel("IMM_V_SORTIE_MONTANT_ANNUEL");

    public final StringPath anneeSortie = createString("anneeSortie");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> montant = createNumber("montant", Long.class);

    public QImmVSortieMontantAnnuel(String variable) {
        super(QImmVSortieMontantAnnuel.class, forVariable(variable), "GFC", "IMM_V_SORTIE_MONTANT_ANNUEL");
        addMetadata();
    }

    public QImmVSortieMontantAnnuel(String variable, String schema, String table) {
        super(QImmVSortieMontantAnnuel.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVSortieMontantAnnuel(Path<? extends QImmVSortieMontantAnnuel> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_SORTIE_MONTANT_ANNUEL");
        addMetadata();
    }

    public QImmVSortieMontantAnnuel(PathMetadata<?> metadata) {
        super(QImmVSortieMontantAnnuel.class, metadata, "GFC", "IMM_V_SORTIE_MONTANT_ANNUEL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(anneeSortie, ColumnMetadata.named("ANNEE_SORTIE").withIndex(2).ofType(Types.VARCHAR).withSize(4));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

