package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmTypeCreditRec is a Querydsl query type for QVAdmTypeCreditRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmTypeCreditRec extends com.mysema.query.sql.RelationalPathBase<QVAdmTypeCreditRec> {

    private static final long serialVersionUID = -1850647064;

    public static final QVAdmTypeCreditRec vAdmTypeCreditRec = new QVAdmTypeCreditRec("V_ADM_TYPE_CREDIT_REC");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath tcdAbrege = createString("tcdAbrege");

    public final StringPath tcdBudget = createString("tcdBudget");

    public final StringPath tcdCode = createString("tcdCode");

    public final StringPath tcdLibelle = createString("tcdLibelle");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> tcdPresident = createNumber("tcdPresident", java.math.BigDecimal.class);

    public final StringPath tcdSect = createString("tcdSect");

    public final StringPath tcdType = createString("tcdType");

    public QVAdmTypeCreditRec(String variable) {
        super(QVAdmTypeCreditRec.class, forVariable(variable), "GFC", "V_ADM_TYPE_CREDIT_REC");
        addMetadata();
    }

    public QVAdmTypeCreditRec(String variable, String schema, String table) {
        super(QVAdmTypeCreditRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmTypeCreditRec(Path<? extends QVAdmTypeCreditRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_TYPE_CREDIT_REC");
        addMetadata();
    }

    public QVAdmTypeCreditRec(PathMetadata<?> metadata) {
        super(QVAdmTypeCreditRec.class, metadata, "GFC", "V_ADM_TYPE_CREDIT_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(tcdAbrege, ColumnMetadata.named("TCD_ABREGE").withIndex(5).ofType(Types.VARCHAR).withSize(12).notNull());
        addMetadata(tcdBudget, ColumnMetadata.named("TCD_BUDGET").withIndex(9).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tcdCode, ColumnMetadata.named("TCD_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(tcdLibelle, ColumnMetadata.named("TCD_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(40).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdPresident, ColumnMetadata.named("TCD_PRESIDENT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(tcdSect, ColumnMetadata.named("TCD_SECT").withIndex(6).ofType(Types.CHAR).withSize(1).notNull());
        addMetadata(tcdType, ColumnMetadata.named("TCD_TYPE").withIndex(8).ofType(Types.VARCHAR).withSize(20));
    }

}

