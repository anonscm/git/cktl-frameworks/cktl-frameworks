package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVOrganAll is a Querydsl query type for QVOrganAll
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVOrganAll extends com.mysema.query.sql.RelationalPathBase<QVOrganAll> {

    private static final long serialVersionUID = -12216187;

    public static final QVOrganAll vOrganAll = new QVOrganAll("V_ORGAN_ALL");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> orgCr = createSimple("orgCr", Object.class);

    public final SimplePath<Object> orgEtab = createSimple("orgEtab", Object.class);

    public final SimplePath<Object> orgLib = createSimple("orgLib", Object.class);

    public final SimplePath<Object> orgLucrativite = createSimple("orgLucrativite", Object.class);

    public final SimplePath<Object> orgNiv = createSimple("orgNiv", Object.class);

    public final SimplePath<Object> orgPere = createSimple("orgPere", Object.class);

    public final SimplePath<Object> orgSouscr = createSimple("orgSouscr", Object.class);

    public final SimplePath<Object> orgUb = createSimple("orgUb", Object.class);

    public final SimplePath<Object> orgUniv = createSimple("orgUniv", Object.class);

    public QVOrganAll(String variable) {
        super(QVOrganAll.class, forVariable(variable), "GFC", "V_ORGAN_ALL");
        addMetadata();
    }

    public QVOrganAll(String variable, String schema, String table) {
        super(QVOrganAll.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVOrganAll(Path<? extends QVOrganAll> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ORGAN_ALL");
        addMetadata();
    }

    public QVOrganAll(PathMetadata<?> metadata) {
        super(QVOrganAll.class, metadata, "GFC", "V_ORGAN_ALL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(orgCr, ColumnMetadata.named("ORG_CR").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(orgEtab, ColumnMetadata.named("ORG_ETAB").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(orgLib, ColumnMetadata.named("ORG_LIB").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(orgLucrativite, ColumnMetadata.named("ORG_LUCRATIVITE").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(orgNiv, ColumnMetadata.named("ORG_NIV").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(orgPere, ColumnMetadata.named("ORG_PERE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(orgSouscr, ColumnMetadata.named("ORG_SOUSCR").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(orgUniv, ColumnMetadata.named("ORG_UNIV").withIndex(5).ofType(Types.OTHER).withSize(0));
    }

}

