package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QFacture is a Querydsl query type for QFacture
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QFacture extends com.mysema.query.sql.RelationalPathBase<QFacture> {

    private static final long serialVersionUID = 1716314971;

    public static final QFacture facture = new QFacture("FACTURE");

    public final NumberPath<Long> echeId = createNumber("echeId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> facDateSaisie = createDateTime("facDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> facHtSaisie = createNumber("facHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> facId = createNumber("facId", Long.class);

    public final StringPath facLib = createString("facLib");

    public final NumberPath<java.math.BigDecimal> facMontantBudgetaire = createNumber("facMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> facMontantBudgetaireReste = createNumber("facMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<Long> facNumero = createNumber("facNumero", Long.class);

    public final NumberPath<java.math.BigDecimal> facTtcSaisie = createNumber("facTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> facTvaSaisie = createNumber("facTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> morOrdre = createNumber("morOrdre", Long.class);

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QFacture> facturePk = createPrimaryKey(facId);

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> factureUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEchEcheancier> factureEcheIdFk = createForeignKey(echeId, "ECH_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> factureTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QPersonne> facturePersIdFk = createForeignKey(persId, "PERS_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> factureExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> factureOrgIdFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QModeRecouvrement> factureMorOrdreFk = createForeignKey(morOrdre, "MOD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplication> factureTyapIdFk = createForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> factureFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> factureTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> factureTcdOrdreFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecRecette> _recRecetteFacIdFk = createInvForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlConvention> _fconFacIdFk = createInvForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QPiEngFac> _pefFacIdFk = createInvForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlAnalytique> _fanaFacIdFk = createInvForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlAction> _factFacIdFk = createInvForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlPlanco> _fpcoFacIdFk = createInvForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapFacIdFk = createInvForeignKey(facId, "FAC_ID");

    public QFacture(String variable) {
        super(QFacture.class, forVariable(variable), "GFC", "FACTURE");
        addMetadata();
    }

    public QFacture(String variable, String schema, String table) {
        super(QFacture.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QFacture(Path<? extends QFacture> path) {
        super(path.getType(), path.getMetadata(), "GFC", "FACTURE");
        addMetadata();
    }

    public QFacture(PathMetadata<?> metadata) {
        super(QFacture.class, metadata, "GFC", "FACTURE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(echeId, ColumnMetadata.named("ECHE_ID").withIndex(17).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(facDateSaisie, ColumnMetadata.named("FAC_DATE_SAISIE").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(facHtSaisie, ColumnMetadata.named("FAC_HT_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(facId, ColumnMetadata.named("FAC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(facLib, ColumnMetadata.named("FAC_LIB").withIndex(7).ofType(Types.VARCHAR).withSize(500));
        addMetadata(facMontantBudgetaire, ColumnMetadata.named("FAC_MONTANT_BUDGETAIRE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(facMontantBudgetaireReste, ColumnMetadata.named("FAC_MONTANT_BUDGETAIRE_RESTE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(facNumero, ColumnMetadata.named("FAC_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(facTtcSaisie, ColumnMetadata.named("FAC_TTC_SAISIE").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(facTvaSaisie, ColumnMetadata.named("FAC_TVA_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(morOrdre, ColumnMetadata.named("MOR_ORDRE").withIndex(13).ofType(Types.DECIMAL).withSize(0));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(16).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(18).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(19).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(20).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

