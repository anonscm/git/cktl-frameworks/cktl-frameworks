package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRepartSbRecettePlanco is a Querydsl query type for QRepartSbRecettePlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRepartSbRecettePlanco extends com.mysema.query.sql.RelationalPathBase<QRepartSbRecettePlanco> {

    private static final long serialVersionUID = 195346287;

    public static final QRepartSbRecettePlanco repartSbRecettePlanco = new QRepartSbRecettePlanco("REPART_SB_RECETTE_PLANCO");

    public final NumberPath<java.math.BigDecimal> montantHt = createNumber("montantHt", java.math.BigDecimal.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> srOrdre = createNumber("srOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRepartSbRecettePlanco> repartSbRecettePlancoPk = createPrimaryKey(pcoNum, srOrdre);

    public QRepartSbRecettePlanco(String variable) {
        super(QRepartSbRecettePlanco.class, forVariable(variable), "GFC", "REPART_SB_RECETTE_PLANCO");
        addMetadata();
    }

    public QRepartSbRecettePlanco(String variable, String schema, String table) {
        super(QRepartSbRecettePlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRepartSbRecettePlanco(Path<? extends QRepartSbRecettePlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REPART_SB_RECETTE_PLANCO");
        addMetadata();
    }

    public QRepartSbRecettePlanco(PathMetadata<?> metadata) {
        super(QRepartSbRecettePlanco.class, metadata, "GFC", "REPART_SB_RECETTE_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(montantHt, ColumnMetadata.named("MONTANT_HT").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(srOrdre, ColumnMetadata.named("SR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

