package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVExecutionGlobale is a Querydsl query type for QVExecutionGlobale
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVExecutionGlobale extends com.mysema.query.sql.RelationalPathBase<QVExecutionGlobale> {

    private static final long serialVersionUID = -989528929;

    public static final QVExecutionGlobale vExecutionGlobale = new QVExecutionGlobale("V_EXECUTION_GLOBALE");

    public final NumberPath<Long> aeeEngHt = createNumber("aeeEngHt", Long.class);

    public final NumberPath<Long> aeeExecution = createNumber("aeeExecution", Long.class);

    public final NumberPath<Long> aeeLiqHt = createNumber("aeeLiqHt", Long.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public QVExecutionGlobale(String variable) {
        super(QVExecutionGlobale.class, forVariable(variable), "GFC", "V_EXECUTION_GLOBALE");
        addMetadata();
    }

    public QVExecutionGlobale(String variable, String schema, String table) {
        super(QVExecutionGlobale.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVExecutionGlobale(Path<? extends QVExecutionGlobale> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EXECUTION_GLOBALE");
        addMetadata();
    }

    public QVExecutionGlobale(PathMetadata<?> metadata) {
        super(QVExecutionGlobale.class, metadata, "GFC", "V_EXECUTION_GLOBALE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeeEngHt, ColumnMetadata.named("AEE_ENG_HT").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeExecution, ColumnMetadata.named("AEE_EXECUTION").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeLiqHt, ColumnMetadata.named("AEE_LIQ_HT").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

