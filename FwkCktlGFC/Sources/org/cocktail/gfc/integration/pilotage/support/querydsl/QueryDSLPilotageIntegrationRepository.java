/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.integration.pilotage.support.querydsl;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.integration.pilotage.support.PilotageIntegrationRepository;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepAe;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepCp;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraRec;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepAe;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepCp;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.types.query.ListSubQuery;
import com.mysema.query.types.query.NumberSubQuery;

public class QueryDSLPilotageIntegrationRepository implements PilotageIntegrationRepository {

	private final QBudBudget budget = QBudBudget.budBudget;
	private final QOpeTrancheBud tranche = QOpeTrancheBud.opeTrancheBud;
	private final QOpeTrancheBudDepAe aeTranche = QOpeTrancheBudDepAe.opeTrancheBudDepAe;
	private final QOpeTrancheBudDepCp cpTranche = QOpeTrancheBudDepCp.opeTrancheBudDepCp;
	private final QOpeTrancheBudRec trancheRec = QOpeTrancheBudRec.opeTrancheBudRec;

	private final QBudPrevOpeTraDepAe previsionDepAeOpeTra = QBudPrevOpeTraDepAe.budPrevOpeTraDepAe;
	private final QBudPrevOpeTraDepCp previsionDepCpOpeTra = QBudPrevOpeTraDepCp.budPrevOpeTraDepCp;
    private final QBudPrevOpeTraRec previsionRecOpeTra = QBudPrevOpeTraRec.budPrevOpeTraRec;
    
	private QueryDslJdbcTemplate template;
	
	public QueryDSLPilotageIntegrationRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }
	
	public boolean isTrancheIntegreeAuBudget(BudgetId budgetId, EntiteBudgetaireId ebId) {
	    Long budgetIdNumber = budgetId.getId();
        Long ebIdNumber = ebId.getId();
        return !existeTrancheRecNonIntegree(budgetIdNumber, ebIdNumber) && !existeTrancheDepNonIntegree(budgetIdNumber, ebIdNumber)
                && !existeTrancheDepCpNonIntegree(budgetIdNumber, ebIdNumber);
    }
	
	protected NumberSubQuery<Integer> exerciceBudgetQuery(Long budgetIdNumber) {
	    return new SQLSubQuery().from(budget).where(budget.idBudBudget.eq(budgetIdNumber)).unique(budget.exeOrdre);
	}
	
	protected boolean existeTrancheRecNonIntegree(Long budgetIdNumber, Long ebIdNumber) {
	    ListSubQuery<Long> tranchesRecsIntegreesQuery = new SQLSubQuery()
	        .from(previsionRecOpeTra)
	        .where(previsionRecOpeTra.idBudBudget.eq(budgetIdNumber)
	                .and(previsionRecOpeTra.idAdmEb.eq(ebIdNumber)
	                .and(previsionRecOpeTra.idOpeTrancheBudRec.eq(trancheRec.idOpeTrancheBudRec))))
	        .list(previsionRecOpeTra.idBudPrevOpeTraRec);
	    SQLQuery query = template.newSqlQuery();
	    long existsRecettesNonIntegrees = template.count(
                query.from(tranche)         
                 .join(trancheRec).on(tranche.idOpeTrancheBud.eq(trancheRec.idOpeTrancheBud))
                 .where(tranche.exeOrdre.eq(exerciceBudgetQuery(budgetIdNumber))
                     .and(tranche.tyetId.eq(TypeEtatService.Code.VALIDE.id())
                     .and(trancheRec.idAdmEb.eq(ebIdNumber)))
                     .and(tranchesRecsIntegreesQuery.notExists())
                 ));
	    return existsRecettesNonIntegrees > 0;
	}
	
	protected boolean existeTrancheDepNonIntegree(Long budgetIdNumber, Long ebIdNumber) {
	    ListSubQuery<Long> tranchesDepAeIntegreesQuery = new SQLSubQuery()
           .from(previsionDepAeOpeTra)
           .where(previsionDepAeOpeTra.idBudBudget.eq(budgetIdNumber)
                   .and(previsionDepAeOpeTra.idAdmEb.eq(ebIdNumber)
                   .and(previsionDepAeOpeTra.idOpeTrancheBudDepAe.eq(aeTranche.idOpeTrancheBudDepAe))))
           .list(previsionDepAeOpeTra.idBudPrevOpeTraDepAe);           
       SQLQuery query = template.newSqlQuery();
       long existsDepensesAeNonIntegrees = template.count(
               query.from(tranche)           
                .join(aeTranche).on(tranche.idOpeTrancheBud.eq(aeTranche.idOpeTrancheBud))
                .where(tranche.exeOrdre.eq(exerciceBudgetQuery(budgetIdNumber))
                    .and(tranche.tyetId.eq(TypeEtatService.Code.VALIDE.id())
                    .and(aeTranche.idAdmEb.eq(ebIdNumber)))
                    .and(tranchesDepAeIntegreesQuery.notExists())
                ));
       return existsDepensesAeNonIntegrees > 0;
	}
	
	protected boolean existeTrancheDepCpNonIntegree(Long budgetIdNumber, Long ebIdNumber) {
        ListSubQuery<Long> tranchesDepCpIntegreesQuery = new SQLSubQuery()
           .from(previsionDepCpOpeTra)
           .where(previsionDepCpOpeTra.idBudBudget.eq(budgetIdNumber)
                   .and(previsionDepCpOpeTra.idAdmEb.eq(ebIdNumber)
                   .and(previsionDepCpOpeTra.idOpeTrancheBudDepCp.eq(cpTranche.idOpeTrancheBudDepCp))))
           .list(previsionDepCpOpeTra.idBudPrevOpeTraDepCp);           
       SQLQuery query = template.newSqlQuery();
       long existsDepensesCpNonIntegrees = template.count(
               query.from(tranche)           
                .join(cpTranche).on(tranche.idOpeTrancheBud.eq(cpTranche.idOpeTrancheBud))
                .where(tranche.exeOrdre.eq(exerciceBudgetQuery(budgetIdNumber))
                    .and(tranche.tyetId.eq(TypeEtatService.Code.VALIDE.id())
                    .and(cpTranche.idAdmEb.eq(ebIdNumber)))
                    .and(tranchesDepCpIntegreesQuery.notExists())
                ));
       return existsDepensesCpNonIntegrees > 0;
    }
}
