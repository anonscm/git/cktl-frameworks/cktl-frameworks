/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.validation;

import java.util.Date;

import javax.annotation.Nullable;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Builder;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;

/**
 * @author bourges
 *
 */
@Getter
@Setter
public class BudgetRecette {
    @Nullable
    private BudgetRecetteId budgetRecetteId;
    private BudgetId budgetId;
    private EntiteBudgetaireId entiteBudgetaireId;
    private Montant montantRecette;
    private Montant montantTitre;
    private SectionRecette sectionRecette;
    private NatureRecetteId natureRecetteId;
    private OrigineRecetteId origineRecetteId;
    @Nullable
    private OperationId operationId;
    private Long persIdCreation;
    private Date dateCreation;
    @Nullable
    private Long persIdModification;
    @Nullable
    private Date dateModification;

    @Builder
    private static BudgetRecette init(
            BudgetRecetteId budgetRecetteId,
            @NonNull BudgetId budgetId,
            @NonNull EntiteBudgetaireId entiteBudgetaireId,
            @NonNull Montant montantRecette,
            @NonNull Montant montantTitre,
            @NonNull SectionRecette sectionRecette,
            @NonNull NatureRecetteId natureRecetteId,
            @NonNull OrigineRecetteId origineRecetteId,
            OperationId operationId,
            @NonNull Long persIdCreation,
            @NonNull Date dateCreation,
            Long persIdModification,
            Date dateModification
            ) {
        BudgetRecette ret = new BudgetRecette();
        ret.setBudgetRecetteId(budgetRecetteId);
        ret.setBudgetId(budgetId);
        ret.setEntiteBudgetaireId(entiteBudgetaireId);
        ret.setMontantRecette(montantRecette);
        ret.setMontantTitre(montantTitre);
        ret.setSectionRecette(sectionRecette);
        ret.setNatureRecetteId(natureRecetteId);
        ret.setOrigineRecetteId(origineRecetteId);
        ret.setOperationId(operationId);
        ret.setPersIdCreation(persIdCreation);
        ret.setDateCreation(dateCreation);
        ret.setPersIdModification(persIdModification);
        ret.setDateModification(dateModification);
        return ret;
    }
}

