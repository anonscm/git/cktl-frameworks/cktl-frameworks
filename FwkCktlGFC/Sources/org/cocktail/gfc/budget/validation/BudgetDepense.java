/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.validation;

import java.util.Date;

import javax.annotation.Nullable;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Builder;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;

/**
 * @author bourges
 *
 */
@Getter
@Setter
public class BudgetDepense {
    @Nullable 
    private BudgetDepenseId budgetDepenseId;
    private BudgetId budgetId;
    private EntiteBudgetaireId entiteBudgetaireId;
    private Montant montantAE;
    private Montant montantCP;
    private EnveloppeBudgetaireId enveloppeBudgetaireId;
    private NatureDepenseId natureDepenseId;
    private DestinationDepenseId destinationDepenseId;
    @Nullable
    private OperationId operationId;
    private Long persIdCreation;
    private Date dateCreation;
    @Nullable
    private Long persIdModification;
    @Nullable
    private Date dateModification;
        
    @Builder
    private static BudgetDepense init(
            BudgetDepenseId budgetDepenseId,
            @NonNull BudgetId budgetId,
            @NonNull EntiteBudgetaireId entiteBudgetaireId,
            @NonNull Montant montantAE,
            @NonNull Montant montantCP,
            @NonNull EnveloppeBudgetaireId enveloppeBudgetaireId,
            @NonNull NatureDepenseId natureDepenseId,
            @NonNull DestinationDepenseId destinationDepenseId,
            OperationId operationId,
            @NonNull Long persIdCreation,
            @NonNull Date dateCreation,
            Long persIdModification,
            Date dateModification
            ) {
        BudgetDepense ret = new BudgetDepense();
        ret.setBudgetDepenseId(budgetDepenseId);
        ret.setBudgetId(budgetId);
        ret.setEntiteBudgetaireId(entiteBudgetaireId);
        ret.setMontantAE(montantAE);
        ret.setMontantCP(montantCP);
        ret.setEnveloppeBudgetaireId(enveloppeBudgetaireId);
        ret.setNatureDepenseId(natureDepenseId);
        ret.setDestinationDepenseId(destinationDepenseId);
        ret.setOperationId(operationId);
        ret.setPersIdCreation(persIdCreation);
        ret.setDateCreation(dateCreation);
        ret.setPersIdModification(persIdModification);
        ret.setDateModification(dateModification);
        return ret;
    }
}
