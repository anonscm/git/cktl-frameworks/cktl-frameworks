/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.validation.support.querydsl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.validation.BudgetDepense;
import org.cocktail.gfc.budget.validation.BudgetDepenseId;
import org.cocktail.gfc.budget.validation.support.BudgetDepenseRepository;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudBudgetDep;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpDep;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepAe;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepCp;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlDeleteCallback;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.dml.SQLDeleteClause;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Path;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.path.SimplePath;

/**
 * @author bourges
 *
 */
public class QueryDSLBudgetDepenseRepository extends GfcRepository implements BudgetDepenseRepository {
    
    private QueryDslJdbcTemplate template;
    
    private final QBudBudgetDep budgetDep = QBudBudgetDep.budBudgetDep;
    private final SimplePath<BigDecimal> montantAeSum = Expressions.path(BigDecimal.class, "montantAeSum");
    private final SimplePath<BigDecimal> montantCpSum = Expressions.path(BigDecimal.class, "montantCpSum");
    
    private Path<?>[] columnsForInsert;
    
    public QueryDSLBudgetDepenseRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
        @SuppressWarnings("unchecked")
        List<Path<?>> cols = new ArrayList<Path<?>>(Arrays.asList(
                budgetDep.idBudBudgetDep,
                budgetDep.idBudBudget,
                budgetDep.idAdmEb,
                budgetDep.montantAe,
                budgetDep.montantCp,
                budgetDep.idBudEnveloppe,
                budgetDep.idAdmNatureDep,
                budgetDep.idAdmDestinationDepense,
                budgetDep.idOpeOperation,
                budgetDep.persIdCreation,
                budgetDep.dCreation,
                budgetDep.persIdModification,
                budgetDep.dModification
                ));
        columnsForInsert = cols.toArray(new Path[cols.size()]);
    }

    public List<BudgetDepense> findByBudget(BudgetId budgetId) {
        QBudBudgetDep t = QBudBudgetDep.budBudgetDep;
        List<BudgetDepense> ret = new ArrayList<BudgetDepense>();
        SQLQuery query = template.newSqlQuery();
        query.from(t)
            .where(t.idBudBudget.eq(budgetId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                t.idBudBudgetDep, t.idBudBudget, t.idAdmEb, t.montantAe, t.montantCp,
                t.idBudEnveloppe, t.idAdmNatureDep, t.idAdmDestinationDepense, t.idOpeOperation,
                t.persIdCreation, t.dCreation, t.persIdModification, t.dModification));
        for (Tuple row : result) {
            Long longOperationId = row.get(t.idOpeOperation);
            OperationId operationId = null;
            if (longOperationId != null) {
                operationId = new OperationId(longOperationId);
            }
            BudgetDepense budgetDepense = BudgetDepense.builder()
                    .budgetDepenseId(new BudgetDepenseId(row.get(t.idBudBudgetDep)))
                    .budgetId(new BudgetId(row.get(t.idBudBudget)))
                    .entiteBudgetaireId(new EntiteBudgetaireId(row.get(t.idAdmEb)))
                    .montantAE(new Montant(row.get(t.montantAe)))
                    .montantCP(new Montant(row.get(t.montantCp)))
                    .enveloppeBudgetaireId(new EnveloppeBudgetaireId(row.get(t.idBudEnveloppe)))
                    .natureDepenseId(new NatureDepenseId(row.get(t.idAdmNatureDep)))
                    .destinationDepenseId(new DestinationDepenseId(row.get(t.idAdmDestinationDepense)))
                    .operationId(operationId)
                    .persIdCreation(row.get(t.persIdCreation))
                    .dateCreation(row.get(t.dCreation))
                    .persIdModification(row.get(t.persIdModification))
                    .dateModification(row.get(t.dModification))
                    .build();
            ret.add(budgetDepense);
        }
        return ret;
    }

    public BudgetDepense createBudgetDepense(BudgetDepense budgetDepense) {
        BudgetDepense ret = budgetDepense;
        Long id = template.insertWithKey(budgetDep, createBudgetDepenseCallback(budgetDepense));
        ret.setBudgetDepenseId(new BudgetDepenseId(id));
        return ret;
    }

    public long removeByBudget(BudgetId budgetId) {
        return template.delete(budgetDep, removeByBudgetCallback(budgetId));
    }
    
    public void createBudgetDepenseOnValidationBudget(BudgetId budgetId, Long persID) {
        createBudgetDepenseFromOperationsIntegrees(budgetId, persID);
        createBudgetDepenseFromPrevisions(budgetId, persID);
    }

    private void createBudgetDepenseFromPrevisions(BudgetId budgetId, Long persID) {
        QBudPrevHorsOpDep table = QBudPrevHorsOpDep.budPrevHorsOpDep;
        SQLQuery query = template.newSqlQuery();
        query.from(table)
            .groupBy(table.idBudBudget, table.idAdmEb, table.idBudEnveloppe,
                    table.idAdmNatureDep, table.idAdmDestinationDepense)
            .where(table.idBudBudget.eq(budgetId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                table.montantAe.sum().as(montantAeSum),
                table.montantCp.sum().as(montantCpSum),
                table.idAdmEb,
                table.idBudEnveloppe,
                table.idAdmNatureDep,
                table.idAdmDestinationDepense));
        for (Tuple row : result) {
            BudgetDepense budgetDepense = BudgetDepense.builder().budgetId(budgetId)
                .entiteBudgetaireId(new EntiteBudgetaireId(row.get(table.idAdmEb)))
                .montantAE(new Montant(row.get(montantAeSum)))
                .montantCP(new Montant(row.get(montantCpSum)))
                .enveloppeBudgetaireId(new EnveloppeBudgetaireId(row.get(table.idBudEnveloppe)))
                .natureDepenseId(new NatureDepenseId(row.get(table.idAdmNatureDep)))
                .destinationDepenseId(new DestinationDepenseId(row.get(table.idAdmDestinationDepense)))
                .persIdCreation(persID)
                .dateCreation(new Date())
                .build();
            createBudgetDepense(budgetDepense);
        }        
    }

    private void createBudgetDepenseFromOperationsIntegrees(BudgetId budgetId, Long persID) {
        QBudPrevOpeTraDepAe previsionAeOpe = QBudPrevOpeTraDepAe.budPrevOpeTraDepAe;
        QBudPrevOpeTraDepCp previsionCpOpe = QBudPrevOpeTraDepCp.budPrevOpeTraDepCp;
        
        SQLQuery query = template.newSqlQuery();
        query.from(previsionAeOpe)
                .fullJoin(previsionCpOpe).on(previsionAeOpe.idBudBudget.eq(previsionCpOpe.idBudBudget),
                        previsionAeOpe.idAdmEb.eq(previsionCpOpe.idAdmEb),
                        previsionAeOpe.idBudEnveloppe.eq(previsionCpOpe.idBudEnveloppe),
                        previsionAeOpe.idAdmNatureDep.eq(previsionCpOpe.idAdmNatureDep),
                        previsionAeOpe.idAdmDestinationDepense.eq(previsionCpOpe.idAdmDestinationDepense),
                        previsionAeOpe.idOpeOperation.eq(previsionCpOpe.idOpeOperation))
                .groupBy(previsionAeOpe.idBudBudget, previsionAeOpe.idAdmEb, previsionAeOpe.idBudEnveloppe,
                        previsionAeOpe.idAdmNatureDep, previsionAeOpe.idAdmDestinationDepense, previsionAeOpe.idOpeOperation)
                .where(previsionAeOpe.idBudBudget.eq(budgetId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                previsionAeOpe.montantAe.sum().coalesce(BigDecimal.ZERO).as(montantAeSum),
                previsionCpOpe.montantCp.sum().coalesce(BigDecimal.ZERO).as(montantCpSum),
                previsionAeOpe.idAdmEb,
                previsionAeOpe.idBudEnveloppe,
                previsionAeOpe.idAdmNatureDep,
                previsionAeOpe.idAdmDestinationDepense,
                previsionAeOpe.idOpeOperation));
        for (Tuple row : result) {
            BudgetDepense budgetDepense = BudgetDepense.builder().budgetId(budgetId)
                .entiteBudgetaireId(new EntiteBudgetaireId(row.get(previsionAeOpe.idAdmEb)))
                .montantAE(new Montant(row.get(montantAeSum)))
                .montantCP(new Montant(row.get(montantCpSum)))
                .enveloppeBudgetaireId(new EnveloppeBudgetaireId(row.get(previsionAeOpe.idBudEnveloppe)))
                .natureDepenseId(new NatureDepenseId(row.get(previsionAeOpe.idAdmNatureDep)))
                .destinationDepenseId(new DestinationDepenseId(row.get(previsionAeOpe.idAdmDestinationDepense)))
                .operationId(new OperationId(row.get(previsionAeOpe.idOpeOperation)))
                .persIdCreation(persID)
                .dateCreation(new Date())
                .build();
            createBudgetDepense(budgetDepense);
        }
    }

    private SqlDeleteCallback removeByBudgetCallback(final BudgetId budgetId) {
        return new SqlDeleteCallback() {
            
            public long doInSqlDeleteClause(SQLDeleteClause delete) {
                return delete.where(budgetDep.idBudBudget.eq(budgetId.getId())).execute();
            }
        };
    }

    private SqlInsertWithKeyCallback<Long> createBudgetDepenseCallback(final BudgetDepense budgetDepense) {
        return new SqlInsertWithKeyCallback<Long>() {

            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(budgetDep));
                values.addAll(budgetDepenseAsList(budgetDepense));
                return insert.columns(columnsForInsert).values(values.toArray()).executeWithKey(budgetDep.idBudBudgetDep);
            }
        };
    }

    private List<Object> budgetDepenseAsList(BudgetDepense budgetDepense) {
        List<Object> ret = new ArrayList<Object>();
        ret.add(budgetDepense.getBudgetId().getId());
        ret.add(budgetDepense.getEntiteBudgetaireId().getId());
        ret.add(budgetDepense.getMontantAE().value());
        ret.add(budgetDepense.getMontantCP().value());
        ret.add(budgetDepense.getEnveloppeBudgetaireId().getId());        
        ret.add(budgetDepense.getNatureDepenseId().getId());
        ret.add(budgetDepense.getDestinationDepenseId().getId());
        OperationId operationId = budgetDepense.getOperationId();
        if (operationId != null) {
            ret.add(operationId.getId());
        } else {
            ret.add(null);
        }
        ret.add(budgetDepense.getPersIdCreation());
        ret.add(budgetDepense.getDateCreation());
        ret.add(budgetDepense.getPersIdModification());
        ret.add(budgetDepense.getDateModification());
        return ret;
    }
}
