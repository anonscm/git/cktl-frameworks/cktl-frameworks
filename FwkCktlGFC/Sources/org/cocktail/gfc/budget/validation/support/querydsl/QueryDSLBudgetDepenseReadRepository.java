/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.validation.support.querydsl;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaire;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.read.PrevisionDepenseRead;
import org.cocktail.gfc.budget.validation.support.BudgetDepenseReadRepository;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudBudgetDep;
import org.cocktail.gfc.support.querydsl.QBudEnveloppe;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.path.NumberPath;

public class QueryDSLBudgetDepenseReadRepository implements BudgetDepenseReadRepository {

    // Tables
    private final QBudBudgetDep budgetDep = QBudBudgetDep.budBudgetDep;
    private final QAdmEb eb = QAdmEb.admEb;
    private final QBudEnveloppe enveloppe = QBudEnveloppe.budEnveloppe;
    private final QAdmNatureDep natureDepense = QAdmNatureDep.admNatureDep;
    private final QAdmDestinationDepense destinationDepense = QAdmDestinationDepense.admDestinationDepense;
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = QAdmEbExer.admEbExer;

    private QueryDslJdbcTemplate template;

    public QueryDSLBudgetDepenseReadRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
    }

    private Integer estSaisissable(Integer saisissableAsNumber) {
        if (saisissableAsNumber == null) {
            return 0;
        }
        return saisissableAsNumber;
    }

    public List<PrevisionDepenseRead> findByBudget(BudgetId budgetId) {
        SQLQuery sqlQuery = getBudgetDepenseQuery().where(budgetDep.idBudBudget.eq(budgetId.getId())).orderBy(eb.orgUniv.asc(), eb.orgEtab.asc(),
                eb.orgUb.asc(), eb.orgCr.asc(), eb.orgSouscr.asc());
        QTuple tuple = new QTuple(budgetDep.idBudBudgetDep, budgetDep.montantAe, budgetDep.montantCp, eb.idAdmEb, eb.orgUniv, eb.orgEtab, eb.orgUb,
                eb.orgCr, eb.orgSouscr, eb.orgLib, ebExer.saisieBudgetaire, enveloppe.idBudEnveloppe, enveloppe.codeEnveloppe, enveloppe.llEnveloppe,
                natureDepense.idAdmNatureDep, natureDepense.code, natureDepense.libelle, natureDepense.fongible,
                destinationDepense.idAdmDestinationDepense, destinationDepense.code, destinationDepense.abreviation, destinationDepense.libelle,
                operation.idOpeOperation, operation.llOperation, operation.estFlechee, typeOperation.idOpeTypeOperation,
                typeOperation.codeTypeOperation, typeOperation.llTypeOperation);
        List<Tuple> queryResult = template.query(sqlQuery, tuple);

        List<PrevisionDepenseRead> ret = new ArrayList<PrevisionDepenseRead>();
        for (Tuple row : queryResult) {
            EntiteBudgetaire ebRet = new EntiteBudgetaire(row.get(eb.idAdmEb), row.get(eb.orgUniv), row.get(eb.orgEtab), row.get(eb.orgUb),
                    row.get(eb.orgCr), row.get(eb.orgSouscr), row.get(eb.orgLib), estSaisissable(row.get(ebExer.saisieBudgetaire)));
            EnveloppeBudgetaire enveloppeRet = new EnveloppeBudgetaire(row.get(enveloppe.idBudEnveloppe), row.get(enveloppe.codeEnveloppe),
                    row.get(enveloppe.llEnveloppe));
            NatureDepense natureDepenseRet = new NatureDepense(row.get(natureDepense.idAdmNatureDep), row.get(natureDepense.code),
                    row.get(natureDepense.libelle), row.get(natureDepense.fongible));
            DestinationDepense destinationDepenseRet = new DestinationDepense(row.get(destinationDepense.idAdmDestinationDepense),
                    row.get(destinationDepense.code), row.get(destinationDepense.abreviation), row.get(destinationDepense.libelle));
            PrevisionDepenseRead previsionDepenseRet;
            if (row.get(operation.idOpeOperation) != null) {
                TypeOperation typeOperationRet = new TypeOperation(row.get(typeOperation.idOpeTypeOperation),
                        row.get(typeOperation.codeTypeOperation), row.get(typeOperation.llTypeOperation));
                OperationId operationId = new OperationId(row.get(operation.idOpeOperation));
                boolean estFlechee = (row.get(operation.estFlechee) == 1);
                Operation operationRet = new Operation(operationId, row.get(operation.llOperation), estFlechee, typeOperationRet);
                previsionDepenseRet = PrevisionDepenseRead.builder().previsionId(row.get(budgetDep.idBudBudgetDep)).eb(ebRet).enveloppe(enveloppeRet)
                        .natureDepense(natureDepenseRet).destinationDepense(destinationDepenseRet).operation(operationRet)
                        .montantAE(row.get(budgetDep.montantAe)).montantCP(row.get(budgetDep.montantCp)).build();
            } else {
                previsionDepenseRet = PrevisionDepenseRead.builder().previsionId(row.get(budgetDep.idBudBudgetDep)).eb(ebRet).enveloppe(enveloppeRet)
                        .natureDepense(natureDepenseRet).destinationDepense(destinationDepenseRet).montantAE(row.get(budgetDep.montantAe))
                        .montantCP(row.get(budgetDep.montantCp)).build();
            }
            ret.add(previsionDepenseRet);
        }
        return ret;
    }

    private SQLQuery getBudgetDepenseQuery() {
        SQLQuery sqlQuery = template
                .newSqlQuery()
                .from(budgetDep)
                .join(eb)
                .on(budgetDep.idAdmEb.eq(eb.idAdmEb))
                .join(enveloppe)
                .on(budgetDep.idBudEnveloppe.eq(enveloppe.idBudEnveloppe))
                .join(natureDepense)
                .on(budgetDep.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense)
                .on(budgetDep.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                // comme on veut l'info saisissable sur les EB alors il faut
                // faire une jointure avec le budget pour savoir sur quelle
                // année on est et chercher l'info saisissable sur l'EB pour
                // cette année là :
                .join(budget).on(budgetDep.idBudBudget.eq(budget.idBudBudget)).leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb))

                .leftJoin(operation).on(budgetDep.idOpeOperation.eq(operation.idOpeOperation)).leftJoin(typeOperation)
                .on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation));
        return sqlQuery;
    }
}
