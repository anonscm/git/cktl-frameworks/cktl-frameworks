/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.validation.support.querydsl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.validation.BudgetRecette;
import org.cocktail.gfc.budget.validation.BudgetRecetteId;
import org.cocktail.gfc.budget.validation.support.BudgetRecetteRepository;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudBudgetRec;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpRec;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlDeleteCallback;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.dml.SQLDeleteClause;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Path;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.path.SimplePath;

/**
 * @author bourges
 * 
 */
public class QueryDSLBudgetRecetteRepository extends GfcRepository implements BudgetRecetteRepository {

    private QueryDslJdbcTemplate template;

    private QBudBudgetRec budgetRec = QBudBudgetRec.budBudgetRec;

    private Path<?>[] columnsForInsert;

    private final SimplePath<BigDecimal> montantRecetteSum = Expressions.path(BigDecimal.class, "montantRecetteSum");
    private final SimplePath<BigDecimal> montantTitreSum = Expressions.path(BigDecimal.class, "montantTitreSum");

    public QueryDSLBudgetRecetteRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
        @SuppressWarnings("unchecked")
        List<Path<?>> cols = new ArrayList<Path<?>>(Arrays.asList(
                budgetRec.idBudBudgetRec,
                budgetRec.idBudBudget,
                budgetRec.idAdmEb,
                budgetRec.montantRec,
                budgetRec.montantTitre,
                budgetRec.section,
                budgetRec.idAdmNatureRec,
                budgetRec.idAdmOrigineRecette,
                budgetRec.idOpeOperation,
                budgetRec.persIdCreation,
                budgetRec.dCreation,
                budgetRec.persIdModification,
                budgetRec.dModification
                ));
        columnsForInsert = cols.toArray(new Path[cols.size()]);
    }

    public List<BudgetRecette> findByBudget(BudgetId budgetId) {
        QBudBudgetRec t = QBudBudgetRec.budBudgetRec;
        List<BudgetRecette> ret = new ArrayList<BudgetRecette>();
        SQLQuery query = template.newSqlQuery();
        query.from(t)
                .where(t.idBudBudget.eq(budgetId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                t.idBudBudgetRec, t.idBudBudget, t.idAdmEb, t.montantRec, t.montantTitre,
                t.section, t.idAdmNatureRec, t.idAdmOrigineRecette, t.idOpeOperation,
                t.persIdCreation, t.dCreation, t.persIdModification, t.dModification));
        for (Tuple row : result) {
            Long longOperationId = row.get(t.idOpeOperation);
            OperationId operationId = null;
            if (longOperationId != null) {
                operationId = new OperationId(longOperationId);
            }
            BudgetRecette budgetRecette = BudgetRecette.builder()
                    .budgetRecetteId(new BudgetRecetteId(row.get(t.idBudBudgetRec)))
                    .budgetId(new BudgetId(row.get(t.idBudBudget)))
                    .entiteBudgetaireId(new EntiteBudgetaireId(row.get(t.idAdmEb)))
                    .montantRecette(new Montant(row.get(t.montantRec)))
                    .montantTitre(new Montant(row.get(t.montantTitre)))
                    .sectionRecette(SectionRecetteCacheService.findByAbreviation(row.get(t.section)))
                    .natureRecetteId(new NatureRecetteId(row.get(t.idAdmNatureRec)))
                    .origineRecetteId(new OrigineRecetteId(row.get(t.idAdmOrigineRecette)))
                    .operationId(operationId)
                    .persIdCreation(row.get(t.persIdCreation))
                    .dateCreation(row.get(t.dCreation))
                    .persIdModification(row.get(t.persIdModification))
                    .dateModification(row.get(t.dModification))
                    .build();
            ret.add(budgetRecette);
        }
        return ret;
    }

    public BudgetRecette createBudgetRecette(BudgetRecette budgetRecette) {
        BudgetRecette ret = budgetRecette;
        Long id = template.insertWithKey(budgetRec, createBudgetRecetteCallback(budgetRecette));
        ret.setBudgetRecetteId(new BudgetRecetteId(id));
        return ret;
    }

    public long removeByBudget(BudgetId budgetId) {
        return template.delete(budgetRec, removeByBudgetCallback(budgetId));
    }

    public void createBudgetRecetteOnValidationBudget(BudgetId budgetId, Long persID) {
        createBudgetRecetteFromOperationsIntegrees(budgetId, persID);
        createBudgetRecetteFromPrevisions(budgetId, persID);
    }

    private SqlDeleteCallback removeByBudgetCallback(final BudgetId budgetId) {
        return new SqlDeleteCallback() {

            public long doInSqlDeleteClause(SQLDeleteClause delete) {
                return delete.where(budgetRec.idBudBudget.eq(budgetId.getId())).execute();
            }
        };
    }

    private SqlInsertWithKeyCallback<Long> createBudgetRecetteCallback(final BudgetRecette budgetRecette) {
        return new SqlInsertWithKeyCallback<Long>() {

            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(budgetRec));
                values.addAll(budgetRcetteAsList(budgetRecette));
                return insert.columns(columnsForInsert).values(values.toArray()).executeWithKey(budgetRec.idBudBudgetRec);
            }
        };
    }

    private List<Object> budgetRcetteAsList(BudgetRecette budgetRecette) {
        List<Object> ret = new ArrayList<Object>();
        ret.add(budgetRecette.getBudgetId().getId());
        ret.add(budgetRecette.getEntiteBudgetaireId().getId());
        ret.add(budgetRecette.getMontantRecette().value());
        ret.add(budgetRecette.getMontantTitre().value());
        ret.add(budgetRecette.getSectionRecette().getAbreviation());
        ret.add(budgetRecette.getNatureRecetteId().getId());
        ret.add(budgetRecette.getOrigineRecetteId().getId());
        OperationId operationId = budgetRecette.getOperationId();
        if (operationId != null) {
            ret.add(operationId.getId());
        } else {
            ret.add(null);
        }
        ret.add(budgetRecette.getPersIdCreation());
        ret.add(budgetRecette.getDateCreation());
        ret.add(budgetRecette.getPersIdModification());
        ret.add(budgetRecette.getDateModification());
        return ret;
    }

    private void createBudgetRecetteFromOperationsIntegrees(BudgetId budgetId, Long persID) {
        QBudPrevOpeTraRec table = QBudPrevOpeTraRec.budPrevOpeTraRec;
        SQLQuery query = template.newSqlQuery();
        query.from(table)
                .groupBy(table.idBudBudget, table.idAdmEb, table.section,
                        table.idAdmNatureRec, table.idAdmOrigineRecette, table.idOpeOperation)
                .where(table.idBudBudget.eq(budgetId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                table.montantRec.sum().as(montantRecetteSum),
                table.montantTitre.sum().as(montantTitreSum),
                table.idAdmEb,
                table.section,
                table.idAdmNatureRec,
                table.idAdmOrigineRecette,
                table.idOpeOperation));
        for (Tuple row : result) {
            BudgetRecette budgetRecette = BudgetRecette.builder().budgetId(budgetId)
                    .montantRecette(new Montant(row.get(montantRecetteSum)))
                    .montantTitre(new Montant(row.get(montantTitreSum)))
                    .entiteBudgetaireId(new EntiteBudgetaireId(row.get(table.idAdmEb)))
                    .sectionRecette(SectionRecetteCacheService.findByAbreviation(row.get(table.section)))
                    .natureRecetteId(new NatureRecetteId(row.get(table.idAdmNatureRec)))
                    .origineRecetteId(new OrigineRecetteId(row.get(table.idAdmOrigineRecette)))
                    .operationId(new OperationId(row.get(table.idOpeOperation)))
                    .persIdCreation(persID)
                    .dateCreation(new Date())
                    .build();
            createBudgetRecette(budgetRecette);
        }
    }

    private void createBudgetRecetteFromPrevisions(BudgetId budgetId, Long persID) {
        QBudPrevHorsOpRec table = QBudPrevHorsOpRec.budPrevHorsOpRec;
        SQLQuery query = template.newSqlQuery();
        query.from(table)
                .groupBy(table.idBudBudget, table.idAdmEb, table.section,
                        table.idAdmNatureRec, table.idAdmOrigineRecette)
                .where(table.idBudBudget.eq(budgetId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                table.montantRec.sum().as(montantRecetteSum),
                table.montantTitre.sum().as(montantTitreSum),
                table.idAdmEb,
                table.section,
                table.idAdmNatureRec,
                table.idAdmOrigineRecette));
        for (Tuple row : result) {
            BudgetRecette budgetRecette = BudgetRecette.builder().budgetId(budgetId)
                    .montantRecette(new Montant(row.get(montantRecetteSum)))
                    .montantTitre(new Montant(row.get(montantTitreSum)))
                    .entiteBudgetaireId(new EntiteBudgetaireId(row.get(table.idAdmEb)))
                    .sectionRecette(SectionRecetteCacheService.findByAbreviation(row.get(table.section)))
                    .natureRecetteId(new NatureRecetteId(row.get(table.idAdmNatureRec)))
                    .origineRecetteId(new OrigineRecetteId(row.get(table.idAdmOrigineRecette)))
                    .persIdCreation(persID)
                    .dateCreation(new Date())
                    .build();
            createBudgetRecette(budgetRecette);
        }
    }

}
