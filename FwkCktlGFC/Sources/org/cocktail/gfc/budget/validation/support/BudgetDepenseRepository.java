/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.validation.support;

import java.util.List;

import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.validation.BudgetDepense;

/**
 * @author bourges
 *
 */
public interface BudgetDepenseRepository {
    /**
     * @param budgetDepense - {@link BudgetDepense} à enregistrer en base
     * @return budgetDepnse avec son Id
     */
    BudgetDepense createBudgetDepense(BudgetDepense budgetDepense);
    
    /**
     * suppression des {@link BudgetDepense} liées à un budget
     * @param budgetId - {@link BudgetId} correspondant aux {@link BudgetDepense} à supprimer
     * @return le nombre de {@link BudgetDepense} supprimées
     */
    long removeByBudget(BudgetId budgetId);
    
    /**
     * Créer les enregistrements {@link BudgetDepense} à la validation du budget
     * @param budgetId - l'id de budget qui est validé
     * @param persId - l'id de l'utilisateur qui fait cette action
     */
    void createBudgetDepenseOnValidationBudget(BudgetId budgetId, Long persId);

    /**
     * La liste des {@link BudgetDepense} par budget
     * @param budgetId - l'id de budget
     * @return
     */
    List<BudgetDepense> findByBudget(BudgetId budgetId);}
