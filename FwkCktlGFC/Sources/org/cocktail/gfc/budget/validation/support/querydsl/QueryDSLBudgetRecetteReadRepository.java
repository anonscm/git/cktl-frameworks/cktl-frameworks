/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.validation.support.querydsl;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.NatureRecette;
import org.cocktail.gfc.admin.nomenclature.OrigineRecette;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.read.PrevisionRecetteRead;
import org.cocktail.gfc.budget.validation.support.BudgetRecetteReadRepository;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureRec;
import org.cocktail.gfc.support.querydsl.QAdmOrigineRecette;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudBudgetRec;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.QTuple;

public class QueryDSLBudgetRecetteReadRepository implements BudgetRecetteReadRepository {

    // Tables
    private final QBudBudgetRec budgetRec = QBudBudgetRec.budBudgetRec;
    private final QAdmEb eb = QAdmEb.admEb;
    private final QAdmNatureRec natureRecette = QAdmNatureRec.admNatureRec;
    private final QAdmOrigineRecette origineRecette = QAdmOrigineRecette.admOrigineRecette;
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = QAdmEbExer.admEbExer;

    private QueryDslJdbcTemplate template;

    public QueryDSLBudgetRecetteReadRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
    }

    public List<PrevisionRecetteRead> findByBudget(BudgetId budgetId) {
        SQLQuery sqlQuery = getBudgetRecetteQuery()
                .where(budgetRec.idBudBudget.eq(budgetId.getId()))
                .orderBy(eb.orgUniv.asc(), eb.orgEtab.asc(), eb.orgUb.asc(), eb.orgCr.asc(), eb.orgSouscr.asc());
        QTuple tuple = new QTuple(
                budgetRec.idBudBudgetRec, budgetRec.montantRec, budgetRec.montantTitre, budgetRec.section,
                eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, ebExer.saisieBudgetaire,
                natureRecette.idAdmNatureRec, natureRecette.code, natureRecette.libelle,
                natureRecette.tyetId,
                origineRecette.idAdmOrigineRecette, origineRecette.code,
                origineRecette.abreviation, origineRecette.libelle,
                operation.idOpeOperation, operation.llOperation, operation.estFlechee,
                typeOperation.idOpeTypeOperation, typeOperation.codeTypeOperation,
                typeOperation.llTypeOperation
                );
        List<Tuple> queryResult = template.query(sqlQuery, tuple);

        List<PrevisionRecetteRead> ret = new ArrayList<PrevisionRecetteRead>();
        for (Tuple row : queryResult) {
            EntiteBudgetaire ebRet = new EntiteBudgetaire(row.get(eb.idAdmEb), row.get(eb.orgUniv), row.get(eb.orgEtab), row.get(eb.orgUb),
                    row.get(eb.orgCr), row.get(eb.orgSouscr), row.get(eb.orgLib), row.get(ebExer.saisieBudgetaire));
            NatureRecette natureRecetteRet = new NatureRecette(row.get(natureRecette.idAdmNatureRec), row.get(natureRecette.code),
                    row.get(natureRecette.libelle), row.get(natureRecette.tyetId));
            OrigineRecette origineRecetteRet = new OrigineRecette(row.get(origineRecette.idAdmOrigineRecette), row.get(origineRecette.code),
                    row.get(origineRecette.libelle), row.get(origineRecette.abreviation));
            SectionRecette sectionRecetteRet = SectionRecetteCacheService.findByAbreviation(row.get(budgetRec.section));
            PrevisionRecetteRead previsionRecetteRet;
            if (row.get(operation.idOpeOperation) != null) {
                TypeOperation typeOperationRet = new TypeOperation(row.get(typeOperation.idOpeTypeOperation),
                        row.get(typeOperation.codeTypeOperation), row.get(typeOperation.llTypeOperation));
                OperationId operationId = new OperationId(row.get(operation.idOpeOperation));
                boolean estFlechee = (row.get(operation.estFlechee) == 1);
                Operation operationRet = new Operation(operationId, row.get(operation.llOperation), estFlechee, typeOperationRet);
                previsionRecetteRet = PrevisionRecetteRead.builder().previsionId(row.get(budgetRec.idBudBudgetRec)).eb(ebRet)
                        .natureRecette(natureRecetteRet).origineRecette(origineRecetteRet)
                        .operation(operationRet)
                        .sectionRecette(sectionRecetteRet).montantBudgetaire(row.get(budgetRec.montantRec))
                        .montantTitre(row.get(budgetRec.montantTitre))
                        .build();
            } else {
                previsionRecetteRet = PrevisionRecetteRead.builder().previsionId(row.get(budgetRec.idBudBudgetRec)).eb(ebRet)
                        .natureRecette(natureRecetteRet).origineRecette(origineRecetteRet)
                        .sectionRecette(sectionRecetteRet).montantBudgetaire(row.get(budgetRec.montantRec))
                        .montantTitre(row.get(budgetRec.montantTitre))
                        .build();
            }
            ret.add(previsionRecetteRet);
        }
        return ret;
    }

    private SQLQuery getBudgetRecetteQuery() {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(budgetRec)
                .join(eb).on(budgetRec.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(budgetRec.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(origineRecette).on(budgetRec.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                // comme on veut l'info saisissable sur les EB alors il faut
                // faire une jointure avec le budget pour savoir sur quelle
                // année on est et chercher l'info saisissable sur l'EB pour
                // cette année là :
                .join(budget).on(budgetRec.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb))

                .leftJoin(operation).on(budgetRec.idOpeOperation.eq(operation.idOpeOperation))
                .leftJoin(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation));
        return sqlQuery;
    }
}
