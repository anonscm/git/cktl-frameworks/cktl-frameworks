/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import lombok.Getter;
import lombok.Setter;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.admin.nomenclature.TypeEtatService;

/**
 * Etat de l'Eb sur un budget
 * @author bourges
 *
 */
@Getter
@Setter
public class EbEtat {

    private EbEtatId ebEtatId;
    private BudgetId budgetId;
    private EntiteBudgetaireId ebId;
    private TypeEtat etat;
    
    public EbEtat(Long budgetEbEtatId, BudgetId budgetId, EntiteBudgetaireId ebId, TypeEtat typeEtat) {
    	if(budgetEbEtatId != null)
    		this.ebEtatId = new EbEtatId(budgetEbEtatId);
        this.budgetId = budgetId;
        this.ebId = ebId;
        this.etat = typeEtat;
    }
    
    public EbEtat(Long budgetEbEtatId, Long budgetId, Long ebId, Long typeEtat) {
        this.ebEtatId = initEbEtatId(budgetEbEtatId);
        this.budgetId = initBudgetId(budgetId);
        this.ebId = initEbId(ebId);
        this.etat = typeEtat != null ? TypeEtatService.getTypeEtatById(typeEtat) : TypeEtatService.Code.NON_RENSEIGNEE.etat();
    }
    
    private EbEtatId initEbEtatId(Long budgetEbEtatId) {
    	EbEtatId id = null;
    	if (budgetEbEtatId != null) {
    		id = new EbEtatId(budgetEbEtatId);
    	}
    	return id;
    }
    
    private BudgetId initBudgetId(Long budgetId) {
    	BudgetId id = null;
    	if (budgetId != null) {
    		id = new BudgetId(budgetId);
    	}
    	return id;
    }
    
    private EntiteBudgetaireId initEbId(Long ebId) {
    	EntiteBudgetaireId id = null;
    	if (ebId != null) {
    		id = new EntiteBudgetaireId(ebId);
    	}
    	return id;
    }
}
