/**
 * 
 */
package org.cocktail.gfc.budget.preparation;

import java.text.MessageFormat;

import org.cocktail.gfc.admin.nomenclature.NatureRecette;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.common.specification.AbstractSpecification;

import com.google.common.base.Optional;

/**
 * @author  Raymond NANEON <raymond.naneon at asso-cocktail.fr>
 *
 */
public class RegleNatureExerciceSurPrevisionRecette extends	AbstractSpecification<PrevisionRecetteHorsOp> {
	
	private static final String ERR_NATURE_DOIT_EXISTER_EXERCICE = "La nature de recette {0} doit exister pour l'exercice en cours d'utilisation";
    
    private NomenclatureFacade facade;
    private BudgetRepository budRepo;
    private String libelle;
    
    public RegleNatureExerciceSurPrevisionRecette(NomenclatureFacade facade, BudgetRepository budRepo) {
		// TODO Auto-generated constructor stub
        super();
        this.facade = facade;
        this.budRepo = budRepo;
        this.libelle = "";
	}

	@Override
	public boolean isSatisfiedBy(PrevisionRecetteHorsOp previsionRecette) {
		// TODO Auto-generated method stub
		boolean ret = false;
		NatureRecetteId id = previsionRecette.getCombinaison().getNatureRecetteId();
        BudgetId budId = previsionRecette.getBudgetId();
        int exercice = budRepo.findById(budId).getExeOrdre();
        Optional<NatureRecette> nature = facade.getNatureRecetteByIdAndByExercice(id, exercice);
        ret = nature.isPresent();
        libelle = ret ? nature.get().getLibelle() : id.getId().toString();
		return ret;
	}
	
	public String getLibelle() {
		return libelle;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return MessageFormat.format(ERR_NATURE_DOIT_EXISTER_EXERCICE, getLibelle());
	}

}
