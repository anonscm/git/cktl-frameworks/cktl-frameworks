/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.read;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Builder;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.budget.preparation.EbEtatId;
import org.cocktail.gfc.common.montant.Montant;

@Getter
@Setter
public class EbEtatSommesRead {
    
	private EbEtatId budgetEbEtatId;
    private EntiteBudgetaire eb;
    private Montant montantAE;
    private Montant montantCP;
    private Montant montantBudgetaire;
    private Montant soldeBudgetaire;
    private TypeEtat etat;
    
    @Builder
    public EbEtatSommesRead(Long budgetEbEtatIdNumber, TypeEtat typeEtat, EntiteBudgetaire eb, BigDecimal montantAE, BigDecimal montantCP, BigDecimal montantBudgetaire) {
    	//TODO FLA 18.03.2015 : decider si on passe un EtatEbId au constructeur directement. 
    	this.budgetEbEtatId = initId(budgetEbEtatIdNumber);
        this.eb = eb;
        this.montantAE = new Montant(montantAE);
        this.montantCP = new Montant(montantCP);
        this.montantBudgetaire = new Montant(montantBudgetaire);
        this.soldeBudgetaire = new Montant(getSoldeBudgetaire(montantBudgetaire, montantCP));
        this.etat = typeEtat;
    }
    
    private EbEtatId initId(Long budgetEbEtatIdNumber) {
    	if (budgetEbEtatIdNumber == null) {
        	return null;	
        }
    	return new EbEtatId(budgetEbEtatIdNumber);
    }
    
    private BigDecimal getSoldeBudgetaire(BigDecimal montantBudgetaire, BigDecimal montantCP) {
    	BigDecimal solde = BigDecimal.ZERO;
    	if (montantBudgetaire != null && montantCP != null) {
    		solde = montantBudgetaire.subtract(montantCP);
    	}
    	return solde;
    }
}
