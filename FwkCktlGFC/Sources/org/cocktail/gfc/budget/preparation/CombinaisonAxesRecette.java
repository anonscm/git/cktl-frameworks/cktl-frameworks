/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import static org.apache.commons.lang.Validate.notNull;
import lombok.Getter;
import lombok.experimental.Builder;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.operation.OperationId;

@Getter
public class CombinaisonAxesRecette {
	private EntiteBudgetaireId entiteBudgetaireId;
	private NatureRecetteId natureRecetteId;
	private OrigineRecetteId origineRecetteId;
	private SectionRecette sectionRecette;
	private OperationId operationId;

	public CombinaisonAxesRecette(EntiteBudgetaireId entiteBudgetaireId,
			NatureRecetteId natureRecetteId, OrigineRecetteId origineRecetteId,
			SectionRecette sectionRecette) {
		this(entiteBudgetaireId, natureRecetteId, null, origineRecetteId,
				sectionRecette);
	}

	@Builder
	public CombinaisonAxesRecette(EntiteBudgetaireId entiteBudgetaireId,
			NatureRecetteId natureRecetteId, OperationId operationId,
			OrigineRecetteId origineRecetteId, SectionRecette sectionRecette) {
		notNull(entiteBudgetaireId, "L'entité budgétaire ne peut être null");
		notNull(natureRecetteId, "La nature de recette ne peut être null");
		notNull(origineRecetteId, "L'origine de recette ne peut être null");
		notNull(sectionRecette, "La section de recette ne peut être null");
		this.entiteBudgetaireId = entiteBudgetaireId;
		this.natureRecetteId = natureRecetteId;
		this.operationId = operationId;
		this.origineRecetteId = origineRecetteId;
		this.sectionRecette = sectionRecette;
	}

	@Override
	public String toString() {
		return "CombinaisonAxes [entiteBudgetaireId=" + entiteBudgetaireId
				+ ", natureRecetteId= " + natureRecetteId + ", operationId= "
				+ operationId + ", origineRecetteId=" + origineRecetteId
				+ "sectionRecette=" + sectionRecette + "]";
	}
}
