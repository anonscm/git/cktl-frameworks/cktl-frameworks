/**
 * 
 */
package org.cocktail.gfc.budget.preparation;

import java.text.MessageFormat;

import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.common.specification.AbstractSpecification;

import com.google.common.base.Optional;

/**
 * @author  Raymond NANEON <raymond.naneon at asso-cocktail.fr>
 *
 */
public class RegleNatureExerciceSurPrevisionDepense extends	AbstractSpecification<PrevisionDepenseHorsOp> {
	
	private static final String ERR_NATURE_DOIT_EXISTER_EXERCICE = "La nature de dépense {0} doit exister pour l'exercice en cours d'utilisation";
    
    private NomenclatureFacade facade;
    private BudgetRepository budRepo;
    private String libelle;
    
    public RegleNatureExerciceSurPrevisionDepense(NomenclatureFacade facade, BudgetRepository budRepo) {
		// TODO Auto-generated constructor stub
        super();
        this.facade = facade;
        this.budRepo = budRepo;
        this.libelle = "";
	}

	@Override
	public boolean isSatisfiedBy(PrevisionDepenseHorsOp previsionDepense) {
		// TODO Auto-generated method stub
		boolean ret = false;
		NatureDepenseId id = previsionDepense.getCombinaison().getNatureDepenseId();
        BudgetId budId = previsionDepense.getBudgetId();
        int exercice = budRepo.findById(budId).getExeOrdre();
        Optional<NatureDepense> nature = facade.getNatureDepenseByIdAndByExercice(id, exercice);
        ret = nature.isPresent();
        libelle = ret ? nature.get().getLibelle() : id.getId().toString();
		return ret;
	}
	
	public String getLibelle() {
		return libelle;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return MessageFormat.format(ERR_NATURE_DOIT_EXISTER_EXERCICE, getLibelle());
	}

}
