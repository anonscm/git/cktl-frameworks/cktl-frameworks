/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import java.text.MessageFormat;

import org.cocktail.common.support.AbstractValidator;
import org.cocktail.common.support.ValidationNotificationHandler;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.budget.preparation.support.EbEtatRepository;

import com.google.common.base.Optional;

public class PrevisionDepenseValidator extends AbstractValidator<PrevisionDepenseHorsOp> {

    private static final String ERR_NATURE_NON_TROUVEE = "La nature de dépense pour cette prévision n'existe pas";
    private static final String ERR_EB_ETAT_NE_PERMET_PAS_SAISIE = "L''état de l''entité budgétaire {0} ne permet pas la saisie de prévisions.";

    private BudgetRepository budgetRepository;
    private EbEtatRepository ebEtatRepository;
    private NomenclatureFacade nomenclatureFacade;

    public PrevisionDepenseValidator(ValidationNotificationHandler aHandler, BudgetRepository budgetRepository, EbEtatRepository ebEtatRepository,
    		NomenclatureFacade nomenclatureFacade) {
        super(aHandler);
        this.budgetRepository = budgetRepository;
        this.ebEtatRepository = ebEtatRepository;
        this.nomenclatureFacade = nomenclatureFacade;
    }

    @Override
    public boolean isValid(PrevisionDepenseHorsOp previsionDepense) {
        boolean isValid = true;
        BudgetId budId = previsionDepense.getBudgetId();
        Budget budget = budgetRepository.findById(budId);

        RegleAEPositifSiBudgetInitial regleMtAEBudgetInitial = new RegleAEPositifSiBudgetInitial(budget.estInitial());
        if (!regleMtAEBudgetInitial.isSatisfiedBy(previsionDepense)) {
            isValid = false;
            getNotificationHandler().handleError(regleMtAEBudgetInitial.getMessage());
        }

        RegleCPPositifSiBudgetInitial regleMtCPBudgetInitial = new RegleCPPositifSiBudgetInitial(budget.estInitial());
        if (!regleMtCPBudgetInitial.isSatisfiedBy(previsionDepense)) {
            isValid = false;
            getNotificationHandler().handleError(regleMtCPBudgetInitial.getMessage());
        }

        Optional<NatureDepense> optionalNatureDep = nomenclatureFacade.getNatureDepenseById(previsionDepense.getCombinaison().getNatureDepenseId());
        if (optionalNatureDep.isPresent()) {
            NatureDepense nature = optionalNatureDep.get();
            RegleAECPEgaux aeEtCPEgaux = new RegleAECPEgaux(nature);
            if (!aeEtCPEgaux.isSatisfiedBy(previsionDepense)) {
                isValid = false;
                getNotificationHandler().handleError(aeEtCPEgaux.getMessage());
            }
        } else {
            getNotificationHandler().handleError(ERR_NATURE_NON_TROUVEE);
            isValid = false;
        }

        RegleEBSaisissableSurPrevisionDepense regleEBSaisissable = new RegleEBSaisissableSurPrevisionDepense(nomenclatureFacade, budgetRepository);
        if (!regleEBSaisissable.isSatisfiedBy(previsionDepense)) {
            isValid = false;
            getNotificationHandler().handleError(regleEBSaisissable.getMessage());
        }
        
        RegleDestinationSaisissableSurPrevisionDepense regleDestionationSaisissable = new RegleDestinationSaisissableSurPrevisionDepense(nomenclatureFacade, budgetRepository);
        if(!regleDestionationSaisissable.isSatisfiedBy(previsionDepense)) {
        	isValid = false;
        	getNotificationHandler().handleError(regleDestionationSaisissable.getMessage());
        }
        
        RegleNatureExerciceSurPrevisionDepense regleNatureExerciceSurPrevisionDepense = new RegleNatureExerciceSurPrevisionDepense(nomenclatureFacade, budgetRepository);
        if(!regleNatureExerciceSurPrevisionDepense.isSatisfiedBy(previsionDepense)) {
        	isValid = false;
        	getNotificationHandler().handleError(regleNatureExerciceSurPrevisionDepense.getMessage());
        }

        EntiteBudgetaireId ebId = previsionDepense.getCombinaison().getEntiteBudgetaireId();
        Optional<EbEtat> ebEtatOptional = ebEtatRepository.findByBudgetIdAndEntiteBudgetaireId(budId, ebId);
        RegleEbEtatPermetSaisiePrevision regleEbEtatNonRenseignee = new RegleEbEtatPermetSaisiePrevision();        
        if (!regleEbEtatNonRenseignee.isSatisfiedBy(ebEtatOptional)) {
            isValid = false;
            Optional<EntiteBudgetaire> eb = nomenclatureFacade.findByIdAndExercice(ebId, budget.getExeOrdre());
            String libelle = eb.isPresent() ? eb.get().getLibelle() : ebId.getId().toString();
            getNotificationHandler().handleError(MessageFormat.format(ERR_EB_ETAT_NE_PERMET_PAS_SAISIE, libelle));
        }

        return isValid;
    }

}
