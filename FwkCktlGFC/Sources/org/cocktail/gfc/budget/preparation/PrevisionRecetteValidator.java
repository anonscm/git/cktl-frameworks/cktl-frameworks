/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import java.text.MessageFormat;

import org.cocktail.common.support.AbstractValidator;
import org.cocktail.common.support.ValidationNotificationHandler;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.budget.preparation.support.EbEtatRepository;

import com.google.common.base.Optional;

public class PrevisionRecetteValidator extends AbstractValidator<PrevisionRecetteHorsOp> {

    private BudgetRepository budgetRepository;
    private EbEtatRepository ebEtatRepository;
    private NomenclatureFacade nomenclatureFacade;
    private static final String ERR_EB_ETAT_NE_PERMET_PAS_SAISIE = "L''état de l''entité budgétaire {0} ne permet pas la saisie de prévisions.";
    
    public PrevisionRecetteValidator(ValidationNotificationHandler aHandler, BudgetRepository budgetRepository, EbEtatRepository ebEtatRepository,
            NomenclatureFacade nomenclatureFacade) {
        super(aHandler);
        this.budgetRepository = budgetRepository;
        this.nomenclatureFacade = nomenclatureFacade;
        this.ebEtatRepository = ebEtatRepository;
    }

    @Override
    public boolean isValid(PrevisionRecetteHorsOp previsionRecette) {
        boolean isValid = true;
        
        Budget budget = budgetRepository.findById(previsionRecette.getBudgetId());
        RegleMontantPositifSiBudgetInitial regleMtBudBudgetInitial = new RegleMontantPositifSiBudgetInitial(budget.estInitial()); 
        if (!regleMtBudBudgetInitial.isSatisfiedBy(previsionRecette.getMontantBudgetaire())) {
            isValid = false;
            getNotificationHandler().handleError(regleMtBudBudgetInitial.getMessage());
        }

        RegleMontantPositifSiBudgetInitial regleMtTitreBudgetInitial = new RegleMontantPositifSiBudgetInitial(budget.estInitial()); 
        if (!regleMtTitreBudgetInitial.isSatisfiedBy(previsionRecette.getMontantTitre())) {
        	isValid = false;
        	getNotificationHandler().handleError(regleMtTitreBudgetInitial.getMessage());
        }
        
        RegleEBSaisissableSurPrevisionRecette regleEBSaisissable = new RegleEBSaisissableSurPrevisionRecette(nomenclatureFacade, budgetRepository);
        if (!regleEBSaisissable.isSatisfiedBy(previsionRecette)) {
            isValid = false;
            getNotificationHandler().handleError(regleEBSaisissable.getMessage());
        }
        
        RegleNatureExerciceSurPrevisionRecette regleNatureExerciceSurPrevisionRecette = new RegleNatureExerciceSurPrevisionRecette(nomenclatureFacade, budgetRepository);
        if(!regleNatureExerciceSurPrevisionRecette.isSatisfiedBy(previsionRecette)) {
        	isValid = false;
        	getNotificationHandler().handleError(regleNatureExerciceSurPrevisionRecette.getMessage());
        }
        
        RegleOrigineExerciceSurPrevisionRecette regleOrigineExerciceSurPrevisionRecette = new RegleOrigineExerciceSurPrevisionRecette(nomenclatureFacade, budgetRepository);
        if(!regleOrigineExerciceSurPrevisionRecette.isSatisfiedBy(previsionRecette)) {
        	isValid = false;
        	getNotificationHandler().handleError(regleOrigineExerciceSurPrevisionRecette.getMessage());
        }
        
        BudgetId budId = previsionRecette.getBudgetId();
        EntiteBudgetaireId ebId = previsionRecette.getCombinaison().getEntiteBudgetaireId();        
        Optional<EbEtat> ebEtatOptional = ebEtatRepository.findByBudgetIdAndEntiteBudgetaireId(budId, ebId);
        RegleEbEtatPermetSaisiePrevision regleEbEtatNonRenseignee = new RegleEbEtatPermetSaisiePrevision();
        if (!regleEbEtatNonRenseignee.isSatisfiedBy(ebEtatOptional)) {
            isValid = false;
            Optional<EntiteBudgetaire> eb = nomenclatureFacade.findByIdAndExercice(ebId, budget.getExeOrdre());
            String libelle = eb.isPresent() ? eb.get().getLibelle() : ebId.getId().toString();
            getNotificationHandler().handleError(MessageFormat.format(ERR_EB_ETAT_NE_PERMET_PAS_SAISIE, libelle));
        }
        
        return isValid;
    }
}
