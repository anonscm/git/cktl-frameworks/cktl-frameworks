/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import lombok.Getter;
import lombok.Setter;

import org.cocktail.common.support.CktlDateValidator;
import org.cocktail.common.support.CktlGFCDate;
import org.cocktail.gfc.support.exception.GfcValidationException;
import org.joda.time.DateTime;

@Getter
@Setter
public class CalendrierBudget {

	private CktlGFCDate dateVote; // Date de vote du budget au CA
	private CktlGFCDate dateEnvoi; // Date d'envoi aux tutelles
	private CktlGFCDate dateNonApprobation; // Date de non approbation
	private CktlGFCDate dateApprobation; // Date d'approbation
	private CktlGFCDate dateExecution; // Date de mise en exécution

	public CalendrierBudget() {
		super();
		this.dateVote = new CktlGFCDate();
		this.dateEnvoi = new CktlGFCDate();
		this.dateNonApprobation = new CktlGFCDate();
		this.dateApprobation = new CktlGFCDate();
		this.dateExecution = new CktlGFCDate();
	}

	public CalendrierBudget(DateTime dateVote, DateTime dateEnvoi, DateTime dateNonApprobation, DateTime dateApprobation, DateTime dateExecution) throws GfcValidationException {
		super();
		this.dateVote = new CktlGFCDate(dateVote);
		this.dateEnvoi = new CktlGFCDate(dateEnvoi);
		this.dateNonApprobation = new CktlGFCDate(dateNonApprobation);
		this.dateApprobation = new CktlGFCDate(dateApprobation);
		this.dateExecution = new CktlGFCDate(dateExecution);
	}

	public CalendrierBudget(String dateVote, String dateEnvoi, String dateNonApprobation, String dateApprobation, String dateExecution) throws GfcValidationException {
		super();
		this.dateVote = new CktlGFCDate(dateVote);
		this.dateEnvoi = new CktlGFCDate(dateEnvoi);
		this.dateNonApprobation = new CktlGFCDate(dateNonApprobation);
		this.dateApprobation = new CktlGFCDate(dateApprobation);
		this.dateExecution = new CktlGFCDate(dateExecution);
	}

	// -- Date VOTE

	public CalendrierBudget checkDateVoteValide(CktlGFCDate newDateVote) throws GfcValidationException {
		CktlDateValidator dateValidator = new CktlDateValidator(newDateVote);
		dateValidator.checkIsNotNull().checkIsBeforeToday();
		return this;
	}

	public CalendrierBudget checkDateVoteModifiable() throws GfcValidationException {
		if (dateEnvoi.isNotNull() || dateNonApprobation.isNotNull() || dateApprobation.isNotNull() || dateExecution.isNotNull()) {
			throw new GfcValidationException("Les dates d'envoi, de non approbation, d'approbation et d'exécution doivent être vides");
		}
		return this;
	}

	public CalendrierBudget checkDateVoteSupprimable() throws GfcValidationException {
		return checkDateVoteModifiable();
	}

	// -- Date ENVOI

	public CalendrierBudget checkDateEnvoiValide(CktlGFCDate newDateEnvoi) throws GfcValidationException {
		CktlDateValidator dateValidator = new CktlDateValidator(newDateEnvoi);
		dateValidator.checkIsNotNull().checkIsBeforeToday()
				.checkIsNotNull(dateVote, "La date de vote doit être renseignée")
				.checkIsAfter(dateVote, "La date d'envoi doit être supérieure ou égale à la date de vote");
		return this;
	}

	public CalendrierBudget checkDateEnvoiModifiable() throws GfcValidationException {
		if (dateVote.isNull()) {
			throw new GfcValidationException("La date de vote doit être renseignée");
		}
		if (dateNonApprobation.isNotNull() || dateApprobation.isNotNull() || dateExecution.isNotNull()) {
			throw new GfcValidationException("Les dates de non approbation, d'approbation et d'exécution doivent être vides");
		}
		return this;
	}

	public CalendrierBudget checkDateEnvoiSupprimable() throws GfcValidationException {
		if (dateNonApprobation.isNotNull() || dateApprobation.isNotNull() || dateExecution.isNotNull()) {
			throw new GfcValidationException("Les dates de non approbation, d'approbation et d'exécution doivent être vides");
		}
		return this;
	}

	// -- Date NON-APPROBATION

	public CalendrierBudget checkDateNonApprobationValide(CktlGFCDate newDateNonApprobation) throws GfcValidationException {
		CktlDateValidator dateValidator = new CktlDateValidator(newDateNonApprobation);
		dateValidator.checkIsNotNull().checkIsBeforeToday()
				.checkIsNotNull(dateVote, "La date de vote doit être renseignée")
				.checkIsAfter(dateVote, "La date de non approbation doit être supérieure ou égale à la date de vote")
				.checkIsNotNull(dateEnvoi, "La date d'envoi doit être renseignée")
				.checkIsAfter(dateEnvoi, "La date non approbation doit être supérieure ou égale à la date d'envoi");
		return this;
	}

	public CalendrierBudget checkDateNonApprobationModifiable() throws GfcValidationException {
		if (dateApprobation.isNotNull() || dateExecution.isNotNull()) {
			throw new GfcValidationException("Les dates d'approbation et d'exécution doivent être vides");
		}
		return this;
	}

	public CalendrierBudget checkDateNonApprobationSupprimable() throws GfcValidationException {
		return checkDateNonApprobationModifiable();
	}

	// -- Date APPROBATION

	public CalendrierBudget checkDateApprobationValide(CktlGFCDate newDateApprobation) throws GfcValidationException {
		CktlDateValidator dateValidator = new CktlDateValidator(newDateApprobation);
		dateValidator.checkIsNotNull().checkIsBeforeToday()
				.checkIsNotNull(dateVote, "La date de vote doit être renseignée")
				.checkIsAfter(dateVote, "La date d'approbation doit être supérieure ou égale à la date de vote")
				.checkIsNotNull(dateEnvoi, "La date d'envoi doit être renseignée")
				.checkIsAfter(dateEnvoi, "La date non approbation doit être supérieure ou égale à la date d'envoi");
		return this;
	}

	public CalendrierBudget checkDateApprobationModifiable() throws GfcValidationException {
		if (dateNonApprobation.isNotNull() || dateExecution.isNotNull()) {
			throw new GfcValidationException("Les dates de non approbation et d'exécution doivent être vides");
		}
		return this;
	}

	public CalendrierBudget checkDateApprobationSupprimable() throws GfcValidationException {
		return checkDateApprobationModifiable();
	}

	// -- Date EXECUTION

	public CalendrierBudget checkDateExecutionValide(CktlGFCDate newDateExecution, boolean isBudgetInitial, int exercice) throws GfcValidationException {
		CktlDateValidator dateValidator = new CktlDateValidator(newDateExecution);
		dateValidator.checkIsNotNull().checkIsAfterToday()
				.checkIsNotNull(dateVote, "La date de vote doit être renseignée")
				.checkIsAfter(dateVote, "La date d'éxecution doit être supérieure ou égale à la date de vote")
		        .checkIsStrictlyAfter(dateApprobation, "La date d'éxecution doit être postérieure à la date d'approbation");
		if (dateEnvoi.isNotNull()) {
			dateValidator.checkIsAfter(dateEnvoi, "La date d'éxecution doit être supérieure ou égale à la date d'envoi");
		}
		if (isBudgetInitial) {
			CktlGFCDate premierJanvier = CktlGFCDate.premierJanvier(exercice);
			dateValidator.checkIsAfter(premierJanvier, "La date d'éxecution doit être supérieure ou égale au 1er janvier");
		}
		return this;
	}

	public CalendrierBudget checkDateExecutionModifiable() {
		return this;
	}

	public String getDateVoteToString() {
		if (dateVote != null) {
			return dateVote.toDateWithoutTime();
		}
		return null;
	}

	public String getDateEnvoiToString() {
		if (dateEnvoi != null) {
			return dateEnvoi.toDateWithoutTime();
		}
		return null;
	}

	public String getDateNonApprobationToString() {
		if (dateNonApprobation != null) {
			return dateNonApprobation.toDateWithoutTime();
		}
		return null;
	}

	public String getDateApprobationToString() {
		if (dateApprobation != null) {
			return dateApprobation.toDateWithoutTime();
		}
		return null;
	}

	public String getDateExecutionToString() {
		if (dateExecution != null) {
			return dateExecution.toDateWithoutTime();
		}
		return null;
	}
}
