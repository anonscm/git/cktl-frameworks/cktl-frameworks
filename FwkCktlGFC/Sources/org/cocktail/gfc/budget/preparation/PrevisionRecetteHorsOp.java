/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.common.montant.Montant;

@AllArgsConstructor
@Getter
@Setter
public class PrevisionRecetteHorsOp {
	
	private PrevisionRecetteId previsionId;
	private BudgetId budgetId;
	private CombinaisonAxesRecette combinaison;
	private Montant montantBudgetaire;
	private Montant montantTitre;
	private String commentaire;
	private Long persIdCreation;
	private Date dateCreation;
	private Long persIdModification;
	private Date dateModification;

	private void initPrevisionRecette(PrevisionRecetteId previsionRecetteId,
			BudgetId budgetId, 
			CombinaisonAxesRecette combinaison,
			Montant montantBudgetaire,
			Montant montantTitre,
			String commentaire, 
			Long persIdCreation,
			Date dateCreation, 
			Long persIdModification, 
			Date dateModification) {
		this.previsionId = previsionRecetteId;
		this.budgetId = budgetId;
		this.combinaison = combinaison;
		this.montantBudgetaire = montantBudgetaire;
		this.montantTitre = montantTitre;
		this.commentaire = commentaire;
		this.persIdCreation = persIdCreation;
		this.dateCreation = dateCreation;
		this.persIdModification = persIdModification;
		this.dateModification = dateModification;
	}

	public PrevisionRecetteHorsOp(Long colPrevisionId, Long colBudgetId,
			Long colEntiteBudgetaireId, Long colNatureRecetteId,
			Long colOrigineRecetteId, SectionRecette colSection, 
			BigDecimal colMontantBudgetaire,
			BigDecimal colMontantTitre,
			String colCommentaire, Long colPersIdCreation,
			Date colDateCreation, Long colPersIdModification,
			Date colDateModification) {
		
		PrevisionRecetteId prevId = new PrevisionRecetteId(colPrevisionId);
		BudgetId budId = new BudgetId(colBudgetId);
		EntiteBudgetaireId eBId = new EntiteBudgetaireId(colEntiteBudgetaireId);
		NatureRecetteId nRId = new NatureRecetteId(colNatureRecetteId);
		OrigineRecetteId oRId = new OrigineRecetteId(colOrigineRecetteId);
		CombinaisonAxesRecette combi = 
				new CombinaisonAxesRecette(eBId, nRId, oRId, colSection);
		Montant mBudgetaire = new Montant(colMontantBudgetaire);
		Montant mTitre = new Montant(colMontantTitre);
		
		initPrevisionRecette(prevId, budId, combi, mBudgetaire, 
		        mTitre, colCommentaire, colPersIdCreation, colDateCreation, 
				colPersIdModification, colDateModification);
	}

	public PrevisionRecetteHorsOp(Long persIdCreation, BudgetId budgetId,
			CombinaisonAxesRecette combinaison, Montant montantBudgetaire, Montant montantTitre, String commentaire) {
		PrevisionRecetteId id = null;
		Date dateCrea = new Date();
		Long persIdModif = null;
		Date dateModif = null;
		initPrevisionRecette(id, budgetId, combinaison, montantBudgetaire, montantTitre, 
				commentaire, persIdCreation, dateCrea, persIdModif, dateModif);
	}

    public void mettreAJour(CombinaisonAxesRecette combinaison, Montant montantBudgetaire, Montant montantTitre, String commentaire, Long persIdModificateur) {
        setCombinaison(combinaison);
        setMontantBudgetaire(montantBudgetaire);
        setMontantTitre(montantTitre);
        setCommentaire(commentaire);
        setPersIdModification(persIdModificateur);
        setDateModification(new Date());
    }
}
