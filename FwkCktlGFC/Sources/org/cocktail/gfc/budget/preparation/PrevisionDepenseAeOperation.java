/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import static org.apache.commons.lang.Validate.notNull;

import java.util.Date;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Builder;

import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.TrancheDepenseAeId;

@Getter
@Setter
public class PrevisionDepenseAeOperation {

	private PrevisionDepenseId previsionId;
	private BudgetId budgetId;
	private CombinaisonAxes combinaison;
	private TrancheDepenseAeId trancheDepenseAeId;
	private Montant montantAE;
	private String commentaire;
	private Long persIdCreation;
	private Date dateCreation;
	private Long persIdModification;
	private Date dateModification;
	
	@Builder
	private PrevisionDepenseAeOperation(PrevisionDepenseId previsionDepenseId, @NonNull BudgetId budgetId,
			CombinaisonAxes combinaison, @NonNull TrancheDepenseAeId trancheDepenseAeId, @NonNull Montant montantAE, String commentaire, 
			@NonNull Long persIdCreation, @NonNull Date dateCreation, Long persIdModification, Date dateModification) {	    
		this.previsionId = previsionDepenseId;
		this.budgetId = budgetId;
		setCombinaison(combinaison);
		this.trancheDepenseAeId = trancheDepenseAeId;
		this.montantAE = montantAE;
		this.commentaire = commentaire;
		this.persIdCreation = persIdCreation;
		this.dateCreation = dateCreation;
		this.persIdModification = persIdModification;
		this.dateModification = dateModification;
	}

	public void setCombinaison(CombinaisonAxes combinaison) {
		notNull(combinaison, "La combinaison d'axes est obligatoire.");
		notNull(combinaison.getOperationId(), "L'Opération est obligatoire.");
		
		this.combinaison = combinaison;
	}

}
