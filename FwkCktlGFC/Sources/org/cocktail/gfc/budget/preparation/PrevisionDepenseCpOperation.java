package org.cocktail.gfc.budget.preparation;

import static org.apache.commons.lang.Validate.notNull;

import java.util.Date;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Builder;

import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.TrancheDepenseCpId;

@Getter
@Setter
public class PrevisionDepenseCpOperation {
    
    private PrevisionDepenseId previsionId;
    private BudgetId budgetId;
    private CombinaisonAxes combinaison;
    private TrancheDepenseCpId trancheDepenseCpId;
    private Montant montantCP;
    private String commentaire;
    private Long persIdCreation;
    private Date dateCreation;
    private Long persIdModification;
    private Date dateModification;

    @Builder
    private PrevisionDepenseCpOperation(PrevisionDepenseId previsionDepenseId, @NonNull BudgetId budgetId,
            CombinaisonAxes combinaison, @NonNull TrancheDepenseCpId trancheDepenseCpId, @NonNull Montant montantCP,
            @NonNull Long persIdCreation, @NonNull Date dateCreation, Long persIdModification, Date dateModification) {
        this.previsionId = previsionDepenseId;
        this.budgetId = budgetId;
        setCombinaison(combinaison);
        this.trancheDepenseCpId = trancheDepenseCpId;
        this.montantCP = montantCP;
        this.persIdCreation = persIdCreation;
        this.dateCreation = dateCreation;
        this.persIdModification = persIdModification;
        this.dateModification = dateModification;
    }

    public void setCombinaison(CombinaisonAxes combinaison) {
        notNull(combinaison, "La combinaison d'axes est obligatoire.");
        notNull(combinaison.getOperationId(), "L'Opération est obligatoire.");

        this.combinaison = combinaison;
    }
}
