/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import static org.apache.commons.lang.Validate.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Builder;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.operation.OperationId;

@Getter
@AllArgsConstructor
public class CombinaisonAxes {

	private EntiteBudgetaireId entiteBudgetaireId;
	private NatureDepenseId natureDepenseId;
	private EnveloppeBudgetaireId enveloppeId;
	private OperationId operationId;
	private DestinationDepenseId destinationDepenseId;

	public CombinaisonAxes(EntiteBudgetaireId entiteBudgetaireId, EnveloppeBudgetaireId enveloppeId, NatureDepenseId natureDepenseId, DestinationDepenseId destinationDepenseId) {
		this(entiteBudgetaireId, enveloppeId, natureDepenseId, null, destinationDepenseId);
	}
	
	@Builder
	public CombinaisonAxes(EntiteBudgetaireId entiteBudgetaireId, EnveloppeBudgetaireId enveloppeId, NatureDepenseId natureDepenseId, OperationId operationId, DestinationDepenseId destinationDepenseId) {
		notNull(entiteBudgetaireId, "L'entité budgétaire ne peut être null");
		notNull(enveloppeId, "L'enveloppe budgétaire ne peut être null");
		notNull(natureDepenseId, "La nature de dépense ne peut être null");
		notNull(destinationDepenseId, "La destination de dépense ne peut être null");
		this.entiteBudgetaireId = entiteBudgetaireId;
		this.natureDepenseId = natureDepenseId;
		this.enveloppeId = enveloppeId;
		this.operationId = operationId;
		this.destinationDepenseId = destinationDepenseId;
	}
	
    @Override
    public String toString() {
        return "CombinaisonAxes [entiteBudgetaireId=" + entiteBudgetaireId
        		+ ", natureDepenseId= " + natureDepenseId
        		+ ", enveloppeId= " + enveloppeId
        		+ ", operationId= " + operationId + "]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((destinationDepenseId == null) ? 0 : destinationDepenseId
						.hashCode());
		result = prime
				* result
				+ ((entiteBudgetaireId == null) ? 0 : entiteBudgetaireId
						.hashCode());
		result = prime * result
				+ ((enveloppeId == null) ? 0 : enveloppeId.hashCode());
		result = prime * result
				+ ((natureDepenseId == null) ? 0 : natureDepenseId.hashCode());
		result = prime * result
				+ ((operationId == null) ? 0 : operationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CombinaisonAxes other = (CombinaisonAxes) obj;
		if (destinationDepenseId == null) {
			if (other.destinationDepenseId != null)
				return false;
		} else if (!destinationDepenseId.equals(other.destinationDepenseId))
			return false;
		if (entiteBudgetaireId == null) {
			if (other.entiteBudgetaireId != null)
				return false;
		} else if (!entiteBudgetaireId.equals(other.entiteBudgetaireId))
			return false;
		if (enveloppeId == null) {
			if (other.enveloppeId != null)
				return false;
		} else if (!enveloppeId.equals(other.enveloppeId))
			return false;
		if (natureDepenseId == null) {
			if (other.natureDepenseId != null)
				return false;
		} else if (!natureDepenseId.equals(other.natureDepenseId))
			return false;
		if (operationId == null) {
			if (other.operationId != null)
				return false;
		} else if (!operationId.equals(other.operationId))
			return false;
		return true;
	}
}
