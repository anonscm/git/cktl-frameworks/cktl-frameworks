/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.SyntheseOperation;
import org.cocktail.gfc.operation.TrancheId;

@AllArgsConstructor
@Getter
@Setter
public class BudgetSyntheseOperation {
    
    private TrancheId trancheId;
    private Operation operation;
    private BudgetId budgetId;
	private Integer exercice;
	private Montant sommeAE;
	private Montant sommeCP;
	private Montant sommeBudgetaire;
	private Boolean valide;	
	private Boolean integree;
	
	public BudgetSyntheseOperation(SyntheseOperation syntheseOperation, long budgetId, boolean integree) {
        this(syntheseOperation.getTrancheId(),
                syntheseOperation.getOperation(),
                new BudgetId(budgetId),
                syntheseOperation.getExercice(),
                syntheseOperation.getSommeAE(),
                syntheseOperation.getSommeCP(),
                syntheseOperation.getSommeBudgetaire(),
                syntheseOperation.getValide(),
                new Boolean(integree));
    }
	
	public Boolean getValide() {
		return this.valide;
	}

}

