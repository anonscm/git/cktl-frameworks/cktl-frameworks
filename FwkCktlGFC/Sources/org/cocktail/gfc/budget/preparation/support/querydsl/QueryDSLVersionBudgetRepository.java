/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.util.List;

import org.cocktail.gfc.budget.preparation.VersionBudget;
import org.cocktail.gfc.budget.preparation.support.VersionBudgetRepository;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudVersionBudget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.MappingProjection;

public class QueryDSLVersionBudgetRepository extends GfcRepository implements VersionBudgetRepository {
	
	@Autowired
	private QueryDslJdbcTemplate template;
	
	// Tables
	private QBudVersionBudget vbud = new QBudVersionBudget("vbud");
	
	// Projection
	private Projection projection = new Projection(vbud);

	private SQLQuery getQuery() {
		SQLQuery sqlQuery = template.newSqlQuery().from(vbud);
		return sqlQuery;
	}

	private SQLQuery addByVersionCodeClause(SQLQuery sqlQuery, String code) {
		sqlQuery = sqlQuery.where(vbud.code.eq(code));
		return sqlQuery;
	}

	public List<VersionBudget> findAll() {
		return template.query(getQuery(), projection);
	}

	public VersionBudget findByCode(String code) {
		SQLQuery query = addByVersionCodeClause(getQuery(), code);
		return template.queryForObject(query, projection);
	}

	private class Projection extends MappingProjection<VersionBudget> {

		private static final long serialVersionUID = 1L;

		private QBudVersionBudget vbud;

		public Projection(QBudVersionBudget v) {
			super(VersionBudget.class, v.idBudVersionBudget, v.code, v.libelle);
			this.vbud = v;
		}

		@Override
		protected VersionBudget map(Tuple row) {
			return VersionBudget.builder()
					.id(row.get(vbud.idBudVersionBudget))
					.code(row.get(vbud.code))
					.libelle(row.get(vbud.libelle))
					.build();
		}
	}
}
