/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import static com.mysema.query.support.Expressions.as;
import static com.mysema.query.support.Expressions.numberPath;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLEntiteBudgetaireProjection;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaire;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseId;
import org.cocktail.gfc.budget.preparation.read.PrevisionDepenseRead;
import org.cocktail.gfc.budget.preparation.support.PrevisionDepenseReadRepository;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.operation.TypeOperationId;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudEnveloppe;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpDep;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepAe;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepCp;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.query.ListSubQuery;

public class QueryDSLPrevisionDepenseReadRepository implements PrevisionDepenseReadRepository {

    // Tables
    private final QBudPrevHorsOpDep prevision = QBudPrevHorsOpDep.budPrevHorsOpDep;
    private final QBudPrevOpeTraDepAe previsionAeOpeTra = QBudPrevOpeTraDepAe.budPrevOpeTraDepAe;
    private final QBudPrevOpeTraDepCp previsionCpOpeTra = QBudPrevOpeTraDepCp.budPrevOpeTraDepCp;
    private final QAdmEb eb = new QAdmEb("eb");
    private final QBudEnveloppe enveloppe = QBudEnveloppe.budEnveloppe;
    private final QAdmNatureDep natureDepense = QAdmNatureDep.admNatureDep;
    private final QAdmDestinationDepense destinationDepense = QAdmDestinationDepense.admDestinationDepense;
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = new QAdmEbExer("ebex");

    // Paths
    private final NumberPath<Long> budId = numberPath(Long.class, "budId");
    private final NumberPath<Long> ebId = numberPath(Long.class, "ebId");
    private final NumberPath<Long> destinationId = numberPath(Long.class, "destinationId");
    private final NumberPath<Long> natureDepId = numberPath(Long.class, "natureDepId");
    private final NumberPath<Long> opeId = numberPath(Long.class, "opeId");
    private final NumberPath<Long> enveloppeId = numberPath(Long.class, "enveloppeId");
    private final NumberPath<BigDecimal> montantAeSum = numberPath(BigDecimal.class, "montantAeSum");
    private final NumberPath<BigDecimal> montantCp = numberPath(BigDecimal.class, "montantCp");
    
    private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "sr");
    private final NumberPath<Long> sousReqBudId = numberPath(Long.class, sousReqPath, "budId");
    private final NumberPath<Long> sousReqEbId = numberPath(Long.class, sousReqPath, "ebId");
    private final NumberPath<Long> sousReqDestinationId =numberPath(Long.class, sousReqPath, "destinationId");
    private final NumberPath<Long> sousReqNatureDepId = numberPath(Long.class, sousReqPath, "natureDepId");
    private final NumberPath<Long> sousReqOpeId = numberPath(Long.class, sousReqPath, "opeId");
    private final NumberPath<Long> sousReqEnveloppeId = numberPath(Long.class, sousReqPath, "enveloppeId");
    private final NumberPath<BigDecimal> sousReqMontantAeSum = numberPath(BigDecimal.class, sousReqPath, "montantAeSum");
    private final NumberPath<BigDecimal> sousReqMontantCp = numberPath(BigDecimal.class, sousReqPath, "montantCp");
    
    // Projections
    private QueryDSLEntiteBudgetaireProjection projectionEb = new QueryDSLEntiteBudgetaireProjection();

    private final ConstructorExpression<EnveloppeBudgetaire> constructeurEnveloppe = Projections.constructor(EnveloppeBudgetaire.class,
            Projections.constructor(EnveloppeBudgetaireId.class, enveloppe.idBudEnveloppe), enveloppe.codeEnveloppe, enveloppe.llEnveloppe);

    private final ConstructorExpression<NatureDepense> constructeurNatureDep = Projections.constructor(NatureDepense.class,
            Projections.constructor(NatureDepenseId.class, natureDepense.idAdmNatureDep), natureDepense.code, natureDepense.libelle,
            natureDepense.fongible);

    private final ConstructorExpression<DestinationDepense> constructorDestinationDepense = Projections.constructor(DestinationDepense.class,
            Projections.constructor(DestinationDepenseId.class, destinationDepense.idAdmDestinationDepense), destinationDepense.code,
            destinationDepense.abreviation, destinationDepense.libelle);

    private final ConstructorExpression<Operation> constructeurOperation = Projections.constructor(Operation.class, Projections.constructor(
            OperationId.class, operation.idOpeOperation), operation.llOperation, operation.estFlechee, Projections.constructor(TypeOperation.class,
            Projections.constructor(TypeOperationId.class, typeOperation.idOpeTypeOperation), typeOperation.codeTypeOperation,
            typeOperation.llTypeOperation));

    private final ConstructorExpression<PrevisionDepenseRead> constructeurPrevision = Projections.constructor(PrevisionDepenseRead.class,
            prevision.idBudPrevHorsOpDep, projectionEb, constructeurEnveloppe, constructeurNatureDep, constructorDestinationDepense,
            prevision.montantAe, prevision.montantCp, prevision.commentaire);

    private final ConstructorExpression<PrevisionDepenseRead> constructeurPrevisionOpeTra = Projections.constructor(PrevisionDepenseRead.class,
            projectionEb, constructeurEnveloppe, constructeurNatureDep, constructorDestinationDepense,
            constructeurOperation,sousReqMontantAeSum, sousReqMontantCp);
    
    private QueryDslJdbcTemplate template;

    public QueryDSLPrevisionDepenseReadRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }

    public Optional<PrevisionDepenseRead> queryById(PrevisionDepenseId id) {
        SQLQuery baseQuery = getPrevisionsQuery();
        SQLQuery finalQuery = baseQuery.where(prevision.idBudPrevHorsOpDep.eq(id.getId()));
        PrevisionDepenseRead previsionResultat = getPrevision(finalQuery, false);
        return Optional.of(previsionResultat);
    }

    public List<PrevisionDepenseRead> queryByBudgetId(BudgetId budgetId, boolean avecOperationsIntegreesAuBudget) {
        SQLQuery baseQuery = getPrevisionsQuery();
        SQLQuery finalQuery = baseQuery.where(prevision.idBudBudget.eq(budgetId.getId()));

        List<PrevisionDepenseRead> previsions = getPrevisions(finalQuery, false);
        if (avecOperationsIntegreesAuBudget) {
            baseQuery = getPrevisionsOpeTraQuery();
            finalQuery = baseQuery.where(sousReqBudId.eq(budgetId.getId()));
            previsions.addAll(getPrevisions(finalQuery, true));
        }
        return previsions;
    }

    private SQLQuery getPrevisionsQuery() {
        SQLQuery sqlQuery = template.newSqlQuery().from(prevision)
                .join(eb).on(prevision.idAdmEb.eq(eb.idAdmEb))
                .join(enveloppe).on(prevision.idBudEnveloppe.eq(enveloppe.idBudEnveloppe))
                .join(natureDepense).on(prevision.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense).on(prevision.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                // comme on veut l'info saisissable sur les EB alors il faut
                // faire une jointure avec le budget pour savoir sur quelle
                // année on est et chercher l'info saisissable sur l'EB pour
                // cette année là :
                .join(budget).on(prevision.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb));
        return sqlQuery;
    }

    private ListSubQuery<Tuple> getPrevisionsAeCpOpeTraUnion() {
        ListSubQuery<Tuple> listSubAe = new SQLSubQuery()
                .from(previsionAeOpeTra)
                .groupBy(previsionAeOpeTra.idAdmEb, previsionAeOpeTra.idBudEnveloppe, previsionAeOpeTra.idAdmDestinationDepense,
                        previsionAeOpeTra.idAdmNatureDep, previsionAeOpeTra.idOpeOperation, previsionAeOpeTra.idBudBudget)
                .list(previsionAeOpeTra.idAdmEb.as(ebId), previsionAeOpeTra.idBudEnveloppe.as(enveloppeId),
                        previsionAeOpeTra.idAdmDestinationDepense.as(destinationId),
                        previsionAeOpeTra.idAdmNatureDep.as(natureDepId), previsionAeOpeTra.idOpeOperation.as(opeId),
                        previsionAeOpeTra.idBudBudget.as(budId),
                        previsionAeOpeTra.montantAe.sum().as(montantAeSum), as(ConstantImpl.create(BigDecimal.ZERO), montantCp));
        ListSubQuery<Tuple> listSubCp = new SQLSubQuery()
                .from(previsionCpOpeTra)
                .groupBy(previsionCpOpeTra.idAdmEb, previsionCpOpeTra.idBudEnveloppe, previsionCpOpeTra.idAdmDestinationDepense,
                        previsionCpOpeTra.idAdmNatureDep, previsionCpOpeTra.idOpeOperation, previsionCpOpeTra.idBudBudget)
                .list(previsionCpOpeTra.idAdmEb.as(ebId), previsionCpOpeTra.idBudEnveloppe.as(enveloppeId),
                        previsionCpOpeTra.idAdmDestinationDepense.as(destinationId),
                        previsionCpOpeTra.idAdmNatureDep.as(natureDepId), previsionCpOpeTra.idOpeOperation.as(opeId),
                        previsionCpOpeTra.idBudBudget.as(budId),
                        as(ConstantImpl.create(BigDecimal.ZERO), montantAeSum), previsionCpOpeTra.montantCp.sum().as(montantCp));
        ListSubQuery<Tuple> listSubDepense = new SQLSubQuery()
                .from(new SQLSubQuery().union(listSubAe, listSubCp).as(sousReqPath))
                .groupBy(ebId, enveloppeId, natureDepId, destinationId, opeId, budId)
                .list(ebId, enveloppeId, natureDepId, destinationId, opeId, budId, montantAeSum.sum().as(montantAeSum),
                        montantCp.sum().as(montantCp));
        return listSubDepense;
    }
    
    private SQLQuery getPrevisionsOpeTraQuery() {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(getPrevisionsAeCpOpeTraUnion().as(sousReqPath))
                .join(eb).on(sousReqEbId.eq(eb.idAdmEb))
                .join(enveloppe).on(sousReqEnveloppeId.eq(enveloppe.idBudEnveloppe))
                .join(natureDepense).on(sousReqNatureDepId.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense).on(sousReqDestinationId.eq(destinationDepense.idAdmDestinationDepense))
                .join(operation).on(sousReqOpeId.eq(operation.idOpeOperation))
                .join(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                // comme on veut l'info saisissable sur les EB alors il faut
                // faire une jointure avec le budget pour savoir sur quelle
                // année on est et chercher l'info saisissable sur l'EB pour
                // cette année là :
                .join(budget).on(sousReqBudId.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb));
        return sqlQuery;
    }

    private List<PrevisionDepenseRead> getPrevisions(SQLQuery sqlQuery, boolean avecOperationsIntegreesAuBudget) {
        List<PrevisionDepenseRead> previsions;
        if (avecOperationsIntegreesAuBudget) {
            previsions = template.query(sqlQuery, constructeurPrevisionOpeTra);
        } else {
            previsions = template.query(sqlQuery, constructeurPrevision);
        }
        return previsions;
    }

    private PrevisionDepenseRead getPrevision(SQLQuery sqlQuery, boolean estOperationIntegreeAuBudget) {
        PrevisionDepenseRead res;
        if(estOperationIntegreeAuBudget) {
        	res = template.queryForObject(sqlQuery, constructeurPrevisionOpeTra);
        } else {
        	res = template.queryForObject(sqlQuery, constructeurPrevision);
        }
        return res;
    }

}
