/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureRecette;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecette;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLEntiteBudgetaireProjection;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLSectionRecetteCacheProjection;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.PrevisionRecetteId;
import org.cocktail.gfc.budget.preparation.read.PrevisionRecetteRead;
import org.cocktail.gfc.budget.preparation.support.PrevisionRecetteReadRepository;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.operation.TypeOperationId;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureRec;
import org.cocktail.gfc.support.querydsl.QAdmOrigineRecette;
import org.cocktail.gfc.support.querydsl.QAdmTypeEtat;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpRec;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraRec;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;
import com.mysema.query.types.query.ListSubQuery;

public class QueryDSLPrevisionRecetteReadRepository implements
        PrevisionRecetteReadRepository {

    // Tables
    private final QBudPrevHorsOpRec prevision = QBudPrevHorsOpRec.budPrevHorsOpRec;
    private final QBudPrevOpeTraRec previsionOpeTra = QBudPrevOpeTraRec.budPrevOpeTraRec;
    private final QAdmEb eb = new QAdmEb("eb");
    private final QAdmNatureRec natureRecette = QAdmNatureRec.admNatureRec;
    private final QAdmTypeEtat natureTypeEtat = QAdmTypeEtat.admTypeEtat;
    private final QAdmOrigineRecette origineRecette = QAdmOrigineRecette.admOrigineRecette;
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = new QAdmEbExer("ebex");
    

	private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "mySousRequette");
    private StringPath sousReqOrgEbPath = Expressions.stringPath(sousReqPath, "ORG_ETAB");
    private StringPath sousReqOrgUbPath = Expressions.stringPath(sousReqPath, "ORG_UB");
    private StringPath sousReqOrgCrPath = Expressions.stringPath(sousReqPath, "ORG_CR");
    private StringPath sousReqOrgSouCrPath = Expressions.stringPath( sousReqPath, "ORG_SOUSCR");
    private StringPath sousReqOrgLibPath = Expressions.stringPath(sousReqPath, "ORG_LIB");
    private StringPath sousReqNaturePath = Expressions.stringPath(sousReqPath, "NATURE");
    private StringPath sousReqTypePath = Expressions.stringPath(sousReqPath, "TYET_LIBELLE");
    private NumberPath<BigDecimal> sousReqMontantRecPath = Expressions.numberPath(BigDecimal.class, sousReqPath, "MONTANT_REC");

    // Projections
    private QueryDSLEntiteBudgetaireProjection projectionEb = new QueryDSLEntiteBudgetaireProjection();

    private final ConstructorExpression<NatureRecette> constructeurNatureRec = Projections
            .constructor(NatureRecette.class, Projections.constructor(
                    NatureRecetteId.class, natureRecette.idAdmNatureRec),
                    natureRecette.code, natureRecette.libelle,
                    natureRecette.tyetId);

    private final ConstructorExpression<OrigineRecette> constructorOrigineRec = Projections
            .constructor(OrigineRecette.class,
                    Projections.constructor(OrigineRecetteId.class,
                            origineRecette.idAdmOrigineRecette),
                    origineRecette.code, origineRecette.libelle,
                    origineRecette.abreviation);

    private final ConstructorExpression<Operation> constructeurOperation = Projections.constructor(Operation.class, Projections.constructor(
            OperationId.class, operation.idOpeOperation), operation.llOperation, operation.estFlechee, Projections.constructor(TypeOperation.class,
            Projections.constructor(TypeOperationId.class, typeOperation.idOpeTypeOperation), typeOperation.codeTypeOperation,
            typeOperation.llTypeOperation));

    private final ConstructorExpression<PrevisionRecetteRead> constructeurPrevision = Projections
            .constructor(
                    PrevisionRecetteRead.class,
                    prevision.idBudPrevHorsOpRec,
                    projectionEb,
                    constructeurNatureRec,
                    constructorOrigineRec,
                    new QueryDSLSectionRecetteCacheProjection(prevision.section),
                    prevision.montantRec,
                    prevision.montantTitre,
                    prevision.commentaire
            );

    private final ConstructorExpression<PrevisionRecetteRead> constructeurPrevisionOpeTra = Projections.constructor(PrevisionRecetteRead.class,
            previsionOpeTra.idBudPrevOpeTraRec,
            projectionEb,
            constructeurNatureRec,
            constructorOrigineRec,
            new QueryDSLSectionRecetteCacheProjection(previsionOpeTra.section),
            constructeurOperation,
            previsionOpeTra.montantRec,
            previsionOpeTra.montantTitre, previsionOpeTra.commentaire);

    private QueryDslJdbcTemplate template;

    public QueryDSLPrevisionRecetteReadRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }

    public Optional<PrevisionRecetteRead> queryById(PrevisionRecetteId id, boolean estOperationIntegreeAuBudget) {
        SQLQuery baseQuery;
        SQLQuery finalQuery;
        if (estOperationIntegreeAuBudget) {
            baseQuery = getPrevisionsOpeTraQuery();
            finalQuery = baseQuery.where(previsionOpeTra.idBudPrevOpeTraRec.eq(id.getId()));
        } else {
            baseQuery = getPrevisionsQuery();
            finalQuery = baseQuery.where(prevision.idBudPrevHorsOpRec.eq(id.getId()));
        }
        PrevisionRecetteRead previsionResultat = getPrevision(finalQuery, estOperationIntegreeAuBudget);
        return Optional.of(previsionResultat);
    }

    public List<PrevisionRecetteRead> queryByBudgetId(BudgetId budgetId, boolean avecOperationsIntegreesAuBudget) {
        SQLQuery baseQuery = getPrevisionsQuery();
        SQLQuery finalQuery = baseQuery.where(prevision.idBudBudget.eq(budgetId.getId()));
        List<PrevisionRecetteRead> previsions = getPrevisions(finalQuery, false);
        if (avecOperationsIntegreesAuBudget) {
            baseQuery = getPrevisionsOpeTraQuery();
            finalQuery = baseQuery.where(previsionOpeTra.idBudBudget.eq(budgetId.getId()));
            previsions.addAll(getPrevisions(finalQuery, true));
        }
        return previsions;
    }

    private SQLQuery getPrevisionsQuery() {
        SQLQuery sqlQuery = template
                .newSqlQuery()
                .from(prevision)
                .join(eb).on(prevision.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(prevision.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(origineRecette).on(prevision.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                // comme on veut l'info saisissable sur les EB alors il faut
                // faire une jointure avec le budget pour savoir sur quelle
                // année on est et chercher l'info saisissable sur l'EB pour
                // cette année là :
                .join(budget).on(prevision.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue())).on(eb.idAdmEb.eq(ebExer.idAdmEb));
        return sqlQuery;
    }

    private SQLQuery getPrevisionsOpeTraQuery() {
        SQLQuery sqlQuery = template
                .newSqlQuery()
                .from(previsionOpeTra)
                .join(eb).on(previsionOpeTra.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(previsionOpeTra.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(origineRecette).on(previsionOpeTra.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                .join(operation).on(previsionOpeTra.idOpeOperation.eq(operation.idOpeOperation))
                .join(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                // comme on veut l'info saisissable sur les EB alors il faut
                // faire une jointure avec le budget pour savoir sur quelle
                // année on est et chercher l'info saisissable sur l'EB pour
                // cette année là :
                .join(budget).on(previsionOpeTra.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue())).on(eb.idAdmEb.eq(ebExer.idAdmEb));
        return sqlQuery;
    }

    private List<PrevisionRecetteRead> getPrevisions(SQLQuery sqlQuery, boolean avecOperationsIntegreesAuBudget) {
        List<PrevisionRecetteRead> previsions;
        if (avecOperationsIntegreesAuBudget) {
            previsions = template.query(sqlQuery, constructeurPrevisionOpeTra);
        } else {
            previsions = template.query(sqlQuery, constructeurPrevision);
        }
        return previsions;
    }

    private PrevisionRecetteRead getPrevision(SQLQuery sqlQuery, boolean estOperationIntegreeAuBudget) {
        PrevisionRecetteRead res;
        if (estOperationIntegreeAuBudget) {
            res = template.queryForObject(sqlQuery, constructeurPrevisionOpeTra);
        } else {
            res = template.queryForObject(sqlQuery, constructeurPrevision);
        }
        return res;
    }
}
