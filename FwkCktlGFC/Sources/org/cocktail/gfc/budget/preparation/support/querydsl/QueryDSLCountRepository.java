/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.budget.preparation.support.CountRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudBudgetDep;
import org.cocktail.gfc.support.querydsl.QBudBudgetRec;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpDep;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;

/**
 * @author bourges
 *
 */
public class QueryDSLCountRepository implements CountRepository {
    
    private QAdmEb admEb = QAdmEb.admEb;
    private QBudPrevHorsOpDep budPrevHorsOpDep = QBudPrevHorsOpDep.budPrevHorsOpDep;
    private QBudPrevHorsOpRec budPrevHorsOpRec = QBudPrevHorsOpRec.budPrevHorsOpRec;
    private QBudBudgetDep budBudgetDep = QBudBudgetDep.budBudgetDep;
    private QBudBudgetRec budBudgetRec = QBudBudgetRec.budBudgetRec;
    private QBudBudget budBudget = QBudBudget.budBudget;
    private QueryDslJdbcTemplate template;
    
    public QueryDSLCountRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }
    
    public long countPreparationsBudgetairesEnDepense(EntiteBudgetaireId ebId, int exercice) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(admEb)
                .join(budPrevHorsOpDep)
                    .on(budPrevHorsOpDep.idAdmEb.eq(admEb.idAdmEb))
                .join(budBudget)
                    .on(budPrevHorsOpDep.idBudBudget.eq(budBudget.idBudBudget))
                .where(admEb.idAdmEb.eq(ebId.getId())
                        .and(budBudget.exeOrdre.eq(exercice)));
        return template.count(sqlQuery);
    }


    public long countPreparationsBudgetairesEnRecette(EntiteBudgetaireId ebId, int exercice) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(admEb)
                .join(budPrevHorsOpRec)
                    .on(budPrevHorsOpRec.idAdmEb.eq(admEb.idAdmEb))
                .join(budBudget)
                    .on(budPrevHorsOpRec.idBudBudget.eq(budBudget.idBudBudget))
                .where(admEb.idAdmEb.eq(ebId.getId())
                        .and(budBudget.exeOrdre.eq(exercice)));
        return template.count(sqlQuery);
    }


    public long countDepensesVotees(EntiteBudgetaireId ebId, int exercice) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(admEb)
                .join(budBudgetDep)
                    .on(budBudgetDep.idAdmEb.eq(admEb.idAdmEb))
                .join(budBudget)
                    .on(budBudgetDep.idBudBudget.eq(budBudget.idBudBudget))
                .where(admEb.idAdmEb.eq(ebId.getId())
                        .and(budBudget.exeOrdre.eq(exercice)));
        return template.count(sqlQuery);
    }


    public long countRecettesVotees(EntiteBudgetaireId ebId, int exercice) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(admEb)
                .join(budBudgetRec)
                    .on(budBudgetRec.idAdmEb.eq(admEb.idAdmEb))
                .join(budBudget)
                    .on(budBudgetRec.idBudBudget.eq(budBudget.idBudBudget))
                .where(admEb.idAdmEb.eq(ebId.getId())
                        .and(budBudget.exeOrdre.eq(exercice)));
        return template.count(sqlQuery);
    }

}
