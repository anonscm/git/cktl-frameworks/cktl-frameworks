/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.read.EbEtatRead;
import org.cocktail.gfc.budget.preparation.support.EbEtatReadRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QBudBudgetEbEtat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.MappingProjection;
import com.mysema.query.types.OrderSpecifier;

public class QueryDSLEbEtatReadRepository implements EbEtatReadRepository {

    @Autowired
    private QueryDslJdbcTemplate template;

    //Tables
    private QAdmEb eb = new QAdmEb("eb");
    private QAdmEbExer ebExercice = new QAdmEbExer("ebex");
    private final QBudBudgetEbEtat budEbEtat = new QBudBudgetEbEtat("ebetat");
    
    // Projections
    private QueryDSLEbEtatReadProjection projection = new QueryDSLEbEtatReadProjection();
    
    protected static class QueryDSLEbEtatReadProjection extends MappingProjection<EbEtatRead> {

        private static final long serialVersionUID = 1L;
        private static final QAdmEb EB = new QAdmEb("eb");
        private static final QAdmEbExer EB_EXER = new QAdmEbExer("ebex");
        private static final QBudBudgetEbEtat EB_ETAT = new QBudBudgetEbEtat("ebetat");

        public QueryDSLEbEtatReadProjection() {
            super(EbEtatRead.class, EB.idAdmEb, EB.orgPere, EB.orgNiv, EB.orgUniv, EB.orgEtab, EB.orgUb, EB.orgCr, EB.orgSouscr,
                    EB.orgLib, EB_EXER.saisieBudgetaire, EB_ETAT.tyetId);
        }
        
        @Override
        protected EbEtatRead map(Tuple row) {
            return new EbEtatRead(EntiteBudgetaire.builder()
                    .id(row.get(EB.idAdmEb))
                    .idPere(row.get(EB.orgPere))
                    .niveau(row.get(EB.orgNiv))
                    .universite(row.get(EB.orgUniv))
                    .etablissement(row.get(EB.orgEtab))
                    .uniteBudgetaire(row.get(EB.orgUb))
                    .centreDeResponsabilite(row.get(EB.orgCr))
                    .sousCentreDeResponsabilite(row.get(EB.orgSouscr))
                    .libelle(row.get(EB.orgLib))
                    .saisissable(row.get(EB_EXER.saisieBudgetaire) == 1)
                    .build(), row.get(EB_ETAT.tyetId));
        }
    }    
    
    public QueryDSLEbEtatReadRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }
    
    //Orders
    private OrderSpecifier<String> orderByOrgUnivAsc() {
        return eb.orgUniv.asc();
    }

    private OrderSpecifier<String> orderByOrgEtabAsc() {
        return eb.orgEtab.asc().nullsFirst();
    }

    private OrderSpecifier<String> orderByOrgUbAsc() {
        return eb.orgUb.asc().nullsFirst();
    }

    private OrderSpecifier<String> orderByOrgCrAsc() {
        return eb.orgCr.asc().nullsFirst();
    }

    private OrderSpecifier<String> orderBySouscrCrAsc() {
        return eb.orgSouscr.asc().nullsFirst();
    }
        
    // Queries
    private SQLQuery getEBEtatQuery(Long budIdNumber, Integer exercice) {
        SQLQuery sqlQuery = template.newSqlQuery().from(eb);
        //on sélectionne toujours via un exercice
        sqlQuery = sqlQuery.join(ebExercice).on(eb.idAdmEb.eq(ebExercice.idAdmEb))
                .leftJoin(budEbEtat).on(eb.idAdmEb.eq(budEbEtat.idAdmEb).and(budEbEtat.idBudBudget.eq(budIdNumber)))
                .where(ebExercice.exeOrdre.eq(exercice));
        //On ne prend pas la racine technique
        sqlQuery = sqlQuery.where(eb.orgNiv.ne(0));
        //ajout d'un tri par defaut
        sqlQuery.orderBy(
                orderByOrgUnivAsc(),
                orderByOrgEtabAsc(),
                orderByOrgUbAsc(),
                orderByOrgCrAsc(),
                orderBySouscrCrAsc()
                );

        return sqlQuery;
    }
    
    private SQLQuery addSaisissableClause(SQLQuery sqlQuery, boolean isSaisissable) {
        int flag = 0;
        if (isSaisissable) flag = 1;
        sqlQuery = sqlQuery.where(ebExercice.saisieBudgetaire.eq(flag));
        return sqlQuery;
    }
    
    public List<EbEtatRead> findByExercice(BudgetId budId, Integer exercice) {        
        SQLQuery sqlQuery = getEBEtatQuery(budId.getId(), exercice);
        List<EbEtatRead> entiteBudgetaires = template.query(sqlQuery, projection);
        return entiteBudgetaires;
    }
    

    public List<EbEtatRead> findByExerciceAndSaisissable(BudgetId budId, int exercice, boolean isSaisissable) {
        SQLQuery sqlQuery = getEBEtatQuery(budId.getId(), exercice);
        sqlQuery = addSaisissableClause(sqlQuery, isSaisissable);
        List<EbEtatRead> entiteBudgetaires = template.query(sqlQuery, projection);
        return entiteBudgetaires;
    }
}
