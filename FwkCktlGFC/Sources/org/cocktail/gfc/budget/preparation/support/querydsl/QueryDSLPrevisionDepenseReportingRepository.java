/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.support.PrevisionDepenseReportingRepository;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudEnveloppe;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpDep;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepAe;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepCp;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;
import com.mysema.query.types.query.ListSubQuery;

/**
 * @author  Raymond NANEON <raymond.naneon at asso-cocktail.fr>
 *
 */
public class QueryDSLPrevisionDepenseReportingRepository implements PrevisionDepenseReportingRepository {
    
    // Tables
    private final QBudPrevHorsOpDep prevision = QBudPrevHorsOpDep.budPrevHorsOpDep;
    private final QBudPrevOpeTraDepAe previsionAeOpeTra = QBudPrevOpeTraDepAe.budPrevOpeTraDepAe;
    private final QBudPrevOpeTraDepCp previsionCpOpeTra = QBudPrevOpeTraDepCp.budPrevOpeTraDepCp;
    private final QAdmEb eb = new QAdmEb("eb");
    private final QBudEnveloppe enveloppe = QBudEnveloppe.budEnveloppe;
    private final QAdmNatureDep natureDepense = QAdmNatureDep.admNatureDep;
    private final QAdmDestinationDepense destinationDepense = QAdmDestinationDepense.admDestinationDepense;
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = new QAdmEbExer("ebex");    

    // Paths
	private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "mySousRequete");
    private StringPath sousReqOrgEbPath = Expressions.stringPath(sousReqPath, "ORG_ETAB");
    private StringPath sousReqOrgUbPath = Expressions.stringPath(sousReqPath, "ORG_UB");
    private StringPath sousReqOrgCrPath = Expressions.stringPath(sousReqPath, "ORG_CR");
    private StringPath sousReqOrgSouCrPath = Expressions.stringPath( sousReqPath, "ORG_SOUSCR");
    private StringPath sousReqOrgLibPath = Expressions.stringPath(sousReqPath, "ORG_LIB");
    private StringPath sousReqDestinationPath = Expressions.stringPath(sousReqPath, "DESTINATION");
    private StringPath sousReqNaturePath = Expressions.stringPath(sousReqPath, "NATURE");
    private NumberPath<BigDecimal> sousReqMontantAePath = Expressions.numberPath(BigDecimal.class, sousReqPath, "MONTANT_AE");
    private NumberPath<BigDecimal> sousReqMontantCpPath = Expressions.numberPath(BigDecimal.class, sousReqPath, "MONTANT_CP");
    
    private QueryDslJdbcTemplate template;

	public QueryDSLPrevisionDepenseReportingRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}

    public String getPrevisionsFiltreQuery(int exercice, BudgetId budgetId, List<Long> ebId) {
        BooleanBuilder whereBuilderDep = new BooleanBuilder();
        BooleanBuilder whereBuilderOpe = new BooleanBuilder();
        whereBuilderDep.and(prevision.idBudBudget.eq(budgetId.getId()));
        whereBuilderDep.and(budget.exeOrdre.eq(exercice));
        whereBuilderOpe.and(previsionAeOpeTra.idBudBudget.eq(budgetId.getId()));
        whereBuilderOpe.and(budget.exeOrdre.eq(exercice));
        if(ebId != null) {
        	whereBuilderDep.and(prevision.idAdmEb.in(ebId));
        	whereBuilderOpe.and(previsionAeOpeTra.idAdmEb.in(ebId));
        }
        ListSubQuery<Tuple> sqlQueryOpe = new SQLSubQuery().from(previsionAeOpeTra)
                .fullJoin(previsionCpOpeTra).on(previsionAeOpeTra.idBudBudget.eq(previsionCpOpeTra.idBudBudget),
                        previsionAeOpeTra.idAdmEb.eq(previsionCpOpeTra.idAdmEb),
                        previsionAeOpeTra.idBudEnveloppe.eq(previsionCpOpeTra.idBudEnveloppe),
                        previsionAeOpeTra.idAdmNatureDep.eq(previsionCpOpeTra.idAdmNatureDep),
                        previsionAeOpeTra.idAdmDestinationDepense.eq(previsionCpOpeTra.idAdmDestinationDepense),
                        previsionAeOpeTra.idOpeOperation.eq(previsionCpOpeTra.idOpeOperation))
        		.join(eb).on(previsionAeOpeTra.idAdmEb.eq(eb.idAdmEb))
                .join(enveloppe).on(previsionAeOpeTra.idBudEnveloppe.eq(enveloppe.idBudEnveloppe))
                .join(natureDepense).on(previsionAeOpeTra.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense).on(previsionAeOpeTra.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                .join(operation).on(previsionAeOpeTra.idOpeOperation.eq(operation.idOpeOperation))
                .join(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                .join(budget).on(previsionAeOpeTra.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderOpe)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureDepense.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureDepense.libelle.as("NATURE"), 
                		previsionAeOpeTra.montantAe.sum().coalesce(BigDecimal.ZERO).as("MONTANT_AE"),
                		previsionCpOpeTra.montantCp.sum().coalesce(BigDecimal.ZERO).as("MONTANT_CP"));     

        ListSubQuery<Tuple> sqlQueryDep = new SQLSubQuery().from(prevision)
                .join(eb).on(prevision.idAdmEb.eq(eb.idAdmEb))
                .join(enveloppe).on(prevision.idBudEnveloppe.eq(enveloppe.idBudEnveloppe))
                .join(natureDepense).on(prevision.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense).on(prevision.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                .join(budget).on(prevision.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderDep)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureDepense.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureDepense.libelle.as("NATURE"), 
                		prevision.montantAe.sum().as("MONTANT_AE"),
                		prevision.montantCp.sum().as("MONTANT_CP"));
        
        SQLQuery sqlQuery = template.newSqlQuery().from(new SQLSubQuery()
        .union(sqlQueryDep, sqlQueryOpe).as(sousReqPath))
        .groupBy(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqNaturePath)
        .orderBy(sousReqOrgEbPath.asc(), sousReqOrgUbPath.asc(), sousReqOrgCrPath.asc(), 
        		sousReqOrgSouCrPath.asc(), sousReqOrgLibPath.asc(), sousReqNaturePath.asc());
        sqlQuery.setUseLiterals(true);
        String query = sqlQuery
                		.getSQL(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqNaturePath, 
        		sousReqMontantAePath.sum().as("MONTANT_AE"),
        		sousReqMontantCpPath.sum().as("MONTANT_CP")).getSQL();

        return query;
    }

	public String getPrevisionsFiltreByDestinationQuery(int exercice,
			BudgetId budgetId, List<Long> ebId) {
		// TODO Auto-generated method stub
        BooleanBuilder whereBuilderDep = new BooleanBuilder();
        BooleanBuilder whereBuilderOpe = new BooleanBuilder();
        whereBuilderDep.and(prevision.idBudBudget.eq(budgetId.getId()));
        whereBuilderDep.and(budget.exeOrdre.eq(exercice));
        whereBuilderOpe.and(previsionAeOpeTra.idBudBudget.eq(budgetId.getId()));
        whereBuilderOpe.and(budget.exeOrdre.eq(exercice));
        if(ebId != null) {
        	whereBuilderDep.and(prevision.idAdmEb.in(ebId));
        	whereBuilderOpe.and(previsionAeOpeTra.idAdmEb.in(ebId));
        }
        ListSubQuery<Tuple> sqlQueryOpe = new SQLSubQuery().from(previsionAeOpeTra)
                .fullJoin(previsionCpOpeTra).on(previsionAeOpeTra.idBudBudget.eq(previsionCpOpeTra.idBudBudget),
                        previsionAeOpeTra.idAdmEb.eq(previsionCpOpeTra.idAdmEb),
                        previsionAeOpeTra.idBudEnveloppe.eq(previsionCpOpeTra.idBudEnveloppe),
                        previsionAeOpeTra.idAdmNatureDep.eq(previsionCpOpeTra.idAdmNatureDep),
                        previsionAeOpeTra.idAdmDestinationDepense.eq(previsionCpOpeTra.idAdmDestinationDepense),
                        previsionAeOpeTra.idOpeOperation.eq(previsionCpOpeTra.idOpeOperation))
        		.join(eb).on(previsionAeOpeTra.idAdmEb.eq(eb.idAdmEb))
                .join(enveloppe).on(previsionAeOpeTra.idBudEnveloppe.eq(enveloppe.idBudEnveloppe))
                .join(natureDepense).on(previsionAeOpeTra.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense).on(previsionAeOpeTra.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                .join(operation).on(previsionAeOpeTra.idOpeOperation.eq(operation.idOpeOperation))
                .join(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                .join(budget).on(previsionAeOpeTra.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderOpe)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, destinationDepense.libelle, 
                		natureDepense.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, destinationDepense.libelle.as("DESTINATION"), 
                		natureDepense.libelle.as("NATURE"), 
                		previsionAeOpeTra.montantAe.sum().coalesce(BigDecimal.ZERO).as("MONTANT_AE"),
                		previsionCpOpeTra.montantCp.sum().coalesce(BigDecimal.ZERO).as("MONTANT_CP"));
        
        ListSubQuery<Tuple> sqlQueryDep = new SQLSubQuery().from(prevision)
                .join(eb).on(prevision.idAdmEb.eq(eb.idAdmEb))
                .join(enveloppe).on(prevision.idBudEnveloppe.eq(enveloppe.idBudEnveloppe))
                .join(natureDepense).on(prevision.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense).on(prevision.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                .join(budget).on(prevision.idBudBudget.eq(budget.idBudBudget))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue()))
                .on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderDep)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, destinationDepense.libelle, 
                		natureDepense.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, destinationDepense.libelle.as("DESTINATION"), 
                		natureDepense.libelle.as("NATURE"), 
                		prevision.montantAe.sum().as("MONTANT_AE"),
                		prevision.montantCp.sum().as("MONTANT_CP"));
        
        SQLQuery sqlQuery = template.newSqlQuery().from(new SQLSubQuery()
        .union(sqlQueryDep, sqlQueryOpe).as(sousReqPath))
        .groupBy(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqDestinationPath, 
        		sousReqNaturePath)
        .orderBy(sousReqOrgEbPath.asc(), sousReqOrgUbPath.asc(), sousReqOrgCrPath.asc(), 
        		sousReqOrgSouCrPath.asc(), sousReqOrgLibPath.asc(), sousReqDestinationPath.asc(), 
        		sousReqNaturePath.asc());
        sqlQuery.setUseLiterals(true);
        String query = sqlQuery
                		.getSQL(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqDestinationPath, sousReqNaturePath, 
        		sousReqMontantAePath.sum().as("MONTANT_AE"),
        		sousReqMontantCpPath.sum().as("MONTANT_CP")).getSQL();
		return query;
	}

}
