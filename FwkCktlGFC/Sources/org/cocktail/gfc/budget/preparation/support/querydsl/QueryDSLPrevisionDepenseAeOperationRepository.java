/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.CombinaisonAxes;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseAeOperation;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseId;
import org.cocktail.gfc.budget.preparation.support.PrevisionDepenseAeOperationRepository;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TrancheDepenseAeId;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepAe;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepAe;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlDeleteCallback;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.sql.dml.SQLDeleteClause;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.types.Path;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.query.ListSubQuery;

/**
 * @author bourges
 *
 */
public class QueryDSLPrevisionDepenseAeOperationRepository extends GfcRepository implements PrevisionDepenseAeOperationRepository {
    
    private QueryDslJdbcTemplate template;
    
    private QBudPrevOpeTraDepAe prevOpeTraDepAe = QBudPrevOpeTraDepAe.budPrevOpeTraDepAe;
    Path<?>[] colsForInsert;

    public QueryDSLPrevisionDepenseAeOperationRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
        @SuppressWarnings("unchecked")
        List<Path<?>> cols = new ArrayList<Path<?>>(Arrays.asList(
                prevOpeTraDepAe.idBudPrevOpeTraDepAe,
                prevOpeTraDepAe.commentaire,
                prevOpeTraDepAe.dCreation,
                prevOpeTraDepAe.dModification,
                prevOpeTraDepAe.idAdmDestinationDepense,
                prevOpeTraDepAe.idAdmEb,
                prevOpeTraDepAe.idAdmNatureDep,
                prevOpeTraDepAe.idBudBudget,
                prevOpeTraDepAe.idBudEnveloppe,
                prevOpeTraDepAe.idOpeOperation,
                prevOpeTraDepAe.idOpeTrancheBudDepAe,
                prevOpeTraDepAe.montantAe,
                prevOpeTraDepAe.persIdCreation,
                prevOpeTraDepAe.persIdModification
                ));
         colsForInsert = cols.toArray(new Path[cols.size()]);
    }

    /**
     * @see org.cocktail.gfc.budget.preparation.support.PrevisionDepenseAeOperationRepository#createPrevisionDepenseOperationOnIntegrationTranche(org.cocktail.gfc.operation.TrancheId)
     */
    public void createPrevisionDepenseOperationOnIntegrationTranche(TrancheId trancheId, BudgetId budgetId, Long persId) {
        QOpeTrancheBudDepAe aeTranche = QOpeTrancheBudDepAe.opeTrancheBudDepAe;
        QOpeTrancheBud opeTrancheBud = QOpeTrancheBud.opeTrancheBud;
        SQLQuery query = template.newSqlQuery();
        query.from(aeTranche)
            .join(opeTrancheBud).on(opeTrancheBud.idOpeTrancheBud.eq(aeTranche.idOpeTrancheBud))
            .where(aeTranche.idOpeTrancheBud.eq(trancheId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                aeTranche.commentaire,
                aeTranche.idAdmDestinationDepense,
                aeTranche.idAdmEb,
                aeTranche.idAdmNatureDep,
                aeTranche.idBudEnveloppe,
                opeTrancheBud.idOpeOperation,
                aeTranche.idOpeTrancheBudDepAe,
                aeTranche.montantAe));
        for (Tuple row : result) {
            PrevisionDepenseAeOperation previsionDepenseOperation = PrevisionDepenseAeOperation.builder()
                    .commentaire(row.get(aeTranche.commentaire))
                    .combinaison(CombinaisonAxes.builder()
                            .destinationDepenseId(new DestinationDepenseId(row.get(aeTranche.idAdmDestinationDepense)))
                            .entiteBudgetaireId(new EntiteBudgetaireId(row.get(aeTranche.idAdmEb)))
                            .natureDepenseId(new NatureDepenseId(row.get(aeTranche.idAdmNatureDep)))
                            .enveloppeId(new EnveloppeBudgetaireId(row.get(aeTranche.idBudEnveloppe)))
                            .operationId(new OperationId(row.get(opeTrancheBud.idOpeOperation)))
                            .build())
                    .budgetId(budgetId)
                    .trancheDepenseAeId(new TrancheDepenseAeId(row.get(aeTranche.idOpeTrancheBudDepAe)))
                    .montantAE(new Montant(row.get(aeTranche.montantAe)))
                    .persIdCreation(persId)
                    .dateCreation(new Date())
                    .build();
            createPrevisionDepenseAeOperation(previsionDepenseOperation);
        }    
    }

    /**
     * @see org.cocktail.gfc.budget.preparation.support.PrevisionDepenseAeOperationRepository#createPrevisionDepenseAeOperation(org.cocktail.gfc.budget.preparation.PrevisionDepenseAeOperation)
     */
    public PrevisionDepenseAeOperation createPrevisionDepenseAeOperation(PrevisionDepenseAeOperation previsionDepenseAeOperation) {
        PrevisionDepenseAeOperation ret = previsionDepenseAeOperation;
        Long id = template.insertWithKey(prevOpeTraDepAe, createBudgetDepenseCallback(previsionDepenseAeOperation));
        ret.setPrevisionId(new PrevisionDepenseId(id));
        return ret;
    }

    public void removeByTrancheAndBudget(TrancheId trancheId, BudgetId budgetId) {
        template.delete(prevOpeTraDepAe, removeByTrancheAndBudgetCallBack(trancheId, budgetId));
        
    }

    private SqlDeleteCallback removeByTrancheAndBudgetCallBack(final TrancheId trancheId, final BudgetId budgetId) {
        return new SqlDeleteCallback() {
            
            public long doInSqlDeleteClause(SQLDeleteClause delete) {
            	QOpeTrancheBudDepAe opeTrancheBudDep = QOpeTrancheBudDepAe.opeTrancheBudDepAe;
                //on a besoin de supprimer tous les tuples liés à la Tranche via la table opeTrancheBudDep
                ListSubQuery<Long> sub = new SQLSubQuery()
                        .from(opeTrancheBudDep)
                        .where(opeTrancheBudDep.idOpeTrancheBud.eq(trancheId.id()))
                        .list(opeTrancheBudDep.idOpeTrancheBudDepAe);
                SQLDeleteClause deleteComplete = delete
                        .where(prevOpeTraDepAe.idBudBudget.eq(budgetId.getId())
                                .and(prevOpeTraDepAe.idOpeTrancheBudDepAe.in(sub)));
                return deleteComplete.execute();
            }
        };
    }

    private SqlInsertWithKeyCallback<Long> createBudgetDepenseCallback(final PrevisionDepenseAeOperation previsionDepenseOperation) {
        return new SqlInsertWithKeyCallback<Long>() {

            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(prevOpeTraDepAe));
                values.addAll(asList(previsionDepenseOperation));
                return insert.columns(colsForInsert).values(values.toArray()).executeWithKey(prevOpeTraDepAe.idBudPrevOpeTraDepAe);
            }

        };
    }

    private List<Object> asList(PrevisionDepenseAeOperation prev) {
        List<Object> ret = new ArrayList<Object>();
        ret.add(prev.getCommentaire());
        ret.add(prev.getDateCreation());
        ret.add(prev.getDateModification());
        ret.add(prev.getCombinaison().getDestinationDepenseId().getId());
        ret.add(prev.getCombinaison().getEntiteBudgetaireId().getId());
        ret.add(prev.getCombinaison().getNatureDepenseId().getId());
        ret.add(prev.getBudgetId().getId());
        ret.add(prev.getCombinaison().getEnveloppeId().getId());
        ret.add(prev.getCombinaison().getOperationId().getId());            
        ret.add(prev.getTrancheDepenseAeId().getId());
        ret.add(prev.getMontantAE().value());
        ret.add(prev.getPersIdCreation());
        ret.add(prev.getPersIdModification());
        return ret;
    }

}
