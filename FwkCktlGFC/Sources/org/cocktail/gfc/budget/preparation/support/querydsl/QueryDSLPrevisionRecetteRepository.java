/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLSectionRecetteCacheProjection;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.PrevisionRecetteHorsOp;
import org.cocktail.gfc.budget.preparation.PrevisionRecetteId;
import org.cocktail.gfc.budget.preparation.support.PrevisionRecetteRepository;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlDeleteCallback;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;
import org.springframework.data.jdbc.query.SqlUpdateCallback;

import com.google.common.base.Optional;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.dml.SQLDeleteClause;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.sql.dml.SQLUpdateClause;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Path;
import com.mysema.query.types.Projections;

public class QueryDSLPrevisionRecetteRepository extends GfcRepository implements PrevisionRecetteRepository {

    // Tables
    private QBudPrevHorsOpRec prevision = QBudPrevHorsOpRec.budPrevHorsOpRec;

    // Projections
    private QueryDSLSectionRecetteCacheProjection queryDSLSectionRecetteCacheProjection = new QueryDSLSectionRecetteCacheProjection(prevision.section);

    private ConstructorExpression<PrevisionRecetteHorsOp> constructeurPrevisionRecette = Projections
            .constructor(
                    PrevisionRecetteHorsOp.class,
                    prevision.idBudPrevHorsOpRec,
                    prevision.idBudBudget,
                    prevision.idAdmEb,
                    prevision.idAdmNatureRec,
                    prevision.idAdmOrigineRecette,
                    queryDSLSectionRecetteCacheProjection,
                    prevision.montantRec,
                    prevision.montantTitre,
                    prevision.commentaire,
                    prevision.persIdCreation,
                    prevision.dCreation,
                    prevision.persIdModification,
                    prevision.dModification
            );

    private QueryDslJdbcTemplate template;
    private Path<?>[] columnsForInsert;
    private Path<?>[] columnsForUpdate;

    public QueryDSLPrevisionRecetteRepository(QueryDslJdbcTemplate template) {
        this.template = template;
        initDMLColumns();
    }

    @SuppressWarnings("unchecked")
    private void initDMLColumns() {
        List<Path<?>> columnsExceptId = new ArrayList<Path<?>>(Arrays.asList(
                prevision.idBudBudget, prevision.idAdmEb,
                prevision.idAdmNatureRec, prevision.idAdmOrigineRecette,
                prevision.section, prevision.montantRec, prevision.montantTitre,
                prevision.commentaire, prevision.persIdCreation, prevision.dCreation,
                prevision.persIdModification, prevision.dModification));

        List<Path<?>> forInsert = new ArrayList<Path<?>>();
        forInsert.add(prevision.idBudPrevHorsOpRec);
        forInsert.addAll(columnsExceptId);

        List<Path<?>> forUpdate = new ArrayList<Path<?>>(columnsExceptId);

        this.columnsForInsert = forInsert.toArray(new Path[forInsert.size()]);
        this.columnsForUpdate = forUpdate.toArray(new Path[forUpdate.size()]);
    }

    public PrevisionRecetteHorsOp createPrevisionRecette(PrevisionRecetteHorsOp nvllePrevision) {
        Long newId = savePrevision(nvllePrevision);
        PrevisionRecetteId newPrevisionId = new PrevisionRecetteId(newId);
        nvllePrevision.setPrevisionId(newPrevisionId);

        return nvllePrevision;
    }

    public long updatePrevisionRecette(PrevisionRecetteHorsOp previsionMaj) {
        long nbPrevisionsUpdated = template.update(prevision, createUpdateCallback(previsionMaj));
        return nbPrevisionsUpdated;
    }

    public long remove(PrevisionRecetteId previsionRecetteId) {
        return template.delete(prevision, createDeleteCallback(previsionRecetteId));
    }

    public Optional<PrevisionRecetteHorsOp> findById(PrevisionRecetteId previsionRecetteId) {
        SQLQuery query = addByIdClause(getPrevisionQuery(),
                previsionRecetteId.getId());
        PrevisionRecetteHorsOp previsionRecette = template.queryForObject(query,
                constructeurPrevisionRecette);

        return Optional.of(previsionRecette);
    }

    public List<PrevisionRecetteHorsOp> findByBudgetId(BudgetId budgetId) {
        SQLQuery query = addByBudgetIdClause(getPrevisionQuery(),
                budgetId.getId());
        List<PrevisionRecetteHorsOp> previsions = getPrevisionsFromQuery(query);
        return previsions;
    }

    protected Long savePrevision(PrevisionRecetteHorsOp nvllePrevision) {
        return template.insertWithKey(prevision, createInsertCallback(nvllePrevision));
    }

    private SqlInsertWithKeyCallback<Long> createInsertCallback(final PrevisionRecetteHorsOp nvllePrevision) {
        return new SqlInsertWithKeyCallback<Long>() {
            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(prevision));
                values.addAll(previsionRecetteAsList(nvllePrevision));
                return insert.columns(columnsForInsert).values(values.toArray()).executeWithKey(prevision.idBudPrevHorsOpRec);
            }
        };
    }

    private SqlUpdateCallback createUpdateCallback(final PrevisionRecetteHorsOp previsionMaj) {
        return new SqlUpdateCallback() {
            public long doInSqlUpdateClause(SQLUpdateClause update) {
                List<Path<?>> forUpdate = Arrays.asList(columnsForUpdate);
                List<Object> values = previsionRecetteAsList(previsionMaj);

                return update.set(forUpdate, values).where(prevision.idBudPrevHorsOpRec.eq(previsionMaj.getPrevisionId().getId())).execute();
            }
        };
    }

    private SqlDeleteCallback createDeleteCallback(final PrevisionRecetteId id) {
        return new SqlDeleteCallback() {
            public long doInSqlDeleteClause(SQLDeleteClause delete) {
                return delete.where(prevision.idBudPrevHorsOpRec.eq(id.getId())).execute();
            }
        };
    }

    // Queries
    private SQLQuery getPrevisionQuery() {
        SQLQuery sqlQuery = template.newSqlQuery().from(prevision);
        return sqlQuery;
    }

    private SQLQuery addByBudgetIdClause(SQLQuery sqlQuery, long id) {
        sqlQuery = sqlQuery.where(prevision.idBudBudget.eq(id));
        return sqlQuery;
    }

    private SQLQuery addByIdClause(SQLQuery sqlQuery, long id) {
        sqlQuery = sqlQuery.where(prevision.idBudPrevHorsOpRec.eq(id));
        return sqlQuery;
    }

    private List<PrevisionRecetteHorsOp> getPrevisionsFromQuery(SQLQuery sqlQuery) {
        List<PrevisionRecetteHorsOp> res = template.query(sqlQuery, constructeurPrevisionRecette);
        return res;
    }

    // Utils
    private List<Object> previsionRecetteAsList(PrevisionRecetteHorsOp previsionRecette) {
        List<Object> values = new ArrayList<Object>();
        values.add(previsionRecette.getBudgetId().getId());
        values.add(previsionRecette.getCombinaison().getEntiteBudgetaireId().getId());
        values.add(previsionRecette.getCombinaison().getNatureRecetteId().getId());
        values.add(previsionRecette.getCombinaison().getOrigineRecetteId().getId());
        values.add(previsionRecette.getCombinaison().getSectionRecette().getAbreviation());
        values.add(previsionRecette.getMontantBudgetaire().value());
        values.add(previsionRecette.getMontantTitre().value());
        values.add(previsionRecette.getCommentaire());
        values.add(previsionRecette.getPersIdCreation());
        values.add(previsionRecette.getDateCreation());
        values.add(previsionRecette.getPersIdModification());
        values.add(previsionRecette.getDateModification());
        return values;
    }

}
