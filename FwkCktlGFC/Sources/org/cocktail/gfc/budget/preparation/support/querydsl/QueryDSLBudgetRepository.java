/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.gfc.budget.preparation.Budget;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.CalendrierBudget;
import org.cocktail.gfc.budget.preparation.EtatBudget;
import org.cocktail.gfc.budget.preparation.VersionBudget;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.support.exception.GfcValidationException;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudEtatBudget;
import org.cocktail.gfc.support.querydsl.QBudVersionBudget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;
import org.springframework.data.jdbc.query.SqlUpdateCallback;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.sql.dml.SQLUpdateClause;
import com.mysema.query.types.MappingProjection;
import com.mysema.query.types.Path;

public class QueryDSLBudgetRepository extends GfcRepository implements BudgetRepository {

    @Autowired
    private QueryDslJdbcTemplate template;

    // Tables
    private QBudBudget budget = new QBudBudget("bud");
    private QBudVersionBudget versionBudget = new QBudVersionBudget("vbud");
    private QBudEtatBudget etatBudget = new QBudEtatBudget("etatbud");

    private QueryDSLBudgetProjection projection = new QueryDSLBudgetProjection(budget, versionBudget, etatBudget);

    public List<Budget> findAll() {
        SQLQuery query = getBudgetQuery().orderBy(budget.exeOrdre.asc(), budget.numeroBudget.asc());
        return getBudgetsFromQuery(query);
    }

    public List<Budget> findByExercice(int exercice) {
        SQLQuery query = addByExerciceClause(getBudgetQuery(), exercice).orderBy(budget.numeroBudget.asc());
        return getBudgetsFromQuery(query);
    }

    public Budget findById(BudgetId budgetId) {
        return findById(budgetId.getId());
    }

    public Budget findById(long id) {
        SQLQuery query = addByIdClause(getBudgetQuery(), id);
        List<Budget> budgets = getBudgetsFromQuery(query);
        if (budgets.isEmpty()) {
            return null;
        }
        return budgets.get(0);
    }

    protected boolean existsForExercice(int exercice) {
        SQLQuery query = addByExerciceClause(getBudgetQuery(), exercice);
        return template.exists(query);
    }

    public long countByExercice(int exercice) {
        SQLQuery query = addByExerciceClause(getBudgetQuery(), exercice);
        return template.count(query);
    }

    public long countByExerciceAndVersion(int exercice, String codeVersion) {
        SQLQuery query = addByExerciceClause(getBudgetQuery(), exercice);
        query = addByVersionCodeClause(query, codeVersion);
        return template.count(query);
    }

    public Budget create(Budget newBudget) {
        Long newId = executeInsert(newBudget);
        newBudget.setIdBudget(new BudgetId(newId));
        return newBudget;
    }

    public long getMaxNumeroByExercice(int exercice) {
        long numeroMax = 0;
        if (existsForExercice(exercice)) {
            SQLQuery query = addByExerciceClause(getBudgetQuery(), exercice);
            numeroMax = template.queryForObject(query, budget.numeroBudget.max());
        }
        return numeroMax;
    }

    public void enregistrerBudget(Budget bud, Long persIdModification) {
        BudgetId budgetId = bud.getIdBudget();

        Path<?>[] fields = {
                budget.llBudget, budget.exeOrdre, budget.numeroBudget, budget.persIdCreation,
                budget.dCreation, budget.codeEtatBudget, budget.dVote, budget.dEnvoi,
                budget.dNonApprobation, budget.dApprobation, budget.dExecution, budget.dModification, budget.persIdModification
        };
        Object[] values = {
                bud.getLibelle(), bud.getExeOrdre(), bud.getNumeroBudget(), bud.getPersIdCreateur(),
                bud.getDateCreation(), bud.getEtatBudget().getCode(),
                bud.getCalendrierBudget().getDateVoteToString(),
                bud.getCalendrierBudget().getDateEnvoiToString(),
                bud.getCalendrierBudget().getDateNonApprobationToString(),
                bud.getCalendrierBudget().getDateApprobationToString(),
                bud.getCalendrierBudget().getDateExecutionToString(),
                new Date(),
                persIdModification
        };

        updateBudget(budgetId, fields, values);
    }

    public long updateBudget(BudgetId budgetId, Path<?>[] fields, Object[] values) {
        long nbPrevisionsUpdated = template.update(budget, createUpdateCallback(budgetId.getId(), fields, values));
        return nbPrevisionsUpdated;
    }

    // Queries
    private SqlUpdateCallback createUpdateCallback(final Long budgetId, final Path<?>[] fields, final Object[] values) {
        return new SqlUpdateCallback() {
            public long doInSqlUpdateClause(SQLUpdateClause update) {
                List<Path<?>> colonnes = new ArrayList<Path<?>>(Arrays.asList(fields));
                List<Object> valeurs = new ArrayList<Object>(Arrays.asList(values));
                return update.set(colonnes, valeurs).where(budget.idBudBudget.eq(budgetId)).execute();
            }
        };
    }

    private SQLQuery getBudgetQuery() {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(budget)
                .innerJoin(versionBudget).on(budget.idBudVersionBudget.eq(versionBudget.idBudVersionBudget))
                .innerJoin(etatBudget).on(budget.codeEtatBudget.eq(etatBudget.code));
        return sqlQuery;
    }

    private SQLQuery addByExerciceClause(SQLQuery sqlQuery, int exercice) {
        sqlQuery = sqlQuery.where(budget.exeOrdre.eq(exercice));
        return sqlQuery;
    }

    private SQLQuery addByIdClause(SQLQuery sqlQuery, long id) {
        sqlQuery = sqlQuery.where(budget.idBudBudget.eq(id));
        return sqlQuery;
    }

    private SQLQuery addByVersionCodeClause(SQLQuery sqlQuery, String codeVersion) {
        sqlQuery = sqlQuery.where(versionBudget.code.eq(codeVersion));
        return sqlQuery;
    }

    private Long executeInsert(final Budget nouveauBudget) {
        return template.insertWithKey(budget, new SqlInsertWithKeyCallback<Long>() {
            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                return insert
                        .columns(budget.idBudBudget, budget.exeOrdre, budget.persIdCreation, budget.dCreation, budget.llBudget, budget.numeroBudget,
                                budget.codeEtatBudget, budget.idBudVersionBudget)
                        .values(nextval(budget),
                                nouveauBudget.getExeOrdre(), // exeOrdre
                                nouveauBudget.getPersIdCreateur(), // persIdCreation
                                nouveauBudget.getDateCreation(), // dateCreation
                                nouveauBudget.getLibelle(), // llBudget
                                nouveauBudget.getNumeroBudget(), // numeroBudget
                                nouveauBudget.getEtatBudget().getCode(), // codeEtatBudget
                                nouveauBudget.getVersionBudget().getId() // idVersionBudget
                        )
                        .executeWithKey(budget.idBudBudget);
            }
        });
    }

    private List<Budget> getBudgetsFromQuery(SQLQuery sqlQuery) {
        List<Budget> res = template.query(sqlQuery, projection);
        return res;
    }

    public class QueryDSLBudgetProjection extends MappingProjection<Budget> {

        private static final long serialVersionUID = 1L;
        private QBudBudget budget;
        private QBudVersionBudget versionBudget;
        private QBudEtatBudget etatBudget;

        public QueryDSLBudgetProjection(QBudBudget budget, QBudVersionBudget versionBudget, QBudEtatBudget etatBudget) {
            super(Budget.class, budget.idBudBudget, budget.llBudget, budget.exeOrdre,
                    budget.numeroBudget, budget.persIdCreation, budget.dCreation,
                    budget.persIdModification, budget.dModification,
                    etatBudget.idBudEtatBudget, etatBudget.code, etatBudget.libelle,
                    versionBudget.idBudVersionBudget, versionBudget.code, versionBudget.libelle,
                    budget.dVote, budget.dEnvoi, budget.dNonApprobation,
                    budget.dApprobation, budget.dExecution);
            this.budget = budget;
            this.versionBudget = versionBudget;
            this.etatBudget = etatBudget;
        }

        @Override
        protected Budget map(Tuple row) {
            EtatBudget etatBudgetBean = EtatBudget.builder()
                    .id(row.get(etatBudget.idBudEtatBudget))
                    .code(row.get(etatBudget.code))
                    .libelle(row.get(etatBudget.libelle))
                    .build();
            VersionBudget versionBudgetBean = VersionBudget.builder()
                    .id(row.get(versionBudget.idBudVersionBudget))
                    .code(row.get(versionBudget.code))
                    .libelle(row.get(versionBudget.libelle))
                    .build();

            CalendrierBudget calendrierBudgetBean;
            try {
                calendrierBudgetBean = new CalendrierBudget(row.get(budget.dVote), row.get(budget.dEnvoi),
                        row.get(budget.dNonApprobation), row.get(budget.dApprobation), row.get(budget.dExecution));
            } catch (GfcValidationException e) {
                // FIXME RPR 27/03/2015 : un peu violent, voir comment faire
                // pour gérer les erreurs de mapping
                throw new RuntimeException("Erreur lors de la construction du mapping d'un objet Budget :" + e.getMessage());
            }

            return Budget.builder()
                    .idBudget(new BudgetId(row.get(budget.idBudBudget)))
                    .libelle(row.get(budget.llBudget))
                    .exeOrdre(row.get(budget.exeOrdre))
                    .numeroBudget(row.get(budget.numeroBudget))
                    .persIdCreateur(row.get(budget.persIdCreation))
                    .dateCreation(row.get(budget.dCreation))
                    .persIdModification(row.get(budget.persIdModification))
                    .dateModification(row.get(budget.dModification))
                    .etatBudget(etatBudgetBean)
                    .versionBudget(versionBudgetBean)
                    .calendrierBudget(calendrierBudgetBean)
                    .build();
        }

    }

}