/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.util.List;

import org.cocktail.gfc.budget.preparation.EtatBudget;
import org.cocktail.gfc.budget.preparation.EtatBudgetCode;
import org.cocktail.gfc.budget.preparation.support.EtatBudgetRepository;
import org.cocktail.gfc.support.querydsl.QBudEtatBudget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;

public class QueryDSLEtatBudgetRepository implements EtatBudgetRepository {

	@Autowired
	private QueryDslJdbcTemplate template;
	
	private static final QBudEtatBudget ETAT_BUDGET = QBudEtatBudget.budEtatBudget;
	private static final ConstructorExpression<EtatBudget> FACTORY_ETAT_BUDGET = ConstructorExpression.create(EtatBudget.class, ETAT_BUDGET.idBudEtatBudget, ETAT_BUDGET.code, ETAT_BUDGET.libelle);
	
	public List<EtatBudget> findAll() {
		SQLQuery query = getEtatBudgetQuery();
		return template.query(query, FACTORY_ETAT_BUDGET);
	}

	public EtatBudget findByCode(EtatBudgetCode code) {
		SQLQuery query = getEtatBudgetQuery();
		query = addByEtatBudgetCodeClause(query, code);
		return template.queryForObject(query, FACTORY_ETAT_BUDGET);
	}
	
	private SQLQuery getEtatBudgetQuery() {
		return template.newSqlQuery().from(ETAT_BUDGET);
	}
	
	private SQLQuery addByEtatBudgetCodeClause(SQLQuery sqlQuery, EtatBudgetCode code) {
		sqlQuery = sqlQuery.where(ETAT_BUDGET.code.eq(code.toString()));
		return sqlQuery;
	}
}
