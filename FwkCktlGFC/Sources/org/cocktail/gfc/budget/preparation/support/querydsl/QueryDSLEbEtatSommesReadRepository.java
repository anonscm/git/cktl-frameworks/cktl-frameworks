/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import static com.mysema.query.support.Expressions.as;
import static com.mysema.query.support.Expressions.numberPath;
import static com.mysema.query.support.Expressions.stringPath;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.read.EbEtatSommesRead;
import org.cocktail.gfc.budget.preparation.support.EbEtatSommesReadRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudBudgetEbEtat;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpDep;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpRec;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepAe;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepCp;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraRec;
import org.cocktail.gfc.support.querydsl.QVAdmEtabForEb;
import org.cocktail.gfc.support.querydsl.QVBudPrevision;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;
import com.mysema.query.types.Path;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.SimplePath;
import com.mysema.query.types.path.StringPath;
import com.mysema.query.types.query.ListSubQuery;

public class QueryDSLEbEtatSommesReadRepository implements EbEtatSommesReadRepository {

    // Tables
	private final QVBudPrevision prevision = QVBudPrevision.vBudPrevision;
    private final QBudPrevHorsOpDep previsionDep = QBudPrevHorsOpDep.budPrevHorsOpDep;
    private final QBudPrevOpeTraDepAe previsionAeOpeTra = QBudPrevOpeTraDepAe.budPrevOpeTraDepAe;
    private final QBudPrevOpeTraDepCp previsionCpOpeTra = QBudPrevOpeTraDepCp.budPrevOpeTraDepCp;
    private final QBudPrevHorsOpRec previsionRec = QBudPrevHorsOpRec.budPrevHorsOpRec;
    private final QBudPrevOpeTraRec previsionRecOpeTra = QBudPrevOpeTraRec.budPrevOpeTraRec;
    private final QAdmEb eb = QAdmEb.admEb;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = QAdmEbExer.admEbExer;
    private final QBudBudgetEbEtat budEbEtat = QBudBudgetEbEtat.budBudgetEbEtat;
    private final QVAdmEtabForEb etabForEb = QVAdmEtabForEb.vAdmEtabForEb;

    private QueryDslJdbcTemplate template;

    // Paths
    private NumberPath<Long> idBudPath = Expressions.numberPath(Long.class, "id_bud");
    private NumberPath<Long> idEbPath = Expressions.numberPath(Long.class, "id_eb");    
    private NumberPath<BigDecimal> montantAEPath = Expressions.numberPath(BigDecimal.class, "ae");
    private NumberPath<BigDecimal> montantCPPath = Expressions.numberPath(BigDecimal.class, "cp");
    private NumberPath<BigDecimal> montantBudgetairePath = Expressions.numberPath(BigDecimal.class, "mb");    
    private NumberPath<Integer> exeOrdrePath = Expressions.numberPath(Integer.class, "exe"); 
    private NumberPath<Integer> saisissablePath = Expressions.numberPath(Integer.class, "sais");
    private StringPath orgLib = Expressions.stringPath("orgLib");
    private StringPath orgUniv = Expressions.stringPath("orgUniv");
    private StringPath orgEtab = Expressions.stringPath("orgEtab");
    private StringPath orgUb = Expressions.stringPath("orgUb");
    private StringPath orgCr = Expressions.stringPath("orgCr");
    private StringPath orgSouscr = Expressions.stringPath("orgSouscr"); 
    
    private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "sr");
    private NumberPath<Long> sousReqIdBudPath = numberPath(Long.class, sousReqPath, "id_bud");
    private NumberPath<Long> sousReqIdEbPath = numberPath(Long.class, sousReqPath, "id_eb");
    private NumberPath<BigDecimal> sousReqMontantAEPath = numberPath(BigDecimal.class, sousReqPath, "ae");
    private NumberPath<BigDecimal> sousReqMontantCPPath = numberPath(BigDecimal.class, sousReqPath, "cp");
    private NumberPath<BigDecimal> sousReqMontantBudgetairePath = numberPath(BigDecimal.class, sousReqPath, "mb");
    private NumberPath<Integer> sousReqExeOrdrePath = numberPath(Integer.class, sousReqPath, "exe");
    private NumberPath<Integer> sousReqSaisissablePath = numberPath(Integer.class, sousReqPath, "sais");
    private StringPath sousReqOrgLib = stringPath(sousReqPath, "orgLib");
    private StringPath sousReqOrgUniv = stringPath(sousReqPath, "orgUniv");
    private StringPath sousReqOrgEtab = stringPath(sousReqPath, "orgEtab");
    private StringPath sousReqOrgUb = stringPath(sousReqPath, "orgUb");
    private StringPath sousReqOrgCr = stringPath(sousReqPath, "orgCr");
    private StringPath sousReqOrgSouscr = stringPath(sousReqPath, "orgSouscr");     

    // Paths Etablissement
    private Path<Long> etabIdPath = new PathBuilder<Long>(Long.class, "id_etab");
    private Path<Long> etabIdPerePath = new PathBuilder<Long>(Long.class, "etab_id_pere");
    private Path<Integer> etabNiveauPath = new PathBuilder<Integer>(Integer.class, "etab_niveau");
    private Path<String> etabUnivPath = new PathBuilder<String>(String.class, "etab_univ");
    private Path<String> etabEtablissementPath = new PathBuilder<String>(String.class, "etab_etab");
    private Path<String> etabUbPath = new PathBuilder<String>(String.class, "etab_ub");
    private Path<String> etabCrPath = new PathBuilder<String>(String.class, "etab_cr");
    private Path<String> etabSousCrPath = new PathBuilder<String>(String.class, "etab_sous_cr");
    private Path<String> etabLibellePath = new PathBuilder<String>(String.class, "etab_libelle");
    private Path<Integer> etabSaisissablePath = new PathBuilder<Integer>(Integer.class, "etab_saisissable");
    private PathBuilder<Long> subEtablissementsIdPath = new PathBuilder<Long>(Long.class, "subEtablissementsId");
    private SimplePath<Long> subEtabIdFullPath = subEtablissementsIdPath.get(etabIdPath);
    private SimplePath<Long> subEtabIdPereFullPath = subEtablissementsIdPath.get(etabIdPerePath);
    private SimplePath<Integer> subEtabNiveauFullPath = subEtablissementsIdPath.get(etabNiveauPath);
    private SimplePath<String> subEtabUnivFullPath = subEtablissementsIdPath.get(etabUnivPath);
    private SimplePath<String> subEtabEtablissementFullPath = subEtablissementsIdPath.get(etabEtablissementPath);
    private SimplePath<String> subEtabUbFullPath = subEtablissementsIdPath.get(etabUbPath);
    private SimplePath<String> subEtabCrFullPath = subEtablissementsIdPath.get(etabCrPath);
    private SimplePath<String> subEtabSousCrFullPath = subEtablissementsIdPath.get(etabSousCrPath);
    private SimplePath<String> subEtabLibelleFullPath = subEtablissementsIdPath.get(etabLibellePath);
    private SimplePath<Integer> subEtabSaisissableFullPath = subEtablissementsIdPath.get(etabSaisissablePath);
	private SimplePath[] etabGroupBy = new SimplePath[] {subEtabIdFullPath, subEtabIdPereFullPath,
        				subEtabNiveauFullPath, subEtabUnivFullPath, subEtabEtablissementFullPath,
        				subEtabUbFullPath, subEtabCrFullPath, subEtabSousCrFullPath, subEtabLibelleFullPath,
        				subEtabSaisissableFullPath};
	private NumberPath<Long> previsionEtabIdPath = numberPath(Long.class, "id_etab");
	private NumberPath<BigDecimal> previsionMtAePath = numberPath(BigDecimal.class, "montant_ae");
	private NumberPath<BigDecimal> previsionMtCpPath = numberPath(BigDecimal.class, "montant_cp");
	private NumberPath<BigDecimal> previsionMtBudgetairePath = numberPath(BigDecimal.class, "montant_budgetaire");
	private PathBuilder<Tuple> subPrevisionsPath = new PathBuilder<Tuple>(Tuple.class, "subPrevisions");
	private NumberPath<Long> subPrevisionEtabIdFullPath = subPrevisionsPath.get(previsionEtabIdPath);
	private NumberPath<BigDecimal> subPrevisionMtAeFullPath = subPrevisionsPath.get(previsionMtAePath);
	private NumberPath<BigDecimal> subPrevisionMtCpFullPath = subPrevisionsPath.get(previsionMtCpPath);
	private NumberPath<BigDecimal> subPrevisionMtBudgetaireFullPath = subPrevisionsPath.get(previsionMtBudgetairePath);
	
    // Projections
    private Expression<EbEtatSommesRead> budgetEBEtatRead = Projections.constructor(EbEtatSommesRead.class, 
            budEbEtat.idBudBudgetEbEtat,
            new TypeEtatProjection(),
            Projections.constructor(EntiteBudgetaire.class, 
                    sousReqIdEbPath, sousReqOrgUniv, sousReqOrgEtab, sousReqOrgUb, sousReqOrgCr, sousReqOrgSouscr, 
                    sousReqOrgLib, sousReqSaisissablePath),
            sousReqMontantAEPath, sousReqMontantCPPath, sousReqMontantBudgetairePath);
    
    
    public QueryDSLEbEtatSommesReadRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }
    
    // Queries
    public Optional<EbEtatSommesRead> queryByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId entiteBudgetaireId) {
        ListSubQuery<Tuple> depenseHorsOpeCumul = getDepenseHorsOpeTuples(
                addClauseHorsOpDepByEntiteBudgetaire(getDepenseAeOpeTuplesSubQuery(budgetId), entiteBudgetaireId));
        ListSubQuery<Tuple> depenseAeOpeCumul = getDepenseAeOpeTuples(
                addClauseTrancheOpeDepByEntiteBudgetaire(getDepenseAeOpeSubQuery(budgetId), entiteBudgetaireId));
        ListSubQuery<Tuple> depenseCpOpeCumul = getDepenseCpOpeTuples(
                addClauseTrancheOpeDepCpByEntiteBudgetaire(getDepenseCpOpeSubQuery(budgetId), entiteBudgetaireId));
        ListSubQuery<Tuple> recetteHorsOpeCumul = getRecetteHorsOpeTuples(
                addClauseHorsOpRecByEntiteBudgetaire(getRecetteHorsOpeSubQuery(budgetId), entiteBudgetaireId));
        ListSubQuery<Tuple> recetteOpeCumul = getRecetteOpeTuples(
                addClauseTrancheOpeRecByEntiteBudgetaire(getRecetteOpeSubQuery(budgetId), entiteBudgetaireId));
        ListSubQuery<Tuple> ebSaisissables = getMontantsEBSaisissablesTuples(
                addClauseEBSaisissablesByEntiteBudgetaire(getMontantsEBSaisissablesSubQuery(budgetId), entiteBudgetaireId), budgetId);
        SQLSubQuery unionCumul = new SQLSubQuery().from(new SQLSubQuery()
                .unionAll(depenseHorsOpeCumul, depenseAeOpeCumul, depenseCpOpeCumul, recetteHorsOpeCumul, recetteOpeCumul,
                        ebSaisissables)
                .as(sousReqPath));
        SQLQuery query = addEbInfoToSommes(unionCumul);
        return Optional.fromNullable(template.queryForObject(query, budgetEBEtatRead));
    }

    public List<EbEtatSommesRead> queryByBudgetIdAndListEntiteBudgetaireId(BudgetId budgetId, List<EntiteBudgetaireId> listeEbId) {
        ListSubQuery<Tuple> depenseHorsOpeCumul = getDepenseHorsOpeTuples(
                addClauseDepHorsOpeByEntiteBudgetaire(getDepenseAeOpeTuplesSubQuery(budgetId), listeEbId));
        ListSubQuery<Tuple> depenseAeOpeCumul = getDepenseAeOpeTuples(
                addClauseDepAeOpeByEntiteBudgetaire(getDepenseAeOpeSubQuery(budgetId), listeEbId));
        ListSubQuery<Tuple> depenseCpOpeCumul = getDepenseCpOpeTuples(
                addClauseDepCpOpeByEntiteBudgetaire(getDepenseCpOpeSubQuery(budgetId), listeEbId));
        ListSubQuery<Tuple> recetteHorsOpeCumul = getRecetteHorsOpeTuples(
                addClauseRecHorsOpeByEntiteBudgetaire(getRecetteHorsOpeSubQuery(budgetId), listeEbId));
        ListSubQuery<Tuple> recetteOpeCumul = getRecetteOpeTuples(
                addClauseRecOpeByEntiteBudgetaire(getRecetteOpeSubQuery(budgetId), listeEbId));
        ListSubQuery<Tuple> ebSaisissables = getMontantsEBSaisissablesTuples(
                addClauseMontantsEBSaisissablesByEntiteBudgetaire(getMontantsEBSaisissablesSubQuery(budgetId), listeEbId), budgetId);
        SQLSubQuery unionCumul = new SQLSubQuery().from(new SQLSubQuery()
                .unionAll(depenseHorsOpeCumul, depenseAeOpeCumul, depenseCpOpeCumul, recetteHorsOpeCumul, recetteOpeCumul,
                        ebSaisissables)
                .as(sousReqPath));
        SQLQuery query = addEbInfoToSommes(unionCumul);
        return template.query(query, budgetEBEtatRead);
    }

    public List<EbEtatSommesRead> queryByBudgetId(BudgetId budgetId) {
        ListSubQuery<Tuple> depenseHorsOpeCumul = getDepenseHorsOpeTuples(getDepenseAeOpeTuplesSubQuery(budgetId));
        ListSubQuery<Tuple> depenseAeOpeCumul = getDepenseAeOpeTuples(getDepenseAeOpeSubQuery(budgetId));
        ListSubQuery<Tuple> depenseCpOpeCumul = getDepenseCpOpeTuples(getDepenseCpOpeSubQuery(budgetId));
        ListSubQuery<Tuple> recetteHorsOpeCumul = getRecetteHorsOpeTuples(getRecetteHorsOpeSubQuery(budgetId));
        ListSubQuery<Tuple> recetteOpeCumul = getRecetteOpeTuples(getRecetteOpeSubQuery(budgetId));
        ListSubQuery<Tuple> ebSaisissables = getMontantsEBSaisissablesTuples(getMontantsEBSaisissablesSubQuery(budgetId), budgetId);
        SQLSubQuery unionCumul = new SQLSubQuery().from(new SQLSubQuery()
                .unionAll(depenseHorsOpeCumul, depenseAeOpeCumul, depenseCpOpeCumul, recetteHorsOpeCumul, recetteOpeCumul,
                        ebSaisissables)
                .as(sousReqPath));
        SQLQuery query = addEbInfoToSommes(unionCumul);
        return template.query(query, budgetEBEtatRead);
    }

    private SQLSubQuery getDepenseAeOpeTuplesSubQuery(BudgetId budgetId) {
        SQLSubQuery subQ = new SQLSubQuery().from(previsionDep)
                .groupBy(previsionDep.idBudBudget, previsionDep.idAdmEb)
                .where(previsionDep.idBudBudget.eq(budgetId.getId()));
        return subQ;
    }
    
    private SQLSubQuery addClauseHorsOpDepByEntiteBudgetaire(SQLSubQuery subQuery, EntiteBudgetaireId entiteBudgetaireId) {
        return subQuery.where(previsionDep.idAdmEb.eq(entiteBudgetaireId.getId()));
    }
    
    private SQLSubQuery addClauseDepHorsOpeByEntiteBudgetaire(SQLSubQuery subQuery, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
        for (EntiteBudgetaireId entiteBudgetaireEbId : listeEntiteBudgetaireId) {
            listeEntiteBudgetaireIdNumber.add(entiteBudgetaireEbId.getId());
        }
        return subQuery.where(previsionDep.idAdmEb.in(listeEntiteBudgetaireIdNumber));
    }
    
    private ListSubQuery<Tuple> getDepenseHorsOpeTuples(SQLSubQuery subQuery) {
        return subQuery.list(previsionDep.idBudBudget.as(idBudPath),
                previsionDep.idAdmEb.as(idEbPath),
                previsionDep.montantAe.sum().as(montantAEPath),
                previsionDep.montantCp.sum().as(montantCPPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantBudgetairePath));
    }
    
    private SQLSubQuery getDepenseAeOpeSubQuery(BudgetId budgetId) {
        SQLSubQuery subQ = new SQLSubQuery().from(previsionAeOpeTra)
                .groupBy(previsionAeOpeTra.idBudBudget, previsionAeOpeTra.idAdmEb)
                .where(previsionAeOpeTra.idBudBudget.eq(budgetId.getId()));
        return subQ;
    }
    
    private SQLSubQuery addClauseTrancheOpeDepByEntiteBudgetaire(SQLSubQuery subQuery, EntiteBudgetaireId entiteBudgetaireId) {
        return subQuery.where(previsionAeOpeTra.idAdmEb.eq(entiteBudgetaireId.getId()));
    }
    
    private SQLSubQuery addClauseDepAeOpeByEntiteBudgetaire(SQLSubQuery subQuery, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
        for (EntiteBudgetaireId entiteBudgetaireEbId : listeEntiteBudgetaireId) {
            listeEntiteBudgetaireIdNumber.add(entiteBudgetaireEbId.getId());
        }
        return subQuery.where(previsionAeOpeTra.idAdmEb.in(listeEntiteBudgetaireIdNumber));
    }
    
    private ListSubQuery<Tuple> getDepenseAeOpeTuples(SQLSubQuery subQuery) {
        return subQuery.list(previsionAeOpeTra.idBudBudget.as(idBudPath),
                previsionAeOpeTra.idAdmEb.as(idEbPath),
                previsionAeOpeTra.montantAe.sum().as(montantAEPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantCPPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantBudgetairePath));
    }
    
    private SQLSubQuery getDepenseCpOpeSubQuery(BudgetId budgetId) {
        SQLSubQuery subQ = new SQLSubQuery().from(previsionCpOpeTra)
                .groupBy(previsionCpOpeTra.idBudBudget, previsionCpOpeTra.idAdmEb)
                .where(previsionCpOpeTra.idBudBudget.eq(budgetId.getId()));
        return subQ;
    }
    
    private SQLSubQuery addClauseTrancheOpeDepCpByEntiteBudgetaire(SQLSubQuery subQuery, EntiteBudgetaireId entiteBudgetaireId) {
        return subQuery.where(previsionCpOpeTra.idAdmEb.eq(entiteBudgetaireId.getId()));
    }
    
    private SQLSubQuery addClauseDepCpOpeByEntiteBudgetaire(SQLSubQuery subQuery, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
        for (EntiteBudgetaireId entiteBudgetaireEbId : listeEntiteBudgetaireId) {
            listeEntiteBudgetaireIdNumber.add(entiteBudgetaireEbId.getId());
        }
        return subQuery.where(previsionCpOpeTra.idAdmEb.in(listeEntiteBudgetaireIdNumber));
    }
    
    private ListSubQuery<Tuple> getDepenseCpOpeTuples(SQLSubQuery subQuery) {
        return subQuery.list(previsionCpOpeTra.idBudBudget.as(idBudPath),
                previsionCpOpeTra.idAdmEb.as(idEbPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantAEPath),
                previsionCpOpeTra.montantCp.sum().as(montantCPPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantBudgetairePath));
    }
    
    private SQLSubQuery getRecetteHorsOpeSubQuery(BudgetId budgetId) {
        SQLSubQuery subQ = new SQLSubQuery().from(previsionRec)
                .groupBy(previsionRec.idBudBudget, previsionRec.idAdmEb)
                .where(previsionRec.idBudBudget.eq(budgetId.getId()));
        return subQ;
    }
    
    private SQLSubQuery addClauseHorsOpRecByEntiteBudgetaire(SQLSubQuery subQuery, EntiteBudgetaireId entiteBudgetaireId) {
        return subQuery.where(previsionRec.idAdmEb.eq(entiteBudgetaireId.getId()));
    }
    
    private SQLSubQuery addClauseRecHorsOpeByEntiteBudgetaire(SQLSubQuery subQuery, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
        for (EntiteBudgetaireId entiteBudgetaireEbId : listeEntiteBudgetaireId) {
            listeEntiteBudgetaireIdNumber.add(entiteBudgetaireEbId.getId());
        }
        return subQuery.where(previsionRec.idAdmEb.in(listeEntiteBudgetaireIdNumber));
    }
    
    private ListSubQuery<Tuple> getRecetteHorsOpeTuples(SQLSubQuery subQuery) {
        return subQuery.list(previsionRec.idBudBudget.as(idBudPath),
                previsionRec.idAdmEb.as(idEbPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantAEPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantCPPath),
                previsionRec.montantRec.sum().as(montantBudgetairePath));
    }
    
    private SQLSubQuery getRecetteOpeSubQuery(BudgetId budgetId) {
        SQLSubQuery subQ = new SQLSubQuery().from(previsionRecOpeTra)
                .groupBy(previsionRecOpeTra.idBudBudget, previsionRecOpeTra.idAdmEb)
                .where(previsionRecOpeTra.idBudBudget.eq(budgetId.getId()));
        return subQ;
    }
    
    private SQLSubQuery addClauseTrancheOpeRecByEntiteBudgetaire(SQLSubQuery subQuery, EntiteBudgetaireId entiteBudgetaireId) {
        return subQuery.where(previsionRecOpeTra.idAdmEb.eq(entiteBudgetaireId.getId()));
    }

    private SQLSubQuery addClauseRecOpeByEntiteBudgetaire(SQLSubQuery subQuery, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
        for (EntiteBudgetaireId entiteBudgetaireEbId : listeEntiteBudgetaireId) {
            listeEntiteBudgetaireIdNumber.add(entiteBudgetaireEbId.getId());
        }
        return subQuery.where(previsionRecOpeTra.idAdmEb.in(listeEntiteBudgetaireIdNumber));
    }
    
    private ListSubQuery<Tuple> getRecetteOpeTuples(SQLSubQuery subQuery) {
        return subQuery.list(previsionRecOpeTra.idBudBudget.as(idBudPath),
                previsionRecOpeTra.idAdmEb.as(idEbPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantAEPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantCPPath),
                previsionRecOpeTra.montantRec.sum().as(montantBudgetairePath));
    }
    
    private SQLSubQuery getMontantsEBSaisissablesSubQuery(BudgetId budgetId) {
        SQLSubQuery subQ = new SQLSubQuery().from(ebExer)
        .join(budget)
        .on(budget.idBudBudget.eq(budgetId.getId()).and(ebExer.exeOrdre.eq(budget.exeOrdre)))
        .where(ebExer.saisieBudgetaire.eq(1));
        return subQ;
    }
    
    private SQLSubQuery addClauseEBSaisissablesByEntiteBudgetaire(SQLSubQuery subQuery, EntiteBudgetaireId entiteBudgetaireId) {
        return subQuery.where(ebExer.idAdmEb.eq(entiteBudgetaireId.getId()));
    }
    
    private SQLSubQuery addClauseMontantsEBSaisissablesByEntiteBudgetaire(SQLSubQuery subQuery, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
        for (EntiteBudgetaireId entiteBudgetaireEbId : listeEntiteBudgetaireId) {
            listeEntiteBudgetaireIdNumber.add(entiteBudgetaireEbId.getId());
        }
        return subQuery.where(ebExer.idAdmEb.in(listeEntiteBudgetaireIdNumber));
    }
    
    private ListSubQuery<Tuple> getMontantsEBSaisissablesTuples(SQLSubQuery subQuery, BudgetId budgetId) {
        return subQuery.list(as(ConstantImpl.create(budgetId.getId()), idBudPath),
                ebExer.idAdmEb.as(idEbPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantAEPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantCPPath),
                as(ConstantImpl.create(BigDecimal.ZERO), montantBudgetairePath));
    }
    
    private SQLQuery addEbInfoToSommes(SQLSubQuery unionCumul) {
        ListSubQuery<Tuple> unionCumulTuples = unionCumul
                .groupBy(sousReqIdEbPath, sousReqIdBudPath)
                .list(sousReqIdBudPath.as(idBudPath), 
                        sousReqIdEbPath.as(idEbPath),
                        sousReqMontantAEPath.sum().as(montantAEPath),
                        sousReqMontantCPPath.sum().as(montantCPPath),
                        sousReqMontantBudgetairePath.sum().as(montantBudgetairePath));

        ListSubQuery<Tuple> joinBudgetEb = new SQLSubQuery().from(unionCumulTuples.as(sousReqPath))
                .join(budget)
                .on(budget.idBudBudget.eq(idBudPath))
                .join(eb)
                .on(eb.idAdmEb.eq(idEbPath))
                .list(sousReqIdBudPath.as(idBudPath), sousReqIdEbPath.as(idEbPath),
                        sousReqMontantAEPath.as(montantAEPath), sousReqMontantCPPath.as(montantCPPath), 
                        sousReqMontantBudgetairePath.as(montantBudgetairePath),
                        budget.exeOrdre.as(exeOrdrePath),
                        eb.orgLib.as(orgLib), eb.orgUniv.as(orgUniv), eb.orgEtab.as(orgEtab), eb.orgUb.as(orgUb),
                        eb.orgCr.as(orgCr), eb.orgSouscr.as(orgSouscr));
        
        ListSubQuery<Tuple> joinSaisissable = new SQLSubQuery().from(joinBudgetEb.as(sousReqPath))
                .leftJoin(ebExer)
                .on(ebExer.exeOrdre.eq(sousReqExeOrdrePath).and(ebExer.idAdmEb.eq(idEbPath)))
                .list(sousReqIdBudPath.as(idBudPath), sousReqIdEbPath.as(idEbPath),
                        sousReqMontantAEPath.as(montantAEPath), sousReqMontantCPPath.as(montantCPPath), 
                        sousReqMontantBudgetairePath.as(montantBudgetairePath),
                        sousReqExeOrdrePath.as(exeOrdrePath), 
                        sousReqOrgUniv.as(orgUniv), sousReqOrgEtab.as(orgEtab), sousReqOrgUb.as(orgUb), 
                        sousReqOrgCr.as(orgCr), sousReqOrgSouscr.as(orgSouscr),
                        sousReqOrgLib.as(orgLib), ebExer.saisieBudgetaire.as(saisissablePath));
        
        SQLQuery query = template.newSqlQuery().from(joinSaisissable.as(sousReqPath))
                .leftJoin(budEbEtat)
                .on(budEbEtat.idBudBudget.eq(sousReqIdBudPath).and(budEbEtat.idAdmEb.eq(sousReqIdEbPath)))
                .orderBy(sousReqIdEbPath.asc());
        
        return query;
    }
    
    public List<EbEtatSommesRead> synthesePreparationNiveauEtablissement(BudgetId budgetId) {
    	
    	SQLSubQuery subEtablissementsQuery = listerEtablissements(budgetId);
    	QTuple etablissementProjection = new QTuple(
    			eb.idAdmEb.as(etabIdPath),
    			eb.orgPere.as(etabIdPerePath),
    			eb.orgNiv.as(etabNiveauPath),
    			eb.orgUniv.as(etabUnivPath),
    			eb.orgEtab.as(etabEtablissementPath),
    			eb.orgUb.as(etabUbPath),
    			eb.orgCr.as(etabCrPath),
    			eb.orgSouscr.as(etabSousCrPath),
    			eb.orgLib.as(etabLibellePath),
    			ebExer.saisieBudgetaire.as(etabSaisissablePath));
    	
    	ListSubQuery<Tuple> selectSubEtablissementsIdExpr = subEtablissementsQuery.list(etablissementProjection);

    	
    	NumberPath<Long> previsionEtabIdPath = numberPath(Long.class, "id_etab");
    	NumberPath<BigDecimal> previsionMtAePath = numberPath(BigDecimal.class, "montant_ae");
    	NumberPath<BigDecimal> previsionMtCpPath = numberPath(BigDecimal.class, "montant_cp");
    	NumberPath<BigDecimal> previsionMtBudgetairePath = numberPath(BigDecimal.class, "montant_budgetaire");
    	QTuple previsionProjection = new QTuple(
    			etabForEb.idAdmEbEtab.as(previsionEtabIdPath),
    			prevision.montantAe.as(previsionMtAePath), 
    			prevision.montantCp.as(previsionMtCpPath),
    			prevision.montantBudgetaire.as(previsionMtBudgetairePath));
    	PathBuilder<Tuple> subPrevisionsPath = new PathBuilder<Tuple>(Tuple.class, "subPrevisions");
    	
    	SQLSubQuery subPrevisionsQuery = listerPrevisionsAvecEtablissement(budgetId);
    	ListSubQuery<Tuple> selectSubPrevisionsQuery = subPrevisionsQuery.list(previsionProjection);

    	NumberPath<Long> subPrevisionEtabIdFullPath = subPrevisionsPath.get(previsionEtabIdPath);
    	NumberPath<BigDecimal> subPrevisionMtAeFullPath = subPrevisionsPath.get(previsionMtAePath);
    	NumberPath<BigDecimal> subPrevisionMtCpFullPath = subPrevisionsPath.get(previsionMtCpPath);
    	NumberPath<BigDecimal> subPrevisionMtBudgetaireFullPath = subPrevisionsPath.get(previsionMtBudgetairePath);
    	SQLQuery synthesePreparationParEtablissementQuery = template.newSqlQuery()
    			.from(selectSubEtablissementsIdExpr, subEtablissementsIdPath)
    			.leftJoin(selectSubPrevisionsQuery, subPrevisionsPath)
    			.on(subPrevisionEtabIdFullPath.eq(subEtabIdFullPath))
    			.groupBy(etabGroupBy);
    	List<EbEtatSommesRead> synthesePreparationParEtablissement = template.query(
    			synthesePreparationParEtablissementQuery, 
    			new SynthesePreparationParEtablissementProjection(
        			    subEtabIdFullPath,
        			    subEtabIdPereFullPath,
        				subEtabNiveauFullPath,
        				subEtabUnivFullPath,
        				subEtabEtablissementFullPath,
        				subEtabUbFullPath,
        				subEtabCrFullPath,
        				subEtabSousCrFullPath,
        				subEtabLibelleFullPath,
        				subEtabSaisissableFullPath,
        				subPrevisionMtAeFullPath.sum(),
    	    			subPrevisionMtCpFullPath.sum(), 
    	    			subPrevisionMtBudgetaireFullPath.sum()));

    	return synthesePreparationParEtablissement;
    }
    
    private ListSubQuery<Tuple> synthesePreparationSelectSubEtablissementsIdExpr(BudgetId budgetId) {
    	SQLSubQuery subEtablissementsQuery = listerEtablissements(budgetId);
    	QTuple etablissementProjection = new QTuple(
    			eb.idAdmEb.as(etabIdPath),
    			eb.orgPere.as(etabIdPerePath),
    			eb.orgNiv.as(etabNiveauPath),
    			eb.orgUniv.as(etabUnivPath),
    			eb.orgEtab.as(etabEtablissementPath),
    			eb.orgUb.as(etabUbPath),
    			eb.orgCr.as(etabCrPath),
    			eb.orgSouscr.as(etabSousCrPath),
    			eb.orgLib.as(etabLibellePath),
    			ebExer.saisieBudgetaire.as(etabSaisissablePath));
    	
    	return subEtablissementsQuery.list(etablissementProjection);
    }
    
    private ListSubQuery<Tuple> synthesePreparationSelectSubPrevisionsQuery(BudgetId budgetId) {
    	QTuple previsionProjection = new QTuple(
    			etabForEb.idAdmEbEtab.as(previsionEtabIdPath),
    			prevision.montantAe.as(previsionMtAePath), 
    			prevision.montantCp.as(previsionMtCpPath),
    			prevision.montantBudgetaire.as(previsionMtBudgetairePath));
    	
    	SQLSubQuery subPrevisionsQuery = listerPrevisionsAvecEtablissement(budgetId);
    	return subPrevisionsQuery.list(previsionProjection);
    }
    
    private SQLQuery synthesePreparationQuery(ListSubQuery<Tuple> selectSubEtablissementsIdExpr,
    		ListSubQuery<Tuple> selectSubPrevisionsQuery) {
    	SQLQuery synthesePreparationParEtablissementQuery = template.newSqlQuery()
    			.from(selectSubEtablissementsIdExpr, subEtablissementsIdPath)
    			.leftJoin(selectSubPrevisionsQuery, subPrevisionsPath)
    			.on(subPrevisionEtabIdFullPath.eq(subEtabIdFullPath))
    			.groupBy(etabGroupBy);
    	
    	return synthesePreparationParEtablissementQuery;
    }
    
    //---
    // TODO FLA 09.03.2015 supprimer duplication de code
    public EbEtatSommesRead synthesePreparationNiveauEtablissement(BudgetId budgetId, EntiteBudgetaireId etablissementId) {
    	ListSubQuery<Tuple> selectSubEtablissementsIdExpr = synthesePreparationSelectSubEtablissementsIdExpr(budgetId);
    	ListSubQuery<Tuple> selectSubPrevisionsQuery = synthesePreparationSelectSubPrevisionsQuery(budgetId);
    	BooleanExpression etabRestriction = subEtabIdFullPath.eq(etablissementId.getId());

    	SQLQuery synthesePreparationParEtablissementQuery = synthesePreparationQuery(
    			selectSubEtablissementsIdExpr, selectSubPrevisionsQuery);
    	synthesePreparationParEtablissementQuery.where(etabRestriction);
    	
    	EbEtatSommesRead synthesePreparationParEtablissement = template.queryForObject(
    			synthesePreparationParEtablissementQuery, 
    			new SynthesePreparationParEtablissementProjection(
    			    subEtabIdFullPath,
    			    subEtabIdPereFullPath,
    				subEtabNiveauFullPath,
    				subEtabUnivFullPath,
    				subEtabEtablissementFullPath,
    				subEtabUbFullPath,
    				subEtabCrFullPath,
    				subEtabSousCrFullPath,
    				subEtabLibelleFullPath,
    				subEtabSaisissableFullPath,
	    			subPrevisionMtAeFullPath.sum(), 
	    			subPrevisionMtCpFullPath.sum(), 
	    			subPrevisionMtBudgetaireFullPath.sum()));

    	return synthesePreparationParEtablissement;
    }
    //---
    
    private SQLSubQuery listerEtablissements(BudgetId budgetId) {
    	BooleanExpression rattachementBudget = new SQLSubQuery()
    		.from(budget)
    		.where(budget.idBudBudget.eq(budgetId.getId())
				.and(ebExer.exeOrdre.eq(budget.exeOrdre)))
			.exists();
    	SQLSubQuery etablissementsQuery = new SQLSubQuery()
    		.from(eb)
    		.innerJoin(ebExer).on(ebExer.idAdmEb.eq(eb.idAdmEb))
    		.where(rattachementBudget.and(eb.orgNiv.eq(1)));
    	
    	return etablissementsQuery;
    }
    
    private SQLSubQuery listerPrevisionsAvecEtablissement(BudgetId budgetId) {
    	SQLSubQuery previsionsQuery = new SQLSubQuery()
    		.from(prevision)
    		.innerJoin(etabForEb).on(etabForEb.idAdmEb.eq(prevision.idEb))
    		.where(prevision.idBudget.eq(budgetId.getId()));
    	
    	return previsionsQuery;
    }
       
    private class SynthesePreparationParEtablissementProjection extends MappingProjection<EbEtatSommesRead> {

		private static final long serialVersionUID = 1L;

		private EntiteBudgetaireProjection ebProjection;
		private Expression<BigDecimal> montantAE;
		private Expression<BigDecimal> montantCP;
		private Expression<BigDecimal> montantRec;
		
		public SynthesePreparationParEtablissementProjection(Expression<Long> id, Expression<Long> idPere, Expression<Integer> niveau, 
				Expression<String> univ, Expression<String> etab, Expression<String> ub, Expression<String> cr, 
				Expression<String> sousCr, Expression<String> libelle, Expression<Integer> saisissable,
				Expression<BigDecimal> montantAE, Expression<BigDecimal> montantCP, Expression<BigDecimal> montantRec) {
			super(EbEtatSommesRead.class, id, idPere, niveau, etab, ub, cr, sousCr, libelle, saisissable, montantAE, montantCP, montantRec);
			this.ebProjection = new EntiteBudgetaireProjection(id, idPere, niveau, univ, etab, ub, cr, sousCr, libelle, saisissable);
			this.montantAE = montantAE;
			this.montantCP = montantCP;
			this.montantRec = montantRec;
		}

		@Override
		protected EbEtatSommesRead map(Tuple row) {
			// TODO 08.03.2015 : utiliser modelmapper ou passeren attributs de classes les Path necessaires a la conversion ou Projections.constructors
			return EbEtatSommesRead.builder()
					.budgetEbEtatIdNumber(null)
					.eb(ebProjection.map(row))
					.montantAE(coalesceToZero(row.get(montantAE)))
					.montantCP(coalesceToZero(row.get(montantCP)))
					.montantBudgetaire(coalesceToZero(row.get(montantRec)))
					.typeEtat(TypeEtatService.Code.NON_RENSEIGNEE.etat())
					.build();
		}
		
		private BigDecimal coalesceToZero(BigDecimal montant) {
			if (montant == null) {
				return BigDecimal.ZERO;
			}
			return montant;
		}
    }
    
    private class TypeEtatProjection extends MappingProjection<TypeEtat> {

		private static final long serialVersionUID = 1L;

		public TypeEtatProjection() {
			super(TypeEtat.class, budEbEtat.tyetId);
		}
    	
		@Override
		protected TypeEtat map(Tuple row) {
			Long typeEtatIdNumber = row.get(budEbEtat.tyetId);
			TypeEtat etat = TypeEtatService.Code.NON_RENSEIGNEE.etat();
			if (typeEtatIdNumber != null) {
				etat = TypeEtatService.getTypeEtatById(typeEtatIdNumber);
			}
			
			return etat;
		}
    	
    }
    
    private class EntiteBudgetaireProjection extends MappingProjection<EntiteBudgetaire> {

		private static final long serialVersionUID = 1L;
		
		private Expression<Long> id;
    	private Expression<Long> idPere;
    	private Expression<Integer> niveau;
    	private Expression<String> univ;
    	private Expression<String> etab;
    	private Expression<String> ub;
    	private Expression<String> cr;
    	private Expression<String> sousCr;
		private Expression<String> libelle;
		private Expression<Integer> saisissable; 
    	
		public EntiteBudgetaireProjection(Expression<Long> id, Expression<Long> idPere, Expression<Integer> niveau, 
				Expression<String> univ, Expression<String> etab, Expression<String> ub, Expression<String> cr, 
				Expression<String> sousCr, Expression<String> libelle, Expression<Integer> saisissable) {
			super(EntiteBudgetaire.class, id, idPere, niveau, etab, ub, cr, sousCr, libelle, saisissable);
			this.id = id;
			this.idPere = idPere;
			this.niveau = niveau;
			this.univ = univ;
			this.etab = etab;
			this.ub = ub;
			this.cr = cr;
			this.sousCr = sousCr;
			this.libelle = libelle;
			this.saisissable = saisissable;
		}

		@Override
		protected EntiteBudgetaire map(Tuple row) {
			return EntiteBudgetaire.builder()
					.id(row.get(id))
					.idPere(row.get(idPere))
					.niveau(row.get(niveau))
					.universite(row.get(univ))
					.etablissement(row.get(etab))
					.uniteBudgetaire(row.get(ub))
					.centreDeResponsabilite(row.get(cr))
					.sousCentreDeResponsabilite(row.get(sousCr))
					.libelle(row.get(libelle))
					.saisissable(isSaisissable(row.get(saisissable)))
					.build();
		}
		
		private Boolean isSaisissable(Integer saisissableFlag) {
			return Integer.valueOf(1).equals(saisissableFlag);
		}
    	
    }

}
