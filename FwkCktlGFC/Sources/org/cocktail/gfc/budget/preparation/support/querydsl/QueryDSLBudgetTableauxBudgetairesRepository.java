/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.support.BudgetTableauxBudgetairesRepository;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepExer;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QAdmNatureRec;
import org.cocktail.gfc.support.querydsl.QAdmOrigineRecette;
import org.cocktail.gfc.support.querydsl.QAdmTypeEtat;
import org.cocktail.gfc.support.querydsl.QBudBudgetDep;
import org.cocktail.gfc.support.querydsl.QBudBudgetRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Expression;

public class QueryDSLBudgetTableauxBudgetairesRepository implements BudgetTableauxBudgetairesRepository {

    // Tables
    private final QBudBudgetDep budgetDep = QBudBudgetDep.budBudgetDep;
    private final QAdmEb eb = QAdmEb.admEb;
    private final QAdmNatureDep natureDepense = QAdmNatureDep.admNatureDep;
    private final QAdmDestinationDepense destinationDepense = QAdmDestinationDepense.admDestinationDepense;
    private final QAdmDestinationDepExer destinationDepExer = QAdmDestinationDepExer.admDestinationDepExer;
    private final QBudBudgetRec budgetRec = QBudBudgetRec.budBudgetRec;
    private final QAdmNatureRec natureRecette = QAdmNatureRec.admNatureRec;
    private final QAdmOrigineRecette origineRecette = QAdmOrigineRecette.admOrigineRecette;
    private final QAdmTypeEtat typeEtat = QAdmTypeEtat.admTypeEtat;

    // Cases expressions
    Expression<String> typeRecette = typeEtat.tyetLibelle
            .when("FLECHEE").then("Fléchée")
            .when("GLOBALISEE").then("Globalisée")
            .otherwise(typeEtat.tyetLibelle);

    private QueryDslJdbcTemplate template;

    public QueryDSLBudgetTableauxBudgetairesRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
    }

    public String getAutorisationsBudgetairesDepensesByBudgetId(BudgetId budgetId) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(budgetDep)
                .join(eb).on(budgetDep.idAdmEb.eq(eb.idAdmEb))
                .join(natureDepense).on(budgetDep.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .where(budgetDep.idBudBudget.eq(budgetId.getId()))
                .orderBy(eb.orgEtab.asc(), natureDepense.libelle.asc())
                .groupBy(eb.orgEtab, natureDepense.libelle);

        sqlQuery.setUseLiterals(true);
        String query = sqlQuery.getSQL(eb.orgEtab, natureDepense.libelle, budgetDep.montantAe.sum().as("montantae"),
                budgetDep.montantCp.sum().as("montantcp")).getSQL();
        return query;

    }

    public String getAutorisationsBudgetairesRecettesByBudgetId(BudgetId budgetId) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(budgetRec)
                .join(eb).on(budgetRec.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(budgetRec.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(typeEtat).on(natureRecette.tyetId.eq(typeEtat.tyetId))
                .where(budgetRec.idBudBudget.eq(budgetId.getId()))
                .orderBy(eb.orgEtab.asc(), typeEtat.tyetLibelle.asc(), natureRecette.libelle.asc())
                .groupBy(eb.orgEtab, typeEtat.tyetLibelle, natureRecette.libelle);

        sqlQuery.setUseLiterals(true);
        String query = sqlQuery.getSQL(eb.orgEtab, Expressions.as(typeRecette, "code"), natureRecette.libelle,
                budgetRec.montantRec.sum().as("montantrec")).getSQL();
        return query;
    }

    public String getDepensesParDestinationsByBudgetId(BudgetId budgetId) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(budgetDep)
                .join(eb).on(budgetDep.idAdmEb.eq(eb.idAdmEb))
                .join(destinationDepense).on(budgetDep.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                .join(destinationDepExer).on(budgetDep.idAdmDestinationDepense.eq(destinationDepExer.idAdmDestinationDepense))
                .join(natureDepense).on(budgetDep.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .where(budgetDep.idBudBudget.eq(budgetId.getId())
                        .and(destinationDepExer.editionBudget.eq(1)))
                .orderBy(eb.orgEtab.asc(), destinationDepense.libelle.asc(), natureDepense.libelle.asc())
                .groupBy(eb.orgEtab, destinationDepense.libelle, natureDepense.libelle);

        sqlQuery.setUseLiterals(true);
        String query = sqlQuery.getSQL(eb.orgEtab, destinationDepense.libelle.as("destination"), natureDepense.libelle.as("nature"),
                budgetDep.montantAe.sum().as("montantae"), budgetDep.montantCp.sum().as("montantcp")).getSQL();
        return query;
    }

    public String getRecettesParOriginesByBudgetId(BudgetId budgetId) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(budgetRec)
                .join(eb).on(budgetRec.idAdmEb.eq(eb.idAdmEb))
                .join(origineRecette).on(budgetRec.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                .join(natureRecette).on(budgetRec.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(typeEtat).on(natureRecette.tyetId.eq(typeEtat.tyetId))
                .where(budgetRec.idBudBudget.eq(budgetId.getId()))
                .orderBy(eb.orgEtab.asc(), origineRecette.libelle.asc(), typeEtat.tyetLibelle.asc(), natureRecette.libelle.asc())
                .groupBy(eb.orgEtab, origineRecette.libelle, typeEtat.tyetLibelle, natureRecette.libelle);

        sqlQuery.setUseLiterals(true);
        String query = sqlQuery.getSQL(eb.orgEtab, origineRecette.libelle.as("origine"), Expressions.as(typeRecette, "code"),
                natureRecette.libelle.as("nature"), budgetRec.montantRec.sum().as("montantrec")).getSQL();
        return query;
    }
}
