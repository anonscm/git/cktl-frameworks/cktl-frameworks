/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support;

import org.cocktail.gfc.budget.preparation.BudgetFactory;
import org.cocktail.gfc.budget.preparation.CreditsService;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLBudgetRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLBudgetTableauxBudgetairesRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLCountRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLEbEtatReadRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLEbEtatRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLEbEtatSommesReadRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLEtatBudgetRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionDepenseAeOperationRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionDepenseCpOperationRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionDepenseReadRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionDepenseReportingRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionDepenseRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionOperationTrancheRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionRecetteOperationRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionRecetteReadRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionRecetteReportingRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionRecetteRepository;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLVersionBudgetRepository;
import org.cocktail.gfc.operation.controller.OperationFacade;
import org.cocktail.gfc.operation.support.SyntheseOperationRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

@Configuration
public class PreparationBudgetaireConfig {
    

    @Bean
    public PrevisionDepenseReportingRepository previsionDepenseReportingRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionDepenseReportingRepository(template);
    }

    @Bean
    public PrevisionRecetteReportingRepository previsionRecetteReportingRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionRecetteReportingRepository(template);
    }
    
    @Bean
    public CountRepository budPrepaCountRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLCountRepository(template);
    }
    
    @Bean
    public CreditsService budPrepaCreditsService(CountRepository countRepository) {
        return new CreditsService(countRepository);
    }

    @Bean
    public PrevisionRecetteOperationRepository previsionRecetteOperationRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionRecetteOperationRepository(template);
    }

    @Bean
    public PrevisionDepenseAeOperationRepository previsionDepenseAeOperationRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionDepenseAeOperationRepository(template);
    }
    
    @Bean
    public PrevisionDepenseCpOperationRepository previsionDepenseCpOperationRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionDepenseCpOperationRepository(template);
    }

    @Bean
    public BudgetRepository budgetRepository() {
        return new QueryDSLBudgetRepository();
    }

    @Bean
    public PrevisionDepenseRepository previsionDepenseRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionDepenseRepository(template);
    }

    @Bean
    public PrevisionDepenseReadRepository previsionDepenseReadRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionDepenseReadRepository(template);
    }

    @Bean
    public PrevisionRecetteRepository previsionRecetteRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionRecetteRepository(template);
    }

    @Bean
    public PrevisionRecetteReadRepository previsionRecetteReadRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionRecetteReadRepository(template);
    }

    @Bean
    public PrevisionOperationTrancheRepository previsionOperationTrancheRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLPrevisionOperationTrancheRepository(template);
    }

    @Bean
    public VersionBudgetRepository vbudgetRepository() {
        return new QueryDSLVersionBudgetRepository();
    }

    @Bean
    public EtatBudgetRepository etatBudgetRepository() {
        return new QueryDSLEtatBudgetRepository();
    }

    @Bean
    public BudgetFactory budgetFactory(BudgetRepository budgetRepository, VersionBudgetRepository versionBudgetRepository,
            EtatBudgetRepository etatBudgetRepository) {
        return new BudgetFactory(budgetRepository, versionBudgetRepository, etatBudgetRepository);
    }

    @Bean
    public BudgetSyntheseOperationService budgetSyntheseOperationService(BudgetRepository budgetRepository,
            PrevisionOperationTrancheRepository previsionOperationTrancheRepository, SyntheseOperationRepository syntheseOperationRepository,
            OperationFacade operationFacade) {
        return new BudgetSyntheseOperationService(budgetRepository, previsionOperationTrancheRepository, syntheseOperationRepository, operationFacade);
    }

    @Bean
    public EbEtatSommesReadRepository budgetEbEtatReadRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLEbEtatSommesReadRepository(template);
    }

    @Bean
    public EbEtatRepository ebEtatRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLEbEtatRepository(template);
    }

    @Bean
    public EbEtatReadRepository ebEtatReadRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLEbEtatReadRepository(template);
    }

    @Bean
    BudgetTableauxBudgetairesRepository budgetTableauxBudgetairesRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLBudgetTableauxBudgetairesRepository(template);
    }

}
