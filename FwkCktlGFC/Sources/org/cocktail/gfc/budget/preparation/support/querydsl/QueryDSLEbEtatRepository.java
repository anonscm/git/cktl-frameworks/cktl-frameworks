/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.EbEtat;
import org.cocktail.gfc.budget.preparation.EbEtatId;
import org.cocktail.gfc.budget.preparation.support.EbEtatRepository;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudBudgetEbEtat;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpDep;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpRec;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepAe;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraRec;
import org.cocktail.gfc.support.querydsl.QVAdmCrForEb;
import org.cocktail.gfc.support.querydsl.QVAdmEtabForEb;
import org.cocktail.gfc.support.querydsl.QVAdmUbForEb;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;
import org.springframework.data.jdbc.query.SqlUpdateCallback;

import com.google.common.base.Optional;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.sql.dml.SQLUpdateClause;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Path;
import com.mysema.query.types.Projections;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.query.ListSubQuery;

public class QueryDSLEbEtatRepository extends GfcRepository implements EbEtatRepository {

    // Tables
    private final QBudBudgetEbEtat budEbEtat = QBudBudgetEbEtat.budBudgetEbEtat;
    private final QBudPrevHorsOpDep previsionDep = QBudPrevHorsOpDep.budPrevHorsOpDep;
    private final QBudPrevOpeTraDepAe previsionDepOpeTra = QBudPrevOpeTraDepAe.budPrevOpeTraDepAe;
    private final QBudPrevHorsOpRec previsionRec = QBudPrevHorsOpRec.budPrevHorsOpRec;
    private final QBudPrevOpeTraRec previsionRecOpeTra = QBudPrevOpeTraRec.budPrevOpeTraRec;
    private final QAdmEb eb = QAdmEb.admEb;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = QAdmEbExer.admEbExer;
    private final QVAdmEtabForEb vAdmEtabForEb = QVAdmEtabForEb.vAdmEtabForEb;
    private final QVAdmCrForEb vAdmCrForEb = QVAdmCrForEb.vAdmCrForEb;
    private final QVAdmUbForEb vAdmUbForEb = QVAdmUbForEb.vAdmUbForEb;

    // Projections
    private ConstructorExpression<EbEtat> constructeurBudgetEbEtat = Projections.constructor(EbEtat.class, budEbEtat.idBudBudgetEbEtat,
            budEbEtat.idBudBudget, budEbEtat.idAdmEb, budEbEtat.tyetId);

    // Paths
    private NumberPath<Long> idBudPath = Expressions.numberPath(Long.class, "id_bud");
    private NumberPath<Long> idEbPath = Expressions.numberPath(Long.class, "id_eb");
    private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "sr");
    private NumberPath<Long> sousReqIdBudPath = Expressions.numberPath(Long.class, sousReqPath, "id_bud");
    private NumberPath<Long> sousReqIdEbPath = Expressions.numberPath(Long.class, sousReqPath, "id_eb");
    
    private QueryDslJdbcTemplate template;
    private Path<?>[] columnsForInsert;
    private List<Path<?>> columnsForUpdate;

    public QueryDSLEbEtatRepository(QueryDslJdbcTemplate template) {
        this.template = template;
        initDMLColumns();
    }

    @SuppressWarnings("unchecked")
    private void initDMLColumns() {
        List<Path<?>> forInsert = new ArrayList<Path<?>>(Arrays.asList(budEbEtat.idBudBudgetEbEtat, budEbEtat.idBudBudget, budEbEtat.idAdmEb,
                budEbEtat.tyetId));
        this.columnsForInsert = forInsert.toArray(new Path[forInsert.size()]);
        this.columnsForUpdate = new ArrayList<Path<?>>(Arrays.asList(budEbEtat.tyetId));
    }

    public long addBudgetEbEtat(EbEtat budgetEbEtat) {
        // TODO FBR 18.02.15 voir si on veut renvoyer l'objet ou non
        long idBudgetEbEtatAdded = template.insertWithKey(budEbEtat, createInsertCallback(budgetEbEtat));
        return idBudgetEbEtatAdded;
    }

    public long updateBudgetEbEtat(EbEtat budgetEbEtat) {
        long idBudgetEbEtatUpdated = template.update(budEbEtat, createUpdateCallback(budgetEbEtat));
        return idBudgetEbEtatUpdated;
    }

    public long updateBudgetEbEtatList(BudgetId budgetId, List<EntiteBudgetaireId> listeEntiteBudgetaireId, TypeEtat typeEtat) {
        return template.update(budEbEtat, createUpdateCallback(budgetId, listeEntiteBudgetaireId, typeEtat));
    }

    public Optional<EbEtat> findByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId entiteBudgetaireId) {
        SQLQuery query = addByBudgetIdAndEntiteBudgetaireIdClause(getEbEtatQuery(), budgetId, entiteBudgetaireId);
        EbEtat budgetEbEtat = template.queryForObject(query, constructeurBudgetEbEtat);

        return Optional.fromNullable(budgetEbEtat);
    }

    public List<EbEtat> findByAllByBudgetIdAndListEntiteBudgetaireId(BudgetId budgetId, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        SQLQuery query = addByBudgetIdAndEntiteBudgetaireIdClause(getEbEtatQuery(), budgetId, listeEntiteBudgetaireId);
        List<EbEtat> budgetEbEtat = template.query(query, constructeurBudgetEbEtat);
        return budgetEbEtat;
    }
    
    public List<EbEtat> findChildrenByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId ebRacineId) {
        Long ebRacineIdNumber = ebRacineId.getId();
        SQLQuery query = getEbEtatSubTreeQuery(budgetId, ebRacineId);  
        query = query.where(sousReqIdEbPath.ne(ebRacineIdNumber));
        List<EbEtat> listeBudgetEbEtat = template.query(query, constructeurBudgetEbEtat);
        return listeBudgetEbEtat;
    }
    
    public List<EbEtat> findSubTreeByBudgetIdAndEbId(BudgetId budgetId, EntiteBudgetaireId ebRacineId) {
        SQLQuery query = getEbEtatSubTreeQuery(budgetId, ebRacineId);
        List<EbEtat> listeBudgetEbEtat = template.query(query, constructeurBudgetEbEtat);
        return listeBudgetEbEtat;
    }
    
    public EbEtat findBudgetEbEtaById(EbEtatId budgetEbEtatId) {
    	SQLQuery query = addByEbEtatId(getEbEtatQuery(), budgetEbEtatId);
    	EbEtat budgetEbEtat = template.queryForObject(query, constructeurBudgetEbEtat);
    	return budgetEbEtat;
    }

    private SqlInsertWithKeyCallback<Long> createInsertCallback(final EbEtat budgetEbEtat) {
        return new SqlInsertWithKeyCallback<Long>() {
            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(budEbEtat));
                values.addAll(budgetEbEtatAsList(budgetEbEtat));
                return insert.columns(columnsForInsert).values(values.toArray()).executeWithKey(budEbEtat.idBudBudgetEbEtat);
            }
        };
    }   
    
    private SqlUpdateCallback createUpdateCallback(final EbEtat budgetEbEtatMaj) {
        return new SqlUpdateCallback() {
            public long doInSqlUpdateClause(SQLUpdateClause update) {
                List<Object> values = new ArrayList<Object>();
                values.add(budgetEbEtatMaj.getEtat().getIdTypeEtat());
                return update.set(columnsForUpdate, values)
                		     .where(budEbEtat.idBudBudgetEbEtat.eq(budgetEbEtatMaj.getEbEtatId().getId()))
                		     .execute();
            }
        };
    }

    private SqlUpdateCallback createUpdateCallback(final BudgetId budgetId, final List<EntiteBudgetaireId> listeEntiteBudgetaireId, final TypeEtat typeEtat) {
        return new SqlUpdateCallback() {
            public long doInSqlUpdateClause(SQLUpdateClause update) {
                List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
                for (EntiteBudgetaireId entiteBudgetaireId : listeEntiteBudgetaireId) {
                    listeEntiteBudgetaireIdNumber.add(entiteBudgetaireId.getId());
                }
                List<Object> values = new ArrayList<Object>();
                values.add(typeEtat.getIdTypeEtat());
                return update.set(columnsForUpdate, values)
                             .where(budEbEtat.idBudBudget.eq(budgetId.getId())
                    		 .and(budEbEtat.idAdmEb.in(listeEntiteBudgetaireIdNumber)))
                             .execute();
            }
        };
    }

    // Queries
    private SQLQuery getEbEtatQuery() {
        SQLQuery sqlQuery = template.newSqlQuery().from(budEbEtat);
        return sqlQuery;
    }
    
    private SQLQuery getEbEtatSubTreeQuery(BudgetId budgetId, EntiteBudgetaireId ebRacineId) {
        Long budgetIdNumber = budgetId.getId();
        ListSubQuery<Tuple> previsionDepTuple = new SQLSubQuery()
                .from(previsionDep)
                .groupBy(previsionDep.idBudBudget, previsionDep.idAdmEb)
                .where(previsionDep.idBudBudget.eq(budgetIdNumber))
                .list(previsionDep.idBudBudget.as(idBudPath), previsionDep.idAdmEb.as(idEbPath));
        ListSubQuery<Tuple> previsionRecTuple = new SQLSubQuery()
                .from(previsionRec)
                .groupBy(previsionRec.idBudBudget, previsionRec.idAdmEb)
                .where(previsionRec.idBudBudget.eq(budgetIdNumber))
                .list(previsionRec.idBudBudget.as(idBudPath), previsionRec.idAdmEb.as(idEbPath));
        ListSubQuery<Tuple> trancheOpeDepTuple = new SQLSubQuery()
                .from(previsionDepOpeTra)
                .groupBy(previsionDepOpeTra.idBudBudget, previsionDepOpeTra.idAdmEb)
                .where(previsionDepOpeTra.idBudBudget.eq(budgetIdNumber))
                .list(previsionDepOpeTra.idBudBudget.as(idBudPath), previsionDepOpeTra.idAdmEb.as(idEbPath));
        ListSubQuery<Tuple> trancheOpeRecTuple = new SQLSubQuery()
                .from(previsionRecOpeTra)
                .groupBy(previsionRecOpeTra.idBudBudget, previsionRecOpeTra.idAdmEb)
                .where(previsionRecOpeTra.idBudBudget.eq(budgetIdNumber))
                .list(previsionRecOpeTra.idBudBudget.as(idBudPath), previsionRecOpeTra.idAdmEb.as(idEbPath));
        ListSubQuery<Tuple> ebSaisissables = new SQLSubQuery()
                .from(ebExer)
                .join(budget)
                .on(budget.idBudBudget.eq(budgetId.getId()).and(budget.exeOrdre.eq(ebExer.exeOrdre.intValue())))
                .where(ebExer.saisieBudgetaire.eq(1))
                .list(budget.idBudBudget.as(idBudPath), ebExer.idAdmEb.as(idEbPath));
        
        ListSubQuery<Tuple> union = new SQLSubQuery().from(new SQLSubQuery()
                .union(previsionDepTuple, previsionRecTuple, trancheOpeDepTuple, trancheOpeRecTuple, ebSaisissables)
                .as(sousReqPath))
                .list(sousReqIdBudPath.as(idBudPath), sousReqIdEbPath.as(idEbPath));
        
        SQLQuery query = template.newSqlQuery().from(union.as(sousReqPath));
        query = addClauseIsInSubTree(query, ebRacineId)
                .leftJoin(budEbEtat)
                .on(sousReqIdBudPath.eq(budEbEtat.idBudBudget).and(sousReqIdEbPath.eq(budEbEtat.idAdmEb)));
        return query;
    }
    
    private SQLQuery addByEbEtatId(SQLQuery sqlQuery, EbEtatId budgetEbEtatId) {
    	sqlQuery = sqlQuery.where(budEbEtat.idBudBudgetEbEtat.eq(budgetEbEtatId.getId()));
    	return sqlQuery;
    }

    private SQLQuery addByBudgetIdAndEntiteBudgetaireIdClause(SQLQuery sqlQuery, BudgetId budgetId, EntiteBudgetaireId entiteBudgetaireId) {
        sqlQuery = sqlQuery.where(budEbEtat.idBudBudget.eq(budgetId.getId()).and(budEbEtat.idAdmEb.eq(entiteBudgetaireId.getId())));
        return sqlQuery;
    }
    
    private SQLQuery addByBudgetIdAndEntiteBudgetaireIdClause(SQLQuery sqlQuery, BudgetId budgetId, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<Long> listeEntiteBudgetaireIdNumber = new ArrayList<Long>();
        for (EntiteBudgetaireId entiteBudgetaireEbId : listeEntiteBudgetaireId) {
            listeEntiteBudgetaireIdNumber.add(entiteBudgetaireEbId.getId());
        }
        return sqlQuery.where(budEbEtat.idBudBudget.eq(budgetId.getId()).and(budEbEtat.idAdmEb.in(listeEntiteBudgetaireIdNumber)));
    }

    private SQLQuery addClauseIsInSubTree(SQLQuery sqlQuery, EntiteBudgetaireId ebId) {
        Long ebIdNumber = ebId.getId();
        Integer niveau = getNiveauEb(ebId);
        switch (niveau) {
        case 1:
            return sqlQuery.where(sousReqIdEbPath.in(new SQLSubQuery().from(vAdmEtabForEb).where(vAdmEtabForEb.idAdmEbEtab.eq(ebIdNumber)).list(vAdmEtabForEb.idAdmEb)));
        case 2:
            return sqlQuery.where(sousReqIdEbPath.in(new SQLSubQuery().from(vAdmUbForEb).where(vAdmUbForEb.idAdmEbUb.eq(ebIdNumber)).list(vAdmUbForEb.idAdmEb)));
        case 3:
            return sqlQuery.where(sousReqIdEbPath.in(new SQLSubQuery().from(vAdmCrForEb).where(vAdmCrForEb.idAdmEbCr.eq(ebIdNumber)).list(vAdmCrForEb.idAdmEb)));
        case 4:
            return sqlQuery.where(sousReqIdEbPath.eq(ebIdNumber));
        default:
            throw new IllegalArgumentException(ebId.getId().toString());
        }
    }

    protected Integer getNiveauEb(EntiteBudgetaireId ebId) {
        Long ebIdNumber = ebId.getId();
        SQLQuery ebQuery = template.newSqlQuery().from(eb).where(eb.idAdmEb.eq(ebIdNumber));
        Expression<Integer> niveauConstructor = Projections.constructor(Integer.class, eb.orgNiv);
        Integer niveau = template.queryForObject(ebQuery, niveauConstructor);
        return niveau;
    }
    
    // Utils
    private List<Object> budgetEbEtatAsList(EbEtat budgetEbEtat) {
        List<Object> values = new ArrayList<Object>();
        values.add(budgetEbEtat.getBudgetId().getId());
        values.add(budgetEbEtat.getEbId().getId());
        values.add(budgetEbEtat.getEtat().getIdTypeEtat());
        return values;
    }
}
