/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.CombinaisonAxesRecette;
import org.cocktail.gfc.budget.preparation.PrevisionRecetteId;
import org.cocktail.gfc.budget.preparation.PrevisionRecetteOperation;
import org.cocktail.gfc.budget.preparation.support.PrevisionRecetteOperationRepository;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.operation.TrancheRecetteId;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraRec;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlDeleteCallback;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.sql.dml.SQLDeleteClause;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.types.Path;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.query.ListSubQuery;

/**
 * @author bourges
 * 
 */
public class QueryDSLPrevisionRecetteOperationRepository extends GfcRepository implements PrevisionRecetteOperationRepository {

    private QueryDslJdbcTemplate template;
    private SectionRecetteCacheService sectionRecetteService;

    private QBudPrevOpeTraRec prevOpeTraRec = QBudPrevOpeTraRec.budPrevOpeTraRec;
    Path<?>[] colsForInsert;

    public QueryDSLPrevisionRecetteOperationRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
        @SuppressWarnings("unchecked")
        List<Path<?>> cols = new ArrayList<Path<?>>(Arrays.asList(
                prevOpeTraRec.idBudPrevOpeTraRec,
                prevOpeTraRec.commentaire,
                prevOpeTraRec.idAdmOrigineRecette,
                prevOpeTraRec.idAdmEb,
                prevOpeTraRec.idAdmNatureRec,
                prevOpeTraRec.section,
                prevOpeTraRec.idBudBudget,
                prevOpeTraRec.idOpeOperation,
                prevOpeTraRec.idOpeTrancheBudRec,
                prevOpeTraRec.montantRec,
                prevOpeTraRec.montantTitre,
                prevOpeTraRec.dCreation,
                prevOpeTraRec.dModification,
                prevOpeTraRec.persIdCreation,
                prevOpeTraRec.persIdModification
                ));
        colsForInsert = cols.toArray(new Path[cols.size()]);
    }

    /**
     * @see org.cocktail.gfc.budget.preparation.support.PrevisionRecetteOperationRepository#createPrevisionRecetteOperationOnIntegrationTranche(org.cocktail.gfc.operation.TrancheId,
     *      org.cocktail.gfc.budget.preparation.BudgetId, java.lang.Long)
     */
    public void createPrevisionRecetteOperationOnIntegrationTranche(TrancheId trancheId, BudgetId budgetId, Long persId) {
        QOpeTrancheBudRec opeTrancheBudRec = QOpeTrancheBudRec.opeTrancheBudRec;
        QOpeTrancheBud opeTrancheBud = QOpeTrancheBud.opeTrancheBud;
        SQLQuery query = template.newSqlQuery();
        query.from(opeTrancheBudRec)
                .join(opeTrancheBud).on(opeTrancheBud.idOpeTrancheBud.eq(opeTrancheBudRec.idOpeTrancheBud))
                .where(opeTrancheBudRec.idOpeTrancheBud.eq(trancheId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                opeTrancheBudRec.commentaire,
                opeTrancheBudRec.idAdmOrigineRecette,
                opeTrancheBudRec.idAdmEb,
                opeTrancheBudRec.section,
                opeTrancheBudRec.idAdmNatureRec,
                opeTrancheBud.idOpeOperation,
                opeTrancheBudRec.idOpeTrancheBudRec,
                opeTrancheBudRec.montantRec,
                opeTrancheBudRec.montantTitre));
        for (Tuple row : result) {
            PrevisionRecetteOperation previsionRecetteOperation = PrevisionRecetteOperation.builder()
                    .commentaire(row.get(opeTrancheBudRec.commentaire))
                    .combinaison(CombinaisonAxesRecette.builder()
                            .origineRecetteId(new OrigineRecetteId(row.get(opeTrancheBudRec.idAdmOrigineRecette)))
                            .entiteBudgetaireId(new EntiteBudgetaireId(row.get(opeTrancheBudRec.idAdmEb)))
                            .sectionRecette(SectionRecetteCacheService.findByAbreviation(row.get(opeTrancheBudRec.section)))
                            .natureRecetteId(new NatureRecetteId(row.get(opeTrancheBudRec.idAdmNatureRec)))
                            .operationId(new OperationId(row.get(opeTrancheBud.idOpeOperation)))
                            .build())
                    .budgetId(budgetId)
                    .trancheRecetteId(new TrancheRecetteId(row.get(opeTrancheBudRec.idOpeTrancheBudRec)))
                    .montantRecette(new Montant(row.get(opeTrancheBudRec.montantRec)))
                    .montantTitre(new Montant(row.get(opeTrancheBudRec.montantTitre)))
                    .persIdCreation(persId)
                    .dateCreation(new Date())
                    .build();
            createPrevisionRecetteOperation(previsionRecetteOperation);
        }
    }

    /**
     * @see org.cocktail.gfc.budget.preparation.support.PrevisionRecetteOperationRepository#createPrevisionRecetteOperation(org.cocktail.gfc.budget.preparation.PrevisionRecetteOperation)
     */
    public PrevisionRecetteOperation createPrevisionRecetteOperation(PrevisionRecetteOperation previsionRecetteOperation) {
        PrevisionRecetteOperation ret = previsionRecetteOperation;
        Long id = template.insertWithKey(prevOpeTraRec, createBudgetRecetteCallback(previsionRecetteOperation));
        ret.setPrevisionId(new PrevisionRecetteId(id));
        return ret;
    }

    public void removeByTrancheAndBudget(TrancheId trancheId, BudgetId budgetId) {
        template.delete(prevOpeTraRec, removeByTrancheAndBudgetCallBack(trancheId, budgetId));

    }

    private SqlDeleteCallback removeByTrancheAndBudgetCallBack(final TrancheId trancheId, final BudgetId budgetId) {
        return new SqlDeleteCallback() {

            public long doInSqlDeleteClause(SQLDeleteClause delete) {
                QOpeTrancheBudRec opeTrancheBudRec = QOpeTrancheBudRec.opeTrancheBudRec;
                // on a besoin de supprimer tous les tuples liés à la Tranche
                // via la table opeTrancheBudRec
                ListSubQuery<Long> sub = new SQLSubQuery()
                        .from(opeTrancheBudRec)
                        .where(opeTrancheBudRec.idOpeTrancheBud.eq(trancheId.id()))
                        .list(opeTrancheBudRec.idOpeTrancheBudRec);
                SQLDeleteClause deleteComplete = delete
                        .where(prevOpeTraRec.idBudBudget.eq(budgetId.getId())
                                .and(prevOpeTraRec.idOpeTrancheBudRec.in(sub)));
                return deleteComplete.execute();
            }
        };
    }

    private SqlInsertWithKeyCallback<Long> createBudgetRecetteCallback(final PrevisionRecetteOperation previsionRecetteOperation) {
        return new SqlInsertWithKeyCallback<Long>() {

            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(prevOpeTraRec));
                values.addAll(asList(previsionRecetteOperation));
                return insert.columns(colsForInsert).values(values.toArray()).executeWithKey(prevOpeTraRec.idBudPrevOpeTraRec);
            }

        };
    }

    private List<Object> asList(PrevisionRecetteOperation prev) {
        List<Object> ret = new ArrayList<Object>();
        ret.add(prev.getCommentaire());
        ret.add(prev.getCombinaison().getOrigineRecetteId().getId());
        ret.add(prev.getCombinaison().getEntiteBudgetaireId().getId());
        ret.add(prev.getCombinaison().getNatureRecetteId().getId());
        ret.add(prev.getCombinaison().getSectionRecette().getAbreviation());
        ret.add(prev.getBudgetId().getId());
        ret.add(prev.getCombinaison().getOperationId().getId());
        ret.add(prev.getTrancheRecetteId().getId());
        ret.add(prev.getMontantRecette().value());
        ret.add(prev.getMontantTitre().value());
        ret.add(prev.getDateCreation());
        ret.add(prev.getDateModification());
        ret.add(prev.getPersIdCreation());
        ret.add(prev.getPersIdModification());
        return ret;
    }

}
