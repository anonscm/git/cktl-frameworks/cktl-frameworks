/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.budget.preparation.EbEtat;
import org.cocktail.gfc.budget.preparation.EbEtatId;
import org.cocktail.gfc.budget.preparation.BudgetId;

import com.google.common.base.Optional;

public interface EbEtatRepository {
    
    /**
     * @return L'état d'entité budgétaire ayant l'id de budget et l'id d'entité budgétaire donnés
     */
    Optional<EbEtat> findByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId entiteBudgetaireId);
    
    /**
     * @return La liste d'états d'entité budgétaire ayant l'id de budget et les id d'entités budgétaires donnés
     */
    List<EbEtat> findByAllByBudgetIdAndListEntiteBudgetaireId(BudgetId budgetId, List<EntiteBudgetaireId> listeEntiteBudgetaireId);
    
    /**
     * @return La liste des états d'entité budgétaire enfants de la racine donnée pour le budget donné
     */
    List<EbEtat> findChildrenByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId ebRacineId);
    
    /**
     * @return La liste des états d'entité budgétaire du sous-arbre de racine donnée pour le budget donné
     */
    List<EbEtat> findSubTreeByBudgetIdAndEbId(BudgetId budgetId, EntiteBudgetaireId ebRacineId);
    
    /**
     * @return L'état d'entité budgétaire d'id donné
     */
    EbEtat findBudgetEbEtaById(EbEtatId id);
    
    /**
     * Ajoute l'état d'entité budgétaire donnée
     * @return L'id du nouvel état d'entité budgétaire
     */
    long addBudgetEbEtat(EbEtat budgetEbEtat);
    
    /**
     * Met à jour l'état d'entité budgétaire (de même id)
     * @return L'id de l'état d'entité budgétaire mis à jour
     */
    long updateBudgetEbEtat(EbEtat budgetEbEtat);

    /**
     * Met à jour l'état pour les états d'entité budgétaires ayant l'id de budget et les id d'entités budgétaires donnés
     */
    long updateBudgetEbEtatList(BudgetId budgetId, List<EntiteBudgetaireId> listeEntiteBudgetaireId, TypeEtat typeEtat);
}
