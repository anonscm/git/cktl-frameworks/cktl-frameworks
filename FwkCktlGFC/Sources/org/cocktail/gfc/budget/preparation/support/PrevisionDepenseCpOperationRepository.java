package org.cocktail.gfc.budget.preparation.support;

import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseCpOperation;
import org.cocktail.gfc.operation.TrancheId;

public interface PrevisionDepenseCpOperationRepository {
    
    /**
     * @param previsionDepenseCpOperation - {@link PrevisionDepenseCpOperation} à insérer en base
     * @return l'objet créé en base
     */
    PrevisionDepenseCpOperation createPrevisionDepenseCpOperation(PrevisionDepenseCpOperation previsionDepenseCpOperation);
    
    /**
     * Création, côté budget, des détails d'opération au moment de l'intégration d'une tranche
     * @param trancheId - l'id de la tranche à intégrer
     * @param budgetId - l'id du budget sur lequel on intègre la tranche
     * @param persId - l'id de la personne qui fait l'opération
     */
    void createPrevisionDepenseCpOperationOnIntegrationTranche(TrancheId trancheId, BudgetId budgetId, Long persId);
    
    /**
     * Supression des détails d'opération au moment de "désintégration" d'une tranche
     * @param trancheId - l'id de la tranche à désintégrer
     * @param budgetId - l'id du budget sur lequel on désintègre la tranche
     */
    void removeByTrancheAndBudget(TrancheId trancheId, BudgetId budgetId);

}
