package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.CombinaisonAxes;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseCpOperation;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseId;
import org.cocktail.gfc.budget.preparation.support.PrevisionDepenseCpOperationRepository;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TrancheDepenseCpId;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraDepCp;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepCp;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlDeleteCallback;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.sql.dml.SQLDeleteClause;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.types.Path;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.query.ListSubQuery;

public class QueryDSLPrevisionDepenseCpOperationRepository extends GfcRepository implements PrevisionDepenseCpOperationRepository {

    private QueryDslJdbcTemplate template;

    private QOpeTrancheBudDepCp cpTranche = QOpeTrancheBudDepCp.opeTrancheBudDepCp;
    private QOpeTrancheBud tranche = QOpeTrancheBud.opeTrancheBud;
    private QBudPrevOpeTraDepCp prevCpOperation = QBudPrevOpeTraDepCp.budPrevOpeTraDepCp;
    Path<?>[] colsForInsert;

    public QueryDSLPrevisionDepenseCpOperationRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
        @SuppressWarnings("unchecked")
        List<Path<?>> cols = new ArrayList<Path<?>>(Arrays.asList(
                prevCpOperation.idBudPrevOpeTraDepCp,
                prevCpOperation.dCreation,
                prevCpOperation.dModification,
                prevCpOperation.idAdmDestinationDepense,
                prevCpOperation.idAdmEb,
                prevCpOperation.idAdmNatureDep,
                prevCpOperation.idBudBudget,
                prevCpOperation.idBudEnveloppe,
                prevCpOperation.idOpeOperation,
                prevCpOperation.idOpeTrancheBudDepCp,
                prevCpOperation.montantCp,
                prevCpOperation.persIdCreation,
                prevCpOperation.persIdModification
                ));
        colsForInsert = cols.toArray(new Path[cols.size()]);
    }

    public PrevisionDepenseCpOperation createPrevisionDepenseCpOperation(PrevisionDepenseCpOperation previsionDepenseCpOperation) {
        PrevisionDepenseCpOperation ret = previsionDepenseCpOperation;
        Long id = template.insertWithKey(prevCpOperation, createBudgetDepenseCallback(previsionDepenseCpOperation));
        ret.setPrevisionId(new PrevisionDepenseId(id));
        return ret;
    }

    public void createPrevisionDepenseCpOperationOnIntegrationTranche(TrancheId trancheId, BudgetId budgetId, Long persId) {
        SQLQuery query = template.newSqlQuery();
        query.from(cpTranche)
            .join(tranche).on(tranche.idOpeTrancheBud.eq(cpTranche.idOpeTrancheBud))
            .where(cpTranche.idOpeTrancheBud.eq(trancheId.getId()));
        List<Tuple> result = template.query(query, new QTuple(
                cpTranche.idAdmDestinationDepense,
                cpTranche.idAdmEb,
                cpTranche.idAdmNatureDep,
                cpTranche.idBudEnveloppe,
                tranche.idOpeOperation,
                cpTranche.idOpeTrancheBudDepCp,
                cpTranche.montantCp));
        for (Tuple row : result) {
            PrevisionDepenseCpOperation previsionDepenseOperation = PrevisionDepenseCpOperation.builder()
                    .combinaison(CombinaisonAxes.builder()
                            .destinationDepenseId(new DestinationDepenseId(row.get(cpTranche.idAdmDestinationDepense)))
                            .entiteBudgetaireId(new EntiteBudgetaireId(row.get(cpTranche.idAdmEb)))
                            .natureDepenseId(new NatureDepenseId(row.get(cpTranche.idAdmNatureDep)))
                            .enveloppeId(new EnveloppeBudgetaireId(row.get(cpTranche.idBudEnveloppe)))
                            .operationId(new OperationId(row.get(tranche.idOpeOperation)))
                            .build())
                    .budgetId(budgetId)
                    .trancheDepenseCpId(new TrancheDepenseCpId(row.get(cpTranche.idOpeTrancheBudDepCp)))
                    .montantCP(new Montant(row.get(cpTranche.montantCp)))
                    .persIdCreation(persId)
                    .dateCreation(new Date())
                    .build();
            createPrevisionDepenseCpOperation(previsionDepenseOperation);
        }
    }

    public void removeByTrancheAndBudget(TrancheId trancheId, BudgetId budgetId) {
        template.delete(prevCpOperation, removeByTrancheAndBudgetCallBack(trancheId, budgetId));
    }
    
    private SqlDeleteCallback removeByTrancheAndBudgetCallBack(final TrancheId trancheId, final BudgetId budgetId) {
        return new SqlDeleteCallback() {
            
            public long doInSqlDeleteClause(SQLDeleteClause delete) {
                //on a besoin de supprimer tous les tuples liés à la Tranche via la table opeTrancheBudDepCp                
                ListSubQuery<Long> sub = new SQLSubQuery()
                        .from(cpTranche)
                        .where(cpTranche.idOpeTrancheBud.eq(trancheId.id()))
                        .list(cpTranche.idOpeTrancheBudDepCp);
                SQLDeleteClause deleteComplete = delete
                        .where(prevCpOperation.idBudBudget.eq(budgetId.getId())
                                .and(prevCpOperation.idOpeTrancheBudDepCp.in(sub)));
                return deleteComplete.execute();
            }
        };
    }

    private SqlInsertWithKeyCallback<Long> createBudgetDepenseCallback(final PrevisionDepenseCpOperation prev) {
        return new SqlInsertWithKeyCallback<Long>() {

            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(prevCpOperation));
                values.addAll(asList(prev));
                return insert.columns(colsForInsert).values(values.toArray()).executeWithKey(prevCpOperation.idBudPrevOpeTraDepCp);
            }

        };
    }
    
    private List<Object> asList(PrevisionDepenseCpOperation prev) {
        List<Object> ret = new ArrayList<Object>();
        ret.add(prev.getDateCreation());
        ret.add(prev.getDateModification());
        ret.add(prev.getCombinaison().getDestinationDepenseId().getId());
        ret.add(prev.getCombinaison().getEntiteBudgetaireId().getId());
        ret.add(prev.getCombinaison().getNatureDepenseId().getId());
        ret.add(prev.getBudgetId().getId());
        ret.add(prev.getCombinaison().getEnveloppeId().getId());
        ret.add(prev.getCombinaison().getOperationId().getId());            
        ret.add(prev.getTrancheDepenseCpId().getId());
        ret.add(prev.getMontantCP().value());
        ret.add(prev.getPersIdCreation());
        ret.add(prev.getPersIdModification());
        return ret;
    }
}
