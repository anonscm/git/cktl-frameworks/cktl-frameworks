/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.support.PrevisionRecetteReportingRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureRec;
import org.cocktail.gfc.support.querydsl.QAdmOrigineRecette;
import org.cocktail.gfc.support.querydsl.QAdmTypeEtat;
import org.cocktail.gfc.support.querydsl.QBudBudget;
import org.cocktail.gfc.support.querydsl.QBudPrevHorsOpRec;
import org.cocktail.gfc.support.querydsl.QBudPrevOpeTraRec;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;
import com.mysema.query.types.query.ListSubQuery;

/**
 * @author  Raymond NANEON <raymond.naneon at asso-cocktail.fr>
 *
 */
public class QueryDSLPrevisionRecetteReportingRepository implements
		PrevisionRecetteReportingRepository {

    // Tables
    private final QBudPrevHorsOpRec prevision = QBudPrevHorsOpRec.budPrevHorsOpRec;
    private final QBudPrevOpeTraRec previsionOpeTra = QBudPrevOpeTraRec.budPrevOpeTraRec;
    private final QAdmEb eb = new QAdmEb("eb");
    private final QAdmNatureRec natureRecette = QAdmNatureRec.admNatureRec;
    private final QAdmTypeEtat natureTypeEtat = QAdmTypeEtat.admTypeEtat;
    private final QAdmOrigineRecette origineRecette = QAdmOrigineRecette.admOrigineRecette;
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private final QBudBudget budget = QBudBudget.budBudget;
    private final QAdmEbExer ebExer = new QAdmEbExer("ebex");
    

	private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "mySousRequette");
    private StringPath sousReqOrgEbPath = Expressions.stringPath(sousReqPath, "ORG_ETAB");
    private StringPath sousReqOrgUbPath = Expressions.stringPath(sousReqPath, "ORG_UB");
    private StringPath sousReqOrgCrPath = Expressions.stringPath(sousReqPath, "ORG_CR");
    private StringPath sousReqOrgSouCrPath = Expressions.stringPath( sousReqPath, "ORG_SOUSCR");
    private StringPath sousReqOrgLibPath = Expressions.stringPath(sousReqPath, "ORG_LIB");
    private StringPath sousReqTypePath = Expressions.stringPath(sousReqPath, "ETAT");
    private StringPath sousReqOriginePath = Expressions.stringPath(sousReqPath, "ORIGINE");
    private StringPath sousReqNaturePath = Expressions.stringPath(sousReqPath, "NATURE");
    private NumberPath<BigDecimal> sousReqMontantRecPath = Expressions.numberPath(BigDecimal.class, sousReqPath, "MONTANT_REC");

    private QueryDslJdbcTemplate template;
    
    public QueryDSLPrevisionRecetteReportingRepository(QueryDslJdbcTemplate template) {
		// TODO Auto-generated constructor stub
    	this.template = template;
	}

	/* (non-Javadoc)
	 * @see org.cocktail.gfc.budget.preparation.support.PrevisionRecetteReportingRepository#getPrevisionsFiltreQuery(int, org.cocktail.gfc.budget.preparation.BudgetId, org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId)
	 */
    
    public String getPrevisionsFiltreQuery(int exercice, BudgetId budgetId, List<Long> ebIds) {
        BooleanBuilder whereBuilderRec = new BooleanBuilder();
        BooleanBuilder whereBuilderOpe = new BooleanBuilder();
       	whereBuilderRec.and(prevision.idBudBudget.eq(budgetId.getId()));
       	whereBuilderRec.and(budget.exeOrdre.eq(exercice));
       	whereBuilderOpe.and(previsionOpeTra.idBudBudget.eq(budgetId.getId()));
       	whereBuilderOpe.and(budget.exeOrdre.eq(exercice));
        
        if(ebIds != null) {
        	whereBuilderRec.and(prevision.idAdmEb.in(ebIds));
        	whereBuilderOpe.and(previsionOpeTra.idAdmEb.in(ebIds));
        }
        ListSubQuery<Tuple> sqlQueryOpe = new SQLSubQuery().from(previsionOpeTra)
                .join(eb).on(previsionOpeTra.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(previsionOpeTra.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(origineRecette).on(previsionOpeTra.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                .join(operation).on(previsionOpeTra.idOpeOperation.eq(operation.idOpeOperation))
                .join(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                .join(budget).on(previsionOpeTra.idBudBudget.eq(budget.idBudBudget))
                .join(natureTypeEtat).on(natureTypeEtat.tyetId.eq(natureRecette.tyetId))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue())).on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderOpe)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle, 
                		natureRecette.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle.as("ETAT"), 
                		natureRecette.libelle.as("NATURE"),
                		previsionOpeTra.montantRec.sum().as("MONTANT_REC"));     

        ListSubQuery<Tuple> sqlQueryRec = new SQLSubQuery().from(prevision)
                .join(eb).on(prevision.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(prevision.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(origineRecette).on(prevision.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                .join(budget).on(prevision.idBudBudget.eq(budget.idBudBudget))
                .join(natureTypeEtat).on(natureTypeEtat.tyetId.eq(natureRecette.tyetId))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue())).on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderRec)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle, 
                		natureRecette.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle.as("ETAT"), 
                		natureRecette.libelle.as("NATURE"),
                		prevision.montantRec.sum().as("MONTANT_REC"));
        
        SQLQuery sqlQuery = template.newSqlQuery()
        		.from(new SQLSubQuery().union(sqlQueryRec, sqlQueryOpe).as(sousReqPath))
        		.groupBy(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqTypePath, sousReqNaturePath)
        		.orderBy(sousReqOrgEbPath.asc(), sousReqOrgUbPath.asc(), sousReqOrgCrPath.asc(), 
        		sousReqOrgSouCrPath.asc(), sousReqOrgLibPath.asc(), sousReqTypePath.asc(), sousReqNaturePath.asc());
        sqlQuery.setUseLiterals(true);
        String query = sqlQuery.getSQL(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqTypePath, sousReqNaturePath,
        		sousReqMontantRecPath.sum().as("MONTANT_REC")).getSQL(); 
    	return query;
    }

	/* (non-Javadoc)
	 * @see org.cocktail.gfc.budget.preparation.support.PrevisionRecetteReportingRepository#getPrevisionsFiltreByOrigineQuery(int, org.cocktail.gfc.budget.preparation.BudgetId, org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId)
	 */
	public String getPrevisionsFiltreByOrigineQuery(int exercice,
			BudgetId budgetId, List<Long> ebIds) {
		// TODO Auto-generated method stub
        BooleanBuilder whereBuilderRec = new BooleanBuilder();
        BooleanBuilder whereBuilderOpe = new BooleanBuilder();
       	whereBuilderRec.and(prevision.idBudBudget.eq(budgetId.getId()));
       	whereBuilderRec.and(budget.exeOrdre.eq(exercice));
       	whereBuilderOpe.and(previsionOpeTra.idBudBudget.eq(budgetId.getId()));
       	whereBuilderOpe.and(budget.exeOrdre.eq(exercice));
        
        if(ebIds != null) {
        	whereBuilderRec.and(prevision.idAdmEb.in(ebIds));
        	whereBuilderOpe.and(previsionOpeTra.idAdmEb.in(ebIds));
        }
        ListSubQuery<Tuple> sqlQueryOpe = new SQLSubQuery().from(previsionOpeTra)
                .join(eb).on(previsionOpeTra.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(previsionOpeTra.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(origineRecette).on(previsionOpeTra.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                .join(operation).on(previsionOpeTra.idOpeOperation.eq(operation.idOpeOperation))
                .join(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                .join(budget).on(previsionOpeTra.idBudBudget.eq(budget.idBudBudget))
                .join(natureTypeEtat).on(natureTypeEtat.tyetId.eq(natureRecette.tyetId))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue())).on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderOpe)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle, 
                		origineRecette.libelle, natureRecette.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle.as("ETAT"), 
                		origineRecette.libelle.as("ORIGINE"), natureRecette.libelle.as("NATURE"),
                		previsionOpeTra.montantRec.sum().as("MONTANT_REC"));     

        ListSubQuery<Tuple> sqlQueryRec = new SQLSubQuery().from(prevision)
                .join(eb).on(prevision.idAdmEb.eq(eb.idAdmEb))
                .join(natureRecette).on(prevision.idAdmNatureRec.eq(natureRecette.idAdmNatureRec))
                .join(origineRecette).on(prevision.idAdmOrigineRecette.eq(origineRecette.idAdmOrigineRecette))
                .join(budget).on(prevision.idBudBudget.eq(budget.idBudBudget))
                .join(natureTypeEtat).on(natureTypeEtat.tyetId.eq(natureRecette.tyetId))
                .leftJoin(ebExer).on(budget.exeOrdre.eq(ebExer.exeOrdre.intValue())).on(eb.idAdmEb.eq(ebExer.idAdmEb))
                .where(whereBuilderRec)
                .groupBy(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle, 
                		origineRecette.libelle, natureRecette.libelle)
                .list(eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, natureTypeEtat.tyetLibelle.as("ETAT"), 
                		origineRecette.libelle.as("ORIGINE"), natureRecette.libelle.as("NATURE"),
                		prevision.montantRec.sum().as("MONTANT_REC"));
        
        SQLQuery sqlQuery = template.newSqlQuery()
        		.from(new SQLSubQuery().union(sqlQueryRec, sqlQueryOpe).as(sousReqPath))
        		.groupBy(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqTypePath, sousReqOriginePath, sousReqNaturePath)
        		.orderBy(sousReqOrgEbPath.asc(), sousReqOrgUbPath.asc(), sousReqOrgCrPath.asc(), 
        		sousReqOrgSouCrPath.asc(), sousReqOrgLibPath.asc(), sousReqTypePath.asc(), sousReqOriginePath.asc(), sousReqNaturePath.asc());
        sqlQuery.setUseLiterals(true);
        String query = sqlQuery.getSQL(sousReqOrgEbPath, sousReqOrgUbPath, sousReqOrgCrPath, 
        		sousReqOrgSouCrPath, sousReqOrgLibPath, sousReqTypePath, sousReqOriginePath, sousReqNaturePath,
        		sousReqMontantRecPath.sum().as("MONTANT_REC")).getSQL(); 
		return query;
	}

}
