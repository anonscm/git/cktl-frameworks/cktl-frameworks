/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.budget.preparation.Budget;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.BudgetSyntheseOperation;
import org.cocktail.gfc.integration.RegleTrancheIntegrable;
import org.cocktail.gfc.operation.SyntheseOperation;
import org.cocktail.gfc.operation.TrancheOperation;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.operation.TrancheOperationNotFound;
import org.cocktail.gfc.operation.controller.OperationFacade;
import org.cocktail.gfc.operation.support.SyntheseOperationRepository;
import org.cocktail.gfc.support.exception.InvalidActionException;

import com.google.common.base.Optional;

public class BudgetSyntheseOperationService {

	private BudgetRepository budgetRepository;
	private PrevisionOperationTrancheRepository previsionOperationTrancheRepository;
	private SyntheseOperationRepository syntheseOperationRepository;
	private OperationFacade operationFacade;
	private static final String ERR_TRANCHE_NON_INTEGRABLE = "Tranche d''opération \"{0}\" non intégrable.";
	private static final String ERR_TRANCHE_NON_TROUVEE = "Tranche d''id {0} non trouvée.";

	public BudgetSyntheseOperationService(BudgetRepository budgetRepository, PrevisionOperationTrancheRepository previsionOperationTrancheRepository, SyntheseOperationRepository syntheseOperationRepository, OperationFacade operationFacade) {
		this.budgetRepository = budgetRepository;
		this.previsionOperationTrancheRepository = previsionOperationTrancheRepository;
		this.syntheseOperationRepository = syntheseOperationRepository;
		this.operationFacade = operationFacade;
	}

	public List<BudgetSyntheseOperation> getBudgetSyntheseOperationByBudgetId(BudgetId budgetId) {

		Budget budget = budgetRepository.findById(budgetId);
		if (budget != null) {
			List<SyntheseOperation> listeOperations = syntheseOperationRepository.findTrancheOperationByExercice(budget.getExeOrdre());
			List<Long> listeOperationIntegrees = previsionOperationTrancheRepository.findOperationIdIntegreesByBudgetId(budgetId.getId());

			List<BudgetSyntheseOperation> listeBudgetSyntheseOperation = new ArrayList<BudgetSyntheseOperation>();
			for (SyntheseOperation syntheseOperation : listeOperations) {
				boolean integree = listeOperationIntegrees.contains(syntheseOperation.getOperation().getOperationId().id());
				BudgetSyntheseOperation sOp = new BudgetSyntheseOperation(syntheseOperation, budgetId.getId(), integree);
				listeBudgetSyntheseOperation.add(sOp);
			}
			return listeBudgetSyntheseOperation;
		}
		return null;
	}

    /**
     * @param tranche
     * @return true si la tranche est intégrable au budget, false sinon
     */
    public boolean isIntegrable(TrancheOperation tranche) {
        RegleTrancheIntegrable regleTrancheIntegrable = new RegleTrancheIntegrable();
        boolean estIntegrable = regleTrancheIntegrable.isSatisfiedBy(tranche);
        return estIntegrable;
    }
	
    /**
     * Vérifie qu'une tranche est intégrable
     * @param trancheId
     * @throws InvalidActionException
     * @throws TrancheOperationNotFound
     */
	public void verifierTrancheIntegrable(TrancheId trancheId) throws InvalidActionException, TrancheOperationNotFound {
	    Optional<TrancheOperation> optionalTranche = operationFacade.getTrancheById(trancheId);
	    if (optionalTranche.isPresent()) {
	        TrancheOperation tranche = optionalTranche.get();
	        boolean estIntegrable = isIntegrable(tranche);
	        if (!estIntegrable) {
	            throw new InvalidActionException(MessageFormat.format(ERR_TRANCHE_NON_INTEGRABLE, tranche.getOperationLibelle()));
	        }
	    } else {
	        throw new TrancheOperationNotFound(MessageFormat.format(ERR_TRANCHE_NON_TROUVEE, trancheId.getId()));
	    }
	}
}
