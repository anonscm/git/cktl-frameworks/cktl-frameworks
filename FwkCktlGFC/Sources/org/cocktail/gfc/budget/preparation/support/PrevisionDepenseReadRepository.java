/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support;

import java.util.List;

import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseId;
import org.cocktail.gfc.budget.preparation.read.PrevisionDepenseRead;

import com.google.common.base.Optional;

public interface PrevisionDepenseReadRepository {

    /**
     * Récupère une prévision de dépense hors opération avec ses informations détaillées
     * @param id id de prévision de dépense hors opération
     * @return la prévision de dépense d'id donné (Optional)
     */
	Optional<PrevisionDepenseRead> queryById(PrevisionDepenseId id);
	
	/**
	 * Récupère la liste des prévisions de dépense d'un budget avec ou non celles liées à des opérations
	 * @param budgetId
	 * @param opeTra si true, les prévisions de dépense liées à des opérations sont aussi récupérées
	 * @return les prévisions du budget d'id donné
	 */
	List<PrevisionDepenseRead> queryByBudgetId(BudgetId budgetId, boolean opeTra);
	
}
