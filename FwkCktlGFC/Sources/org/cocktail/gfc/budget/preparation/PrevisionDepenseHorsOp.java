/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import static org.apache.commons.lang.Validate.notNull;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Builder;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;

@Getter
@Setter
@Builder
public class PrevisionDepenseHorsOp {

	private PrevisionDepenseId previsionId;
	private BudgetId budgetId;
	private CombinaisonAxes combinaison;
	private Montant montantAE;
	private Montant montantCP;
	private String commentaire;
	private Long persIdCreation;
	private Date dateCreation;
	private Long persIdModification;
	private Date dateModification;
	
	private void initPrevisionDepense(PrevisionDepenseId previsionDepenseId, BudgetId budgetId,
			CombinaisonAxes combinaison, Montant montantAE, Montant montantCP, String commentaire, 
			Long persIdCreation, Date dateCreation, Long persIdModification, Date dateModification) {
		this.previsionId = previsionDepenseId;
		this.budgetId = budgetId;
		setCombinaison(combinaison);
		this.montantAE = montantAE;
		this.montantCP = montantCP;
		this.commentaire = commentaire;
		this.persIdCreation = persIdCreation;
		this.dateCreation = dateCreation;
		this.persIdModification = persIdModification;
		this.dateModification = dateModification;
	}

	public PrevisionDepenseHorsOp(Long persIdCreation, BudgetId budgetId, CombinaisonAxes combinaison, Montant montantAE, Montant montantCP, String commentaire) {
		PrevisionDepenseId id = null;
		Date dateCrea = new Date();
		Long persIdModif = null;
		Date dateModif = null;
		initPrevisionDepense(id, budgetId, combinaison, montantAE, montantCP, commentaire, persIdCreation, dateCrea, persIdModif, dateModif);
	}
	
	public PrevisionDepenseHorsOp(PrevisionDepenseId previsionDepenseId, BudgetId budgetId,
			CombinaisonAxes combinaison, Montant montantAE, Montant montantCP, String commentaire, 
			Long persIdCreation, Date dateCreation, Long persIdModification, Date dateModification) {
		initPrevisionDepense(previsionDepenseId, budgetId, combinaison, montantAE, montantCP, commentaire, persIdCreation, dateCreation, persIdModification, dateModification);
	}
	
	public PrevisionDepenseHorsOp(Long colPrevisionId, Long colBudgetId, Long colEntiteBudgetaireId, Long colEnveloppeId, 
			Long colNatureDepenseId, Long colDestinationDepenseId,
			BigDecimal colMontantAE, BigDecimal colMontantCP, String colCommentaire, Long colPersIdCreation,
			Date colDateCreation, Long colPersIdModification, Date colDateModification) {
		
		PrevisionDepenseId id = new PrevisionDepenseId(colPrevisionId);
		BudgetId idBudget = new BudgetId(colBudgetId);
		
		// Création de la combinaison
		EntiteBudgetaireId entiteBudgetaireId = new EntiteBudgetaireId(colEntiteBudgetaireId);
		EnveloppeBudgetaireId enveloppeId = new EnveloppeBudgetaireId(colEnveloppeId);
		NatureDepenseId natureDepenseId = new NatureDepenseId(colNatureDepenseId);
		OperationId operationId = null;
		DestinationDepenseId destinationDepenseId = new DestinationDepenseId(colDestinationDepenseId);
		CombinaisonAxes combi = new CombinaisonAxes(entiteBudgetaireId, enveloppeId, natureDepenseId, operationId, destinationDepenseId);
		
		Montant mAE = new Montant(colMontantAE);
		Montant mCP = new Montant(colMontantCP);

		initPrevisionDepense(id, idBudget, combi, mAE, mCP, colCommentaire, colPersIdCreation, colDateCreation, colPersIdModification, colDateModification);
	}

	public void setPrevisionId(PrevisionDepenseId previsionId) {
		if (getPrevisionId() != null) {
			throw new IllegalArgumentException("Clé déjà existante !");
		}
		this.previsionId = previsionId;
	}
	
	public void mettreAJour(CombinaisonAxes combinaison, Montant ae, Montant cp, String commentaire, Long persIdModificateur) {
		setCombinaison(combinaison);
		setMontantAE(ae);
		setMontantCP(cp);
		setCommentaire(commentaire);
		setPersIdModification(persIdModificateur);
		setDateModification(new Date());
	}
	
	public void setCombinaison(CombinaisonAxes combinaison) {
		notNull(combinaison, "La combinaison d'axes est obligatoire.");
		
		if (getCombinaison() != null) {
			RegleEntiteBudgetaireNonModifiable regleEbNonModifiable =
					new RegleEntiteBudgetaireNonModifiable(getCombinaison().getEntiteBudgetaireId());
			if (!regleEbNonModifiable.isSatisfiedBy(combinaison.getEntiteBudgetaireId())) {
				throw new IllegalArgumentException(regleEbNonModifiable.getMessage());
			}
		}
		
		this.combinaison = combinaison;
	}

}
