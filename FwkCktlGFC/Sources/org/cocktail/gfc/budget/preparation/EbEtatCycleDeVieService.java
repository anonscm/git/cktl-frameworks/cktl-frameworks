/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.budget.preparation.ebetat.ArbreEbEtat;
import org.cocktail.gfc.budget.preparation.ebetat.ArbreEbEtatFactory;
import org.cocktail.gfc.budget.preparation.ebetat.RegleBudgetEstValidable;
import org.cocktail.gfc.budget.preparation.ebetat.RegleEtablissementEstDevalidable;
import org.cocktail.gfc.budget.preparation.ebetat.RegleVerifierEtatEtablissement;
import org.cocktail.gfc.budget.preparation.read.EbEtatSommesRead;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.budget.preparation.support.EbEtatRepository;
import org.cocktail.gfc.budget.preparation.support.EbEtatSommesReadRepository;
import org.cocktail.gfc.budget.validation.support.BudgetDepenseRepository;
import org.cocktail.gfc.budget.validation.support.BudgetRecetteRepository;
import org.cocktail.gfc.integration.pilotage.PilotageIntegrationService;
import org.cocktail.gfc.support.exception.InvalidActionException;

import com.google.common.base.Optional;

public class EbEtatCycleDeVieService {

    private static final String ERR_LIGNE_BUD_NON_CONTROLABLE = "L''entité budgétaire {1} du budget {0} ne peut pas être contrôlée car elle n''est pas dans l''état \"Verrouillée\".";
    private static final String ERR_LIGNE_BUD_NON_VERROUILLABLE = "L''entité budgétaire {1} du budget {0} ne peut pas être verrouillée car elle n''est pas dans l''état \"Non renseignée\".";
    private static final String ERR_LIGNE_BUD_NON_VERROUILLABLE_NON_INTEGREE = "L''entité budgétaire {1} du budget {0} ne peut pas être verrouillée car il existe des tranches d''opérations la concernant qui ne sont pas intégrées.";
    private static final String ERR_LIGNE_BUD_NON_DEVERROUILLABLE_NON_RENSEIGNEE = "L''entité budgétaire {1} du budget {0} ne peut pas être déverrouillée car elle est dans l''état \"Non renseignée\".";
    private static final String ERR_LIGNE_BUD_NON_DEVERROUILLABLE_DEJA_CONTROLEE = "L''entité budgétaire {1} du budget {0} ne peut plus être déverrouillée.";
    
    private PilotageIntegrationService integrationService;
    private NomenclatureFacade nomenclatureFacade;
    private EbEtatRepository ebEtatRepository;
    private EbEtatSommesReadRepository budgetEbEtatReadRepository;
    private ArbreEbEtatFactory arbreEbEtatFactory;
    private BudgetRepository budgetRepository;
    private BudgetRecetteRepository budgetRecetteRepository;
    private BudgetDepenseRepository budgetDepenseRepository;
    
    // AMLEIORATION FLA 09.03.2015 : 5 classes de dependances. Cela me parait excessif (difficile a tester car bcp a mocker).
    public EbEtatCycleDeVieService(PilotageIntegrationService integrationService, NomenclatureFacade nomenclatureFacade,
            EbEtatRepository ebEtatRepository, EbEtatSommesReadRepository budgetEbEtatReadRepository,
            ArbreEbEtatFactory arbreEbEtatFactory, BudgetRepository budgetRepository, BudgetRecetteRepository budgetRecetteRepository,
            BudgetDepenseRepository budgetDepenseRepository) {
        this.integrationService = integrationService;
        this.nomenclatureFacade = nomenclatureFacade;
        this.ebEtatRepository = ebEtatRepository;
        this.budgetEbEtatReadRepository = budgetEbEtatReadRepository;
        this.arbreEbEtatFactory = arbreEbEtatFactory;
        this.budgetRepository = budgetRepository;
        this.budgetRecetteRepository = budgetRecetteRepository;
        this.budgetDepenseRepository = budgetDepenseRepository;
    }
    
    public EbEtat creerBudgetEbEtatVerrouillee(BudgetId budgetId, EntiteBudgetaireId ebId) throws InvalidActionException {
        if (!integrationService.isTrancheIntegreeAuBudget(budgetId, ebId)) {
            Budget budget = budgetRepository.findById(budgetId);
            String ebLibelle = getEbLibelle(budget, ebId);
            String budgetLibelle = (budget != null) ? budget.getLibelle() : budgetId.getId().toString(); 
            throw new InvalidActionException(MessageFormat.format(ERR_LIGNE_BUD_NON_VERROUILLABLE_NON_INTEGREE, budgetLibelle, ebLibelle));
        }

        return new EbEtat(null, budgetId, ebId, TypeEtatService.Code.VERROUILLEE.etat());
    }

    public void verrouiller(EbEtat budgetEbEtat) throws InvalidActionException {
        BudgetId budgetId = budgetEbEtat.getBudgetId();
        EntiteBudgetaireId ebId = budgetEbEtat.getEbId();
        boolean nEstPasNonRenseignee = !budgetEbEtat.getEtat().estNonRenseignee();
        boolean estDansTrancheNonIntegree = !integrationService.isTrancheIntegreeAuBudget(budgetId, ebId);
        if (nEstPasNonRenseignee || estDansTrancheNonIntegree) {
            Budget budget = budgetRepository.findById(budgetId);
            String ebLibelle = getEbLibelle(budget, ebId);
            String budgetLibelle = (budget != null) ? budget.getLibelle() : budgetId.getId().toString();
            if (nEstPasNonRenseignee) {
                throw new InvalidActionException(MessageFormat.format(ERR_LIGNE_BUD_NON_VERROUILLABLE, budgetLibelle, ebLibelle));
            }
            if (estDansTrancheNonIntegree) {
                throw new InvalidActionException(MessageFormat.format(ERR_LIGNE_BUD_NON_VERROUILLABLE_NON_INTEGREE, budgetLibelle, ebLibelle));
            }
        }

        budgetEbEtat.setEtat(TypeEtatService.Code.VERROUILLEE.etat());
    }

    public EbEtat deverrouiller(Optional<EbEtat> budgetEbEtatOpt, BudgetId budgetId, EntiteBudgetaireId ebId)
            throws InvalidActionException {
        EbEtat budgetEbEtat;
        boolean estNonRenseignee = !budgetEbEtatOpt.isPresent() || budgetEbEtatOpt.get().getEtat().estNonRenseignee();
        boolean nEstPasVerrouillee = !budgetEbEtatOpt.get().getEtat().estVerrouillee();
        if (estNonRenseignee || nEstPasVerrouillee) {
            Budget budget = budgetRepository.findById(budgetId);
            String ebLibelle = getEbLibelle(budget, ebId);
            String budgetLibelle = (budget != null) ? budget.getLibelle() : budgetId.getId().toString();
            if (estNonRenseignee) {
                throw new InvalidActionException(MessageFormat.format(ERR_LIGNE_BUD_NON_DEVERROUILLABLE_NON_RENSEIGNEE, budgetLibelle, ebLibelle));
            }
            if (nEstPasVerrouillee) {
                throw new InvalidActionException(MessageFormat.format(ERR_LIGNE_BUD_NON_DEVERROUILLABLE_DEJA_CONTROLEE, budgetLibelle, ebLibelle));
            }
        }
        budgetEbEtat = budgetEbEtatOpt.get();
        budgetEbEtat.setEtat(TypeEtatService.Code.NON_RENSEIGNEE.etat());
        return budgetEbEtat;
    }
    
    public List<EntiteBudgetaireId> verifierControlable(BudgetId budgetId, EntiteBudgetaireId ebRacineId)
            throws InvalidActionException {
        List<EbEtat> listeSousArbreEb = ebEtatRepository.findSubTreeByBudgetIdAndEbId(budgetId, ebRacineId);
        List<EntiteBudgetaireId> listeEbIdControle = new ArrayList<EntiteBudgetaireId>();
        for (EbEtat budgetEbEtat : listeSousArbreEb) {
            if (!budgetEbEtat.getEtat().estVerrouillee()) {
                if (!budgetEbEtat.getEtat().estControlee()) {
                    Budget budget = budgetRepository.findById(budgetId);
                    String ebLibelle = getEbLibelle(budget, budgetEbEtat.getEbId());
                    String budgetLibelle = (budget != null) ? budget.getLibelle() : budgetId.getId().toString();
                    throw new InvalidActionException(MessageFormat.format(ERR_LIGNE_BUD_NON_CONTROLABLE, budgetLibelle, ebLibelle));
                }
            } else {
                listeEbIdControle.add(budgetEbEtat.getEbId());
            }
        }
        return listeEbIdControle;
    }    

    public List<EntiteBudgetaireId> verifierControleAnnulable(BudgetId budgetId, EntiteBudgetaireId ebRacineId) throws InvalidActionException {
        List<EbEtat> listeSousArbreEb = ebEtatRepository.findSubTreeByBudgetIdAndEbId(budgetId, ebRacineId);
        List<EntiteBudgetaireId> listeEbIdControleAnnulable = new ArrayList<EntiteBudgetaireId>();
        for (EbEtat budgetEbEtat : listeSousArbreEb) {
            if (budgetEbEtat.getEtat().estControlee()) {
                listeEbIdControleAnnulable.add(budgetEbEtat.getEbId());
            }
        }
        return listeEbIdControleAnnulable;
    }

    public List<EbEtatSommesRead> synthesePreparationNiveauEtablissement(BudgetId budgetId) {
        //On va chercher tous les établissements "rattachés" à ce budget
		List<EbEtatSommesRead> synthesePreparationNiveauEtablissement =
				new ArrayList<EbEtatSommesRead>(budgetEbEtatReadRepository.synthesePreparationNiveauEtablissement(budgetId));
		//On va voir toutes les EB enfants afin de calculer l'état de l'établissement
		for (EbEtatSommesRead budgetEbEtatRead : synthesePreparationNiveauEtablissement) {
			ArbreEbEtat arbreEbPourEtablissement = arbreEbEtatFactory.create(budgetId, budgetEbEtatRead.getEb().getEntiteBudgetaireId());
			TypeEtat etatArbreEtablissement = arbreEbPourEtablissement.etatEnfants();
			budgetEbEtatRead.setEtat(etatArbreEtablissement);
        }
		return synthesePreparationNiveauEtablissement;
    }
    
    public EbEtatSommesRead synthesePreparationNiveauEtablissement(BudgetId budgetId, EntiteBudgetaireId etablissementId) {
		EbEtatSommesRead synthesePreparationEtablissement =
				budgetEbEtatReadRepository.synthesePreparationNiveauEtablissement(budgetId, etablissementId);
		Validate.isTrue(synthesePreparationEtablissement.getEb().getNiveau() == 1, "La synthèse niveau établissement doit être appelée au niveau établissement !");
		ArbreEbEtat arbreEbPourEtablissement = arbreEbEtatFactory.create(budgetId, etablissementId);
        //Si Eb est un établissement alors l'état de l'arbre est calculé à partir des seuls enfants
		TypeEtat etatArbreEtablissement = arbreEbPourEtablissement.etatEnfants();
		synthesePreparationEtablissement.setEtat(etatArbreEtablissement);
		return synthesePreparationEtablissement;
    }
    
    public void validerUnEtablissement(Budget budget, EntiteBudgetaireId etablissementId) {

		ArbreEbEtat arbreEbEtat = arbreEbEtatFactory.create(budget.getIdBudget(), etablissementId);
		RegleVerifierEtatEtablissement regleEbsFillesControlees = 
				new RegleVerifierEtatEtablissement(TypeEtatService.Code.CONTROLEE.etat()); 
		
		if (!regleEbsFillesControlees.isSatisfiedBy(arbreEbEtat)) {
		    throw new IllegalArgumentException(regleEbsFillesControlees.getMessage());
		}
		
    	List<EntiteBudgetaireId> listeEbId = extractEbId(arbreEbEtat.getEnfants());
    	ebEtatRepository.updateBudgetEbEtatList(budget.getIdBudget(), listeEbId, TypeEtatService.Code.VALIDEE.etat());
    }
    
    public void devaliderUnEtablissement(Budget budget, EntiteBudgetaireId etablissementId) {

        ArbreEbEtat arbreEbEtat = arbreEbEtatFactory.create(budget.getIdBudget(), etablissementId);
        RegleVerifierEtatEtablissement regleEbsFillesValidees = 
                new RegleVerifierEtatEtablissement(TypeEtatService.Code.VALIDEE.etat()); 
        RegleEtablissementEstDevalidable regleEtablissementEstDevalidable = new RegleEtablissementEstDevalidable();
        
        if (!regleEbsFillesValidees.isSatisfiedBy(arbreEbEtat)) {
            throw new IllegalArgumentException(regleEbsFillesValidees.getMessage());
        }
        
        //RNA 02/04/2015
        if(!regleEtablissementEstDevalidable.isSatisfiedBy(budget)) {
        	throw new IllegalArgumentException(MessageFormat.format(regleEtablissementEstDevalidable.getMessage(), budget.getEtatBudget().getLibelle()));
        }
        
        List<EntiteBudgetaireId> listeEbId = extractEbId(arbreEbEtat.getEnfants());
        ebEtatRepository.updateBudgetEbEtatList(budget.getIdBudget(), listeEbId, TypeEtatService.Code.CONTROLEE.etat());
    }
    
    public Budget validerBudget(Budget budget, Long persId) {
    	List<EbEtatSommesRead> syntheseEtatsEtablissements = synthesePreparationNiveauEtablissement(budget.getIdBudget());
    	RegleBudgetEstValidable regleBudgetEstvalidable = new RegleBudgetEstValidable();
    	if (!regleBudgetEstvalidable.isSatisfiedBy(syntheseEtatsEtablissements)) {
    		// TODO FLA 11.03.205 GfcValidation est Checked ... :/
    		throw new IllegalArgumentException(regleBudgetEstvalidable.getMessage());
    	}
    	
    	fusionPreparation(budget.getIdBudget(), persId);
    	
    	budget.setEtatBudget(EtatBudget.VALIDE);
    	return budget;
    }
    
    public Budget devaliderBudget(Budget budget) {
    	suppressionFusionPreparation(budget.getIdBudget());
    	budget.setEtatBudget(EtatBudget.PREPARATION);
    	return budget;
    }
    
    private void fusionPreparation(BudgetId budgetId, Long persId) {
        budgetDepenseRepository.createBudgetDepenseOnValidationBudget(budgetId, persId);
        budgetRecetteRepository.createBudgetRecetteOnValidationBudget(budgetId, persId);
    }
    
    private void suppressionFusionPreparation(BudgetId budgetId) {
        budgetDepenseRepository.removeByBudget(budgetId);
        budgetRecetteRepository.removeByBudget(budgetId);
    }
    
    private List<EntiteBudgetaireId> extractEbId(List<EbEtat> listeEbEtat) {
    	List<EntiteBudgetaireId> listeEbId = new ArrayList<EntiteBudgetaireId>();
    	for (EbEtat currentEbEtat : listeEbEtat) {
    		listeEbId.add(currentEbEtat.getEbId());
    	}
    	return listeEbId;
    }
    
    protected String getEbLibelle(Budget budget, EntiteBudgetaireId ebId) {
        String ebLibelle = ebId.getId().toString();
        if (budget != null) {
            int exercice = budget.getExeOrdre();
            Optional<EntiteBudgetaire> eb = nomenclatureFacade.findByIdAndExercice(ebId, exercice);
            if (eb.isPresent()) {
                ebLibelle = eb.get().getConcat();
            } 
        }
        return ebLibelle;
    }
}
