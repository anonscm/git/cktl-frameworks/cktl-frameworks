/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import java.text.MessageFormat;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.common.specification.AbstractSpecification;

import com.google.common.base.Optional;

/**
 * @author  Raymond NANEON <raymond.naneon at asso-cocktail.fr>
 *
 */
public class RegleDestinationSaisissableSurPrevisionDepense extends AbstractSpecification<PrevisionDepenseHorsOp> {
	
	private static final String ERR_DESTINATION_DOIT_ETRE_SAISISSABLE = "La destination de dépense {0} doit être saisissable";
    
    private NomenclatureFacade facade;
    private BudgetRepository budRepo;
    private String libelle;
    
    public RegleDestinationSaisissableSurPrevisionDepense(NomenclatureFacade facade, BudgetRepository budRepo) {
        super();
        this.facade = facade;
        this.budRepo = budRepo;
        this.libelle = "";
    }

	@Override
	public boolean isSatisfiedBy(PrevisionDepenseHorsOp previsionDepense) {
		// TODO Auto-generated method stub
		boolean ret = false;
		DestinationDepenseId id = previsionDepense.getCombinaison().getDestinationDepenseId();
        BudgetId budId = previsionDepense.getBudgetId();
        int exercice = budRepo.findById(budId).getExeOrdre();
        Optional<DestinationDepense> destination = facade.getDestinationDepenseByIdAndExercie(id, exercice);
        if(destination.isPresent()) {
        	ret = destination.get().getSaisissable();
        	libelle = destination.get().getAbreviation();
        } else {
        	libelle = id.getId().toString();
        }
		return ret;
	}
	
	public String getLibelle() {
		return libelle;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return MessageFormat.format(ERR_DESTINATION_DOIT_ETRE_SAISISSABLE, getLibelle());
	}

}
