/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.ebetat;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.budget.preparation.read.EbEtatSommesRead;
import org.cocktail.gfc.common.specification.AbstractSpecification;

public class RegleBudgetEstValidable extends AbstractSpecification<List<EbEtatSommesRead>> {

	@Override
	public boolean isSatisfiedBy(List<EbEtatSommesRead> listeEtatsEtablissements) {
		// TODO FLA 11.03.2015 : je pense que c'est moche qu'une regle s'appuie sur des objets de lecture.
		// et sur une List en param. Verifier des regles metiers sur des objets metiers me semble plus elegant.
		if (listeEtatsEtablissements == null) {
			return false;
		}

		for (EbEtatSommesRead ebEtatEtablissement : listeEtatsEtablissements) {
			//Modif RNA 18/03/2015 le "!" bloque la validation du budget
			if (TypeEtatService.Code.CONTROLEE.equals(ebEtatEtablissement.getEtat())
					|| TypeEtatService.Code.NON_RENSEIGNEE.equals(ebEtatEtablissement.getEtat())
					|| TypeEtatService.Code.VERROUILLEE.equals(ebEtatEtablissement.getEtat())) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public String getMessage() {
		return "Le budget ne peut être validé qu'une fois tous les établissements validés";
	}

}
