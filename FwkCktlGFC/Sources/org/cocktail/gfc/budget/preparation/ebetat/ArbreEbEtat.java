/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.ebetat;

import static org.apache.commons.lang.Validate.*;
import static org.cocktail.gfc.admin.nomenclature.TypeEtatService.Code.*;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.budget.preparation.EbEtat;

/**
 * "Arbre" des {@link EbEtat}<br>
 * on stocke la racine de l'arbre et toutes les branches sont à plat dans une liste 
 * @author bourges
 *
 */
@Getter
public class ArbreEbEtat {

	private EbEtat racine;
	private List<EbEtat> enfants;
	
	ArbreEbEtat(EbEtat racine, List<EbEtat> listeBudgetEbEtat) {
		notNull(racine);
		this.racine = racine;
		this.enfants = initEnfants(listeBudgetEbEtat);
	}
	
	private List<EbEtat> initEnfants(List<EbEtat> listeBudgetEbEtat) {
		List<EbEtat> listeEnfants = new ArrayList<EbEtat>();
		if (listeBudgetEbEtat != null) {
			listeEnfants.addAll(listeBudgetEbEtat);
		}
		return listeEnfants;
	}
	
	public boolean estDansEtat(TypeEtat typeEtat) {
	    return pereEstDansEtat(typeEtat) && enfantsSontDansEtat(typeEtat);
	}
	
	private boolean pereEstDansEtat(TypeEtat typeEtat) {
		return racine.getEtat().estDeType(typeEtat);
	}
	
	private boolean enfantsSontDansEtat(TypeEtat typeEtat) {
	    if (enfants.size() == 0) {
	    	return false;
	    }
	    
		for (EbEtat ebEtat : enfants) {
			if (!ebEtat.getEtat().estDeType(typeEtat)) {
			    return false;
			}
		}
		return true;
	}
	
	public TypeEtat etatArbre() {
		// AMELIORATION FLA 09.03.2015 : on doit pouvoir faire mieux que les if.
		TypeEtatService.Code etatCode = NON_RENSEIGNEE;
		if (estDansEtat(VALIDEE.etat())) {
			etatCode = VALIDEE;
		} else if (estDansEtat(CONTROLEE.etat())) {
			etatCode = CONTROLEE;
		} else if (estDansEtat(VERROUILLEE.etat())) {
			etatCode = VERROUILLEE;
		}
		
		return etatCode.etat();
	}
	
	public TypeEtat etatEnfants() {
		// AMELIORATION FLA 09.03.2015 : on doit pouvoir faire mieux que les if.
		TypeEtatService.Code etatCode = NON_RENSEIGNEE;
		if (enfantsSontDansEtat(VALIDEE.etat())) {
			etatCode = VALIDEE;
		} else if (enfantsSontDansEtat(CONTROLEE.etat())) {
			etatCode = CONTROLEE;
		} else if (enfantsSontDansEtat(VERROUILLEE.etat())) {
			etatCode = VERROUILLEE;
		}
		
		return etatCode.etat();
	}
}
