/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import java.util.Date;

import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.budget.preparation.support.EtatBudgetRepository;
import org.cocktail.gfc.budget.preparation.support.VersionBudgetRepository;

public class BudgetFactory {

    private static final Long NUMERO_BUDGET_INITIAL = 0L;
    private static final BudgetId NULL_ID = null;

    private BudgetRepository budgetRepository;
    private VersionBudgetRepository vbudRepository;
    private EtatBudgetRepository etatBudgetRepository;

    public BudgetFactory(BudgetRepository budgetRepository, VersionBudgetRepository vbudRepository, EtatBudgetRepository etatBudgetRepository) {
        this.budgetRepository = budgetRepository;
        this.vbudRepository = vbudRepository;
        this.etatBudgetRepository = etatBudgetRepository;
    }

    public Budget creerBudget(String libelle, Integer exercice, Long persId) {
        if (isBudgetInitialExists(exercice)) {
            return creerBudgetRectificatif(libelle, exercice, persId);
        }
        return creerBudgetInitial(libelle, exercice, persId);
    }

    protected Budget creerBudgetInitial(String libelle, Integer exercice, Long persId) {
        VersionBudget versionBudgetInitial = getVersionBudget(VersionBudgetCode.BI);
        EtatBudget enPreparation = getEtatEnPreparation();
        Budget budget = Budget.builder()
                .idBudget(NULL_ID)
                .libelle(libelle)
                .exeOrdre(exercice)
                .numeroBudget(NUMERO_BUDGET_INITIAL)
                .persIdCreateur(persId)
                .dateCreation(getDateDuJour())
                .etatBudget(enPreparation)
                .versionBudget(versionBudgetInitial)
                .build();
        return budget;
    }

    protected Budget creerBudgetRectificatif(String libelle, Integer exercice, Long persId) {
        VersionBudget versionBudgetRectificatif = getVersionBudget(VersionBudgetCode.BR);
        EtatBudget enPreparation = getEtatEnPreparation();
        Budget budget = Budget.builder()
                .idBudget(NULL_ID)
                .libelle(libelle)
                .exeOrdre(exercice)
                .numeroBudget(nextNumeroBudgetRectificatif(exercice))
                .persIdCreateur(persId)
                .dateCreation(getDateDuJour())
                .etatBudget(enPreparation)
                .versionBudget(versionBudgetRectificatif)
                .build();
        return budget;
    }

    protected boolean isBudgetInitialExists(Integer exercice) {
        long nb = budgetRepository.countByExerciceAndVersion(exercice, VersionBudgetCode.BI.toString());
        return nb > 0;
    }

    protected Date getDateDuJour() {
        return new Date();
    }

    protected EtatBudget getEtatEnPreparation() {
        return etatBudgetRepository.findByCode(EtatBudgetCode.PREPARATION);
    }

    protected VersionBudget getVersionBudget(VersionBudgetCode code) {
        return vbudRepository.findByCode(code.toString());
    }

    protected Long nextNumeroBudgetRectificatif(int exercice) {
        return budgetRepository.getMaxNumeroByExercice(exercice) + 1L;
    }

}
