/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Builder;
import lombok.experimental.Wither;

import org.cocktail.common.support.CktlGFCDate;
import org.cocktail.gfc.support.exception.GfcValidationException;

@Builder
@Wither
@Getter
@Setter
public class Budget {

    private BudgetId idBudget;
    private String libelle;
    private int exeOrdre;
    private Long numeroBudget;
    private Long persIdCreateur;
    private Date dateCreation;
    private Long persIdModification;
    private Date dateModification;
    private EtatBudget etatBudget;
    private VersionBudget versionBudget;
    private CalendrierBudget calendrierBudget;

    /*
     * public Budget(Long id, String libelle, int exeOrdre, Long numeroBudget,
     * Long persIdCreateur, Date dateCreation, EtatBudget etatBudget,
     * VersionBudget versionBudget, CalendrierBudget calendrierBudget, Long
     * persIdModification, CktlGFCDate dateModification) { this(new
     * BudgetId(id), libelle, exeOrdre, numeroBudget, persIdCreateur,
     * dateCreation, etatBudget, versionBudget, calendrierBudget,
     * persIdModification, dateModification); }
     * 
     * public Budget(BudgetId id, String libelle, int exeOrdre, Long
     * numeroBudget, Long persIdCreateur, Date dateCreation, EtatBudget
     * etatBudget, VersionBudget versionBudget, CalendrierBudget
     * calendrierBudget, Long persIdModification, CktlGFCDate dateModification)
     * { this(id, libelle, exeOrdre, numeroBudget, persIdCreateur, dateCreation,
     * etatBudget, versionBudget, persIdModification, dateModification);
     * this.calendrierBudget = calendrierBudget; }
     * 
     * public Budget(Long id, String libelle, int exeOrdre, Long numeroBudget,
     * Long persIdCreateur, Date dateCreation, EtatBudget etatBudget,
     * VersionBudget versionBudget, Long persIdModification, CktlGFCDate
     * dateModification) { this(new BudgetId(id), libelle, exeOrdre,
     * numeroBudget, persIdCreateur, dateCreation, etatBudget, versionBudget,
     * persIdModification, dateModification); }
     * 
     * public Budget(BudgetId id, String libelle, int exeOrdre, Long
     * numeroBudget, Long persIdCreateur, Date dateCreation, EtatBudget
     * etatBudget, VersionBudget versionBudget, Long persIdModification,
     * CktlGFCDate dateModification) { this.idBudget = id; this.libelle =
     * libelle; this.exeOrdre = exeOrdre; this.numeroBudget = numeroBudget;
     * this.persIdCreateur = persIdCreateur; this.dateCreation = dateCreation;
     * this.etatBudget = etatBudget; this.versionBudget = versionBudget;
     * this.persIdModification = persIdModification; this.dateModification =
     * dateModification; }
     */
    public boolean estInitial() {
        return VersionBudgetCode.BI.toString().equals(getVersionBudget().getCode());
    }

    public boolean estValide() {
        return EtatBudgetCode.VALIDE.toString().equals(getEtatBudget().getCode());
    }

    public void checkBudgetValide() throws GfcValidationException {
        if (!estValide()) {
            throw new GfcValidationException("Le budget doit être à l'état validé");
        }
    }

    public boolean estVote() {
        return EtatBudgetCode.VOTE.toString().equals(getEtatBudget().getCode());
    }

    public void checkBudgetVote() throws GfcValidationException {
        if (!estVote()) {
            throw new GfcValidationException("Le budget doit être à l'état voté");
        }
    }

    public boolean estApprouve() {
        return EtatBudgetCode.APPROUVE.toString().equals(getEtatBudget().getCode());
    }

    public void checkBudgetVoteOuApprouve() throws GfcValidationException {
        if (!estVote() && !estApprouve()) {
            throw new GfcValidationException("Le budget doit être à l'état voté ou approuvé");
        }
    }

    public boolean estOuvert() {
        return EtatBudgetCode.OUVERT.toString().equals(getEtatBudget().getCode());
    }

    public void checkBudgetNonOuvert() throws GfcValidationException {
        if (estOuvert()) {
            throw new GfcValidationException("Le budget ne doit pas être ouvert");
        }
    }

    // Date de vote du budget //
    private void checkDateVoteSupprimable() throws GfcValidationException {
        getCalendrierBudget().checkDateVoteSupprimable();
    }

    private void checkDateVoteEnregistrable(CktlGFCDate date) throws GfcValidationException {
        checkBudgetValide();
        getCalendrierBudget().checkDateVoteModifiable().checkDateVoteValide(date);
    }

    public void enregistrerDateVote(CktlGFCDate date) throws GfcValidationException {
        checkDateVoteEnregistrable(date);
        getCalendrierBudget().setDateVote(date);
        passerEtatAVOTE();
    }

    public void supprimerDateVote() throws GfcValidationException {
        checkDateVoteSupprimable();
        getCalendrierBudget().setDateVote(null);
        passerEtatAVALIDE();
    }

    // Date d'envoi du budget //
    private void checkDateEnvoiSupprimable() throws GfcValidationException {
        getCalendrierBudget().checkDateEnvoiSupprimable();
    }

    private void checkDateEnvoiEnregistrable(CktlGFCDate date) throws GfcValidationException {
        checkBudgetVote();
        getCalendrierBudget().checkDateEnvoiModifiable().checkDateEnvoiValide(date);
    }

    public void enregistrerDateEnvoi(CktlGFCDate date) throws GfcValidationException {
        checkDateEnvoiEnregistrable(date);
        getCalendrierBudget().setDateEnvoi(date);
    }

    public void supprimerDateEnvoi() throws GfcValidationException {
        checkDateEnvoiSupprimable();
        getCalendrierBudget().setDateEnvoi(null);
        passerEtatAVOTE();
    }

    // Date de non approbation du budget //
    public void estDateNonApprobationSupprimable() throws GfcValidationException {
        getCalendrierBudget().checkDateNonApprobationSupprimable();
    }

    private void estDateNonApprobationEnregistrable(CktlGFCDate date) throws GfcValidationException {
        checkBudgetVote();
        getCalendrierBudget().checkDateNonApprobationModifiable().checkDateNonApprobationValide(date);
    }

    public void enregistrerDateNonApprobation(CktlGFCDate date) throws GfcValidationException {
        estDateNonApprobationEnregistrable(date);
        getCalendrierBudget().setDateNonApprobation(date);
        passerEtatANON_APPROUVE();
    }

    public void supprimerDateNonApprobation() throws GfcValidationException {
        estDateNonApprobationSupprimable();
        getCalendrierBudget().setDateNonApprobation(null);
        passerEtatAVOTE();
    }

    // Date d'approbation du budget //
    public void checkDateApprobationSupprimable() throws GfcValidationException {
        getCalendrierBudget().checkDateApprobationSupprimable();
    }

    private void checkDateApprobationEnregistrable(CktlGFCDate date) throws GfcValidationException {
        checkBudgetVote();
        getCalendrierBudget().checkDateApprobationModifiable().checkDateApprobationValide(date);
    }

    public void enregistrerDateApprobation(CktlGFCDate date) throws GfcValidationException {
        checkDateApprobationEnregistrable(date);
        getCalendrierBudget().setDateApprobation(date);
        passerEtatAAPPROUVE();
    }

    public void supprimerDateApprobation() throws GfcValidationException {
        checkDateApprobationSupprimable();
        getCalendrierBudget().setDateApprobation(null);
        passerEtatAVOTE();
    }

    // Date d'exécution du budget //
    public void estDateExecutionSupprimable() throws GfcValidationException {
        checkBudgetNonOuvert();
    }

    private void estDateExecutionEnregistrable(CktlGFCDate date) throws GfcValidationException {
        checkBudgetVoteOuApprouve();
        checkBudgetNonOuvert();
        getCalendrierBudget().checkDateExecutionModifiable().checkDateExecutionValide(date, estInitial(), exeOrdre);
    }

    public void enregistrerDateExecution(CktlGFCDate date) throws GfcValidationException {
        estDateExecutionEnregistrable(date);
        getCalendrierBudget().setDateExecution(date);
    }

    public void supprimerDateExecution() throws GfcValidationException {
        estDateExecutionSupprimable();
        getCalendrierBudget().setDateExecution(null);
        if (getCalendrierBudget().getDateApprobation() != null) {
            passerEtatAAPPROUVE();
        } else {
            passerEtatAVOTE();
        }
    }

    public void passerEtatAOUVERT() {
        changerEtat(EtatBudget.OUVERT);
    }

    public void passerEtatAAPPROUVE() {
        changerEtat(EtatBudget.APPROUVE);
    }

    public void passerEtatANON_APPROUVE() {
        changerEtat(EtatBudget.NON_APPROUVE);
    }

    public void passerEtatAVOTE() {
        changerEtat(EtatBudget.VOTE);
    }

    public void passerEtatAVALIDE() {
        changerEtat(EtatBudget.VALIDE);
    }

    private void changerEtat(EtatBudget newEtat) {
        setEtatBudget(newEtat);
    }
}
