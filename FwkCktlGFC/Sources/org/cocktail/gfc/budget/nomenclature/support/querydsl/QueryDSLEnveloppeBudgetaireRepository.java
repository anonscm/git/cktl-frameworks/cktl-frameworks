/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.nomenclature.support.querydsl;

import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaire;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.nomenclature.support.EnveloppeBudgetaireRepository;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QBudEnveloppe;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;

public class QueryDSLEnveloppeBudgetaireRepository extends GfcRepository implements EnveloppeBudgetaireRepository {

	// Tables
	private QBudEnveloppe enveloppe = QBudEnveloppe.budEnveloppe;
	
	// Projections
	private Expression<EnveloppeBudgetaireId> constructeurEnveloppeId = Projections.constructor(
			EnveloppeBudgetaireId.class, enveloppe.idBudEnveloppe);
	private Expression<EnveloppeBudgetaire> constructeurEnveloppe = 
			Projections.constructor(EnveloppeBudgetaire.class, 
					constructeurEnveloppeId, enveloppe.codeEnveloppe, enveloppe.llEnveloppe);
	
	private QueryDslJdbcTemplate template;
	
	public QueryDSLEnveloppeBudgetaireRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	public Optional<EnveloppeBudgetaire> findEnveloppeBudgetaireStandard() {
		SQLQuery query = addByCodeClause(getEnveloppeQuery(), EnveloppeBudgetaire.CODE_ENVELOPPE_STANDARD); 
		return Optional.of(template.queryForObject(query, constructeurEnveloppe));
	}
	
	// Queries
	private SQLQuery getEnveloppeQuery() {
		return template.newSqlQuery().from(enveloppe);
	}
	
	private SQLQuery addByCodeClause(SQLQuery sqlQuery, String code) {
		return sqlQuery.where(enveloppe.codeEnveloppe.eq(code));
	}
	
}
