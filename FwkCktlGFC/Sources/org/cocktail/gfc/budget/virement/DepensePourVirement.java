package org.cocktail.gfc.budget.virement;

import java.math.BigDecimal;

import lombok.Getter;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.Operation;

@Getter
public class DepensePourVirement implements Comparable<DepensePourVirement> {
    private static final int EQUAL = 0;
    private EntiteBudgetaire entiteBudgetaire;
    private Operation operation;
    private NatureDepense natureDepense;
    private DestinationDepense destinationDepense;
    private Montant aeDisponible;
    private Montant cpDisponible;

    public DepensePourVirement(EntiteBudgetaire entiteBudgetaire, Operation operation, NatureDepense natureDepense, DestinationDepense destinationDepense, BigDecimal aeDisponible, BigDecimal cpDisponible) {
        super();
        this.entiteBudgetaire = entiteBudgetaire;
        this.operation = operation;
        this.natureDepense = natureDepense;
        this.destinationDepense = destinationDepense;
        this.aeDisponible = new Montant(aeDisponible);
        this.cpDisponible = new Montant(cpDisponible);
    }

    public int compareTo(DepensePourVirement v) {   
        int comparison;
        
        comparison = compareGroupTo(v);
        if (comparison != EQUAL) {
            return comparison;
        }
        
        comparison = destinationDepense.compareTo(v.destinationDepense);
        if (comparison != EQUAL) {
            return comparison;
        }
        
        return EQUAL;
    }

    public int compareGroupTo(DepensePourVirement v) {
        int comparison;

        comparison = entiteBudgetaire.compareTo(v.entiteBudgetaire);
        if (comparison != EQUAL) {
            return comparison;
        }
       
        if (operation == null && v.operation == null) {
            comparison = EQUAL;
        } else {
            comparison = operation.compareTo(v.operation);
        }      
        if (comparison != EQUAL) {
            return comparison;
        }
        
        comparison = natureDepense.compareTo(v.natureDepense);
        if (comparison != EQUAL) {
            return comparison;
        }
        
        return EQUAL;
    }

}
