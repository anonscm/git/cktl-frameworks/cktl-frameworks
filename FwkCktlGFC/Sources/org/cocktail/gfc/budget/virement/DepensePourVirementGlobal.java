package org.cocktail.gfc.budget.virement;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.Operation;

@Getter
public class DepensePourVirementGlobal {
    private EntiteBudgetaire entiteBudgetaire;
    private Operation operation;
    private NatureDepense natureDepense;
    private List<MontantDisponibleParDestination> montantDisponibleList;
    private Montant aeDisponible;
    private Montant cpDisponible;
        
    public DepensePourVirementGlobal(DepensePourVirement depense, List<MontantDisponibleParDestination> disponible) {
        this.entiteBudgetaire = depense.getEntiteBudgetaire();
        this.operation = depense.getOperation();
        this.natureDepense = depense.getNatureDepense();
        this.montantDisponibleList = disponible;
        this.aeDisponible = depense.getAeDisponible(); 
        this.cpDisponible = depense.getCpDisponible();
    }         
}
