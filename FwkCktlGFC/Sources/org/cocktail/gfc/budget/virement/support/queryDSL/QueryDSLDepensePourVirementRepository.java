package org.cocktail.gfc.budget.virement.support.queryDSL;

import java.util.List;

import org.cocktail.gfc.budget.virement.DepensePourVirement;
import org.cocktail.gfc.budget.virement.support.DepensePourVirementRepository;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.cocktail.gfc.support.querydsl.QVCptbudDepSoldeDetail;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;

public class QueryDSLDepensePourVirementRepository extends GfcRepository implements DepensePourVirementRepository {
    
    // Tables
    private final QVCptbudDepSoldeDetail soldeDetail = QVCptbudDepSoldeDetail.vCptbudDepSoldeDetail;
    private final QAdmEb eb = QAdmEb.admEb;
    private final QAdmNatureDep natureDepense = QAdmNatureDep.admNatureDep;
    private final QAdmDestinationDepense destinationDepense = QAdmDestinationDepense.admDestinationDepense;
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private QueryDSLDepensePourVirementProjection projection = new QueryDSLDepensePourVirementProjection();

    private QueryDslJdbcTemplate template;

    public QueryDSLDepensePourVirementRepository(QueryDslJdbcTemplate template) {
        super();
        this.template = template;
    }
    
    public List<DepensePourVirement> findByExercice(int exercice) {
        SQLQuery sqlQuery = getAllDepensesQuery().where(soldeDetail.exeOrdre.eq(exercice)).orderBy(eb.orgUniv.asc(), eb.orgEtab.asc(),
                eb.orgUb.asc(), eb.orgCr.asc(), eb.orgSouscr.asc());
        
        List<DepensePourVirement> rows = template.query(sqlQuery, projection);
        return rows;     
    }
    
    private SQLQuery getAllDepensesQuery() {
        SQLQuery sqlQuery = template
                .newSqlQuery()
                .from(soldeDetail)
                .join(eb)
                .on(soldeDetail.idAdmEb.eq(eb.idAdmEb))
                .join(natureDepense)
                .on(soldeDetail.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
                .join(destinationDepense)
                .on(soldeDetail.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense))
                .leftJoin(operation).on(soldeDetail.idOpeOperation.eq(operation.idOpeOperation))
                .leftJoin(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation));
        return sqlQuery;
    }
}
