package org.cocktail.gfc.budget.virement.support.queryDSL;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.budget.virement.DepensePourVirement;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.cocktail.gfc.support.querydsl.QVCptbudDepSoldeDetail;

import com.mysema.query.Tuple;
import com.mysema.query.types.MappingProjection;

public class QueryDSLDepensePourVirementProjection extends MappingProjection<DepensePourVirement>{
    private static final long serialVersionUID = 1L;

    // Tables
    private static final QVCptbudDepSoldeDetail soldeDetail = QVCptbudDepSoldeDetail.vCptbudDepSoldeDetail;
    private static final QAdmEb eb = QAdmEb.admEb;
    private static final QAdmNatureDep natureDepense = QAdmNatureDep.admNatureDep;
    private static final QAdmDestinationDepense destinationDepense = QAdmDestinationDepense.admDestinationDepense;
    private final static QOpeOperation operation = QOpeOperation.opeOperation;
    private final static QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;

    public QueryDSLDepensePourVirementProjection() {
        super(DepensePourVirement.class, eb.idAdmEb, eb.orgUniv, eb.orgEtab, eb.orgUb, eb.orgCr, eb.orgSouscr, eb.orgLib, 
                natureDepense.idAdmNatureDep, natureDepense.code, natureDepense.libelle, natureDepense.fongible,
                destinationDepense.idAdmDestinationDepense, destinationDepense.code, destinationDepense.abreviation, destinationDepense.libelle,
                operation.idOpeOperation, operation.llOperation, operation.estFlechee, typeOperation.idOpeTypeOperation,
                typeOperation.codeTypeOperation, typeOperation.llTypeOperation,
                soldeDetail.aeSolde, soldeDetail.cpSolde);
    }

    @Override
    public DepensePourVirement map(Tuple row) {
        EntiteBudgetaire ebRet = new EntiteBudgetaire(row.get(eb.idAdmEb), row.get(eb.orgUniv), row.get(eb.orgEtab), row.get(eb.orgUb),
                row.get(eb.orgCr), row.get(eb.orgSouscr), row.get(eb.orgLib));
        NatureDepense natureDepenseRet = new NatureDepense(row.get(natureDepense.idAdmNatureDep), row.get(natureDepense.code),
                row.get(natureDepense.libelle), row.get(natureDepense.fongible));
        DestinationDepense destinationDepenseRet = new DestinationDepense(row.get(destinationDepense.idAdmDestinationDepense),
                row.get(destinationDepense.code), row.get(destinationDepense.abreviation), row.get(destinationDepense.libelle));
        DepensePourVirement depensePourVirementRet;
        if (row.get(operation.idOpeOperation) != null) {
            TypeOperation typeOperationRet = new TypeOperation(row.get(typeOperation.idOpeTypeOperation),
                    row.get(typeOperation.codeTypeOperation), row.get(typeOperation.llTypeOperation));
            OperationId operationId = new OperationId(row.get(operation.idOpeOperation));
            boolean estFlechee = (row.get(operation.estFlechee) == 1);
            Operation operationRet = new Operation(operationId, row.get(operation.llOperation), estFlechee, typeOperationRet);
            depensePourVirementRet = new DepensePourVirement(ebRet, operationRet, natureDepenseRet, destinationDepenseRet, row.get(soldeDetail.aeSolde), row.get(soldeDetail.cpSolde));
        } else {
            depensePourVirementRet = new DepensePourVirement(ebRet, null, natureDepenseRet, destinationDepenseRet, row.get(soldeDetail.aeSolde), row.get(soldeDetail.cpSolde));
        }

        return depensePourVirementRet;      
    }
}
