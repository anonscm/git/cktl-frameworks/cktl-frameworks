package org.cocktail.gfc.budget.virement.support;

import org.cocktail.gfc.budget.virement.support.queryDSL.QueryDSLDepensePourVirementRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

@Configuration
public class VirementConfig {

    @Bean
    public DepensePourVirementRepository depensePourVirementRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLDepensePourVirementRepository(template);
    }
}
