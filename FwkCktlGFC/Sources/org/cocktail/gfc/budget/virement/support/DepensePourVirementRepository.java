package org.cocktail.gfc.budget.virement.support;

import java.util.List;

import org.cocktail.gfc.budget.virement.DepensePourVirement;

public interface DepensePourVirementRepository {

    public List<DepensePourVirement> findByExercice(int exercice);
}
