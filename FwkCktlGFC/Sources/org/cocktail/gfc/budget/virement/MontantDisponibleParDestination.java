package org.cocktail.gfc.budget.virement;

import lombok.AllArgsConstructor;
import lombok.Getter;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.common.montant.Montant;

@AllArgsConstructor
@Getter
public class MontantDisponibleParDestination {
    private DestinationDepense destinationDepense;
    private Montant aeDisponible;
    private Montant cpDisponible;  
}
