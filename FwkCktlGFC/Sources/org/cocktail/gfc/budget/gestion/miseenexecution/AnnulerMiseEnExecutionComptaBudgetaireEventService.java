/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.gestion.miseenexecution;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.cocktail.gfc.budget.preparation.Budget;
import org.cocktail.gfc.comptabudgetaire.ComptaBudgetaireEventMiseEnExecutionService;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEvent;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventHash;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventType;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEventRepository;
import org.cocktail.gfc.support.exception.GfcValidationException;
import org.cocktail.gfc.support.exception.NotFoundException;

public class AnnulerMiseEnExecutionComptaBudgetaireEventService {

    private static final Logger LOG = Logger.getLogger(AnnulerMiseEnExecutionComptaBudgetaireEventService.class);
    private ComptaBudgetaireEventMiseEnExecutionService comptaBudgetaireEventMiseEnExecutionService;
    private ComptaBudgetaireEventRepository comptaBudgetaireEventRepository;

    public AnnulerMiseEnExecutionComptaBudgetaireEventService(
            ComptaBudgetaireEventMiseEnExecutionService comptaBudgetaireEventMiseEnExecutionService,
            ComptaBudgetaireEventRepository comptaBudgetaireEventRepository) {
        this.comptaBudgetaireEventMiseEnExecutionService = comptaBudgetaireEventMiseEnExecutionService;
        this.comptaBudgetaireEventRepository = comptaBudgetaireEventRepository;
    }

    public ComptaBudgetaireEvent creerEvt(Long persIdCreation, Budget budget) throws IOException, GfcValidationException, NotFoundException {
        Date debut = new Date();
        LOG.info("Création event annulation mise en exécution - début");
        ComptaBudgetaireEventHash originalEventHash = new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, budget
                .getIdBudget().id() + "", "nc", false);
        ComptaBudgetaireEvent oldEvent = getLastEventFromHash(originalEventHash);
        ComptaBudgetaireEvent newEvent = comptaBudgetaireEventMiseEnExecutionService.creerAnnulationEvt(persIdCreation, oldEvent);
        Date fin = new Date();
        LOG.log(Level.INFO, "Création event annulation mise en exécution - fin : " + (fin.getTime() - debut.getTime())/1000. + " sec");
        return newEvent;
    }

    private ComptaBudgetaireEvent getLastEventFromHash(ComptaBudgetaireEventHash hash) throws NotFoundException {
        ComptaBudgetaireEvent result = null;
        List<ComptaBudgetaireEvent> rows = comptaBudgetaireEventRepository.findByHashLike(hash);
        if (rows.size() == 0) {
            throw new NotFoundException("Impossible de trouver l'évènement correspondant au hash = " + hash);
        }

        for (ComptaBudgetaireEvent comptaBudgetaireEvent : rows) {
            if (comptaBudgetaireEvent.estTraite()) {
                result = comptaBudgetaireEvent;
                break;
            }
        }
        if (result == null) {
            throw new NotFoundException("L'évènement correspondant au hash n'a pas été traité :" + hash);
        }
        return result;
    }

}
