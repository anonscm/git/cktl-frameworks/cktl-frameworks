/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.gestion.miseenexecution;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.cocktail.common.support.CktlGFCDate;
import org.cocktail.gfc.budget.preparation.Budget;
import org.cocktail.gfc.budget.validation.BudgetDepense;
import org.cocktail.gfc.budget.validation.BudgetRecette;
import org.cocktail.gfc.budget.validation.support.BudgetDepenseRepository;
import org.cocktail.gfc.budget.validation.support.BudgetRecetteRepository;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.ComptaBudgetaireEventMiseEnExecutionService;
import org.cocktail.gfc.comptabudgetaire.RegleAxesObligatoiresPourEventMessageLigneCreation;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEvent;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventHash;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventType;
import org.cocktail.gfc.support.exception.GfcValidationException;

import com.fasterxml.jackson.core.JsonProcessingException;

public class MiseEnExecutionComptaBudgetaireEventService {

    private static final Logger LOG = Logger.getLogger(MiseEnExecutionComptaBudgetaireEventService.class);
    private ComptaBudgetaireEventMiseEnExecutionService comptaBudgetaireEventMiseEnExecutionService;
    private BudgetDepenseRepository budgetDepenseRepository;
    private BudgetRecetteRepository budgetRecetteRepository;

    public MiseEnExecutionComptaBudgetaireEventService(ComptaBudgetaireEventMiseEnExecutionService comptaBudgetaireEventMiseEnExecutionService,
            BudgetDepenseRepository budgetDepenseRepository, BudgetRecetteRepository budgetRecetteRepository) {
        this.comptaBudgetaireEventMiseEnExecutionService = comptaBudgetaireEventMiseEnExecutionService;
        this.budgetDepenseRepository = budgetDepenseRepository;
        this.budgetRecetteRepository = budgetRecetteRepository;
    }

    /**
     * 
     * @param persIdCreation
     * @param budgetId
     * @param appName
     *            ex. "gfc-Budget"
     * @param eventHash
     *            ex. "budget/gestion/miseenexecution/12" (path rest à l'origine
     *            de la création de l'évènement)
     * @return
     * @throws JsonProcessingException
     * @throws GfcValidationException
     */
    public ComptaBudgetaireEvent creerEvtMiseEnExecution(Long persIdCreation, Budget budget, String appName) throws JsonProcessingException,
            GfcValidationException {
        Date debut = new Date();
        LOG.log(Level.INFO, "Création event mise en exécution - début");
        String libelle = buildLibelle(budget);
        String message = creerMessageMiseEnExecution(budget);
        String srcAppName = appName;

        ComptaBudgetaireEventHash eventHash = new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, budget
                .getIdBudget().id().toString(), CktlGFCDate.now().toDateWithTime(), false);
        ComptaBudgetaireEvent event = comptaBudgetaireEventMiseEnExecutionService.creerEvtMiseEnExecution(persIdCreation, libelle, message, srcAppName, eventHash);
        
        Date fin = new Date();
        LOG.log(Level.INFO, "Création event mise en exécution - fin : " + (fin.getTime() - debut.getTime())/1000. + " sec");
        return event;
    }

    protected String creerMessageMiseEnExecution(Budget budget) throws JsonProcessingException, GfcValidationException {
        List<ComptaBudgetaireEventMessageLigne> lignes = new ArrayList<ComptaBudgetaireEventMessageLigne>();
        List<BudgetDepense> budgetDepenses = budgetDepenseRepository.findByBudget(budget.getIdBudget());
        TypeEcritureEnum typeEcriture;
        if (budget.estInitial()) {
            typeEcriture = TypeEcritureEnum.BI;
        } else {
            typeEcriture = TypeEcritureEnum.BR;
        }
        for (BudgetDepense budgetDepense : budgetDepenses) {
            // creer une ligne pour AE + une ligne pour CP
            if (!Montant.ZERO.equals(budgetDepense.getMontantAE())) {
                ComptaBudgetaireEventMessageLigne ligneAECredit = comptaBudgetaireEventMiseEnExecutionService.creerEvtMessageLigneDepAECredit(
                        budget.getExeOrdre(), typeEcriture, budgetDepense.getEntiteBudgetaireId(), budgetDepense.getOperationId(), budgetDepense.getMontantAE(),
                        budgetDepense.getNatureDepenseId(), budgetDepense.getDestinationDepenseId());
                lignes.add(ligneAECredit);
            }

            if (!Montant.ZERO.equals(budgetDepense.getMontantCP())) {
                ComptaBudgetaireEventMessageLigne ligneCPCredit = comptaBudgetaireEventMiseEnExecutionService.creerEvtMessageLigneDepAECredit(
                        budget.getExeOrdre(), typeEcriture, budgetDepense.getEntiteBudgetaireId(), budgetDepense.getOperationId(), budgetDepense.getMontantCP(),
                        budgetDepense.getNatureDepenseId(), budgetDepense.getDestinationDepenseId());

                lignes.add(ligneCPCredit);
            }
        }

        List<BudgetRecette> budgetRecettes = budgetRecetteRepository.findByBudget(budget.getIdBudget());
        for (BudgetRecette budgetRecette : budgetRecettes) {
            if (!Montant.ZERO.equals(budgetRecette.getMontantRecette())) {

                ComptaBudgetaireEventMessageLigne ligneRECCredit = comptaBudgetaireEventMiseEnExecutionService.creerEvtMessageLigneRECCredit(
                        budget.getExeOrdre(), typeEcriture, budgetRecette.getEntiteBudgetaireId(), budgetRecette.getOperationId(),
                        budgetRecette.getMontantRecette(), budgetRecette.getNatureRecetteId(), budgetRecette.getOrigineRecetteId(),
                        budgetRecette.getSectionRecette());

                lignes.add(ligneRECCredit);
            }
        }

        String json = comptaBudgetaireEventMiseEnExecutionService.convertToJson(lignes);
        return json;
    }

    protected String buildLibelle(Budget budget) {
        return "Mise en exécution du budget " + budget.getExeOrdre() + "/" + budget.getLibelle();
    }

}
