/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.support.querydsl;

import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEvent;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventEtat;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventHash;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventId;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventType;
import org.cocktail.gfc.support.querydsl.QCptbudEvent;

import com.mysema.query.Tuple;
import com.mysema.query.types.MappingProjection;

public class QueryDSLComptaBudgetaireEventProjection extends MappingProjection<ComptaBudgetaireEvent> {

    private static final long serialVersionUID = 1L;
    private static final QCptbudEvent EV = QCptbudEvent.cptbudEvent;

    public QueryDSLComptaBudgetaireEventProjection() {
        super(ComptaBudgetaireEvent.class, EV.idCptbudEvent, EV.idCptbudEventType, EV.srcAppName, EV.srcHash, EV.libelle, EV.etat, EV.dCreation,
                EV.persIdCreation, EV.message);
    }

    @Override
    protected ComptaBudgetaireEvent map(Tuple row) {
        return ComptaBudgetaireEvent.builder().idCptbudEvent(new ComptaBudgetaireEventId(row.get(EV.idCptbudEvent)))
                .cptbudEventType(ComptaBudgetaireEventType.valueOf(row.get(EV.idCptbudEventType))).srcAppName(row.get(EV.srcAppName))
                .srcHash(ComptaBudgetaireEventHash.valueOf(row.get(EV.srcHash))).libelle(row.get(EV.libelle))
                .etat(new ComptaBudgetaireEventEtat(row.get(EV.etat))).dCreation(row.get(EV.dCreation)).persIdCreation(row.get(EV.persIdCreation))
                .message(row.get(EV.message)).build();
    }

}
