/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.support.querydsl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.support.EntiteBudgetaireRepository;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLEntiteBudgetaireRepository;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEcritureMinMax;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventId;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEcritureRepository;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QCptbudEcriture;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.MappingProjection;
import com.mysema.query.types.path.NumberPath;

public class QueryDSLComptaBudgetaireEcritureRepository extends GfcRepository implements ComptaBudgetaireEcritureRepository {

    private final QCptbudEcriture ecriture = QCptbudEcriture.cptbudEcriture;

    private QueryDslJdbcTemplate template;
    private QueryDSLEntiteBudgetaireRepository ebRepository;

    private NumberPath<Long> ecrNumeroMinPath = Expressions.numberPath(Long.class, "ecrNumeroMin");
    private NumberPath<Long> ecrNumeroMaxPath = Expressions.numberPath(Long.class, "ecrNumeroMax");

    public QueryDSLComptaBudgetaireEcritureRepository(QueryDslJdbcTemplate template, QueryDSLEntiteBudgetaireRepository ebRepository) {
        this.template = template;
        this.ebRepository = ebRepository;
    }

    public Long count() {
        SQLQuery query = template.newSqlQuery();
        query.from(ecriture);
        return template.count(query);
    }

    public ComptaBudgetaireEcritureMinMax findEcritureMinMaxByEventId(Long eventId) {
        QueryDSLComptaBudgetaireEcritureMinMaxProjection projection = new QueryDSLComptaBudgetaireEcritureMinMaxProjection();
        SQLQuery query = template.newSqlQuery();
        query.from(ecriture);
        query.where(ecriture.idCptbudEvent.eq(eventId));
        query.groupBy(ecriture.idCptbudEvent);

        List<ComptaBudgetaireEcritureMinMax> rows = template.query(query, projection);
        if (rows.size() > 0) {
            return rows.get(0);
        }
        return null;
    }

    public class QueryDSLComptaBudgetaireEcritureMinMaxProjection extends MappingProjection<ComptaBudgetaireEcritureMinMax> {

        private static final long serialVersionUID = 1L;

        public QueryDSLComptaBudgetaireEcritureMinMaxProjection() {
            super(ComptaBudgetaireEcritureMinMax.class,
                    ecriture.idCptbudEvent,
                    ecriture.ecrNumero.min().as(ecrNumeroMinPath),
                    ecriture.ecrNumero.max().as(ecrNumeroMaxPath));
        }

        @Override
        protected ComptaBudgetaireEcritureMinMax map(Tuple row) {
            return ComptaBudgetaireEcritureMinMax.builder()
                    .idCptbudEvent(new ComptaBudgetaireEventId(row.get(ecriture.idCptbudEvent)))
                    .ecrNumeroMin(row.get(ecrNumeroMinPath))
                    .ecrNumeroMax(row.get(ecrNumeroMaxPath))
                    .build();
        }

    }
    
    
    public List<EntiteBudgetaire> findEntiteBudgetaireByExercice(int exercice) {
        Set<Long> ids = findEntiteBudgetaireIdByExercice(exercice);
        
        List<EntiteBudgetaire> ebs = new ArrayList<EntiteBudgetaire>();
        for (Long id : ids) {
            EntiteBudgetaireId entiteBudgetaireId = new EntiteBudgetaireId(id);
            Optional <EntiteBudgetaire> eb = ebRepository.findByIdAndExercice(entiteBudgetaireId, exercice); 
            
            if (eb.isPresent()) {
                ebs.add(eb.get());
            }
        }
        
        return ebs;
    }

    
   private Set<Long> findEntiteBudgetaireIdByExercice(int exercice) {
        SQLQuery query = template.newSqlQuery();
        query.from(ecriture)
                .where(ecriture.exeOrdre.eq(exercice)
                        .and(ecriture.aeCredit.isNotNull().and(ecriture.aeCredit.ne(new BigDecimal(0)))
                                .or(ecriture.cpCredit.isNotNull().and(ecriture.cpCredit.ne(new BigDecimal(0))))));
        List<Long> idList = template.query(query, ecriture.idAdmEb);
        Set<Long> ids = new HashSet<Long>(idList); 
        return ids;      
    }

}
