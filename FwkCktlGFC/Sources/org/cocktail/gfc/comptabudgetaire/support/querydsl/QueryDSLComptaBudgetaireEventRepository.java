/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.support.querydsl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEvent;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventHash;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventId;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEventRepository;
import org.cocktail.gfc.support.querydsl.GfcRepository;
import org.cocktail.gfc.support.querydsl.QCptbudEvent;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.SqlInsertWithKeyCallback;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;

public class QueryDSLComptaBudgetaireEventRepository extends GfcRepository implements ComptaBudgetaireEventRepository {

    private final QCptbudEvent event = QCptbudEvent.cptbudEvent;

    private QueryDslJdbcTemplate template;
    private Path<?>[] columnsForInsert;

    private QueryDSLComptaBudgetaireEventProjection projection = new QueryDSLComptaBudgetaireEventProjection();

    public QueryDSLComptaBudgetaireEventRepository(QueryDslJdbcTemplate template) {
        this.template = template;
        initDMLColumns();
    }

    @SuppressWarnings("unchecked")
    private void initDMLColumns() {
        List<Path<?>> forInsert = new ArrayList<Path<?>>(Arrays.asList(event.idCptbudEvent, event.idCptbudEventType, event.dCreation,
                event.persIdCreation, event.srcAppName, event.srcHash, event.libelle, event.message));

        this.columnsForInsert = forInsert.toArray(new Path[forInsert.size()]);

    }

    public ComptaBudgetaireEvent createComptaBudgetaireEvent(ComptaBudgetaireEvent newEvent) {
        Long newId = template.insertWithKey(event, createInsertCallback(newEvent));
        newEvent.setIdCptbudEvent(new ComptaBudgetaireEventId(newId));
        return newEvent;
    }

    private SqlInsertWithKeyCallback<Long> createInsertCallback(final ComptaBudgetaireEvent newEvent) {
        return new SqlInsertWithKeyCallback<Long>() {
            public Long doInSqlInsertWithKeyClause(SQLInsertClause insert) throws SQLException {
                List<Object> values = new ArrayList<Object>();
                values.add(nextval(event));
                values.addAll(eventAsList(newEvent));
                return insert.columns(columnsForInsert).values(values.toArray()).executeWithKey(event.idCptbudEvent);
            }
        };
    }

    // Utils
    private List<Object> eventAsList(ComptaBudgetaireEvent newEvent) {
        List<Object> values = new ArrayList<Object>();
        values.add(newEvent.getCptbudEventType().getId());
        values.add(newEvent.getDCreation());
        values.add(newEvent.getPersIdCreation());
        values.add(newEvent.getSrcAppName());
        values.add(newEvent.getSrcHash().value());
        values.add(newEvent.getLibelle());
        values.add(newEvent.getMessage());
        return values;
    }

    private SQLQuery getQuery() {
        SQLQuery sqlQuery = template.newSqlQuery().from(event);
        // ajout d'un tri par defaut
        sqlQuery.orderBy(orderByIdCptbudEventDesc());
        return sqlQuery;
    }

    // Orders
    private OrderSpecifier<Long> orderByIdCptbudEventDesc() {
        return event.idCptbudEvent.desc();
    }

    public List<ComptaBudgetaireEvent> findByHashLike(ComptaBudgetaireEventHash hash) {
        SQLQuery sqlQuery = getQuery();
        addBySrcHashLikeClause(sqlQuery, hash);
        List<ComptaBudgetaireEvent> rows = template.query(sqlQuery, projection);
        return rows;
    }

    private SQLQuery addBySrcHashLikeClause(SQLQuery sqlQuery, ComptaBudgetaireEventHash hash) {
        String hashString = hash.valueWithoutSuffix();
        sqlQuery = sqlQuery.where(event.srcHash.like(hashString + "%"));
        return sqlQuery;
    }

}
