/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire;

import org.cocktail.common.support.CktlGFCDate;
import org.cocktail.common.support.DefaultValidationNotificationHandler;
import org.cocktail.common.support.ValidationNotificationHandler;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEvent;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventHash;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventType;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.ComptaBudgetaireEventMessageLigneBuilder;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEventRepository;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.support.exception.GfcValidationException;

/**
 * Logiquement ne pas utiliser cette classe à partir de la couche
 * applicationServices. Cf. MiseEnExecutionComptaBudgetaireEventService
 * 
 * @author Rodolphe PRIN <rodolphe.prin at asso-cocktail.fr>
 * 
 */
public class ComptaBudgetaireEventMiseEnExecutionService extends ComptaBudgetaireEventService {

    public ComptaBudgetaireEventMiseEnExecutionService(ComptaBudgetaireEventRepository comptaBudgetaireEventRepository) {
        super(comptaBudgetaireEventRepository);
    }

    public ComptaBudgetaireEvent creerEvtMiseEnExecution(Long persIdCreation, String libelle, String message, String srcAppName,
            ComptaBudgetaireEventHash srcHash) {
        return creerEvt(persIdCreation, libelle, message, srcAppName, srcHash, ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION);
    }

    public ComptaBudgetaireEventMessageLigne creerEvtMessageLigneDepAECredit(Integer exeOrdre, TypeEcritureEnum typeEcriture, EntiteBudgetaireId idAdmEb,
            OperationId idOpeOperation, Montant montant, NatureDepenseId idAdmNatureDepense, DestinationDepenseId idAdmDestinationDepense)
            throws GfcValidationException {
        ComptaBudgetaireEventMessageLigneBuilder builder = ComptaBudgetaireEventMessageLigne.builder();
        builder = addAxesCommuns(builder, exeOrdre, typeEcriture, idAdmEb, idOpeOperation);
        builder = addAxesDepense(builder, idAdmNatureDepense, idAdmDestinationDepense);
        builder = addAE(builder);
        builder = addCredit(builder);
        builder = builder.montant(montant);
        ComptaBudgetaireEventMessageLigne ligne = builder.build();
        validerLignePourCreation(ligne);
        return ligne;
    }

    public ComptaBudgetaireEventMessageLigne creerEvtMessageLigneDepAEDebit(Integer exeOrdre, TypeEcritureEnum typeEcriture, EntiteBudgetaireId idAdmEb, OperationId idOpeOperation,
            Montant montant, NatureDepenseId idAdmNatureDepense, DestinationDepenseId idAdmDestinationDepense) throws GfcValidationException {
        ComptaBudgetaireEventMessageLigneBuilder builder = ComptaBudgetaireEventMessageLigne.builder();
        builder = addAxesCommuns(builder, exeOrdre, typeEcriture, idAdmEb, idOpeOperation);
        builder = addAxesDepense(builder, idAdmNatureDepense, idAdmDestinationDepense);
        builder = addAE(builder);
        builder = addDebit(builder);
        builder = builder.montant(montant);
        ComptaBudgetaireEventMessageLigne ligne = builder.build();
        validerLignePourCreation(ligne);
        return ligne;
    }

    public ComptaBudgetaireEventMessageLigne creerEvtMessageLigneDepCPCredit(Integer exeOrdre, TypeEcritureEnum typeEcriture, EntiteBudgetaireId idAdmEb,
            OperationId idOpeOperation, Montant montant, NatureDepenseId idAdmNatureDepense, DestinationDepenseId idAdmDestinationDepense)
            throws GfcValidationException {
        ComptaBudgetaireEventMessageLigneBuilder builder = ComptaBudgetaireEventMessageLigne.builder();
        builder = addAxesCommuns(builder, exeOrdre, typeEcriture, idAdmEb, idOpeOperation);
        builder = addAxesDepense(builder, idAdmNatureDepense, idAdmDestinationDepense);
        builder = addCP(builder);
        builder = addCredit(builder);
        builder = builder.montant(montant);
        ComptaBudgetaireEventMessageLigne ligne = builder.build();
        validerLignePourCreation(ligne);
        return ligne;
    }

    public ComptaBudgetaireEventMessageLigne creerEvtMessageLigneDepCPDebit(Integer exeOrdre, TypeEcritureEnum typeEcriture, EntiteBudgetaireId idAdmEb, OperationId idOpeOperation,
            Montant montant, NatureDepenseId idAdmNatureDepense, DestinationDepenseId idAdmDestinationDepense) throws GfcValidationException {
        ComptaBudgetaireEventMessageLigneBuilder builder = ComptaBudgetaireEventMessageLigne.builder();
        builder = addAxesCommuns(builder, exeOrdre, typeEcriture, idAdmEb, idOpeOperation);
        builder = addAxesDepense(builder, idAdmNatureDepense, idAdmDestinationDepense);
        builder = addCP(builder);
        builder = addDebit(builder);
        builder = builder.montant(montant);
        ComptaBudgetaireEventMessageLigne ligne = builder.build();
        validerLignePourCreation(ligne);
        return ligne;
    }

    public ComptaBudgetaireEventMessageLigne creerEvtMessageLigneRECCredit(Integer exeOrdre, TypeEcritureEnum typeEcriture, EntiteBudgetaireId idAdmEb, OperationId idOpeOperation,
            Montant montant, NatureRecetteId idAdmNatureRecette, OrigineRecetteId idAdmOrigineRecette, SectionRecette sectionRecette)
            throws GfcValidationException {
        ComptaBudgetaireEventMessageLigneBuilder builder = ComptaBudgetaireEventMessageLigne.builder();
        builder = addAxesCommuns(builder, exeOrdre, typeEcriture, idAdmEb, idOpeOperation);
        builder = addAxesRecette(builder, idAdmNatureRecette, idAdmOrigineRecette, sectionRecette);
        builder = addREC(builder);
        builder = addCredit(builder);
        builder = builder.montant(montant);
        ComptaBudgetaireEventMessageLigne ligne = builder.build();
        validerLignePourCreation(ligne);
        return ligne;
    }

    public ComptaBudgetaireEventMessageLigne creerEvtMessageLigneRECDebit(Integer exeOrdre, TypeEcritureEnum typeEcriture, EntiteBudgetaireId idAdmEb, OperationId idOpeOperation,
            Montant montant, NatureRecetteId idAdmNatureRecette, OrigineRecetteId idAdmOrigineRecette, SectionRecette sectionRecette)
            throws GfcValidationException {
        ComptaBudgetaireEventMessageLigneBuilder builder = ComptaBudgetaireEventMessageLigne.builder();
        builder = addAxesCommuns(builder, exeOrdre, typeEcriture, idAdmEb, idOpeOperation);
        builder = addAxesRecette(builder, idAdmNatureRecette, idAdmOrigineRecette, sectionRecette);
        builder = addREC(builder);
        builder = addDebit(builder);
        builder = builder.montant(montant);
        ComptaBudgetaireEventMessageLigne ligne = builder.build();
        validerLignePourCreation(ligne);
        return ligne;
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addAxesCommuns(ComptaBudgetaireEventMessageLigneBuilder builder, Integer exeOrdre,
            TypeEcritureEnum typeEcriture, EntiteBudgetaireId idAdmEb, OperationId idOpeOperation) {
        return builder
                .exeOrdre(exeOrdre)
                .typeEcriture(typeEcriture)
                .idAdmEb(idAdmEb)
                .idOpeOperation(idOpeOperation);
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addAxesDepense(ComptaBudgetaireEventMessageLigneBuilder builder,
            NatureDepenseId idAdmNatureDepense, DestinationDepenseId idAdmDestinationDepense) {
        return builder.idAdmNatureDepense(idAdmNatureDepense).idAdmDestinationDepense(idAdmDestinationDepense);
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addAxesRecette(ComptaBudgetaireEventMessageLigneBuilder builder,
            NatureRecetteId idAdmNatureRecette, OrigineRecetteId idAdmOrigineRecette, SectionRecette sectionRecette) {
        return builder.idAdmNatureRecette(idAdmNatureRecette).idAdmOrigineRecette(idAdmOrigineRecette).section(sectionRecette);
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addAE(ComptaBudgetaireEventMessageLigneBuilder builder) {
        return builder.typeMontant(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.AE);
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addCP(ComptaBudgetaireEventMessageLigneBuilder builder) {
        return builder.typeMontant(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.CP);
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addREC(ComptaBudgetaireEventMessageLigneBuilder builder) {
        return builder.typeMontant(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.REC);
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addCredit(ComptaBudgetaireEventMessageLigneBuilder builder) {
        return builder.sens(ComptaBudgetaireEventMessageLigne.SensEnum.CREDIT);
    }

    protected ComptaBudgetaireEventMessageLigneBuilder addDebit(ComptaBudgetaireEventMessageLigneBuilder builder) {
        return builder.sens(ComptaBudgetaireEventMessageLigne.SensEnum.DEBIT);
    }

}
