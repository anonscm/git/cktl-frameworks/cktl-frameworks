/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.cocktail.common.support.CktlGFCDate;
import org.cocktail.common.support.DefaultValidationNotificationHandler;
import org.cocktail.common.support.ValidationNotificationHandler;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEvent;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventHash;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.SensEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeMontantEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventType;
import org.cocktail.gfc.comptabudgetaire.representations.ComptaBudgetaireEventMessageLigneRepresentation;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEventRepository;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.support.exception.GfcValidationException;
import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ComptaBudgetaireEventService {

    private static final Logger LOG = Logger.getLogger(ComptaBudgetaireEventService.class);
    private static final String NULL_VALUE = "null";
    private ComptaBudgetaireEventRepository comptaBudgetaireEventRepository;

    public static final long TYPE_EVENEMENT_MISE_EN_EXECUTION = 1L;

    public ComptaBudgetaireEventService(ComptaBudgetaireEventRepository comptaBudgetaireEventRepository) {
        this.comptaBudgetaireEventRepository = comptaBudgetaireEventRepository;
    }

    public String convertToJson(List<ComptaBudgetaireEventMessageLigne> lignes) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper.writeValueAsString(ComptaBudgetaireEventMessageLigneRepresentation.asList(lignes));

    }

    public List<ComptaBudgetaireEventMessageLigne> convertFromJson(String jsonArray) throws JsonParseException, IOException, GfcValidationException {
        List<ComptaBudgetaireEventMessageLigne> list = new ArrayList<ComptaBudgetaireEventMessageLigne>();
        // TODO RPR 26/03/2015: pas terrible comme implementation, passer par
        // des deserializer ?
        JsonFactory f = new JsonFactory();
        JsonParser jp = f.createParser(jsonArray);
        jp.nextToken(); // will return JsonToken.START_OBJECT (verify?)
        while (jp.nextToken() == JsonToken.START_OBJECT) {
            list.add(createFromJsonParser(jp));
        }
        jp.close();
        return list;
    }

    protected ComptaBudgetaireEvent creerEvenementInRepository(ComptaBudgetaireEvent evt) {
        Date debut = new Date();
        LOG.log(Level.INFO, "Insertion event - début");
        ComptaBudgetaireEvent event = comptaBudgetaireEventRepository.createComptaBudgetaireEvent(evt);
        Date fin = new Date();
        LOG.log(Level.INFO, "Insertion event - fin ; temps = " + (fin.getTime() - debut.getTime())/1000. + " sec");
        return event;
    }

    private ComptaBudgetaireEventMessageLigne createFromJsonParser(JsonParser jp) throws JsonParseException, IOException, GfcValidationException {
        ComptaBudgetaireEventMessageLigne.ComptaBudgetaireEventMessageLigneBuilder ligneBuilder = ComptaBudgetaireEventMessageLigne.builder();
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldname = jp.getCurrentName();
            jp.nextToken(); // move to value, or START_OBJECT/START_ARRAY
            if ("exeOrdre".equals(fieldname)) { // contains an object
                ligneBuilder = ligneBuilder.exeOrdre(Integer.valueOf(jp.getText()));
            } else if ("typeEcriture".equals(fieldname)) {
                ligneBuilder = ligneBuilder.typeEcriture(TypeEcritureEnum.valueOf(jp.getText()));
            } else if ("typeMontant".equals(fieldname)) {
                ligneBuilder = ligneBuilder.typeMontant(TypeMontantEnum.valueOf(jp.getText()));
            } else if ("sens".equals(fieldname)) {
                ligneBuilder = ligneBuilder.sens("C".equals(jp.getText()) ? SensEnum.CREDIT : ("D".equals(jp.getText()) ? SensEnum.DEBIT : null));
            } else if ("montant".equals(fieldname)) {
                ligneBuilder = ligneBuilder.montant(new Montant(jp.getDecimalValue()));
            } else if ("idAdmEb".equals(fieldname)) {
                ligneBuilder = ligneBuilder.idAdmEb(new EntiteBudgetaireId(jp.getLongValue()));
            } else if ("idOpeOperation".equals(fieldname)) {
                if (jp.getText() != null && !jp.getText().equals(NULL_VALUE)) {
                    ligneBuilder = ligneBuilder.idOpeOperation(new OperationId(jp.getLongValue()));
                }
            } else if ("idAdmNatureDepense".equals(fieldname)) {
                if (jp.getText() != null && !jp.getText().equals(NULL_VALUE)) {
                    ligneBuilder = ligneBuilder.idAdmNatureDepense(new NatureDepenseId(jp.getLongValue()));
                }
            } else if ("idAdmDestinationDepense".equals(fieldname)) {
                if (jp.getText() != null && !jp.getText().equals(NULL_VALUE)) {
                    ligneBuilder = ligneBuilder.idAdmDestinationDepense(new DestinationDepenseId(jp.getLongValue()));
                }
            } else if ("idAdmNatureRecette".equals(fieldname)) {
                if (jp.getText() != null && !jp.getText().equals(NULL_VALUE)) {
                    ligneBuilder = ligneBuilder.idAdmNatureRecette(new NatureRecetteId(jp.getLongValue()));
                }
            } else if ("idAdmOrigineRecette".equals(fieldname)) {
                if (jp.getText() != null && !jp.getText().equals(NULL_VALUE)) {
                    ligneBuilder = ligneBuilder.idAdmOrigineRecette(new OrigineRecetteId(jp.getLongValue()));
                }
            } else if ("section".equals(fieldname)) {
                if (jp.getText() != null && !jp.getText().equals(NULL_VALUE)) {
                    ligneBuilder = ligneBuilder.section(SectionRecetteCacheService.findByAbreviation(jp.getText()));
                }
            } else {
                throw new IllegalStateException("Unrecognized field '" + fieldname + "'!");
            }
        }
        ComptaBudgetaireEventMessageLigne ligne = ligneBuilder.build();
        validerLignePourCreation(ligne);
        return ligne;
    }

    public void validerLignePourCreation(ComptaBudgetaireEventMessageLigne ligne) throws GfcValidationException {
        ValidationNotificationHandler handler = new DefaultValidationNotificationHandler();
        ComptaBudgetaireEventMessageLignePourCreationValidator creationValidator = new ComptaBudgetaireEventMessageLignePourCreationValidator(handler);
        if (!creationValidator.isValid(ligne)) {
            throw new GfcValidationException("Impossible de créer la ligne du message", handler.errors());
        }
    }

    public ComptaBudgetaireEvent creerAnnulationEvt(Long persIdCreation, ComptaBudgetaireEvent oldEvent) throws JsonParseException, IOException,
            GfcValidationException {
        List<ComptaBudgetaireEventMessageLigne> lignesOrigines = convertFromJson(oldEvent.getMessage());
        String libelle = "[Annulation] " + oldEvent.getLibelle();
        String srcAppName = oldEvent.getSrcAppName();
        ComptaBudgetaireEventHash srcHash = new ComptaBudgetaireEventHash(oldEvent.getSrcHash().getEventType(), oldEvent.getSrcHash().getBaseHash(),
                oldEvent.getSrcHash().getSuffix(), Boolean.TRUE);
        String message = convertToJson(creerLignesAnnulation(lignesOrigines));
        ComptaBudgetaireEvent newEvent = creerEvt(persIdCreation, libelle, message, srcAppName, srcHash, oldEvent.getCptbudEventType());
        return newEvent;

    }

    public List<ComptaBudgetaireEventMessageLigne> creerLignesAnnulation(List<ComptaBudgetaireEventMessageLigne> lignesOrigines)
            throws GfcValidationException {
        List<ComptaBudgetaireEventMessageLigne> lignesNew = new ArrayList<ComptaBudgetaireEventMessageLigne>();
        for (ComptaBudgetaireEventMessageLigne ligneOrigine : lignesOrigines) {
            lignesNew.add(creerLigneAnnulation(ligneOrigine));
        }
        return lignesNew;
    }

    public ComptaBudgetaireEventMessageLigne creerLigneAnnulation(ComptaBudgetaireEventMessageLigne ligneOrigine) throws GfcValidationException {
        ComptaBudgetaireEventMessageLigne ligneNew = ComptaBudgetaireEventMessageLigne.builder().build();
        BeanUtils.copyProperties(ligneOrigine, ligneNew);
        ligneNew.setMontant(new Montant(ligneOrigine.getMontant().value().negate()));
        validerLignePourCreation(ligneNew);
        return ligneNew;
    }

    protected ComptaBudgetaireEvent creerEvt(Long persIdCreation, String libelle, String message, String srcAppName,
            ComptaBudgetaireEventHash srcHash, ComptaBudgetaireEventType type) {
        String dCreation = CktlGFCDate.now().toDateWithTime();
        ComptaBudgetaireEvent evt = ComptaBudgetaireEvent.builder().cptbudEventType(type).persIdCreation(persIdCreation).dCreation(dCreation)
                .libelle(libelle).message(message).srcAppName(srcAppName).srcHash(srcHash).build();
        return creerEvenementInRepository(evt);
    }

}
