/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.beans;

public class ComptaBudgetaireEventHash {

    private String baseHash;
    private ComptaBudgetaireEventType eventType;
    private String suffix;
    private Boolean annulation = Boolean.FALSE;
    public static final String FLAG_ANNULATION = "[Annulation]";

    public ComptaBudgetaireEventHash(ComptaBudgetaireEventType eventType, String baseHash, String suffix, Boolean annulation) {
        if (eventType == null) {
            throw new IllegalArgumentException("eventType est obligatoire");
        }
        if (baseHash == null || baseHash.length() == 0) {
            throw new IllegalArgumentException("baseHash est obligatoire");
        }
        if (suffix == null || suffix.length() == 0) {
            throw new IllegalArgumentException("suffix est obligatoire");
        }
        this.eventType = eventType;
        this.baseHash = baseHash;
        this.suffix = suffix;
        if (annulation != null) {
            this.annulation = annulation;
        }
    }

    public String value() {
        String res = valueWithoutSuffix();
        res += "$" + suffix;
        return res;
    }

    public String valueWithoutSuffix() {
        String res = eventType.getKey();
        res += "$" + baseHash;
        if (annulation) {
            res += FLAG_ANNULATION;
        }
        return res;
    }

    public Boolean getAnnulation() {
        return annulation;
    }

    public void setAnnulation(Boolean val) {
        annulation = val;
    }

    public static final ComptaBudgetaireEventHash valueOf(String hashString) {
        if (hashString == null || hashString.length() == 0) {
            throw new IllegalArgumentException("hashString ne peut pas être null ou vide");
        }
        String[] strings = hashString.split("\\$");
        if (strings.length != 3) {
            throw new IllegalArgumentException("hashString ne respecte pas le bon format : eventType$baseHash$suffix (" + hashString + ")");
        }
        String eventTypeAsString = strings[0];
        String baseHash = strings[1];
        String suffix = strings[2];
        Boolean annulation = false;
        if (baseHash.endsWith(FLAG_ANNULATION)) {
            annulation = true;
            baseHash = baseHash.substring(0, baseHash.indexOf(FLAG_ANNULATION));
        }
        return new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.valueOf(eventTypeAsString), baseHash, suffix, annulation);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((annulation == null) ? 0 : annulation.hashCode());
        result = prime * result + ((baseHash == null) ? 0 : baseHash.hashCode());
        result = prime * result + ((eventType == null) ? 0 : eventType.hashCode());
        result = prime * result + ((suffix == null) ? 0 : suffix.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComptaBudgetaireEventHash other = (ComptaBudgetaireEventHash) obj;
        if (annulation == null) {
            if (other.annulation != null)
                return false;
        } else if (!annulation.equals(other.annulation))
            return false;
        if (baseHash == null) {
            if (other.baseHash != null)
                return false;
        } else if (!baseHash.equals(other.baseHash))
            return false;
        if (eventType == null) {
            if (other.eventType != null)
                return false;
        } else if (!eventType.equals(other.eventType))
            return false;
        if (suffix == null) {
            if (other.suffix != null)
                return false;
        } else if (!suffix.equals(other.suffix))
            return false;
        return true;
    }

    public String getBaseHash() {
        return baseHash;
    }

    public ComptaBudgetaireEventType getEventType() {
        return eventType;
    }

    public String getSuffix() {
        return suffix;
    }

}
