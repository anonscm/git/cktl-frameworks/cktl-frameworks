/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.beans;

import javax.annotation.Nullable;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Builder;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;

@Getter
@Setter
public class ComptaBudgetaireEventMessageLigne {
    public static enum SensEnum {
        DEBIT, CREDIT
    }

    public static enum TypeMontantEnum {
        AE, CP, REC
    }

    @Getter
    public enum TypeEcritureEnum {
        /**
         * BUDGET_INITIAL
         */
        BI("BUDGET_INITIAL"),
        /**
         * BUDGET_RECTIFICATIF
         */
        BR("BUDGET_RECTIFICATIF"),
        /**
         * VIREMENT_ALLOCATION_INITIALE
         */
        VAI("VIREMENT_ALLOCATION_INITIALE"),
        /**
         * VIREMENT_REALLOCATION
         */
        VR("VIREMENT_REALLOCATION"),
        /**
         * VIREMENT_ALLOCATION_URGENCE
         */
        VAU("VIREMENT_ALLOCATION_URGENCE");        
        
        private String libLong;

        private TypeEcritureEnum(String libLong) {
            this.libLong = libLong;
        }
    }

    private Integer exeOrdre;
    private TypeEcritureEnum typeEcriture;
    private TypeMontantEnum typeMontant;
    private SensEnum sens;
    private Montant montant;
    private EntiteBudgetaireId idAdmEb;
    @Nullable
    private OperationId idOpeOperation;
    @Nullable
    private NatureDepenseId idAdmNatureDepense;
    @Nullable
    private DestinationDepenseId idAdmDestinationDepense;
    @Nullable
    private NatureRecetteId idAdmNatureRecette;
    @Nullable
    private OrigineRecetteId idAdmOrigineRecette;
    @Nullable
    private SectionRecette section;

    @Builder
    public ComptaBudgetaireEventMessageLigne(
            @NonNull Integer exeOrdre, @NonNull TypeEcritureEnum typeEcriture, @NonNull TypeMontantEnum typeMontant,
            @NonNull SensEnum sens, @NonNull Montant montant, @NonNull EntiteBudgetaireId idAdmEb, 
            OperationId idOpeOperation, NatureDepenseId idAdmNatureDepense, DestinationDepenseId idAdmDestinationDepense,
            NatureRecetteId idAdmNatureRecette, OrigineRecetteId idAdmOrigineRecette, SectionRecette section) {
        super();
        this.exeOrdre = exeOrdre;
        this.typeMontant = typeMontant;
        this.sens = sens;
        this.montant = montant;
        this.idAdmEb = idAdmEb;
        this.typeEcriture = typeEcriture;
        this.idOpeOperation = idOpeOperation;
        this.idAdmNatureDepense = idAdmNatureDepense;
        this.idAdmDestinationDepense = idAdmDestinationDepense;
        this.idAdmNatureRecette = idAdmNatureRecette;
        this.idAdmOrigineRecette = idAdmOrigineRecette;
        this.section = section;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((exeOrdre == null) ? 0 : exeOrdre.hashCode());
        result = prime * result + ((idAdmDestinationDepense == null) ? 0 : idAdmDestinationDepense.hashCode());
        result = prime * result + ((idAdmEb == null) ? 0 : idAdmEb.hashCode());
        result = prime * result + ((idAdmNatureDepense == null) ? 0 : idAdmNatureDepense.hashCode());
        result = prime * result + ((idAdmNatureRecette == null) ? 0 : idAdmNatureRecette.hashCode());
        result = prime * result + ((idAdmOrigineRecette == null) ? 0 : idAdmOrigineRecette.hashCode());
        result = prime * result + ((idOpeOperation == null) ? 0 : idOpeOperation.hashCode());
        result = prime * result + ((montant == null) ? 0 : montant.hashCode());
        result = prime * result + ((section == null) ? 0 : section.hashCode());
        result = prime * result + ((sens == null) ? 0 : sens.hashCode());
        result = prime * result + ((typeEcriture == null) ? 0 : typeEcriture.hashCode());
        result = prime * result + ((typeMontant == null) ? 0 : typeMontant.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComptaBudgetaireEventMessageLigne other = (ComptaBudgetaireEventMessageLigne) obj;
        if (exeOrdre == null) {
            if (other.exeOrdre != null)
                return false;
        } else if (!exeOrdre.equals(other.exeOrdre))
            return false;
        if (idAdmDestinationDepense == null) {
            if (other.idAdmDestinationDepense != null)
                return false;
        } else if (!idAdmDestinationDepense.equals(other.idAdmDestinationDepense))
            return false;
        if (idAdmEb == null) {
            if (other.idAdmEb != null)
                return false;
        } else if (!idAdmEb.equals(other.idAdmEb))
            return false;
        if (idAdmNatureDepense == null) {
            if (other.idAdmNatureDepense != null)
                return false;
        } else if (!idAdmNatureDepense.equals(other.idAdmNatureDepense))
            return false;
        if (idAdmNatureRecette == null) {
            if (other.idAdmNatureRecette != null)
                return false;
        } else if (!idAdmNatureRecette.equals(other.idAdmNatureRecette))
            return false;
        if (idAdmOrigineRecette == null) {
            if (other.idAdmOrigineRecette != null)
                return false;
        } else if (!idAdmOrigineRecette.equals(other.idAdmOrigineRecette))
            return false;
        if (idOpeOperation == null) {
            if (other.idOpeOperation != null)
                return false;
        } else if (!idOpeOperation.equals(other.idOpeOperation))
            return false;
        if (montant == null) {
            if (other.montant != null)
                return false;
        } else if (!montant.equals(other.montant))
            return false;
        if (section == null) {
            if (other.section != null)
                return false;
        } else if (!section.equals(other.section))
            return false;
        if (sens != other.sens)
            return false;
        if (typeEcriture != other.typeEcriture)
            return false;
        if (typeMontant != other.typeMontant)
            return false;
        return true;
    }

}
