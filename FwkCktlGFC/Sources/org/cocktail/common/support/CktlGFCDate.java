/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.common.support;

import lombok.Getter;
import lombok.Setter;

import org.cocktail.gfc.support.exception.GfcValidationException;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

@Getter
@Setter
public class CktlGFCDate {
    private DateTime date;

    public CktlGFCDate() {
        date = null;
    }

    public CktlGFCDate(Object date) throws GfcValidationException {
        setDate(date);
    }

    public static CktlGFCDate premierJanvier(int exercice) throws GfcValidationException {
        return new CktlGFCDate(new DateTime(exercice, 1, 1, 0, 0).toLocalDate());
    }

    /**
     * 
     * @return Une date sans les données d'heures
     */
    public static CktlGFCDate today() {
        return new CktlGFCDate().getToday();
    }

    /**
     * 
     * @return Une date avec les données d'heures.
     */

    public static CktlGFCDate now() {
        return new CktlGFCDate().getNow();
    }

    public static CktlGFCDate yesterday() {
        try {
            return new CktlGFCDate(today().getDate().plusDays(-1));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static CktlGFCDate tomorrow() {
        try {
            return new CktlGFCDate(today().getDate().plusDays(1));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public CktlGFCDate getToday() {
        try {
            return new CktlGFCDate(new DateTime().toLocalDate());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public CktlGFCDate getNow() {
        try {
            return new CktlGFCDate(new DateTime().toLocalDateTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setDate(Object date) throws GfcValidationException {
        if (date == null) {
            this.date = null;
        } else if (date instanceof String) {
            setStrDate((String) date);
        } else if (date instanceof DateTime) {
            setDateTime((DateTime) date);
        } else if (date instanceof LocalDate) {
            setDateTime(new DateTime(((LocalDate) date).toDateMidnight()));
        } else if (date instanceof LocalDateTime) {
            setDateTime((((LocalDateTime) date)).toDateTime());
        } else {
            throw new GfcValidationException("Format de date non reconnu");
        }
    }

    private void setStrDate(String strDate) {
        if (strDate == null || strDate.length() == 0) {
            this.date = null;
        } else {
            this.date = new DateTime(strDate);
        }
    }

    private void setDateTime(DateTime dateTime) {
        this.date = dateTime;
    }

    public boolean isNull() {
        return date == null;
    }

    public boolean isNotNull() {
        return !isNull();
    }

    /**
     * 
     * @param uneDate
     * @return true si la date est antérieure <b>ou égale</b> à la date du jour.
     * @throws GfcValidationException
     */
    public boolean isBeforeToday() {
        return isBefore(getToday());
    }

    /**
     * 
     * @param uneDate
     * @return true si la date est postérieure <b>ou égale</b> à la date du
     *         jour.
     * @throws GfcValidationException
     */
    public boolean isAfterToday() {
        return isAfter(getToday());
    }

    /**
     * 
     * @param uneDate
     * @return true si la date est antérieure <b>ou égale</b> à la date passée
     *         en paramètre.
     * @throws GfcValidationException
     */
    public boolean isBefore(CktlGFCDate uneDate) {
        if (date == null) {
            return false;
        }
        if (uneDate == null) {
            uneDate = getToday();
        }
        return date.isEqual(uneDate.date) || date.isBefore(uneDate.date);
    }

    /**
     * 
     * @param uneDate
     * @return true si la date est postérieure <b>ou égale</b> à la date passée
     *         en paramètre.
     * @throws GfcValidationException
     */
    public boolean isAfter(CktlGFCDate uneDate) {
        if (date == null) {
            return false;
        }
        if (uneDate == null) {
            uneDate = getToday();
        }
        return date.isEqual(uneDate.date) || date.isAfter(uneDate.date);
    }
    
    public boolean isStrictlyAfter(CktlGFCDate uneDate) {
        if (date == null) {
            return false;
        }
        if (uneDate == null) {
            uneDate = getToday();
        }
        return date.isAfter(uneDate.date);
    }

    public String toDateWithoutTime() {
        if (date == null) {
            return null;
        }
        return date.toString(DateConversionUtil.DATE_WITHOUT_TIME_FORMATTER);
    }

    public String toDateWithTime() {
        if (date == null) {
            return null;
        }
        return date.toString(DateConversionUtil.DATE_WITH_TIME_FORMATTER);
    }

    @Override
    public String toString() {
        if (date == null) {
            return null;
        }
        return date.toString();
    }
}
