/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.common.support;

import org.cocktail.gfc.support.exception.GfcValidationException;

public class CktlDateValidator {

    CktlGFCDate date;

    public CktlDateValidator(CktlGFCDate date) {
        this.date = date;
    }

    public CktlDateValidator checkIsNotNull() throws GfcValidationException {
        return checkIsNotNull(date);
    }

    public CktlDateValidator checkIsNotNull(CktlGFCDate uneDate) throws GfcValidationException {
        return checkIsNotNull(uneDate, "La date ne peut pas être vide");
    }

    public CktlDateValidator checkIsNotNull(CktlGFCDate uneDate, String errorString) throws GfcValidationException {
        if (uneDate == null || uneDate.isNull()) {
            throw new GfcValidationException(errorString);
        }
        return this;
    }

    public CktlDateValidator checkIsBeforeToday() throws GfcValidationException {
        if (!date.isBeforeToday()) {
            throw new GfcValidationException("La date doit être inférieure ou égale à la date du jour");
        }
        return this;
    }

    public CktlDateValidator checkIsAfterToday() throws GfcValidationException {
        if (!date.isAfterToday()) {
            throw new GfcValidationException("La date doit être supérieure ou égale à la date du jour");
        }
        return this;
    }

    public CktlDateValidator checkIsAfter(CktlGFCDate uneDate) throws GfcValidationException {
        return checkIsAfter(date, "La date doit être supérieure ou égale au " + uneDate.toDateWithoutTime());
    }

    public CktlDateValidator checkIsAfter(CktlGFCDate uneDate, String errorString) throws GfcValidationException {
        if (!date.isAfter(uneDate)) {
            throw new GfcValidationException(errorString);
        }
        return this;
    }

    public CktlDateValidator checkIsStrictlyAfter(CktlGFCDate uneDate, String errorString) throws GfcValidationException {
        if (!date.isStrictlyAfter(uneDate)) {
            throw new GfcValidationException(errorString);
        }
        return this;
    }

}
