/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.common.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class DefaultValidationNotificationHandler implements ValidationNotificationHandler {
    
    private static final Logger LOG = Logger.getLogger(DefaultValidationNotificationHandler.class);

    private enum Level {
    	INFO,
    	WARN,
    	ERROR
    }
    
    private Map<Level, List<String>> notifications;
    
    public DefaultValidationNotificationHandler() {
    	this.notifications = new HashMap<DefaultValidationNotificationHandler.Level, List<String>>();
    }
    
	public void handleError(String aNotificationMessage) {
	    LOG.error(aNotificationMessage);
	    addError(aNotificationMessage);
	}

	public void handleError(String aNotification, Object anObject) {
		String message = getFullMessage(aNotification, anObject);
		handleError(message);
	}

    public void handleInfo(String aNotificationMessage) {
        LOG.info(aNotificationMessage);
        addInfo(aNotificationMessage);
	}

	public void handleInfo(String aNotification, Object anObject) {
		String message = getFullMessage(aNotification, anObject);
		handleInfo(message);
	}

	public void handleWarning(String aNotificationMessage) {
	    LOG.warn(aNotificationMessage);
	    addWarn(aNotificationMessage);
	}

	public void handleWarning(String aNotification, Object anObject) {
		String message = getFullMessage(aNotification, anObject);
		handleWarning(message);
	}
	
	public List<String> errors() {
		return notifications.get(Level.ERROR);
	}
	
	private void addInfo(String message) {
		addNotifications(Level.INFO, message);
	}

	private void addWarn(String message) {
		addNotifications(Level.WARN, message);
	}
	
	private void addError(String message) {
		addNotifications(Level.ERROR, message);
	}

	
	private void addNotifications(Level level, String message) {
		if (!this.notifications.containsKey(level)) {
			this.notifications.put(level, new ArrayList<String>());
		}
		
		this.notifications.get(level).add(message);
	}

    private String getFullMessage(String aNotification, Object anObject) {
        StringBuffer buffer = new StringBuffer(aNotification).
                append(" (").append(anObject).append(")");
        return buffer.toString();
    }

}
