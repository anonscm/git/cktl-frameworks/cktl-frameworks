/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.common.support;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Permet de formater des dates initialement représentée sous forme de String ISO (du type YYYY-MM-DD) en dates locales. Le parse n'est pas
 * implémenté.
 * 
 * @author rprin
 */
public class ISODateTextToLocalDateFormat extends Format {
	public static final String FRENCH_SHORT_DATE_PATTERN = "dd/MM/yyyy";
	public static final String FRENCH_DATETIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	private static final long serialVersionUID = 1L;
	private String pattern;

	/**
	 * @param pattern pattern pour le format de la date (cf. {@link DateTimeFormatter})
	 */
	public ISODateTextToLocalDateFormat(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		try {
			ReadablePartial dt = null;
			dt = DateConversionUtil.sharedInstance().parseDateTimeSilent((String) obj);
			if (dt == null) {
				dt = DateConversionUtil.sharedInstance().parseDateSilent((String) obj);
			}
			DateTimeFormatter jodaOutputFormat = DateTimeFormat.forPattern(pattern);
			return new StringBuffer(jodaOutputFormat.print(dt));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * non implémenté.
	 *
	 * @return null
	 */
	@Override
	public Object parseObject(String source, ParsePosition pos) {
		return null;
	}
}
