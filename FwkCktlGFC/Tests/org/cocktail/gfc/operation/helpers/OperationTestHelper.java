package org.cocktail.gfc.operation.helpers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.operation.TypeOperationId;

/**
 * @author bourges
 *
 */
public class OperationTestHelper {
    
    /**
     * @param tag - correspond à l'id et est utilisé sur tous les champs de l'opération créée
     * @return une opération fictive
     */
    public static Operation buildOperation(Long tag) {
        return Operation.builder()
                .operationId(new OperationId(tag))
                .libelle("lib" + tag)
                .type(TypeOperation.builder().typeOperationId(new TypeOperationId(tag)).build())
                .code("OPE-2015-" + tag)
                .montantHT(new Montant(new BigDecimal(1000 + tag)))
                .montantNet(new Montant(new BigDecimal(2000 + tag)))
                .build();
    }
    
    /**
     * @param tags - tags à passer à {@link OperationTestHelper#buildOperation(int)}
     * @return une liste d'opérations fictives
     */
    public static List<Operation> buidOperations(Long... tags) {
        List<Operation> ret = new ArrayList<Operation>();
        for (Long tag : tags) {
            ret.add(buildOperation(tag));
        }
        return ret;
    }

}
