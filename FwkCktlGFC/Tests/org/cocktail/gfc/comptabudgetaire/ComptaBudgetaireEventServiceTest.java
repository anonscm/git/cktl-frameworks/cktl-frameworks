/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.helpers.ComptaBudgetaireEventMessageLigneTestHelper;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEventRepository;
import org.jglue.fluentjson.JsonBuilderFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ComptaBudgetaireEventServiceTest {
    @Mock
    private ComptaBudgetaireEventRepository comptaBudgetaireEventRepository;

    private ComptaBudgetaireEventService service;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        service = new ComptaBudgetaireEventService(comptaBudgetaireEventRepository);

    }

    @Test
    public void testConvertFromJsonDoitIntialiserLeBonNombredObjets() throws Exception {
        BigDecimal montant1 = BigDecimal.valueOf(120.02d);
        BigDecimal montant2 = BigDecimal.valueOf(120.04d).negate();
        String json1 = ComptaBudgetaireEventMessageLigneTestHelper.createJson_2013_BI_AE_Credit_Eb1_Op2_Nat4_Dest4(montant1);
        String json2 = ComptaBudgetaireEventMessageLigneTestHelper.createJson_2013_BI_CP_Debit_Eb1_Op0_Nat4_Dest5(montant2);

        List<ComptaBudgetaireEventMessageLigne> lignes = service.convertFromJson("[" + json1 + "," + json2 + "]");
        assertEquals(2, lignes.size());
    }

    @Test
    public void testConvertFromJsonDoitBienIntialiserLesObjets() throws Exception {
        BigDecimal montant1 = BigDecimal.valueOf(120.02d);
        BigDecimal montant2 = BigDecimal.valueOf(120.04d).negate();

        ComptaBudgetaireEventMessageLigne ligne1 = ComptaBudgetaireEventMessageLigneTestHelper
                .createLigne_2013_BI_AE_Credit_Eb1_Op2_Nat4_Dest4(montant1);
        ComptaBudgetaireEventMessageLigne ligne2 = ComptaBudgetaireEventMessageLigneTestHelper.createLigne_2013_BI_CP_Debit_Eb1_Op0_Nat4_Dest5(montant2);

        String json1 = ComptaBudgetaireEventMessageLigneTestHelper.createJson_2013_BI_AE_Credit_Eb1_Op2_Nat4_Dest4(montant1);
        String json2 = ComptaBudgetaireEventMessageLigneTestHelper.createJson_2013_BI_CP_Debit_Eb1_Op0_Nat4_Dest5(montant2);

        List<ComptaBudgetaireEventMessageLigne> lignes = service.convertFromJson("[" + json1 + "," + json2 + "]");

        assertEquals(ligne1, lignes.get(0));
        assertEquals(ligne2, lignes.get(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertFromJsonDoitDeclencherUneErreurSiLeJsonNestPasCorrect() throws Exception {
        service.convertFromJson(getJsonExemple2());
    }

    private String getJsonExemple2() {
        String json = JsonBuilderFactory.buildObject()
                    .add("exeOrdre", 2015)
                    .add("idAdmEb", 10032)
                    .add("idOpeOperation", 1)
                    .getJson().toString();
        json += ","
                + "{\"exeOrdre\":2015,\"idAdmEb\":10032,\"idOpeOperation\":1,\"typeMontant\":\"CP\",\"sens\":\"C\",\"montant\":20000.01,\"idAdmNatureDepense\":10007,\"idAdmDestinationDepense\":6,\"idAdmNatureRecette\":null, \"idAdmOrigineRecette\":null,\"section\":null}";
        json = "[" + json + "]";
        return json;
    }

}