/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.helpers;

import java.math.BigDecimal;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.ComptaBudgetaireEventMessageLigneBuilder;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.SensEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeMontantEnum;
import org.cocktail.gfc.operation.OperationId;
import org.jglue.fluentjson.JsonBuilderFactory;

;

public class ComptaBudgetaireEventMessageLigneTestHelper {

    public static final Integer EXE_ORDRE_2013 = 2013;
    public static final EntiteBudgetaireId ID_ADM_EB_1 = new EntiteBudgetaireId(1L);
    public static final OperationId ID_OPE_OPERATION_2 = new OperationId(2L);

    public static final NatureDepenseId ID_NATURE_DEPENSE_4 = new NatureDepenseId(4L);

    public static final DestinationDepenseId ID_DESTINATION_DEPENSE_4 = new DestinationDepenseId(4L);
    public static final DestinationDepenseId ID_DESTINATION_DEPENSE_5 = new DestinationDepenseId(5L);

    private static ComptaBudgetaireEventMessageLigneBuilder builder() {
        return ComptaBudgetaireEventMessageLigne.builder();
    }

    public static ComptaBudgetaireEventMessageLigne createLigne_2013_BI_AE_Credit_Eb1_Op2_Nat4_Dest4(BigDecimal montantAsBigDecimal) {
        Montant montant = new Montant(montantAsBigDecimal);
        SensEnum sens = SensEnum.CREDIT;
        TypeMontantEnum typeMontant = TypeMontantEnum.AE;

        return builder()
                .exeOrdre(EXE_ORDRE_2013)
                .typeEcriture(TypeEcritureEnum.BI)
                .typeMontant(typeMontant)
                .sens(sens)
                .idAdmEb(ID_ADM_EB_1)
                .idOpeOperation(ID_OPE_OPERATION_2)
                .idAdmNatureDepense(ID_NATURE_DEPENSE_4)
                .idAdmDestinationDepense(ID_DESTINATION_DEPENSE_4)
                .montant(montant)
                .build();
    }

    public static String createJson_2013_BI_AE_Credit_Eb1_Op2_Nat4_Dest4(BigDecimal montantAsBigDecimal) {
        return createJson(EXE_ORDRE_2013, TypeEcritureEnum.BI, TypeMontantEnum.AE, SensEnum.CREDIT, ID_ADM_EB_1, ID_OPE_OPERATION_2, ID_NATURE_DEPENSE_4,
                ID_DESTINATION_DEPENSE_4, null, null, null, new Montant(montantAsBigDecimal));
    }

    public static ComptaBudgetaireEventMessageLigne createLigne_2013_BI_CP_Debit_Eb1_Op0_Nat4_Dest5(BigDecimal montantAsBigDecimal) {
        Montant montant = new Montant(montantAsBigDecimal);
        SensEnum sens = SensEnum.DEBIT;
        TypeMontantEnum typeMontant = TypeMontantEnum.CP;

        return builder()
                .exeOrdre(EXE_ORDRE_2013)
                .typeEcriture(TypeEcritureEnum.BI)
                .typeMontant(typeMontant)
                .sens(sens)
                .idAdmEb(ID_ADM_EB_1)
                .idAdmNatureDepense(ID_NATURE_DEPENSE_4)
                .idAdmDestinationDepense(ID_DESTINATION_DEPENSE_5)
                .montant(montant)
                .build();
    }

    public static String createJson_2013_BI_CP_Debit_Eb1_Op0_Nat4_Dest5(BigDecimal montantAsBigDecimal) {
        return createJson(EXE_ORDRE_2013, TypeEcritureEnum.BI, TypeMontantEnum.CP, SensEnum.DEBIT, ID_ADM_EB_1, null, ID_NATURE_DEPENSE_4, ID_DESTINATION_DEPENSE_5, null,
                null, null, new Montant(montantAsBigDecimal));
    }

    private static String createJson(Integer exeOrdre, TypeEcritureEnum typeEcriture, TypeMontantEnum typeMontant, SensEnum sens, EntiteBudgetaireId idAdmEb,
            OperationId idOpeOperation, NatureDepenseId idAdmNatureDepense, DestinationDepenseId idAdmDestinationDepense,
            NatureRecetteId idAdmNatureRecette, OrigineRecetteId idAdmOrigineRecette, SectionRecette section, Montant montant) {
        String res = JsonBuilderFactory.buildObject()
                .add("exeOrdre", exeOrdre)
                .add("typeEcriture", typeEcriture.toString())
                .add("typeMontant", typeMontant.toString())
                .add("sens", sens.toString().substring(0, 1))
                .add("idAdmEb", idAdmEb.id())
                .add("idOpeOperation", (idOpeOperation == null ? null : idOpeOperation.id()))
                .add("idAdmNatureDepense", (idAdmNatureDepense == null ? null : idAdmNatureDepense.id()))
                .add("idAdmDestinationDepense", (idAdmDestinationDepense == null ? null : idAdmDestinationDepense.id()))
                .add("idAdmNatureRecette", (idAdmNatureRecette == null ? null : idAdmNatureRecette.id()))
                .add("idAdmOrigineRecette", (idAdmOrigineRecette == null ? null : idAdmOrigineRecette.id()))
                .add("section", (section == null ? null : section.getAbreviation()))
                .add("montant", montant.value())
                .getJson().toString();
        return res;
    }
}
