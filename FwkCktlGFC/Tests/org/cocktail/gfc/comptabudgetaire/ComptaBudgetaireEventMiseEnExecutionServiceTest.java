/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEventRepository;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.support.exception.GfcValidationException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ComptaBudgetaireEventMiseEnExecutionServiceTest {

    @Mock
    private ComptaBudgetaireEventRepository comptaBudgetaireEventRepository;

    private ComptaBudgetaireEventMiseEnExecutionService service;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        service = new ComptaBudgetaireEventMiseEnExecutionService(comptaBudgetaireEventRepository);

    }

    @Test
    public void testCreerEvtMessageLigneDepAECredit() throws GfcValidationException {
        Integer exeOrdre = 2013;
        EntiteBudgetaireId idAdmEb = new EntiteBudgetaireId(1L);
        OperationId idOpeOperation = new OperationId(2L);
        Montant montant = new Montant(120.2d);
        NatureDepenseId idAdmNatureDepense = new NatureDepenseId(3L);
        DestinationDepenseId idAdmDestinationDepense = new DestinationDepenseId(4L);

        ComptaBudgetaireEventMessageLigne ligne = service.creerEvtMessageLigneDepAECredit(exeOrdre, TypeEcritureEnum.BI, idAdmEb, idOpeOperation, montant,
                idAdmNatureDepense, idAdmDestinationDepense);

        assertEquals(exeOrdre, ligne.getExeOrdre());
        assertEquals(idAdmEb, ligne.getIdAdmEb());
        assertEquals(idOpeOperation, ligne.getIdOpeOperation());
        assertEquals(montant, ligne.getMontant());
        assertEquals(idAdmNatureDepense, ligne.getIdAdmNatureDepense());
        assertEquals(idAdmDestinationDepense, ligne.getIdAdmDestinationDepense());
        assertEquals(ComptaBudgetaireEventMessageLigne.SensEnum.CREDIT, ligne.getSens());
        assertEquals(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.AE, ligne.getTypeMontant());
        assertNull(ligne.getIdAdmNatureRecette());
        assertNull(ligne.getIdAdmOrigineRecette());
        assertNull(ligne.getSection());

    }

    @Test
    public void testCreerEvtMessageLigneDepAEDebit() throws GfcValidationException {
        Integer exeOrdre = 2013;
        EntiteBudgetaireId idAdmEb = new EntiteBudgetaireId(1L);
        OperationId idOpeOperation = new OperationId(2L);
        Montant montant = new Montant(120.2d);
        NatureDepenseId idAdmNatureDepense = new NatureDepenseId(3L);
        DestinationDepenseId idAdmDestinationDepense = new DestinationDepenseId(4L);

        ComptaBudgetaireEventMessageLigne ligne = service.creerEvtMessageLigneDepAEDebit(exeOrdre, TypeEcritureEnum.BI, idAdmEb, idOpeOperation, montant,
                idAdmNatureDepense, idAdmDestinationDepense);

        assertEquals(exeOrdre, ligne.getExeOrdre());
        assertEquals(idAdmEb, ligne.getIdAdmEb());
        assertEquals(idOpeOperation, ligne.getIdOpeOperation());
        assertEquals(montant, ligne.getMontant());
        assertEquals(idAdmNatureDepense, ligne.getIdAdmNatureDepense());
        assertEquals(idAdmDestinationDepense, ligne.getIdAdmDestinationDepense());
        assertEquals(ComptaBudgetaireEventMessageLigne.SensEnum.DEBIT, ligne.getSens());
        assertEquals(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.AE, ligne.getTypeMontant());
        assertNull(ligne.getIdAdmNatureRecette());
        assertNull(ligne.getIdAdmOrigineRecette());
        assertNull(ligne.getSection());

    }

    @Test
    public void testCreerEvtMessageLigneDepCPCredit() throws GfcValidationException {
        Integer exeOrdre = 2013;
        EntiteBudgetaireId idAdmEb = new EntiteBudgetaireId(1L);
        OperationId idOpeOperation = new OperationId(2L);
        Montant montant = new Montant(120.2d);
        NatureDepenseId idAdmNatureDepense = new NatureDepenseId(3L);
        DestinationDepenseId idAdmDestinationDepense = new DestinationDepenseId(4L);

        ComptaBudgetaireEventMessageLigne ligne = service.creerEvtMessageLigneDepCPCredit(exeOrdre, TypeEcritureEnum.BI, idAdmEb, idOpeOperation, montant,
                idAdmNatureDepense, idAdmDestinationDepense);

        assertEquals(exeOrdre, ligne.getExeOrdre());
        assertEquals(idAdmEb, ligne.getIdAdmEb());
        assertEquals(idOpeOperation, ligne.getIdOpeOperation());
        assertEquals(montant, ligne.getMontant());
        assertEquals(idAdmNatureDepense, ligne.getIdAdmNatureDepense());
        assertEquals(idAdmDestinationDepense, ligne.getIdAdmDestinationDepense());
        assertEquals(ComptaBudgetaireEventMessageLigne.SensEnum.CREDIT, ligne.getSens());
        assertEquals(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.CP, ligne.getTypeMontant());
        assertNull(ligne.getIdAdmNatureRecette());
        assertNull(ligne.getIdAdmOrigineRecette());
        assertNull(ligne.getSection());

    }

    @Test
    public void testCreerEvtMessageLigneDepCPDebit() throws GfcValidationException {
        Integer exeOrdre = 2013;
        EntiteBudgetaireId idAdmEb = new EntiteBudgetaireId(1L);
        OperationId idOpeOperation = new OperationId(2L);
        Montant montant = new Montant(120.2d);
        NatureDepenseId idAdmNatureDepense = new NatureDepenseId(3L);
        DestinationDepenseId idAdmDestinationDepense = new DestinationDepenseId(4L);

        ComptaBudgetaireEventMessageLigne ligne = service.creerEvtMessageLigneDepCPDebit(exeOrdre, TypeEcritureEnum.BI, idAdmEb, idOpeOperation, montant,
                idAdmNatureDepense, idAdmDestinationDepense);

        assertEquals(exeOrdre, ligne.getExeOrdre());
        assertEquals(idAdmEb, ligne.getIdAdmEb());
        assertEquals(idOpeOperation, ligne.getIdOpeOperation());
        assertEquals(montant, ligne.getMontant());
        assertEquals(idAdmNatureDepense, ligne.getIdAdmNatureDepense());
        assertEquals(idAdmDestinationDepense, ligne.getIdAdmDestinationDepense());
        assertEquals(ComptaBudgetaireEventMessageLigne.SensEnum.DEBIT, ligne.getSens());
        assertEquals(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.CP, ligne.getTypeMontant());
        assertNull(ligne.getIdAdmNatureRecette());
        assertNull(ligne.getIdAdmOrigineRecette());
        assertNull(ligne.getSection());

    }

    @Test
    public void testCreerEvtMessageLigneRECDebit() throws GfcValidationException {
        Integer exeOrdre = 2013;
        EntiteBudgetaireId idAdmEb = new EntiteBudgetaireId(1L);
        OperationId idOpeOperation = new OperationId(2L);
        Montant montant = new Montant(120.2d);
        NatureRecetteId idAdmNatureRecette = new NatureRecetteId(3L);
        OrigineRecetteId idAdmOrigineRecette = new OrigineRecetteId(4L);
        SectionRecette sectionRecette = new SectionRecette("F", "Fonctionnement");

        ComptaBudgetaireEventMessageLigne ligne = service.creerEvtMessageLigneRECDebit(exeOrdre, TypeEcritureEnum.BI, idAdmEb, idOpeOperation, montant,
                idAdmNatureRecette, idAdmOrigineRecette, sectionRecette);
        assertEquals(exeOrdre, ligne.getExeOrdre());
        assertEquals(idAdmEb, ligne.getIdAdmEb());
        assertEquals(idOpeOperation, ligne.getIdOpeOperation());
        assertEquals(montant, ligne.getMontant());
        assertEquals(idAdmNatureRecette, ligne.getIdAdmNatureRecette());
        assertEquals(idAdmOrigineRecette, ligne.getIdAdmOrigineRecette());
        assertEquals(sectionRecette, ligne.getSection());
        assertEquals(ComptaBudgetaireEventMessageLigne.SensEnum.DEBIT, ligne.getSens());
        assertEquals(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.REC, ligne.getTypeMontant());
        assertNull(ligne.getIdAdmNatureDepense());
        assertNull(ligne.getIdAdmDestinationDepense());

    }

    @Test
    public void testCreerEvtMessageLigneRECCredit() throws GfcValidationException {
        Integer exeOrdre = 2013;
        EntiteBudgetaireId idAdmEb = new EntiteBudgetaireId(1L);
        OperationId idOpeOperation = new OperationId(2L);
        Montant montant = new Montant(120.2d);
        NatureRecetteId idAdmNatureRecette = new NatureRecetteId(3L);
        OrigineRecetteId idAdmOrigineRecette = new OrigineRecetteId(4L);
        SectionRecette sectionRecette = new SectionRecette("F", "Fonctionnement");

        ComptaBudgetaireEventMessageLigne ligne = service.creerEvtMessageLigneRECCredit(exeOrdre, TypeEcritureEnum.BI, idAdmEb, idOpeOperation, montant,
                idAdmNatureRecette, idAdmOrigineRecette, sectionRecette);
        assertEquals(exeOrdre, ligne.getExeOrdre());
        assertEquals(idAdmEb, ligne.getIdAdmEb());
        assertEquals(idOpeOperation, ligne.getIdOpeOperation());
        assertEquals(montant, ligne.getMontant());
        assertEquals(idAdmNatureRecette, ligne.getIdAdmNatureRecette());
        assertEquals(idAdmOrigineRecette, ligne.getIdAdmOrigineRecette());
        assertEquals(sectionRecette, ligne.getSection());
        assertEquals(ComptaBudgetaireEventMessageLigne.SensEnum.CREDIT, ligne.getSens());
        assertEquals(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.REC, ligne.getTypeMontant());
        assertNull(ligne.getIdAdmNatureDepense());
        assertNull(ligne.getIdAdmDestinationDepense());

    }

}
