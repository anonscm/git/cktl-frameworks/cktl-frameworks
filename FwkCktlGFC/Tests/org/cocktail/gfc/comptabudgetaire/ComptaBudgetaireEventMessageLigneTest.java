package org.cocktail.gfc.comptabudgetaire;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.SensEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeMontantEnum;
import org.junit.Test;

public class ComptaBudgetaireEventMessageLigneTest {
    
    @Test(expected = IllegalArgumentException.class)
    public void champsNonNullManquants() {
        @SuppressWarnings("unused")
        ComptaBudgetaireEventMessageLigne ligne = ComptaBudgetaireEventMessageLigne
                .builder()
                .build();
    }

    
    private Integer exeOrdre;
    private TypeMontantEnum typeMontant;
    private SensEnum sens;
    private Montant montant;
    private EntiteBudgetaireId idAdmEb;
    private TypeEcritureEnum typeEcriture;
    
    @Test
    public void champsNonNullPresents() {
        @SuppressWarnings("unused")
        ComptaBudgetaireEventMessageLigne ligne = ComptaBudgetaireEventMessageLigne
                .builder()
                .exeOrdre(2014)
                .typeMontant(typeMontant.AE)
                .sens(SensEnum.CREDIT)
                .montant(Montant.ZERO)
                .idAdmEb(new EntiteBudgetaireId(1L))
                .typeEcriture(TypeEcritureEnum.BI)                
                .build();
    }

}
