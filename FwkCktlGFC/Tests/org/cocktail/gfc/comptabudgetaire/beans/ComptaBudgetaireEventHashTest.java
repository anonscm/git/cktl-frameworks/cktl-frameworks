/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.beans;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ComptaBudgetaireEventHashTest {

    @Before
    public void before() {

    }

    @Test
    public void testValueDoitRenvoyerUneChaineBienConstruite() {
        ComptaBudgetaireEventHash hash = new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, "12345", "2015-04-03",
                false);
        String expected = ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION.getKey() + "$" + "12345" + "$" + "2015-04-03";
        assertEquals(expected, hash.value());
    }

    @Test
    public void testValueDoitRenvoyerUneChaineAnnulationBienConstruite() {
        ComptaBudgetaireEventHash hash = new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, "12345", "2015-04-03",
                true);
        String expected = ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION.getKey() + "$" + "12345" + ComptaBudgetaireEventHash.FLAG_ANNULATION
                + "$" + "2015-04-03";
        assertEquals(expected, hash.value());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoitDeclencherUneExceptionSiTypeNull() {
        new ComptaBudgetaireEventHash(null, "12345", "2015-04-03", false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoitDeclencherUneExceptionSiBaseHashNull() {
        new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, null, "2015-04-03", false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoitDeclencherUneExceptionSiSuffixNull() {
        new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, "12345", null, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoitDeclencherUneExceptionSiBaseHashVide() {
        new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, "", "2015-04-03", false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoitDeclencherUneExceptionSiSuffixVide() {
        new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, "12345", "", false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoitDeclencherUneExceptionQuandLaStringPasseeNeRespectePasLeBonFormat() {
        String string = ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION.getKey() + "$" + "12345" + "|" + "2015-04-03";
        ComptaBudgetaireEventHash.valueOf(string);
    }

    @Test
    public void testDoitConstruireLeBonHashAPartirdUneString() {
        ComptaBudgetaireEventHash expected = new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, "12345",
                "2015-04-03", false);
        String string = ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION.getKey() + "$" + "12345" + "$" + "2015-04-03";

        assertEquals(expected, ComptaBudgetaireEventHash.valueOf(string));
    }

    @Test
    public void testDoitConstruireLeBonHashAPartirdUneStringAnnulation() {
        ComptaBudgetaireEventHash expected = new ComptaBudgetaireEventHash(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION, "12345",
                "2015-04-03", true);
        String string = ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION.getKey() + "$" + "12345" + ComptaBudgetaireEventHash.FLAG_ANNULATION
                + "$" + "2015-04-03";

        assertEquals(expected, ComptaBudgetaireEventHash.valueOf(string));
    }

}