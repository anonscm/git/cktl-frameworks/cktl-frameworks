/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.representations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.SensEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.representations.ComptaBudgetaireEventMessageLigneRepresentation;
import org.junit.Before;
import org.junit.Test;

public class ComptaBudgetaireEventMessageLigneRepresentationTest {

    ComptaBudgetaireEventMessageLigne comptaBudgetaireEventMessageLigne;

    @Before
    public void init() {
        comptaBudgetaireEventMessageLigne = ComptaBudgetaireEventMessageLigne
                .builder()
                .idAdmEb(new EntiteBudgetaireId(1L))
                .exeOrdre(2014)
                .typeEcriture(TypeEcritureEnum.BI)
                .sens(SensEnum.CREDIT)
                .montant(new Montant(BigDecimal.valueOf(10.2)))
                .typeMontant(ComptaBudgetaireEventMessageLigne.TypeMontantEnum.AE)
                .build();
    }

    @Test
    public void getTypeEcriture() {
        comptaBudgetaireEventMessageLigne.setTypeEcriture(TypeEcritureEnum.BI);
        ComptaBudgetaireEventMessageLigneRepresentation representation = new ComptaBudgetaireEventMessageLigneRepresentation(
                comptaBudgetaireEventMessageLigne);
        assertEquals("BI", representation.getTypeEcriture());        
    }
    
    @Test
    public void getSensRepresentationCreditTest() {
        comptaBudgetaireEventMessageLigne.setSens(ComptaBudgetaireEventMessageLigne.SensEnum.CREDIT);
        ComptaBudgetaireEventMessageLigneRepresentation representation = new ComptaBudgetaireEventMessageLigneRepresentation(
                comptaBudgetaireEventMessageLigne);

        assertEquals("C", representation.getSensRepresentation(comptaBudgetaireEventMessageLigne.getSens()));
    }

    @Test
    public void getSensRepresentationDebitTest() {
        comptaBudgetaireEventMessageLigne.setSens(ComptaBudgetaireEventMessageLigne.SensEnum.DEBIT);
        ComptaBudgetaireEventMessageLigneRepresentation representation = new ComptaBudgetaireEventMessageLigneRepresentation(
                comptaBudgetaireEventMessageLigne);

        assertEquals("D", representation.getSensRepresentation(comptaBudgetaireEventMessageLigne.getSens()));
    }

}
