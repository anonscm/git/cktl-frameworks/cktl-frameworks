/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.support.SectionRecetteRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SectionRecetteCacheServiceTest {

    private static final List<SectionRecette> DEFAULT_LIST = new ArrayList<SectionRecette>();

    private static final SectionRecette SECTION_RECETTE_F = new SectionRecette("F", "Fonctionnement");
    private static final SectionRecette SECTION_RECETTE_G = new SectionRecette("G", "Global");

    static {
        DEFAULT_LIST.add(SECTION_RECETTE_F);
        DEFAULT_LIST.add(new SectionRecette("I", "Investissement"));
    }

    @Mock
    private SectionRecetteRepository sectionRecetteRepository;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        when(sectionRecetteRepository.findAll()).thenReturn(DEFAULT_LIST);
        when(sectionRecetteRepository.findByCode("F")).thenReturn(SECTION_RECETTE_F);
        when(sectionRecetteRepository.findByCode("G")).thenReturn(SECTION_RECETTE_G);
        new SectionRecetteCacheService(sectionRecetteRepository);
    }

    @Test
    public void testFindAllApresInitialisation() {
        SectionRecetteCacheService.clear();
        List<SectionRecette> res = SectionRecetteCacheService.findAll();
        assertEquals(res, DEFAULT_LIST);
    }

    @Test
    public void testFindByAbreviation() {
        SectionRecetteCacheService.clear();
        SectionRecette res = SectionRecetteCacheService.findByAbreviation("F");
        assertEquals(DEFAULT_LIST.get(0), res);
    }

    @Test
    public void testFindByAbreviationApresInit() {
        SectionRecetteCacheService.clear();
        SectionRecetteCacheService.findAll();
        SectionRecette res = SectionRecetteCacheService.findByAbreviation("F");
        assertEquals(DEFAULT_LIST.get(0), res);
    }

    @Test
    public void testFindByAbreviationNouvelle() {
        SectionRecetteCacheService.clear();
        SectionRecette res = SectionRecetteCacheService.findByAbreviation("G");
        assertEquals(SECTION_RECETTE_G, res);
    }

    @Test
    public void testFindByAbreviationInexistante() {
        SectionRecetteCacheService.clear();
        SectionRecette res = SectionRecetteCacheService.findByAbreviation("X");
        assertEquals(null, res);
    }

}