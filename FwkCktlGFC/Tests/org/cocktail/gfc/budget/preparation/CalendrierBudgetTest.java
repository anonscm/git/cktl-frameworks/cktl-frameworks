/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import static org.junit.Assert.fail;

import org.cocktail.common.support.CktlGFCDate;
import org.cocktail.gfc.support.exception.GfcValidationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class CalendrierBudgetTest {
    private static final int EXERCICE = 2015;
    private static final String HIER = "2015-06-04T00:00:00Z";
    private static final String AUJOURDHUI = "2015-06-05T00:00:00Z";
    private static final String DEMAIN = "2015-06-06T00:00:00Z";

    private CktlGFCDate aujourdhui;
    private CktlGFCDate hier;
    private CktlGFCDate demain;

    private CalendrierBudget calendrier;

    private CktlGFCDate cktlGFCDateVide() {
        return new CktlGFCDate();
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    private CktlGFCDate getMockedCktlGFCDate(String date) throws GfcValidationException {
        return new CktlGFCDate(date) {
            @Override
            public CktlGFCDate getToday() {
                try {
                    return new CktlGFCDate(AUJOURDHUI);
                } catch (GfcValidationException e) {
                    e.printStackTrace();
                    return null;
                }

            }
        };
    }

    @Before
    public void initCalendrier() throws GfcValidationException {
        aujourdhui = getMockedCktlGFCDate(AUJOURDHUI);
        hier = getMockedCktlGFCDate(HIER);
        demain = getMockedCktlGFCDate(DEMAIN);
        calendrier = new CalendrierBudget();
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateVoteValide(org.cocktail.common.support.CktlGFCDate)}
     * .
     * 
     * @throws GfcValidationException
     */
    @Test
    public void testCheckDateVoteValide() {
        // Doit être non nulle
        try {
            calendrier.checkDateVoteValide(null);
            fail("Une date de vote vide doit lever une erreur");
        } catch (GfcValidationException e) {
        }
        // Doit être antérieure ou égale à la date du jour
        try {
            calendrier.checkDateVoteValide(hier);
            calendrier.checkDateVoteValide(aujourdhui);
        } catch (GfcValidationException e) {
            fail("Aujourd'hui ou hier doivent être des dates de vote valides");
        }
        try {
            calendrier.checkDateVoteValide(demain);
            fail("Une date de vote à demain (" + demain.toDateWithoutTime() + ") doit lever une erreur");
        } catch (GfcValidationException e) {
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateVoteModifiable()}
     * .
     */
    @Test
    public void testCheckDateVoteModifiable() {
        // Modifiable si pas d'autres dates
        try {
            calendrier.checkDateVoteModifiable();
        } catch (GfcValidationException e) {
            fail("La date de vote doit être modifiable si aucune date n'est positionnée");
        }

        // Date d'envoi doit être vide
        calendrier.setDateEnvoi(aujourdhui);
        try {
            calendrier.checkDateVoteModifiable();
            fail("La date de vote ne doit pas être modifiable si la date d'envoi est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
        }
        // Date d'approbation doit être vide
        calendrier.setDateApprobation(aujourdhui);
        try {
            calendrier.checkDateVoteModifiable();
            fail("La date de vote ne doit pas être modifiable si la date d'approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
        }
        // Date de non approbation doit être vide
        calendrier.setDateNonApprobation(aujourdhui);
        try {
            calendrier.checkDateVoteModifiable();
            fail("La date de vote ne doit pas être modifiable si la date de non approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateNonApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateVoteModifiable();
            fail("La date de vote ne doit pas être modifiable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateVoteSupprimable()}
     * .
     */
    @Test
    public void testCheckDateVoteSupprimable() {
        try {
            calendrier.checkDateVoteSupprimable();
        } catch (GfcValidationException e) {
            fail("La date de vote doit être supprimable si aucune date n'est positionnée");
        }

        // Date d'envoi doit être vide
        calendrier.setDateEnvoi(aujourdhui);
        try {
            calendrier.checkDateVoteSupprimable();
            fail("La date de vote ne doit pas être supprimable si la date d'envoi est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
        }
        // Date d'approbation doit être vide
        calendrier.setDateApprobation(aujourdhui);
        try {
            calendrier.checkDateVoteSupprimable();
            fail("La date de vote ne doit pas être supprimable si la date d'approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
        }
        // Date de non approbation doit être vide
        calendrier.setDateNonApprobation(aujourdhui);
        try {
            calendrier.checkDateVoteSupprimable();
            fail("La date de vote ne doit pas être supprimable si la date de non approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateNonApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateVoteSupprimable();
            fail("La date de vote ne doit pas être supprimable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateEnvoiValide(org.cocktail.common.support.CktlGFCDate)}
     * .
     */
    @Test
    public void testCheckDateEnvoiValide() {
        // Doit être non nulle
        try {
            calendrier.checkDateEnvoiValide(null);
            fail("Une date d'envoi vide doit lever une erreur");
        } catch (GfcValidationException e) {
        }
        // Doit être antérieure ou égale à la date du jour
        try {
            calendrier.setDateVote(hier);
            calendrier.checkDateEnvoiValide(hier);
            calendrier.checkDateEnvoiValide(aujourdhui);
            calendrier.setDateVote(cktlGFCDateVide());
        } catch (GfcValidationException e) {
            fail("Aujourd'hui (" + aujourdhui.toDateWithoutTime() + ") ou hier (" + hier.toDateWithoutTime()
                    + ") doivent être des dates de vote valides");
        }
        try {
            calendrier.checkDateEnvoiValide(demain);
            fail("Une date d'envoi à demain (" + demain.toDateWithoutTime() + ") doit lever une erreur");
        } catch (GfcValidationException e) {
        }
        // Doit être postérieure ou égale à la date de vote
        try {
            calendrier.setDateVote(aujourdhui);
            calendrier.checkDateEnvoiValide(hier);
            calendrier.setDateVote(cktlGFCDateVide());
            fail("Une date d'envoi antérieure à la date de vote ne doit pas être valide");
        } catch (GfcValidationException e) {
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateEnvoiModifiable()}
     * .
     */
    @Test
    public void testCheckDateEnvoiModifiable() {
        // Doit être modifiable uniquement si la date de vote est positionnée
        try {
            calendrier.checkDateEnvoiModifiable();
            fail("La date d'envoi ne doit pas être modifiable si la date de vote n'est pas positionnée");
        } catch (GfcValidationException e) {
        }

        try {
            calendrier.setDateVote(hier);
            calendrier.checkDateEnvoiModifiable();
        } catch (GfcValidationException e) {
            fail("La date d'envoi doit être modifiable si la date de vote est positionnée");
        }

        // Date d'approbation doit être vide
        calendrier.setDateApprobation(aujourdhui);
        try {
            calendrier.checkDateEnvoiModifiable();
            fail("La date d'envoi ne doit pas être modifiable si la date d'approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
        }
        // Date de non approbation doit être vide
        calendrier.setDateNonApprobation(aujourdhui);
        try {
            calendrier.checkDateEnvoiModifiable();
            fail("La date d'envoi ne doit pas être modifiable si la date de non approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateNonApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateEnvoiModifiable();
            fail("La date d'envoi ne doit pas être modifiable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateEnvoiSupprimable()}
     * .
     */
    @Test
    public void testCheckDateEnvoiSupprimable() {
        // Date d'approbation doit être vide
        calendrier.setDateApprobation(aujourdhui);
        try {
            calendrier.checkDateEnvoiSupprimable();
            fail("La date d'envoi ne doit pas être supprimable si la date d'approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
        }
        // Date de non approbation doit être vide
        calendrier.setDateNonApprobation(aujourdhui);
        try {
            calendrier.checkDateEnvoiSupprimable();
            fail("La date d'envoi ne doit pas être supprimable si la date de non approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateNonApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateEnvoiSupprimable();
            fail("La date d'envoi ne doit pas être supprimable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateNonApprobationValide(org.cocktail.common.support.CktlGFCDate)}
     * .
     */
    @Test
    public void testCheckDateNonApprobationValide() {
        // Doit être non nulle
        try {
            calendrier.checkDateNonApprobationValide(null);
            fail("Une date de non approbation vide doit lever une erreur");
        } catch (GfcValidationException e) {
        }
        // La date d'envoi ne doit pas être vide
        try {
            calendrier.setDateVote(hier);
            calendrier.checkDateNonApprobationValide(aujourdhui);
            fail("Si la date d'envoi est vide, la date de non approbation ne doit pas être positionnable");
        } catch (GfcValidationException e) {
            calendrier.setDateVote(cktlGFCDateVide());
        }
        // Doit être antérieure ou égale à la date du jour
        try {
            calendrier.setDateVote(hier);
            calendrier.setDateEnvoi(hier);

            calendrier.checkDateNonApprobationValide(hier);
            calendrier.checkDateNonApprobationValide(aujourdhui);

            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        } catch (GfcValidationException e) {
            fail("Aujourd'hui (" + aujourdhui.toDateWithoutTime() + ") ou hier (" + hier.toDateWithoutTime()
                    + ") doivent être des dates de non approbation valides");
        }
        try {
            calendrier.setDateVote(hier);
            calendrier.setDateEnvoi(hier);

            calendrier.checkDateNonApprobationValide(demain);
            fail("Une date de non approbation à demain (" + demain.toDateWithoutTime() + ") doit lever une erreur");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
        // Doit être postérieure ou égale à la date de vote et à la date d'envoi
        try {
            calendrier.setDateVote(hier);
            calendrier.setDateEnvoi(aujourdhui);
            calendrier.checkDateNonApprobationValide(hier);
            fail("La date de non approbation doit être postérieure ou égale à la date d'envoi");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
        try {
            calendrier.setDateVote(aujourdhui);
            calendrier.setDateEnvoi(aujourdhui);
            calendrier.checkDateNonApprobationValide(hier);
            fail("La date de non approbation doit être postérieure ou égale à la date d'envoi");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateNonApprobationModifiable()}
     * .
     */
    @Test
    public void testCheckDateNonApprobationModifiable() {
        // Date d'approbation doit être vide
        calendrier.setDateApprobation(aujourdhui);
        try {
            calendrier.checkDateNonApprobationModifiable();
            fail("La date de non approbation ne doit pas être modifiable si la date d'approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateNonApprobationModifiable();
            fail("La date de non approbation ne doit pas être modifiable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateNonApprobationSupprimable()}
     * .
     */
    @Test
    public void testCheckDateNonApprobationSupprimable() {
        // Date d'approbation doit être vide
        calendrier.setDateApprobation(aujourdhui);
        try {
            calendrier.checkDateNonApprobationSupprimable();
            fail("La date de non approbation ne doit pas être supprimable si la date d'approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateNonApprobationSupprimable();
            fail("La date de non approbation ne doit pas être supprimable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateApprobationValide(org.cocktail.common.support.CktlGFCDate)}
     * .
     */
    @Test
    public void testCheckDateApprobationValide() {
        // Doit être non nulle
        try {
            calendrier.checkDateApprobationValide(null);
            fail("Une date d'approbation vide doit lever une erreur");
        } catch (GfcValidationException e) {
        }
        // La date d'envoi ne doit pas être vide
        try {
            calendrier.setDateVote(hier);
            calendrier.checkDateApprobationValide(aujourdhui);
            fail("Si la date d'envoi est vide, la date d'approbation ne doit pas être positionnable");
        } catch (GfcValidationException e) {
            calendrier.setDateVote(cktlGFCDateVide());
        }
        // Doit être antérieure ou égale à la date du jour
        try {
            calendrier.setDateVote(hier);
            calendrier.setDateEnvoi(hier);

            calendrier.checkDateApprobationValide(hier);
            calendrier.checkDateApprobationValide(aujourdhui);

            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        } catch (GfcValidationException e) {
            fail("Aujourd'hui (" + aujourdhui.toDateWithoutTime() + ") ou hier (" + hier.toDateWithoutTime()
                    + ") doivent être des dates de non approbation valides");
        }
        try {
            calendrier.setDateVote(hier);
            calendrier.setDateEnvoi(hier);

            calendrier.checkDateApprobationValide(demain);
            fail("Une date d'approbation à demain (" + demain.toDateWithoutTime() + ") doit lever une erreur");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
        // Doit être postérieure ou égale à la date de vote et à la date d'envoi
        try {
            calendrier.setDateVote(hier);
            calendrier.setDateEnvoi(aujourdhui);
            calendrier.checkDateApprobationValide(hier);
            fail("La date d'approbation doit être postérieure ou égale à la date d'envoi");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
        try {
            calendrier.setDateVote(aujourdhui);
            calendrier.setDateEnvoi(aujourdhui);
            calendrier.checkDateApprobationValide(hier);
            fail("La date d'approbation doit être postérieure ou égale à la date d'envoi");
        } catch (GfcValidationException e) {
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateApprobationModifiable()}
     * .
     */
    @Test
    public void testCheckDateApprobationModifiable() {
        // Date d'approbation doit être vide
        calendrier.setDateNonApprobation(aujourdhui);
        try {
            calendrier.checkDateApprobationModifiable();
            fail("La date d'approbation ne doit pas être modifiable si la date de non approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateNonApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateApprobationModifiable();
            fail("La date d'approbation ne doit pas être modifiable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateApprobationSupprimable()}
     * .
     */
    @Test
    public void testCheckDateApprobationSupprimable() {
        // Date d'approbation doit être vide
        calendrier.setDateNonApprobation(aujourdhui);
        try {
            calendrier.checkDateApprobationSupprimable();
            fail("La date d'approbation ne doit pas être supprimable si la date de non approbation est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateNonApprobation(cktlGFCDateVide());
        }
        // Date d'execution doit être vide
        calendrier.setDateExecution(aujourdhui);
        try {
            calendrier.checkDateApprobationSupprimable();
            fail("La date d'approbation ne doit pas être supprimable si la date d'execution est positionnée");
        } catch (GfcValidationException e) {
            calendrier.setDateExecution(cktlGFCDateVide());
        }
    }

    /**
     * Test method for
     * {@link org.cocktail.gfc.budget.preparation.CalendrierBudget#checkDateExecutionValide(org.cocktail.common.support.CktlGFCDate, boolean, int)}
     * .
     */
    @Test
    public void testCheckDateExecutionValide() {
        // Doit être non nulle
        try {
            calendrier.checkDateExecutionValide(null, false, EXERCICE);
            fail("Une date d'exécution vide doit lever une erreur");
        } catch (GfcValidationException e) {
        }
        // La date de vote ne doit pas être vide
        try {
            calendrier.setDateVote(cktlGFCDateVide());
            calendrier.checkDateApprobationValide(aujourdhui);
            fail("Si la date de vote est vide, la date d'exécution ne doit pas être positionnable");
        } catch (GfcValidationException e) {
        }
        // Doit être postérieure ou égale à la date du jour
        try {
            calendrier.setDateVote(hier);

            calendrier.checkDateExecutionValide(aujourdhui, false, EXERCICE);
            calendrier.checkDateExecutionValide(demain, false, EXERCICE);

            calendrier.setDateVote(cktlGFCDateVide());
        } catch (GfcValidationException e) {
            fail("Aujourd'hui (" + aujourdhui.toDateWithoutTime() + ") ou demain (" + hier.toDateWithoutTime()
                    + ") doivent être des dates d'exécution valides");
        }
        try {
            calendrier.setDateVote(hier);

            calendrier.checkDateExecutionValide(hier, false, EXERCICE);
            fail("Une date d'execution à demain (" + demain.toDateWithoutTime() + ") doit lever une erreur");
        } catch (GfcValidationException e) {
            calendrier.setDateVote(cktlGFCDateVide());
        }
        // Doit être postérieure ou égale à la date d'envoi si elle existe
        try {
            calendrier.setDateVote(hier);
            calendrier.setDateEnvoi(aujourdhui);

            calendrier.checkDateExecutionValide(hier, false, EXERCICE);
            fail("La date d'exécution ne peut pas être antérieure à la date d'envoi");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
        // Doit être postérieure ou égale au 1er janvier de l'exercice si BI
        try {
            calendrier.setDateVote(hier);
            calendrier.checkDateExecutionValide(aujourdhui, true, EXERCICE + 1);
            fail("La date d'exécution ne peut pas être antérieure au 1er janvier de l'exercice");
        } catch (GfcValidationException e) {
            calendrier.setDateApprobation(cktlGFCDateVide());
            calendrier.setDateEnvoi(cktlGFCDateVide());
            calendrier.setDateVote(cktlGFCDateVide());
        }
    }
}
