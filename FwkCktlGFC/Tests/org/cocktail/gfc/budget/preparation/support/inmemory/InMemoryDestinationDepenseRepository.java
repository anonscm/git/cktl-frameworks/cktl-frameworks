/**
 * 
 */
package org.cocktail.gfc.budget.preparation.support.inmemory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.support.DestinationDepenseRepository;

import com.google.common.base.Optional;

/**
 * @author  Raymond NANEON <raymond.naneon at asso-cocktail.fr>
 *
 */
public class InMemoryDestinationDepenseRepository implements DestinationDepenseRepository {
	
	private Map<DestinationDepenseId, DestinationDepense> destinationDepenseMap;

	public List<DestinationDepense> findAll() {
		// TODO Auto-generated method stub
		return new ArrayList<DestinationDepense>(destinationDepenseMap.values());
	}

	public List<DestinationDepense> findValides() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public List<DestinationDepense> findValidesByExercice(int exercice) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public List<DestinationDepense> findDestSaisissablesAndByExercie(
			int exercice) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public Optional<DestinationDepense> findByIdAndExercice(
			DestinationDepenseId id, int exercice) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

}
