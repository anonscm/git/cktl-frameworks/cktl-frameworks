/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.inmemory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.read.EbEtatSommesRead;
import org.cocktail.gfc.budget.preparation.support.EbEtatSommesReadRepository;

import com.google.common.base.Optional;
import com.mysema.commons.lang.Pair;

public class InMemoryBudgetEbEtatReadRepository implements EbEtatSommesReadRepository {

    private Map<Pair<BudgetId, EntiteBudgetaireId>, EbEtatSommesRead> budgetEbEtatReadMap;
    
    public InMemoryBudgetEbEtatReadRepository() {
        this.budgetEbEtatReadMap = new HashMap<Pair<BudgetId,EntiteBudgetaireId>, EbEtatSommesRead>();
    }
    
    public void addBudgetEbEtatRead(BudgetId budgetId, EbEtatSommesRead budgetEbEtatRead) {
        Pair<BudgetId, EntiteBudgetaireId> key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, new EntiteBudgetaireId(budgetEbEtatRead.getEb().getId()));
        budgetEbEtatReadMap.put(key, budgetEbEtatRead);
    }
    
    public void modifyBudgetEbEtatRead(BudgetId budgetId, EntiteBudgetaireId ebId, TypeEtat typeEtat) {
        Pair<BudgetId, EntiteBudgetaireId> key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, ebId);
        EbEtatSommesRead budgetEbEtatRead = budgetEbEtatReadMap.get(key);
        if (budgetEbEtatRead != null) {
            budgetEbEtatReadMap.get(key).setEtat(typeEtat);
        }
    }
    
    private static boolean areEquals(Object a, Object b) {
        return a == b || (a != null && a.equals(b));
    } 
    
    public Optional<EbEtatSommesRead> queryByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId ebId) {
        Pair<BudgetId, EntiteBudgetaireId> key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, ebId);
        return Optional.fromNullable(this.budgetEbEtatReadMap.get(key));
    }

    public List<EbEtatSommesRead> queryByBudgetId(BudgetId budgetId) {
        List<EbEtatSommesRead> listeBudgetEbEtatRead = new ArrayList<EbEtatSommesRead>();
        EbEtatSommesRead budgetEbEtatRead;
        Pair<BudgetId, EntiteBudgetaireId> key;
        for (Entry<Pair<BudgetId, EntiteBudgetaireId>, EbEtatSommesRead> entry : budgetEbEtatReadMap.entrySet()) {
            key = entry.getKey();
            if (areEquals(key.getFirst(), budgetId)) {
                budgetEbEtatRead = budgetEbEtatReadMap.get(key);
                if (budgetEbEtatRead != null) {
                    listeBudgetEbEtatRead.add(budgetEbEtatRead);
                }
            }            
        }
        return listeBudgetEbEtatRead;
    }

    public List<EbEtatSommesRead> queryByBudgetIdAndListEntiteBudgetaireId(BudgetId budgetId, List<EntiteBudgetaireId> listeEbId) {
        List<EbEtatSommesRead> listeBudgetEbEtat = new ArrayList<EbEtatSommesRead>();
        EbEtatSommesRead budgetEbEtatRead;
        Pair<BudgetId, EntiteBudgetaireId> key;
        for (EntiteBudgetaireId ebId : listeEbId) {
            key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, ebId);
            budgetEbEtatRead = budgetEbEtatReadMap.get(key);
            if (budgetEbEtatRead != null) {
                listeBudgetEbEtat.add(budgetEbEtatRead);
            }
        }
        return listeBudgetEbEtat;
    }

	public List<EbEtatSommesRead> synthesePreparationNiveauEtablissement(BudgetId budgetId) {
		return null;
	}

	public EbEtatSommesRead synthesePreparationNiveauEtablissement(BudgetId budgetId, EntiteBudgetaireId etablissementId) {
		return null;
	}
}
