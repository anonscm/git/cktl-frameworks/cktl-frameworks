/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.inmemory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.gfc.budget.preparation.Budget;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;

public class InMemoryBudgetRepository implements BudgetRepository {

    private Map<BudgetId, Budget> budgetsMap;

    public InMemoryBudgetRepository() {
        this.budgetsMap = new HashMap<BudgetId, Budget>();
    }

    public List<Budget> findAll() {
        return new ArrayList<Budget>(budgetsMap.values());
    }

    public List<Budget> findByExercice(int exercice) {
        List<Budget> budgetsFiltres = new ArrayList<Budget>();
        Collection<Budget> budgets = budgetsMap.values();
        for (Budget currentBudget : budgets) {
            if (currentBudget.getExeOrdre() == exercice) {
                budgetsFiltres.add(currentBudget);
            }
        }
        return budgetsFiltres;
    }

    public Budget findById(long id) {
        return findById(new BudgetId(id));
    }

    public Budget findById(BudgetId budgetId) {
        return budgetsMap.get(budgetId);
    }

    public long countByExercice(int exercice) {
        return findByExercice(exercice).size();
    }

    public long countByExerciceAndVersion(int exercice, String codeVersion) {
        return 0;
    }

    public Budget create(Budget newBudget) {
        BudgetId budgetId = null;
        if (newBudget.getIdBudget() == null) {
            budgetId = new BudgetId(Long.valueOf(budgetsMap.size() + 1));
            newBudget.setIdBudget(budgetId);
        } else {
            budgetId = newBudget.getIdBudget();
        }
        budgetsMap.put(budgetId, newBudget);
        return newBudget;
    }

    public long getMaxNumeroByExercice(int exercice) {
        return 0;
    }

    public void enregistrerBudget(Budget budget, Long PersId) {

    }

}
