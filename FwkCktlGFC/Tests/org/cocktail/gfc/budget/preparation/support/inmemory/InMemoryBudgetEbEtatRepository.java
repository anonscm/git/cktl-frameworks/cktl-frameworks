/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.inmemory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.budget.preparation.EbEtat;
import org.cocktail.gfc.budget.preparation.EbEtatId;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.support.EbEtatRepository;

import com.google.common.base.Optional;
import com.mysema.commons.lang.Pair;

public class InMemoryBudgetEbEtatRepository implements EbEtatRepository {

    private Map<Pair<BudgetId,EntiteBudgetaireId>, EbEtat> budgetEbEtatMap;
    private InMemoryBudgetEbEtatReadRepository budgetEbEtatReadRepository;
   
    public InMemoryBudgetEbEtatRepository(InMemoryBudgetEbEtatReadRepository budgetEbEtatReadRepository) {
        this.budgetEbEtatMap = new HashMap<Pair<BudgetId,EntiteBudgetaireId>, EbEtat>();
        this.budgetEbEtatReadRepository = budgetEbEtatReadRepository;
    }
    
    public Optional<EbEtat> findByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId entiteBudgetaireId) {
        Pair<BudgetId, EntiteBudgetaireId> key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, entiteBudgetaireId);
        return Optional.fromNullable(this.budgetEbEtatMap.get(key));
    }

    public List<EbEtat> findByAllByBudgetIdAndListEntiteBudgetaireId(BudgetId budgetId, List<EntiteBudgetaireId> listeEntiteBudgetaireId) {
        List<EbEtat> listeBudgetEbEtat = new ArrayList<EbEtat>();
        EbEtat budgetEbEtat;
        Pair<BudgetId, EntiteBudgetaireId> key;
        for (EntiteBudgetaireId ebId : listeEntiteBudgetaireId) {
            key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, ebId);
            budgetEbEtat = budgetEbEtatMap.get(key);
            if (budgetEbEtat != null) {
                listeBudgetEbEtat.add(budgetEbEtat);
            }
        }
        return listeBudgetEbEtat;
    }

    public EbEtat findBudgetEbEtaById(EbEtatId id) {
        throw new UnsupportedOperationException();
    }

    public long addBudgetEbEtat(EbEtat budgetEbEtat) {
        BudgetId budId = budgetEbEtat.getBudgetId();
        EntiteBudgetaireId ebId = budgetEbEtat.getEbId();
        Pair<BudgetId, EntiteBudgetaireId> key = new Pair<BudgetId, EntiteBudgetaireId>(budId, ebId);
        this.budgetEbEtatMap.put(key, budgetEbEtat);
        budgetEbEtatReadRepository.modifyBudgetEbEtatRead(budId, ebId, budgetEbEtat.getEtat());
        return 0;
    }

    public long updateBudgetEbEtat(EbEtat budgetEbEtat) {
        Pair<BudgetId, EntiteBudgetaireId> key = new Pair<BudgetId, EntiteBudgetaireId>(budgetEbEtat.getBudgetId(), budgetEbEtat.getEbId());
        this.budgetEbEtatMap.put(key, budgetEbEtat);
        budgetEbEtatReadRepository.modifyBudgetEbEtatRead(budgetEbEtat.getBudgetId(), budgetEbEtat.getEbId(), budgetEbEtat.getEtat());
        return 0;
    }

    public long removeBudgetEbEtat(BudgetId budgetId, EntiteBudgetaireId entiteBudgetaireId) {
        Pair<BudgetId, EntiteBudgetaireId> key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, entiteBudgetaireId);
        this.budgetEbEtatMap.remove(key);
        budgetEbEtatReadRepository.modifyBudgetEbEtatRead(budgetId, entiteBudgetaireId, null);
        return 0;
    }

    public long updateBudgetEbEtatList(BudgetId budgetId, List<EntiteBudgetaireId> listeEntiteBudgetaireId, TypeEtat typeEtat) {
        Pair<BudgetId, EntiteBudgetaireId> key;
        for (EntiteBudgetaireId ebId : listeEntiteBudgetaireId) {
            key = new Pair<BudgetId, EntiteBudgetaireId>(budgetId, ebId);
            EbEtat budgetEbEtat = this.budgetEbEtatMap.get(key);
            if (budgetEbEtat != null) {
                budgetEbEtat.setEtat(typeEtat);
                budgetEbEtatReadRepository.modifyBudgetEbEtatRead(budgetId, ebId, typeEtat);
            }
        }
        return 0;
    }

	public List<EbEtat> findChildrenByBudgetIdAndEntiteBudgetaireId(BudgetId budgetId, EntiteBudgetaireId ebRacineId) {
	    throw new UnsupportedOperationException();
	}

    public List<EbEtat> findSubTreeByBudgetIdAndEbId(BudgetId budgetId, EntiteBudgetaireId ebRacineId) {
        throw new UnsupportedOperationException();
    }
}