/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.base.Optional;

public class RegleEBSaisissableTest {
    
    private static final EntiteBudgetaireId EB_ID = new EntiteBudgetaireId(1L);
    private static final EnveloppeBudgetaireId ENV_ID = new EnveloppeBudgetaireId(1L);
    private static final NatureDepenseId NAT_DEP_ID = new NatureDepenseId(1L);
    private static final DestinationDepenseId DEST_DEP_IS = new DestinationDepenseId(1L);
    
    private static final int EXER = 2015;
    private RegleEBSaisissableSurPrevisionDepense regle;
    @Mock private PrevisionDepenseHorsOp prevision;
    @Mock private NomenclatureFacade facade;
    @Mock private BudgetRepository budRepo;
    
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        regle = new RegleEBSaisissableSurPrevisionDepense(facade, budRepo);
        //la prévision utilise une EB avec un ID
        CombinaisonAxes combinaisonAxes = CombinaisonAxes.builder()
        		.entiteBudgetaireId(EB_ID)
        		.enveloppeId(ENV_ID)
        		.natureDepenseId(NAT_DEP_ID)
        		.destinationDepenseId(DEST_DEP_IS)
        		.build();
        Mockito.when(prevision.getCombinaison()).thenReturn(combinaisonAxes);
        //Le budget est sur une année
        Budget budget = Budget.builder().exeOrdre(EXER).build();
        Mockito.when(budRepo.findById(Mockito.any(BudgetId.class))).thenReturn(budget);
        
    }
    
    @Test
    public void ebSaisissable() {
        getEb(true);
        Assert.assertTrue(regle.isSatisfiedBy(prevision));
    }

    @Test
    public void ebNonSaisissable() {
        getEb(false);
        Assert.assertFalse(regle.isSatisfiedBy(prevision));
    }

    @Test
    public void ebPasTrouveeDansRepo() {
        Optional<EntiteBudgetaire> option = Optional.absent();
        Mockito.when(facade.findByIdAndExercice(EB_ID, EXER)).thenReturn(option);
        Assert.assertFalse(regle.isSatisfiedBy(prevision));
    }

    private void getEb(boolean tmp) {
        EntiteBudgetaire eb = EntiteBudgetaire.builder()
        		.saisissable(tmp)
        		.build();
        Mockito.when(facade.findByIdAndExercice(EB_ID, EXER)).thenReturn(Optional.of(eb));
    }

}
