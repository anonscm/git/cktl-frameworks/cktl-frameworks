/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtat;
import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Optional;

public class RegleEbEtatPermetSaisiePrevisionTest {
    
    private RegleEbEtatPermetSaisiePrevision regle;
    private BudgetId budId = new BudgetId(1L);
    private EntiteBudgetaireId ebId1 = new EntiteBudgetaireId(1L);
    EbEtat[] ebEtats;
    
    @Before
    public void init() {
        regle = new RegleEbEtatPermetSaisiePrevision();        
    }
    
    @Test
    public void ebEtatPermettantSaisie() {
        EbEtat ebEtat = null;
        assertTrue(regle.isSatisfiedBy(Optional.fromNullable(ebEtat)));
        assertTrue(regle.isSatisfiedBy(getOptionalEbEtat(TypeEtatService.Code.NON_RENSEIGNEE.etat())));
        
    }
    
    @Test
    public void ebEtatAutre() {
        assertFalse(regle.isSatisfiedBy(getOptionalEbEtat(TypeEtatService.Code.VERROUILLEE.etat())));
        assertFalse(regle.isSatisfiedBy(getOptionalEbEtat(TypeEtatService.Code.CONTROLEE.etat())));
        assertFalse(regle.isSatisfiedBy(getOptionalEbEtat(TypeEtatService.Code.VALIDEE.etat())));
        assertFalse(regle.isSatisfiedBy(getOptionalEbEtat(TypeEtatService.Code.OPERATION_NON_DECAISSABLE.etat())));
    }
    
    protected Optional<EbEtat> getOptionalEbEtat(TypeEtat typeEtat) {
        return Optional.of(new EbEtat(null, budId, ebId1, typeEtat));
    }
}
