/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Date;

import org.cocktail.gfc.budget.preparation.support.BudgetRepository;
import org.cocktail.gfc.budget.preparation.support.EtatBudgetRepository;
import org.cocktail.gfc.budget.preparation.support.VersionBudgetRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class BudgetFactoryTest {

    @Mock
    private BudgetRepository budgetRepository;

    @Mock
    private VersionBudgetRepository versionBudgetRepository;

    @Mock
    private EtatBudgetRepository etatBudgetRepository;

    private BudgetFactory factory;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        factory = Mockito.spy(new BudgetFactory(budgetRepository, versionBudgetRepository, etatBudgetRepository));
    }

    @Test
    public void testCreerBudgetInitial() {
        String libelle = "Budget Test";
        int exercice = 2014;
        Long persId = 4L;
        Date date = new Date();
        when(factory.getDateDuJour()).thenReturn(date);
        when(factory.getEtatEnPreparation())
                .thenReturn(new EtatBudget(1L, EtatBudgetCode.PREPARATION.toString(), "Libelle"));
        when(factory.getVersionBudget(VersionBudgetCode.BI))
                .thenReturn(new VersionBudget(1L, VersionBudgetCode.BI.toString(), "Libelle"));
        Budget budgetInitial = factory.creerBudgetInitial(libelle, exercice, persId);
        assertNotNull(budgetInitial);
        assertNull(budgetInitial.getIdBudget());
        assertEquals(libelle, budgetInitial.getLibelle());
        assertEquals(exercice, budgetInitial.getExeOrdre());
        assertEquals(Long.valueOf(persId), budgetInitial.getPersIdCreateur());
        assertEquals(Long.valueOf(0L), budgetInitial.getNumeroBudget());
        assertEquals(date, budgetInitial.getDateCreation());
        assertEquals(EtatBudgetCode.PREPARATION.toString(), budgetInitial.getEtatBudget().getCode());
        assertEquals(VersionBudgetCode.BI.toString(), budgetInitial.getVersionBudget().getCode());
    }

}
