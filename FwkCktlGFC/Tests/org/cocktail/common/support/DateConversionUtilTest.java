/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.common.support;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.ReadablePartial;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DateConversionUtilTest {
	private DateConversionUtil dateConversionUtil;
	
    @Before
    public void setUp() throws Exception {
        dateConversionUtil = new DateConversionUtil();        
    }
    
    @After
    public void tearDown() throws Exception {
    	dateConversionUtil = null;
    }	
	
	@Test
	public void testFormatDateWithTimeISO() {
		ReadablePartial readablePartial = new LocalDateTime(2015, 03, 20, 10, 23, 0, 789);
		String dateStr = dateConversionUtil.formatDateWithTimeISO(readablePartial);
		assertEquals("2015-03-20T10:23:00.789", dateStr);
	}
	
	@Test
	public void testFormatDateWithoutTimeISO1() {
		ReadablePartial readablePartial = new LocalDateTime(2015, 03, 20, 10, 23, 0, 789);
		String dateStr = dateConversionUtil.formatDateWithoutTimeISO(readablePartial);
		assertEquals("2015-03-20", dateStr);
	}
	
	@Test
	public void testFormatDateWithoutTimeISO2() {
		// Timestamp in milliseconds for 2015-03-20 10:23:00.000 : 1426846380000 
		String dateStr = dateConversionUtil.formatDateWithoutTimeISO(1426846380000L);
		assertEquals("2015-03-20", dateStr);
	}

	@Test
	public void testFormatDateShort() {
		ReadablePartial readablePartial = new LocalDateTime(2015, 03, 20, 10, 23, 0, 789);
		String dateStr = dateConversionUtil.formatDateShort(readablePartial);
		assertEquals("20/03/2015", dateStr);
	}
	
	@Test
	public void testParseDateTime1() {
		LocalDateTime dateExpected = new LocalDateTime(2015, 03, 20, 10, 23, 0, 789);
		LocalDateTime date = dateConversionUtil.parseDateTime("2015-03-20T10:23:00.789");
		assertEquals(dateExpected, date);
	}
	
	@Test
	public void testParseDateTime2() {
		LocalDateTime dateExpected = new LocalDateTime(2015, 03, 20, 0, 0, 0, 0);
		LocalDateTime date = dateConversionUtil.parseDateTime("2015-03-20");
		assertEquals(dateExpected, date);
	}
	
	@Test
	public void testParseDate1() {
		LocalDate dateExpected = new LocalDate(2015, 03, 20);
		LocalDate date = dateConversionUtil.parseDate("2015-03-20T10:23:00.789");
		assertEquals(dateExpected, date);
	}
	
	@Test
	public void testParseDate2() {
		LocalDate dateExpected = new LocalDate(2015, 03, 20);
		LocalDate date = dateConversionUtil.parseDate("2015-03-20");
		assertEquals(dateExpected, date);
	}
	
	@Test
	public void testParseDate3() {
		try {
			dateConversionUtil.parseDate("2015-03-32");
			fail("Une exception aurait dû être levée");
		} catch (Exception e){
		}
	}
	
	@Test
	public void testParseDateSilent1() {
		LocalDate dateExpected = new LocalDate(2015, 03, 20);
		LocalDate date = dateConversionUtil.parseDateSilent("2015-03-20T10:23:00.789");
		assertEquals(dateExpected, date);
	}
	
	@Test
	public void testParseDateSilent2() {
		LocalDate dateExpected = new LocalDate(2015, 03, 20);
		LocalDate date = dateConversionUtil.parseDateSilent("2015-03-20");
		assertEquals(dateExpected, date);
	}
	
	@Test
	public void testParseDateSilent3() {
		LocalDate date;
		date = dateConversionUtil.parseDateSilent("2015-03-32");
		assertNull(date);
	}
	
	@Test
	public void testParseDateTimeSilent1() {
		LocalDateTime dateExpected = new LocalDateTime(2015, 03, 20, 10, 23, 0, 789);
		LocalDateTime date = dateConversionUtil.parseDateTimeSilent("2015-03-20T10:23:00.789");
		assertEquals(dateExpected, date);
	}
	
	@Test
	public void testParseDateTimeSilent2() {
		LocalDateTime date;
		date = dateConversionUtil.parseDateTimeSilent("2015-03-32");
		assertNull(date);
	}
	
	@Test
	public void testIsValidDateTimeISO1() {
		String dateStr = "2015-03-20T10:23:00.789";
		assertTrue(dateConversionUtil.isValidDateTimeISO(dateStr));
	}
	
	@Test
	public void testIsValidDateTimeISO2() {
		String dateStr = "2015-03-32T10:23:00.000";
		assertFalse(dateConversionUtil.isValidDateTimeISO(dateStr));
	}
	
	public void testIsValidDateISO1() {
		String dateStr = "2015-03-20";
		assertTrue(dateConversionUtil.isValidDateISO(dateStr));
	}
	
	@Test
	public void testIsValidDateISO2() {
		String dateStr = "2015-03-32";
		assertFalse(dateConversionUtil.isValidDateISO(dateStr));
	}
}
