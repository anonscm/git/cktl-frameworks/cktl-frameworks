/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.common.support;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.cocktail.gfc.support.exception.GfcValidationException;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

public class CktlGFCDateTest {

    // Tests de validation de la classe CktlGFCDate

    @Test
    public void testCktlGFCDate() {
        CktlGFCDate gfcDate = new CktlGFCDate();
        assertNull(gfcDate.getDate());
    }

    @Test
    public void testPremierJanvier() throws GfcValidationException {
        CktlGFCDate gfcDate = CktlGFCDate.premierJanvier(2030);
        assertEquals(new LocalDate(2030, 1, 1), gfcDate.getDate().toLocalDate());
    }

    @Test
    public void testToday() {
        CktlGFCDate gfcDate = CktlGFCDate.today();
        assertEquals(new DateTime().toLocalDate(), gfcDate.getDate().toLocalDate());
    }

    @Test
    public void testGetToday() {
        CktlGFCDate gfcDate = new CktlGFCDate();
        CktlGFCDate gfcToday = gfcDate.getToday();
        assertEquals(new DateTime().toLocalDate(), gfcToday.getDate().toLocalDate());
    }

    @Test
    public void testGetNow() {
        DateTime nowPrec = new DateTime();
        CktlGFCDate gfcDate = new CktlGFCDate();
        CktlGFCDate gfcNow = gfcDate.getNow();
        DateTime now = gfcNow.getDate();
        DateTime nowPost = new DateTime();

        Assert.assertTrue(now.isAfter(nowPrec) || now.equals(nowPrec));
        Assert.assertTrue(now.isBefore(nowPost) || now.equals(nowPost));

    }

    @Test
    public void testSetDate1() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        Object objectDate = null;
        gfcDate.setDate(objectDate);
        assertNull(gfcDate.getDate());
    }

    @Test
    public void testSetDate2() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        Object objectDate = new DateTime().toString();
        gfcDate.setDate(objectDate);
        assertEquals(objectDate, gfcDate.getDate().toString());
    }

    @Test
    public void testSetDate3() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        Object objectDate = new DateTime();
        gfcDate.setDate(objectDate);
        assertEquals(objectDate, gfcDate.getDate());
    }

    @Test
    public void testSetDate4() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        Object objectDate = new LocalDate();
        gfcDate.setDate(objectDate);
        assertEquals(objectDate, gfcDate.getDate().toLocalDate());
    }

    @Test
    public void testSetDate5() {
        CktlGFCDate gfcDate = new CktlGFCDate();
        Object objectDate = new Integer(0);
        try {
            gfcDate.setDate(objectDate);
            fail("Une exception de type GfcValidationException aurait dû être levée");
        } catch (GfcValidationException e) {
        }
    }

    @Test
    public void testIsNull1() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        gfcDate.setDate(null);
        assertTrue(gfcDate.isNull());
    }

    @Test
    public void testIsNull2() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        assertFalse(gfcDate.isNull());
    }

    @Test
    public void testIsNotNull1() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        assertTrue(gfcDate.isNotNull());
    }

    @Test
    public void testIsNotNull2() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        gfcDate.setDate(null);
        assertFalse(gfcDate.isNotNull());
    }

    @Test
    public void testIsBeforeToday1() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate().minusDays(1));
        assertTrue(gfcDate.isBeforeToday());
    }

    @Test
    public void testIsBeforeToday2() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        assertTrue(gfcDate.isBeforeToday());
    }

    @Test
    public void testIsBeforeToday3() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate().plusDays(1));
        assertFalse(gfcDate.isBeforeToday());
    }

    @Test
    public void testIsAfterToday1() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate().plusDays(1));
        assertTrue(gfcDate.isAfterToday());
    }

    @Test
    public void testIsAfterToday2() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        assertTrue(gfcDate.isAfterToday());
    }

    @Test
    public void testIsAfterToday3() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate().minusDays(1));
        assertFalse(gfcDate.isAfterToday());
    }

    @Test
    public void testIsBefore1() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        CktlGFCDate gfcDateToday = new CktlGFCDate(new LocalDate());
        assertFalse(gfcDate.isBefore(gfcDateToday));
    }

    @Test
    public void testIsBefore2() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        assertTrue(gfcDate.isBefore(null));
    }

    @Test
    public void testIsBefore3() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate().plusDays(1));
        assertFalse(gfcDate.isBefore(null));
    }

    @Test
    public void testIsBefore4() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        CktlGFCDate gfcDateToday = new CktlGFCDate(new LocalDate());
        assertTrue(gfcDate.isBefore(gfcDateToday));
    }

    @Test
    public void testIsBefore5() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        CktlGFCDate gfcDateTomorrow = new CktlGFCDate(new LocalDate().plusDays(1));
        assertTrue(gfcDate.isBefore(gfcDateTomorrow));
    }

    @Test
    public void testIsBefore6() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        CktlGFCDate gfcDateYesterday = new CktlGFCDate(new LocalDate().minusDays(1));
        assertFalse(gfcDate.isBefore(gfcDateYesterday));
    }

    @Test
    public void testIsAfter1() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate();
        CktlGFCDate gfcDateToday = new CktlGFCDate(new LocalDate());
        assertFalse(gfcDate.isAfter(gfcDateToday));
    }

    @Test
    public void testIsAfter2() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        assertTrue(gfcDate.isAfter(null));
    }

    @Test
    public void testIsAfter3() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate().minusDays(1));
        assertFalse(gfcDate.isAfter(null));
    }

    @Test
    public void testIsAfter4() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        CktlGFCDate gfcDateToday = new CktlGFCDate(new LocalDate());
        assertTrue(gfcDate.isAfter(gfcDateToday));
    }

    @Test
    public void testIsAfter5() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        CktlGFCDate gfcDateYesterday = new CktlGFCDate(new LocalDate().minusDays(1));
        assertTrue(gfcDate.isAfter(gfcDateYesterday));
    }

    @Test
    public void testIsAfter6() throws GfcValidationException {
        CktlGFCDate gfcDate = new CktlGFCDate(new LocalDate());
        CktlGFCDate gfcDateTomorrow = new CktlGFCDate(new LocalDate().plusDays(1));
        assertFalse(gfcDate.isAfter(gfcDateTomorrow));
    }

    @Test
    public void testToDateWithoutTime1() {
        CktlGFCDate gfcDate = new CktlGFCDate();
        assertNull(gfcDate.toDateWithoutTime());
    }

    @Test
    public void testToDateWithoutTime2() throws GfcValidationException {
        DateTime dateTime = new DateTime();
        CktlGFCDate gfcDate = new CktlGFCDate(dateTime);
        assertEquals(dateTime.toString(DateConversionUtil.DATE_WITHOUT_TIME_FORMATTER), gfcDate.toDateWithoutTime());
    }

    @Test
    public void testToDateWithTime1() {
        CktlGFCDate gfcDate = new CktlGFCDate();
        assertNull(gfcDate.toDateWithoutTime());
    }

    @Test
    public void testToDateWithTime2() throws GfcValidationException {
        DateTime dateTime = new DateTime();
        CktlGFCDate gfcDate = new CktlGFCDate(dateTime);
        assertEquals(dateTime.toString(DateConversionUtil.DATE_WITH_TIME_FORMATTER), gfcDate.toDateWithTime());
    }

    @Test
    public void testToString1() {
        CktlGFCDate gfcDate = new CktlGFCDate();
        assertNull(gfcDate.toString());
    }

    @Test
    public void testToString2() throws GfcValidationException {
        DateTime dateTime = new DateTime();
        CktlGFCDate gfcDate = new CktlGFCDate(dateTime);
        assertEquals(dateTime.toString(), gfcDate.toString());
    }
}
