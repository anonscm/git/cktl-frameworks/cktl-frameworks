/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server;

import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Représente un utilisateur d'application pour le frmk fwkCktlGFCEos (utilisateur GFC).
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class GFCApplicationUser extends ApplicationUser {

	private static final String OUI = "O";
	public static final String TYAP_STR_ID_DEPENSE = "DEPENSE";
	public static final String TYAP_STR_ID_JEFYADMIN = "JEFYADMIN";
	public static final String TYAP_STR_ID_SCOLARIX = "SCOLARIX";

	private IPersonne iPersonne = null;
	
	/** Codes analytiques */
	public static final String FON_ID_ADCANAL = "ADCANAL";

	/** Codes analytiques restreints */
	public static final String FON_ID_ADCANALR = "ADCANALR";

	/** admin/utilisateurs */
	public static final String FON_ID_ADUTA = "ADUTA";

	/** affectation de lignes budgétaires */
	public static final String FON_ID_ADUTORG = "ADUTORG";

	/** Droit sur toutes les lignes budgétaires */
	public static final String FON_ID_TOUTORG = "TOUTORG";

	/** Organigramme budgétaire */
	public static final String FON_ID_ADORGAN = "ADORGAN";

	/** actions / destinations depenses */
	public static final String FON_ID_ADACTLOD = "ADACTLOD";

	/** actions / destinations recettes */
	public static final String FON_ID_ADACTLOR = "ADACTLOR";

	/** Types de crédit */
	public static final String FON_ID_ADTCD = "ADTCD";

	/** Exercices */
	public static final String FON_ID_ADEXER = "ADEXER";

	/** Signatures */
	public static final String FON_ID_ADSIGN = "ADSIGN";

	public static final String FON_ID_ADTVA = "ADTVA";

	public static final String FON_ID_ADPRORAT = "ADPRORAT";

	public static final String FON_ID_ADPARAM = "ADPARAM";

	public static final String IFON_ID_ADPJ = "ADPJ";

	public static final String FON_ID_IMADTAUX = "IMADTAUX";
	public static final String FON_ID_IMADDGP = "IMADDGP";

	/** Bordereaux */
	public static final String FON_ID_BORPAI = "BORPAI";
	public static final String FON_ID_BORRMB = "BORRMB";

	private EOUtilisateur jefyAdminUtilisateur;

	public GFCApplicationUser(EOEditingContext ec, EOUtilisateur utilisateur) {
		super(ec, utilisateur);
	}

	public GFCApplicationUser(EOEditingContext ec, Integer persId) {
		super(ec, persId);
	}

	public GFCApplicationUser(EOEditingContext ec, String tyapStrId, EOUtilisateur utilisateur) {
		super(ec, tyapStrId, utilisateur);
	}

	public GFCApplicationUser(EOEditingContext ec, String tyapStrId, Integer persId) {
		super(ec, tyapStrId, persId);
	}

	public boolean hasDroitSaisieCodeAnalytiquesAll() {
		return this.isFonctionAutoriseeByFonID(TYAP_STR_ID_JEFYADMIN, FON_ID_ADCANAL, null);
	}

	public boolean hasDroitSaisieCodeAnalytiquesPrives() {
		return this.isFonctionAutoriseeByFonID(TYAP_STR_ID_JEFYADMIN, FON_ID_ADCANALR, null);
	}

	public boolean hasDroitGererOrganigrammeBudgetaire() {
		return this.isFonctionAutoriseeByFonID(TYAP_STR_ID_JEFYADMIN, FON_ID_ADORGAN, null);
	}

	public boolean hasDroitCreerBordereauInscription() {
		return this.isFonctionAutoriseeByFonID(TYAP_STR_ID_SCOLARIX, FON_ID_BORPAI, null);
	}

	public boolean hasDroitCreerBordereauRemboursement() {
		return this.isFonctionAutoriseeByFonID(TYAP_STR_ID_SCOLARIX, FON_ID_BORRMB, null);
	}

	/**
	 * @param ec
	 * @param exercice Facultatif.
	 * @return Les organ affectees a l'utilisateur sur l'exercice si celui-ci est precisé.
	 */
	public NSArray getOrgansAutorisees(EOEditingContext ec, EOExercice exercice) {
		EOUtilisateur util;
		util = EOUtilisateur.fetchByKeyValue(
				ec,
				EOUtilisateur.PERS_ID_KEY,
				this.personne.persId());

		//        NSArray res =  (NSArray) util.valueForKeyPath( _EOUtilisateur.UTILISATEUR_ORGANS_KEY+"."+ EOUtilisateurOrgan.ORGAN_KEY);

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOEb.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurEb.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, util));
		if (exercice != null) {
			quals.addObject(EOEb.qualOrganOuvertes(exercice));
		}
		NSArray res = EOEb.fetchAll(ec, new EOAndQualifier(quals), new NSArray(new Object[] {
				EOEb.SORT_ORG_UNIV_ASC, EOEb.SORT_ORG_ETAB_ASC, EOEb.SORT_ORG_UB_ASC, EOEb.SORT_ORG_CR_ASC, EOEb.SORT_ORG_SOUSCR_ASC
		}));
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				EOSortOrdering.sortOrderingWithKey(EOEb.LONG_STRING_KEY, EOSortOrdering.CompareAscending)
		}));
	}

	/**
	 * @param ec
	 * @param exercice Facultatif.
	 * @return Les organ affectees a l'utilisateur sur l'exercice si celui-ci est precisé.
	 */
	public NSArray getOrgansAutorisees(EOEditingContext ec, String exercice) {
		EOExercice exer = EOExercice.fetchByKeyValue(ec, EOExercice.EXE_EXERCICE_KEY, Integer.valueOf(exercice));
		return getOrgansAutorisees(ec, exer);
	}


	/**
	 * @return L'objet IPersonne identifiant l'utilisateur.
	 */
	public IPersonne iPersonne() {
		if (iPersonne == null) {
			if (getPersonne() != null) {
				if (getPersonne().isStructure()) {
					iPersonne = EOStructure.structurePourCode(getEditingContext(), String.valueOf(getPersonne().persOrdre()));
				}
				else {
					iPersonne = EOIndividu.individuPourNumero(getEditingContext(), getPersonne().persOrdre());
				}
			}
		}
		return iPersonne;
	}
	
	/**
	 * @return Les services dont depend l'utilisateur. (Tableau de EOStructure). L'utilisateur doit etre un individu.
	 */
	public NSArray getServices() {
		NSArray res = NSArray.EmptyArray;
		if (iPersonne() != null && (iPersonne() instanceof EOIndividu)) {
			res = ((EOIndividu) iPersonne()).getServices();
		}
		return res;
	}	

	/**
	 * @return Les établissements affectes à l'utilisateur. Tableau de EOStructure). L'utilisateur doit etre un individu.
	 */
	public NSArray getEtablissementsAffectation() {
		NSArray res = NSArray.EmptyArray;
		if (iPersonne() != null && (iPersonne() instanceof EOIndividu)) {
			res = ((EOIndividu) iPersonne()).getEtablissementsAffectation(null);
		}
		return res;
	}

}
