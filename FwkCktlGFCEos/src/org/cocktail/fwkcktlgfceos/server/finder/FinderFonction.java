/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.metier.EOFonction;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public final class FinderFonction extends Finder {

	/**
	 * Recherche d'une fonction suivant son identifiant et le type d'application.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param libelle
	 *        identifiant de la fonction
	 * @param typeApplication
	 *        type de l'application qui utilise cette fonction
	 * @return
	 *        une EOFonction
	 */
    public static final EOFonction getFonction(EOEditingContext ed, String libelle, EOTypeApplication typeApplication) {
    	return getUneFonction(ed, libelle, typeApplication);
    }

	/**
	 * Recherche d'une fonction suivant son identifiant et le type d'application.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param libelle
	 *        identifiant de la fonction
	 * @param typeApplication
	 *        type de l'application qui utilise cette fonction
	 * @return
	 *        une EOFonction
	 */
    private static EOFonction getUneFonction(EOEditingContext ed, String libelle, EOTypeApplication typeApplication) {
    	NSArray lesFonctions=null;
    	EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.FON_ID_INTERNE_KEY+"=%@ and "+EOFonction.TYPE_APPLICATION_KEY+"."+EOTypeApplication.TYAP_LIBELLE_KEY+"=%@", 
				new NSArray(new Object[]{libelle, typeApplication.tyapLibelle()}));
   	
    	lesFonctions = Finder.fetchArray(EOFonction.ENTITY_NAME, qual, null, ed, true);
    	if (lesFonctions==null || lesFonctions.count()==0) {
    		return null;
    	}
    	
        return (EOFonction)lesFonctions.objectAtIndex(0);
    }

}
