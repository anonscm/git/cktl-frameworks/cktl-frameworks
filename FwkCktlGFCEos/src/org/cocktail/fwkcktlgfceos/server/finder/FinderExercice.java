/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.finder;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public final class FinderExercice extends Finder {

	/**
	 * Recherche d'un exercice non clos correspondant a une date.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param date
	 *        date de reference pour laquelle on veut l'exercice
	 * @return
	 *        un EOExercice
	 */
    public static EOExercice getExerciceNonClos(EOEditingContext ed, NSTimestamp date) {
    	Integer exe = Integer.valueOf(new SimpleDateFormat("yyyy").format(date));
    	EOExercice exer =  EOExercice.fetchByKeyValue(ed, EOExercice.EXE_EXERCICE_KEY, exe);
    	if (exer == null) {
    		NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
    		array.addObject(new EOKeyValueQualifier(EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, exe));
    		array.addObject(new EOKeyValueQualifier(EOExercice.EXE_STAT_KEY, EOQualifier.QualifierOperatorNotEqual, EOExercice.EXE_ETAT_CLOS));
    		NSArray<EOExercice> res = EOExercice.fetchAll(ed, new EOAndQualifier(array), new NSArray<EOSortOrdering>(EOExercice.SORT_EXE_EXERCICE_DESC));
    		if (res.count() > 0) {
    			return res.objectAtIndex(0);
    		}
    	}
    	return exer;
    }
    
    /**
     * Recherche du dernier exercice ouvert ou en préparation
     * <BR>
     * @param ed
     *        editingContext dans lequel se fait le fetch
     * @return
     *        un EOExercice
     */
    public static EOExercice getDernierExerciceOuvertOuEnPreparation(EOEditingContext ed) {
        NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
        array.addObject(new EOKeyValueQualifier(EOExercice.EXE_STAT_KEY, EOQualifier.QualifierOperatorEqual, EOExercice.EXE_ETAT_OUVERT));
        array.addObject(new EOKeyValueQualifier(EOExercice.EXE_STAT_KEY, EOQualifier.QualifierOperatorEqual, EOExercice.EXE_ETAT_PREPARATION));
        NSArray<EOExercice> res = EOExercice.fetchAll(ed, new EOOrQualifier(array), new NSArray<EOSortOrdering>(EOExercice.SORT_EXE_EXERCICE_DESC));
        if (res.count() > 0) {
            return (EOExercice) res.objectAtIndex(0);
        }
        return null;
    }

    public static final NSArray getExercices(EOEditingContext ed) {
    	return FinderExercice.getLesExercices(ed);
    }
    
    private static final NSArray getLesExercices(EOEditingContext ed) {
    	return fetchExercices(ed);
    }
    
    
    
    /**
     * @param ed
     * @param date
     * @return Le premier exercice ouvert pour les depenses (engagement ouvert ou restreint) trouvé pour la date.
     */
    public static final EOExercice getExerciceDepensePourDate(EOEditingContext ed, NSTimestamp date) {
    	return getExerciceDepensePourUneDate(ed, date);
    }

	/**
	 * Recherche d'un exercice correspondant a une date.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param date
	 *        date de reference pour laquelle on veut l'exercice
	 * @return
	 *        un EOExercice
	 */
    private static final EOExercice getExerciceDepensePourUneDate(EOEditingContext ed, NSTimestamp date) {
       	NSMutableArray arrayQualifier=new NSMutableArray();
       	NSArray larray;
    	    	
       	NSArray arrayExercices=fetchExercices(ed);
    	
    	arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_CLOTURE_KEY+"=nil or "+
    			EOExercice.EXE_CLOTURE_KEY+">=%@", new NSArray(date)));
    	arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_OUVERTURE_KEY+"<=%@", new NSArray(date)));
    	larray = EOQualifier.filteredArrayWithQualifier(arrayExercices, new EOAndQualifier(arrayQualifier));
   	
    	if (larray.count()!=1) {
    		for (int i=0; i<larray.count(); i++)
    			if (((EOExercice)larray.objectAtIndex(i)).estEngOuvert())
    				return (EOExercice)larray.objectAtIndex(i);

    		for (int i=0; i<larray.count(); i++)
    			if (((EOExercice)larray.objectAtIndex(i)).estEngRestreint())
    				return (EOExercice)larray.objectAtIndex(i);
    		
    		return (EOExercice)larray.objectAtIndex(0);
    	}
    	
    	return (EOExercice)larray.objectAtIndex(0);
    }

//    /**
//     * Recherche des exercices ouverts a un utilisateur.<BR>
//     * @param ed
//     * @param utilisateur
//     * @return
//     */
//    public static final NSArray getExercicesOuvertsPourUtilisateur(EOEditingContext ed, EOUtilisateur utilisateur) {
//    	NSMutableArray array=new NSMutableArray();
//    	
//    	// on verifie si l'utilisateur a les droits a ce jour
//    	if (MyDateCtrl.now().before(utilisateur.utlOuverture()))
//    		return new NSArray();
//    	if (utilisateur.utlFermeture()!=null && utilisateur.utlFermeture().before(MyDateCtrl.now()))
//    		return new NSArray();
//
//    	// on prend tout les exercices engageables
//    	array.addObjectsFromArray(FinderExercice.getExercicesEngageablesPourUtilisateur(ed, utilisateur));
//    	
//    	// on recupere l'exercice en cours
//    	EOExercice exerciceCourant=getExercicePourUneDate(ed, MyDateCtrl.now());
//    	
//    	// on regarde si cet exercice est pas present dans le array sinon on le rajoute
//    	if (exerciceCourant.estEngOuvert()) {
//    		if (!array.containsObject(exerciceCourant))
//    			array.addObject(exerciceCourant);
//    	}
//    	
//    	return array;
//    }
//    
//    /**
//     * Recherche des exercices pour lesquels l'utilisateur a le droit d'engager.<BR>
//     * @param ed
//     * @param utilisateur
//     * @return
//     */
//    public static final NSArray getExercicesEngageablesPourUtilisateur(EOEditingContext ed, EOUtilisateur utilisateur) {
//    	NSArray	larray=new NSArray();
//    	NSMutableArray lesExercices=new NSMutableArray();
//    	NSMutableDictionary mesBindings=new NSMutableDictionary();
//    	
//    	// on verifie si l'utilisateur a les droits a ce jour
//    	if (MyDateCtrl.now().before(utilisateur.utlOuverture()))
//    		return new NSArray();
//    	if (utilisateur.utlFermeture()!=null && utilisateur.utlFermeture().before(MyDateCtrl.now()))
//    		return new NSArray();
//    	
//    	// recuperer les exercices correspondants au droit d'engager hors periode d'inventaire
//    	EOUtilisateurFonction utilisateurFonction;
//
//    	utilisateurFonction=FinderUtilisateurFonction.getUtilisateurFonction(ed, utilisateur, EOFonction.FONCTION_ENGAGE);
//    	if (utilisateurFonction!=null) {
//        	mesBindings=new NSMutableDictionary();
//    		mesBindings.setObjectForKey(utilisateurFonction, "utilisateurFonction");
//    		larray=FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, mesBindings);
//
//    		// trier les exercices pour garder ceux qui sont engageables
//    		for (int i=0; i<larray.count(); i++) {
//    			EOExercice exercice=((EOUtilisateurFonctionExercice)larray.objectAtIndex(i)).exercice();   		
//    			if (exercice!=null && exercice.estEngageable())
//    				lesExercices.addObject(exercice);
//    		}
//    	}
//
//    	// recuperer les exercices correspondants au droit d'engager pendant la periode d'inventaire   	
//    	utilisateurFonction=FinderUtilisateurFonction.getUtilisateurFonction(ed, utilisateur, EOFonction.FONCTION_ENGAGE_PERIODE_INVENTAIRE);
//    	if (utilisateurFonction!=null) {
//        	mesBindings=new NSMutableDictionary();
//    		mesBindings.setObjectForKey(utilisateurFonction, "utilisateurFonction");
//    		larray=FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, mesBindings);
//
//    		// trier les exercices pour garder ceux qui sont engageables
//    		for (int i=0; i<larray.count(); i++) {
//    			EOExercice exercice=((EOUtilisateurFonctionExercice)larray.objectAtIndex(i)).exercice();   		
//    			if (exercice != null && exercice.estEngageableRestreint())
//    				lesExercices.addObject(exercice);
//    		}
//    	}
//
//    	return lesExercices;
//    }
//    
//    public static final boolean isExercicePrestationPourUtilisateur(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur) {
//    	
//    	if (exercice==null || utilisateur==null)
//    		return false;
//    	
//		NSMutableDictionary mesBindings=new NSMutableDictionary();
//		
//    	mesBindings.setObjectForKey(utilisateur, "utilisateur");
//    	mesBindings.setObjectForKey(exercice, "exercice");
//    	mesBindings.setObjectForKey(EOFonction.FONCTION_CREATION_PRESTATION, "fonIdInterne");
//
//		return (FinderUtilisateurFonction.getUtilisateurFontion(ed, mesBindings)!=null);
//
//    }
//    
//    /**
//     * Recherche des exercices pour lesquels l'utilisateur a le droit de liquider.<BR>
//     * @param ed
//     * @param utilisateur
//     * @return
//     */
//    public static final NSArray getExercicesLiquidablesPourUtilisateur(EOEditingContext ed, EOUtilisateur utilisateur) {
//    	NSArray	larray=new NSArray();
//    	NSMutableArray lesExercices=new NSMutableArray();
//    	NSMutableDictionary mesBindings=new NSMutableDictionary();
//    	
//    	// on verifie si l'utilisateur a les droits a ce jour
//    	if (ZDateUtil.currentDateNSTimeStamp().before(utilisateur.utlOuverture()))
//    		return new NSArray();
//    	if (utilisateur.utlFermeture()!=null && utilisateur.utlFermeture().before(ZDateUtil.currentDateNSTimeStamp()))
//    		return new NSArray();
//    	
//    	// recuperer les exercices correspondants au droit de liquider hors periode d'inventaire
//    	EOUtilisateurFonction utilisateurFonction;
//
//    	
//    	utilisateurFonction=FinderUtilisateurFonction.getUtilisateurFonction(ed, utilisateur, EOFonction.FONCTION_LIQUIDE);
//    	if (utilisateurFonction!=null) {
//    		mesBindings=new NSMutableDictionary();
//    		mesBindings.setObjectForKey(utilisateurFonction, "utilisateurFonction");
//    		larray=FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, mesBindings);
//
//    		// trier les exercices pour garder ceux qui sont liquidables
//    		for (int i=0; i<larray.count(); i++) {
//    			EOExercice exercice=((EOUtilisateurFonctionExercice)larray.objectAtIndex(i)).exercice();   		
//    			if (exercice!=null && exercice.estLiquidable())
//    				lesExercices.addObject(exercice);
//    		}
//    	}
//    	
//    	// recuperer les exercices correspondants au droit de liquider pendant la periode d'inventaire
//    	
//    	utilisateurFonction=FinderUtilisateurFonction.getUtilisateurFonction(ed, utilisateur, EOFonction.FONCTION_LIQUIDE_PERIODE_INVENTAIRE);
//    	if (utilisateurFonction!=null) {
//    		mesBindings=new NSMutableDictionary();
//    		mesBindings.setObjectForKey(utilisateurFonction, "utilisateurFonction");
//    		larray=FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, mesBindings);
//
//    		// trier les exercices pour garder ceux qui sont liquidables
//    		for (int i=0; i<larray.count(); i++) {
//    			EOExercice exercice=((EOUtilisateurFonctionExercice)larray.objectAtIndex(i)).exercice();   		
//    			if (exercice!=null && exercice.estLiquidableRestreint())
//    				lesExercices.addObject(exercice);
//    		}
//    	}
//    	return lesExercices;
//    }

    /**
     * Fetch l'exercice correspondant a l'exeOrdre
     * <BR>
     * @param ed
     *        editingContext dans lequel se fait le fetch
     * @param exeOrdre
     *        cle de l'exercice
     */
	public static EOExercice getExercice(EOEditingContext edc, Integer exeOrdre) {
		return EOExercice.fetchByKeyValue(edc, EOExercice.EXE_EXERCICE_KEY,exeOrdre);
	}

	/**
     * Fetch tous les exercices pour les garder en memoire et eviter de refetcher ensuite
     * <BR>
     * @param ed
     *        editingContext dans lequel se fait le fetch
     */
    private static NSArray fetchExercices(EOEditingContext ed) {
    	return Finder.fetchArray(ed,EOExercice.ENTITY_NAME,null,null,
    			new NSArray(EOSortOrdering.sortOrderingWithKey(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending)),false);
    }

}
