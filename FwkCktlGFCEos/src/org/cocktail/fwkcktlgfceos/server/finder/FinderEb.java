/**
 * 
 */
package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author Raymond NANEON <raymond.naneon at utt.fr>
 *
 */
public class FinderEb extends Finder {
	
    /**
     * @param edc - editing context
     * @param exeOrdre - exercice
     * @return liste d'EB
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EOEb> findByExercice(EOEditingContext edc, Number exeOrdre) {
		EOQualifier qualExercice = EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXE_ORDRE).eq(exeOrdre.intValue()).and(EOEb.ORG_NIVEAU.greaterThanOrEqualTo(EOEb.ORG_NIV_2));
		return  (NSArray<EOEb>) Finder.fetchArray(EOEb.ENTITY_NAME, qualExercice, EOEb.SORT_DEFAULT, edc, false);
	}

}
