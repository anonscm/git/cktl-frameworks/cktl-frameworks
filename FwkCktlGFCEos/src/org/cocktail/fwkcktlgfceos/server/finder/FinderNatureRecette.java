package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.metier.EONatureRec;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureRecExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public class FinderNatureRecette extends Finder {

	/**
	 * @param edc - editing context
	 * @param exeOrdre - exercice (int)
	 * @return liste des natures sur l'exercice
	 */
	@SuppressWarnings("unchecked")
    public static NSArray<EONatureRec> findByExercice(EOEditingContext edc, int exeOrdre) {
        EOQualifier qualValideEtSurExercice = EONatureRec.ADM_NATURE_REC_EXERCICES.dot(EONatureRecExercice.EXE_ORDRE).eq(exeOrdre);
        return (NSArray<EONatureRec>) fetchArray(EONatureRec.ENTITY_NAME, qualValideEtSurExercice, EONatureRec.LIBELLE.descs(), edc, false);
    }

	/**
	 * @param edc - editing context
	 * @param exeOrdre - exercice (long)
	 * @return liste des natures sur l'exercice
	 */
	public static NSArray<EONatureRec> findByExercice(EOEditingContext edc, Long exeOrdre) {
	    return findByExercice(edc, exeOrdre.intValue());
    }
}