/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/*
 * TODO 
 * 
 * finder utilisateur qui recupere avec un utilisateur en parametre ceux dont il peut voir les precommandes,
 * suivant les c_structure des organ auxquels il a le droit
 */

/*
 * TODO
 * le getUrilisateurs de FinderUtilisateur "elimine" systematiquement les utilisateurs deja contactes
 [13:26:44] ULR - Emmanuel GEZE a dit : il faudrait donc creer une autre api qui donne tous les utilsateurs
 [13:27:25] ULR - Emmanuel GEZE a dit : en effet dasn certains cas je ne souhaite pas recuperer uniquement les utilisateurs non contactes
 [13:27:50] ULR - Emmanuel GEZE a dit : Pour faire le distinct c un peu embetant car il faut le faire sur l'individu
 [13:28:48] ULR - Emmanuel GEZE a dit : en effet le fetch ramene des doublons meme si je ne compren pas pourquoi ?
 [13:28:51] ULR - Emmanuel GEZE a dit : a+
 [13:38:12] ULR - Emmanuel GEZE a dit : j'ai fait le tri et le distinct, je les remonte
 [13:38:53] ULR - Emmanuel GEZE a dit : je crois qu'il faudrait creer une api getUtilisateursNonContactes equivalente a getUtilisaterus aujourd'hui
 [13:39:14] ULR - Emmanuel GEZE a dit : et modifier getUtilisateurs pour qu'elle remonte tous les utilisateurs
 [13:39:20] ULR - Emmanuel GEZE a dit : qu'en penses-etu ?
 */

/**
 * @author egeze
 */
public final class FinderUtilisateur extends Finder {

	public static final NSArray getUtilisateursContactes(EOEditingContext ed, EOUtilisateur utilisateur) {
		return new NSArray();
		/*
		 * NSMutableDictionary mesBindings=new NSMutableDictionary(); mesBindings.setObjectForKey(utilisateur, "utilisateur");
		 * 
		 * NSArray array=EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOUtilisateurPreferenceMail.ENTITY_NAME, "Recherche", mesBindings);
		 * 
		 * return Finder.tableauTrie((NSArray)array.valueForKeyPath(EOUtilisateurPreferenceMail.UTILISATEUR_MAIL_KEY), sort());
		 */
	}

	public static final NSArray getUtilisateurs(EOEditingContext ed, EOUtilisateur utilisateur,
			NSArray utilisateursContactes) {

		if (utilisateursContactes == null || utilisateursContactes.count() == 0)
			utilisateursContactes = getUtilisateursContactes(ed, utilisateur);

		NSMutableDictionary mesBindings = new NSMutableDictionary();
		mesBindings.setObjectForKey(FinderTypeEtat.getTypeEtat(ed, EOTypeEtat.ETAT_VALIDE), "typeEtat");
		mesBindings.setObjectForKey(new NSTimestamp(), "date");

		NSArray res = null;
		try {
			res = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOUtilisateur.ENTITY_NAME, "Recherche", mesBindings);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		NSMutableArray array = new NSMutableArray(res);

		for (int i = 0; i < utilisateursContactes.count(); i++) {
			EOUtilisateur utilisateurContacte = (EOUtilisateur) utilisateursContactes.objectAtIndex(i);
			array.removeObject(utilisateurContacte);
		}

		return Finder.tableauTrie(array, sort());
	}

	//	public static final NSArray getUtilisateursContactes(EOEditingContext ed, EOUtilisateur utilisateur, EOCommande commande) {
	//		NSArray array=getUtilisateursContactes(ed, utilisateur);
	//		NSMutableArray resultats=new NSMutableArray();
	//
	//		for (int i=0; i<array.count(); i++) {
	//			EOUtilisateur util=(EOUtilisateur)array.objectAtIndex(i);
	//			if (isDroitModification(util, commande))
	//				resultats.addObject(util);
	//		}
	//
	//		return Finder.tableauTrie(resultats, sort());
	//	}
	//	
	//	public static final NSArray getUtilisateurs(EOEditingContext ed, EOUtilisateur utilisateur, EOCommande commande,
	//			NSArray utilisateursContactes) {
	//		NSArray array=getUtilisateurs(ed, utilisateur, utilisateursContactes);
	//		NSMutableArray resultats=new NSMutableArray();
	//
	//		for (int i=0; i<array.count(); i++) {
	//			EOUtilisateur util=(EOUtilisateur)array.objectAtIndex(i);
	//			if (isDroitModification(util, commande))
	//				resultats.addObject(util);
	//		}
	//
	//		return Finder.tableauTrie(resultats, sort());
	//	}

	public static EOUtilisateur getUtilisateur(EOEditingContext edc, Integer utlOrdre) {
		EOUtilisateur utilisateur = null;
		NSArray utilisateurs = EOUtilities.objectsMatchingKeyAndValue(edc, EOUtilisateur.ENTITY_NAME, "utlOrdre", utlOrdre);
		if (utilisateurs != null && utilisateurs.count() == 1) {
			utilisateur = (EOUtilisateur) utilisateurs.lastObject();
			if (utilisateur.isValide() != true) {
				utilisateur = null;
			}
		}

		return utilisateur;
	}

	//
	//	public static final boolean isDroitModification(EOUtilisateur utilisateur, EOCommande commande) {
	//		// TODO : modifier cette methode pour ajouter les restrictions pour modification de la commande
	//
	//		return true;
	//	}
	//	
	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
		array.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
		return array;
	}

}
