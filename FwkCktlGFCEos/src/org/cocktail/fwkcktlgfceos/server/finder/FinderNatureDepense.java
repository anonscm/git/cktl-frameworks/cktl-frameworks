package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.metier.EONatureDep;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderNatureDepense extends Finder {

	/**
	 * @param edc - editing context
	 * @param exeOrdre - exercice (int)
	 * @return liste des natures sur l'exercice
	 */
	@SuppressWarnings("unchecked")
    public static NSArray<EONatureDep> findByExercice(EOEditingContext edc, Number exeOrdre) {
        EOQualifier qualExercice = EONatureDep.ADM_NATURE_DEP_EXERCICES.dot(EONatureDepExercice.EXE_ORDRE).eq(exeOrdre.intValue());
        return (NSArray<EONatureDep>) fetchArray(EONatureDep.ENTITY_NAME, qualExercice, EONatureDep.LIBELLE.descs(), edc, false);
    }
	
}
