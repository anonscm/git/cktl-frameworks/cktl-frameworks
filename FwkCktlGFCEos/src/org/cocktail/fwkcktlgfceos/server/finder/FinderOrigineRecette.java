/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.finder;



import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette;
import org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * @author bourges (enfin le premier à mettre de la javadoc !)
 */
public final class FinderOrigineRecette extends Finder {
	public static final String LOLF_NIVEAU_RECETTE_PARAM_KEY = "LOLF_NIVEAU_RECETTE";	
	
    /**
     * @param edc - editing context
     * @param exeOrdre - exercice (int)
     * @return la liste des origines de recette
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EOOrigineRecette> findByExercice(EOEditingContext edc, int exeOrdre) {
        EOQualifier qualValideEtSurExercice = EOOrigineRecette.TO_ORIGINE_RECETTE_EXERCICES.dot(EOOrigineRecetteExercice.EXE_ORDRE).eq(exeOrdre)
        		.and(EOOrigineRecette.QUAL_VALIDE);
        return (NSArray<EOOrigineRecette>) Finder.fetchArray(EOOrigineRecette.ENTITY_NAME, qualValideEtSurExercice, EOOrigineRecette.LIBELLE.descs(), edc, false);
    }

    /**
     * @param edc - editing context
     * @param exeOrdre - exercice (long)
     * @return la liste des origines de recette
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EOOrigineRecette> findByExercice(EOEditingContext edc, Long exeOrdre) {
        return findByExercice(edc, exeOrdre.intValue());
    }

    /**
	 * @param ed - editing context
	 * @param exercice - exercice
	 * @return Le niveau des actions a utiliser pour une recette (defini dans la table de parametrage).
	 */
	public static Integer getCurrentNiveau(EOEditingContext ed, EOExercice exercice) {
		EOParametre param = FinderParametre.getParametre(ed, LOLF_NIVEAU_RECETTE_PARAM_KEY, exercice);
		return Integer.valueOf(param.parValue());
	}
}
