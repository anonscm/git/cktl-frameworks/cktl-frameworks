/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfceos.server.finder;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire;
import org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FinderOrganSignataire extends Finder{


    /**
     * Recupere une liste de organSignataires associes e une branche de l'organigrame budgetaire. 
     * Si aucun signataire possible (en fonction des limitations par type de credit) n'est trouve au niveau de la branche, on remonte e la branche pere, 
     * jusqu'eventuellement en haut de l'organigramme.
     * 
     * @param organ Branche de l'organigramme sur laquelle est faite la depense
     * @param timestamp Date (logiquement la date du jour)
     * @param montant Montant de la depense
     * @param typeCredit typeCredit Type de credit associe e la depense 
     * @return un NSArray des ordonnateurs (responsables) associes e une branche de l'organigramme budgetaire.
     */
    public static NSArray getOrganSignataires(final EOEb organ, final NSTimestamp timestamp, final BigDecimal montant, final EOTypeCredit typeCredit) {
        final NSMutableArray signataires = new NSMutableArray();  
        EOEb org = organ;
        
        while (signataires.count()==0 && org != null ) {
//          Filtre sur la date        
            final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("("+EOEbSignataire.ORSI_DATE_OUVERTURE_KEY +"=nil or " + 
                    EOEbSignataire.ORSI_DATE_OUVERTURE_KEY +"<=%@) and (" +EOEbSignataire.ORSI_DATE_CLOTURE_KEY +"=nil or " + 
                    EOEbSignataire.ORSI_DATE_CLOTURE_KEY +">=%@)", new NSArray(new Object[]{timestamp, timestamp}));

            //Recuperer tous les organSignataires associes e la ligne budgetaire.
            final NSArray organSignataires = EOQualifier.filteredArrayWithQualifier(org.organSignataires(), qual);
            
            signataires.addObjectsFromArray(organSignataires);
            final EOQualifier qualTcBad = EOQualifier.qualifierWithQualifierFormat(EOEbSignataireTc.TYPE_CREDIT_KEY +"=%@ and "+ EOEbSignataireTc.OST_MAX_MONTANT_TTC_KEY + "<%@", new NSArray(new Object[]{typeCredit, montant}));
                    
//          Verifier s'il y a des limites depassees sur les types de credits, dans ce cas on ecarte les signataires
            for (int i = 0; i < organSignataires.count(); i++) {
                final EOEbSignataire element = (EOEbSignataire) organSignataires.objectAtIndex(i);
                if (EOQualifier.filteredArrayWithQualifier(element.organSignataireTcs(), qualTcBad).count() > 0 ) {
                    signataires.removeObject(element);
                }
            }
            org = org.organPere(); 
        }
        return signataires.immutableClone();
    }
    
}
