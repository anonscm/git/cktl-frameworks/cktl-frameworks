/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.exception.NullBindingException;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public final class FinderTypeCredit extends Finder {

	/**
	 * Recherche des types de credit par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : exercice, tcdCode <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOTypeCredit
	 */
	public static final NSArray getTypeCredits(EOEditingContext ed, NSDictionary bindings) {
		if (bindings == null)
			throw new NullBindingException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new NullBindingException("le bindings 'exercice' est obligatoire");

		NSArray res;
		try {
			res = Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOTypeCredit.ENTITY_NAME, "Recherche", bindings), sort());
		} catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return res;

	}

	public static final EOTypeCredit getTypeCredit(EOEditingContext ed, NSDictionary bindings) {
		NSArray array = getTypeCredits(ed, bindings);
		if (array == null || array.count() != 1)
			return null;
		return (EOTypeCredit) array.objectAtIndex(0);
	}

	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}

}
