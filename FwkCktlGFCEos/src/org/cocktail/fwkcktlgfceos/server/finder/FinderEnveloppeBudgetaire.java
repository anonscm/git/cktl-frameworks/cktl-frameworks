package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

public class FinderEnveloppeBudgetaire extends Finder {

	/**
     * @param edc - editing context
     * @return l'enveloppe budgetaire standard si existe ; null sinon.
     */
    public static EOEnveloppeBudgetaire findEnveloppeStandard(EOEditingContext edc) {
		EOQualifier qualEnveloppeStandard = EOEnveloppeBudgetaire.CODE.eq(EOEnveloppeBudgetaire.CODE_ENVELOPPE_STANDARD);
		return (EOEnveloppeBudgetaire) fetchObject(EOEnveloppeBudgetaire.ENTITY_NAME, qualEnveloppeStandard, edc, true);
	}
}
