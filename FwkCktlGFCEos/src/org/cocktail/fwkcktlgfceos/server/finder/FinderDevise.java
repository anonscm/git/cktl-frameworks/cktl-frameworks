/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.finder;

import org.cocktail.fwkcktlgfceos.server.metier.EODevise;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public final class FinderDevise extends Finder {

    public static final EODevise getDeviseEnCours(EOEditingContext ed, EOExercice exercice) {
    	String devCode=FinderParametre.getDeviseCode(ed, exercice);
    	EODevise devise=FinderDevise.getDevisePourCode(ed, devCode);
    	
    	if (devise==null) {
           	NSArray arrayDevises=fetchDevises(ed);

    		if (arrayDevises.count()>0)
    			devise=(EODevise)arrayDevises.objectAtIndex(0);
    	}
    	
    	return devise;
    }

	/**
	 * Recherche d'une devise pour un libelle.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param libelle
	 *        libelle pour lequel on veut la devise
	 * @return
	 *        une EODevise
	 */
    public static final EODevise getDevise(EOEditingContext ed, String libelle) {
    	return FinderDevise.getDevisePourLibelle(ed, libelle);
    }

    public static final NSArray getDevises(EOEditingContext ed) {
    	return FinderDevise.getLesDevises(ed);
    }
    
    private static final NSArray getLesDevises(EOEditingContext ed) {
    	return fetchDevises(ed);
    }

	/**
	 * Recherche d'une devise correspondant a un libelle.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param date
	 *        libelle pour lequel on veut la devise
	 * @return
	 *        un EODevise
	 */
    private static final EODevise getDevisePourLibelle(EOEditingContext ed, String libelle) {
       	NSArray larray;
    	    	
       	NSArray arrayDevises=fetchDevises(ed);
    	
    	larray = EOQualifier.filteredArrayWithQualifier(arrayDevises, 
    			EOQualifier.qualifierWithQualifierFormat(EODevise.DEV_LIBELLE_KEY+"=%@", new NSArray(libelle)));
   	
    	if (larray.count()!=1)
    		return null;
    	
    	return (EODevise)larray.objectAtIndex(0);
    }

    private static final EODevise getDevisePourCode(EOEditingContext ed, String code) {
       	NSArray larray;
    	
       	NSArray arrayDevises=fetchDevises(ed);
    	
    	larray = EOQualifier.filteredArrayWithQualifier(arrayDevises, 
    			EOQualifier.qualifierWithQualifierFormat(EODevise.DEV_CODE_KEY+"=%@", new NSArray(code)));
   	
    	if (larray==null || larray.count()==0)
    		return null;
    	
    	return (EODevise)larray.objectAtIndex(0);
    }

    /**
     * Fetch toutes les devises pour les garder en memoire et eviter de refetcher ensuite
     * <BR>
     * @param ed
     *        editingContext dans lequel se fait le fetch
     */
    private static NSArray fetchDevises(EOEditingContext ed) {
    	return Finder.fetchArray(ed,EODevise.ENTITY_NAME,null,null,null,false);
    }
}
