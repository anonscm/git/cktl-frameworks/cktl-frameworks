/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktlgfceos.server.exception.IllegalArgumentException;
import org.cocktail.fwkcktlgfceos.server.exception.NullBindingException;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public final class FinderOrgan extends Finder {

	public static final String ORGAN_DEPENSE = "DEPENSE";
	public static final String ORGAN_RECETTE = "RECETTE";
	public static final String ORGAN_BUDGET = "BUDGET";

	// tyet_id pris en compte :
	// 16 : TOUTES
	// 19 : AUCUNE
	// 20 : RECETTES
	// 21 : DEPENSES

	public static final String DECODE_DEPENSE = "decode(o.org_op_autorisees, 19,0,20,0,1)";
	public static final String DECODE_BUDGET = "1";
	public static final String DECODE_RECETTE = "decode(o.org_op_autorisees, 19,0,21,0,1)";

	/**
	 * @param ec
	 * @param structureUlr La structure de rattachement
	 * @return Un tableau contenant toutes les organ rattaches a la structure (ainsi que les organ enfants sans structure specifiee).
	 */
	public static NSArray fetchOrgansForStructure(EOEditingContext ec,
			String cStructure, EOExercice exercice) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				EOEb.C_STRUCTURE_KEY + "=%@ and " + EOEb.ORGAN_EXERCICE_KEY + "." + EOEbExercice.EXERCICE_KEY + "=%@",
				new NSArray(new Object[] {
						cStructure, exercice
				}));
		// recuperer tous les organ directement rattaches a la structure
		EOFetchSpecification fs = new EOFetchSpecification(EOEb.ENTITY_NAME, qual, null);
		NSArray res1 = ec.objectsWithFetchSpecification(fs);
		NSMutableArray res = new NSMutableArray(res1);
		// recuperer tous les enfants
		for (int i = 0; i < res1.count(); i++) {
			res.addObjectsFromArray(getAllOrganFilsSansStructure((EOEb) res1.objectAtIndex(i)));
		}
		return res;
	}

	/**
	 * @param ec
	 * @param structureUlr La structure de rattachement
	 * @param exercice l'exercice des organs à récupérer
	 * @param utlOrdre l'utilisateur ayant les droits sur les organ à récupérer
	 * @param niveauMin le niveau d'organ minimum, exemple : niveauMin à 4 va récupérer tous les sous CR, niveauMin à 3 tous les CR et sous CR...
	 * @param modeRa si true, renvoie uniquement les organs de type ressources affectées
	 * @return Un tableau contenant toutes les organ rattaches a la structure (ainsi que les organ enfants).
	 */
	public static NSArray fetchOrgansForStructureAndUtl(EOEditingContext ec,
			String cStructure, EOExercice exercice, Integer utlOrdre, Integer niveauMin, Boolean typeRA) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				EOEb.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateur.UTL_ORDRE_KEY + "=%@ and " +
						EOEb.C_STRUCTURE_KEY + "=%@ and " + EOEb.ORGAN_EXERCICE_KEY + "." + EOEbExercice.EXERCICE_KEY + "=%@",
				new NSArray(new Object[] {
						utlOrdre, cStructure, exercice
				}));
		// recuperer tous les organ directement rattaches a la structure
		NSTimestamp now = new NSTimestamp();

		EOFetchSpecification fs = new EOFetchSpecification(EOEb.ENTITY_NAME, qual, null);
		NSArray<EOEb> organs = ec.objectsWithFetchSpecification(fs);
		NSMutableArray<EOEb> res = new NSMutableArray<EOEb>();
		// recuperer tous les enfants
		for (EOEb organ : organs) {
			res.addObject(organ);
			res.addObjectsFromArray(getAllOrganFils(organ, exercice));
		}
		// On filtre selon le niveau min donné
		if (niveauMin != null) {
			ERXQ.filter(res, ERXQ.greaterThanOrEqualTo(EOEb.ORG_NIVEAU_KEY, niveauMin));
		}
		// On filtre selon le type RA
		if (typeRA != null) {
			ERXQ.filter(res, ERXQ.equals(EOEb.IS_TYPE_RA_KEY, typeRA));
		}
		// On ordonne
		ERXS.sort(res, EOEb.SORT_DEFAULT);
		return ERXArrayUtilities.arrayWithoutDuplicates(res);
	}

	/**
	 * @param organ
	 * @return Un tableau de tous les organ fils (recursif) sans structure specifiee.
	 */
	public static NSArray getAllOrganFilsSansStructure(EOEb organ) {
		NSMutableArray res = new NSMutableArray();
		Enumeration enumeration = organ.organFils().objectEnumerator();
		while (enumeration.hasMoreElements()) {
			final EOEb object = (EOEb) enumeration.nextElement();
			if (object.cStructure() == null || NSKeyValueCoding.NullValue.equals(object.cStructure())) {
				res.addObject(object);
				res.addObjectsFromArray(getAllOrganFilsSansStructure(object));
			}
		}
		return res.immutableClone();
	}

	/**
	 * @param organ
	 * @return Un tableau de tous les organ fils (recursif).
	 */
	public static NSArray getAllOrganFils(EOEb organ) {
		return getAllOrganFils(organ, null);
	}

	/**
	 * @param organ
	 * @param exercice L'exercice en cours
	 * @return Un tableau de tous les organ fils (recursif).
	 */
	public static NSArray getAllOrganFils(EOEb organ, EOExercice exercice) {
		NSMutableArray res = new NSMutableArray();
		Enumeration enumeration;
		if (exercice != null) {
			EOQualifier qual = ERXQ.equals(EOEb.ORGAN_EXERCICE_KEY + "." + EOEbExercice.EXERCICE_KEY, exercice);
			enumeration = organ.organFils(qual, true).objectEnumerator();
		} else {
			enumeration = organ.organFils().objectEnumerator();
		}
		while (enumeration.hasMoreElements()) {
			final EOEb object = (EOEb) enumeration.nextElement();
			res.addObject(object);
			res.addObjectsFromArray(getAllOrganFils(object, exercice));
		}
		return res.immutableClone();
	}

	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOEb.ORG_UB_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOEb.ORG_CR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOEb.ORG_SOUSCR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}

	public static final NSArray getRawRowOrgan(EOEditingContext ed, Integer exeOrdre, Integer utlOrdre, String typeUtilisation, int niveauMax) {
		if (exeOrdre == null) {
			throw new NullBindingException("le parametre 'exeOrdre' est obligatoire");
		}
		if (utlOrdre == null) {
			throw new NullBindingException("le parametre 'utlOrdre' est obligatoire");
		}

		try {
			return EOUtilities.rawRowsForSQL(ed, MODEL_NAME, constructionChaine(ed, exeOrdre, utlOrdre, typeUtilisation, niveauMax), null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	protected static String constructionChaine(EOEditingContext ed, Integer exeOrdre, Integer utlOrdre, String typeUtilisation, int niveauMax) {
		String select = "";

		if (typeUtilisation == null || (!typeUtilisation.equals(FinderOrgan.ORGAN_BUDGET) && !typeUtilisation.equals(FinderOrgan.ORGAN_RECETTE) &&
				!typeUtilisation.equals(FinderOrgan.ORGAN_DEPENSE))) {
			typeUtilisation = FinderOrgan.ORGAN_DEPENSE;
		}

		String decodeTypeUtilisation = "";
		if (typeUtilisation.equals(FinderOrgan.ORGAN_DEPENSE)) {
			decodeTypeUtilisation = FinderOrgan.DECODE_DEPENSE;
		}
		if (typeUtilisation.equals(FinderOrgan.ORGAN_BUDGET)) {
			decodeTypeUtilisation = FinderOrgan.DECODE_BUDGET;
		}
		if (typeUtilisation.equals(FinderOrgan.ORGAN_RECETTE)) {
			decodeTypeUtilisation = FinderOrgan.DECODE_RECETTE;
		}

		if (niveauMax < 0) {
			throw new IllegalArgumentException("le niveau maximum doit etre >=0");
		}

		// tous les sous cr
		if (niveauMax >= 4) {
			select = select + "select o.org_id, o.org_niv, o.org_pere, o.org_univ, o.org_etab, o.org_ub, o.org_cr, o.org_lib, o.org_lucrativite, ";
			select = select + " o.c_structure, o.log_ordre, o.tyor_id, o.org_souscr, " + decodeTypeUtilisation + " droit ";
			select = select + "from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + " and uo.exe_ordre=" + exeOrdre;

			select = select + " union all ";
		}

		// tous les cr
		if (niveauMax >= 3) {
			select = select + "select id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select id_adm_eb, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in ";
			select = select + "(select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "  and uo.exe_ordre=exeOrdre)) ";
			select = select + "     union all ";
			select = select + "   select o.id_adm_eb, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + "  o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR, " + decodeTypeUtilisation + " from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "  and uo.exe_ordre=exeOrdre) ";
			select = select + " )  ";
			select = select + "  ) ";
			select = select + " group by id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// toutes les ub
		if (niveauMax >= 2) {
			select = select + "select id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select id_adm_eb, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from " + EOEb.ENTITY_TABLE_NAME + " o where id_adm_eb in ";
			select = select + "   (select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre))";
			select = select + "    union all";
			select = select + "    select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre))";
			select = select + "    union all";
			select = select + "   select o.id_adm_eb, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + "  o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "  and uo.exe_ordre=exeOrdre) ";
			select = select + " )  ";
			select = select + "  ) ";
			select = select + " group by id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// tous les etab
		if (niveauMax >= 1) {
			select = select + "select id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select id_adm_eb, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from " + EOEb.ENTITY_TABLE_NAME + " o where id_adm_eb in ";
			select = select + "   (select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME
					+ " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre)))";
			select = select + "    union all";
			select = select + "    select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre))";
			select = select + "    union all";
			select = select + "    select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre))";
			select = select + "    union all";
			select = select + "   select o.id_adm_eb, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + "  o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=1 and utl_ordre=" + utlOrdre;
			select = select + "  and uo.exe_ordre=exeOrdre) ";
			select = select + " )  ";
			select = select + "  ) ";
			select = select + " group by id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// toutes les racines
		if (niveauMax >= 0) {
			select = select + "select id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select id_adm_eb, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from " + EOEb.ENTITY_TABLE_NAME + " o where id_adm_eb in ";
			select = select
					+ "   (select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select o.org_pere from " + EOEb.ENTITY_TABLE_NAME
					+ " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=4 and utl_ordre="
					+ utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre))))";
			select = select + "    union all";
			select = select + "    select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME
					+ " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre)))";
			select = select + "    union all";
			select = select + "    select org_pere from " + EOEb.ENTITY_TABLE_NAME + " where id_adm_eb in (select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre))";
			select = select + "    union all";
			select = select + "    select o.org_pere from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=1 and utl_ordre=" + utlOrdre;
			select = select + "   and uo.exe_ordre=exeOrdre))";
			select = select + "    union all";
			select = select + "   select o.id_adm_eb, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + "  o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from " + EOEb.ENTITY_TABLE_NAME + " o, " + EOUtilisateurEb.ENTITY_TABLE_NAME + " uo where o.id_adm_eb=uo.id_adm_eb and org_niv=0 and utl_ordre=" + utlOrdre;
			select = select + "  and uo.exe_ordre=exeOrdre) ";
			select = select + " )  ";
			select = select + "  ) ";
			select = select + " group by id_adm_eb, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " c_structure, log_ordre, tyor_id, org_souscr  ";
		}

		return select;
	}

	/**
	 * Recupere la structure associee a une branche de l'organigrame budgetaire. Si aucune structure n'est definie sur la branche on recupere la
	 * structure definie sur la branche parente. niveauMin permet de specifier jusqu'ou on remonte pour chercher la structure. cf
	 * {@link EOEb#ORG_NIV_0} et les autres constantes de niveau definies dans EOOrgan.
	 * 
	 * @param organ Branche de l'organigramme sur laquelle est faite la depense
	 * @param niveauMin si null, le niveau 0 est utilise.
	 * @return un objet Structure ou null si aucune structure n'a été trouvée.
	 */
	public static EOStructure getStructureForOrgan(final EOEb organ, Integer niveauMin) {
		EOStructure res = null;
		EOEb org = organ;
		if (niveauMin == null) {
			niveauMin = EOEb.ORG_NIV_0;
		}
		res = org.structure();
		while (res == null && org != null && org.orgNiveau().intValue() >= niveauMin.intValue()) {
			org = org.organPere();
			res = org.structure();
		}
		return res;
	}

	/**
	 * Renvoie toutes les structures reliées à au moins un organ, sans doublons.
	 * 
	 * @param editingContext
	 * @return ensemble des structures étant reliées à au moins un organ
	 */
	public static NSArray getAllStructureAvecOrgan(EOEditingContext editingContext) {
		NSMutableSet res = new NSMutableSet();
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOEb.STRUCTURE_KEY, EOKeyValueQualifier.QualifierOperatorNotEqual, null);
		NSArray allOrgansWithStructure = EOEb.fetchAll(editingContext, qualifier);
		if (allOrgansWithStructure != null && !allOrgansWithStructure.isEmpty()) {
			res = new NSMutableSet((NSArray) allOrgansWithStructure.valueForKey(EOEb.STRUCTURE_KEY));
		}
		return res.allObjects();
	}

}
