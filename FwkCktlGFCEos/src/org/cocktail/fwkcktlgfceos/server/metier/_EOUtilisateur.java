/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateur.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOUtilisateur extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_Utilisateur";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_UTILISATEUR";

//Attribute Keys
public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
public static final ERXKey<NSTimestamp> UTL_FERMETURE = new ERXKey<NSTimestamp>("utlFermeture");
public static final ERXKey<NSTimestamp> UTL_OUVERTURE = new ERXKey<NSTimestamp>("utlOuverture");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne>("personne");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtat");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction> UTILISATEUR_FONCTIONS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction>("utilisateurFonctions");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> UTILISATEUR_INFO = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo>("utilisateurInfo");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> UTILISATEUR_ORGANS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb>("utilisateurOrgans");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference> UTILISATEUR_PREFERENCES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference>("utilisateurPreferences");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

// Attributs non visibles
	public static final String PERS_ID_KEY = "persId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UTL_ORDRE2_KEY = "utlOrdre2";

//Colonnes dans la base de donnees
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
	public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";

	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String UTL_ORDRE2_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String PERSONNE_KEY = "personne";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_FONCTIONS_KEY = "utilisateurFonctions";
	public static final String UTILISATEUR_INFO_KEY = "utilisateurInfo";
	public static final String UTILISATEUR_ORGANS_KEY = "utilisateurOrgans";
	public static final String UTILISATEUR_PREFERENCES_KEY = "utilisateurPreferences";



	// Accessors methods
  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public NSTimestamp utlFermeture() {
    return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
  }

  public void setUtlFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_FERMETURE_KEY);
  }

  public NSTimestamp utlOuverture() {
    return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
  }

  public void setUtlOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne personne() {
    return (org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo utilisateurInfo() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo)storedValueForKey(UTILISATEUR_INFO_KEY);
  }

  public void setUtilisateurInfoRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo oldValue = utilisateurInfo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_INFO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_INFO_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction> utilisateurFonctions() {
    return (NSArray)storedValueForKey(UTILISATEUR_FONCTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction> utilisateurFonctions(EOQualifier qualifier) {
    return utilisateurFonctions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction> utilisateurFonctions(EOQualifier qualifier, boolean fetch) {
    return utilisateurFonctions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction> utilisateurFonctions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurFonctions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurFonctionsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
  }

  public void removeFromUtilisateurFonctionsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction createUtilisateurFonctionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_UtilisateurFonction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_FONCTIONS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction) eo;
  }

  public void deleteUtilisateurFonctionsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurFonctionsRelationships() {
    Enumeration objects = utilisateurFonctions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurFonctionsRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans() {
    return (NSArray)storedValueForKey(UTILISATEUR_ORGANS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans(EOQualifier qualifier) {
    return utilisateurOrgans(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans(EOQualifier qualifier, boolean fetch) {
    return utilisateurOrgans(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public void removeFromUtilisateurOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb createUtilisateurOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_UtilisateurEb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_ORGANS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb) eo;
  }

  public void deleteUtilisateurOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurOrgansRelationships() {
    Enumeration objects = utilisateurOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurOrgansRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference> utilisateurPreferences() {
    return (NSArray)storedValueForKey(UTILISATEUR_PREFERENCES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference> utilisateurPreferences(EOQualifier qualifier) {
    return utilisateurPreferences(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference> utilisateurPreferences(EOQualifier qualifier, boolean fetch) {
    return utilisateurPreferences(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference> utilisateurPreferences(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurPreferences();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurPreferencesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_PREFERENCES_KEY);
  }

  public void removeFromUtilisateurPreferencesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_PREFERENCES_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference createUtilisateurPreferencesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_UtilisateurPreference");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_PREFERENCES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference) eo;
  }

  public void deleteUtilisateurPreferencesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_PREFERENCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurPreferencesRelationships() {
    Enumeration objects = utilisateurPreferences().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurPreferencesRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurPreference)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOUtilisateur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOUtilisateur createEOUtilisateur(EOEditingContext editingContext, NSTimestamp utlOuverture
, org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne personne, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat			) {
    EOUtilisateur eo = (EOUtilisateur) createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);    
		eo.setUtlOuverture(utlOuverture);
    eo.setPersonneRelationship(personne);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EOUtilisateur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateur)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateur creerInstance(EOEditingContext editingContext) {
	  		EOUtilisateur object = (EOUtilisateur)createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOUtilisateur localInstanceIn(EOEditingContext editingContext, EOUtilisateur eo) {
    EOUtilisateur localInstance = (eo == null) ? null : (EOUtilisateur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOUtilisateur#localInstanceIn a la place.
   */
	public static EOUtilisateur localInstanceOf(EOEditingContext editingContext, EOUtilisateur eo) {
		return EOUtilisateur.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
