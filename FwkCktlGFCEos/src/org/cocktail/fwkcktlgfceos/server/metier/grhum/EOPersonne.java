/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier.grhum;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPersonne extends _EOPersonne {
	public static final String PERS_TYPE_STR = "STR";
	public static final String NOM_AND_PRENOM_KEY = "NomAndPrenom";

	public EOPersonne() {
		super();
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream out)
	 * throws java.io.IOException { }
	 * 
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	@Override
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	@Override
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public boolean isStructure() {
		return PERS_TYPE_STR.equals(persType());
	}

	public boolean isIndividu() {
		return !isStructure();
	}

	/**
	 * Surcharge de la methode pour qu'elle ne renvoie que les comptes valides.
	 * 
	 * @see org.cocktail.fwkcktldroitsutils.server.metier._EOPersonne#toComptes()
	 */
	@Override
	public NSArray toComptes() {
		NSArray res = super.toComptes();
		if (res != null) {
			return EOQualifier.filteredArrayWithQualifier(res, EOCompte.QUAL_CPT_VALIDE_OUI);
		}
		return null;

	}

	public String getNomAndPrenom() {
		return persLc() + " " + persLibelle();
	}
}
