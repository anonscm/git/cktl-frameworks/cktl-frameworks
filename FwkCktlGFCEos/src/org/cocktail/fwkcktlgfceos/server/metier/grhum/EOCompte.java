/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier.grhum;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOCompte extends _EOCompte {

	public static final String CPT_VALIDE_OUI = "O";
	public static final String CPT_VALIDE_NON = "N";
	public static final String CPT_CHARTE_OUI = "O";
	public static final String CPT_CHARTE_NON = "N";
	public static final String CPT_LISTE_ROUGE_OUI = "N";
	public static final String CPT_LISTE_ROUGE_NON = "N";
	public static final String CPT_CRYPTE_OUI = "O";
	public static final String CPT_PRINCIPAL_OUI = "O";
	public static final String CPT_PRINCIPAL_NON = "N";

	public static final int LOGIN_LENGTH_MIN = 1;
	public static final int LOGIN_LENGTH_MAX = 8;
	public static final int PASSWORD_LENGTH_MIN = 8;
	public static final int PASSWORD_LENGTH_MAX = 8;

	public static final EOQualifier QUAL_CPT_VALIDE_OUI = new EOKeyValueQualifier(EOCompte.CPT_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EOCompte.CPT_VALIDE_OUI);
	public static final EOQualifier QUAL_CPT_VLAN_X = EOQualifier.qualifierWithQualifierFormat(EOCompte.TO_VLAN_KEY + "." + EOVlans.C_VLAN_KEY + "=%@", new NSArray(new Object[] {
			EOVlans.VLAN_X
	}));

	public static final EOSortOrdering SORT_VLANS_PRIORITE = EOSortOrdering.sortOrderingWithKey(TO_VLAN_KEY + "." + EOVlans.PRIORITE_KEY, EOSortOrdering.CompareAscending);

	public EOCompte() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

}
