/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFonction.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOFonction extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_Fonction";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_FONCTION";

//Attribute Keys
public static final ERXKey<String> FON_CATEGORIE = new ERXKey<String>("fonCategorie");
public static final ERXKey<String> FON_DESCRIPTION = new ERXKey<String>("fonDescription");
public static final ERXKey<String> FON_ID_INTERNE = new ERXKey<String>("fonIdInterne");
public static final ERXKey<String> FON_LIBELLE = new ERXKey<String>("fonLibelle");
public static final ERXKey<String> FON_SPEC_EXERCICE = new ERXKey<String>("fonSpecExercice");
public static final ERXKey<String> FON_SPEC_GESTION = new ERXKey<String>("fonSpecGestion");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication> TYPE_APPLICATION = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication>("typeApplication");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fonOrdre";

	public static final String FON_CATEGORIE_KEY = "fonCategorie";
	public static final String FON_DESCRIPTION_KEY = "fonDescription";
	public static final String FON_ID_INTERNE_KEY = "fonIdInterne";
	public static final String FON_LIBELLE_KEY = "fonLibelle";
	public static final String FON_SPEC_EXERCICE_KEY = "fonSpecExercice";
	public static final String FON_SPEC_GESTION_KEY = "fonSpecGestion";

// Attributs non visibles
	public static final String FON_ORDRE_KEY = "fonOrdre";
	public static final String TYAP_ID_KEY = "tyapId";

//Colonnes dans la base de donnees
	public static final String FON_CATEGORIE_COLKEY = "FON_CATEGORIE";
	public static final String FON_DESCRIPTION_COLKEY = "FON_DESCRIPTION";
	public static final String FON_ID_INTERNE_COLKEY = "FON_ID_INTERNE";
	public static final String FON_LIBELLE_COLKEY = "FON_LIBELLE";
	public static final String FON_SPEC_EXERCICE_COLKEY = "FON_SPEC_EXERCICE";
	public static final String FON_SPEC_GESTION_COLKEY = "FON_SPEC_GESTION";

	public static final String FON_ORDRE_COLKEY = "FON_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";


	// Relationships
	public static final String TYPE_APPLICATION_KEY = "typeApplication";



	// Accessors methods
  public String fonCategorie() {
    return (String) storedValueForKey(FON_CATEGORIE_KEY);
  }

  public void setFonCategorie(String value) {
    takeStoredValueForKey(value, FON_CATEGORIE_KEY);
  }

  public String fonDescription() {
    return (String) storedValueForKey(FON_DESCRIPTION_KEY);
  }

  public void setFonDescription(String value) {
    takeStoredValueForKey(value, FON_DESCRIPTION_KEY);
  }

  public String fonIdInterne() {
    return (String) storedValueForKey(FON_ID_INTERNE_KEY);
  }

  public void setFonIdInterne(String value) {
    takeStoredValueForKey(value, FON_ID_INTERNE_KEY);
  }

  public String fonLibelle() {
    return (String) storedValueForKey(FON_LIBELLE_KEY);
  }

  public void setFonLibelle(String value) {
    takeStoredValueForKey(value, FON_LIBELLE_KEY);
  }

  public String fonSpecExercice() {
    return (String) storedValueForKey(FON_SPEC_EXERCICE_KEY);
  }

  public void setFonSpecExercice(String value) {
    takeStoredValueForKey(value, FON_SPEC_EXERCICE_KEY);
  }

  public String fonSpecGestion() {
    return (String) storedValueForKey(FON_SPEC_GESTION_KEY);
  }

  public void setFonSpecGestion(String value) {
    takeStoredValueForKey(value, FON_SPEC_GESTION_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOFonction avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFonction createEOFonction(EOEditingContext editingContext, String fonCategorie
, String fonIdInterne
, String fonLibelle
, String fonSpecExercice
, String fonSpecGestion
, org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication typeApplication			) {
    EOFonction eo = (EOFonction) createAndInsertInstance(editingContext, _EOFonction.ENTITY_NAME);    
		eo.setFonCategorie(fonCategorie);
		eo.setFonIdInterne(fonIdInterne);
		eo.setFonLibelle(fonLibelle);
		eo.setFonSpecExercice(fonSpecExercice);
		eo.setFonSpecGestion(fonSpecGestion);
    eo.setTypeApplicationRelationship(typeApplication);
    return eo;
  }

  
	  public EOFonction localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFonction)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFonction creerInstance(EOEditingContext editingContext) {
	  		EOFonction object = (EOFonction)createAndInsertInstance(editingContext, _EOFonction.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOFonction localInstanceIn(EOEditingContext editingContext, EOFonction eo) {
    EOFonction localInstance = (eo == null) ? null : (EOFonction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFonction#localInstanceIn a la place.
   */
	public static EOFonction localInstanceOf(EOEditingContext editingContext, EOFonction eo) {
		return EOFonction.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOFonction>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFonction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFonction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFonction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFonction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFonction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFonction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFonction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFonction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
