/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVapiCptbudAllocConsoDep.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOVapiCptbudAllocConsoDep extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_VapiCptbudAllocConsoDep";
	public static final String ENTITY_TABLE_NAME = "GFC.VAPI_CPTBUD_ALLOC_CONSO_DEP";

//Attribute Keys
public static final ERXKey<java.math.BigDecimal> AE_ALLOUEES = new ERXKey<java.math.BigDecimal>("aeAllouees");
public static final ERXKey<java.math.BigDecimal> AE_CONSOMMEES = new ERXKey<java.math.BigDecimal>("aeConsommees");
public static final ERXKey<java.math.BigDecimal> CP_ALLOUES = new ERXKey<java.math.BigDecimal>("cpAlloues");
public static final ERXKey<java.math.BigDecimal> CP_CONSOMMES = new ERXKey<java.math.BigDecimal>("cpConsommes");
public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> TO_DESTINATION_DEPENSE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>("toDestinationDepense");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> TO_EB = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("toEb");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice>("toExercice");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> TO_NATURE_DEP = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep>("toNatureDep");

	// Attributes


	public static final String AE_ALLOUEES_KEY = "aeAllouees";
	public static final String AE_CONSOMMEES_KEY = "aeConsommees";
	public static final String CP_ALLOUES_KEY = "cpAlloues";
	public static final String CP_CONSOMMES_KEY = "cpConsommes";
	public static final String ID_OPE_OPERATION_KEY = "idOpeOperation";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ID_ADM_DESTINATION_DEPENSE_KEY = "idAdmDestinationDepense";
	public static final String ID_ADM_EB_KEY = "idAdmEb";
	public static final String ID_ADM_NATURE_DEP_KEY = "idAdmNatureDep";

//Colonnes dans la base de donnees
	public static final String AE_ALLOUEES_COLKEY = "AE_ALLOUEES";
	public static final String AE_CONSOMMEES_COLKEY = "AE_CONSOMMEES";
	public static final String CP_ALLOUES_COLKEY = "CP_ALLOUES";
	public static final String CP_CONSOMMES_COLKEY = "CP_CONSOMMES";
	public static final String ID_OPE_OPERATION_COLKEY = "ID_OPE_OPERATION";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ID_ADM_DESTINATION_DEPENSE_COLKEY = "ID_ADM_DESTINATION_DEPENSE";
	public static final String ID_ADM_EB_COLKEY = "ID_ADM_EB";
	public static final String ID_ADM_NATURE_DEP_COLKEY = "ID_ADM_NATURE_DEP";


	// Relationships
	public static final String TO_DESTINATION_DEPENSE_KEY = "toDestinationDepense";
	public static final String TO_EB_KEY = "toEb";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_NATURE_DEP_KEY = "toNatureDep";



	// Accessors methods
  public java.math.BigDecimal aeAllouees() {
    return (java.math.BigDecimal) storedValueForKey(AE_ALLOUEES_KEY);
  }

  public void setAeAllouees(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AE_ALLOUEES_KEY);
  }

  public java.math.BigDecimal aeConsommees() {
    return (java.math.BigDecimal) storedValueForKey(AE_CONSOMMEES_KEY);
  }

  public void setAeConsommees(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AE_CONSOMMEES_KEY);
  }

  public java.math.BigDecimal cpAlloues() {
    return (java.math.BigDecimal) storedValueForKey(CP_ALLOUES_KEY);
  }

  public void setCpAlloues(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CP_ALLOUES_KEY);
  }

  public java.math.BigDecimal cpConsommes() {
    return (java.math.BigDecimal) storedValueForKey(CP_CONSOMMES_KEY);
  }

  public void setCpConsommes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CP_CONSOMMES_KEY);
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    takeStoredValueForKey(value, ID_OPE_OPERATION_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense toDestinationDepense() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense)storedValueForKey(TO_DESTINATION_DEPENSE_KEY);
  }

  public void setToDestinationDepenseRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense oldValue = toDestinationDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DESTINATION_DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DESTINATION_DEPENSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb toEb() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(TO_EB_KEY);
  }

  public void setToEbRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = toEb();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EB_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EONatureDep toNatureDep() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EONatureDep)storedValueForKey(TO_NATURE_DEP_KEY);
  }

  public void setToNatureDepRelationship(org.cocktail.fwkcktlgfceos.server.metier.EONatureDep value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EONatureDep oldValue = toNatureDep();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_NATURE_DEP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_NATURE_DEP_KEY);
    }
  }
  

/**
 * Créer une instance de EOVapiCptbudAllocConsoDep avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVapiCptbudAllocConsoDep createEOVapiCptbudAllocConsoDep(EOEditingContext editingContext, Integer idOpeOperation
, org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense toDestinationDepense, org.cocktail.fwkcktlgfceos.server.metier.EOEb toEb, org.cocktail.fwkcktlgfceos.server.metier.EOExercice toExercice, org.cocktail.fwkcktlgfceos.server.metier.EONatureDep toNatureDep			) {
    EOVapiCptbudAllocConsoDep eo = (EOVapiCptbudAllocConsoDep) createAndInsertInstance(editingContext, _EOVapiCptbudAllocConsoDep.ENTITY_NAME);    
		eo.setIdOpeOperation(idOpeOperation);
    eo.setToDestinationDepenseRelationship(toDestinationDepense);
    eo.setToEbRelationship(toEb);
    eo.setToExerciceRelationship(toExercice);
    eo.setToNatureDepRelationship(toNatureDep);
    return eo;
  }

  
	  public EOVapiCptbudAllocConsoDep localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVapiCptbudAllocConsoDep)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVapiCptbudAllocConsoDep creerInstance(EOEditingContext editingContext) {
	  		EOVapiCptbudAllocConsoDep object = (EOVapiCptbudAllocConsoDep)createAndInsertInstance(editingContext, _EOVapiCptbudAllocConsoDep.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOVapiCptbudAllocConsoDep localInstanceIn(EOEditingContext editingContext, EOVapiCptbudAllocConsoDep eo) {
    EOVapiCptbudAllocConsoDep localInstance = (eo == null) ? null : (EOVapiCptbudAllocConsoDep)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVapiCptbudAllocConsoDep#localInstanceIn a la place.
   */
	public static EOVapiCptbudAllocConsoDep localInstanceOf(EOEditingContext editingContext, EOVapiCptbudAllocConsoDep eo) {
		return EOVapiCptbudAllocConsoDep.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVapiCptbudAllocConsoDep fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVapiCptbudAllocConsoDep fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVapiCptbudAllocConsoDep eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVapiCptbudAllocConsoDep)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVapiCptbudAllocConsoDep fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVapiCptbudAllocConsoDep fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVapiCptbudAllocConsoDep eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVapiCptbudAllocConsoDep)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVapiCptbudAllocConsoDep fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVapiCptbudAllocConsoDep eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVapiCptbudAllocConsoDep ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVapiCptbudAllocConsoDep fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
