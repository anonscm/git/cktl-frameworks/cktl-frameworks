/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EODestinationDepense extends _EODestinationDepense {

	public EODestinationDepense() {
		super();
	}

	public static final String LOLF_NIVEAU_LIB_KEY = "niveauLib";
	public static final String MODE_RECETTE = "RECETTE";
	public static final String MODE_DEPENSE = "DEPENSE";

	public static final String LONG_STRING_KEY = "longString";

	public static final EOSortOrdering SORT_LOLF_CODE_ASC = new EOSortOrdering(EODestinationDepense.CODE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_LOLF_ORDRE_AFFICHAGE_ASC = new EOSortOrdering(EODestinationDepense.ORDRE_AFFICHAGE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_LOLF_ORDRE_AFFICHAGE_DESC = new EOSortOrdering(EODestinationDepense.ORDRE_AFFICHAGE_KEY, EOSortOrdering.CompareDescending);

	public static final EOQualifier QUAL_VALIDE = EOQualifier.qualifierWithQualifierFormat(EODestinationDepense.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(new Object[] {
			EOTypeEtat.ETAT_VALIDE
	}));;
	public static final EOQualifier QUAL_LOLF_NOMENCLATURE_RACINES = new EOKeyValueQualifier(EODestinationDepense.NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(-1));

    public boolean estDansUnEtatValide() {
    	return typeEtat() != null && EOTypeEtat.ETAT_VALIDE.equals(typeEtat().tyetLibelle());
    }
    
	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public boolean isDestination() {
		return (EODestinationDepenseType.TYPE_D.equals(destinationType().type()));
	}

	public boolean isSousDestination() {
		return (EODestinationDepenseType.TYPE_SD.equals(destinationType().type()));
	}

	public boolean isProgramme() {
		return (EODestinationDepenseType.TYPE_P.equals(destinationType().type()));
	}

	public boolean isAction() {
		return (EODestinationDepenseType.TYPE_A.equals(destinationType().type()));
	}

	public boolean isSousAction() {
		return (EODestinationDepenseType.TYPE_SA.equals(destinationType().type()));
	}

	public boolean isRegroupement() {
		return (EODestinationDepenseType.TYPE_RG.equals(destinationType().type()));
	}

	public String getLongString() {
		String res = abreviation();
		int i = niveau().intValue();
		if (i > 0) {
			res = destinationPere().getLongString() + " / " + res;
		}
		return res;
	}

	public String libelleLong() {
		return code() + " (" + libelle() + " - Niv. " + niveau() + ")";
	}

	/**
	 * @param exercice
	 * @return Un qualifier portant sur les dates a partir de l'exercice.
	 */
	public static EOQualifier getQualifierForExercice(EOExercice exercice) {
		EOQualifier qual = new EOKeyValueQualifier(EODestinationDepense.TO_DESTINATION_DEPENSE_EXERCICES_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return qual;
	}

	public static EOQualifier getQualifierForExercice(Integer exercice) {
		EOQualifier qual = new EOKeyValueQualifier(EODestinationDepense.TO_DESTINATION_DEPENSE_EXERCICES_KEY + "." + EODestinationDepenseExercice.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return qual;
	}

	public static NSArray getLolfNomenclaturesRacine(EOEditingContext edc) {
		EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
				EODestinationDepense.QUAL_VALIDE, QUAL_LOLF_NOMENCLATURE_RACINES
		}));
		return fetchAll(edc, qual, new NSArray(new Object[] {
				SORT_LOLF_CODE_ASC
		}));
	}

}
