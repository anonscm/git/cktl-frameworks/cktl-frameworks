/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrigineRecetteExercice.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOOrigineRecetteExercice extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_OrigineRecetteExercice";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_ORIGINE_RECETTE_EXER";

//Attribute Keys
public static final ERXKey<Integer> EXE_ORDRE = new ERXKey<Integer>("exeOrdre");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice>("exercice");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> ORIGINE_RECETTE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette>("origineRecette");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idOrigineRecetteExercice";

	public static final String EXE_ORDRE_KEY = "exeOrdre";

// Attributs non visibles
	public static final String ID_ORIGINE_RECETTE_KEY = "idOrigineRecette";
	public static final String ID_ORIGINE_RECETTE_EXERCICE_KEY = "idOrigineRecetteExercice";

//Colonnes dans la base de donnees
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

	public static final String ID_ORIGINE_RECETTE_COLKEY = "ID_ADM_ORIGINE_RECETTE";
	public static final String ID_ORIGINE_RECETTE_EXERCICE_COLKEY = "ID_ADM_ORIGINE_RECETTE_EXER";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORIGINE_RECETTE_KEY = "origineRecette";



	// Accessors methods
  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette origineRecette() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette)storedValueForKey(ORIGINE_RECETTE_KEY);
  }

  public void setOrigineRecetteRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette oldValue = origineRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_RECETTE_KEY);
    }
  }
  

/**
 * Créer une instance de EOOrigineRecetteExercice avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOrigineRecetteExercice createEOOrigineRecetteExercice(EOEditingContext editingContext, Integer exeOrdre
, org.cocktail.fwkcktlgfceos.server.metier.EOExercice exercice, org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette origineRecette			) {
    EOOrigineRecetteExercice eo = (EOOrigineRecetteExercice) createAndInsertInstance(editingContext, _EOOrigineRecetteExercice.ENTITY_NAME);    
		eo.setExeOrdre(exeOrdre);
    eo.setExerciceRelationship(exercice);
    eo.setOrigineRecetteRelationship(origineRecette);
    return eo;
  }

  
	  public EOOrigineRecetteExercice localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrigineRecetteExercice)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrigineRecetteExercice creerInstance(EOEditingContext editingContext) {
	  		EOOrigineRecetteExercice object = (EOOrigineRecetteExercice)createAndInsertInstance(editingContext, _EOOrigineRecetteExercice.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOOrigineRecetteExercice localInstanceIn(EOEditingContext editingContext, EOOrigineRecetteExercice eo) {
    EOOrigineRecetteExercice localInstance = (eo == null) ? null : (EOOrigineRecetteExercice)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOrigineRecetteExercice#localInstanceIn a la place.
   */
	public static EOOrigineRecetteExercice localInstanceOf(EOEditingContext editingContext, EOOrigineRecetteExercice eo) {
		return EOOrigineRecetteExercice.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOrigineRecetteExercice fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOrigineRecetteExercice fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrigineRecetteExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrigineRecetteExercice)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrigineRecetteExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrigineRecetteExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrigineRecetteExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrigineRecetteExercice)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOrigineRecetteExercice fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrigineRecetteExercice eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrigineRecetteExercice ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrigineRecetteExercice fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
