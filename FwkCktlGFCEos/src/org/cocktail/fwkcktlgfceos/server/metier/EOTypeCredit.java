/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

@Deprecated
public class EOTypeCredit extends _EOTypeCredit {

    public static final String TCD_TYPE_RECETTE="RECETTE";
    public static final String TCD_TYPE_DEPENSE="DEPENSE";
    public static final String TCD_BUDGET_BUDGETAIRE="BUDGETAIRE";
    public static final String TCD_BUDGET_EXECUTOIRE="EXECUTOIRE";
    public static final String TCD_BUDGET_RESERVE="RESERVE";
    
    public static final EOSortOrdering SORT_TCD_CODE_ASC = EOSortOrdering.sortOrderingWithKey(TCD_CODE_KEY, EOSortOrdering.CompareAscending);
    
    public static final EOQualifier QUAL_RECETTE = EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_TYPE_KEY+"=%@", new NSArray(new Object[]{EOTypeCredit.TCD_TYPE_RECETTE}));
    public static final EOQualifier QUAL_DEPENSE = EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_TYPE_KEY+"=%@", new NSArray(new Object[]{EOTypeCredit.TCD_TYPE_DEPENSE}));
    public static final String TCD_SECT_1 = "1";
    public static final String TCD_SECT_2 = "2";
    public static final String TCD_SECT_3 = "3";
    
    public static final String TCD_CODE_LIB_ABREGE_KEY = "tcdCodeEtLibAbrege";
    
    public EOTypeCredit() {
        super();
    }

    
    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        super.validateBeforeTransactionSave();
    }

    public static EOTypeCredit findWithId(EOEditingContext ec, Integer id) {
        EOQualifier qual = new EOKeyValueQualifier(TCD_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, id);
        return fetchFirstByQualifier(ec, qual);
    }
    
    public boolean isSection3() {
        return "3".equals(tcdSect()); 
    }
    
    public String tcdCodeEtLibAbrege() {
        return tcdCode() + "-" + tcdAbrege();
    }
    
    public String tcdCodeEtLib() {
        return tcdCode() + "-" + tcdLibelle();
    }
    
    public boolean isTypeDepense() {
        return TCD_TYPE_DEPENSE.equals(tcdType());
    }
    
    public boolean isTypeRecette() {
        return TCD_TYPE_RECETTE.equals(tcdType());
    }
}
