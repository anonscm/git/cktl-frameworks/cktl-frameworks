/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONatureDep.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EONatureDep extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_NatureDep";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_NATURE_DEP";

//Attribute Keys
public static final ERXKey<String> CODE = new ERXKey<String>("code");
public static final ERXKey<Boolean> FONGIBLE = new ERXKey<Boolean>("fongible");
public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice> ADM_NATURE_DEP_EXERCICES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice>("admNatureDepExercices");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idAdmNatureDep";

	public static final String CODE_KEY = "code";
	public static final String FONGIBLE_KEY = "fongible";
	public static final String LIBELLE_KEY = "libelle";

// Attributs non visibles
	public static final String ID_ADM_NATURE_DEP_KEY = "idAdmNatureDep";

//Colonnes dans la base de donnees
	public static final String CODE_COLKEY = "CODE";
	public static final String FONGIBLE_COLKEY = "FONGIBLE";
	public static final String LIBELLE_COLKEY = "LIBELLE";

	public static final String ID_ADM_NATURE_DEP_COLKEY = "ID_ADM_NATURE_DEP";


	// Relationships
	public static final String ADM_NATURE_DEP_EXERCICES_KEY = "admNatureDepExercices";



	// Accessors methods
  public String code() {
    return (String) storedValueForKey(CODE_KEY);
  }

  public void setCode(String value) {
    takeStoredValueForKey(value, CODE_KEY);
  }

  public Boolean fongible() {
    return (Boolean) storedValueForKey(FONGIBLE_KEY);
  }

  public void setFongible(Boolean value) {
    takeStoredValueForKey(value, FONGIBLE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice> admNatureDepExercices() {
    return (NSArray)storedValueForKey(ADM_NATURE_DEP_EXERCICES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice> admNatureDepExercices(EOQualifier qualifier) {
    return admNatureDepExercices(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice> admNatureDepExercices(EOQualifier qualifier, boolean fetch) {
    return admNatureDepExercices(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice> admNatureDepExercices(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice.ADM_NATURE_DEP_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = admNatureDepExercices();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAdmNatureDepExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ADM_NATURE_DEP_EXERCICES_KEY);
  }

  public void removeFromAdmNatureDepExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ADM_NATURE_DEP_EXERCICES_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice createAdmNatureDepExercicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_NatureDepExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ADM_NATURE_DEP_EXERCICES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice) eo;
  }

  public void deleteAdmNatureDepExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ADM_NATURE_DEP_EXERCICES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAdmNatureDepExercicesRelationships() {
    Enumeration objects = admNatureDepExercices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAdmNatureDepExercicesRelationship((org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice)objects.nextElement());
    }
  }


/**
 * Créer une instance de EONatureDep avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONatureDep createEONatureDep(EOEditingContext editingContext, String code
, Boolean fongible
, String libelle
			) {
    EONatureDep eo = (EONatureDep) createAndInsertInstance(editingContext, _EONatureDep.ENTITY_NAME);    
		eo.setCode(code);
		eo.setFongible(fongible);
		eo.setLibelle(libelle);
    return eo;
  }

  
	  public EONatureDep localInstanceIn(EOEditingContext editingContext) {
	  		return (EONatureDep)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONatureDep creerInstance(EOEditingContext editingContext) {
	  		EONatureDep object = (EONatureDep)createAndInsertInstance(editingContext, _EONatureDep.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EONatureDep localInstanceIn(EOEditingContext editingContext, EONatureDep eo) {
    EONatureDep localInstance = (eo == null) ? null : (EONatureDep)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONatureDep#localInstanceIn a la place.
   */
	public static EONatureDep localInstanceOf(EOEditingContext editingContext, EONatureDep eo) {
		return EONatureDep.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EONatureDep fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONatureDep fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONatureDep eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONatureDep)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONatureDep fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONatureDep fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONatureDep eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONatureDep)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONatureDep fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONatureDep eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONatureDep ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONatureDep fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
