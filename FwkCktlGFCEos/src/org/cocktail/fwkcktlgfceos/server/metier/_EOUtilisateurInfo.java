/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateurInfo.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOUtilisateurInfo extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_UtilisateurInfo";
	public static final String ENTITY_TABLE_NAME = "GFC.V_ADM_UTILISATEUR_INFO";

//Attribute Keys
public static final ERXKey<String> CPT_LOGIN = new ERXKey<String>("cptLogin");
public static final ERXKey<String> EMAIL = new ERXKey<String>("email");
public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
public static final ERXKey<String> NOM_PATRONYMIQUE = new ERXKey<String>("nomPatronymique");
public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
public static final ERXKey<String> PERS_LC = new ERXKey<String>("persLc");
public static final ERXKey<String> PERS_LIBELLE = new ERXKey<String>("persLibelle");
public static final ERXKey<Integer> TYET_ID = new ERXKey<Integer>("tyetId");
public static final ERXKey<NSTimestamp> UTL_FERMETURE = new ERXKey<NSTimestamp>("utlFermeture");
public static final ERXKey<NSTimestamp> UTL_OUVERTURE = new ERXKey<NSTimestamp>("utlOuverture");

// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String EMAIL_KEY = "email";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

// Attributs non visibles
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String CPT_LOGIN_COLKEY = "CPT_LOGIN";
	public static final String EMAIL_COLKEY = "EMAIL";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
	public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";

	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships



	// Accessors methods
  public String cptLogin() {
    return (String) storedValueForKey(CPT_LOGIN_KEY);
  }

  public void setCptLogin(String value) {
    takeStoredValueForKey(value, CPT_LOGIN_KEY);
  }

  public String email() {
    return (String) storedValueForKey(EMAIL_KEY);
  }

  public void setEmail(String value) {
    takeStoredValueForKey(value, EMAIL_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String nomPatronymique() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
  }

  public void setNomPatronymique(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String persLc() {
    return (String) storedValueForKey(PERS_LC_KEY);
  }

  public void setPersLc(String value) {
    takeStoredValueForKey(value, PERS_LC_KEY);
  }

  public String persLibelle() {
    return (String) storedValueForKey(PERS_LIBELLE_KEY);
  }

  public void setPersLibelle(String value) {
    takeStoredValueForKey(value, PERS_LIBELLE_KEY);
  }

  public Integer tyetId() {
    return (Integer) storedValueForKey(TYET_ID_KEY);
  }

  public void setTyetId(Integer value) {
    takeStoredValueForKey(value, TYET_ID_KEY);
  }

  public NSTimestamp utlFermeture() {
    return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
  }

  public void setUtlFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_FERMETURE_KEY);
  }

  public NSTimestamp utlOuverture() {
    return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
  }

  public void setUtlOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
  }


/**
 * Créer une instance de EOUtilisateurInfo avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOUtilisateurInfo createEOUtilisateurInfo(EOEditingContext editingContext, Integer persId
, String persLibelle
, Integer tyetId
, NSTimestamp utlOuverture
			) {
    EOUtilisateurInfo eo = (EOUtilisateurInfo) createAndInsertInstance(editingContext, _EOUtilisateurInfo.ENTITY_NAME);    
		eo.setPersId(persId);
		eo.setPersLibelle(persLibelle);
		eo.setTyetId(tyetId);
		eo.setUtlOuverture(utlOuverture);
    return eo;
  }

  
	  public EOUtilisateurInfo localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateurInfo)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateurInfo creerInstance(EOEditingContext editingContext) {
	  		EOUtilisateurInfo object = (EOUtilisateurInfo)createAndInsertInstance(editingContext, _EOUtilisateurInfo.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOUtilisateurInfo localInstanceIn(EOEditingContext editingContext, EOUtilisateurInfo eo) {
    EOUtilisateurInfo localInstance = (eo == null) ? null : (EOUtilisateurInfo)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOUtilisateurInfo#localInstanceIn a la place.
   */
	public static EOUtilisateurInfo localInstanceOf(EOEditingContext editingContext, EOUtilisateurInfo eo) {
		return EOUtilisateurInfo.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurInfo>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateurInfo fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateurInfo fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurInfo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurInfo)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurInfo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurInfo)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateurInfo fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurInfo eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurInfo ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurInfo fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
