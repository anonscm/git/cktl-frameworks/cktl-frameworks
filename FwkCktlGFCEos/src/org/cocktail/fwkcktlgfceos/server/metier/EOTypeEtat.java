/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.finder.Finder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTypeEtat extends _EOTypeEtat {
	public static final String ETAT_VALIDE = "VALIDE";
    public static final String ETAT_ANNULE = "ANNULE";
    public static final String ETAT_OUI = "OUI";
    public static final String ETAT_NON = "NON";
    public static final String ETAT_SUPPRIME = "SUPPRIME";
    public static final String ETAT_EN_ATTENTE = "EN ATTENTE";
    public static final String ETAT_A_VALIDER = "A VALIDER";
    public static final String ETAT_DECAISSABLE = "OPERATION DECAISSABLE";
    public static final String ETAT_NON_DECAISSABLE = "OPERATION NON DECAISSABLE";    
    public static final String ETAT_NE_RIEN_FAIRE = "NE RIEN FAIRE";    
    public static final String ETAT_BLOCAGE = "BLOCAGE";    
    public static final String ETAT_ALERTE = "ALERTE";    
    public static final String ETAT_RECETTES = "RECETTES";    
    public static final String ETAT_DEPENSES = "DEPENSES";    
    public static final String ETAT_TOUS = "TOUS";    
    public static final String ETAT_TOUTES = "TOUTES";    
    public static final String ETAT_AUCUN = "AUCUN";    
    public static final String ETAT_AUCUNE = "AUCUNE";
	public static final String ETAT_EN_COURS = "EN COURS";    
	
	public static final Integer ID_ETAT_VALIDE = 1;
    public static final Integer ID_ETAT_ANNULE = 2;
    public static final Integer ID_ETAT_OUI = 3;
    public static final Integer ID_ETAT_NON = 4;
    public static final Integer ID_ETAT_SUPPRIME = 5;
    public static final Integer ID_ETAT_EN_ATTENTE = 6;
    public static final Integer ID_ETAT_A_VALIDER = 7;
    public static final Integer ID_ETAT_DECAISSABLE = 11;
    public static final Integer ID_ETAT_NON_DECAISSABLE = 12;    
    public static final Integer ID_ETAT_NE_RIEN_FAIRE = 13;    
    public static final Integer ID_ETAT_ALERTE = 14;
    public static final Integer ID_ETAT_BLOCAGE = 15;
    public static final Integer ID_ETAT_TOUTES = 16;    
    public static final Integer ID_ETAT_TOUS = 17;
    public static final Integer ID_ETAT_AUCUN = 18;    
    public static final Integer ID_ETAT_AUCUNE = 19;
    public static final Integer ID_ETAT_RECETTES = 20;    
    public static final Integer ID_ETAT_DEPENSES = 21;    
	public static final Integer ID_ETAT_EN_COURS = 100;    
    
    public static EOSortOrdering SORT_TYET_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(EOTypeEtat.TYET_LIBELLE_KEY, EOSortOrdering.CompareAscending);
    public static EOSortOrdering SORT_TYET_ID_ASC = EOSortOrdering.sortOrderingWithKey(EOTypeEtat.TYET_ID_KEY, EOSortOrdering.CompareAscending);
    
    public EOTypeEtat() {
        super();
    }

    
    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        super.validateBeforeTransactionSave();
    }

	/**
	 * Recherche d'un type etat par son libelle.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param libelle
	 *        libelle auquel correspond l'etat
	 * @return
	 *        un EOTypeEtat
	 */
    public static final EOTypeEtat getTypeEtat(EOEditingContext ed, String libelle) {
    	return getUnTypeEtat(ed, libelle);
    }

	/**
	 * Recherche d'un type etat par son libelle.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param libelle
	 *        libelle auquel correspond l'etat
	 * @return
	 *        un EOTypeEtat
	 */
    private static EOTypeEtat getUnTypeEtat(EOEditingContext ed, String libelle) {
    	NSArray typeEtats=null;
    	EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeEtat.TYET_LIBELLE_KEY+"=%@", new NSArray(libelle));
    	typeEtats = Finder.fetchArray(EOTypeEtat.ENTITY_NAME, qual, null, ed, true);
    	if (typeEtats==null || typeEtats.count()==0)
    		return null;
    	
        return (EOTypeEtat)typeEtats.objectAtIndex(0);
    }
    
}
