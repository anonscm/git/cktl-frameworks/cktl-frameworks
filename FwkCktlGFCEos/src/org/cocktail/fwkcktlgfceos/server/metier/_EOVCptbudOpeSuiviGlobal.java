/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVCptbudOpeSuiviGlobal.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOVCptbudOpeSuiviGlobal extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_VCptbudOpeSuiviGlobal";
	public static final String ENTITY_TABLE_NAME = "GFC.V_CPTBUD_OPE_SUIVI_GLOBAL";

//Attribute Keys
public static final ERXKey<java.math.BigDecimal> AE_CREDIT = new ERXKey<java.math.BigDecimal>("aeCredit");
public static final ERXKey<java.math.BigDecimal> AE_DEBIT = new ERXKey<java.math.BigDecimal>("aeDebit");
public static final ERXKey<java.math.BigDecimal> AE_SOLDE = new ERXKey<java.math.BigDecimal>("aeSolde");
public static final ERXKey<java.math.BigDecimal> CP_CREDIT = new ERXKey<java.math.BigDecimal>("cpCredit");
public static final ERXKey<java.math.BigDecimal> CP_DEBIT = new ERXKey<java.math.BigDecimal>("cpDebit");
public static final ERXKey<java.math.BigDecimal> CP_SOLDE = new ERXKey<java.math.BigDecimal>("cpSolde");
public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");
public static final ERXKey<String> LL_OPERATION = new ERXKey<String>("llOperation");
public static final ERXKey<String> OPE_NUMERO = new ERXKey<String>("opeNumero");
public static final ERXKey<java.math.BigDecimal> REC_CREDIT = new ERXKey<java.math.BigDecimal>("recCredit");
public static final ERXKey<java.math.BigDecimal> REC_DEBIT = new ERXKey<java.math.BigDecimal>("recDebit");
public static final ERXKey<java.math.BigDecimal> REC_SOLDE = new ERXKey<java.math.BigDecimal>("recSolde");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice>("toExercice");

	// Attributes


	public static final String AE_CREDIT_KEY = "aeCredit";
	public static final String AE_DEBIT_KEY = "aeDebit";
	public static final String AE_SOLDE_KEY = "aeSolde";
	public static final String CP_CREDIT_KEY = "cpCredit";
	public static final String CP_DEBIT_KEY = "cpDebit";
	public static final String CP_SOLDE_KEY = "cpSolde";
	public static final String ID_OPE_OPERATION_KEY = "idOpeOperation";
	public static final String LL_OPERATION_KEY = "llOperation";
	public static final String OPE_NUMERO_KEY = "opeNumero";
	public static final String REC_CREDIT_KEY = "recCredit";
	public static final String REC_DEBIT_KEY = "recDebit";
	public static final String REC_SOLDE_KEY = "recSolde";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String AE_CREDIT_COLKEY = "AE_CREDIT";
	public static final String AE_DEBIT_COLKEY = "AE_DEBIT";
	public static final String AE_SOLDE_COLKEY = "AE_SOLDE";
	public static final String CP_CREDIT_COLKEY = "CP_CREDIT";
	public static final String CP_DEBIT_COLKEY = "CP_DEBIT";
	public static final String CP_SOLDE_COLKEY = "CP_SOLDE";
	public static final String ID_OPE_OPERATION_COLKEY = "ID_OPE_OPERATION";
	public static final String LL_OPERATION_COLKEY = "LL_OPERATION";
	public static final String OPE_NUMERO_COLKEY = "OPE_NUMERO";
	public static final String REC_CREDIT_COLKEY = "REC_CREDIT";
	public static final String REC_DEBIT_COLKEY = "REC_DEBIT";
	public static final String REC_SOLDE_COLKEY = "REC_SOLDE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";



	// Accessors methods
  public java.math.BigDecimal aeCredit() {
    return (java.math.BigDecimal) storedValueForKey(AE_CREDIT_KEY);
  }

  public void setAeCredit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AE_CREDIT_KEY);
  }

  public java.math.BigDecimal aeDebit() {
    return (java.math.BigDecimal) storedValueForKey(AE_DEBIT_KEY);
  }

  public void setAeDebit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AE_DEBIT_KEY);
  }

  public java.math.BigDecimal aeSolde() {
    return (java.math.BigDecimal) storedValueForKey(AE_SOLDE_KEY);
  }

  public void setAeSolde(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AE_SOLDE_KEY);
  }

  public java.math.BigDecimal cpCredit() {
    return (java.math.BigDecimal) storedValueForKey(CP_CREDIT_KEY);
  }

  public void setCpCredit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CP_CREDIT_KEY);
  }

  public java.math.BigDecimal cpDebit() {
    return (java.math.BigDecimal) storedValueForKey(CP_DEBIT_KEY);
  }

  public void setCpDebit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CP_DEBIT_KEY);
  }

  public java.math.BigDecimal cpSolde() {
    return (java.math.BigDecimal) storedValueForKey(CP_SOLDE_KEY);
  }

  public void setCpSolde(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CP_SOLDE_KEY);
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    takeStoredValueForKey(value, ID_OPE_OPERATION_KEY);
  }

  public String llOperation() {
    return (String) storedValueForKey(LL_OPERATION_KEY);
  }

  public void setLlOperation(String value) {
    takeStoredValueForKey(value, LL_OPERATION_KEY);
  }

  public String opeNumero() {
    return (String) storedValueForKey(OPE_NUMERO_KEY);
  }

  public void setOpeNumero(String value) {
    takeStoredValueForKey(value, OPE_NUMERO_KEY);
  }

  public java.math.BigDecimal recCredit() {
    return (java.math.BigDecimal) storedValueForKey(REC_CREDIT_KEY);
  }

  public void setRecCredit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_CREDIT_KEY);
  }

  public java.math.BigDecimal recDebit() {
    return (java.math.BigDecimal) storedValueForKey(REC_DEBIT_KEY);
  }

  public void setRecDebit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_DEBIT_KEY);
  }

  public java.math.BigDecimal recSolde() {
    return (java.math.BigDecimal) storedValueForKey(REC_SOLDE_KEY);
  }

  public void setRecSolde(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_SOLDE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  

/**
 * Créer une instance de EOVCptbudOpeSuiviGlobal avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVCptbudOpeSuiviGlobal createEOVCptbudOpeSuiviGlobal(EOEditingContext editingContext, Integer idOpeOperation
, String llOperation
, org.cocktail.fwkcktlgfceos.server.metier.EOExercice toExercice			) {
    EOVCptbudOpeSuiviGlobal eo = (EOVCptbudOpeSuiviGlobal) createAndInsertInstance(editingContext, _EOVCptbudOpeSuiviGlobal.ENTITY_NAME);    
		eo.setIdOpeOperation(idOpeOperation);
		eo.setLlOperation(llOperation);
    eo.setToExerciceRelationship(toExercice);
    return eo;
  }

  
	  public EOVCptbudOpeSuiviGlobal localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVCptbudOpeSuiviGlobal)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVCptbudOpeSuiviGlobal creerInstance(EOEditingContext editingContext) {
	  		EOVCptbudOpeSuiviGlobal object = (EOVCptbudOpeSuiviGlobal)createAndInsertInstance(editingContext, _EOVCptbudOpeSuiviGlobal.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOVCptbudOpeSuiviGlobal localInstanceIn(EOEditingContext editingContext, EOVCptbudOpeSuiviGlobal eo) {
    EOVCptbudOpeSuiviGlobal localInstance = (eo == null) ? null : (EOVCptbudOpeSuiviGlobal)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVCptbudOpeSuiviGlobal#localInstanceIn a la place.
   */
	public static EOVCptbudOpeSuiviGlobal localInstanceOf(EOEditingContext editingContext, EOVCptbudOpeSuiviGlobal eo) {
		return EOVCptbudOpeSuiviGlobal.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVCptbudOpeSuiviGlobal fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVCptbudOpeSuiviGlobal fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVCptbudOpeSuiviGlobal eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVCptbudOpeSuiviGlobal)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVCptbudOpeSuiviGlobal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVCptbudOpeSuiviGlobal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVCptbudOpeSuiviGlobal eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVCptbudOpeSuiviGlobal)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVCptbudOpeSuiviGlobal fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVCptbudOpeSuiviGlobal eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVCptbudOpeSuiviGlobal ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVCptbudOpeSuiviGlobal fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
