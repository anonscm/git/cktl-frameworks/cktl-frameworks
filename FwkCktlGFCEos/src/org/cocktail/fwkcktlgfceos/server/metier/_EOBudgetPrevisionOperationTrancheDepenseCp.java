/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetPrevisionOperationTrancheDepenseCp.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOBudgetPrevisionOperationTrancheDepenseCp extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_BudgetPrevisionOperationTrancheDepenseCp";
	public static final String ENTITY_TABLE_NAME = "GFC.BUD_PREV_OPE_TRA_DEP_CP";

//Attribute Keys
public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
public static final ERXKey<Integer> ID_ADM_DESTINATION_DEPENSE = new ERXKey<Integer>("idAdmDestinationDepense");
public static final ERXKey<Integer> ID_ADM_EB = new ERXKey<Integer>("idAdmEb");
public static final ERXKey<Integer> ID_ADM_NATURE_DEP = new ERXKey<Integer>("idAdmNatureDep");
public static final ERXKey<Integer> ID_BUD_BUDGET = new ERXKey<Integer>("idBudBudget");
public static final ERXKey<Integer> ID_BUD_ENVELOPPE = new ERXKey<Integer>("idBudEnveloppe");
public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");
public static final ERXKey<Integer> ID_OPE_TRANCHE_BUD_DEP_CP = new ERXKey<Integer>("idOpeTrancheBudDepCp");
public static final ERXKey<java.math.BigDecimal> MONTANT_CP = new ERXKey<java.math.BigDecimal>("montantCp");
public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");

// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idBudPrevOpeTraDepCp";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_ADM_DESTINATION_DEPENSE_KEY = "idAdmDestinationDepense";
	public static final String ID_ADM_EB_KEY = "idAdmEb";
	public static final String ID_ADM_NATURE_DEP_KEY = "idAdmNatureDep";
	public static final String ID_BUD_BUDGET_KEY = "idBudBudget";
	public static final String ID_BUD_ENVELOPPE_KEY = "idBudEnveloppe";
	public static final String ID_OPE_OPERATION_KEY = "idOpeOperation";
	public static final String ID_OPE_TRANCHE_BUD_DEP_CP_KEY = "idOpeTrancheBudDepCp";
	public static final String MONTANT_CP_KEY = "montantCp";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

// Attributs non visibles
	public static final String ID_BUD_PREV_OPE_TRA_DEP_CP_KEY = "idBudPrevOpeTraDepCp";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ID_ADM_DESTINATION_DEPENSE_COLKEY = "ID_ADM_DESTINATION_DEPENSE";
	public static final String ID_ADM_EB_COLKEY = "ID_ADM_EB";
	public static final String ID_ADM_NATURE_DEP_COLKEY = "ID_ADM_NATURE_DEP";
	public static final String ID_BUD_BUDGET_COLKEY = "ID_BUD_BUDGET";
	public static final String ID_BUD_ENVELOPPE_COLKEY = "ID_BUD_ENVELOPPE";
	public static final String ID_OPE_OPERATION_COLKEY = "ID_OPE_OPERATION";
	public static final String ID_OPE_TRANCHE_BUD_DEP_CP_COLKEY = "ID_OPE_TRANCHE_BUD_DEP_CP";
	public static final String MONTANT_CP_COLKEY = "MONTANT_CP";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String ID_BUD_PREV_OPE_TRA_DEP_CP_COLKEY = "ID_BUD_PREV_OPE_TRA_DEP_CP";


	// Relationships



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer idAdmDestinationDepense() {
    return (Integer) storedValueForKey(ID_ADM_DESTINATION_DEPENSE_KEY);
  }

  public void setIdAdmDestinationDepense(Integer value) {
    takeStoredValueForKey(value, ID_ADM_DESTINATION_DEPENSE_KEY);
  }

  public Integer idAdmEb() {
    return (Integer) storedValueForKey(ID_ADM_EB_KEY);
  }

  public void setIdAdmEb(Integer value) {
    takeStoredValueForKey(value, ID_ADM_EB_KEY);
  }

  public Integer idAdmNatureDep() {
    return (Integer) storedValueForKey(ID_ADM_NATURE_DEP_KEY);
  }

  public void setIdAdmNatureDep(Integer value) {
    takeStoredValueForKey(value, ID_ADM_NATURE_DEP_KEY);
  }

  public Integer idBudBudget() {
    return (Integer) storedValueForKey(ID_BUD_BUDGET_KEY);
  }

  public void setIdBudBudget(Integer value) {
    takeStoredValueForKey(value, ID_BUD_BUDGET_KEY);
  }

  public Integer idBudEnveloppe() {
    return (Integer) storedValueForKey(ID_BUD_ENVELOPPE_KEY);
  }

  public void setIdBudEnveloppe(Integer value) {
    takeStoredValueForKey(value, ID_BUD_ENVELOPPE_KEY);
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    takeStoredValueForKey(value, ID_OPE_OPERATION_KEY);
  }

  public Integer idOpeTrancheBudDepCp() {
    return (Integer) storedValueForKey(ID_OPE_TRANCHE_BUD_DEP_CP_KEY);
  }

  public void setIdOpeTrancheBudDepCp(Integer value) {
    takeStoredValueForKey(value, ID_OPE_TRANCHE_BUD_DEP_CP_KEY);
  }

  public java.math.BigDecimal montantCp() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_CP_KEY);
  }

  public void setMontantCp(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_CP_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }


/**
 * Créer une instance de EOBudgetPrevisionOperationTrancheDepenseCp avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetPrevisionOperationTrancheDepenseCp createEOBudgetPrevisionOperationTrancheDepenseCp(EOEditingContext editingContext, NSTimestamp dCreation
, Integer idAdmDestinationDepense
, Integer idAdmEb
, Integer idAdmNatureDep
, Integer idBudBudget
, Integer idBudEnveloppe
, Integer idOpeOperation
, Integer idOpeTrancheBudDepCp
, java.math.BigDecimal montantCp
, Integer persIdCreation
			) {
    EOBudgetPrevisionOperationTrancheDepenseCp eo = (EOBudgetPrevisionOperationTrancheDepenseCp) createAndInsertInstance(editingContext, _EOBudgetPrevisionOperationTrancheDepenseCp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setIdAdmDestinationDepense(idAdmDestinationDepense);
		eo.setIdAdmEb(idAdmEb);
		eo.setIdAdmNatureDep(idAdmNatureDep);
		eo.setIdBudBudget(idBudBudget);
		eo.setIdBudEnveloppe(idBudEnveloppe);
		eo.setIdOpeOperation(idOpeOperation);
		eo.setIdOpeTrancheBudDepCp(idOpeTrancheBudDepCp);
		eo.setMontantCp(montantCp);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  
	  public EOBudgetPrevisionOperationTrancheDepenseCp localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetPrevisionOperationTrancheDepenseCp)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetPrevisionOperationTrancheDepenseCp creerInstance(EOEditingContext editingContext) {
	  		EOBudgetPrevisionOperationTrancheDepenseCp object = (EOBudgetPrevisionOperationTrancheDepenseCp)createAndInsertInstance(editingContext, _EOBudgetPrevisionOperationTrancheDepenseCp.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOBudgetPrevisionOperationTrancheDepenseCp localInstanceIn(EOEditingContext editingContext, EOBudgetPrevisionOperationTrancheDepenseCp eo) {
    EOBudgetPrevisionOperationTrancheDepenseCp localInstance = (eo == null) ? null : (EOBudgetPrevisionOperationTrancheDepenseCp)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetPrevisionOperationTrancheDepenseCp#localInstanceIn a la place.
   */
	public static EOBudgetPrevisionOperationTrancheDepenseCp localInstanceOf(EOEditingContext editingContext, EOBudgetPrevisionOperationTrancheDepenseCp eo) {
		return EOBudgetPrevisionOperationTrancheDepenseCp.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetPrevisionOperationTrancheDepenseCp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetPrevisionOperationTrancheDepenseCp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetPrevisionOperationTrancheDepenseCp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetPrevisionOperationTrancheDepenseCp)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetPrevisionOperationTrancheDepenseCp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetPrevisionOperationTrancheDepenseCp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetPrevisionOperationTrancheDepenseCp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetPrevisionOperationTrancheDepenseCp)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetPrevisionOperationTrancheDepenseCp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetPrevisionOperationTrancheDepenseCp eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetPrevisionOperationTrancheDepenseCp ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetPrevisionOperationTrancheDepenseCp fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
