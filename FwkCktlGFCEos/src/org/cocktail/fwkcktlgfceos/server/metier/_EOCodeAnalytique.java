/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeAnalytique.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOCodeAnalytique extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_CodeAnalytique";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_CODE_ANALYTIQUE";

//Attribute Keys
public static final ERXKey<String> CAN_CODE = new ERXKey<String>("canCode");
public static final ERXKey<NSTimestamp> CAN_FERMETURE = new ERXKey<NSTimestamp>("canFermeture");
public static final ERXKey<String> CAN_LIBELLE = new ERXKey<String>("canLibelle");
public static final ERXKey<java.math.BigDecimal> CAN_MONTANT = new ERXKey<java.math.BigDecimal>("canMontant");
public static final ERXKey<Integer> CAN_NIVEAU = new ERXKey<Integer>("canNiveau");
public static final ERXKey<NSTimestamp> CAN_OUVERTURE = new ERXKey<NSTimestamp>("canOuverture");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> CODE_ANALYTIQUE_FILS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique>("codeAnalytiqueFils");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb> CODE_ANALYTIQUE_ORGANS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb>("codeAnalytiqueOrgans");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> CODE_ANALYTIQUE_PERE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique>("codeAnalytiquePere");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtat");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT_DEPASSEMENT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtatDepassement");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT_PUBLIC = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtatPublic");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT_UTILISABLE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtatUtilisable");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "canId";

	public static final String CAN_CODE_KEY = "canCode";
	public static final String CAN_FERMETURE_KEY = "canFermeture";
	public static final String CAN_LIBELLE_KEY = "canLibelle";
	public static final String CAN_MONTANT_KEY = "canMontant";
	public static final String CAN_NIVEAU_KEY = "canNiveau";
	public static final String CAN_OUVERTURE_KEY = "canOuverture";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CAN_ID_PERE_KEY = "canIdPere";
	public static final String CAN_MONTANT_DEPASSEMENT_KEY = "canMontantDepassement";
	public static final String CAN_PUBLIC_KEY = "canPublic";
	public static final String CAN_UTILISABLE_KEY = "canUtilisable";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String CAN_CODE_COLKEY = "CAN_CODE";
	public static final String CAN_FERMETURE_COLKEY = "CAN_FERMETURE";
	public static final String CAN_LIBELLE_COLKEY = "CAN_LIBELLE";
	public static final String CAN_MONTANT_COLKEY = "CAN_MONTANT";
	public static final String CAN_NIVEAU_COLKEY = "CAN_NIVEAU";
	public static final String CAN_OUVERTURE_COLKEY = "CAN_OUVERTURE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CAN_ID_PERE_COLKEY = "CAN_ID_PERE";
	public static final String CAN_MONTANT_DEPASSEMENT_COLKEY = "CAN_MONTANT_DEPASSEMENT";
	public static final String CAN_PUBLIC_COLKEY = "CAN_PUBLIC";
	public static final String CAN_UTILISABLE_COLKEY = "CAN_UTILISABLE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String CODE_ANALYTIQUE_FILS_KEY = "codeAnalytiqueFils";
	public static final String CODE_ANALYTIQUE_ORGANS_KEY = "codeAnalytiqueOrgans";
	public static final String CODE_ANALYTIQUE_PERE_KEY = "codeAnalytiquePere";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_ETAT_DEPASSEMENT_KEY = "typeEtatDepassement";
	public static final String TYPE_ETAT_PUBLIC_KEY = "typeEtatPublic";
	public static final String TYPE_ETAT_UTILISABLE_KEY = "typeEtatUtilisable";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String canCode() {
    return (String) storedValueForKey(CAN_CODE_KEY);
  }

  public void setCanCode(String value) {
    takeStoredValueForKey(value, CAN_CODE_KEY);
  }

  public NSTimestamp canFermeture() {
    return (NSTimestamp) storedValueForKey(CAN_FERMETURE_KEY);
  }

  public void setCanFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_FERMETURE_KEY);
  }

  public String canLibelle() {
    return (String) storedValueForKey(CAN_LIBELLE_KEY);
  }

  public void setCanLibelle(String value) {
    takeStoredValueForKey(value, CAN_LIBELLE_KEY);
  }

  public java.math.BigDecimal canMontant() {
    return (java.math.BigDecimal) storedValueForKey(CAN_MONTANT_KEY);
  }

  public void setCanMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CAN_MONTANT_KEY);
  }

  public Integer canNiveau() {
    return (Integer) storedValueForKey(CAN_NIVEAU_KEY);
  }

  public void setCanNiveau(Integer value) {
    takeStoredValueForKey(value, CAN_NIVEAU_KEY);
  }

  public NSTimestamp canOuverture() {
    return (NSTimestamp) storedValueForKey(CAN_OUVERTURE_KEY);
  }

  public void setCanOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_OUVERTURE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique codeAnalytiquePere() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_PERE_KEY);
  }

  public void setCodeAnalytiquePereRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique oldValue = codeAnalytiquePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtatDepassement() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_DEPASSEMENT_KEY);
  }

  public void setTypeEtatDepassementRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtatDepassement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_DEPASSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_DEPASSEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtatPublic() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_PUBLIC_KEY);
  }

  public void setTypeEtatPublicRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtatPublic();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_PUBLIC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_PUBLIC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtatUtilisable() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_UTILISABLE_KEY);
  }

  public void setTypeEtatUtilisableRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtatUtilisable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_UTILISABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_UTILISABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> codeAnalytiqueFils() {
    return (NSArray)storedValueForKey(CODE_ANALYTIQUE_FILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> codeAnalytiqueFils(EOQualifier qualifier) {
    return codeAnalytiqueFils(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> codeAnalytiqueFils(EOQualifier qualifier, boolean fetch) {
    return codeAnalytiqueFils(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> codeAnalytiqueFils(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique.CODE_ANALYTIQUE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = codeAnalytiqueFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCodeAnalytiqueFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_FILS_KEY);
  }

  public void removeFromCodeAnalytiqueFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_FILS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique createCodeAnalytiqueFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_CodeAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODE_ANALYTIQUE_FILS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique) eo;
  }

  public void deleteCodeAnalytiqueFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodeAnalytiqueFilsRelationships() {
    Enumeration objects = codeAnalytiqueFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodeAnalytiqueFilsRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb> codeAnalytiqueOrgans() {
    return (NSArray)storedValueForKey(CODE_ANALYTIQUE_ORGANS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb> codeAnalytiqueOrgans(EOQualifier qualifier) {
    return codeAnalytiqueOrgans(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb> codeAnalytiqueOrgans(EOQualifier qualifier, boolean fetch) {
    return codeAnalytiqueOrgans(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb> codeAnalytiqueOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb.CODE_ANALYTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = codeAnalytiqueOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
  }

  public void removeFromCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb createCodeAnalytiqueOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_CodeAnalytiqueEb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODE_ANALYTIQUE_ORGANS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb) eo;
  }

  public void deleteCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodeAnalytiqueOrgansRelationships() {
    Enumeration objects = codeAnalytiqueOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodeAnalytiqueOrgansRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCodeAnalytique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCodeAnalytique createEOCodeAnalytique(EOEditingContext editingContext, String canCode
, String canLibelle
, Integer canNiveau
, NSTimestamp canOuverture
, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtatDepassement, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtatPublic, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtatUtilisable			) {
    EOCodeAnalytique eo = (EOCodeAnalytique) createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME);    
		eo.setCanCode(canCode);
		eo.setCanLibelle(canLibelle);
		eo.setCanNiveau(canNiveau);
		eo.setCanOuverture(canOuverture);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setTypeEtatDepassementRelationship(typeEtatDepassement);
    eo.setTypeEtatPublicRelationship(typeEtatPublic);
    eo.setTypeEtatUtilisableRelationship(typeEtatUtilisable);
    return eo;
  }

  
	  public EOCodeAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeAnalytique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnalytique creerInstance(EOEditingContext editingContext) {
	  		EOCodeAnalytique object = (EOCodeAnalytique)createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOCodeAnalytique localInstanceIn(EOEditingContext editingContext, EOCodeAnalytique eo) {
    EOCodeAnalytique localInstance = (eo == null) ? null : (EOCodeAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCodeAnalytique#localInstanceIn a la place.
   */
	public static EOCodeAnalytique localInstanceOf(EOEditingContext editingContext, EOCodeAnalytique eo) {
		return EOCodeAnalytique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
