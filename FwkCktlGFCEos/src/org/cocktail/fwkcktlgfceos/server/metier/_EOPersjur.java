/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersjur.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOPersjur extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_Persjur";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_PERSJUR";

//Attribute Keys
public static final ERXKey<String> PJ_COMMENTAIRE = new ERXKey<String>("pjCommentaire");
public static final ERXKey<NSTimestamp> PJ_DATE_DEBUT = new ERXKey<NSTimestamp>("pjDateDebut");
public static final ERXKey<NSTimestamp> PJ_DATE_FIN = new ERXKey<NSTimestamp>("pjDateFin");
public static final ERXKey<String> PJ_LIBELLE = new ERXKey<String>("pjLibelle");
public static final ERXKey<Integer> PJ_NIVEAU = new ERXKey<Integer>("pjNiveau");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> PERSJUR = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur>("persjur");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne> PERSJUR_PERSONNES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne>("persjurPersonnes");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> PERSJURS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur>("persjurs");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb> PRM_ORGANS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb>("prmOrgans");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypePersjur> TYPE_PERSJUR = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypePersjur>("typePersjur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pjId";

	public static final String PJ_COMMENTAIRE_KEY = "pjCommentaire";
	public static final String PJ_DATE_DEBUT_KEY = "pjDateDebut";
	public static final String PJ_DATE_FIN_KEY = "pjDateFin";
	public static final String PJ_LIBELLE_KEY = "pjLibelle";
	public static final String PJ_NIVEAU_KEY = "pjNiveau";

// Attributs non visibles
	public static final String PJ_ID_KEY = "pjId";
	public static final String PJ_PERE_KEY = "pjPere";
	public static final String TPJ_ID_KEY = "tpjId";

//Colonnes dans la base de donnees
	public static final String PJ_COMMENTAIRE_COLKEY = "PJ_COMMENTAIRE";
	public static final String PJ_DATE_DEBUT_COLKEY = "PJ_DATE_DEBUT";
	public static final String PJ_DATE_FIN_COLKEY = "PJ_DATE_FIN";
	public static final String PJ_LIBELLE_COLKEY = "PJ_LIBELLE";
	public static final String PJ_NIVEAU_COLKEY = "PJ_NIVEAU";

	public static final String PJ_ID_COLKEY = "PJ_ID";
	public static final String PJ_PERE_COLKEY = "PJ_PERE";
	public static final String TPJ_ID_COLKEY = "TPJ_ID";


	// Relationships
	public static final String PERSJUR_KEY = "persjur";
	public static final String PERSJUR_PERSONNES_KEY = "persjurPersonnes";
	public static final String PERSJURS_KEY = "persjurs";
	public static final String PRM_ORGANS_KEY = "prmOrgans";
	public static final String TYPE_PERSJUR_KEY = "typePersjur";



	// Accessors methods
  public String pjCommentaire() {
    return (String) storedValueForKey(PJ_COMMENTAIRE_KEY);
  }

  public void setPjCommentaire(String value) {
    takeStoredValueForKey(value, PJ_COMMENTAIRE_KEY);
  }

  public NSTimestamp pjDateDebut() {
    return (NSTimestamp) storedValueForKey(PJ_DATE_DEBUT_KEY);
  }

  public void setPjDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, PJ_DATE_DEBUT_KEY);
  }

  public NSTimestamp pjDateFin() {
    return (NSTimestamp) storedValueForKey(PJ_DATE_FIN_KEY);
  }

  public void setPjDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, PJ_DATE_FIN_KEY);
  }

  public String pjLibelle() {
    return (String) storedValueForKey(PJ_LIBELLE_KEY);
  }

  public void setPjLibelle(String value) {
    takeStoredValueForKey(value, PJ_LIBELLE_KEY);
  }

  public Integer pjNiveau() {
    return (Integer) storedValueForKey(PJ_NIVEAU_KEY);
  }

  public void setPjNiveau(Integer value) {
    takeStoredValueForKey(value, PJ_NIVEAU_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOPersjur persjur() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPersjur)storedValueForKey(PERSJUR_KEY);
  }

  public void setPersjurRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPersjur value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOPersjur oldValue = persjur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSJUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSJUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypePersjur typePersjur() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypePersjur)storedValueForKey(TYPE_PERSJUR_KEY);
  }

  public void setTypePersjurRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypePersjur value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypePersjur oldValue = typePersjur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PERSJUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PERSJUR_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne> persjurPersonnes() {
    return (NSArray)storedValueForKey(PERSJUR_PERSONNES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne> persjurPersonnes(EOQualifier qualifier) {
    return persjurPersonnes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne> persjurPersonnes(EOQualifier qualifier, boolean fetch) {
    return persjurPersonnes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne> persjurPersonnes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = persjurPersonnes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersjurPersonnesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
  }

  public void removeFromPersjurPersonnesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne createPersjurPersonnesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_PersjurPersonne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSJUR_PERSONNES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne) eo;
  }

  public void deletePersjurPersonnesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersjurPersonnesRelationships() {
    Enumeration objects = persjurPersonnes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersjurPersonnesRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOPersjurPersonne)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> persjurs() {
    return (NSArray)storedValueForKey(PERSJURS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> persjurs(EOQualifier qualifier) {
    return persjurs(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> persjurs(EOQualifier qualifier, boolean fetch) {
    return persjurs(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> persjurs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOPersjur.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOPersjur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = persjurs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersjursRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPersjur object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSJURS_KEY);
  }

  public void removeFromPersjursRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPersjur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJURS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOPersjur createPersjursRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_Persjur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSJURS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPersjur) eo;
  }

  public void deletePersjursRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPersjur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJURS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersjursRelationships() {
    Enumeration objects = persjurs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersjursRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOPersjur)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb> prmOrgans() {
    return (NSArray)storedValueForKey(PRM_ORGANS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb> prmOrgans(EOQualifier qualifier) {
    return prmOrgans(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb> prmOrgans(EOQualifier qualifier, boolean fetch) {
    return prmOrgans(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb> prmOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prmOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPrmOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
  }

  public void removeFromPrmOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb createPrmOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_PrmEb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRM_ORGANS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb) eo;
  }

  public void deletePrmOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrmOrgansRelationships() {
    Enumeration objects = prmOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrmOrgansRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOPrmEb)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPersjur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersjur createEOPersjur(EOEditingContext editingContext, NSTimestamp pjDateDebut
, Integer pjNiveau
, org.cocktail.fwkcktlgfceos.server.metier.EOTypePersjur typePersjur			) {
    EOPersjur eo = (EOPersjur) createAndInsertInstance(editingContext, _EOPersjur.ENTITY_NAME);    
		eo.setPjDateDebut(pjDateDebut);
		eo.setPjNiveau(pjNiveau);
    eo.setTypePersjurRelationship(typePersjur);
    return eo;
  }

  
	  public EOPersjur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersjur)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersjur creerInstance(EOEditingContext editingContext) {
	  		EOPersjur object = (EOPersjur)createAndInsertInstance(editingContext, _EOPersjur.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOPersjur localInstanceIn(EOEditingContext editingContext, EOPersjur eo) {
    EOPersjur localInstance = (eo == null) ? null : (EOPersjur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersjur#localInstanceIn a la place.
   */
	public static EOPersjur localInstanceOf(EOEditingContext editingContext, EOPersjur eo) {
		return EOPersjur.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOPersjur>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersjur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersjur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersjur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersjur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersjur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersjur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersjur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersjur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
