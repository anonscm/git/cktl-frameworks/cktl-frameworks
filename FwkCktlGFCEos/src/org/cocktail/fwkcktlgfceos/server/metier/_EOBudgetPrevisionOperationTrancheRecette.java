/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetPrevisionOperationTrancheRecette.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOBudgetPrevisionOperationTrancheRecette extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_BudgetPrevisionOperationTrancheRecette";
	public static final String ENTITY_TABLE_NAME = "GFC.BUD_PREV_OPE_TRA_REC";

//Attribute Keys
public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
public static final ERXKey<Integer> ID_ADM_EB = new ERXKey<Integer>("idAdmEb");
public static final ERXKey<Integer> ID_ADM_NATURE_REC = new ERXKey<Integer>("idAdmNatureRec");
public static final ERXKey<Integer> ID_ADM_ORIGINE_RECETTE = new ERXKey<Integer>("idAdmOrigineRecette");
public static final ERXKey<Integer> ID_BUD_BUDGET = new ERXKey<Integer>("idBudBudget");
public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");
public static final ERXKey<Integer> ID_OPE_TRANCHE_BUD_REC = new ERXKey<Integer>("idOpeTrancheBudRec");
public static final ERXKey<java.math.BigDecimal> MONTANT_REC = new ERXKey<java.math.BigDecimal>("montantRec");
public static final ERXKey<java.math.BigDecimal> MONTANT_TITRE = new ERXKey<java.math.BigDecimal>("montantTitre");
public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
public static final ERXKey<String> SECTION = new ERXKey<String>("section");

// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idBudPrevOpeTraRec";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_ADM_EB_KEY = "idAdmEb";
	public static final String ID_ADM_NATURE_REC_KEY = "idAdmNatureRec";
	public static final String ID_ADM_ORIGINE_RECETTE_KEY = "idAdmOrigineRecette";
	public static final String ID_BUD_BUDGET_KEY = "idBudBudget";
	public static final String ID_OPE_OPERATION_KEY = "idOpeOperation";
	public static final String ID_OPE_TRANCHE_BUD_REC_KEY = "idOpeTrancheBudRec";
	public static final String MONTANT_REC_KEY = "montantRec";
	public static final String MONTANT_TITRE_KEY = "montantTitre";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String SECTION_KEY = "section";

// Attributs non visibles
	public static final String ID_BUD_PREV_OPE_TRA_REC_KEY = "idBudPrevOpeTraRec";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ID_ADM_EB_COLKEY = "ID_ADM_EB";
	public static final String ID_ADM_NATURE_REC_COLKEY = "ID_ADM_NATURE_REC";
	public static final String ID_ADM_ORIGINE_RECETTE_COLKEY = "ID_ADM_ORIGINE_RECETTE";
	public static final String ID_BUD_BUDGET_COLKEY = "ID_BUD_BUDGET";
	public static final String ID_OPE_OPERATION_COLKEY = "ID_OPE_OPERATION";
	public static final String ID_OPE_TRANCHE_BUD_REC_COLKEY = "ID_OPE_TRANCHE_BUD_REC";
	public static final String MONTANT_REC_COLKEY = "MONTANT_REC";
	public static final String MONTANT_TITRE_COLKEY = "MONTANT_TITRE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String SECTION_COLKEY = "SECTION";

	public static final String ID_BUD_PREV_OPE_TRA_REC_COLKEY = "ID_BUD_PREV_OPE_TRA_REC";


	// Relationships



	// Accessors methods
  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer idAdmEb() {
    return (Integer) storedValueForKey(ID_ADM_EB_KEY);
  }

  public void setIdAdmEb(Integer value) {
    takeStoredValueForKey(value, ID_ADM_EB_KEY);
  }

  public Integer idAdmNatureRec() {
    return (Integer) storedValueForKey(ID_ADM_NATURE_REC_KEY);
  }

  public void setIdAdmNatureRec(Integer value) {
    takeStoredValueForKey(value, ID_ADM_NATURE_REC_KEY);
  }

  public Integer idAdmOrigineRecette() {
    return (Integer) storedValueForKey(ID_ADM_ORIGINE_RECETTE_KEY);
  }

  public void setIdAdmOrigineRecette(Integer value) {
    takeStoredValueForKey(value, ID_ADM_ORIGINE_RECETTE_KEY);
  }

  public Integer idBudBudget() {
    return (Integer) storedValueForKey(ID_BUD_BUDGET_KEY);
  }

  public void setIdBudBudget(Integer value) {
    takeStoredValueForKey(value, ID_BUD_BUDGET_KEY);
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    takeStoredValueForKey(value, ID_OPE_OPERATION_KEY);
  }

  public Integer idOpeTrancheBudRec() {
    return (Integer) storedValueForKey(ID_OPE_TRANCHE_BUD_REC_KEY);
  }

  public void setIdOpeTrancheBudRec(Integer value) {
    takeStoredValueForKey(value, ID_OPE_TRANCHE_BUD_REC_KEY);
  }

  public java.math.BigDecimal montantRec() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_REC_KEY);
  }

  public void setMontantRec(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_REC_KEY);
  }

  public java.math.BigDecimal montantTitre() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_TITRE_KEY);
  }

  public void setMontantTitre(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_TITRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String section() {
    return (String) storedValueForKey(SECTION_KEY);
  }

  public void setSection(String value) {
    takeStoredValueForKey(value, SECTION_KEY);
  }


/**
 * Créer une instance de EOBudgetPrevisionOperationTrancheRecette avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetPrevisionOperationTrancheRecette createEOBudgetPrevisionOperationTrancheRecette(EOEditingContext editingContext, NSTimestamp dCreation
, Integer idAdmEb
, Integer idAdmNatureRec
, Integer idAdmOrigineRecette
, Integer idBudBudget
, Integer idOpeOperation
, Integer idOpeTrancheBudRec
, java.math.BigDecimal montantRec
, java.math.BigDecimal montantTitre
, Integer persIdCreation
, String section
			) {
    EOBudgetPrevisionOperationTrancheRecette eo = (EOBudgetPrevisionOperationTrancheRecette) createAndInsertInstance(editingContext, _EOBudgetPrevisionOperationTrancheRecette.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setIdAdmEb(idAdmEb);
		eo.setIdAdmNatureRec(idAdmNatureRec);
		eo.setIdAdmOrigineRecette(idAdmOrigineRecette);
		eo.setIdBudBudget(idBudBudget);
		eo.setIdOpeOperation(idOpeOperation);
		eo.setIdOpeTrancheBudRec(idOpeTrancheBudRec);
		eo.setMontantRec(montantRec);
		eo.setMontantTitre(montantTitre);
		eo.setPersIdCreation(persIdCreation);
		eo.setSection(section);
    return eo;
  }

  
	  public EOBudgetPrevisionOperationTrancheRecette localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetPrevisionOperationTrancheRecette)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetPrevisionOperationTrancheRecette creerInstance(EOEditingContext editingContext) {
	  		EOBudgetPrevisionOperationTrancheRecette object = (EOBudgetPrevisionOperationTrancheRecette)createAndInsertInstance(editingContext, _EOBudgetPrevisionOperationTrancheRecette.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOBudgetPrevisionOperationTrancheRecette localInstanceIn(EOEditingContext editingContext, EOBudgetPrevisionOperationTrancheRecette eo) {
    EOBudgetPrevisionOperationTrancheRecette localInstance = (eo == null) ? null : (EOBudgetPrevisionOperationTrancheRecette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetPrevisionOperationTrancheRecette#localInstanceIn a la place.
   */
	public static EOBudgetPrevisionOperationTrancheRecette localInstanceOf(EOEditingContext editingContext, EOBudgetPrevisionOperationTrancheRecette eo) {
		return EOBudgetPrevisionOperationTrancheRecette.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetPrevisionOperationTrancheRecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetPrevisionOperationTrancheRecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetPrevisionOperationTrancheRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetPrevisionOperationTrancheRecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetPrevisionOperationTrancheRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetPrevisionOperationTrancheRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetPrevisionOperationTrancheRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetPrevisionOperationTrancheRecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetPrevisionOperationTrancheRecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetPrevisionOperationTrancheRecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetPrevisionOperationTrancheRecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetPrevisionOperationTrancheRecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
