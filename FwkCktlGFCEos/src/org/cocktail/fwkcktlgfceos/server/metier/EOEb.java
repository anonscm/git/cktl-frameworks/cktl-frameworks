/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software is governed by the CeCILL license under French law and abiding by the rules of distribution
 * of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL "http://www.cecill.info". As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license,
 * users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that
 * it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to
 * use and operate it in the same conditions as regards security. The fact that you are presently reading this means that you have had knowledge of the CeCILL
 * license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.IERXChainableQualifier;

public class EOEb extends _EOEb {
	public final static Logger logger = Logger.getLogger(EOEb.class);

	private static final String CONST_VIDE = "";
	private static final String CONST_SLASH = " / ";
	public static final Integer ORG_NIV_0 = new Integer(0);
	public static final Integer ORG_NIV_1 = new Integer(1);
	public static final Integer ORG_NIV_2 = new Integer(2);
	public static final Integer ORG_NIV_3 = new Integer(3);
	public static final Integer ORG_NIV_4 = new Integer(4);
	public static final int ORG_NIV_MAX = EOEb.ORG_NIV_4.intValue();

	public static final String LONG_STRING_KEY = "longString";
	public static final String LONG_STRING_WITH_LIB_KEY = "longStringWithLib";
	public static final String IS_TYPE_RA_KEY = "isTypeRA";

	public static final EOQualifier QUAL_NIVEAU_ETAB = new EOKeyValueQualifier(EOEb.ORG_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, ORG_NIV_1);
	public static final EOQualifier QUAL_NIVEAU_UB = new EOKeyValueQualifier(EOEb.ORG_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, ORG_NIV_2);
	public static final EOQualifier QUAL_NIVEAU_CR = new EOKeyValueQualifier(EOEb.ORG_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, ORG_NIV_3);
	public static final EOQualifier QUAL_NIVEAU_SOUSCR = new EOKeyValueQualifier(EOEb.ORG_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, ORG_NIV_4);

	public static final EOSortOrdering SORT_ORG_UNIV_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UNIV_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_ETAB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_ETAB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_UB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_CR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_CR_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_SOUSCR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending);

	/** Tableau de tri par defaut (univ, etab, ub, cr, souscr) */
	public static final NSArray SORT_DEFAULT = new NSArray(new Object[] {
			SORT_ORG_UNIV_ASC, SORT_ORG_ETAB_ASC, SORT_ORG_UB_ASC, SORT_ORG_CR_ASC,
			SORT_ORG_SOUSCR_ASC
	});

	public static NSValidation.ValidationException EXCEPTION_DELETE_ORGAN_A_ENFANTS = new NSValidation.ValidationException(
			"Impossible de supprimer une ligne budgétaire qui a des enfants.");
	public static NSValidation.ValidationException EXCEPTION_DELETE_ORGAN_A_UTILISATEURS = new NSValidation.ValidationException(
			"Impossible de supprimer une ligne budgétaire qui a des utilisateurs affectés. Supprimez les autilisateurs affectés à cette ligne pour pouvoir la supprimer.");

	public static final String ORG_NIV_0_LIB = "UNIVERSITE";
	public static final String ORG_NIV_1_LIB = "ETABLISSEMENT";
	public static final String ORG_NIV_2_LIB = "UB";
	public static final String ORG_NIV_3_LIB = "CR";
	public static final String ORG_NIV_4_LIB = "SOUS CR";

	/** Libellés des niveaux (par niveau) */
	public static final Map NIV_LIB_MAP = new HashMap();
	public static final int NIVEAU_MIN_CONV_RA = ORG_NIV_3.intValue();

	public static final String DEFAULT_LIBELLE = "Nouveau";
	public static final int MAX_CHARS_UNIV = 10;
	public static final int MAX_CHARS_ETAB = 10;
	public static final int MAX_CHARS_UB = 10;
	public static final int MAX_CHARS_CR = 50;
	public static final int MAX_CHARS_SOUSCR = 50;

	static {
		NIV_LIB_MAP.put(ORG_NIV_0, ORG_NIV_0_LIB);
		NIV_LIB_MAP.put(ORG_NIV_1, ORG_NIV_1_LIB);
		NIV_LIB_MAP.put(ORG_NIV_2, ORG_NIV_2_LIB);
		NIV_LIB_MAP.put(ORG_NIV_3, ORG_NIV_3_LIB);
		NIV_LIB_MAP.put(ORG_NIV_4, ORG_NIV_4_LIB);
	};

	public EOEb() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		if (organFils() != null && organFils().count() > 0) {
			throw EXCEPTION_DELETE_ORGAN_A_ENFANTS;
		}
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public final String getLongString() {
		// String tmp = orgUniv();
		String tmp = orgEtab();
		tmp = tmp + (orgUb() != null ? CONST_SLASH + orgUb() : CONST_VIDE);
		tmp = tmp + (orgCr() != null ? CONST_SLASH + orgCr() : CONST_VIDE);
		tmp = tmp + (orgSouscr() != null ? CONST_SLASH + orgSouscr() : CONST_VIDE);

		return tmp;
	}

	public final String getLongStringWithLib() {
		// String tmp = orgUniv();
		String tmp = orgEtab();
		tmp = tmp + (orgUb() != null ? CONST_SLASH + orgUb() : CONST_VIDE);
		tmp = tmp + (orgCr() != null ? CONST_SLASH + orgCr() : CONST_VIDE);
		tmp = tmp + (orgSouscr() != null ? CONST_SLASH + orgSouscr() : CONST_VIDE);
		tmp = tmp + (orgLibelle() != null ? " (" + orgLibelle() + ")" : CONST_VIDE);
		return tmp;
	}

	/**
	 * Renvoie selon le niveau le bon champ (orgUniv ou orgEtab ou orgUb, etc.)
	 * 
	 * @param s
	 */
	public final String getShortString() {
		final int niv = orgNiveau().intValue();
		String res = null;
		switch (niv) {
		case 0:
			res = orgUniv();
			break;

		case 1:
			res = orgEtab();
			break;

		case 2:
			res = orgUb();
			break;

		case 3:
			res = orgCr();
			break;

		case 4:
			res = orgSouscr();
			break;

		default:
			break;
		}
		return res;
	}

	public final String getNiveauLib() {
		return (String) NIV_LIB_MAP.get(new Integer(orgNiveau().intValue()));
	}

	/**
	 * @param exercice
	 * @return Un qualifier pour selectionner les organ ouvertes sur l'exercice
	 */
	public static EOQualifier qualOrganOuvertes(EOExercice exercice) {
		final Date dateDebut = DateCtrl.getFirstDayOfYear(exercice.exeExercice().intValue());
		final Date dateFin = DateCtrl.addDHMS(DateCtrl.getLastDayOfYear(exercice.exeExercice().intValue()), 1, 0, 0, 0);

		EOQualifier qual = EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXERCICE).dot(EOExercice.EXE_OUVERTURE).lessThanOrEqualTo(new NSTimestamp(dateDebut))
				.and(EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXERCICE).dot(EOExercice.EXE_CLOTURE).isNull()
				.or(EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXERCICE).dot(EOExercice.EXE_CLOTURE).greaterThanOrEqualTo(new NSTimestamp(dateFin))));
		return qual;

	}

	public static EOQualifier qualFromOrgan(EOEb organ) {
		IERXChainableQualifier qual = ERXQ.equals(ORG_UNIV_KEY, organ.orgUniv());
		if (organ.orgEtab() != null)
			qual = qual.and(ERXQ.equals(ORG_ETAB_KEY, organ.orgEtab()));
		if (organ.orgUb() != null)
			qual = qual.and(ERXQ.equals(ORG_UB_KEY, organ.orgUb()));
		if (organ.orgCr() != null)
			qual = qual.and(ERXQ.equals(ORG_CR_KEY, organ.orgCr()));
		if (organ.orgSouscr() != null)
			qual = qual.and(ERXQ.equals(ORG_SOUSCR_KEY, organ.orgSouscr()));
		return (EOQualifier) qual;
	}

	/**
	 * Cree un nouvel organ
	 * 
	 * @param ec
	 * @param organPere
	 * @param shortLib
	 * @return
	 * @throws Exception
	 */
	public static EOEb initNewEOOrgan(final EOEditingContext ec, final EOEb organPere, String shortLib) throws Exception {
		final EOEb organ = creerInstance(ec);
		Integer niveau = new Integer(0);

		if (shortLib != null) {
			shortLib = shortLib.trim();
		}

		if (organPere != null) {
			niveau = new Integer(organPere.orgNiveau().intValue() + 1);
			if (niveau.intValue() > EOEb.ORG_NIV_MAX) {
				throw new NSValidation.ValidationException("Impossible de créer une ligne budgétaire de niveau " + niveau + ".");
			}
			organ.setOrganPereRelationship(organPere);

			organ.setOrgUniv(organPere.orgUniv());
			organ.setOrgEtab(organPere.orgEtab());
			organ.setOrgUb(organPere.orgUb());
			organ.setOrgCr(organPere.orgCr());
			organ.setTypeOrganRelationship(organPere.typeOrgan());
		}

		organ.setOrgNiveau(niveau);
		switch (niveau.intValue()) {
		case 0:
			organ.setOrgUniv(shortLib);
			break;

		case 1:
			organ.setOrgEtab(shortLib);
			break;

		case 2:
			organ.setOrgUb(shortLib);
			break;

		case 3:
			organ.setOrgCr(shortLib);
			break;

		case 4:
			organ.setOrgSouscr(shortLib);
			break;
		default:
			break;
		}
		return organ;
	}

	public static EOEb creerNewEOOrgan(final EOEditingContext ec, final EOEb organPere, String shortLib) throws Exception {
		final EOEb organ = EOEb.creerInstance(ec);
		Integer niveau = new Integer(0);

		if (shortLib != null) {
			shortLib = shortLib.trim();
		}
		organ.setOrgLibelle(shortLib);
		if (organPere != null) {
			niveau = new Integer(organPere.orgNiveau().intValue() + 1);
			if (niveau.intValue() > EOEb.ORG_NIV_MAX) {
				throw new Exception("Impossible de créer une ligne budgétaire de niveau " + niveau + ".");
			}
			organ.setOrganPereRelationship(organPere);

			organ.setOrgUniv(organPere.orgUniv());
			organ.setOrgEtab(organPere.orgEtab());
			organ.setOrgUb(organPere.orgUb());
			organ.setOrgCr(organPere.orgCr());
			organ.setTypeOrganRelationship(organPere.typeOrgan());
		}

		organ.setOrgNiveau(niveau);
		switch (niveau.intValue()) {
		case 0:
			organ.setOrgUniv(shortLib);
			break;

		case 1:
			organ.setOrgEtab(shortLib);
			break;

		case 2:
			organ.setOrgUb(shortLib);
			break;

		case 3:
			organ.setOrgCr(shortLib);
			break;

		case 4:
			organ.setOrgSouscr(shortLib);
			break;
		default:
			break;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("creerNewEOOrgan Organ = " + organ);
		}
		return organ;
	}

	public void affecteStructure(final EOEditingContext editingContext, final EOEb organ, final EOStructure ulr) {
		organ.setStructureRelationship(ulr);
	}
	
	/**
	 * Met à jour orgUniv pour organ et tous ses fils.
	 * 
	 * @param ec
	 * @param s
	 */
	private void setOrgUnivRecursive(final EOEditingContext ec, final EOEb organ, final String s) {
		organ.setOrgUniv(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOEb element = (EOEb) organ.organFils().objectAtIndex(i);
			setOrgUnivRecursive(ec, element, s);
		}
	}

	private void setOrgEtabRecursive(final EOEditingContext ec, final EOEb organ, final String s) {
		organ.setOrgEtab(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOEb element = (EOEb) organ.organFils().objectAtIndex(i);
			setOrgEtabRecursive(ec, element, s);
		}
	}

	private void setOrgUbRecursive(final EOEditingContext ec, final EOEb organ, final String s) {
		organ.setOrgUb(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOEb element = (EOEb) organ.organFils().objectAtIndex(i);
			setOrgUbRecursive(ec, element, s);
		}
	}

	private void setOrgCrRecursive(final EOEditingContext ec, final EOEb organ, final String s) {
		organ.setOrgCr(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOEb element = (EOEb) organ.organFils().objectAtIndex(i);
			setOrgCrRecursive(ec, element, s);
		}
	}

	private void setOrgSouscrRecursive(final EOEditingContext ec, final EOEb organ, final String s) {
		organ.setOrgSouscr(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOEb element = (EOEb) organ.organFils().objectAtIndex(i);
			setOrgSouscrRecursive(ec, element, s);
		}
	}

	/**
	 * Met à jour selon le niveau le bon champ (orgUniv ou orgEtab ou orgUb, etc.)
	 * 
	 * @param s
	 */
	public final void setShortString(final EOEditingContext ec, final EOEb organ, final String s) {
		final String s1 = s.trim();
		final int niv = organ.orgNiveau().intValue();
		switch (niv) {
		case 0:
			setOrgUnivRecursive(ec, organ, StringCtrl.cut(s1, MAX_CHARS_UNIV));
			break;

		case 1:
			setOrgEtabRecursive(ec, organ, StringCtrl.cut(s1, MAX_CHARS_ETAB));
			break;

		case 2:
			setOrgUbRecursive(ec, organ, StringCtrl.cut(s1, MAX_CHARS_UB));
			break;

		case 3:
			setOrgCrRecursive(ec, organ, StringCtrl.cut(s1, MAX_CHARS_CR));
			break;

		case 4:
			setOrgSouscrRecursive(ec, organ, StringCtrl.cut(s1, MAX_CHARS_SOUSCR));

		default:
			break;
		}
	}

	/**
	 * rempli les parametres univ, etab etc. en fonction du niveau de l'organ.
	 * 
	 * @param ec
	 * @param organ
	 * @param s
	 * @param univ
	 * @param etab
	 * @param ub
	 * @param cr
	 * @param souscr
	 */
	public static final Map convertShortString(final EOEditingContext ec, final EOEb organ, final String s) {
		final String s1 = s.trim();
		Map res = new HashMap();
		final int niv = organ.orgNiveau().intValue();
		switch (niv) {
		case 0:
			res.put(EOEb.ORG_UNIV_KEY, s1);
			res.put(EOEb.ORG_ETAB_KEY, null);
			res.put(EOEb.ORG_UB_KEY, null);
			res.put(EOEb.ORG_CR_KEY, null);
			res.put(EOEb.ORG_SOUSCR_KEY, null);
			break;

		case 1:
			res.put(EOEb.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOEb.ORG_ETAB_KEY, s1);
			res.put(EOEb.ORG_UB_KEY, null);
			res.put(EOEb.ORG_CR_KEY, null);
			res.put(EOEb.ORG_SOUSCR_KEY, null);
			break;

		case 2:
			res.put(EOEb.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOEb.ORG_ETAB_KEY, organ.orgEtab());
			res.put(EOEb.ORG_UB_KEY, s1);
			res.put(EOEb.ORG_CR_KEY, null);
			res.put(EOEb.ORG_SOUSCR_KEY, null);
			break;

		case 3:
			res.put(EOEb.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOEb.ORG_ETAB_KEY, organ.orgEtab());
			res.put(EOEb.ORG_UB_KEY, organ.orgUb());
			res.put(EOEb.ORG_CR_KEY, s1);
			res.put(EOEb.ORG_SOUSCR_KEY, null);
			break;

		case 4:
			res.put(EOEb.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOEb.ORG_ETAB_KEY, organ.orgEtab());
			res.put(EOEb.ORG_UB_KEY, organ.orgUb());
			res.put(EOEb.ORG_CR_KEY, organ.orgCr());
			res.put(EOEb.ORG_SOUSCR_KEY, s1);
			break;

		default:
			break;
		}
		return res;
	}

	public EOEbSignataire creerNewEOOrganSignataire(final EOEditingContext ec, final EOTypeSignature typeSignature, final EOEb organ,
			final EOIndividu individuUlr) {
		final EOEbSignataire organSignataire = EOEbSignataire.creerInstance(ec);
		organSignataire.setTypeSignatureRelationship(typeSignature);
		organSignataire.setOrganRelationship(organ);
		organSignataire.setIndividuRelationship(individuUlr);
		if (logger.isDebugEnabled()) {
			logger.debug("Nouvel objet EOOrganSignataire = " + organSignataire);
		}
		return organSignataire;
	}

	public void supprimeEOOrganSignataire(final EOEditingContext ec, EOEbSignataire organSignataire) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("supprimeEOOrganSignataire = " + organSignataire);
		}
		for (int i = 0; i < organSignataire.organSignataireTcs().count(); i++) {
			supprimeEOOrganSignataireTc(ec, (EOEbSignataireTc) organSignataire.organSignataireTcs().objectAtIndex(i));
		}
		organSignataire.setOrganRelationship(null);
		ec.deleteObject(organSignataire);
	}

	public void supprimeEOOrganSignataireTc(EOEditingContext editingContext, EOEbSignataireTc ost) {
		if (logger.isDebugEnabled()) {
			logger.debug("supprimeEOOrganSignataireTc = " + ost);
		}
		ost.setOrganSignataireRelationship(null);
		editingContext.deleteObject(ost);
	}

	public EOEbSignataireTc creerNewEOOrganSignataireTc(EOEditingContext editingContext, EOEbSignataire os, EOTypeCredit tc, BigDecimal decimal)
			throws Exception {
		final EOEbSignataireTc organSignataireTc = EOEbSignataireTc.creerInstance(editingContext);
		organSignataireTc.setOrganSignataireRelationship(os);
		organSignataireTc.setTypeCreditRelationship(tc);
		organSignataireTc.setOstMaxMontantTtc(decimal);
		return organSignataireTc;
	}

	/**
	 * Affecte un taux de prorata à une branche de l'organigramme budgétaire.
	 * 
	 * @param editingContext
	 * @param tauxProrata
	 * @param organ
	 * @param exercice
	 * @return
	 * @throws Exception
	 */
	public EOEbProrata creerNewEOOrganProrata(final EOEditingContext editingContext, final EOTauxProrata tauxProrata, final EOEb organ,
			final EOExercice exercice) throws Exception {
		// vérifier que l'affectation n'existe pas déjà
		EOEbProrata obj = findOrganProrata(tauxProrata, organ, exercice);
		if (obj == null) {
			obj = EOEbProrata.creerInstance(editingContext);

			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEbProrata.EXERCICE_KEY + "=%@", new NSArray(new Object[] {
					exercice
			}));

			if (EOQualifier.filteredArrayWithQualifier(organ.organProratas(), qual).count() == 0) {
				obj.setOrpPriorite(EOEbProrata.ORP_PRIORITE_0);
			}
			else {
				obj.setOrpPriorite(EOEbProrata.ORP_PRIORITE_1);
			}
			obj.setOrganRelationship(organ);
			obj.setTauxProrataRelationship(tauxProrata);
			obj.setExerciceRelationship(exercice);

		}
		return obj;

	}

	/**
	 * Affecte un taux de prorata a la branche et a ses enfants.
	 * 
	 * @param editingContext
	 * @param tauxProrata
	 * @param organ
	 * @param exercice
	 * @return
	 * @throws Exception
	 */
	public EOEbProrata creerNewEOOrganProrataRecursive(final EOEditingContext editingContext, final EOTauxProrata tauxProrata, final EOEb organ,
			final EOExercice exercice) throws Exception {
		final EOEbProrata obj = creerNewEOOrganProrata(editingContext, tauxProrata, organ, exercice);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOEb element = (EOEb) organ.organFils().objectAtIndex(i);
			creerNewEOOrganProrataRecursive(editingContext, tauxProrata, element, exercice);
		}
		return obj;
	}

	public boolean isTypeLigneBudgetaire() {
		return typeOrgan().tyorLibelle().equals(EOTypeEb.TYPE_LIGNE_BUDGETAIRE);
	}

	public boolean isTypeRA() {
		return typeOrgan().tyorLibelle().equals(EOTypeEb.TYPE_CONVENTION_RA);
	}

	private EOEbProrata findOrganProrata(final EOTauxProrata tauxProrata, final EOEb organ, final EOExercice exercice) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEbProrata.TAUX_PRORATA_KEY + "=%@ and " + EOEbProrata.EXERCICE_KEY + "=%@",
				new NSArray(new Object[] {
						tauxProrata, exercice
				}));
		NSArray res = EOQualifier.filteredArrayWithQualifier(organ.organProratas(), qual);
		if (res.count() > 0) {
			return (EOEbProrata) res.objectAtIndex(0);
		}
		return null;
	}

	public static EOEb organFinancier(EOEditingContext editingContext, EOExercice exercice) {
		EOEb organFinancier = null;
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOParametre.EXERCICE_KEY, exercice),
				ERXQ.equals(EOParametre.PAR_KEY_KEY, "org.cocktail.gfc.accords.cr_financier_defaut"));
		EOParametre crFinancierParam = EOParametre.fetchByQualifier(editingContext, qualifier);
		if (crFinancierParam != null) {
			organFinancier = fetchByKeyValue(editingContext, ID_ADM_EB_KEY, Integer.valueOf(crFinancierParam.parValue()));
		}

		return organFinancier;
	}

}
