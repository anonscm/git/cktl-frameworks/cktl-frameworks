/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODestinationDepenseExercice.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EODestinationDepenseExercice extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_DestinationDepenseExercice";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_DESTINATION_DEP_EXER";

//Attribute Keys
public static final ERXKey<Boolean> EDITION_BUDGET = new ERXKey<Boolean>("editionBudget");
public static final ERXKey<Boolean> SAISIE_BUDGETAIRE = new ERXKey<Boolean>("saisieBudgetaire");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> DESTINATION_DEPENSE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>("destinationDepense");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idDestinationDepenseExercice";

	public static final String EDITION_BUDGET_KEY = "editionBudget";
	public static final String SAISIE_BUDGETAIRE_KEY = "saisieBudgetaire";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ID_DESTINATION_DEPENSE_KEY = "idDestinationDepense";
	public static final String ID_DESTINATION_DEPENSE_EXERCICE_KEY = "idDestinationDepenseExercice";

//Colonnes dans la base de donnees
	public static final String EDITION_BUDGET_COLKEY = "EDITION_BUDGET";
	public static final String SAISIE_BUDGETAIRE_COLKEY = "SAISIE_BUDGETAIRE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ID_DESTINATION_DEPENSE_COLKEY = "ID_ADM_DESTINATION_DEPENSE";
	public static final String ID_DESTINATION_DEPENSE_EXERCICE_COLKEY = "ID_ADM_DESTINATION_DEP_EXER";


	// Relationships
	public static final String DESTINATION_DEPENSE_KEY = "destinationDepense";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public Boolean editionBudget() {
    return (Boolean) storedValueForKey(EDITION_BUDGET_KEY);
  }

  public void setEditionBudget(Boolean value) {
    takeStoredValueForKey(value, EDITION_BUDGET_KEY);
  }

  public Boolean saisieBudgetaire() {
    return (Boolean) storedValueForKey(SAISIE_BUDGETAIRE_KEY);
  }

  public void setSaisieBudgetaire(Boolean value) {
    takeStoredValueForKey(value, SAISIE_BUDGETAIRE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense destinationDepense() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense)storedValueForKey(DESTINATION_DEPENSE_KEY);
  }

  public void setDestinationDepenseRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense oldValue = destinationDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DESTINATION_DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DESTINATION_DEPENSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

/**
 * Créer une instance de EODestinationDepenseExercice avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODestinationDepenseExercice createEODestinationDepenseExercice(EOEditingContext editingContext, Boolean editionBudget
, Boolean saisieBudgetaire
, org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense destinationDepense, org.cocktail.fwkcktlgfceos.server.metier.EOExercice exercice			) {
    EODestinationDepenseExercice eo = (EODestinationDepenseExercice) createAndInsertInstance(editingContext, _EODestinationDepenseExercice.ENTITY_NAME);    
		eo.setEditionBudget(editionBudget);
		eo.setSaisieBudgetaire(saisieBudgetaire);
    eo.setDestinationDepenseRelationship(destinationDepense);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

  
	  public EODestinationDepenseExercice localInstanceIn(EOEditingContext editingContext) {
	  		return (EODestinationDepenseExercice)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODestinationDepenseExercice creerInstance(EOEditingContext editingContext) {
	  		EODestinationDepenseExercice object = (EODestinationDepenseExercice)createAndInsertInstance(editingContext, _EODestinationDepenseExercice.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EODestinationDepenseExercice localInstanceIn(EOEditingContext editingContext, EODestinationDepenseExercice eo) {
    EODestinationDepenseExercice localInstance = (eo == null) ? null : (EODestinationDepenseExercice)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODestinationDepenseExercice#localInstanceIn a la place.
   */
	public static EODestinationDepenseExercice localInstanceOf(EOEditingContext editingContext, EODestinationDepenseExercice eo) {
		return EODestinationDepenseExercice.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODestinationDepenseExercice fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODestinationDepenseExercice fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODestinationDepenseExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODestinationDepenseExercice)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODestinationDepenseExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODestinationDepenseExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODestinationDepenseExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODestinationDepenseExercice)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODestinationDepenseExercice fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODestinationDepenseExercice eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODestinationDepenseExercice ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODestinationDepenseExercice fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
