/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrigineRecette.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOOrigineRecette extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_OrigineRecette";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_ORIGINE_RECETTE";

//Attribute Keys
public static final ERXKey<String> ABREVIATION = new ERXKey<String>("abreviation");
public static final ERXKey<String> CODE = new ERXKey<String>("code");
public static final ERXKey<NSTimestamp> FERMETURE = new ERXKey<NSTimestamp>("fermeture");
public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
public static final ERXKey<Integer> ORDRE_AFFICHAGE = new ERXKey<Integer>("ordreAffichage");
public static final ERXKey<NSTimestamp> OUVERTURE = new ERXKey<NSTimestamp>("ouverture");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> TO_ORIGINE_RECETTE_EXERCICES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice>("toOrigineRecetteExercices");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idAdmOrigineRecette";

	public static final String ABREVIATION_KEY = "abreviation";
	public static final String CODE_KEY = "code";
	public static final String FERMETURE_KEY = "fermeture";
	public static final String LIBELLE_KEY = "libelle";
	public static final String ORDRE_AFFICHAGE_KEY = "ordreAffichage";
	public static final String OUVERTURE_KEY = "ouverture";

// Attributs non visibles
	public static final String ID_ADM_ORIGINE_RECETTE_KEY = "idAdmOrigineRecette";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String ABREVIATION_COLKEY = "ABREVIATION";
	public static final String CODE_COLKEY = "CODE";
	public static final String FERMETURE_COLKEY = "FERMETURE";
	public static final String LIBELLE_COLKEY = "LIBELLE";
	public static final String ORDRE_AFFICHAGE_COLKEY = "ORDRE_AFFICHAGE";
	public static final String OUVERTURE_COLKEY = "OUVERTURE";

	public static final String ID_ADM_ORIGINE_RECETTE_COLKEY = "ID_ADM_ORIGINE_RECETTE";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String TO_ORIGINE_RECETTE_EXERCICES_KEY = "toOrigineRecetteExercices";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public String abreviation() {
    return (String) storedValueForKey(ABREVIATION_KEY);
  }

  public void setAbreviation(String value) {
    takeStoredValueForKey(value, ABREVIATION_KEY);
  }

  public String code() {
    return (String) storedValueForKey(CODE_KEY);
  }

  public void setCode(String value) {
    takeStoredValueForKey(value, CODE_KEY);
  }

  public NSTimestamp fermeture() {
    return (NSTimestamp) storedValueForKey(FERMETURE_KEY);
  }

  public void setFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, FERMETURE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public Integer ordreAffichage() {
    return (Integer) storedValueForKey(ORDRE_AFFICHAGE_KEY);
  }

  public void setOrdreAffichage(Integer value) {
    takeStoredValueForKey(value, ORDRE_AFFICHAGE_KEY);
  }

  public NSTimestamp ouverture() {
    return (NSTimestamp) storedValueForKey(OUVERTURE_KEY);
  }

  public void setOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, OUVERTURE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> toOrigineRecetteExercices() {
    return (NSArray)storedValueForKey(TO_ORIGINE_RECETTE_EXERCICES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> toOrigineRecetteExercices(EOQualifier qualifier) {
    return toOrigineRecetteExercices(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> toOrigineRecetteExercices(EOQualifier qualifier, boolean fetch) {
    return toOrigineRecetteExercices(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> toOrigineRecetteExercices(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice.ORIGINE_RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toOrigineRecetteExercices();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToOrigineRecetteExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_ORIGINE_RECETTE_EXERCICES_KEY);
  }

  public void removeFromToOrigineRecetteExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ORIGINE_RECETTE_EXERCICES_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice createToOrigineRecetteExercicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_OrigineRecetteExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_ORIGINE_RECETTE_EXERCICES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice) eo;
  }

  public void deleteToOrigineRecetteExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ORIGINE_RECETTE_EXERCICES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToOrigineRecetteExercicesRelationships() {
    Enumeration objects = toOrigineRecetteExercices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToOrigineRecetteExercicesRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOOrigineRecette avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOrigineRecette createEOOrigineRecette(EOEditingContext editingContext, String abreviation
, String code
, String libelle
, Integer ordreAffichage
, NSTimestamp ouverture
, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat			) {
    EOOrigineRecette eo = (EOOrigineRecette) createAndInsertInstance(editingContext, _EOOrigineRecette.ENTITY_NAME);    
		eo.setAbreviation(abreviation);
		eo.setCode(code);
		eo.setLibelle(libelle);
		eo.setOrdreAffichage(ordreAffichage);
		eo.setOuverture(ouverture);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EOOrigineRecette localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrigineRecette)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrigineRecette creerInstance(EOEditingContext editingContext) {
	  		EOOrigineRecette object = (EOOrigineRecette)createAndInsertInstance(editingContext, _EOOrigineRecette.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOOrigineRecette localInstanceIn(EOEditingContext editingContext, EOOrigineRecette eo) {
    EOOrigineRecette localInstance = (eo == null) ? null : (EOOrigineRecette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOrigineRecette#localInstanceIn a la place.
   */
	public static EOOrigineRecette localInstanceOf(EOEditingContext editingContext, EOOrigineRecette eo) {
		return EOOrigineRecette.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOrigineRecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOrigineRecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrigineRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrigineRecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrigineRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrigineRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrigineRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrigineRecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOrigineRecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrigineRecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrigineRecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrigineRecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
