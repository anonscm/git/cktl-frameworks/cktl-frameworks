/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSValidation;

public class EOExercice extends _EOExercice {
	public static final String EXE_ETAT_OUVERT = "O";
	public static final String EXE_ETAT_CLOS = "C";
	public static final String EXE_ETAT_RESTREINT = "R";
	public static final String EXE_ETAT_PREPARATION = "P";

	public static final String EXE_ETAT_OUVERT_LIBELLE = "Ouvert";
	public static final String EXE_ETAT_CLOS_LIBELLE = "Clos";
	public static final String EXE_ETAT_RESTREINT_LIBELLE = "Restreint";
	public static final String EXE_ETAT_PREPARATION_LIBELLE = "Préparation";

	public static final String EXE_TYPE_COMPTABLE = "C";
	public static final String EXE_TYPE_TRESORERIE = "T";

	public static final String EXE_TYPE_COMPTABLE_LIBELLE = "Comptable";
	public static final String EXE_TYPE_TRESORERIE_LIBELLE = "Trésorerie";

	public static final String EXE_ETAT_ENG_LIBELLE_KEY = "etatEngLibelle";
	public static final String EXE_ETAT_FAC_LIBELLE_KEY = "etatFacLibelle";
	public static final String EXE_ETAT_LIQ_LIBELLE_KEY = "etatLiqLibelle";
	public static final String EXE_ETAT_REC_LIBELLE_KEY = "etatRecLibelle";
	public static final String EXE_ETAT_LIBELLE_KEY = "etatLibelle";
	public static final String EXE_TYPE_LIBELLE_KEY = "typeLibelle";

	public static final EOSortOrdering SORT_EXE_EXERCICE_DESC = EOSortOrdering.sortOrderingWithKey(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending);
	public static final EOQualifier QUAL_EXERCICE_OUVERT = new EOKeyValueQualifier(EOExercice.EXE_STAT_KEY, EOQualifier.QualifierOperatorEqual, EOExercice.EXE_ETAT_OUVERT);
	public static final EOQualifier QUAL_EXERCICE_RESTREINT = new EOKeyValueQualifier(EOExercice.EXE_STAT_KEY, EOQualifier.QualifierOperatorEqual, EOExercice.EXE_ETAT_RESTREINT);
	public static final EOQualifier QUAL_EXERCICE_PREPARATION = new EOKeyValueQualifier(EOExercice.EXE_STAT_KEY, EOQualifier.QualifierOperatorEqual, EOExercice.EXE_ETAT_PREPARATION);
	
	public EOExercice() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 */
	@Override
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	/**
	 * @return Le libellé correspondant au champ exeStat.
	 */
	public final String getEtatLibelle() {
		if (EXE_ETAT_CLOS.equals(this.exeStat())) {
			return EXE_ETAT_CLOS_LIBELLE;
		}
		else if (EXE_ETAT_RESTREINT.equals(this.exeStat())) {
			return EXE_ETAT_RESTREINT_LIBELLE;
		}
		else if (EXE_ETAT_OUVERT.equals(this.exeStat())) {
			return EXE_ETAT_OUVERT_LIBELLE;
		}
		else if (EXE_ETAT_PREPARATION.equals(this.exeStat())) {
			return EXE_ETAT_PREPARATION_LIBELLE;
		}
		else {
			return null;
		}
	}

	public final String getEtatEngLibelle() {
		if (EXE_ETAT_CLOS.equals(this.exeStatEng())) {
			return EXE_ETAT_CLOS_LIBELLE;
		}
		else if (EXE_ETAT_RESTREINT.equals(this.exeStatEng())) {
			return EXE_ETAT_RESTREINT_LIBELLE;
		}
		else if (EXE_ETAT_OUVERT.equals(this.exeStatEng())) {
			return EXE_ETAT_OUVERT_LIBELLE;
		}
		else if (EXE_ETAT_PREPARATION.equals(this.exeStatEng())) {
			return EXE_ETAT_PREPARATION_LIBELLE;
		}
		else {
			return null;
		}
	}

	public final String getEtatFacLibelle() {
		if (EXE_ETAT_CLOS.equals(this.exeStatFac())) {
			return EXE_ETAT_CLOS_LIBELLE;
		}
		else if (EXE_ETAT_RESTREINT.equals(this.exeStatFac())) {
			return EXE_ETAT_RESTREINT_LIBELLE;
		}
		else if (EXE_ETAT_OUVERT.equals(this.exeStatFac())) {
			return EXE_ETAT_OUVERT_LIBELLE;
		}
		else if (EXE_ETAT_PREPARATION.equals(this.exeStatFac())) {
			return EXE_ETAT_PREPARATION_LIBELLE;
		}
		else {
			return null;
		}
	}

	public final String getEtatLiqLibelle() {
		if (EXE_ETAT_CLOS.equals(this.exeStatLiq())) {
			return EXE_ETAT_CLOS_LIBELLE;
		}
		else if (EXE_ETAT_RESTREINT.equals(this.exeStatLiq())) {
			return EXE_ETAT_RESTREINT_LIBELLE;
		}
		else if (EXE_ETAT_OUVERT.equals(this.exeStatLiq())) {
			return EXE_ETAT_OUVERT_LIBELLE;
		}
		else if (EXE_ETAT_PREPARATION.equals(this.exeStatLiq())) {
			return EXE_ETAT_PREPARATION_LIBELLE;
		}
		else {
			return null;
		}
	}

	public final String getEtatRecLibelle() {
		if (EXE_ETAT_CLOS.equals(this.exeStatRec())) {
			return EXE_ETAT_CLOS_LIBELLE;
		}
		else if (EXE_ETAT_RESTREINT.equals(this.exeStatRec())) {
			return EXE_ETAT_RESTREINT_LIBELLE;
		}
		else if (EXE_ETAT_OUVERT.equals(this.exeStatRec())) {
			return EXE_ETAT_OUVERT_LIBELLE;
		}
		else if (EXE_ETAT_PREPARATION.equals(this.exeStatRec())) {
			return EXE_ETAT_PREPARATION_LIBELLE;
		}
		else {
			return null;
		}
	}

	/**
	 * @return Le libelle correspondant au champ exeType.
	 */
	public final String getTypeLibelle() {
		if (EXE_TYPE_COMPTABLE.equals(this.exeType())) {
			return EXE_TYPE_COMPTABLE_LIBELLE;
		}
		else if (EXE_TYPE_TRESORERIE.equals(this.exeType())) {
			return EXE_TYPE_TRESORERIE_LIBELLE;
		}
		else {
			return null;
		}
	}

	public final boolean estEngOuvert() {
		return EXE_ETAT_OUVERT.equals(exeStatEng());
	}

	public final boolean estEngClos() {
		return EXE_ETAT_CLOS.equals(exeStatEng());
	}

	public final boolean estEngRestreint() {
		return EXE_ETAT_RESTREINT.equals(exeStatEng());
	}

	public final boolean estClos() {
		return EXE_ETAT_CLOS.equals(exeStat());
	}

	public final boolean estOuvert() {
		return EXE_ETAT_OUVERT.equals(exeStat());
	}

	public final boolean estRestreint() {
		return EXE_ETAT_RESTREINT.equals(exeStat());
	}

	public final boolean estPreparation() {
		return EXE_ETAT_PREPARATION.equals(exeStat());
	}

	public final boolean estTresorerie() {
		return EXE_TYPE_TRESORERIE.equals(exeType());
	}

	public final boolean estComptable() {
		return EXE_TYPE_COMPTABLE.equals(exeType());
	}

	public final boolean estFacOuvert() {
		return EXE_ETAT_OUVERT.equals(exeStatFac());
	}

	public final boolean estFacClos() {
		return EXE_ETAT_CLOS.equals(exeStatFac());
	}

	public final boolean estFacRestreint() {
		return EXE_ETAT_RESTREINT.equals(exeStatFac());
	}

	public final boolean estLiqOuvert() {
		return EXE_ETAT_OUVERT.equals(exeStatLiq());
	}

	public final boolean estLiqClos() {
		return EXE_ETAT_CLOS.equals(exeStatLiq());
	}

	public final boolean estLiqRestreint() {
		return EXE_ETAT_RESTREINT.equals(exeStatLiq());
	}

	public final boolean estRecOuvert() {
		return EXE_ETAT_OUVERT.equals(exeStatRec());
	}

	public final boolean estRecClos() {
		return EXE_ETAT_CLOS.equals(exeStatRec());
	}

	public final boolean estRecRestreint() {
		return EXE_ETAT_RESTREINT.equals(exeStatRec());
	}

	/**
	 * Fetch l'exercice correspondant a l'exeOrdre <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exeOrdre cle de l'exercice
	 */
	public static EOExercice getExercice(EOEditingContext edc, Integer exeOrdre) {
		return EOExercice.fetchByKeyValue(edc, EOExercice.EXE_EXERCICE_KEY, exeOrdre);
	}

	/**
	 * @param edc
	 * @return L'exercice comptable ouvert.
	 */
	public static EOExercice getExerciceOuvert(EOEditingContext edc) {
		EOQualifier qualOuvert = EOExercice.QUAL_EXERCICE_OUVERT;
		return EOExercice.fetchByQualifier(edc, qualOuvert);
	}

}
