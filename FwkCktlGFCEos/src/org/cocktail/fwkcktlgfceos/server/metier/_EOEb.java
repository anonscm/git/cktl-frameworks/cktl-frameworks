/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEb.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOEb extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_Eb";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_EB";

//Attribute Keys
public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
public static final ERXKey<String> ORG_CR = new ERXKey<String>("orgCr");
public static final ERXKey<String> ORG_ETAB = new ERXKey<String>("orgEtab");
public static final ERXKey<String> ORG_LIBELLE = new ERXKey<String>("orgLibelle");
public static final ERXKey<Integer> ORG_NIVEAU = new ERXKey<Integer>("orgNiveau");
public static final ERXKey<String> ORG_SOUSCR = new ERXKey<String>("orgSouscr");
public static final ERXKey<String> ORG_UB = new ERXKey<String>("orgUb");
public static final ERXKey<String> ORG_UNIV = new ERXKey<String>("orgUniv");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice> ORGAN_EXERCICE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice>("organExercice");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN_FILS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organFils");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN_PERE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organPere");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata> ORGAN_PRORATAS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata>("organProratas");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> ORGAN_SIGNATAIRES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire>("organSignataires");
public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structure");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEb> TYPE_ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEb>("typeOrgan");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> UTILISATEUR_ORGANS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb>("utilisateurOrgans");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "IdAdmEb";

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String ORG_CR_KEY = "orgCr";
	public static final String ORG_ETAB_KEY = "orgEtab";
	public static final String ORG_LIBELLE_KEY = "orgLibelle";
	public static final String ORG_NIVEAU_KEY = "orgNiveau";
	public static final String ORG_SOUSCR_KEY = "orgSouscr";
	public static final String ORG_UB_KEY = "orgUb";
	public static final String ORG_UNIV_KEY = "orgUniv";

// Attributs non visibles
	public static final String ID_ADM_EB_KEY = "IdAdmEb";
	public static final String LOG_ORDRE_KEY = "logOrdre";
	public static final String ORG_PERE_KEY = "orgPere";
	public static final String TYOR_ID_KEY = "tyorId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String ORG_CR_COLKEY = "ORG_CR";
	public static final String ORG_ETAB_COLKEY = "ORG_ETAB";
	public static final String ORG_LIBELLE_COLKEY = "ORG_LIB";
	public static final String ORG_NIVEAU_COLKEY = "ORG_NIV";
	public static final String ORG_SOUSCR_COLKEY = "ORG_SOUSCR";
	public static final String ORG_UB_COLKEY = "ORG_UB";
	public static final String ORG_UNIV_COLKEY = "ORG_UNIV";

	public static final String ID_ADM_EB_COLKEY = "ID_ADM_EB";
	public static final String LOG_ORDRE_COLKEY = "LOG_ORDRE";
	public static final String ORG_PERE_COLKEY = "ORG_PERE";
	public static final String TYOR_ID_COLKEY = "TYOR_ID";


	// Relationships
	public static final String ORGAN_EXERCICE_KEY = "organExercice";
	public static final String ORGAN_FILS_KEY = "organFils";
	public static final String ORGAN_PERE_KEY = "organPere";
	public static final String ORGAN_PRORATAS_KEY = "organProratas";
	public static final String ORGAN_SIGNATAIRES_KEY = "organSignataires";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TYPE_ORGAN_KEY = "typeOrgan";
	public static final String UTILISATEUR_ORGANS_KEY = "utilisateurOrgans";



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public String orgCr() {
    return (String) storedValueForKey(ORG_CR_KEY);
  }

  public void setOrgCr(String value) {
    takeStoredValueForKey(value, ORG_CR_KEY);
  }

  public String orgEtab() {
    return (String) storedValueForKey(ORG_ETAB_KEY);
  }

  public void setOrgEtab(String value) {
    takeStoredValueForKey(value, ORG_ETAB_KEY);
  }

  public String orgLibelle() {
    return (String) storedValueForKey(ORG_LIBELLE_KEY);
  }

  public void setOrgLibelle(String value) {
    takeStoredValueForKey(value, ORG_LIBELLE_KEY);
  }

  public Integer orgNiveau() {
    return (Integer) storedValueForKey(ORG_NIVEAU_KEY);
  }

  public void setOrgNiveau(Integer value) {
    takeStoredValueForKey(value, ORG_NIVEAU_KEY);
  }

  public String orgSouscr() {
    return (String) storedValueForKey(ORG_SOUSCR_KEY);
  }

  public void setOrgSouscr(String value) {
    takeStoredValueForKey(value, ORG_SOUSCR_KEY);
  }

  public String orgUb() {
    return (String) storedValueForKey(ORG_UB_KEY);
  }

  public void setOrgUb(String value) {
    takeStoredValueForKey(value, ORG_UB_KEY);
  }

  public String orgUniv() {
    return (String) storedValueForKey(ORG_UNIV_KEY);
  }

  public void setOrgUniv(String value) {
    takeStoredValueForKey(value, ORG_UNIV_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organPere() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(ORGAN_PERE_KEY);
  }

  public void setOrganPereRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure structure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEb typeOrgan() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEb)storedValueForKey(TYPE_ORGAN_KEY);
  }

  public void setTypeOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEb value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEb oldValue = typeOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ORGAN_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice> organExercice() {
    return (NSArray)storedValueForKey(ORGAN_EXERCICE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice> organExercice(EOQualifier qualifier) {
    return organExercice(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice> organExercice(EOQualifier qualifier, boolean fetch) {
    return organExercice(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice> organExercice(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organExercice();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
  }

  public void removeFromOrganExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice createOrganExerciceRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_EbExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_EXERCICE_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice) eo;
  }

  public void deleteOrganExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganExerciceRelationships() {
    Enumeration objects = organExercice().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganExerciceRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> organFils() {
    return (NSArray)storedValueForKey(ORGAN_FILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> organFils(EOQualifier qualifier) {
    return organFils(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> organFils(EOQualifier qualifier, boolean fetch) {
    return organFils(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> organFils(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOEb.ORGAN_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOEb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
  }

  public void removeFromOrganFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOEb createOrganFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_Eb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_FILS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb) eo;
  }

  public void deleteOrganFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganFilsRelationships() {
    Enumeration objects = organFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganFilsRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOEb)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata> organProratas() {
    return (NSArray)storedValueForKey(ORGAN_PRORATAS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata> organProratas(EOQualifier qualifier) {
    return organProratas(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata> organProratas(EOQualifier qualifier, boolean fetch) {
    return organProratas(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata> organProratas(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organProratas();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganProratasRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
  }

  public void removeFromOrganProratasRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata createOrganProratasRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_EbProrata");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_PRORATAS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata) eo;
  }

  public void deleteOrganProratasRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganProratasRelationships() {
    Enumeration objects = organProratas().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganProratasRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOEbProrata)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> organSignataires() {
    return (NSArray)storedValueForKey(ORGAN_SIGNATAIRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> organSignataires(EOQualifier qualifier) {
    return organSignataires(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> organSignataires(EOQualifier qualifier, boolean fetch) {
    return organSignataires(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> organSignataires(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organSignataires();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganSignatairesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
  }

  public void removeFromOrganSignatairesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire createOrganSignatairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_EbSignataire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_SIGNATAIRES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire) eo;
  }

  public void deleteOrganSignatairesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganSignatairesRelationships() {
    Enumeration objects = organSignataires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganSignatairesRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans() {
    return (NSArray)storedValueForKey(UTILISATEUR_ORGANS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans(EOQualifier qualifier) {
    return utilisateurOrgans(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans(EOQualifier qualifier, boolean fetch) {
    return utilisateurOrgans(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> utilisateurOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public void removeFromUtilisateurOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb createUtilisateurOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_UtilisateurEb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_ORGANS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb) eo;
  }

  public void deleteUtilisateurOrgansRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurOrgansRelationships() {
    Enumeration objects = utilisateurOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurOrgansRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurEb)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEb avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEb createEOEb(EOEditingContext editingContext, String orgLibelle
, Integer orgNiveau
, String orgUniv
, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEb typeOrgan			) {
    EOEb eo = (EOEb) createAndInsertInstance(editingContext, _EOEb.ENTITY_NAME);    
		eo.setOrgLibelle(orgLibelle);
		eo.setOrgNiveau(orgNiveau);
		eo.setOrgUniv(orgUniv);
    eo.setTypeOrganRelationship(typeOrgan);
    return eo;
  }

  
	  public EOEb localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEb)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEb creerInstance(EOEditingContext editingContext) {
	  		EOEb object = (EOEb)createAndInsertInstance(editingContext, _EOEb.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOEb localInstanceIn(EOEditingContext editingContext, EOEb eo) {
    EOEb localInstance = (eo == null) ? null : (EOEb)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEb#localInstanceIn a la place.
   */
	public static EOEb localInstanceOf(EOEditingContext editingContext, EOEb eo) {
		return EOEb.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEb>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEb fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEb fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEb eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEb)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEb fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEb fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEb eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEb)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEb fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEb eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEb ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEb fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
