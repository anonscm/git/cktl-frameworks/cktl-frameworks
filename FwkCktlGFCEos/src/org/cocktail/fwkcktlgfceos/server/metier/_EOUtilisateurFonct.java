/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateurFonct.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;



public abstract class _EOUtilisateurFonct extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkJefyAdmin_UtilisateurFonct";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.UTILISATEUR_FONCT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ufOrdre";

	public static final String FON_ORDRE_KEY = "fonOrdre";

// Attributs non visibles
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UF_ORDRE_KEY = "ufOrdre";

//Colonnes dans la base de donnees
	public static final String FON_ORDRE_COLKEY = "FON_ORDRE";

	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String UF_ORDRE_COLKEY = "UF_ORDRE";


	// Relationships
	public static final String FONCTION_KEY = "fonction";
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String UTILISATEUR_FONCT_EXERCICES_KEY = "utilisateurFonctExercices";
	public static final String UTILISATEUR_FONCT_GESTIONS_KEY = "utilisateurFonctGestions";



	// Accessors methods
  public Double fonOrdre() {
    return (Double) storedValueForKey(FON_ORDRE_KEY);
  }

  public void setFonOrdre(Double value) {
    takeStoredValueForKey(value, FON_ORDRE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOFonction fonction() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOFonction)storedValueForKey(FONCTION_KEY);
  }

  public void setFonctionRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOFonction value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOFonction oldValue = fonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FONCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FONCTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray utilisateurFonctExercices() {
    return (NSArray)storedValueForKey(UTILISATEUR_FONCT_EXERCICES_KEY);
  }

  public NSArray utilisateurFonctExercices(EOQualifier qualifier) {
    return utilisateurFonctExercices(qualifier, null, false);
  }

  public NSArray utilisateurFonctExercices(EOQualifier qualifier, boolean fetch) {
    return utilisateurFonctExercices(qualifier, null, fetch);
  }

  public NSArray utilisateurFonctExercices(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice.UTILISATEUR_FONCT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurFonctExercices();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurFonctExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCT_EXERCICES_KEY);
  }

  public void removeFromUtilisateurFonctExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCT_EXERCICES_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice createUtilisateurFonctExercicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_UtilisateurFonctExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_FONCT_EXERCICES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice) eo;
  }

  public void deleteUtilisateurFonctExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCT_EXERCICES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurFonctExercicesRelationships() {
    Enumeration objects = utilisateurFonctExercices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurFonctExercicesRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctExercice)objects.nextElement());
    }
  }

  public NSArray utilisateurFonctGestions() {
    return (NSArray)storedValueForKey(UTILISATEUR_FONCT_GESTIONS_KEY);
  }

  public NSArray utilisateurFonctGestions(EOQualifier qualifier) {
    return utilisateurFonctGestions(qualifier, null, false);
  }

  public NSArray utilisateurFonctGestions(EOQualifier qualifier, boolean fetch) {
    return utilisateurFonctGestions(qualifier, null, fetch);
  }

  public NSArray utilisateurFonctGestions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion.UTILISATEUR_FONCT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurFonctGestions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurFonctGestionsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCT_GESTIONS_KEY);
  }

  public void removeFromUtilisateurFonctGestionsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCT_GESTIONS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion createUtilisateurFonctGestionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_UtilisateurFonctGestion");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_FONCT_GESTIONS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion) eo;
  }

  public void deleteUtilisateurFonctGestionsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCT_GESTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurFonctGestionsRelationships() {
    Enumeration objects = utilisateurFonctGestions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurFonctGestionsRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctGestion)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOUtilisateurFonct avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOUtilisateurFonct createEOUtilisateurFonct(EOEditingContext editingContext, Double fonOrdre
, org.cocktail.fwkcktlgfceos.server.metier.EOFonction fonction, org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur utilisateur			) {
    EOUtilisateurFonct eo = (EOUtilisateurFonct) createAndInsertInstance(editingContext, _EOUtilisateurFonct.ENTITY_NAME);    
		eo.setFonOrdre(fonOrdre);
    eo.setFonctionRelationship(fonction);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOUtilisateurFonct localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateurFonct)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateurFonct creerInstance(EOEditingContext editingContext) {
	  		EOUtilisateurFonct object = (EOUtilisateurFonct)createAndInsertInstance(editingContext, _EOUtilisateurFonct.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOUtilisateurFonct localInstanceIn(EOEditingContext editingContext, EOUtilisateurFonct eo) {
    EOUtilisateurFonct localInstance = (eo == null) ? null : (EOUtilisateurFonct)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOUtilisateurFonct#localInstanceIn a la place.
   */
	public static EOUtilisateurFonct localInstanceOf(EOEditingContext editingContext, EOUtilisateurFonct eo) {
		return EOUtilisateurFonct.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateurFonct fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateurFonct fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurFonct eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurFonct)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurFonct fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurFonct fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurFonct eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurFonct)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateurFonct fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurFonct eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurFonct ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurFonct fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
