/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEbSignataire.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EOEbSignataire extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_EbSignataire";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_EB_SIGNATAIRE";

//Attribute Keys
public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
public static final ERXKey<NSTimestamp> ORSI_DATE_CLOTURE = new ERXKey<NSTimestamp>("orsiDateCloture");
public static final ERXKey<NSTimestamp> ORSI_DATE_OUVERTURE = new ERXKey<NSTimestamp>("orsiDateOuverture");
public static final ERXKey<String> ORSI_LIBELLE_SIGNATAIRE = new ERXKey<String>("orsiLibelleSignataire");
public static final ERXKey<String> ORSI_REFERENCE_DELEGATION = new ERXKey<String>("orsiReferenceDelegation");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organ");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc> ORGAN_SIGNATAIRE_TCS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc>("organSignataireTcs");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeSignature> TYPE_SIGNATURE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeSignature>("typeSignature");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "orsiId";

	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String ORSI_DATE_CLOTURE_KEY = "orsiDateCloture";
	public static final String ORSI_DATE_OUVERTURE_KEY = "orsiDateOuverture";
	public static final String ORSI_LIBELLE_SIGNATAIRE_KEY = "orsiLibelleSignataire";
	public static final String ORSI_REFERENCE_DELEGATION_KEY = "orsiReferenceDelegation";

// Attributs non visibles
	public static final String ID_ADM_EB_KEY = "idAdmEb";
	public static final String ORSI_ID_KEY = "orsiId";
	public static final String TYSI_ID_KEY = "tysiId";

//Colonnes dans la base de donnees
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String ORSI_DATE_CLOTURE_COLKEY = "ORSI_DATE_CLOTURE";
	public static final String ORSI_DATE_OUVERTURE_COLKEY = "ORSI_DATE_OUVERTURE";
	public static final String ORSI_LIBELLE_SIGNATAIRE_COLKEY = "ORSI_LIBELLE_SIGNATAIRE";
	public static final String ORSI_REFERENCE_DELEGATION_COLKEY = "ORSI_REFERENCE_DELEGATION";

	public static final String ID_ADM_EB_COLKEY = "id_adm_eb";
	public static final String ORSI_ID_COLKEY = "ORSI_ID";
	public static final String TYSI_ID_COLKEY = "TYSI_ID";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String ORGAN_KEY = "organ";
	public static final String ORGAN_SIGNATAIRE_TCS_KEY = "organSignataireTcs";
	public static final String TYPE_SIGNATURE_KEY = "typeSignature";



	// Accessors methods
  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public NSTimestamp orsiDateCloture() {
    return (NSTimestamp) storedValueForKey(ORSI_DATE_CLOTURE_KEY);
  }

  public void setOrsiDateCloture(NSTimestamp value) {
    takeStoredValueForKey(value, ORSI_DATE_CLOTURE_KEY);
  }

  public NSTimestamp orsiDateOuverture() {
    return (NSTimestamp) storedValueForKey(ORSI_DATE_OUVERTURE_KEY);
  }

  public void setOrsiDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, ORSI_DATE_OUVERTURE_KEY);
  }

  public String orsiLibelleSignataire() {
    return (String) storedValueForKey(ORSI_LIBELLE_SIGNATAIRE_KEY);
  }

  public void setOrsiLibelleSignataire(String value) {
    takeStoredValueForKey(value, ORSI_LIBELLE_SIGNATAIRE_KEY);
  }

  public String orsiReferenceDelegation() {
    return (String) storedValueForKey(ORSI_REFERENCE_DELEGATION_KEY);
  }

  public void setOrsiReferenceDelegation(String value) {
    takeStoredValueForKey(value, ORSI_REFERENCE_DELEGATION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organ() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeSignature typeSignature() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeSignature)storedValueForKey(TYPE_SIGNATURE_KEY);
  }

  public void setTypeSignatureRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeSignature value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeSignature oldValue = typeSignature();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_SIGNATURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_SIGNATURE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc> organSignataireTcs() {
    return (NSArray)storedValueForKey(ORGAN_SIGNATAIRE_TCS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc> organSignataireTcs(EOQualifier qualifier) {
    return organSignataireTcs(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc> organSignataireTcs(EOQualifier qualifier, boolean fetch) {
    return organSignataireTcs(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc> organSignataireTcs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc.ORGAN_SIGNATAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organSignataireTcs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganSignataireTcsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRE_TCS_KEY);
  }

  public void removeFromOrganSignataireTcsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRE_TCS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc createOrganSignataireTcsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_EbSignataireTc");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_SIGNATAIRE_TCS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc) eo;
  }

  public void deleteOrganSignataireTcsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRE_TCS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganSignataireTcsRelationships() {
    Enumeration objects = organSignataireTcs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganSignataireTcsRelationship((org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataireTc)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEbSignataire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEbSignataire createEOEbSignataire(EOEditingContext editingContext, Integer noIndividu
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu, org.cocktail.fwkcktlgfceos.server.metier.EOEb organ, org.cocktail.fwkcktlgfceos.server.metier.EOTypeSignature typeSignature			) {
    EOEbSignataire eo = (EOEbSignataire) createAndInsertInstance(editingContext, _EOEbSignataire.ENTITY_NAME);    
		eo.setNoIndividu(noIndividu);
    eo.setIndividuRelationship(individu);
    eo.setOrganRelationship(organ);
    eo.setTypeSignatureRelationship(typeSignature);
    return eo;
  }

  
	  public EOEbSignataire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEbSignataire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEbSignataire creerInstance(EOEditingContext editingContext) {
	  		EOEbSignataire object = (EOEbSignataire)createAndInsertInstance(editingContext, _EOEbSignataire.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EOEbSignataire localInstanceIn(EOEditingContext editingContext, EOEbSignataire eo) {
    EOEbSignataire localInstance = (eo == null) ? null : (EOEbSignataire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEbSignataire#localInstanceIn a la place.
   */
	public static EOEbSignataire localInstanceOf(EOEditingContext editingContext, EOEbSignataire eo) {
		return EOEbSignataire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOEbSignataire>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEbSignataire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEbSignataire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEbSignataire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEbSignataire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEbSignataire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEbSignataire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEbSignataire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEbSignataire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEbSignataire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEbSignataire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEbSignataire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEbSignataire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
