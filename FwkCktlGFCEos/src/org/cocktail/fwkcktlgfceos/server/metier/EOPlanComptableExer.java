/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOPlanComptableExer extends _EOPlanComptableExer {

	public static final String PCONUM_ET_LIBELLE_KEY = "pcoNumEtLibelle";
	public static final String etatAnnule = "ANNULE";
	public static final String etatValide = "VALIDE";

	public EOPlanComptableExer() {
		super();
	}

	public String pcoNumEtLibelle() {
		return pcoNum() + " " + pcoLibelle();
	}

	public static EOPlanComptableExer fetchWithPcoNumAndExercice(EOEditingContext ec, String pcoNum, int exo) {
		EOQualifier qual = ERXQ.equals(PCO_NUM_KEY, pcoNum).and(ERXQ.equals(EXE_ORDRE_KEY, exo));
		return fetchFirstByQualifier(ec, qual);
	}

	public static EOPlanComptableExer getCompte(EOEditingContext editingContext, EOExercice exercice, String pcoNum) {
		EOQualifier qual1 = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		EOQualifier qual2 = new EOKeyValueQualifier(EOPlanComptableExer.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return EOPlanComptableExer.fetchByQualifier(editingContext, new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		})));
	}

	@SuppressWarnings("unchecked")
	public static NSArray<EOPlanComptableExer> fetchWithExercice(EOEditingContext ec, EOExercice exercice, String type) {
		EOQualifier qual = ERXQ.equals(EXERCICE_KEY, exercice).and(ERXQ.equals(PCO_VALIDITE_KEY, etatValide));
		if (type != null) {
			qual = ERXQ.and(qual, ERXQ.equals(PCO_NATURE_KEY, type));
		}
		return fetchAll(ec, qual, ERXS.asc(EOPlanComptable.PCO_NUM_KEY).array());
	}

	public boolean isValide() {
		return etatValide.equals(pcoValidite());
	}
}
