/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODestinationDepense.java instead.
package org.cocktail.fwkcktlgfceos.server.metier;

import org.cocktail.fwkcktlgfceos.server.metier.AfwkJefyAdminRecord;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


@SuppressWarnings("all")
public abstract class _EODestinationDepense extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkGFCEos_DestinationDepense";
	public static final String ENTITY_TABLE_NAME = "GFC.ADM_DESTINATION_DEPENSE";

//Attribute Keys
public static final ERXKey<String> ABREVIATION = new ERXKey<String>("abreviation");
public static final ERXKey<String> CODE = new ERXKey<String>("code");
public static final ERXKey<NSTimestamp> FERMETURE = new ERXKey<NSTimestamp>("fermeture");
public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
public static final ERXKey<Integer> ORDRE_AFFICHAGE = new ERXKey<Integer>("ordreAffichage");
public static final ERXKey<NSTimestamp> OUVERTURE = new ERXKey<NSTimestamp>("ouverture");

// Relationship Keys
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> DESTINATION_FILS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>("destinationFils");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> DESTINATION_PERE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>("destinationPere");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseType> DESTINATION_TYPE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseType>("destinationType");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> TO_DESTINATION_DEPENSE_EXERCICES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice>("toDestinationDepenseExercices");
public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idDestinationDepense";

	public static final String ABREVIATION_KEY = "abreviation";
	public static final String CODE_KEY = "code";
	public static final String FERMETURE_KEY = "fermeture";
	public static final String LIBELLE_KEY = "libelle";
	public static final String NIVEAU_KEY = "niveau";
	public static final String ORDRE_AFFICHAGE_KEY = "ordreAffichage";
	public static final String OUVERTURE_KEY = "ouverture";

// Attributs non visibles
	public static final String ID_DESTINATION_DEPENSE_KEY = "idDestinationDepense";
	public static final String PERE_KEY = "pere";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYPE_KEY = "type";

//Colonnes dans la base de donnees
	public static final String ABREVIATION_COLKEY = "ABREVIATION";
	public static final String CODE_COLKEY = "CODE";
	public static final String FERMETURE_COLKEY = "FERMETURE";
	public static final String LIBELLE_COLKEY = "LIBELLE";
	public static final String NIVEAU_COLKEY = "NIVEAU";
	public static final String ORDRE_AFFICHAGE_COLKEY = "ORDRE_AFFICHAGE";
	public static final String OUVERTURE_COLKEY = "OUVERTURE";

	public static final String ID_DESTINATION_DEPENSE_COLKEY = "ID_ADM_DESTINATION_DEPENSE";
	public static final String PERE_COLKEY = "PERE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYPE_COLKEY = "TYPE";


	// Relationships
	public static final String DESTINATION_FILS_KEY = "destinationFils";
	public static final String DESTINATION_PERE_KEY = "destinationPere";
	public static final String DESTINATION_TYPE_KEY = "destinationType";
	public static final String TO_DESTINATION_DEPENSE_EXERCICES_KEY = "toDestinationDepenseExercices";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public String abreviation() {
    return (String) storedValueForKey(ABREVIATION_KEY);
  }

  public void setAbreviation(String value) {
    takeStoredValueForKey(value, ABREVIATION_KEY);
  }

  public String code() {
    return (String) storedValueForKey(CODE_KEY);
  }

  public void setCode(String value) {
    takeStoredValueForKey(value, CODE_KEY);
  }

  public NSTimestamp fermeture() {
    return (NSTimestamp) storedValueForKey(FERMETURE_KEY);
  }

  public void setFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, FERMETURE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    takeStoredValueForKey(value, NIVEAU_KEY);
  }

  public Integer ordreAffichage() {
    return (Integer) storedValueForKey(ORDRE_AFFICHAGE_KEY);
  }

  public void setOrdreAffichage(Integer value) {
    takeStoredValueForKey(value, ORDRE_AFFICHAGE_KEY);
  }

  public NSTimestamp ouverture() {
    return (NSTimestamp) storedValueForKey(OUVERTURE_KEY);
  }

  public void setOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, OUVERTURE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense destinationPere() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense)storedValueForKey(DESTINATION_PERE_KEY);
  }

  public void setDestinationPereRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense oldValue = destinationPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DESTINATION_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DESTINATION_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseType destinationType() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseType)storedValueForKey(DESTINATION_TYPE_KEY);
  }

  public void setDestinationTypeRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseType value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseType oldValue = destinationType();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DESTINATION_TYPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DESTINATION_TYPE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> destinationFils() {
    return (NSArray)storedValueForKey(DESTINATION_FILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> destinationFils(EOQualifier qualifier) {
    return destinationFils(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> destinationFils(EOQualifier qualifier, boolean fetch) {
    return destinationFils(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> destinationFils(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense.DESTINATION_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = destinationFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDestinationFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DESTINATION_FILS_KEY);
  }

  public void removeFromDestinationFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DESTINATION_FILS_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense createDestinationFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_DestinationDepense");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DESTINATION_FILS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense) eo;
  }

  public void deleteDestinationFilsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DESTINATION_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDestinationFilsRelationships() {
    Enumeration objects = destinationFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDestinationFilsRelationship((org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> toDestinationDepenseExercices() {
    return (NSArray)storedValueForKey(TO_DESTINATION_DEPENSE_EXERCICES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> toDestinationDepenseExercices(EOQualifier qualifier) {
    return toDestinationDepenseExercices(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> toDestinationDepenseExercices(EOQualifier qualifier, boolean fetch) {
    return toDestinationDepenseExercices(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> toDestinationDepenseExercices(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice.DESTINATION_DEPENSE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDestinationDepenseExercices();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDestinationDepenseExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_DESTINATION_DEPENSE_EXERCICES_KEY);
  }

  public void removeFromToDestinationDepenseExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DESTINATION_DEPENSE_EXERCICES_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice createToDestinationDepenseExercicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGFCEos_DestinationDepenseExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_DESTINATION_DEPENSE_EXERCICES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice) eo;
  }

  public void deleteToDestinationDepenseExercicesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DESTINATION_DEPENSE_EXERCICES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToDestinationDepenseExercicesRelationships() {
    Enumeration objects = toDestinationDepenseExercices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDestinationDepenseExercicesRelationship((org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice)objects.nextElement());
    }
  }


/**
 * Créer une instance de EODestinationDepense avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODestinationDepense createEODestinationDepense(EOEditingContext editingContext, String abreviation
, String code
, String libelle
, Integer niveau
, Integer ordreAffichage
, NSTimestamp ouverture
, org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseType destinationType, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat			) {
    EODestinationDepense eo = (EODestinationDepense) createAndInsertInstance(editingContext, _EODestinationDepense.ENTITY_NAME);    
		eo.setAbreviation(abreviation);
		eo.setCode(code);
		eo.setLibelle(libelle);
		eo.setNiveau(niveau);
		eo.setOrdreAffichage(ordreAffichage);
		eo.setOuverture(ouverture);
    eo.setDestinationTypeRelationship(destinationType);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EODestinationDepense localInstanceIn(EOEditingContext editingContext) {
	  		return (EODestinationDepense)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODestinationDepense creerInstance(EOEditingContext editingContext) {
	  		EODestinationDepense object = (EODestinationDepense)createAndInsertInstance(editingContext, _EODestinationDepense.ENTITY_NAME);
	  		return object;
		}

	
	
  
  public static EODestinationDepense localInstanceIn(EOEditingContext editingContext, EODestinationDepense eo) {
    EODestinationDepense localInstance = (eo == null) ? null : (EODestinationDepense)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODestinationDepense#localInstanceIn a la place.
   */
	public static EODestinationDepense localInstanceOf(EOEditingContext editingContext, EODestinationDepense eo) {
		return EODestinationDepense.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> eoObjects = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODestinationDepense fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODestinationDepense fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODestinationDepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODestinationDepense)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODestinationDepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODestinationDepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODestinationDepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODestinationDepense)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODestinationDepense fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODestinationDepense eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODestinationDepense ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODestinationDepense fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
