/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfceos.server.metier;

import java.util.Date;

import org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOUtilisateur extends _EOUtilisateur {

	private static final String VIDE = "";
	private static final String SPACE = " ";
	public static final String ETAT_VALIDE = EOTypeEtat.ETAT_VALIDE;
	public static final String ETAT_SUPPRIME = EOTypeEtat.ETAT_SUPPRIME;
	public static final String UTL_NOM_KEY = EOUtilisateur.PERSONNE_KEY + "." + EOPersonne.PERS_LIBELLE_KEY;
	public static final String UTL_NOM_PATRONYMIQUE_KEY = EOUtilisateur.PERSONNE_KEY + "." + EOPersonne.PERS_NOMPTR_KEY;
	public static final String UTL_PRENOM_KEY = EOUtilisateur.PERSONNE_KEY + "." + EOPersonne.PERS_LC_KEY;
	public static final String UTL_NOM_PRENOM_KEY = "nomAndPrenom";
	public static final String UTL_PRENOM_NOM_KEY = "prenomAndNom";
	public static final String UTL_NOM_NOMPATRO_PRENOM_KEY = "nomAndNomPatronymiqueAndPrenom";
	public static final String UTL_PRENOM_NOM_NOMPATRO_KEY = "prenomAndNomAndNomPatronymique";

	public static final String TYPE_UTILISATEUR_KEY = "typeUtilisateur";
	public static final String TYPE_UTILISATEUR_INDIVIDU = "I";
	public static final String TYPE_UTILISATEUR_STRUCTURE = "S";

	//    public static final EOSortOrdering SORT_UTL_NOM_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EOUtilisateur.UTL_NOM_KEY, EOSortOrdering.CompareAscending);
	//    public static final EOSortOrdering SORT_UTL_PRENOM_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EOUtilisateur.UTL_PRENOM_KEY, EOSortOrdering.CompareAscending);

	public static final EOSortOrdering SORT_UTL_NOM_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EOUtilisateur.UTL_NOM_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_UTL_PRENOM_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EOUtilisateur.UTL_PRENOM_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_TYPE_UTILISATEUR_ASC = EOSortOrdering.sortOrderingWithKey(EOUtilisateur.TYPE_UTILISATEUR_KEY, EOSortOrdering.CompareAscending);

	public static final EOQualifier QUALIFIER_UTILISATEUR_VALIDE = EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(EOUtilisateur.ETAT_VALIDE));
	public static final EOQualifier QUALIFIER_UTILISATEUR_NON_VALIDE = EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "<>%@", new NSArray(EOUtilisateur.ETAT_VALIDE));

	private String utlNomCache = null;
	private String utlPrenomCache = null;
	private String typeUtilisateurCache = null;
	private String utlEmail = null;
	private String utlLogin = null;
	private String utlNomPatronymiqueCache = null;

	public EOUtilisateur() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public String getNomAndPrenom() {
		return utlNom() + (utlPrenom() != null ? SPACE + utlPrenom() : VIDE);
	}

	public String getPrenomAndNom() {
		return (utlPrenom() != null ? utlPrenom() + SPACE : VIDE) + utlNom();
	}

	public String getNomAndNomPatronymiqueAndPrenom() {
		return utlNom() + (utlNomPatronymique().length() > 0 ? SPACE + "(" + utlNomPatronymique() + ")" : VIDE) + (utlPrenom() != null ? SPACE + utlPrenom() : VIDE);
	}

	public String getPrenomAndNomAndNomPatronymique() {
		return (utlPrenom() != null ? utlPrenom() + SPACE : VIDE) + utlNom() + (utlNomPatronymique().length() > 0 ? SPACE + utlNomPatronymique() : VIDE);
	}

	public String getEmail() {
		if (utlEmail == null) {
			utlEmail = (utilisateurInfo() != null ? utilisateurInfo().email() : null);
		}
		return utlEmail;
	}

	public String getLogin() {
		if (utlLogin == null) {
			utlLogin = (utilisateurInfo() != null ? utilisateurInfo().cptLogin() : null);
		}
		return utlLogin;
	}

	public String utlNom() {
		if (utlNomCache == null) {
			utlNomCache = personne().persLibelle();
		}
		return utlNomCache;
	}

	public String utlNomPatronymique() {
		if (utlNomPatronymiqueCache == null) {
			utlNomPatronymiqueCache = (personne().persNomptr());
		}
		return utlNomPatronymiqueCache;
	}

	public String utlPrenom() {
		if (utlPrenomCache == null) {
			utlPrenomCache = StringCtrl.capitalizedWords(personne().persLc(), null);
		}
		return utlPrenomCache;
	}

	/**
	 * Indique si l'utilisateur est relie a  un individu ou pas.
	 * 
	 * @return
	 */
	public final boolean isIndividu() {
		return (individu() != null);
	}

	/**
	 * @return le type utilisateur (I pour individu ou S pour structure)
	 */
	public final String typeUtilisateur() {
		if (typeUtilisateurCache == null) {
			if (isIndividu()) {
				typeUtilisateurCache = TYPE_UTILISATEUR_INDIVIDU;
			}
			else {
				typeUtilisateurCache = TYPE_UTILISATEUR_STRUCTURE;
			}
		}
		return typeUtilisateurCache;
	}

	/**
	 * @param exercice Si nul, ne tient pas compte de l'exercice.
	 * @return un tableau des ORGAN associées a l'utilisateur. Se base sur utilisateurOrgans().
	 */
	public NSArray getOrgans(final EOExercice exercice) {
		EOQualifier qual = null;
		NSArray res = (NSArray) valueForKeyPath(EOUtilisateur.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurEb.ORGAN_KEY);
		if (exercice != null) {
			final Date dateDebut = DateCtrl.getFirstDayOfYear(exercice.exeExercice().intValue());
			final Date dateFin = DateCtrl.addDHMS(DateCtrl.getLastDayOfYear(exercice.exeExercice().intValue()), 1, 0, 0, 0);
			qual = EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXERCICE).dot(EOExercice.EXE_OUVERTURE).isNull()
					.or(EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXERCICE).dot(EOExercice.EXE_OUVERTURE).lessThan(new NSTimestamp(dateDebut)))
					.and(EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXERCICE).dot(EOExercice.EXE_CLOTURE).isNull()
					.or(EOEb.ORGAN_EXERCICE.dot(EOEbExercice.EXERCICE).dot(EOExercice.EXE_CLOTURE).greaterThanOrEqualTo(new NSTimestamp(dateFin))));
			res = EOQualifier.filteredArrayWithQualifier(res, qual);
		}
		return res;
	}

	public boolean isFonctionAutorisee(String fonIdInterne) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonct.FONCTION_KEY + "." + EOFonction.FON_ID_INTERNE_KEY + "=%@", new NSArray(new Object[] {
				fonIdInterne
		}));
		if (EOQualifier.filteredArrayWithQualifier(utilisateurFonctions(), qual).count() > 0) {
			return true;
		}
		return false;
	}

	public boolean isValide() {
		if (typeEtat() == null)
			return false;
		if (typeEtat().tyetLibelle() == null || !typeEtat().tyetLibelle().equals(ETAT_VALIDE))
			return false;
		if (utlOuverture() == null)
			return false;
		if (DateCtrl.isAfter(utlOuverture(), new NSTimestamp()))
			return false;
		if (utlFermeture() != null && DateCtrl.isBefore(utlFermeture(), new NSTimestamp()))
			return false;
		return true;
	}

	public static EOQualifier buildStrSrchQualifier(String s) {
		return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.UTL_NOM_PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(new Object[] {
				s
		}));
	}

	/**
	 * @param date (si null, la date du jour est utilisee).
	 * @return True si la date est comprise entre utlOuverture et utlFermeture.
	 */
	public boolean isUtilisateurOuvert(NSTimestamp date) {
		if (date == null) {
			date = new NSTimestamp(new Date());
		}
		return ((utlFermeture() == null || utlFermeture().after(date) || date.equals(utlFermeture())) && (utlOuverture() == null || utlOuverture().before(date) || date.equals(utlOuverture())));
	}

	public EOPersonne toPersonne() {
		return personne();
	}

	public Integer utlOrdre() {
		return (Integer) EOUtilities.primaryKeyForObject(editingContext(), this).valueForKey(EOUtilisateur.UTL_ORDRE_KEY);
	}

}
