/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfceos.server;

import java.util.Date;

import org.cocktail.fwkcktlgfceos.server.finder.Finder;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOFonction;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeApplication;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonctionExercice;
import org.cocktail.fwkcktlgfceos.server.metier.grhum.EOCompte;
import org.cocktail.fwkcktlgfceos.server.metier.grhum.EOCompteEmail;
import org.cocktail.fwkcktlgfceos.server.metier.grhum.EOPersonne;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Represente un utilisateur de l'application. Si l'utilisateur dispose de droits sur plusieurs applications, et que vous en avez besoin, créez une
 * instance de cette classe pour chaque application.<br/>
 * Cette classe exploite les tables du user oracle GFC mis en place pour la GBCP.
 */
public class ApplicationUser {

	/**
	 * Tableau des EOFonction (dependantes et independantes d'un exercice) autorisees pour l'utilisateur pour l'exercice. Doit etre initialise.
	 */
	protected NSArray<EOFonction> allowedFonctionsForExercice = new NSArray<EOFonction>();

	protected EOUtilisateur utilisateur;
	protected EOPersonne personne;

	private String tyapStrId = null;
	private EOEditingContext editingContext;

	private String login;

	/**
	 * Cree une instance d'un utilisateur à partir d'un persId en filtrant sur un type d'application JEFYADMIN. L'utilisateur n'est pas forcément
	 * existant dans JefyAdmin.
	 * 
	 * @param ec
	 * @param tyapStrId Facultatif. Type de l'application
	 * @param persId Obligatoire. Doit correspondre à un persId existant.
	 */
	public ApplicationUser(EOEditingContext ec, String tyapStrId, Integer persId) {
		this(ec, tyapStrId, EOUtilisateur.fetchByKeyValue(ec, EOUtilisateur.PERS_ID_KEY, persId));
		if (persId == null) {
			throw new RuntimeException("ApplicationUser : Le persId est obligatoire");
		}
		if (getPersonne() == null) {
			EOKeyValueQualifier qual2 = new EOKeyValueQualifier(EOPersonne.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, persId);
			setPersonne(EOPersonne.fetchByQualifier(ec, qual2));
		}
		if (getPersonne() == null) {
			throw new RuntimeException("ApplicationUser : La personne correspondant au persid=" + persId + " n'a pas été trouvée.");
		}
	}

	/**
	 * Cree une instance d'un utilisateur à partir d'un utilisateur (defini dans jefyadmin) en filtrant sur un type d'application JEFYADMIN.
	 * 
	 * @param ec
	 * @param tyapStrId
	 * @param utilisateur
	 */
	public ApplicationUser(EOEditingContext ec, String tyapStrId, EOUtilisateur utilisateur) {
		setEditingContext(ec);
		this.tyapStrId = tyapStrId;
		this.utilisateur = utilisateur;
		if (utilisateur != null) {
			setPersonne(utilisateur.personne());
		}
	}

	public ApplicationUser(EOEditingContext ec, EOUtilisateur utilisateur) {
		this(ec, null, utilisateur);
	}

	public ApplicationUser(EOEditingContext ec, Integer persId) {
		this(ec, null, persId);
	}

	private ApplicationUser() {
		super();
	}

	public void updateAllowedFonctions(String exercice) {
		NSArray<EOUtilisateurFonction> utilisateurFonctions = new NSArray<EOUtilisateurFonction>();
		if (getUtilisateur() != null && tyapStrId != null) {
			//On recupere les autorisations independantes des exercices et celles autorisees sur l'exercice
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + Finder.QUAL_POINT + EOFonction.TYPE_APPLICATION_KEY + Finder.QUAL_POINT + EOTypeApplication.TYAP_STRID_KEY + Finder.QUAL_EQUALS, new NSArray(tyapStrId));
			quals.addObject(qual);
			EOKeyValueQualifier qual1 = new EOKeyValueQualifier(EOUtilisateurFonction.UTILISATEUR_KEY, EOKeyValueQualifier.QualifierOperatorEqual, getUtilisateur());
			quals.addObject(qual1);
			EOKeyValueQualifier qual2 = new EOKeyValueQualifier(EOUtilisateurFonction.FONCTION_KEY, EOKeyValueQualifier.QualifierOperatorNotEqual, null);
			quals.addObject(qual2);
			EOKeyValueQualifier qual3 = new EOKeyValueQualifier(EOUtilisateurFonction.FONCTION_KEY + Finder.QUAL_POINT + EOFonction.FON_ID_INTERNE_KEY, EOKeyValueQualifier.QualifierOperatorNotEqual, null);
			quals.addObject(qual3);

			EOKeyValueQualifier qual4 = new EOKeyValueQualifier(EOUtilisateurFonction.FONCTION_KEY + Finder.QUAL_POINT + EOFonction.FON_SPEC_EXERCICE_KEY, EOKeyValueQualifier.QualifierOperatorEqual, "N");
			quals.addObject(qual4);
			NSArray<EOUtilisateurFonction> utilisateurFonctionsSansEx = EOUtilisateurFonction.fetchAll(getEditingContext(), new EOAndQualifier(quals), null);

			if (exercice != null) {
				quals.removeObject(qual4);
				EOKeyValueQualifier qual5 = new EOKeyValueQualifier(EOUtilisateurFonction.UTILISATEUR_FONCTION_EXERCICES_KEY + Finder.QUAL_POINT + EOUtilisateurFonctionExercice.EXERCICE_KEY + Finder.QUAL_POINT + EOExercice.EXE_EXERCICE_KEY, EOKeyValueQualifier.QualifierOperatorEqual,
						Integer.valueOf(exercice));
				//				quals.addObject(new EOOrQualifier(new NSArray(new EOQualifier[] { qual4, qual5 })));
				quals.addObject(qual5);
				NSArray<EOUtilisateurFonction> utilisateurFonctionsAvecEx = EOUtilisateurFonction.fetchAll(getEditingContext(), new EOAndQualifier(quals), null);
				utilisateurFonctions = NSArrayCtrl.unionOfNSArrays(new NSArray[] {
						utilisateurFonctionsSansEx, utilisateurFonctionsAvecEx
				});
			}
			else {
				utilisateurFonctions = utilisateurFonctionsSansEx;
			}

			//			utilisateurFonctions = EOUtilisateurFonction.fetchAll(getEditingContext(), new EOAndQualifier(quals), null);

			/*
			 * NSArray tmp = getMyAppUtilisateurFonctionsForUtilisateur(getEditingContext(), getUtilisateur()); EOQualifier qual =
			 * EOQualifier.qualifierWithQualifierFormat (EOUtilisateurFonction.TO_UTILISATEUR_FONCTION_EXERCICES_KEY + "." +
			 * EOUtilisateurFonctionExercice.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY + "='" + exercice + "'", null); EOQualifier qual2 =
			 * EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction .TO_FONCTION_KEY + "." + EOFonction.FON_SPEC_EXERCICE_KEY + "='N'",
			 * null); if (exercice != null) { tmp = EOQualifier.filteredArrayWithQualifier(tmp, new EOOrQualifier(new NSArray(new Object[] { qual,
			 * qual2 }))); } else { tmp = EOQualifier.filteredArrayWithQualifier(tmp, new EOOrQualifier(new NSArray(new Object[] { qual2 }))); }
			 * 
			 * for (int i = 0; i < tmp.count(); i++) { if (((EOUtilisateurFonction) tmp.objectAtIndex(i)).toFonction() != null &&
			 * ((EOUtilisateurFonction) tmp.objectAtIndex(i)).toFonction().fonIdInterne() != null) { if (tmp1.indexOfObject(((EOUtilisateurFonction)
			 * tmp.objectAtIndex(i)).toFonction()) == NSArray.NotFound) { tmp1.addObject(((EOUtilisateurFonction) tmp.objectAtIndex(i)).toFonction());
			 * } } else { System.out.println("Autorisation orpheline : " + tmp.objectAtIndex(i).toString()); } }
			 */}
		allowedFonctionsForExercice = ((NSArray<EOFonction>) utilisateurFonctions.valueForKeyPath(EOUtilisateurFonction.FONCTION_KEY)).immutableClone();
	}

	/**
	 * @return Les fonctions autorisees pour l'utilisateur (fonctions independantes de l'exercice et celles dependantes de l'exercice specifie)
	 */
	public final NSArray<EOFonction> getAllowedFonctions(String exercice) {
		if (allowedFonctionsForExercice == null || allowedFonctionsForExercice.count() == 0) {
			updateAllowedFonctions(exercice);
		}
		return allowedFonctionsForExercice;
	}

	/**
	 * @return L'utilisateur recupere a partir du persId specifie lors d'initialisation de l'objet.
	 */
	public EOUtilisateur getUtilisateur() {
		if (utilisateur == null && personne != null) {
			utilisateur = EOUtilisateur.fetchByKeyValue(editingContext, EOUtilisateur.PERS_ID_KEY, personne.persId());
		}
		return utilisateur;
	}

	/**
	 * @param fonction
	 * @param exercice
	 * @return true si la fonction est autorisee (dans {@link ApplicationUser#getAllowedFonctions(String)} ) et si l'utilisateur est valide pour la
	 *         date du jour.
	 */
	public boolean isFonctionAutorisee(EOFonction fonction, String exercice) {
		if (isUtilisateurValide(null)) {
			NSArray<EOFonction> tmp = getAllowedFonctions(exercice);
			return (tmp.indexOfObject(fonction) != NSArray.NotFound);
		}
		return false;
	}

	/**
	 * @param tyapStrId Identifiant de l'application
	 * @param id Identifiant de la fonction.
	 * @param exercice Exercice d'autorisation de la fonction (facultatif si la fonction ne necessite pas de restriction par exercice).
	 * @return true si la fonction est autorisee (dans {@link ApplicationUser#getAllowedFonctions(String)} ) et si l'utilisateur est valide pour la
	 *         date du jour.
	 */
	public boolean isFonctionAutoriseeByFonID(String tyapStrId, String id, String exercice) {
		return isFonctionAutoriseeByFonID(tyapStrId, id, exercice, null);
	}

	/**
	 * @param tyapStrId Identifiant de l'application
	 * @param id Identifiant de la fonction.
	 * @param exercice Exercice d'autorisation de la fonction (facultatif si la fonction ne necessite pas de restriction par exercice).
	 * @date date a laquelle verifier si l'utilisateur est valide (si null, la date du jour est utilisee).
	 * @return TRUE si la fonction est autorisee (dans {@link ApplicationUser#getAllowedFonctions(String)} ) et si l'utilisateur est valide pour la
	 *         date.
	 */
	public boolean isFonctionAutoriseeByFonID(String tyapStrId, String id, String exercice, NSTimestamp date) {
		if (date == null) {
			date = new NSTimestamp(new Date());
		}
		if (isUtilisateurValide(date)) {
			setTyapStrId(tyapStrId);
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.addObject(new EOKeyValueQualifier(EOFonction.TYPE_APPLICATION_KEY + "." + EOTypeApplication.TYAP_STRID_KEY, EOQualifier.QualifierOperatorEqual, tyapStrId));
			quals.addObject(new EOKeyValueQualifier(EOFonction.FON_ID_INTERNE_KEY, EOQualifier.QualifierOperatorEqual, id));
			EOFonction fonction = EOFonction.fetchByQualifier(editingContext, new EOAndQualifier(quals));
			if (fonction != null) {
				return isFonctionAutorisee(fonction, exercice);
			}
		}
		return false;
	}

	/**
	 * @return Le nom et et prenom de l'utilisateur.
	 */
	public String getNomAndPrenom() {
		return getPersonne().persLc() + " " + getPersonne().persLibelle();
		//		return getUtilisateur().getNomAndPrenom();
	}

	/**
	 * Verifie si l'utilisateur est autorise en fonction de la date. Attention il faut que l'attribut EOUtilisateur ait été initialisé.
	 * 
	 * @param date
	 * @throws DroitException Si la date est en dehors des periodes d'ouverture de l'utilisateur.
	 * @see EOUtilisateur#isUtilisateurOuvert(NSTimestamp)
	 */
	public void checkUtilisateurValide(NSTimestamp date) throws Exception {
		if (utilisateur == null && personne == null) {
			throw new DroitException("Utilisateur non recupere.");
		}
		if (!isUtilisateurValide(new NSTimestamp(date))) {
			throw new DroitException("Utilisateur n'est pas autorise pour cette date.");
		}
	}

	/**
	 * @param date facultatif. Date du jour par defaut.
	 * @return true si l'utilisateur (au sens jefyAdmin) est valide
	 */
	public boolean isUtilisateurValide(NSTimestamp date) {
		if (getUtilisateur() == null) {
			return false;
		}
		if (date == null) {
			date = new NSTimestamp(new Date());
		}
		return (getUtilisateur().isUtilisateurOuvert(date));
	}

	/**
	 * @param ec
	 * @param utilisateur
	 * @return Toutes les objets EOUtilisateurFonction pour l'utilisateur pour le type d'application (dependantes et independantes d'un exercice).
	 */
	private NSArray<EOUtilisateurFonction> getMyAppUtilisateurFonctionsForUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) {
		if (utilisateur != null) {
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + Finder.QUAL_POINT + EOFonction.TYPE_APPLICATION_KEY + Finder.QUAL_POINT + EOTypeApplication.TYAP_STRID_KEY + Finder.QUAL_EQUALS, new NSArray(tyapStrId));
			EOKeyValueQualifier qual1 = new EOKeyValueQualifier(EOUtilisateurFonction.UTILISATEUR_KEY, EOKeyValueQualifier.QualifierOperatorEqual, utilisateur);
			NSArray<EOUtilisateurFonction> utilisateurFonctions = EOUtilisateurFonction.fetchAll(ec, new EOAndQualifier(new NSArray(new EOQualifier[] {
					qual1, qual
			})), null);
			return utilisateurFonctions;
			//			NSArray utilisateurFontions = utilisateur.toUtilisateurFonctions();
			//			return EOQualifier.filteredArrayWithQualifier(utilisateurFontions, EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.TO_FONCTION_KEY + Finder.QUAL_POINT + EOFonction.TO_TYPE_APPLICATION_KEY + Finder.QUAL_POINT + EOTypeApplication.TYAP_STRID_KEY
			//					+ Finder.QUAL_EQUALS, new NSArray(tyapStrId)));
		}
		return new NSArray<EOUtilisateurFonction>();

	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @param tyapStrId
	 * @return Toutes les fonctions disponibles pour une application.
	 */
	public static NSArray getAllFonctions(EOEditingContext ec, String tyapStrId) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.TYPE_APPLICATION_KEY + "." + EOTypeApplication.TYAP_STRID_KEY + "='" + tyapStrId + "'", null);

		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOFonction.FON_ID_INTERNE_KEY, EOSortOrdering.CompareAscending));

		return EOFonction.fetchAll(ec, qual, sorts, true);
	}

	public EOPersonne getPersonne() {
		return personne;
	}

	/**
	 * @return le n° individu si l'utilisateur est un individu.
	 */
	public Integer getNoIndividu() {
		return (isIndividu() && getPersonne().persOrdre() != null ? getPersonne().persOrdre() : null);
	}

	public Integer getPersId() {
		return getPersonne().persId();
	}

	/**
	 * @return true si l'utilisateur est un individu.
	 */
	public boolean isIndividu() {
		return (!EOPersonne.PERS_TYPE_STR.equals(getPersonne().persType()));
	}

	public void setPersonne(EOPersonne personne) {
		this.personne = personne;
	}

	public String getTyapStrId() {
		return tyapStrId;
	}

	public void setTyapStrId(String tyapStrId) {
		this.tyapStrId = tyapStrId;
	}

	/**
	 * Renvoie le login de l'utilisateur. Attention : dans la mesure où un utilisateur peut avoir plusieurs comptes, cette classe ne peut récupérer le
	 * login avec lequel l'utilisateur s'est connecte a l'application. Charge donc au devloppeur de l'application d'initialiser le login si besoin est
	 * via {@link ApplicationUser#setLogin(String)}. Sinon, utilisez la methode {@link ApplicationUser#getLogins()} pour recuperer l'ensemble des
	 * logins.
	 * 
	 * @return Le login de l'utilisateur (celui memorise ou bien si ce dernier est nul, le premier trouvé dans la table compte (classé par ordre de de
	 *         VLAN).
	 */
	public String getLogin() {
		if (login == null) {
			NSArray logins = getLogins();
			if (logins.count() > 0) {
				login = (String) logins.objectAtIndex(0);
			}
		}
		return login;
	}

	/**
	 * @return Les login valides de l'utilisateur classés par ordre de priorite VLAN.
	 */
	public NSArray<String> getLogins() {
		NSMutableArray<String> res = new NSMutableArray<String>();

		NSArray<EOCompte> comptes = getPersonne().toComptes(EOCompte.QUAL_CPT_VALIDE_OUI, new NSArray(EOCompte.SORT_VLANS_PRIORITE));

		for (int i = 0; i < comptes.count(); i++) {
			String login = ((EOCompte) comptes.objectAtIndex(i)).cptLogin();
			if (res.indexOfObject(login) == NSArray.NotFound) {
				res.addObject(login);
			}
		}
		return res.immutableClone();
	}

	/**
	 * @return Les emails de la personne (ceux de la table compte_email)
	 */
	public NSArray<String> getEmails() {
		NSMutableArray<String> res = new NSMutableArray<String>();
		NSArray<EOCompte> comptes = getPersonne().toComptes(EOCompte.QUAL_CPT_VALIDE_OUI, new NSArray(EOCompte.SORT_VLANS_PRIORITE));

		for (int i = 0; i < comptes.count(); i++) {
			EOCompte compte = ((EOCompte) comptes.objectAtIndex(i));
			NSArray<EOCompteEmail> cptEmails = compte.toCompteEmails();
			for (int j = 0; j < cptEmails.count(); j++) {
				String email = ((EOCompteEmail) cptEmails.objectAtIndex(j)).getEmailFormatte();
				if (res.indexOfObject(email) == NSArray.NotFound) {
					res.addObject(email);
				}
			}

		}
		return res.immutableClone();
	}

	/**
	 * Affecte le login de l'utilisateur.
	 * 
	 * @param login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

}
