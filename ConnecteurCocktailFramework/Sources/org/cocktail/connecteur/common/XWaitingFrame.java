/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.connecteur.common;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import com.webobjects.eoapplication.client.EOClientResourceBundle;

/**
 Classe destinee a afficher de facon simple des messages d'attente pour les utilisateurs.
 Merci a Titou pour avoir trouve la bonne solution pour l'affichage.
 */
public class XWaitingFrame extends JFrame {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 5944414103239883149L;

	JComponent myContentPane;
    
    String title, message1, message2;
    JLabel texte1, texte2, image;
    
    private JProgressBar myProgressBar;
    private int progressValue;

    /**
     Cree une nouvelle fenetre qui permet d'afficher un message d'attente. Bien entendu cette classe peut etre utilisee pour tous types de messages a afficher. Les parametres anIntro et aMessage peuvent etre nuls. 
     @param aTitle : titre de la fenetre.
     @param anIntro : message d'introduction. Ex : "Traitement en cours : "
     @param aMessage : message d'information. Ex : "Chargement des valeurs..."
     */
    public XWaitingFrame(String aTitle, String aMessage,String aMessage2, boolean progressBar) {
                
        super(aTitle);
        setSize(400,150);
        progressValue=0;
                        
        // Initialisations
        title = aTitle;
        message1 = aMessage;
        message2 = aMessage2;
        
        myProgressBar = new JProgressBar(0, 100);
        myProgressBar.setIndeterminate(false);
        myProgressBar.setStringPainted(true);
        myProgressBar.setBorder(BorderFactory.createEmptyBorder());
        
        // Recuperer les dimensions de l'ecran
        int x = (int)this.getGraphicsConfiguration().getBounds().getWidth();
        int y = (int)this.getGraphicsConfiguration().getBounds().getHeight();
        
        myContentPane = createUI(progressBar);
        myContentPane.setOpaque(true);
        
        this.setContentPane(myContentPane);
        this.setLocation((x/2)-((int)this.getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)this.getContentPane().getMinimumSize().getHeight()/2)));
        // prevenir la fermeture de l'application en cas de souci
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        //Display the window.
        this.pack();
        this.setVisible(true);
        //this.paintAll(this.getGraphics());
    }
    
    /**
     * 
     * @param yn
     */
    public void setProgressBarVisible(boolean yn)	{
       myProgressBar.setVisible(yn); 
    }
    
    // Creation du panneau et des composants a afficher
    private JPanel createUI(boolean progressbar) {
        JPanel container=new JPanel(new BorderLayout());
        
        texte1 = new JLabel(message1);
        texte2 = new JLabel(message2);
        image = new JLabel((ImageIcon)new EOClientResourceBundle().getObject("Sablier"));

        //JPanel panelImage=new JPanel(new BorderLayout());
        container.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(20,20,10,20));
        texte1.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        texte2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        
        panel.add(Box.createVerticalStrut(5)); //extra space
        panel.add(texte1);
        panel.add(texte2);
         
        if (progressbar)
            panel.add(myProgressBar);
        
        panel.add(Box.createRigidArea(new Dimension(300, 40)));
        
        container.add(panel, BorderLayout.CENTER);
        container.add(image, BorderLayout.WEST);
        
        return container;
    }
    
    /**
     Modification du titre de la fenetre. Prend en compte la modification immediatement.
     */
    public void setTitle(String aTitle) {
        title = aTitle;
        this.setTitle(title);
        this.paintAll(this.getGraphics());
    }
    
    public void setStringProgress(String texte)	{
        myProgressBar.setString(texte);
        //this.paintAll(this.getGraphics());
    }
    
    public void setMaxProgress(int value)	{
        myProgressBar.setMaximum(value);
        //this.paintAll(this.getGraphics());
    }
    
    public void setMinProgress(int value)	{
        myProgressBar.setMinimum(value);
        //this.paintAll(this.getGraphics());
    }
    
    public class MonThread extends Thread {
        public MonThread() {
            super();
        }
        public void run() {
            boolean running=true;
            while (running) {
                try {
                    sleep(100);
                    myProgressBar.setValue(progressValue);
                    myProgressBar.setString(progressValue + " / " + myProgressBar.getMaximum());
                    
                    myProgressBar.paintAll(myProgressBar.getGraphics());
                    myContentPane.paintAll(myContentPane.getGraphics());
                } catch( InterruptedException e ) {
                    running=false;
                }
            }
        }
    }    
    
    public void startProgress() {
        progressValue=0;
    }
    
    public void setCurrentProgress(int value)	{
        progressValue=value;
        myProgressBar.setValue(value);
        myProgressBar.setString(value+"/"+myProgressBar.getMaximum());
    }
    
    public void setIndeterminateProgress(boolean yn){
        myProgressBar.setIndeterminate(yn);
        this.paintAll(this.getGraphics());
    }
    
    /**
     Modification du message d'information de la fenetre. Prend en compte la modification immediatement.
     */
    public void setMessages(String m1, String m2) {
        message1 = m1;
        message2 = m2;
        texte1.setText(message1);
        texte2.setText(message2);
        this.paintAll(this.getGraphics());
    }
        
    /**
     Fermeture de la fenetre d'attente.
     */
    public void close() {
        this.setVisible(false);
        this.dispose();
    }
    
    
    
}
