//
//  CRICursor.java
//  CRIJavaClient
//
//  Created by jfguilla on Mon Jul 21 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//
package org.cocktail.connecteur.common;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Window;

import javax.swing.SwingUtilities;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.EOSimpleWindowController;

/**
Classe qui implemente des methodes statiques pour l'affichage de curseurs d'attente pour les applications Java Client.
 Elle est destinee a remplacer l'usage de la classe WaitingCursor du framework CRIWaiting.
 Celle-ci prend en compte la version 5.2 de WO et les problemes qui sonta apparus.

 Pour un bon affichage des curseurs, suivre les instructions donnees avec chaque methode.
 */
public class CRICursor {
    /**
    Affichage d'un curseur d'attente sur le composant comp :
     */
    public static void setWaitCursor(Component comp) {
        comp.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }
    
    /**
    Methode pratique pour ceux qui preferent :
     - Positionne un curseur d'attente si isWaiting est vrai (isWaiting == true).
     - Positionne un curseur standard si isWait est faux.
     Permet d'utiliser la meme methode au debut et a la fin de l'execution qui necessite le curseur d'attente.
     */
    public static void setWaitCursor(Component comp, boolean isWaiting) {
        if(isWaiting)
            comp.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        else
            comp.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
    
    /**
    Affichage du curseur standard sur le composant comp : a utiliser apres setWaitingCursor() pour remettre le curseur par defaut.
     De preference placer setDefaultCursor() dans un clause "finally" d'un try/catch/finally - ca evite a l'ulitisateur de conserver un curseur d'attente alirs que tout est termine depuis longtemps.
     */
    public static void setDefaultCursor(Component comp) {
        comp.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    /**
    Permet de positionner un curseur d'attente sur la fenetre utilisee par un EOInterfaceController lorsque celui-ci est lance avec EOFrameController.runControllerInNewFrame(), EODialogController.runControllerInNewDialog(), EOModalDialogController.runControllerInNewModalDialog()...
     - La methode recupere le composant graphique (Window) et affiche un curseur d'attente.

     @param controller : un objet de type EOInterfaceController
     */
    public static void setWaitCursor(EOInterfaceController controller) {
        // Les supercontrollers descendent de EOSimpleWindowController...
        EOSimpleWindowController swController = (EOSimpleWindowController)controller.supercontroller();
        if(swController != null) {
            swController.window().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
    }

    /**
    Permet de positionner un curseur d'attente sur la fenetre utilisee si isWaiting est vrai, sinon positionne un curseur standard par un EOInterfaceController lorsque celui-ci est lance avec EOFrameController.runControllerInNewFrame(), EODialogController.runControllerInNewDialog(), EOModalDialogController.runControllerInNewModalDialog()...
     - La methode recupere le composant graphique (Window) et affiche un curseur d'attente.
     Dans la plupart des cas, l'usage sera:
     
     CRICursor.setWaitCursor(this, true);
     // Traitements a effectuer...
     ...
     CRICursor.setWaitCursor(this, false);

     @param controller : un objet de type EOInterfaceController
     @param isWaiting : true s'il faut afficher un curseur d'attente, faux sinon - affichage d'un curseur standard
     */
    public static void setWaitCursor(EOInterfaceController controller, boolean isWaiting) {
        // Les supercontrollers descendent de EOSimpleWindowController...
        EOSimpleWindowController swController = (EOSimpleWindowController)controller.supercontroller();
        if(swController != null) {
            if(isWaiting)
                swController.window().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            else
                swController.window().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    /**
    Permet de positionner un curseur standard sur la fenetre utilisee par un EOInterfaceController lorsque celui-ci est lance avec EOFrameController.runControllerInNewFrame(), EODialogController.runControllerInNewDialog(), EOModalDialogController.runControllerInNewModalDialog()...
     - La methode recupere le composant graphique (Window) et affiche un curseur par default.
     Ne pas oublier de l'utiliser apres un setWaitCursor (merci pour les utilisateurs;-)

     @param controller : un objet de type EOInterfaceController
     */
    public static void setDefaultCursor(EOInterfaceController controller) {
        // Les supercontrollers descendent de EOSimpleWindowController...
        EOSimpleWindowController swController = (EOSimpleWindowController)controller.supercontroller();
        if(swController != null) {
            swController.window().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }


    /**
    Affichage d'un curseur d'attente sur la fenetre utilisee par un objet de type EOFrameController, EODialogController, EOModalDialogController qui descendent tous de EOSimpleWindowController lorsqu'ils sont lances directement (ex : activateWindow()).
     Recupere l'objet Window approprie et positionne un curseur d'attente.

     @param controller : un objet de type EOSimpleWindowController
     */
    public static void setWaitCursor(EOSimpleWindowController controller) {
        controller.window().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }
    
    /**
    Affichage d'un curseur standard sur la fenetre utilisee par un objet de type EOFrameController, EODialogController, EOModalDialogController qui descendent tous de EOSimpleWindowController lorsqu'ils sont lances directement (ex : activateWindow()).
     Recupere l'objet Window approprie et positionne un curseur standard.

     @param controller : un objet de type EOSimpleWindowController
     */
    public static void setDefaultCursor(EOSimpleWindowController controller) {
        controller.window().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    /**
    Methode de convenence : choix du curseur a afficher sur la vue d'un objet de type EOSimpleWindowController (EOFrameController, EODialogController, EOModalDialogController).
     Recupere l'objet Window approprie et positionne un curseur d'attente ou un curseur standard.
     @param controller : controller pour lequel on doit afficher un curseur
     @param isWaiting : true s'il faut afficher un curseur d'attente, faux sinon - affichage d'un curseur standard    
    */
    public static void setWaitCursor(EOSimpleWindowController controller, boolean isWaiting) {
        if(isWaiting)
            controller.window().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        else
            controller.window().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

    }
    
    /**
    
    */
    /**
    Affichage d'un curseur d'attente sur l'objet Window qui contient le composant comp. Cette methode doit etre utilisee lorsqu'il n'est pas possible d'afficher le curseur d'attente directement sur le composant ou sur la window... bref, si l'on a tout essaye et que ca ne fonctionne pas !
     L'usage de cette methode est plutot a reserver aux cas desesperes... utiliser de preference l'une des autres methodes en fonction de la situation.

     En general le "component()" d'un EOInterfaceController ne permet pas de recuperer d'objet "Window" sur lequelle le setCursor() peut s'appliquer, il vaut donc mieux passer un composant de la vue en parametre. Ce peut etre, par exemple un bouton contenu dans la vue sur laquelle on veut afficher le curseur d'attente ou tout autre composant graphique.
     @param comp : un composant graphique de la vue
     */
    public static void setWaitCursorForWindowOf(Component comp) {
        Window win = SwingUtilities.windowForComponent(comp);
        // Si la window recuperee n'est pas nulle :
        if(win != null)
            win.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        // sinon, on tente quand meme quelque chose.
        else
            comp.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    /**
    Methode de convenence : choix d'un curseur sur l'objet Window qui contient le composant comp. Cette methode doit etre utilisee lorsqu'il n'est pas possible d'afficher le curseur d'attente directement sur le composant ou sur la window... bref, si l'on a tout essaye et que ca ne fonctionne pas !
     L'usage de cette methode est plutot a reserver aux cas desesperes... utiliser de preference l'une des autres methodes en fonction de la situation.

     En general le "component()" d'un EOInterfaceController ne permet pas de recuperer d'objet "Window" sur lequelle le setCursor() peut s'appliquer, il vaut donc mieux passer un composant de la vue en parametre. Ce peut etre, par exemple un bouton contenu dans la vue sur laquelle on veut afficher le curseur d'attente ou tout autre composant graphique.
     @param comp : un composant graphique de la vue
     @param isWaiting : true s'il faut afficher le curseur d'attente, faux sinon i.e. affichage d'un curseur standard
     */
    public static void setWaitCursorForWindowOf(Component comp, boolean isWaiting) {
        Window win = SwingUtilities.windowForComponent(comp);
        // Si la window recuperee n'est pas nulle :
        if(win != null) {
            if(isWaiting)
                win.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            else
                win.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        // sinon, on tente quand meme quelque chose.
        else {
            if(isWaiting)
                comp.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            else
                comp.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    /**
    Affichage d'un curseur standard sur l'objet Window qui contient le composant comp. Cette methode doit etre utilisee lorsqu'il n'est pas possible d'afficher le curseur standard directement sur le composant ou sur la window... bref, si l'on a tout essaye et que ca ne fonctionne pas !
     L'usage de cette methode est plutot a reserver aux cas desesperes... utiliser de preference l'une des autres methodes en fonction de la situation.

     En general le "component()" d'un EOInterfaceController ne permet pas de recuperer d'objet "Window" sur lequelle le setCursor() peut s'appliquer, il vaut donc mieux passer un composant de la vue en parametre. Ce peut etre, par exemple un bouton contenu dans la vue sur laquelle on veut afficher le curseur d'attente ou tout autre composant graphique.
     */
    public static void setDefaultCursorForWindowOf(Component comp) {
        Window win = SwingUtilities.windowForComponent(comp);
        // Si la window recuperee n'est pas nulle :
        if(win != null)
            win.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        // sinon, on tente quand meme quelque chose... comme pour le setWaiting
        else
            comp.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

}
