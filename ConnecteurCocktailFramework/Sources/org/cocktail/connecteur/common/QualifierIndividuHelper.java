package org.cocktail.connecteur.common;

import java.util.Arrays;
import java.util.List;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class QualifierIndividuHelper {

	// Attributes
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PRENOM_KEY = "prenom";
	public static final String TEM_VALIDE_KEY = "temValide";

	public NSArray rechercherHomonymes(EOEditingContext editingContext, String nom, String prenom) {
		return rechercherHomonymes(editingContext, nom, prenom, null, null);
	}

	public NSArray rechercherHomonymes(EOEditingContext editingContext, EOIndividu individu) {
		EOQualifier qualifier;
		String parametreAttributsHomonyme = EOImportParametres.attributsHomonymeIndividu();
		if (parametreAttributsHomonyme != null && !parametreAttributsHomonyme.equals(""))
			return rechercherHomonymesDepuisParametre(editingContext, individu, parametreAttributsHomonyme);
		else
			return rechercherHomonymes(editingContext, individu.nomUsuel(), individu.prenom(), individu.dateNaissance(), individu.noInsee());
	}

	/**
	 * Recherche les personnes avec le m&ecirc;me nom, prenom et si la date de naissance ou le numero Insee sont fournis, la m&ecirc;me date de naissance ou le
	 * m&ecirc;me numero Insee, sinon les individus valides
	 * 
	 * @param editingContext
	 * @param nom
	 * @param prenom
	 * @param dateNaissance
	 * @param noInsee
	 * @return
	 */
	public NSArray rechercherHomonymes(EOEditingContext editingContext, String nom, String prenom, NSTimestamp dateNaissance, String noInsee) {
		EOFetchSpecification fs = new EOFetchSpecification("GrhumIndividu", qualifierPourHomonyme(editingContext, nom, prenom, dateNaissance, noInsee), null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	private NSArray rechercherHomonymesDepuisParametre(EOEditingContext editingContext, EOIndividu individu, String parametreAttributs) {
		if (parametreAttributs == null)
			return null;

		List<String> attributs = Arrays.asList(parametreAttributs.split(","));
		if (attributs.size() < 1)
			return null;

		NSMutableArray qualifiers = new NSMutableArray();
		for (String attribut : attributs) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(attribut + " = %@", new NSArray(individu.valueForKey(attribut))));
		}

		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification fs = new EOFetchSpecification(EOGrhumIndividu.ENTITY_NAME, qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	public EOQualifier qualifierPourHomonyme(EOEditingContext editingContext, String nom, String prenom, NSTimestamp dateNaissance, String noInsee) {
		String nomUsuel = nom.toUpperCase();
		if (nomUsuel.startsWith("DE ")) {
			nomUsuel = nomUsuel.replaceFirst("DE ", "*");
		}
		String nomPropre = QualifierIndividuHelper.formaterPourRechercheComplete(nomUsuel);
		String prenomPropre = StringCtrl.chaineClaire(prenom, true).toUpperCase();
		String prenomMajuscule = QualifierIndividuHelper.formaterPourRechercheComplete(prenom.toUpperCase());

		NSMutableArray qualifiers = new NSMutableArray();
		// NSMutableArray homonymesPursQualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray(nomPropre)));
		// homonymesPursQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray(nomPropre)));

		if (prenomMajuscule.equals(prenomPropre) == false) {
			NSMutableArray orQualifiersPrenom = new NSMutableArray();
			orQualifiersPrenom.addObject(EOQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(prenomMajuscule)));
			orQualifiersPrenom.addObject(EOQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@",
					new NSArray(QualifierIndividuHelper.formaterPourRechercheComplete(prenomPropre.toUpperCase()))));

			qualifiers.addObject(new EOOrQualifier(orQualifiersPrenom));
			// homonymesPursQualifiers.addObject(new EOOrQualifier(orQualifiersPrenom));
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(prenomMajuscule)));
			// homonymesPursQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new
			// NSArray(prenomMajuscule)));
		}

		if (dateNaissance != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_NAISSANCE_KEY + " = %@", new NSArray(dateNaissance)));
		}

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DECES_KEY + " = nil", null));
		// homonymesPursQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		// homonymesPursQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DECES_KEY + " = nil", null));

		if (noInsee != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(IND_NO_INSEE_KEY + "=%@", new NSArray(noInsee)));
		}

		NSMutableArray inseeQualifiers = new NSMutableArray();
		if (noInsee != null) {
			inseeQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));

			inseeQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DECES_KEY + " = nil", null));

			inseeQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(IND_NO_INSEE_KEY + "=%@", new NSArray(noInsee)));
		}

		EOQualifier qual = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] { new EOAndQualifier(qualifiers) // ,
				// new EOAndQualifier(homonymesPursQualifiers),
				// new EOAndQualifier(inseeQualifiers)
				}));

		return qual;

	}

	public NSArray<EOQualifier> qualifierPourNomPrenomHomonyme(EOEditingContext editingContext, String nom, String prenom) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray();

		String nomUsuel = nom.toUpperCase();
		if (nomUsuel.startsWith("DE ")) {
			nomUsuel = nomUsuel.replaceFirst("DE ", "*");
		}
		String nomPropre = QualifierIndividuHelper.formaterPourRechercheComplete(nomUsuel);
		String prenomPropre = StringCtrl.chaineClaire(prenom, true).toUpperCase();
		String prenomMajuscule = QualifierIndividuHelper.formaterPourRechercheComplete(prenom.toUpperCase());

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray(nomPropre)));

		if (prenomMajuscule.equals(prenomPropre) == false) {
			NSMutableArray orQualifiersPrenom = new NSMutableArray();
			orQualifiersPrenom.addObject(EOQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(prenomMajuscule)));
			orQualifiersPrenom.addObject(EOQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@",
					new NSArray(QualifierIndividuHelper.formaterPourRechercheComplete(prenomPropre.toUpperCase()))));

			qualifiers.addObject(new EOOrQualifier(orQualifiersPrenom));
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(prenomMajuscule)));
		}

		return qualifiers;
	}

	public NSArray<EOQualifier> notQualifierPourNomPrenomHomonyme(EOEditingContext editingContext, String nom, String prenom) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray();

		String nomUsuel = nom.toUpperCase();
		if (nomUsuel.startsWith("DE ")) {
			nomUsuel = nomUsuel.replaceFirst("DE ", "*");
		}
		String nomPropre = QualifierIndividuHelper.formaterPourRechercheComplete(nomUsuel);
		String prenomPropre = StringCtrl.chaineClaire(prenom, true).toUpperCase();
		String prenomMajuscule = QualifierIndividuHelper.formaterPourRechercheComplete(prenom.toUpperCase());

		qualifiers.addObject(EONotQualifier.qualifierWithQualifierFormat(NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray(nomPropre)));

		if (prenomMajuscule.equals(prenomPropre) == false) {
			NSMutableArray orQualifiersPrenom = new NSMutableArray();
			orQualifiersPrenom.addObject(EONotQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(prenomMajuscule)));
			orQualifiersPrenom.addObject(EONotQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(
					QualifierIndividuHelper.formaterPourRechercheComplete(prenomPropre.toUpperCase()))));

			qualifiers.addObject(new EOOrQualifier(orQualifiersPrenom));
		} else {
			qualifiers.addObject(EONotQualifier.qualifierWithQualifierFormat(PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(prenomMajuscule)));
		}

		return qualifiers;
	}

	public NSArray<EOQualifier> qualifierValiditeIndividu() {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DECES_KEY + " = nil", null));

		return qualifiers;
	}

	public EOQualifier qualifierPurHomonyme(EOEditingContext editingContext, String nom, String prenom) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray();

		qualifiers.addObjectsFromArray(qualifierPourNomPrenomHomonyme(editingContext, nom, prenom));
		qualifiers.addObjectsFromArray(qualifierValiditeIndividu());

		EOQualifier qual = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] { new EOAndQualifier(qualifiers) }));

		return qual;
	}

	// Méthodes statiques
	// Créer une string avec des étoiles par tout
	public static String formaterPourRechercheComplete(String aStr) {
		// Modif. le 22/04/2005 :spécif. UNC pour les noms du style N'GUYEN, le car. ' est remplacé par rien
		aStr = aStr.replaceAll("'", "");
		// 17/06/09 : on remplace le car "-" par ""
		aStr = aStr.replaceAll("-", "");
		// 17/06/09 : on remplace le car " " par ""
		aStr = aStr.replaceAll(" ", "");
		String newStr = "";
		for (int i = 0; i < aStr.length(); i++) {
			newStr = newStr + aStr.substring(i, i + 1) + "*";
		}
		return newStr;
	}

}
