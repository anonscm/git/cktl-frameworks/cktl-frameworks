package org.cocktail.connecteur.common;

import java.awt.Color;

import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class CocktailConstantes 
{
	protected	static final EOClientResourceBundle resourceBundle = new EOClientResourceBundle();
	
	public static final String[] LISTE_MOIS = new String[]{"JANVIER","FEVRIER","MARS","AVRIL","MAI","JUIN",
		"JUILLET","AOUT","SEPTEMBRE","OCTOBRE","NOVEMBRE","DECEMBRE"};

	public static final String [] LETTRES_ALPHABET = new String[] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

	public static final Integer[] LISTE_ANNEES_DESC = new Integer[]{new Integer("2015"),new Integer("2014"),new Integer("2013"),new Integer("2012"),
		new Integer("2011"),new Integer("2010"), new Integer("2009"),new Integer("2008"), new Integer("2007"), 
				new Integer("2006"), new Integer("2005"), new Integer("2004")};
	public static final Integer[] LISTE_ANNEES_ASC = new Integer[]{new Integer("2004"),new Integer("2005"),
		new Integer("2006"),new Integer("2007"), new Integer("2008"),new Integer("2009"), new Integer("2010"), 
				new Integer("2011"), new Integer("2012"), new Integer("2013"), new Integer("2014"), new Integer("2015")};

	public static final String SAUT_DE_LIGNE = "\n";
	public static final String SEPARATEUR_TABULATION = "\t";
	public static final String SEPARATEUR_EXPORT = ";";
	
	public static final String EXTENSION_CSV = ".csv";
	public static final String EXTENSION_PDF = ".pdf";
	public static final String EXTENSION_EXCEL = ".xls";
	public static final String EXTENSION_TXT = ".txt";
	public static final String EXTENSION_JASPER = ".jasper";
	public static final String EXTENSION_XML = ".xml";
	
	public static final String VRAI = "O";
	public static final String FAUX = "N";

	public final static String MONSIEUR = "M.";
	public final static String MADAME = "MME";
	public final static String MADEMOISELLE = "MLLE";

	public static final String STRING_EURO = " \u20ac";
	
	// COULEURS
	public static Color BG_COLOR_WHITE = new Color(255,255,255);
	public static Color BG_COLOR_BLACK = new Color(0,0,0);
	public static Color BG_COLOR_GREY = new Color(100, 100, 100);
	public static Color BG_COLOR_RED = new Color(255,0,0);
	public static Color BG_COLOR_GREEN = new Color(0,255,0);
	public static Color BG_COLOR_BLUE = new Color(0,0,255);
	public static Color BG_COLOR_YELLOW = new Color(255,255,0);
	public static Color BG_COLOR_LIGHT_GREY = new Color(100,100,100);
	public static Color BG_COLOR_CYAN = new Color(224, 255, 255);
	public static Color BG_COLOR_PINK = new Color(248,192,218);

	public static Color COULEUR_FOND_INACTIF = new Color(222,222,222);
	public static Color COULEUR_FOND_ACTIF = new Color(255, 255, 255);
	 
	public static final Color COLOR_INACTIVE_BACKGROUND = new Color(222,222,222);
	public static final Color COLOR_ACTIVE_BACKGROUND = new Color(0,0,0);

    public static final Color COLOR_BKG_TABLE_VIEW = new Color(230, 230, 230);
    public static final Color COLOR_SELECTED_ROW = new Color(127,155,165);

    public static final Color COLOR_SELECT_NOMENCLATURES = new Color(100,100,100);
    public static final Color COLOR_FOND_NOMENCLATURES = new Color(220,220,220);
    public static final Color COLOR_FILTRES_NOMENCLATURES = new Color(240,240,240);

    
    public static final String DATE_NON_VALIDE_TITRE = "Date non valide";
    public static final String DATE_NON_VALIDE_MESSAGE = "La date saisie n'est pas valide !";
    public static final String ATTENTION = "ATTENTION";
    public static final String ERREUR = "ERREUR";
    
    /* Message d'erreur général */
    public static final String ERREUR_DATE_DEBUT_NON_RENSEIGNE = "Veuillez renseigner une date de début";
    public static final String ERREUR_DATE_FIN_AVANT_DATE_DEBUT = "La date de fin doit être postérieure à la date de début";
    public static final String ERREUR_CATEGORIE_EMPLOI_NON_RENSEIGNE = "Veuillez renseigner une catégorie d'emploi";
    public static final String ERREUR_STRUCTURE_NON_RENSEIGNE = "Veuillez renseigner une structure";
    public static final String ERREUR_UAI_NON_RENSEIGNE = "Veuillez renseigner une structure UAI";
    public static final String ERREUR_BUDGET_NON_RENSEIGNE = "Veuillez renseigner un budget";
    public static final String ERREUR_QUOTITE_NON_RENSEIGNE = "Veuillez renseigner la quotité";
    public static final String ERREUR_AFFECTATION_QUOTITE_SUPERIEUR_CENT = "La quotité totale des affectations ne peut pas dépasser 100% pour cette période";
    public static final String ERREUR_AFFECTATION_MEME_STRUCTURE = "L'agent ne peut pas être affecté à la même structure pendant la même période";
    public static final String ERREUR_QUOTITE_SUPERIEUR_CENT = "La quotité ne peut pas dépasser 100%";
    public static final String ERREUR_PROGRAMME_NON_RENSEIGNE = "Veuillez renseigner un programme";
    public static final String ERREUR_TITRE_NON_RENSEIGNE = "Veuillez renseigner un titre";
    
    
    /* EMPLOI */
    public static final String ERREUR_EMPLOI_NO_NON_RENSEIGNE = "Veuillez renseigner un numéro d'emploi";
    public static final String ERREUR_EMPLOI_EXISTE_DEJA = "Ce numéro d'emploi existe déjà";
    public static final String ERREUR_EMPLOI_CATEGORIE_CHEVAUCHEMENT = "Cet emploi a déjà des catégories d'emploi pendant cette période";
    public static final String ERREUR_EMPLOI_SPEC_CHEVAUCHEMENT = "Cet emploi a déjà des spécialisations pendant cette période";
    public static final String ERREUR_EMPLOI_BUDGET_CHEVAUCHEMENT = "Cet emploi a déjà des budgets pendant cette période";
    public static final String ERREUR_EMPLOI_DATE_PUBLICATION_AVANT_EFFET = "La date de publication doit être postérieure à la date d'effet";
    public static final String ERREUR_EMPLOI_DATE_FERMETURE_AVANT_PUBLICATION = "La date de fermeture doit être postérieure à la date de publication";
    public static final String ERREUR_EMPLOI_DATE_FERMETURE_AVANT_EFFET = "La date de fermeture doit être postérieure à la date d'effet"; 
    public static final String ERREUR_EMPLOI_DATE_DEBUT_AVANT_EFFET = "La date de début doit être postérieure à la date d'effet de l'emploi";
    public static final String ERREUR_EMPLOI_DATE_DEBUT_AVANT_PUBLICATION = "La date de début doit être postérieure à la date de publication de l'emploi";
    public static final String ERREUR_EMPLOI_DATE_FIN_APRES_FERMETURE = "La date de fin doit être antérieure à la date de fermeture de l'emploi";
    public static final String ERREUR_EMPLOI_ELEMENTS_DATE_FIN_NON_RENSEIGNE = "L'emploi est fermé. Il faut renseigner la date de fin";
    public static final String ERREUR_EMPLOI_TEMPORAIRE_DATE_FERMETURE = "La date de fermeture est obligatoire pour un emploi temporaire. Veuillez la renseigner";
    public static final String ERREUR_EMPLOI_OCCUPATION_DATE_FERMETURE_KO = "Il existe des occupations courantes pour cet emploi, vous ne pouvez pas le fermer";
    public static final String ERREUR_EMPLOI_OCCUPATION_SUPPRESSION_KO = "Il existe des occupations courantes pour cet emploi, vous ne pouvez pas le supprimer";
    public static final String ATTENTION_EMPLOI_ENSEIGNANT_MODIF_SPECIALISATION = "Pensez à modifier la spécialisaion de cet emploi";
    
    
    /* OCCUPATION */
    public static final String ERREUR_OCCUPATION_QUOTITE_DISPONIBLE = "Il ne reste que %s % de quotité d'occupation disponible sur cet emploi.";

}