package org.cocktail.connecteur.common.metier.controles.periode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IControle;
import org.cocktail.connecteur.common.metier.controles.ResultatControle;
import org.cocktail.connecteur.common.metier.controles.TypeControle;

public class PeriodeControleDebutFin implements IControle<Periode> {
	private static final TypeControle[] LISTE_TYPES_CONTROLES = { PeriodeTypeControles.VALIDATION_ATTRIBUT };

	private static final String ERROR_DEBUT_NULL = "La date de début ne peut pas être nulle";
	private static final String ERROR_DEBUT_APRES_FIN = "La date de début (%s) se situe après la date de fin (%s)";

	public boolean checkable(Periode objAControler) {
		return true;
	}

	public ResultatControle check(Periode objAControler) {
		Date debut = objAControler.getDebut();
		if (debut == null) {
			return new ResultatControle(false, ERROR_DEBUT_NULL);
		}

		Date now = new Date();
		Date fin = objAControler.getFin();

		List<String> errorMessages = new ArrayList<String>();

		if (fin != null) {
			if (debut.after(fin))
				errorMessages.add(String.format(ERROR_DEBUT_APRES_FIN, debut.toString(), fin.toString()));
		}
		if (errorMessages.size() > 0)
			return new ResultatControle(false, errorMessages);
		else
			return ResultatControle.RESULTAT_OK;
	}

	public List<TypeControle> getTypes() {
		return Arrays.asList(LISTE_TYPES_CONTROLES);
	}

}
