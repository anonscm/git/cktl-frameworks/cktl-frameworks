package org.cocktail.connecteur.common.metier.controles;

import java.util.Collection;
import java.util.List;

public interface IControleService<T> {

	List<ResultatControle> appliqueTousControles(T objAControler);
	
	List<ResultatControle> appliqueListeControles(T objAControler, List<Class> controleClasses, boolean erreurSiNonTrouve);

	List<ResultatControle> appliqueControlesDeType(T objAControler,TypeControle type, boolean erreurSiAucunControleTrouve);

	ResultatControle appliqueControle(T objAControler, Class controleClass, boolean erreurSiNonTrouve);

	IControle<T> getControle(Class controleClass);

	List<IControle<T>> getControlesDeType(TypeControle type);

	Collection<IControle<T>> getControles();
	
	void ajouteControle(IControle<T> controle);
	
	void setControles(Collection<IControle<T>> controles);
}
