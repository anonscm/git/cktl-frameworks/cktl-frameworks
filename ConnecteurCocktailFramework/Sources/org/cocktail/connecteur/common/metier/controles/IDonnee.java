package org.cocktail.connecteur.common.metier.controles;

public interface IDonnee {
	String SEPARATEUR_CLE_PRIMAIRE="|";
	
	/**
	 * Retourne une chaine représentant la clé primaire de la donnée.
	 * 	Dans le cas d'une clé sur plusieurs attibuts, retourne la concaténation des attributs séparé par "|"
	 * @return La donnée de clé primaire (une contaténation séparée pas "|" des colonnes dans le cas d'une clé multiple) 
	 */
	String clePrimaire();
}
