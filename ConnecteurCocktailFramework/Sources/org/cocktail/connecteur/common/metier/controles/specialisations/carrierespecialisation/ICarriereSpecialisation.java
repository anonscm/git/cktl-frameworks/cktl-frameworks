package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import java.util.Date;

import org.cocktail.connecteur.common.metier.controles.periode.Periode;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ICnu;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IDisciplineSecondDegre;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IReferensEmploi;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteAtos;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteItarf;

public interface ICarriereSpecialisation {
	String codeemploi();
	String cBap();
	String cDiscSecondDegre();
	String cSpecialiteItarf();
	String cSpecialiteAtos();
	String cSectionCnu();
	String cSousSectionCnu();
	Date dDebut();
	Date dFin();
	Periode periodeEffet();
	
	void setCodeemploi(String codeemploi);
	void setCBap(String codeBap);
	void setCDiscSecondDegre(String codeDisciplineSecondDegre);
	void setCSpecialiteItarf(String specialiteItarf);
	void setCSpecialiteAtos(String specialiteAtos);
	void setCSectionCnu(String codeSectionCnu);
	void setCSousSectionCnu(String codeSousSectionCnu);
	void setDDebut(Date debut);
	void setDFin(Date fin);
	
	IBap getBap();
	IDisciplineSecondDegre getDisciplineSecondDegre();
	ICnu getCnu();
	IReferensEmploi getEmploi();
	ISpecialiteItarf getSpecialiteItarf();
	ISpecialiteAtos getSpecialiteAtos();
}
