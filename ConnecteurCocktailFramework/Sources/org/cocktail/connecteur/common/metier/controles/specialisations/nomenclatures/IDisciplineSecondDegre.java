package org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.IDonnee;

public interface IDisciplineSecondDegre extends IDonnee {
	String cDiscSecondDegre();
}
