package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IControle;
import org.cocktail.connecteur.common.metier.controles.ResultatControle;
import org.cocktail.connecteur.common.metier.controles.TypeControle;
import org.cocktail.connecteur.common.metier.repositories.MainManager;

public class CarriereSpecialisationControleExisteDansNomenclature implements IControle<ICarriereSpecialisation> {
	private static final TypeControle[] LISTE_TYPES_CONTROLES = {CarriereSpecialisationTypeControle.VALIDATION_ATTRIBUT};
	
	private static final String ERREUR_CODE_BAP = "Code BAP '%s' non trouvé dans la nomenclature BAP";
	private static final String ERREUR_CODE_EMPLOI = "Code '%s' non trouvé dans la nomenclature REFERENS_EMPLOI";
	private static final String ERREUR_CODE_CNU = "SectionCnu,SousSectionCnu '%s,%s' non trouvées dans la nomenclature Cnu";
	private static final String ERREUR_CODE_DISC_SD_DEG = "Code '%s' non trouvé dans la nomenclature DisciplineSecondDegre";
	private static final String ERREUR_CODE_ATOS = "Code '%s' non trouvé dans la nomenclature Spécialité Atos";
	private static final String ERREUR_CODE_ITARF = "Bap,SpecialiteItarf '%s,%s' non trouvés dans la nomenclature SpécialitéItarf";
	
	public boolean checkable(ICarriereSpecialisation objAControler) {
		return true;
	}
	
	public ResultatControle check(ICarriereSpecialisation objAControler) {
		MainManager m=MainManager.instance();
		ICarriereSpecialisation o=objAControler;

		List<String> errorMessages=new ArrayList<String>();
		
		if ((o.cBap()!=null && o.cSpecialiteItarf()==null) && m.getBapRepository().fetchCBap(o.cBap())==null)
			errorMessages.add(String.format(ERREUR_CODE_BAP,o.cBap()));
		
		if ((o.cSectionCnu()!=null ||  o.cSousSectionCnu()!=null) && m.getCnuRepository().fetchSectionCnuSousSectionCnu(o.cSectionCnu(), o.cSousSectionCnu())==null)
			errorMessages.add(String.format(ERREUR_CODE_CNU,o.cSectionCnu(),o.cSousSectionCnu()));
		
		if (o.codeemploi()!=null && m.getReferensEmploiRepository().fetchCodeEmploi(o.codeemploi())==null)
			errorMessages.add(String.format(ERREUR_CODE_EMPLOI,o.codeemploi()));
		
		if (o.cDiscSecondDegre()!=null && m.getDiscSecondDegreRepository().fetchCDiscSecondDegre(o.cDiscSecondDegre())==null)
			errorMessages.add(String.format(ERREUR_CODE_DISC_SD_DEG,o.cDiscSecondDegre()));
		
		if (o.cSpecialiteAtos()!=null && m.getSpecialiteAtosRepository().fetchCSpecialiteAtos(o.cSpecialiteAtos())==null)
			errorMessages.add(String.format(ERREUR_CODE_ATOS,o.cSpecialiteAtos()));
		
		if (o.cSpecialiteItarf()!=null && m.getSpecialiteItarfRepository().fetchCSpecialiteItarf(o.cBap(),o.cSpecialiteItarf())==null)
			errorMessages.add(String.format(ERREUR_CODE_ITARF,o.cBap(),o.cSpecialiteItarf()));

		if (errorMessages.size()>0)
			return new ResultatControle(false, errorMessages);
		
		return ResultatControle.RESULTAT_OK;
	}

	public List<TypeControle> getTypes() {
		return Arrays.asList(LISTE_TYPES_CONTROLES);
	}
}
