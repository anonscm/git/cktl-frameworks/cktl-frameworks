package org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.IDonnee;

public interface ISpecialiteItarf extends IDonnee{
	String cSpecialiteItarf();
}
