package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import java.util.Arrays;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IControle;
import org.cocktail.connecteur.common.metier.controles.IControleService;
import org.cocktail.connecteur.common.metier.controles.ResultatControle;
import org.cocktail.connecteur.common.metier.controles.TypeControle;
import org.cocktail.connecteur.common.metier.controles.periode.Periode;
import org.cocktail.connecteur.common.metier.controles.periode.PeriodeTypeControles;
import org.cocktail.connecteur.common.metier.repositories.MainManager;

public class CarriereSpecialisationControlePeriode implements IControle<ICarriereSpecialisation> {
	private static final TypeControle[] LISTE_TYPES_CONTROLES = {CarriereSpecialisationTypeControle.VALIDATION_ATTRIBUT};

	public boolean checkable(ICarriereSpecialisation objAControler) {
		return true;
	}
	
	public ResultatControle check(ICarriereSpecialisation objAControler) {
		IControleService<Periode> periodeControleService=MainManager.instance().getManagerPeriode().getControleService();
		
		List<ResultatControle> results=periodeControleService.appliqueControlesDeType(objAControler.periodeEffet(), PeriodeTypeControles.VALIDATION_ATTRIBUT, true);
		return ResultatControle.mergeListeEnUnResultat(results);
	}

	public List<TypeControle> getTypes() {
		return Arrays.asList(LISTE_TYPES_CONTROLES);
	}

}
