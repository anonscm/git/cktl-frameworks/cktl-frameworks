/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.connecteur.common.metier.controles;

import java.util.ArrayList;
import java.util.List;

public class ResultatControle {
    
	/**
	 * Résultat OK. Il convient d'utliser cette constante en retour d'un contrôle
	 * afin de minimiser la création d'objet
	 */
    public static final ResultatControle RESULTAT_OK = new ResultatControle(true);

    public static final ResultatControle NON_CHECKABLE = 
            new ResultatControle(false, "Les conditions ne sont pas réunis pour effectuer ce contrôle");

    private List<String> messages;
    private boolean valide = false;
    
    /**
     * Instancie un RésulatControle sans message
     * 
     * @param valide
     */
    private ResultatControle(boolean valide) {
        this.valide = valide;
    }
    
    /**
     * Instancie un RésultatControle avec message
     * 
     * @param valide
     * @param message
     */
    public ResultatControle(boolean valide, String message) {
    	this.messages=new ArrayList<String>();
        this.messages.add(message);
        this.valide = valide;
    }
    
    /**
     * Instancie un RésultatControle avec plusieurs messages
     * 
     * @param valide
     * @param message
     */
    public ResultatControle(boolean valide, List<String> messages) {
        this.messages=messages;
        this.valide = valide;
    }
    
    /**
     * Retourne le message du contrôle
     * 
     * @return Une chaîne de caractère
     */
    public String getMessage() {
    	String texte="";
    	boolean estPremier=false;
    	for (String message : messages) {
    		// On ne rajoute pas le "\n" sur la dernière ligne
    		if (estPremier) {
    			texte=message;
    		} else {
    			texte+="\n"+message;
    		}
    	}
        return texte;
    }
    
    /**
     * Retourne le message du contrôle
     * 
     * @return Une chaîne de caractère
     */
    public List<String> getMessages() {
        return messages;
    }

    /**
     * Retourne l'indicateur de résultat
     * 
     * @return True ou False
     */
    public boolean valide() {
        return valide;
    }    
    
    public static ResultatControle mergeListeEnUnResultat(List<ResultatControle> list) {
    	List<String> messages=new ArrayList<String>();
		boolean auMoinsUnResultatEnErreur=false;
		for (ResultatControle resultat : list) {
			if (!resultat.valide()) {
				messages.addAll(resultat.getMessages());
				auMoinsUnResultatEnErreur=true;
			}
		}
		if (auMoinsUnResultatEnErreur) {
			return new ResultatControle(false, messages);
		} else {
		return ResultatControle.RESULTAT_OK;
		}

    }
}
