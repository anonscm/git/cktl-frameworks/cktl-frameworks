package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IControleService;
import org.cocktail.connecteur.common.metier.controles.ResultatControle;

public interface ICarriereSpecialisationControleService extends IControleService<ICarriereSpecialisation> {
	public List<ResultatControle> appliqueControleAttributs(ICarriereSpecialisation carriereSpecialisation);
}
