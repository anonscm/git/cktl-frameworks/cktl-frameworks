package org.cocktail.connecteur.common.metier.controles.periode;

import java.util.Date;

public class Periode {
	private Date debut;
	private Date fin;
	
	public Periode(Date debut, Date fin) {
		this.debut=debut;
		this.fin=fin;
	}

	public Date getDebut() {
		return debut;
	}

	public Date getFin() {
		return fin;
	}
}
