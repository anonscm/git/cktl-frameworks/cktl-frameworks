package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.ControleServiceImpl;
import org.cocktail.connecteur.common.metier.controles.ResultatControle;

public class CarriereSpecialisationControleServiceImpl extends ControleServiceImpl<ICarriereSpecialisation> implements ICarriereSpecialisationControleService {
	public List<ResultatControle> appliqueControleAttributs(ICarriereSpecialisation carriereSpecialisation) {
		List<ResultatControle> resultats=new ArrayList<ResultatControle>();
		
		resultats.add(appliqueControle(carriereSpecialisation, CarriereSpecialisationControleExisteDansNomenclature.class,true));
		resultats.add(appliqueControle(carriereSpecialisation, CarriereSpecialisationControleUnique.class,false));
		
		return resultats;
	}
}
