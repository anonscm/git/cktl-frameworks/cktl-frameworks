package org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.IDonnee;

public interface IBap extends IDonnee {
	String CODE_NON_RENSEIGNE="00";
	
	String cBap();
	boolean estRenseigne();
}
