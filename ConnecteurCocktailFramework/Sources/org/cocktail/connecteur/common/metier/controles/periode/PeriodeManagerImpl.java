package org.cocktail.connecteur.common.metier.controles.periode;

import org.cocktail.connecteur.common.metier.controles.ControleServiceImpl;
import org.cocktail.connecteur.common.metier.controles.IControleService;
import org.cocktail.connecteur.common.metier.controles.IManager;

public class PeriodeManagerImpl implements IManager<Periode> {
	IControleService<Periode> controleService;
	
	public PeriodeManagerImpl() {
	}
	
	private void initControleService() {
		controleService=new ControleServiceImpl<Periode>();
		
		controleService.ajouteControle(new PeriodeControleDebutFin());
	}
	
	public IControleService<Periode> getControleService() {
		if (controleService==null) {
			initControleService();
		}
		return controleService; 
	}
	
}
