package org.cocktail.connecteur.common.metier.controles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ControleServiceImpl<T> implements IControleService<T> {

	private HashMap<Class,IControle<T>> classeVersControle = new HashMap<Class,IControle<T>>();
	private HashMap<String,List<IControle<T>>> typeVersControles = new HashMap<String,List<IControle<T>>>();

	private ResultatControle appliqueControle(T objAControler,IControle<T> controle) {
		if (controle.checkable(objAControler)) {
			return controle.check(objAControler);
		}
		return ResultatControle.RESULTAT_OK;
	}
	
	private List<ResultatControle> appliqueListeControlesSpecifiques(T objAControler,Collection<IControle<T>> controles) {
		List<ResultatControle> resultats = new ArrayList<ResultatControle>();
		
		for (IControle<T> controle : controles) {
			ResultatControle resultat = appliqueControle(objAControler, controle);
			if (!resultat.valide()) {
				resultats.add(resultat);
			}
		}
		return resultats;
	}
	
	public List<ResultatControle> appliqueListeControles(T objAControler,
			List<Class> controleClasses, boolean erreurSiNonTrouve) {
		List<ResultatControle> resultats = new ArrayList<ResultatControle>();
		
		for (Class classe : controleClasses) {
			ResultatControle resultat=appliqueControle(objAControler,classe,erreurSiNonTrouve);
			if (!resultat.valide()) {
				resultats.add(resultat);
			}
		}
		
		return resultats;
	}

	public List<ResultatControle> appliqueControlesDeType(T objAControler,TypeControle type, boolean erreurSiAucunControleTrouve) {
		List<ResultatControle> resultats;
		List<IControle<T>> controles=getControlesDeType(type);
		if (erreurSiAucunControleTrouve && controles.size()==0) {
			resultats=new ArrayList<ResultatControle>();
			resultats.add(new ResultatControle(false,"Aucun contrôle trouvé pour le type de contrôle '"+type.getNom()+"'"));
		} else {
			resultats=appliqueListeControlesSpecifiques(objAControler,controles);
		}
		return resultats;
	}

	public List<ResultatControle> appliqueTousControles(T objAControler) {
		return appliqueListeControlesSpecifiques(objAControler,getControles());
	}

	public ResultatControle appliqueControle(T objAControler,
			Class controleClass, boolean erreurSiNonTrouve) {
		IControle<T> controle=getControle(controleClass);
		if (controle==null) {
			if (erreurSiNonTrouve) {
				return new ResultatControle (false, "Contrôle '"+controleClass.getName()+"' non chargé");
			} else {
				return ResultatControle.RESULTAT_OK;
			}
		}
		return appliqueControle(objAControler, controle);
	}

	public IControle<T> getControle(Class controleClass) {
		return classeVersControle.get(controleClass);
	}
	
	public List<IControle<T>> getControlesDeType(TypeControle type) {
		List<IControle<T>> controlesDeCeType=typeVersControles.get(type.getNom());
		if (controlesDeCeType==null) {
			controlesDeCeType=new ArrayList<IControle<T>>();
		}
		return controlesDeCeType;
	}

	public Collection<IControle<T>> getControles() {
		return classeVersControle.values();
	}

	public void ajouteControle(IControle<T> controle) {
		// On n'ajoute qu'une seule fois le même type de contrôle
		if (classeVersControle.containsKey(controle.getClass())) {
			return;
		}
		classeVersControle.put(controle.getClass(),controle);
		
		for (TypeControle type : controle.getTypes()) {
			List<IControle<T>> controlesDeCeType=typeVersControles.get(type.getNom());
			if (controlesDeCeType==null) {
				controlesDeCeType=new ArrayList<IControle<T>>();
				typeVersControles.put(type.getNom(), controlesDeCeType);
			}
			controlesDeCeType.add(controle);
		}
	}

	public void setControles(Collection<IControle<T>> controles) {
		classeVersControle.clear();
		for (IControle<T> controle : controles) {
			ajouteControle(controle);
		}		
	}
}
