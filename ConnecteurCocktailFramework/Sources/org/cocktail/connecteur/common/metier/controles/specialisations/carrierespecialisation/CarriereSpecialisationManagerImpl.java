package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import org.cocktail.connecteur.common.metier.controles.IControleService;
import org.cocktail.connecteur.common.metier.controles.IManager;

public class CarriereSpecialisationManagerImpl implements IManager<ICarriereSpecialisation> {
	ICarriereSpecialisationControleService controleService;
	
	public CarriereSpecialisationManagerImpl() {
		
	}
	
	private void initControleService() {
		controleService=new CarriereSpecialisationControleServiceImpl();
		
		controleService.ajouteControle(new CarriereSpecialisationControleExisteDansNomenclature());
		controleService.ajouteControle(new CarriereSpecialisationControleUnique());
		controleService.ajouteControle(new CarriereSpecialisationControlePeriode());
	}
	
	public IControleService<ICarriereSpecialisation> getControleService() {
		if (controleService==null) {
			initControleService();
		}
		return controleService; 
	}
	
}
