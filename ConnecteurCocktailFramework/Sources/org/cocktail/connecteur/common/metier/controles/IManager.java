package org.cocktail.connecteur.common.metier.controles;


public interface IManager<T> {
	/**
	 * Retourne un service pour les contrôles sur un type donné
	 * @return
	 */
	IControleService<T> getControleService();

}
