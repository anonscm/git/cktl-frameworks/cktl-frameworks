package org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.IDonnee;

public interface ICnu extends IDonnee{
	String cSectionCnu();
	String cSousSectionCnu();
}
