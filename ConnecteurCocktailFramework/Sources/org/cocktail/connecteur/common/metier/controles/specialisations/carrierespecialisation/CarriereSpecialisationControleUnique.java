package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import java.util.Arrays;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IControle;
import org.cocktail.connecteur.common.metier.controles.ResultatControle;
import org.cocktail.connecteur.common.metier.controles.TypeControle;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;

public class CarriereSpecialisationControleUnique implements IControle<ICarriereSpecialisation> {
	private static final String ERROR_MESSAGE = "Nombre de spécialités (%d) supérieur à 1";
	private static final TypeControle[] LISTE_TYPES_CONTROLES = {CarriereSpecialisationTypeControle.VALIDATION_ATTRIBUT};

	public boolean checkable(ICarriereSpecialisation objAControler) {
		return true;
	}

	private int compteNbSpecialite(ICarriereSpecialisation carriereSpec) {
		int nbSpecialite=0;
		IBap bap=carriereSpec.getBap();
		if (bap!=null && bap.estRenseigne())
			// Si Itarf est vide, alors la spécialité est de type bap, si itarf non vide alors c'est une spécialité itarf.
			//		Dans les deux cas, cela ne compte que pour une spécialité 
			nbSpecialite++;
		
		if (carriereSpec.getCnu()!=null)
			nbSpecialite++;
		
		if (carriereSpec.getDisciplineSecondDegre()!=null)
			nbSpecialite++;
		
		if (carriereSpec.getEmploi()!=null)
			nbSpecialite++;

		if (carriereSpec.getSpecialiteAtos()!=null)
			nbSpecialite++;
		
		return nbSpecialite;
	}
	
	public ResultatControle check(ICarriereSpecialisation objAControler) {
		int nbSpecialite=compteNbSpecialite(objAControler);
		
		if (nbSpecialite>1) {
			return new ResultatControle(false, String.format(ERROR_MESSAGE,nbSpecialite));
		}
		return ResultatControle.RESULTAT_OK;
	}

	public List<TypeControle> getTypes() {
		return Arrays.asList(LISTE_TYPES_CONTROLES);
	}

}
