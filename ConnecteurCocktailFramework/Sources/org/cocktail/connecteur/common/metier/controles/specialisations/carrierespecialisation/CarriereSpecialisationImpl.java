package org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation;

import java.util.Date;

import org.cocktail.connecteur.common.metier.controles.periode.Periode;
import org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation.ICarriereSpecialisation;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ICnu;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IDisciplineSecondDegre;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IReferensEmploi;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteAtos;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteItarf;
import org.cocktail.connecteur.common.metier.repositories.MainManager;

public class CarriereSpecialisationImpl implements ICarriereSpecialisation {
	private String codeemploi;
	private String cBap;
	private String cDiscSecondDegre;
	private String cSpecialiteItarf;
	private String cSpecialiteAtos;
	private String cSectionCnu;
	private String cSousSectionCnu;
	private Date debut;
	private Date fin;
	
	public String codeemploi() {
		return codeemploi;
	}

	public String cBap() {
		return cBap;
	}

	public String cDiscSecondDegre() {
		return cDiscSecondDegre;
	}

	public String cSpecialiteItarf() {
		return cSpecialiteItarf;
	}

	public String cSpecialiteAtos() {
		return cSpecialiteAtos;
	}

	public String cSectionCnu() {
		return cSectionCnu;
	}

	public String cSousSectionCnu() {
		return cSousSectionCnu;
	}

	public void setCodeemploi(String codeemploi) {
		this.codeemploi=codeemploi;
	}

	public void setCBap(String codeBap) {
		this.cBap=codeBap;// TODO Auto-generated method stub
	}

	public void setCDiscSecondDegre(String codeDisciplineSecondDegre) {
		this.cDiscSecondDegre=codeDisciplineSecondDegre;
	}

	public void setCSpecialiteItarf(String specialiteItarf) {
		this.cSpecialiteItarf=specialiteItarf;		
	}

	public void setCSpecialiteAtos(String specialiteAtos) {
		this.cSpecialiteAtos=specialiteAtos;
	}

	public void setCSectionCnu(String codeSectionCnu) {
		this.cSectionCnu=codeSectionCnu;
	}

	public void setCSousSectionCnu(String codeSousSectionCnu) {
		this.cSousSectionCnu=codeSousSectionCnu;
	}

	public IBap getBap() {
		return MainManager.instance().getBapRepository().fetchCBap(cBap());
	}

	public IDisciplineSecondDegre getDisciplineSecondDegre() {
		return MainManager.instance().getDiscSecondDegreRepository().fetchCDiscSecondDegre(cDiscSecondDegre());
	}

	public ICnu getCnu() {
		return MainManager.instance().getCnuRepository().fetchSectionCnuSousSectionCnu(cSectionCnu(),cSousSectionCnu());
	}

	public IReferensEmploi getEmploi() {
		return MainManager.instance().getReferensEmploiRepository().fetchCodeEmploi(codeemploi());
	}

	public ISpecialiteItarf getSpecialiteItarf() {
		return MainManager.instance().getSpecialiteItarfRepository().fetchCSpecialiteItarf(cBap(),cSpecialiteItarf());
	}

	public ISpecialiteAtos getSpecialiteAtos() {
		return MainManager.instance().getSpecialiteAtosRepository().fetchCSpecialiteAtos(cSpecialiteAtos());
	}

	public Date dDebut() {
		return debut;
	}

	public Date dFin() {
		return fin;
	}

	public void setDDebut(Date debut) {
		this.debut=debut;
	}

	public void setDFin(Date fin) {
		this.fin=fin;
	}

	public Periode periodeEffet() {
		return new Periode(dDebut(), dFin());
	}
}
