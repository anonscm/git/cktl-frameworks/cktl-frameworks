package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IReferensEmploi;
import org.cocktail.connecteur.common.metier.repositories.IRepository;

public interface IReferensEmploiRepository extends IRepository<IReferensEmploi> {
	IReferensEmploi fetchCodeEmploi(String codeemploi);
}
