package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ICnu;
import org.cocktail.connecteur.common.metier.repositories.IRepository;

public interface ICnuRepository extends IRepository<ICnu> {
	ICnu fetchSectionCnuSousSectionCnu(String sectionCnu, String sousSectionCnu);
}
