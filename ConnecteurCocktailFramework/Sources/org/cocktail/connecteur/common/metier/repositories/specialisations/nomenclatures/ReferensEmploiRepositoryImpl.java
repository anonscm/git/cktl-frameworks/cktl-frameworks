package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IReferensEmploi;
import org.cocktail.connecteur.common.metier.repositories.RepositoryImpl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOReferensEmplois;

import com.webobjects.eocontrol.EOEditingContext;

public class ReferensEmploiRepositoryImpl extends RepositoryImpl<IReferensEmploi> implements IReferensEmploiRepository {
	public ReferensEmploiRepositoryImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	@Override
	public List<IReferensEmploi> fetchAll() {
		return new ArrayList<IReferensEmploi>(EOReferensEmplois.fetchAllReferensEmploises(editingContext()));
	}

	public IReferensEmploi fetchCodeEmploi(String codeemploi) {
		return depuisClePrimaire(codeemploi);
	}
}
