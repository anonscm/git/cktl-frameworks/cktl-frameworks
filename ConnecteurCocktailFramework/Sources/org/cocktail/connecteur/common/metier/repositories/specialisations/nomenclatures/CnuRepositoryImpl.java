package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IDonnee;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ICnu;
import org.cocktail.connecteur.common.metier.repositories.RepositoryImpl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;

import com.webobjects.eocontrol.EOEditingContext;

public class CnuRepositoryImpl extends RepositoryImpl<ICnu> implements ICnuRepository {
	public CnuRepositoryImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	@Override
	public List<ICnu> fetchAll() {
		return new ArrayList<ICnu>(EOCnu.fetchAllCnus(editingContext()));
	}

	public ICnu fetchSectionCnuSousSectionCnu(String sectionCnu, String sousSectionCnu) {
		return depuisClePrimaire(sectionCnu+IDonnee.SEPARATEUR_CLE_PRIMAIRE+sousSectionCnu);
	}
}
