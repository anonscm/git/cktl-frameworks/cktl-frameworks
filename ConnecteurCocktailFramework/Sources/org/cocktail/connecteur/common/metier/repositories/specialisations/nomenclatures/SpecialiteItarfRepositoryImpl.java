package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IDonnee;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteItarf;
import org.cocktail.connecteur.common.metier.repositories.RepositoryImpl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf;

import com.webobjects.eocontrol.EOEditingContext;

public class SpecialiteItarfRepositoryImpl extends RepositoryImpl<ISpecialiteItarf> implements ISpecialiteItarfRepository {
	public SpecialiteItarfRepositoryImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	@Override
	public List<ISpecialiteItarf> fetchAll() {
		return new ArrayList<ISpecialiteItarf>(EOSpecialiteItarf.fetchAllSpecialiteItarfs(editingContext()));
	}

	public ISpecialiteItarf fetchCSpecialiteItarf(String cBap, String cSpecialiteItarf) {
		return depuisClePrimaire(cBap+IDonnee.SEPARATEUR_CLE_PRIMAIRE+cSpecialiteItarf);
	}
}
