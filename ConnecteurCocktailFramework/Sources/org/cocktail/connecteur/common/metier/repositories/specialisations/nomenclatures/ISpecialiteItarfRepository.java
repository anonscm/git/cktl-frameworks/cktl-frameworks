package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteItarf;
import org.cocktail.connecteur.common.metier.repositories.IRepository;

public interface ISpecialiteItarfRepository extends IRepository<ISpecialiteItarf> {
	ISpecialiteItarf fetchCSpecialiteItarf(String cBap, String cSpecialiteItarf);
}
