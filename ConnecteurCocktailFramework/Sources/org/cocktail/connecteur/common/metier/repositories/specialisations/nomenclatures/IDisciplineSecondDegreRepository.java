package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IDisciplineSecondDegre;
import org.cocktail.connecteur.common.metier.repositories.IRepository;

public interface IDisciplineSecondDegreRepository extends IRepository<IDisciplineSecondDegre> {
	IDisciplineSecondDegre fetchCDiscSecondDegre(String cDiscSecondDegre);
}
