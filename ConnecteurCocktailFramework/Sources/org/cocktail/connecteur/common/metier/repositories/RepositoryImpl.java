package org.cocktail.connecteur.common.metier.repositories;

import java.util.HashMap;
import java.util.List;

import org.cocktail.common.LogManager;
import org.cocktail.connecteur.common.metier.controles.IDonnee;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteItarf;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.SpecialiteItarfRepositoryImpl;

import com.webobjects.eocontrol.EOEditingContext;

public abstract class RepositoryImpl<T extends IDonnee> {
	private EOEditingContext edc;
	private HashMap<String,T> tousElements=null;
	private boolean allBapsInitialise=false;
	
	public RepositoryImpl(EOEditingContext editingContext) {
		tousElements=new HashMap<String, T>();
		edc=editingContext;
	}
	
	private void initTousElementSiNecessaire() {
		if (allBapsInitialise)
			return;

		if (edc==null) {
			LogManager.logErreur("Erreur: BapRepositoryImpl.initAllBapSiNecessaire - Editing context null");
			return;
		}
		
		tousElements=new HashMap<String, T>();
		for (T element: fetchAll()) {
			tousElements.put(element.clePrimaire(), element);
		}
		allBapsInitialise=true;
	}
	
	protected abstract List<T> fetchAll();
	
	protected EOEditingContext editingContext() {
		return edc;
	};

	public T depuisClePrimaire(String cle) {
		initTousElementSiNecessaire();
		
		return tousElements.get(cle);
	}

	public boolean clePrimaireExiste(String cle) {
		initTousElementSiNecessaire();
		
		return tousElements.containsKey(cle);
	}

}
