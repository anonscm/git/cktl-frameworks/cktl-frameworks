package org.cocktail.connecteur.common.metier.repositories;

import java.util.List;

public interface IRepository<T> {
	T depuisClePrimaire(String cle);
	
	boolean clePrimaireExiste(String cle);
	
	List<T> fetchAll();

}
