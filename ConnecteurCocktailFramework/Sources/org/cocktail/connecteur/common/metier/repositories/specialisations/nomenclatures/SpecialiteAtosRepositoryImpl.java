package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteAtos;
import org.cocktail.connecteur.common.metier.repositories.RepositoryImpl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteAtos;

import com.webobjects.eocontrol.EOEditingContext;

public class SpecialiteAtosRepositoryImpl extends RepositoryImpl<ISpecialiteAtos> implements ISpecialiteAtosRepository {
	public SpecialiteAtosRepositoryImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	@Override
	public List<ISpecialiteAtos> fetchAll() {
		return new ArrayList<ISpecialiteAtos>(EOSpecialiteAtos.fetchAllSpecialiteAtoses(editingContext()));
	}

	public ISpecialiteAtos fetchCSpecialiteAtos(String cSpecialiteAtos) {
		return depuisClePrimaire(cSpecialiteAtos);
	}
}
