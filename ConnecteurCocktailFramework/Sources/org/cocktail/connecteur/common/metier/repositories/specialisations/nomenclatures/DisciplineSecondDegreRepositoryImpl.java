package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IDisciplineSecondDegre;
import org.cocktail.connecteur.common.metier.repositories.RepositoryImpl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EODiscSecondDegre;

import com.webobjects.eocontrol.EOEditingContext;

public class DisciplineSecondDegreRepositoryImpl extends RepositoryImpl<IDisciplineSecondDegre> implements IDisciplineSecondDegreRepository {
	public DisciplineSecondDegreRepositoryImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	@Override
	public List<IDisciplineSecondDegre> fetchAll() {
		return new ArrayList<IDisciplineSecondDegre>(EODiscSecondDegre.fetchAllDiscSecondDegres(editingContext()));
	}

	public IDisciplineSecondDegre fetchCDiscSecondDegre(String cDiscSecondDegre) {
		return depuisClePrimaire(cDiscSecondDegre);
	}
}
