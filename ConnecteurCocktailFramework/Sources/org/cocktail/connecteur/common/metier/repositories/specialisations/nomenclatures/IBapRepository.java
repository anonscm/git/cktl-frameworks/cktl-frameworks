package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.repositories.IRepository;

public interface IBapRepository extends IRepository<IBap> {
	IBap fetchCBap(String cBap);
}
