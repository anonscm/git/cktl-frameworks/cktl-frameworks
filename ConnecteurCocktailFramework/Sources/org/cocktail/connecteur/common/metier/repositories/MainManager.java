package org.cocktail.connecteur.common.metier.repositories;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.metier.controles.IManager;
import org.cocktail.connecteur.common.metier.controles.periode.Periode;
import org.cocktail.connecteur.common.metier.controles.periode.PeriodeManagerImpl;
import org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation.CarriereSpecialisationManagerImpl;
import org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation.ICarriereSpecialisation;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.BapRepositoryImpl;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.CnuRepositoryImpl;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.DisciplineSecondDegreRepositoryImpl;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.IBapRepository;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.ICnuRepository;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.IDisciplineSecondDegreRepository;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.IReferensEmploiRepository;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.ISpecialiteAtosRepository;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.ISpecialiteItarfRepository;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.ReferensEmploiRepositoryImpl;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.SpecialiteAtosRepositoryImpl;
import org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures.SpecialiteItarfRepositoryImpl;

import com.webobjects.eocontrol.EOEditingContext;

public class MainManager {
	static private MainManager instance=null;
	private EOEditingContext editingContext=null;

	private IManager<ICarriereSpecialisation> managerCarriereSpecialisation;
	private IManager<Periode> managerPeriode=new PeriodeManagerImpl();

	private IBapRepository bapRepository;
	private ICnuRepository cnuRepository;
	private IDisciplineSecondDegreRepository discSecondDegreRepository;
	private IReferensEmploiRepository referensEmploiRepository;
	private ISpecialiteAtosRepository specialiteAtosRepository;
	private ISpecialiteItarfRepository specialiteItarfRepository;
	

	public static void init(EOEditingContext edc) {
		instance=new MainManager(edc);
	}
	
	public MainManager(EOEditingContext edc) {
		setEditingContext(edc);
	}
	
	public static MainManager instance() {
		// On suppose que la fonction init a déjà été lancé avec le bon Editing Context
		//   Personne ne peut donc récupérer une instance de MainManager sans Editing Context
		//	 Cela permet de pouvoir récupérer des repository sans savoir ce qu'est un EOEditingContext
		if (instance==null)
			LogManager.logErreur("Erreur: MainManager.instance - manager non initialisé");
		return instance;
	}

	public IManager<ICarriereSpecialisation> getManagerCarriereSpecialisation() {
		if (managerCarriereSpecialisation==null)
			managerCarriereSpecialisation = new CarriereSpecialisationManagerImpl();

		return managerCarriereSpecialisation;
	}

	public IManager<Periode> getManagerPeriode() {
		if (managerPeriode==null)
			managerPeriode=new PeriodeManagerImpl();
		
		return managerPeriode;
	}
	
	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	private void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	public IBapRepository getBapRepository() {
		if (bapRepository==null) {
			bapRepository=new BapRepositoryImpl(editingContext);
		}
		return bapRepository;
	}

	public ICnuRepository getCnuRepository() {
		if (cnuRepository==null) {
			cnuRepository=new CnuRepositoryImpl(editingContext);
		}
		return cnuRepository;
	}

	public IDisciplineSecondDegreRepository getDiscSecondDegreRepository() {
		if (discSecondDegreRepository==null) {
			discSecondDegreRepository=new DisciplineSecondDegreRepositoryImpl(editingContext);
		}
		return discSecondDegreRepository;
	}

	public IReferensEmploiRepository getReferensEmploiRepository() {
		if (referensEmploiRepository==null) {
			referensEmploiRepository=new ReferensEmploiRepositoryImpl(editingContext);
		}
		return referensEmploiRepository;
	}

	public ISpecialiteAtosRepository getSpecialiteAtosRepository() {
		if (specialiteAtosRepository==null) {
			specialiteAtosRepository=new SpecialiteAtosRepositoryImpl(editingContext);
		}
		return specialiteAtosRepository;
	}

	public ISpecialiteItarfRepository getSpecialiteItarfRepository() {
		if (specialiteItarfRepository==null) {
			specialiteItarfRepository=new SpecialiteItarfRepositoryImpl(editingContext);
		}
		return specialiteItarfRepository;
	}
	
	
}
