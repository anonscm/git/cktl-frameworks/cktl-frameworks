package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.repositories.RepositoryImpl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;

import com.webobjects.eocontrol.EOEditingContext;

public class BapRepositoryImpl extends RepositoryImpl<IBap> implements IBapRepository {
	public BapRepositoryImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	@Override
	public List<IBap> fetchAll() {
		return new ArrayList<IBap>(EOBap.fetchAllBaps(editingContext()));
	}

	public IBap fetchCBap(String cBap) {
		return EOBap.getFromCode(editingContext(), cBap);
	}
}
