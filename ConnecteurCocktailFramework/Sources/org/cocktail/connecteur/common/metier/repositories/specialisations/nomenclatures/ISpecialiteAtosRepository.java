package org.cocktail.connecteur.common.metier.repositories.specialisations.nomenclatures;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteAtos;
import org.cocktail.connecteur.common.metier.repositories.IRepository;

public interface ISpecialiteAtosRepository extends IRepository<ISpecialiteAtos> {
	ISpecialiteAtos fetchCSpecialiteAtos(String cSpecialiteAtos);
}
