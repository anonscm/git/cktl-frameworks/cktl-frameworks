//ULRUtilities.java
package org.cocktail.connecteur.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.cocktail.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedClassDescription;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOImageView;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class CocktailUtilities 
{

	private static final String LNX_CMD = "evince ";
	private static final String MAC_CMD = "open ";


	private static final	Color COULEUR_FOND_ACTIF = Color.white;
	private static final	Color COULEUR_FOND_INACTIF = new Color(222,222,222);
	//	private static final	Color COULEUR_FOND_INACTIF = new Color(255,0,0);

	private static final	Color COULEUR_TEXTE_ACTIF = Color.black;
	private static final	Color COULEUR_TEXTE_INACTIF = Color.black;

	private static final 	EOClientResourceBundle lesRessources = new EOClientResourceBundle(); 

	private static JTextField myTextField;

	/**
	 * 
	 * @param unPath
	 * @return
	 */
	public static boolean verifierPath(String unPath) {
		File file = new File(unPath);
		return (file.exists());		
	}
	/**
	 * 
	 * @param unPath
	 */
	public static void verifierPathEtCreer(String unPath) {
		File file = new File(unPath);
		if (file.exists() == false) {
			boolean result = file.mkdirs();
			LogManager.logDetail("creation du dir d'impression " + result);
		}
	}
	
	/** Retourne le qualifier pour d&eacute;terminer les objets valides pour une p&eacute;riode
	 * @param champDateDebut	nom du champ de date debut sur lequel construire le qualifier
	 * @param debutPeriode	date de d&eacute;but de periode (ne peut pas &ecirc;tre nulle)
	 * @param champDateFin	nom du champ de date fin sur lequel construire le qualifier
	 * @param finPeriode	date de fin de periode
	 * @return qualifier construit ou null si date debut est nulle
	 */
	public static EOQualifier qualifierPourPeriode(String champDateDebut,NSTimestamp debutPeriode,String champDateFin,NSTimestamp finPeriode) {
		if (debutPeriode == null) {
			return null;
		}
		String stringQualifier = "";
		NSMutableArray args = new NSMutableArray(debutPeriode);
		// champDateFin = nil or champDateFin >= debutPeriode
		stringQualifier = "(" + champDateFin + " = nil OR " + champDateFin + " >= %@)";
		if (finPeriode != null) {
			args.addObject(finPeriode);
			// champDateDebut <= finPeriode
			stringQualifier = stringQualifier + " AND (" + champDateDebut + " = nil OR " + champDateDebut + " <= %@)";
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
	}



	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {

		EODistributedClassDescription description = (EODistributedClassDescription) EOClassDescription.classDescriptionForEntityName(entity);
		EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);

		return object;

	}

	public static BigDecimal appliquerPourcentage(BigDecimal valeur, BigDecimal pourcentage) {

		BigDecimal retour =  ( valeur.multiply(pourcentage)) .divide(new BigDecimal(100));

		return retour.setScale(2, BigDecimal.ROUND_HALF_UP);

	}

	/**
	 * Methode qui <b>tente</b> de contourner le bug EOF qui se produit lors
	 * d'un saveChanges avec l'erreur "reentered responseToMessage()".<br>
	 * <b>Il faut appeler cette methode avant de creer un descendant d'EOCustomObject, donc bien avant le saveChanges()</b><br>
	 *
	 * Le principe est d'appeler la methode EOClassDescription.classDescriptionForEntityName("A") pour chaque relation de l'objet qu'on va creer.
	 * Il faut appeler cette methode avant de creer un objet, au lancement de l'application par exemple, sur toutes les entites du modele.
	 * Par exemple dans le cas d'un objet Facture qui a des objets Ligne, appeler EOClassDescription.classDescriptionForEntityName("Facture")
	 * avant de creer un objet Ligne.
	 * Repeter l'operation pour toutes les relations de l'objet.
	 *
	 * @param list Liste de String identifiant une entite du modele.
	 * @see http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html
	 */
	public static final void fixWoBug_responseToMessage(final String[] list) {
		for (int i = 0; i < list.length; i++) {
			EOClassDescription.classDescriptionForEntityName(list[i]);
		}
	} 

	public static NSTimestamp debutAnneeUniversitaire(NSTimestamp dateReference) {
		Integer mois = new Integer(DateCtrl.getMonth(dateReference));
		Integer annee = new Integer(DateCtrl.getYear(dateReference));
		if (mois < 9 )
			annee = new Integer(annee.intValue() - 1);

		return DateCtrl.stringToDate("01/09/"+annee.toString());
	}
	public static NSTimestamp finAnneeUniversitaire(NSTimestamp dateReference) {
		Integer mois = new Integer(DateCtrl.dateToString(dateReference, "%m"));
		Integer annee = new Integer(DateCtrl.dateToString(dateReference, "%Y"));
		if (mois > 9 )
			annee = new Integer(annee.intValue() + 1);

		return DateCtrl.stringToDate("31/08/"+annee.toString());
	}
	public static NSTimestamp debutAnneeCivile(Integer annee) {		
		return DateCtrl.stringToDate("01/01/"+annee);
	}
	public static NSTimestamp debutAnneeCivile(NSTimestamp dateReference) {		
		return DateCtrl.stringToDate("01/01/"+DateCtrl.getYear(dateReference));
	}
	public static NSTimestamp finAnneeCivile(NSTimestamp dateReference) {
		return DateCtrl.stringToDate("31/12/"+DateCtrl.getYear(dateReference));
	}
	public static NSTimestamp finAnneeCivile(Integer annee) {		
		return DateCtrl.stringToDate("31/12/"+annee);
	}

	public static void setFocusOn(JTextField textField) {
		textField.setRequestFocusEnabled(true);
		textField.requestFocus();
	}

	public static String getErrorDialog(Exception e) {
		e.printStackTrace();

		String text = e.getMessage();

		if ((text == null) || (text.trim().length() == 0)) {
			System.out.println("ERREUR... Impossible de recuperer le message...");
			e.printStackTrace();
			text = "ERREUR. Impossible de récupérer le message, il doit etre accessible dans la console...";
		}
		else {
			String[] msgs = text.split("ORA-20001:");
			if (msgs.length > 1) {
				text = msgs[1].split("\n")[0];
			}
		}
		return text;
	}


	public static void changerFontForLabel(JLabel textField,int style) {
		Font font = textField.getFont();
		textField.setFont(new Font(font.getName(),style,font.getSize()));
	}



	public static BigDecimal getBigDecimalFromField(JTextField textField) {
		try {
			String value = NSArray.componentsSeparatedByString(textField.getText(), ",").componentsJoinedByString(".");
			return new BigDecimal(value);
		}
		catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	} 
	public static Double getDoubleFromField(JTextField textField) {
		try {
			String value = NSArray.componentsSeparatedByString(textField.getText(), ",").componentsJoinedByString(".");
			return new Double(value);
		}
		catch (Exception e) {
			return null;
		}
	} 
	public static Integer getIntegerFromField(JTextField textField) {
		try {
			return new Integer(textField.getText());
		}
		catch (Exception e) {
			return null;
		}
	} 
	public static String getTextFromField(JTextField textField) {
		if (!StringCtrl.chaineVide(textField.getText() ))
			return textField.getText();
		return null;
	} 
	public static String getTextFromArea(JTextArea textField) {
		if (!StringCtrl.chaineVide(textField.getText() ))
			return textField.getText();
		return null;
	} 
	public static NSTimestamp getDateFromField(JTextField field) {
		try {
			return DateCtrl.stringToDate(field.getText());
		}
		catch (Exception e) {
			return null;			
		}
	}

	public static void setTextToArea(JTextArea area, String texte) {
		if (texte == null)
			area.setText("");
		else {
			area.setText(texte);
		}		
	} 
	public static void setTextToField(JTextField textfield, String texte) 
	{
		if (texte == null)
			textfield.setText("");
		else {

			textfield.setText(texte);
			textfield.moveCaretPosition(0);
			BoundedRangeModel tmpBoundedRangeModel = textfield.getHorizontalVisibility();       
			if (tmpBoundedRangeModel.getMaximum()>textfield.getWidth() ) {
				textfield.setToolTipText(texte);
			}
			else {
				textfield.setToolTipText(null);
			}
		}		
	} 
	public static void setDateToField(JTextField textfield, NSTimestamp date) {
		if (date == null)
			textfield.setText("");
		else {
			textfield.setText(DateCtrl.dateToString(date));
		}		
	} 
	public static void setDateToField(JTextField textfield, NSTimestamp date, String format) {
		if (date == null)
			textfield.setText("");
		else {
			textfield.setText(DateCtrl.dateToString(date, format));
		}		
	}
	public static void setNumberToField(JTextField textfield, Number myNumber) {
		if (myNumber == null)
			textfield.setText("");
		else {
			textfield.setText(myNumber.toString());
		}		
	} 
	public static void setTextToLabel(JLabel label, String texte) {
		if (texte == null)
			label.setText("");
		else {
			label.setText(texte);
		}		
	}

	public static void viderLabel(JLabel label)  {
		label.setText("");
	}
	public static void viderTextField(JTextField textfield)  {
		textfield.setText("");
	}
	public static void viderTextArea(JTextArea textArea)  {
		textArea.setText("");
	}

	public static void setImageForImageView(String nomImage, EOImageView laVue)
	{
		laVue.setImageScaling(EOImageView.ScaleToFit);

		if (nomImage != null)
			laVue.setImage(((ImageIcon)lesRessources.getObject(nomImage)).getImage());
		else
			laVue.setImage(null);
	}


	/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
	public static void initTextField(JTextField leChamp, boolean clean, boolean actif)
	{
		if (actif)
		{
			leChamp.setForeground(COULEUR_TEXTE_ACTIF);
			leChamp.setBackground(COULEUR_FOND_ACTIF);
		}
		else
		{
			leChamp.setForeground(COULEUR_TEXTE_INACTIF);
			leChamp.setBackground(COULEUR_FOND_INACTIF);
		}

		if (clean)
			leChamp.setText(null);

		leChamp.setEditable(actif);
	}

	/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
	public static void initTextArea(JTextArea leChamp, boolean clean, boolean actif)
	{
		if (actif)
		{
			leChamp.setForeground(COULEUR_TEXTE_ACTIF);
			leChamp.setBackground(COULEUR_FOND_ACTIF);
		}
		else
		{
			leChamp.setForeground(COULEUR_TEXTE_INACTIF);
			leChamp.setBackground(COULEUR_FOND_INACTIF);
		}

		if (clean)
			leChamp.setText(null);

		leChamp.setEditable(actif);
	}

	/**
	 * 
	 * @param lePopup
	 */
	public static void initPopupAvecListe(JComboBox lePopup, NSArray liste, boolean selectionVide) {

		lePopup.removeAllItems();

		if (selectionVide)
			lePopup.addItem("");

		for (int i=0;i < liste.size();i++)
			lePopup.addItem(liste.objectAtIndex(i));

	}
	public static void initPopupAvecListeString(JComboBox lePopup, String[] liste, boolean selectionVide) {

		lePopup.removeAllItems();

		if (selectionVide)
			lePopup.addItem("");

		for (int i=0;i < liste.length;i++)
			lePopup.addItem(liste[i]);

	}
	/**
	 * 
	 * @param lePopup
	 */
	public static void initPopupOuiNon(JComboBox lePopup) {

		lePopup.removeAllItems();
		lePopup.addItem("*");
		lePopup.addItem(CocktailConstantes.VRAI);
		lePopup.addItem(CocktailConstantes.FAUX);

	}

	/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
	public static void initPopup(JComboBox lePopup, boolean clean, boolean actif) {
		if (actif)
		{
			lePopup.setForeground(COULEUR_TEXTE_ACTIF);
			lePopup.setBackground(COULEUR_FOND_ACTIF);
		}
		else
		{
			lePopup.setForeground(COULEUR_TEXTE_INACTIF);
			lePopup.setBackground(COULEUR_FOND_INACTIF);
		}

		if (clean)
			lePopup.removeAllItems();

		lePopup.setEnabled(actif);
	}



	public static void openFile(String filePath) throws Exception {
		File aFile = new File(filePath);
		Runtime runtime = Runtime.getRuntime();
		if (System.getProperty("os.name").startsWith("Windows")) {
			runtime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\"" });
		}
		else
			if (System.getProperty("os.name").startsWith("Linux")) {
				runtime.exec(LNX_CMD + aFile);
			}
			else {
				runtime.exec(MAC_CMD + aFile);
			}

	}

	/**
	 * 
	 * @param URL
	 * @throws Exception
	 */
	public static void openURL(String URL) throws Exception {

		if (URL.startsWith("http://") == false) {
			URL = "http://" + URL;
			try {
				URL uneUrl = new URL(URL);
			} catch (Exception e) {
				throw new Exception("URL invalide");
			}
		}
		try {
			String os = System.getProperty("os.name");
			if (os.startsWith("Mac OS")) {
				Runtime.getRuntime().exec(MAC_CMD + URL);
			} else if (os.startsWith("Windows")) {
				Runtime.getRuntime().exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", URL} ); 
			}
			else if (os.startsWith("Linux")) {
				Runtime.getRuntime().exec(LNX_CMD + URL);
			}
		} catch (Exception e) {
			throw new Exception("Impossible d'afficher l'URL "+URL+"\nMESSAGE : " + e.getMessage());	
		}

	}


	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static BigDecimal computeSumForKey(EODisplayGroup eo, String key) {

		try {
			BigDecimal total = new BigDecimal(0);

			int i = 0;
			while (i < eo.displayedObjects().count()) {
				try {
					if (NSDictionary.class.getName().equals(eo.allObjects().objectAtIndex(i).getClass().getName())
							|| NSMutableDictionary.class.getName().equals(eo.displayedObjects().objectAtIndex(i).getClass().getName())
							)	{
						NSDictionary dico = (NSDictionary) eo.displayedObjects().objectAtIndex(i);
						total = total.add((BigDecimal)dico.objectForKey(key));
					}
					else	{
						total = total.add(new BigDecimal(((EOEnterpriseObject) eo.displayedObjects().objectAtIndex(i)).valueForKey(key).toString()));
					}
				}
				catch (Exception e) {
					//e.printStackTrace();
					total = new BigDecimal(0);
					break;
				}
				i = i + 1;
			}

			return total.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		catch (Exception e) {
			return new BigDecimal(0);
		}
	}

	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static BigDecimal computeSumForKey(NSArray myArray, String key) {

		BigDecimal total = new BigDecimal(0);

		int i = 0;
		while (i < myArray.count()) {
			try {
				if (NSDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						)	{
					NSDictionary dico = (NSDictionary) myArray.objectAtIndex(i);
					total = total.add((new BigDecimal(dico.objectForKey(key).toString())));
				}
				else	{
					total = total.add((BigDecimal) ((EOEnterpriseObject) myArray.objectAtIndex(i)).valueForKey(key));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				total = new BigDecimal(0);
				break;
			}
			i = i + 1;
		}

		return total.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static Integer computeSumIntegerForKey(NSArray myArray, String key) {

		Integer total = new Integer(0);

		int i = 0;
		while (i < myArray.count()) {
			try {
				if (NSDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						)	{
					NSDictionary dico = (NSDictionary) myArray.objectAtIndex(i);
					total = total.intValue() + (new Integer(dico.objectForKey(key).toString())).intValue();
				}
				else	{
					total = total.intValue() + ((Integer)((EOEnterpriseObject) myArray.objectAtIndex(i)).valueForKey(key)).intValue();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				total = new Integer(0);
				break;
			}
			i = i + 1;
		}

		return total;
	}

	public static void affecterImageEtTextAuBouton(EOInterfaceController sender, ImageIcon icone, String text, JButton leBouton, String message) 
	{
		leBouton.setIcon(icone); 

		leBouton.setText(text);
		leBouton.setSelected(false);
		leBouton.setFocusPainted(false);
		leBouton.setToolTipText(message);
		leBouton.setDefaultCapable(false);

	}
	public static void putView(EOView maSwapView, Component maView)	{
		maSwapView.getComponent(0).setVisible(false);
		maSwapView.removeAll();
		maSwapView.setLayout(new BorderLayout());
		maSwapView.add(maView,BorderLayout.CENTER);
		maView.setVisible(true);
		maSwapView.validate();
	}

	/** Place une vue dans une autre. La swapView et la vue sont passes en parametres	*/
	public static void putView(EOView maSwapView, EOView maView)
	{
		maSwapView.getComponent(0).setVisible(false);
		maSwapView.removeAll();
		maSwapView.setLayout(new BorderLayout());
		maSwapView.add(maView,BorderLayout.CENTER);
		maView.setVisible(true);
		maSwapView.validate();
	}

	public String returnTempStringName() {
		java.util.Calendar currentTime = java.util.Calendar.getInstance();

		String lastModif = "-"
				+ currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "."
				+ currentTime.get(java.util.Calendar.MONTH) + "."
				+ currentTime.get(java.util.Calendar.YEAR) + "-"
				+ currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h"
				+ currentTime.get(java.util.Calendar.MINUTE) + "m"
				+ currentTime.get(java.util.Calendar.SECOND);

		return lastModif;
	}

	/**
	 * 
	 * @param date1
	 * @param date2
	 * @return Nombre de secondes entre 2 dates
	 */
	public static long ecartMilliSecondesEntre(NSTimestamp date1, NSTimestamp date2) {
		// Recuperation du delai entre l'heure actuelle et l'heure de programmation
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.setTime(date1);
		long flag1 = calendar1.getTimeInMillis();

		GregorianCalendar calendar2 = new GregorianCalendar();
		calendar2.setTime(date2);
		long flag2 = calendar2.getTimeInMillis();

		long delay = flag2 - flag1;

		if (delay < 0) {
			delay = 86400000  + delay;
		}
		
		return delay;

	}	
	
	/**
	 * 
	 * @param date1
	 * @param date2
	 * @return Nombre de secondes entre 2 dates
	 */
	public static long ecartSecondesEntre(NSTimestamp date1, NSTimestamp date2) {
		return (ecartMilliSecondesEntre(date1, date2) / 1000);
	}
	
	/**
	 * 
	 * @param date1
	 * @param date2
	 * @return Nombre de minutes entre 2 dates
	 */
	public static long ecartMinutesEntre(NSTimestamp date1, NSTimestamp date2) {
		return (ecartSecondesEntre(date1, date2) / 60);
	}
	
	public static boolean pathExiste(String unPath) {
		if (unPath == null || unPath.length() == 0) {
			return false;
		}
		File file = new File(unPath);
		return file.exists();
	}


}
