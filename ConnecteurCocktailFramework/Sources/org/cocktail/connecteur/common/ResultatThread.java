/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common;

import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

public class ResultatThread implements NSKeyValueCoding, NSCoding {
	private NSMutableDictionary resultat;
	private NSMutableDictionary diagnostic;
	
	public ResultatThread() {
		resultat = new NSMutableDictionary(2);
		diagnostic = new NSMutableDictionary(3);
	}
	public ResultatThread(NSDictionary resultat,NSDictionary diagnostic) {
		this.resultat = new NSMutableDictionary(resultat);
		this.diagnostic = new NSMutableDictionary(diagnostic);
	}
	/** retourne le diagnostic de la methode invoquee sous la forme d'un dictionnaire **/
	public NSDictionary diagnostic() {
		return diagnostic;
	}
	/** retourne la valeur du resultat */
	public Object valeurResultat() {
		return resultat.objectForKey("valeur");
	}
	/** modifie la valeur du resultat */
	public void setValeurResultat(Object valeur) {
		resultat.setObjectForKey(valeur,"valeur");
	}
	/** retourne le status de l'operation en cours */
	public String status() {
		return (String)diagnostic.objectForKey("status");
	}
	/** retourne le message d'information de l'operation en cours. Peut contenir plusieurs messages separes par
	 * "\n" si l'operation a emis plusieurs messages qui n'ont pas ete transmis */
	public String message() {
		String texte = (String)diagnostic.objectForKey("message");
		diagnostic.removeObjectForKey("message");
		return texte;
	}
	/** retourne le message d'exception declenche pendant l'operation */
	public String exception() {
		return (String)diagnostic.objectForKey("exception");
	}
	/** modifie le status de l'operation en cours */
	public void setStatus(String unTexte) {
		diagnostic.setObjectForKey(unTexte,"status");
	}
	/** modifie le message d'information de l'operation en cours */
	public void setMessage(String unTexte) {
		// concaténer les messages si ils n'ont pas encore été lus
		if (diagnostic.objectForKey("message") != null) {
			diagnostic.setObjectForKey(diagnostic.objectForKey("message") + "\n" + unTexte,"message");
		} else {
			diagnostic.setObjectForKey(unTexte,"message");
		}
	}
	/** modifie le message d'exception de l'operation en cours */
	public void setException(String unTexte) {
		if (unTexte != null) {		
			diagnostic.setObjectForKey(unTexte,"exception");
		}
	}
	public String toString() {
		return "resultat :" + System.getProperty("line.separator") + resultat.toString() + System.getProperty("line.separator") +
			"diagnostic :" + System.getProperty("line.separator") + diagnostic().toString();
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	
	// Interface NSCoding pour le transfert des valeurs entre un client et le serveur
	public Class classForCoder() {
		return ResultatThread.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		coder.encodeObject(resultat);
		coder.encodeObject(diagnostic);
	}
	public static Class decodeClass() {
		return ResultatThread.class;
	}
	public static Object decodeObject(NSCoder coder) {
		NSDictionary resultat = (NSDictionary)coder.decodeObject();
		NSDictionary diagnostic = (NSDictionary)coder.decodeObject();
		return new ResultatThread(resultat,diagnostic);
	}
}