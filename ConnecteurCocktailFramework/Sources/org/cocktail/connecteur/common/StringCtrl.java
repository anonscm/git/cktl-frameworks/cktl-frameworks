/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

// 08/09/2011 - ajout de la méthode chainesIdentiques(String str1, String str2)
public class StringCtrl
{

	/**
	 * Formatte une chaine selon une longueur donnee, avec un caractere et un sens donne
	 * 
	 * Exemple : "TOTO",10,"Z","D" ==> return "TOTOZZZZZZ"
	 * 
	 * @param chaine	Chaine a completer
	 * @param nbCars	Nombre de caracteres en retour
	 * @param carac		Caractere de completion
	 * @param sens		Completion vers la droite (D) ou la gauche (G)
	 * @return strinc completee
	 */
	public static String stringCompletion(String chaine, int nbCars,String carac, String sens) {
		String retour = chaine;

		if (retour.length() > nbCars)
			retour = retour.substring(0,nbCars);

		for (int i=0;i<nbCars - chaine.length();i++) {
			if ("D".equals(sens))
				retour = retour + carac;
			else
				retour = carac + retour;			
		}

		return retour;
	}

	/**
	 * Test si le caractere <i>c</i> est une lettre "de base" (a-z, A-Z).
	 * Il ne doit pas etre une lettre accentue, une chiffre ou un autre caractere
	 * special. 
	 */
	public static boolean isBasicLetter(char c) {
		int numVal = Character.getNumericValue(c);
		return (((Character.getNumericValue('a') <= numVal) &&
				(numVal <= Character.getNumericValue('z'))) ||
				((Character.getNumericValue('A') <= numVal) &&
						(numVal <= Character.getNumericValue('Z'))));
	}

	/**
	 *
	 */
	public static String chaineSansAccents(String chaine) {
		String retour = chaineNonAccentuee(chaine, false);
		if (retour == null) {
			return null;
		} else {
			return retour.toUpperCase();
		}
	}

	/**
	 *
	 */
	public static boolean chaineVide(String chaine)	{
		if (!"".equals(recupererChaine(chaine)))
			return false;

		return true;
	}

	/**
	 * Teste si le caractere <i>c</i> est un chiffre entre 0 et 9.
	 */
	public static boolean isBasicDigit(char c) {
		int numVal = Character.getNumericValue(c);
		return ((Character.getNumericValue('0') <= numVal) &&
				(numVal <= Character.getNumericValue('9')));
	}

	/**
	 * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
	 * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
	 * (<i>isBasicDigit</i>) ou un des caracteres <i>acceptChars</i>.
	 * 
	 * @see #isBasicLetter(char)
	 * @see #isBasicDigit(char)
	 * @see #isAcceptChar(char) 
	 */
	public static boolean isAcceptChar(char c, String acceptChars) {
		boolean rep = isBasicLetter(c);
		if (!rep) rep = isBasicDigit(c);
		if ((!rep) && (acceptChars != null)) {
			for(int i=0; i<acceptChars.length(); i++)
				if (c == acceptChars.charAt(i)) return true;
		}
		return rep;
	}

	/**
	 * Retourne la liste des caracteres acceptes par defaut comme caracteres
	 * legales.
	 * 
	 * <p>Cette implementation renvoie la chaine "._-".
	 * 
	 * @see #isAcceptChar(char)
	 */
	public static String defaultAcceptChars() {
		return "._-";
	}

	/**
	 * Test si la chaine de caracteres est "acceptable". Elle l'est si tous
	 * les caracteres de la chaine sont acceptables (<i>isAcceptChar</i>) :
	 * les lettres "de base", les chiffres et les caracteres acceptes par defaut.
	 * 
	 * @see #isAcceptChar(char)
	 */
	public static boolean isAcceptBasicString(String aString) {
		for(int i=0; i<aString.length(); i++) {
			if (!isAcceptChar(aString.charAt(i))) return false;
		}
		return true;
	}

	/**
	 * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
	 * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
	 * (<i>isBasicDigit</i>) ou un des caracteres supplementaires acceptes par
	 * defaut (<i>defaultAcceptChars</i>).
	 * 
	 * @see #isBasicLetter(char)
	 * @see #isBasicDigit(char)
	 * @see #isAcceptChar(char, String)
	 * @see #defaultAcceptChars() 
	 */
	public static boolean isAcceptChar(char c) {
		return isAcceptChar(c, defaultAcceptChars());
	}

	/**
	 * Convertie la chaine de caracteres <i>s</i> en sa representation "legale".
	 * Tous les caracteres de la chaine doivent etre une lettre latine, un
	 * chiffre ou un des caracteres <i>acceptChars</i>. Chaque caractere
	 * "illegal" est remplace par <i>charToReplace</i>.
	 * 
	 * @see #isAcceptChar(char, String)
	 * @see #toBasicString(String)
	 */
	public static String toBasicString(String s, String acceptChars, char charToReplace) {
		StringBuffer newStr;

		if ((s == null) || (s.length() == 0)) return s;
		newStr = new StringBuffer();
		for(int i=0; i<s.length(); i++) {
			if (isAcceptChar(s.charAt(i), acceptChars))
				newStr.append(s.charAt(i));
			else if (charToReplace != Character.MIN_VALUE)
				newStr.append(charToReplace);
		}
		return newStr.toString();
	}

	/**
	 * Convertie la chaine de caracteres <i>s</i> en sa representation "legale".
	 * Tous les caracteres de la chaine doivent etre une lettre latine, un
	 * chiffre ou un des caracteres acceptes par defaut. Chaque caractere
	 * "illegal" est remplace par <i>charToReplace</i>.
	 * 
	 * <p>Cette methode peut etre utilisee pour changer le nom des fichiers en
	 * leur representation "portable" (pas d'espaces, pas des lettres accentuees,
	 * etc.)</p>
	 * 
	 * @see #isAcceptChar(char, String)
	 * @see #defaultAcceptChars()
	 * @see #toBasicString(String, String, char)
	 */
	public static String toBasicString(String aString) {
		return toBasicString(aString, defaultAcceptChars(), '_');
	}

	/** Remplace la chaine1 par la chaine2 dans chaine */
	public static String replace (String chaine, String chaine1, String chaine2)
	{
		return (NSArray.componentsSeparatedByString(chaine,chaine1)).componentsJoinedByString(chaine2);
	}

	/** GET MOIS : renvoie le numero du mois formatte sur 2 caracteres */
	public static String formatter2Chiffres(int nombre)
	{
		if (nombre < 10)
			return "0" + nombre;

		return "" + nombre;
	}


//	CAPITALIZED STRING
	public static String capitalizedString(String aString)
	{
		if ("".equals(aString))
			return "";

		String debut = (aString.substring(0,1)).toUpperCase();
		String fin = (aString.substring(1,aString.length())).toLowerCase();

		return debut.concat(fin);
	}
	
// CAPITALIZED MULTIPLE STRINGS
	public static String capitalizedSerialStrings(String aString)
	{
		int longueur;
		String resultat = "";
		boolean flagCapital = false;
		
		if ("".equals(aString))
			return "";

		longueur = aString.length();
		
		for (int i=0; i < longueur; i++) {
			if (i == 0) {
				resultat = resultat + (aString.substring(i,i+1)).toUpperCase();
			} else {
				if (aString.substring(i,i+1).equals(" ") || aString.substring(i,i+1).equals("-")) {
					resultat = resultat + (aString.substring(i,i+1)).toLowerCase();
					flagCapital = true;
				} else if (flagCapital) {
					resultat = resultat + (aString.substring(i,i+1)).toUpperCase();
					flagCapital = false;
				} else {
					resultat = resultat + (aString.substring(i,i+1)).toLowerCase();
				}
			}
		}
		
		return resultat;
	}

	public static String recupererChaine(String laChaine)
	{
		if ((laChaine==null) ||  (laChaine == NSKeyValueCoding.NullValue.toString())  || (laChaine.equals("")) || (laChaine.equals("*nil*")) )
			return "";

		return laChaine;
	}

	/**  Supprime tous les espaces en debut de cha&icirc;, les doubles espaces, et autres caract&egrave;res, les "/", les "_"
	 * bizarres, remplace les caract&egrave;res accentues par leur valeur non accentuee
	 *	@param uneChaine
	 *	@param supprimerDoubletTiret true si il faut supprimer les "--" (ils sont legaux dans les noms de famille)
	 *  @param estNom true si il s'agit du nom d'un individu
	 *	@return chaine nettoyee
	 */
	public static String chaineClaire(String uneChaine,boolean supprimerDoubleTiret,boolean estNom) {
		if (uneChaine == null || uneChaine.equals("")) { 
			return uneChaine;
		}
		String resultat = chaineNonAccentuee(uneChaine, supprimerDoubleTiret);

		resultat = resultat.toUpperCase();
		if (!estNom) {
			if (resultat.indexOf("'") >= 0) {
				resultat = resultat.replaceAll("'","");
			}
		}
		if (resultat.indexOf("\"") >= 0) {
			resultat = resultat.replaceAll("\"","");
		}
		if (resultat.indexOf("/") >= 0) {
			resultat = resultat.replaceAll("/","");
		}
		if (resultat.indexOf("°") >= 0) {
			resultat = resultat.replaceAll("°","");
		}
		if (resultat.indexOf(",") >= 0) {
			resultat = resultat.replaceAll(",","");
		}
		if (resultat.indexOf("_") >= 0) {
			resultat = resultat.replaceAll("_","");
		}

		// Supprimer maintenant tous les autres caractères illégaux
		resultat = toBasicString(resultat, " -'", Character.MIN_VALUE);
		return resultat;
	}
	/**  Supprime tous les espaces en debut de cha&icirc;, les doubles espaces, et autres caract&egrave;res, les "/", les "_"
	 * bizarres, remplace les caract&egrave;res accentues par leur valeur non accentuee
	 *	@param uneChaine
	 *	@param supprimerDoubletTiret true si il faut supprimer les "--" (ils sont legaux dans les noms de famille)
	 *	@return chaine nettoyee
	 */
	public static String chaineClaire(String uneChaine,boolean supprimerDoubleTiret) {
		return chaineClaire(uneChaine, supprimerDoubleTiret,false);
	}
	public static boolean estNumerique(String num,boolean uniquementValeurPositive) {
		try {
			num = num.trim();
			int valeur = Integer.valueOf(num).intValue();
			if (uniquementValeurPositive && valeur < 0) {
				return false;
			} else {
				return true;
			}
		} catch(NumberFormatException ex) {
			return false;
		}
	}
	public static int stringToInt(String num, int defaultValue) {
		try {
			num = num.trim();
			return Integer.valueOf(num).intValue();
		} catch(NumberFormatException ex) {
			return defaultValue;
		}
	}

	public static Integer stringToInteger(String num, int defaultValue) {
		try {
			num = num.trim();
			return Integer.valueOf(num);
		} catch(NumberFormatException ex) {
			return new Integer(defaultValue);
		}
	}

	public static boolean  characterIsPresentInString(String chaine,String car)	{
		if ((NSArray.componentsSeparatedByString(chaine, car)).count() > 1)
			return true;

		return false;
	}

	public static boolean containsIgnoreCase(String s, String substring) {
		s = s.toUpperCase();
		substring = substring.toUpperCase();
		return (s.indexOf(substring) >= 0);
	}

	public static boolean startsWithIgnoreCase(String s, String substring) {
		s = s.toUpperCase();
		substring = substring.toUpperCase();
		return s.startsWith(substring);
	}

	public static String getSuffix(String s, String prefix) {
		s = s.toUpperCase();
		prefix = prefix.toUpperCase();
		if (s.startsWith(prefix))
			return s.substring(prefix.length());
		else
			return "";
	}

	public static String get0Int(int number, int digits) {
		String s = String.valueOf(number);
		for(;s.length() < digits; s = "0"+s );
		return s;
	}

	public static String toHttp(String aString) {
		int idx, startIdx;
		startIdx = 0;
		do {
			idx = aString.indexOf(" ", startIdx);
			if (idx != -1) {
				aString = aString.substring(0, idx)+"%20"+aString.substring(idx+1);
				startIdx = idx+3;
			}
		} while(idx != -1);
		return aString;
	}



	// COMPONENTS SEPARATED BY STRING : Renvoie un tableau contenant les chaines separees par le separateur donne
	public static NSArray componentsSeparatedByString(String aString,String separateur)
	{
		NSMutableArray localMutableArray = new NSMutableArray();
		int index;

		while (aString.indexOf(separateur) > -1)
		{
			index = aString.indexOf(separateur);
			localMutableArray.addObject(aString.substring(0,index));
			aString = aString.substring(index+(separateur.length()),aString.length());
		}

		localMutableArray.addObject(aString);
		return localMutableArray;
	}
	public static boolean chainesIdentiques(String str1,String str2) {
		return (str1 == null && str2 == null) || (str1 != null && str2 != null && str1.equals(str2));
	}
	public static String chaineNonAccentuee(String uneChaine,boolean supprimerDoubleTiret) {
		if (uneChaine == null || uneChaine.equals("")) { 
			return uneChaine;
		}
		String resultat = uneChaine;

		// supprimer tous les caractères accentués
		if (resultat.indexOf("ç") >= 0) {
			resultat = resultat.replaceAll("ç","c");
		}
		if (resultat.indexOf("à") >= 0) {
			resultat = resultat.replaceAll("à","a");
		}
		if (resultat.indexOf("é") >= 0) {
			resultat = resultat.replaceAll("é","e");
		}
		if (resultat.indexOf("è") >= 0) {
			resultat = resultat.replaceAll("è","e");
		}
		if (resultat.indexOf("ê") >= 0) {
			resultat = resultat.replaceAll("ê","e");
		}
		if (resultat.indexOf("ë") >= 0) {
			resultat = resultat.replaceAll("ë","e");
		}
		if (resultat.indexOf("î") >= 0) {
			resultat = resultat.replaceAll("î","i");
		}
		if (resultat.indexOf("ï") >= 0) {
			resultat = resultat.replaceAll("ï","i");
		}
		if (resultat.indexOf("ô") >= 0) {
			resultat = resultat.replaceAll("ô","o");
		}

		if (resultat.indexOf("ö") >= 0) {
			resultat = resultat.replaceAll("ö","o");
		}
		if (resultat.indexOf("ò") >= 0) {
			resultat = resultat.replaceAll("ò","o");
		}
		if (resultat.indexOf("û") >= 0) {
			resultat = resultat.replaceAll("û","u");
		}
		if (resultat.indexOf("ü") >= 0) {
			resultat = resultat.replaceAll("ü","u");
		}
		resultat = resultat.trim(); // suppression des espaces, en début et fin de chaîne
		while (resultat.indexOf("  ") >= 0) {
			resultat = resultat.replaceAll("  "," ");
		}
		if (supprimerDoubleTiret) {
			while (resultat.indexOf("--") >= 0) {
				resultat = resultat.replaceAll("--","-");
			}
		}
		// supprimer les autres caractères accentués
		if (resultat.indexOf("Ç") >= 0) {
			resultat = resultat.replaceAll("Ç","C");
		}
		if (resultat.indexOf("À") >= 0) {
			resultat = resultat.replaceAll("À","A");
		}
		if (resultat.indexOf("É") >= 0) {
			resultat = resultat.replaceAll("É","E");
		}
		if (resultat.indexOf("È") >= 0) {
			resultat = resultat.replaceAll("È","E");
		}
		if (resultat.indexOf("Ê") >= 0) {
			resultat = resultat.replaceAll("Ê","E");
		}
		if (resultat.indexOf("Ë") >= 0) {
			resultat = resultat.replaceAll("Ë","E");
		}
		if (resultat.indexOf("Â") >= 0) {
			resultat = resultat.replaceAll("Â","A");
		}
		if (resultat.indexOf("Î") >= 0) {
			resultat = resultat.replaceAll("Î","I");
		}
		if (resultat.indexOf("Ï") >= 0) {
			resultat = resultat.replaceAll("Ï","I");
		}
		if (resultat.indexOf("Ô") >= 0) {
			resultat = resultat.replaceAll("Ô","O");
		}
		if (resultat.indexOf("Ö") >= 0) {
			resultat = resultat.replaceAll("Ö","O");
		}
		if (resultat.indexOf("Û") >= 0) {
			resultat = resultat.replaceAll("Û","U");
		}
		if (resultat.indexOf("Ü") >= 0) {
			resultat = resultat.replaceAll("Ü","U");
		}
		return resultat;
	}

}
