/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.connecteur.common;

import javax.swing.ImageIcon;

import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class CocktailIcones extends EOModalDialogController 
{

	protected	static final EOClientResourceBundle resourceBundle = new EOClientResourceBundle();
	
	public static final ImageIcon ICON_ADD 				= (ImageIcon)resourceBundle.getObject("cktl_add_16");
	public static final ImageIcon ICON_UPDATE			= (ImageIcon)resourceBundle.getObject("cktl_update_16");
	public static final ImageIcon ICON_DELETE 			= (ImageIcon)resourceBundle.getObject("cktl_delete_16");
	public static final ImageIcon ICON_CLOSE 			= (ImageIcon)resourceBundle.getObject("cktl_close_16");
	public static final ImageIcon ICON_VALID 			= (ImageIcon)resourceBundle.getObject("cktl_valid_16");
	public static final ImageIcon ICON_VALID2 			= (ImageIcon)resourceBundle.getObject("valider16");
	public static final ImageIcon ICON_CANCEL 			= (ImageIcon)resourceBundle.getObject("cktl_cancel_16");
	public static final ImageIcon ICON_SELECT_16 		= (ImageIcon)resourceBundle.getObject("cktl_why_16");
	public static final ImageIcon ICON_EXIT_16 			= (ImageIcon)resourceBundle.getObject("cktl_exit_16");
	public static final ImageIcon ICON_PRINTER_16 		= (ImageIcon)resourceBundle.getObject("cktl_printer_16");
	public static final ImageIcon ICON_PRINTER_32 		= (ImageIcon)resourceBundle.getObject("Imprimante");
	public static final ImageIcon ICON_PRINTER_ARRETE_32 	= (ImageIcon)resourceBundle.getObject("Imprimante_Arrete");
    public static final ImageIcon ICON_DISQUETTE_32 		= (ImageIcon)resourceBundle.getObject("disquette");
    public static final ImageIcon ICON_EXCEL_16 		= (ImageIcon)resourceBundle.getObject("cktl_excel_16");
    public static final ImageIcon ICON_ACROBAT_16 		= (ImageIcon)resourceBundle.getObject("cktl_acrobat_16");
	public static final ImageIcon ICON_OK 				= (ImageIcon)resourceBundle.getObject("cktl_coche_16");	
    public static final ImageIcon ICON_EURO 			= (ImageIcon)resourceBundle.getObject("cktl_euro_16");
    public static final ImageIcon ICON_COCHE 			= (ImageIcon)resourceBundle.getObject("cktl_coche_16");
	public static final ImageIcon ICON_CALENDAR 		= (ImageIcon)resourceBundle.getObject("cktl_calendar_16");
	public static final ImageIcon ICON_LOUPE_16 		= (ImageIcon)resourceBundle.getObject("cktl_loupe_16");
	public static final ImageIcon ICON_CALCULATE_16 	= (ImageIcon)resourceBundle.getObject("cktl_calculate_16");
	public static final ImageIcon ICON_DISQUETTE_22 	= (ImageIcon)resourceBundle.getObject("cktl_disquette_22");
	public static final ImageIcon ICON_DOSSIER_22 		= (ImageIcon)resourceBundle.getObject("cktl_dossier_22");
	public static final ImageIcon ICON_PARAMS_16 		= (ImageIcon)resourceBundle.getObject("cktl_params_16");

	public static final ImageIcon ICON_WIZARD_16 		= (ImageIcon)resourceBundle.getObject("cktl_wizard_16");

	public static final ImageIcon ICON_DOWNLOAD_16 		= (ImageIcon)resourceBundle.getObject("cktl_download_16");
	public static final ImageIcon ICON_RENOUVELER 		= (ImageIcon)resourceBundle.getObject("renouveler");

	public static final ImageIcon ICON_FLECHE_DROITE 		= (ImageIcon)resourceBundle.getObject("fleche_droite");
	public static final ImageIcon ICON_FLECHE_GAUCHE 		= (ImageIcon)resourceBundle.getObject("f_gauche");
	public static final ImageIcon ICON_FLECHE_UPDATE 		= (ImageIcon)resourceBundle.getObject("fleche_update");

	public static final ImageIcon ICON_REFRESH_16		 = (ImageIcon)resourceBundle.getObject("cktl_refresh_16");
	public static final ImageIcon ICON_REFRESH_22		 = (ImageIcon)resourceBundle.getObject("cktl_refresh_22");
	
	public static final ImageIcon ICON_INDIVIDU 	= (ImageIcon)resourceBundle.getObject("cktl_individu_16");
	public static final ImageIcon ICON_INDIVIDUS 	= (ImageIcon)resourceBundle.getObject("cktl_individus_16");
	public static final ImageIcon ICON_SELECT_ALL 	= (ImageIcon)resourceBundle.getObject("cktl_individus_16");
	public static final ImageIcon ICON_HELP 	= (ImageIcon)resourceBundle.getObject("help");

}