/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

// 28/10/2010 - Correction d'un bug sur le nombre de jours dans joursMoisAnneesEntre
public class DateCtrl {
	public static int NB_JOURS_COMPTABLE = 30;
	public static int NB_JOURS_COMPTABLE_ANNUEL = 360;

//	DATE COMPLETION : un string en entree est transforme en date; Si retour "" -> ERREUR , sinon renvoi de la date formattee avec des /
	public static String dateCompletion(String dateString) {
		GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"));
		NSTimestamp dateJour = new NSTimestamp();
		calendar.setTime(dateJour);
		int i;
		String chiffres = "", resultat = "";
		String jour = "", mois = "" , annee = "";

		for (i = 0; i < dateString.length(); i++) {
			if (NumberCtrl.estUnChiffre("" + dateString.charAt(i))) {
				chiffres = chiffres + dateString.charAt(i);
			}
		}
		// chiffres : String contenant tous les chiffres de la chaine entree
		// On regarde maintenant la taille de cette chaine
		// Si 1 ou 2 : on complete par le mois et l'annee en cours; Si 3 : on regarde si le mois ou le jour sur 1 caractere;

		// Cas de 1 chiffre : 1 journee, completee par le mois en cours et l'annee en cours
		if ((chiffres.length() == 1)) {
			jour = chiffres;
			mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
			annee = String.valueOf(calendar.get(Calendar.YEAR));
		}

		// Cas de 2 chiffres : 1 journee, completee par le mois en cours et l'annee en cours
		if ((chiffres.length() == 2)) {
			jour = chiffres;
			mois = String.valueOf(calendar.get(Calendar.MONTH)  + 1);
			annee = String.valueOf(calendar.get(Calendar.YEAR));
			resultat = formatteNo(jour) + "/" +formatteNo(mois) + "/" + formatterAnnee(annee); 

			if (!isValid(resultat)) {		// On teste si la date est valide avec le mois sur 1 caractere
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 1);
			}
		}

		// Cas de 3 chiffres : 1 jour et 1 mois , reste a determiner
		if ((chiffres.length() == 3)) {
			// L'annee est l'annee en cours
			annee = String.valueOf(calendar.get(Calendar.YEAR));

			mois = chiffres.substring(2).substring(0, 1);	// mois sur 1 caractere
			jour = chiffres.substring(0, 2);
			resultat = formatteNo (jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 

			if (!isValid(resultat))	{	// On teste si la date est valide avec le mois sur 1 caractere
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 2);
			}
		}

		// Cas de 4 chiffres : 1 jour et 1 mois, ou 1 jour, 1 mois et une annee
		if ((chiffres.length() == 4)) {
			jour = chiffres.substring(0, 2);
			mois = chiffres.substring(2).substring(0, 2);
			annee = String.valueOf(calendar.get(Calendar.YEAR));

			resultat = formatteNo (jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 

			if (!isValid (resultat)) {
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 1);
				annee = chiffres.substring(2).substring(0, 2);
			}
		}

		// Cas de 5 chiffres : 1 annee sur 2 caracteres
		if ((chiffres.length() == 5)) {
			// On regarde si les 4 derniers chiffres correspondent a une annee
			annee = chiffres.substring(3).substring(0, 2);

			// Reste 3 caracteres pour le mois et le jour
			mois = chiffres.substring(2).substring(0, 1);	// mois sur 1 caractere
			jour = chiffres.substring(0, 2);
			resultat = formatteNo (jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 

			if (!isValid(resultat))	{	// On teste si la date est valide avec le mois sur 1 caractere
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 2);
			}
		}

		// Cas de 6 chiffres : 2 pour le jour, le mois et 2 pour l'annee, ou 1 pour le jour, 1 pour le mois et 4 pour l'annee
		if ((chiffres.length() == 6)) {
			// On regarde si les 4 derniers chiffres correspondent a une annee
			String localAnnee = chiffres.substring(2).substring(0, 4);

			if (((Number)new Integer(localAnnee)).intValue() > 1900) {	// L'annee est sur 4 chiffres
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 1);
				annee = chiffres.substring(2).substring(0, 4);
			} else {		// L'annee est sur 2 chiffres
				jour = chiffres.substring(0, 2);
				mois = chiffres.substring(2).substring(0, 2);
				annee = chiffres.substring(4).substring(0, 2);
			}
		}

		// Cas de 7 chiffres : 4 chiffres pour l'annee - A choisir 2 pour le jour ou 2 pour le mois selon la saisie ...
		if ((chiffres.length() == 7)) {
			annee = chiffres.substring(3).substring(0, 4);

			// On teste si le mois est valide sur 1 ou 2 caracteres
			mois = chiffres.substring(1).substring(0, 2);	// mois sur 2 caracteres
			if (((Number)new Integer(mois)).intValue() <= 12) {	// mois valide
				jour = chiffres.substring(0, 1);
			} else {
				jour = chiffres.substring(0, 2);
				mois = chiffres.substring(2).substring(0, 1);
			}
		}

		// Cas de 8 chiffres : toute la date est donnee, on verifie si le mois, le jour et l'annee sont correts
		if ((chiffres.length() == 8)) {
			jour = chiffres.substring(0, 2);
			mois = chiffres.substring(2).substring(0, 2);
			annee = chiffres.substring(4).substring(0, 4);;
		}

		resultat = formatteNo(jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 

		if (!isValid(resultat)) {
			return "";
		}
		return resultat;
	}

	public static String formatterAnnee(String annee) {
		if (annee.length() == 2) {
			if (((Number) new Integer(annee)).intValue() < 30) {	// valeur à changer en avançant dans le temps
				return "20" + annee;
			} else {
				return "19" + annee;
			}
		}

		return annee;
	}


	/**
	 * Teste la validite de la date
	 * pour verifier "bon format de date"
	 * la date en string est convertie en date
	 * puis la date est convertie en string
	 * et les 2 strings sont comparees
	 * si elles sont egales, la date est valide
	 */
	public static boolean isValid(String dateString) {
		NSTimestamp aDate = new NSTimestamp();
		String newDateString = new String();

		aDate = stringToDate(dateString);
		newDateString = (String) dateToString(aDate);
		if (!newDateString.equals(dateString)) {
			return false;
		}
		return true;
	}

	/**
	 * Teste si deux dates donn&egrave;es correspondent au meme jour.
	 */
	public static boolean isSameDay(NSTimestamp ts1, NSTimestamp ts2) {
		if ((ts1 == null && ts2 != null) || (ts1 != null && ts2 == null)) {
			return false;
		}
		GregorianCalendar date1 = new GregorianCalendar(), date2 = new GregorianCalendar();
		date1.setTime(ts1);
		date2.setTime(ts2);
		return date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR) &&
		date1.get(Calendar.MONTH) == date2.get(Calendar.MONTH) &&
		date1.get(Calendar.DAY_OF_MONTH) == date2.get(Calendar.DAY_OF_MONTH);
		//return dateToString(date1).equals(dateToString(date2));
	}

	public static boolean isBeforeEq(NSTimestamp date1, NSTimestamp date2) {
		// WO4.5.x != WO5.x 
		// return (date1.getTime() <= date2.getTime());
		return isBefore(date1, date2) || isSameDay(date1, date2);
	}

	/**
	 * Teste si la date <i>date1</i> precede strictement la date <i>date2</i>.
	 */
	public static boolean isBefore(NSTimestamp date1, NSTimestamp date2) {
		// WO4.5.x != WO5.x 
		return (date1.getTime() < date2.getTime());
	}

	/**
	 * Teste si la date <i>date1</i> succede ou est egale a la date
	 * la <i>date2</i>.
	 */
	public static boolean isAfterEq(NSTimestamp date1, NSTimestamp date2) {
		return isBeforeEq(date2, date1);
	}

	/**
	 * Teste si la date <i>date1</i> succede strictement la date <i>date2</i>.
	 */
	public static boolean isAfter(NSTimestamp date1, NSTimestamp date2) {
		return isBefore(date2, date1);
	}



	/**
	 * 
	 */
	public static String dateToString(NSTimestamp gregorianDate, String dateFormat) {
		String dateString = "";

		NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
		try {
			dateString = formatter.format(gregorianDate);
		} catch(Exception ex) { }
		return dateString;
	}

	public static String dateToString(NSTimestamp gregorianDate) {
		return dateToString(gregorianDate, "%d/%m/%Y");
	}

	public static String dateToStringSansDelim(NSTimestamp gregorianDate) {
		return dateToString(gregorianDate, "%d%m%Y");
	}

	/*
	 * Convertit la string en date
	 */
	public static NSTimestamp stringToDate(String dateString, String dateFormat) {
		NSTimestamp date = null;
		NSTimestampFormatter formatter;
		if ((dateFormat == null) || (dateFormat.trim().length() == 0))
			return null;
		try {
			formatter = new NSTimestampFormatter(dateFormat);
			date = (NSTimestamp) formatter.parseObject(dateString);
			if (!dateString.equals(dateToString(date, dateFormat)))
				return null;
		} catch(Exception ex) {}

		return timestampByAddingGregorianUnits(date, 0, 0, 0, 0, 0, 0);
	}

	public static NSTimestamp stringToDate (String dateString) {
		return stringToDate(dateString, "%d/%m/%Y");
	}

	/**
	 * Convertie le numero du jour de la semaine de la representation
	 * anglaise vers celle francaise. 
	 */
	public static int getDayOfWeek(int dayOfWeek) {
		return (dayOfWeek == 0)?(dayOfWeek+6):(dayOfWeek-1);
	}

	/**
	 * Retourne le numero de jour de la semaine correspondant 
	 * a la date indiquee. Lundi est le premier jour de la semaine. 
	 */
	public static int getDayOfWeek(NSTimestamp date) {
		// WO4.5.x != WO5.x 
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.setFirstDayOfWeek(GregorianCalendar.MONDAY);
		return calendar.get(GregorianCalendar.DAY_OF_WEEK);
	}

	public static int getDayOfMonth(NSTimestamp date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar.get(GregorianCalendar.DAY_OF_MONTH);
	}

	public static boolean isJourFerie(NSTimestamp leJour) {	  
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(leJour);

		if (joursFeries(calendar.get(Calendar.YEAR)).containsObject(leJour)) {
			return true;
		}

		return false;
	}

	// JOURS FERIES : Array de tous les jours feries de l'annee en cours
	public static NSArray joursFeries(int annee)
	{
		int jour, mois, nCycleLunaire, nBissextile, nLettDimanche, nC1, nC2, nC3;
		NSTimestamp    leJourTemp;
		NSMutableArray resultat = new NSMutableArray();

		resultat.addObject(DateCtrl.stringToDate("01/01/" + annee, "%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("01/05/" + annee, "%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("08/05/" + annee, "%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("14/07/" + annee, "%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("15/08/" + annee, "%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("01/11/" + annee, "%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("11/11/" + annee, "%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("25/12/" + annee, "%d/%m/%Y"));

		if ((annee == 1954)||(annee == 2049)) {
			jour = 18;
			mois = 4;
		} else {
			if ((annee == 1981)||(annee == 2076)) {
				jour = 19;
				mois = 4;
			} else {
				nCycleLunaire = annee % 19;
				nBissextile = annee % 4;
				nLettDimanche = annee % 7;

				nC1 = ((nCycleLunaire * 19) + 24) % 30;
				nC2 = (nBissextile * 2 + nLettDimanche * 4 + nC1 * 6 + 5) % 7;
				nC3 = nC1 + nC2;

				if (nC3 <= 9)  {
					jour = 22 + nC3;
					mois = 3;
				} else {
					jour= nC3 - 9;
					mois = 4;
				}
			}
		}

		leJourTemp = new NSTimestamp(annee, mois, jour, 0, 0, 0, NSTimeZone.defaultTimeZone());

		// Lundi de Paques
		leJourTemp=timestampByAddingGregorianUnits(leJourTemp, 0, 0, 1, 0, 0, 0);
		resultat.addObject(leJourTemp); 

		// Jeudi de l'ascension : 38 jours apres le lundi de paques
		leJourTemp=timestampByAddingGregorianUnits (leJourTemp, 0, 0, 38, 0, 0, 0);
		resultat.addObject(leJourTemp); 

		// Lundi de Pentecote : 11 jours apres l'ascension
		leJourTemp=timestampByAddingGregorianUnits (leJourTemp, 0, 0, 11, 0, 0, 0);
		resultat.addObject(leJourTemp); 

		return (NSArray) resultat;
	}

	/** GET MOIS : renvoie le numero du mois formatte sur 2 caracteres */
	public static String formatteNoMois(int noMois) {
		if (noMois < 10) {
			return "0" + noMois;
		}

		return "" + noMois;
	}

	/**
	 * 
	 * @param date
	 * @return numero de l'annee
	 */
	public static int getYear(NSTimestamp date)	{

		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.YEAR));
	}

	/**
	 *
	 * @param date
	 * @return numero du mois (0 si janvier)
	 */
	public static int getMonth(NSTimestamp date)	{

		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.MONTH));
	}

	/** GET MOIS : renvoie le numero formatte sur 2 caracteres */
	public static String formatteNo(String numero) {
		if (numero.length() == 1) {
			return "0" + numero;
		}

		return "" + numero;
	}

	/** GET JOURS OUVRABLES : Renvoie le nombre de jours ouvrables entre deux dates
	 * ampm permet de specifier des demi-journees (on attend les strings "am" ou "pm")
	 * ampmDebut et ampmFin peuvent &ecirc;tre nulles. En quel cas : ampmDebut = "am" et ampmFin = "pm"
	 **/
	public static float nbJoursOuvrables(NSTimestamp dateDebut, String ampmDebut, NSTimestamp dateFin, String ampmFin)	{
		NSTimestamp dateATraiter = dateDebut;
		float dureeAbsence = (float) 0;
		if (ampmDebut == null || ampmDebut.length() == 0) {
			ampmDebut = "am";
		}
		if (ampmFin == null || ampmFin.length() == 0) {
			ampmFin = "pm";
		}
		if (DateCtrl.isSameDay(dateDebut,dateFin)) {	// On a pris une seule journee
			if (ampmDebut.equals("am")) {	
				if (ampmFin.equals("pm")) {
					dureeAbsence += 1.0;
				} else if (ampmFin.equals("am")) {
					dureeAbsence += 0.5;
				}
			} else {		//c'est forcément pm pour ampm2 (sinon on n'est pas le même jour)
				dureeAbsence += 0.5;
			}
		} else {
			while (!DateCtrl.isAfter(dateATraiter,dateFin)) {
				// Si c'est un jour ferie, non considere comme jour ouvre
				if (DateCtrl.isJourFerie(dateATraiter)) {	
					dateATraiter = jourSuivant(dateATraiter);
					continue;
				}

				// Si c'est un samedi ou un dimanche : non jour ouvre
				if ((DateCtrl.getDayOfWeek(dateATraiter) == 0) || (DateCtrl.getDayOfWeek(dateATraiter) == 6)) {	
					dateATraiter = jourSuivant(dateATraiter);
					continue;
				}

				// Jour ouvre On calcule si c'est une journee ou une demi journee
				if (DateCtrl.isSameDay(dateATraiter,dateDebut)) {	// On est a la date de debut, on regarde si on compte une demi journee ou une journee
					if (ampmDebut.equals("am")) {	// On a pris une seule journee
						if (!DateCtrl.isSameDay(dateATraiter,dateFin)) {
							dureeAbsence += 1.0;
						} else if (ampmFin.equals("am")) {
							dureeAbsence += 0.5;
						} else {
							dureeAbsence += 1.0;
						}
					} else { // On regarde si on est en conges le matin ou le midi
						dureeAbsence += 0.5;
					}
				} else if (DateCtrl.isSameDay(dateATraiter,dateFin)) {	// On est a la date de fin, on regarde si on compte une demi journee ou une journee
					if (ampmFin.equals("am")) {	// On rembauche le dernier jour l'apres midi
						dureeAbsence += 0.5;
					} else {
						dureeAbsence += 1.0;
					}
				} else {
					dureeAbsence += 1.0;
				}

				dateATraiter = timestampByAddingGregorianUnits(dateATraiter, 0, 0, 1, 0, 0, 0);
			}
		}
		return dureeAbsence;
	}

	/** retourne le nombre de jours ecoules entre deux dates 
	 * le r&acu;tesulat est negatif si la deuxi&egrave;me date est anterieure
	 * retourne -1 si date fin et debut sont bulles
	 * @param dateDebut 
	 * @param dateFin
	 * @param inclureBornes	true si il faut inclure les bornes
	 * @param estDureeComptable true si il faut calculer le nombre de jours, mois, annees en duree comptable
	 * */
	public static int nbJoursEntre(NSTimestamp debut, NSTimestamp fin, boolean inclureBornes, boolean estDureeComptable) {
		if (debut == null || fin == null) {
			return -1;
		}
		if (dateToString(debut).equals("0/00/0000") || dateToString(debut).equals("0/00/0000")) {
			return -1;
		}
		int nbJours = 0;
		boolean estPositif = true;
		NSTimestamp date1 = debut, date2 = fin;
		if (isBefore(fin, debut)) {
			estPositif = false;
			date1 = fin;
			date2 = debut;
		}
		// pour optimiser, on calcule le compte des jours directement sur le 31/12 pour toutes les années intermédiaires
		// au deux dates
		int annee1 = DateCtrl.getYear(date1);
		int annee2 = DateCtrl.getYear(date2);
		if (annee2 - annee1 > 1)  {
			for (int annee = annee1 + 1; annee <= annee2 - 1; annee++) {
				if (estDureeComptable) {
					nbJours += NB_JOURS_COMPTABLE_ANNUEL; 
				} else {
					GregorianCalendar calendar = new GregorianCalendar(); 
					calendar.setTime(DateCtrl.stringToDate("31/12/" + annee)); 
					nbJours += calendar.get(Calendar.DAY_OF_YEAR);
				}
			}
			// ajouter le nombre de jours jusqu'au 31 décembre de la première date
			nbJours += nbJoursEntre(date1, DateCtrl.stringToDate("31/12/" + annee1), true,estDureeComptable);
			//	 ajouter le nombre de jours depuis le 1er janvier jusqu'à la date 2
			nbJours += nbJoursEntre(DateCtrl.stringToDate("01/01/" + annee2), date2, true,estDureeComptable);
		} else {
			while (dateToString(date1).equals(dateToString(date2)) == false) {
				date1 = jourSuivant(date1);
				if (estDureeComptable) {
					GregorianCalendar calendar = new GregorianCalendar(); 
					calendar.setTime(date1); 
					int mois = calendar.get(Calendar.MONTH);
					int jour = calendar.get(Calendar.DAY_OF_MONTH);
					if (jour < 31) {
						nbJours++;
						if (mois == 1) {
							// pour les durées comptables en février
							boolean anneeBissextile = calendar.isLeapYear(calendar.get(Calendar.YEAR));
							if (anneeBissextile && jour == 29) {
								// années bissextile, compter un jour de plus (pour avoir 30 jours)
								nbJours++;
							} else if (!anneeBissextile && jour == 28) {
								// année non bissextile, compter deux jour de plus (les mois sont de 30 jours)
								nbJours += 2;
							}
						}
					}
				} else {
					nbJours++;
				}
			}
			if (inclureBornes) {
				nbJours++;
			}
		}
		if (estPositif) {
			return nbJours;
		} else {
			return -nbJours;
		}

	}

	/** retourne le nombre de jours ecoules entre deux dates 
	 * le r&acu;tesulat est negatif si la deuxi&egrave;me date est anterieure */
	public static int nbJoursEntre(NSTimestamp debut, NSTimestamp fin, boolean inclureBornes) {
		return nbJoursEntre(debut, fin, inclureBornes, false);
	}
	public static int nbJoursDansMois(NSTimestamp date) {
		if (date == null) {
			return 0;
		}
		int mois = getMonth(date) + 1; 	// les mois commencent à partir de zéro en java 

		if ((mois == 4) || (mois == 6) || (mois == 9) || (mois == 11)) {
			return 30;
		} else if (mois == 2) {
			GregorianCalendar calendar = new GregorianCalendar(); 
			calendar.setTime(date); 
			if (calendar.isLeapYear(calendar.get(Calendar.YEAR))) {
				return 29;
			} else {
				return 28;
			}
		}
		return 31;
	}

	public static NSTimestamp timestampByAddingGregorianUnits(NSTimestamp date, int year, int month, int day, int hour, int minute, int second) { 
		if (date == null) {
			return null;
		}
		GregorianCalendar calendar = new GregorianCalendar(); 
		calendar.setTime(date); 
		return new NSTimestamp( 
				calendar.get(GregorianCalendar.YEAR) + year, 
				calendar.get(GregorianCalendar.MONTH) + month + 1, 
				calendar.get(GregorianCalendar.DAY_OF_MONTH) + day, 
				calendar.get(GregorianCalendar.HOUR_OF_DAY) + hour, 
				calendar.get(GregorianCalendar.MINUTE) + minute, 
				calendar.get(GregorianCalendar.SECOND) + second, 
				NSTimeZone.getDefault()); 
	} 

	public static NSTimestamp jourSuivant(NSTimestamp aDate) {
		if (aDate != null) {
			return timestampByAddingGregorianUnits(aDate, 0, 0, 1, 0, 0, 0);

		} else {
			return null;
		}
	}

	public static NSTimestamp jourPrecedent(NSTimestamp aDate) {
		if (aDate != null) {
			return timestampByAddingGregorianUnits(aDate, 0, 0, -1, 0, 0, 0);
		} else {
			return null;
		}
	}

	public static NSTimestamp dateAvecAjoutAnnees(NSTimestamp aDate, int nbAnnees) {
		if (aDate != null) {
			return timestampByAddingGregorianUnits(aDate, nbAnnees, 0, 0, 0, 0, 0);
		} else {
			return null;
		}
	}

	public static NSTimestamp dateAvecAjoutMois(NSTimestamp aDate, int nbMois) {
		if (aDate != null) {
			return timestampByAddingGregorianUnits(aDate, 0, nbMois, 0, 0, 0, 0);
		} else {
			return null;
		}
	}

	public static NSTimestamp dateAvecAjoutJours(NSTimestamp aDate, int nbJours) {
		if (aDate != null) {
			return timestampByAddingGregorianUnits(aDate, 0, 0, nbJours, 0, 0, 0);
		} else {
			return null;
		}
	}

	/** calcule la duree comptable d'une periode en nombre de jours */
	public static int dureeComptable(NSTimestamp dateDebut, NSTimestamp dateFin, boolean inclureBornes) throws Exception {
		if (dateDebut == null || dateFin == null) {
			throw new Exception("Vous devez fournir une date de début et de fin");
		}
		boolean estPositif = DateCtrl.isBefore(dateDebut,dateFin);
		IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
		if (estPositif) {
			joursMoisAnneesEntre(dateDebut, dateFin, anneeRef, moisRef, jourRef, inclureBornes, true);
		} else {
			joursMoisAnneesEntre(dateFin, dateDebut, anneeRef, moisRef, jourRef, inclureBornes, true);
		}
		int result = anneeRef.value * (12 * NB_JOURS_COMPTABLE) + moisRef.value * NB_JOURS_COMPTABLE + jourRef.value;

		return result;
	}

	/** ajoute une duree comptable ae une date et retourne la date obtenue */
	public static NSTimestamp ajouterDureeComptable(NSTimestamp date, int nbJoursComptable) {
		if (date == null) {
			return null;
		}
		if (nbJoursComptable == 0) {
			return date;
		}
		NSTimestamp dateFin = dateAvecAjoutJours(date, nbJoursComptable);
		try {
			if (nbJoursComptable > 0) {
				boolean found = false;
				while (!found) {
					NSTimestamp jourPrecedent = jourPrecedent(dateFin), jourSuivant = jourSuivant(dateFin);
					if (dureeComptable(date, jourPrecedent, true) <= nbJoursComptable && dureeComptable(date, dateFin, true) >= nbJoursComptable && 
							dureeComptable(date, jourSuivant, true) > nbJoursComptable) {
						found = true;
					} else {
						dateFin = jourSuivant;
					}
				}
			} else {	// nbJoursComptable négatif
				boolean found = false;
				while (!found) {
					NSTimestamp jourPrecedent = jourPrecedent(date), jourSuivant = jourSuivant(date);
					if (dureeComptable(dateFin, jourPrecedent, true) <= -nbJoursComptable && dureeComptable(dateFin, date, true) >= -nbJoursComptable && 
							dureeComptable(dateFin, jourSuivant, true) > -nbJoursComptable) {
						found = true;
					} else {
						dateFin = jourPrecedent;
					}
				}
			}
			if (getDayOfMonth(dateFin) == 30) {
				NSTimestamp jourSuivant = jourSuivant(dateFin);
				// si la date se termine un 30 et que le mois a 31 jours => inclure le 31 (il n'a pas d'existence comptable)
				if (getDayOfMonth(jourSuivant) == 31) {
					dateFin = jourSuivant;
				}
			}

			return dateFin;
		} catch (Exception e) {
			// dateDebut nulle
			return null;
		}
	}

	/** Calcule la duree ecoulee entre deux dates
	 * @param dateDebut 
	 * @param dateFin
	 * @param anneeRef	peut &ecirc;tre nul => calcul dans les autres unites
	 * @param moisRef	peut &ecirc;tre nul => calcul dans les autres unites
	 * @param jourRef	peut &ecirc;tre nul => calcul dans les autres unites
	 * @param inclureBornes	true si il faut inclure les bornes
	 * @param estDureeComptable true si il faut calculer le nombre de jours, mois, annees en duree comptable
	 */
	public static void joursMoisAnneesEntre(NSTimestamp dateDebut,
			NSTimestamp dateFin, IntRef anneeRef, IntRef moisRef,
			IntRef jourRef, boolean inclureBornes, boolean estDureeComptable) {
		if (dateDebut == null || dateFin == null) {
			return;
		}
		boolean retournerValeurNegative = DateCtrl.isBefore(dateFin, dateDebut);
		GregorianCalendar date1 = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"));
		GregorianCalendar date2 = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"));
		if (retournerValeurNegative) {
			date1.setTime(dateFin);
			date2.setTime(dateDebut);
		} else {
			date1.setTime(dateDebut);
			date2.setTime(dateFin);
		}
		int annee1 = date1.get(Calendar.YEAR);
		int annee2 = date2.get(Calendar.YEAR);
		int mois1 = date1.get(Calendar.MONTH);
		int mois2 = date2.get(Calendar.MONTH);
		int jour1 = date1.get(Calendar.DAY_OF_MONTH);
		int jour2 = date2.get(Calendar.DAY_OF_MONTH);
		if (anneeRef == null && moisRef == null) {
			if (jourRef != null) {
				// on veut calculer un nombre de jours
				jourRef.value = nbJoursEntre(dateDebut, dateFin, inclureBornes, estDureeComptable);
			}
		} else {
			int differenceAnnee = annee2 - annee1;
			int differenceMois = mois2 - mois1;
			int differenceJours = jour2 - jour1;
			if (differenceMois < 0) {
				differenceAnnee = differenceAnnee - 1;
				differenceMois = 12 + differenceMois;
			}
			if (inclureBornes) {
				// on inclut les bornes
				differenceJours++;
			}
			if (differenceJours >= 0) {	
				// vérifier si le mois est complet en regardant le nombre de jours du mois 
				int dernierJour = nbJoursDansMois(new NSTimestamp(annee2, mois2 + 1, 1, 0, 0, 0, NSTimeZone.defaultTimeZone()), false); // on prend mois2 + 1 car java numérote les mois à partir de zéro
				if ((differenceJours == dernierJour) || (differenceJours == 31 && estDureeComptable)) {
					differenceMois++;
					if (differenceMois == 12) {
						// une année complète
						differenceAnnee++;
						differenceMois = 0;
					}
					differenceJours = 0;
				}
			} else {
				differenceMois = differenceMois - 1;
				if (differenceMois < 0) {
					// les mois étaient identiques
					differenceAnnee = differenceAnnee - 1;
					differenceMois = 12 + differenceMois;
				}
				// déterminer le dernier jour du mois précédent mois2
				int dernierJour = nbJoursDansMois(new NSTimestamp(annee2, mois2, 1, 0, 0, 0, NSTimeZone.defaultTimeZone()), estDureeComptable);
				differenceJours = dernierJour + differenceJours;
			}
			if (moisRef == null) {
				if (jourRef != null) {
					// on veut une différence en années et jours (le cas anneeRef == null && moisRef == null est déjà traité)
					// on se cale n années plus tard pour calculer le nombre de jours
					NSTimestamp nouvelleDateDebut = new NSTimestamp(annee1 + differenceAnnee, mois1 + 1, jour1, 0, 0, 0, NSTimeZone.defaultTimeZone()); // les mois commencent à zéro en java
					differenceJours = nbJoursEntre(nouvelleDateDebut, dateFin, inclureBornes, estDureeComptable);
					jourRef.value = differenceJours;
				}
				if (anneeRef != null ) {
					anneeRef.value = differenceAnnee;
				}
			} else {
				if (anneeRef == null) {
					// on veut un calcul en mois
					differenceMois += differenceAnnee * 12;
				} else {
					anneeRef.value = differenceAnnee;
				}
				moisRef.value = differenceMois;
				if (jourRef != null) {
					jourRef.value = differenceJours;
				}
			}
		}
		if (retournerValeurNegative) {
			if (anneeRef != null) {
				anneeRef.value = -anneeRef.value;
			}
			if (moisRef != null) {
				moisRef.value = -moisRef.value;
			}
			if (jourRef != null) {
				jourRef.value = -jourRef.value;
			}
		}
	}

	/** Calcule la duree ecoulee entre deux dates
	 * @param dateDebut
	 * @param dateFin
	 * @param anneeRef	peut &ecirc;tre nul => calcul dans les autres unites
	 * @param moisRef	peut &ecirc;tre nul => calcul dans les autres unites
	 * @param jourRef	peut &ecirc;tre nul => calcul dans les autres unites
	 * @param inclureBornes	true si il faut inclure les bornes
	 */
	public static void joursMoisAnneesEntre(NSTimestamp dateDebut, NSTimestamp dateFin, IntRef anneeRef, IntRef moisRef, IntRef jourRef, boolean inclureBornes) {
		joursMoisAnneesEntre(dateDebut, dateFin, anneeRef, moisRef, jourRef, inclureBornes, false);
	}

	/** retourne la duree en mois entre deux dates
	 * @param dateDebut
	 * @param dateFin peut &ecirc;tre nulle
	 * @return null si la date fin est nulle
	 */
	public static Integer calculerDureeEnMois(NSTimestamp dateDebut, NSTimestamp dateFin) {
		return calculerDureeEnMois(dateDebut, dateFin, false);
	}

	/** retourne la duree en mois entre deux dates
	 * @param dateDebut
	 * @param dateFin peut &ecirc;tre nulle
	 * @return null si la date fin est nulle
	 */
	public static Integer calculerDureeEnMois(NSTimestamp dateDebut, NSTimestamp dateFin, boolean inclureBornes) {
		if (dateFin == null) {
			return null;
		}
		DateCtrl.IntRef moisRef = new DateCtrl.IntRef();
		DateCtrl.IntRef jourRef = new DateCtrl.IntRef();
		DateCtrl.joursMoisAnneesEntre(dateDebut, dateFin, null, moisRef, jourRef, inclureBornes);
		int nbMois = moisRef.value;
		return new Integer(nbMois);
	}

	/** retourne la valeur du dernier jour du mois de la date passee en param&egrave;tre
	 * @param date pour laquelle on recherche le dernier jour du mois
	 * @param estDureeComptable true si on s'interesse au dure comptable */
	public static int nbJoursDansMois(NSTimestamp aDate, boolean estDureeComptable) {
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(aDate);
		int dernierJour = date.getActualMaximum(Calendar.DAY_OF_MONTH);
		int mois = date.get(Calendar.MONTH);
		boolean anneeBissextile = date.isLeapYear(date.get(Calendar.YEAR));
		if (estDureeComptable) {
			if (dernierJour == 31 || (mois == 1 && ((anneeBissextile && dernierJour == 29) || (!anneeBissextile && dernierJour == 28)))) {
				dernierJour = 30;
			}
		}
		return dernierJour;
	}

	public static class IntRef extends Object {
		public int value;
	}



}