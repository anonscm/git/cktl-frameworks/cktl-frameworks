// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPrioriteGrhum.java instead.
package org.cocktail.connecteur.common.modele;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPrioriteGrhum extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PrioriteGrhum";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PRI_ATTRIBUT_KEY = "priAttribut";
	public static final String PRI_ENTITE_KEY = "priEntite";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOPrioriteGrhum.class);

  public EOPrioriteGrhum localInstanceIn(EOEditingContext editingContext) {
    EOPrioriteGrhum localInstance = (EOPrioriteGrhum)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPrioriteGrhum.LOG.isDebugEnabled()) {
    	_EOPrioriteGrhum.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPrioriteGrhum.LOG.isDebugEnabled()) {
    	_EOPrioriteGrhum.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String priAttribut() {
    return (String) storedValueForKey("priAttribut");
  }

  public void setPriAttribut(String value) {
    if (_EOPrioriteGrhum.LOG.isDebugEnabled()) {
    	_EOPrioriteGrhum.LOG.debug( "updating priAttribut from " + priAttribut() + " to " + value);
    }
    takeStoredValueForKey(value, "priAttribut");
  }

  public String priEntite() {
    return (String) storedValueForKey("priEntite");
  }

  public void setPriEntite(String value) {
    if (_EOPrioriteGrhum.LOG.isDebugEnabled()) {
    	_EOPrioriteGrhum.LOG.debug( "updating priEntite from " + priEntite() + " to " + value);
    }
    takeStoredValueForKey(value, "priEntite");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPrioriteGrhum.LOG.isDebugEnabled()) {
    	_EOPrioriteGrhum.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOPrioriteGrhum createPrioriteGrhum(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String priEntite
, String temValide
) {
    EOPrioriteGrhum eo = (EOPrioriteGrhum) EOUtilities.createAndInsertInstance(editingContext, _EOPrioriteGrhum.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPriEntite(priEntite);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOPrioriteGrhum> fetchAllPrioriteGrhums(EOEditingContext editingContext) {
    return _EOPrioriteGrhum.fetchAllPrioriteGrhums(editingContext, null);
  }

  public static NSArray<EOPrioriteGrhum> fetchAllPrioriteGrhums(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPrioriteGrhum.fetchPrioriteGrhums(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPrioriteGrhum> fetchPrioriteGrhums(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPrioriteGrhum.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPrioriteGrhum> eoObjects = (NSArray<EOPrioriteGrhum>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPrioriteGrhum fetchPrioriteGrhum(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPrioriteGrhum.fetchPrioriteGrhum(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPrioriteGrhum fetchPrioriteGrhum(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPrioriteGrhum> eoObjects = _EOPrioriteGrhum.fetchPrioriteGrhums(editingContext, qualifier, null);
    EOPrioriteGrhum eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPrioriteGrhum)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PrioriteGrhum that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPrioriteGrhum fetchRequiredPrioriteGrhum(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPrioriteGrhum.fetchRequiredPrioriteGrhum(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPrioriteGrhum fetchRequiredPrioriteGrhum(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPrioriteGrhum eoObject = _EOPrioriteGrhum.fetchPrioriteGrhum(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PrioriteGrhum that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPrioriteGrhum localInstanceIn(EOEditingContext editingContext, EOPrioriteGrhum eo) {
    EOPrioriteGrhum localInstance = (eo == null) ? null : (EOPrioriteGrhum)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
