//EOCommune.java
//Created on Wed Jul 25 15:08:17 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOCommune extends _EOCommune {

	public EOCommune() {
		super();
	}

	// Méthodes ajoutées
	/** recherche une commune en fonction du code postal et du libelle long 
	 * @return commune ou null si commune non trouvee */
	public static EOCommune rechercherCommune(EOEditingContext editingContext,String codePostal,String nom) {
		// rechercher la commune dans la table des communes
		try {	// exception si pas de nom de commune pour l'agent
			String nomCommune = nom.toUpperCase();
			int position = nomCommune.indexOf("CEDEX");
			if (position > 0) {
				nomCommune = nomCommune.substring(0,position - 1);	
			}
			// supprimer aussi les espaces en début et en fin de chaîne
			nomCommune = nomCommune.trim();
			NSMutableArray values = new NSMutableArray(nomCommune);
			values.addObject(nomCommune);
			
			String stringQualifier = "(llCom = %@ OR lcCom = %@)";
			if (codePostal != null) {
				values.addObject(codePostal);
				values.addObject(codePostal);
				stringQualifier += "AND (cPostal = %@ or cPostalPrincipal = %@)";
			}
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,values);
			EOFetchSpecification fs = new EOFetchSpecification("Commune",qualifier, null);
			fs.setFetchLimit(1);
			NSArray communes = editingContext.objectsWithFetchSpecification(fs);

			return ((EOCommune)communes.objectAtIndex(0));
		} catch (Exception e) {
			return null;
		}
	}
}
