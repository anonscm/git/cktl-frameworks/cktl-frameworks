// EOTypeCgMatern.java
// Created on Thu Feb 20 14:39:29  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;


import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.eocontrol.EOGenericRecord;
public class EOTypeCgMatern extends EOGenericRecord implements RecordAvecLibelle {
	public static String MATERNITE = "MATER";
	public static String PATHOLOGIE_GROSSESSE = "GROSS";
	public static String PATHOLOGIE_ACCOUCHEMENT = "ACCOU";
    public EOTypeCgMatern() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOTypeCgMatern(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String cTypeCgMatern() {
        return (String)storedValueForKey("cTypeCgMatern");
    }

    public void setCTypeCgMatern(String value) {
        takeStoredValueForKey(value, "cTypeCgMatern");
    }

  /*  public String lcTypeCgMatern() {
        return (String)storedValueForKey("lcTypeCgMatern");
    }

    public void setLcTypeCgMatern(String value) {
        takeStoredValueForKey(value, "lcTypeCgMatern");
    }*/

    public String llTypeCgMatern() {
        return (String)storedValueForKey("llTypeCgMatern");
    }

    public void setLlTypeCgMatern(String value) {
        takeStoredValueForKey(value, "llTypeCgMatern");
    }

    public Number dureeCgMatern() {
        return (Number)storedValueForKey("dureeCgMatern");
    }

    public void setDureeCgMatern(Number value) {
        takeStoredValueForKey(value, "dureeCgMatern");
    }

    /*  public Number ordreCgMatern() {
        return (Number)storedValueForKey("ordreCgMatern");
    }

    public void setOrdreCgMatern(Number value) {
        takeStoredValueForKey(value, "ordreCgMatern");
    }*/
    // méthodes ajoutées
    public boolean estMaternite() {
    		return cTypeCgMatern() != null && cTypeCgMatern().equals(EOTypeCgMatern.MATERNITE);
    }
    public boolean estPathologieGrossesse() {
		return cTypeCgMatern() != null && cTypeCgMatern().equals(EOTypeCgMatern.PATHOLOGIE_GROSSESSE);
    }
    public boolean estPathologieAccouchement() {
		return cTypeCgMatern() != null && cTypeCgMatern().equals(EOTypeCgMatern.PATHOLOGIE_ACCOUCHEMENT);
    }
    // interface RecordAvecLibelle
    public String libelle() {
    		return llTypeCgMatern();
    }
    
}
