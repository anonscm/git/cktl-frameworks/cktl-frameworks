// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOStatutIndividu.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStatutIndividu extends  EOGenericRecord {
	public static final String ENTITY_NAME = "StatutIndividu";

	// Attributes
	public static final String C_QUALITE_KEY = "cQualite";
	public static final String C_STATUT_KEY = "cStatut";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_STATUT_KEY = "lcStatut";
	public static final String LL_STATUT_KEY = "llStatut";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOStatutIndividu.class);

  public EOStatutIndividu localInstanceIn(EOEditingContext editingContext) {
    EOStatutIndividu localInstance = (EOStatutIndividu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cQualite() {
    return (String) storedValueForKey("cQualite");
  }

  public void setCQualite(String value) {
    if (_EOStatutIndividu.LOG.isDebugEnabled()) {
    	_EOStatutIndividu.LOG.debug( "updating cQualite from " + cQualite() + " to " + value);
    }
    takeStoredValueForKey(value, "cQualite");
  }

  public String cStatut() {
    return (String) storedValueForKey("cStatut");
  }

  public void setCStatut(String value) {
    if (_EOStatutIndividu.LOG.isDebugEnabled()) {
    	_EOStatutIndividu.LOG.debug( "updating cStatut from " + cStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "cStatut");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStatutIndividu.LOG.isDebugEnabled()) {
    	_EOStatutIndividu.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStatutIndividu.LOG.isDebugEnabled()) {
    	_EOStatutIndividu.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcStatut() {
    return (String) storedValueForKey("lcStatut");
  }

  public void setLcStatut(String value) {
    if (_EOStatutIndividu.LOG.isDebugEnabled()) {
    	_EOStatutIndividu.LOG.debug( "updating lcStatut from " + lcStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "lcStatut");
  }

  public String llStatut() {
    return (String) storedValueForKey("llStatut");
  }

  public void setLlStatut(String value) {
    if (_EOStatutIndividu.LOG.isDebugEnabled()) {
    	_EOStatutIndividu.LOG.debug( "updating llStatut from " + llStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "llStatut");
  }


  public static EOStatutIndividu createStatutIndividu(EOEditingContext editingContext, String cQualite
, String cStatut
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcStatut
, String llStatut
) {
    EOStatutIndividu eo = (EOStatutIndividu) EOUtilities.createAndInsertInstance(editingContext, _EOStatutIndividu.ENTITY_NAME);    
		eo.setCQualite(cQualite);
		eo.setCStatut(cStatut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcStatut(lcStatut);
		eo.setLlStatut(llStatut);
    return eo;
  }

  public static NSArray<EOStatutIndividu> fetchAllStatutIndividus(EOEditingContext editingContext) {
    return _EOStatutIndividu.fetchAllStatutIndividus(editingContext, null);
  }

  public static NSArray<EOStatutIndividu> fetchAllStatutIndividus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStatutIndividu.fetchStatutIndividus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStatutIndividu> fetchStatutIndividus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStatutIndividu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStatutIndividu> eoObjects = (NSArray<EOStatutIndividu>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStatutIndividu fetchStatutIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutIndividu.fetchStatutIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutIndividu fetchStatutIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStatutIndividu> eoObjects = _EOStatutIndividu.fetchStatutIndividus(editingContext, qualifier, null);
    EOStatutIndividu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStatutIndividu)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one StatutIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutIndividu fetchRequiredStatutIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutIndividu.fetchRequiredStatutIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutIndividu fetchRequiredStatutIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStatutIndividu eoObject = _EOStatutIndividu.fetchStatutIndividu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no StatutIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutIndividu localInstanceIn(EOEditingContext editingContext, EOStatutIndividu eo) {
    EOStatutIndividu localInstance = (eo == null) ? null : (EOStatutIndividu)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
