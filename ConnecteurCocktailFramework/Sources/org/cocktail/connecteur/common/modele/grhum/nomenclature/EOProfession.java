package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;

public class EOProfession extends _EOProfession {
  private static Logger log = Logger.getLogger(EOProfession.class);
  
  public static EOProfession rechercherProfessionPourProCode(EOEditingContext editingContext,String proCode) {
//  	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("proCode = %@", new NSArray(proCode));
  	EOQualifier qualifier = new EOKeyValueQualifier(EOProfession.PRO_CODE_KEY, EOQualifier.QualifierOperatorEqual, proCode);
  	EOFetchSpecification fs = new EOFetchSpecification("Profession",qualifier,null);
  	try {
  		return (EOProfession)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
  	} catch (Exception e) {
  		return null;
  	}
  }
}
