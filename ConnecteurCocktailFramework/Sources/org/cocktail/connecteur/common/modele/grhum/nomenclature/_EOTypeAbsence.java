// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeAbsence.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeAbsence extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeAbsence";

	// Attributes
	public static final String CONGE_LEGAL_KEY = "congeLegal";
	public static final String C_TYPE_ABSENCE_KEY = "cTypeAbsence";
	public static final String C_TYPE_ABSENCE_HARPEGE_KEY = "cTypeAbsenceHarpege";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String JOURS_CONSECUTIFS_KEY = "joursConsecutifs";
	public static final String LC_TYPE_ABSENCE_KEY = "lcTypeAbsence";
	public static final String LL_TYPE_ABSENCE_KEY = "llTypeAbsence";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeAbsence.class);

  public EOTypeAbsence localInstanceIn(EOEditingContext editingContext) {
    EOTypeAbsence localInstance = (EOTypeAbsence)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String congeLegal() {
    return (String) storedValueForKey("congeLegal");
  }

  public void setCongeLegal(String value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating congeLegal from " + congeLegal() + " to " + value);
    }
    takeStoredValueForKey(value, "congeLegal");
  }

  public String cTypeAbsence() {
    return (String) storedValueForKey("cTypeAbsence");
  }

  public void setCTypeAbsence(String value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating cTypeAbsence from " + cTypeAbsence() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAbsence");
  }

  public String cTypeAbsenceHarpege() {
    return (String) storedValueForKey("cTypeAbsenceHarpege");
  }

  public void setCTypeAbsenceHarpege(String value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating cTypeAbsenceHarpege from " + cTypeAbsenceHarpege() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAbsenceHarpege");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String joursConsecutifs() {
    return (String) storedValueForKey("joursConsecutifs");
  }

  public void setJoursConsecutifs(String value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating joursConsecutifs from " + joursConsecutifs() + " to " + value);
    }
    takeStoredValueForKey(value, "joursConsecutifs");
  }

  public String lcTypeAbsence() {
    return (String) storedValueForKey("lcTypeAbsence");
  }

  public void setLcTypeAbsence(String value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating lcTypeAbsence from " + lcTypeAbsence() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeAbsence");
  }

  public String llTypeAbsence() {
    return (String) storedValueForKey("llTypeAbsence");
  }

  public void setLlTypeAbsence(String value) {
    if (_EOTypeAbsence.LOG.isDebugEnabled()) {
    	_EOTypeAbsence.LOG.debug( "updating llTypeAbsence from " + llTypeAbsence() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeAbsence");
  }


  public static EOTypeAbsence createTypeAbsence(EOEditingContext editingContext, String cTypeAbsence
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcTypeAbsence
, String llTypeAbsence
) {
    EOTypeAbsence eo = (EOTypeAbsence) EOUtilities.createAndInsertInstance(editingContext, _EOTypeAbsence.ENTITY_NAME);    
		eo.setCTypeAbsence(cTypeAbsence);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcTypeAbsence(lcTypeAbsence);
		eo.setLlTypeAbsence(llTypeAbsence);
    return eo;
  }

  public static NSArray<EOTypeAbsence> fetchAllTypeAbsences(EOEditingContext editingContext) {
    return _EOTypeAbsence.fetchAllTypeAbsences(editingContext, null);
  }

  public static NSArray<EOTypeAbsence> fetchAllTypeAbsences(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeAbsence.fetchTypeAbsences(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeAbsence> fetchTypeAbsences(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeAbsence.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeAbsence> eoObjects = (NSArray<EOTypeAbsence>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeAbsence fetchTypeAbsence(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAbsence.fetchTypeAbsence(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAbsence fetchTypeAbsence(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeAbsence> eoObjects = _EOTypeAbsence.fetchTypeAbsences(editingContext, qualifier, null);
    EOTypeAbsence eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeAbsence)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeAbsence that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAbsence fetchRequiredTypeAbsence(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAbsence.fetchRequiredTypeAbsence(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAbsence fetchRequiredTypeAbsence(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeAbsence eoObject = _EOTypeAbsence.fetchTypeAbsence(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeAbsence that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAbsence localInstanceIn(EOEditingContext editingContext, EOTypeAbsence eo) {
    EOTypeAbsence localInstance = (eo == null) ? null : (EOTypeAbsence)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
