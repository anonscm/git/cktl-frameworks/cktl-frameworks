// $LastChangedRevision$ DO NOT EDIT.  Make changes to EONatureBonifTaux.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EONatureBonifTaux extends  EOGenericRecord {
	public static final String ENTITY_NAME = "NatureBonifTaux";

	// Attributes
	public static final String NBT_CODE_KEY = "nbtCode";
	public static final String NBT_DEN_TAUX_KEY = "nbtDenTaux";
	public static final String NBT_LIBELLE_KEY = "nbtLibelle";
	public static final String NBT_NUM_TAUX_KEY = "nbtNumTaux";
	public static final String NBT_ORDRE_KEY = "nbtOrdre";

	// Relationships
	public static final String MANGUE_BONIFICATIONSES_KEY = "mangueBonificationses";

  private static Logger LOG = Logger.getLogger(_EONatureBonifTaux.class);

  public EONatureBonifTaux localInstanceIn(EOEditingContext editingContext) {
    EONatureBonifTaux localInstance = (EONatureBonifTaux)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String nbtCode() {
    return (String) storedValueForKey("nbtCode");
  }

  public void setNbtCode(String value) {
    if (_EONatureBonifTaux.LOG.isDebugEnabled()) {
    	_EONatureBonifTaux.LOG.debug( "updating nbtCode from " + nbtCode() + " to " + value);
    }
    takeStoredValueForKey(value, "nbtCode");
  }

  public Integer nbtDenTaux() {
    return (Integer) storedValueForKey("nbtDenTaux");
  }

  public void setNbtDenTaux(Integer value) {
    if (_EONatureBonifTaux.LOG.isDebugEnabled()) {
    	_EONatureBonifTaux.LOG.debug( "updating nbtDenTaux from " + nbtDenTaux() + " to " + value);
    }
    takeStoredValueForKey(value, "nbtDenTaux");
  }

  public String nbtLibelle() {
    return (String) storedValueForKey("nbtLibelle");
  }

  public void setNbtLibelle(String value) {
    if (_EONatureBonifTaux.LOG.isDebugEnabled()) {
    	_EONatureBonifTaux.LOG.debug( "updating nbtLibelle from " + nbtLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "nbtLibelle");
  }

  public Integer nbtNumTaux() {
    return (Integer) storedValueForKey("nbtNumTaux");
  }

  public void setNbtNumTaux(Integer value) {
    if (_EONatureBonifTaux.LOG.isDebugEnabled()) {
    	_EONatureBonifTaux.LOG.debug( "updating nbtNumTaux from " + nbtNumTaux() + " to " + value);
    }
    takeStoredValueForKey(value, "nbtNumTaux");
  }

  public Integer nbtOrdre() {
    return (Integer) storedValueForKey("nbtOrdre");
  }

  public void setNbtOrdre(Integer value) {
    if (_EONatureBonifTaux.LOG.isDebugEnabled()) {
    	_EONatureBonifTaux.LOG.debug( "updating nbtOrdre from " + nbtOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "nbtOrdre");
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications> mangueBonificationses() {
    return (NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications>)storedValueForKey("mangueBonificationses");
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications> mangueBonificationses(EOQualifier qualifier) {
    return mangueBonificationses(qualifier, null, false);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications> mangueBonificationses(EOQualifier qualifier, boolean fetch) {
    return mangueBonificationses(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications> mangueBonificationses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications.TAUX_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications.fetchMangueBonificationses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = mangueBonificationses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMangueBonificationsesRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications object) {
    if (_EONatureBonifTaux.LOG.isDebugEnabled()) {
      _EONatureBonifTaux.LOG.debug("adding " + object + " to mangueBonificationses relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "mangueBonificationses");
  }

  public void removeFromMangueBonificationsesRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications object) {
    if (_EONatureBonifTaux.LOG.isDebugEnabled()) {
      _EONatureBonifTaux.LOG.debug("removing " + object + " from mangueBonificationses relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "mangueBonificationses");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications createMangueBonificationsesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MangueBonifications");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "mangueBonificationses");
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications) eo;
  }

  public void deleteMangueBonificationsesRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "mangueBonificationses");
    editingContext().deleteObject(object);
  }

  public void deleteAllMangueBonificationsesRelationships() {
    Enumeration objects = mangueBonificationses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMangueBonificationsesRelationship((org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications)objects.nextElement());
    }
  }


  public static EONatureBonifTaux createNatureBonifTaux(EOEditingContext editingContext, String nbtCode
, Integer nbtDenTaux
, Integer nbtNumTaux
, Integer nbtOrdre
) {
    EONatureBonifTaux eo = (EONatureBonifTaux) EOUtilities.createAndInsertInstance(editingContext, _EONatureBonifTaux.ENTITY_NAME);    
		eo.setNbtCode(nbtCode);
		eo.setNbtDenTaux(nbtDenTaux);
		eo.setNbtNumTaux(nbtNumTaux);
		eo.setNbtOrdre(nbtOrdre);
    return eo;
  }

  public static NSArray<EONatureBonifTaux> fetchAllNatureBonifTauxes(EOEditingContext editingContext) {
    return _EONatureBonifTaux.fetchAllNatureBonifTauxes(editingContext, null);
  }

  public static NSArray<EONatureBonifTaux> fetchAllNatureBonifTauxes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONatureBonifTaux.fetchNatureBonifTauxes(editingContext, null, sortOrderings);
  }

  public static NSArray<EONatureBonifTaux> fetchNatureBonifTauxes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EONatureBonifTaux.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONatureBonifTaux> eoObjects = (NSArray<EONatureBonifTaux>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EONatureBonifTaux fetchNatureBonifTaux(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBonifTaux.fetchNatureBonifTaux(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBonifTaux fetchNatureBonifTaux(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONatureBonifTaux> eoObjects = _EONatureBonifTaux.fetchNatureBonifTauxes(editingContext, qualifier, null);
    EONatureBonifTaux eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EONatureBonifTaux)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NatureBonifTaux that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBonifTaux fetchRequiredNatureBonifTaux(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBonifTaux.fetchRequiredNatureBonifTaux(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBonifTaux fetchRequiredNatureBonifTaux(EOEditingContext editingContext, EOQualifier qualifier) {
    EONatureBonifTaux eoObject = _EONatureBonifTaux.fetchNatureBonifTaux(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NatureBonifTaux that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBonifTaux localInstanceIn(EOEditingContext editingContext, EONatureBonifTaux eo) {
    EONatureBonifTaux localInstance = (eo == null) ? null : (EONatureBonifTaux)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
