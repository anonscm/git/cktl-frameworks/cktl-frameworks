// EOTypeContratTravail.java
// Created on Tue Jan 29 09:23:22 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import java.util.HashMap;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.eocontrol.EOEditingContext;

public class EOTypeContratTravail extends _EOTypeContratTravail {
	public static String CODE_CDD_BUDGET_PROPRE = "C5";
	private static HashMap<String, EOTypeContratTravail> cTypeCache = new HashMap<String, EOTypeContratTravail>();

	public EOTypeContratTravail() {
		super();
	}

	// Méthodes ajoutées
	public boolean requiertGrade() {
		return temEquivGrade() != null && temEquivGrade().equals(CocktailConstantes.VRAI);
	}

	public boolean estCdi() {
		return temCdi() != null && temCdi().equals(CocktailConstantes.VRAI);
	}

	public boolean estRemunerationPrincipale() {
		return temRemunerationPrincipale() != null && temRemunerationPrincipale().equals(CocktailConstantes.VRAI);
	}

	public boolean estEnseignant() {
		return temEnseignant() != null && temEnseignant().equals(CocktailConstantes.VRAI);
	}

	public boolean estPourTitulaire() {
		return temTitulaire() != null && temTitulaire().equals(CocktailConstantes.VRAI);
	}

	public boolean tempsPartielPossible() {
		return temPartiel() != null && temPartiel().equals(CocktailConstantes.VRAI);
	}

	public boolean estHospitalier() {
		return temAhCuAo() != null && temAhCuAo().equals(CocktailConstantes.VRAI);
	}

	public boolean estVacataire() {
		if (cTypeContratTrav().equals("VA") || cTypeContratTrav().equals("VF") || cTypeContratTrav().equals("VN")) {
			return true;
		}
		return false;
	}

	public static EOTypeContratTravail getFromCode(EOEditingContext editingContext, String code) {
		if (EOImportParametres.cacheMemoireNiveauBasOuPlus() && cTypeCache.containsKey(code)) {
			return cTypeCache.get(code);
		}
		
		EOTypeContratTravail resultat = fetchTypeContratTravail(editingContext, C_TYPE_CONTRAT_TRAV_KEY, code);

		if (EOImportParametres.cacheMemoireNiveauBasOuPlus())
			cTypeCache.put(code, resultat);
		
		return resultat;
	}

	public static void resetCache() {
		cTypeCache.clear();
	}
}
