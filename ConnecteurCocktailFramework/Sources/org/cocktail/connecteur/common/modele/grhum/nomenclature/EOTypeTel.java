package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.apache.log4j.Logger;

public class EOTypeTel extends _EOTypeTel {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -2120565015249992929L;
	public static final String C_TYPE_TEL_PRV = "PRV";
	public static final String C_TYPE_TEL_INT = "INT";
    public static final String C_TYPE_TEL_PRF = "PRF";
    public static final String C_TYPE_TEL_PAR = "PAR";
    public static final String C_TYPE_TEL_ETUD = "ETUD";


    private static Logger log = Logger.getLogger(EOTypeTel.class);
}
