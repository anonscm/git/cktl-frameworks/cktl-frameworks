// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOChapitreArticle.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOChapitreArticle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ChapitreArticle";

	// Attributes
	public static final String C_ARTICLE_KEY = "cArticle";
	public static final String C_CHAPITRE_KEY = "cChapitre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_CHAPITRE_ARTICLE_KEY = "lcChapitreArticle";
	public static final String LL_CHAPITRE_ARTICLE_KEY = "llChapitreArticle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOChapitreArticle.class);

  public EOChapitreArticle localInstanceIn(EOEditingContext editingContext) {
    EOChapitreArticle localInstance = (EOChapitreArticle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cArticle() {
    return (Integer) storedValueForKey("cArticle");
  }

  public void setCArticle(Integer value) {
    if (_EOChapitreArticle.LOG.isDebugEnabled()) {
    	_EOChapitreArticle.LOG.debug( "updating cArticle from " + cArticle() + " to " + value);
    }
    takeStoredValueForKey(value, "cArticle");
  }

  public Integer cChapitre() {
    return (Integer) storedValueForKey("cChapitre");
  }

  public void setCChapitre(Integer value) {
    if (_EOChapitreArticle.LOG.isDebugEnabled()) {
    	_EOChapitreArticle.LOG.debug( "updating cChapitre from " + cChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cChapitre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOChapitreArticle.LOG.isDebugEnabled()) {
    	_EOChapitreArticle.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOChapitreArticle.LOG.isDebugEnabled()) {
    	_EOChapitreArticle.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcChapitreArticle() {
    return (String) storedValueForKey("lcChapitreArticle");
  }

  public void setLcChapitreArticle(String value) {
    if (_EOChapitreArticle.LOG.isDebugEnabled()) {
    	_EOChapitreArticle.LOG.debug( "updating lcChapitreArticle from " + lcChapitreArticle() + " to " + value);
    }
    takeStoredValueForKey(value, "lcChapitreArticle");
  }

  public String llChapitreArticle() {
    return (String) storedValueForKey("llChapitreArticle");
  }

  public void setLlChapitreArticle(String value) {
    if (_EOChapitreArticle.LOG.isDebugEnabled()) {
    	_EOChapitreArticle.LOG.debug( "updating llChapitreArticle from " + llChapitreArticle() + " to " + value);
    }
    takeStoredValueForKey(value, "llChapitreArticle");
  }


  public static EOChapitreArticle createChapitreArticle(EOEditingContext editingContext, Integer cArticle
, Integer cChapitre
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOChapitreArticle eo = (EOChapitreArticle) EOUtilities.createAndInsertInstance(editingContext, _EOChapitreArticle.ENTITY_NAME);    
		eo.setCArticle(cArticle);
		eo.setCChapitre(cChapitre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOChapitreArticle> fetchAllChapitreArticles(EOEditingContext editingContext) {
    return _EOChapitreArticle.fetchAllChapitreArticles(editingContext, null);
  }

  public static NSArray<EOChapitreArticle> fetchAllChapitreArticles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChapitreArticle.fetchChapitreArticles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOChapitreArticle> fetchChapitreArticles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOChapitreArticle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChapitreArticle> eoObjects = (NSArray<EOChapitreArticle>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOChapitreArticle fetchChapitreArticle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChapitreArticle.fetchChapitreArticle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChapitreArticle fetchChapitreArticle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChapitreArticle> eoObjects = _EOChapitreArticle.fetchChapitreArticles(editingContext, qualifier, null);
    EOChapitreArticle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOChapitreArticle)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ChapitreArticle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChapitreArticle fetchRequiredChapitreArticle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChapitreArticle.fetchRequiredChapitreArticle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChapitreArticle fetchRequiredChapitreArticle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChapitreArticle eoObject = _EOChapitreArticle.fetchChapitreArticle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ChapitreArticle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChapitreArticle localInstanceIn(EOEditingContext editingContext, EOChapitreArticle eo) {
    EOChapitreArticle localInstance = (eo == null) ? null : (EOChapitreArticle)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
