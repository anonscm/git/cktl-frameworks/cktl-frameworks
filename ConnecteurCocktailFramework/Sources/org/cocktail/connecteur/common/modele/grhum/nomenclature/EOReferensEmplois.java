package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IReferensEmploi;

import com.webobjects.foundation.NSDictionary;

public class EOReferensEmplois extends _EOReferensEmplois implements IReferensEmploi {
	private static Logger log = Logger.getLogger(EOReferensEmplois.class);

	public String clePrimaire() {
		return codeemploi();
	}
}
