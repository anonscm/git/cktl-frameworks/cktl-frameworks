// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOStatutJuridiqueStr.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStatutJuridiqueStr extends  EOGenericRecord {
	public static final String ENTITY_NAME = "StatutJuridiqueStr";

	// Attributes
	public static final String C_STATUT_JURIDIQUE_KEY = "cStatutJuridique";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String LL_STATUT_JURIDIQUE_KEY = "llStatutJuridique";
	public static final String TEM_STR_GERE_PST_KEY = "temStrGerePst";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOStatutJuridiqueStr.class);

  public EOStatutJuridiqueStr localInstanceIn(EOEditingContext editingContext) {
    EOStatutJuridiqueStr localInstance = (EOStatutJuridiqueStr)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStatutJuridique() {
    return (String) storedValueForKey("cStatutJuridique");
  }

  public void setCStatutJuridique(String value) {
    if (_EOStatutJuridiqueStr.LOG.isDebugEnabled()) {
    	_EOStatutJuridiqueStr.LOG.debug( "updating cStatutJuridique from " + cStatutJuridique() + " to " + value);
    }
    takeStoredValueForKey(value, "cStatutJuridique");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOStatutJuridiqueStr.LOG.isDebugEnabled()) {
    	_EOStatutJuridiqueStr.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOStatutJuridiqueStr.LOG.isDebugEnabled()) {
    	_EOStatutJuridiqueStr.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public String llStatutJuridique() {
    return (String) storedValueForKey("llStatutJuridique");
  }

  public void setLlStatutJuridique(String value) {
    if (_EOStatutJuridiqueStr.LOG.isDebugEnabled()) {
    	_EOStatutJuridiqueStr.LOG.debug( "updating llStatutJuridique from " + llStatutJuridique() + " to " + value);
    }
    takeStoredValueForKey(value, "llStatutJuridique");
  }

  public String temStrGerePst() {
    return (String) storedValueForKey("temStrGerePst");
  }

  public void setTemStrGerePst(String value) {
    if (_EOStatutJuridiqueStr.LOG.isDebugEnabled()) {
    	_EOStatutJuridiqueStr.LOG.debug( "updating temStrGerePst from " + temStrGerePst() + " to " + value);
    }
    takeStoredValueForKey(value, "temStrGerePst");
  }


  public static EOStatutJuridiqueStr createStatutJuridiqueStr(EOEditingContext editingContext, String cStatutJuridique
) {
    EOStatutJuridiqueStr eo = (EOStatutJuridiqueStr) EOUtilities.createAndInsertInstance(editingContext, _EOStatutJuridiqueStr.ENTITY_NAME);    
		eo.setCStatutJuridique(cStatutJuridique);
    return eo;
  }

  public static NSArray<EOStatutJuridiqueStr> fetchAllStatutJuridiqueStrs(EOEditingContext editingContext) {
    return _EOStatutJuridiqueStr.fetchAllStatutJuridiqueStrs(editingContext, null);
  }

  public static NSArray<EOStatutJuridiqueStr> fetchAllStatutJuridiqueStrs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStatutJuridiqueStr.fetchStatutJuridiqueStrs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStatutJuridiqueStr> fetchStatutJuridiqueStrs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStatutJuridiqueStr.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStatutJuridiqueStr> eoObjects = (NSArray<EOStatutJuridiqueStr>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStatutJuridiqueStr fetchStatutJuridiqueStr(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutJuridiqueStr.fetchStatutJuridiqueStr(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutJuridiqueStr fetchStatutJuridiqueStr(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStatutJuridiqueStr> eoObjects = _EOStatutJuridiqueStr.fetchStatutJuridiqueStrs(editingContext, qualifier, null);
    EOStatutJuridiqueStr eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStatutJuridiqueStr)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one StatutJuridiqueStr that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutJuridiqueStr fetchRequiredStatutJuridiqueStr(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutJuridiqueStr.fetchRequiredStatutJuridiqueStr(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutJuridiqueStr fetchRequiredStatutJuridiqueStr(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStatutJuridiqueStr eoObject = _EOStatutJuridiqueStr.fetchStatutJuridiqueStr(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no StatutJuridiqueStr that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutJuridiqueStr localInstanceIn(EOEditingContext editingContext, EOStatutJuridiqueStr eo) {
    EOStatutJuridiqueStr localInstance = (eo == null) ? null : (EOStatutJuridiqueStr)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
