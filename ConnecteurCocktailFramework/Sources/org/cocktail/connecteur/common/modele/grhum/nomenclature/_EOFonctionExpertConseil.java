// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOFonctionExpertConseil.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOFonctionExpertConseil extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FonctionExpertConseil";

	// Attributes
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_FONCTION_KEY = "cFonction";
	public static final String C_TYPE_DECHARGE_KEY = "cTypeDecharge";
	public static final String C_TYPE_PRIME_KEY = "cTypePrime";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_FONCTION_KEY = "lcFonction";
	public static final String LL_FONCTION_KEY = "llFonction";
	public static final String TEM_AVANCEMENT_SPEC_KEY = "temAvancementSpec";
	public static final String TEM_LOCAL_KEY = "temLocal";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOFonctionExpertConseil.class);

  public EOFonctionExpertConseil localInstanceIn(EOEditingContext editingContext) {
    EOFonctionExpertConseil localInstance = (EOFonctionExpertConseil)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public Integer cFonction() {
    return (Integer) storedValueForKey("cFonction");
  }

  public void setCFonction(Integer value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating cFonction from " + cFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "cFonction");
  }

  public String cTypeDecharge() {
    return (String) storedValueForKey("cTypeDecharge");
  }

  public void setCTypeDecharge(String value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating cTypeDecharge from " + cTypeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeDecharge");
  }

  public String cTypePrime() {
    return (String) storedValueForKey("cTypePrime");
  }

  public void setCTypePrime(String value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating cTypePrime from " + cTypePrime() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypePrime");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcFonction() {
    return (String) storedValueForKey("lcFonction");
  }

  public void setLcFonction(String value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating lcFonction from " + lcFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "lcFonction");
  }

  public String llFonction() {
    return (String) storedValueForKey("llFonction");
  }

  public void setLlFonction(String value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating llFonction from " + llFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "llFonction");
  }

  public String temAvancementSpec() {
    return (String) storedValueForKey("temAvancementSpec");
  }

  public void setTemAvancementSpec(String value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating temAvancementSpec from " + temAvancementSpec() + " to " + value);
    }
    takeStoredValueForKey(value, "temAvancementSpec");
  }

  public String temLocal() {
    return (String) storedValueForKey("temLocal");
  }

  public void setTemLocal(String value) {
    if (_EOFonctionExpertConseil.LOG.isDebugEnabled()) {
    	_EOFonctionExpertConseil.LOG.debug( "updating temLocal from " + temLocal() + " to " + value);
    }
    takeStoredValueForKey(value, "temLocal");
  }


  public static EOFonctionExpertConseil createFonctionExpertConseil(EOEditingContext editingContext, Integer cFonction
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcFonction
, String llFonction
, String temAvancementSpec
, String temLocal
) {
    EOFonctionExpertConseil eo = (EOFonctionExpertConseil) EOUtilities.createAndInsertInstance(editingContext, _EOFonctionExpertConseil.ENTITY_NAME);    
		eo.setCFonction(cFonction);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcFonction(lcFonction);
		eo.setLlFonction(llFonction);
		eo.setTemAvancementSpec(temAvancementSpec);
		eo.setTemLocal(temLocal);
    return eo;
  }

  public static NSArray<EOFonctionExpertConseil> fetchAllFonctionExpertConseils(EOEditingContext editingContext) {
    return _EOFonctionExpertConseil.fetchAllFonctionExpertConseils(editingContext, null);
  }

  public static NSArray<EOFonctionExpertConseil> fetchAllFonctionExpertConseils(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFonctionExpertConseil.fetchFonctionExpertConseils(editingContext, null, sortOrderings);
  }

  public static NSArray<EOFonctionExpertConseil> fetchFonctionExpertConseils(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFonctionExpertConseil.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFonctionExpertConseil> eoObjects = (NSArray<EOFonctionExpertConseil>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFonctionExpertConseil fetchFonctionExpertConseil(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFonctionExpertConseil.fetchFonctionExpertConseil(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFonctionExpertConseil fetchFonctionExpertConseil(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFonctionExpertConseil> eoObjects = _EOFonctionExpertConseil.fetchFonctionExpertConseils(editingContext, qualifier, null);
    EOFonctionExpertConseil eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFonctionExpertConseil)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FonctionExpertConseil that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFonctionExpertConseil fetchRequiredFonctionExpertConseil(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFonctionExpertConseil.fetchRequiredFonctionExpertConseil(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFonctionExpertConseil fetchRequiredFonctionExpertConseil(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFonctionExpertConseil eoObject = _EOFonctionExpertConseil.fetchFonctionExpertConseil(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FonctionExpertConseil that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFonctionExpertConseil localInstanceIn(EOEditingContext editingContext, EOFonctionExpertConseil eo) {
    EOFonctionExpertConseil localInstance = (eo == null) ? null : (EOFonctionExpertConseil)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
