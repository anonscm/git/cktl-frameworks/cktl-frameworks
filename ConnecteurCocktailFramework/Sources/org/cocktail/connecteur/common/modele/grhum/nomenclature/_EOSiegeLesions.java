// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSiegeLesions.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSiegeLesions extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SiegeLesions";

	// Attributes
	public static final String SLES_LIBELLE_KEY = "slesLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOSiegeLesions.class);

  public EOSiegeLesions localInstanceIn(EOEditingContext editingContext) {
    EOSiegeLesions localInstance = (EOSiegeLesions)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String slesLibelle() {
    return (String) storedValueForKey("slesLibelle");
  }

  public void setSlesLibelle(String value) {
    if (_EOSiegeLesions.LOG.isDebugEnabled()) {
    	_EOSiegeLesions.LOG.debug( "updating slesLibelle from " + slesLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "slesLibelle");
  }


  public static EOSiegeLesions createSiegeLesions(EOEditingContext editingContext) {
    EOSiegeLesions eo = (EOSiegeLesions) EOUtilities.createAndInsertInstance(editingContext, _EOSiegeLesions.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOSiegeLesions> fetchAllSiegeLesionses(EOEditingContext editingContext) {
    return _EOSiegeLesions.fetchAllSiegeLesionses(editingContext, null);
  }

  public static NSArray<EOSiegeLesions> fetchAllSiegeLesionses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSiegeLesions.fetchSiegeLesionses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSiegeLesions> fetchSiegeLesionses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSiegeLesions.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSiegeLesions> eoObjects = (NSArray<EOSiegeLesions>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSiegeLesions fetchSiegeLesions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSiegeLesions.fetchSiegeLesions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSiegeLesions fetchSiegeLesions(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSiegeLesions> eoObjects = _EOSiegeLesions.fetchSiegeLesionses(editingContext, qualifier, null);
    EOSiegeLesions eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSiegeLesions)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one SiegeLesions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSiegeLesions fetchRequiredSiegeLesions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSiegeLesions.fetchRequiredSiegeLesions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSiegeLesions fetchRequiredSiegeLesions(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSiegeLesions eoObject = _EOSiegeLesions.fetchSiegeLesions(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no SiegeLesions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSiegeLesions localInstanceIn(EOEditingContext editingContext, EOSiegeLesions eo) {
    EOSiegeLesions localInstance = (eo == null) ? null : (EOSiegeLesions)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
