// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeContratTravail.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeContratTravail extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeContratTravail";

	// Attributes
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String DUREE_INIT_CONTRAT_KEY = "dureeInitContrat";
	public static final String DUREE_MAX_CONTRAT_KEY = "dureeMaxContrat";
	public static final String LC_TYPE_CONTRAT_TRAV_KEY = "lcTypeContratTrav";
	public static final String LL_TYPE_CONTRAT_TRAV_KEY = "llTypeContratTrav";
	public static final String TEM_AH_CU_AO_KEY = "temAhCuAo";
	public static final String TEM_CDI_KEY = "temCdi";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_EQUIV_GRADE_KEY = "temEquivGrade";
	public static final String TEM_PARTIEL_KEY = "temPartiel";
	public static final String TEM_REMUNERATION_PRINCIPALE_KEY = "temRemunerationPrincipale";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeContratTravail.class);

  public EOTypeContratTravail localInstanceIn(EOEditingContext editingContext) {
    EOTypeContratTravail localInstance = (EOTypeContratTravail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public Integer dureeInitContrat() {
    return (Integer) storedValueForKey("dureeInitContrat");
  }

  public void setDureeInitContrat(Integer value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating dureeInitContrat from " + dureeInitContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeInitContrat");
  }

  public Integer dureeMaxContrat() {
    return (Integer) storedValueForKey("dureeMaxContrat");
  }

  public void setDureeMaxContrat(Integer value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating dureeMaxContrat from " + dureeMaxContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeMaxContrat");
  }

  public String lcTypeContratTrav() {
    return (String) storedValueForKey("lcTypeContratTrav");
  }

  public void setLcTypeContratTrav(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating lcTypeContratTrav from " + lcTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeContratTrav");
  }

  public String llTypeContratTrav() {
    return (String) storedValueForKey("llTypeContratTrav");
  }

  public void setLlTypeContratTrav(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating llTypeContratTrav from " + llTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeContratTrav");
  }

  public String temAhCuAo() {
    return (String) storedValueForKey("temAhCuAo");
  }

  public void setTemAhCuAo(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating temAhCuAo from " + temAhCuAo() + " to " + value);
    }
    takeStoredValueForKey(value, "temAhCuAo");
  }

  public String temCdi() {
    return (String) storedValueForKey("temCdi");
  }

  public void setTemCdi(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating temCdi from " + temCdi() + " to " + value);
    }
    takeStoredValueForKey(value, "temCdi");
  }

  public String temEnseignant() {
    return (String) storedValueForKey("temEnseignant");
  }

  public void setTemEnseignant(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating temEnseignant from " + temEnseignant() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnseignant");
  }

  public String temEquivGrade() {
    return (String) storedValueForKey("temEquivGrade");
  }

  public void setTemEquivGrade(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating temEquivGrade from " + temEquivGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "temEquivGrade");
  }

  public String temPartiel() {
    return (String) storedValueForKey("temPartiel");
  }

  public void setTemPartiel(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating temPartiel from " + temPartiel() + " to " + value);
    }
    takeStoredValueForKey(value, "temPartiel");
  }

  public String temRemunerationPrincipale() {
    return (String) storedValueForKey("temRemunerationPrincipale");
  }

  public void setTemRemunerationPrincipale(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating temRemunerationPrincipale from " + temRemunerationPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temRemunerationPrincipale");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOTypeContratTravail.LOG.isDebugEnabled()) {
    	_EOTypeContratTravail.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }


  public static EOTypeContratTravail createTypeContratTravail(EOEditingContext editingContext, String cTypeContratTrav
, String temEquivGrade
, String temRemunerationPrincipale
) {
    EOTypeContratTravail eo = (EOTypeContratTravail) EOUtilities.createAndInsertInstance(editingContext, _EOTypeContratTravail.ENTITY_NAME);    
		eo.setCTypeContratTrav(cTypeContratTrav);
		eo.setTemEquivGrade(temEquivGrade);
		eo.setTemRemunerationPrincipale(temRemunerationPrincipale);
    return eo;
  }

  public static NSArray<EOTypeContratTravail> fetchAllTypeContratTravails(EOEditingContext editingContext) {
    return _EOTypeContratTravail.fetchAllTypeContratTravails(editingContext, null);
  }

  public static NSArray<EOTypeContratTravail> fetchAllTypeContratTravails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeContratTravail.fetchTypeContratTravails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeContratTravail> fetchTypeContratTravails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeContratTravail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeContratTravail> eoObjects = (NSArray<EOTypeContratTravail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeContratTravail fetchTypeContratTravail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeContratTravail.fetchTypeContratTravail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeContratTravail fetchTypeContratTravail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeContratTravail> eoObjects = _EOTypeContratTravail.fetchTypeContratTravails(editingContext, qualifier, null);
    EOTypeContratTravail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeContratTravail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeContratTravail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeContratTravail fetchRequiredTypeContratTravail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeContratTravail.fetchRequiredTypeContratTravail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeContratTravail fetchRequiredTypeContratTravail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeContratTravail eoObject = _EOTypeContratTravail.fetchTypeContratTravail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeContratTravail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeContratTravail localInstanceIn(EOEditingContext editingContext, EOTypeContratTravail eo) {
    EOTypeContratTravail localInstance = (eo == null) ? null : (EOTypeContratTravail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
