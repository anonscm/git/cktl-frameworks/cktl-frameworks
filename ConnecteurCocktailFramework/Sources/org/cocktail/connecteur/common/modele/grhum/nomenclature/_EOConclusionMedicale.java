// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOConclusionMedicale.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOConclusionMedicale extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ConclusionMedicale";

	// Attributes
	public static final String COME_LIBELLE_KEY = "comeLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOConclusionMedicale.class);

  public EOConclusionMedicale localInstanceIn(EOEditingContext editingContext) {
    EOConclusionMedicale localInstance = (EOConclusionMedicale)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String comeLibelle() {
    return (String) storedValueForKey("comeLibelle");
  }

  public void setComeLibelle(String value) {
    if (_EOConclusionMedicale.LOG.isDebugEnabled()) {
    	_EOConclusionMedicale.LOG.debug( "updating comeLibelle from " + comeLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "comeLibelle");
  }


  public static EOConclusionMedicale createConclusionMedicale(EOEditingContext editingContext) {
    EOConclusionMedicale eo = (EOConclusionMedicale) EOUtilities.createAndInsertInstance(editingContext, _EOConclusionMedicale.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOConclusionMedicale> fetchAllConclusionMedicales(EOEditingContext editingContext) {
    return _EOConclusionMedicale.fetchAllConclusionMedicales(editingContext, null);
  }

  public static NSArray<EOConclusionMedicale> fetchAllConclusionMedicales(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOConclusionMedicale.fetchConclusionMedicales(editingContext, null, sortOrderings);
  }

  public static NSArray<EOConclusionMedicale> fetchConclusionMedicales(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOConclusionMedicale.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOConclusionMedicale> eoObjects = (NSArray<EOConclusionMedicale>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOConclusionMedicale fetchConclusionMedicale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOConclusionMedicale.fetchConclusionMedicale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOConclusionMedicale fetchConclusionMedicale(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOConclusionMedicale> eoObjects = _EOConclusionMedicale.fetchConclusionMedicales(editingContext, qualifier, null);
    EOConclusionMedicale eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOConclusionMedicale)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ConclusionMedicale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOConclusionMedicale fetchRequiredConclusionMedicale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOConclusionMedicale.fetchRequiredConclusionMedicale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOConclusionMedicale fetchRequiredConclusionMedicale(EOEditingContext editingContext, EOQualifier qualifier) {
    EOConclusionMedicale eoObject = _EOConclusionMedicale.fetchConclusionMedicale(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ConclusionMedicale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOConclusionMedicale localInstanceIn(EOEditingContext editingContext, EOConclusionMedicale eo) {
    EOConclusionMedicale localInstance = (eo == null) ? null : (EOConclusionMedicale)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
