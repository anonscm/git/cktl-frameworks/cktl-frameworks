package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import java.util.HashMap;

import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.eocontrol.EOEditingContext;

public class EOCivilite extends _EOCivilite {
	private static HashMap<String, EOCivilite> codeCache = new HashMap<String, EOCivilite>();
	private static HashMap<Integer, EOCivilite> codeOnpCache = new HashMap<Integer, EOCivilite>();

	public static EOCivilite getFromCode(EOEditingContext editingContext, String code) {
		if (EOImportParametres.cacheMemoireNiveauBasOuPlus() && codeCache.containsKey(code))
			return codeCache.get(code);
		
		EOCivilite resultat = EOCivilite.fetchCivilite(editingContext, C_CIVILITE_KEY, code);

		if (EOImportParametres.cacheMemoireNiveauBasOuPlus())
			codeCache.put(code, resultat);

		return resultat;
	}

	public static EOCivilite getFromCodeOnp(EOEditingContext editingContext, int onpCode) {
		Integer onpCodeInteger = new Integer(onpCode);
		
		if (EOImportParametres.cacheMemoireNiveauBasOuPlus() && codeOnpCache.containsKey(onpCodeInteger))
			return codeOnpCache.get(onpCodeInteger);

		EOCivilite resultat = EOCivilite.fetchCivilite(editingContext, C_CIVILITE_ONP_KEY, onpCode);

		if (EOImportParametres.cacheMemoireNiveauBasOuPlus())
			codeOnpCache.put(onpCodeInteger, resultat);

		return resultat;
	}

	public static void resetCache() {
		codeCache.clear();
		codeOnpCache.clear();
	}
}
