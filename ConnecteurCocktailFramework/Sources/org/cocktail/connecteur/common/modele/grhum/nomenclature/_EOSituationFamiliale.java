// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSituationFamiliale.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSituationFamiliale extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SituationFamiliale";

	// Attributes
	public static final String C_SITUATION_FAMILLE_KEY = "cSituationFamille";
	public static final String L_SITUATION_FAMILLE_KEY = "lSituationFamille";
	public static final String TEM_DATE_SIT_FAMILLE_KEY = "temDateSitFamille";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOSituationFamiliale.class);

  public EOSituationFamiliale localInstanceIn(EOEditingContext editingContext) {
    EOSituationFamiliale localInstance = (EOSituationFamiliale)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cSituationFamille() {
    return (String) storedValueForKey("cSituationFamille");
  }

  public void setCSituationFamille(String value) {
    if (_EOSituationFamiliale.LOG.isDebugEnabled()) {
    	_EOSituationFamiliale.LOG.debug( "updating cSituationFamille from " + cSituationFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "cSituationFamille");
  }

  public String lSituationFamille() {
    return (String) storedValueForKey("lSituationFamille");
  }

  public void setLSituationFamille(String value) {
    if (_EOSituationFamiliale.LOG.isDebugEnabled()) {
    	_EOSituationFamiliale.LOG.debug( "updating lSituationFamille from " + lSituationFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "lSituationFamille");
  }

  public String temDateSitFamille() {
    return (String) storedValueForKey("temDateSitFamille");
  }

  public void setTemDateSitFamille(String value) {
    if (_EOSituationFamiliale.LOG.isDebugEnabled()) {
    	_EOSituationFamiliale.LOG.debug( "updating temDateSitFamille from " + temDateSitFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "temDateSitFamille");
  }


  public static EOSituationFamiliale createSituationFamiliale(EOEditingContext editingContext, String cSituationFamille
, String temDateSitFamille
) {
    EOSituationFamiliale eo = (EOSituationFamiliale) EOUtilities.createAndInsertInstance(editingContext, _EOSituationFamiliale.ENTITY_NAME);    
		eo.setCSituationFamille(cSituationFamille);
		eo.setTemDateSitFamille(temDateSitFamille);
    return eo;
  }

  public static NSArray<EOSituationFamiliale> fetchAllSituationFamiliales(EOEditingContext editingContext) {
    return _EOSituationFamiliale.fetchAllSituationFamiliales(editingContext, null);
  }

  public static NSArray<EOSituationFamiliale> fetchAllSituationFamiliales(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSituationFamiliale.fetchSituationFamiliales(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSituationFamiliale> fetchSituationFamiliales(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSituationFamiliale.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSituationFamiliale> eoObjects = (NSArray<EOSituationFamiliale>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSituationFamiliale fetchSituationFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationFamiliale.fetchSituationFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationFamiliale fetchSituationFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSituationFamiliale> eoObjects = _EOSituationFamiliale.fetchSituationFamiliales(editingContext, qualifier, null);
    EOSituationFamiliale eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSituationFamiliale)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one SituationFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationFamiliale fetchRequiredSituationFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationFamiliale.fetchRequiredSituationFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationFamiliale fetchRequiredSituationFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSituationFamiliale eoObject = _EOSituationFamiliale.fetchSituationFamiliale(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no SituationFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationFamiliale localInstanceIn(EOEditingContext editingContext, EOSituationFamiliale eo) {
    EOSituationFamiliale localInstance = (eo == null) ? null : (EOSituationFamiliale)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
