// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSituationMilitaire.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSituationMilitaire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SituationMilitaire";

	// Attributes
	public static final String C_SIT_MILITAIRE_KEY = "cSitMilitaire";
	public static final String L_SIT_MILITAIRE_KEY = "lSitMilitaire";
	public static final String TEM_PERIODE_MILITAIR_KEY = "temPeriodeMilitair";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOSituationMilitaire.class);

  public EOSituationMilitaire localInstanceIn(EOEditingContext editingContext) {
    EOSituationMilitaire localInstance = (EOSituationMilitaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cSitMilitaire() {
    return (String) storedValueForKey("cSitMilitaire");
  }

  public void setCSitMilitaire(String value) {
    if (_EOSituationMilitaire.LOG.isDebugEnabled()) {
    	_EOSituationMilitaire.LOG.debug( "updating cSitMilitaire from " + cSitMilitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "cSitMilitaire");
  }

  public String lSitMilitaire() {
    return (String) storedValueForKey("lSitMilitaire");
  }

  public void setLSitMilitaire(String value) {
    if (_EOSituationMilitaire.LOG.isDebugEnabled()) {
    	_EOSituationMilitaire.LOG.debug( "updating lSitMilitaire from " + lSitMilitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "lSitMilitaire");
  }

  public String temPeriodeMilitair() {
    return (String) storedValueForKey("temPeriodeMilitair");
  }

  public void setTemPeriodeMilitair(String value) {
    if (_EOSituationMilitaire.LOG.isDebugEnabled()) {
    	_EOSituationMilitaire.LOG.debug( "updating temPeriodeMilitair from " + temPeriodeMilitair() + " to " + value);
    }
    takeStoredValueForKey(value, "temPeriodeMilitair");
  }


  public static EOSituationMilitaire createSituationMilitaire(EOEditingContext editingContext, String cSitMilitaire
) {
    EOSituationMilitaire eo = (EOSituationMilitaire) EOUtilities.createAndInsertInstance(editingContext, _EOSituationMilitaire.ENTITY_NAME);    
		eo.setCSitMilitaire(cSitMilitaire);
    return eo;
  }

  public static NSArray<EOSituationMilitaire> fetchAllSituationMilitaires(EOEditingContext editingContext) {
    return _EOSituationMilitaire.fetchAllSituationMilitaires(editingContext, null);
  }

  public static NSArray<EOSituationMilitaire> fetchAllSituationMilitaires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSituationMilitaire.fetchSituationMilitaires(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSituationMilitaire> fetchSituationMilitaires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSituationMilitaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSituationMilitaire> eoObjects = (NSArray<EOSituationMilitaire>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSituationMilitaire fetchSituationMilitaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationMilitaire.fetchSituationMilitaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationMilitaire fetchSituationMilitaire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSituationMilitaire> eoObjects = _EOSituationMilitaire.fetchSituationMilitaires(editingContext, qualifier, null);
    EOSituationMilitaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSituationMilitaire)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one SituationMilitaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationMilitaire fetchRequiredSituationMilitaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationMilitaire.fetchRequiredSituationMilitaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationMilitaire fetchRequiredSituationMilitaire(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSituationMilitaire eoObject = _EOSituationMilitaire.fetchSituationMilitaire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no SituationMilitaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationMilitaire localInstanceIn(EOEditingContext editingContext, EOSituationMilitaire eo) {
    EOSituationMilitaire localInstance = (eo == null) ? null : (EOSituationMilitaire)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
