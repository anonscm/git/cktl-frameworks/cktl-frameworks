// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOBapCorps.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOBapCorps extends  EOGenericRecord {
	public static final String ENTITY_NAME = "BapCorps";

	// Attributes
	public static final String C_BAP_CORPS_KEY = "cBapCorps";
	public static final String C_BAP_FAMILLE_KEY = "cBapFamille";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LL_BAP_CORPS_KEY = "llBapCorps";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOBapCorps.class);

  public EOBapCorps localInstanceIn(EOEditingContext editingContext) {
    EOBapCorps localInstance = (EOBapCorps)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBapCorps() {
    return (String) storedValueForKey("cBapCorps");
  }

  public void setCBapCorps(String value) {
    if (_EOBapCorps.LOG.isDebugEnabled()) {
    	_EOBapCorps.LOG.debug( "updating cBapCorps from " + cBapCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cBapCorps");
  }

  public String cBapFamille() {
    return (String) storedValueForKey("cBapFamille");
  }

  public void setCBapFamille(String value) {
    if (_EOBapCorps.LOG.isDebugEnabled()) {
    	_EOBapCorps.LOG.debug( "updating cBapFamille from " + cBapFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "cBapFamille");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBapCorps.LOG.isDebugEnabled()) {
    	_EOBapCorps.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOBapCorps.LOG.isDebugEnabled()) {
    	_EOBapCorps.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String llBapCorps() {
    return (String) storedValueForKey("llBapCorps");
  }

  public void setLlBapCorps(String value) {
    if (_EOBapCorps.LOG.isDebugEnabled()) {
    	_EOBapCorps.LOG.debug( "updating llBapCorps from " + llBapCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "llBapCorps");
  }


  public static EOBapCorps createBapCorps(EOEditingContext editingContext, String cBapCorps
) {
    EOBapCorps eo = (EOBapCorps) EOUtilities.createAndInsertInstance(editingContext, _EOBapCorps.ENTITY_NAME);    
		eo.setCBapCorps(cBapCorps);
    return eo;
  }

  public static NSArray<EOBapCorps> fetchAllBapCorpses(EOEditingContext editingContext) {
    return _EOBapCorps.fetchAllBapCorpses(editingContext, null);
  }

  public static NSArray<EOBapCorps> fetchAllBapCorpses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBapCorps.fetchBapCorpses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOBapCorps> fetchBapCorpses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOBapCorps.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBapCorps> eoObjects = (NSArray<EOBapCorps>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOBapCorps fetchBapCorps(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBapCorps.fetchBapCorps(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBapCorps fetchBapCorps(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBapCorps> eoObjects = _EOBapCorps.fetchBapCorpses(editingContext, qualifier, null);
    EOBapCorps eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOBapCorps)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one BapCorps that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBapCorps fetchRequiredBapCorps(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBapCorps.fetchRequiredBapCorps(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBapCorps fetchRequiredBapCorps(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBapCorps eoObject = _EOBapCorps.fetchBapCorps(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no BapCorps that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBapCorps localInstanceIn(EOEditingContext editingContext, EOBapCorps eo) {
    EOBapCorps localInstance = (eo == null) ? null : (EOBapCorps)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
