package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import java.util.HashMap;

import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class EOBap extends _EOBap implements IBap {
	private static HashMap<String, EOBap> codeCache = new HashMap<String, EOBap>();

	public String clePrimaire() {
		return cBap();
	}

	public boolean estRenseigne() {
		return (cBap() != null && !cBap().equals(IBap.CODE_NON_RENSEIGNE));
	}

	public static EOBap getFromCode(EOEditingContext editingContext, String code) {
		if (code == null || editingContext == null)
			return null;

		if (EOImportParametres.cacheMemoireNiveauBasOuPlus() && codeCache.containsKey(code)) {
			return codeCache.get(code);
		}
		EOBap resultat = (EOBap) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOBap.ENTITY_NAME, EOBap.C_BAP_KEY, code);

		if (EOImportParametres.cacheMemoireNiveauBasOuPlus())
			codeCache.put(code, resultat);

		return resultat;
	}

	public static void resetCache() {
		codeCache.clear();
	}
}
