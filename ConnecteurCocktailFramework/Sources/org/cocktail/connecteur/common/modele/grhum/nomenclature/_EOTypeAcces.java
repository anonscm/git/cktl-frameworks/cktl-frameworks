// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeAcces.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeAcces extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeAcces";

	// Attributes
	public static final String C_TYPE_ACCES_KEY = "cTypeAcces";
	public static final String LC_TYPE_ACCES_KEY = "lcTypeAcces";
	public static final String LL_TYPE_ACCES_KEY = "llTypeAcces";
	public static final String TEM_ARRIVEE_KEY = "temArrivee";
	public static final String TEM_INTEGRATION_KEY = "temIntegration";
	public static final String TEM_RECLASSEMENT_KEY = "temReclassement";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeAcces.class);

  public EOTypeAcces localInstanceIn(EOEditingContext editingContext) {
    EOTypeAcces localInstance = (EOTypeAcces)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeAcces() {
    return (String) storedValueForKey("cTypeAcces");
  }

  public void setCTypeAcces(String value) {
    if (_EOTypeAcces.LOG.isDebugEnabled()) {
    	_EOTypeAcces.LOG.debug( "updating cTypeAcces from " + cTypeAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAcces");
  }

  public String lcTypeAcces() {
    return (String) storedValueForKey("lcTypeAcces");
  }

  public void setLcTypeAcces(String value) {
    if (_EOTypeAcces.LOG.isDebugEnabled()) {
    	_EOTypeAcces.LOG.debug( "updating lcTypeAcces from " + lcTypeAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeAcces");
  }

  public String llTypeAcces() {
    return (String) storedValueForKey("llTypeAcces");
  }

  public void setLlTypeAcces(String value) {
    if (_EOTypeAcces.LOG.isDebugEnabled()) {
    	_EOTypeAcces.LOG.debug( "updating llTypeAcces from " + llTypeAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeAcces");
  }

  public String temArrivee() {
    return (String) storedValueForKey("temArrivee");
  }

  public void setTemArrivee(String value) {
    if (_EOTypeAcces.LOG.isDebugEnabled()) {
    	_EOTypeAcces.LOG.debug( "updating temArrivee from " + temArrivee() + " to " + value);
    }
    takeStoredValueForKey(value, "temArrivee");
  }

  public String temIntegration() {
    return (String) storedValueForKey("temIntegration");
  }

  public void setTemIntegration(String value) {
    if (_EOTypeAcces.LOG.isDebugEnabled()) {
    	_EOTypeAcces.LOG.debug( "updating temIntegration from " + temIntegration() + " to " + value);
    }
    takeStoredValueForKey(value, "temIntegration");
  }

  public String temReclassement() {
    return (String) storedValueForKey("temReclassement");
  }

  public void setTemReclassement(String value) {
    if (_EOTypeAcces.LOG.isDebugEnabled()) {
    	_EOTypeAcces.LOG.debug( "updating temReclassement from " + temReclassement() + " to " + value);
    }
    takeStoredValueForKey(value, "temReclassement");
  }


  public static EOTypeAcces createTypeAcces(EOEditingContext editingContext, String cTypeAcces
) {
    EOTypeAcces eo = (EOTypeAcces) EOUtilities.createAndInsertInstance(editingContext, _EOTypeAcces.ENTITY_NAME);    
		eo.setCTypeAcces(cTypeAcces);
    return eo;
  }

  public static NSArray<EOTypeAcces> fetchAllTypeAcceses(EOEditingContext editingContext) {
    return _EOTypeAcces.fetchAllTypeAcceses(editingContext, null);
  }

  public static NSArray<EOTypeAcces> fetchAllTypeAcceses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeAcces.fetchTypeAcceses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeAcces> fetchTypeAcceses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeAcces.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeAcces> eoObjects = (NSArray<EOTypeAcces>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeAcces fetchTypeAcces(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAcces.fetchTypeAcces(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAcces fetchTypeAcces(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeAcces> eoObjects = _EOTypeAcces.fetchTypeAcceses(editingContext, qualifier, null);
    EOTypeAcces eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeAcces)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeAcces that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAcces fetchRequiredTypeAcces(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAcces.fetchRequiredTypeAcces(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAcces fetchRequiredTypeAcces(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeAcces eoObject = _EOTypeAcces.fetchTypeAcces(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeAcces that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAcces localInstanceIn(EOEditingContext editingContext, EOTypeAcces eo) {
    EOTypeAcces localInstance = (eo == null) ? null : (EOTypeAcces)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
