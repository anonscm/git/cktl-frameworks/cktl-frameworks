package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import java.util.HashMap;

import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.eocontrol.EOEditingContext;

public class EORne extends _EORne {
	private static HashMap<String, EORne> codeCache = new HashMap<String, EORne>();

	public static EORne getFromCode(EOEditingContext editingContext, String code) {
		if (code == null)
			return null;

		if (EOImportParametres.cacheMemoireNiveauMoyenOuPlus() && codeCache.containsKey(code)) {
			return codeCache.get(code);
		}

		EORne resultat = EORne.fetchRne(editingContext, C_RNE_KEY, code);

		if (EOImportParametres.cacheMemoireNiveauMoyenOuPlus())
			codeCache.put(code, resultat);

		return resultat;
	}

	public static void resetCache() {
		codeCache.clear();
	}
}
