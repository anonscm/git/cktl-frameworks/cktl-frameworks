package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOCorps extends _EOCorps {
  public static EOCorps getFromCode(EOEditingContext editingContext,String cCorps) {
	  if (cCorps==null)
		  return null;
	  return fetchCorps(editingContext, C_CORPS_KEY, cCorps);
  }
}
