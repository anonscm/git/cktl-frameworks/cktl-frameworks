package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOQualiteIndividu extends _EOQualiteIndividu {
	public static EOQualiteIndividu getFromCode(EOEditingContext editingContext, String codeQualite) {
		return fetchQualiteIndividu(editingContext, C_QUALITE_KEY, codeQualite);
	}
}
