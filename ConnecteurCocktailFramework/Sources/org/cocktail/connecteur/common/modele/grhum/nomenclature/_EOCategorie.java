// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCategorie.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCategorie extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Categorie";

	// Attributes
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String LC_CATEGORIE_KEY = "lcCategorie";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOCategorie.class);

  public EOCategorie localInstanceIn(EOEditingContext editingContext) {
    EOCategorie localInstance = (EOCategorie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOCategorie.LOG.isDebugEnabled()) {
    	_EOCategorie.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public String lcCategorie() {
    return (String) storedValueForKey("lcCategorie");
  }

  public void setLcCategorie(String value) {
    if (_EOCategorie.LOG.isDebugEnabled()) {
    	_EOCategorie.LOG.debug( "updating lcCategorie from " + lcCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "lcCategorie");
  }


  public static EOCategorie createCategorie(EOEditingContext editingContext, String cCategorie
) {
    EOCategorie eo = (EOCategorie) EOUtilities.createAndInsertInstance(editingContext, _EOCategorie.ENTITY_NAME);    
		eo.setCCategorie(cCategorie);
    return eo;
  }

  public static NSArray<EOCategorie> fetchAllCategories(EOEditingContext editingContext) {
    return _EOCategorie.fetchAllCategories(editingContext, null);
  }

  public static NSArray<EOCategorie> fetchAllCategories(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCategorie.fetchCategories(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCategorie> fetchCategories(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCategorie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCategorie> eoObjects = (NSArray<EOCategorie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCategorie fetchCategorie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCategorie.fetchCategorie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCategorie fetchCategorie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCategorie> eoObjects = _EOCategorie.fetchCategories(editingContext, qualifier, null);
    EOCategorie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCategorie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Categorie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCategorie fetchRequiredCategorie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCategorie.fetchRequiredCategorie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCategorie fetchRequiredCategorie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCategorie eoObject = _EOCategorie.fetchCategorie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Categorie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCategorie localInstanceIn(EOEditingContext editingContext, EOCategorie eo) {
    EOCategorie localInstance = (eo == null) ? null : (EOCategorie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
