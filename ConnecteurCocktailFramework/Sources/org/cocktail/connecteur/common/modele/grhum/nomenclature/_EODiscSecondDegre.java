// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODiscSecondDegre.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODiscSecondDegre extends  EOGenericRecord {
	public static final String ENTITY_NAME = "DiscSecondDegre";

	// Attributes
	public static final String C_DISC_SECOND_DEGRE_KEY = "cDiscSecondDegre";
	public static final String LC_DISC_SECOND_DEGRE_KEY = "lcDiscSecondDegre";
	public static final String LL_DISC_SECOND_DEGRE_KEY = "llDiscSecondDegre";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EODiscSecondDegre.class);

  public EODiscSecondDegre localInstanceIn(EOEditingContext editingContext) {
    EODiscSecondDegre localInstance = (EODiscSecondDegre)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDiscSecondDegre() {
    return (String) storedValueForKey("cDiscSecondDegre");
  }

  public void setCDiscSecondDegre(String value) {
    if (_EODiscSecondDegre.LOG.isDebugEnabled()) {
    	_EODiscSecondDegre.LOG.debug( "updating cDiscSecondDegre from " + cDiscSecondDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiscSecondDegre");
  }

  public String lcDiscSecondDegre() {
    return (String) storedValueForKey("lcDiscSecondDegre");
  }

  public void setLcDiscSecondDegre(String value) {
    if (_EODiscSecondDegre.LOG.isDebugEnabled()) {
    	_EODiscSecondDegre.LOG.debug( "updating lcDiscSecondDegre from " + lcDiscSecondDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "lcDiscSecondDegre");
  }

  public String llDiscSecondDegre() {
    return (String) storedValueForKey("llDiscSecondDegre");
  }

  public void setLlDiscSecondDegre(String value) {
    if (_EODiscSecondDegre.LOG.isDebugEnabled()) {
    	_EODiscSecondDegre.LOG.debug( "updating llDiscSecondDegre from " + llDiscSecondDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "llDiscSecondDegre");
  }


  public static EODiscSecondDegre createDiscSecondDegre(EOEditingContext editingContext, String cDiscSecondDegre
) {
    EODiscSecondDegre eo = (EODiscSecondDegre) EOUtilities.createAndInsertInstance(editingContext, _EODiscSecondDegre.ENTITY_NAME);    
		eo.setCDiscSecondDegre(cDiscSecondDegre);
    return eo;
  }

  public static NSArray<EODiscSecondDegre> fetchAllDiscSecondDegres(EOEditingContext editingContext) {
    return _EODiscSecondDegre.fetchAllDiscSecondDegres(editingContext, null);
  }

  public static NSArray<EODiscSecondDegre> fetchAllDiscSecondDegres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODiscSecondDegre.fetchDiscSecondDegres(editingContext, null, sortOrderings);
  }

  public static NSArray<EODiscSecondDegre> fetchDiscSecondDegres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODiscSecondDegre.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODiscSecondDegre> eoObjects = (NSArray<EODiscSecondDegre>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODiscSecondDegre fetchDiscSecondDegre(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiscSecondDegre.fetchDiscSecondDegre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiscSecondDegre fetchDiscSecondDegre(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODiscSecondDegre> eoObjects = _EODiscSecondDegre.fetchDiscSecondDegres(editingContext, qualifier, null);
    EODiscSecondDegre eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODiscSecondDegre)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one DiscSecondDegre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiscSecondDegre fetchRequiredDiscSecondDegre(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiscSecondDegre.fetchRequiredDiscSecondDegre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiscSecondDegre fetchRequiredDiscSecondDegre(EOEditingContext editingContext, EOQualifier qualifier) {
    EODiscSecondDegre eoObject = _EODiscSecondDegre.fetchDiscSecondDegre(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no DiscSecondDegre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiscSecondDegre localInstanceIn(EOEditingContext editingContext, EODiscSecondDegre eo) {
    EODiscSecondDegre localInstance = (eo == null) ? null : (EODiscSecondDegre)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
