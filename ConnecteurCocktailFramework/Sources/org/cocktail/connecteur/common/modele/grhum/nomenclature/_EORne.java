// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORne.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORne extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Rne";

	// Attributes
	public static final String C_RNE_KEY = "cRne";
	public static final String LL_RNE_KEY = "llRne";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EORne.class);

  public EORne localInstanceIn(EOEditingContext editingContext) {
    EORne localInstance = (EORne)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EORne.LOG.isDebugEnabled()) {
    	_EORne.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String llRne() {
    return (String) storedValueForKey("llRne");
  }

  public void setLlRne(String value) {
    if (_EORne.LOG.isDebugEnabled()) {
    	_EORne.LOG.debug( "updating llRne from " + llRne() + " to " + value);
    }
    takeStoredValueForKey(value, "llRne");
  }


  public static EORne createRne(EOEditingContext editingContext, String cRne
) {
    EORne eo = (EORne) EOUtilities.createAndInsertInstance(editingContext, _EORne.ENTITY_NAME);    
		eo.setCRne(cRne);
    return eo;
  }

  public static NSArray<EORne> fetchAllRnes(EOEditingContext editingContext) {
    return _EORne.fetchAllRnes(editingContext, null);
  }

  public static NSArray<EORne> fetchAllRnes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORne.fetchRnes(editingContext, null, sortOrderings);
  }

  public static NSArray<EORne> fetchRnes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORne.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORne> eoObjects = (NSArray<EORne>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORne fetchRne(EOEditingContext editingContext, String keyName, Object value) {
    return _EORne.fetchRne(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORne fetchRne(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORne> eoObjects = _EORne.fetchRnes(editingContext, qualifier, null);
    EORne eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORne)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Rne that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORne fetchRequiredRne(EOEditingContext editingContext, String keyName, Object value) {
    return _EORne.fetchRequiredRne(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORne fetchRequiredRne(EOEditingContext editingContext, EOQualifier qualifier) {
    EORne eoObject = _EORne.fetchRne(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Rne that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORne localInstanceIn(EOEditingContext editingContext, EORne eo) {
    EORne localInstance = (eo == null) ? null : (EORne)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
