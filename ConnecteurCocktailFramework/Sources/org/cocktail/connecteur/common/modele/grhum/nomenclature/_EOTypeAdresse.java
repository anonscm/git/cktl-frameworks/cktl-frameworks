// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeAdresse.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeAdresse extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeAdresse";

	// Attributes
	public static final String TADR_CODE_KEY = "tadrCode";
	public static final String TADR_LIBELLE_COURT_KEY = "tadrLibelleCourt";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeAdresse.class);

  public EOTypeAdresse localInstanceIn(EOEditingContext editingContext) {
    EOTypeAdresse localInstance = (EOTypeAdresse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tadrCode() {
    return (String) storedValueForKey("tadrCode");
  }

  public void setTadrCode(String value) {
    if (_EOTypeAdresse.LOG.isDebugEnabled()) {
    	_EOTypeAdresse.LOG.debug( "updating tadrCode from " + tadrCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tadrCode");
  }

  public String tadrLibelleCourt() {
    return (String) storedValueForKey("tadrLibelleCourt");
  }

  public void setTadrLibelleCourt(String value) {
    if (_EOTypeAdresse.LOG.isDebugEnabled()) {
    	_EOTypeAdresse.LOG.debug( "updating tadrLibelleCourt from " + tadrLibelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, "tadrLibelleCourt");
  }


  public static EOTypeAdresse createTypeAdresse(EOEditingContext editingContext, String tadrCode
) {
    EOTypeAdresse eo = (EOTypeAdresse) EOUtilities.createAndInsertInstance(editingContext, _EOTypeAdresse.ENTITY_NAME);    
		eo.setTadrCode(tadrCode);
    return eo;
  }

  public static NSArray<EOTypeAdresse> fetchAllTypeAdresses(EOEditingContext editingContext) {
    return _EOTypeAdresse.fetchAllTypeAdresses(editingContext, null);
  }

  public static NSArray<EOTypeAdresse> fetchAllTypeAdresses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeAdresse.fetchTypeAdresses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeAdresse> fetchTypeAdresses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeAdresse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeAdresse> eoObjects = (NSArray<EOTypeAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeAdresse fetchTypeAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAdresse.fetchTypeAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAdresse fetchTypeAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeAdresse> eoObjects = _EOTypeAdresse.fetchTypeAdresses(editingContext, qualifier, null);
    EOTypeAdresse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeAdresse)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeAdresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAdresse fetchRequiredTypeAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAdresse.fetchRequiredTypeAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAdresse fetchRequiredTypeAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeAdresse eoObject = _EOTypeAdresse.fetchTypeAdresse(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeAdresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAdresse localInstanceIn(EOEditingContext editingContext, EOTypeAdresse eo) {
    EOTypeAdresse localInstance = (eo == null) ? null : (EOTypeAdresse)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
