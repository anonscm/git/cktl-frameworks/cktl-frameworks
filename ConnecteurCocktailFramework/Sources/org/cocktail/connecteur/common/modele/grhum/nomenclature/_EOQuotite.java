// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOQuotite.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOQuotite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Quotite";

	// Attributes
	public static final String DEN_QUOTITE_KEY = "denQuotite";
	public static final String NUM_QUOTITE_KEY = "numQuotite";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOQuotite.class);

  public EOQuotite localInstanceIn(EOEditingContext editingContext) {
    EOQuotite localInstance = (EOQuotite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Long denQuotite() {
    return (Long) storedValueForKey("denQuotite");
  }

  public void setDenQuotite(Long value) {
    if (_EOQuotite.LOG.isDebugEnabled()) {
    	_EOQuotite.LOG.debug( "updating denQuotite from " + denQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotite");
  }

  public Long numQuotite() {
    return (Long) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Long value) {
    if (_EOQuotite.LOG.isDebugEnabled()) {
    	_EOQuotite.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }


  public static EOQuotite createQuotite(EOEditingContext editingContext, Long denQuotite
, Long numQuotite
) {
    EOQuotite eo = (EOQuotite) EOUtilities.createAndInsertInstance(editingContext, _EOQuotite.ENTITY_NAME);    
		eo.setDenQuotite(denQuotite);
		eo.setNumQuotite(numQuotite);
    return eo;
  }

  public static NSArray<EOQuotite> fetchAllQuotites(EOEditingContext editingContext) {
    return _EOQuotite.fetchAllQuotites(editingContext, null);
  }

  public static NSArray<EOQuotite> fetchAllQuotites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOQuotite.fetchQuotites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOQuotite> fetchQuotites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOQuotite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOQuotite> eoObjects = (NSArray<EOQuotite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOQuotite fetchQuotite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOQuotite.fetchQuotite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOQuotite fetchQuotite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOQuotite> eoObjects = _EOQuotite.fetchQuotites(editingContext, qualifier, null);
    EOQuotite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOQuotite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Quotite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOQuotite fetchRequiredQuotite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOQuotite.fetchRequiredQuotite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOQuotite fetchRequiredQuotite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOQuotite eoObject = _EOQuotite.fetchQuotite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Quotite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOQuotite localInstanceIn(EOEditingContext editingContext, EOQuotite eo) {
    EOQuotite localInstance = (eo == null) ? null : (EOQuotite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
