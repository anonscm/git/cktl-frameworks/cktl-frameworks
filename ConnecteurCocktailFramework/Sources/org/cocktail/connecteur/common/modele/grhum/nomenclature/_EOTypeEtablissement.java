// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeEtablissement.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeEtablissement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeEtablissement";

	// Attributes
	public static final String C_TYPE_ETABLISSEMEN_KEY = "cTypeEtablissemen";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String LC_TYPE_ETABLISSEMEN_KEY = "lcTypeEtablissemen";
	public static final String LL_TYPE_ETABLISSEMEN_KEY = "llTypeEtablissemen";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeEtablissement.class);

  public EOTypeEtablissement localInstanceIn(EOEditingContext editingContext) {
    EOTypeEtablissement localInstance = (EOTypeEtablissement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeEtablissemen() {
    return (String) storedValueForKey("cTypeEtablissemen");
  }

  public void setCTypeEtablissemen(String value) {
    if (_EOTypeEtablissement.LOG.isDebugEnabled()) {
    	_EOTypeEtablissement.LOG.debug( "updating cTypeEtablissemen from " + cTypeEtablissemen() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeEtablissemen");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOTypeEtablissement.LOG.isDebugEnabled()) {
    	_EOTypeEtablissement.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOTypeEtablissement.LOG.isDebugEnabled()) {
    	_EOTypeEtablissement.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public String lcTypeEtablissemen() {
    return (String) storedValueForKey("lcTypeEtablissemen");
  }

  public void setLcTypeEtablissemen(String value) {
    if (_EOTypeEtablissement.LOG.isDebugEnabled()) {
    	_EOTypeEtablissement.LOG.debug( "updating lcTypeEtablissemen from " + lcTypeEtablissemen() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeEtablissemen");
  }

  public String llTypeEtablissemen() {
    return (String) storedValueForKey("llTypeEtablissemen");
  }

  public void setLlTypeEtablissemen(String value) {
    if (_EOTypeEtablissement.LOG.isDebugEnabled()) {
    	_EOTypeEtablissement.LOG.debug( "updating llTypeEtablissemen from " + llTypeEtablissemen() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeEtablissemen");
  }


  public static EOTypeEtablissement createTypeEtablissement(EOEditingContext editingContext, String cTypeEtablissemen
) {
    EOTypeEtablissement eo = (EOTypeEtablissement) EOUtilities.createAndInsertInstance(editingContext, _EOTypeEtablissement.ENTITY_NAME);    
		eo.setCTypeEtablissemen(cTypeEtablissemen);
    return eo;
  }

  public static NSArray<EOTypeEtablissement> fetchAllTypeEtablissements(EOEditingContext editingContext) {
    return _EOTypeEtablissement.fetchAllTypeEtablissements(editingContext, null);
  }

  public static NSArray<EOTypeEtablissement> fetchAllTypeEtablissements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeEtablissement.fetchTypeEtablissements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeEtablissement> fetchTypeEtablissements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeEtablissement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeEtablissement> eoObjects = (NSArray<EOTypeEtablissement>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeEtablissement fetchTypeEtablissement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEtablissement.fetchTypeEtablissement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEtablissement fetchTypeEtablissement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeEtablissement> eoObjects = _EOTypeEtablissement.fetchTypeEtablissements(editingContext, qualifier, null);
    EOTypeEtablissement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeEtablissement)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeEtablissement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEtablissement fetchRequiredTypeEtablissement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEtablissement.fetchRequiredTypeEtablissement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEtablissement fetchRequiredTypeEtablissement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeEtablissement eoObject = _EOTypeEtablissement.fetchTypeEtablissement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeEtablissement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEtablissement localInstanceIn(EOEditingContext editingContext, EOTypeEtablissement eo) {
    EOTypeEtablissement localInstance = (eo == null) ? null : (EOTypeEtablissement)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
