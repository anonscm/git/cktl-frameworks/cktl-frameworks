// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeCsf.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeCsf extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeCsf";

	// Attributes
	public static final String TCSF_CODE_KEY = "tcsfCode";
	public static final String TCSF_LIBELLE_KEY = "tcsfLibelle";
	public static final String TCSF_ORDRE_KEY = "tcsfOrdre";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeCsf.class);

  public EOTypeCsf localInstanceIn(EOEditingContext editingContext) {
    EOTypeCsf localInstance = (EOTypeCsf)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tcsfCode() {
    return (String) storedValueForKey("tcsfCode");
  }

  public void setTcsfCode(String value) {
    if (_EOTypeCsf.LOG.isDebugEnabled()) {
    	_EOTypeCsf.LOG.debug( "updating tcsfCode from " + tcsfCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tcsfCode");
  }

  public String tcsfLibelle() {
    return (String) storedValueForKey("tcsfLibelle");
  }

  public void setTcsfLibelle(String value) {
    if (_EOTypeCsf.LOG.isDebugEnabled()) {
    	_EOTypeCsf.LOG.debug( "updating tcsfLibelle from " + tcsfLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tcsfLibelle");
  }

  public Integer tcsfOrdre() {
    return (Integer) storedValueForKey("tcsfOrdre");
  }

  public void setTcsfOrdre(Integer value) {
    if (_EOTypeCsf.LOG.isDebugEnabled()) {
    	_EOTypeCsf.LOG.debug( "updating tcsfOrdre from " + tcsfOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "tcsfOrdre");
  }


  public static EOTypeCsf createTypeCsf(EOEditingContext editingContext, Integer tcsfOrdre
) {
    EOTypeCsf eo = (EOTypeCsf) EOUtilities.createAndInsertInstance(editingContext, _EOTypeCsf.ENTITY_NAME);    
		eo.setTcsfOrdre(tcsfOrdre);
    return eo;
  }

  public static NSArray<EOTypeCsf> fetchAllTypeCsfs(EOEditingContext editingContext) {
    return _EOTypeCsf.fetchAllTypeCsfs(editingContext, null);
  }

  public static NSArray<EOTypeCsf> fetchAllTypeCsfs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeCsf.fetchTypeCsfs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeCsf> fetchTypeCsfs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeCsf.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeCsf> eoObjects = (NSArray<EOTypeCsf>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeCsf fetchTypeCsf(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCsf.fetchTypeCsf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCsf fetchTypeCsf(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeCsf> eoObjects = _EOTypeCsf.fetchTypeCsfs(editingContext, qualifier, null);
    EOTypeCsf eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeCsf)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeCsf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCsf fetchRequiredTypeCsf(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCsf.fetchRequiredTypeCsf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCsf fetchRequiredTypeCsf(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeCsf eoObject = _EOTypeCsf.fetchTypeCsf(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeCsf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCsf localInstanceIn(EOEditingContext editingContext, EOTypeCsf eo) {
    EOTypeCsf localInstance = (eo == null) ? null : (EOTypeCsf)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
