//EOMotifTempsPartiel.java
//Created on Tue Nov 22 10:06:04 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;

import com.webobjects.eocontrol.EOGenericRecord;

public class EOMotifTempsPartiel extends EOGenericRecord {
	public static String SUR_AUTORISATION = "SA";
	private static String MOTIF_ELEVER_ENFANT = "ME";
	private static String MOTIF_ELEVER_ENFANT_ADOPTE = "EA";
	public static String MOTIF_SOINS = "MS";
	public static String MOTIF_HANDICAPE = "FH";
	public EOMotifTempsPartiel() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOMotifTempsPartiel(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */

	public String lcMotifTempsPartiel() {
		return (String)storedValueForKey("lcMotifTempsPartiel");
	}

	public void setLcMotifTempsPartiel(String value) {
		takeStoredValueForKey(value, "lcMotifTempsPartiel");
	}

	/* public String llMotifTempsPartiel() {
        return (String)storedValueForKey("llMotifTempsPartiel");
    }

    public void setLlMotifTempsPartiel(String value) {
        takeStoredValueForKey(value, "llMotifTempsPartiel");
    }
	 */
	public String temCtrlDureeTps() {
		return (String)storedValueForKey("temCtrlDureeTps");
	}

	public void setTemCtrlDureeTps(String value) {
		takeStoredValueForKey(value, "temCtrlDureeTps");
	}

	public String cMotifTempsPartiel() {
		return (String)storedValueForKey("cMotifTempsPartiel");
	}

	public void setCMotifTempsPartiel(String value) {
		takeStoredValueForKey(value, "cMotifTempsPartiel");
	}

	public String cMotifTempsPartielOnp() {
		return (String)storedValueForKey("cMotifTempsPartielOnp");
	}

	public void cMotifTempsPartielOnp(String value) {
		takeStoredValueForKey(value, "cMotifTempsPartielOnp");
	}
	
	// méthodes ajoutées
	public boolean doitControlerDuree() {
		return temCtrlDureeTps() != null && temCtrlDureeTps().equals(CocktailConstantes.VRAI);
	}
	public boolean estPourEleverEnfant() {
		return cMotifTempsPartiel().equals(MOTIF_ELEVER_ENFANT);
	}
	public boolean estPourEleverEnfantAdopte() {
		return cMotifTempsPartiel().equals(MOTIF_ELEVER_ENFANT_ADOPTE);
	}
	public boolean estPourHandicape() {
		return cMotifTempsPartiel().equals(MOTIF_HANDICAPE);
	}
	public boolean aQuotiteLimitee() {
		if (cMotifTempsPartiel().equals(SUR_AUTORISATION) || estPourHandicape()) {
			return false;
		} else {
			return true;
		}
	}
}
