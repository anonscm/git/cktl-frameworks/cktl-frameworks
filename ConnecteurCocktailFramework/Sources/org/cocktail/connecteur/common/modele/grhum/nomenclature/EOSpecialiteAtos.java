package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteAtos;

import com.webobjects.foundation.NSDictionary;

public class EOSpecialiteAtos extends _EOSpecialiteAtos implements ISpecialiteAtos {
	private static Logger log = Logger.getLogger(EOSpecialiteAtos.class);

	public String clePrimaire() {
		return cSpecialiteAtos();
	}
}
