// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCivilite.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCivilite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Civilite";

	// Attributes
	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String C_CIVILITE_ONP_KEY = "cCiviliteOnp";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String L_CIVILITE_KEY = "lCivilite";
	public static final String SEXE_KEY = "sexe";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOCivilite.class);

  public EOCivilite localInstanceIn(EOEditingContext editingContext) {
    EOCivilite localInstance = (EOCivilite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCivilite() {
    return (String) storedValueForKey("cCivilite");
  }

  public void setCCivilite(String value) {
    if (_EOCivilite.LOG.isDebugEnabled()) {
    	_EOCivilite.LOG.debug( "updating cCivilite from " + cCivilite() + " to " + value);
    }
    takeStoredValueForKey(value, "cCivilite");
  }

  public String cCiviliteOnp() {
    return (String) storedValueForKey("cCiviliteOnp");
  }

  public void setCCiviliteOnp(String value) {
    if (_EOCivilite.LOG.isDebugEnabled()) {
    	_EOCivilite.LOG.debug( "updating cCiviliteOnp from " + cCiviliteOnp() + " to " + value);
    }
    takeStoredValueForKey(value, "cCiviliteOnp");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCivilite.LOG.isDebugEnabled()) {
    	_EOCivilite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCivilite.LOG.isDebugEnabled()) {
    	_EOCivilite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lCivilite() {
    return (String) storedValueForKey("lCivilite");
  }

  public void setLCivilite(String value) {
    if (_EOCivilite.LOG.isDebugEnabled()) {
    	_EOCivilite.LOG.debug( "updating lCivilite from " + lCivilite() + " to " + value);
    }
    takeStoredValueForKey(value, "lCivilite");
  }

  public String sexe() {
    return (String) storedValueForKey("sexe");
  }

  public void setSexe(String value) {
    if (_EOCivilite.LOG.isDebugEnabled()) {
    	_EOCivilite.LOG.debug( "updating sexe from " + sexe() + " to " + value);
    }
    takeStoredValueForKey(value, "sexe");
  }


  public static EOCivilite createCivilite(EOEditingContext editingContext, String cCivilite
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOCivilite eo = (EOCivilite) EOUtilities.createAndInsertInstance(editingContext, _EOCivilite.ENTITY_NAME);    
		eo.setCCivilite(cCivilite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOCivilite> fetchAllCivilites(EOEditingContext editingContext) {
    return _EOCivilite.fetchAllCivilites(editingContext, null);
  }

  public static NSArray<EOCivilite> fetchAllCivilites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCivilite.fetchCivilites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCivilite> fetchCivilites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCivilite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCivilite> eoObjects = (NSArray<EOCivilite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCivilite fetchCivilite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCivilite.fetchCivilite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCivilite fetchCivilite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCivilite> eoObjects = _EOCivilite.fetchCivilites(editingContext, qualifier, null);
    EOCivilite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCivilite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Civilite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCivilite fetchRequiredCivilite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCivilite.fetchRequiredCivilite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCivilite fetchRequiredCivilite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCivilite eoObject = _EOCivilite.fetchCivilite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Civilite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCivilite localInstanceIn(EOEditingContext editingContext, EOCivilite eo) {
    EOCivilite localInstance = (eo == null) ? null : (EOCivilite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
