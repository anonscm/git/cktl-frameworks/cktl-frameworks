package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOFonctionStructurelle extends _EOFonctionStructurelle {
	public static EOFonctionStructurelle getFromCode(EOEditingContext editingContext, Integer codeFonction) {
		return fetchFonctionStructurelle(editingContext, C_FONCTION_KEY, codeFonction);
	}
}
