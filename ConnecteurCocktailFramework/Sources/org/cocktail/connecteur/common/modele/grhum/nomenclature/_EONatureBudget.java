// $LastChangedRevision$ DO NOT EDIT.  Make changes to EONatureBudget.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EONatureBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "NatureBudget";

	// Attributes
	public static final String C_BUDGET_KEY = "cBudget";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_BUDGET_KEY = "lcBudget";
	public static final String LL_BUDGET_KEY = "llBudget";
	public static final String TEM_BUDGET_ETAT_KEY = "temBudgetEtat";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EONatureBudget.class);

  public EONatureBudget localInstanceIn(EOEditingContext editingContext) {
    EONatureBudget localInstance = (EONatureBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBudget() {
    return (String) storedValueForKey("cBudget");
  }

  public void setCBudget(String value) {
    if (_EONatureBudget.LOG.isDebugEnabled()) {
    	_EONatureBudget.LOG.debug( "updating cBudget from " + cBudget() + " to " + value);
    }
    takeStoredValueForKey(value, "cBudget");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EONatureBudget.LOG.isDebugEnabled()) {
    	_EONatureBudget.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EONatureBudget.LOG.isDebugEnabled()) {
    	_EONatureBudget.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcBudget() {
    return (String) storedValueForKey("lcBudget");
  }

  public void setLcBudget(String value) {
    if (_EONatureBudget.LOG.isDebugEnabled()) {
    	_EONatureBudget.LOG.debug( "updating lcBudget from " + lcBudget() + " to " + value);
    }
    takeStoredValueForKey(value, "lcBudget");
  }

  public String llBudget() {
    return (String) storedValueForKey("llBudget");
  }

  public void setLlBudget(String value) {
    if (_EONatureBudget.LOG.isDebugEnabled()) {
    	_EONatureBudget.LOG.debug( "updating llBudget from " + llBudget() + " to " + value);
    }
    takeStoredValueForKey(value, "llBudget");
  }

  public String temBudgetEtat() {
    return (String) storedValueForKey("temBudgetEtat");
  }

  public void setTemBudgetEtat(String value) {
    if (_EONatureBudget.LOG.isDebugEnabled()) {
    	_EONatureBudget.LOG.debug( "updating temBudgetEtat from " + temBudgetEtat() + " to " + value);
    }
    takeStoredValueForKey(value, "temBudgetEtat");
  }


  public static EONatureBudget createNatureBudget(EOEditingContext editingContext, String cBudget
, NSTimestamp dCreation
, NSTimestamp dModification
, String temBudgetEtat
) {
    EONatureBudget eo = (EONatureBudget) EOUtilities.createAndInsertInstance(editingContext, _EONatureBudget.ENTITY_NAME);    
		eo.setCBudget(cBudget);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemBudgetEtat(temBudgetEtat);
    return eo;
  }

  public static NSArray<EONatureBudget> fetchAllNatureBudgets(EOEditingContext editingContext) {
    return _EONatureBudget.fetchAllNatureBudgets(editingContext, null);
  }

  public static NSArray<EONatureBudget> fetchAllNatureBudgets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONatureBudget.fetchNatureBudgets(editingContext, null, sortOrderings);
  }

  public static NSArray<EONatureBudget> fetchNatureBudgets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EONatureBudget.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONatureBudget> eoObjects = (NSArray<EONatureBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EONatureBudget fetchNatureBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBudget.fetchNatureBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBudget fetchNatureBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONatureBudget> eoObjects = _EONatureBudget.fetchNatureBudgets(editingContext, qualifier, null);
    EONatureBudget eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EONatureBudget)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NatureBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBudget fetchRequiredNatureBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBudget.fetchRequiredNatureBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBudget fetchRequiredNatureBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    EONatureBudget eoObject = _EONatureBudget.fetchNatureBudget(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NatureBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBudget localInstanceIn(EOEditingContext editingContext, EONatureBudget eo) {
    EONatureBudget localInstance = (eo == null) ? null : (EONatureBudget)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
