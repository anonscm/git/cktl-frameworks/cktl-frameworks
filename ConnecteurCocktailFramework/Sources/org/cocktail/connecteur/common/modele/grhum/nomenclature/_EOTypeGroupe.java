// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeGroupe.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeGroupe extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeGroupe";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TGRP_CODE_KEY = "tgrpCode";
	public static final String TGRP_LIBELLE_KEY = "tgrpLibelle";
	public static final String TGRP_TEM_UTIL_KEY = "tgrpTemUtil";
	public static final String TGRP_TEM_WEB_KEY = "tgrpTemWeb";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeGroupe.class);

  public EOTypeGroupe localInstanceIn(EOEditingContext editingContext) {
    EOTypeGroupe localInstance = (EOTypeGroupe)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeGroupe.LOG.isDebugEnabled()) {
    	_EOTypeGroupe.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeGroupe.LOG.isDebugEnabled()) {
    	_EOTypeGroupe.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String tgrpCode() {
    return (String) storedValueForKey("tgrpCode");
  }

  public void setTgrpCode(String value) {
    if (_EOTypeGroupe.LOG.isDebugEnabled()) {
    	_EOTypeGroupe.LOG.debug( "updating tgrpCode from " + tgrpCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tgrpCode");
  }

  public String tgrpLibelle() {
    return (String) storedValueForKey("tgrpLibelle");
  }

  public void setTgrpLibelle(String value) {
    if (_EOTypeGroupe.LOG.isDebugEnabled()) {
    	_EOTypeGroupe.LOG.debug( "updating tgrpLibelle from " + tgrpLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tgrpLibelle");
  }

  public String tgrpTemUtil() {
    return (String) storedValueForKey("tgrpTemUtil");
  }

  public void setTgrpTemUtil(String value) {
    if (_EOTypeGroupe.LOG.isDebugEnabled()) {
    	_EOTypeGroupe.LOG.debug( "updating tgrpTemUtil from " + tgrpTemUtil() + " to " + value);
    }
    takeStoredValueForKey(value, "tgrpTemUtil");
  }

  public String tgrpTemWeb() {
    return (String) storedValueForKey("tgrpTemWeb");
  }

  public void setTgrpTemWeb(String value) {
    if (_EOTypeGroupe.LOG.isDebugEnabled()) {
    	_EOTypeGroupe.LOG.debug( "updating tgrpTemWeb from " + tgrpTemWeb() + " to " + value);
    }
    takeStoredValueForKey(value, "tgrpTemWeb");
  }


  public static EOTypeGroupe createTypeGroupe(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String tgrpCode
) {
    EOTypeGroupe eo = (EOTypeGroupe) EOUtilities.createAndInsertInstance(editingContext, _EOTypeGroupe.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTgrpCode(tgrpCode);
    return eo;
  }

  public static NSArray<EOTypeGroupe> fetchAllTypeGroupes(EOEditingContext editingContext) {
    return _EOTypeGroupe.fetchAllTypeGroupes(editingContext, null);
  }

  public static NSArray<EOTypeGroupe> fetchAllTypeGroupes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeGroupe.fetchTypeGroupes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeGroupe> fetchTypeGroupes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeGroupe.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeGroupe> eoObjects = (NSArray<EOTypeGroupe>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeGroupe fetchTypeGroupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeGroupe.fetchTypeGroupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeGroupe fetchTypeGroupe(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeGroupe> eoObjects = _EOTypeGroupe.fetchTypeGroupes(editingContext, qualifier, null);
    EOTypeGroupe eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeGroupe)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeGroupe fetchRequiredTypeGroupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeGroupe.fetchRequiredTypeGroupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeGroupe fetchRequiredTypeGroupe(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeGroupe eoObject = _EOTypeGroupe.fetchTypeGroupe(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeGroupe localInstanceIn(EOEditingContext editingContext, EOTypeGroupe eo) {
    EOTypeGroupe localInstance = (eo == null) ? null : (EOTypeGroupe)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
