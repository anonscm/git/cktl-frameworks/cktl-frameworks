package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOFonctionExpertConseil extends _EOFonctionExpertConseil {
	public static EOFonctionExpertConseil getFromCode(EOEditingContext editingContext, Integer codeFonction) {
		return fetchFonctionExpertConseil(editingContext, C_FONCTION_KEY, codeFonction);
	}
}
