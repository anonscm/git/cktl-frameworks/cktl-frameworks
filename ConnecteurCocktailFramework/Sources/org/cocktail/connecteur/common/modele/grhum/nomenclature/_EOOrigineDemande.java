// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOOrigineDemande.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOOrigineDemande extends  EOGenericRecord {
	public static final String ENTITY_NAME = "OrigineDemande";

	// Attributes
	public static final String C_ORIGINE_DEMANDE_KEY = "cOrigineDemande";
	public static final String LC_ORIGINE_DEMANDE_KEY = "lcOrigineDemande";
	public static final String LL_ORIGINE_DEMANDE_KEY = "llOrigineDemande";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOOrigineDemande.class);

  public EOOrigineDemande localInstanceIn(EOEditingContext editingContext) {
    EOOrigineDemande localInstance = (EOOrigineDemande)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cOrigineDemande() {
    return (String) storedValueForKey("cOrigineDemande");
  }

  public void setCOrigineDemande(String value) {
    if (_EOOrigineDemande.LOG.isDebugEnabled()) {
    	_EOOrigineDemande.LOG.debug( "updating cOrigineDemande from " + cOrigineDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "cOrigineDemande");
  }

  public String lcOrigineDemande() {
    return (String) storedValueForKey("lcOrigineDemande");
  }

  public void setLcOrigineDemande(String value) {
    if (_EOOrigineDemande.LOG.isDebugEnabled()) {
    	_EOOrigineDemande.LOG.debug( "updating lcOrigineDemande from " + lcOrigineDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "lcOrigineDemande");
  }

  public String llOrigineDemande() {
    return (String) storedValueForKey("llOrigineDemande");
  }

  public void setLlOrigineDemande(String value) {
    if (_EOOrigineDemande.LOG.isDebugEnabled()) {
    	_EOOrigineDemande.LOG.debug( "updating llOrigineDemande from " + llOrigineDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "llOrigineDemande");
  }


  public static EOOrigineDemande createOrigineDemande(EOEditingContext editingContext, String cOrigineDemande
, String lcOrigineDemande
, String llOrigineDemande
) {
    EOOrigineDemande eo = (EOOrigineDemande) EOUtilities.createAndInsertInstance(editingContext, _EOOrigineDemande.ENTITY_NAME);    
		eo.setCOrigineDemande(cOrigineDemande);
		eo.setLcOrigineDemande(lcOrigineDemande);
		eo.setLlOrigineDemande(llOrigineDemande);
    return eo;
  }

  public static NSArray<EOOrigineDemande> fetchAllOrigineDemandes(EOEditingContext editingContext) {
    return _EOOrigineDemande.fetchAllOrigineDemandes(editingContext, null);
  }

  public static NSArray<EOOrigineDemande> fetchAllOrigineDemandes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOOrigineDemande.fetchOrigineDemandes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOOrigineDemande> fetchOrigineDemandes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOOrigineDemande.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOOrigineDemande> eoObjects = (NSArray<EOOrigineDemande>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOOrigineDemande fetchOrigineDemande(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrigineDemande.fetchOrigineDemande(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrigineDemande fetchOrigineDemande(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOOrigineDemande> eoObjects = _EOOrigineDemande.fetchOrigineDemandes(editingContext, qualifier, null);
    EOOrigineDemande eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOOrigineDemande)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OrigineDemande that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrigineDemande fetchRequiredOrigineDemande(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrigineDemande.fetchRequiredOrigineDemande(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrigineDemande fetchRequiredOrigineDemande(EOEditingContext editingContext, EOQualifier qualifier) {
    EOOrigineDemande eoObject = _EOOrigineDemande.fetchOrigineDemande(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OrigineDemande that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrigineDemande localInstanceIn(EOEditingContext editingContext, EOOrigineDemande eo) {
    EOOrigineDemande localInstance = (eo == null) ? null : (EOOrigineDemande)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
