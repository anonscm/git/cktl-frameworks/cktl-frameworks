// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODistinctionNiveau.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODistinctionNiveau extends  EOGenericRecord {
	public static final String ENTITY_NAME = "DistinctionNiveau";

	// Attributes
	public static final String C_DIST_N_KEY = "cDistN";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LL_DIST_N_KEY = "llDistN";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EODistinctionNiveau.class);

  public EODistinctionNiveau localInstanceIn(EOEditingContext editingContext) {
    EODistinctionNiveau localInstance = (EODistinctionNiveau)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cDistN() {
    return (Integer) storedValueForKey("cDistN");
  }

  public void setCDistN(Integer value) {
    if (_EODistinctionNiveau.LOG.isDebugEnabled()) {
    	_EODistinctionNiveau.LOG.debug( "updating cDistN from " + cDistN() + " to " + value);
    }
    takeStoredValueForKey(value, "cDistN");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODistinctionNiveau.LOG.isDebugEnabled()) {
    	_EODistinctionNiveau.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODistinctionNiveau.LOG.isDebugEnabled()) {
    	_EODistinctionNiveau.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String llDistN() {
    return (String) storedValueForKey("llDistN");
  }

  public void setLlDistN(String value) {
    if (_EODistinctionNiveau.LOG.isDebugEnabled()) {
    	_EODistinctionNiveau.LOG.debug( "updating llDistN from " + llDistN() + " to " + value);
    }
    takeStoredValueForKey(value, "llDistN");
  }


  public static EODistinctionNiveau createDistinctionNiveau(EOEditingContext editingContext, Integer cDistN
, NSTimestamp dCreation
, NSTimestamp dModification
, String llDistN
) {
    EODistinctionNiveau eo = (EODistinctionNiveau) EOUtilities.createAndInsertInstance(editingContext, _EODistinctionNiveau.ENTITY_NAME);    
		eo.setCDistN(cDistN);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLlDistN(llDistN);
    return eo;
  }

  public static NSArray<EODistinctionNiveau> fetchAllDistinctionNiveaus(EOEditingContext editingContext) {
    return _EODistinctionNiveau.fetchAllDistinctionNiveaus(editingContext, null);
  }

  public static NSArray<EODistinctionNiveau> fetchAllDistinctionNiveaus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODistinctionNiveau.fetchDistinctionNiveaus(editingContext, null, sortOrderings);
  }

  public static NSArray<EODistinctionNiveau> fetchDistinctionNiveaus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODistinctionNiveau.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODistinctionNiveau> eoObjects = (NSArray<EODistinctionNiveau>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODistinctionNiveau fetchDistinctionNiveau(EOEditingContext editingContext, String keyName, Object value) {
    return _EODistinctionNiveau.fetchDistinctionNiveau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODistinctionNiveau fetchDistinctionNiveau(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODistinctionNiveau> eoObjects = _EODistinctionNiveau.fetchDistinctionNiveaus(editingContext, qualifier, null);
    EODistinctionNiveau eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODistinctionNiveau)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one DistinctionNiveau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODistinctionNiveau fetchRequiredDistinctionNiveau(EOEditingContext editingContext, String keyName, Object value) {
    return _EODistinctionNiveau.fetchRequiredDistinctionNiveau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODistinctionNiveau fetchRequiredDistinctionNiveau(EOEditingContext editingContext, EOQualifier qualifier) {
    EODistinctionNiveau eoObject = _EODistinctionNiveau.fetchDistinctionNiveau(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no DistinctionNiveau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODistinctionNiveau localInstanceIn(EOEditingContext editingContext, EODistinctionNiveau eo) {
    EODistinctionNiveau localInstance = (eo == null) ? null : (EODistinctionNiveau)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
