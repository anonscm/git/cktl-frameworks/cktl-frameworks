// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeCgMatern.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeCgMatern extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeCgMatern";

	// Attributes
	public static final String C_TYPE_CG_MATERN_KEY = "cTypeCgMatern";
	public static final String DUREE_CG_MATERN_KEY = "dureeCgMatern";
	public static final String LL_TYPE_CG_MATERN_KEY = "llTypeCgMatern";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeCgMatern.class);

  public EOTypeCgMatern localInstanceIn(EOEditingContext editingContext) {
    EOTypeCgMatern localInstance = (EOTypeCgMatern)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeCgMatern() {
    return (String) storedValueForKey("cTypeCgMatern");
  }

  public void setCTypeCgMatern(String value) {
    if (_EOTypeCgMatern.LOG.isDebugEnabled()) {
    	_EOTypeCgMatern.LOG.debug( "updating cTypeCgMatern from " + cTypeCgMatern() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeCgMatern");
  }

  public Long dureeCgMatern() {
    return (Long) storedValueForKey("dureeCgMatern");
  }

  public void setDureeCgMatern(Long value) {
    if (_EOTypeCgMatern.LOG.isDebugEnabled()) {
    	_EOTypeCgMatern.LOG.debug( "updating dureeCgMatern from " + dureeCgMatern() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeCgMatern");
  }

  public String llTypeCgMatern() {
    return (String) storedValueForKey("llTypeCgMatern");
  }

  public void setLlTypeCgMatern(String value) {
    if (_EOTypeCgMatern.LOG.isDebugEnabled()) {
    	_EOTypeCgMatern.LOG.debug( "updating llTypeCgMatern from " + llTypeCgMatern() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeCgMatern");
  }


  public static EOTypeCgMatern createTypeCgMatern(EOEditingContext editingContext, String cTypeCgMatern
) {
    EOTypeCgMatern eo = (EOTypeCgMatern) EOUtilities.createAndInsertInstance(editingContext, _EOTypeCgMatern.ENTITY_NAME);    
		eo.setCTypeCgMatern(cTypeCgMatern);
    return eo;
  }

  public static NSArray<EOTypeCgMatern> fetchAllTypeCgMaterns(EOEditingContext editingContext) {
    return _EOTypeCgMatern.fetchAllTypeCgMaterns(editingContext, null);
  }

  public static NSArray<EOTypeCgMatern> fetchAllTypeCgMaterns(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeCgMatern.fetchTypeCgMaterns(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeCgMatern> fetchTypeCgMaterns(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeCgMatern.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeCgMatern> eoObjects = (NSArray<EOTypeCgMatern>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeCgMatern fetchTypeCgMatern(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCgMatern.fetchTypeCgMatern(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCgMatern fetchTypeCgMatern(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeCgMatern> eoObjects = _EOTypeCgMatern.fetchTypeCgMaterns(editingContext, qualifier, null);
    EOTypeCgMatern eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeCgMatern)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeCgMatern that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCgMatern fetchRequiredTypeCgMatern(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCgMatern.fetchRequiredTypeCgMatern(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCgMatern fetchRequiredTypeCgMatern(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeCgMatern eoObject = _EOTypeCgMatern.fetchTypeCgMatern(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeCgMatern that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCgMatern localInstanceIn(EOEditingContext editingContext, EOTypeCgMatern eo) {
    EOTypeCgMatern localInstance = (eo == null) ? null : (EOTypeCgMatern)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
