// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODiplomes.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODiplomes extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Diplomes";

	// Attributes
	public static final String C_DIPLOME_KEY = "cDiplome";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EODiplomes.class);

  public EODiplomes localInstanceIn(EOEditingContext editingContext) {
    EODiplomes localInstance = (EODiplomes)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDiplome() {
    return (String) storedValueForKey("cDiplome");
  }

  public void setCDiplome(String value) {
    if (_EODiplomes.LOG.isDebugEnabled()) {
    	_EODiplomes.LOG.debug( "updating cDiplome from " + cDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiplome");
  }


  public static EODiplomes createDiplomes(EOEditingContext editingContext, String cDiplome
) {
    EODiplomes eo = (EODiplomes) EOUtilities.createAndInsertInstance(editingContext, _EODiplomes.ENTITY_NAME);    
		eo.setCDiplome(cDiplome);
    return eo;
  }

  public static NSArray<EODiplomes> fetchAllDiplomeses(EOEditingContext editingContext) {
    return _EODiplomes.fetchAllDiplomeses(editingContext, null);
  }

  public static NSArray<EODiplomes> fetchAllDiplomeses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODiplomes.fetchDiplomeses(editingContext, null, sortOrderings);
  }

  public static NSArray<EODiplomes> fetchDiplomeses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODiplomes.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODiplomes> eoObjects = (NSArray<EODiplomes>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODiplomes fetchDiplomes(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiplomes.fetchDiplomes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiplomes fetchDiplomes(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODiplomes> eoObjects = _EODiplomes.fetchDiplomeses(editingContext, qualifier, null);
    EODiplomes eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODiplomes)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Diplomes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiplomes fetchRequiredDiplomes(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiplomes.fetchRequiredDiplomes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiplomes fetchRequiredDiplomes(EOEditingContext editingContext, EOQualifier qualifier) {
    EODiplomes eoObject = _EODiplomes.fetchDiplomes(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Diplomes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiplomes localInstanceIn(EOEditingContext editingContext, EODiplomes eo) {
    EODiplomes localInstance = (eo == null) ? null : (EODiplomes)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
