// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPassageChevron.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPassageChevron extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PassageChevron";

	// Attributes
	public static final String C_CHEVRON_KEY = "cChevron";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_INDICE_BRUT_KEY = "cIndiceBrut";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_CHEVRON_ANNEES_KEY = "dureeChevronAnnees";
	public static final String DUREE_CHEVRON_MOIS_KEY = "dureeChevronMois";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOPassageChevron.class);

  public EOPassageChevron localInstanceIn(EOEditingContext editingContext) {
    EOPassageChevron localInstance = (EOPassageChevron)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cChevron() {
    return (String) storedValueForKey("cChevron");
  }

  public void setCChevron(String value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating cChevron from " + cChevron() + " to " + value);
    }
    takeStoredValueForKey(value, "cChevron");
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String cIndiceBrut() {
    return (String) storedValueForKey("cIndiceBrut");
  }

  public void setCIndiceBrut(String value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating cIndiceBrut from " + cIndiceBrut() + " to " + value);
    }
    takeStoredValueForKey(value, "cIndiceBrut");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dureeChevronAnnees() {
    return (Integer) storedValueForKey("dureeChevronAnnees");
  }

  public void setDureeChevronAnnees(Integer value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating dureeChevronAnnees from " + dureeChevronAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeChevronAnnees");
  }

  public Integer dureeChevronMois() {
    return (Integer) storedValueForKey("dureeChevronMois");
  }

  public void setDureeChevronMois(Integer value) {
    if (_EOPassageChevron.LOG.isDebugEnabled()) {
    	_EOPassageChevron.LOG.debug( "updating dureeChevronMois from " + dureeChevronMois() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeChevronMois");
  }


  public static EOPassageChevron createPassageChevron(EOEditingContext editingContext, String cChevron
, String cEchelon
, String cGrade
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOPassageChevron eo = (EOPassageChevron) EOUtilities.createAndInsertInstance(editingContext, _EOPassageChevron.ENTITY_NAME);    
		eo.setCChevron(cChevron);
		eo.setCEchelon(cEchelon);
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOPassageChevron> fetchAllPassageChevrons(EOEditingContext editingContext) {
    return _EOPassageChevron.fetchAllPassageChevrons(editingContext, null);
  }

  public static NSArray<EOPassageChevron> fetchAllPassageChevrons(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPassageChevron.fetchPassageChevrons(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPassageChevron> fetchPassageChevrons(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPassageChevron.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPassageChevron> eoObjects = (NSArray<EOPassageChevron>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPassageChevron fetchPassageChevron(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPassageChevron.fetchPassageChevron(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPassageChevron fetchPassageChevron(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPassageChevron> eoObjects = _EOPassageChevron.fetchPassageChevrons(editingContext, qualifier, null);
    EOPassageChevron eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPassageChevron)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PassageChevron that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPassageChevron fetchRequiredPassageChevron(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPassageChevron.fetchRequiredPassageChevron(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPassageChevron fetchRequiredPassageChevron(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPassageChevron eoObject = _EOPassageChevron.fetchPassageChevron(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PassageChevron that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPassageChevron localInstanceIn(EOEditingContext editingContext, EOPassageChevron eo) {
    EOPassageChevron localInstance = (eo == null) ? null : (EOPassageChevron)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
