// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMotifDepart.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMotifDepart extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MotifDepart";

	// Attributes
	public static final String C_MOTIF_DEPART_KEY = "cMotifDepart";
	public static final String C_MOTIF_DEPART_ONP_KEY = "cMotifDepartOnp";
	public static final String TEM_ENS_KEY = "temEns";
	public static final String TEM_FIN_CARRIERE_KEY = "temFinCarriere";
	public static final String TEM_RNE_LIEU_KEY = "temRneLieu";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMotifDepart.class);

  public EOMotifDepart localInstanceIn(EOEditingContext editingContext) {
    EOMotifDepart localInstance = (EOMotifDepart)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifDepart() {
    return (String) storedValueForKey("cMotifDepart");
  }

  public void setCMotifDepart(String value) {
    if (_EOMotifDepart.LOG.isDebugEnabled()) {
    	_EOMotifDepart.LOG.debug( "updating cMotifDepart from " + cMotifDepart() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifDepart");
  }

  public String cMotifDepartOnp() {
    return (String) storedValueForKey("cMotifDepartOnp");
  }

  public void setCMotifDepartOnp(String value) {
    if (_EOMotifDepart.LOG.isDebugEnabled()) {
    	_EOMotifDepart.LOG.debug( "updating cMotifDepartOnp from " + cMotifDepartOnp() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifDepartOnp");
  }

  public String temEns() {
    return (String) storedValueForKey("temEns");
  }

  public void setTemEns(String value) {
    if (_EOMotifDepart.LOG.isDebugEnabled()) {
    	_EOMotifDepart.LOG.debug( "updating temEns from " + temEns() + " to " + value);
    }
    takeStoredValueForKey(value, "temEns");
  }

  public String temFinCarriere() {
    return (String) storedValueForKey("temFinCarriere");
  }

  public void setTemFinCarriere(String value) {
    if (_EOMotifDepart.LOG.isDebugEnabled()) {
    	_EOMotifDepart.LOG.debug( "updating temFinCarriere from " + temFinCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "temFinCarriere");
  }

  public String temRneLieu() {
    return (String) storedValueForKey("temRneLieu");
  }

  public void setTemRneLieu(String value) {
    if (_EOMotifDepart.LOG.isDebugEnabled()) {
    	_EOMotifDepart.LOG.debug( "updating temRneLieu from " + temRneLieu() + " to " + value);
    }
    takeStoredValueForKey(value, "temRneLieu");
  }


  public static EOMotifDepart createMotifDepart(EOEditingContext editingContext, String cMotifDepart
, String temEns
) {
    EOMotifDepart eo = (EOMotifDepart) EOUtilities.createAndInsertInstance(editingContext, _EOMotifDepart.ENTITY_NAME);    
		eo.setCMotifDepart(cMotifDepart);
		eo.setTemEns(temEns);
    return eo;
  }

  public static NSArray<EOMotifDepart> fetchAllMotifDeparts(EOEditingContext editingContext) {
    return _EOMotifDepart.fetchAllMotifDeparts(editingContext, null);
  }

  public static NSArray<EOMotifDepart> fetchAllMotifDeparts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMotifDepart.fetchMotifDeparts(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMotifDepart> fetchMotifDeparts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMotifDepart.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMotifDepart> eoObjects = (NSArray<EOMotifDepart>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMotifDepart fetchMotifDepart(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifDepart.fetchMotifDepart(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifDepart fetchMotifDepart(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMotifDepart> eoObjects = _EOMotifDepart.fetchMotifDeparts(editingContext, qualifier, null);
    EOMotifDepart eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMotifDepart)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MotifDepart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifDepart fetchRequiredMotifDepart(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifDepart.fetchRequiredMotifDepart(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifDepart fetchRequiredMotifDepart(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMotifDepart eoObject = _EOMotifDepart.fetchMotifDepart(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MotifDepart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifDepart localInstanceIn(EOEditingContext editingContext, EOMotifDepart eo) {
    EOMotifDepart localInstance = (eo == null) ? null : (EOMotifDepart)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
