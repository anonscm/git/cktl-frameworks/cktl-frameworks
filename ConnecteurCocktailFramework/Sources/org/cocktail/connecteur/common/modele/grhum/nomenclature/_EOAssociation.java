// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOAssociation.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOAssociation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Association";

	// Attributes
	public static final String ASS_ALIAS_KEY = "assAlias";
	public static final String ASS_CODE_KEY = "assCode";
	public static final String ASS_LIBELLE_KEY = "assLibelle";
	public static final String ASS_LOCALE_KEY = "assLocale";
	public static final String ASS_RACINE_KEY = "assRacine";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_OUVERTURE_KEY = "dOuverture";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOAssociation.class);

  public EOAssociation localInstanceIn(EOEditingContext editingContext) {
    EOAssociation localInstance = (EOAssociation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String assAlias() {
    return (String) storedValueForKey("assAlias");
  }

  public void setAssAlias(String value) {
    if (_EOAssociation.LOG.isDebugEnabled()) {
    	_EOAssociation.LOG.debug( "updating assAlias from " + assAlias() + " to " + value);
    }
    takeStoredValueForKey(value, "assAlias");
  }

  public String assCode() {
    return (String) storedValueForKey("assCode");
  }

  public void setAssCode(String value) {
    if (_EOAssociation.LOG.isDebugEnabled()) {
    	_EOAssociation.LOG.debug( "updating assCode from " + assCode() + " to " + value);
    }
    takeStoredValueForKey(value, "assCode");
  }

  public String assLibelle() {
    return (String) storedValueForKey("assLibelle");
  }

  public void setAssLibelle(String value) {
    if (_EOAssociation.LOG.isDebugEnabled()) {
    	_EOAssociation.LOG.debug( "updating assLibelle from " + assLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "assLibelle");
  }

  public String assLocale() {
    return (String) storedValueForKey("assLocale");
  }

  public void setAssLocale(String value) {
    if (_EOAssociation.LOG.isDebugEnabled()) {
    	_EOAssociation.LOG.debug( "updating assLocale from " + assLocale() + " to " + value);
    }
    takeStoredValueForKey(value, "assLocale");
  }

  public String assRacine() {
    return (String) storedValueForKey("assRacine");
  }

  public void setAssRacine(String value) {
    if (_EOAssociation.LOG.isDebugEnabled()) {
    	_EOAssociation.LOG.debug( "updating assRacine from " + assRacine() + " to " + value);
    }
    takeStoredValueForKey(value, "assRacine");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EOAssociation.LOG.isDebugEnabled()) {
    	_EOAssociation.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey("dOuverture");
  }

  public void setDOuverture(NSTimestamp value) {
    if (_EOAssociation.LOG.isDebugEnabled()) {
    	_EOAssociation.LOG.debug( "updating dOuverture from " + dOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dOuverture");
  }


  public static EOAssociation createAssociation(EOEditingContext editingContext, String assCode
, String assLibelle
, String assLocale
) {
    EOAssociation eo = (EOAssociation) EOUtilities.createAndInsertInstance(editingContext, _EOAssociation.ENTITY_NAME);    
		eo.setAssCode(assCode);
		eo.setAssLibelle(assLibelle);
		eo.setAssLocale(assLocale);
    return eo;
  }

  public static NSArray<EOAssociation> fetchAllAssociations(EOEditingContext editingContext) {
    return _EOAssociation.fetchAllAssociations(editingContext, null);
  }

  public static NSArray<EOAssociation> fetchAllAssociations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAssociation.fetchAssociations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAssociation> fetchAssociations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAssociation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAssociation> eoObjects = (NSArray<EOAssociation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAssociation fetchAssociation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAssociation.fetchAssociation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAssociation fetchAssociation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAssociation> eoObjects = _EOAssociation.fetchAssociations(editingContext, qualifier, null);
    EOAssociation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAssociation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Association that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAssociation fetchRequiredAssociation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAssociation.fetchRequiredAssociation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAssociation fetchRequiredAssociation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAssociation eoObject = _EOAssociation.fetchAssociation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Association that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAssociation localInstanceIn(EOEditingContext editingContext, EOAssociation eo) {
    EOAssociation localInstance = (eo == null) ? null : (EOAssociation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
