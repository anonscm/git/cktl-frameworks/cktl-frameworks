// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeTel.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeTel extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeTel";

	// Attributes
	public static final String C_TYPE_TEL_KEY = "cTypeTel";
	public static final String L_TYPE_TEL_KEY = "lTypeTel";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeTel.class);

  public EOTypeTel localInstanceIn(EOEditingContext editingContext) {
    EOTypeTel localInstance = (EOTypeTel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeTel() {
    return (String) storedValueForKey("cTypeTel");
  }

  public void setCTypeTel(String value) {
    if (_EOTypeTel.LOG.isDebugEnabled()) {
    	_EOTypeTel.LOG.debug( "updating cTypeTel from " + cTypeTel() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeTel");
  }

  public String lTypeTel() {
    return (String) storedValueForKey("lTypeTel");
  }

  public void setLTypeTel(String value) {
    if (_EOTypeTel.LOG.isDebugEnabled()) {
    	_EOTypeTel.LOG.debug( "updating lTypeTel from " + lTypeTel() + " to " + value);
    }
    takeStoredValueForKey(value, "lTypeTel");
  }


  public static EOTypeTel createTypeTel(EOEditingContext editingContext, String cTypeTel
, String lTypeTel
) {
    EOTypeTel eo = (EOTypeTel) EOUtilities.createAndInsertInstance(editingContext, _EOTypeTel.ENTITY_NAME);    
		eo.setCTypeTel(cTypeTel);
		eo.setLTypeTel(lTypeTel);
    return eo;
  }

  public static NSArray<EOTypeTel> fetchAllTypeTels(EOEditingContext editingContext) {
    return _EOTypeTel.fetchAllTypeTels(editingContext, null);
  }

  public static NSArray<EOTypeTel> fetchAllTypeTels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeTel.fetchTypeTels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeTel> fetchTypeTels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeTel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeTel> eoObjects = (NSArray<EOTypeTel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeTel fetchTypeTel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeTel.fetchTypeTel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeTel fetchTypeTel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeTel> eoObjects = _EOTypeTel.fetchTypeTels(editingContext, qualifier, null);
    EOTypeTel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeTel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeTel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeTel fetchRequiredTypeTel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeTel.fetchRequiredTypeTel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeTel fetchRequiredTypeTel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeTel eoObject = _EOTypeTel.fetchTypeTel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeTel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeTel localInstanceIn(EOEditingContext editingContext, EOTypeTel eo) {
    EOTypeTel localInstance = (eo == null) ? null : (EOTypeTel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
