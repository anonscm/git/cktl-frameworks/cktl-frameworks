// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCommune.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCommune extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Commune";

	// Attributes
	public static final String C_DEP_KEY = "cDep";
	public static final String C_INSEE_KEY = "cInsee";
	public static final String C_POSTAL_KEY = "cPostal";
	public static final String C_POSTAL_PRINCIPAL_KEY = "cPostalPrincipal";
	public static final String LC_COM_KEY = "lcCom";
	public static final String LL_COM_KEY = "llCom";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOCommune.class);

  public EOCommune localInstanceIn(EOEditingContext editingContext) {
    EOCommune localInstance = (EOCommune)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDep() {
    return (String) storedValueForKey("cDep");
  }

  public void setCDep(String value) {
    if (_EOCommune.LOG.isDebugEnabled()) {
    	_EOCommune.LOG.debug( "updating cDep from " + cDep() + " to " + value);
    }
    takeStoredValueForKey(value, "cDep");
  }

  public String cInsee() {
    return (String) storedValueForKey("cInsee");
  }

  public void setCInsee(String value) {
    if (_EOCommune.LOG.isDebugEnabled()) {
    	_EOCommune.LOG.debug( "updating cInsee from " + cInsee() + " to " + value);
    }
    takeStoredValueForKey(value, "cInsee");
  }

  public String cPostal() {
    return (String) storedValueForKey("cPostal");
  }

  public void setCPostal(String value) {
    if (_EOCommune.LOG.isDebugEnabled()) {
    	_EOCommune.LOG.debug( "updating cPostal from " + cPostal() + " to " + value);
    }
    takeStoredValueForKey(value, "cPostal");
  }

  public String cPostalPrincipal() {
    return (String) storedValueForKey("cPostalPrincipal");
  }

  public void setCPostalPrincipal(String value) {
    if (_EOCommune.LOG.isDebugEnabled()) {
    	_EOCommune.LOG.debug( "updating cPostalPrincipal from " + cPostalPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "cPostalPrincipal");
  }

  public String lcCom() {
    return (String) storedValueForKey("lcCom");
  }

  public void setLcCom(String value) {
    if (_EOCommune.LOG.isDebugEnabled()) {
    	_EOCommune.LOG.debug( "updating lcCom from " + lcCom() + " to " + value);
    }
    takeStoredValueForKey(value, "lcCom");
  }

  public String llCom() {
    return (String) storedValueForKey("llCom");
  }

  public void setLlCom(String value) {
    if (_EOCommune.LOG.isDebugEnabled()) {
    	_EOCommune.LOG.debug( "updating llCom from " + llCom() + " to " + value);
    }
    takeStoredValueForKey(value, "llCom");
  }


  public static EOCommune createCommune(EOEditingContext editingContext, String cDep
, String cInsee
, String cPostal
, String llCom
) {
    EOCommune eo = (EOCommune) EOUtilities.createAndInsertInstance(editingContext, _EOCommune.ENTITY_NAME);    
		eo.setCDep(cDep);
		eo.setCInsee(cInsee);
		eo.setCPostal(cPostal);
		eo.setLlCom(llCom);
    return eo;
  }

  public static NSArray<EOCommune> fetchAllCommunes(EOEditingContext editingContext) {
    return _EOCommune.fetchAllCommunes(editingContext, null);
  }

  public static NSArray<EOCommune> fetchAllCommunes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCommune.fetchCommunes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCommune> fetchCommunes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCommune.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCommune> eoObjects = (NSArray<EOCommune>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCommune fetchCommune(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCommune.fetchCommune(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCommune fetchCommune(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCommune> eoObjects = _EOCommune.fetchCommunes(editingContext, qualifier, null);
    EOCommune eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCommune)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Commune that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCommune fetchRequiredCommune(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCommune.fetchRequiredCommune(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCommune fetchRequiredCommune(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCommune eoObject = _EOCommune.fetchCommune(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Commune that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCommune localInstanceIn(EOEditingContext editingContext, EOCommune eo) {
    EOCommune localInstance = (eo == null) ? null : (EOCommune)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
