// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeMotDelegation.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeMotDelegation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeMotDelegation";

	// Attributes
	public static final String C_MOT_DELEGATION_KEY = "cMotDelegation";
	public static final String D_DEB_VALIDITE_KEY = "dDebValidite";
	public static final String D_FIN_VALIDITE_KEY = "dFinValidite";
	public static final String DUREE_MAXI_DELEGATION_KEY = "dureeMaxiDelegation";
	public static final String LL_MOT_DELEGATION_KEY = "llMotDelegation";
	public static final String NBR_RECONDUCTION_DELEGATION_KEY = "nbrReconductionDelegation";
	public static final String REF_MOT_DELEGATION_KEY = "refMotDelegation";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeMotDelegation.class);

  public EOTypeMotDelegation localInstanceIn(EOEditingContext editingContext) {
    EOTypeMotDelegation localInstance = (EOTypeMotDelegation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotDelegation() {
    return (String) storedValueForKey("cMotDelegation");
  }

  public void setCMotDelegation(String value) {
    if (_EOTypeMotDelegation.LOG.isDebugEnabled()) {
    	_EOTypeMotDelegation.LOG.debug( "updating cMotDelegation from " + cMotDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotDelegation");
  }

  public NSTimestamp dDebValidite() {
    return (NSTimestamp) storedValueForKey("dDebValidite");
  }

  public void setDDebValidite(NSTimestamp value) {
    if (_EOTypeMotDelegation.LOG.isDebugEnabled()) {
    	_EOTypeMotDelegation.LOG.debug( "updating dDebValidite from " + dDebValidite() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebValidite");
  }

  public NSTimestamp dFinValidite() {
    return (NSTimestamp) storedValueForKey("dFinValidite");
  }

  public void setDFinValidite(NSTimestamp value) {
    if (_EOTypeMotDelegation.LOG.isDebugEnabled()) {
    	_EOTypeMotDelegation.LOG.debug( "updating dFinValidite from " + dFinValidite() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinValidite");
  }

  public Integer dureeMaxiDelegation() {
    return (Integer) storedValueForKey("dureeMaxiDelegation");
  }

  public void setDureeMaxiDelegation(Integer value) {
    if (_EOTypeMotDelegation.LOG.isDebugEnabled()) {
    	_EOTypeMotDelegation.LOG.debug( "updating dureeMaxiDelegation from " + dureeMaxiDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeMaxiDelegation");
  }

  public String llMotDelegation() {
    return (String) storedValueForKey("llMotDelegation");
  }

  public void setLlMotDelegation(String value) {
    if (_EOTypeMotDelegation.LOG.isDebugEnabled()) {
    	_EOTypeMotDelegation.LOG.debug( "updating llMotDelegation from " + llMotDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "llMotDelegation");
  }

  public Integer nbrReconductionDelegation() {
    return (Integer) storedValueForKey("nbrReconductionDelegation");
  }

  public void setNbrReconductionDelegation(Integer value) {
    if (_EOTypeMotDelegation.LOG.isDebugEnabled()) {
    	_EOTypeMotDelegation.LOG.debug( "updating nbrReconductionDelegation from " + nbrReconductionDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrReconductionDelegation");
  }

  public String refMotDelegation() {
    return (String) storedValueForKey("refMotDelegation");
  }

  public void setRefMotDelegation(String value) {
    if (_EOTypeMotDelegation.LOG.isDebugEnabled()) {
    	_EOTypeMotDelegation.LOG.debug( "updating refMotDelegation from " + refMotDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "refMotDelegation");
  }


  public static EOTypeMotDelegation createTypeMotDelegation(EOEditingContext editingContext, String cMotDelegation
, String llMotDelegation
) {
    EOTypeMotDelegation eo = (EOTypeMotDelegation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeMotDelegation.ENTITY_NAME);    
		eo.setCMotDelegation(cMotDelegation);
		eo.setLlMotDelegation(llMotDelegation);
    return eo;
  }

  public static NSArray<EOTypeMotDelegation> fetchAllTypeMotDelegations(EOEditingContext editingContext) {
    return _EOTypeMotDelegation.fetchAllTypeMotDelegations(editingContext, null);
  }

  public static NSArray<EOTypeMotDelegation> fetchAllTypeMotDelegations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeMotDelegation.fetchTypeMotDelegations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeMotDelegation> fetchTypeMotDelegations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeMotDelegation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeMotDelegation> eoObjects = (NSArray<EOTypeMotDelegation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeMotDelegation fetchTypeMotDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMotDelegation.fetchTypeMotDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMotDelegation fetchTypeMotDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeMotDelegation> eoObjects = _EOTypeMotDelegation.fetchTypeMotDelegations(editingContext, qualifier, null);
    EOTypeMotDelegation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeMotDelegation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeMotDelegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMotDelegation fetchRequiredTypeMotDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMotDelegation.fetchRequiredTypeMotDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMotDelegation fetchRequiredTypeMotDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeMotDelegation eoObject = _EOTypeMotDelegation.fetchTypeMotDelegation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeMotDelegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMotDelegation localInstanceIn(EOEditingContext editingContext, EOTypeMotDelegation eo) {
    EOTypeMotDelegation localInstance = (eo == null) ? null : (EOTypeMotDelegation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
