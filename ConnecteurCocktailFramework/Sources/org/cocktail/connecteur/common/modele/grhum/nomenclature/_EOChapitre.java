// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOChapitre.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOChapitre extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Chapitre";

	// Attributes
	public static final String C_CHAPITRE_KEY = "cChapitre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_CHAPITRE_KEY = "lcChapitre";
	public static final String LL_CHAPITRE_KEY = "llChapitre";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOChapitre.class);

  public EOChapitre localInstanceIn(EOEditingContext editingContext) {
    EOChapitre localInstance = (EOChapitre)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cChapitre() {
    return (Integer) storedValueForKey("cChapitre");
  }

  public void setCChapitre(Integer value) {
    if (_EOChapitre.LOG.isDebugEnabled()) {
    	_EOChapitre.LOG.debug( "updating cChapitre from " + cChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cChapitre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOChapitre.LOG.isDebugEnabled()) {
    	_EOChapitre.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOChapitre.LOG.isDebugEnabled()) {
    	_EOChapitre.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcChapitre() {
    return (String) storedValueForKey("lcChapitre");
  }

  public void setLcChapitre(String value) {
    if (_EOChapitre.LOG.isDebugEnabled()) {
    	_EOChapitre.LOG.debug( "updating lcChapitre from " + lcChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "lcChapitre");
  }

  public String llChapitre() {
    return (String) storedValueForKey("llChapitre");
  }

  public void setLlChapitre(String value) {
    if (_EOChapitre.LOG.isDebugEnabled()) {
    	_EOChapitre.LOG.debug( "updating llChapitre from " + llChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "llChapitre");
  }


  public static EOChapitre createChapitre(EOEditingContext editingContext, Integer cChapitre
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOChapitre eo = (EOChapitre) EOUtilities.createAndInsertInstance(editingContext, _EOChapitre.ENTITY_NAME);    
		eo.setCChapitre(cChapitre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOChapitre> fetchAllChapitres(EOEditingContext editingContext) {
    return _EOChapitre.fetchAllChapitres(editingContext, null);
  }

  public static NSArray<EOChapitre> fetchAllChapitres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChapitre.fetchChapitres(editingContext, null, sortOrderings);
  }

  public static NSArray<EOChapitre> fetchChapitres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOChapitre.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChapitre> eoObjects = (NSArray<EOChapitre>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOChapitre fetchChapitre(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChapitre.fetchChapitre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChapitre fetchChapitre(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChapitre> eoObjects = _EOChapitre.fetchChapitres(editingContext, qualifier, null);
    EOChapitre eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOChapitre)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Chapitre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChapitre fetchRequiredChapitre(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChapitre.fetchRequiredChapitre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChapitre fetchRequiredChapitre(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChapitre eoObject = _EOChapitre.fetchChapitre(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Chapitre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChapitre localInstanceIn(EOEditingContext editingContext, EOChapitre eo) {
    EOChapitre localInstance = (eo == null) ? null : (EOChapitre)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
