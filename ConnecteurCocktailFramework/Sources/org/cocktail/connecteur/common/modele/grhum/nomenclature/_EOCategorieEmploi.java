// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCategorieEmploi.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCategorieEmploi extends  EOGenericRecord {
	public static final String ENTITY_NAME = "CategorieEmploi";

	// Attributes
	public static final String C_CATEGORIE_EMPLOI_KEY = "cCategorieEmploi";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_CATEGORIE_EMPLOI_KEY = "lcCategorieEmploi";
	public static final String LL_CATEGORIE_EMPLOI_KEY = "llCategorieEmploi";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOCategorieEmploi.class);

  public EOCategorieEmploi localInstanceIn(EOEditingContext editingContext) {
    EOCategorieEmploi localInstance = (EOCategorieEmploi)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorieEmploi() {
    return (String) storedValueForKey("cCategorieEmploi");
  }

  public void setCCategorieEmploi(String value) {
    if (_EOCategorieEmploi.LOG.isDebugEnabled()) {
    	_EOCategorieEmploi.LOG.debug( "updating cCategorieEmploi from " + cCategorieEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorieEmploi");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCategorieEmploi.LOG.isDebugEnabled()) {
    	_EOCategorieEmploi.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCategorieEmploi.LOG.isDebugEnabled()) {
    	_EOCategorieEmploi.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcCategorieEmploi() {
    return (String) storedValueForKey("lcCategorieEmploi");
  }

  public void setLcCategorieEmploi(String value) {
    if (_EOCategorieEmploi.LOG.isDebugEnabled()) {
    	_EOCategorieEmploi.LOG.debug( "updating lcCategorieEmploi from " + lcCategorieEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "lcCategorieEmploi");
  }

  public String llCategorieEmploi() {
    return (String) storedValueForKey("llCategorieEmploi");
  }

  public void setLlCategorieEmploi(String value) {
    if (_EOCategorieEmploi.LOG.isDebugEnabled()) {
    	_EOCategorieEmploi.LOG.debug( "updating llCategorieEmploi from " + llCategorieEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "llCategorieEmploi");
  }


  public static EOCategorieEmploi createCategorieEmploi(EOEditingContext editingContext, String cCategorieEmploi
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOCategorieEmploi eo = (EOCategorieEmploi) EOUtilities.createAndInsertInstance(editingContext, _EOCategorieEmploi.ENTITY_NAME);    
		eo.setCCategorieEmploi(cCategorieEmploi);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOCategorieEmploi> fetchAllCategorieEmplois(EOEditingContext editingContext) {
    return _EOCategorieEmploi.fetchAllCategorieEmplois(editingContext, null);
  }

  public static NSArray<EOCategorieEmploi> fetchAllCategorieEmplois(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCategorieEmploi.fetchCategorieEmplois(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCategorieEmploi> fetchCategorieEmplois(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCategorieEmploi.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCategorieEmploi> eoObjects = (NSArray<EOCategorieEmploi>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCategorieEmploi fetchCategorieEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCategorieEmploi.fetchCategorieEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCategorieEmploi fetchCategorieEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCategorieEmploi> eoObjects = _EOCategorieEmploi.fetchCategorieEmplois(editingContext, qualifier, null);
    EOCategorieEmploi eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCategorieEmploi)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CategorieEmploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCategorieEmploi fetchRequiredCategorieEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCategorieEmploi.fetchRequiredCategorieEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCategorieEmploi fetchRequiredCategorieEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCategorieEmploi eoObject = _EOCategorieEmploi.fetchCategorieEmploi(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CategorieEmploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCategorieEmploi localInstanceIn(EOEditingContext editingContext, EOCategorieEmploi eo) {
    EOCategorieEmploi localInstance = (eo == null) ? null : (EOCategorieEmploi)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
