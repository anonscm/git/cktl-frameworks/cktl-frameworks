// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOAcademie.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOAcademie extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Academie";

	// Attributes
	public static final String C_ACADEMIE_KEY = "cAcademie";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String L_ACADEMIE_KEY = "lAcademie";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOAcademie.class);

  public EOAcademie localInstanceIn(EOEditingContext editingContext) {
    EOAcademie localInstance = (EOAcademie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cAcademie() {
    return (String) storedValueForKey("cAcademie");
  }

  public void setCAcademie(String value) {
    if (_EOAcademie.LOG.isDebugEnabled()) {
    	_EOAcademie.LOG.debug( "updating cAcademie from " + cAcademie() + " to " + value);
    }
    takeStoredValueForKey(value, "cAcademie");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOAcademie.LOG.isDebugEnabled()) {
    	_EOAcademie.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOAcademie.LOG.isDebugEnabled()) {
    	_EOAcademie.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public String lAcademie() {
    return (String) storedValueForKey("lAcademie");
  }

  public void setLAcademie(String value) {
    if (_EOAcademie.LOG.isDebugEnabled()) {
    	_EOAcademie.LOG.debug( "updating lAcademie from " + lAcademie() + " to " + value);
    }
    takeStoredValueForKey(value, "lAcademie");
  }


  public static EOAcademie createAcademie(EOEditingContext editingContext, String cAcademie
) {
    EOAcademie eo = (EOAcademie) EOUtilities.createAndInsertInstance(editingContext, _EOAcademie.ENTITY_NAME);    
		eo.setCAcademie(cAcademie);
    return eo;
  }

  public static NSArray<EOAcademie> fetchAllAcademies(EOEditingContext editingContext) {
    return _EOAcademie.fetchAllAcademies(editingContext, null);
  }

  public static NSArray<EOAcademie> fetchAllAcademies(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAcademie.fetchAcademies(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAcademie> fetchAcademies(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAcademie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAcademie> eoObjects = (NSArray<EOAcademie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAcademie fetchAcademie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAcademie.fetchAcademie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAcademie fetchAcademie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAcademie> eoObjects = _EOAcademie.fetchAcademies(editingContext, qualifier, null);
    EOAcademie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAcademie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Academie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAcademie fetchRequiredAcademie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAcademie.fetchRequiredAcademie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAcademie fetchRequiredAcademie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAcademie eoObject = _EOAcademie.fetchAcademie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Academie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAcademie localInstanceIn(EOEditingContext editingContext, EOAcademie eo) {
    EOAcademie localInstance = (eo == null) ? null : (EOAcademie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
