package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOTypeGroupe extends _EOTypeGroupe {
	public static EOTypeGroupe getFromCode(EOEditingContext editingContext, String codeTypeGroupe) {
		return fetchTypeGroupe(editingContext, TGRP_CODE_KEY, codeTypeGroupe);
	}
}
