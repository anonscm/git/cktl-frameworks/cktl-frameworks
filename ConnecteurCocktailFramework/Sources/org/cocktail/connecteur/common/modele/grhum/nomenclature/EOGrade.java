package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOGrade extends _EOGrade {
  
  public static EOGrade getFromCode(EOEditingContext editingContext,String cGrade) {
	  if (cGrade==null)
		  return null;
	  return fetchGrade(editingContext, C_GRADE_KEY, cGrade);
  }
}
