// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVlans.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVlans extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Vlans";

	// Attributes
	public static final String C_VLAN_KEY = "cVlan";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LISTE_IP_KEY = "listeIp";
	public static final String LL_VLAN_KEY = "llVlan";
	public static final String PRIORITE_KEY = "priorite";
	public static final String PRISE_COMPTE_KEY = "priseCompte";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOVlans.class);

  public EOVlans localInstanceIn(EOEditingContext editingContext) {
    EOVlans localInstance = (EOVlans)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cVlan() {
    return (String) storedValueForKey("cVlan");
  }

  public void setCVlan(String value) {
    if (_EOVlans.LOG.isDebugEnabled()) {
    	_EOVlans.LOG.debug( "updating cVlan from " + cVlan() + " to " + value);
    }
    takeStoredValueForKey(value, "cVlan");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOVlans.LOG.isDebugEnabled()) {
    	_EOVlans.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOVlans.LOG.isDebugEnabled()) {
    	_EOVlans.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String listeIp() {
    return (String) storedValueForKey("listeIp");
  }

  public void setListeIp(String value) {
    if (_EOVlans.LOG.isDebugEnabled()) {
    	_EOVlans.LOG.debug( "updating listeIp from " + listeIp() + " to " + value);
    }
    takeStoredValueForKey(value, "listeIp");
  }

  public String llVlan() {
    return (String) storedValueForKey("llVlan");
  }

  public void setLlVlan(String value) {
    if (_EOVlans.LOG.isDebugEnabled()) {
    	_EOVlans.LOG.debug( "updating llVlan from " + llVlan() + " to " + value);
    }
    takeStoredValueForKey(value, "llVlan");
  }

  public Double priorite() {
    return (Double) storedValueForKey("priorite");
  }

  public void setPriorite(Double value) {
    if (_EOVlans.LOG.isDebugEnabled()) {
    	_EOVlans.LOG.debug( "updating priorite from " + priorite() + " to " + value);
    }
    takeStoredValueForKey(value, "priorite");
  }

  public String priseCompte() {
    return (String) storedValueForKey("priseCompte");
  }

  public void setPriseCompte(String value) {
    if (_EOVlans.LOG.isDebugEnabled()) {
    	_EOVlans.LOG.debug( "updating priseCompte from " + priseCompte() + " to " + value);
    }
    takeStoredValueForKey(value, "priseCompte");
  }


  public static EOVlans createVlans(EOEditingContext editingContext, String cVlan
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOVlans eo = (EOVlans) EOUtilities.createAndInsertInstance(editingContext, _EOVlans.ENTITY_NAME);    
		eo.setCVlan(cVlan);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOVlans> fetchAllVlanses(EOEditingContext editingContext) {
    return _EOVlans.fetchAllVlanses(editingContext, null);
  }

  public static NSArray<EOVlans> fetchAllVlanses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVlans.fetchVlanses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVlans> fetchVlanses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVlans.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVlans> eoObjects = (NSArray<EOVlans>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVlans fetchVlans(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVlans.fetchVlans(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVlans fetchVlans(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVlans> eoObjects = _EOVlans.fetchVlanses(editingContext, qualifier, null);
    EOVlans eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVlans)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Vlans that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVlans fetchRequiredVlans(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVlans.fetchRequiredVlans(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVlans fetchRequiredVlans(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVlans eoObject = _EOVlans.fetchVlans(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Vlans that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVlans localInstanceIn(EOEditingContext editingContext, EOVlans eo) {
    EOVlans localInstance = (eo == null) ? null : (EOVlans)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
