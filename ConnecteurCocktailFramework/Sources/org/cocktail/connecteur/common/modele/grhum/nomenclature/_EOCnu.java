// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCnu.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCnu extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Cnu";

	// Attributes
	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String LC_SECTION_CNU_KEY = "lcSectionCnu";
	public static final String LL_SECTION_CNU_KEY = "llSectionCnu";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOCnu.class);

  public EOCnu localInstanceIn(EOEditingContext editingContext) {
    EOCnu localInstance = (EOCnu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cSectionCnu() {
    return (String) storedValueForKey("cSectionCnu");
  }

  public void setCSectionCnu(String value) {
    if (_EOCnu.LOG.isDebugEnabled()) {
    	_EOCnu.LOG.debug( "updating cSectionCnu from " + cSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSectionCnu");
  }

  public String cSousSectionCnu() {
    return (String) storedValueForKey("cSousSectionCnu");
  }

  public void setCSousSectionCnu(String value) {
    if (_EOCnu.LOG.isDebugEnabled()) {
    	_EOCnu.LOG.debug( "updating cSousSectionCnu from " + cSousSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSousSectionCnu");
  }

  public String lcSectionCnu() {
    return (String) storedValueForKey("lcSectionCnu");
  }

  public void setLcSectionCnu(String value) {
    if (_EOCnu.LOG.isDebugEnabled()) {
    	_EOCnu.LOG.debug( "updating lcSectionCnu from " + lcSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "lcSectionCnu");
  }

  public String llSectionCnu() {
    return (String) storedValueForKey("llSectionCnu");
  }

  public void setLlSectionCnu(String value) {
    if (_EOCnu.LOG.isDebugEnabled()) {
    	_EOCnu.LOG.debug( "updating llSectionCnu from " + llSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "llSectionCnu");
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey("noCnu");
  }

  public void setNoCnu(Integer value) {
    if (_EOCnu.LOG.isDebugEnabled()) {
    	_EOCnu.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "noCnu");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCnu.LOG.isDebugEnabled()) {
    	_EOCnu.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOCnu createCnu(EOEditingContext editingContext, String cSectionCnu
, Integer noCnu
, String temValide
) {
    EOCnu eo = (EOCnu) EOUtilities.createAndInsertInstance(editingContext, _EOCnu.ENTITY_NAME);    
		eo.setCSectionCnu(cSectionCnu);
		eo.setNoCnu(noCnu);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCnu> fetchAllCnus(EOEditingContext editingContext) {
    return _EOCnu.fetchAllCnus(editingContext, null);
  }

  public static NSArray<EOCnu> fetchAllCnus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCnu.fetchCnus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCnu> fetchCnus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCnu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCnu> eoObjects = (NSArray<EOCnu>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCnu fetchCnu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCnu.fetchCnu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCnu fetchCnu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCnu> eoObjects = _EOCnu.fetchCnus(editingContext, qualifier, null);
    EOCnu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCnu)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Cnu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCnu fetchRequiredCnu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCnu.fetchRequiredCnu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCnu fetchRequiredCnu(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCnu eoObject = _EOCnu.fetchCnu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Cnu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCnu localInstanceIn(EOEditingContext editingContext, EOCnu eo) {
    EOCnu localInstance = (eo == null) ? null : (EOCnu)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
