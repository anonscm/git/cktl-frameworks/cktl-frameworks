package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.metier.controles.IDonnee;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteItarf;

import com.webobjects.foundation.NSDictionary;

public class EOSpecialiteItarf extends _EOSpecialiteItarf implements ISpecialiteItarf {
	private static Logger log = Logger.getLogger(EOSpecialiteItarf.class);

	public String clePrimaire() {
		return cBap()+IDonnee.SEPARATEUR_CLE_PRIMAIRE+cSpecialiteItarf();
	}
}
