// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMotifPosition.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMotifPosition extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MotifPosition";

	// Attributes
	public static final String C_MOTIF_POSITION_KEY = "cMotifPosition";
	public static final String C_POSITION_KEY = "cPosition";
	public static final String LC_MOTIF_POSITION_KEY = "lcMotifPosition";
	public static final String LL_MOTIF_POSITION_KEY = "llMotifPosition";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMotifPosition.class);

  public EOMotifPosition localInstanceIn(EOEditingContext editingContext) {
    EOMotifPosition localInstance = (EOMotifPosition)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifPosition() {
    return (String) storedValueForKey("cMotifPosition");
  }

  public void setCMotifPosition(String value) {
    if (_EOMotifPosition.LOG.isDebugEnabled()) {
    	_EOMotifPosition.LOG.debug( "updating cMotifPosition from " + cMotifPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifPosition");
  }

  public String cPosition() {
    return (String) storedValueForKey("cPosition");
  }

  public void setCPosition(String value) {
    if (_EOMotifPosition.LOG.isDebugEnabled()) {
    	_EOMotifPosition.LOG.debug( "updating cPosition from " + cPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cPosition");
  }

  public String lcMotifPosition() {
    return (String) storedValueForKey("lcMotifPosition");
  }

  public void setLcMotifPosition(String value) {
    if (_EOMotifPosition.LOG.isDebugEnabled()) {
    	_EOMotifPosition.LOG.debug( "updating lcMotifPosition from " + lcMotifPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "lcMotifPosition");
  }

  public String llMotifPosition() {
    return (String) storedValueForKey("llMotifPosition");
  }

  public void setLlMotifPosition(String value) {
    if (_EOMotifPosition.LOG.isDebugEnabled()) {
    	_EOMotifPosition.LOG.debug( "updating llMotifPosition from " + llMotifPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "llMotifPosition");
  }


  public static EOMotifPosition createMotifPosition(EOEditingContext editingContext, String cMotifPosition
, String cPosition
) {
    EOMotifPosition eo = (EOMotifPosition) EOUtilities.createAndInsertInstance(editingContext, _EOMotifPosition.ENTITY_NAME);    
		eo.setCMotifPosition(cMotifPosition);
		eo.setCPosition(cPosition);
    return eo;
  }

  public static NSArray<EOMotifPosition> fetchAllMotifPositions(EOEditingContext editingContext) {
    return _EOMotifPosition.fetchAllMotifPositions(editingContext, null);
  }

  public static NSArray<EOMotifPosition> fetchAllMotifPositions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMotifPosition.fetchMotifPositions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMotifPosition> fetchMotifPositions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMotifPosition.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMotifPosition> eoObjects = (NSArray<EOMotifPosition>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMotifPosition fetchMotifPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifPosition.fetchMotifPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifPosition fetchMotifPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMotifPosition> eoObjects = _EOMotifPosition.fetchMotifPositions(editingContext, qualifier, null);
    EOMotifPosition eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMotifPosition)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MotifPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifPosition fetchRequiredMotifPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifPosition.fetchRequiredMotifPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifPosition fetchRequiredMotifPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMotifPosition eoObject = _EOMotifPosition.fetchMotifPosition(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MotifPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifPosition localInstanceIn(EOEditingContext editingContext, EOMotifPosition eo) {
    EOMotifPosition localInstance = (eo == null) ? null : (EOMotifPosition)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
