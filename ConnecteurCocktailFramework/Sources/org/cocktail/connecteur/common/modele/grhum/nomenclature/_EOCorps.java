// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCorps.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCorps extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Corps";

	// Attributes
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_CORPS_KEY = "dFermetureCorps";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_CORPS_KEY = "dOuvertureCorps";
	public static final String LC_CORPS_KEY = "lcCorps";
	public static final String LL_CORPS_KEY = "llCorps";
	public static final String TEM_CFP_KEY = "temCfp";
	public static final String TEM_CRCT_KEY = "temCrct";
	public static final String TEM_DELEGATION_KEY = "temDelegation";
	public static final String TEM_SURNOMBRE_KEY = "temSurnombre";

	// Relationships
	public static final String TO_TYPE_POPULATION_KEY = "toTypePopulation";

  private static Logger LOG = Logger.getLogger(_EOCorps.class);

  public EOCorps localInstanceIn(EOEditingContext editingContext) {
    EOCorps localInstance = (EOCorps)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFermetureCorps() {
    return (NSTimestamp) storedValueForKey("dFermetureCorps");
  }

  public void setDFermetureCorps(NSTimestamp value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating dFermetureCorps from " + dFermetureCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermetureCorps");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dOuvertureCorps() {
    return (NSTimestamp) storedValueForKey("dOuvertureCorps");
  }

  public void setDOuvertureCorps(NSTimestamp value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating dOuvertureCorps from " + dOuvertureCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "dOuvertureCorps");
  }

  public String lcCorps() {
    return (String) storedValueForKey("lcCorps");
  }

  public void setLcCorps(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating lcCorps from " + lcCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "lcCorps");
  }

  public String llCorps() {
    return (String) storedValueForKey("llCorps");
  }

  public void setLlCorps(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating llCorps from " + llCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "llCorps");
  }

  public String temCfp() {
    return (String) storedValueForKey("temCfp");
  }

  public void setTemCfp(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating temCfp from " + temCfp() + " to " + value);
    }
    takeStoredValueForKey(value, "temCfp");
  }

  public String temCrct() {
    return (String) storedValueForKey("temCrct");
  }

  public void setTemCrct(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating temCrct from " + temCrct() + " to " + value);
    }
    takeStoredValueForKey(value, "temCrct");
  }

  public String temDelegation() {
    return (String) storedValueForKey("temDelegation");
  }

  public void setTemDelegation(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating temDelegation from " + temDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "temDelegation");
  }

  public String temSurnombre() {
    return (String) storedValueForKey("temSurnombre");
  }

  public void setTemSurnombre(String value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
    	_EOCorps.LOG.debug( "updating temSurnombre from " + temSurnombre() + " to " + value);
    }
    takeStoredValueForKey(value, "temSurnombre");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation toTypePopulation() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation)storedValueForKey("toTypePopulation");
  }

  public void setToTypePopulationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation value) {
    if (_EOCorps.LOG.isDebugEnabled()) {
      _EOCorps.LOG.debug("updating toTypePopulation from " + toTypePopulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypePopulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypePopulation");
    }
  }
  

  public static EOCorps createCorps(EOEditingContext editingContext, String cCorps
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOCorps eo = (EOCorps) EOUtilities.createAndInsertInstance(editingContext, _EOCorps.ENTITY_NAME);    
		eo.setCCorps(cCorps);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOCorps> fetchAllCorpses(EOEditingContext editingContext) {
    return _EOCorps.fetchAllCorpses(editingContext, null);
  }

  public static NSArray<EOCorps> fetchAllCorpses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCorps.fetchCorpses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCorps> fetchCorpses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCorps.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCorps> eoObjects = (NSArray<EOCorps>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCorps fetchCorps(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCorps.fetchCorps(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCorps fetchCorps(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCorps> eoObjects = _EOCorps.fetchCorpses(editingContext, qualifier, null);
    EOCorps eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCorps)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Corps that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCorps fetchRequiredCorps(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCorps.fetchRequiredCorps(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCorps fetchRequiredCorps(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCorps eoObject = _EOCorps.fetchCorps(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Corps that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCorps localInstanceIn(EOEditingContext editingContext, EOCorps eo) {
    EOCorps localInstance = (eo == null) ? null : (EOCorps)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
