// $LastChangedRevision$ DO NOT EDIT.  Make changes to EONaf.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EONaf extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Naf";

	// Attributes
	public static final String C_NAF_KEY = "cNaf";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String LL_NAF_KEY = "llNaf";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EONaf.class);

  public EONaf localInstanceIn(EOEditingContext editingContext) {
    EONaf localInstance = (EONaf)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cNaf() {
    return (String) storedValueForKey("cNaf");
  }

  public void setCNaf(String value) {
    if (_EONaf.LOG.isDebugEnabled()) {
    	_EONaf.LOG.debug( "updating cNaf from " + cNaf() + " to " + value);
    }
    takeStoredValueForKey(value, "cNaf");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EONaf.LOG.isDebugEnabled()) {
    	_EONaf.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EONaf.LOG.isDebugEnabled()) {
    	_EONaf.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public String llNaf() {
    return (String) storedValueForKey("llNaf");
  }

  public void setLlNaf(String value) {
    if (_EONaf.LOG.isDebugEnabled()) {
    	_EONaf.LOG.debug( "updating llNaf from " + llNaf() + " to " + value);
    }
    takeStoredValueForKey(value, "llNaf");
  }


  public static EONaf createNaf(EOEditingContext editingContext, String cNaf
) {
    EONaf eo = (EONaf) EOUtilities.createAndInsertInstance(editingContext, _EONaf.ENTITY_NAME);    
		eo.setCNaf(cNaf);
    return eo;
  }

  public static NSArray<EONaf> fetchAllNafs(EOEditingContext editingContext) {
    return _EONaf.fetchAllNafs(editingContext, null);
  }

  public static NSArray<EONaf> fetchAllNafs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONaf.fetchNafs(editingContext, null, sortOrderings);
  }

  public static NSArray<EONaf> fetchNafs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EONaf.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONaf> eoObjects = (NSArray<EONaf>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EONaf fetchNaf(EOEditingContext editingContext, String keyName, Object value) {
    return _EONaf.fetchNaf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONaf fetchNaf(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONaf> eoObjects = _EONaf.fetchNafs(editingContext, qualifier, null);
    EONaf eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EONaf)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Naf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONaf fetchRequiredNaf(EOEditingContext editingContext, String keyName, Object value) {
    return _EONaf.fetchRequiredNaf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONaf fetchRequiredNaf(EOEditingContext editingContext, EOQualifier qualifier) {
    EONaf eoObject = _EONaf.fetchNaf(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Naf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONaf localInstanceIn(EOEditingContext editingContext, EONaf eo) {
    EONaf localInstance = (eo == null) ? null : (EONaf)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
