// $LastChangedRevision$ DO NOT EDIT.  Make changes to EONatureBonifTerritoire.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EONatureBonifTerritoire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "NatureBonifTerritoire";

	// Attributes
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String NBOT_CODE_KEY = "nbotCode";
	public static final String NBOT_LIBELLE_KEY = "nbotLibelle";

	// Relationships
	public static final String TO_TAUX_KEY = "toTaux";

  private static Logger LOG = Logger.getLogger(_EONatureBonifTerritoire.class);

  public EONatureBonifTerritoire localInstanceIn(EOEditingContext editingContext) {
    EONatureBonifTerritoire localInstance = (EONatureBonifTerritoire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey("dateFermeture");
  }

  public void setDateFermeture(NSTimestamp value) {
    if (_EONatureBonifTerritoire.LOG.isDebugEnabled()) {
    	_EONatureBonifTerritoire.LOG.debug( "updating dateFermeture from " + dateFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFermeture");
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey("dateOuverture");
  }

  public void setDateOuverture(NSTimestamp value) {
    if (_EONatureBonifTerritoire.LOG.isDebugEnabled()) {
    	_EONatureBonifTerritoire.LOG.debug( "updating dateOuverture from " + dateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateOuverture");
  }

  public String nbotCode() {
    return (String) storedValueForKey("nbotCode");
  }

  public void setNbotCode(String value) {
    if (_EONatureBonifTerritoire.LOG.isDebugEnabled()) {
    	_EONatureBonifTerritoire.LOG.debug( "updating nbotCode from " + nbotCode() + " to " + value);
    }
    takeStoredValueForKey(value, "nbotCode");
  }

  public String nbotLibelle() {
    return (String) storedValueForKey("nbotLibelle");
  }

  public void setNbotLibelle(String value) {
    if (_EONatureBonifTerritoire.LOG.isDebugEnabled()) {
    	_EONatureBonifTerritoire.LOG.debug( "updating nbotLibelle from " + nbotLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "nbotLibelle");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux toTaux() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux)storedValueForKey("toTaux");
  }

  public void setToTauxRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux value) {
    if (_EONatureBonifTerritoire.LOG.isDebugEnabled()) {
      _EONatureBonifTerritoire.LOG.debug("updating toTaux from " + toTaux() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux oldValue = toTaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTaux");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTaux");
    }
  }
  

  public static EONatureBonifTerritoire createNatureBonifTerritoire(EOEditingContext editingContext) {
    EONatureBonifTerritoire eo = (EONatureBonifTerritoire) EOUtilities.createAndInsertInstance(editingContext, _EONatureBonifTerritoire.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EONatureBonifTerritoire> fetchAllNatureBonifTerritoires(EOEditingContext editingContext) {
    return _EONatureBonifTerritoire.fetchAllNatureBonifTerritoires(editingContext, null);
  }

  public static NSArray<EONatureBonifTerritoire> fetchAllNatureBonifTerritoires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONatureBonifTerritoire.fetchNatureBonifTerritoires(editingContext, null, sortOrderings);
  }

  public static NSArray<EONatureBonifTerritoire> fetchNatureBonifTerritoires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EONatureBonifTerritoire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONatureBonifTerritoire> eoObjects = (NSArray<EONatureBonifTerritoire>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EONatureBonifTerritoire fetchNatureBonifTerritoire(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBonifTerritoire.fetchNatureBonifTerritoire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBonifTerritoire fetchNatureBonifTerritoire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONatureBonifTerritoire> eoObjects = _EONatureBonifTerritoire.fetchNatureBonifTerritoires(editingContext, qualifier, null);
    EONatureBonifTerritoire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EONatureBonifTerritoire)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NatureBonifTerritoire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBonifTerritoire fetchRequiredNatureBonifTerritoire(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBonifTerritoire.fetchRequiredNatureBonifTerritoire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBonifTerritoire fetchRequiredNatureBonifTerritoire(EOEditingContext editingContext, EOQualifier qualifier) {
    EONatureBonifTerritoire eoObject = _EONatureBonifTerritoire.fetchNatureBonifTerritoire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NatureBonifTerritoire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBonifTerritoire localInstanceIn(EOEditingContext editingContext, EONatureBonifTerritoire eo) {
    EONatureBonifTerritoire localInstance = (eo == null) ? null : (EONatureBonifTerritoire)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
