// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOAnciennete.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOAnciennete extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ParamCgntAccTrav";

	// Attributes
	public static final String C_ANCIENNETE_KEY = "cAnciennete";
	public static final String DUREE_PLEIN_TRAIT_KEY = "dureePleinTrait";
	public static final String L_ANCIENNETE_KEY = "lAnciennete";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOAnciennete.class);

  public EOAnciennete localInstanceIn(EOEditingContext editingContext) {
    EOAnciennete localInstance = (EOAnciennete)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cAnciennete() {
    return (String) storedValueForKey("cAnciennete");
  }

  public void setCAnciennete(String value) {
    if (_EOAnciennete.LOG.isDebugEnabled()) {
    	_EOAnciennete.LOG.debug( "updating cAnciennete from " + cAnciennete() + " to " + value);
    }
    takeStoredValueForKey(value, "cAnciennete");
  }

  public Integer dureePleinTrait() {
    return (Integer) storedValueForKey("dureePleinTrait");
  }

  public void setDureePleinTrait(Integer value) {
    if (_EOAnciennete.LOG.isDebugEnabled()) {
    	_EOAnciennete.LOG.debug( "updating dureePleinTrait from " + dureePleinTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "dureePleinTrait");
  }

  public String lAnciennete() {
    return (String) storedValueForKey("lAnciennete");
  }

  public void setLAnciennete(String value) {
    if (_EOAnciennete.LOG.isDebugEnabled()) {
    	_EOAnciennete.LOG.debug( "updating lAnciennete from " + lAnciennete() + " to " + value);
    }
    takeStoredValueForKey(value, "lAnciennete");
  }


  public static EOAnciennete createParamCgntAccTrav(EOEditingContext editingContext, String cAnciennete
, Integer dureePleinTrait
) {
    EOAnciennete eo = (EOAnciennete) EOUtilities.createAndInsertInstance(editingContext, _EOAnciennete.ENTITY_NAME);    
		eo.setCAnciennete(cAnciennete);
		eo.setDureePleinTrait(dureePleinTrait);
    return eo;
  }

  public static NSArray<EOAnciennete> fetchAllParamCgntAccTravs(EOEditingContext editingContext) {
    return _EOAnciennete.fetchAllParamCgntAccTravs(editingContext, null);
  }

  public static NSArray<EOAnciennete> fetchAllParamCgntAccTravs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAnciennete.fetchParamCgntAccTravs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAnciennete> fetchParamCgntAccTravs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAnciennete.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAnciennete> eoObjects = (NSArray<EOAnciennete>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAnciennete fetchParamCgntAccTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAnciennete.fetchParamCgntAccTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAnciennete fetchParamCgntAccTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAnciennete> eoObjects = _EOAnciennete.fetchParamCgntAccTravs(editingContext, qualifier, null);
    EOAnciennete eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAnciennete)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ParamCgntAccTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAnciennete fetchRequiredParamCgntAccTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAnciennete.fetchRequiredParamCgntAccTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAnciennete fetchRequiredParamCgntAccTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAnciennete eoObject = _EOAnciennete.fetchParamCgntAccTrav(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ParamCgntAccTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAnciennete localInstanceIn(EOEditingContext editingContext, EOAnciennete eo) {
    EOAnciennete localInstance = (eo == null) ? null : (EOAnciennete)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
