// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeInstance.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeInstance extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeInstance";

	// Attributes
	public static final String C_TYPE_INSTANCE_KEY = "cTypeInstance";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_TYPE_INSTANCE_KEY = "lcTypeInstance";
	public static final String LL_TYPE_INSTANCE_KEY = "llTypeInstance";
	public static final String MAQUETTE_EDITION_KEY = "maquetteEdition";
	public static final String TEM_DOUBLE_CARRIERE_KEY = "temDoubleCarriere";
	public static final String TEM_ELC_CAP_KEY = "temElcCap";
	public static final String TEM_ELC_CNU_KEY = "temElcCnu";
	public static final String TEM_ELC_CS_KEY = "temElcCs";
	public static final String TEM_ELC_CSES_KEY = "temElcCses";
	public static final String TEM_ELC_CUFR_KEY = "temElcCufr";
	public static final String TEM_INSERTION_INDIVIDUS_KEY = "temInsertionIndividus";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String TEM_SECTEUR_KEY = "temSecteur";
	public static final String TEM_STR_PRINCIPALE_KEY = "temStrPrincipale";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeInstance.class);

  public EOTypeInstance localInstanceIn(EOEditingContext editingContext) {
    EOTypeInstance localInstance = (EOTypeInstance)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeInstance() {
    return (String) storedValueForKey("cTypeInstance");
  }

  public void setCTypeInstance(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating cTypeInstance from " + cTypeInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeInstance");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcTypeInstance() {
    return (String) storedValueForKey("lcTypeInstance");
  }

  public void setLcTypeInstance(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating lcTypeInstance from " + lcTypeInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeInstance");
  }

  public String llTypeInstance() {
    return (String) storedValueForKey("llTypeInstance");
  }

  public void setLlTypeInstance(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating llTypeInstance from " + llTypeInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeInstance");
  }

  public String maquetteEdition() {
    return (String) storedValueForKey("maquetteEdition");
  }

  public void setMaquetteEdition(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating maquetteEdition from " + maquetteEdition() + " to " + value);
    }
    takeStoredValueForKey(value, "maquetteEdition");
  }

  public String temDoubleCarriere() {
    return (String) storedValueForKey("temDoubleCarriere");
  }

  public void setTemDoubleCarriere(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temDoubleCarriere from " + temDoubleCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "temDoubleCarriere");
  }

  public String temElcCap() {
    return (String) storedValueForKey("temElcCap");
  }

  public void setTemElcCap(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temElcCap from " + temElcCap() + " to " + value);
    }
    takeStoredValueForKey(value, "temElcCap");
  }

  public String temElcCnu() {
    return (String) storedValueForKey("temElcCnu");
  }

  public void setTemElcCnu(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temElcCnu from " + temElcCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "temElcCnu");
  }

  public String temElcCs() {
    return (String) storedValueForKey("temElcCs");
  }

  public void setTemElcCs(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temElcCs from " + temElcCs() + " to " + value);
    }
    takeStoredValueForKey(value, "temElcCs");
  }

  public String temElcCses() {
    return (String) storedValueForKey("temElcCses");
  }

  public void setTemElcCses(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temElcCses from " + temElcCses() + " to " + value);
    }
    takeStoredValueForKey(value, "temElcCses");
  }

  public String temElcCufr() {
    return (String) storedValueForKey("temElcCufr");
  }

  public void setTemElcCufr(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temElcCufr from " + temElcCufr() + " to " + value);
    }
    takeStoredValueForKey(value, "temElcCufr");
  }

  public String temInsertionIndividus() {
    return (String) storedValueForKey("temInsertionIndividus");
  }

  public void setTemInsertionIndividus(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temInsertionIndividus from " + temInsertionIndividus() + " to " + value);
    }
    takeStoredValueForKey(value, "temInsertionIndividus");
  }

  public String temLocal() {
    return (String) storedValueForKey("temLocal");
  }

  public void setTemLocal(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temLocal from " + temLocal() + " to " + value);
    }
    takeStoredValueForKey(value, "temLocal");
  }

  public String temSecteur() {
    return (String) storedValueForKey("temSecteur");
  }

  public void setTemSecteur(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temSecteur from " + temSecteur() + " to " + value);
    }
    takeStoredValueForKey(value, "temSecteur");
  }

  public String temStrPrincipale() {
    return (String) storedValueForKey("temStrPrincipale");
  }

  public void setTemStrPrincipale(String value) {
    if (_EOTypeInstance.LOG.isDebugEnabled()) {
    	_EOTypeInstance.LOG.debug( "updating temStrPrincipale from " + temStrPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temStrPrincipale");
  }


  public static EOTypeInstance createTypeInstance(EOEditingContext editingContext, String cTypeInstance
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcTypeInstance
, String llTypeInstance
, String maquetteEdition
, String temElcCap
, String temElcCnu
, String temElcCs
, String temElcCses
, String temElcCufr
, String temLocal
, String temSecteur
, String temStrPrincipale
) {
    EOTypeInstance eo = (EOTypeInstance) EOUtilities.createAndInsertInstance(editingContext, _EOTypeInstance.ENTITY_NAME);    
		eo.setCTypeInstance(cTypeInstance);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcTypeInstance(lcTypeInstance);
		eo.setLlTypeInstance(llTypeInstance);
		eo.setMaquetteEdition(maquetteEdition);
		eo.setTemElcCap(temElcCap);
		eo.setTemElcCnu(temElcCnu);
		eo.setTemElcCs(temElcCs);
		eo.setTemElcCses(temElcCses);
		eo.setTemElcCufr(temElcCufr);
		eo.setTemLocal(temLocal);
		eo.setTemSecteur(temSecteur);
		eo.setTemStrPrincipale(temStrPrincipale);
    return eo;
  }

  public static NSArray<EOTypeInstance> fetchAllTypeInstances(EOEditingContext editingContext) {
    return _EOTypeInstance.fetchAllTypeInstances(editingContext, null);
  }

  public static NSArray<EOTypeInstance> fetchAllTypeInstances(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeInstance.fetchTypeInstances(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeInstance> fetchTypeInstances(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeInstance.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeInstance> eoObjects = (NSArray<EOTypeInstance>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeInstance fetchTypeInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInstance.fetchTypeInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInstance fetchTypeInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeInstance> eoObjects = _EOTypeInstance.fetchTypeInstances(editingContext, qualifier, null);
    EOTypeInstance eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeInstance)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInstance fetchRequiredTypeInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInstance.fetchRequiredTypeInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInstance fetchRequiredTypeInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeInstance eoObject = _EOTypeInstance.fetchTypeInstance(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInstance localInstanceIn(EOEditingContext editingContext, EOTypeInstance eo) {
    EOTypeInstance localInstance = (eo == null) ? null : (EOTypeInstance)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
