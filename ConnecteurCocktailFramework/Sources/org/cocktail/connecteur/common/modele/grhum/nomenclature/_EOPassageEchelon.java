// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPassageEchelon.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPassageEchelon extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PassageEchelon";

	// Attributes
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_GR_CHOIX_ANNEES_KEY = "dureeGrChoixAnnees";
	public static final String DUREE_GR_CHOIX_MOIS_KEY = "dureeGrChoixMois";
	public static final String DUREE_PASSAGE_ANNEES_KEY = "dureePassageAnnees";
	public static final String DUREE_PASSAGE_MOIS_KEY = "dureePassageMois";
	public static final String DUREE_PT_CHOIX_ANNEES_KEY = "dureePtChoixAnnees";
	public static final String DUREE_PT_CHOIX_MOIS_KEY = "dureePtChoixMois";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOPassageEchelon.class);

  public EOPassageEchelon localInstanceIn(EOEditingContext editingContext) {
    EOPassageEchelon localInstance = (EOPassageEchelon)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dureeGrChoixAnnees() {
    return (Integer) storedValueForKey("dureeGrChoixAnnees");
  }

  public void setDureeGrChoixAnnees(Integer value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dureeGrChoixAnnees from " + dureeGrChoixAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeGrChoixAnnees");
  }

  public Integer dureeGrChoixMois() {
    return (Integer) storedValueForKey("dureeGrChoixMois");
  }

  public void setDureeGrChoixMois(Integer value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dureeGrChoixMois from " + dureeGrChoixMois() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeGrChoixMois");
  }

  public Integer dureePassageAnnees() {
    return (Integer) storedValueForKey("dureePassageAnnees");
  }

  public void setDureePassageAnnees(Integer value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dureePassageAnnees from " + dureePassageAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "dureePassageAnnees");
  }

  public Integer dureePassageMois() {
    return (Integer) storedValueForKey("dureePassageMois");
  }

  public void setDureePassageMois(Integer value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dureePassageMois from " + dureePassageMois() + " to " + value);
    }
    takeStoredValueForKey(value, "dureePassageMois");
  }

  public Integer dureePtChoixAnnees() {
    return (Integer) storedValueForKey("dureePtChoixAnnees");
  }

  public void setDureePtChoixAnnees(Integer value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dureePtChoixAnnees from " + dureePtChoixAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "dureePtChoixAnnees");
  }

  public Integer dureePtChoixMois() {
    return (Integer) storedValueForKey("dureePtChoixMois");
  }

  public void setDureePtChoixMois(Integer value) {
    if (_EOPassageEchelon.LOG.isDebugEnabled()) {
    	_EOPassageEchelon.LOG.debug( "updating dureePtChoixMois from " + dureePtChoixMois() + " to " + value);
    }
    takeStoredValueForKey(value, "dureePtChoixMois");
  }


  public static EOPassageEchelon createPassageEchelon(EOEditingContext editingContext, String cEchelon
, String cGrade
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOPassageEchelon eo = (EOPassageEchelon) EOUtilities.createAndInsertInstance(editingContext, _EOPassageEchelon.ENTITY_NAME);    
		eo.setCEchelon(cEchelon);
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOPassageEchelon> fetchAllPassageEchelons(EOEditingContext editingContext) {
    return _EOPassageEchelon.fetchAllPassageEchelons(editingContext, null);
  }

  public static NSArray<EOPassageEchelon> fetchAllPassageEchelons(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPassageEchelon.fetchPassageEchelons(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPassageEchelon> fetchPassageEchelons(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPassageEchelon.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPassageEchelon> eoObjects = (NSArray<EOPassageEchelon>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPassageEchelon fetchPassageEchelon(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPassageEchelon.fetchPassageEchelon(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPassageEchelon fetchPassageEchelon(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPassageEchelon> eoObjects = _EOPassageEchelon.fetchPassageEchelons(editingContext, qualifier, null);
    EOPassageEchelon eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPassageEchelon)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PassageEchelon that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPassageEchelon fetchRequiredPassageEchelon(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPassageEchelon.fetchRequiredPassageEchelon(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPassageEchelon fetchRequiredPassageEchelon(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPassageEchelon eoObject = _EOPassageEchelon.fetchPassageEchelon(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PassageEchelon that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPassageEchelon localInstanceIn(EOEditingContext editingContext, EOPassageEchelon eo) {
    EOPassageEchelon localInstance = (eo == null) ? null : (EOPassageEchelon)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
