package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOTypeInstance extends _EOTypeInstance {
	public static EOTypeInstance getFromCode(EOEditingContext editingContext, String typeInstance) {
		return fetchTypeInstance(editingContext, C_TYPE_INSTANCE_KEY, typeInstance);
	}
}
