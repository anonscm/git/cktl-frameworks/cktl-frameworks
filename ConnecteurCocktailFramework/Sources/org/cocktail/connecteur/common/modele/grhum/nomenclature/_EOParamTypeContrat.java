// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOParamTypeContrat.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOParamTypeContrat extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ParamTypeContrat";

	// Attributes
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String PTC_INDICE_KEY = "ptcIndice";
	public static final String PTC_MONTANT_KEY = "ptcMontant";
	public static final String PTC_NBR_UNITE_KEY = "ptcNbrUnite";
	public static final String PTC_QUOTITE_KEY = "ptcQuotite";
	public static final String PTC_SMIC_KEY = "ptcSmic";
	public static final String PTC_TAUX_HORAIRE_KEY = "ptcTauxHoraire";
	public static final String PTC_TEM_PONCTUEL_KEY = "ptcTemPonctuel";
	public static final String PTC_TYPE_MONTANT_KEY = "ptcTypeMontant";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOParamTypeContrat.class);

  public EOParamTypeContrat localInstanceIn(EOEditingContext editingContext) {
    EOParamTypeContrat localInstance = (EOParamTypeContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public String ptcIndice() {
    return (String) storedValueForKey("ptcIndice");
  }

  public void setPtcIndice(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcIndice from " + ptcIndice() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcIndice");
  }

  public String ptcMontant() {
    return (String) storedValueForKey("ptcMontant");
  }

  public void setPtcMontant(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcMontant from " + ptcMontant() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcMontant");
  }

  public String ptcNbrUnite() {
    return (String) storedValueForKey("ptcNbrUnite");
  }

  public void setPtcNbrUnite(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcNbrUnite from " + ptcNbrUnite() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcNbrUnite");
  }

  public String ptcQuotite() {
    return (String) storedValueForKey("ptcQuotite");
  }

  public void setPtcQuotite(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcQuotite from " + ptcQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcQuotite");
  }

  public String ptcSmic() {
    return (String) storedValueForKey("ptcSmic");
  }

  public void setPtcSmic(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcSmic from " + ptcSmic() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcSmic");
  }

  public String ptcTauxHoraire() {
    return (String) storedValueForKey("ptcTauxHoraire");
  }

  public void setPtcTauxHoraire(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcTauxHoraire from " + ptcTauxHoraire() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcTauxHoraire");
  }

  public String ptcTemPonctuel() {
    return (String) storedValueForKey("ptcTemPonctuel");
  }

  public void setPtcTemPonctuel(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcTemPonctuel from " + ptcTemPonctuel() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcTemPonctuel");
  }

  public String ptcTypeMontant() {
    return (String) storedValueForKey("ptcTypeMontant");
  }

  public void setPtcTypeMontant(String value) {
    if (_EOParamTypeContrat.LOG.isDebugEnabled()) {
    	_EOParamTypeContrat.LOG.debug( "updating ptcTypeMontant from " + ptcTypeMontant() + " to " + value);
    }
    takeStoredValueForKey(value, "ptcTypeMontant");
  }


  public static EOParamTypeContrat createParamTypeContrat(EOEditingContext editingContext, String cTypeContratTrav
) {
    EOParamTypeContrat eo = (EOParamTypeContrat) EOUtilities.createAndInsertInstance(editingContext, _EOParamTypeContrat.ENTITY_NAME);    
		eo.setCTypeContratTrav(cTypeContratTrav);
    return eo;
  }

  public static NSArray<EOParamTypeContrat> fetchAllParamTypeContrats(EOEditingContext editingContext) {
    return _EOParamTypeContrat.fetchAllParamTypeContrats(editingContext, null);
  }

  public static NSArray<EOParamTypeContrat> fetchAllParamTypeContrats(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParamTypeContrat.fetchParamTypeContrats(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParamTypeContrat> fetchParamTypeContrats(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOParamTypeContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParamTypeContrat> eoObjects = (NSArray<EOParamTypeContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOParamTypeContrat fetchParamTypeContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParamTypeContrat.fetchParamTypeContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParamTypeContrat fetchParamTypeContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParamTypeContrat> eoObjects = _EOParamTypeContrat.fetchParamTypeContrats(editingContext, qualifier, null);
    EOParamTypeContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOParamTypeContrat)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ParamTypeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParamTypeContrat fetchRequiredParamTypeContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParamTypeContrat.fetchRequiredParamTypeContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParamTypeContrat fetchRequiredParamTypeContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParamTypeContrat eoObject = _EOParamTypeContrat.fetchParamTypeContrat(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ParamTypeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParamTypeContrat localInstanceIn(EOEditingContext editingContext, EOParamTypeContrat eo) {
    EOParamTypeContrat localInstance = (eo == null) ? null : (EOParamTypeContrat)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
