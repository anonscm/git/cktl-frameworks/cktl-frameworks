package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOTypeMad extends _EOTypeMad {
	public static EOTypeMad getFromCode(EOEditingContext editingContext, String typeMad) {
		return EOTypeMad.fetchTypeMad(editingContext, C_TYPE_MAD_KEY, typeMad);
	}
}
