package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOFonctionRelevantInstance extends _EOFonctionRelevantInstance {
	public static EOFonctionRelevantInstance getFromCode(EOEditingContext editingContext, Integer codeFonction) {
		return fetchFonctionRelevantInstance(editingContext, C_FONCTION_KEY, codeFonction);
	}
}
