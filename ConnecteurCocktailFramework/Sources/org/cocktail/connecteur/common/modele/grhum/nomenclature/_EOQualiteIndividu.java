// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOQualiteIndividu.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOQualiteIndividu extends  EOGenericRecord {
	public static final String ENTITY_NAME = "QualiteIndividu";

	// Attributes
	public static final String C_QUALITE_KEY = "cQualite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_QUALITE_KEY = "lcQualite";
	public static final String LL_QUALITE_KEY = "llQualite";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOQualiteIndividu.class);

  public EOQualiteIndividu localInstanceIn(EOEditingContext editingContext) {
    EOQualiteIndividu localInstance = (EOQualiteIndividu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cQualite() {
    return (String) storedValueForKey("cQualite");
  }

  public void setCQualite(String value) {
    if (_EOQualiteIndividu.LOG.isDebugEnabled()) {
    	_EOQualiteIndividu.LOG.debug( "updating cQualite from " + cQualite() + " to " + value);
    }
    takeStoredValueForKey(value, "cQualite");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOQualiteIndividu.LOG.isDebugEnabled()) {
    	_EOQualiteIndividu.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOQualiteIndividu.LOG.isDebugEnabled()) {
    	_EOQualiteIndividu.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcQualite() {
    return (String) storedValueForKey("lcQualite");
  }

  public void setLcQualite(String value) {
    if (_EOQualiteIndividu.LOG.isDebugEnabled()) {
    	_EOQualiteIndividu.LOG.debug( "updating lcQualite from " + lcQualite() + " to " + value);
    }
    takeStoredValueForKey(value, "lcQualite");
  }

  public String llQualite() {
    return (String) storedValueForKey("llQualite");
  }

  public void setLlQualite(String value) {
    if (_EOQualiteIndividu.LOG.isDebugEnabled()) {
    	_EOQualiteIndividu.LOG.debug( "updating llQualite from " + llQualite() + " to " + value);
    }
    takeStoredValueForKey(value, "llQualite");
  }


  public static EOQualiteIndividu createQualiteIndividu(EOEditingContext editingContext, String cQualite
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcQualite
, String llQualite
) {
    EOQualiteIndividu eo = (EOQualiteIndividu) EOUtilities.createAndInsertInstance(editingContext, _EOQualiteIndividu.ENTITY_NAME);    
		eo.setCQualite(cQualite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcQualite(lcQualite);
		eo.setLlQualite(llQualite);
    return eo;
  }

  public static NSArray<EOQualiteIndividu> fetchAllQualiteIndividus(EOEditingContext editingContext) {
    return _EOQualiteIndividu.fetchAllQualiteIndividus(editingContext, null);
  }

  public static NSArray<EOQualiteIndividu> fetchAllQualiteIndividus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOQualiteIndividu.fetchQualiteIndividus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOQualiteIndividu> fetchQualiteIndividus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOQualiteIndividu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOQualiteIndividu> eoObjects = (NSArray<EOQualiteIndividu>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOQualiteIndividu fetchQualiteIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOQualiteIndividu.fetchQualiteIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOQualiteIndividu fetchQualiteIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOQualiteIndividu> eoObjects = _EOQualiteIndividu.fetchQualiteIndividus(editingContext, qualifier, null);
    EOQualiteIndividu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOQualiteIndividu)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one QualiteIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOQualiteIndividu fetchRequiredQualiteIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOQualiteIndividu.fetchRequiredQualiteIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOQualiteIndividu fetchRequiredQualiteIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    EOQualiteIndividu eoObject = _EOQualiteIndividu.fetchQualiteIndividu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no QualiteIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOQualiteIndividu localInstanceIn(EOEditingContext editingContext, EOQualiteIndividu eo) {
    EOQualiteIndividu localInstance = (eo == null) ? null : (EOQualiteIndividu)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
