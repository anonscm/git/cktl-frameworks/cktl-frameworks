// EOBanque.java
// Created on Wed Jul 25 15:08:01 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOBanque extends _EOBanque {

	public EOBanque() {
		super();
	}

	// Méthodes ajoutées
	public void initAvec(String banque, String guichet, String bic, String domiciliation) {
		setCBanque(banque);
		setCGuichet(guichet);
		setDomiciliation(domiciliation);
		setBic(bic);
		NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
	}

	public static boolean estIbanFrancais(String iban) {
		if (iban == null)
			return false;
		return iban.startsWith("FR");
	}

	public static String codeBanqueDepuisIban(String iban) {
		if (iban == null || iban.length() < 9)
			return null;
		return iban.substring(4, 4 + 5);
	}

	public static String codeGuichetDepuisIban(String iban) {
		if (iban == null || iban.length() < 14)
			return null;
		return iban.substring(9, 9 + 5);
	}

	/**
	 * Recherche une banque Recherche en fonction du code banque et guichet
	 * s'ils sont présents. Sinon, recherche le guichet et banque dans l'iban.
	 * Pour les iban étranger, recherche dans la base le bic uniquement.
	 */
	public static EOBanque fetchBanquePourCodeBanqueGuichetIbanBic(EOEditingContext editingContext, String codeBanque, String codeGuichet, String iban,
			String bic) {
		if (codeGuichet != null && codeBanque != null) {
			return fetchBanquePourBanqueGuichet(editingContext, codeBanque, codeGuichet);
		}
		return fetchBanquePourIbanBic(editingContext, iban, bic);
	}

	private static EOBanque fetchBanquePourBanqueGuichet(EOEditingContext editingContext, String codeBanque, String codeGuichet) {
		if (editingContext == null || codeBanque == null || codeGuichet == null)
			return null;

		NSMutableArray args = new NSMutableArray(codeBanque);
		args.addObject(codeGuichet);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_BANQUE_KEY + " = %@ AND " + C_GUICHET_KEY + " = %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		NSArray resultats = editingContext.objectsWithFetchSpecification(myFetch);
		if (resultats.count() == 1)
			return (EOBanque) resultats.objectAtIndex(0);
		return null;
	}

	private static EOBanque fetchBanquePourIbanBic(EOEditingContext editingContext, String iban, String bic) {
		if (editingContext == null || iban == null || bic == null)
			return null;

		if (estIbanFrancais(iban)) {
			return fetchBanquePourBanqueGuichet(editingContext, codeBanqueDepuisIban(iban), codeGuichetDepuisIban(iban));
		}
		else {
			return fetchBanque(editingContext, BIC_KEY, bic);
		}
	}
}
