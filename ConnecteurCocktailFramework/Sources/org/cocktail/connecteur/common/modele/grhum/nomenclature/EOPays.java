// EOPays.java
// Created on Wed Jul 25 15:12:56 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOPays extends EOGenericRecord {
	public final static String CODE_PAYS_DEFAUT = "100";
	public final static String CODE_PAYS_INCONNU = "999";
    public EOPays() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPays(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String llPays() {
        return (String)storedValueForKey("llPays");
    }

    public void setLlPays(String value) {
        takeStoredValueForKey(value, "llPays");
    }

    public String lcPays() {
        return (String)storedValueForKey("lcPays");
    }

    public void setLcPays(String value) {
        takeStoredValueForKey(value, "lcPays");
    }

    public String lNationalite() {
        return (String)storedValueForKey("lNationalite");
    }

    public void setLNationalite(String value) {
        takeStoredValueForKey(value, "lNationalite");
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey("dCreation");
    }

    public void setDCreation(NSTimestamp value) {
        takeStoredValueForKey(value, "dCreation");
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey("dModification");
    }

    public void setDModification(NSTimestamp value) {
        takeStoredValueForKey(value, "dModification");
    }

    public String cPays() {
        return (String)storedValueForKey("cPays");
    }

    public void setCPays(String value) {
        takeStoredValueForKey(value, "cPays");
    }
    // Méthodes statiques
    public static EOQualifier qualifierValiditePaysPourDate(NSTimestamp date) {
    	NSMutableArray qualifiers = new NSMutableArray();
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dDebVal <= %@ OR dDebVal = nil" , new NSArray(date)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dFinVal >= %@ OR dFinVal = nil", new NSArray(date)));
    	return new EOAndQualifier(qualifiers);
    }
    /** Retourne true si le pays correspondant au code pays est valide a la date fournie */
    public static boolean estPaysValideADate(EOEditingContext editingContext,String cPays, NSTimestamp date) {
    	if (cPays == null || date == null) {
    		return false;
    	}
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("cPays = %@",new NSArray(cPays)));
    	qualifiers.addObject(qualifierValiditePaysPourDate(date));
    	EOFetchSpecification fs = new EOFetchSpecification("Pays",new EOAndQualifier(qualifiers),null);
    	return editingContext.objectsWithFetchSpecification(fs).count() > 0;
    }
}
