// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOLienFiliationEnfant.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLienFiliationEnfant extends  EOGenericRecord {
	public static final String ENTITY_NAME = "LienFiliationEnfant";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LFEN_CODE_KEY = "lfenCode";
	public static final String LFEN_EDITION_KEY = "lfenEdition";
	public static final String LFEN_LIBELLE_KEY = "lfenLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOLienFiliationEnfant.class);

  public EOLienFiliationEnfant localInstanceIn(EOEditingContext editingContext) {
    EOLienFiliationEnfant localInstance = (EOLienFiliationEnfant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOLienFiliationEnfant.LOG.isDebugEnabled()) {
    	_EOLienFiliationEnfant.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOLienFiliationEnfant.LOG.isDebugEnabled()) {
    	_EOLienFiliationEnfant.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOLienFiliationEnfant.LOG.isDebugEnabled()) {
    	_EOLienFiliationEnfant.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOLienFiliationEnfant.LOG.isDebugEnabled()) {
    	_EOLienFiliationEnfant.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lfenCode() {
    return (String) storedValueForKey("lfenCode");
  }

  public void setLfenCode(String value) {
    if (_EOLienFiliationEnfant.LOG.isDebugEnabled()) {
    	_EOLienFiliationEnfant.LOG.debug( "updating lfenCode from " + lfenCode() + " to " + value);
    }
    takeStoredValueForKey(value, "lfenCode");
  }

  public String lfenEdition() {
    return (String) storedValueForKey("lfenEdition");
  }

  public void setLfenEdition(String value) {
    if (_EOLienFiliationEnfant.LOG.isDebugEnabled()) {
    	_EOLienFiliationEnfant.LOG.debug( "updating lfenEdition from " + lfenEdition() + " to " + value);
    }
    takeStoredValueForKey(value, "lfenEdition");
  }

  public String lfenLibelle() {
    return (String) storedValueForKey("lfenLibelle");
  }

  public void setLfenLibelle(String value) {
    if (_EOLienFiliationEnfant.LOG.isDebugEnabled()) {
    	_EOLienFiliationEnfant.LOG.debug( "updating lfenLibelle from " + lfenLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "lfenLibelle");
  }


  public static EOLienFiliationEnfant createLienFiliationEnfant(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String lfenCode
) {
    EOLienFiliationEnfant eo = (EOLienFiliationEnfant) EOUtilities.createAndInsertInstance(editingContext, _EOLienFiliationEnfant.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLfenCode(lfenCode);
    return eo;
  }

  public static NSArray<EOLienFiliationEnfant> fetchAllLienFiliationEnfants(EOEditingContext editingContext) {
    return _EOLienFiliationEnfant.fetchAllLienFiliationEnfants(editingContext, null);
  }

  public static NSArray<EOLienFiliationEnfant> fetchAllLienFiliationEnfants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLienFiliationEnfant.fetchLienFiliationEnfants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLienFiliationEnfant> fetchLienFiliationEnfants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLienFiliationEnfant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLienFiliationEnfant> eoObjects = (NSArray<EOLienFiliationEnfant>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLienFiliationEnfant fetchLienFiliationEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLienFiliationEnfant.fetchLienFiliationEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLienFiliationEnfant fetchLienFiliationEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLienFiliationEnfant> eoObjects = _EOLienFiliationEnfant.fetchLienFiliationEnfants(editingContext, qualifier, null);
    EOLienFiliationEnfant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLienFiliationEnfant)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one LienFiliationEnfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLienFiliationEnfant fetchRequiredLienFiliationEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLienFiliationEnfant.fetchRequiredLienFiliationEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLienFiliationEnfant fetchRequiredLienFiliationEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLienFiliationEnfant eoObject = _EOLienFiliationEnfant.fetchLienFiliationEnfant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no LienFiliationEnfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLienFiliationEnfant localInstanceIn(EOEditingContext editingContext, EOLienFiliationEnfant eo) {
    EOLienFiliationEnfant localInstance = (eo == null) ? null : (EOLienFiliationEnfant)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
