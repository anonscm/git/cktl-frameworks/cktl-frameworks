// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODepartement.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODepartement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Departement";

	// Attributes
	public static final String C_DEPARTEMENT_KEY = "cDepartement";
	public static final String LL_DEPARTEMENT_KEY = "llDepartement";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EODepartement.class);

  public EODepartement localInstanceIn(EOEditingContext editingContext) {
    EODepartement localInstance = (EODepartement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDepartement() {
    return (String) storedValueForKey("cDepartement");
  }

  public void setCDepartement(String value) {
    if (_EODepartement.LOG.isDebugEnabled()) {
    	_EODepartement.LOG.debug( "updating cDepartement from " + cDepartement() + " to " + value);
    }
    takeStoredValueForKey(value, "cDepartement");
  }

  public String llDepartement() {
    return (String) storedValueForKey("llDepartement");
  }

  public void setLlDepartement(String value) {
    if (_EODepartement.LOG.isDebugEnabled()) {
    	_EODepartement.LOG.debug( "updating llDepartement from " + llDepartement() + " to " + value);
    }
    takeStoredValueForKey(value, "llDepartement");
  }


  public static EODepartement createDepartement(EOEditingContext editingContext, String cDepartement
) {
    EODepartement eo = (EODepartement) EOUtilities.createAndInsertInstance(editingContext, _EODepartement.ENTITY_NAME);    
		eo.setCDepartement(cDepartement);
    return eo;
  }

  public static NSArray<EODepartement> fetchAllDepartements(EOEditingContext editingContext) {
    return _EODepartement.fetchAllDepartements(editingContext, null);
  }

  public static NSArray<EODepartement> fetchAllDepartements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODepartement.fetchDepartements(editingContext, null, sortOrderings);
  }

  public static NSArray<EODepartement> fetchDepartements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODepartement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODepartement> eoObjects = (NSArray<EODepartement>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODepartement fetchDepartement(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepartement.fetchDepartement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepartement fetchDepartement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODepartement> eoObjects = _EODepartement.fetchDepartements(editingContext, qualifier, null);
    EODepartement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODepartement)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Departement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepartement fetchRequiredDepartement(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepartement.fetchRequiredDepartement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepartement fetchRequiredDepartement(EOEditingContext editingContext, EOQualifier qualifier) {
    EODepartement eoObject = _EODepartement.fetchDepartement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Departement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepartement localInstanceIn(EOEditingContext editingContext, EODepartement eo) {
    EODepartement localInstance = (eo == null) ? null : (EODepartement)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
