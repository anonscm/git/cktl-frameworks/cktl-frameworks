// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeOrigineFinancement.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeOrigineFinancement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeOrigineFinancement";

	// Attributes
	public static final String TOF_CODE_KEY = "tofCode";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeOrigineFinancement.class);

  public EOTypeOrigineFinancement localInstanceIn(EOEditingContext editingContext) {
    EOTypeOrigineFinancement localInstance = (EOTypeOrigineFinancement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tofCode() {
    return (String) storedValueForKey("tofCode");
  }

  public void setTofCode(String value) {
    if (_EOTypeOrigineFinancement.LOG.isDebugEnabled()) {
    	_EOTypeOrigineFinancement.LOG.debug( "updating tofCode from " + tofCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tofCode");
  }


  public static EOTypeOrigineFinancement createTypeOrigineFinancement(EOEditingContext editingContext, String tofCode
) {
    EOTypeOrigineFinancement eo = (EOTypeOrigineFinancement) EOUtilities.createAndInsertInstance(editingContext, _EOTypeOrigineFinancement.ENTITY_NAME);    
		eo.setTofCode(tofCode);
    return eo;
  }

  public static NSArray<EOTypeOrigineFinancement> fetchAllTypeOrigineFinancements(EOEditingContext editingContext) {
    return _EOTypeOrigineFinancement.fetchAllTypeOrigineFinancements(editingContext, null);
  }

  public static NSArray<EOTypeOrigineFinancement> fetchAllTypeOrigineFinancements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeOrigineFinancement.fetchTypeOrigineFinancements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeOrigineFinancement> fetchTypeOrigineFinancements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeOrigineFinancement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeOrigineFinancement> eoObjects = (NSArray<EOTypeOrigineFinancement>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeOrigineFinancement fetchTypeOrigineFinancement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeOrigineFinancement.fetchTypeOrigineFinancement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeOrigineFinancement fetchTypeOrigineFinancement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeOrigineFinancement> eoObjects = _EOTypeOrigineFinancement.fetchTypeOrigineFinancements(editingContext, qualifier, null);
    EOTypeOrigineFinancement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeOrigineFinancement)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeOrigineFinancement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeOrigineFinancement fetchRequiredTypeOrigineFinancement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeOrigineFinancement.fetchRequiredTypeOrigineFinancement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeOrigineFinancement fetchRequiredTypeOrigineFinancement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeOrigineFinancement eoObject = _EOTypeOrigineFinancement.fetchTypeOrigineFinancement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeOrigineFinancement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeOrigineFinancement localInstanceIn(EOEditingContext editingContext, EOTypeOrigineFinancement eo) {
    EOTypeOrigineFinancement localInstance = (eo == null) ? null : (EOTypeOrigineFinancement)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
