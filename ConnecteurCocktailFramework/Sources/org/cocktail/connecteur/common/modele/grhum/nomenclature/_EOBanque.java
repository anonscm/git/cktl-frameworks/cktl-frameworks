// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOBanque.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOBanque extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Banque";

	// Attributes
	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DOMICILIATION_KEY = "domiciliation";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOBanque.class);

  public EOBanque localInstanceIn(EOEditingContext editingContext) {
    EOBanque localInstance = (EOBanque)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String bic() {
    return (String) storedValueForKey("bic");
  }

  public void setBic(String value) {
    if (_EOBanque.LOG.isDebugEnabled()) {
    	_EOBanque.LOG.debug( "updating bic from " + bic() + " to " + value);
    }
    takeStoredValueForKey(value, "bic");
  }

  public String cBanque() {
    return (String) storedValueForKey("cBanque");
  }

  public void setCBanque(String value) {
    if (_EOBanque.LOG.isDebugEnabled()) {
    	_EOBanque.LOG.debug( "updating cBanque from " + cBanque() + " to " + value);
    }
    takeStoredValueForKey(value, "cBanque");
  }

  public String cGuichet() {
    return (String) storedValueForKey("cGuichet");
  }

  public void setCGuichet(String value) {
    if (_EOBanque.LOG.isDebugEnabled()) {
    	_EOBanque.LOG.debug( "updating cGuichet from " + cGuichet() + " to " + value);
    }
    takeStoredValueForKey(value, "cGuichet");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBanque.LOG.isDebugEnabled()) {
    	_EOBanque.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOBanque.LOG.isDebugEnabled()) {
    	_EOBanque.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String domiciliation() {
    return (String) storedValueForKey("domiciliation");
  }

  public void setDomiciliation(String value) {
    if (_EOBanque.LOG.isDebugEnabled()) {
    	_EOBanque.LOG.debug( "updating domiciliation from " + domiciliation() + " to " + value);
    }
    takeStoredValueForKey(value, "domiciliation");
  }


  public static EOBanque createBanque(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String domiciliation
) {
    EOBanque eo = (EOBanque) EOUtilities.createAndInsertInstance(editingContext, _EOBanque.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setDomiciliation(domiciliation);
    return eo;
  }

  public static NSArray<EOBanque> fetchAllBanques(EOEditingContext editingContext) {
    return _EOBanque.fetchAllBanques(editingContext, null);
  }

  public static NSArray<EOBanque> fetchAllBanques(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBanque.fetchBanques(editingContext, null, sortOrderings);
  }

  public static NSArray<EOBanque> fetchBanques(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOBanque.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBanque> eoObjects = (NSArray<EOBanque>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOBanque fetchBanque(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBanque.fetchBanque(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBanque fetchBanque(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBanque> eoObjects = _EOBanque.fetchBanques(editingContext, qualifier, null);
    EOBanque eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOBanque)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Banque that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBanque fetchRequiredBanque(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBanque.fetchRequiredBanque(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBanque fetchRequiredBanque(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBanque eoObject = _EOBanque.fetchBanque(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Banque that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBanque localInstanceIn(EOEditingContext editingContext, EOBanque eo) {
    EOBanque localInstance = (eo == null) ? null : (EOBanque)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
