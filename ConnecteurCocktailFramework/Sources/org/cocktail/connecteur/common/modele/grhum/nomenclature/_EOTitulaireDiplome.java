// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTitulaireDiplome.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTitulaireDiplome extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TitulaireDiplome";

	// Attributes
	public static final String C_TITULAIRE_DIPLOME_KEY = "cTitulaireDiplome";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTitulaireDiplome.class);

  public EOTitulaireDiplome localInstanceIn(EOEditingContext editingContext) {
    EOTitulaireDiplome localInstance = (EOTitulaireDiplome)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTitulaireDiplome() {
    return (String) storedValueForKey("cTitulaireDiplome");
  }

  public void setCTitulaireDiplome(String value) {
    if (_EOTitulaireDiplome.LOG.isDebugEnabled()) {
    	_EOTitulaireDiplome.LOG.debug( "updating cTitulaireDiplome from " + cTitulaireDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "cTitulaireDiplome");
  }


  public static EOTitulaireDiplome createTitulaireDiplome(EOEditingContext editingContext, String cTitulaireDiplome
) {
    EOTitulaireDiplome eo = (EOTitulaireDiplome) EOUtilities.createAndInsertInstance(editingContext, _EOTitulaireDiplome.ENTITY_NAME);    
		eo.setCTitulaireDiplome(cTitulaireDiplome);
    return eo;
  }

  public static NSArray<EOTitulaireDiplome> fetchAllTitulaireDiplomes(EOEditingContext editingContext) {
    return _EOTitulaireDiplome.fetchAllTitulaireDiplomes(editingContext, null);
  }

  public static NSArray<EOTitulaireDiplome> fetchAllTitulaireDiplomes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTitulaireDiplome.fetchTitulaireDiplomes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTitulaireDiplome> fetchTitulaireDiplomes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTitulaireDiplome.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTitulaireDiplome> eoObjects = (NSArray<EOTitulaireDiplome>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTitulaireDiplome fetchTitulaireDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTitulaireDiplome.fetchTitulaireDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTitulaireDiplome fetchTitulaireDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTitulaireDiplome> eoObjects = _EOTitulaireDiplome.fetchTitulaireDiplomes(editingContext, qualifier, null);
    EOTitulaireDiplome eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTitulaireDiplome)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TitulaireDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTitulaireDiplome fetchRequiredTitulaireDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTitulaireDiplome.fetchRequiredTitulaireDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTitulaireDiplome fetchRequiredTitulaireDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTitulaireDiplome eoObject = _EOTitulaireDiplome.fetchTitulaireDiplome(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TitulaireDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTitulaireDiplome localInstanceIn(EOEditingContext editingContext, EOTitulaireDiplome eo) {
    EOTitulaireDiplome localInstance = (eo == null) ? null : (EOTitulaireDiplome)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
