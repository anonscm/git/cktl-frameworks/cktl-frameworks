// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMotifReduction.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMotifReduction extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MotifReduction";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String MORE_LIBELLE_KEY = "moreLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMotifReduction.class);

  public EOMotifReduction localInstanceIn(EOEditingContext editingContext) {
    EOMotifReduction localInstance = (EOMotifReduction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMotifReduction.LOG.isDebugEnabled()) {
    	_EOMotifReduction.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMotifReduction.LOG.isDebugEnabled()) {
    	_EOMotifReduction.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String moreLibelle() {
    return (String) storedValueForKey("moreLibelle");
  }

  public void setMoreLibelle(String value) {
    if (_EOMotifReduction.LOG.isDebugEnabled()) {
    	_EOMotifReduction.LOG.debug( "updating moreLibelle from " + moreLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "moreLibelle");
  }


  public static EOMotifReduction createMotifReduction(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String moreLibelle
) {
    EOMotifReduction eo = (EOMotifReduction) EOUtilities.createAndInsertInstance(editingContext, _EOMotifReduction.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setMoreLibelle(moreLibelle);
    return eo;
  }

  public static NSArray<EOMotifReduction> fetchAllMotifReductions(EOEditingContext editingContext) {
    return _EOMotifReduction.fetchAllMotifReductions(editingContext, null);
  }

  public static NSArray<EOMotifReduction> fetchAllMotifReductions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMotifReduction.fetchMotifReductions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMotifReduction> fetchMotifReductions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMotifReduction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMotifReduction> eoObjects = (NSArray<EOMotifReduction>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMotifReduction fetchMotifReduction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifReduction.fetchMotifReduction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifReduction fetchMotifReduction(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMotifReduction> eoObjects = _EOMotifReduction.fetchMotifReductions(editingContext, qualifier, null);
    EOMotifReduction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMotifReduction)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MotifReduction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifReduction fetchRequiredMotifReduction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifReduction.fetchRequiredMotifReduction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifReduction fetchRequiredMotifReduction(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMotifReduction eoObject = _EOMotifReduction.fetchMotifReduction(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MotifReduction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifReduction localInstanceIn(EOEditingContext editingContext, EOMotifReduction eo) {
    EOMotifReduction localInstance = (eo == null) ? null : (EOMotifReduction)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
