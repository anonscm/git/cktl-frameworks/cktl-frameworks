// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeModDelegation.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeModDelegation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeModDelegation";

	// Attributes
	public static final String C_MOD_DELEGATION_KEY = "cModDelegation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_MOD_DELEGATION_KEY = "lcModDelegation";
	public static final String LL_MOD_DELEGATION_KEY = "llModDelegation";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeModDelegation.class);

  public EOTypeModDelegation localInstanceIn(EOEditingContext editingContext) {
    EOTypeModDelegation localInstance = (EOTypeModDelegation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cModDelegation() {
    return (String) storedValueForKey("cModDelegation");
  }

  public void setCModDelegation(String value) {
    if (_EOTypeModDelegation.LOG.isDebugEnabled()) {
    	_EOTypeModDelegation.LOG.debug( "updating cModDelegation from " + cModDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "cModDelegation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeModDelegation.LOG.isDebugEnabled()) {
    	_EOTypeModDelegation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeModDelegation.LOG.isDebugEnabled()) {
    	_EOTypeModDelegation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcModDelegation() {
    return (String) storedValueForKey("lcModDelegation");
  }

  public void setLcModDelegation(String value) {
    if (_EOTypeModDelegation.LOG.isDebugEnabled()) {
    	_EOTypeModDelegation.LOG.debug( "updating lcModDelegation from " + lcModDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "lcModDelegation");
  }

  public String llModDelegation() {
    return (String) storedValueForKey("llModDelegation");
  }

  public void setLlModDelegation(String value) {
    if (_EOTypeModDelegation.LOG.isDebugEnabled()) {
    	_EOTypeModDelegation.LOG.debug( "updating llModDelegation from " + llModDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "llModDelegation");
  }


  public static EOTypeModDelegation createTypeModDelegation(EOEditingContext editingContext, String cModDelegation
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOTypeModDelegation eo = (EOTypeModDelegation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeModDelegation.ENTITY_NAME);    
		eo.setCModDelegation(cModDelegation);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOTypeModDelegation> fetchAllTypeModDelegations(EOEditingContext editingContext) {
    return _EOTypeModDelegation.fetchAllTypeModDelegations(editingContext, null);
  }

  public static NSArray<EOTypeModDelegation> fetchAllTypeModDelegations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeModDelegation.fetchTypeModDelegations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeModDelegation> fetchTypeModDelegations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeModDelegation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeModDelegation> eoObjects = (NSArray<EOTypeModDelegation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeModDelegation fetchTypeModDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeModDelegation.fetchTypeModDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeModDelegation fetchTypeModDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeModDelegation> eoObjects = _EOTypeModDelegation.fetchTypeModDelegations(editingContext, qualifier, null);
    EOTypeModDelegation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeModDelegation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeModDelegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeModDelegation fetchRequiredTypeModDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeModDelegation.fetchRequiredTypeModDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeModDelegation fetchRequiredTypeModDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeModDelegation eoObject = _EOTypeModDelegation.fetchTypeModDelegation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeModDelegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeModDelegation localInstanceIn(EOEditingContext editingContext, EOTypeModDelegation eo) {
    EOTypeModDelegation localInstance = (eo == null) ? null : (EOTypeModDelegation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
