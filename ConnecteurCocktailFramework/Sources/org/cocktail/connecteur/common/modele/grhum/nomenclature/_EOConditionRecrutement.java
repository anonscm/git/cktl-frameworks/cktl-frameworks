// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOConditionRecrutement.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOConditionRecrutement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ConditionRecrutement";

	// Attributes
	public static final String C_COND_RECRUT_KEY = "cCondRecrut";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String DUREE_MAX_CR_KEY = "dureeMaxCr";
	public static final String ID_COND_RECRUT_KEY = "idCondRecrut";
	public static final String L_COND_RECRUT_KEY = "lCondRecrut";

	// Relationships
	public static final String TYPE_CONTRAT_TRAVAIL_KEY = "typeContratTravail";

  private static Logger LOG = Logger.getLogger(_EOConditionRecrutement.class);

  public EOConditionRecrutement localInstanceIn(EOEditingContext editingContext) {
    EOConditionRecrutement localInstance = (EOConditionRecrutement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCondRecrut() {
    return (String) storedValueForKey("cCondRecrut");
  }

  public void setCCondRecrut(String value) {
    if (_EOConditionRecrutement.LOG.isDebugEnabled()) {
    	_EOConditionRecrutement.LOG.debug( "updating cCondRecrut from " + cCondRecrut() + " to " + value);
    }
    takeStoredValueForKey(value, "cCondRecrut");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOConditionRecrutement.LOG.isDebugEnabled()) {
    	_EOConditionRecrutement.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public Integer dureeMaxCr() {
    return (Integer) storedValueForKey("dureeMaxCr");
  }

  public void setDureeMaxCr(Integer value) {
    if (_EOConditionRecrutement.LOG.isDebugEnabled()) {
    	_EOConditionRecrutement.LOG.debug( "updating dureeMaxCr from " + dureeMaxCr() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeMaxCr");
  }

  public Integer idCondRecrut() {
    return (Integer) storedValueForKey("idCondRecrut");
  }

  public void setIdCondRecrut(Integer value) {
    if (_EOConditionRecrutement.LOG.isDebugEnabled()) {
    	_EOConditionRecrutement.LOG.debug( "updating idCondRecrut from " + idCondRecrut() + " to " + value);
    }
    takeStoredValueForKey(value, "idCondRecrut");
  }

  public String lCondRecrut() {
    return (String) storedValueForKey("lCondRecrut");
  }

  public void setLCondRecrut(String value) {
    if (_EOConditionRecrutement.LOG.isDebugEnabled()) {
    	_EOConditionRecrutement.LOG.debug( "updating lCondRecrut from " + lCondRecrut() + " to " + value);
    }
    takeStoredValueForKey(value, "lCondRecrut");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail typeContratTravail() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail)storedValueForKey("typeContratTravail");
  }

  public void setTypeContratTravailRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail value) {
    if (_EOConditionRecrutement.LOG.isDebugEnabled()) {
      _EOConditionRecrutement.LOG.debug("updating typeContratTravail from " + typeContratTravail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail oldValue = typeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeContratTravail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeContratTravail");
    }
  }
  

  public static EOConditionRecrutement createConditionRecrutement(EOEditingContext editingContext, String cCondRecrut
, String cTypeContratTrav
, Integer idCondRecrut
, String lCondRecrut
, org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail typeContratTravail) {
    EOConditionRecrutement eo = (EOConditionRecrutement) EOUtilities.createAndInsertInstance(editingContext, _EOConditionRecrutement.ENTITY_NAME);    
		eo.setCCondRecrut(cCondRecrut);
		eo.setCTypeContratTrav(cTypeContratTrav);
		eo.setIdCondRecrut(idCondRecrut);
		eo.setLCondRecrut(lCondRecrut);
    eo.setTypeContratTravailRelationship(typeContratTravail);
    return eo;
  }

  public static NSArray<EOConditionRecrutement> fetchAllConditionRecrutements(EOEditingContext editingContext) {
    return _EOConditionRecrutement.fetchAllConditionRecrutements(editingContext, null);
  }

  public static NSArray<EOConditionRecrutement> fetchAllConditionRecrutements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOConditionRecrutement.fetchConditionRecrutements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOConditionRecrutement> fetchConditionRecrutements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOConditionRecrutement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOConditionRecrutement> eoObjects = (NSArray<EOConditionRecrutement>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOConditionRecrutement fetchConditionRecrutement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOConditionRecrutement.fetchConditionRecrutement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOConditionRecrutement fetchConditionRecrutement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOConditionRecrutement> eoObjects = _EOConditionRecrutement.fetchConditionRecrutements(editingContext, qualifier, null);
    EOConditionRecrutement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOConditionRecrutement)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ConditionRecrutement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOConditionRecrutement fetchRequiredConditionRecrutement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOConditionRecrutement.fetchRequiredConditionRecrutement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOConditionRecrutement fetchRequiredConditionRecrutement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOConditionRecrutement eoObject = _EOConditionRecrutement.fetchConditionRecrutement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ConditionRecrutement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOConditionRecrutement localInstanceIn(EOEditingContext editingContext, EOConditionRecrutement eo) {
    EOConditionRecrutement localInstance = (eo == null) ? null : (EOConditionRecrutement)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
