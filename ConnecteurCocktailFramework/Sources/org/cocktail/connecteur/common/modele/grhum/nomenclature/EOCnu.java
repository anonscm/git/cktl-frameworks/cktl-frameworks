// EOCnu.java
// Created on Tue May 19 10:38:01 Europe/Paris 2009 by Apple EOModeler Version 5.2

package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.metier.controles.IDonnee;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ICnu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class EOCnu extends _EOCnu implements ICnu {

    public EOCnu() {
        super();
    }


    // Méthodes ajoutées
    public boolean estValide() {
    	return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
    }
    public static EOCnu rechercherCnuPourNoCnu(EOEditingContext editingContext,Number noCnu) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("noCnu = %@", new NSArray(noCnu));
    	EOFetchSpecification fs = new EOFetchSpecification("Cnu",qualifier,null);
    	try {
    		return (EOCnu)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
    public static EOCnu rechercherCnuPourSectionEtSousSection(EOEditingContext editingContext,String sectionCnu,String sousSection) {
    	if (sectionCnu == null) {
    		return null;
    	}
    	NSMutableArray args = new NSMutableArray(sectionCnu);
    	String stringQualifier = "cSectionCnu = %@";
    	if (sousSection != null) {
    		args.addObject(sousSection);
    		stringQualifier += " AND cSousSectionCnu = %@";
    	} else {
    		args.addObject("00");
    		args.addObject("000");
    		stringQualifier += " AND (cSousSectionCnu = NIL OR cSousSectionCnu=%@ OR cSousSectionCnu=%@)";
    	}
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
    	EOFetchSpecification fs = new EOFetchSpecification("Cnu",qualifier,null);
    	try {
    		return (EOCnu)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }

	public String clePrimaire() {
		return cSectionCnu()+IDonnee.SEPARATEUR_CLE_PRIMAIRE+cSousSectionCnu();
	}
}
