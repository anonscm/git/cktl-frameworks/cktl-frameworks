// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeNoTel.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeNoTel extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeNoTel";

	// Attributes
	public static final String C_TYPE_NO_TEL_KEY = "cTypeNoTel";
	public static final String L_TYPE_NO_TEL_KEY = "lTypeNoTel";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeNoTel.class);

  public EOTypeNoTel localInstanceIn(EOEditingContext editingContext) {
    EOTypeNoTel localInstance = (EOTypeNoTel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeNoTel() {
    return (String) storedValueForKey("cTypeNoTel");
  }

  public void setCTypeNoTel(String value) {
    if (_EOTypeNoTel.LOG.isDebugEnabled()) {
    	_EOTypeNoTel.LOG.debug( "updating cTypeNoTel from " + cTypeNoTel() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeNoTel");
  }

  public String lTypeNoTel() {
    return (String) storedValueForKey("lTypeNoTel");
  }

  public void setLTypeNoTel(String value) {
    if (_EOTypeNoTel.LOG.isDebugEnabled()) {
    	_EOTypeNoTel.LOG.debug( "updating lTypeNoTel from " + lTypeNoTel() + " to " + value);
    }
    takeStoredValueForKey(value, "lTypeNoTel");
  }


  public static EOTypeNoTel createTypeNoTel(EOEditingContext editingContext, String cTypeNoTel
, String lTypeNoTel
) {
    EOTypeNoTel eo = (EOTypeNoTel) EOUtilities.createAndInsertInstance(editingContext, _EOTypeNoTel.ENTITY_NAME);    
		eo.setCTypeNoTel(cTypeNoTel);
		eo.setLTypeNoTel(lTypeNoTel);
    return eo;
  }

  public static NSArray<EOTypeNoTel> fetchAllTypeNoTels(EOEditingContext editingContext) {
    return _EOTypeNoTel.fetchAllTypeNoTels(editingContext, null);
  }

  public static NSArray<EOTypeNoTel> fetchAllTypeNoTels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeNoTel.fetchTypeNoTels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeNoTel> fetchTypeNoTels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeNoTel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeNoTel> eoObjects = (NSArray<EOTypeNoTel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeNoTel fetchTypeNoTel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeNoTel.fetchTypeNoTel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeNoTel fetchTypeNoTel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeNoTel> eoObjects = _EOTypeNoTel.fetchTypeNoTels(editingContext, qualifier, null);
    EOTypeNoTel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeNoTel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeNoTel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeNoTel fetchRequiredTypeNoTel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeNoTel.fetchRequiredTypeNoTel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeNoTel fetchRequiredTypeNoTel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeNoTel eoObject = _EOTypeNoTel.fetchTypeNoTel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeNoTel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeNoTel localInstanceIn(EOEditingContext editingContext, EOTypeNoTel eo) {
    EOTypeNoTel localInstance = (eo == null) ? null : (EOTypeNoTel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
