// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOBap.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOBap extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Bap";

	// Attributes
	public static final String C_BAP_KEY = "cBap";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_BAP_KEY = "lcBap";
	public static final String LL_BAP_KEY = "llBap";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOBap.class);

  public EOBap localInstanceIn(EOEditingContext editingContext) {
    EOBap localInstance = (EOBap)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOBap.LOG.isDebugEnabled()) {
    	_EOBap.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBap.LOG.isDebugEnabled()) {
    	_EOBap.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOBap.LOG.isDebugEnabled()) {
    	_EOBap.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcBap() {
    return (String) storedValueForKey("lcBap");
  }

  public void setLcBap(String value) {
    if (_EOBap.LOG.isDebugEnabled()) {
    	_EOBap.LOG.debug( "updating lcBap from " + lcBap() + " to " + value);
    }
    takeStoredValueForKey(value, "lcBap");
  }

  public String llBap() {
    return (String) storedValueForKey("llBap");
  }

  public void setLlBap(String value) {
    if (_EOBap.LOG.isDebugEnabled()) {
    	_EOBap.LOG.debug( "updating llBap from " + llBap() + " to " + value);
    }
    takeStoredValueForKey(value, "llBap");
  }


  public static EOBap createBap(EOEditingContext editingContext, String cBap
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOBap eo = (EOBap) EOUtilities.createAndInsertInstance(editingContext, _EOBap.ENTITY_NAME);    
		eo.setCBap(cBap);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOBap> fetchAllBaps(EOEditingContext editingContext) {
    return _EOBap.fetchAllBaps(editingContext, null);
  }

  public static NSArray<EOBap> fetchAllBaps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBap.fetchBaps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOBap> fetchBaps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOBap.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBap> eoObjects = (NSArray<EOBap>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOBap fetchBap(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBap.fetchBap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBap fetchBap(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBap> eoObjects = _EOBap.fetchBaps(editingContext, qualifier, null);
    EOBap eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOBap)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Bap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBap fetchRequiredBap(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBap.fetchRequiredBap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBap fetchRequiredBap(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBap eoObject = _EOBap.fetchBap(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Bap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBap localInstanceIn(EOEditingContext editingContext, EOBap eo) {
    EOBap localInstance = (eo == null) ? null : (EOBap)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
