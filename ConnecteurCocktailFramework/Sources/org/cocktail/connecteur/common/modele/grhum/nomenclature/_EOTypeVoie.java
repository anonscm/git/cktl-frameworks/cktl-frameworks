// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeVoie.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeVoie extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeVoie";

	// Attributes
	public static final String CODE_VOIE_KEY = "codeVoie";
	public static final String LIBELLE_VOIE_KEY = "libelleVoie";

	// Relationships
	public static final String ADRESSES_KEY = "adresses";

  private static Logger LOG = Logger.getLogger(_EOTypeVoie.class);

  public EOTypeVoie localInstanceIn(EOEditingContext editingContext) {
    EOTypeVoie localInstance = (EOTypeVoie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeVoie() {
    return (String) storedValueForKey("codeVoie");
  }

  public void setCodeVoie(String value) {
    if (_EOTypeVoie.LOG.isDebugEnabled()) {
    	_EOTypeVoie.LOG.debug( "updating codeVoie from " + codeVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "codeVoie");
  }

  public String libelleVoie() {
    return (String) storedValueForKey("libelleVoie");
  }

  public void setLibelleVoie(String value) {
    if (_EOTypeVoie.LOG.isDebugEnabled()) {
    	_EOTypeVoie.LOG.debug( "updating libelleVoie from " + libelleVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "libelleVoie");
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse> adresses() {
    return (NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse>)storedValueForKey("adresses");
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse> adresses(EOQualifier qualifier) {
    return adresses(qualifier, null, false);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse> adresses(EOQualifier qualifier, boolean fetch) {
    return adresses(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse> adresses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse.TO_TYPE_VOIE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse.fetchAdresses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = adresses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAdressesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse object) {
    if (_EOTypeVoie.LOG.isDebugEnabled()) {
      _EOTypeVoie.LOG.debug("adding " + object + " to adresses relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "adresses");
  }

  public void removeFromAdressesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse object) {
    if (_EOTypeVoie.LOG.isDebugEnabled()) {
      _EOTypeVoie.LOG.debug("removing " + object + " from adresses relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "adresses");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse createAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Adresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "adresses");
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse) eo;
  }

  public void deleteAdressesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "adresses");
    editingContext().deleteObject(object);
  }

  public void deleteAllAdressesRelationships() {
    Enumeration objects = adresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAdressesRelationship((org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse)objects.nextElement());
    }
  }


  public static EOTypeVoie createTypeVoie(EOEditingContext editingContext, String codeVoie
, String libelleVoie
) {
    EOTypeVoie eo = (EOTypeVoie) EOUtilities.createAndInsertInstance(editingContext, _EOTypeVoie.ENTITY_NAME);    
		eo.setCodeVoie(codeVoie);
		eo.setLibelleVoie(libelleVoie);
    return eo;
  }

  public static NSArray<EOTypeVoie> fetchAllTypeVoies(EOEditingContext editingContext) {
    return _EOTypeVoie.fetchAllTypeVoies(editingContext, null);
  }

  public static NSArray<EOTypeVoie> fetchAllTypeVoies(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeVoie.fetchTypeVoies(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeVoie> fetchTypeVoies(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeVoie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeVoie> eoObjects = (NSArray<EOTypeVoie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeVoie fetchTypeVoie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeVoie.fetchTypeVoie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeVoie fetchTypeVoie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeVoie> eoObjects = _EOTypeVoie.fetchTypeVoies(editingContext, qualifier, null);
    EOTypeVoie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeVoie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeVoie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeVoie fetchRequiredTypeVoie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeVoie.fetchRequiredTypeVoie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeVoie fetchRequiredTypeVoie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeVoie eoObject = _EOTypeVoie.fetchTypeVoie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeVoie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeVoie localInstanceIn(EOEditingContext editingContext, EOTypeVoie eo) {
    EOTypeVoie localInstance = (eo == null) ? null : (EOTypeVoie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
