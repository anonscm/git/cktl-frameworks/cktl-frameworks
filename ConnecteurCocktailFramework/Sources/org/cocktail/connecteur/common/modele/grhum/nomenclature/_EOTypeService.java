// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeService.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeService extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeService";

	// Attributes
	public static final String C_TYPE_SERVICE_KEY = "cTypeService";
	public static final String LC_TYPE_SERVICE_KEY = "lcTypeService";
	public static final String LL_TYPE_SERVICE_KEY = "llTypeService";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeService.class);

  public EOTypeService localInstanceIn(EOEditingContext editingContext) {
    EOTypeService localInstance = (EOTypeService)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeService() {
    return (String) storedValueForKey("cTypeService");
  }

  public void setCTypeService(String value) {
    if (_EOTypeService.LOG.isDebugEnabled()) {
    	_EOTypeService.LOG.debug( "updating cTypeService from " + cTypeService() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeService");
  }

  public String lcTypeService() {
    return (String) storedValueForKey("lcTypeService");
  }

  public void setLcTypeService(String value) {
    if (_EOTypeService.LOG.isDebugEnabled()) {
    	_EOTypeService.LOG.debug( "updating lcTypeService from " + lcTypeService() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeService");
  }

  public String llTypeService() {
    return (String) storedValueForKey("llTypeService");
  }

  public void setLlTypeService(String value) {
    if (_EOTypeService.LOG.isDebugEnabled()) {
    	_EOTypeService.LOG.debug( "updating llTypeService from " + llTypeService() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeService");
  }


  public static EOTypeService createTypeService(EOEditingContext editingContext, String cTypeService
, String lcTypeService
, String llTypeService
) {
    EOTypeService eo = (EOTypeService) EOUtilities.createAndInsertInstance(editingContext, _EOTypeService.ENTITY_NAME);    
		eo.setCTypeService(cTypeService);
		eo.setLcTypeService(lcTypeService);
		eo.setLlTypeService(llTypeService);
    return eo;
  }

  public static NSArray<EOTypeService> fetchAllTypeServices(EOEditingContext editingContext) {
    return _EOTypeService.fetchAllTypeServices(editingContext, null);
  }

  public static NSArray<EOTypeService> fetchAllTypeServices(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeService.fetchTypeServices(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeService> fetchTypeServices(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeService.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeService> eoObjects = (NSArray<EOTypeService>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeService fetchTypeService(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeService.fetchTypeService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeService fetchTypeService(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeService> eoObjects = _EOTypeService.fetchTypeServices(editingContext, qualifier, null);
    EOTypeService eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeService)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeService fetchRequiredTypeService(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeService.fetchRequiredTypeService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeService fetchRequiredTypeService(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeService eoObject = _EOTypeService.fetchTypeService(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeService localInstanceIn(EOEditingContext editingContext, EOTypeService eo) {
    EOTypeService localInstance = (eo == null) ? null : (EOTypeService)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
