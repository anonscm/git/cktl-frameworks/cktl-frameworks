// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOReferensEmplois.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOReferensEmplois extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ReferensEmplois";

	// Attributes
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String CODEMEN_KEY = "codemen";
	public static final String DEFINITION_KEY = "definition";
	public static final String DEFINTION_CLEAN_KEY = "defintionClean";
	public static final String INTITULEMPLOI_KEY = "intitulemploi";
	public static final String LETTREBAP_KEY = "lettrebap";
	public static final String NUMBAP_KEY = "numbap";
	public static final String NUMEMPLOI_KEY = "numemploi";
	public static final String OUVERTCONCOURS_KEY = "ouvertconcours";
	public static final String SIGLECORPS_KEY = "siglecorps";

	// Relationships
	public static final String TO_BAP_KEY = "toBap";

  private static Logger LOG = Logger.getLogger(_EOReferensEmplois.class);

  public EOReferensEmplois localInstanceIn(EOEditingContext editingContext) {
    EOReferensEmplois localInstance = (EOReferensEmplois)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeemploi() {
    return (String) storedValueForKey("codeemploi");
  }

  public void setCodeemploi(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating codeemploi from " + codeemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeemploi");
  }

  public String codemen() {
    return (String) storedValueForKey("codemen");
  }

  public void setCodemen(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating codemen from " + codemen() + " to " + value);
    }
    takeStoredValueForKey(value, "codemen");
  }

  public String definition() {
    return (String) storedValueForKey("definition");
  }

  public void setDefinition(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating definition from " + definition() + " to " + value);
    }
    takeStoredValueForKey(value, "definition");
  }

  public String defintionClean() {
    return (String) storedValueForKey("defintionClean");
  }

  public void setDefintionClean(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating defintionClean from " + defintionClean() + " to " + value);
    }
    takeStoredValueForKey(value, "defintionClean");
  }

  public String intitulemploi() {
    return (String) storedValueForKey("intitulemploi");
  }

  public void setIntitulemploi(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating intitulemploi from " + intitulemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "intitulemploi");
  }

  public String lettrebap() {
    return (String) storedValueForKey("lettrebap");
  }

  public void setLettrebap(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating lettrebap from " + lettrebap() + " to " + value);
    }
    takeStoredValueForKey(value, "lettrebap");
  }

  public String numbap() {
    return (String) storedValueForKey("numbap");
  }

  public void setNumbap(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating numbap from " + numbap() + " to " + value);
    }
    takeStoredValueForKey(value, "numbap");
  }

  public String numemploi() {
    return (String) storedValueForKey("numemploi");
  }

  public void setNumemploi(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating numemploi from " + numemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "numemploi");
  }

  public String ouvertconcours() {
    return (String) storedValueForKey("ouvertconcours");
  }

  public void setOuvertconcours(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating ouvertconcours from " + ouvertconcours() + " to " + value);
    }
    takeStoredValueForKey(value, "ouvertconcours");
  }

  public String siglecorps() {
    return (String) storedValueForKey("siglecorps");
  }

  public void setSiglecorps(String value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
    	_EOReferensEmplois.LOG.debug( "updating siglecorps from " + siglecorps() + " to " + value);
    }
    takeStoredValueForKey(value, "siglecorps");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap toBap() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap)storedValueForKey("toBap");
  }

  public void setToBapRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap value) {
    if (_EOReferensEmplois.LOG.isDebugEnabled()) {
      _EOReferensEmplois.LOG.debug("updating toBap from " + toBap() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap oldValue = toBap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBap");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toBap");
    }
  }
  

  public static EOReferensEmplois createReferensEmplois(EOEditingContext editingContext, String codeemploi
) {
    EOReferensEmplois eo = (EOReferensEmplois) EOUtilities.createAndInsertInstance(editingContext, _EOReferensEmplois.ENTITY_NAME);    
		eo.setCodeemploi(codeemploi);
    return eo;
  }

  public static NSArray<EOReferensEmplois> fetchAllReferensEmploises(EOEditingContext editingContext) {
    return _EOReferensEmplois.fetchAllReferensEmploises(editingContext, null);
  }

  public static NSArray<EOReferensEmplois> fetchAllReferensEmploises(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOReferensEmplois.fetchReferensEmploises(editingContext, null, sortOrderings);
  }

  public static NSArray<EOReferensEmplois> fetchReferensEmploises(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOReferensEmplois.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOReferensEmplois> eoObjects = (NSArray<EOReferensEmplois>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOReferensEmplois fetchReferensEmplois(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReferensEmplois.fetchReferensEmplois(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReferensEmplois fetchReferensEmplois(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOReferensEmplois> eoObjects = _EOReferensEmplois.fetchReferensEmploises(editingContext, qualifier, null);
    EOReferensEmplois eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOReferensEmplois)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ReferensEmplois that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReferensEmplois fetchRequiredReferensEmplois(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReferensEmplois.fetchRequiredReferensEmplois(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReferensEmplois fetchRequiredReferensEmplois(EOEditingContext editingContext, EOQualifier qualifier) {
    EOReferensEmplois eoObject = _EOReferensEmplois.fetchReferensEmplois(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ReferensEmplois that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReferensEmplois localInstanceIn(EOEditingContext editingContext, EOReferensEmplois eo) {
    EOReferensEmplois localInstance = (eo == null) ? null : (EOReferensEmplois)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
