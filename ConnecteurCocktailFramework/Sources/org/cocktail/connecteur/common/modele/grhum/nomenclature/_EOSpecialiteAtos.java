// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSpecialiteAtos.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSpecialiteAtos extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SpecialiteAtos";

	// Attributes
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_SPECIALITE_ATOS_KEY = "lcSpecialiteAtos";
	public static final String LL_SPECIALITE_ATOS_KEY = "llSpecialiteAtos";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOSpecialiteAtos.class);

  public EOSpecialiteAtos localInstanceIn(EOEditingContext editingContext) {
    EOSpecialiteAtos localInstance = (EOSpecialiteAtos)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cSpecialiteAtos() {
    return (String) storedValueForKey("cSpecialiteAtos");
  }

  public void setCSpecialiteAtos(String value) {
    if (_EOSpecialiteAtos.LOG.isDebugEnabled()) {
    	_EOSpecialiteAtos.LOG.debug( "updating cSpecialiteAtos from " + cSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteAtos");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOSpecialiteAtos.LOG.isDebugEnabled()) {
    	_EOSpecialiteAtos.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOSpecialiteAtos.LOG.isDebugEnabled()) {
    	_EOSpecialiteAtos.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcSpecialiteAtos() {
    return (String) storedValueForKey("lcSpecialiteAtos");
  }

  public void setLcSpecialiteAtos(String value) {
    if (_EOSpecialiteAtos.LOG.isDebugEnabled()) {
    	_EOSpecialiteAtos.LOG.debug( "updating lcSpecialiteAtos from " + lcSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "lcSpecialiteAtos");
  }

  public String llSpecialiteAtos() {
    return (String) storedValueForKey("llSpecialiteAtos");
  }

  public void setLlSpecialiteAtos(String value) {
    if (_EOSpecialiteAtos.LOG.isDebugEnabled()) {
    	_EOSpecialiteAtos.LOG.debug( "updating llSpecialiteAtos from " + llSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "llSpecialiteAtos");
  }


  public static EOSpecialiteAtos createSpecialiteAtos(EOEditingContext editingContext, String cSpecialiteAtos
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOSpecialiteAtos eo = (EOSpecialiteAtos) EOUtilities.createAndInsertInstance(editingContext, _EOSpecialiteAtos.ENTITY_NAME);    
		eo.setCSpecialiteAtos(cSpecialiteAtos);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOSpecialiteAtos> fetchAllSpecialiteAtoses(EOEditingContext editingContext) {
    return _EOSpecialiteAtos.fetchAllSpecialiteAtoses(editingContext, null);
  }

  public static NSArray<EOSpecialiteAtos> fetchAllSpecialiteAtoses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSpecialiteAtos.fetchSpecialiteAtoses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSpecialiteAtos> fetchSpecialiteAtoses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSpecialiteAtos.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSpecialiteAtos> eoObjects = (NSArray<EOSpecialiteAtos>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSpecialiteAtos fetchSpecialiteAtos(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSpecialiteAtos.fetchSpecialiteAtos(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSpecialiteAtos fetchSpecialiteAtos(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSpecialiteAtos> eoObjects = _EOSpecialiteAtos.fetchSpecialiteAtoses(editingContext, qualifier, null);
    EOSpecialiteAtos eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSpecialiteAtos)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one SpecialiteAtos that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSpecialiteAtos fetchRequiredSpecialiteAtos(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSpecialiteAtos.fetchRequiredSpecialiteAtos(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSpecialiteAtos fetchRequiredSpecialiteAtos(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSpecialiteAtos eoObject = _EOSpecialiteAtos.fetchSpecialiteAtos(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no SpecialiteAtos that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSpecialiteAtos localInstanceIn(EOEditingContext editingContext, EOSpecialiteAtos eo) {
    EOSpecialiteAtos localInstance = (eo == null) ? null : (EOSpecialiteAtos)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
