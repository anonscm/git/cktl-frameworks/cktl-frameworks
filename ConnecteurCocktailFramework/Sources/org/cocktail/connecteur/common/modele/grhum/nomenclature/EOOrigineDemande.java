package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;

public class EOOrigineDemande extends _EOOrigineDemande {
	private static Logger log = Logger.getLogger(EOOrigineDemande.class);

	public static EOOrigineDemande rechercherPourCOrigineDemande(EOEditingContext editingContext, String cOrigineDemande) {
    	if (cOrigineDemande == null) {
    		return null;
    	}
    	return EOOrigineDemande.fetchOrigineDemande(editingContext, C_ORIGINE_DEMANDE_KEY, cOrigineDemande);
	}
}
