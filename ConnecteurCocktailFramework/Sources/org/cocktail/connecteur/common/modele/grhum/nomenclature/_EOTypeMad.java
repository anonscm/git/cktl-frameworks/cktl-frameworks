// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeMad.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeMad extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeMad";

	// Attributes
	public static final String C_TYPE_MAD_KEY = "cTypeMad";
	public static final String C_TYPE_MAD_ONP_KEY = "cTypeMadOnp";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_TYPE_MAD_KEY = "lcTypeMad";
	public static final String LL_TYPE_MAD_KEY = "llTypeMad";
	public static final String TEM_MAD_ORG_SYNDICALE_KEY = "TemMadOrgSyndicale";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeMad.class);

  public EOTypeMad localInstanceIn(EOEditingContext editingContext) {
    EOTypeMad localInstance = (EOTypeMad)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeMad() {
    return (String) storedValueForKey("cTypeMad");
  }

  public void setCTypeMad(String value) {
    if (_EOTypeMad.LOG.isDebugEnabled()) {
    	_EOTypeMad.LOG.debug( "updating cTypeMad from " + cTypeMad() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeMad");
  }

  public String cTypeMadOnp() {
    return (String) storedValueForKey("cTypeMadOnp");
  }

  public void setCTypeMadOnp(String value) {
    if (_EOTypeMad.LOG.isDebugEnabled()) {
    	_EOTypeMad.LOG.debug( "updating cTypeMadOnp from " + cTypeMadOnp() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeMadOnp");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeMad.LOG.isDebugEnabled()) {
    	_EOTypeMad.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeMad.LOG.isDebugEnabled()) {
    	_EOTypeMad.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcTypeMad() {
    return (String) storedValueForKey("lcTypeMad");
  }

  public void setLcTypeMad(String value) {
    if (_EOTypeMad.LOG.isDebugEnabled()) {
    	_EOTypeMad.LOG.debug( "updating lcTypeMad from " + lcTypeMad() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeMad");
  }

  public String llTypeMad() {
    return (String) storedValueForKey("llTypeMad");
  }

  public void setLlTypeMad(String value) {
    if (_EOTypeMad.LOG.isDebugEnabled()) {
    	_EOTypeMad.LOG.debug( "updating llTypeMad from " + llTypeMad() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeMad");
  }

  public String TemMadOrgSyndicale() {
    return (String) storedValueForKey("TemMadOrgSyndicale");
  }

  public void setTemMadOrgSyndicale(String value) {
    if (_EOTypeMad.LOG.isDebugEnabled()) {
    	_EOTypeMad.LOG.debug( "updating TemMadOrgSyndicale from " + TemMadOrgSyndicale() + " to " + value);
    }
    takeStoredValueForKey(value, "TemMadOrgSyndicale");
  }


  public static EOTypeMad createTypeMad(EOEditingContext editingContext, String cTypeMad
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOTypeMad eo = (EOTypeMad) EOUtilities.createAndInsertInstance(editingContext, _EOTypeMad.ENTITY_NAME);    
		eo.setCTypeMad(cTypeMad);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOTypeMad> fetchAllTypeMads(EOEditingContext editingContext) {
    return _EOTypeMad.fetchAllTypeMads(editingContext, null);
  }

  public static NSArray<EOTypeMad> fetchAllTypeMads(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeMad.fetchTypeMads(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeMad> fetchTypeMads(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeMad.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeMad> eoObjects = (NSArray<EOTypeMad>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeMad fetchTypeMad(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMad.fetchTypeMad(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMad fetchTypeMad(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeMad> eoObjects = _EOTypeMad.fetchTypeMads(editingContext, qualifier, null);
    EOTypeMad eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeMad)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeMad that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMad fetchRequiredTypeMad(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMad.fetchRequiredTypeMad(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMad fetchRequiredTypeMad(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeMad eoObject = _EOTypeMad.fetchTypeMad(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeMad that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMad localInstanceIn(EOEditingContext editingContext, EOTypeMad eo) {
    EOTypeMad localInstance = (eo == null) ? null : (EOTypeMad)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
