// $LastChangedRevision$ DO NOT EDIT.  Make changes to EONatureLesions.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EONatureLesions extends  EOGenericRecord {
	public static final String ENTITY_NAME = "NatureLesions";

	// Attributes
	public static final String NLES_LIBELLE_KEY = "nlesLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EONatureLesions.class);

  public EONatureLesions localInstanceIn(EOEditingContext editingContext) {
    EONatureLesions localInstance = (EONatureLesions)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String nlesLibelle() {
    return (String) storedValueForKey("nlesLibelle");
  }

  public void setNlesLibelle(String value) {
    if (_EONatureLesions.LOG.isDebugEnabled()) {
    	_EONatureLesions.LOG.debug( "updating nlesLibelle from " + nlesLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "nlesLibelle");
  }


  public static EONatureLesions createNatureLesions(EOEditingContext editingContext) {
    EONatureLesions eo = (EONatureLesions) EOUtilities.createAndInsertInstance(editingContext, _EONatureLesions.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EONatureLesions> fetchAllNatureLesionses(EOEditingContext editingContext) {
    return _EONatureLesions.fetchAllNatureLesionses(editingContext, null);
  }

  public static NSArray<EONatureLesions> fetchAllNatureLesionses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONatureLesions.fetchNatureLesionses(editingContext, null, sortOrderings);
  }

  public static NSArray<EONatureLesions> fetchNatureLesionses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EONatureLesions.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONatureLesions> eoObjects = (NSArray<EONatureLesions>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EONatureLesions fetchNatureLesions(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureLesions.fetchNatureLesions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureLesions fetchNatureLesions(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONatureLesions> eoObjects = _EONatureLesions.fetchNatureLesionses(editingContext, qualifier, null);
    EONatureLesions eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EONatureLesions)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NatureLesions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureLesions fetchRequiredNatureLesions(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureLesions.fetchRequiredNatureLesions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureLesions fetchRequiredNatureLesions(EOEditingContext editingContext, EOQualifier qualifier) {
    EONatureLesions eoObject = _EONatureLesions.fetchNatureLesions(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NatureLesions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureLesions localInstanceIn(EOEditingContext editingContext, EONatureLesions eo) {
    EONatureLesions localInstance = (eo == null) ? null : (EONatureLesions)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
