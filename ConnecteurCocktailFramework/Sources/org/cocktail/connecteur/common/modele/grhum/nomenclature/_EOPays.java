// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPays.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPays extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Pays";

	// Attributes
	public static final String C_PAYS_KEY = "cPays";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_PAYS_KEY = "lcPays";
	public static final String LL_PAYS_KEY = "llPays";
	public static final String L_NATIONALITE_KEY = "lNationalite";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOPays.class);

  public EOPays localInstanceIn(EOEditingContext editingContext) {
    EOPays localInstance = (EOPays)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cPays() {
    return (String) storedValueForKey("cPays");
  }

  public void setCPays(String value) {
    if (_EOPays.LOG.isDebugEnabled()) {
    	_EOPays.LOG.debug( "updating cPays from " + cPays() + " to " + value);
    }
    takeStoredValueForKey(value, "cPays");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPays.LOG.isDebugEnabled()) {
    	_EOPays.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPays.LOG.isDebugEnabled()) {
    	_EOPays.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcPays() {
    return (String) storedValueForKey("lcPays");
  }

  public void setLcPays(String value) {
    if (_EOPays.LOG.isDebugEnabled()) {
    	_EOPays.LOG.debug( "updating lcPays from " + lcPays() + " to " + value);
    }
    takeStoredValueForKey(value, "lcPays");
  }

  public String llPays() {
    return (String) storedValueForKey("llPays");
  }

  public void setLlPays(String value) {
    if (_EOPays.LOG.isDebugEnabled()) {
    	_EOPays.LOG.debug( "updating llPays from " + llPays() + " to " + value);
    }
    takeStoredValueForKey(value, "llPays");
  }

  public String lNationalite() {
    return (String) storedValueForKey("lNationalite");
  }

  public void setLNationalite(String value) {
    if (_EOPays.LOG.isDebugEnabled()) {
    	_EOPays.LOG.debug( "updating lNationalite from " + lNationalite() + " to " + value);
    }
    takeStoredValueForKey(value, "lNationalite");
  }


  public static EOPays createPays(EOEditingContext editingContext, String cPays
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOPays eo = (EOPays) EOUtilities.createAndInsertInstance(editingContext, _EOPays.ENTITY_NAME);    
		eo.setCPays(cPays);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOPays> fetchAllPayses(EOEditingContext editingContext) {
    return _EOPays.fetchAllPayses(editingContext, null);
  }

  public static NSArray<EOPays> fetchAllPayses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPays.fetchPayses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPays> fetchPayses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPays.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPays> eoObjects = (NSArray<EOPays>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPays fetchPays(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPays.fetchPays(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPays fetchPays(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPays> eoObjects = _EOPays.fetchPayses(editingContext, qualifier, null);
    EOPays eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPays)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Pays that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPays fetchRequiredPays(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPays.fetchRequiredPays(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPays fetchRequiredPays(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPays eoObject = _EOPays.fetchPays(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Pays that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPays localInstanceIn(EOEditingContext editingContext, EOPays eo) {
    EOPays localInstance = (eo == null) ? null : (EOPays)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
