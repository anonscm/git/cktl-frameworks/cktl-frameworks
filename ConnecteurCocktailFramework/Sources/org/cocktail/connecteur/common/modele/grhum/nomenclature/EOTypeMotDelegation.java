package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOTypeMotDelegation extends _EOTypeMotDelegation {
	public static EOTypeMotDelegation rechercherPourCModDelegation(EOEditingContext editingContext, String cMotDelegation) {
		return fetchTypeMotDelegation(editingContext, C_MOT_DELEGATION_KEY, cMotDelegation);
	}
}
