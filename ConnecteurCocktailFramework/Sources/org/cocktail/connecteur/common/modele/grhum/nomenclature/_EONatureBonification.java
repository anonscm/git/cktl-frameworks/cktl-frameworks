// $LastChangedRevision$ DO NOT EDIT.  Make changes to EONatureBonification.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EONatureBonification extends  EOGenericRecord {
	public static final String ENTITY_NAME = "NatureBonification";

	// Attributes
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String NBO_CODE_KEY = "nboCode";
	public static final String NBO_LIBELLE_COURT_KEY = "nboLibelleCourt";
	public static final String NBO_LIBELLE_LONG_KEY = "nboLibelleLong";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EONatureBonification.class);

  public EONatureBonification localInstanceIn(EOEditingContext editingContext) {
    EONatureBonification localInstance = (EONatureBonification)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey("dateFermeture");
  }

  public void setDateFermeture(NSTimestamp value) {
    if (_EONatureBonification.LOG.isDebugEnabled()) {
    	_EONatureBonification.LOG.debug( "updating dateFermeture from " + dateFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFermeture");
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey("dateOuverture");
  }

  public void setDateOuverture(NSTimestamp value) {
    if (_EONatureBonification.LOG.isDebugEnabled()) {
    	_EONatureBonification.LOG.debug( "updating dateOuverture from " + dateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateOuverture");
  }

  public String nboCode() {
    return (String) storedValueForKey("nboCode");
  }

  public void setNboCode(String value) {
    if (_EONatureBonification.LOG.isDebugEnabled()) {
    	_EONatureBonification.LOG.debug( "updating nboCode from " + nboCode() + " to " + value);
    }
    takeStoredValueForKey(value, "nboCode");
  }

  public String nboLibelleCourt() {
    return (String) storedValueForKey("nboLibelleCourt");
  }

  public void setNboLibelleCourt(String value) {
    if (_EONatureBonification.LOG.isDebugEnabled()) {
    	_EONatureBonification.LOG.debug( "updating nboLibelleCourt from " + nboLibelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, "nboLibelleCourt");
  }

  public String nboLibelleLong() {
    return (String) storedValueForKey("nboLibelleLong");
  }

  public void setNboLibelleLong(String value) {
    if (_EONatureBonification.LOG.isDebugEnabled()) {
    	_EONatureBonification.LOG.debug( "updating nboLibelleLong from " + nboLibelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, "nboLibelleLong");
  }


  public static EONatureBonification createNatureBonification(EOEditingContext editingContext) {
    EONatureBonification eo = (EONatureBonification) EOUtilities.createAndInsertInstance(editingContext, _EONatureBonification.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EONatureBonification> fetchAllNatureBonifications(EOEditingContext editingContext) {
    return _EONatureBonification.fetchAllNatureBonifications(editingContext, null);
  }

  public static NSArray<EONatureBonification> fetchAllNatureBonifications(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONatureBonification.fetchNatureBonifications(editingContext, null, sortOrderings);
  }

  public static NSArray<EONatureBonification> fetchNatureBonifications(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EONatureBonification.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONatureBonification> eoObjects = (NSArray<EONatureBonification>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EONatureBonification fetchNatureBonification(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBonification.fetchNatureBonification(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBonification fetchNatureBonification(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONatureBonification> eoObjects = _EONatureBonification.fetchNatureBonifications(editingContext, qualifier, null);
    EONatureBonification eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EONatureBonification)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NatureBonification that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBonification fetchRequiredNatureBonification(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureBonification.fetchRequiredNatureBonification(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureBonification fetchRequiredNatureBonification(EOEditingContext editingContext, EOQualifier qualifier) {
    EONatureBonification eoObject = _EONatureBonification.fetchNatureBonification(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NatureBonification that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureBonification localInstanceIn(EOEditingContext editingContext, EONatureBonification eo) {
    EONatureBonification localInstance = (eo == null) ? null : (EONatureBonification)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
