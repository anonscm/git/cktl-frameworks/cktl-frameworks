package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOTypeModDelegation extends _EOTypeModDelegation {

	public static EOTypeModDelegation rechercherPourCModDelegation(EOEditingContext editingContext, String cModDelegation) {
		return fetchTypeModDelegation(editingContext, C_MOD_DELEGATION_KEY, cModDelegation);
	}

}
