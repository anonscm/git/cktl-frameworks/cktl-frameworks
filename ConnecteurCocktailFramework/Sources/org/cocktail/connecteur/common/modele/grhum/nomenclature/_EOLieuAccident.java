// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOLieuAccident.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLieuAccident extends  EOGenericRecord {
	public static final String ENTITY_NAME = "LieuAccident";

	// Attributes
	public static final String LACC_LIBELLE_KEY = "laccLibelle";
	public static final String LACC_ORDRE_KEY = "laccOrdre";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOLieuAccident.class);

  public EOLieuAccident localInstanceIn(EOEditingContext editingContext) {
    EOLieuAccident localInstance = (EOLieuAccident)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String laccLibelle() {
    return (String) storedValueForKey("laccLibelle");
  }

  public void setLaccLibelle(String value) {
    if (_EOLieuAccident.LOG.isDebugEnabled()) {
    	_EOLieuAccident.LOG.debug( "updating laccLibelle from " + laccLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "laccLibelle");
  }

  public Integer laccOrdre() {
    return (Integer) storedValueForKey("laccOrdre");
  }

  public void setLaccOrdre(Integer value) {
    if (_EOLieuAccident.LOG.isDebugEnabled()) {
    	_EOLieuAccident.LOG.debug( "updating laccOrdre from " + laccOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "laccOrdre");
  }


  public static EOLieuAccident createLieuAccident(EOEditingContext editingContext, Integer laccOrdre
) {
    EOLieuAccident eo = (EOLieuAccident) EOUtilities.createAndInsertInstance(editingContext, _EOLieuAccident.ENTITY_NAME);    
		eo.setLaccOrdre(laccOrdre);
    return eo;
  }

  public static NSArray<EOLieuAccident> fetchAllLieuAccidents(EOEditingContext editingContext) {
    return _EOLieuAccident.fetchAllLieuAccidents(editingContext, null);
  }

  public static NSArray<EOLieuAccident> fetchAllLieuAccidents(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLieuAccident.fetchLieuAccidents(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLieuAccident> fetchLieuAccidents(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLieuAccident.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLieuAccident> eoObjects = (NSArray<EOLieuAccident>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLieuAccident fetchLieuAccident(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLieuAccident.fetchLieuAccident(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLieuAccident fetchLieuAccident(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLieuAccident> eoObjects = _EOLieuAccident.fetchLieuAccidents(editingContext, qualifier, null);
    EOLieuAccident eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLieuAccident)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one LieuAccident that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLieuAccident fetchRequiredLieuAccident(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLieuAccident.fetchRequiredLieuAccident(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLieuAccident fetchRequiredLieuAccident(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLieuAccident eoObject = _EOLieuAccident.fetchLieuAccident(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no LieuAccident that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLieuAccident localInstanceIn(EOEditingContext editingContext, EOLieuAccident eo) {
    EOLieuAccident localInstance = (eo == null) ? null : (EOLieuAccident)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
