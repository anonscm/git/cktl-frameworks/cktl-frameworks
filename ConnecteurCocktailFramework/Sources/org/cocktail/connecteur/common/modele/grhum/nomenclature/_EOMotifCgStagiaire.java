// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMotifCgStagiaire.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMotifCgStagiaire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MotifCgStagiaire";

	// Attributes
	public static final String C_CIR_KEY = "cCir";
	public static final String C_MOTIF_CG_RFP_KEY = "cMotifCgRfp";
	public static final String LL_MOTIF_CG_STAGIAIRE_KEY = "llMotifCgStagiaire";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMotifCgStagiaire.class);

  public EOMotifCgStagiaire localInstanceIn(EOEditingContext editingContext) {
    EOMotifCgStagiaire localInstance = (EOMotifCgStagiaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCir() {
    return (String) storedValueForKey("cCir");
  }

  public void setCCir(String value) {
    if (_EOMotifCgStagiaire.LOG.isDebugEnabled()) {
    	_EOMotifCgStagiaire.LOG.debug( "updating cCir from " + cCir() + " to " + value);
    }
    takeStoredValueForKey(value, "cCir");
  }

  public String cMotifCgRfp() {
    return (String) storedValueForKey("cMotifCgRfp");
  }

  public void setCMotifCgRfp(String value) {
    if (_EOMotifCgStagiaire.LOG.isDebugEnabled()) {
    	_EOMotifCgStagiaire.LOG.debug( "updating cMotifCgRfp from " + cMotifCgRfp() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifCgRfp");
  }

  public String llMotifCgStagiaire() {
    return (String) storedValueForKey("llMotifCgStagiaire");
  }

  public void setLlMotifCgStagiaire(String value) {
    if (_EOMotifCgStagiaire.LOG.isDebugEnabled()) {
    	_EOMotifCgStagiaire.LOG.debug( "updating llMotifCgStagiaire from " + llMotifCgStagiaire() + " to " + value);
    }
    takeStoredValueForKey(value, "llMotifCgStagiaire");
  }


  public static EOMotifCgStagiaire createMotifCgStagiaire(EOEditingContext editingContext, String cMotifCgRfp
) {
    EOMotifCgStagiaire eo = (EOMotifCgStagiaire) EOUtilities.createAndInsertInstance(editingContext, _EOMotifCgStagiaire.ENTITY_NAME);    
		eo.setCMotifCgRfp(cMotifCgRfp);
    return eo;
  }

  public static NSArray<EOMotifCgStagiaire> fetchAllMotifCgStagiaires(EOEditingContext editingContext) {
    return _EOMotifCgStagiaire.fetchAllMotifCgStagiaires(editingContext, null);
  }

  public static NSArray<EOMotifCgStagiaire> fetchAllMotifCgStagiaires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMotifCgStagiaire.fetchMotifCgStagiaires(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMotifCgStagiaire> fetchMotifCgStagiaires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMotifCgStagiaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMotifCgStagiaire> eoObjects = (NSArray<EOMotifCgStagiaire>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMotifCgStagiaire fetchMotifCgStagiaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifCgStagiaire.fetchMotifCgStagiaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifCgStagiaire fetchMotifCgStagiaire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMotifCgStagiaire> eoObjects = _EOMotifCgStagiaire.fetchMotifCgStagiaires(editingContext, qualifier, null);
    EOMotifCgStagiaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMotifCgStagiaire)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MotifCgStagiaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifCgStagiaire fetchRequiredMotifCgStagiaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifCgStagiaire.fetchRequiredMotifCgStagiaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifCgStagiaire fetchRequiredMotifCgStagiaire(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMotifCgStagiaire eoObject = _EOMotifCgStagiaire.fetchMotifCgStagiaire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MotifCgStagiaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifCgStagiaire localInstanceIn(EOEditingContext editingContext, EOMotifCgStagiaire eo) {
    EOMotifCgStagiaire localInstance = (eo == null) ? null : (EOMotifCgStagiaire)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
