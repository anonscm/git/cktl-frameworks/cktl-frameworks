// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOProgrammeTitre.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOProgrammeTitre extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ProgrammeTitre";

	// Attributes
	public static final String C_TITRE_KEY = "cTitre";
	public static final String LL_TITRE_KEY = "llTitre";
	public static final String TEM_BUDGET_ETAT_KEY = "temBudgetEtat";
	public static final String TEM_EMPLOI_GAGE_KEY = "temEmploiGage";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOProgrammeTitre.class);

  public EOProgrammeTitre localInstanceIn(EOEditingContext editingContext) {
    EOProgrammeTitre localInstance = (EOProgrammeTitre)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTitre() {
    return (String) storedValueForKey("cTitre");
  }

  public void setCTitre(String value) {
    if (_EOProgrammeTitre.LOG.isDebugEnabled()) {
    	_EOProgrammeTitre.LOG.debug( "updating cTitre from " + cTitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cTitre");
  }

  public String llTitre() {
    return (String) storedValueForKey("llTitre");
  }

  public void setLlTitre(String value) {
    if (_EOProgrammeTitre.LOG.isDebugEnabled()) {
    	_EOProgrammeTitre.LOG.debug( "updating llTitre from " + llTitre() + " to " + value);
    }
    takeStoredValueForKey(value, "llTitre");
  }

  public String temBudgetEtat() {
    return (String) storedValueForKey("temBudgetEtat");
  }

  public void setTemBudgetEtat(String value) {
    if (_EOProgrammeTitre.LOG.isDebugEnabled()) {
    	_EOProgrammeTitre.LOG.debug( "updating temBudgetEtat from " + temBudgetEtat() + " to " + value);
    }
    takeStoredValueForKey(value, "temBudgetEtat");
  }

  public String temEmploiGage() {
    return (String) storedValueForKey("temEmploiGage");
  }

  public void setTemEmploiGage(String value) {
    if (_EOProgrammeTitre.LOG.isDebugEnabled()) {
    	_EOProgrammeTitre.LOG.debug( "updating temEmploiGage from " + temEmploiGage() + " to " + value);
    }
    takeStoredValueForKey(value, "temEmploiGage");
  }


  public static EOProgrammeTitre createProgrammeTitre(EOEditingContext editingContext, String cTitre
, String temBudgetEtat
) {
    EOProgrammeTitre eo = (EOProgrammeTitre) EOUtilities.createAndInsertInstance(editingContext, _EOProgrammeTitre.ENTITY_NAME);    
		eo.setCTitre(cTitre);
		eo.setTemBudgetEtat(temBudgetEtat);
    return eo;
  }

  public static NSArray<EOProgrammeTitre> fetchAllProgrammeTitres(EOEditingContext editingContext) {
    return _EOProgrammeTitre.fetchAllProgrammeTitres(editingContext, null);
  }

  public static NSArray<EOProgrammeTitre> fetchAllProgrammeTitres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOProgrammeTitre.fetchProgrammeTitres(editingContext, null, sortOrderings);
  }

  public static NSArray<EOProgrammeTitre> fetchProgrammeTitres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOProgrammeTitre.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOProgrammeTitre> eoObjects = (NSArray<EOProgrammeTitre>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOProgrammeTitre fetchProgrammeTitre(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProgrammeTitre.fetchProgrammeTitre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProgrammeTitre fetchProgrammeTitre(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOProgrammeTitre> eoObjects = _EOProgrammeTitre.fetchProgrammeTitres(editingContext, qualifier, null);
    EOProgrammeTitre eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOProgrammeTitre)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ProgrammeTitre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProgrammeTitre fetchRequiredProgrammeTitre(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProgrammeTitre.fetchRequiredProgrammeTitre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProgrammeTitre fetchRequiredProgrammeTitre(EOEditingContext editingContext, EOQualifier qualifier) {
    EOProgrammeTitre eoObject = _EOProgrammeTitre.fetchProgrammeTitre(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ProgrammeTitre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProgrammeTitre localInstanceIn(EOEditingContext editingContext, EOProgrammeTitre eo) {
    EOProgrammeTitre localInstance = (eo == null) ? null : (EOProgrammeTitre)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
