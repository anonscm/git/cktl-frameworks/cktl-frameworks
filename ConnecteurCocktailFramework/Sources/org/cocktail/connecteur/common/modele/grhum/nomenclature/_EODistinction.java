// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODistinction.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODistinction extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Distinction";

	// Attributes
	public static final String C_DIST_KEY = "cDist";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LL_DIST_KEY = "llDist";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EODistinction.class);

  public EODistinction localInstanceIn(EOEditingContext editingContext) {
    EODistinction localInstance = (EODistinction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDist() {
    return (String) storedValueForKey("cDist");
  }

  public void setCDist(String value) {
    if (_EODistinction.LOG.isDebugEnabled()) {
    	_EODistinction.LOG.debug( "updating cDist from " + cDist() + " to " + value);
    }
    takeStoredValueForKey(value, "cDist");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODistinction.LOG.isDebugEnabled()) {
    	_EODistinction.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODistinction.LOG.isDebugEnabled()) {
    	_EODistinction.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String llDist() {
    return (String) storedValueForKey("llDist");
  }

  public void setLlDist(String value) {
    if (_EODistinction.LOG.isDebugEnabled()) {
    	_EODistinction.LOG.debug( "updating llDist from " + llDist() + " to " + value);
    }
    takeStoredValueForKey(value, "llDist");
  }


  public static EODistinction createDistinction(EOEditingContext editingContext, String cDist
, NSTimestamp dCreation
, NSTimestamp dModification
, String llDist
) {
    EODistinction eo = (EODistinction) EOUtilities.createAndInsertInstance(editingContext, _EODistinction.ENTITY_NAME);    
		eo.setCDist(cDist);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLlDist(llDist);
    return eo;
  }

  public static NSArray<EODistinction> fetchAllDistinctions(EOEditingContext editingContext) {
    return _EODistinction.fetchAllDistinctions(editingContext, null);
  }

  public static NSArray<EODistinction> fetchAllDistinctions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODistinction.fetchDistinctions(editingContext, null, sortOrderings);
  }

  public static NSArray<EODistinction> fetchDistinctions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODistinction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODistinction> eoObjects = (NSArray<EODistinction>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODistinction fetchDistinction(EOEditingContext editingContext, String keyName, Object value) {
    return _EODistinction.fetchDistinction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODistinction fetchDistinction(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODistinction> eoObjects = _EODistinction.fetchDistinctions(editingContext, qualifier, null);
    EODistinction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODistinction)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Distinction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODistinction fetchRequiredDistinction(EOEditingContext editingContext, String keyName, Object value) {
    return _EODistinction.fetchRequiredDistinction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODistinction fetchRequiredDistinction(EOEditingContext editingContext, EOQualifier qualifier) {
    EODistinction eoObject = _EODistinction.fetchDistinction(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Distinction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODistinction localInstanceIn(EOEditingContext editingContext, EODistinction eo) {
    EODistinction localInstance = (eo == null) ? null : (EODistinction)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
