// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeMotProlongation.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeMotProlongation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeMotProlongation";

	// Attributes
	public static final String C_TYPE_MOT_PROLONGATION_KEY = "cTypeMotProlongation";
	public static final String C_TYPE_MOT_PROLONGATION_ONP_KEY = "cTypeMotProlongationOnp";
	public static final String LC_TYPE_MOT_PROLONGATION_KEY = "lcTypeMotProlongation";
	public static final String LL_TYPE_MOT_PROLONGATION_KEY = "llTypeMotProlongation";
	public static final String REF_MOT_PROLONGATION_KEY = "refMotProlongation";
	public static final String TYPE_MOTIF_KEY = "typeMotif";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeMotProlongation.class);

  public EOTypeMotProlongation localInstanceIn(EOEditingContext editingContext) {
    EOTypeMotProlongation localInstance = (EOTypeMotProlongation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeMotProlongation() {
    return (String) storedValueForKey("cTypeMotProlongation");
  }

  public void setCTypeMotProlongation(String value) {
    if (_EOTypeMotProlongation.LOG.isDebugEnabled()) {
    	_EOTypeMotProlongation.LOG.debug( "updating cTypeMotProlongation from " + cTypeMotProlongation() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeMotProlongation");
  }

  public String cTypeMotProlongationOnp() {
    return (String) storedValueForKey("cTypeMotProlongationOnp");
  }

  public void setCTypeMotProlongationOnp(String value) {
    if (_EOTypeMotProlongation.LOG.isDebugEnabled()) {
    	_EOTypeMotProlongation.LOG.debug( "updating cTypeMotProlongationOnp from " + cTypeMotProlongationOnp() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeMotProlongationOnp");
  }

  public String lcTypeMotProlongation() {
    return (String) storedValueForKey("lcTypeMotProlongation");
  }

  public void setLcTypeMotProlongation(String value) {
    if (_EOTypeMotProlongation.LOG.isDebugEnabled()) {
    	_EOTypeMotProlongation.LOG.debug( "updating lcTypeMotProlongation from " + lcTypeMotProlongation() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeMotProlongation");
  }

  public String llTypeMotProlongation() {
    return (String) storedValueForKey("llTypeMotProlongation");
  }

  public void setLlTypeMotProlongation(String value) {
    if (_EOTypeMotProlongation.LOG.isDebugEnabled()) {
    	_EOTypeMotProlongation.LOG.debug( "updating llTypeMotProlongation from " + llTypeMotProlongation() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeMotProlongation");
  }

  public String refMotProlongation() {
    return (String) storedValueForKey("refMotProlongation");
  }

  public void setRefMotProlongation(String value) {
    if (_EOTypeMotProlongation.LOG.isDebugEnabled()) {
    	_EOTypeMotProlongation.LOG.debug( "updating refMotProlongation from " + refMotProlongation() + " to " + value);
    }
    takeStoredValueForKey(value, "refMotProlongation");
  }

  public String typeMotif() {
    return (String) storedValueForKey("typeMotif");
  }

  public void setTypeMotif(String value) {
    if (_EOTypeMotProlongation.LOG.isDebugEnabled()) {
    	_EOTypeMotProlongation.LOG.debug( "updating typeMotif from " + typeMotif() + " to " + value);
    }
    takeStoredValueForKey(value, "typeMotif");
  }


  public static EOTypeMotProlongation createTypeMotProlongation(EOEditingContext editingContext, String cTypeMotProlongation
, String typeMotif
) {
    EOTypeMotProlongation eo = (EOTypeMotProlongation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeMotProlongation.ENTITY_NAME);    
		eo.setCTypeMotProlongation(cTypeMotProlongation);
		eo.setTypeMotif(typeMotif);
    return eo;
  }

  public static NSArray<EOTypeMotProlongation> fetchAllTypeMotProlongations(EOEditingContext editingContext) {
    return _EOTypeMotProlongation.fetchAllTypeMotProlongations(editingContext, null);
  }

  public static NSArray<EOTypeMotProlongation> fetchAllTypeMotProlongations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeMotProlongation.fetchTypeMotProlongations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeMotProlongation> fetchTypeMotProlongations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeMotProlongation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeMotProlongation> eoObjects = (NSArray<EOTypeMotProlongation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeMotProlongation fetchTypeMotProlongation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMotProlongation.fetchTypeMotProlongation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMotProlongation fetchTypeMotProlongation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeMotProlongation> eoObjects = _EOTypeMotProlongation.fetchTypeMotProlongations(editingContext, qualifier, null);
    EOTypeMotProlongation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeMotProlongation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeMotProlongation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMotProlongation fetchRequiredTypeMotProlongation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMotProlongation.fetchRequiredTypeMotProlongation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMotProlongation fetchRequiredTypeMotProlongation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeMotProlongation eoObject = _EOTypeMotProlongation.fetchTypeMotProlongation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeMotProlongation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMotProlongation localInstanceIn(EOEditingContext editingContext, EOTypeMotProlongation eo) {
    EOTypeMotProlongation localInstance = (eo == null) ? null : (EOTypeMotProlongation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
