package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOTypeDechargeService extends _EOTypeDechargeService {
	public static EOTypeDechargeService getFromCode(EOEditingContext editingContext, String code) {
		return fetchTypeDechargeService(editingContext, C_TYPE_DECHARGE_KEY, code);
	}
}
