package org.cocktail.connecteur.common.modele.grhum.nomenclature;

public class EOTypeMotProlongation extends _EOTypeMotProlongation {
	public static final String MOTIF_RECUL_AGE = "R";
	public static final String MOTIF_PROLONGATION = "P";
}
