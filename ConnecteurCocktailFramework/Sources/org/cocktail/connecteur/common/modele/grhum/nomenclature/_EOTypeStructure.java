// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeStructure.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeStructure extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeStructure";

	// Attributes
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String L_TYPE_STRUCTURE_KEY = "lTypeStructure";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeStructure.class);

  public EOTypeStructure localInstanceIn(EOEditingContext editingContext) {
    EOTypeStructure localInstance = (EOTypeStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeStructure() {
    return (String) storedValueForKey("cTypeStructure");
  }

  public void setCTypeStructure(String value) {
    if (_EOTypeStructure.LOG.isDebugEnabled()) {
    	_EOTypeStructure.LOG.debug( "updating cTypeStructure from " + cTypeStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeStructure");
  }

  public String lTypeStructure() {
    return (String) storedValueForKey("lTypeStructure");
  }

  public void setLTypeStructure(String value) {
    if (_EOTypeStructure.LOG.isDebugEnabled()) {
    	_EOTypeStructure.LOG.debug( "updating lTypeStructure from " + lTypeStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "lTypeStructure");
  }


  public static EOTypeStructure createTypeStructure(EOEditingContext editingContext, String cTypeStructure
) {
    EOTypeStructure eo = (EOTypeStructure) EOUtilities.createAndInsertInstance(editingContext, _EOTypeStructure.ENTITY_NAME);    
		eo.setCTypeStructure(cTypeStructure);
    return eo;
  }

  public static NSArray<EOTypeStructure> fetchAllTypeStructures(EOEditingContext editingContext) {
    return _EOTypeStructure.fetchAllTypeStructures(editingContext, null);
  }

  public static NSArray<EOTypeStructure> fetchAllTypeStructures(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeStructure.fetchTypeStructures(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeStructure> fetchTypeStructures(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeStructure.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeStructure> eoObjects = (NSArray<EOTypeStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeStructure fetchTypeStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeStructure.fetchTypeStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeStructure fetchTypeStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeStructure> eoObjects = _EOTypeStructure.fetchTypeStructures(editingContext, qualifier, null);
    EOTypeStructure eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeStructure)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeStructure fetchRequiredTypeStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeStructure.fetchRequiredTypeStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeStructure fetchRequiredTypeStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeStructure eoObject = _EOTypeStructure.fetchTypeStructure(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeStructure localInstanceIn(EOEditingContext editingContext, EOTypeStructure eo) {
    EOTypeStructure localInstance = (eo == null) ? null : (EOTypeStructure)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
