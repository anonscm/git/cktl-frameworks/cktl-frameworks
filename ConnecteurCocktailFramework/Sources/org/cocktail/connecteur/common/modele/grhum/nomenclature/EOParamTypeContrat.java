//EOParamTypeContrat.java
//Created on Fri Feb 07 08:15:23  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/** R&egrave;gles de validation :<BR>
 * Si un taux horaire est fourni, le nombre d'unites doit &ecirc;tre fourni<BR>
 * Si un montant ou un taux smic est fourni, le type du montant doit &ecirc;tre fourni, dans les autres cas il ne doit pas l'&ecirc;tre<BR>
 * Si un indice est fourni, la quotite doit &ecirc;tre fournie<BR>
 * @author christine
 *
 */

public class EOParamTypeContrat extends EOGenericRecord {

	public EOParamTypeContrat() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOParamTypeContrat(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */

	public String ptcIndice() {
		return (String)storedValueForKey("ptcIndice");
	}

	public void setPtcIndice(String value) {
		takeStoredValueForKey(value, "ptcIndice");
	}

	public String ptcQuotite() {
		return (String)storedValueForKey("ptcQuotite");
	}

	public void setPtcQuotite(String value) {
		takeStoredValueForKey(value, "ptcQuotite");
	}

	public String ptcMontant() {
		return (String)storedValueForKey("ptcMontant");
	}

	public void setPtcMontant(String value) {
		takeStoredValueForKey(value, "ptcMontant");
	}

	public String ptcTypeMontant() {
		return (String)storedValueForKey("ptcTypeMontant");
	}

	public void setPtcTypeMontant(String value) {
		takeStoredValueForKey(value, "ptcTypeMontant");
	}

	public String ptcNbrUnite() {
		return (String)storedValueForKey("ptcNbrUnite");
	}

	public void setPtcNbrUnite(String value) {
		takeStoredValueForKey(value, "ptcNbrUnite");
	}

	public String ptcTemPonctuel() {
		return (String)storedValueForKey("ptcTemPonctuel");
	}

	public void setPtcTemPonctuel(String value) {
		takeStoredValueForKey(value, "ptcTemPonctuel");
	}

	public String ptcSmic() {
		return (String)storedValueForKey("ptcSmic");
	}

	public void setPtcSmic(String value) {
		takeStoredValueForKey(value, "ptcSmic");
	}

	public String ptcTauxHoraire() {
		return (String)storedValueForKey("ptcTauxHoraire");
	}

	public void setPtcTauxHoraire(String value) {
		takeStoredValueForKey(value, "ptcTauxHoraire");
	}

	public String cTypeContratTrav() {
		return (String)storedValueForKey("cTypeContratTrav");
	}

	public void setCTypeContratTrav(String value) {
		takeStoredValueForKey(value, "cTypeContratTrav");
	}
	// Méthodes ajoutées
	public boolean fournirIndice() {
		return ptcIndice() != null && ptcIndice().equals(CocktailConstantes.VRAI);
	}
	public void setFournirIndice(boolean aBool) {
		if (aBool) {
			setPtcIndice(CocktailConstantes.VRAI);
			setPtcQuotite(CocktailConstantes.VRAI);
		} else {
			setPtcIndice(CocktailConstantes.FAUX);
		}
		
	}
	public boolean fournirQuotite() {
		return ptcQuotite() != null && ptcQuotite().equals(CocktailConstantes.VRAI);
	}
	public boolean fournirMontant() {
		return ptcMontant() != null && ptcMontant().equals(CocktailConstantes.VRAI);
	}
	public boolean fournirTypeMontant() {
		return ptcTypeMontant() != null && ptcTypeMontant().equals(CocktailConstantes.VRAI);
	}
	public void setFournirTypeMontant(boolean aBool) {
		if (aBool) {
			setPtcTypeMontant(CocktailConstantes.VRAI);
		} else {
			setPtcTypeMontant(CocktailConstantes.FAUX);
		}
	}
	public boolean fournirNbrUnite() {
		return ptcNbrUnite() != null && ptcNbrUnite().equals(CocktailConstantes.VRAI);
	}
	public boolean fournirTauxSmic() {
		return ptcSmic() != null && ptcSmic().equals(CocktailConstantes.VRAI);
	}
	public boolean fournirTauxHoraire() {
		return ptcTauxHoraire() != null && ptcTauxHoraire().equals(CocktailConstantes.VRAI);
	}
	public boolean paiementPonctuel() {
		return ptcTemPonctuel() != null && ptcTemPonctuel().equals(CocktailConstantes.VRAI);
	}
	// Méthodes statiques
	/** Recherche le param&egrave;trage associe au type de contrat travail
	 * @return parametre param&egrave;tre trouve ou null
	 */
	public static EOParamTypeContrat rechercherParametrePourTypeContrat(EOEditingContext editingContext,String typeContrat) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cTypeContratTrav = %@", new NSArray(typeContrat));
		EOFetchSpecification fs = new EOFetchSpecification("ParamTypeContrat",qualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return (EOParamTypeContrat)results.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
}
