// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPosition.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPosition extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Position";

	// Attributes
	public static final String C_POSITION_KEY = "cPosition";
	public static final String DUREE_MAX_PERIOD_POS_KEY = "dureeMaxPeriodPos";
	public static final String LC_POSITION_KEY = "lcPosition";
	public static final String LL_POSITION_KEY = "llPosition";
	public static final String TEM_ACTIVITE_KEY = "temActivite";
	public static final String TEM_D_FIN_OBLIG_KEY = "temDFinOblig";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOPosition.class);

  public EOPosition localInstanceIn(EOEditingContext editingContext) {
    EOPosition localInstance = (EOPosition)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cPosition() {
    return (String) storedValueForKey("cPosition");
  }

  public void setCPosition(String value) {
    if (_EOPosition.LOG.isDebugEnabled()) {
    	_EOPosition.LOG.debug( "updating cPosition from " + cPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cPosition");
  }

  public Integer dureeMaxPeriodPos() {
    return (Integer) storedValueForKey("dureeMaxPeriodPos");
  }

  public void setDureeMaxPeriodPos(Integer value) {
    if (_EOPosition.LOG.isDebugEnabled()) {
    	_EOPosition.LOG.debug( "updating dureeMaxPeriodPos from " + dureeMaxPeriodPos() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeMaxPeriodPos");
  }

  public String lcPosition() {
    return (String) storedValueForKey("lcPosition");
  }

  public void setLcPosition(String value) {
    if (_EOPosition.LOG.isDebugEnabled()) {
    	_EOPosition.LOG.debug( "updating lcPosition from " + lcPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "lcPosition");
  }

  public String llPosition() {
    return (String) storedValueForKey("llPosition");
  }

  public void setLlPosition(String value) {
    if (_EOPosition.LOG.isDebugEnabled()) {
    	_EOPosition.LOG.debug( "updating llPosition from " + llPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "llPosition");
  }

  public String temActivite() {
    return (String) storedValueForKey("temActivite");
  }

  public void setTemActivite(String value) {
    if (_EOPosition.LOG.isDebugEnabled()) {
    	_EOPosition.LOG.debug( "updating temActivite from " + temActivite() + " to " + value);
    }
    takeStoredValueForKey(value, "temActivite");
  }

  public String temDFinOblig() {
    return (String) storedValueForKey("temDFinOblig");
  }

  public void setTemDFinOblig(String value) {
    if (_EOPosition.LOG.isDebugEnabled()) {
    	_EOPosition.LOG.debug( "updating temDFinOblig from " + temDFinOblig() + " to " + value);
    }
    takeStoredValueForKey(value, "temDFinOblig");
  }


  public static EOPosition createPosition(EOEditingContext editingContext, String cPosition
, String temActivite
, String temDFinOblig
) {
    EOPosition eo = (EOPosition) EOUtilities.createAndInsertInstance(editingContext, _EOPosition.ENTITY_NAME);    
		eo.setCPosition(cPosition);
		eo.setTemActivite(temActivite);
		eo.setTemDFinOblig(temDFinOblig);
    return eo;
  }

  public static NSArray<EOPosition> fetchAllPositions(EOEditingContext editingContext) {
    return _EOPosition.fetchAllPositions(editingContext, null);
  }

  public static NSArray<EOPosition> fetchAllPositions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPosition.fetchPositions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPosition> fetchPositions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPosition.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPosition> eoObjects = (NSArray<EOPosition>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPosition fetchPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPosition.fetchPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPosition fetchPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPosition> eoObjects = _EOPosition.fetchPositions(editingContext, qualifier, null);
    EOPosition eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPosition)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Position that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPosition fetchRequiredPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPosition.fetchRequiredPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPosition fetchRequiredPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPosition eoObject = _EOPosition.fetchPosition(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Position that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPosition localInstanceIn(EOEditingContext editingContext, EOPosition eo) {
    EOPosition localInstance = (eo == null) ? null : (EOPosition)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
