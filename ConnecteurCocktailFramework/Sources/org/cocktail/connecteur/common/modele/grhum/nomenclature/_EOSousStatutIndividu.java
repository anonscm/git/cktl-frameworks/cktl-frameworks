// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSousStatutIndividu.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSousStatutIndividu extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SousStatutIndividu";

	// Attributes
	public static final String C_SS_STATUT_KEY = "cSsStatut";
	public static final String C_STATUT_KEY = "cStatut";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_SS_STATUT_KEY = "lcSsStatut";
	public static final String LL_SS_STATUT_KEY = "llSsStatut";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOSousStatutIndividu.class);

  public EOSousStatutIndividu localInstanceIn(EOEditingContext editingContext) {
    EOSousStatutIndividu localInstance = (EOSousStatutIndividu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cSsStatut() {
    return (String) storedValueForKey("cSsStatut");
  }

  public void setCSsStatut(String value) {
    if (_EOSousStatutIndividu.LOG.isDebugEnabled()) {
    	_EOSousStatutIndividu.LOG.debug( "updating cSsStatut from " + cSsStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "cSsStatut");
  }

  public String cStatut() {
    return (String) storedValueForKey("cStatut");
  }

  public void setCStatut(String value) {
    if (_EOSousStatutIndividu.LOG.isDebugEnabled()) {
    	_EOSousStatutIndividu.LOG.debug( "updating cStatut from " + cStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "cStatut");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOSousStatutIndividu.LOG.isDebugEnabled()) {
    	_EOSousStatutIndividu.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOSousStatutIndividu.LOG.isDebugEnabled()) {
    	_EOSousStatutIndividu.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcSsStatut() {
    return (String) storedValueForKey("lcSsStatut");
  }

  public void setLcSsStatut(String value) {
    if (_EOSousStatutIndividu.LOG.isDebugEnabled()) {
    	_EOSousStatutIndividu.LOG.debug( "updating lcSsStatut from " + lcSsStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "lcSsStatut");
  }

  public String llSsStatut() {
    return (String) storedValueForKey("llSsStatut");
  }

  public void setLlSsStatut(String value) {
    if (_EOSousStatutIndividu.LOG.isDebugEnabled()) {
    	_EOSousStatutIndividu.LOG.debug( "updating llSsStatut from " + llSsStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "llSsStatut");
  }


  public static EOSousStatutIndividu createSousStatutIndividu(EOEditingContext editingContext, String cSsStatut
, String cStatut
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcSsStatut
, String llSsStatut
) {
    EOSousStatutIndividu eo = (EOSousStatutIndividu) EOUtilities.createAndInsertInstance(editingContext, _EOSousStatutIndividu.ENTITY_NAME);    
		eo.setCSsStatut(cSsStatut);
		eo.setCStatut(cStatut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcSsStatut(lcSsStatut);
		eo.setLlSsStatut(llSsStatut);
    return eo;
  }

  public static NSArray<EOSousStatutIndividu> fetchAllSousStatutIndividus(EOEditingContext editingContext) {
    return _EOSousStatutIndividu.fetchAllSousStatutIndividus(editingContext, null);
  }

  public static NSArray<EOSousStatutIndividu> fetchAllSousStatutIndividus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSousStatutIndividu.fetchSousStatutIndividus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSousStatutIndividu> fetchSousStatutIndividus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSousStatutIndividu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSousStatutIndividu> eoObjects = (NSArray<EOSousStatutIndividu>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSousStatutIndividu fetchSousStatutIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSousStatutIndividu.fetchSousStatutIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSousStatutIndividu fetchSousStatutIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSousStatutIndividu> eoObjects = _EOSousStatutIndividu.fetchSousStatutIndividus(editingContext, qualifier, null);
    EOSousStatutIndividu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSousStatutIndividu)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one SousStatutIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSousStatutIndividu fetchRequiredSousStatutIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSousStatutIndividu.fetchRequiredSousStatutIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSousStatutIndividu fetchRequiredSousStatutIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSousStatutIndividu eoObject = _EOSousStatutIndividu.fetchSousStatutIndividu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no SousStatutIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSousStatutIndividu localInstanceIn(EOEditingContext editingContext, EOSousStatutIndividu eo) {
    EOSousStatutIndividu localInstance = (eo == null) ? null : (EOSousStatutIndividu)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
