package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IDisciplineSecondDegre;

import com.webobjects.foundation.NSDictionary;

public class EODiscSecondDegre extends _EODiscSecondDegre implements IDisciplineSecondDegre {
	private static Logger log = Logger.getLogger(EODiscSecondDegre.class);

	public String clePrimaire() {
		return cDiscSecondDegre();
	}
}
