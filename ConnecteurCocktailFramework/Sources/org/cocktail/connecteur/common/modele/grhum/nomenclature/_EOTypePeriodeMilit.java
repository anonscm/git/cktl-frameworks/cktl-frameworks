// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypePeriodeMilit.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypePeriodeMilit extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypePeriodeMilit";

	// Attributes
	public static final String C_PERIODE_MILITAIRE_KEY = "cPeriodeMilitaire";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypePeriodeMilit.class);

  public EOTypePeriodeMilit localInstanceIn(EOEditingContext editingContext) {
    EOTypePeriodeMilit localInstance = (EOTypePeriodeMilit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cPeriodeMilitaire() {
    return (String) storedValueForKey("cPeriodeMilitaire");
  }

  public void setCPeriodeMilitaire(String value) {
    if (_EOTypePeriodeMilit.LOG.isDebugEnabled()) {
    	_EOTypePeriodeMilit.LOG.debug( "updating cPeriodeMilitaire from " + cPeriodeMilitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "cPeriodeMilitaire");
  }


  public static EOTypePeriodeMilit createTypePeriodeMilit(EOEditingContext editingContext, String cPeriodeMilitaire
) {
    EOTypePeriodeMilit eo = (EOTypePeriodeMilit) EOUtilities.createAndInsertInstance(editingContext, _EOTypePeriodeMilit.ENTITY_NAME);    
		eo.setCPeriodeMilitaire(cPeriodeMilitaire);
    return eo;
  }

  public static NSArray<EOTypePeriodeMilit> fetchAllTypePeriodeMilits(EOEditingContext editingContext) {
    return _EOTypePeriodeMilit.fetchAllTypePeriodeMilits(editingContext, null);
  }

  public static NSArray<EOTypePeriodeMilit> fetchAllTypePeriodeMilits(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypePeriodeMilit.fetchTypePeriodeMilits(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypePeriodeMilit> fetchTypePeriodeMilits(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypePeriodeMilit.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypePeriodeMilit> eoObjects = (NSArray<EOTypePeriodeMilit>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypePeriodeMilit fetchTypePeriodeMilit(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePeriodeMilit.fetchTypePeriodeMilit(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePeriodeMilit fetchTypePeriodeMilit(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypePeriodeMilit> eoObjects = _EOTypePeriodeMilit.fetchTypePeriodeMilits(editingContext, qualifier, null);
    EOTypePeriodeMilit eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypePeriodeMilit)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypePeriodeMilit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePeriodeMilit fetchRequiredTypePeriodeMilit(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePeriodeMilit.fetchRequiredTypePeriodeMilit(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePeriodeMilit fetchRequiredTypePeriodeMilit(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypePeriodeMilit eoObject = _EOTypePeriodeMilit.fetchTypePeriodeMilit(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypePeriodeMilit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePeriodeMilit localInstanceIn(EOEditingContext editingContext, EOTypePeriodeMilit eo) {
    EOTypePeriodeMilit localInstance = (eo == null) ? null : (EOTypePeriodeMilit)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
