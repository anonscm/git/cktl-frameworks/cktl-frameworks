// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeDechargeService.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeDechargeService extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeDechargeService";

	// Attributes
	public static final String C_TYPE_DECHARGE_KEY = "cTypeDecharge";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String LC_TYPE_DECHARGE_KEY = "lcTypeDecharge";
	public static final String LL_TYPE_DECHARGE_KEY = "llTypeDecharge";
	public static final String TEM_HCOMP_KEY = "temHcomp";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeDechargeService.class);

  public EOTypeDechargeService localInstanceIn(EOEditingContext editingContext) {
    EOTypeDechargeService localInstance = (EOTypeDechargeService)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeDecharge() {
    return (String) storedValueForKey("cTypeDecharge");
  }

  public void setCTypeDecharge(String value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating cTypeDecharge from " + cTypeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeDecharge");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey("dOuverture");
  }

  public void setDOuverture(NSTimestamp value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating dOuverture from " + dOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dOuverture");
  }

  public String lcTypeDecharge() {
    return (String) storedValueForKey("lcTypeDecharge");
  }

  public void setLcTypeDecharge(String value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating lcTypeDecharge from " + lcTypeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeDecharge");
  }

  public String llTypeDecharge() {
    return (String) storedValueForKey("llTypeDecharge");
  }

  public void setLlTypeDecharge(String value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating llTypeDecharge from " + llTypeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeDecharge");
  }

  public String temHcomp() {
    return (String) storedValueForKey("temHcomp");
  }

  public void setTemHcomp(String value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating temHcomp from " + temHcomp() + " to " + value);
    }
    takeStoredValueForKey(value, "temHcomp");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOTypeDechargeService.LOG.isDebugEnabled()) {
    	_EOTypeDechargeService.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOTypeDechargeService createTypeDechargeService(EOEditingContext editingContext, String cTypeDecharge
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcTypeDecharge
, String llTypeDecharge
) {
    EOTypeDechargeService eo = (EOTypeDechargeService) EOUtilities.createAndInsertInstance(editingContext, _EOTypeDechargeService.ENTITY_NAME);    
		eo.setCTypeDecharge(cTypeDecharge);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcTypeDecharge(lcTypeDecharge);
		eo.setLlTypeDecharge(llTypeDecharge);
    return eo;
  }

  public static NSArray<EOTypeDechargeService> fetchAllTypeDechargeServices(EOEditingContext editingContext) {
    return _EOTypeDechargeService.fetchAllTypeDechargeServices(editingContext, null);
  }

  public static NSArray<EOTypeDechargeService> fetchAllTypeDechargeServices(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeDechargeService.fetchTypeDechargeServices(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeDechargeService> fetchTypeDechargeServices(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeDechargeService.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeDechargeService> eoObjects = (NSArray<EOTypeDechargeService>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeDechargeService fetchTypeDechargeService(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeDechargeService.fetchTypeDechargeService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeDechargeService fetchTypeDechargeService(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeDechargeService> eoObjects = _EOTypeDechargeService.fetchTypeDechargeServices(editingContext, qualifier, null);
    EOTypeDechargeService eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeDechargeService)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeDechargeService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeDechargeService fetchRequiredTypeDechargeService(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeDechargeService.fetchRequiredTypeDechargeService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeDechargeService fetchRequiredTypeDechargeService(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeDechargeService eoObject = _EOTypeDechargeService.fetchTypeDechargeService(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeDechargeService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeDechargeService localInstanceIn(EOEditingContext editingContext, EOTypeDechargeService eo) {
    EOTypeDechargeService localInstance = (eo == null) ? null : (EOTypeDechargeService)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
