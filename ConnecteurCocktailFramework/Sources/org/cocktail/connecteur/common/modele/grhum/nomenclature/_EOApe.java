// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOApe.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOApe extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Ape";

	// Attributes
	public static final String APE_CODE_KEY = "apeCode";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOApe.class);

  public EOApe localInstanceIn(EOEditingContext editingContext) {
    EOApe localInstance = (EOApe)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String apeCode() {
    return (String) storedValueForKey("apeCode");
  }

  public void setApeCode(String value) {
    if (_EOApe.LOG.isDebugEnabled()) {
    	_EOApe.LOG.debug( "updating apeCode from " + apeCode() + " to " + value);
    }
    takeStoredValueForKey(value, "apeCode");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOApe.LOG.isDebugEnabled()) {
    	_EOApe.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOApe.LOG.isDebugEnabled()) {
    	_EOApe.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }


  public static EOApe createApe(EOEditingContext editingContext, String apeCode
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOApe eo = (EOApe) EOUtilities.createAndInsertInstance(editingContext, _EOApe.ENTITY_NAME);    
		eo.setApeCode(apeCode);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOApe> fetchAllApes(EOEditingContext editingContext) {
    return _EOApe.fetchAllApes(editingContext, null);
  }

  public static NSArray<EOApe> fetchAllApes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOApe.fetchApes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOApe> fetchApes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOApe.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOApe> eoObjects = (NSArray<EOApe>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOApe fetchApe(EOEditingContext editingContext, String keyName, Object value) {
    return _EOApe.fetchApe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOApe fetchApe(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOApe> eoObjects = _EOApe.fetchApes(editingContext, qualifier, null);
    EOApe eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOApe)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Ape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOApe fetchRequiredApe(EOEditingContext editingContext, String keyName, Object value) {
    return _EOApe.fetchRequiredApe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOApe fetchRequiredApe(EOEditingContext editingContext, EOQualifier qualifier) {
    EOApe eoObject = _EOApe.fetchApe(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Ape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOApe localInstanceIn(EOEditingContext editingContext, EOApe eo) {
    EOApe localInstance = (eo == null) ? null : (EOApe)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
