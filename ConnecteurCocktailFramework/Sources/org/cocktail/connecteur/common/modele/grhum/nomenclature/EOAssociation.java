package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EOAssociation extends _EOAssociation {

	public static EOAssociation getFromCode(EOEditingContext editingContext, String assCode) {
	if (assCode==null)
		return null;
	
	EOQualifier qualifier=EOQualifier.qualifierWithQualifierFormat(ASS_CODE_KEY+" = %@", new NSArray(assCode));
	
	// Dans le cas où il y a plusieurs association pour ce code, on prend la plus récente
	NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(D_OUVERTURE_KEY, EOSortOrdering.CompareDescending));
			
	NSArray<EOAssociation> associations=fetchAssociations(editingContext, qualifier, sorts);
	if (associations==null || associations.count()<1)
		return null;
	
	return associations.get(0);
}
}
