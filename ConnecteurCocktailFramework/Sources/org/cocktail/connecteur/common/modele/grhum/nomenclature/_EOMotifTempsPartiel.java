// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMotifTempsPartiel.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMotifTempsPartiel extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MotifTempsPartiel";

	// Attributes
	public static final String C_MOTIF_TEMPS_PARTIEL_KEY = "cMotifTempsPartiel";
	public static final String C_MOTIF_TEMPS_PARTIEL_ONP_KEY = "cMotifTempsPartielOnp";
	public static final String TEM_CTRL_DUREE_TPS_KEY = "temCtrlDureeTps";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMotifTempsPartiel.class);

  public EOMotifTempsPartiel localInstanceIn(EOEditingContext editingContext) {
    EOMotifTempsPartiel localInstance = (EOMotifTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifTempsPartiel() {
    return (String) storedValueForKey("cMotifTempsPartiel");
  }

  public void setCMotifTempsPartiel(String value) {
    if (_EOMotifTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMotifTempsPartiel.LOG.debug( "updating cMotifTempsPartiel from " + cMotifTempsPartiel() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifTempsPartiel");
  }

  public String cMotifTempsPartielOnp() {
    return (String) storedValueForKey("cMotifTempsPartielOnp");
  }

  public void setCMotifTempsPartielOnp(String value) {
    if (_EOMotifTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMotifTempsPartiel.LOG.debug( "updating cMotifTempsPartielOnp from " + cMotifTempsPartielOnp() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifTempsPartielOnp");
  }

  public String temCtrlDureeTps() {
    return (String) storedValueForKey("temCtrlDureeTps");
  }

  public void setTemCtrlDureeTps(String value) {
    if (_EOMotifTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMotifTempsPartiel.LOG.debug( "updating temCtrlDureeTps from " + temCtrlDureeTps() + " to " + value);
    }
    takeStoredValueForKey(value, "temCtrlDureeTps");
  }


  public static EOMotifTempsPartiel createMotifTempsPartiel(EOEditingContext editingContext, String cMotifTempsPartiel
) {
    EOMotifTempsPartiel eo = (EOMotifTempsPartiel) EOUtilities.createAndInsertInstance(editingContext, _EOMotifTempsPartiel.ENTITY_NAME);    
		eo.setCMotifTempsPartiel(cMotifTempsPartiel);
    return eo;
  }

  public static NSArray<EOMotifTempsPartiel> fetchAllMotifTempsPartiels(EOEditingContext editingContext) {
    return _EOMotifTempsPartiel.fetchAllMotifTempsPartiels(editingContext, null);
  }

  public static NSArray<EOMotifTempsPartiel> fetchAllMotifTempsPartiels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMotifTempsPartiel.fetchMotifTempsPartiels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMotifTempsPartiel> fetchMotifTempsPartiels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMotifTempsPartiel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMotifTempsPartiel> eoObjects = (NSArray<EOMotifTempsPartiel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMotifTempsPartiel fetchMotifTempsPartiel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifTempsPartiel.fetchMotifTempsPartiel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifTempsPartiel fetchMotifTempsPartiel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMotifTempsPartiel> eoObjects = _EOMotifTempsPartiel.fetchMotifTempsPartiels(editingContext, qualifier, null);
    EOMotifTempsPartiel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMotifTempsPartiel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MotifTempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifTempsPartiel fetchRequiredMotifTempsPartiel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifTempsPartiel.fetchRequiredMotifTempsPartiel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifTempsPartiel fetchRequiredMotifTempsPartiel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMotifTempsPartiel eoObject = _EOMotifTempsPartiel.fetchMotifTempsPartiel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MotifTempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifTempsPartiel localInstanceIn(EOEditingContext editingContext, EOMotifTempsPartiel eo) {
    EOMotifTempsPartiel localInstance = (eo == null) ? null : (EOMotifTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
