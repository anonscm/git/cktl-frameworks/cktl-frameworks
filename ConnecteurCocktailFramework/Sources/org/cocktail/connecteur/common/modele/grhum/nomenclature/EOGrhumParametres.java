// EOGrhumParametres.java
// Created on Wed Jul 25 15:44:44 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOGrhumParametres extends EOGenericRecord {

    public EOGrhumParametres() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOGrhumParametres(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String paramKey() {
        return (String)storedValueForKey("paramKey");
    }

    public void setParamKey(String value) {
        takeStoredValueForKey(value, "paramKey");
    }

    public String paramValue() {
        return (String)storedValueForKey("paramValue");
    }

    public void setParamValue(String value) {
        takeStoredValueForKey(value, "paramValue");
    }

    public String paramCommentaires() {
        return (String)storedValueForKey("paramCommentaires");
    }

    public void setParamCommentaires(String value) {
        takeStoredValueForKey(value, "paramCommentaires");
    }
    
    // Méthodes ajoutées
    public static String parametrePourCle(EOEditingContext editingContext,String cle) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("paramKey = %@",new NSArray(cle));
		EOFetchSpecification fs = new EOFetchSpecification("GrhumParametres",qualifier,null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return ((EOGrhumParametres)results.objectAtIndex(0)).paramValue();
		} catch(Exception e) {
			return null;
		}
    }
    /** Retourne la longueur maximum du login */
    public static int maxLogin(EOEditingContext editingContext) {
		String loginLg = EOGrhumParametres.parametrePourCle(editingContext, "ANNUAIRE_LOGIN_MAX");
		int maxLogin = 7;
		if (loginLg != null) {
			try {
				maxLogin = new Integer(loginLg).intValue();
			} catch(Exception e) {e.printStackTrace();}
		}
		return maxLogin;
	}
}
