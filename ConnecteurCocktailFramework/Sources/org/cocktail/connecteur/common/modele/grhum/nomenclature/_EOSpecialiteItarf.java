// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSpecialiteItarf.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSpecialiteItarf extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SpecialiteItarf";

	// Attributes
	public static final String C_BAP_KEY = "cBap";
	public static final String C_SPECIALITE_ITARF_KEY = "cSpecialiteItarf";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_SPECIALITE_KEY = "lcSpecialite";
	public static final String LL_SPECIALITE_ITARF_KEY = "llSpecialiteItarf";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOSpecialiteItarf.class);

  public EOSpecialiteItarf localInstanceIn(EOEditingContext editingContext) {
    EOSpecialiteItarf localInstance = (EOSpecialiteItarf)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOSpecialiteItarf.LOG.isDebugEnabled()) {
    	_EOSpecialiteItarf.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public String cSpecialiteItarf() {
    return (String) storedValueForKey("cSpecialiteItarf");
  }

  public void setCSpecialiteItarf(String value) {
    if (_EOSpecialiteItarf.LOG.isDebugEnabled()) {
    	_EOSpecialiteItarf.LOG.debug( "updating cSpecialiteItarf from " + cSpecialiteItarf() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteItarf");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOSpecialiteItarf.LOG.isDebugEnabled()) {
    	_EOSpecialiteItarf.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOSpecialiteItarf.LOG.isDebugEnabled()) {
    	_EOSpecialiteItarf.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcSpecialite() {
    return (String) storedValueForKey("lcSpecialite");
  }

  public void setLcSpecialite(String value) {
    if (_EOSpecialiteItarf.LOG.isDebugEnabled()) {
    	_EOSpecialiteItarf.LOG.debug( "updating lcSpecialite from " + lcSpecialite() + " to " + value);
    }
    takeStoredValueForKey(value, "lcSpecialite");
  }

  public String llSpecialiteItarf() {
    return (String) storedValueForKey("llSpecialiteItarf");
  }

  public void setLlSpecialiteItarf(String value) {
    if (_EOSpecialiteItarf.LOG.isDebugEnabled()) {
    	_EOSpecialiteItarf.LOG.debug( "updating llSpecialiteItarf from " + llSpecialiteItarf() + " to " + value);
    }
    takeStoredValueForKey(value, "llSpecialiteItarf");
  }


  public static EOSpecialiteItarf createSpecialiteItarf(EOEditingContext editingContext, String cBap
, String cSpecialiteItarf
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOSpecialiteItarf eo = (EOSpecialiteItarf) EOUtilities.createAndInsertInstance(editingContext, _EOSpecialiteItarf.ENTITY_NAME);    
		eo.setCBap(cBap);
		eo.setCSpecialiteItarf(cSpecialiteItarf);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOSpecialiteItarf> fetchAllSpecialiteItarfs(EOEditingContext editingContext) {
    return _EOSpecialiteItarf.fetchAllSpecialiteItarfs(editingContext, null);
  }

  public static NSArray<EOSpecialiteItarf> fetchAllSpecialiteItarfs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSpecialiteItarf.fetchSpecialiteItarfs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSpecialiteItarf> fetchSpecialiteItarfs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSpecialiteItarf.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSpecialiteItarf> eoObjects = (NSArray<EOSpecialiteItarf>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSpecialiteItarf fetchSpecialiteItarf(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSpecialiteItarf.fetchSpecialiteItarf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSpecialiteItarf fetchSpecialiteItarf(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSpecialiteItarf> eoObjects = _EOSpecialiteItarf.fetchSpecialiteItarfs(editingContext, qualifier, null);
    EOSpecialiteItarf eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSpecialiteItarf)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one SpecialiteItarf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSpecialiteItarf fetchRequiredSpecialiteItarf(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSpecialiteItarf.fetchRequiredSpecialiteItarf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSpecialiteItarf fetchRequiredSpecialiteItarf(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSpecialiteItarf eoObject = _EOSpecialiteItarf.fetchSpecialiteItarf(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no SpecialiteItarf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSpecialiteItarf localInstanceIn(EOEditingContext editingContext, EOSpecialiteItarf eo) {
    EOSpecialiteItarf localInstance = (eo == null) ? null : (EOSpecialiteItarf)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
