// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumParametres.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumParametres extends  EOGenericRecord {
	public static final String ENTITY_NAME = "GrhumParametres";

	// Attributes
	public static final String PARAM_COMMENTAIRES_KEY = "paramCommentaires";
	public static final String PARAM_KEY_KEY = "paramKey";
	public static final String PARAM_VALUE_KEY = "paramValue";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOGrhumParametres.class);

  public EOGrhumParametres localInstanceIn(EOEditingContext editingContext) {
    EOGrhumParametres localInstance = (EOGrhumParametres)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String paramCommentaires() {
    return (String) storedValueForKey("paramCommentaires");
  }

  public void setParamCommentaires(String value) {
    if (_EOGrhumParametres.LOG.isDebugEnabled()) {
    	_EOGrhumParametres.LOG.debug( "updating paramCommentaires from " + paramCommentaires() + " to " + value);
    }
    takeStoredValueForKey(value, "paramCommentaires");
  }

  public String paramKey() {
    return (String) storedValueForKey("paramKey");
  }

  public void setParamKey(String value) {
    if (_EOGrhumParametres.LOG.isDebugEnabled()) {
    	_EOGrhumParametres.LOG.debug( "updating paramKey from " + paramKey() + " to " + value);
    }
    takeStoredValueForKey(value, "paramKey");
  }

  public String paramValue() {
    return (String) storedValueForKey("paramValue");
  }

  public void setParamValue(String value) {
    if (_EOGrhumParametres.LOG.isDebugEnabled()) {
    	_EOGrhumParametres.LOG.debug( "updating paramValue from " + paramValue() + " to " + value);
    }
    takeStoredValueForKey(value, "paramValue");
  }


  public static EOGrhumParametres createGrhumParametres(EOEditingContext editingContext) {
    EOGrhumParametres eo = (EOGrhumParametres) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumParametres.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOGrhumParametres> fetchAllGrhumParametreses(EOEditingContext editingContext) {
    return _EOGrhumParametres.fetchAllGrhumParametreses(editingContext, null);
  }

  public static NSArray<EOGrhumParametres> fetchAllGrhumParametreses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumParametres.fetchGrhumParametreses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumParametres> fetchGrhumParametreses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumParametres.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumParametres> eoObjects = (NSArray<EOGrhumParametres>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumParametres fetchGrhumParametres(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumParametres.fetchGrhumParametres(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumParametres fetchGrhumParametres(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumParametres> eoObjects = _EOGrhumParametres.fetchGrhumParametreses(editingContext, qualifier, null);
    EOGrhumParametres eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumParametres)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumParametres that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumParametres fetchRequiredGrhumParametres(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumParametres.fetchRequiredGrhumParametres(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumParametres fetchRequiredGrhumParametres(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumParametres eoObject = _EOGrhumParametres.fetchGrhumParametres(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumParametres that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumParametres localInstanceIn(EOEditingContext editingContext, EOGrhumParametres eo) {
    EOGrhumParametres localInstance = (eo == null) ? null : (EOGrhumParametres)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
