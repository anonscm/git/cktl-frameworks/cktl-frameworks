// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeFonctionPublique.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeFonctionPublique extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeFonctionPublique";

	// Attributes
	public static final String C_TYPE_FP_KEY = "cTypeFp";
	public static final String LC_TYPE_FP_KEY = "lcTypeFp";
	public static final String LL_TYPE_FP_KEY = "llTypeFp";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeFonctionPublique.class);

  public EOTypeFonctionPublique localInstanceIn(EOEditingContext editingContext) {
    EOTypeFonctionPublique localInstance = (EOTypeFonctionPublique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeFp() {
    return (String) storedValueForKey("cTypeFp");
  }

  public void setCTypeFp(String value) {
    if (_EOTypeFonctionPublique.LOG.isDebugEnabled()) {
    	_EOTypeFonctionPublique.LOG.debug( "updating cTypeFp from " + cTypeFp() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeFp");
  }

  public String lcTypeFp() {
    return (String) storedValueForKey("lcTypeFp");
  }

  public void setLcTypeFp(String value) {
    if (_EOTypeFonctionPublique.LOG.isDebugEnabled()) {
    	_EOTypeFonctionPublique.LOG.debug( "updating lcTypeFp from " + lcTypeFp() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeFp");
  }

  public String llTypeFp() {
    return (String) storedValueForKey("llTypeFp");
  }

  public void setLlTypeFp(String value) {
    if (_EOTypeFonctionPublique.LOG.isDebugEnabled()) {
    	_EOTypeFonctionPublique.LOG.debug( "updating llTypeFp from " + llTypeFp() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeFp");
  }


  public static EOTypeFonctionPublique createTypeFonctionPublique(EOEditingContext editingContext, String cTypeFp
, String lcTypeFp
, String llTypeFp
) {
    EOTypeFonctionPublique eo = (EOTypeFonctionPublique) EOUtilities.createAndInsertInstance(editingContext, _EOTypeFonctionPublique.ENTITY_NAME);    
		eo.setCTypeFp(cTypeFp);
		eo.setLcTypeFp(lcTypeFp);
		eo.setLlTypeFp(llTypeFp);
    return eo;
  }

  public static NSArray<EOTypeFonctionPublique> fetchAllTypeFonctionPubliques(EOEditingContext editingContext) {
    return _EOTypeFonctionPublique.fetchAllTypeFonctionPubliques(editingContext, null);
  }

  public static NSArray<EOTypeFonctionPublique> fetchAllTypeFonctionPubliques(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeFonctionPublique.fetchTypeFonctionPubliques(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeFonctionPublique> fetchTypeFonctionPubliques(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeFonctionPublique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeFonctionPublique> eoObjects = (NSArray<EOTypeFonctionPublique>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeFonctionPublique fetchTypeFonctionPublique(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFonctionPublique.fetchTypeFonctionPublique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFonctionPublique fetchTypeFonctionPublique(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeFonctionPublique> eoObjects = _EOTypeFonctionPublique.fetchTypeFonctionPubliques(editingContext, qualifier, null);
    EOTypeFonctionPublique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeFonctionPublique)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeFonctionPublique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFonctionPublique fetchRequiredTypeFonctionPublique(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFonctionPublique.fetchRequiredTypeFonctionPublique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFonctionPublique fetchRequiredTypeFonctionPublique(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeFonctionPublique eoObject = _EOTypeFonctionPublique.fetchTypeFonctionPublique(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeFonctionPublique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFonctionPublique localInstanceIn(EOEditingContext editingContext, EOTypeFonctionPublique eo) {
    EOTypeFonctionPublique localInstance = (eo == null) ? null : (EOTypeFonctionPublique)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
