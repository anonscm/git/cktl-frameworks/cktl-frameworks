// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypePopulation.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypePopulation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypePopulation";

	// Attributes
	public static final String C_TYPE_POPULATION_KEY = "cTypePopulation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String LC_TYPE_POPULATION_KEY = "lcTypePopulation";
	public static final String LL_TYPE_POPULATION_KEY = "llTypePopulation";
	public static final String TEM1_DEGRE_KEY = "tem1Degre";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_FONCTIONNAIRE_KEY = "temFonctionnaire";
	public static final String TEM_HOSPITALIER_KEY = "temHospitalier";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypePopulation.class);

  public EOTypePopulation localInstanceIn(EOEditingContext editingContext) {
    EOTypePopulation localInstance = (EOTypePopulation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypePopulation() {
    return (String) storedValueForKey("cTypePopulation");
  }

  public void setCTypePopulation(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating cTypePopulation from " + cTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypePopulation");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public String lcTypePopulation() {
    return (String) storedValueForKey("lcTypePopulation");
  }

  public void setLcTypePopulation(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating lcTypePopulation from " + lcTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypePopulation");
  }

  public String llTypePopulation() {
    return (String) storedValueForKey("llTypePopulation");
  }

  public void setLlTypePopulation(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating llTypePopulation from " + llTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypePopulation");
  }

  public String tem1Degre() {
    return (String) storedValueForKey("tem1Degre");
  }

  public void setTem1Degre(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating tem1Degre from " + tem1Degre() + " to " + value);
    }
    takeStoredValueForKey(value, "tem1Degre");
  }

  public String temEnseignant() {
    return (String) storedValueForKey("temEnseignant");
  }

  public void setTemEnseignant(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating temEnseignant from " + temEnseignant() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnseignant");
  }

  public String temFonctionnaire() {
    return (String) storedValueForKey("temFonctionnaire");
  }

  public void setTemFonctionnaire(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating temFonctionnaire from " + temFonctionnaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temFonctionnaire");
  }

  public String temHospitalier() {
    return (String) storedValueForKey("temHospitalier");
  }

  public void setTemHospitalier(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating temHospitalier from " + temHospitalier() + " to " + value);
    }
    takeStoredValueForKey(value, "temHospitalier");
  }


  public static EOTypePopulation createTypePopulation(EOEditingContext editingContext, String cTypePopulation
) {
    EOTypePopulation eo = (EOTypePopulation) EOUtilities.createAndInsertInstance(editingContext, _EOTypePopulation.ENTITY_NAME);    
		eo.setCTypePopulation(cTypePopulation);
    return eo;
  }

  public static NSArray<EOTypePopulation> fetchAllTypePopulations(EOEditingContext editingContext) {
    return _EOTypePopulation.fetchAllTypePopulations(editingContext, null);
  }

  public static NSArray<EOTypePopulation> fetchAllTypePopulations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypePopulation.fetchTypePopulations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypePopulation> fetchTypePopulations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypePopulation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypePopulation> eoObjects = (NSArray<EOTypePopulation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypePopulation fetchTypePopulation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePopulation.fetchTypePopulation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePopulation fetchTypePopulation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypePopulation> eoObjects = _EOTypePopulation.fetchTypePopulations(editingContext, qualifier, null);
    EOTypePopulation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypePopulation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypePopulation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePopulation fetchRequiredTypePopulation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePopulation.fetchRequiredTypePopulation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePopulation fetchRequiredTypePopulation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypePopulation eoObject = _EOTypePopulation.fetchTypePopulation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypePopulation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePopulation localInstanceIn(EOEditingContext editingContext, EOTypePopulation eo) {
    EOTypePopulation localInstance = (eo == null) ? null : (EOTypePopulation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
