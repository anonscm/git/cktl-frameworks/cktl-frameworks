// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOProgramme.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOProgramme extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Programme";

	// Attributes
	public static final String C_PROGRAMME_KEY = "cProgramme";
	public static final String LL_PROGRAMME_KEY = "llProgramme";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOProgramme.class);

  public EOProgramme localInstanceIn(EOEditingContext editingContext) {
    EOProgramme localInstance = (EOProgramme)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cProgramme() {
    return (String) storedValueForKey("cProgramme");
  }

  public void setCProgramme(String value) {
    if (_EOProgramme.LOG.isDebugEnabled()) {
    	_EOProgramme.LOG.debug( "updating cProgramme from " + cProgramme() + " to " + value);
    }
    takeStoredValueForKey(value, "cProgramme");
  }

  public String llProgramme() {
    return (String) storedValueForKey("llProgramme");
  }

  public void setLlProgramme(String value) {
    if (_EOProgramme.LOG.isDebugEnabled()) {
    	_EOProgramme.LOG.debug( "updating llProgramme from " + llProgramme() + " to " + value);
    }
    takeStoredValueForKey(value, "llProgramme");
  }


  public static EOProgramme createProgramme(EOEditingContext editingContext, String cProgramme
) {
    EOProgramme eo = (EOProgramme) EOUtilities.createAndInsertInstance(editingContext, _EOProgramme.ENTITY_NAME);    
		eo.setCProgramme(cProgramme);
    return eo;
  }

  public static NSArray<EOProgramme> fetchAllProgrammes(EOEditingContext editingContext) {
    return _EOProgramme.fetchAllProgrammes(editingContext, null);
  }

  public static NSArray<EOProgramme> fetchAllProgrammes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOProgramme.fetchProgrammes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOProgramme> fetchProgrammes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOProgramme.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOProgramme> eoObjects = (NSArray<EOProgramme>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOProgramme fetchProgramme(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProgramme.fetchProgramme(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProgramme fetchProgramme(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOProgramme> eoObjects = _EOProgramme.fetchProgrammes(editingContext, qualifier, null);
    EOProgramme eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOProgramme)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Programme that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProgramme fetchRequiredProgramme(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProgramme.fetchRequiredProgramme(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProgramme fetchRequiredProgramme(EOEditingContext editingContext, EOQualifier qualifier) {
    EOProgramme eoObject = _EOProgramme.fetchProgramme(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Programme that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProgramme localInstanceIn(EOEditingContext editingContext, EOProgramme eo) {
    EOProgramme localInstance = (eo == null) ? null : (EOProgramme)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
