// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMotifCgntRfp.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMotifCgntRfp extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MotifCgntRfp";

	// Attributes
	public static final String C_MOTIF_CG_RFP_KEY = "cMotifCgRfp";
	public static final String LL_MOTIF_CG_RFP_KEY = "llMotifCgRfp";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMotifCgntRfp.class);

  public EOMotifCgntRfp localInstanceIn(EOEditingContext editingContext) {
    EOMotifCgntRfp localInstance = (EOMotifCgntRfp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifCgRfp() {
    return (String) storedValueForKey("cMotifCgRfp");
  }

  public void setCMotifCgRfp(String value) {
    if (_EOMotifCgntRfp.LOG.isDebugEnabled()) {
    	_EOMotifCgntRfp.LOG.debug( "updating cMotifCgRfp from " + cMotifCgRfp() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifCgRfp");
  }

  public String llMotifCgRfp() {
    return (String) storedValueForKey("llMotifCgRfp");
  }

  public void setLlMotifCgRfp(String value) {
    if (_EOMotifCgntRfp.LOG.isDebugEnabled()) {
    	_EOMotifCgntRfp.LOG.debug( "updating llMotifCgRfp from " + llMotifCgRfp() + " to " + value);
    }
    takeStoredValueForKey(value, "llMotifCgRfp");
  }


  public static EOMotifCgntRfp createMotifCgntRfp(EOEditingContext editingContext, String cMotifCgRfp
) {
    EOMotifCgntRfp eo = (EOMotifCgntRfp) EOUtilities.createAndInsertInstance(editingContext, _EOMotifCgntRfp.ENTITY_NAME);    
		eo.setCMotifCgRfp(cMotifCgRfp);
    return eo;
  }

  public static NSArray<EOMotifCgntRfp> fetchAllMotifCgntRfps(EOEditingContext editingContext) {
    return _EOMotifCgntRfp.fetchAllMotifCgntRfps(editingContext, null);
  }

  public static NSArray<EOMotifCgntRfp> fetchAllMotifCgntRfps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMotifCgntRfp.fetchMotifCgntRfps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMotifCgntRfp> fetchMotifCgntRfps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMotifCgntRfp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMotifCgntRfp> eoObjects = (NSArray<EOMotifCgntRfp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMotifCgntRfp fetchMotifCgntRfp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifCgntRfp.fetchMotifCgntRfp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifCgntRfp fetchMotifCgntRfp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMotifCgntRfp> eoObjects = _EOMotifCgntRfp.fetchMotifCgntRfps(editingContext, qualifier, null);
    EOMotifCgntRfp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMotifCgntRfp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MotifCgntRfp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifCgntRfp fetchRequiredMotifCgntRfp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMotifCgntRfp.fetchRequiredMotifCgntRfp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMotifCgntRfp fetchRequiredMotifCgntRfp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMotifCgntRfp eoObject = _EOMotifCgntRfp.fetchMotifCgntRfp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MotifCgntRfp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMotifCgntRfp localInstanceIn(EOEditingContext editingContext, EOMotifCgntRfp eo) {
    EOMotifCgntRfp localInstance = (eo == null) ? null : (EOMotifCgntRfp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
