package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOGrhumRepartEnfant;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOSousStatutIndividu extends _EOSousStatutIndividu {
	public static EOSousStatutIndividu getFromStatutSsStatut(EOEditingContext editingContext, String codeStatut, String codeSsStatut) {
		EOSousStatutIndividu resultat = null;

		if (codeStatut != null && codeSsStatut != null) {
			NSArray args = new NSMutableArray();
			args.add(codeStatut);
			args.add(codeSsStatut);
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_STATUT_KEY + " = %@ AND " + C_SS_STATUT_KEY + " = %@", args);
			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
			NSArray resultats = editingContext.objectsWithFetchSpecification(fs);
			if (resultats.count() == 1)
				resultat = (EOSousStatutIndividu) resultats.objectAtIndex(0);
		}
		return resultat;
	}
}
