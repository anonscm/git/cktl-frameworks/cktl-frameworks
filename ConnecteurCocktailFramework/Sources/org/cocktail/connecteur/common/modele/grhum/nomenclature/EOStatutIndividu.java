package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eocontrol.EOEditingContext;

public class EOStatutIndividu extends _EOStatutIndividu {
	public static EOStatutIndividu getFromCode(EOEditingContext editingContext, String codeStatut) {
		return fetchStatutIndividu(editingContext, C_STATUT_KEY, codeStatut);
	}
}
