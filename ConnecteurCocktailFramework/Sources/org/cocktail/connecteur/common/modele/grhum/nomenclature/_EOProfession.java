// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOProfession.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOProfession extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Profession";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String PRO_DETAILS_KEY = "proDetails";
	public static final String PRO_LIBELLE_KEY = "proLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOProfession.class);

  public EOProfession localInstanceIn(EOEditingContext editingContext) {
    EOProfession localInstance = (EOProfession)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOProfession.LOG.isDebugEnabled()) {
    	_EOProfession.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EOProfession.LOG.isDebugEnabled()) {
    	_EOProfession.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOProfession.LOG.isDebugEnabled()) {
    	_EOProfession.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey("dOuverture");
  }

  public void setDOuverture(NSTimestamp value) {
    if (_EOProfession.LOG.isDebugEnabled()) {
    	_EOProfession.LOG.debug( "updating dOuverture from " + dOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dOuverture");
  }

  public String proCode() {
    return (String) storedValueForKey("proCode");
  }

  public void setProCode(String value) {
    if (_EOProfession.LOG.isDebugEnabled()) {
    	_EOProfession.LOG.debug( "updating proCode from " + proCode() + " to " + value);
    }
    takeStoredValueForKey(value, "proCode");
  }

  public String proDetails() {
    return (String) storedValueForKey("proDetails");
  }

  public void setProDetails(String value) {
    if (_EOProfession.LOG.isDebugEnabled()) {
    	_EOProfession.LOG.debug( "updating proDetails from " + proDetails() + " to " + value);
    }
    takeStoredValueForKey(value, "proDetails");
  }

  public String proLibelle() {
    return (String) storedValueForKey("proLibelle");
  }

  public void setProLibelle(String value) {
    if (_EOProfession.LOG.isDebugEnabled()) {
    	_EOProfession.LOG.debug( "updating proLibelle from " + proLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "proLibelle");
  }


  public static EOProfession createProfession(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String proCode
, String proLibelle
) {
    EOProfession eo = (EOProfession) EOUtilities.createAndInsertInstance(editingContext, _EOProfession.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setProCode(proCode);
		eo.setProLibelle(proLibelle);
    return eo;
  }

  public static NSArray<EOProfession> fetchAllProfessions(EOEditingContext editingContext) {
    return _EOProfession.fetchAllProfessions(editingContext, null);
  }

  public static NSArray<EOProfession> fetchAllProfessions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOProfession.fetchProfessions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOProfession> fetchProfessions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOProfession.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOProfession> eoObjects = (NSArray<EOProfession>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOProfession fetchProfession(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProfession.fetchProfession(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProfession fetchProfession(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOProfession> eoObjects = _EOProfession.fetchProfessions(editingContext, qualifier, null);
    EOProfession eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOProfession)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Profession that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProfession fetchRequiredProfession(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProfession.fetchRequiredProfession(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProfession fetchRequiredProfession(EOEditingContext editingContext, EOQualifier qualifier) {
    EOProfession eoObject = _EOProfession.fetchProfession(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Profession that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProfession localInstanceIn(EOEditingContext editingContext, EOProfession eo) {
    EOProfession localInstance = (eo == null) ? null : (EOProfession)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
