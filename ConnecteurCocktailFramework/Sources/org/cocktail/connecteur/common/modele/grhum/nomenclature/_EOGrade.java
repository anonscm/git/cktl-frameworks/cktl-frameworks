// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrade.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrade extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Grade";

	// Attributes
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String ECHELLE_KEY = "echelle";
	public static final String IRM_GRADE_KEY = "irmGrade";
	public static final String LC_GRADE_KEY = "lcGrade";
	public static final String LL_GRADE_KEY = "llGrade";

	// Relationships
	public static final String TO_CORPS_KEY = "toCorps";

  private static Logger LOG = Logger.getLogger(_EOGrade.class);

  public EOGrade localInstanceIn(EOEditingContext editingContext) {
    EOGrade localInstance = (EOGrade)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey("dOuverture");
  }

  public void setDOuverture(NSTimestamp value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating dOuverture from " + dOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dOuverture");
  }

  public String echelle() {
    return (String) storedValueForKey("echelle");
  }

  public void setEchelle(String value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating echelle from " + echelle() + " to " + value);
    }
    takeStoredValueForKey(value, "echelle");
  }

  public Integer irmGrade() {
    return (Integer) storedValueForKey("irmGrade");
  }

  public void setIrmGrade(Integer value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating irmGrade from " + irmGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "irmGrade");
  }

  public String lcGrade() {
    return (String) storedValueForKey("lcGrade");
  }

  public void setLcGrade(String value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating lcGrade from " + lcGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "lcGrade");
  }

  public String llGrade() {
    return (String) storedValueForKey("llGrade");
  }

  public void setLlGrade(String value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
    	_EOGrade.LOG.debug( "updating llGrade from " + llGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "llGrade");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps toCorps() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps)storedValueForKey("toCorps");
  }

  public void setToCorpsRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps value) {
    if (_EOGrade.LOG.isDebugEnabled()) {
      _EOGrade.LOG.debug("updating toCorps from " + toCorps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCorps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCorps");
    }
  }
  

  public static EOGrade createGrade(EOEditingContext editingContext, String cGrade
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcGrade
, String llGrade
, org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps toCorps) {
    EOGrade eo = (EOGrade) EOUtilities.createAndInsertInstance(editingContext, _EOGrade.ENTITY_NAME);    
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcGrade(lcGrade);
		eo.setLlGrade(llGrade);
    eo.setToCorpsRelationship(toCorps);
    return eo;
  }

  public static NSArray<EOGrade> fetchAllGrades(EOEditingContext editingContext) {
    return _EOGrade.fetchAllGrades(editingContext, null);
  }

  public static NSArray<EOGrade> fetchAllGrades(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrade.fetchGrades(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrade> fetchGrades(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrade.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrade> eoObjects = (NSArray<EOGrade>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrade fetchGrade(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrade.fetchGrade(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrade fetchGrade(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrade> eoObjects = _EOGrade.fetchGrades(editingContext, qualifier, null);
    EOGrade eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrade)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Grade that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrade fetchRequiredGrade(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrade.fetchRequiredGrade(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrade fetchRequiredGrade(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrade eoObject = _EOGrade.fetchGrade(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Grade that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrade localInstanceIn(EOEditingContext editingContext, EOGrade eo) {
    EOGrade localInstance = (eo == null) ? null : (EOGrade)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
