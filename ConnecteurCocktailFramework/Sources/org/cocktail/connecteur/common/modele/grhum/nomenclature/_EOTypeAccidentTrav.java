// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeAccidentTrav.java instead.
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeAccidentTrav extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeAccidentTrav";

	// Attributes
	public static final String C_TYPE_ACC_TRAV_KEY = "cTypeAccTrav";
	public static final String LC_TYPE_ACC_TRAV_KEY = "lcTypeAccTrav";
	public static final String LL_TYPE_ACCIDENT_TRAV_KEY = "llTypeAccidentTrav";
	public static final String TEM_CONTRACTUEL_KEY = "temContractuel";
	public static final String TEM_FONCTIONNAIRE_KEY = "temFonctionnaire";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeAccidentTrav.class);

  public EOTypeAccidentTrav localInstanceIn(EOEditingContext editingContext) {
    EOTypeAccidentTrav localInstance = (EOTypeAccidentTrav)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeAccTrav() {
    return (String) storedValueForKey("cTypeAccTrav");
  }

  public void setCTypeAccTrav(String value) {
    if (_EOTypeAccidentTrav.LOG.isDebugEnabled()) {
    	_EOTypeAccidentTrav.LOG.debug( "updating cTypeAccTrav from " + cTypeAccTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAccTrav");
  }

  public String lcTypeAccTrav() {
    return (String) storedValueForKey("lcTypeAccTrav");
  }

  public void setLcTypeAccTrav(String value) {
    if (_EOTypeAccidentTrav.LOG.isDebugEnabled()) {
    	_EOTypeAccidentTrav.LOG.debug( "updating lcTypeAccTrav from " + lcTypeAccTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "lcTypeAccTrav");
  }

  public String llTypeAccidentTrav() {
    return (String) storedValueForKey("llTypeAccidentTrav");
  }

  public void setLlTypeAccidentTrav(String value) {
    if (_EOTypeAccidentTrav.LOG.isDebugEnabled()) {
    	_EOTypeAccidentTrav.LOG.debug( "updating llTypeAccidentTrav from " + llTypeAccidentTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "llTypeAccidentTrav");
  }

  public String temContractuel() {
    return (String) storedValueForKey("temContractuel");
  }

  public void setTemContractuel(String value) {
    if (_EOTypeAccidentTrav.LOG.isDebugEnabled()) {
    	_EOTypeAccidentTrav.LOG.debug( "updating temContractuel from " + temContractuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temContractuel");
  }

  public String temFonctionnaire() {
    return (String) storedValueForKey("temFonctionnaire");
  }

  public void setTemFonctionnaire(String value) {
    if (_EOTypeAccidentTrav.LOG.isDebugEnabled()) {
    	_EOTypeAccidentTrav.LOG.debug( "updating temFonctionnaire from " + temFonctionnaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temFonctionnaire");
  }


  public static EOTypeAccidentTrav createTypeAccidentTrav(EOEditingContext editingContext, String cTypeAccTrav
, String temContractuel
, String temFonctionnaire
) {
    EOTypeAccidentTrav eo = (EOTypeAccidentTrav) EOUtilities.createAndInsertInstance(editingContext, _EOTypeAccidentTrav.ENTITY_NAME);    
		eo.setCTypeAccTrav(cTypeAccTrav);
		eo.setTemContractuel(temContractuel);
		eo.setTemFonctionnaire(temFonctionnaire);
    return eo;
  }

  public static NSArray<EOTypeAccidentTrav> fetchAllTypeAccidentTravs(EOEditingContext editingContext) {
    return _EOTypeAccidentTrav.fetchAllTypeAccidentTravs(editingContext, null);
  }

  public static NSArray<EOTypeAccidentTrav> fetchAllTypeAccidentTravs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeAccidentTrav.fetchTypeAccidentTravs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeAccidentTrav> fetchTypeAccidentTravs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeAccidentTrav.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeAccidentTrav> eoObjects = (NSArray<EOTypeAccidentTrav>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeAccidentTrav fetchTypeAccidentTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAccidentTrav.fetchTypeAccidentTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAccidentTrav fetchTypeAccidentTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeAccidentTrav> eoObjects = _EOTypeAccidentTrav.fetchTypeAccidentTravs(editingContext, qualifier, null);
    EOTypeAccidentTrav eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeAccidentTrav)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeAccidentTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAccidentTrav fetchRequiredTypeAccidentTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAccidentTrav.fetchRequiredTypeAccidentTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAccidentTrav fetchRequiredTypeAccidentTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeAccidentTrav eoObject = _EOTypeAccidentTrav.fetchTypeAccidentTrav(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeAccidentTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAccidentTrav localInstanceIn(EOEditingContext editingContext, EOTypeAccidentTrav eo) {
    EOTypeAccidentTrav localInstance = (eo == null) ? null : (EOTypeAccidentTrav)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
