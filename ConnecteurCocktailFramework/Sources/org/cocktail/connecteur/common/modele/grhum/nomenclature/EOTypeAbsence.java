// EOTypeAbsence.java
// Created on Fri Mar 21 12:06:29 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele.grhum.nomenclature;

import org.cocktail.common.Constantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// 12/07/2011 - Ajout du témoin congé légal
public class EOTypeAbsence extends _EOTypeAbsence {

	public final static String TYPE_STAGE = "STAGE";

    public EOTypeAbsence() {
        super();
    }


	public boolean estCongeLegal() {
		return congeLegal() != null && congeLegal().equals(Constantes.VRAI);
	}
    // Méthodes statiques
    /** retourne le type absence pour le nom de table et le type passes en param&egrave;tre
	 * typeAbsence peut &ecirc;tre nulle si il y a une seule entree dans la table pour le nom de table  */
	public static EOTypeAbsence rechercherTypeAbsencePourTable(EOEditingContext editingContext,String nomTable,String typeAbsence) {
		NSMutableArray args = new NSMutableArray(nomTable);
		//String stringQualifier = "(cTypeAbsenceHarpege = %@ OR cTypeAbsenceHarpege = nil)";
		String stringQualifier = "cTypeAbsenceHarpege = %@";
		if (typeAbsence != null) {
			args.addObject(typeAbsence);
			stringQualifier = stringQualifier + " AND cTypeAbsence = %@";
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification fs = new EOFetchSpecification("TypeAbsence",qualifier,null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return (EOTypeAbsence)results.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
}
