/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;



/** Class utilitaire pour verifier le chevauchement d'objets sur des periodes en prenant en compte la
 * quotite ou non
 * @author christine
 *
 */
public class PeriodeAvecQuotite implements NSKeyValueCoding {
	private NSTimestamp dateDebut,dateFin;
	private Number quotite;

	public PeriodeAvecQuotite(NSTimestamp dateDebut,NSTimestamp dateFin,Number quotite) {
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.quotite = quotite;
	}
	// Accesseurs
	public NSTimestamp dateDebut() {
		return dateDebut;
	}
	public void setDateDebut(NSTimestamp dateDebut) {
		this.dateDebut = dateDebut;
	}
	public NSTimestamp dateFin() {
		return dateFin;
	}
	public void setDateFin(NSTimestamp dateFin) {
		this.dateFin = dateFin;
	}

	public Number quotite() {
		return quotite;
	}
	public void setQuotite(Number quotite) {
		this.quotite = quotite;
	}
	public String toString() {
		String temp = "debut : ";
		if (dateDebut != null) {
			temp += DateCtrl.dateToString(dateDebut);
		} else {
			temp += "null";
		}
		temp += ", fin : " ;
		if (dateFin != null) {
			temp += DateCtrl.dateToString(dateFin);
		} else {
			temp += " null";
		}
		temp += ", quotite : ";
		if (quotite != null) {
			temp += quotite.toString();
		}
		return  temp;
	}

	// Interface keyValue coding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Méthodes statiques
	/** Calcule la quotite des donnees passees en param&egrave;tre. Retourne la valeur trouvee */
	public static Number calculerQuotiteTotale(NSArray periodesAvecQuotite) {
		return verifierPeriodes(periodesAvecQuotite, true);
	}
	public static boolean existeChevauchementsPeriode(NSArray periodesAvecQuotite) {
		Number result = verifierPeriodes(periodesAvecQuotite, false);
		return result != null && result.intValue() > 100;
	}
	// Calculer quotité
	private static Number verifierPeriodes(NSArray periodesAvecQuotite,boolean prendreEnCompteQuotite) {
		// On trie les périodes par ordre de date croissante, au fur et à mesure que leur quotité est ajoutée
		// on les ajoute dans les périodes à prendre en considération et on les supprime lorsqu'elles ne sont plus
		// à prendre en considération parce que leur quotité a été retirée
		// Exemple : periode1 du 01/01/07 au 31/12/07 quotité 50
		// periode2 du 01/01/07 au 31/07/07 quotité 50 et periode3 du 01/08/07 au 31/12/07 quotité 50
		// Il n'y a pas de dépassement de la quotité sur la période 1

		LogManager.logDetail("Verification des periodes avec prise en compte de la quotite " + prendreEnCompteQuotite);
		if (periodesAvecQuotite == null || periodesAvecQuotite.count() == 0) {
			return null;
		}

		NSMutableArray periodesAConsiderer = new NSMutableArray();
		// Les ranger par ordre croissant pour évaluer la quotité sur les périodes
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("dateFin", EOSortOrdering.CompareAscending));
		periodesAvecQuotite = EOSortOrdering.sortedArrayUsingKeyOrderArray(periodesAvecQuotite, sorts);
		double quotiteTotale = 0.00;
		java.util.Enumeration e = periodesAvecQuotite.objectEnumerator();
		while (e.hasMoreElements()) {
			PeriodeAvecQuotite periodeCourante = (PeriodeAvecQuotite)e.nextElement();
			if (prendreEnCompteQuotite && periodeCourante.quotite() != null) {
				quotiteTotale = quotiteTotale + periodeCourante.quotite().doubleValue();
			} else {
				quotiteTotale = quotiteTotale + 100.00;
			}
			LogManager.logDetail("quotité totale " + quotiteTotale);
			//	 Vérifier si la période courante est postérieure aux périodes prises en compte, en quel cas il ne faut
			// plus prendre en compte leur quotité

			if (periodesAConsiderer.count() > 0) {
				NSArray periodes = new NSArray(periodesAConsiderer);	// On duplique le tableau pour qu'en cas de suppression, il n'y ait pas de souci
				java.util.Enumeration e1 = periodes.objectEnumerator();
				while (e1.hasMoreElements()) {
					PeriodeAvecQuotite periodeASupprimer = (PeriodeAvecQuotite)e1.nextElement();
					if (periodeASupprimer.dateFin() != null && DateCtrl.isBefore(periodeASupprimer.dateFin(), periodeCourante.dateDebut())) {
						// La période n'a plus court sur la période en cours d'analyse
						// Enlever sa quotité et la supprimer des périodes prises en compte, les périodes étant classées
						// par date début croissant, on trouvera ensuite toujours des périodes postérieures
						LogManager.logDetail("Suppression de la quotité la periode du " + DateCtrl.dateToString(periodeASupprimer.dateDebut()));
						if (periodeASupprimer.quotite() != null) {
							quotiteTotale = quotiteTotale - periodeASupprimer.quotite().doubleValue();
						}
						LogManager.logDetail("quotité totale après suppression " + quotiteTotale);
						periodesAConsiderer.removeObject(periodeASupprimer);
					}
				}
			}
			periodesAConsiderer.addObject(periodeCourante);
		}
		return new Double(quotiteTotale);
	}
	
}
