// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumPrioriteAttribut.java instead.
package org.cocktail.connecteur.common.modele;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumPrioriteAttribut extends  EOGenericRecord {
	public static final String ENTITY_NAME = "GrhumPrioriteAttribut";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIBELLE_KEY = "libelle";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ENTITE_KEY = "entite";

  private static Logger LOG = Logger.getLogger(_EOGrhumPrioriteAttribut.class);

  public EOGrhumPrioriteAttribut localInstanceIn(EOEditingContext editingContext) {
    EOGrhumPrioriteAttribut localInstance = (EOGrhumPrioriteAttribut)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumPrioriteAttribut.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteAttribut.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumPrioriteAttribut.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteAttribut.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String libelle() {
    return (String) storedValueForKey("libelle");
  }

  public void setLibelle(String value) {
    if (_EOGrhumPrioriteAttribut.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteAttribut.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, "libelle");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOGrhumPrioriteAttribut.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteAttribut.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite entite() {
    return (org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite)storedValueForKey("entite");
  }

  public void setEntiteRelationship(org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite value) {
    if (_EOGrhumPrioriteAttribut.LOG.isDebugEnabled()) {
      _EOGrhumPrioriteAttribut.LOG.debug("updating entite from " + entite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite oldValue = entite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "entite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "entite");
    }
  }
  

  public static EOGrhumPrioriteAttribut createGrhumPrioriteAttribut(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String libelle
, String temValide
, org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite entite) {
    EOGrhumPrioriteAttribut eo = (EOGrhumPrioriteAttribut) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumPrioriteAttribut.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLibelle(libelle);
		eo.setTemValide(temValide);
    eo.setEntiteRelationship(entite);
    return eo;
  }

  public static NSArray<EOGrhumPrioriteAttribut> fetchAllGrhumPrioriteAttributs(EOEditingContext editingContext) {
    return _EOGrhumPrioriteAttribut.fetchAllGrhumPrioriteAttributs(editingContext, null);
  }

  public static NSArray<EOGrhumPrioriteAttribut> fetchAllGrhumPrioriteAttributs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumPrioriteAttribut.fetchGrhumPrioriteAttributs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumPrioriteAttribut> fetchGrhumPrioriteAttributs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumPrioriteAttribut.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumPrioriteAttribut> eoObjects = (NSArray<EOGrhumPrioriteAttribut>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumPrioriteAttribut fetchGrhumPrioriteAttribut(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPrioriteAttribut.fetchGrhumPrioriteAttribut(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPrioriteAttribut fetchGrhumPrioriteAttribut(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumPrioriteAttribut> eoObjects = _EOGrhumPrioriteAttribut.fetchGrhumPrioriteAttributs(editingContext, qualifier, null);
    EOGrhumPrioriteAttribut eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumPrioriteAttribut)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumPrioriteAttribut that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPrioriteAttribut fetchRequiredGrhumPrioriteAttribut(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPrioriteAttribut.fetchRequiredGrhumPrioriteAttribut(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPrioriteAttribut fetchRequiredGrhumPrioriteAttribut(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumPrioriteAttribut eoObject = _EOGrhumPrioriteAttribut.fetchGrhumPrioriteAttribut(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumPrioriteAttribut that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPrioriteAttribut localInstanceIn(EOEditingContext editingContext, EOGrhumPrioriteAttribut eo) {
    EOGrhumPrioriteAttribut localInstance = (eo == null) ? null : (EOGrhumPrioriteAttribut)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
