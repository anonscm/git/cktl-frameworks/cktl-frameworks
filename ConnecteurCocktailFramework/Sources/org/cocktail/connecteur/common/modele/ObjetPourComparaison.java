/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele;

import org.cocktail.connecteur.common.DateCtrl;

import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public class ObjetPourComparaison implements NSKeyValueCoding, NSCoding {
	private String cle;
	private String prioriteDestination;
	private Object valeurImport;
	private Object valeurDestination;
	
	/** Constructeur */
	public ObjetPourComparaison(String cle,Object valeurImport, Object valeurDestination,String prioriteDestination) {
		this.cle = cle;
		if (valeurImport != NSKeyValueCoding.NullValue) {
			if (valeurImport instanceof Number) {
				this.valeurImport = valeurImport.toString();
			} else if (valeurImport instanceof NSTimestamp) {
				this.valeurImport = DateCtrl.dateToString((NSTimestamp)valeurImport);
			} else {
				this.valeurImport = valeurImport;
			}
		}	
		if (valeurDestination != NSKeyValueCoding.NullValue) {
			if (valeurDestination instanceof Number) {
				this.valeurDestination = valeurDestination.toString();
			} else if (valeurDestination instanceof NSTimestamp) {
				this.valeurDestination = DateCtrl.dateToString((NSTimestamp)valeurDestination);
			} else {
				this.valeurDestination = valeurDestination;
			}
		}	
		this.prioriteDestination = prioriteDestination;

	}
	// Accesseurs
	public String cle() {
		return cle;
	}
	public Object valeurImport() {
		return valeurImport;
	}
	public Object valeurDestination() {
		return valeurDestination;
	}
	public String prioriteDestination() {
		return prioriteDestination;
	}
	public String toString() {
		String result = "cle :" + cle + ", valeurImport :" + valeurImport() + ", valeurDestination : " + valeurDestination();
		if (prioriteDestination() != null) {
			result += ", priorite destination";
		}
		result += ";";
		return result;
	}
	// Interface Key-Value Codind
	public void takeValueForKey(Object valeur, String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);

	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
	}
	// Interface NSCoding
	public void encodeWithCoder(NSCoder coder) {
		coder.encodeObject(cle);
		if (valeurImport != null) {
			coder.encodeObject(valeurImport);
		} else {
			coder.encodeObject(NSKeyValueCoding.NullValue);
		}
		if (valeurDestination != null) {
			coder.encodeObject(valeurDestination);
		} else {
			coder.encodeObject(NSKeyValueCoding.NullValue);
		}
		if (prioriteDestination != null) {
			coder.encodeObject(prioriteDestination);
		} else {
			coder.encodeObject(NSKeyValueCoding.NullValue);
		}
	}
	public static Class decodeClass() {
		return ObjetPourComparaison.class;
	}
	public static Object decodeObject(NSCoder coder) {
		String cle = (String)coder.decodeObject();
	    Object valeurImport = coder.decodeObject();
	    if (valeurImport == NSKeyValueCoding.NullValue) {
	    	valeurImport = null;
	    }
	    Object valeurDestin = coder.decodeObject();
	    if (valeurImport == NSKeyValueCoding.NullValue) {
	    	valeurDestin = null;
	    }
	    Object pri = (Object)coder.decodeObject();
	    String priorite = null;
	    if (pri != null && pri != NSKeyValueCoding.NullValue) {
	    	priorite = (String)pri;
	    } else {
	    	priorite = null;
	    }
	 
	    return new ObjetPourComparaison(cle,valeurImport,valeurDestin,priorite);
	}
	public Class classForCoder() {
		return ObjetPourComparaison.class;
	}
}
