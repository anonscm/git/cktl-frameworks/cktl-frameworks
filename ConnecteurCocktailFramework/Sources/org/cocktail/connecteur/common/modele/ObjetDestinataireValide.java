package org.cocktail.connecteur.common.modele;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;

import com.webobjects.eocontrol.EOGenericRecord;

/** Classe abstraite pour les objets du SI Destinataire.<BR>
 *  Les objets du SI Destinataire doivent forcément avoir un temoin de validite. Si ce temoin n'est pas
 * directement dans l'objet metier (voir EOGrhumAdresse), c'est de la responsabilite de l'objet metier
 * de retourner ou setter les valeurs correctes (a travers les methodes estValide et setEstValide). La revalidation des objets est appelee uniquement pour les objets 
 * en update (si l'objet destinataire est invalide alors on fait une insertion dans le SI Destinataire et non plus un
 * update pour forcer la r&egrave;gle que les donnees importees sont necessairement valides). Il est possible,
 * comme dans EOGrhumAdresse de surcharger la methode updateAvecRecord pour garder une trace de l'objet d'import.
 * @author christine
 *
 */
public abstract class ObjetDestinataireValide extends EOGenericRecord {
	public String temValide() {
		return (String)storedValueForKey("temValide");
	}

	public void setTemValide(String value) {
		takeStoredValueForKey(value, "temValide");
	}
	/** return true si l'objet est valide. A surcharger si l'objet metier n'a pas de temoin de validite direct */
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	/** Revalide l'objet si il avait ete invalide
	 * dans le SI Destinataire (l'import l'emporte sur le SI Destinataire).<BR>
	 * A surcharger si l'objet metier n'a pas de temoin de validite direct<BR> 
	 * return true si revalidation */
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}
}
