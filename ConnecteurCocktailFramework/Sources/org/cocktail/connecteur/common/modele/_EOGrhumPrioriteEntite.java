// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumPrioriteEntite.java instead.
package org.cocktail.connecteur.common.modele;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumPrioriteEntite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "GrhumPrioriteEntite";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIBELLE_KEY = "libelle";
	public static final String TEM_REVALIDER_KEY = "temRevalider";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ATTRIBUTS_KEY = "attributs";

  private static Logger LOG = Logger.getLogger(_EOGrhumPrioriteEntite.class);

  public EOGrhumPrioriteEntite localInstanceIn(EOEditingContext editingContext) {
    EOGrhumPrioriteEntite localInstance = (EOGrhumPrioriteEntite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumPrioriteEntite.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteEntite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumPrioriteEntite.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteEntite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String libelle() {
    return (String) storedValueForKey("libelle");
  }

  public void setLibelle(String value) {
    if (_EOGrhumPrioriteEntite.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteEntite.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, "libelle");
  }

  public String temRevalider() {
    return (String) storedValueForKey("temRevalider");
  }

  public void setTemRevalider(String value) {
    if (_EOGrhumPrioriteEntite.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteEntite.LOG.debug( "updating temRevalider from " + temRevalider() + " to " + value);
    }
    takeStoredValueForKey(value, "temRevalider");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOGrhumPrioriteEntite.LOG.isDebugEnabled()) {
    	_EOGrhumPrioriteEntite.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut> attributs() {
    return (NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut>)storedValueForKey("attributs");
  }

  public NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut> attributs(EOQualifier qualifier) {
    return attributs(qualifier, null, false);
  }

  public NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut> attributs(EOQualifier qualifier, boolean fetch) {
    return attributs(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut> attributs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut.ENTITE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut.fetchGrhumPrioriteAttributs(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = attributs();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAttributsRelationship(org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut object) {
    if (_EOGrhumPrioriteEntite.LOG.isDebugEnabled()) {
      _EOGrhumPrioriteEntite.LOG.debug("adding " + object + " to attributs relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "attributs");
  }

  public void removeFromAttributsRelationship(org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut object) {
    if (_EOGrhumPrioriteEntite.LOG.isDebugEnabled()) {
      _EOGrhumPrioriteEntite.LOG.debug("removing " + object + " from attributs relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "attributs");
  }

  public org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut createAttributsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GrhumPrioriteAttribut");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "attributs");
    return (org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut) eo;
  }

  public void deleteAttributsRelationship(org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "attributs");
    editingContext().deleteObject(object);
  }

  public void deleteAllAttributsRelationships() {
    Enumeration objects = attributs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAttributsRelationship((org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut)objects.nextElement());
    }
  }


  public static EOGrhumPrioriteEntite createGrhumPrioriteEntite(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String libelle
, String temRevalider
, String temValide
) {
    EOGrhumPrioriteEntite eo = (EOGrhumPrioriteEntite) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumPrioriteEntite.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLibelle(libelle);
		eo.setTemRevalider(temRevalider);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOGrhumPrioriteEntite> fetchAllGrhumPrioriteEntites(EOEditingContext editingContext) {
    return _EOGrhumPrioriteEntite.fetchAllGrhumPrioriteEntites(editingContext, null);
  }

  public static NSArray<EOGrhumPrioriteEntite> fetchAllGrhumPrioriteEntites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumPrioriteEntite.fetchGrhumPrioriteEntites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumPrioriteEntite> fetchGrhumPrioriteEntites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumPrioriteEntite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumPrioriteEntite> eoObjects = (NSArray<EOGrhumPrioriteEntite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumPrioriteEntite fetchGrhumPrioriteEntite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPrioriteEntite.fetchGrhumPrioriteEntite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPrioriteEntite fetchGrhumPrioriteEntite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumPrioriteEntite> eoObjects = _EOGrhumPrioriteEntite.fetchGrhumPrioriteEntites(editingContext, qualifier, null);
    EOGrhumPrioriteEntite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumPrioriteEntite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumPrioriteEntite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPrioriteEntite fetchRequiredGrhumPrioriteEntite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPrioriteEntite.fetchRequiredGrhumPrioriteEntite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPrioriteEntite fetchRequiredGrhumPrioriteEntite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumPrioriteEntite eoObject = _EOGrhumPrioriteEntite.fetchGrhumPrioriteEntite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumPrioriteEntite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPrioriteEntite localInstanceIn(EOEditingContext editingContext, EOGrhumPrioriteEntite eo) {
    EOGrhumPrioriteEntite localInstance = (eo == null) ? null : (EOGrhumPrioriteEntite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
