// EOGrhumPrioriteEntite.java
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOGrhumPrioriteEntite extends _EOGrhumPrioriteEntite {

    public EOGrhumPrioriteEntite() {
        super();
    }

    public void initAvecNomEntite(String nomEntite) {
    	setLibelle(nomEntite);
    	setEstValide(true);
    	setEstARevalider(false);
    	setDCreation(new NSTimestamp());
    	setDModification(dCreation());
    }
    public boolean estValide() {
    	return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
    }
    public void setEstValide(boolean aBool) {
    	if (aBool) {
    		setTemValide(CocktailConstantes.VRAI);
    	} else {
    		setTemValide(CocktailConstantes.FAUX);
    	}
    }
    public boolean estARevalider() {
    	return temRevalider() != null && temRevalider().equals(CocktailConstantes.VRAI);
    }
    public void setEstARevalider(boolean aBool) {
    	if (aBool) {
    		setTemRevalider(CocktailConstantes.VRAI);
    	} else {
    		setTemRevalider(CocktailConstantes.FAUX);
    	}
    }
    public void invalider() {
    	setEstValide(false);
    	setDModification(new NSTimestamp());
    }
    public boolean existeAttributsValides() {
    	if (attributs() == null || attributs().count() == 0) {
    		return false;
    	}
    	java.util.Enumeration e = attributs().objectEnumerator();
    	while (e.hasMoreElements()) {
    		EOGrhumPrioriteAttribut attribut = (EOGrhumPrioriteAttribut)e.nextElement();
    		if (attribut.estValide()) {
    			return true;
    		}
    	}
    	return false;
    }
    public boolean estPrioriteGlobale() {
    	return existeAttributsValides() == false;
    }
    public EOGrhumPrioriteAttribut attributPourLibelle(String libelle, boolean valideSeulement) {
    	if (attributs() == null || attributs().count() == 0) {
    		return null;
    	}
    	java.util.Enumeration e = attributs().objectEnumerator();
    	while (e.hasMoreElements()) {
    		EOGrhumPrioriteAttribut attribut = (EOGrhumPrioriteAttribut)e.nextElement();
    		if (attribut.libelle().equals(libelle)) {
    			if (!valideSeulement || attribut.estValide()) {
    				return attribut;
    			} else {
    				return null;
    			}
    		}
    	}
    	return null;
    }
    /** Retourne un tableau d'attributs valides (EOGrhumPrioriteAttribut), tableau vide si pas d'attributs valides */
    public NSArray attributsValides() {
    	if (attributs() == null || attributs().count() == 0) {
    		return new NSArray();
    	}
    	NSMutableArray attributsValides = new NSMutableArray();
    	java.util.Enumeration e = attributs().objectEnumerator();
    	while (e.hasMoreElements()) {
    		EOGrhumPrioriteAttribut attribut = (EOGrhumPrioriteAttribut)e.nextElement();
    		if (attribut.estValide()) {
    			attributsValides.addObject(attribut);
    		}
    	}
    	return new NSArray(attributsValides);
    }
    public String description() {
    	NSArray attributs = attributsValides();
    	if (attributs.count() == 0) {
    		return "priorite globale";
    	} else {
    		return "attributs : " + attributs.toString();
    	} 
    }
    // Méthodes statiques
    /** Retourne les priorites pour les entites Grhum. Si validesSeulement est false,
     * retourne toutes les priorites. Si raffraichir est vrai, effectue un refresh dans la base
     */
    public static NSArray rechercherPrioriteEntites(EOEditingContext editingContext,boolean validesSeulement,boolean raffraichir) {
    	return rechercherPrioritesEntitePourEntite(editingContext, null, validesSeulement,false,raffraichir);
    }
    /** Retourne les priorites pour les entites Grhum. Si validesSeulement est false,
     * retourne toutes les priorites
     */
    public static NSArray rechercherPrioriteEntites(EOEditingContext editingContext,boolean validesSeulement) {
    	return rechercherPrioritesEntitePourEntite(editingContext, null, validesSeulement,false,false);
    }
    /** Retourne la priorite pour une entite Grhum, null si non trouvee
     * retourne toutes les priorites
     */
    public static EOGrhumPrioriteEntite rechercherPrioriteEntitePourEntite(EOEditingContext editingContext,String nomEntite) {
    	NSArray entites = rechercherPrioritesEntitePourEntite(editingContext, nomEntite, true,true,true);
    	try {
    		return (EOGrhumPrioriteEntite)entites.objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
    // Méthodes privées
    private static NSArray rechercherPrioritesEntitePourEntite(EOEditingContext editingContext,String nomEntite,boolean validesSeulement,boolean prefetch,boolean raffraichir) {
    	NSMutableArray qualifiers = new NSMutableArray();
    	if (nomEntite != null) {
    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("libelle = %@", new NSArray(nomEntite)));
    	}
    	if (validesSeulement) {
    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temValide = 'O'", null));
    	}
    	EOFetchSpecification fs = new EOFetchSpecification("GrhumPrioriteEntite",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("libelle",EOSortOrdering.CompareAscending)));
    	if (prefetch) {
    		fs.setPrefetchingRelationshipKeyPaths(new NSArray("attributs"));
    	}
    	if (raffraichir) {
    		fs.setRefreshesRefetchedObjects(raffraichir);
    	}
    	return editingContext.objectsWithFetchSpecification(fs);
    }
 
}
