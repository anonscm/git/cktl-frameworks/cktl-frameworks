package org.cocktail.connecteur.common.modele.jefy;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOKxElement extends _EOKxElement {
	public static EOKxElement getFromIdeltEtMois(EOEditingContext editingContext, String idelt, Integer mois) {
		if (idelt==null || mois==null)
			return null;
		NSMutableArray args = new NSMutableArray(idelt, mois, mois);
		String stringQualifier = IDELT_KEY+" = %@ AND "+MOIS_CODE_DEBUT_KEY+" <= %@ AND ("+MOIS_CODE_FIN_KEY+" = nil OR "+MOIS_CODE_FIN_KEY+" >= %@)";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, myQualifier, null);
		NSArray resultats = editingContext.objectsWithFetchSpecification(fs);

		EOKxElement resultat = null;
		if (resultats.count() == 1)
			resultat = (EOKxElement) resultats.get(0);

		return resultat;
	}
}
