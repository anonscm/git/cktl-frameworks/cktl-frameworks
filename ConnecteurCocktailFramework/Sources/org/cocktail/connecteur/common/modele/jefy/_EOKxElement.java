// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOKxElement.java instead.
package org.cocktail.connecteur.common.modele.jefy;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOKxElement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "KxElement";

	// Attributes
	public static final String IDELT_KEY = "idelt";
	public static final String KELM_ID_KEY = "kelmId";
	public static final String MOIS_CODE_DEBUT_KEY = "moisCodeDebut";
	public static final String MOIS_CODE_FIN_KEY = "moisCodeFin";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOKxElement.class);

  public EOKxElement localInstanceIn(EOEditingContext editingContext) {
    EOKxElement localInstance = (EOKxElement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String idelt() {
    return (String) storedValueForKey("idelt");
  }

  public void setIdelt(String value) {
    if (_EOKxElement.LOG.isDebugEnabled()) {
    	_EOKxElement.LOG.debug( "updating idelt from " + idelt() + " to " + value);
    }
    takeStoredValueForKey(value, "idelt");
  }

  public Integer kelmId() {
    return (Integer) storedValueForKey("kelmId");
  }

  public void setKelmId(Integer value) {
    if (_EOKxElement.LOG.isDebugEnabled()) {
    	_EOKxElement.LOG.debug( "updating kelmId from " + kelmId() + " to " + value);
    }
    takeStoredValueForKey(value, "kelmId");
  }

  public Integer moisCodeDebut() {
    return (Integer) storedValueForKey("moisCodeDebut");
  }

  public void setMoisCodeDebut(Integer value) {
    if (_EOKxElement.LOG.isDebugEnabled()) {
    	_EOKxElement.LOG.debug( "updating moisCodeDebut from " + moisCodeDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCodeDebut");
  }

  public Integer moisCodeFin() {
    return (Integer) storedValueForKey("moisCodeFin");
  }

  public void setMoisCodeFin(Integer value) {
    if (_EOKxElement.LOG.isDebugEnabled()) {
    	_EOKxElement.LOG.debug( "updating moisCodeFin from " + moisCodeFin() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCodeFin");
  }


  public static EOKxElement createKxElement(EOEditingContext editingContext, String idelt
, Integer kelmId
, Integer moisCodeDebut
) {
    EOKxElement eo = (EOKxElement) EOUtilities.createAndInsertInstance(editingContext, _EOKxElement.ENTITY_NAME);    
		eo.setIdelt(idelt);
		eo.setKelmId(kelmId);
		eo.setMoisCodeDebut(moisCodeDebut);
    return eo;
  }

  public static NSArray<EOKxElement> fetchAllKxElements(EOEditingContext editingContext) {
    return _EOKxElement.fetchAllKxElements(editingContext, null);
  }

  public static NSArray<EOKxElement> fetchAllKxElements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOKxElement.fetchKxElements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOKxElement> fetchKxElements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOKxElement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOKxElement> eoObjects = (NSArray<EOKxElement>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOKxElement fetchKxElement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOKxElement.fetchKxElement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOKxElement fetchKxElement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOKxElement> eoObjects = _EOKxElement.fetchKxElements(editingContext, qualifier, null);
    EOKxElement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOKxElement)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one KxElement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOKxElement fetchRequiredKxElement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOKxElement.fetchRequiredKxElement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOKxElement fetchRequiredKxElement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOKxElement eoObject = _EOKxElement.fetchKxElement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no KxElement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOKxElement localInstanceIn(EOEditingContext editingContext, EOKxElement eo) {
    EOKxElement localInstance = (eo == null) ? null : (EOKxElement)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
