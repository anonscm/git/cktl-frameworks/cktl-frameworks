// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOLolfNomenclatureDepense.java instead.
package org.cocktail.connecteur.common.modele.jefy;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLolfNomenclatureDepense extends  EOGenericRecord {
	public static final String ENTITY_NAME = "LolfNomenclatureDepense";

	// Attributes
	public static final String LOLF_CODE_KEY = "lolfCode";
	public static final String LOLF_FERMETURE_KEY = "lolfFermeture";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String LOLF_NIVEAU_KEY = "LolfNiveau";
	public static final String LOLF_OUVERTURE_KEY = "lolfOuverture";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOLolfNomenclatureDepense.class);

  public EOLolfNomenclatureDepense localInstanceIn(EOEditingContext editingContext) {
    EOLolfNomenclatureDepense localInstance = (EOLolfNomenclatureDepense)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String lolfCode() {
    return (String) storedValueForKey("lolfCode");
  }

  public void setLolfCode(String value) {
    if (_EOLolfNomenclatureDepense.LOG.isDebugEnabled()) {
    	_EOLolfNomenclatureDepense.LOG.debug( "updating lolfCode from " + lolfCode() + " to " + value);
    }
    takeStoredValueForKey(value, "lolfCode");
  }

  public NSTimestamp lolfFermeture() {
    return (NSTimestamp) storedValueForKey("lolfFermeture");
  }

  public void setLolfFermeture(NSTimestamp value) {
    if (_EOLolfNomenclatureDepense.LOG.isDebugEnabled()) {
    	_EOLolfNomenclatureDepense.LOG.debug( "updating lolfFermeture from " + lolfFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "lolfFermeture");
  }

  public Integer lolfId() {
    return (Integer) storedValueForKey("lolfId");
  }

  public void setLolfId(Integer value) {
    if (_EOLolfNomenclatureDepense.LOG.isDebugEnabled()) {
    	_EOLolfNomenclatureDepense.LOG.debug( "updating lolfId from " + lolfId() + " to " + value);
    }
    takeStoredValueForKey(value, "lolfId");
  }

  public Integer LolfNiveau() {
    return (Integer) storedValueForKey("LolfNiveau");
  }

  public void setLolfNiveau(Integer value) {
    if (_EOLolfNomenclatureDepense.LOG.isDebugEnabled()) {
    	_EOLolfNomenclatureDepense.LOG.debug( "updating LolfNiveau from " + LolfNiveau() + " to " + value);
    }
    takeStoredValueForKey(value, "LolfNiveau");
  }

  public NSTimestamp lolfOuverture() {
    return (NSTimestamp) storedValueForKey("lolfOuverture");
  }

  public void setLolfOuverture(NSTimestamp value) {
    if (_EOLolfNomenclatureDepense.LOG.isDebugEnabled()) {
    	_EOLolfNomenclatureDepense.LOG.debug( "updating lolfOuverture from " + lolfOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "lolfOuverture");
  }


  public static EOLolfNomenclatureDepense createLolfNomenclatureDepense(EOEditingContext editingContext, String lolfCode
, Integer lolfId
, Integer LolfNiveau
, NSTimestamp lolfOuverture
) {
    EOLolfNomenclatureDepense eo = (EOLolfNomenclatureDepense) EOUtilities.createAndInsertInstance(editingContext, _EOLolfNomenclatureDepense.ENTITY_NAME);    
		eo.setLolfCode(lolfCode);
		eo.setLolfId(lolfId);
		eo.setLolfNiveau(LolfNiveau);
		eo.setLolfOuverture(lolfOuverture);
    return eo;
  }

  public static NSArray<EOLolfNomenclatureDepense> fetchAllLolfNomenclatureDepenses(EOEditingContext editingContext) {
    return _EOLolfNomenclatureDepense.fetchAllLolfNomenclatureDepenses(editingContext, null);
  }

  public static NSArray<EOLolfNomenclatureDepense> fetchAllLolfNomenclatureDepenses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLolfNomenclatureDepense.fetchLolfNomenclatureDepenses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLolfNomenclatureDepense> fetchLolfNomenclatureDepenses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLolfNomenclatureDepense.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLolfNomenclatureDepense> eoObjects = (NSArray<EOLolfNomenclatureDepense>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLolfNomenclatureDepense fetchLolfNomenclatureDepense(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLolfNomenclatureDepense.fetchLolfNomenclatureDepense(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLolfNomenclatureDepense fetchLolfNomenclatureDepense(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLolfNomenclatureDepense> eoObjects = _EOLolfNomenclatureDepense.fetchLolfNomenclatureDepenses(editingContext, qualifier, null);
    EOLolfNomenclatureDepense eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLolfNomenclatureDepense)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one LolfNomenclatureDepense that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLolfNomenclatureDepense fetchRequiredLolfNomenclatureDepense(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLolfNomenclatureDepense.fetchRequiredLolfNomenclatureDepense(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLolfNomenclatureDepense fetchRequiredLolfNomenclatureDepense(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLolfNomenclatureDepense eoObject = _EOLolfNomenclatureDepense.fetchLolfNomenclatureDepense(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no LolfNomenclatureDepense that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLolfNomenclatureDepense localInstanceIn(EOEditingContext editingContext, EOLolfNomenclatureDepense eo) {
    EOLolfNomenclatureDepense localInstance = (eo == null) ? null : (EOLolfNomenclatureDepense)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
