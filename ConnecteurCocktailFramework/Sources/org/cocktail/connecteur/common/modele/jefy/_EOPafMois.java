// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPafMois.java instead.
package org.cocktail.connecteur.common.modele.jefy;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPafMois extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PafMois";

	// Attributes
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String MOIS_DEBUT_KEY = "moisDebut";
	public static final String MOIS_FIN_KEY = "moisFin";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOPafMois.class);

  public EOPafMois localInstanceIn(EOEditingContext editingContext) {
    EOPafMois localInstance = (EOPafMois)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer moisCode() {
    return (Integer) storedValueForKey("moisCode");
  }

  public void setMoisCode(Integer value) {
    if (_EOPafMois.LOG.isDebugEnabled()) {
    	_EOPafMois.LOG.debug( "updating moisCode from " + moisCode() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCode");
  }

  public NSTimestamp moisDebut() {
    return (NSTimestamp) storedValueForKey("moisDebut");
  }

  public void setMoisDebut(NSTimestamp value) {
    if (_EOPafMois.LOG.isDebugEnabled()) {
    	_EOPafMois.LOG.debug( "updating moisDebut from " + moisDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "moisDebut");
  }

  public NSTimestamp moisFin() {
    return (NSTimestamp) storedValueForKey("moisFin");
  }

  public void setMoisFin(NSTimestamp value) {
    if (_EOPafMois.LOG.isDebugEnabled()) {
    	_EOPafMois.LOG.debug( "updating moisFin from " + moisFin() + " to " + value);
    }
    takeStoredValueForKey(value, "moisFin");
  }


  public static EOPafMois createPafMois(EOEditingContext editingContext, Integer moisCode
, NSTimestamp moisDebut
, NSTimestamp moisFin
) {
    EOPafMois eo = (EOPafMois) EOUtilities.createAndInsertInstance(editingContext, _EOPafMois.ENTITY_NAME);    
		eo.setMoisCode(moisCode);
		eo.setMoisDebut(moisDebut);
		eo.setMoisFin(moisFin);
    return eo;
  }

  public static NSArray<EOPafMois> fetchAllPafMoises(EOEditingContext editingContext) {
    return _EOPafMois.fetchAllPafMoises(editingContext, null);
  }

  public static NSArray<EOPafMois> fetchAllPafMoises(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPafMois.fetchPafMoises(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPafMois> fetchPafMoises(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPafMois.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPafMois> eoObjects = (NSArray<EOPafMois>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPafMois fetchPafMois(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPafMois.fetchPafMois(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPafMois fetchPafMois(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPafMois> eoObjects = _EOPafMois.fetchPafMoises(editingContext, qualifier, null);
    EOPafMois eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPafMois)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PafMois that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPafMois fetchRequiredPafMois(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPafMois.fetchRequiredPafMois(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPafMois fetchRequiredPafMois(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPafMois eoObject = _EOPafMois.fetchPafMois(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PafMois that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPafMois localInstanceIn(EOEditingContext editingContext, EOPafMois eo) {
    EOPafMois localInstance = (eo == null) ? null : (EOPafMois)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
