package org.cocktail.connecteur.common.modele.jefy;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOTypeCredit extends _EOTypeCredit {
	private static final String CODE_PERSONNEL = "30";

	private static EOTypeCredit typeCreditPersonnel = null;

	public static EOTypeCredit getFromId(EOEditingContext editingContext, Integer tcdOrdre) {
		return fetchTypeCredit(editingContext, TCD_ORDRE_KEY, tcdOrdre);
	}

	public static EOTypeCredit typePersonnel(EOEditingContext editingContext) {
		if (typeCreditPersonnel == null) {
			NSMutableArray args = new NSMutableArray(CODE_PERSONNEL);
			String stringQualifier = TCD_CODE_KEY + " = %@";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);

			EOSortOrdering ordering = new EOSortOrdering(EXE_ORDRE_KEY, EOSortOrdering.CompareDescending);
			NSMutableArray orderings = new NSMutableArray(ordering);

			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, myQualifier, orderings);
			NSArray resultats = editingContext.objectsWithFetchSpecification(fs);

			if (resultats.count() > 0)
				typeCreditPersonnel=(EOTypeCredit)resultats.get(0);
		}

		return typeCreditPersonnel;
	}
}
