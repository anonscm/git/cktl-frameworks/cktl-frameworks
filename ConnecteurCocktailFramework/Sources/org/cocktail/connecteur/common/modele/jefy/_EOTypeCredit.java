// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeCredit.java instead.
package org.cocktail.connecteur.common.modele.jefy;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeCredit extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeCredit";

	// Attributes
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TCD_ABREGE_KEY = "tcdAbrege";
	public static final String TCD_BUDGET_KEY = "tcdBudget";
	public static final String TCD_CODE_KEY = "tcdCode";
	public static final String TCD_LIBELLE_KEY = "tcdLibelle";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TCD_SECT_KEY = "tcdSect";
	public static final String TCD_TYPE_KEY = "tcdType";
	public static final String TYET_ID_KEY = "tyetId";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeCredit.class);

  public EOTypeCredit localInstanceIn(EOEditingContext editingContext) {
    EOTypeCredit localInstance = (EOTypeCredit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey("exeOrdre");
  }

  public void setExeOrdre(Integer value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating exeOrdre from " + exeOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "exeOrdre");
  }

  public String tcdAbrege() {
    return (String) storedValueForKey("tcdAbrege");
  }

  public void setTcdAbrege(String value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tcdAbrege from " + tcdAbrege() + " to " + value);
    }
    takeStoredValueForKey(value, "tcdAbrege");
  }

  public String tcdBudget() {
    return (String) storedValueForKey("tcdBudget");
  }

  public void setTcdBudget(String value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tcdBudget from " + tcdBudget() + " to " + value);
    }
    takeStoredValueForKey(value, "tcdBudget");
  }

  public String tcdCode() {
    return (String) storedValueForKey("tcdCode");
  }

  public void setTcdCode(String value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tcdCode from " + tcdCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tcdCode");
  }

  public String tcdLibelle() {
    return (String) storedValueForKey("tcdLibelle");
  }

  public void setTcdLibelle(String value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tcdLibelle from " + tcdLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tcdLibelle");
  }

  public Integer tcdOrdre() {
    return (Integer) storedValueForKey("tcdOrdre");
  }

  public void setTcdOrdre(Integer value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tcdOrdre from " + tcdOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "tcdOrdre");
  }

  public String tcdSect() {
    return (String) storedValueForKey("tcdSect");
  }

  public void setTcdSect(String value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tcdSect from " + tcdSect() + " to " + value);
    }
    takeStoredValueForKey(value, "tcdSect");
  }

  public String tcdType() {
    return (String) storedValueForKey("tcdType");
  }

  public void setTcdType(String value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tcdType from " + tcdType() + " to " + value);
    }
    takeStoredValueForKey(value, "tcdType");
  }

  public Integer tyetId() {
    return (Integer) storedValueForKey("tyetId");
  }

  public void setTyetId(Integer value) {
    if (_EOTypeCredit.LOG.isDebugEnabled()) {
    	_EOTypeCredit.LOG.debug( "updating tyetId from " + tyetId() + " to " + value);
    }
    takeStoredValueForKey(value, "tyetId");
  }


  public static EOTypeCredit createTypeCredit(EOEditingContext editingContext, Integer exeOrdre
, String tcdAbrege
, String tcdCode
, String tcdLibelle
, Integer tcdOrdre
, String tcdSect
, Integer tyetId
) {
    EOTypeCredit eo = (EOTypeCredit) EOUtilities.createAndInsertInstance(editingContext, _EOTypeCredit.ENTITY_NAME);    
		eo.setExeOrdre(exeOrdre);
		eo.setTcdAbrege(tcdAbrege);
		eo.setTcdCode(tcdCode);
		eo.setTcdLibelle(tcdLibelle);
		eo.setTcdOrdre(tcdOrdre);
		eo.setTcdSect(tcdSect);
		eo.setTyetId(tyetId);
    return eo;
  }

  public static NSArray<EOTypeCredit> fetchAllTypeCredits(EOEditingContext editingContext) {
    return _EOTypeCredit.fetchAllTypeCredits(editingContext, null);
  }

  public static NSArray<EOTypeCredit> fetchAllTypeCredits(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeCredit.fetchTypeCredits(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeCredit> fetchTypeCredits(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeCredit.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeCredit> eoObjects = (NSArray<EOTypeCredit>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeCredit fetchTypeCredit(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCredit.fetchTypeCredit(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCredit fetchTypeCredit(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeCredit> eoObjects = _EOTypeCredit.fetchTypeCredits(editingContext, qualifier, null);
    EOTypeCredit eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeCredit)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeCredit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCredit fetchRequiredTypeCredit(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCredit.fetchRequiredTypeCredit(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCredit fetchRequiredTypeCredit(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeCredit eoObject = _EOTypeCredit.fetchTypeCredit(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeCredit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCredit localInstanceIn(EOEditingContext editingContext, EOTypeCredit eo) {
    EOTypeCredit localInstance = (eo == null) ? null : (EOTypeCredit)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
