package org.cocktail.connecteur.common.modele.jefy;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOLolfNomenclatureDepense extends _EOLolfNomenclatureDepense {

	public static EOLolfNomenclatureDepense lolfFromCodeEtNiveau(EOEditingContext editingContext, String lolfCode, Integer lolfNiveau) {
		NSMutableArray args = new NSMutableArray(lolfCode, lolfNiveau);
		String stringQualifier = LOLF_CODE_KEY+" = %@ AND "+LOLF_NIVEAU_KEY+" = %@";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		// On retroune le + récent
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(LOLF_OUVERTURE_KEY, EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, myQualifier, sorts);
		NSArray resultats = editingContext.objectsWithFetchSpecification(fs);

		EOLolfNomenclatureDepense resultat = null;
		if (resultats.count() >= 1)
			resultat = (EOLolfNomenclatureDepense) resultats.get(0);

		return resultat;
	}
}
