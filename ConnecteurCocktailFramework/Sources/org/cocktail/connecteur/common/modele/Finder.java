/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.common.modele;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.importer.EOErreurImport;
import org.cocktail.connecteur.serveur.modele.importer.EOFichierImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/** Regroupe toutes les methodes statiques communes au serveur et au client*/
public class Finder {
	/** Retourne l'import courant. On retourne un EOGenericRecord pour faire le bon cast sur le client ou le serveur */
	public static EOGenericRecord importCourant(EOEditingContext editingContext) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temValide = 'O'", null);
		EOFetchSpecification fs = new EOFetchSpecification("FichierImport",qualifier,null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOGenericRecord)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EOGenericRecord lastImport(EOEditingContext editingContext) {
		try {
			EOFetchSpecification fs = new EOFetchSpecification(EOFichierImport.ENTITY_NAME, null, 
					new NSArray(EOSortOrdering.sortOrderingWithKey(EOFichierImport.D_MODIFICATION_KEY, EOSortOrdering.CompareDescending)));
			fs.setFetchLimit(1);
			fs.setRefreshesRefetchedObjects(true);
			return (EOGenericRecord)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Retourne le record d'erreur courant. On retourne un EOGenericRecord pour faire le bon cast sur le client ou le serveur */
	public static EOGenericRecord erreurCourante(EOEditingContext editingContext) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOErreurImport.TEM_VALIDE_KEY + " = 'O'", null);
		EOFetchSpecification fs = new EOFetchSpecification("ErreurImport",qualifier,null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOGenericRecord)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}  
	/** retourne le generic record pour la globalID passee en param&egrave;tre dans l'editing context */
	public static EOGenericRecord objetForGlobalIDDansEditingContext(EOGlobalID globalID,EOEditingContext editingContext) {
		return (EOGenericRecord)editingContext.faultForGlobalID(globalID,editingContext);
	}

	// UtilitairesGraphiques de recherche
	/** Recherche toutes les entites definies par le qualifier fourni en param&egrave;tre
	 * @param editingContext
	 * @param nomEntite 	a rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @param forceRefresh true si forcer le refresh des objets
	 * @return objets trouves
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon,boolean forceRefresh) {
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,null);
		if(!nomEntite.equals("PhotosEmployes") && !nomEntite.equals("EmploiType") && !nomEntite.equals("PhotosStructuresGrhum")) {
			fs.setUsesDistinct(sansDoublon);
		}
		fs.setRefreshesRefetchedObjects(forceRefresh);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Recherche toutes les entites definies par le qualifier fourni en param&egrave;tre
	 * @param editingContext
	 * @param nomEntite 	a rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @return objets trouves
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon) {
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,sansDoublon,false);
	}
	/** Retourne le contenu complet d'une table sans qualification des entites
	 * @param editingContext
	 * @param nomEntite a rechercher
	 * @return objets trouves
	 */
	public static NSArray rechercherEntite(EOEditingContext editingContext,String nomEntite) {
		return rechercherAvecQualifier(editingContext,nomEntite,null,true);
	}
	/** Retourne le contenu complet d'une table sans qualification des entites et avec un refresh
	 * @param editingContext
	 * @param nomEntite a rechercher
	 * @return objets trouves
	 */
	public static NSArray rechercherEntiteAvecRefresh(EOEditingContext editingContext,String nomEntite) {
		return rechercherAvecQualifier(editingContext,nomEntite,null,true,true);
	}
	/** Recherche toutes les entites dont un champ a une certaine valeur
	 * @param editingContext
	 * @param nomEntite 	a rechercher
	 * @param nomLabel	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ a recherche
	 * @return objets trouves
	 */
	public static NSArray rechercherAvecAttribut(EOEditingContext editingContext,String nomEntite,String nomLabel,String valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomLabel + " caseInsensitiveLike %@",values);
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,true);
	}
	/** Recherche toutes les entites dont un champ a une certaine valeur
	 * @param editingContext
	 * @param nomEntite 	a rechercher
	 * @param nomLabel	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ a recherche
	 * @return objets trouves
	 */
	public static NSArray rechercherAvecAttribut(EOEditingContext editingContext,String nomEntite,String nomLabel,Integer valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomLabel + " = %@",values);
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,true);
	}
	/** Recherche toutes les entites dont un champ a une certaine valeur en faisant une egalite
	 * @param editingContext
	 * @param nomEntite 	a rechercher
	 * @param nomLabel	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ a recherche
	 * @return objets trouves
	 */
	public static NSArray rechercherAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String nomLabel,Object valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomLabel + " = %@",values);
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,true,true);
	}
	/** Recherche l'entite dont un champ a une certaine valeur en faisant une egalite
	 * @param editingContext
	 * @param nomEntite 	a rechercher
	 * @param nomLabel	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ a recherche
	 * @return objet trouve
	 */
	public static EOGenericRecord rechercherObjetAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String nomLabel,Object valeur) {
		NSArray resultats = rechercherAvecAttributEtValeurEgale(editingContext,nomEntite,nomLabel,valeur);
		try {
			return (EOGenericRecord)resultats.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** retourne une date formatee sous forme de String : le format est jj/mm/aaaa
	 * @param record	record pour lequel on doit formatter une date
	 * @param cle cle du champ date
	 * @return date formatee
	 */
	public static String dateFormatee(EOGenericRecord record,String cle) {
		if (record.valueForKey(cle) == null) {
			return "";
		} else {	
			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
			return formatter.format(record.valueForKey(cle));
		}
	}

	/** transforme une date fournie sous forme de string en une NSTimestamp. Si la date n'est pas compl&egrave;te (i.e jj/mm/aaaa), elle
	 * sera automatiquement completee
	 * @param record record pour lequel on doit fournir une date (NSTimestamp)
	 * @param cle cle du champ date
	 * @param uneDate valeur de la date (String)
	 */
	public static void setDateFormatee(EOGenericRecord record,String cle,String uneDate) {
		if (uneDate == null) {
			record.takeValueForKey(null,cle);
		}
		String myDate = DateCtrl.dateCompletion((String)uneDate);
		if (myDate.equals("")) {
			record.takeValueForKey(null,cle);
		} else {
			record.takeValueForKey(DateCtrl.stringToDate(myDate),cle);
		} 
	}
	/** retourne une date formatee sous forme de String : le format est jj/mm/aaaa
	 * @param object	objet pour lequel on doit formatter une date (doit implementer le key-value coding)
	 * @param cle cle du champ date
	 * @return date formatee
	 */
	public static String dateFormatee(NSKeyValueCoding object,String cle) {
		if (object.valueForKey(cle) == null) {
			return "";
		} else {	
			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
			return formatter.format(object.valueForKey(cle));
		}
	}

	/** transforme une date fournie sous forme de string en une NSTimestamp. Si la date n'est pas compl&egrave;te (i.e jj/mm/aaaa), elle
	 * sera automatiquement completee
	 * @param object objet pour lequel on doit fournir une date (NSTimestamp)  (doit implementer le key-value coding)
	 * @param cle cle du champ date
	 * @param uneDate valeur de la date (String)
	 */
	public static void setDateFormatee(NSKeyValueCoding object,String cle,String uneDate) {
		if (uneDate == null) {
			object.takeValueForKey(null,cle);
		}
		String myDate = DateCtrl.dateCompletion((String)uneDate);
		if (myDate.equals("")) {
			object.takeValueForKey(null,cle);
		} else {
			object.takeValueForKey(DateCtrl.stringToDate(myDate),cle);
		} 
	}
	/** Retourne le qualifier pour determiner les objets valides pour une periode
	 * @param champDateDebut	nom du champ de date debut sur lequel construire le qualifier
	 * @param debutPeriode	date de debut de periode (ne peut pas &ecirc;tre nulle)
	 * @param champDateFin	nom du champ de date fin sur lequel construire le qualifier
	 * @param finPeriode	date de fin de periode
	 * @return qualifier construit ou null si date debut est nulle
	 */
	public static EOQualifier qualifierPourPeriode(String champDateDebut,NSTimestamp debutPeriode,String champDateFin,NSTimestamp finPeriode) {
		if (debutPeriode == null) {
			return null;
		}
		String stringQualifier = "";
		NSMutableArray args = new NSMutableArray(debutPeriode);
		// champDateFin = nil or champDateFin >= debutPeriode
		stringQualifier = "(" + champDateFin + " = nil OR " + champDateFin + " >= %@)";
		if (finPeriode != null) {
			args.addObject(finPeriode);
			// champDateDebut <= finPeriode
			stringQualifier = stringQualifier + " AND (" + champDateDebut + " <= %@)";
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
	}
	/** Construit un OR qualifier pour l'attribut passe en param&egrave;tre &grave; partir des valeurs fournies
	 * @param attribut attribut sur lequel construire le qualifier
	 * @param valeurs	liste des valeurs recherchees
	 * @return qualifier construit ou null si liste des valeurs vides
	 */
	public static EOQualifier construireORQualifier(String attribut,NSArray valeurs) {
		return construireQualifier(attribut,valeurs," OR ");
	}
	/** Construit un AND qualifier pour l'attribut passe en param&egrave;tre &grave; partir des valeurs fournies
	 * @param attribut attribut sur lequel construire le qualifier
	 * @param valeurs	liste des valeurs recherchees
	 * @return qualifier construit ou null si liste des valeurs vides
	 */
	public static EOQualifier construireAndQualifier(String attribut,NSArray valeurs) {
		return construireQualifier(attribut,valeurs," AND ");
	}
	/** Supprime le dernier operateur du string qualifier
	 * @param stringQualifier string qualifier pour lequel on veut supprimer l'operator
	 * @param operateurASupprimer	operateur a supprimer :
	 * @return qualifier construit ou null si liste des valeurs vides
	 */
	public static String nettoyerQualifier(String stringQualifier,String operateurASupprimer) {
		int lastIndex = stringQualifier.lastIndexOf(operateurASupprimer);
		if (lastIndex > 0) {
			stringQualifier = stringQualifier.substring(0,lastIndex);
		}
		return stringQualifier;
	}
	// méthodes privées
	private static EOQualifier construireQualifier(String attribut,NSArray valeurs,String operateur) {
		String stringQualifier = "";
		if (valeurs == null || valeurs.count() == 0) {
			return null;
		}
		java.util.Enumeration e = valeurs.objectEnumerator();
		while (e.hasMoreElements()) {
			Object valeur = (Object)e.nextElement();
			stringQualifier = stringQualifier + attribut + " = '" + valeur + "'" + operateur;

		}
		stringQualifier = nettoyerQualifier(stringQualifier,operateur);
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,null);
	}

}
