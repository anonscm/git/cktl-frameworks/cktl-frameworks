/*
 * Created on 23 sept. 2004
 *
 *	Execute le parsing XML
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.modele;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author christine
 *
 * AnalyseurXML
 * Classe dont les methodes sont invoquees comme callbacks lors du parsing du fichier XML contenant les specifications
 * du mod&egrave;le. On l'utilise :
 * - pour importer les donnees des fichiers XML  dans le fichier de description le nom d'entite correspond au nom de table
 *  et l'autre nom au nom d'entite dans le mod&egrave;le<BR>
 * - pour transferer les donnees dans la base destinataire : dans le fichier de description le nom d'entite correspond 
 * au nom d'entite dans le mod&egrave;le de la base d'import et l'autre nom au nom d'entite dans le mod&egrave;le de la base
 * Destinataire
 */
public class AnalyseurXML extends DefaultHandler {
	private Entite entiteCourante;
	private Attribut attributCourant;
	private String attributEnCours;
	private boolean litElement;
	private Modele modeleDonnees;

	/** Constructeur
	 * 
	 * @param dictionnaire : contiendra les entites lues, le nom d'entite servant de cle
	 */
	public AnalyseurXML(Modele modeleDonnees) {
		super();
		this.modeleDonnees = modeleDonnees;
		litElement = false;
	}
	public void startElement(String namespaceURI, String localName, String qName,Attributes atts) throws SAXException {
		litElement = true;
		attributEnCours = "";
		if (localName.equals("entite")) {
			attributCourant = null;
			String nomSource = atts.getValue("nom_source");
			String nomDestination = atts.getValue("nom_destination");
			String priorite = atts.getValue("priorite");
			entiteCourante = modeleDonnees.ajouterEntiteAvecNomsEtPriorite(nomSource,nomDestination,new Integer(priorite));
		} else if (localName.equals("attribut")) {
			String nomSource = atts.getValue("nom_source");
			String nomDestination = atts.getValue("nom_destination");
			boolean estObligatoire = (atts.getValue("obligatoire") != null && atts.getValue("obligatoire").equals("oui"));
			boolean estAttributComparaison = (atts.getValue("attribut_comparaison") != null && atts.getValue("attribut_comparaison").equals("oui"));
			String type = atts.getValue("type");
			String lgStr = atts.getValue("longueur");
			int lgAttribut = Attribut.LONGUEUR_INCONNUE;
			if (lgStr != null) {
				lgAttribut = new Integer(lgStr).intValue();
				if (lgAttribut <= 0) {
					lgAttribut = Attribut.LONGUEUR_INCONNUE;
				}
			}
			String temp = atts.getValue("libelle_affichage");
			int positionLibelleAffichage = 0;
			if (temp != null) {
				try {
					positionLibelleAffichage = new Integer(temp).intValue();
				} catch (Exception e) {}
			}
			String commentaire = atts.getValue("commentaire");
			String valeurParDefaut = atts.getValue("valeur_par_defaut");
			
			boolean aLongueurFixe = (atts.getValue("fixe") != null) && atts.getValue("fixe").equals("oui");
			attributCourant = new Attribut(nomSource,nomDestination, type,lgAttribut,positionLibelleAffichage,commentaire,estObligatoire,aLongueurFixe,estAttributComparaison,valeurParDefaut);
			entiteCourante.ajouterAttribut(attributCourant);
		}
	}

	public void characters(char[] ch,int start,int length) throws SAXException {
		if (litElement) {		
			attributEnCours = attributEnCours.concat(new String(ch,start,length));
		}
	}
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		litElement = false;
	}
}
