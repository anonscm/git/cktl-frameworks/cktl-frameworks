/*
 * Created on 17 mai 2006
 *
 * Contient la description des attributs d'impression
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.modele;


/**
 * @author christine
 *
 * Contient la description des attributs
 */
// 05/07/2011 - Modification pour supporter les attributs libelle_affichage (position dans le libellé d'affichage, 
// si pas fourni alors l'attribut n'apparaît pas dans le libellé) et commentaire (pour afficher dans l'interface générique)

public class Attribut {
	public final static int LONGUEUR_INCONNUE = -1;
	private String nomSource,nomDestination;
	private boolean estObligatoire,aLongueurFixe,estAttributComparaison;
	private int positionDansLibelleAffichage = 0;
	private Object valeurParDefaut;
	private String type;
	private String commentaire;
	private int lgMax;

	// constructeur
	/**
	 * @param nomSource nom de l'attribut source
	 * @param nomDestination nom de l'attribut destination
	 * @param type type de l'attribut
	 * @param lgAttribut longueur maximale des donnees
	 * @param estObligatoire
	 * @para 
	 */
	public Attribut(String nomSource,String nomDestination, String type,int lgAttribut, int positionDansLibelleAffichage,String commentaire, boolean estObligatoire,boolean aLongueurFixe,boolean estAttributComparaison,String valeurParDefaut) {
		this.nomSource = nomSource;
		this.nomDestination = nomDestination;
		this.type = type;
		lgMax = lgAttribut;
		this.estObligatoire = estObligatoire;
		this.aLongueurFixe = aLongueurFixe;
		this.estAttributComparaison = estAttributComparaison;
		this.valeurParDefaut = valeurParDefaut;
		this.positionDansLibelleAffichage = positionDansLibelleAffichage;
		this.commentaire = commentaire;
	}
	// accesseurs
	/**
	 * @return Retourne le nom de l'attribut source
	 */
	public String nomSource() {
		return nomSource;
	}
	/**
	 * @return Retourne le nom de l'attribut destination
	 */
	public String nomDestination() {
		return nomDestination;
	}
	/**
	 * @return Retourne le type de l'attribut
	 */
	public String type() {
		return type;
	}
	/**
	 * @return Retourne la longueur maximum de l'attribut
	 */
	public int lgMax() {
		return lgMax;
	}
	/**
	 * @return Retourne le commentaire lie a l'attribut
	 */
	public String commentaire() {
		return commentaire;
	}
	/**
	 * @return Retourne la position dans le libelle d'affichage pour l'interface generique
	 */
	public int positionDansLibelleAffichage() {
		return positionDansLibelleAffichage;
	}
	/**
	 * @return Retourne true si l'attribut doit toujours &ecirc;tre fourni
	 */
	public boolean estObligatoire() {
		return estObligatoire;
	}
	/**
	 * @return Retourne true si l'attribut est utilise dans les comparaisons de record dans le fichier XML
	 */
	public boolean estAttributComparaison() {
		return estAttributComparaison;
	}
	/**
	 * @return Retourne true si l'attribut doit avoir une longueur fixe
	 */
	public boolean aLongueurFixe() {
		return aLongueurFixe;
	}
	/**
	 * @return Retourne true si il faut prendre en compte la longueur de l'attribut
	 */
	public boolean longueurAPrendreEnCompte() {
		return lgMax() != LONGUEUR_INCONNUE;
	}
	/**
	 * @return Retourne true si l'attribut est de type String
	 */
	public boolean estTypeString() {
		return type() != null && type().equals("string");
	}
	/**
	 * @return Retourne true si l'attribut est de type entier
	 */
	public boolean estTypeEntier(){
		return type() != null && type().equals("entier");
	}
	/**
	 * @return Retourne true si l'attribut est de type decimal
	 */
	public boolean estTypeDecimal() {
		return type() != null && type().equals("decimal");
	}
	/**
	 * @return Retourne true si il l'attribut est de type Date
	 */
	public boolean estTypeDate() {
		return type() != null && type().equals("date");
	}
	
	/**
	 * @return Retourne la valeur par defaut de l'attribut (peut etre une string, un nombre ou une date)
	 */
	public Object valeurParDefaut() {
		return valeurParDefaut;
	}
	public String toString() {
		String res = nomSource();
		res = res + " de type " + type();
		if (estObligatoire()) {
			res = res + " obligatoire";
		}
		return res;
	}
	
}
