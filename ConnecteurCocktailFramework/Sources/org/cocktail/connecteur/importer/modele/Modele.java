/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.modele;

import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportAvecSource;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuEtPeriode;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuOuStructure;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** classe pour décrire le mod&egrave;le */
// 05/07/2011 - Modifier pour retourner la liste des entités affichables dans l'interface générique. Rajout d'un booléen pour indiquer
// si il s'agit du modèle d'import
public class Modele {
	private static final String ENTITE_INCONNUE = "Inconnu";
	private NSMutableDictionary dictionnaireEntites;	// pour stocker les entités (Entite), la clé est le nom
	private boolean estModeleImport;
	private NSArray entitesAffichageGenerique = null;

	public Modele(boolean estModeleImport) {
		this(new NSMutableDictionary());
		this.estModeleImport = estModeleImport;
	}
	public Modele(NSMutableDictionary dictionnaire) {
		this.dictionnaireEntites = dictionnaire;
	}
	
	/**
	 * 
	 * @param nom
	 * @return
	 */
	public Entite entiteAvecNomSource(String nom) {
		java.util.Enumeration e = dictionnaireEntites.objectEnumerator();
		while (e.hasMoreElements()) {
			Entite entite = (Entite)e.nextElement();
			if (entite.nomSource().equals(nom) || entite.nomSource().toUpperCase().equals(nom.toUpperCase())) {
				return entite;
			}
		}
		return null;
	}
	public Entite entiteAvecNomDestination(String nom) {
		return (Entite)dictionnaireEntites.objectForKey(nom);
	}
	public Entite ajouterEntiteAvecNomsEtPriorite(String nomSource,String nomDestination,Integer priorite) {
		Entite entite = new Entite(nomSource,nomDestination,priorite);
		dictionnaireEntites.setObjectForKey(entite,nomDestination);
		return entite;
	}
	
	/** 
	 * retourne la liste des entites (String) classees par ordre de priorite
	 * @param nomCle critere retenu pour retourner les noms d'entites (2 valeurs possibles : nomSource et nomDestination). 
	 * Supprime les entites qui ont comme non "Inconnu" dans le nom source ou le nom destination 
	 * 
	 */
	public NSArray<String> nomEntitesTrieesParPriorite(String nomCle) {
		NSArray<String> entitesTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(dictionnaireEntites.allValues(), new NSArray(EOSortOrdering.sortOrderingWithKey("priorite", EOSortOrdering.CompareAscending)));
		NSArray<String> temp = (NSArray<String>)entitesTriees.valueForKey(nomCle);
		// Pour pouvoir gérer des entités qui ont un nom source ou destination inconnu (voir ModeleDestination : DeclarationMaternite)
		NSMutableArray result = new NSMutableArray();
		
		for (String nom : temp) {
			if (nom.equals(ENTITE_INCONNUE) == false) {
				result.addObject(nom);
			}
		}
		return result;
	}
	/** retourne la liste des entites (String) du modele d'import qui supportent l'affichage generique 
	 * (les classes sont des sous-classes de ObjetImportPourIndividuEtPeriode, en fait ce sont des sous-classes de ObjetImport mais
	 * il faut alors tester l'interface générique avec toutes les entités)
	 *  classees par ordre alphabetique
	 * @return
	 */
	public NSArray<String> entitesAvecAffichageGenerique(EOEditingContext editingContext) {
		
		if (!estModeleImport) {
			return new NSArray();
		}
		
		if (entitesAffichageGenerique == null) {
			NSArray<Entite> entitesTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(dictionnaireEntites.allValues(), new NSArray(EOSortOrdering.sortOrderingWithKey("nomDestination", EOSortOrdering.CompareAscending)));
			NSMutableArray entitesGeneriques = new NSMutableArray();
			
			for (Entite entite : entitesTriees) {

				try {
					// Rechercher la classe de l'entité du modèle (elle est définie par nomDestination dans le modèle d'import)
					String nomClasse = EOUtilities.entityNamed(editingContext, entite.nomDestination()).className();
					Class classeEntite = Class.forName(nomClasse);
					Object object = classeEntite.newInstance();
					if (object instanceof ObjetImportPourIndividuEtPeriode 
							|| object instanceof ObjetImportPourIndividuOuStructure 
							|| object instanceof ObjetImportAvecSource 
							|| object instanceof EOCldDetail 
							|| object instanceof EOIndividuDiplomes) {
						entitesGeneriques.addObject(entite.nomDestination());
					}
				} catch (Exception exc) {}
			}
			
			entitesAffichageGenerique = new NSArray(entitesGeneriques);
			
		}
		return entitesAffichageGenerique;
	}
	
	/**
	 * 
	 */
	public String toString() {
		return dictionnaireEntites.toString();
	}
	
}