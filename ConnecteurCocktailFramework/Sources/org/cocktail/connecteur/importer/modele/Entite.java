/*
 * Created on 17 mai 2006
 *
 * Decrit une entite a imprimer
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.modele;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Decrit une entite avec son nom dans l'espace d'origine et son nom dans l'espace de destination. Dans ModeleImport.XML,
 * le nom source est le tag de cette entite dans le fichier XML (ou le nom du fichier csv) et le nom  destination est le nom de 
 * l'entite dans le mod&egrave;le Import (.eomodeld). Dans ModeleDestination.XML, le nom source  est le nom de l'entite EOF
 * dans le mod&egrave;le Import (.eomodeld) et le nom de la destination le nom de l'entite EOF dans le mod&egrave;le GRhum ou Mangue.<BR>
 * 
 * @author christine
 */
// 08/07/2011 - Modifiee pour supporter la notion d'entité affichable dans l'interface générique
public class Entite {
	private String nomSource,nomDestination;
	private Integer priorite;
	private NSMutableDictionary dictionnaireAttributs;
	
	// constructeur
	/** Constructeur
	 * @param nomSource nom de l'entite source
	 * @param nom de l'entite destination
	 */
	public Entite(String nomSource,String nomDestination,Integer priorite) {
		this.nomSource = nomSource;
		this.nomDestination = nomDestination;
		this.priorite = priorite;
		dictionnaireAttributs = new NSMutableDictionary();
	}
	/**
	 * @return Retourne le nom de l'attribut source
	 */
	public String nomSource() {
		return nomSource;
	}
	/**
	 * @return Retourne le nom de l'attribut destination
	 */
	public String nomDestination() {
		return nomDestination;
	}
	public Integer priorite() {
		return priorite;
	}
	/**
	 * @return Retourne la liste des attributs.
	 */
	public NSArray<Attribut> attributs() {
		return dictionnaireAttributs.allValues();
	}
	/** @return Retourne la liste des noms des attributs de comparaison 
	 * 
	 */
	public NSArray nomsAttributsComparaison() {
		NSArray attributs = attributs();
		NSMutableArray noms = new NSMutableArray(attributs.count());
		java.util.Enumeration e = attributs.objectEnumerator();
		while (e.hasMoreElements()) {
			Attribut attribut = (Attribut)e.nextElement();
			if (attribut.estAttributComparaison()) {
				noms.addObject(attribut.nomDestination());
			}
		}
		return noms;
	}
	/**
	 * Ajoute une attribut a la liste des attributs
	 * @param attribut attribut à ajouter
	 */
	public void ajouterAttribut(Attribut attribut) {
		dictionnaireAttributs.setObjectForKey(attribut,attribut.nomDestination());
	}
	/** Retourne l'attribut qui a pour nom source : nomAttribut */
	public Attribut attributAvecNomSource(String nomAttribut) {
		java.util.Enumeration e = dictionnaireAttributs.objectEnumerator();
		while (e.hasMoreElements()) {
			Attribut attribut = (Attribut)e.nextElement();
			if (attribut.nomSource().equals(nomAttribut)) {
				return attribut;
			}
		}
		return null;
	}
	/** Retourne l'attribut qui a pour nomSource de colonne : nomSource */
	public Attribut attributAvecNomDestination(String nom) {
		return (Attribut)dictionnaireAttributs.objectForKey(nom);	
	}
	/** Retourne la liste des noms d'attribut.
	 * @param nom crit&egrave;re retenu pour retourner les noms d'attributs (2 valeurs possibles : nomSource et nomDestination)
	 */
	public NSArray nomAttributsPourType(String nom) {
			return (NSArray)attributs().valueForKey(nom);
	}
	public String toString() {
		String s = nomDestination() + ", attributs :\n(";
		java.util.Enumeration e = attributs().objectEnumerator();
		while (e.hasMoreElements()) {
			s = s + e.nextElement().toString() + ",";
		}
		s = s.substring(0,s.length() - 1) + ")";
		return s;
	}
	
}
