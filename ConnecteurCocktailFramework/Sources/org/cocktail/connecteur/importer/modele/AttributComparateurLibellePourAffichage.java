/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.modele;


import com.webobjects.foundation.NSComparator;

/** Comparateur utilise pour trier les attributs (Attribut) par ordre de libelle d'affichage croissant
 * 
 * @author christine
 *
 */
public class AttributComparateurLibellePourAffichage extends NSComparator {
	public AttributComparateurLibellePourAffichage() {
		super();
	}
	public int compare(Object arg0, Object arg1) throws ComparisonException {
		//   OrderedAscending : la valeur du premier argument est moins que la valeur du second
		if (arg0 instanceof Attribut && arg1 instanceof Attribut) {
			Attribut attribut0 = (Attribut)arg0, attribut1 = (Attribut)arg1;
			if (attribut0.positionDansLibelleAffichage() == attribut1.positionDansLibelleAffichage()) {
				return NSComparator.OrderedSame;
			} else if (attribut0.positionDansLibelleAffichage() < attribut1.positionDansLibelleAffichage()) {
				return NSComparator.OrderedAscending;	 // debut null en tête
			} else {	
				return NSComparator.OrderedDescending;
			}
		} else {	// Pas de comparaison
			throw new ComparisonException("Ce comparateur ne peut que trier des objets de type Attribut");
		}
	}
}
