package org.cocktail.connecteur.importer.moteur;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.foundation.NSMutableArray;

public class LogDepart {
	private static LogDepart sharedInstance;	
	NSMutableArray listeDeparts;
	
	public static LogDepart sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new LogDepart();
			sharedInstance.initialiserListe();
		}
		return sharedInstance;
	}
	/** Initialise la liste des departs */
	public void initialiserListe() {
		listeDeparts = new NSMutableArray();
	}
	/** Resette la liste des departs */
	public void reset() {
		initialiserListe();
	}
	/** Retourne true si des departs ont ete ajoutes */
	public boolean existeInformationsPourDepart() {
		return listeDeparts != null && listeDeparts.count() > 0;
	}
	/** Ajoute un depart a la liste des departs
	 * @param depart
	 * @param operationSurDepart : I(NSERTION), C(ORRESPONDANCE), U(PDATE)
	 * @param metFinACarriere : true si le motif de d&eacute:part met fin a la carri&egrave;
	*/
	public void ajouterDepart(EOMangueDepart depart,String operationDepart,boolean metFinACarriere) {
		InformationDepart information = new InformationDepart(depart,operationDepart,metFinACarriere);
		listeDeparts.addObject(information);
	}
	/** Gen&egrave;re le rapport pour les departs et le retourne comme une String */
	public String genererRapport() {
		String texte = "Rapport sur les départs\n";
		texte += "Des départs ont été transférés dans Mangue. Avec l'application Mangue, vous devez sélectionner chaque départ, modifier le commentaire puis valider.\n";
		texte += "Cette action aura pour conséquences :\n";
		texte += "1/ Réalisation de validations supplémentaires pour le Cir.\n\n";
		texte += "2 /Fermeture des informations :\nSi le départ met fin à la carrière :\n- la fermeture des carrières, éléments de carrière et stages à cheval sur le départ et l'ajout du départ au segment de carrière concerné\n"; 
		texte += "ou\n- la fermeture des contrats (modification de la date de fin anticipée) et détails de contrat etjout du départ au contrat concerné\n";
		texte += "- la fermeture des affectations/occupations\n";
		texte += "- la fermeture des absences et de tous les congés\n";
		texte += "- la fermeture des CPA, CFA, délégations, MTT, prolongations d'activité, reculs d'âge, temps partiels si ces informations existent pour l'individu concerné\n";
		texte += "Si le départ ne met pas fin à la carrière :\n- la fermeture des affectations/occupations\n"; 
		texte += "\n******\nListe des individus concernés\n";
		texte += "N°Individu\tNom Prénom\tType de Modification\tMet Fin à Carrière\tDébut Départ - Fin Départ\tMotif Départ\n";
		java.util.Enumeration e = listeDeparts.objectEnumerator();
		while (e.hasMoreElements()) {
			InformationDepart information = (InformationDepart)e.nextElement();
			texte += information.toString() + "\n";
		}
		return texte;
	}
	private class InformationDepart {
		private String identiteIndividu;
		private String noIndividu;
		private String dateDebutDepart;
		private String dateFinDepart;
		private String motifDepart;
		private String operationDepart;
		private boolean metFinCarriere;
		
		/** Constructeur
		 * 
		 * @param depart
		 * @param operationSurDepart : I(NSERTION), C(ORRESPONDANCE), U(PDATE)
		 */
		public InformationDepart(EOMangueDepart depart, String operationSurDepart,boolean metFinACarriere) {
			this.noIndividu = depart.individu().noIndividu().toString();
			this.identiteIndividu = depart.individu().nomUsuel() + " " + depart.individu().prenom();
			this.dateDebutDepart = DateCtrl.dateToString(depart.dateDebut());
			if (depart.dateFin() != null) {
				this.dateFinDepart = DateCtrl.dateToString(depart.dateFin());
			}
			if (operationSurDepart.equals(ObjetImport.OPERATION_INSERTION)) {
				this.operationDepart = "Insertion";
			} else if (operationSurDepart.equals(ObjetImport.OPERATION_UPDATE)) {
				this.operationDepart = "Update";
			} else {
				this.operationDepart = "Correspondance";	// Ne devrait pas arriver car les départs en correspondance sont déjà validés dans le SI Destinataire
			}
			this.motifDepart = depart.cMotifDepart();
			this.metFinCarriere = metFinACarriere;
		}
		public String toString() {
			String temp = noIndividu + "\t" + identiteIndividu + "\t" + operationDepart + "\t"; 
			if (metFinCarriere) {
				temp += "Oui";
			} else {
				temp += "Non";
			}
			temp += "\t" + dateDebutDepart;
			if (dateFinDepart != null) {
				temp += " - " + dateFinDepart;
			}
			temp += "\t" + motifDepart;
			return temp;
		}
	}
}
