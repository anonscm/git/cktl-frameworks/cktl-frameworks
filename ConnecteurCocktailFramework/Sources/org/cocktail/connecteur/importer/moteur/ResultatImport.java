/*
 * Created on 9 mai 2005
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.importer.moteur.erreurs.ErreurManager;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.InterfaceHomonymie;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.importer.EOLogErreurs;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author christine
 *
 * Stocke le resultat de l'import des donnes depuis des fichiers dans la base d'import. Les donnees valides
 * sont stockees sous forme de records de la base d'import, les donnees invalides sous forme de dictionnaires. Les
 * erreurs sont stockees dans un tableau.
 */
public class ResultatImport {
	private final String ATTRIBUT_INCONNU = "Attribut inconnu: ";
	
	private boolean checkDoublons = true;
	private Donnees enregistrementsValides;
	private Donnees enregistrementsInvalides;
	private int nbEnregistrementsEnDoubleDansImport, nbEnregistrementsDejaImportes;
	private NSMutableArray messagesPourRecordsTronques;
	EOEditingContext editingContext;
	
	private ReglesImport reglesImport;
	
	public ResultatImport(ReglesImport reglesImport) {
		
		this.reglesImport = reglesImport;
		
		enregistrementsValides = new Donnees();
		enregistrementsInvalides = new Donnees();
		messagesPourRecordsTronques = new NSMutableArray();
		nbEnregistrementsEnDoubleDansImport = 0;
		nbEnregistrementsDejaImportes = 0;
		editingContext=new EOEditingContext();
	}
	
	public void setCheckDoublons(boolean check) {
		checkDoublons = check;
	}
	
	public boolean isCheckDoublons() {
		return checkDoublons;
	}
		
	public Donnees enregistrementsValides() {
		return enregistrementsValides;
	}
	public Donnees enregistrementsInvalides() {
		return enregistrementsInvalides;
	}
	public int nbEnregistrementValides() {
		return enregistrementsValides.nbEnregistrements();
	}
	public int nbEnregistrementInvalides() {
		return enregistrementsInvalides.nbEnregistrements();
	}
	public int nbEnregistrementsEnDoubleDansImport() {
		return nbEnregistrementsEnDoubleDansImport;
	}
	public int nbEnregistrementsDejaImportes() {
		return nbEnregistrementsDejaImportes;
	}
	public NSArray messagesPourRecordsTronques() {
		return messagesPourRecordsTronques;
	}
	
	public EOEditingContext editingContext() {
		return editingContext;
	}
	
	/** Ajoute un message d'erreur a la liste des messages */
	public void ajouterMessageTronque(String message) {
		messagesPourRecordsTronques.addObject(message);
	}
	/** Verifie si le dictionnaire des attributs correspond aux attentes (attributs obligatoires, type des donnees, nomenclatures).
	 * Dans le cas positif, ajoute un record initialise avec ce dictionnaire aux enregistrements valides si il n'est pas deja
	 * dans l'import courant.
	 * Dans le cas negatif, ajoute le dictionnaire aux enregistrements invalides.
	 * @param valeursAttributs
	 * @param nomEntite
	 */
	public void ajouterRecord(String nomEntite, NSMutableDictionary valeursAttributs, int ligneDebutEntite) {
		// Vérifier si les données respectent les contraintes
		String resultat = reglesImport.preparerEtValiderEntiteImport(nomEntite, valeursAttributs);
		if (resultat == null || resultat.length() == 0) {

			ObjetImport record = instanciationEnregistrement(nomEntite, valeursAttributs);
			
			// Avant de fournir les valeurs, vérifier la longueur des champs et tronquer si nécessaire
			resultat = reglesImport.validerLongueursAttributs(nomEntite, valeursAttributs,true);
			if (resultat != null) {
				// Valeurs tronquees
				record.setStatut(ObjetImport.STATUT_TRONQUE);
				String message = reglesImport.remettreMessageErreurEnFormePourEntite(resultat, nomEntite);
				ajouterMessageTronque(message);
			}
			record.takeValuesFromDictionary(valeursAttributs);
			
			boolean doitEtreImporte=true;
			if (checkDoublons && estUnDoublonDansLeLotImporte(record)) {
				nbEnregistrementsEnDoubleDansImport++;
				doitEtreImporte=false;
				
				Integer idSource = (Integer) valeursAttributs.objectForKey("idSource");
				EOLogErreurs.creer(editingContext, nomEntite, "Doublon", ligneDebutEntite,idSource);	
				ErreurManager.instance().ajouterErreur(ErreurManager.ETAPE_DOUBLON,nomEntite, valeursAttributs, "Doublon", ligneDebutEntite);
			} else if (reglesImport.estIdentiqueDernierRecordDeBaseImport(record)) {
				nbEnregistrementsDejaImportes++;
				doitEtreImporte=false;
			}
			if (doitEtreImporte) {
				enregistrementsValides.ajouterObjetPourEntite(record, nomEntite);
			} 
		} else {
						
			// pour les objets invalides, on crée des objets avec Erreur
			try {
				
				String texte = resultat;

				if (nomEntite.equals(EOIndividu.ENTITY_NAME)) {
					texte = valeursAttributs.objectForKey(EOIndividu.NOM_USUEL_KEY) + " - " + texte;
				}
				Integer idSource= (Integer) valeursAttributs.objectForKey("idSource");

				// On crée la donnée mais le saveChanges est fait une fois l'import totalement terminé.
				//    Sinon, on multiplie le nombre de saveChanges et crée une fuite mémoire importante 
				//	  Ex: une erreur sur chaque ligne d'un fichier de 10'000 lignes crée 1 Go de fuite 
				//			(et une exception out of memory dans certains établissements
				EOLogErreurs.creer(editingContext, nomEntite, texte, ligneDebutEntite, idSource);	
				ErreurManager.instance().ajouterErreur(ErreurManager.ETAPE_IMPORT,nomEntite, valeursAttributs, texte, ligneDebutEntite);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			ObjetAvecErreur objet = new ObjetAvecErreur(valeursAttributs,resultat);
			enregistrementsInvalides.ajouterObjetPourEntite(objet,nomEntite);
		}
	}

	/**
	 * Génere un enregistrement d'import correctement typé
	 * @param valeursAttributs 
	 * @param nomEntite
	 * @return
	 */
	protected ObjetImport instanciationEnregistrement(String nomEntite, 
			NSMutableDictionary valeursAttributs) {

		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(nomEntite);
		EOEditingContext edc=AutomateImport.sharedInstance().editingContext();
		ObjetImport record = (ObjetImport) classDescription.createInstanceWithEditingContext(edc,null);
		record.setReglesImport(reglesImport);
		record.init();

		if (record instanceof EOIndividu) {
			String temPersonnel = (String)valeursAttributs.valueForKey("temPersonnel");
			if (temPersonnel != null && temPersonnel.equals(CocktailConstantes.VRAI)) {
				((EOIndividu)record).preparerPourPersonnel();
			}
		}
		return record;
	}
	
	public String toString() {
		return "enregistrements valides" + System.getProperty("line.separator") + enregistrementsValides().toString() + 
			System.getProperty("line.separator") + "enregistrements invalides" + System.getProperty("line.separator") + 
			enregistrementsInvalides().toString() ;
	}
	
	/**
	 * Dans un lot d'import est considéré comme doublon :
	 * les enregistrements totalement identiques,
	 * les enregistrements identiques sur nom usuel, prénom et N° INSEE.
	 * 
	 * @param record
	 * @return true si un doublon
	 */
	private boolean estUnDoublonDansLeLotImporte(ObjetImport record) {
		NSArray<ObjetImport> objetsValides = enregistrementsValides.donneesPourNomEntite(record.entityName());
		if (objetsValides == null || objetsValides.count() == 0) {
			return false;
		}
		boolean doublonTrouve=false;
		for (ObjetImport objet : objetsValides) {
			if (reglesImport.ontAttributsComparaisonIdentiques(record, objet)) {
				return true;
			} else if (objet instanceof InterfaceHomonymie) {
				doublonTrouve=((InterfaceHomonymie)objet).memesDonneesQueRecord(record);
				if (doublonTrouve)
					return doublonTrouve;
			}
		}
		return false;
	}
	
	/** Modelise les donnees lues dans un fichier */
	public class Donnees {
		// le dictionnaire contient les données sous forme de tableaux pour chaque type d'entité (la clé est le nom de l'entité). 
		// Ces tableaux contiennent soit des objets (records de la base d'import), soit des dictionnaires (attributs d'import)
		private NSMutableDictionary dictDonnees;
		public Donnees() {
			this.dictDonnees = new NSMutableDictionary();
		}

		public NSArray donneesPourNomEntite(String nomEntite) {
			return (NSArray)dictDonnees.valueForKey(nomEntite);
		}
		
		public void ajouterObjetPourEntite(Object objet,String nomEntite) {
			NSMutableArray listeObjets = (NSMutableArray)dictDonnees.valueForKey(nomEntite);
			if (listeObjets == null) {	// entité pas encore ajoutée
				listeObjets = new NSMutableArray();
				dictDonnees.takeValueForKey(listeObjets,nomEntite);
			}
			listeObjets.addObject(objet);

		}
		public NSArray entites() {
			return dictDonnees.allKeys();
		}
		public int nbEnregistrements() {
			int total = 0;
			java.util.Enumeration e = entites().objectEnumerator();
			while (e.hasMoreElements()) {
				NSArray items = donneesPourNomEntite((String)e.nextElement());
				total += items.count();
			}
			return total;
		}
		public String toString() {
			return dictDonnees.toString();
		}
	}
	public class ObjetAvecErreur {
		public final static String DELIMITEUR = " >>";
		// Le dictionnaire contient les valeurs des attributs de l'objet erroné
		private NSMutableDictionary valeurs;
		private NSMutableDictionary messagesErreur;
		
		public ObjetAvecErreur(NSMutableDictionary valeurs,String messageErreur) {
			this.valeurs = valeurs;
			messagesErreur = new NSMutableDictionary();
			parser(messageErreur);
		}
		public NSArray clesPourObjet() {
			return valeurs.allKeys();
		}
		public Object valeurPourCle(String cle) {
			return valeurs.objectForKey(cle);
		}
		public NSArray clesPourErreur() {
			return messagesErreur.allKeys();
		}
		public String messageErreurPourCle(String cle) {
			return (String)messagesErreur.objectForKey(cle);
		}
		public String toString() {
			return "Valeurs" + System.getProperty("line.separator") + valeurs.toString() + System.getProperty("line.separator")
			+ "Erreurs " + System.getProperty("line.separator") + messagesErreur.toString();
		}
		// Méthodes privées
		private void parser(String message) {
					
			// Les messages d'erreur sont stockées dans une String, chaque message est délimité par un retour chariot.
			// Un message est structuré avec un label suivi de >>, suivi ou non d'un texte
			String[] messages = message.split(System.getProperty("line.separator"));
			for (int i = 0; i < messages.length;i++) {
				String erreur = messages[i];
				int position = erreur.indexOf(DELIMITEUR);
				if (position >= 0) {
					String cle = erreur.substring(0,position);
					String texte = "A corriger";
					if (erreur.length() > position + DELIMITEUR.length()) {
						texte = erreur.substring(position + DELIMITEUR.length());
					}
					// Vérifier si il existe déjà un message pour cette clé, en quel cas, concaténet les messages
					String texte1 = (String)messagesErreur.objectForKey(cle);
					if (texte1 != null) {
						texte = texte1 + ";" + texte;
					}
					messagesErreur.setObjectForKey(texte, cle);
				}
			}
		}
	}
	public void ajouterErreurAttributInconnu(String nomEntite, String attribut, int ligneDebutEntite) {
		String message = ATTRIBUT_INCONNU + attribut;
		EOLogErreurs.creer(editingContext, nomEntite, message, ligneDebutEntite, null);			
		ErreurManager.instance().ajouterErreur(ErreurManager.ETAPE_IMPORT,nomEntite, new NSDictionary<String, String>(), message, ligneDebutEntite);
	}

}
