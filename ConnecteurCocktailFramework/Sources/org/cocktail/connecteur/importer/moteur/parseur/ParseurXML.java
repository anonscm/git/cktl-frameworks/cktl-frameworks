/*
 * Created on 23 sept. 2004
 *
 *	Execute le parsing XML
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur.parseur;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.importer.moteur.ResultatImport;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;

/**
 * @author christine
 *
 * Parseur XML
 * Classe dont les methodes sont invoquees comme callbacks lors du parsing du fichier XML contenant les donnees
 * du mod&egrave;le
 */
public class ParseurXML extends DefaultHandler {
	
	public final static String NOUVELLE_ENTITE = "NouvelleEntite";
	private Entite entiteCourante;
	private NSMutableDictionary dictionnaireAttributs;
	private String attributLu,entiteLue,groupeLu;
	private boolean litAttributs;
	private String attributEnCours;
	private ResultatImport resultatImport;
	private ReglesImport reglesImport;
	
	private int ligneDebutEntiteCourante=-1;
	
	private Locator locator;
	
	/** Constructeur
	 * 
	 *
	 */
	public ParseurXML(ReglesImport reglesImport, boolean checkDoublons) {
		super();
		this.reglesImport = reglesImport;
		this.resultatImport = new ResultatImport(this.reglesImport);
		this.resultatImport.setCheckDoublons(checkDoublons);
	}
	
	@Override
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub
		this.locator=locator;
	}

	public ResultatImport resultatImport() {
		return resultatImport;
	}

	public void startElement(String namespaceURI, String localName, String qName,Attributes atts) throws SAXException {
		attributLu = null;
		attributEnCours = "";
		if (groupeLu == null) {		// On est sur un groupe d'entités
			if (estEntitePluriel(localName)) {	
				startElementGroupeEntites(localName);
			}
			//TODO : QUe faire d'un début de groupe non au pluiel
		} else {
			if (litAttributs == false) {		// On est sur une entité
				startElementEntite(localName);
			} else {	// on est sur un attribut
				startElementAttribut(localName);
			}
		}
	}
	
	/**
	 * Opérations de début de lecture d'un attribut
	 * @param nomAttribut
	 */
	protected void startElementAttribut(String nomAttribut) {
		attributLu = nomAttribut;
	}
	
	/**
	 * Opérations de début de lecture d'une entité
	 * @param nomEntite
	 * @throws SAXException
	 */
	protected void startElementEntite(String nomEntite) throws SAXException {
		NSNotificationCenter.defaultCenter().postNotification(NOUVELLE_ENTITE,this);
		if (entiteLue != null && entiteLue.equals(nomEntite)) {
			// On est toujours sur une entité de même type
			dictionnaireAttributs = new NSMutableDictionary();
		} else {
			entiteLue = nomEntite;
			entiteCourante = getReglesImport().entiteAvecNomSource(nomEntite);
			if (entiteCourante != null) {
				dictionnaireAttributs = new NSMutableDictionary();
			} else {
				throw new SAXException(nomEntite + " : entite invalide");
			}
		} 
		litAttributs = true;
		ligneDebutEntiteCourante=locator.getLineNumber();
		LogManager.logDetail("start entite " + entiteLue);
	}
	
	/**
	 * Opérations de début de lecture d'un groupe d'entités
	 * @param nomGroupe
	 */
	protected void startElementGroupeEntites(String nomGroupe) {
		groupeLu = nomGroupe;
		entiteCourante = null;
		entiteLue = null;
		litAttributs = false;
		ligneDebutEntiteCourante=-1;
		LogManager.logDetail("start groupeLu " + groupeLu);
	}
	
	public void characters(char[] ch,int start,int length) throws SAXException {
		if (attributLu != null) {		
			attributEnCours = attributEnCours.concat(new String(ch,start,length));
		}
	}
	
	/**
	 * Opérations de fin de lecture d'un constituant XML
	 */
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if (attributLu != null) {
			endElementAttribut();
		} else if (entiteLue != null && entiteLue.equals(localName) && entiteCourante != null) {
			endElementEntite();
		} else if (groupeLu != null && groupeLu.equals(localName)) {
			endElementGroupeEntites();
		}
		//TODO: Que faire d'une fin de lecture d'un constituant XML non prévue ?
	}
	
	/**
	 * Opérations de fin de lecture d'un attribut
	 */
	protected void endElementAttribut() {
		//LogManager.logDetail("attributLu " + attributLu);
		attributLu = attributLu.toUpperCase(); // au cas où par erreur, le tag XML est en minuscule
		Attribut attribut = entiteCourante.attributAvecNomSource(attributLu);
		if (attribut != null) {		
			// attribut == null => attribut inconnu, ignorer cet attribut
			// On prend en compte, les attributs nuls
			if (attributEnCours.length() > 0) {
				supprimerCaracteresParasites();
			}
			dictionnaireAttributs.setObjectForKey(attributEnCours, attribut.nomDestination());
		} else {
			resultatImport.ajouterErreurAttributInconnu(entiteCourante.nomDestination(), attributLu, ligneDebutEntiteCourante);
		}
		attributLu = null;
	}
	
	/**
	 * Opérations de fin de lecture d'une entité
	 */
	protected void endElementEntite() {
		// LogManager.logDetail("entitelue " + entiteLue);
		resultatImport.ajouterRecord(entiteCourante.nomDestination(), dictionnaireAttributs, ligneDebutEntiteCourante);
		litAttributs = false;
		ligneDebutEntiteCourante=-1;
	}
	
	/**
	 * Opération de fin de lecture d'un groupe d'entité
	 */
	protected void endElementGroupeEntites() {
		LogManager.logDetail("groupeLu " + groupeLu);
		entiteCourante = null;
		groupeLu = null;
		entiteLue = null;
	}
	
	private void supprimerCaracteresParasites() {
		// Supprimer tous les caractères parasites en début et fin d'attribut : espace, tab et retour chariot
		boolean attributOK = attributEnCours.startsWith(" ") == false && attributEnCours.startsWith("\t") == false && attributEnCours.startsWith("\n") == false;
		while (!attributOK) {
			while (attributEnCours.indexOf(" ") == 0) {
				attributEnCours = attributEnCours.substring(1);
			}
			while (attributEnCours.indexOf("\t") == 0) {
				attributEnCours = attributEnCours.substring(1);
			}
			while (attributEnCours.indexOf("\n") == 0) {
				attributEnCours = attributEnCours.substring(1);
			}
			attributOK = attributEnCours.startsWith(" ") == false && attributEnCours.startsWith("\t") == false && attributEnCours.startsWith("\n") == false;
		}
		attributOK = attributEnCours.endsWith(" ") == false && attributEnCours.endsWith("\t") == false && attributEnCours.endsWith("\n") == false;
		while (!attributOK) {
			while (attributEnCours.endsWith(" ")) {
				attributEnCours = attributEnCours.substring(0,attributEnCours.length() - 1);
			}
			while (attributEnCours.endsWith("\t")) {
				attributEnCours = attributEnCours.substring(0,attributEnCours.length() - 1);
			}
			while (attributEnCours.endsWith("\n")) {
				attributEnCours = attributEnCours.substring(0,attributEnCours.length() - 1);
			}
			attributOK = attributEnCours.endsWith(" ") == false && attributEnCours.endsWith("\t") == false && attributEnCours.endsWith("\n") == false;
		}
	}

	/**
	 * Retourne les rêgles d'imports
	 * @return
	 */
	protected ReglesImport getReglesImport() {
		return reglesImport;
	}
	
	// méthodes privées
	private boolean estEntitePluriel(String nom) {
		String nomTable = nom.substring(0,nom.length()-1);
		return getReglesImport().entiteAvecNomSource(nomTable) != null;
	}
}
