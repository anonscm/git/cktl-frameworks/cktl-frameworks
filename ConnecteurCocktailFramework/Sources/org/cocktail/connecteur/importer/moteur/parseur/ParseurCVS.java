
//ParseurCVS.java
//Classe de base pour le parsing d'un fichier : contient des méthodes abstraites
//Created by Christine Buttin on Fri May 14 2004.
//Copyright (c) 2004 __MyCompanyName__. All rights reserved.

/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** prototype du parsing de donnees */
package org.cocktail.connecteur.importer.moteur.parseur;

import java.io.File;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.modele.ModeleTexte;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.importer.moteur.ResultatImport;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Parse de fichiers de format texte:<BR>
 * Le nom du fichier indique l'entite a importer, il doit avoir le format ENTITE.txt ou ENTITE_XXX.txt<BR>
 * Le format du fichier est :<BR>
 * ATTRIBUT1	ATTRIBUT2	ATTRIBUT3<BR>
 * VALEUR1		VALEUR2		VALEUR3<BR>
 *
 */
public class ParseurCVS {

	private ReglesImport reglesImport;
	private boolean checkDoublons = false;
	
	public ParseurCVS(ReglesImport reglesImport, boolean checkDoublons) {
		this.reglesImport = reglesImport;
		this.checkDoublons = checkDoublons;
	}
	
	/** 
	 * retourne le resultat de l'import
	 */
	public ResultatImport parser(String path) throws Exception {

		LectureFichierDelimite lfd = new LectureFichierDelimite(new File(path));

		String ligneCourante;
		int numeroLigneCourante = 0;
		int debut = 0;
		int fin = 0;

		System.out.println("ParseurCVS.parser() - Parser PATH : " + path);

		if (path.lastIndexOf("/") > 0) {
			debut = path.lastIndexOf("/") + 1;
			System.out.println("\tParseurCVS.parser() - path.lastIndexOf(/) : " + debut);
		}
		else {
				if (path.indexOf("\\") > 0) {
					debut = path.lastIndexOf("\\") + 1;			
					System.out.println("\tParseurCVS.parser() - path.lastIndexOf(\\) : " + debut);
			}
		}

		fin = path.lastIndexOf(".");

		System.out.println("\tParseurCVS.parser() - Index DEBUT : " + debut);
		System.out.println("\tParseurCVS.parser() - Index FIN : " + fin);

		String nomEntite = path.substring(debut,fin);	
		int index = nomEntite.indexOf("_");

		if (index > 0) {
			nomEntite = nomEntite.substring(0,index);
		}

		System.out.println("ParseurCVS.parser() - Index FIN : " + nomEntite);

		Entite entiteCourante = reglesImport.entiteAvecNomSource(nomEntite);
		if (entiteCourante == null) {
			throw new Exception("Entité inconnue ! ( Fichier : " + path + " , nom entité : " + nomEntite + " )");
		}

		ModeleTexte modeleTexte = new ModeleTexte();
		NSArray attributs = null;
		ResultatImport resultatImport = new ResultatImport(reglesImport);
		resultatImport.setCheckDoublons(checkDoublons);
		try {
			while ((ligneCourante = lfd.lireLigne()) != null) {
				numeroLigneCourante ++;
				if (numeroLigneCourante == 1) {	// La première ligne contient les attributs
					LogManager.logDetail("Extraction des attributs");
					attributs = extraireEtValiderAttributsPourEntite(ligneCourante,entiteCourante);
					if (attributs == null) {
						throw new Exception(nomEntite + " : attributs inconnus");
					} else {
						modeleTexte.ajouterAttributsPourEntite(entiteCourante.nomDestination(), attributs);
					}
				} else {
					AutomateImport.sharedInstance().informerThread("Traitement de la ligne " + numeroLigneCourante);
					// Lire les valeurs
					NSMutableDictionary valeursAttributs = extraireValeurs(ligneCourante,entiteCourante.nomDestination(),modeleTexte);
					if (valeursAttributs != null) {
						resultatImport.ajouterRecord(entiteCourante.nomDestination(), valeursAttributs,numeroLigneCourante);	
					}
				} 
			}
		}
		catch (Exception e) {
			AutomateImport.sharedInstance().informerThread("Une erreur s'est produite lors de la lecture du fichier de donnees.");	
			AutomateImport.sharedInstance().signalerExceptionThread(e);
		}
		return resultatImport;
	}

	/**
	 * 
	 * @param texte
	 * @param entite
	 * @return
	 * @throws Exception
	 */
	private NSArray extraireEtValiderAttributsPourEntite(String texte,Entite entite) throws Exception {
		if (texte.equals("")) {
			// ligne vide
			return null;
		}
		NSMutableArray attributs = new NSMutableArray();
		ExtracteurChampSequentiel extracteur = new ExtracteurChampSequentiel(texte,"\t");
		String nomAttribut;
		try {
			while ((nomAttribut = extracteur.extraire()) != null) {
				LogManager.logDetail("nom attribut source " + nomAttribut);
				Attribut attribut = entite.attributAvecNomSource(nomAttribut) ;
				if (attribut == null) {
					throw new Exception(nomAttribut + " : attribut inconnu");
				} else {
					attributs.addObject(attribut.nomDestination());
				}
			}
			return attributs;
		} catch (Exception e) {
			throw new Exception("Erreur pendant l'extraction des attributs de " + entite.nomSource() + " message " + e.getMessage());
		}
	}

	// extrait toutes les valeurs d'une ligne qui correspondent aux données 
	// retourne un dictionnaire contenant les données avec comme clé le nom de l'attribut

	/**
	 * 
	 * @param texte
	 * @param nomEntite
	 * @param modeleTexte
	 * @return
	 * @throws Exception
	 */
	private NSMutableDictionary extraireValeurs(String texte,String nomEntite,ModeleTexte modeleTexte) throws Exception {
		if (texte.equals("")) {
			// ligne vide
			return null;
		}
		NSMutableDictionary valeurs = new NSMutableDictionary();
		ExtracteurChampSequentiel extracteur = new ExtracteurChampSequentiel(texte,"\t");
		int colonNum = 0;
		String champCourant = null;
		while ((champCourant = extracteur.extraire()) != null) {
			colonNum++;
			String nomAttribut = modeleTexte.nomAttributPourEntiteEtColonne(nomEntite, colonNum);
			if (nomAttribut != null) {	// Null si on a dépassé le nombre de colonnes
				valeurs.setObjectForKey(champCourant, nomAttribut);
			}
		}
		return valeurs;
	}
}
