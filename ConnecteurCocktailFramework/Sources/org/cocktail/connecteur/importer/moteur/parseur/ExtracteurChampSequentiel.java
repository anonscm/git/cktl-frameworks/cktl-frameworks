//
//  ExtractionChamp.java
//
//  Created by Christine Buttin on Thu May 13 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** Classe pour extraire les champs dans un texte. On fournit les delimiteur de debut et fin (ils peuvent &ecirc;tre modifies en cours
de traitement. L'extraction est sequentielle. */
package org.cocktail.connecteur.importer.moteur.parseur;

public class ExtracteurChampSequentiel {
	private String texteAnalyse;
	private String delimiteurDebut;
	private String delimiteurFin;
	
	public ExtracteurChampSequentiel(String texte,String finDelim) {
		texteAnalyse = texte;
		delimiteurFin = finDelim;
	}
	
	public ExtracteurChampSequentiel(String texte,String debDelim,String finDelim) {
		texteAnalyse = texte;
		delimiteurDebut = debDelim;
		delimiteurFin = finDelim;
	}
	public String delimiteurDebut() {
		return delimiteurDebut;
	}
	public void setDelimiteurDebut(String delimiter) {
		delimiteurDebut = delimiter;
	}
	public String delimiteurFin() {
		return delimiteurFin;
	}
	public void setDelimiteurFin(String delimiter) {
		delimiteurFin = delimiter;
	}
	public String extraire() throws Exception {
		if (delimiteurFin == null)
			throw new Exception("delimiteur fin inconnu");
		if (texteAnalyse == null) {
			return null;
		}
		if (delimiteurDebut != null) {
			int deb = texteAnalyse.indexOf(delimiteurDebut);
			if (deb < 0)
				return null;
			else {
				texteAnalyse = texteAnalyse.substring(deb + delimiteurDebut.length());
			}
		}
		int fin = texteAnalyse.indexOf(delimiteurFin);
		String resultat;
		if (fin == -1) {
			// fin du texter
			resultat = texteAnalyse;
			texteAnalyse = null;
		} else {
			resultat = texteAnalyse.substring(0,fin);
			texteAnalyse = texteAnalyse.substring(fin + delimiteurFin.length());
		}
		return resultat;
	}
}
