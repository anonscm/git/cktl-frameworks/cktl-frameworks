//
//  LectureFichierDelimite.java

//  Created by Christine Buttin on Thu May 13 2004.
//
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** Classe  pour parser un fichier texte delimite */
//-----------------------------------------------------------------------------------------------
package org.cocktail.connecteur.importer.moteur.parseur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class LectureFichierDelimite {
	private BufferedReader	lineReader;
	private File locationFichier;
	private String delimiteurLigne;
	
	/** constructeur utilise pour lire un fichier en prenant comme delimiteur de ligne la fin de la ligne */
	public LectureFichierDelimite (File fichier) {
		locationFichier = fichier;
	}
	
	/** constructeur general */
	public LectureFichierDelimite (File fichier, String delimiteur) {
		locationFichier = fichier;
		delimiteurLigne = delimiteur;
	}
	public String delimiteurLigne() {
		return delimiteurLigne;
	}
	public void setDelimiteurLigne(String delimiteur) {
		delimiteurLigne = delimiteur;
	}
		
	public String lireLigne() throws Exception, FileNotFoundException, IOException {
		if (lineReader == null) {
			lineReader = new BufferedReader (new FileReader (locationFichier));
		}
		String resultat = null;
		boolean lireUneLigne = (delimiteurLigne == null);
		boolean termine = false;

		while (!termine) {
			String ligneCourante = lineReader.readLine();
			if (ligneCourante != null) {
				if (resultat == null) {
					resultat = ligneCourante;
				} else {
					resultat = resultat + ligneCourante;
				}
				if (lireUneLigne || resultat.endsWith(delimiteurLigne) == true) {
					termine = true;
				}
			} else {
				termine = true;
			}
		}

		if (resultat == null) {
			lineReader.close();
			lineReader = null;
		}
		return resultat;
	}
}
