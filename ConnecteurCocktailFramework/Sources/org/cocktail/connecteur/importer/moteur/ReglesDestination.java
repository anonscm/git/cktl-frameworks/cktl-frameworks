/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.serveur.modele.correspondance.EOTelephoneCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.InterfaceGestionIdUtilisateur;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataireAvecDates;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

// 24/06/2010 - modification pour prendre en compte les records qui supportent l'interface de gestion des utilisateurs qui font
// des créations/modifications
// 08/03/2011 - on force le persIdCreation si il est nul lors d'une modification
// 03/10/2011 - Modification pour gérer les dates de décès dans les individus et les départs
public class ReglesDestination extends ReglesPourModele {
	private static ReglesDestination sharedInstance = null;

	/** Retourne les noms d'entites de la base d'import triees par priorite */
	public NSArray<String> nomEntitesBaseImportTrieesParPriorite() {
		return modeleDonnees().nomEntitesTrieesParPriorite("nomSource"); // Dans le modèle XML de destination, nomSource désigne le nom d'entité d'import
	}

	/** Retourne les noms d'entites de la destination triees par priorite */
	public NSArray nomEntitesDestinationTrieesParPriorite() {
		return modeleDonnees().nomEntitesTrieesParPriorite("nomDestination"); // Dans le modèle XML de destination, nomDestination désigne le nom d'entité de
																				// destination
	}

	// Méthodes statiques
	public static ReglesDestination sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new ReglesDestination();
		}
		return sharedInstance;
	}

	private ObjetPourSIDestinataire effectuerInsertion(EOEditingContext editingContext, ObjetImport recordImport, String nomEntiteImport) throws Exception {
		LogManager.logDetail("Insertion du record");
		// Commencer par créer un nouveau record dans le SI Destinataire et l'enregistrer dans la base pour pouvoir
		// enregistrer ensuite la correspondance
		ObjetPourSIDestinataire recordDestination = creerDestinationEtEnregistrer(editingContext, recordImport, nomEntiteImport);
		// Créer l'objet de correspondance et l'enregistrer
		if (recordDestination != null) {
			creerCorrespondanceEtEnregistrer(editingContext, nomEntiteImport, recordImport, recordDestination);
		}
		return recordDestination;
	}

	private ObjetPourSIDestinataire effectuerHomonymie(EOEditingContext editingContext, ObjetImport recordImport, String nomEntiteImport) throws Exception {
//		LogManager.logDetail("Mise à jour Homonyme du record");
		// Les homonymes sont gérés comme des correspondances
		ObjetPourSIDestinataire recordDestination = effectuerCorrespondance(editingContext, recordImport, nomEntiteImport);
		recordDestination.onHomonymieEffectuee();
		return recordDestination;

	}

	private ObjetPourSIDestinataire effectuerUpdate(EOEditingContext editingContext, ObjetImport recordImport, String nomEntiteImport) throws Exception {
		LogManager.logDetail("Update du record");
		// Rechecher la correspondance associée à un record équivalent
		ObjetCorresp correspondance = recordImport.correspondance();
		if (correspondance == null) {
			throw new Exception("Record en update et pas de correspondance pour le record");
		}
		// Modifier le record destinataire de la correspondance en remplaçant la valeur sur l'ancien objet d'import par
		// le nouvel objet d'import
		ObjetPourSIDestinataire recordDestination = (ObjetPourSIDestinataire) correspondance.valueForKey(correspondance.nomRelationBaseDestinataire());

		if (modifierDestinationAvecRecord(recordDestination, recordImport)) {
			LogManager.logDetail("Sauvegarde de l'editing context");
			editingContext.saveChanges();
		}

		// Modifier la correspondance pour garder comme record d'import ce record d'import
		correspondance.updaterAvecRecord(recordImport);

		try {
			LogManager.logDetail("Sauvegarde de l'editing context pour la correspondance");
			editingContext.saveChanges();
		} catch (Exception exc) {
			LogManager.logException(exc);
			throw new Exception("Lors d'un update, erreur lors de la sauvegarde de l'editing context");
		}
		return recordDestination;
	}

	private ObjetPourSIDestinataire effectuerCorrespondance(EOEditingContext editingContext, ObjetImport recordImport, String nomEntiteImport) throws Exception {
		LogManager.logDetail("Creation d'une correspondance");
		// Rechercher le record dans le SI destinataire équivalent qui fait que l'opération est une correspondance
		ObjetPourSIDestinataire recordDestination = recordImport.destinataireEquivalent();
		if (recordDestination == null) {
			throw new Exception("Destinataire de type " + recordImport.entityName() + " inconnu");
		}
		// Vérifier si il fallait juste faire une correspondance car il y a des règles de priorité
		// Vérifier si les records sont identiques, sinon modifier le destinataire
		Entite entite = ReglesDestination.sharedInstance().modeleDonnees().entiteAvecNomDestination(recordDestination.entityName());
		if (entite != null && recordDestination.aAttributsIdentiques(recordImport, entite) == false) {
			if (modifierDestinationAvecRecord(recordDestination, recordImport)) {
				LogManager.logDetail("Sauvegarde de l'editing context");
				editingContext.saveChanges();
			}
		}
		// Pour les correspondances, on n'a recherché que les records valides, il n'est donc pas besoin de revalider
		// Créer cette correspondance, l'insérer dans l'editing context et sauvegarder l'editing context
		creerCorrespondanceEtEnregistrer(editingContext, nomEntiteImport, recordImport, recordDestination);
		return recordDestination;
	}

	/**
	 * Effectue l'operation du record d'import (insertion, update, correspondance). Retourne null (si OK) ou le message de l'exception generee
	 * 
	 * @param editingContext
	 *            editing context
	 * @param recordImport
	 *            record en provenance de la base d'import
	 * @param nomEntiteImport
	 *            nom d'entite a creer
	 **/
	public String effectuerOperation(EOEditingContext editingContext, ObjetImport recordImport, String nomEntiteImport) {
		if (recordImport.operation() != null) {
			ObjetPourSIDestinataire recordDestination = null;
			try {
				if (recordImport.statut().equals(ObjetImport.STATUT_HOMONYME)) {
					recordDestination = effectuerHomonymie(editingContext, recordImport, nomEntiteImport);
				} else if (recordImport.operation().equals(ObjetImport.OPERATION_INSERTION) && !recordImport.statut().equals(ObjetImport.STATUT_HOMONYME)) {
					recordDestination = effectuerInsertion(editingContext, recordImport, nomEntiteImport);
				} else if (recordImport.operation().equals(ObjetImport.OPERATION_UPDATE)) {
					recordDestination = effectuerUpdate(editingContext, recordImport, nomEntiteImport);
				} else if (recordImport.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE)) {
					recordDestination = effectuerCorrespondance(editingContext, recordImport, nomEntiteImport);
				}
			} catch (Exception e) {
				LogManager.logException(e);
				return e.getMessage();
			}

			// Pour gérer les GrhumRepartEnfant en fonction du SFT
			if (recordDestination != null && recordDestination instanceof EOGrhumEnfant) {
				if (((EOGrhumEnfant) recordDestination).gererRepartEnfant((EOEnfant) recordImport)) {
					try {
						LogManager.logDetail("Sauvegarde de l'editing context pour les repart enfants");
						editingContext.saveChanges();
					} catch (Exception exc) {
						LogManager.logException(exc);
						return "Lors de l'enregistement d'une repart, erreur lors de la sauvegarde de l'editing context";
					}
				}
			}
			// 03/10/2011 - Pour gérer les dates de décès et les départs
			if (recordDestination != null && recordDestination instanceof EOGrhumIndividu) {
				if (((EOGrhumIndividu) recordDestination).effectuerModificationDepartPourDeces((EOIndividu) recordImport)) {
					try {
						LogManager.logDetail("Sauvegarde de l'editing context pour la date de deces");
						editingContext.saveChanges();
					} catch (Exception exc) {
						LogManager.logException(exc);
						return "Lors de l'enregistement pour la date de deces, erreur lors de la sauvegarde de l'editing context";
					}
				}
			}
		}

		return null;
	}

	// Méthodes statiques privées
	/** Instancie le record de la base destinataire et l'initialise a partir du record d'import */
	private static ObjetPourSIDestinataire creerDestinationEtEnregistrer(EOEditingContext editingContext, ObjetImport recordImport, String nomEntiteImport)
			throws Exception {
		// Rechercher l'entité associée
		Entite entite = ReglesDestination.sharedInstance().entiteAvecNomSource(nomEntiteImport);
		if (entite == null) {
			return null;
		}
		// Instancier le record
		ObjetPourSIDestinataire recordDestinataire = null;
		if (recordImport instanceof EOEnfant) {
			EOEnfant enfantImport = (EOEnfant) recordImport;

			// Vérifier si il n'existe pas dans la base un enfant avec le même nom, prénom et date de naissance
			// C'est le cas pour les enfants dont les deux parents sont dans l'établissement
			// Exceptionnellement, on pourrait tomber sur le cas d'un enfant qui existe et qui ne serait pas rattaché à un parent
			EOGrhumEnfant enfantGrhum = EOGrhumEnfant.enfantPourNomPrenomDateNaissance(editingContext, enfantImport.nom(), enfantImport.prenom(),
					enfantImport.dNaissance());
			if (enfantGrhum != null) {
				recordDestinataire = enfantGrhum;
			}
		}
		if (recordDestinataire == null) {
			// Pas d'enfant équivalent trouvé
			String nomEntiteDestinataire = entite.nomDestination();
			EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(nomEntiteDestinataire);
			recordDestinataire = (ObjetPourSIDestinataire) classDescription.createInstanceWithEditingContext(null, null);
			if (recordDestinataire != null) {
				editingContext.insertObject(recordDestinataire); // Pour pouvoir faire l'initialisation des relations, l'objet doit d'abord être inséré dans
																	// l'editing context
				try {
					recordDestinataire.initAvecImport(recordImport); // l'initialisation peut générer une exception
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
		// 24/06/2010
		if (recordDestinataire instanceof InterfaceGestionIdUtilisateur) {
			InterfaceGestionIdUtilisateur recordModif = (InterfaceGestionIdUtilisateur) recordDestinataire;
			EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
			recordModif.setPersIdCreation(utilisateur.persId());
			recordModif.setPersIdModification(utilisateur.persId());
		}
		LogManager.logDetail("Sauvegarde de l'editing context");
		editingContext.saveChanges();

		return recordDestinataire;
	}

	private static void creerCorrespondanceEtEnregistrer(EOEditingContext editingContext, String nomEntiteImport, ObjetImport recordImport,
			ObjetPourSIDestinataire recordDestinataire) {	
		LogManager.logDetail("Creation de la correspondance");
		String nomEntiteCorresp = nomEntiteImport + "Corresp";
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(nomEntiteCorresp);
		ObjetCorresp correspondance = (ObjetCorresp) classDescription.createInstanceWithEditingContext(null, null);
		editingContext.insertObject(correspondance);
		correspondance.initAvec(recordImport, recordDestinataire);
		if (correspondance instanceof EOTelephoneCorresp) {
			correspondance.updaterAvecRecord(recordImport);
		}
		LogManager.logDetail("Sauvegarde de l'editing context");
		editingContext.saveChanges();
	}

	// 24/06/2010 - pour homogénéiser les traitements sur l'objet destinataire en mode Update ou Correspondance
	private boolean modifierDestinationAvecRecord(ObjetPourSIDestinataire recordDestination, ObjetImport recordImport) {
		try {
			boolean estModifie = recordDestination.updateAvecRecord(recordImport);
			// Vérifier si le record était valide, sinon le revalider si nécessaire
			boolean validiteModifiee = false;
			if (recordDestination.estValide() == false && recordDestination.importExigeRevalidation()) {
				recordDestination.setEstValide(true);
				validiteModifiee = true;
			}
			if (estModifie || validiteModifiee) {
				if (recordDestination instanceof ObjetPourSIDestinataireAvecDates) {
					((ObjetPourSIDestinataireAvecDates) recordDestination).setDModification(new NSTimestamp());
				}
				// 24/06/2010
				if (recordDestination instanceof InterfaceGestionIdUtilisateur) {
					EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
					InterfaceGestionIdUtilisateur record = (InterfaceGestionIdUtilisateur) recordDestination;
					record.setPersIdModification(utilisateur.persId());
					// 08/03/2011 - Forcer le persIdCreation si il est nul
					if (record.persIdCreation() == null) {
						record.setPersIdCreation(utilisateur.persId());
					}
				}
			}
			return estModifie || validiteModifiee;
		} catch (Exception e) {
			return false;
		}
	}
}
