//AutomateImport.java

//Created by Christine Buttin on Fri May 06 2005.
//Copyright (c) 2005 __MyCompanyName__. All rights reserved.
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import java.util.Date;

import org.apache.xerces.jaxp.SAXParserFactoryImpl;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.CocktailUtilities;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.ResultatImport.Donnees;
import org.cocktail.connecteur.importer.moteur.ResultatImport.ObjetAvecErreur;
import org.cocktail.connecteur.importer.moteur.erreurs.ErreurManager;
import org.cocktail.connecteur.importer.moteur.parseur.ParseurCVS;
import org.cocktail.connecteur.importer.moteur.parseur.ParseurXML;
import org.cocktail.connecteur.serveur.ServerThreadManager;
import org.cocktail.connecteur.serveur.TaskImportAutomatique;
import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCompte;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel;
import org.cocktail.connecteur.serveur.modele.entite_import.EOStructure;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.conges.Absences;
import org.cocktail.connecteur.serveur.modele.importer.EOCible;
import org.cocktail.connecteur.serveur.modele.importer.EOErreurImport;
import org.cocktail.connecteur.serveur.modele.importer.EOFichierImport;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;
import org.cocktail.connecteur.serveur.modele.importer.EOSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.ParserAdapter;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

// 22/06/2010 - modifié pour gérer des individus qui ne sont pas des personnels. Lors de l'import, si on trouve des données de carrière ou
// de contrat ou des occupations qui concernent un individu qui n'est pas un personnel, on ne les importe pas
// 19/08/2011 - Ajout du reset des absences lors de l'import des entités au cas où il y aurait plusieurs imports de suite
// 27/09/2011 - Correction d'un bug lorsqu'il y existe une priorité globale sur l'entité GrhumPersonnel : lorsqu'il s'agit d'une opération d'insertion, le personnel doit être créé
// 30/09/2011 - Ajout de la génération d'un log lorsque des départs ont été transférés dans le SI Destinataire
public class AutomateImport {
	private static AutomateImport sharedInstance;
	private ResultatImport resultatImport;
	private ServerThreadManager threadCourant;
	private String nomFichierImport;
	private String repertoireFichierImport;
	private EOSource applicationSource;
	private EOCible applicationCible;
	private EOGrhumIndividu responsableImport;
	private EOEditingContext editingContext;
	private boolean traitementAutomatiqueActif;
	private int nbRecordsLus, nbRecordsRejetesPourPriorite, nbRecordsRejetesPourIdentite;
	private static int numFichierErreur, numDiagnostic; // utiliser pour incrémenter les numéros de fichier
	private final static String PATH_IMPORT = "PATH_IMPORT";

	private final static String NOM_FICHIER_ERREUR_IMPORT = "ERREUR_IMPORT";
	private final static String NOM_FICHIER_ERREUR_DB = "ERREUR_TRANSFERT_DB";
	private final static String NOM_FICHIER_RAPPORT = "RAPPORT_IMPORT";

	private final static String NOM_FICHIER_LOG_DEPART = "Log_Departs_Pour_";

	/** texte contenu dans le message retourne en fin de lecture du fichier d'import */
	public final static String FIN_PARSING = "Lecture fichier terminee";
	/** texte contenu dans le message retourne en fin de lecture du fichier d'import en cas d'erreur */
	public final static String DEJA_IMPORTE = "Toutes les donnees ont deja ete importees";
	/**
	 * texte contenu dans le message retourne en fin de lecture du fichier d'import en cas d'erreur si il y a des records valides
	 */
	public final static String FICHIER_ERRONE = "Erreurs detectees pendant la lecture des donnees";
	/**
	 * texte contenu dans le message retourne en fin de lecture du fichier d'import en cas d'erreur si il y a des records valides
	 */
	public final static String FIN_PARSING_AVEC_ERREUR = "Erreurs detectees pendant la lecture des donnees";
	/** texte contenu dans le message retourne en fin d'import */
	public final static String FIN_IMPORT = "Import termine";
	/** texte contenu dans le message retourne en fin d'import lorsque des erreurs ont ete detectees */
	public final static String FIN_IMPORT_AVEC_ERREUR = "Import termine avec erreurs";
	/** texte contenu dans le message retourne en fin de transfert dans le SI Destinataire */
	public final static String FIN_TRANSFERT = "Transfert termine";
	/**
	 * texte contenu dans le message retourne en fin de transfert dans le SI Destinataire si les homonymes n'ont pas ete traites
	 */
	public final static String FIN_TRANSFERT_AVEC_HOMONYMES = "Transfert termine avec homonymes";
	/** texte contenu dans le message retourne en cas d'exception */
	public final static String SIGNALER_EXCEPTION = "Erreur : ";

	/** Etape du parsing */
	public final static int ETAPE_PARSING = 1;
	/** Etape de l'import dans la base d'import */
	public final static int ETAPE_IMPORT = 2;
	/** Etape du transfert dans le SI Destinataire */
	public final static int ETAPE_TRANSFERT = 3;
	/** Etape de generation des logs sur les departs apres leur transfert dans le SI Destinataire */
	public final static int ETAPE_LOG_DEPART = 4;

	private final static String NOM_PARAM_MODE_AUTOMATIQUE = "MODE_AUTOMATIQUE";
	private final static String NOM_PARAM_CHECK_DOUBLON = "VERIFIER_DOUBLONS";
	private final static String NOM_PARAM_SAVE_CHANGES_STEP = "SAVE_CHANGES_STEP";

	// Par défaut, si on ne trouve pas le paramètre, on active la recherche des doublon
	private final static boolean PARAM_CHECK_DOUBLON_VALEUR_PAR_DEFAUT = true;
	private final static int PARAM_SAVE_CHANGES_STEP_VALEUR_PAR_DEFAUT = 500;

	private ReglesImport reglesImport = new ReglesImport();

	private boolean modeAutomatique, parsingEnCours;
	private boolean checkDoublons = PARAM_CHECK_DOUBLON_VALEUR_PAR_DEFAUT;
	private int saveChangesStep = PARAM_SAVE_CHANGES_STEP_VALEUR_PAR_DEFAUT;

	public AutomateImport() {
		editingContext = TaskImportAutomatique.editingContextPermanent();
	}

	public static AutomateImport sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new AutomateImport();
			numFichierErreur = 0;
			numDiagnostic = 0;
			String param = EOImportParametres.valeurParametrePourCle(sharedInstance.editingContext(), NOM_PARAM_MODE_AUTOMATIQUE);
			sharedInstance.setModeAutomatique(param != null && param.equals(CocktailConstantes.VRAI));
			String paramCheckDoublons = EOImportParametres.valeurParametrePourCle(sharedInstance.editingContext(), NOM_PARAM_CHECK_DOUBLON);
			if (paramCheckDoublons == null)
				sharedInstance.setCheckDoublons(PARAM_CHECK_DOUBLON_VALEUR_PAR_DEFAUT);
			else
				sharedInstance.setCheckDoublons(paramCheckDoublons.equals(CocktailConstantes.VRAI));
			String paramSaveChangesStep = EOImportParametres.valeurParametrePourCle(sharedInstance.editingContext(), NOM_PARAM_SAVE_CHANGES_STEP);
			if (paramSaveChangesStep == null)
				sharedInstance.setSaveChangesStep(PARAM_SAVE_CHANGES_STEP_VALEUR_PAR_DEFAUT);
			else {
				try {
					sharedInstance.setSaveChangesStep(Integer.parseInt(paramSaveChangesStep));
				} catch (NumberFormatException e) {
					sharedInstance.setSaveChangesStep(PARAM_SAVE_CHANGES_STEP_VALEUR_PAR_DEFAUT);
				}
			}
		}
		return sharedInstance;
	}

	// Accesseurs
	public EOEditingContext editingContext() {
		return editingContext;
	}

	public void setCheckDoublons(boolean value) {
		checkDoublons = value;
	}

	public boolean isCheckDoublons() {
		return checkDoublons;
	}

	public int getSaveChangesStep() {
		return saveChangesStep;
	}

	public void setSaveChangesStep(int saveChangesStep) {
		this.saveChangesStep = saveChangesStep;
	}

	public String pathImport() {
		String path = EOImportParametres.valeurParametrePourCle(new EOEditingContext(), "PATH_IMPORT");
		return path;
	}

	public String pathDiagnoticServer() {
		String path = StringCtrl.replace((pathImport() + "/"), "//", "/") + "Diagnostics/";
		LogManager.logInformation((new Date()) + ",   - AutomateImport.pathDiagnoticServer() PATH DIAG : " + path);
		CocktailUtilities.verifierPathEtCreer(path);
		return path;
	}

	public String pathErreursServer() {
		String path = StringCtrl.replace((pathImport() + "/"), "//", "/") + "Erreurs/";
		LogManager.logInformation((new Date()) + ",   - AutomateImport.pathErreursServer() PATH ERREUR : " + path);
		CocktailUtilities.verifierPathEtCreer(path);
		return path;
	}

	/** retourne l'application dont proviennent les donnees */
	public EOSource applicationSource() {
		return applicationSource;
	}

	/** retourne l'application destinataire des donnees */
	public EOCible applicationCible() {
		return applicationCible;
	}

	/** retourne l'utilisateur responsable de l'import */
	public EOGrhumIndividu responsableImport() {
		return responsableImport;
	}

	/** retourne true si on peut faire un nouvel import */
	public boolean peutImporter() {
		return importCourant() == null && parsingEnCours == false;
	}

	/** retourne true si il y a un import en cours */
	public boolean peutTransfererDansDestination() {
		EOFichierImport importCourant = importCourant();
		return importCourant != null && (importCourant.donneesImportees() || importCourant.transfertEnCours());
	}

	/** retourne true si l'automate travaille en mode automatique */
	public boolean modeAutomatique() {
		return modeAutomatique;
	}

	public void setModeAutomatique(boolean aBool) {
		modeAutomatique = aBool;
	}

	/** retourne true si il y a un traitement automatique en route */
	public boolean traitementAutomatiqueActif() {
		return traitementAutomatiqueActif;
	}

	public void setTraitementAutomatiqueActif(boolean aBool) {
		traitementAutomatiqueActif = aBool;
	}

	/**
	 * 
	 * @return
	 */
	public NSArray listeFichiersImport() {
		String path = EOImportParametres.valeurParametrePourCle(new EOEditingContext(), PATH_IMPORT);

		if (path != null) {
			if (path.substring(path.length() - 1).equals(File.separator) == false) {
				path = path + File.separator;
			}
			File directory = new File(path);
			if (directory.isDirectory()) {

				File[] files = directory.listFiles();

				NSMutableArray result = new NSMutableArray();
				for (int i = 0; i < files.length; i++) {
					File file = files[i];
					if (file.isDirectory() == false && file.getName().startsWith(".") == false) {
						result.addObject(file.getName());
					}
				}
				return (NSArray) result;
			}
		}
		return null;
	}

	/**
	 * Initialise un import
	 * 
	 * @param nomFichierImport
	 *            nom du fichier d'import
	 * @param applicationSourceID
	 *            global id de l'application source
	 * @param applicationCibleID
	 *            global id de l'application cible
	 */
	public void init(String repertoireFichierImport, String nomFichierImport, EOGlobalID applicationSourceID, EOGlobalID applicationCibleID) {
		this.nomFichierImport = nomFichierImport;

		if (repertoireFichierImport == null || repertoireFichierImport.equals("")) {
			repertoireFichierImport = EOImportParametres.valeurParametrePourCle(new EOEditingContext(), PATH_IMPORT);

			if (repertoireFichierImport == null)
				repertoireFichierImport = "";
		}
		this.repertoireFichierImport = repertoireFichierImport;

		if (applicationSourceID != null)
			this.applicationSource = (EOSource) Finder.objetForGlobalIDDansEditingContext(applicationSourceID, editingContext);
		else
			this.applicationSource = EOSource.getDefault(editingContext);

		if (applicationCibleID != null)
			this.applicationCible = (EOCible) Finder.objetForGlobalIDDansEditingContext(applicationCibleID, editingContext);
		else
			this.applicationCible = EOCible.getDefault(editingContext);
	}

	/**
	 * Importe les donnees depuis un fichier (XML ou CVS) sans les enregistrer dans les tables d'import.<BR>
	 * L'import se deroule en deux phases :<BR>
	 * Dans la premiere phase, les donnees sont lues dans le fichier et on determine si elles sont valides ou non : - respect des champs obligatoires,respect
	 * des nomenclatures, respect des types de donnees<BR>
	 * A l'issue de cette phase, les donnees lues dans les fichiers sont separees en deux groupes (donnees valides (donc importables) et donnees invalides) et
	 * un fichier listant les erreurs est genere pour aider l'utilisateur a faire les corrections<BR>
	 * 
	 * @return le diagnostic du traitement
	 */
	public String importerDonnees() {
		if (parsingEnCours) { // Traitement automatique lancé
			return "Autre import en cours";
		}
		return importer(new Boolean(false), null);
	}

	/**
	 * Importe les donnees depuis un fichier (XML ou CVS) et les enregistre ou non directement dans les tables d'import.<BR>
	 * L'import se deroule en deux phases :<BR>
	 * Dans la premiere phase, les donnees sont lues dans le fichier et on determine si elles sont valides ou non : - respect des champs obligatoires,respect
	 * des nomenclatures, respect des types de donnees<BR>
	 * A l'issue de cette phase, les donnees lues dans les fichiers sont separerees en deux groupes (donnees valides (donc importables) et donnees invalides) et
	 * un fichier listant les erreurs est genere pour aider l'utilisateur a faire les corrections<BR>
	 * La deuxieme phase n'est declenchee que si enregistrerDonnees vaut true.
	 * 
	 * @param enregistrerDonnees
	 *            true si les donnees valides doivent etre directement enregistrees dans les tables d'import
	 * @param serverThread
	 *            traitement dans un thread specifique
	 * @return le diagnostic du traitement
	 */
	public String importerDonnees(Boolean enregistrerDonnees, ServerThreadManager serverThread) {
		// Le traitement réel est fait dans la méthode importer(), ici on assure qu'il n'y a pas plusieurs parsing en cours
		if (parsingEnCours || (serverThread != null && traitementAutomatiqueActif())) { // Traitement automatique lancé
			if (serverThread != null) {
				serverThread.setMessage("Termine");
			}
			return "Autre import en cours";
		}
		return importer(enregistrerDonnees, serverThread);
	}

	/**
	 * Enregistre les donnees precedemment lues dans un fichier dans la base d'import.<BR>
	 * Dans cette phase, une entree est generee dans la table FICHIER_IMPORT pour definir l'import courant : il n'est plus alors possible d'importer un nouveau
	 * fichier. Les donnees valides sont analysees pour determiner l'operation a effectuer lors de l'import dans le SI destinataire ainsi que leur statut
	 * (detection d'homonymes dans le SI destinataire, valeurs tronquees. Puis, elles sont enregistrees dans la base d'import.<BR>
	 * 
	 * @return le diagnostic du traitement
	 * 
	 **/
	public String enregistrerDonneesImport(ServerThreadManager serverThread) {
		if (parsingEnCours || (serverThread != null && traitementAutomatiqueActif())) { // Traitement lancé par un client
			if (serverThread != null) {
				serverThread.setMessage("Termine");
			}
			return "Autre import en cours";
		}
		informerThread("Enregistrement des donnees dans la base d'import");
		threadCourant = serverThread;
		if (resultatImport != null) {
			if (enregistrerDonneesDansImport(resultatImport.enregistrementsValides())) {
				// Vérifier si des logs ont été générés
				int nbLogsErreurs = EOLogImport.nbLogsErreursPourImport(editingContext(), importCourant());
				int nbLogsTronques = EOLogImport.nbLogsTronquesPourImport(editingContext(), importCourant());
				String message = messageOperationImport(resultatImport.enregistrementsValides());

				int nbTotal = resultatImport.nbEnregistrementValides() - nbRecordsRejetesPourIdentite - nbRecordsRejetesPourPriorite;
				message += "\n" + "Nombre de records importes : " + nbTotal;
				message += "\n";
				if (nbRecordsRejetesPourIdentite > 0) {
					message += "\nNombre de records non pris en compte car dans le SI Destinataire : " + nbRecordsRejetesPourIdentite + "\n";
				}
				if (nbRecordsRejetesPourPriorite > 0) {
					message += "\nNombre de records non pris en compte pour des raisons de priorite : " + nbRecordsRejetesPourPriorite + "\n";
				}
				if (nbLogsErreurs > 0) {
					message += "\nNombre d'erreurs generees : " + nbLogsErreurs + "\n";
				}
				if (nbLogsTronques > 0) {
					message += "\nNombre de records tronques : " + nbLogsTronques + "\n";
				}
				if (nbLogsErreurs > 0 || nbLogsTronques > 0) {
					message += "VEUILLEZ VERIFIER LES LOGS";
				}
				message += "\n" + FIN_IMPORT;
				informerThread(message);
				ajouterRapport(ETAPE_IMPORT, "\n\nImport dans la base d'import\n" + message);
				resultatImport = null;
				if (traitementAutomatiqueActif()) {
					if (nbLogsErreurs == 0) {
						return FIN_IMPORT;
					} else {
						return FIN_IMPORT_AVEC_ERREUR;
					}
				} else {
					return message;
				}
			} else { // Une erreur s'est produite
				return "Erreur pendant l'import des donnees dans la base d'import";
			}
		} else {
			return "Pas de donnees a importer";
		}
	}

	/**
	 * Enregistre les donnees des tables d'import dans le SI Destinataire
	 * 
	 * @param responsableID
	 *            globalID du responsable de l'import des donnees dans le SI destinataire
	 * @param prendreEnCompteEntitesTronquees
	 *            true si il faut aussi prendre en compte les entites tronquées
	 * @param serverThread
	 *            traitement dans un thread specifique
	 * @return le diagnostic du traitement
	 */
	public String enregistrerImportDansBaseDestinataire(EOGlobalID responsableID, Boolean prendreEnCompteEntitesTronquees, Boolean existeHomonymes,
			ServerThreadManager serverThread) {
		if (parsingEnCours || (serverThread != null && traitementAutomatiqueActif())) { // Traitement lancé par un client
			if (serverThread != null) {
				serverThread.setMessage("Termine");
			}
			return "Autre import en cours";
		}
		threadCourant = serverThread;
		informerThread("Enregistrement de l'import dans le SI destinataire");
		// Commencer par lire le modèle de données

		if (ReglesDestination.sharedInstance().modeleDonnees() == null) {
			// charger le modèle de données de la base destinataire
			ReglesDestination.sharedInstance().initAvecModele("ModeleDestination");
		}
		if (nomFichierImport == null) { // quand le serveur est relancé et qu'on est en train dans une étape de transfert
			EOFichierImport importCourant = (EOFichierImport) Finder.importCourant(editingContext());
			nomFichierImport = importCourant.nomFichier();
		}
		if (peutTransfererDansDestination()) {
			responsableImport = (EOGrhumIndividu) Finder.objetForGlobalIDDansEditingContext(responsableID, editingContext());
			return enregistrerImportDansDestination(prendreEnCompteEntitesTronquees.booleanValue(), existeHomonymes.booleanValue());
		} else {
			return "Import non possible dans le SI destinataire , pas de donnees a importer";
		}
	}

	/**
	 * Termine une operation d'import alors qu'un transfert est en cours
	 * 
	 * @return le diagnostic du traitement
	 */
	public String abandonnerTransfertImportCourant(ServerThreadManager serverThread) {
		informerThread("Fin de l'import courant");
		EOFichierImport fichierImport = importCourant();
		if (fichierImport.transfertEnCours() == false) {
			return "";
		}
		chargerLeModeleDeDonnees();
		java.util.Enumeration e = nomEntitesImportTrieesParPriorite().reverseObjectEnumerator(); // on commence par les priorités inférieures
		while (e.hasMoreElements()) {
			String nomEntite = (String) e.nextElement();
			NSArray objetsImport = ObjetImport.rechercherObjetsPourImport(editingContext(), nomEntite);
			if (objetsImport.count() > 0) {
				informerThread("Invalidation des records " + nomEntite);
				java.util.Enumeration e1 = objetsImport.objectEnumerator();
				while (e1.hasMoreElements()) {
					ObjetImport objet = (ObjetImport) e1.nextElement();
					objet.setTemImport(ObjetImport.ABANDON_TRANSFERT);
				}
				if (nomEntite.equals("Individu")) {
					// Commencer par invalider les personnels
					informerThread("Invalidation des records de type Personnel");
					objetsImport = ObjetImport.rechercherObjetsPourImport(editingContext(), "Personnel");
					if (objetsImport.count() > 0) {
						informerThread("Invalidation des records " + nomEntite);
						e1 = objetsImport.objectEnumerator();
						while (e1.hasMoreElements()) {
							ObjetImport objet = (ObjetImport) e1.nextElement();
							objet.setTemImport(ObjetImport.ABANDON_TRANSFERT);
						}
					}
				}
			}
		}
		EOFichierImport importCourant = importCourant();
		informerThread("Invalidation des logs");
		EOLogImport.invaliderLogsPourImport(editingContext(), importCourant);
		importCourant.terminerPhaseTransfert();
		EOErreurImport erreurImport = recordErreurCourant();
		if (erreurImport != null) {
			erreurImport.setTemValide(CocktailConstantes.FAUX);
		}
		try {
			sauvegardeEditingContext();
			;
			return "Import courant termine";
		} catch (Exception exc) {
			signalerExceptionThread(exc);
			return "";
		}
	}

	/**
	 * Invalide toutes les donnees de l'import courant ainsi que les logs et l'import lui-meme, avant que le transfert dans le SI Destinataire n'est commence.
	 * On sauvegarde l'editing context apres annulation de tous les records d'une entite
	 */
	public String supprimerImportCourant(ServerThreadManager serverThread) {
		try {
			threadCourant = serverThread;
			// Travailler dans un nouvel editing context pour éviter les problèmes
			chargerLeModeleDeDonnees();
			EOFichierImport fichierImport = importCourant();
			if (fichierImport.donneesLuesOuImportees() == false) {
				return "";
			}
			informerThread("Suppression des logs");
			editingContext().lock();
			editingContext().setUndoManager(null); // Pour ne pas que tout soit stocker dans l'undo manager
			invaliderRecords(EOLogImport.rechercherLogsValidesPourImport(editingContext(), fichierImport));
			// Il faut sauvegarder l'editing context maintenant car on ne sait pas dans quel ordre seront les objets supprimés
			// dans l'editing context et on peut avoir des problèmes de contraintes d'intégrité
			LogManager.logDetail("Sauvegarde de l'editing context apres destruction des logs");
			java.util.Enumeration e = nomEntitesImportTrieesParPriorite().reverseObjectEnumerator(); // on commence par les priorités inférieures
			while (e.hasMoreElements()) {
				String nomEntite = (String) e.nextElement();
				if (nomEntite.equals("Individu")) {
					// Commencer par supprimer les personnels
					informerThread("Suppression des records de type Personnel");
					invaliderRecords(ObjetImport.rechercherObjetsPourImport(editingContext(), "Personnel"));
				}
				informerThread("Suppression des records de type " + nomEntite);
				invaliderRecords(ObjetImport.rechercherObjetsPourImport(editingContext(), nomEntite));
			}
			LogManager.logDetail("Sauvegarde de l'editing context apres destruction des imports");
			informerThread("Suppression du record d'import");
			EOErreurImport erreurImport = recordErreurCourant();
			if (erreurImport != null) {
				erreurImport.supprimerRelations();
				editingContext().deleteObject(erreurImport);
				// Sauvegarder les données
				sauvegardeEditingContext(); // Pour être sûr qu'il est supprimé
			}
			fichierImport.invaliderImportCourant();

			// Sauvegarder les données
			sauvegardeEditingContext();
			return "Suppression Import courant OK";
		} catch (Exception exc) {
			signalerExceptionThread(exc);
			return "";
		} finally {
			editingContext().unlock();
		}
	}

	/** Supprime tous les records annules de la base d'import. Retourne true si pas de pb */
	public boolean nettoyerBaseImport() {
		try {
			EOUtilities.executeStoredProcedureNamed(new EOEditingContext(), "nettoyerBase", new NSDictionary());
			return true;
		} catch (Exception e) {
			LogManager.logException(e);
			return false;
		}
	}

	/**
	 * 
	 * @return
	 */
	public void nettoyageBaseImport() throws Exception {
		EOUtilities.executeStoredProcedureNamed(new EOEditingContext(), "nettoyerImport", new NSDictionary());
	}

	/**
	 * 
	 * @return
	 */
	public boolean nettoyageBaseDestination() {
		try {
			EOUtilities.executeStoredProcedureNamed(new EOEditingContext(), "nettoyerDestination", new NSDictionary());
			return true;
		} catch (Exception e) {
			LogManager.logException(e);
			return false;
		}
	}

	public NSArray<String> nomEntitesImportTrieesParPriorite() {
		chargerLeModeleDeDonnees();
		return reglesImport.nomEntitesBaseImportTrieesParPriorite();
	}

	public Entite entitePourNomTable(String nomTable) {
		chargerLeModeleDeDonnees();
		return reglesImport.entiteAvecNomSource(nomTable);
	}

	public Entite entiteImportPourNom(String nom) {
		chargerLeModeleDeDonnees();
		return reglesImport.entiteAvecNomDestination(nom);
	}

	public NSArray nomAttributsComparaisonPourEntiteImport(String nomEntiteImport) {
		Entite entite = entiteImportPourNom(nomEntiteImport);
		if (entite != null) {
			return entite.nomsAttributsComparaison();
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public NSArray entitesGeneriques() {
		chargerLeModeleDeDonnees();

		return reglesImport.modeleDonnees().entitesAvecAffichageGenerique(editingContext());
	}

	public NSArray nomEntitesDestination() {
		if (ReglesDestination.sharedInstance().modeleDonnees() == null) {
			// charger le modèle de données de la base destinataire
			ReglesDestination.sharedInstance().initAvecModele("ModeleDestination");
		}
		return ReglesDestination.sharedInstance().nomEntitesDestinationTrieesParPriorite();
	}

	/** Retourne le nom des attributs d'une entite du systeme de destination */
	public NSArray nomAttributsPourEntiteDestination(String nomEntiteDestination) {
		if (ReglesDestination.sharedInstance().modeleDonnees() == null) {
			// charger le modèle de données de la base destinataire
			ReglesDestination.sharedInstance().initAvecModele("ModeleDestination");
		}
		Entite entite = ReglesDestination.sharedInstance().entiteAvecNomDestination(nomEntiteDestination);
		return entite.nomAttributsPourType("nomDestination");
	}

	/** Retourne le nom de l'entite de destination liee a l'entite d'import */
	public String nomEntiteDestinationPourEntiteImport(String nomEntiteImport) {
		Entite entite = entiteDestinationPourEntiteImport(nomEntiteImport);
		if (entite != null) {
			return entite.nomDestination();
		} else {
			return null;
		}
	}

	public Entite entiteDestinationPourEntiteImport(String nomEntiteImport) {
		if (ReglesDestination.sharedInstance().modeleDonnees() == null) {
			// charger le modèle de données
			ReglesDestination.sharedInstance().initAvecModele("ModeleDestination");
		}
		return ReglesDestination.sharedInstance().entiteAvecNomSource(nomEntiteImport);
	}

	/** Retourne le diagnostic de la verification sous la forme d'une string */
	public String verifierDonnees(String nomEntite, NSDictionary valeursAttributs) {

		chargerLeModeleDeDonnees();
		String resultat = reglesImport.validerEntiteImport(nomEntite, new NSMutableDictionary(valeursAttributs));
		if (resultat != null && resultat.length() > 0) {
			return reglesImport.remettreMessageErreurEnFormePourEntite(resultat, nomEntite);
		} else {
			return resultat;
		}
	}

	/** Retourne le diagnostic de la verification sous la forme d'une string */
	public String verifierReglesMetier(String nomEntite, NSDictionary valeursAttributs) {
		chargerLeModeleDeDonnees();
		return reglesImport.verifierReglesMetier(nomEntite, new NSMutableDictionary(valeursAttributs));
	}

	public String rapatrierFichierDiagnostic() {
		LogManager.logInformation("Rapatriement du fichier de diagnostic");
		EOFichierImport importCourant = importCourant();
		if (importCourant == null || importCourant.nomRapport() == null) {
			return null;
		}
		String result = null;
		String path = pathDiagnoticServer() + importCourant.nomRapport();
		LogManager.logDetail("path rapport " + path);
		result = lireFichier(path);
		if (result != null && (result.length() == 0 || result.indexOf("Erreur") == 0)) {
			result = null;
		}

		return result;
	}

	// 30/09/2011 - Gestion des logs de départ
	public String rapatrierLogsDepart() {
		LogManager.logInformation("Rapatriement du fichier de diagnostic");
		String nomFichierLogDepart = nomFichierLogDepart();
		if (nomFichierLogDepart == null) {
			return null;
		}
		LogManager.logDetail("Rapatriement du log sur les departs");
		String path = pathDiagnoticServer() + nomFichierLogDepart;
		LogManager.logDetail("path logDepart " + path);
		String result = lireFichier(path);

		return result;
	}

	/**
	 * Retourne le rapport genere a l'etape passee en parametre pour le fichier d'import passe en parametre
	 */
	public String rapatrierRapport(String nomFichierImport, int etape) {
		LogManager.logInformation("Rapatriement du rapport de l'etape " + etape);
		// Commencer par rapatrier le rapport de parsing
		String nomRapport = null;
		if (etape == ETAPE_TRANSFERT) {
			EOFichierImport lastImport = EOFichierImport.lastImport(editingContext(), nomFichierImport);
			if (lastImport != null) {
				nomRapport = lastImport.nomRapport();
			}
		}
		if (nomRapport == null) {
			int lg = nomFichierImport.indexOf(".");
			if (lg >= 0) {
				nomRapport = NOM_FICHIER_RAPPORT + "_" + nomFichierImport.substring(0, lg);
			}
			String date = DateCtrl.dateToString(new NSTimestamp());
			date = date.replaceAll("/", "");
			nomRapport = nomRapport + date + "_" + numDiagnostic + ".txt";
		}
		String pathFichier = pathDiagnoticServer() + nomRapport;
		LogManager.logDetail("path rapport parsing " + pathFichier);
		if (pathFichier == null) {
			return "Chemin d'accès pour le rapport de parsing invalide";
		}
		return lireFichier(pathFichier);
	}

	public String rapatrierFichierErreur(boolean estErreurImport) {
		LogManager.logInformation("Rapatriement du fichier d'erreur");
		EOErreurImport erreurImport = recordErreurCourant();
		if (erreurImport == null) {
			return null;
		}
		String nomFichier = "";
		if (estErreurImport) {
			nomFichier = erreurImport.nomErreurImport();
		} else {
			nomFichier = erreurImport.nomErreurTransfert();
		}

		String path = pathErreursServer() + nomFichier;
		LogManager.logDetail("path " + path);
		String result = lireFichier(path);
		if (result != null && (result.length() == 0 || result.indexOf("Erreur") == 0)) {
			result = null;
		}
		return result;
	}

	/** Verifie que le path defini par la clede la table IMPORT_PARAMETRES existe bien */
	public String verifierPathPourCle(String clePath) {
		String path = EOImportParametres.valeurParametrePourCle(new EOEditingContext(), clePath);
		if (path != null) {
			File file = new File(path);
			if (file.exists()) {
				return "";
			} else {
				return path + " : chemin d'acces invalide\n";
			}
		} else {
			return clePath + " parametre non defini\n";
		}
	}

	// 30/09/2011 - Gestion des logs pour départ
	public boolean existeLogsPourDepart() {
		return nomFichierLogDepart() != null;
	}

	/** envoie un message au thread courant */
	public void informerThread(String message) {
		if (threadCourant != null) {
			threadCourant.setMessage(message);
		}
		LogManager.logInformation((new Date()) + ",   - " + message);
	}

	/** signale une exception au thread courant */
	public void signalerExceptionThread(Exception e) {
		LogManager.logException(e);
		if (threadCourant != null && e.getMessage() != null) {
			threadCourant.setException(e.getMessage());
		}
	}

	// Notifications
	public void lectureNouveauRecord(NSNotification aNotif) {
		nbRecordsLus++;
		// On n'affiche que tous les 50 records
		if (!traitementAutomatiqueActif() && nbRecordsLus % 50 == 0) {
			informerThread("Lecture du record " + nbRecordsLus);
		}
	}

	// méthodes privées
	// Retourne null si l'initialisation est ou les param&egrave;tres sont incorrects */
	private String verifierInitialisation() {
		String message = "";
		if (applicationSource == null) {
			message += "Application source non initialisee\n";
		}
		if (applicationCible == null) {
			message += "Application cible non initialisee\n";
		}
		if (nomFichierImport == null) {
			message += "Fichier d'import inconnu\n";
		}
		if (repertoireFichierImport == null) {
			message += "Répertoire d'import inconnu\n";
		}
		String formatDate = EOImportParametres.formatDate();
		if (formatDate == null) {
			message += "Format de date inconnu\n";
		}

		// Vérifier les paths
		message += verifierPathPourCle(PATH_IMPORT);
		if (message.length() > 0) {
			informerThread(message);
		} else {
			message = null;
		}
		return message;
	}

	private String importer(Boolean enregistrerDonnees, ServerThreadManager serverThread) {

		threadCourant = serverThread;

		informerThread("Demarrage de l'import");
		String result = verifierInitialisation();
		if (result != null) {
			LogManager.logInformation(result);
			LogManager.logInformation("Import non initialise");
			informerThread("Termine");
			ajouterRapport(ETAPE_PARSING, nomFichierImport + "\nImport non initialise + \n" + result);
			return "Import non initialise";
		}
		try {
			parsingEnCours = true;
			annulerErreur(true); // Au cas où il existe une erreur précédente et qu'on réimporte

			// Analyse des données des fichiers pour vérifier leur cohérence et stockage dans resultatImport
			importerFichier();

			if (resultatImport == null) {
				LogManager.logInformation("Erreur pendant la lecture du fichier. Impossible de faire l'import");
				ajouterRapport(ETAPE_PARSING, nomFichierImport + "\n" + FICHIER_ERRONE + " : impossible de faire l'import");
				parsingEnCours = false;
				return FICHIER_ERRONE;
			}

			int nbErreurs = resultatImport.nbEnregistrementInvalides();
			LogManager.logInformation((new Date()) + ",   - AutomateImport.importer() NB ERREUSRS : " + nbErreurs);
			int nbEnregistrements = resultatImport.nbEnregistrementValides();
			LogManager.logInformation((new Date()) + ",   - AutomateImport.importer() NB ENREGISTREMENTS : " + nbEnregistrements);
			int nbDansBase = resultatImport.nbEnregistrementsDejaImportes();
			LogManager.logInformation((new Date()) + ",   - AutomateImport.importer() NB DANS BASE : " + nbDansBase);
			int nbEnDouble = resultatImport.nbEnregistrementsEnDoubleDansImport();
			LogManager.logInformation((new Date()) + ",   - AutomateImport.importer() NB EN DOUBLE : " + nbEnDouble);
			int total = nbErreurs + nbEnregistrements;
			if (total == 0) {
				LogManager.logInformation(DEJA_IMPORTE);
				ajouterRapport(ETAPE_PARSING, nomFichierImport + "\n" + DEJA_IMPORTE);
				parsingEnCours = false;
				return DEJA_IMPORTE;
			} else {
				String message = buildMessageNewsTraitement(nbErreurs, nbEnregistrements, nbDansBase, nbEnDouble, total);
				informerThread(message);

				// Valeurs tronquées
				if (resultatImport.messagesPourRecordsTronques().count() > 0) {
					informerThread("Records Tronques");
					message += "Records Tronques\n";
					java.util.Enumeration e = resultatImport.messagesPourRecordsTronques().objectEnumerator();
					while (e.hasMoreElements()) {
						String messageTronque = ((String) e.nextElement());
						informerThread(messageTronque);
						message += messageTronque + "\n";
					}
				}
				ajouterRapport(ETAPE_PARSING, nomFichierImport + "\nLecture et analyse\n" + message);
				// Erreurs
				result = "";
				if (nbErreurs > 0) {
					// Imprimer le fichier d'erreurs si il y a des erreurs.
					informerThread("Generation du fichier d'erreurs");

					// Enregistrement des erreurs dans une table

					if (!genererFichierErreur(nomFichierImport, NOM_FICHIER_ERREUR_IMPORT, resultatImport.enregistrementsInvalides(), null, true)) {
						parsingEnCours = false;
						return FIN_PARSING_AVEC_ERREUR + "\nImpossible de generer le fichier d'erreurs";
					}
					informerThread(FIN_PARSING_AVEC_ERREUR + " - Fichier Erreur genere");
					result = FIN_PARSING_AVEC_ERREUR;
					if (nbEnregistrements > 0) {
						result = FIN_PARSING_AVEC_ERREUR + " - " + nbEnregistrements + " valides";
					}
				} else {
					// Si il existe un record d'erreur, annuler l'erreur
					annulerErreur(false);
					result = FIN_PARSING;
				}
				parsingEnCours = false;
				informerThread("Lecture des donnees terminees");
				// On enregistre les données OK dans la base d'import
				if (enregistrerDonnees.booleanValue() && nbErreurs != total) {
					return enregistrerDonneesImport(serverThread);
				} else {
					return result;
				}
			}
		} catch (Exception e) {
			parsingEnCours = false;
			signalerExceptionThread(e);
			return SIGNALER_EXCEPTION + e.getMessage();
		}
	}

	protected void importerFichier() throws Exception {
		resultatImport = null;

		if (nomFichierImport.indexOf(".XML") > 0 || nomFichierImport.indexOf(".xml") > 0) {
			// Il s'agit d'un fichier XML
			resultatImport = importerFichierXMLAvecNotification(reglesImport);
		} else {
			resultatImport = importerFichierCVS();
		}
	}

	private String buildMessageNewsTraitement(int nbErreurs, int nbEnregistrements, int nbDansBase, int nbEnDouble, int total) {
		String message = "";
		if (nbDansBase > 0) {
			message = message + nbDansBase + "/" + nbRecordsLus + " records importés auparavant" + System.getProperty("line.separator");
		}
		message = message + nbEnregistrements + "/" + total + " valides";
		if (nbEnDouble > 0) {
			message = message + System.getProperty("line.separator") + nbEnDouble + "/" + nbRecordsLus + " en double dans le fichier d'import";
		}
		if (nbErreurs > 0) {
			message = message + System.getProperty("line.separator") + "nombre d'Erreurs : " + nbErreurs + "/" + total;
		}
		return message;
	}

	/**
	 * 
	 * @param nomFichier
	 * @param cle
	 * @return
	 */
	private String construirePath(String nomFichier) {
		String path = repertoireFichierImport;

		if (path != null) {

			String dernierCaractere = path.substring(path.length() - 1);

			if (dernierCaractere.equals("/") == false && dernierCaractere.equals("\\") == false) {
				path = path + "/";
			}

			return path + nomFichier;
		} else {
			return null;
		}
	}

	private EOFichierImport importCourant() {
		return (EOFichierImport) Finder.importCourant(editingContext());
	}

	private EOErreurImport recordErreurCourant() {
		return (EOErreurImport) Finder.erreurCourante(editingContext());
	}

	private String ajouterTexte(String texte, String nouveauTexte) {
		if (nouveauTexte != null && nouveauTexte.length() > 0) {
			if (texte != null && texte.length() > 0) {
				texte = texte + System.getProperty("line.separator") + nouveauTexte;
			} else {
				texte = nouveauTexte;
			}
		}
		return texte;
	}

	private ResultatImport importerFichierXMLAvecNotification(ReglesImport reglesImport) throws Exception {
		String pathFichierImport = construirePath(nomFichierImport);
		informerThread("Import XML du fichier " + pathFichierImport);

		// S'ajouter pour recevoir les notifications de lecture de record par le parseur XML
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lectureNouveauRecord", new Class[] { NSNotification.class }),
				ParseurXML.NOUVELLE_ENTITE, null);

		ResultatImport resultats = importerFichierXML(reglesImport, pathFichierImport);

		NSNotificationCenter.defaultCenter().removeObserver(this);

		// On sauve d'éventuelles insertions (logErreur par ex). Le but est de faire un saveChanges massif une fois l'import terminé
		// et non un saveChanges à chaque insertion: problème de mémoire et de performance
		informerThread("Sauvegarde des erreurs dans la base de données");
		resultats.editingContext().saveChanges();
		informerThread("Fin de la lecture du fichier XML");
		return resultats;

	}

	private ResultatImport importerFichierXML(ReglesImport reglesImport, String pathFichierImport) throws SAXException, ParserConfigurationException,
			IOException {
		chargerLeModeleDeDonnees();

		nbRecordsLus = 0;

		LogManager.logDetail("Début parsing");

		ParseurXML analyseur = new ParseurXML(reglesImport, checkDoublons);
		ParserAdapter parseur = getXMLParseur(analyseur);
		parseur.parse(pathFichierImport);

		LogManager.logDetail("Fin parsing");

		return analyseur.resultatImport();
	}

	private void chargerLeModeleDeDonnees() {
		if (reglesImport.modeleDonnees() == null) {
			reglesImport.initAvecModele("ModeleImport");
		}
	}

	private ParserAdapter getXMLParseur(ParseurXML analyseur) throws SAXException, ParserConfigurationException {
		SAXParserFactoryImpl parserFactory = new SAXParserFactoryImpl();
		ParserAdapter parseur = null;
		parseur = new ParserAdapter(parserFactory.newSAXParser().getParser());
		parseur.setContentHandler(analyseur);
		return parseur;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	private ResultatImport importerFichierCVS() throws Exception {
		chargerLeModeleDeDonnees();
		String pathFichierImport = construirePath(nomFichierImport);
		informerThread("Import du fichier texte" + pathFichierImport);
		ParseurCVS parseur = new ParseurCVS(reglesImport, checkDoublons);
		ResultatImport resultat = parseur.parser(pathFichierImport);
		informerThread("Fin de la lecture du fichier texte");
		return resultat;
	}

	/**
	 * 
	 * @param donnees
	 * @return
	 */
	private boolean enregistrerDonneesDansImport(Donnees donnees) {

		LogManager.logDetail("Début du passage dans enregistrerDonneesDansImport");

		LogManager.logInformation(new Date() + ",   - Param " + NOM_PARAM_CHECK_DOUBLON + " = " + Boolean.toString(checkDoublons));
		LogManager.logInformation(new Date() + ",   - Param " + ObjetImport.NOM_PARAM_CHECK_HOMONYMS + " = "
				+ Boolean.toString(ObjetImport.litParametreCheckHomonyms(editingContext())));
		LogManager.logInformation(new Date() + ",   - Param " + ObjetImport.NOM_PARAM_CHECK_UPDATES + " = "
				+ Boolean.toString(ObjetImport.litParametreCheckUpdates(editingContext())));
		LogManager.logInformation(new Date() + ",   - Param " + NOM_PARAM_SAVE_CHANGES_STEP + " = " + saveChangesStep);

		String param = EOImportParametres.valeurParametrePourCle(editingContext(), "COMPTE_AUTO");
		boolean creationAutomatiqueComptes = (param != null && param.equals(CocktailConstantes.VRAI));

		LogManager.logDetail("Création automatique des comptes : " + creationAutomatiqueComptes);
		LogManager.logDetail("Nom du fichier d'import : " + nomFichierImport);
		LogManager.logDetail("Application source : " + applicationSource().srcLibelle());
		LogManager.logDetail("Application ciblee : " + applicationCible().cibLibelle());
		// Commencer par générer un record de type FichierImport
		EOFichierImport fichierImport = new EOFichierImport();
		fichierImport.initAvecNomFichier(nomFichierImport);
		editingContext().insertObject(fichierImport);
		fichierImport.preparerRelations(applicationSource(), applicationCible());
		sauvegardeEditingContext();
		nbRecordsRejetesPourPriorite = 0;
		nbRecordsRejetesPourIdentite = 0;

		EOGrhumPrioriteEntite prioritePersonnel = EOGrhumPrioriteEntite.rechercherPrioriteEntitePourEntite(editingContext(), EOGrhumPersonnel.ENTITY_NAME);
		NSMutableArray entitesImportees = new NSMutableArray(); // pour vérifier à la fin les correspondances
		// On prend les entités dans l'ordre de priorité de création (créer les individus puis les structures,...)

		for (String nomEntite : nomEntitesImportTrieesParPriorite()) {

			Absences.reset();
			Entite entiteDestination = entiteDestinationPourEntiteImport(nomEntite);
			NSArray donneesPourEntite = donnees.donneesPourNomEntite(nomEntite);

			if (donneesPourEntite != null && donneesPourEntite.count() > 0) {
				entitesImportees.addObject(nomEntite);

				// ajoute la donnée même si elle peut ne déclencher aucune erreur. Permet ainsi de tracer les données avec aucune erreur
				ErreurManager.instance().ajouterEntite(nomEntite);

				EOGrhumPrioriteEntite prioriteEntite = EOGrhumPrioriteEntite.rechercherPrioriteEntitePourEntite(editingContext(),
						entiteDestination.nomDestination());
				int totalRecordsPourEntite = donneesPourEntite.count();
				LogManager.logDetail("Nombre entités avant tri : " + totalRecordsPourEntite);
				informerThread(nomEntite + " : " + totalRecordsPourEntite + " records à importer");
				donneesPourEntite = TrieurEntitesImport.trier(donneesPourEntite);

				java.util.Enumeration e1 = donneesPourEntite.objectEnumerator();
				int indexCourant = 0;
				while (e1.hasMoreElements()) {
					ObjetImport record = (ObjetImport) e1.nextElement();
					LogManager.logDetail("Record : " + record.userPresentableDescription());
					indexCourant++;
					// On affiche tous les 1000 records
					if (indexCourant % 1000 == 1) {
						informerThread("Import - Création du record " + indexCourant + "/" + totalRecordsPourEntite);
					}
					if (indexCourant % saveChangesStep == 0) {
						sauvegardeEditingContext();
					}
					// insérer le record dans l'editing context
					editingContext().insertObject(record);
					// Préparer les relations, on ne pouvait le faire avant car le record n'était pas inséré dans un editing context
					record.preparerRelations();
				}
				sauvegardeEditingContext();
				e1 = donneesPourEntite.objectEnumerator();
				indexCourant = 0;
				while (e1.hasMoreElements()) {
					ObjetImport record = (ObjetImport) e1.nextElement();
					LogManager.logDetail("Record : " + record.userPresentableDescription());
					indexCourant++;
					// On affiche tous les 1000 records
					if (indexCourant % 1000 == 1) {
						informerThread("Import - Vérification du record " + indexCourant + "/" + totalRecordsPourEntite);
					}
					if (indexCourant % saveChangesStep == 0) {
						sauvegardeEditingContext();
					}
if (record instanceof EOPersonnel) 
{
	EOPersonnel personnel=(EOPersonnel)record;
	System.out.println("!! Personnel "+personnel.noMatricule());
} else if (record instanceof EOIndividu) 
{
	EOIndividu individu=(EOIndividu)record;
	System.out.println("!! Individu "+individu.idSource());
} else if (record instanceof EOStructure) 
{
	EOStructure struct=(EOStructure)record;
	System.out.println("!! Structure "+struct.strSource());
}
					// Vérifier que le record est correct (i.e que toutes les relations attendues sont initialisees) sinon ajouter le message de log
					// A ce stade, les erreurs générées ne sont pas réparables
					EOLogImport log = record.verifierRecord();
					//if (log==null)
						//log = record.verifierRecordApresOperation();	// Vérifications une fois que l'objet est créé

					try {
						if (log == null)
							log = record.determinerStatutEtOperation(prioriteEntite);

						LogManager.logDetail("Statut : " + record.statut());
						LogManager.logInformation("Opération : " + record.operation()+", Statut : " + record.statut());

						if (log != null) {
							LogManager.logDetail("Insertion d'un log");
							editingContext().insertObject(log);
							log.ajouterRelation(record);
							String errMsg = "null";
							if (log.messageErreur() != null)
								errMsg = log.messageErreur().mesTexte();
							ErreurManager.instance().ajouterErreur(ErreurManager.VERIFICATION_IMPORT, nomEntite, record, errMsg);
							continue;
						}

						if (record.operation().equals(ObjetImport.OPERATION_UPDATE)) {
							// Dans le cas des updates, on ne prend pas en compte les records dont les données sont
							// absolument identiques à celle du destinataire si ce dernier est valide ou qu'il n'est pas à revalider
							// On ne prend pas non plus en compte les records sur lesquels pèse une priorité globale
							// sauf dans le cas des individus où il faut vérifier d'abord les données du personnel :
							// si il y a une priorité globale de la destination sur les personnels ou si l'individu importé
							// n'est pas un personnel ou si les données personnel sont identiques entre la base d'import et la
							// destination alors on peut vérifier si l'individu est gardé ou non
							boolean peutOublierRecord = true;
							boolean shouldRevert = false;
							if (record instanceof EOIndividu && (prioritePersonnel == null || prioritePersonnel.estPrioriteGlobale() == false)) {

								EOIndividu individu = (EOIndividu) record;
								if (individu.estPersonnelEtablissement()) {
									// à ce point le personnel est intialisé
									EOGrhumPersonnel personnelGrhum = (EOGrhumPersonnel) individu.personnel().destinataireEquivalent();
									Entite entiteDestinationPersonnel = entiteDestinationPourEntiteImport("Personnel");
									if (personnelGrhum == null
											|| personnelGrhum.aAttributsIdentiques(individu.personnel(), entiteDestinationPersonnel) == false) {
										peutOublierRecord = false;
									}
								}
							}
							if (peutOublierRecord) {
								String operation = null;
								// LogManager.logInformation(">>> record.operation " + record.operation());
								ObjetPourSIDestinataire destinataire = record.correspondance().destinataire();
								if (destinataire.aAttributsIdentiques(record, entiteDestination)) {
									if (destinataire.estValide()) {
										shouldRevert = true;
										nbRecordsRejetesPourIdentite++;
										operation = ObjetImport.OPERATION_A_OUBLIER;
									} else if (record.statut().equals(ObjetImport.STATUT_PRIORITE_DESTINATION)) {
										if (prioriteEntite != null && prioriteEntite.estARevalider() == false) {
											shouldRevert = true;
											operation = ObjetImport.OPERATION_A_OUBLIER_POUR_PRIORITE;
											nbRecordsRejetesPourPriorite++;
										}
									}
								} else if (record.statut().equals(ObjetImport.STATUT_PRIORITE_DESTINATION)) {
									if (prioriteEntite != null && prioriteEntite.estPrioriteGlobale()) {
										shouldRevert = true;
										operation = ObjetImport.OPERATION_A_OUBLIER_POUR_PRIORITE;
										nbRecordsRejetesPourPriorite++;
									}
								}
								if (operation != null) {
									// pour mémoriser le type d'opération effectuée en vue de l'afficher à l'utilisateur
									record.setOperation(operation);
									LogManager.logDetail("Changement Operation : " + record.operation() + ", statut :" + record.statut());
								}
							}
							if (shouldRevert) {
								LogManager.logDetail("Revert de l'editing context");
								editingContext().deleteObject(record);
							}
						}
						
						// 22/06/2010 - ne se préoccuper des personnels que si l'individu correspond à un personnel
						if (record instanceof EOIndividu && ((EOIndividu) record).estPersonnelEtablissement()) {
							// Insérer à ce moment pour être sûr que l'individu est créé dans la base, sinon pb de contraintes d'intégrité
							boolean shouldInsert = true;
							EOPersonnel personnel = ((EOIndividu) record).personnel();
							// 27/09/2011 - correction lorsqu'il s'agit d'une opération d'insertion, le personnel doit être créé
							if (prioritePersonnel != null && personnel.operation().equals(ObjetImport.OPERATION_INSERTION) == false) {
								LogManager.logDetail("entite Personnel avec priorite " + prioritePersonnel.description());
								if (prioritePersonnel.estPrioriteGlobale()) {
									shouldInsert = false;
								} else {
									personnel.setStatut(ObjetImport.STATUT_PRIORITE_DESTINATION);
								}
							}
							if (shouldInsert) {
								LogManager.logDetail("Insertion du record de type Personnel");
								editingContext().insertObject(personnel);
								// Pour pouvoir afficher le nombre d'individu insérer
								donnees.ajouterObjetPourEntite(((EOIndividu) record).personnel(), "Personnel");
							}
						}
					} catch (Exception exc) {
						exc.printStackTrace();
						editingContext().revert();
						signalerExceptionThread(exc);
						LogManager.logDetail("Fin suite à exception du passage dans enregistrerDonneesDansImport");
						return false;
					}
				}

				// Une fois tous les éléments insérés, on fait un saveChanges
				sauvegardeEditingContext();

			}
			if (nomEntite.equals("Individu") && creationAutomatiqueComptes) {
				informerThread("Creation automatique de comptes pour les individus");
				preparerComptesDansImport(resultatImport.enregistrementsValides());
			}
		}
		// Vérifier les records en correspondance au cas où parmi ceux-ci il y en ait qui correspondent à d'autres records en update
		// c'est-à-dire qu'elles pointent sur un même record du SI Destinataire
		verifierCorrespondances(entitesImportees);
		// Modifier le statut de l'import pour signaler que les données ont été importées
		fichierImport.terminerPhaseImport();
		sauvegardeEditingContext();
		LogManager.logDetail("Fin du passage dans enregistrerDonneesDansImport");
		return true;
	}

	/**
	 * Prepare des comptes pour les individus pour lesquels c'est nécessaire et les insère dans les données à enregistrer dans la base
	 * 
	 * @param donnees
	 */
	private void preparerComptesDansImport(Donnees donnees) {

		LogManager.logDetail("Preparation des comptes");

		// Rechercher tous les individus
		NSArray<EOIndividu> individus = donnees.donneesPourNomEntite("Individu");
		if (individus != null && individus.count() > 0) {

			int nbComptes = 0;
			NSArray donneesCompte = (NSArray) donnees.donneesPourNomEntite("Compte");
			NSArray individusAvecCompte = null;
			if (donneesCompte != null) {
				individusAvecCompte = (NSArray) donneesCompte.valueForKey("idSource"); // On se base sur l'id source car à ce moment-là, les relations ne sont
																						// pas prêtes
			}

			NSDictionary result = EOUtilities.executeStoredProcedureNamed(editingContext, "uidLibre", new NSDictionary());
			Number uid = (Number) result.objectForKey("uid");
			int uidgid = -1;
			if (uid != null) {
				uidgid = uid.intValue();
			}

			for (EOIndividu individu : individus) {

				if ((individusAvecCompte == null || individusAvecCompte.containsObject(individu.idSource()) == false)
						&& individu.operation().equals(ObjetImport.OPERATION_INSERTION)) {
					// Pas de compte pour cet individu
					EOCompte nouveauCompte = EOCompte.creerComptePourIndividu(individu);
					if (nouveauCompte != null) {
						nouveauCompte.setOperation(ObjetImport.OPERATION_INSERTION);
						if (uidgid != -1) {
							nouveauCompte.setCptUid(new Integer(uidgid++));
						}
						donnees.ajouterObjetPourEntite(nouveauCompte, "Compte");

						nbComptes++;
					}
				}
			}
			informerThread("Nombre de comptes crees : " + nbComptes);
		}
	}

	/**
	 * 
	 * @param importerDonneesTronquees
	 * @param existeHomonymes
	 * @return
	 */
	private String enregistrerImportDansDestination(boolean importerDonneesTronquees, boolean existeHomonymes) {
		// Commencer par changer l'état de l'import
		importCourant().demarrerPhaseTransfert();
		sauvegardeEditingContext();
		int nbRecordsImportesDansDestination = 0, nbTotal = 0;
		int nbInsertions = 0, nbUpdates = 0, nbUpdatePrioritesDestin = 0, nbCorrespondances = 0, nbCorrespPrioritesDestin = 0;
		NSMutableArray erreurs = new NSMutableArray();
		// On prend les entités dans l'ordre de priorité de création (créer les individus puis les structures)
		// On a la liste des noms d'entités de la base d'import
		// Les records à importer sont les records valides + les records avec priorité à la destination + les records tronqués (optionnel)

		java.util.Enumeration e = ReglesDestination.sharedInstance().nomEntitesBaseImportTrieesParPriorite().objectEnumerator();
		while (e.hasMoreElements()) {
			String nomEntiteImport = (String) e.nextElement();
	
			NSMutableArray recordsImports = new NSMutableArray(ObjetImport.rechercherObjetsPourImportValides(editingContext(), nomEntiteImport));
			if (EOImportParametres.homonymeMiseAJourAuto())
				recordsImports.addObjectsFromArray(ObjetImport.rechercherObjetsPourImportAvecHomonyme(editingContext(), nomEntiteImport));
			// informerThread("****** " + nomEntiteImport);
			if (importerDonneesTronquees) {
				recordsImports.addObjectsFromArray(ObjetImport.rechercherObjetsPourImportTronques(editingContext(), nomEntiteImport));
			}
			recordsImports.addObjectsFromArray(ObjetImport.rechercherObjetsPourImportAvecPrioriteDestination(editingContext(), nomEntiteImport));
			if (recordsImports != null && recordsImports.count() > 0) {
				// ajoute la donnée même si elle peut ne déclencher aucune erreur. Permet ainsi de tracer les données avec aucune erreur
				ErreurManager.instance().ajouterEntite(nomEntiteImport);

				informerThread("Import ou mise à jour des records de type " + nomEntiteImport);
				nbTotal += recordsImports.count();
				NSArray records = TrieurEntitesImport.trier(recordsImports); // Pour ranger les records dans un ordre cohérent avec le type d'entité
				java.util.Enumeration e1 = records.objectEnumerator();
				int total = records.count(), numRecord = 0;
				while (e1.hasMoreElements()) {
					ObjetImport recordImport = (ObjetImport) e1.nextElement();
					numRecord++;
					if (numRecord % 1000 == 1) {
						informerThread("Transfert - Record " + numRecord + "/" + total);
					}
					String message = ReglesDestination.sharedInstance().effectuerOperation(editingContext(), recordImport, nomEntiteImport);
					if (message == null) { // Pas d'erreur
						try {
							// Mettre à jour les logs liés à cet objet d'import pour indiquer que le traitement est terminé
							EOLogImport.terminerLogsApresTransfertPourImportEtRecord(editingContext(), importCourant(), recordImport);
							// Signaler que l'objet dans la base d'import a été importé
							recordImport.setTemImport(ObjetImport.TRANSFERE);
							sauvegardeEditingContext();
							nbRecordsImportesDansDestination++;
							if (recordImport.operation().equals(ObjetImport.OPERATION_INSERTION)) {
								nbInsertions++;
							} else if (recordImport.operation().equals(ObjetImport.OPERATION_UPDATE)) {
								nbUpdates++;
								if (recordImport.statut().equals(ObjetImport.STATUT_PRIORITE_DESTINATION)) {
									nbUpdatePrioritesDestin++;
								}
							} else if (recordImport.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE)) {
								nbCorrespondances++;
								if (recordImport.statut().equals(ObjetImport.STATUT_PRIORITE_DESTINATION)) {
									nbCorrespPrioritesDestin++;
								}
							}

						} catch (Exception exc) {
							erreurs.addObject(SuperFinder.recordAsDict(recordImport).toString() + "\n" + exc.getMessage()); // Pour garder les erreurs
							ErreurManager.instance().ajouterErreur(ErreurManager.ETAPE_TRANSFERT, nomEntiteImport, recordImport, exc.getMessage());
							signalerExceptionThread(exc);
							// break;
						}
					} else {
						erreurs.addObject(SuperFinder.recordAsDict(recordImport).toString() + "\n" + message); // Pour garder les erreurs
						ErreurManager.instance().ajouterErreur(ErreurManager.ETAPE_TRANSFERT, nomEntiteImport, recordImport, message);
						editingContext().revert(); // Pour remettre l'editing context en etat
						// break;
					}
				}
			}
		}
		String message = "" + nbRecordsImportesDansDestination + "/" + nbTotal + " records importes ou modifies dans le SI Destinataire\n";
		message += "\tnb Insertions : " + nbInsertions;
		message += "\n\tnb Updates : " + nbUpdates;
		message += "\n\tnb Erreurs : " + erreurs.size();
		if (nbUpdatePrioritesDestin > 0) {
			message += " dont " + nbUpdatePrioritesDestin + " avec application des règles de priorité sur la destination";
		}
		message += "\n\tnb Correspondances : " + nbCorrespondances;
		if (nbCorrespPrioritesDestin > 0) {
			message += " dont " + nbCorrespPrioritesDestin + " avec application des règles de priorité sur la destination";
		}
		informerThread(message);
		ajouterRapport(ETAPE_TRANSFERT, "\nTransfert dans le SI Destinataire\n" + message);
		// 30/09/2011 - Gestion du fichier de log suite au transfert de départs dans le SI Destinataire
		if (LogDepart.sharedInstance().existeInformationsPourDepart()) {
			informerThread("Un log va être généré suite au transfert de départs dans le SI Destinataire.");
			String texte = LogDepart.sharedInstance().genererRapport();
			ajouterRapport(ETAPE_LOG_DEPART, texte);
			// On peut maintenant resetter les logs sur les départs car le rapport a été généré
			// et éviter ainsi si il y a d'autres enregistrements dans le SI Destinataire qu'un rapport sur les départs soit généré
			// alors qu'il n'y a pas de départ
			LogDepart.sharedInstance().reset();
		}
		if (erreurs.count() > 0) {
			informerThread("Erreur pendant le transfert. VEUILLEZ CONSULTER LE FICHIER D'ERREURS");
			// il y a eu des erreurs, editer le fichier d'erreurs et signaler le problème
			genererFichierErreur(importCourant().nomFichier(), NOM_FICHIER_ERREUR_DB, null, erreurs, false);
			return "Erreur pendant le transfert des donnees dans le SI destinataire, veuillez vérifier le fichier d'erreurs";
		} else {
			if (existeHomonymes) {
				// Ne pas invalider l'import courant
				return FIN_TRANSFERT_AVEC_HOMONYMES;
			}
			// Si tout c'est bien passé, indiquer que l'import courant est terminé
			importCourant().terminerPhaseTransfert();
			// Annuler l'erreur courante si elle existe
			EOErreurImport erreurImport = recordErreurCourant();
			if (erreurImport != null) {
				erreurImport.setNomErreurTransfert(null);
				erreurImport.setTemValide(CocktailConstantes.FAUX);
			}
			try {
				LogManager.logDetail("Sauvegarde de l'editing context pour l'import courant");
				sauvegardeEditingContext();
				return FIN_TRANSFERT;
			} catch (Exception exc) {
				signalerExceptionThread(exc);
				return SIGNALER_EXCEPTION + "apres transfert des donnees - " + exc.getMessage();
			}
		}
	}

	private void invaliderRecords(NSArray records) {
		if (records != null && records.count() > 0) {
			java.util.Enumeration e;
			if (records.objectAtIndex(0) instanceof EOStructure) {
				// Commencer par trier les records pour être sûr qu'il n'y ait pas de problème de contraintes d'intégrité
				// Cas des structures avec structures père, il faut commencer par les fils
				// le tri retourne le classement avec les pères en tête, on prend l'opérateur reverse
				e = TrieurEntitesImport.trier(records).reverseObjectEnumerator();
			} else {
				e = records.objectEnumerator();
			}
			while (e.hasMoreElements()) {
				EOGenericRecord record = (EOGenericRecord) e.nextElement();
				if (record instanceof ObjetImport) {
					// Vérifier si il existe une correspondance pour ce record. Si c'est le cas, cela veut dire qu'elle a
					// été créée suite à une résolution d'homonymie sur le client. En effet, les relations de correspondance
					// ne sont mises à jour sinon qu'une fois que les données sont importées dans le SI Destinataire
					ObjetCorresp correspondance = ObjetCorresp.rechercherObjetCorrespPourRecordImport(editingContext(), (ObjetImport) record, true);
					if (correspondance != null) {
						correspondance.supprimerRelations();
						editingContext().deleteObject(correspondance);
						sauvegardeEditingContext(); // pour être sûr que les correspondances sont supprimées en premier
					}
					((ObjetImport) record).invalider();
				} else if (record instanceof EOLogImport) {
					((EOLogImport) record).invalider();
				}
			}
		}
		if (editingContext().hasChanges()) {
			LogManager.logDetail("Sauvegarde de l'editing context");
			sauvegardeEditingContext();
		}
	}

	private String messageOperationImport(Donnees donnees) {
		String message = "";
		java.util.Enumeration e = nomEntitesImportTrieesParPriorite().objectEnumerator();
		while (e.hasMoreElements()) {
			String nomEntite = (String) e.nextElement();
			NSArray donneesPourEntite = donnees.donneesPourNomEntite(nomEntite);
			if (donneesPourEntite != null && donneesPourEntite.count() > 0) {
				message += "\n" + nomEntite + " - " + messagePourDonnees(donneesPourEntite);
			}
			if (nomEntite.equals("Individu")) {
				// Ajouter les informations sur les personnels
				donneesPourEntite = donnees.donneesPourNomEntite("Personnel");
				if (donneesPourEntite != null && donneesPourEntite.count() > 0) {
					message += "\n" + "Personnel - " + messagePourDonnees(donneesPourEntite);
				}
			}
		}
		return message;
	}

	// 24/06/2010 - pour compléter les messages avec les données non prises en compte pour les non personnels
	private String messagePourDonnees(NSArray donneesPourEntite) {
		String message = "";
		donneesPourEntite = TrieurEntitesImport.trier(donneesPourEntite);
		java.util.Enumeration e1 = donneesPourEntite.objectEnumerator();
		int nbInsertions = 0, nbCorrespondances = 0, nbUpdates = 0, nbOublies = 0, nbOubliesPourPriorite = 0, nbErreurs = 0, nbOubliesPourPersonnel = 0;
		while (e1.hasMoreElements()) {
			ObjetImport record = (ObjetImport) e1.nextElement();
			if (record.operation().equals(ObjetImport.OPERATION_INSERTION)) {
				nbInsertions++;
			} else if (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE)) {
				nbCorrespondances++;
			} else if (record.operation().equals(ObjetImport.OPERATION_UPDATE)) {
				nbUpdates++;
			} else if (record.operation().equals(ObjetImport.OPERATION_A_OUBLIER)) {
				nbOublies++;
			} else if (record.operation().equals(ObjetImport.OPERATION_A_OUBLIER_POUR_PRIORITE)) {
				nbOubliesPourPriorite++;
			} else if (record.operation().equals(ObjetImport.OPERATION_A_OUBLIER_POUR_PERSONNEL)) {
				nbOubliesPourPersonnel++;
			} else if (record.operation().equals(ObjetImport.PAS_OPERATION) || record.statut().equals(ObjetImport.STATUT_ERREUR)) {
				nbErreurs++;
			}
		}
		message = "nb Insertions : " + nbInsertions + ", nb Correspondances : " + nbCorrespondances + ", nb Updates : " + nbUpdates + ", nb Erreurs : "
				+ nbErreurs;
		if (nbOublies > 0) {
			message += ", deja dans SI Destinataire : " + nbOublies;
		}
		if (nbOubliesPourPriorite > 0) {
			message += ", priorite du SI Destinataire : " + nbOubliesPourPriorite;
		}
		if (nbOubliesPourPersonnel > 0) {
			message += ", non pris en compte car l'individu n'est pas un personnel ou qu'il existe une priorité globale de GRhum sur les personnels : "
					+ nbOubliesPourPersonnel;
		}

		return message;
	}

	private void annulerErreur(boolean forceAnnulation) {
		EOErreurImport erreurCourante = recordErreurCourant();
		if (erreurCourante != null) {
			if (erreurCourante.fichierImport() != importCourant() || forceAnnulation) {
				erreurCourante.setTemValide(CocktailConstantes.FAUX);
			} else {
				erreurCourante.setNomErreurImport(null);
			}
			sauvegardeEditingContext();
		}
	}

	/**
	 * 
	 * @return
	 */
	private String nomFichierLogDepart() {

		String date = "";
		EOFichierImport importCourant = (EOFichierImport) Finder.lastImport(editingContext());

		String nomFichierLog = importCourant.nomFichier();
		if (importCourant.nomRapport() != null) {
			// Récupérer la date dans le nom du rapport
			// Format de nomRapport : nomFichierDate_numFichier.txt
			int lg = importCourant.nomRapport().lastIndexOf("_");
			date = importCourant.nomRapport().substring(lg - 8, lg);
		}
		// supprimer l'extension si nécessaire
		int lg = nomFichierLog.indexOf(".");
		if (lg >= 0) {
			nomFichierLog = nomFichierLog.substring(0, lg);
		}
		nomFichierLog = NOM_FICHIER_LOG_DEPART + nomFichierLog + date + ".xls";
		String path = pathDiagnoticServer() + nomFichierLog;
		File file = new File(path);

		if (file.exists()) {
			return nomFichierLog;
		} else {
			return null;
		}

	}

	// Cette méthode est appelée par les différentes étapes de l'import des données
	// dans la phase de parsing ou d'import, on génère un fichier de diagnostic constitué d'un mot-clé suivi du nom du fichier,
	// d'une date et d'un numéro
	// dans la phase de transfert, on reprend le nom de fichier stocké dans le record d'import courant
	private void ajouterRapport(int etape, String texte) {
		// 30/09/2011 - Modification pour gestion des logs sur les départs
		String nomRapport = "";
		boolean evaluerNomFichier = true;
		if (etape == ETAPE_TRANSFERT) {
			// On récupère de toute façon le dernier import car si il y avait eu un autre import et un transfert après
			// les données importées précédemment auraient été aussi transférées avec le dernier transfert
			EOFichierImport lastImport = EOFichierImport.lastImport(editingContext(), nomFichierImport);
			if (lastImport != null && lastImport.nomRapport() != null) {
				nomRapport = lastImport.nomRapport();
				evaluerNomFichier = false;
			}
		}
		String date = DateCtrl.dateToString(new NSTimestamp());
		date = date.replaceAll("/", "");
		if (etape == ETAPE_LOG_DEPART) {
			nomRapport = nomFichierImport;
			// Supprimer l'extension si elle existe
			int lg = nomRapport.indexOf(".");
			if (lg >= 0) {
				nomRapport = nomRapport.substring(0, lg);
			}
			nomRapport = NOM_FICHIER_LOG_DEPART + nomRapport + date + ".xls";
		} else if (evaluerNomFichier) { // Il s'agit d'un rapport d'import, il n'est pas encore connu
			int lg = nomFichierImport.indexOf(".");
			if (lg >= 0) {
				nomRapport = NOM_FICHIER_RAPPORT + "_" + nomFichierImport.substring(0, lg);
			}
			if (etape == ETAPE_PARSING) {
				numDiagnostic++;
			}
			nomRapport = nomRapport + date + "_" + numDiagnostic + ".txt";
		}
		String pathFichier = pathDiagnoticServer() + nomRapport;
		if (pathFichier == null) {
			informerThread("Chemin d'accès pour le fichier de diagnostic invalide");
			return;
		}
		genererFichier(pathFichier, texte, etape != ETAPE_PARSING);
		// Dans le cas d'un import et lorsqu'on a évalué le nom du fichier sauvegarder le résultat dans l'editing context
		if (etape == ETAPE_IMPORT) {
			EOFichierImport importCourant = importCourant();
			if (importCourant != null) {
				importCourant.setNomRapport(nomRapport);
				try {
					sauvegardeEditingContext();
				} catch (Exception e) {
					LogManager.logException(e);
					informerThread("Exception pendant la sauvegarde du nom du rapport : " + e.getMessage());
				}
			} else {
				LogManager.logInformation("Pas d'import courant defini pendant la phase d'import");
			}
		}
	}

	private String lireFichier(String pathFichier) {
		File fichierErreur = new File(pathFichier);
		if (fichierErreur.exists()) {
			try {
				byte[] b = new byte[(int) fichierErreur.length()];
				FileInputStream stream = new FileInputStream(fichierErreur);
				stream.read(b);
				stream.close();
				return new String(b);
			} catch (Exception e) {
				e.printStackTrace();
				return "Erreur en lisant le fichier " + pathFichier + " : " + e.getMessage();
			}
		} else {
			LogManager.logInformation(pathFichier + " : fichier inconnu");
			return "";
		}

	}

	// On a soit des objets invalides si il s'agit de l'import depuis un fichier, soit des messages si import vers
	// la base destination
	private boolean genererFichierErreur(String nomFichierImportCourant, String nomFichier, Donnees objetsInvalides, NSArray messagesErreur,
			boolean estImportFichier) {

		int lg = nomFichierImportCourant.indexOf(".");
		if (lg >= 0) {
			nomFichier = nomFichierImportCourant.substring(0, lg) + "_" + nomFichier;
		}
		numFichierErreur++;
		String date = DateCtrl.dateToString(new NSTimestamp());
		date = date.replaceAll("/", "");
		nomFichier = nomFichier + date + "_" + numFichierErreur;
		if (estImportFichier) {
			nomFichier += CocktailConstantes.EXTENSION_EXCEL;
		} else {
			nomFichier += ".txt";
		}
		String pathFichier = pathErreursServer() + nomFichier;
		if (pathFichier == null) {
			informerThread("Chemin d'accès pour le fichier d'erreur invalide");
			return false;
		}
		String texte = "";
		if (estImportFichier) {
			texte = preparerTexte(objetsInvalides);
		} else {
			texte = preparerTexte(messagesErreur);
		}
		genererFichier(pathFichier, texte);
		EOErreurImport erreurImport = recordErreurCourant();
		EOFichierImport importCourant = importCourant();
		if (erreurImport == null) {
			// Pas d'erreur générée auparavant, créer le record
			erreurImport = new EOErreurImport();
			erreurImport.initAvecImport(importCourant, nomFichier, nomFichier.indexOf(NOM_FICHIER_ERREUR_IMPORT) >= 0);
			editingContext().insertObject(erreurImport);
		} else {
			if (nomFichier.indexOf(NOM_FICHIER_ERREUR_IMPORT) >= 0) {
				erreurImport.setNomErreurImport(nomFichier);
			} else {
				erreurImport.setNomErreurTransfert(nomFichier);
			}
			if (erreurImport.fichierImport() == null && importCourant != null) {
				erreurImport.addObjectToBothSidesOfRelationshipWithKey(importCourant, "fichierImport");
			}
		}

		sauvegardeEditingContext();
		return true;
	}

	/**
	 * 
	 * @param objetsInvalides
	 * @return
	 */
	private String preparerTexte(Donnees objetsInvalides) {

		String texte = "Import :" + nomFichierImport + System.getProperty("line.separator");

		for (String nomEntite : nomEntitesImportTrieesParPriorite()) {

			NSArray objets = objetsInvalides.donneesPourNomEntite(nomEntite);
			if (objets != null && objets.count() > 0) {

				// Afficher en en-tête le nom 11de l'entité
				// Puis afficher une ligne comportant dans les colonnes, les noms des attributs et noms des messages d'erreur
				// ensuite afficher données et erreurs

				Entite entite = reglesImport.entiteAvecNomDestination(nomEntite);

				texte += System.getProperty("line.separator") + entite.nomSource() + System.getProperty("line.separator");
				NSArray colonnes = colonnesPourEntiteEtObjetsInvalides(entite, objetsInvalides);
				texte += preparerHeader(entite, colonnes) + System.getProperty("line.separator");
				java.util.Enumeration e1 = objets.objectEnumerator();
				while (e1.hasMoreElements()) {
					ObjetAvecErreur objet = (ObjetAvecErreur) e1.nextElement();
					texte += preparerDonneesErronees(objet, colonnes) + System.getProperty("line.separator");
				}
			}
		}
		return texte;
	}

	/**
	 * 
	 * @param erreurs
	 * @return
	 */
	private String preparerTexte(NSArray erreurs) {
		String texte = "Import :" + nomFichierImport;
		java.util.Enumeration e = erreurs.objectEnumerator();
		while (e.hasMoreElements()) {
			texte = ajouterTexte(texte, (String) e.nextElement());
		}
		return texte;
	}

	private void genererFichier(String pathFichier, String texte, boolean append) {
		try {
			// créer le fichier
			if (append) {
				texte += "\n";
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(pathFichier, append));
			writer.write(texte, 0, texte.length());
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void genererFichier(String pathFichier, String texte) {
		genererFichier(pathFichier, texte, false);
	}

	private NSArray colonnesPourEntiteEtObjetsInvalides(Entite entite, Donnees objetsInvalides) {
		NSMutableArray colonnes = new NSMutableArray();
		java.util.Enumeration e = entite.attributs().objectEnumerator();
		while (e.hasMoreElements()) {
			Attribut attribut = (Attribut) e.nextElement();
			colonnes.addObject(attribut.nomDestination());
		}
		// Ajouter une colonne avec comme libellé "Erreur"
		colonnes.addObject(new String("Erreur_Import"));
		// Construire le tableau de tous les types de messages possibles
		NSMutableArray clesPossibles = new NSMutableArray();
		e = objetsInvalides.donneesPourNomEntite(entite.nomDestination()).objectEnumerator();
		while (e.hasMoreElements()) {
			ObjetAvecErreur objet = (ObjetAvecErreur) e.nextElement();
			java.util.Enumeration e1 = objet.clesPourErreur().objectEnumerator();
			while (e1.hasMoreElements()) {
				String cle = (String) e1.nextElement();
				if (clesPossibles.containsObject(cle) == false) {
					clesPossibles.addObject(cle);
				}
			}
		}
		colonnes.addObjectsFromArray(clesPossibles);
		return colonnes;
	}

	private String preparerHeader(Entite entite, NSArray colonnes) {
		String resultat = "";
		java.util.Enumeration e = colonnes.objectEnumerator();
		boolean estErreur = false, firstElement = true;
		while (e.hasMoreElements()) {
			String colonne = (String) e.nextElement();
			if (colonne.equals("Erreur_Import")) {
				estErreur = true;
			} else {
				if (!firstElement) {
					resultat += "\t";
				} else {
					firstElement = false;
				}
				if (estErreur) {
					// Il s'agit d'un message d'erreur
					if (colonne.indexOf("General_") >= 0) {
						resultat += ConstantesErreur.messageGeneralPourCle(colonne);
					} else {
						resultat += ConstantesErreur.messagePourEntiteEtCle(entite.nomDestination(), colonne);
					}
				} else {
					Attribut attribut = entite.attributAvecNomDestination(colonne);
					resultat += attribut.nomSource();
				}
			}
		}
		return resultat;
	}

	private String preparerDonneesErronees(ObjetAvecErreur objet, NSArray colonnes) {
		String resultat = "";
		java.util.Enumeration e = colonnes.objectEnumerator();
		boolean estErreur = false, firstElement = true;
		while (e.hasMoreElements()) {
			String colonne = (String) e.nextElement();
			if (colonne.equals("Erreur_Import")) {
				estErreur = true;
			} else {
				if (!firstElement) {
					resultat += "\t";
				} else {
					firstElement = false;
				}
				if (estErreur) {
					// Il s'agit d'un message d'erreur
					String message = objet.messageErreurPourCle(colonne);
					if (message != null) {
						resultat += message;
					}
				} else {
					// Il s'agit d'un valeur de l'objet
					Object valeur = objet.valeurPourCle(colonne);
					if (valeur != null) {
						resultat += valeur.toString();
					}
				}
			}
		}
		return resultat;
	}

	private void verifierCorrespondances(NSArray entitesImportees) {
		LogManager.logDetail("Début du Passage dans verifierCorrespondances");
		boolean shouldSave = false;
		java.util.Enumeration e = entitesImportees.objectEnumerator();
		LogManager.logDetail("Nombre d'entités importées : " + entitesImportees.count());
		while (e.hasMoreElements()) {
			String nomEntite = (String) e.nextElement();
			// Rechercher tous les records en correspondance pour cette entité
			NSArray objetsPourCorrespondance = ObjetImport.rechercherObjetsEnCorrespondancePourImportCourant(editingContext(), nomEntite);
			LogManager.logDetail(nomEntite.toString() + " a " + objetsPourCorrespondance.count() + " objets pour correspondance");
			if (objetsPourCorrespondance.count() > 0) {
				// Rechercher tous les records en update pour cette entité
				NSArray objetsPourUpdate = ObjetImport.rechercherObjetsEnUpdatePourImportCourant(editingContext(), nomEntite);
				LogManager.logDetail(nomEntite.toString() + " a " + objetsPourUpdate.count() + " objets pour update");
				if (objetsPourUpdate.count() > 0) {
					// Si il y a des correspondances et des updates alors vérifier que 2 objets ne pointent pas sur la même correspondance
					java.util.Enumeration e1 = objetsPourCorrespondance.objectEnumerator();
					while (e1.hasMoreElements()) {
						ObjetImport recordPourCorresp = (ObjetImport) e1.nextElement();
						LogManager.logDetail(nomEntite.toString() + " : " + recordPourCorresp.entityName() + " avec statut " + recordPourCorresp.statut());
						java.util.Enumeration e2 = objetsPourUpdate.objectEnumerator();
						while (e2.hasMoreElements()) {
							ObjetImport recordPourUpdate = (ObjetImport) e2.nextElement();
							LogManager.logDetail(nomEntite.toString() + " : " + recordPourUpdate.entityName() + " avec statut " + recordPourUpdate.statut());
							if (recordPourCorresp.destinataireEquivalent() == recordPourUpdate.correspondance().destinataire()) {
								LogManager.logDetail("Verification des correspondances, entite : " + nomEntite
										+ ", 2 records pointent sur un record du SI Destinataire");
								LogManager.logDetail("record en correspondance : " + recordPourCorresp);
								LogManager.logDetail("record en update : " + recordPourUpdate);
								// On a trouvé 2 records qui pointent sur le même destinataire
								// on transforme le record de correspondance en record en insertion
								recordPourCorresp.setOperation(ObjetImport.OPERATION_INSERTION);
								if (recordPourCorresp instanceof EOIndividu && ((EOIndividu) recordPourCorresp).estPersonnelEtablissement()) {
									EOPersonnel personnel = ((EOIndividu) recordPourCorresp).personnel();
									if (personnel == null) {
										((EOIndividu) recordPourCorresp).preparerPersonnel(); // on a forcément à ce point un personnel créé
									}
									if (personnel != null && personnel.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE)) {
										personnel.setOperation(ObjetImport.OPERATION_INSERTION);
									}
								}
								shouldSave = true;
							}
						}
					}
				}
			}
		}
		if (shouldSave) {
			sauvegardeEditingContext();
		}
	}

	private void sauvegardeEditingContext() {
		try {
			editingContext().saveChanges();
		} catch (Exception e) {
			LogManager.logInformation("Exception lors de la sauvegarde dans la base de données.");
			LogManager.logException(e);
			editingContext().revert();
		}
	}
}
