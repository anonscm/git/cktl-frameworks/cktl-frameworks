//ReglesImport.java
//Created by Christine Buttin on Mon May 17 2004.
//Copyright (c) 2004 __MyCompanyName__. All rights reserved.
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.serveur.modele.InterfaceRecordGenerique;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.InterfaceValiditePourDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuEtPeriode;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOMessageErreur;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** Contient toutes les regles de validation des entites qui sont appliquees lors de l'import des donnees :<BR>
 * <UL>Verification de la presence obligatoire des attributs</UL>
 * <UL>Valeur des attributs de type nombre ou date : dans ce cas les attributs sont convertis dans le type attendu</UL>
 * <UL>Valeur des attributs faisant reference a des nomenclatures. Reprend toutes les contraintes d'integrite</UL> */

public class ReglesImport extends ReglesPourModele {
	
	public void initAvecModele(String nomModele) {
		super.initAvecModele(nomModele);
	}
	/** Retourne les noms d'entites de la base d'import triees par priorite */
	public NSArray<String> nomEntitesBaseImportTrieesParPriorite() {
		return modeleDonnees().nomEntitesTrieesParPriorite("nomDestination"); // Dans le modèle XML d'import, nomDestination désigne le nom d'entité d'import
	}
	/** Compare deux records lies au modele de donnees et retourne true si ils sont identiques
	@param objet1 
	@param objet2
	 */
	public boolean sontIdentiques(ObjetImport objet1,ObjetImport objet2) {
		String nomEntite1 = objet1.entityName(),nomEntite2 = objet2.entityName();
		if (nomEntite1 == null || nomEntite2 == null || nomEntite1.equals(nomEntite2) == false) {
			return false;
		}
		Entite entite = modeleDonnees().entiteAvecNomDestination(nomEntite1);
		NSArray nomAttributs = entite.nomAttributsPourType("nomDestination");
		java.util.Enumeration e = nomAttributs.objectEnumerator();
		while (e.hasMoreElements()) {
			String nomAttribut = (String)e.nextElement();
			Object attributObjet1 = objet1.valueForKey(nomAttribut);
			Object attributObjet2 = objet2.valueForKey(nomAttribut);
			if ((attributObjet1 == null && attributObjet2 != null) ||
					(attributObjet1 != null && attributObjet2 == null) ||	
					(attributObjet1 != null && attributObjet2 != null && ReglesPourModele.sontAttributsIdentiques(attributObjet1,attributObjet2) == false)) {  

				return false;
			}
		}
		return true;
	}
	/** Retourne true si 2 objets de m&circ;me type ont les attributs de comparaison identiques */
	public boolean ontAttributsComparaisonIdentiques(ObjetImport objet1,ObjetImport objet2) {
		if (objet1.entityName().equals(objet2.entityName()) == false) {
			return false;
		}
		Entite entite = modeleDonnees().entiteAvecNomDestination(objet1.entityName());
		java.util.Enumeration e = entite.nomsAttributsComparaison().objectEnumerator();
		while (e.hasMoreElements()) {
			String nomAttribut = (String)e.nextElement();
			Object attributObjet1 = objet1.valueForKey(nomAttribut);
			Object attributObjet2 = objet2.valueForKey(nomAttribut);
			if ((attributObjet1 == null && attributObjet2 != null) ||
					(attributObjet1 != null && attributObjet2 == null) ||	
					(attributObjet1 != null && attributObjet2 != null && ReglesPourModele.sontAttributsIdentiques(attributObjet1,attributObjet2) == false)) {  
				return false; 
			}
		}
		return true;
	}
	
	/** Verifie si les donnees sont conformes au modele et retourne le diagnostic sous forme de 
	 * String (null si OK). Au passage, supprime le donnees avec l'attribut "NULL",
	 * convertit les donnees qui doivent l'etre en nombres/dates si necessaire (verifierType = true) <BR>
	 * Verifie aussi que les nomenclatures sont respectees et que tous les champs necessaires sont fournis */
	public String preparerEtValiderEntiteImport(String nomEntite,NSMutableDictionary valeursAttributs) {
		LogManager.logDetail("Preparation et validation de l'entite");
		Entite entite = modeleDonnees().entiteAvecNomDestination(nomEntite);
		String resultat = "";
		// Vérifier qu'il ne manque pas d'attributs obligatoires et que les attributs peuvent être convertis quand c'est nécessaire
		// en nombre ou date
		// Les vérifications sur les longueurs de chaînes de caractères seront faites plus tard
		java.util.Enumeration e = entite.attributs().objectEnumerator();
		while (e.hasMoreElements()) {
			Attribut attribut = (Attribut)e.nextElement();
			String valeur = (String)valeursAttributs.objectForKey(attribut.nomDestination());

			valeur = trimAttribut(valeursAttributs, attribut, valeur);
			valeur = fullfillAffectationIfEmpty(valeursAttributs, attribut,
					valeur);
			resultat = checkMandatoryAttribut(resultat, attribut, valeur);

			resultat = checkTypeAttribut(valeursAttributs, resultat,
					attribut, valeur);

		}
		if (resultat.length() == 0) {	// Pas d'erreur, on peut valider l'entité sinon il pourrait y avoir des erreurs de type
			resultat = resultat + validerEntite(nomEntite,valeursAttributs);
		}
		return resultat;
	}
	
	private String fullfillAffectationIfEmpty(
			NSMutableDictionary valeursAttributs, Attribut attribut,
			String valeur) {
		// Avant de vérifier les attributs, vérifier pour les affectations si la quotité est fournie
		// sinon la générer à 100%
		if (attribut.nomDestination().equals("numQuotAffectation") && valeur == null) {
			valeur = "100";
			valeursAttributs.setObjectForKey(valeur, "numQuotAffectation");
		}
		return valeur;
	}
	
	private String trimAttribut(NSMutableDictionary valeursAttributs,
			Attribut attribut, String valeur) {
		if (valeur != null) {
			//  Supprimer les espaces inutiles en début et fin de chaîne pour vérifier si la chaîne est vide
			String nouvelleValeur = valeur.trim();
			if (nouvelleValeur.toUpperCase().equals("NULL") || nouvelleValeur.length() == 0) {
				valeur = null;
				// 16/05/08 - Supprimer les chaînes vides des attributs
				valeursAttributs.removeObjectForKey(attribut.nomDestination());
				// remplacer les chaînes vides par une valeur nulle
				//	valeursAttributs.setObjectForKey(NSKeyValueCoding.NullValue, attribut.nomDestination());
			} else {
				if (nouvelleValeur.indexOf(System.getProperty("line.separator")) >= 0) {
					nouvelleValeur = nouvelleValeur.replaceAll(System.getProperty("line.separator")," ");
				}
				if (attribut.nomDestination().equals("noTelephone") == false) {
					valeur = nouvelleValeur;
				}
			}
		}
		return valeur;
	}
	
	private String checkMandatoryAttribut(String resultat, Attribut attribut,
			String valeur) {
		if (attribut.estObligatoire() && valeur == null) {
			resultat += "General_Attribut_Obligatoire >>" + attribut.nomSource() + System.getProperty("line.separator");
		}
		return resultat;
	}

	private String checkTypeAttribut(NSMutableDictionary valeursAttributs,
			String resultat, Attribut attribut, String valeur) {
		if (valeur == null) {
			return resultat;
		}
		if (attribut.estTypeString()) {
			valeursAttributs.setObjectForKey(valeur, attribut.nomDestination());
			// Pour les strings de longueur fixe, vérifier qu'elles ont la longueur requise
			if (attribut.aLongueurFixe()) {
				if (valeur.length() != attribut.lgMax()) {
					resultat += "General_Longueur_Invalide >>" + attribut.nomSource() + System.getProperty("line.separator");
				}
			}
		}
		if (attribut.estTypeEntier()) {
			try {
				int lg = valeur.indexOf(".");
				if (lg > 0) {
					valeur = valeur.substring(0,lg);	// 26/05/09 - pour supprimer les .00 dans les nombres fournis
				}
				Integer nouvelleValeur = new Integer(valeur);
				valeursAttributs.setObjectForKey(nouvelleValeur, attribut.nomDestination());
			} catch (Exception e1) {
				resultat += "General_Nombre_Entier >>" + attribut.nomSource() + System.getProperty("line.separator");
			}
		} else if (attribut.estTypeDecimal()) {
			try {
				Double nouvelleValeur = new Double(valeur);
				valeursAttributs.setObjectForKey(nouvelleValeur, attribut.nomDestination());
			} catch (Exception e1) {
				resultat += "General_Nombre_Decimal >>" + attribut.nomSource() + System.getProperty("line.separator");
			}
		} else if (attribut.estTypeDate()) {
			String formatDate=EOImportParametres.formatDate();
			try {
				NSTimestamp date = DateCtrl.stringToDate(valeur, formatDate);
				valeursAttributs.setObjectForKey(date, attribut.nomDestination());
			} catch (Exception e1) {
				//e1.printStackTrace();
				resultat += "General_Date_Invalide (valeur="+valeur+",format="+formatDate+")>>" + attribut.nomSource() + System.getProperty("line.separator");
			}
		}
		return resultat;
	}
	
	/** Verifie si les donnees sont conformes au modele et retourne le diagnostic sous forme de 
	 * String (null si OK). Verifie aussi que les nomenclatures sont respectees et que tous les champs necessaires 
	 * sont fournis */
	public String validerEntiteImport(String nomEntite,NSMutableDictionary valeursAttributs) {
		
		LogManager.logDetail("Début du passage dans ReglesImport.validerEntiteImport");
		LogManager.logDetail("Validation de l'entite transmise depuis le client");
		Entite entite = modeleDonnees().entiteAvecNomDestination(nomEntite);
		String resultat = "";
		java.util.Enumeration e = entite.attributs().objectEnumerator();
		while (e.hasMoreElements()) {
			Attribut attribut = (Attribut)e.nextElement();
			Object valeur = valeursAttributs.objectForKey(attribut.nomDestination());
			// Supprimer les valeurs nulles ou les chaînes vides
			if (valeur != null && valeur instanceof String) {
				//  Supprimer les espaces inutiles en début et fin de chaîne pour vérifier si la chaîne est vide
				String nouvelleValeur = ((String)valeur).trim();
				if (nouvelleValeur.toUpperCase().equals("NULL") || nouvelleValeur.length() == 0) {
					valeur = null;
					// remplacer les chaînes vides par une valeur nulle
					valeursAttributs.setObjectForKey(NSKeyValueCoding.NullValue, attribut.nomDestination());
				} else {
					valeur = nouvelleValeur;
				}
			}
			// Avant de vérifier les attributs, vérifier pour les affectations si la quotité est fournie
			// sinon la générer à 100%
			if (attribut.nomDestination().equals("numQuotAffectation") && valeur == null) {
				valeur = "100";
				valeursAttributs.setObjectForKey(new Double(100.00), "numQuotAffectation");
			}
			if (attribut.estObligatoire() && valeur == null) {
				resultat += "General_Attribut_Obligatoire >>" + attribut.nomSource() + System.getProperty("line.separator");
			} else if (valeur != null) {
				if (attribut.estTypeString()) {
					valeursAttributs.setObjectForKey(valeur, attribut.nomDestination());
					// Pour les strings de longueur fixe, vérifier qu'elles ont la longueur requise
					if (attribut.aLongueurFixe()) {
						if (((String)valeur).length() != attribut.lgMax()) {
							resultat += "General_Longueur_Invalide >>" + attribut.nomSource() + System.getProperty("line.separator");
						}
					}
				}
			}
		}
		// Valider la longueur des champs
		if (resultat.length() == 0) {
			String nouveauResultat = validerLongueursAttributs(nomEntite, valeursAttributs,false);
			if (nouveauResultat != null) {
				resultat = resultat + nouveauResultat;
			}
		}
		if (resultat.length() == 0) {	// Pas d'erreur, on peut valider l'entité sinon il pourrait y avoir des erreurs de type
			resultat = resultat + validerEntite(nomEntite,valeursAttributs);
		}
		if (resultat.length() == 0) { 
			resultat = null;
		}
		LogManager.logDetail("Début du passage dans ReglesImport.validerEntiteImport");
		return resultat;
	}
	/** Verifie les r&egrave;gles metier pour l'objet correspondant aux valeurs passees dans le dictionnaire */
	public String verifierReglesMetier(String nomEntite,NSMutableDictionary valeursAttributs) {
		// 18/07/2011 - dans le cas des objets génériques, vérifier les données, responsabilité est faite à chaque classe d'entité
		// de vérifier correctement les conditions puisqu'on travaille sur un objet nouvellement instancier
		try {
			String nomClasse = EOUtilities.entityNamed(editingContext(), nomEntite).className();
			Class classeEntite = Class.forName(nomClasse);
			Object objet = classeEntite.newInstance();
			if (objet instanceof ObjetImport && objet instanceof InterfaceRecordGenerique) {
				ObjetImport record = (ObjetImport)objet;
				// Initialiser cet objet avec les données du dictionnaire
				record.takeValuesFromDictionary(valeursAttributs);
				String message = ((InterfaceRecordGenerique)record).verifierRecordPourClient(new EOEditingContext());	// On travaille dans un editing context à part pour fetcher les données et ne pas perturber l'editing context courant
				if (message != null) {
					EOMessageErreur erreur = EOMessageErreur.messagePourCle(editingContext(), message);
					return erreur.mesTexte();
				}
			} else 	if (objet instanceof EOCldDetail) {
				EOCldDetail record = (EOCldDetail)objet;
				// Initialiser cet objet avec les données du dictionnaire
				record.takeValuesFromDictionary(valeursAttributs);
				String message = record.verifierRecordPourClient(new EOEditingContext());	// On travaille dans un editing context à part pour fetcher les données et ne pas perturber l'editing context courant
				if (message != null) {
					EOMessageErreur erreur = EOMessageErreur.messagePourCle(editingContext(), message);
					return erreur.mesTexte();
				}
			} else if (objet instanceof ObjetImportPourIndividuEtPeriode) {
				ObjetImportPourIndividuEtPeriode record = (ObjetImportPourIndividuEtPeriode)objet;
				// Initialiser cet objet avec les données du dictionnaire
				record.takeValuesFromDictionary(valeursAttributs);
				String message = record.verifierRecordPourClient(new EOEditingContext());	// On travaille dans un editing context à part pour fetcher les données et ne pas perturber l'editing context courant
				if (message != null) {
					EOMessageErreur erreur = EOMessageErreur.messagePourCle(editingContext(), message);
					return erreur.mesTexte();
				}
			} 
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;
	}
	/**  retourne le diagnostic sous forme de String (null si OK) si des strings ont ete tronquees */
	public String validerLongueursAttributs(String nomEntite,NSMutableDictionary valeursAttributs,boolean estVerificationLocale) {
		LogManager.logDetail("Début du passage dans ReglesImport.ValiderLongueursAttributs");
//		LogManager.logDetail("Validation des longueurs de champs");
		Entite entite = modeleDonnees().entiteAvecNomDestination(nomEntite); 
		if (entite == null) {
			return null;
		}
		String resultat = "";
		java.util.Enumeration listeAttributs  = entite.attributs().objectEnumerator();
		while (listeAttributs.hasMoreElements()) {
			Attribut attribut = (Attribut)listeAttributs.nextElement();
			Object objet = valeursAttributs.objectForKey(attribut.nomDestination());
			if (objet != null && attribut.longueurAPrendreEnCompte()) {
				if (attribut.estTypeString() && objet instanceof String) {
					String texte = (String)objet;
					if (texte.length() > attribut.lgMax()) {
						if (estVerificationLocale) {
							valeursAttributs.setObjectForKey(texte.substring(0,attribut.lgMax()), attribut.nomDestination());
							resultat += "General_Attribut_Tronque >>" + attribut.nomSource() + " valeur originale " + texte + System.getProperty("line.separator");
						} else {
							resultat += "General_Longueur_Invalide >>" + attribut.nomSource() + " maximum " + attribut.lgMax() + " caractères" + System.getProperty("line.separator");
						}

					}
				} else if ((attribut.estTypeEntier() || attribut.estTypeDecimal()) && objet instanceof Number) {
					String texte  = objet.toString();
					if (texte.length() > attribut.lgMax()) {
						texte = texte.substring(0,attribut.lgMax());
						if (objet instanceof Integer) {
							valeursAttributs.setObjectForKey(new Integer(texte), attribut.nomDestination());
						} else if (objet instanceof Double) {
							valeursAttributs.setObjectForKey(new Double(texte), attribut.nomDestination());
						}
						if (estVerificationLocale) {
							resultat += "General_Attribut_Tronque >>" + attribut.nomSource() + System.getProperty("line.separator");
						} else {
							resultat += "General_Longueur_Invalide >>" + attribut.nomSource() + " comporte au maximum " + attribut.lgMax() + " chiffres" + System.getProperty("line.separator");
						}
					}
				}
			}
		}
		if (resultat.length() == 0) { 
			resultat = null; 
		}
		LogManager.logDetail("Fin du passage dans ReglesImport.ValiderLongueursAttributs");
		return resultat;
	}
	/** Verifie si le dernier record dans la base d'import est absolument identique (au niveau des attributs d'import)
	 * et si le record destinataire n'a pas ete invalide */
	public boolean estIdentiqueDernierRecordDeBaseImport(ObjetImport record) {
		LogManager.logDetail("Début du passage dans ReglesImport.estIdentiqueDernierRecordDeBaseImport");
		EOQualifier qualifier = record.construireQualifierImportPourRecordPrecedent();
		if (qualifier != null) {
			NSMutableArray qualifiers = new NSMutableArray(qualifier);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = 'O'", null));
			qualifier = new EOAndQualifier(qualifiers);
			// On trie par date de création décroissante
			EOFetchSpecification fs = new EOFetchSpecification(
					record.entityName(),qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("dCreation", EOSortOrdering.CompareDescending)));
			fs.setFetchLimit(1);
			fs.setRefreshesRefetchedObjects(true);	// pour l'automatisation, l'objet a peut-être été créé par un import précédent
			NSArray results = editingContext().objectsWithFetchSpecification(fs);
			LogManager.logDetail("Nombre d'enregistrement déjà importés : " + results.count());
			
			if (results.count() == 0) {
				if (record.dejaDansSIDestinataire()) {
					LogManager.logDetail("Il y a une équivalence dans le SI destinataire");
				} else {
					LogManager.logDetail("Pas d'équivalence dans le SI destinataire");
				}
				ObjetCorresp corresp1 = record.correspondance();
				if (corresp1 != null) {
					LogManager.logDetail("Il y a une CORRESPONDANCE");
				} else {
					LogManager.logDetail("Il n'y a aucune CORRESPONDANCE");
				}
			}
			
			if (results.count() > 0) {
				ObjetImport ancienRecord = (ObjetImport)results.objectAtIndex(0);
				// Il y a forcément une correspondance puisque l'objet a déjà été importé
				ObjetCorresp correspondance = ObjetCorresp.rechercherObjetCorrespPourRecordImport(editingContext(), ancienRecord, true);
				if (correspondance != null) {
					ObjetPourSIDestinataire destinataire = (ObjetPourSIDestinataire)correspondance.valueForKey(correspondance.nomRelationBaseDestinataire());
					boolean estValide = destinataire.estValide();
					if (destinataire instanceof InterfaceValiditePourDestinataire) {
						estValide = ((InterfaceValiditePourDestinataire)destinataire).destinataireValidePourObjetImport(ancienRecord);
					}
					if (estValide == false) {
						// l'objet a été invalidé dans le SI Destinataire, on va donc prendre en compte
						// l'objet importé de manière à faire l'import de nouveau en update et forcer la revalidation
						// de l'objet
						LogManager.logDetail("l'objet a été invalidé dans le SI Destinataire, on va donc prendre en compte"
								+ " l'objet importé de manière à faire l'import de nouveau en update et forcer la revalidation"
								+ " de l'objet");
						return false;
					} else {
						// pas d'import si les objets sont identiques et que l'ancien record n'avait pas une priorité sur
						// la destination
						boolean sontIdentiques = sontIdentiques(ancienRecord,record);
						// Il se peut si il n'y a plus de priorité sur la destination que certains records bien qu'identiques n'aient pas été traités
						// en quel cas on signale que les records ne sont pas identiques
						if (sontIdentiques && (ancienRecord.statut().equals(ObjetImport.STATUT_PRIORITE_DESTINATION) && record.statut().equals(ObjetImport.STATUT_PRIORITE_DESTINATION) == false)) {
							sontIdentiques = false;
						}
						if (sontIdentiques && record instanceof EOIndividu) {
							// 06/01/2010 - Suite à problème de transfert dans les personnels identifié par Bordeaux
							// Pour pouvoir réimporter les personnels, vérifier si les records personnels sont identiques i.e déjà importés
							sontIdentiques = comparerPersonnels(((EOIndividu)ancienRecord).personnel(),((EOIndividu)record).personnel());
						}
						LogManager.logDetail("Objet à importer valide");
						return sontIdentiques;
					}
				} else {
					LogManager.logDetail("Aucune correspondance toruvée");
					return false;
				}
			} else {
				// pas d'objet => jamais importé
				LogManager.logDetail("Pas de résultats trouvé. pas d'objet => jamais importé");
				return false;
			}

		} else {
			LogManager.logDetail("Sortie car le qualifier est nul");
			return false;
		}
	}
	/** Retourne un dictionnaire des valeurs avec comme cle les noms sources des attributs */
	public NSDictionary dictionnaireAvecAttributNomSource(Entite entite,NSDictionary valeursAttributs) {
		NSMutableDictionary dict = new NSMutableDictionary();
		java.util.Enumeration e = valeursAttributs.keyEnumerator();
		while (e.hasMoreElements()) {
			String nomAttribut = (String)e.nextElement();
			Object valeur = valeursAttributs.objectForKey(nomAttribut);
			Attribut attribut = entite.attributAvecNomDestination(nomAttribut);
			dict.setObjectForKey(valeur, attribut.nomSource());
		}
		return dict;
	}
	public String remettreMessageErreurEnFormePourEntite(String messageErreur,String nomEntite) {
		// Les messages d'erreur sont stockées dans une String, chaque message est délimité par un retour chariot.
		// Un message est structuré avec un label suivi de >>, suivi ou non d'un texte
		String resultat = "";
		String[] messages = messageErreur.split(System.getProperty("line.separator"));
		for (int i = 0; i < messages.length;i++) {
			String erreur = messages[i];
			int position = erreur.indexOf(ResultatImport.ObjetAvecErreur.DELIMITEUR);
			String cle = erreur.substring(0,position);
			String messagePourCle = "";
			if (cle.indexOf("General_") >= 0) {
				messagePourCle = ConstantesErreur.messageGeneralPourCle(cle);
			} else {
				messagePourCle = ConstantesErreur.messagePourEntiteEtCle(nomEntite, cle);
			}
			String texte = "";
			if (erreur.length() >= position + ResultatImport.ObjetAvecErreur.DELIMITEUR.length()) {
				texte = erreur.substring(position + ResultatImport.ObjetAvecErreur.DELIMITEUR.length());
			}
			if (texte.length() > 0) {
				resultat += messagePourCle + " : " + texte + System.getProperty("line.separator");
			} else {
				resultat += messagePourCle + System.getProperty("line.separator");
			}
		}
		return resultat;
	}

	//	Méthodes privées
	private EOEditingContext editingContext() {
		return AutomateImport.sharedInstance().editingContext();
	}

	// Vérifie toutes les nomenclatures et modifie éventuellement les données (capitalisation...)
	// Les objets d'import doivent implémenter la méthode statique valider
	private String validerEntite(String nomEntite,NSMutableDictionary valeursAttributs) {
		LogManager.logDetail("Validation des attributs l'entite");
		//TODO A déplacer dans le modèle Import
		String nomClasse = "org.cocktail.connecteur.serveur.modele.entite_import.EO" + nomEntite;
		try {
			Class maClasse = Class.forName(nomClasse);
			java.lang.reflect.Method methode = maClasse.getDeclaredMethod("validerAttributs",new Class[] {EOEditingContext.class,NSMutableDictionary.class});
			return (String)methode.invoke(null,new Object[] {editingContext(),valeursAttributs});	// Invocation d'une méthode statique, pas d'objet
		} catch (Exception e) {
			LogManager.logInformation("Une erreur est survenue lors de la validation des attributs de l'entité " + nomClasse);
			LogManager.logException(e);
			return "";
		}
	}

	private boolean comparerPersonnels(EOPersonnel ancienPersonnel,EOPersonnel personnel) {
		// 22/06/2010 - prendre en compte le fait que les individus importés ne sont pas nécessairement des personnels
		if (ancienPersonnel == null && personnel == null) {
			return true;
		}
		if ((ancienPersonnel == null && personnel != null) || (ancienPersonnel != null && personnel == null)) {
			return false;
		}
		// Vérifier si les anciens personnels ont un statut valide
		if (ancienPersonnel != null && ancienPersonnel.temImport().equals(ObjetImport.TRANSFERE) == false) {
			// Le personnel n'a pas été importé
			return false;
		}
		// 05/11/2010 - correction d'un bug : comparaison de ancienPersonnel avec ancienPersonnel !!
		if ((personnel.noMatricule() != null && ancienPersonnel != null && ancienPersonnel.noMatricule() == null) || (personnel.noMatricule() != null && ancienPersonnel != null && ancienPersonnel.noMatricule() != null && ancienPersonnel.noMatricule().equals(personnel.noMatricule()) == false)) {
			return false;
		}
		if ((personnel.nbEnfants() != null && ancienPersonnel != null && ancienPersonnel.nbEnfants() == null) || (personnel.nbEnfants() != null && ancienPersonnel != null && ancienPersonnel.nbEnfants() != null && ancienPersonnel.nbEnfants().intValue() != personnel.nbEnfants().intValue())) {
			return false;
		}
		if ((personnel.txtLibre() != null && ancienPersonnel != null && ancienPersonnel.txtLibre() == null) || (personnel.txtLibre() != null && ancienPersonnel != null && ancienPersonnel.txtLibre() != null && ancienPersonnel.txtLibre().equals(personnel.txtLibre()) == false)) {
			return false;
		}
		if ((personnel.temTitulaire() != null && ancienPersonnel != null && ancienPersonnel.temTitulaire() == null) || (personnel.temTitulaire() != null && ancienPersonnel != null && ancienPersonnel.temTitulaire() != null && ancienPersonnel.temTitulaire().equals(personnel.temTitulaire()) == false)) {
			return false;
		}
		if ((personnel.numen() != null && ancienPersonnel != null && ancienPersonnel.numen() == null) || (personnel.numen() != null && ancienPersonnel != null && ancienPersonnel.numen() != null && ancienPersonnel.numen().equals(personnel.numen()) == false)) {
			return false;
		}

		return true;
	}
}
