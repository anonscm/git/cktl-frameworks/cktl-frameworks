/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur;

import com.webobjects.foundation.NSMutableDictionary;

// 22/06/2010 - validation de l'iban. Erreur sur les codes banques
// 16/03/2O11 - Erreur sur les rne dans les contrats
// 16/08/2011 - Messages pour les congés
public class ConstantesErreur {
	private final static String[][] messagesGeneral = {
		{"General_Attribut_Obligatoire","Attribut obligatoire"},
		{"General_Longueur_Invalide","Longueur invalide"},
		{"General_Nombre_Entier","Nombre entier attendu"},
		{"General_Nombre_Decimal","Nombre decimal attendu"},
		{"General_Date_Invalide","Le format de date choisi ne correspond pas à la date"},
		{"General_Attribut_Tronque","Attribut tronqué"}
	};
	private final static String[][] messagesIndividu = {
		{"Civilite_Inconnue","Civilite inconnue"},
		{"Date_Naissance","Date de naissance posterieure a la date du jour"},
		{"Commune_Inconnue","Commune de naissance inconnue"},
		{"Departement_Inconnu","Departement inconnu"},
		{"Pays_Inconnu","Pays inconnu ou invalide "},
		{"Situation_Militaire_Inconnue", "Situation militaire inconnue"},
		{"Situation_Familiale_Inconnue","Situation familiale est inconnue"},
		{"Valeur_Temoin","Uniquement O ou N"},
		{"Erreur_Numero_SS","Erreur du Numero Insee"},
		{"Prise_Cpt_Insee","Prise en compte Insee"}
	};
	private final static String[][] messagesStructure = {
		{"Type_Etablissement_Invalide","Type Etablissement invalide"},
		{"Academie_Invalide","Academie invalide"},
		{"Statut_Invalide","Statut juridique invalide"},
		{"Type_Structure_Invalide", "Type structure invalide"},
		{"Rne_Inconnu","Rne inconnu"},
		{"Structure_parent","Doit avoir une structure parent"},
		{"Siret_Invalide","Siret invalide"},
		{"Code_Ape_Inconnu", "Code Ape inconnu"},
		{"Date_Ouverture","Date d'ouverture posterieure a la date de fermeture"},
		{"Pourcentage_Export_Invalide","Pourcentage d'export > 100"}
	};
	private final static String[][] messagesEmploi = {
		{"Categorie_Emploi_Invalide","Categorie Emploi invalide"},
		{"Chapitre_Invalide","Chapitre invalide"},
		{"Article_Invalide","Article invalide"},
		{"Programme_Invalide","Programme invalide"},
		{"Budget_Invalide","Nature Budget invalide"},
		{"Programme_Titre_Invalide", "Programme Titre invalide"},
		{"Rne_Inconnu","Rne inconnu"},
		{"Referens_Emploi_Inconnu","Referens Emploi inconnu"},
		{"Bap_Inconnu","Bap inconnu"},
		{"Disc2ndDegre_Inconnu", "Disc 2nd degre inconnu"},
		{"SectionCnu_Inconnue"," Section Cnu inconnue"},
		{"SousSectionCnu_Inconnue"," Sous-section Cnu inconnue"},
		{"Specitalite_Itarf_Inconnue","Specialite Itarf inconnue"},
		{"Specitalite_Atos_Inconnue","Specialite Atos inconnue"},
		{"Specialites_Trop_Nombreuses","Trop de spécialités ou code Sous-section CNU trop long"},
		{"Quotite_Nulle","Quotité à 0%"},
		{"Quotite_Invalide", "Quotite > 100%"},
		{"Date_Creation","Date de creation posterieure a la date de suppression"},
		{"Date_Publication","Date de publication posterieure a la date de suppression"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesAdresse = {
		{"ID_SOURCE_ou_STR_SOURCE","Fournir ID_SOURCE ou STR_SOURCE"},
		{"UNE_SEULE_ID","ID_SOURCE et STR_SOURCE fournis"},
		{"Commune_Inconnue","Commune inconnue/incompatible avec code postal"},
		{"Pays_Inconnu","Pays inconnu"},
		{"Type_Adresse_Inconnu","Type adresse inconnu"}
	};
	private final static String[][] messagesTelephone = {
		{"ID_SOURCE_ou_STR_SOURCE","Fournir ID_SOURCE ou STR_SOURCE"},
		{"UNE_SEULE_ID","ID_SOURCE et STR_SOURCE fournis"},
		{"Type_No_Inconnu", "Type de numero inconnu"},
		{"Type_Tel_Inconnu","Type de telephone inconnu"},
		{"No_Tel_Invalide","Numero Tel invalide"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesRib = {
		{"ID_SOURCE_ou_STR_SOURCE","Fournir ID_SOURCE ou STR_SOURCE"},
		{"UNE_SEULE_ID","ID_SOURCE et STR_SOURCE fournis"},
		{"Banque_Guichet","Vous devez fournir le code banque-guichet ou le bic"},
		{"Domiciliation_Inconnue","Domiciliation absente"},
		{"Erreur_Creation","Erreur de creation de la banque"},
		{"Rib_Incomplet","RIB incomplet"},
		{"Rib_Invalide","RIB invalide"},
		{"Iban_Invalide","IBAN invalide"}
	};
	private final static String[][] messagesEnfant = {
		{"Id_Source","ID_SOURCE absent"},
		{"Sexe_Invalide",  "Sexe invalide"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesIndividuFamiliale = {
		{"Id_Source","ID_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Code_Situation_Inconnu","Code situation familiale inconnu"}
	};
	private final static String[][] messagesCarriere = {
		{"Id_Source","ID_SOURCE absent"},
		{"Type_Population_Inconnu","Type population inconnu"},
		{"Date_Invalide","Date de debut > Date de fin"},
	};
	private final static String[][] messagesChangementPosition = {
		{"Id_Source","ID_SOURCE absent"},
		{"Carriere_Accueil","CARRIERE_ACCUEIL obligatoire"},
		{"Position_Inconnue","Position inconnue"},
		{"Motif_Obligatoire","Motif obligatoire pour cette position"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Duree_Invalide","Durée maximum"},
		{"Date_Fin_Obligatoire","Date de fin obligatoire"},
		{"Rne_Inconnu","Rne inconnue"},
		{"Rne_Origine_Inconnu","Rne Origine inconnue"},
		{"Quotite_Nulle","Quotité à 0%"},
		{"Quotite_Invalide", "Quotite > 100%"}
	};
	private final static String[][] messagesElementCarriere = {
		{"Id_Source","ID_SOURCE absent"},
		{"Car_Source","CAR_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Referens_Emploi_Inconnu","Referens Emploi inconnu"},
		{"Bap_Inconnu","Bap inconnu"},
		{"Disc2ndDegre_Inconnu", "Disc 2nd degre inconnu"},
		{"SectionCnu_Inconnue"," Section Cnu inconnue"},
		{"SousSectionCnu_Inconnue"," Sous-section Cnu inconnue"},
		{"Specitalite_Itarf_Inconnue","Specialite Itarf inconnue"},
		{"Specitalite_Atos_Inconnue","Specialite Atos inconnue"},
		{"Specialites_Trop_Nombreuses","Trop de spécialités ou code Sous-section CNU trop long"},
		{"Corps_Inconnu","Corps inconnu"},
		{"Grade_Inconnu","Grade inconnu"},
		{"Corps_Grade_Incompatible","Corps et grades incompatibles"},
		{"Echelon_Inconnu","Echelon inconnu pour le grade"},
		{"Chevron_Inconnu"," Chevron inconnu pour le grade et l'echelon"},
		{"Type_Acces_Inconnu","Type Acces inconnu"},
		{"Quotite_Nulle","Quotité à 0%"},
		{"Quotite_Invalide", "Quotite > 100%"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesContrat = {
		{"Id_Source","ID_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Type_Contrat_Inconnu","Type Contrat inconnu"},
		{"Rne_Inconnu","Rne inconnu"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesContratAvenant = {
		{"Ctr_Source","CTR_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Referens_Emploi_Inconnu","Referens Emploi inconnu"},
		{"Bap_Inconnu","Bap inconnu"},
		{"Disc2ndDegre_Inconnu", "Disc 2nd degre inconnu"},
		{"SectionCnu_Inconnue"," Section Cnu inconnue"},
		{"SousSectionCnu_Inconnue"," Sous-section Cnu inconnue"},
		{"Specitalite_Itarf_Inconnue","Specialite Itarf inconnue"},
		{"Specitalite_Atos_Inconnue","Specialite Atos inconnue"},
		{"Categorie_Inconnue","Categorie inconnue"},
		{"Condition_Recrutement_Inconnue","Condition Recrutement inconnue"},
		{"Grade_Inconnu","Grade inconnu"},
		{"Echelon_Inconnu","Echelon inconnu pour le grade"},
		{"Grade_Obligatoire"," Fournir le grade"},
		{"Quotite_Nulle","Quotité à 0%"},
		{"Quotite_Invalide", "Quotite > 100%"},
		{"Type_Montant_Invalide","Type Montant invalide"},
		{"Valeur_Temoin","Uniquement O ou N"}

	};
	private final static String[][] messagesAffectation = {
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Quotite_Nulle","Quotité à 0%"},
		{"Quotite_Invalide", "Quotite > 100%"}
	};
	private final static String[][] messagesOccupation = {
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Quotite_Nulle","Quotité à 0%"},
		{"Quotite_Invalide", "Quotite > 100%"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesCompte = {
		{"ID_SOURCE_ou_STR_SOURCE","Fournir ID_SOURCE ou STR_SOURCE"},
		{"UNE_SEULE_ID","ID_SOURCE et STR_SOURCE fournis"},
		{"Vlan_Inconnu", "Vlan inconnu"},
		{"Valeur_Temoin","Uniquement O ou N"},
		{"Valeur_Temoin_Numerique","Uniquement 0 ou 1"}
	};
	private final static String[][] messagesConge = {
		{"Id_Source","ID_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messageCongeMaternite = {
		{"Id_Source","ID_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Type_Conge_Matern_Inconnu","Type de conge Maternite inconnu"},
		{"Valeur_Temoin","Uniquement O ou N"},
		{"Date_Naissance_Prev","Date Naissance Previsionnelle > Date de constatation"},
		{"Date_Accouchement","Date Accouchement > Date de constatation"}
	};
	
	private final static String[][] messageCgntMaladie = {
		{"Id_Source","ID_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Valeur_Temoin","Uniquement O ou N"},
		{"Anciennete_Inconnue","Anciennete inconnue (verifier la table de nomenclature GRHUM.PARAM_CGNT_MAL)"}
	};
	private final static String[][] messageCgntAccidentTrav = {
		{"Id_Source","ID_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"DatesPT_Invalides","Date de debut Plein Trait > Date de fin de Plein Trait"},
		{"DateDebPT_Invalide", "Date de debut Plein Trait < Date de debut"},
		{"DateFinPT_Invalide","Date de fin Plein Trait > Date de fin"},
		{"Valeur_Temoin","Uniquement O ou N"},
		{"Anciennete_Inconnue","Anciennete inconnue (verifier la table de nomenclature GRHUM.PARAM_CGNT_ACC_TRAV)"}
	};
	private final static String[][] messagesDetailCld = {
		{"Id_Source","ID_SOURCE absent"},
		{"Eimp_Source","CG_SOURCE absent"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Num_Taux","0 <= NUM_TAUX <= 100%"},
		{"Den_Taux", "0 < DEN_TAUX <= 100%"}
	};
	private final static String[][] messagesConservationAnciennete = {
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Valeur_Temoin","Uniquement O ou N"},
		{"Anc_Nb_Annees","Nb Années : entre 0 et 99 maximum"},
		{"Anc_Nb_Mois","Nb Mois : entre 0 et 11"},
		{"Anc_Nb_Jours","NbJours : entre 0 et 30"},
		{"Anc_Annee","Année sur 4 caractères (AAAA)"}
	};
	private final static String[][] messagesReliquatsAnciennete = {
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Motif_Reduction_Inconnu","Motif de réduction inconnu"},
		{"Anc_Nb_Annees","Nb Années : entre 0 et 99 maximum"},
		{"Anc_Nb_Mois","Nb Mois : entre 0 et 11"},
		{"Anc_Nb_Jours","NbJours : entre 0 et 30"},
		{"Anc_Annee","Année sur 4 caractères (AAAA)"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesIndividuDiplome = {
		{"Diplome_Inconnu","Type de diplome inconnu"},
		{"Titulaire_Diplome_Inconnu","Type d'obtention du diplome inconnu"}
	};
	private final static String[][] messagesPeriodesMilitaires = {
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Type_Periode_Inconnu","Type de periode inconnu"}
	};
	private final static String[][] messagesPasse = {
		{"Quotite_Invalide", "Quotité de service = 0 ou  50% <= quotité <= 100%"},
		{"Quotite_Cotisation_Invalide", "Quotité de cotisation = 0 ou  50% <= quotité <= 100%"},
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Categorie_Inconnue","Catégorie inconnue"},
		{"Type_Population_Inconnu","Type de population inconnu"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesDepart = {
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Motif_Inconnu","Motif inconnu"},
		{"Rne_Inconnu","Rne inconnu"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private final static String[][] messagesTempsPartiel = {
		{"Date_Invalide","Date de debut > Date de fin"},
		{"Motif_Inconnu","Motif inconnu"},
		{"Periodicite_Invalide","6 <= périodicité <= 36 (mois)"},
		{"Quotite_Indefinie","Quotité indéfinie dans la nomenclature (GRHUM.QUOTITE)"},
		{"Quotite_Invalide", "0% < quotité <= 90%"},
		{"Valeur_Temoin","Uniquement O ou N"}
	};
	private static NSMutableDictionary messages;
	
	// Utilitaires statiques
	/** Retourne le message general associe ae une cle */
	public static String messageGeneralPourCle(String cle) {
		for (int i = 0; i < messagesGeneral.length;i++) {
			String[] message = (String[])messagesGeneral[i];
			if (message[0].equals(cle)) {
				return message[1];
			}
		}
		return "";
	}
	/** Retourne le message associe ae une entite et une cle */
	public static String messagePourEntiteEtCle(String nomEntite,String cle) {
		if (messages == null) {
			buildMessages();
		}
		String[][] messagesForEntite = (String[][])messages.valueForKey(nomEntite);
		if (messagesForEntite != null) {
			for (int i = 0; i < messagesForEntite.length;i++) {
				String[] message = (String[])messagesForEntite[i];
				if (message[0].equals(cle)) {
					return message[1];
				}
			}
		}
		return "";
	}
	/** Retourne le nombre de messages associes ae une entite */
	public static int nbMessagesPourEntite(String nomEntite) {
		String[][] messagesForEntite = (String[][])messages.valueForKey(nomEntite);
		if (messagesForEntite != null) {
			return messagesForEntite.length;
		} else {
			return 0;
		}
	}
	
	// Méthodes statiques
	private static void buildMessages() {
		messages = new NSMutableDictionary();
		messages.setObjectForKey(messagesIndividu, "Individu");
		messages.setObjectForKey(messagesStructure, "Structure");
		messages.setObjectForKey(messagesEmploi, "Emploi");
		messages.setObjectForKey(messagesAdresse, "Adresse");
		messages.setObjectForKey(messagesTelephone, "Telephone");
		messages.setObjectForKey(messagesRib, "Rib");
		messages.setObjectForKey(messagesEnfant, "Enfant");
		messages.setObjectForKey(messagesIndividuFamiliale, "IndividuFamiliale");
		messages.setObjectForKey(messagesCarriere, "Carriere");
		messages.setObjectForKey(messagesChangementPosition, "ChangementPosition");
		messages.setObjectForKey(messagesElementCarriere, "ElementCarriere");
		messages.setObjectForKey(messagesContrat, "Contrat");
		messages.setObjectForKey(messagesContratAvenant, "ContratAvenant");
		messages.setObjectForKey(messagesAffectation, "Affectation");
		messages.setObjectForKey(messagesOccupation,"Occupation");
		messages.setObjectForKey(messagesCompte, "Compte");
		messages.setObjectForKey(messagesConge, "CongeMaladie");
		messages.setObjectForKey(messagesConge, "CongeMaladieSsTrt");
		messages.setObjectForKey(messagesConge, "CongeAccidentServ");
		messages.setObjectForKey(messagesConge, "Clm");
		messages.setObjectForKey(messagesConge, "Cld");
		messages.setObjectForKey(messageCongeMaternite, "CongeMaternite");
		messages.setObjectForKey(messagesDetailCld, "CldDetail");
		messages.setObjectForKey(messageCgntMaladie, "CgntMaladie");
		messages.setObjectForKey(messagesConge, "CgntCgm");
		messages.setObjectForKey(messageCgntAccidentTrav, "CgntAccidentTrav");
		messages.setObjectForKey(messagesConservationAnciennete, "ConservationAnciennete");
		messages.setObjectForKey(messagesReliquatsAnciennete, "ReliquatsAnciennete");
		messages.setObjectForKey(messagesIndividuDiplome, "IndividuDiplomes");
		messages.setObjectForKey(messagesPeriodesMilitaires, "PeriodesMilitaires");
		messages.setObjectForKey(messagesPasse, "Passe");
		messages.setObjectForKey(messagesDepart, "Depart");
		messages.setObjectForKey(messagesTempsPartiel, "TempsPartiel");
	}
}
