/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;
import org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCld;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite;
import org.cocktail.connecteur.serveur.modele.entite_import.EOContrat;
import org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant;
import org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes;
import org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation;
import org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein;
import org.cocktail.connecteur.serveur.modele.entite_import.EOStructure;
import org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuEtPeriode;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuOuStructure;
import org.cocktail.connecteur.serveur.modele.importer.StructureComparator;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator.ComparisonException;
import com.webobjects.foundation.NSMutableArray;

// 08/02/2011 - Modification pour trier les records sur leur source
// 09/2011 - Modification pour trier les congés par date de début croissant
// 17/10/2011 - Modification pour trier les congés de maternité par type de congé décroissant
public class TrieurEntitesImport {
	/** Methode de tri des records &agrave importer
	 * Tri specifique pour les structures sur le type de structure (pour &ecirc;tre s&ucirc;r que les p&egrave;res sont importes
	 * avant les fils), sinon tri sur l'idSource ascending pour les objets de type Individu ou ObjetImportPourIndividu  
	 * ou sur le strSource pour les objets de type ObjetImportPourIndividuOuStructure ou sur la date debut pour les
	 * objets de type ObjetImportPourIndividuEtPeriode ou pour les details de Cld */
	public static NSArray trier(NSArray records) {
		if (records == null || records.count() == 0) {
			return records;
		}
		Object firstObjet = records.objectAtIndex(0);
		if (firstObjet instanceof ObjetImport == false) {
			return records;
		}
		if (firstObjet instanceof EOStructure) {
			try {
				NSArray recordsTries = trierStructures(records);	// Trier les structures selon la hiérarchie de structures
				/*java.util.Enumeration e = recordsTries.objectEnumerator();
				while (e.hasMoreElements()) {
					EOStructure structure = (EOStructure)e.nextElement();
					System.out.println("structure : " + structure.llStructure() + " source : " + structure.strSource() + " pere : " + structure.strSourcePere());
				}*/
				return recordsTries ;
			} catch (Exception e) {
				e.printStackTrace();
				return records;	// pas de tri
			}
		} else {
			ObjetImport firstObj = (ObjetImport)firstObjet;
			NSMutableArray sorts = new NSMutableArray();
			if (firstObj instanceof EOIndividu || firstObj instanceof ObjetImportPourIndividu) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("idSource",EOSortOrdering.CompareAscending));	
			} 
			if (firstObj instanceof ObjetImportPourIndividuOuStructure) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("strSource",EOSortOrdering.CompareAscending));	
			}
			if (firstObjet instanceof EOContrat) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("ctrSource",EOSortOrdering.CompareAscending));	
			}
			if (firstObjet instanceof EOContratAvenant) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("ctrSource",EOSortOrdering.CompareAscending));	
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("avSource",EOSortOrdering.CompareAscending));	
			} 
			if (firstObj instanceof EOCarriere || firstObj instanceof ObjetImportPourCarriere) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("carSource",EOSortOrdering.CompareAscending));	
			}		
			if (firstObj instanceof EOElementCarriere) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("elSource",EOSortOrdering.CompareAscending));	
			}
			if (firstObj instanceof EOChangementPosition) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("cpSource",EOSortOrdering.CompareAscending));	
			}
			if (firstObj instanceof EOOccupation) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("occSource",EOSortOrdering.CompareAscending));	
			}
			if (firstObj instanceof EOAffectation) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("affSource",EOSortOrdering.CompareAscending));	
			}
			if (firstObj instanceof EOIndividuDiplomes) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("dDiplome",EOSortOrdering.CompareAscending));	
			}
			// 17/10/2011 - on veut que les congés de type maternité soient les premiers (type de CG MATER - GROSS - ACCOU)
			if (firstObj instanceof EOCongeMaternite) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("cTypeCgMatern",EOSortOrdering.CompareDescending));	
			}
			// Pour les congés, on s'assure que les congés avec une annulation sont après ceux n'en ayant pas
			if (firstObj instanceof CongeAvecArrete) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOCld.ANNULATION_SOURCE_KEY,EOSortOrdering.CompareAscending));	
			}
			// Pour les tempsPartiel, on s'assure que ceux avec une annulation sont après ceux n'en ayant pas
			if (firstObj instanceof EOTempsPartiel) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOTempsPartiel.ARRETE_ANNULATION_SOURCE_KEY,EOSortOrdering.CompareAscending));	
			}
			// Pour les RepriseTempsPlein, on s'assure que ceux avec une annulation sont après ceux n'en ayant pas
			if (firstObj instanceof EORepriseTempsPlein) {
				sorts.addObject(EOSortOrdering.sortOrderingWithKey(EORepriseTempsPlein.ARRETE_ANNULATION_SOURCE_KEY,EOSortOrdering.CompareAscending));	
			}
			if (firstObj instanceof ObjetImportPourIndividuEtPeriode || firstObj instanceof EOCldDetail) {
				// Trier les objets par ordre de date croissante
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending));	
			}
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(records,sorts);	
			
		}
	}
	// Méthodes privées
	// On trie les structures en mettant les pères et toute la hiérarchie de fils de chaque père
	private static NSArray trierStructures(NSArray structures) {
		NSMutableArray structuresTriees = new NSMutableArray();
		try {
			// Trier les structures pour avoir celles sans parent en tête
			NSMutableArray structuresATrier = new NSMutableArray(structures.sortedArrayUsingComparator(new StructureComparator()));	// pour avoir les racines en tête
			java.util.Enumeration e = structuresATrier.objectEnumerator();
			while (e.hasMoreElements()) {
				EOStructure structure = (EOStructure)e.nextElement();
				// Si c'est un noeud racine, on l'ajoute au tableau et on recherche tous ses fils récursivement
				if (structure.strSourcePere() == null || structure.strSourcePere().equals(structure.strSource())) {
					// c'est une racine
					structuresTriees.addObject(structure);
					structuresTriees.addObjectsFromArray(rechercherFils(structure,structuresATrier));
				} else {
					// Pour les structures qui ne sont pas encore triées via les fils et qui ont un parent
					// On se base sur le strSources pour vérifier le containsObject
					if (((NSArray)structuresTriees.valueForKey("strSource")).containsObject(structure.strSource()) == false) {
						// Rechercher le parent principal de cette structure
						EOStructure parent = rechercherParentRacine(structure,structuresATrier);
						if (parent == null) {
							// pas de parent, l'ajouter directement
							structuresTriees.addObject(structure);
						} else {
							if (((NSArray)structuresTriees.valueForKey("strSource")).containsObject(parent.strSource()) == false) {
								structuresTriees.addObject(parent);
							}
							// Rechercher tous les fils de ce parent
							structuresTriees.addObjectsFromArray(rechercherFils(parent,structuresATrier));
						}
					}
				}
			}
			return structuresTriees;
		} catch (ComparisonException e) {
			e.printStackTrace();
			LogManager.logException(e);
			return structures;
		}
	}
	private static NSArray rechercherFils(EOStructure structureParent, NSMutableArray structuresATrier) {
		NSMutableArray fils = new NSMutableArray();
		java.util.Enumeration e = structuresATrier.objectEnumerator();
		while (e.hasMoreElements()) {
			EOStructure structure = (EOStructure)e.nextElement();
			// Si on n'a pas déjà vu ce fils et que la structure a pour parent structureParent et qu'il ne s'agit pas d'elle-même (cas des racines)
			// containsObject directement sur la structure ne marche pas
			if (((NSArray)fils.valueForKey("strSource")).containsObject(structure.strSource()) == false && structure.strSource().equals(structureParent.strSource()) == false && structure.strSourcePere() != null &&  structure.strSourcePere().equals(structureParent.strSource())) {
				fils.addObject(structure);
				fils.addObjectsFromArray(rechercherFils(structure,structuresATrier));
			}
		}
		LogManager.logDetail("fils de " + structureParent.llStructure() + "\n" + fils.valueForKey("llStructure"));
		return fils;
	}
	private static EOStructure rechercherParentRacine(EOStructure structureFils,NSArray structures) {
		java.util.Enumeration e = structures.objectEnumerator();
		EOStructure parent = null;
		while (e.hasMoreElements()) {
			EOStructure structure = (EOStructure)e.nextElement();
			if (structure != structureFils && structureFils.strSourcePere() != null && structureFils.strSourcePere().equals(structure.strSource())) {
				parent = structure;
				EOStructure nouveauParent = rechercherParentRacine(structure,structures);
				if (nouveauParent != null) {	// on a trouvé un parent plus haut
					parent = nouveauParent;
					break;
				}
			}
		}
		if (parent != null) {
			LogManager.logDetail("parent de " + structureFils.llStructure() + " : " + parent.llStructure());
		} else {
			LogManager.logDetail("pas de parent pour " + structureFils.llStructure());
		}
		return parent;
	}
}
