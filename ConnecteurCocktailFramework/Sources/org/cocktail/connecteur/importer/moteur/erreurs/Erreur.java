package org.cocktail.connecteur.importer.moteur.erreurs;

import com.webobjects.foundation.NSDictionary;

public class Erreur {
	NSDictionary valAttributs;
	String message;
	String etape;
	String nomEntite;
	int noLigne;

	public Erreur(String etape, String nomEntite, NSDictionary valAttributs, String msg, int noLigne) {
		this.valAttributs = valAttributs;
		this.etape = etape;
		this.nomEntite = nomEntite;
		message = msg.replace('\n', ' ');
		message = message.replace('\t', ' ');
		this.noLigne=noLigne;
	}

	public NSDictionary getValAttributs() {
		return valAttributs;
	}

	public String getMessage() {
		return message;
	}

	public String getEtape() {
		return etape;
	}

	public String getNomEntite() {
		return nomEntite;
	}

	public int getNoLigne() {
		return noLigne;
	}
}
