package org.cocktail.connecteur.importer.moteur.erreurs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.cocktail.connecteur.common.CocktailUtilities;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

public class ErreurExcelGenerateur {
	private CellStyle getStyleEntete(Workbook workbook) {
		Font font = workbook.createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short)10);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		//font.setColor(IndexedColors.WHITE.getIndex());
		
		CellStyle style=workbook.createCellStyle();
		style.setFont(font);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setAlignment(CellStyle.ALIGN_CENTER);;
		
		return style;
	}

	private CellStyle getStyleDonneeAttribut(Workbook workbook, boolean aligneCentre, boolean estPaire) {
		Font font = workbook.createFont();
		font.setFontName("Arial");
	    font.setFontHeightInPoints((short)10);

		CellStyle style=workbook.createCellStyle();
		style.setFont(font);
		if (aligneCentre)
			style.setAlignment(CellStyle.ALIGN_CENTER);
		else
			style.setAlignment(CellStyle.ALIGN_LEFT);
			
		if (estPaire) {
			style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		} else {
			style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		}
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		
		return style;
	}

	private CellStyle getStyleAucuneErreur(Workbook workbook) {
		Font font = workbook.createFont();
		font.setFontName("Arial");
	    font.setFontHeightInPoints((short)10);
	    font.setItalic(true);

		CellStyle style=workbook.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_LEFT);;
		style.setFont(font);
		
		return style;
	}
	
	private void ajouteCellule(Row row, int col, String texte, CellStyle style) {
		Cell cell=row.createCell(col);
		cell.setCellStyle(style);;
		cell.setCellValue(texte);
	}
	
	/**
	 * @param sheet
	 * @param rowIndex
	 * @param entite
	 * @return Le nombre de colonne
	 */
	private int ajouteEntete(Sheet sheet, int rowIndex, Entite entite) {
		CellStyle styleEntete=getStyleEntete(sheet.getWorkbook());
		
		Row row=sheet.createRow(rowIndex);
		
		int col=0;
		ajouteCellule(row,col++,"N° LIGNE",styleEntete);
		ajouteCellule(row,col++,"ETAPE",styleEntete);
		ajouteCellule(row,col++,"MESSAGE",styleEntete);

		for (Attribut attribut : entite.attributs()) {
			ajouteCellule(row,col++,attribut.nomSource(),styleEntete);
		}
		
		return col;
	}
	
	private void ajouteAttributs(Sheet sheet, int rowIndex, Entite entite, ErreurManager erreurManager,String nomEntite) {
		List<Erreur> erreurs = erreurManager.getErreurs(nomEntite);
		if (erreurs == null)
			return;

		if (erreurs.size()==0) {
			CellStyle styleAucuneErreur=getStyleAucuneErreur(sheet.getWorkbook());
			Row row=sheet.createRow(rowIndex);
			ajouteCellule(row,0,"Aucune erreur",styleAucuneErreur);
			return;
		}

		CellStyle styleDonneeAttributPairCentre=getStyleDonneeAttribut(sheet.getWorkbook(), true, true);
		CellStyle styleDonneeAttributImpairCentre=getStyleDonneeAttribut(sheet.getWorkbook(), true, false);
		CellStyle styleDonneeAttributPairNonCentre=getStyleDonneeAttribut(sheet.getWorkbook(), false, true);
		CellStyle styleDonneeAttributImpairNonCentre=getStyleDonneeAttribut(sheet.getWorkbook(), false, false);

		for (Erreur err : erreurs) {
			Row row=sheet.createRow(rowIndex);
			
			int col=0;		
			CellStyle styleLigneCentre;
			CellStyle styleLigneNonCentre;
			if (rowIndex%2==0) {
				styleLigneCentre=styleDonneeAttributPairCentre;
				styleLigneNonCentre=styleDonneeAttributPairNonCentre;
			}
			else {
				styleLigneCentre=styleDonneeAttributImpairCentre;
				styleLigneNonCentre=styleDonneeAttributImpairNonCentre;
			}

			NSDictionary dict = err.getValAttributs();

			String noLigne="";
			if (err.getNoLigne()>0)
				noLigne=Integer.toString(err.getNoLigne());
			ajouteCellule(row,col++,noLigne,styleLigneNonCentre);
			ajouteCellule(row,col++,err.getEtape(),styleLigneNonCentre);
			ajouteCellule(row,col++,err.getMessage(),styleLigneNonCentre);

			for (Attribut attribut : entite.attributs()) {
				Object value = dict.valueForKey(attribut.nomDestination());
				String valueString;
				if (value instanceof NSTimestamp) {
					NSTimestamp date = (NSTimestamp) value;
					valueString= DateCtrl.dateToString(date, EOImportParametres.formatDate());
				} else if (value == null) {
					valueString = "null";;
				} else {
					valueString = value.toString();
				}
				ajouteCellule(row,col++,valueString,styleLigneCentre);
			}
			rowIndex++;
		}
	}
	
	private void retailleColonnes(Sheet sheet, int nbCol) {
		// Numéro de ligne
		sheet.setColumnWidth(0, 10*256);
		// Le nom de l'étape
		sheet.setColumnWidth(1, 20*256);
		// Le message d'erreur
		sheet.setColumnWidth(2, 80*256);

		// Les attributs : autoSize + 10%
		for (int iCol = 3; iCol < nbCol; iCol++) {
			sheet.autoSizeColumn(iCol);
			int width=sheet.getColumnWidth(iCol);
			width=(int)(width*1.1);
			sheet.setColumnWidth(iCol, width);
		}
	}
	
	private void saveAjouteEntite(Workbook workbook, ErreurManager erreurManager, String nomEntite) {
		ReglesImport reglesImport = new ReglesImport();
		reglesImport.initAvecModele("ModeleImport");

		Entite entite = reglesImport.entiteAvecNomDestination(nomEntite);
		if (entite==null)
			return;

		String sheetName = WorkbookUtil.createSafeSheetName(nomEntite);
		Sheet sheet = workbook.createSheet(sheetName);

		int nbCol=ajouteEntete(sheet, 0, entite);
		ajouteAttributs(sheet, 1, entite, erreurManager, nomEntite);
		retailleColonnes(sheet, nbCol);
	}
	
	private String getFileName(List<String> entites) {
		String path = StringCtrl.replace( (EOImportParametres.pathImport()+"/"), "//", "/") + "Erreurs/";
		CocktailUtilities.verifierPathEtCreer(path);
		
		Date aujourdhui=new Date();
		SimpleDateFormat formater = new SimpleDateFormat("ddMMyyyy_hh-mm");
		
		String nomEntite="";
		if (entites.size()>0)
			nomEntite=entites.get(0)+"_";
		String nomFichier="ResumeErreurs_"+nomEntite+formater.format(aujourdhui)+".xls";
		return path+nomFichier;
	}

	public void save(ErreurManager manager) {
		List<String> entites=manager.getEntites();
		LogManager.logInformation(new Date()+ " - Début sauvegarde fichier résumé. nom fichier = "+getFileName(entites)+", nombre entités = "+entites.size());
		HSSFWorkbook workbook = new HSSFWorkbook();
		for (String nomEntite : entites) {
			saveAjouteEntite(workbook, manager, nomEntite);
		}
		try {
			FileOutputStream fileOut = new FileOutputStream(getFileName(entites));
			workbook.write(fileOut);
		    fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		LogManager.logInformation(new Date()+ " - Fin sauvegarde fichier résumé.");
	}

}
