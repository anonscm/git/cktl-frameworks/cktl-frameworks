package org.cocktail.connecteur.importer.moteur.erreurs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSDictionary;

public class ErreurManager {
	private static ErreurManager instance;

	public static final String ETAPE_IMPORT = "Import";
	public static final String VERIFICATION_IMPORT = "Vérification import";
	public static final String ETAPE_TRANSFERT = "Transfert";

	public static final String ETAPE_DOUBLON = "Vérification Doublons";
	
	private boolean generationErreurActivee=true;

	HashMap<String, List<Erreur>> dictErreurs = new HashMap<String, List<Erreur>>();
	List<String> entites=new ArrayList<String>();

	public static ErreurManager instance() {
		if (instance == null)
			instance = new ErreurManager();
		return instance;
	}

	public ErreurManager() {
		reset();
		generationErreurActivee=EOImportParametres.generationFichierResumeActivee();
	}

	public void ajouterErreur(String etape, String nomEntite, EOGenericRecord record, String error) {
		ajouterErreur(etape, nomEntite, SuperFinder.recordAsDict(record), error, -1);
	}

	public void ajouterErreur(String etape, String nomEntite, NSDictionary valAttributs, String error, int noLigne) {
		if (!generationErreurActivee)
			return;
		
		if (!dictErreurs.containsKey(nomEntite)) {
			dictErreurs.put(nomEntite, new ArrayList<Erreur>());
			if (!entites.contains(nomEntite)) {
				entites.add(nomEntite);
			}
		}
		dictErreurs.get(nomEntite).add(new Erreur(etape, nomEntite, valAttributs, error, noLigne));
	}

	public void save() {
		if (!generationErreurActivee)
			return;

		ErreurExcelGenerateur generateurFichier=new ErreurExcelGenerateur();
		generateurFichier.save(this);
	}

	public List<String> getEntites() {
		return entites;
	}

	public List<Erreur> getErreurs(String nomEntite) {
		return dictErreurs.get(nomEntite);
	}

	public void reset() {
		dictErreurs = new HashMap<String, List<Erreur>>();
		entites=new ArrayList<String>();
	}

	public void ajouterEntite(String nomEntite) {
		if (!generationErreurActivee)
			return;

		if (!entites.contains(nomEntite)) {
			entites.add(nomEntite);
			dictErreurs.put(nomEntite, new ArrayList<Erreur>());
		}		
	}

}
