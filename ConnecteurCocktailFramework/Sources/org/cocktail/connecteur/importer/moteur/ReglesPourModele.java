/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.importer.moteur;

import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.xerces.jaxp.SAXParserFactoryImpl;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.importer.modele.AnalyseurXML;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.modele.Modele;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.ParserAdapter;

import com.webobjects.appserver.WOApplication;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class ReglesPourModele {
	private  Modele modeleDonnees;
	
	// Méthodes publiques
	public void initAvecModele(String nomModele) {
		chargerDescriptions(nomModele);
	}
	public Entite entiteAvecNomSource(String nom) {		
		if (modeleDonnees() != null) {
			return modeleDonnees().entiteAvecNomSource(nom);
		} else {
			return null;
		}
	}
	public Entite entiteAvecNomDestination(String nom) {
		if (modeleDonnees() != null) {
			return modeleDonnees().entiteAvecNomDestination(nom);
		} else {
			return null;
		}
	}
	public String nomEntiteDestinationPourEntiteImport(String nomEntiteImport) {
		if (nomEntiteImport != null && modeleDonnees() != null) {
			return modeleDonnees().entiteAvecNomSource(nomEntiteImport).nomDestination();
		} else {
			return null;
		}
	}
	/** Methode a implementer dans les sous-classes pour determiner la liste des noms d'entites
	 * de la base d'import */
	public abstract NSArray nomEntitesBaseImportTrieesParPriorite();
	protected Modele modeleDonnees() {
		return modeleDonnees;
	}

	
	/**
	 * 
	 * @param attribut1
	 * @param attribut2
	 * @return
	 */
	public static boolean sontAttributsIdentiques(Object attribut1,Object attribut2) {
		if (attribut1 instanceof String) {
			return attribut1.equals(attribut2) || ((String)attribut1).toUpperCase().equals(((String)attribut2).toUpperCase());
		} else if (attribut1 instanceof NSTimestamp) {
			return DateCtrl.dateToString((NSTimestamp)attribut1).equals(DateCtrl.dateToString((NSTimestamp)attribut2));			
		} else if (attribut1 instanceof Number) {
			return attribut1.toString().equals(attribut2.toString());
		} else {
			return attribut1.equals(attribut2);
		}
	}
	// Méthodes privées
	
	/**
	 * 
	 * @param nomModele
	 */
	private void chargerDescriptions(String nomModele) {

		URL urlDescription;
		try {
			urlDescription = WOApplication.application().resourceManager().pathURLForResourceNamed(nomModele + ".XML","ConnecteurCocktailFramework",null);
		} catch (Exception e) {
			// on est en version 5.2.1
			String path = WOApplication.application().path() + "/Contents/Resources/" + nomModele + ".XML";
			try {
				urlDescription = new URL("file://" + path);
			} catch (Exception e1) {
				// URL malformée
				LogManager.logInformation("URL invalide pour " + path);
				LogManager.logException(e1);
				return;
			}
		}
		modeleDonnees = new Modele(nomModele.equals("ModeleImport"));
		AnalyseurXML analyseur = new AnalyseurXML(modeleDonnees);
		try {
			SAXParserFactoryImpl parserFactory = new SAXParserFactoryImpl();
			ParserAdapter parseur = null;
			try {
				parseur = new ParserAdapter(parserFactory.newSAXParser().getParser());
				parseur.setContentHandler(analyseur);
				try {
					parseur.parse(urlDescription.toString());
				} catch (IOException e1) {
					LogManager.logException(e1);
				}
			} catch (ParserConfigurationException e2) {
				LogManager.logException(e2);
			}

		} catch (SAXException e) {
			LogManager.logException(e);
		}	
	}
}
