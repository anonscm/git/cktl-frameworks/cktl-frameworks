//
//  ServerThreadManager.java
//  TestThread
//
//  Created by Christine Buttin on Mon Jun 28 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** @author christine
 * G&egrave;re le lancement de threads sur le serveur d'applications :
 * La methode invoque doit admettre comme dernier param&eegrave;tre une ref&eacu;terence sur le thread manager
 * pour pouvoir communiquer avec celui-ci
 * ex: public void imprimer(Object parametre1,Object parametre2,org.cocktail.mangue.serveur.ServerThreadManager thread)
 */

package org.cocktail.connecteur.serveur;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.ResultatThread;

public class ServerThreadManager extends Thread {
	private ResultatThread resultat;
	private String nomMethodeAInvoquer;
	private Class[] classeParametres;
	private Object[] parametres;
	private Object target;
	private Class classeTarget;

	/** Statuts du serveur */
	public static final String EN_COURS = "En cours";
	public static final String TERMINE = "Termine";
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la methode
	 * @param classeCible classe de cet objet
	 * @param nomMethode methode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la methode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Object objetCible,Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		target = objetCible;
		classeTarget = classeCible;
		nomMethodeAInvoquer = nomMethode;
		preparerParametres(classes,valeurs);	// pour ajouter la référence sur le thread manager
		resultat = new ResultatThread();
	}
	
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la methode
	 * @param nomMethode methode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la methode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Object objetCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(objetCible,objetCible.getClass(),nomMethode,classes,valeurs);
	}
	/** constructeur pour invocation d'une methode de classe
	 * @param classeCible classe
	 * @param nomMethode methode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la methode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(null,classeCible,nomMethode,classes,valeurs);
	}
	/** Retourne le statut du thread */
	public String status() {
		if (resultat != null) {
			return resultat.status();
		} else {
			return null;
		}
	}
	/** Retourne le dernie message envoye par la methode invoquee */
	public String message() {
		if (resultat != null) {
			return resultat.message();
		} else {
			return null;
		}
	}
	/** retourne le resultat de la methode invoquee  **/
	public ResultatThread resultat() {
		return resultat;
	}
	/** transmet le resultat de la methode invoquee */
	public void setResultat(Object valeur) {
		resultat.setValeurResultat(valeur);
	}
	
	/** transmet un message */
	public void setMessage(String unTexte) {
		resultat.setMessage(unTexte);
	}
	
	/** transmet une exception */
	public void setException(String unTexte) {
		resultat.setException(unTexte);
	}
	/** lance l'execution de la methode : a utiliser pour une application Java Client */
	public void run() {
		lancerOperation();
	}
	/** lance l'execution de la methode : a utiliser pour une application WO HTML */
	public void lancerOperation() {
		try {
			resultat.setStatus(EN_COURS);
			java.lang.reflect.Method methode = classeTarget.getMethod(nomMethodeAInvoquer,classeParametres);
			Object object = methode.invoke(target,parametres);
			if (object != null) {
				resultat.setValeurResultat(object);
			}
			resultat.setStatus(TERMINE);

		} catch(Exception e) {
			LogManager.logException(e);
			resultat.setException(e.getMessage());
			resultat.setStatus(TERMINE);

		}
	}
	private void preparerParametres(Class[] classes,Object[] valeurs) {
		classeParametres = new Class[classes.length + 1];
		for (int i = 0; i < classes.length; i++) {
			classeParametres[i] = classes[i];
		}
		try {
			classeParametres[classes.length] = this.getClass();
		} catch (Exception e) {}
		parametres = new Object[valeurs.length + 1];
		for (int i = 0; i < valeurs.length; i++) {
			parametres[i] = valeurs[i];
		}
		parametres[valeurs.length] = this;
	}	

	
}
