package org.cocktail.connecteur.serveur;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

public class VersionFramework extends A_CktlVersion {
	 /** Version Courante du Framework */
	public static String NOM_FRAMEWORK = "Framework Connecteur Cocktail";
	/** Date Modification de l'application */
	public static String DATE = "24/10/2013";
	public static int VERSION_MAJEURE = 2;
	public static int VERSION_MINEURE = 0;
	public static int VERSION_BUILD = 0;
	public static int VERSION_PATCH = 2;
	

	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.Version(), "3", null, true),
				new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects(), "5.3.3", null, false),
				new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava(), "1.5", null, false),
				new CktlVersionRequirements(new VersionOracleUserGrhum(), "1.7.1.4", null, true),
				new CktlVersionRequirements(new VersionOracleUserImport(), "1.1.0.0", null, true),
				new CktlVersionRequirements(new VersionOracleUserMangue(), "1.6.0.3", null, true)
		};
	}
	public String name() {
		return NOM_FRAMEWORK;
	}
	public int versionNumBuild() {
		return VERSION_BUILD;
	}
	public int versionNumMaj() {
		return VERSION_MAJEURE;
	}
	public int versionNumMin() {
		return VERSION_MINEURE;
	}
	public int versionNumPatch() {
		return VERSION_PATCH;
	}
	public String date() {
	    return DATE;
	  }
}
