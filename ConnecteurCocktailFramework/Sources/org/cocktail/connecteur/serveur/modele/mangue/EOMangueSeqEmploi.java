package org.cocktail.connecteur.serveur.modele.mangue;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrhumParametres;
import org.cocktail.connecteur.serveur.modele.SuperFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EOMangueSeqEmploi extends _EOMangueSeqEmploi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509083906128731388L;
	private static Logger log = Logger.getLogger(EOMangueSeqEmploi.class);

	public EOMangueSeqEmploi() {
		super();
	}


	/** calcule la clé primaire en fonction de la base */
	public static Number clePrimairePour(EOEditingContext editingContext) {
		String valeur = EOGrhumParametres.parametrePourCle(editingContext,"BASE_DONNEES");
		if (valeur == null || valeur.equals("ORACLE")) {
			return SuperFinder.numeroSequenceOracle(editingContext,"MangueSeqEmploi",true);
		} else {
			// Pour les bases autres qu'Oracle :
			// cherche un enregistrement dans la table ayant le même nom que la table Oracle
			// incrémente le compteur et le stocke dans cette table en utilisant un nouveau record et retourne la valeur
			EOFetchSpecification fs = new EOFetchSpecification("MangueSeqEmploi",null,new NSArray(EOSortOrdering.sortOrderingWithKey("nextval",EOSortOrdering.CompareDescending)));
			fs.setRefreshesRefetchedObjects(true);
			NSArray compteurs = editingContext.objectsWithFetchSpecification(fs);	
			try {
				EOMangueSeqEmploi compteur = (EOMangueSeqEmploi)compteurs.objectAtIndex(0);
				Integer nouvelleValeur = new Integer(compteur.nextval().intValue() + 1);
				// ajouter la nouvelle valeur car on ne peut pas modifier les clés primaires ni les supprimer
				EOMangueSeqEmploi nouveauCompteur = new EOMangueSeqEmploi();
				nouveauCompteur.setNextval(nouvelleValeur);
				editingContext.insertObject(nouveauCompteur);
				editingContext.deleteObject(compteur);
				return nouvelleValeur;
			} catch (Exception e) {
				LogManager.logException(e);
				return null;
			}
		}
	}
}
