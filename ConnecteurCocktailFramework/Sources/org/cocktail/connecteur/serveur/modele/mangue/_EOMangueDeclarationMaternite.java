// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueDeclarationMaternite.java instead.
package org.cocktail.connecteur.serveur.modele.mangue;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueDeclarationMaternite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueDeclarationMaternite";

	// Attributes
	public static final String D_ACCOUCHEMENT_KEY = "dAccouchement";
	public static final String D_CONSTAT_MATERN_KEY = "dConstatMatern";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAIS_PREV_KEY = "dNaisPrev";
	public static final String NB_ENFANTS_DECL_KEY = "nbEnfantsDecl";
	public static final String TEM_ANNULE_KEY = "temAnnule";
	public static final String TEM_ANNULE_BIS_KEY = "temAnnuleBis";
	public static final String TEM_GROSSESSE_GEMELLAIRE_KEY = "temGrossesseGemellaire";
	public static final String TEM_GROSSESSE_TRIPLE_KEY = "temGrossesseTriple";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueDeclarationMaternite.class);

  public EOMangueDeclarationMaternite localInstanceIn(EOEditingContext editingContext) {
    EOMangueDeclarationMaternite localInstance = (EOMangueDeclarationMaternite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dAccouchement() {
    return (NSTimestamp) storedValueForKey("dAccouchement");
  }

  public void setDAccouchement(NSTimestamp value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating dAccouchement from " + dAccouchement() + " to " + value);
    }
    takeStoredValueForKey(value, "dAccouchement");
  }

  public NSTimestamp dConstatMatern() {
    return (NSTimestamp) storedValueForKey("dConstatMatern");
  }

  public void setDConstatMatern(NSTimestamp value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating dConstatMatern from " + dConstatMatern() + " to " + value);
    }
    takeStoredValueForKey(value, "dConstatMatern");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dNaisPrev() {
    return (NSTimestamp) storedValueForKey("dNaisPrev");
  }

  public void setDNaisPrev(NSTimestamp value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating dNaisPrev from " + dNaisPrev() + " to " + value);
    }
    takeStoredValueForKey(value, "dNaisPrev");
  }

  public Integer nbEnfantsDecl() {
    return (Integer) storedValueForKey("nbEnfantsDecl");
  }

  public void setNbEnfantsDecl(Integer value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating nbEnfantsDecl from " + nbEnfantsDecl() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfantsDecl");
  }

  public String temAnnule() {
    return (String) storedValueForKey("temAnnule");
  }

  public void setTemAnnule(String value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating temAnnule from " + temAnnule() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnule");
  }

  public String temAnnuleBis() {
    return (String) storedValueForKey("temAnnuleBis");
  }

  public void setTemAnnuleBis(String value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating temAnnuleBis from " + temAnnuleBis() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnuleBis");
  }

  public String temGrossesseGemellaire() {
    return (String) storedValueForKey("temGrossesseGemellaire");
  }

  public void setTemGrossesseGemellaire(String value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating temGrossesseGemellaire from " + temGrossesseGemellaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temGrossesseGemellaire");
  }

  public String temGrossesseTriple() {
    return (String) storedValueForKey("temGrossesseTriple");
  }

  public void setTemGrossesseTriple(String value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationMaternite.LOG.debug( "updating temGrossesseTriple from " + temGrossesseTriple() + " to " + value);
    }
    takeStoredValueForKey(value, "temGrossesseTriple");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueDeclarationMaternite.LOG.isDebugEnabled()) {
      _EOMangueDeclarationMaternite.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueDeclarationMaternite createMangueDeclarationMaternite(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temAnnule
, String temGrossesseGemellaire
, String temGrossesseTriple
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueDeclarationMaternite eo = (EOMangueDeclarationMaternite) EOUtilities.createAndInsertInstance(editingContext, _EOMangueDeclarationMaternite.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemAnnule(temAnnule);
		eo.setTemGrossesseGemellaire(temGrossesseGemellaire);
		eo.setTemGrossesseTriple(temGrossesseTriple);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueDeclarationMaternite> fetchAllMangueDeclarationMaternites(EOEditingContext editingContext) {
    return _EOMangueDeclarationMaternite.fetchAllMangueDeclarationMaternites(editingContext, null);
  }

  public static NSArray<EOMangueDeclarationMaternite> fetchAllMangueDeclarationMaternites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueDeclarationMaternite.fetchMangueDeclarationMaternites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueDeclarationMaternite> fetchMangueDeclarationMaternites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueDeclarationMaternite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueDeclarationMaternite> eoObjects = (NSArray<EOMangueDeclarationMaternite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueDeclarationMaternite fetchMangueDeclarationMaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDeclarationMaternite.fetchMangueDeclarationMaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDeclarationMaternite fetchMangueDeclarationMaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueDeclarationMaternite> eoObjects = _EOMangueDeclarationMaternite.fetchMangueDeclarationMaternites(editingContext, qualifier, null);
    EOMangueDeclarationMaternite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueDeclarationMaternite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueDeclarationMaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDeclarationMaternite fetchRequiredMangueDeclarationMaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDeclarationMaternite.fetchRequiredMangueDeclarationMaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDeclarationMaternite fetchRequiredMangueDeclarationMaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueDeclarationMaternite eoObject = _EOMangueDeclarationMaternite.fetchMangueDeclarationMaternite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueDeclarationMaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDeclarationMaternite localInstanceIn(EOEditingContext editingContext, EOMangueDeclarationMaternite eo) {
    EOMangueDeclarationMaternite localInstance = (eo == null) ? null : (EOMangueDeclarationMaternite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
