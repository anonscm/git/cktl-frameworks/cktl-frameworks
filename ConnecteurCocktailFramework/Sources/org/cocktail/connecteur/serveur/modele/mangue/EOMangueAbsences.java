// EOMangueAbsences.java
// Created on Fri Mar 21 11:48:08 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.mangue;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeAbsence;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.MangueCongeLegal;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueAbsences extends _EOMangueAbsences {
	public static String MATIN = "am";
	public static String APRES_MIDI = "pm";
    public EOMangueAbsences() {
        super();
    }

    public void initAvecTypeEtIndividu(String type,Integer noIndividu) {
    	setAbsAmpmDebut(MATIN);
		setAbsAmpmFin(APRES_MIDI);
		setAbsValide(CocktailConstantes.VRAI);
    	setNoIndividu(noIndividu);
    	setAbsTypeCode(type);
    	NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
    }
    public void modifierDates(NSTimestamp dateDebut, NSTimestamp dateFin) {
    	setDModification(new NSTimestamp());
		setAbsDebut(dateDebut);
		setAbsFin(dateFin);
		setAbsDureeTotale("" + (float)DateCtrl.nbJoursEntre(dateDebut,dateFin,true));
    }
    public boolean correspondCongeLegal() {
    	EOTypeAbsence type = (EOTypeAbsence)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "TypeAbsence", "cTypeAbsence", absTypeCode());
    	return type.estCongeLegal();
    }
    /** Retourne le conge legal lie a l'absence et null si il ne s'agit pas d'un conge genere
     * dans le connecteur
     * @param editingContext
     * @param absence
     * @return
     */
    public MangueCongeLegal congePourAbsence(EOEditingContext editingContext) {
		NSMutableArray args = new NSMutableArray(this);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("absence = %@ AND absence.absValide = 'O'",args);
    	EOTypeAbsence type = (EOTypeAbsence)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "TypeAbsence", "cTypeAbsence", absTypeCode());
    	// les entités de Mangue ont leur nom qui commence par Mangue
    	String nomEntite = EOEntity.nameForExternalName("MANGUE_" + type.cTypeAbsenceHarpege(),"_",true);
		if (nomEntite != null) {
			EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,null);
			NSArray results = editingContext.objectsWithFetchSpecification(fs);
			if (results.count() > 0) {
				return (MangueCongeLegal)results.objectAtIndex(0);
			} else {
				// il s'agit d'un ancien congé, rechercher par date et individu
				args = new NSMutableArray(this.absDebut());
				args.addObject(this.absFin());
				args.addObject(this.noIndividu());
				qualifier = EOQualifier.qualifierWithQualifierFormat("dateDebut = %@ AND dateFin = %@ and individu.noIndividu = %@",args);
				fs = new EOFetchSpecification(nomEntite,qualifier,null);
				try {
					return (MangueCongeLegal)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
				} catch (Exception e) {
					return null;
				}
			}
		} else {
			return null;
		}
	
	}
    // Méthodes statiques 
    /** Trouve l'absence d'un certain type pour un individu avec les dates identiques a celles passees en param&egrave;tre
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param dateDebut 
	 * @param dateFin peut &ecirc;tre nulle. On recup&egrave;re alors la premi&egrave;re absence trouvee
	 * @return liste des absences trouvees
	 */
	public static EOMangueAbsences rechercherAbsencePourIndividuDateEtTypeAbsence(EOEditingContext editingContext,Number noIndividu,NSTimestamp dateDebut,NSTimestamp dateFin,String cTypeAbsence) {
		if (cTypeAbsence == null || noIndividu == null || dateDebut == null || dateFin == null) {
			return null;
		}
		NSArray mySort = new NSArray(EOSortOrdering.sortOrderingWithKey("absDebut", EOSortOrdering.CompareDescending));
		NSMutableArray args = new NSMutableArray(noIndividu);
		args.addObject(cTypeAbsence);
		args.addObject(dateDebut);
		String stringQualifier = "absValide = 'O' AND noIndividu = %@ AND absTypeCode = %@ AND absDebut = %@";
		if (dateFin != null) {
			args.addObject(dateFin);
			stringQualifier = stringQualifier + " AND absFin = %@";
		}
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification myFetch = new EOFetchSpecification ("MangueAbsences", myQualifier,mySort);
		myFetch.setRefreshesRefetchedObjects(true); 
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			return (EOMangueAbsences)result.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Recherche toutes les absences valides de l'individu sur la periode. Retourne toutes les absences valides si la periode est nulle.
	 * Les absences sont triees par ordre de date debut croissante.
	 * @param noIndividu (si nul, retourne nul)
	 * @param dateDebut debut periode, peut &ecirc;tre nulle
	 * @param dateFin fin periode, peut &ecirc;tre nulle */
	public static NSArray rechercherAbsencesPourIndividuEtPeriode(EOEditingContext editingContext,Number noIndividu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (noIndividu == null) {
			return null;
		}
		NSArray mySort = new NSArray(EOSortOrdering.sortOrderingWithKey("absDebut", EOSortOrdering.CompareDescending));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("absValide = 'O' AND noIndividu = %@",new NSArray(noIndividu));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifier = Finder.qualifierPourPeriode("absDebut", dateDebut, "absFin", dateFin);
		if (qualifier != null) {
			qualifiers.addObject(qualifier); 
		}
		EOFetchSpecification myFetch = new EOFetchSpecification ("MangueAbsences", new EOAndQualifier(qualifiers),mySort);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** Recherche toutes les absences valides de l'individu posterieures a la date.
	 * @param noIndividu (si nul, retourne nul)
	 * @param date ne peut &ecirc;tre nulle
	 */
	public static NSArray rechercherAbsencesPosterieuresDate(EOEditingContext editingContext,Number noIndividu,NSTimestamp date) {
		if (noIndividu == null) {
			return null;
		}
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("absValide = 'O' AND noIndividu = %@",new NSArray(noIndividu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("absDebut >= %@",new NSArray(date))); 
	
		EOFetchSpecification myFetch = new EOFetchSpecification ("MangueAbsences", new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** ferme les affectations d'un agent valides a la date passee en param&egrave;tre  */
	public static void fermerAbsencesSaufAbsencesDepart(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp date) {
		NSArray objets = rechercherAbsencesPourIndividuEtPeriode(editingContext,individu.noIndividu(),date,date);
		java.util.Enumeration e = objets.objectEnumerator();
		while (e.hasMoreElements()) {
			// fermer l'absence
			EOMangueAbsences absence = (EOMangueAbsences)e.nextElement();
			if (absence.absTypeCode().equals(EOMangueDepart.TYPE_ABSENCE_DEPART) == false) {
				absence.setAbsFin(date);
			}
		}
		// Invalide toutes les absences sauf celle concernant le départ en cours de traitement (il est unique car les chevauchements ne sont pas permis)
		objets = rechercherAbsencesPosterieuresDate(editingContext,individu.noIndividu(),date);
		e = objets.objectEnumerator();
		while (e.hasMoreElements()) {
			EOMangueAbsences absence = (EOMangueAbsences)e.nextElement();
			if (absence.absTypeCode().equals(EOMangueDepart.TYPE_ABSENCE_DEPART) == false) {
				absence.setAbsValide(CocktailConstantes.FAUX);
			}
		}
	}
}
