// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueSeqEmploi.java instead.
package org.cocktail.connecteur.serveur.modele.mangue;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueSeqEmploi extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueSeqEmploi";

	// Attributes
	public static final String NEXTVAL_KEY = "nextval";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMangueSeqEmploi.class);

  public EOMangueSeqEmploi localInstanceIn(EOEditingContext editingContext) {
    EOMangueSeqEmploi localInstance = (EOMangueSeqEmploi)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer nextval() {
    return (Integer) storedValueForKey("nextval");
  }

  public void setNextval(Integer value) {
    if (_EOMangueSeqEmploi.LOG.isDebugEnabled()) {
    	_EOMangueSeqEmploi.LOG.debug( "updating nextval from " + nextval() + " to " + value);
    }
    takeStoredValueForKey(value, "nextval");
  }


  public static EOMangueSeqEmploi createMangueSeqEmploi(EOEditingContext editingContext, Integer nextval
) {
    EOMangueSeqEmploi eo = (EOMangueSeqEmploi) EOUtilities.createAndInsertInstance(editingContext, _EOMangueSeqEmploi.ENTITY_NAME);    
		eo.setNextval(nextval);
    return eo;
  }

  public static NSArray<EOMangueSeqEmploi> fetchAllMangueSeqEmplois(EOEditingContext editingContext) {
    return _EOMangueSeqEmploi.fetchAllMangueSeqEmplois(editingContext, null);
  }

  public static NSArray<EOMangueSeqEmploi> fetchAllMangueSeqEmplois(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueSeqEmploi.fetchMangueSeqEmplois(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueSeqEmploi> fetchMangueSeqEmplois(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueSeqEmploi.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueSeqEmploi> eoObjects = (NSArray<EOMangueSeqEmploi>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueSeqEmploi fetchMangueSeqEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueSeqEmploi.fetchMangueSeqEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueSeqEmploi fetchMangueSeqEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueSeqEmploi> eoObjects = _EOMangueSeqEmploi.fetchMangueSeqEmplois(editingContext, qualifier, null);
    EOMangueSeqEmploi eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueSeqEmploi)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueSeqEmploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueSeqEmploi fetchRequiredMangueSeqEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueSeqEmploi.fetchRequiredMangueSeqEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueSeqEmploi fetchRequiredMangueSeqEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueSeqEmploi eoObject = _EOMangueSeqEmploi.fetchMangueSeqEmploi(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueSeqEmploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueSeqEmploi localInstanceIn(EOEditingContext editingContext, EOMangueSeqEmploi eo) {
    EOMangueSeqEmploi localInstance = (eo == null) ? null : (EOMangueSeqEmploi)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
