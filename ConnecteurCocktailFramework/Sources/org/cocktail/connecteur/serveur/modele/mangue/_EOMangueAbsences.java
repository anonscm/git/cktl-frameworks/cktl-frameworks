// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueAbsences.java instead.
package org.cocktail.connecteur.serveur.modele.mangue;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueAbsences extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueAbsences";

	// Attributes
	public static final String ABS_AMPM_DEBUT_KEY = "absAmpmDebut";
	public static final String ABS_AMPM_FIN_KEY = "absAmpmFin";
	public static final String ABS_DEBUT_KEY = "absDebut";
	public static final String ABS_DUREE_TOTALE_KEY = "absDureeTotale";
	public static final String ABS_FIN_KEY = "absFin";
	public static final String ABS_MOTIF_KEY = "absMotif";
	public static final String ABS_TYPE_CODE_KEY = "absTypeCode";
	public static final String ABS_VALIDE_KEY = "absValide";
	public static final String C_TYPE_EXCLUSION_KEY = "cTypeExclusion";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMangueAbsences.class);

  public EOMangueAbsences localInstanceIn(EOEditingContext editingContext) {
    EOMangueAbsences localInstance = (EOMangueAbsences)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String absAmpmDebut() {
    return (String) storedValueForKey("absAmpmDebut");
  }

  public void setAbsAmpmDebut(String value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absAmpmDebut from " + absAmpmDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "absAmpmDebut");
  }

  public String absAmpmFin() {
    return (String) storedValueForKey("absAmpmFin");
  }

  public void setAbsAmpmFin(String value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absAmpmFin from " + absAmpmFin() + " to " + value);
    }
    takeStoredValueForKey(value, "absAmpmFin");
  }

  public NSTimestamp absDebut() {
    return (NSTimestamp) storedValueForKey("absDebut");
  }

  public void setAbsDebut(NSTimestamp value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absDebut from " + absDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "absDebut");
  }

  public String absDureeTotale() {
    return (String) storedValueForKey("absDureeTotale");
  }

  public void setAbsDureeTotale(String value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absDureeTotale from " + absDureeTotale() + " to " + value);
    }
    takeStoredValueForKey(value, "absDureeTotale");
  }

  public NSTimestamp absFin() {
    return (NSTimestamp) storedValueForKey("absFin");
  }

  public void setAbsFin(NSTimestamp value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absFin from " + absFin() + " to " + value);
    }
    takeStoredValueForKey(value, "absFin");
  }

  public String absMotif() {
    return (String) storedValueForKey("absMotif");
  }

  public void setAbsMotif(String value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absMotif from " + absMotif() + " to " + value);
    }
    takeStoredValueForKey(value, "absMotif");
  }

  public String absTypeCode() {
    return (String) storedValueForKey("absTypeCode");
  }

  public void setAbsTypeCode(String value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absTypeCode from " + absTypeCode() + " to " + value);
    }
    takeStoredValueForKey(value, "absTypeCode");
  }

  public String absValide() {
    return (String) storedValueForKey("absValide");
  }

  public void setAbsValide(String value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating absValide from " + absValide() + " to " + value);
    }
    takeStoredValueForKey(value, "absValide");
  }

  public String cTypeExclusion() {
    return (String) storedValueForKey("cTypeExclusion");
  }

  public void setCTypeExclusion(String value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating cTypeExclusion from " + cTypeExclusion() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeExclusion");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey("noIndividu");
  }

  public void setNoIndividu(Integer value) {
    if (_EOMangueAbsences.LOG.isDebugEnabled()) {
    	_EOMangueAbsences.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividu");
  }


  public static EOMangueAbsences createMangueAbsences(EOEditingContext editingContext, String absValide
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noIndividu
) {
    EOMangueAbsences eo = (EOMangueAbsences) EOUtilities.createAndInsertInstance(editingContext, _EOMangueAbsences.ENTITY_NAME);    
		eo.setAbsValide(absValide);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoIndividu(noIndividu);
    return eo;
  }

  public static NSArray<EOMangueAbsences> fetchAllMangueAbsenceses(EOEditingContext editingContext) {
    return _EOMangueAbsences.fetchAllMangueAbsenceses(editingContext, null);
  }

  public static NSArray<EOMangueAbsences> fetchAllMangueAbsenceses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueAbsences.fetchMangueAbsenceses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueAbsences> fetchMangueAbsenceses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueAbsences.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueAbsences> eoObjects = (NSArray<EOMangueAbsences>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueAbsences fetchMangueAbsences(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueAbsences.fetchMangueAbsences(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueAbsences fetchMangueAbsences(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueAbsences> eoObjects = _EOMangueAbsences.fetchMangueAbsenceses(editingContext, qualifier, null);
    EOMangueAbsences eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueAbsences)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueAbsences that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueAbsences fetchRequiredMangueAbsences(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueAbsences.fetchRequiredMangueAbsences(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueAbsences fetchRequiredMangueAbsences(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueAbsences eoObject = _EOMangueAbsences.fetchMangueAbsences(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueAbsences that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueAbsences localInstanceIn(EOEditingContext editingContext, EOMangueAbsences eo) {
    EOMangueAbsences localInstance = (eo == null) ? null : (EOMangueAbsences)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
