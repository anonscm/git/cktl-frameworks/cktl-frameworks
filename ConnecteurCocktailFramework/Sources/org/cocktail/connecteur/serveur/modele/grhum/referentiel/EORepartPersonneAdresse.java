// EORepartPersonneAdresse.java
// Created on Wed Jul 25 15:14:02 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EORepartPersonneAdresse extends _EORepartPersonneAdresse {
	public final static String TYPE_FACTURATION = "FACT";
	public final static String TYPE_PERSO = "PERSO";
    public EORepartPersonneAdresse() {
        super();
    }


    public void init() {
    	NSTimestamp today = new NSTimestamp();
    	setDCreation(today);
		setDModification(today);
		setRpaValide(CocktailConstantes.VRAI);
    }
    public boolean estValide() {
    	return rpaValide() != null && rpaValide().equals(CocktailConstantes.VRAI);
    }
    public void setEstValide(boolean aBool) {
    	if (aBool) {
    		setRpaValide(CocktailConstantes.VRAI);
    	} else {
    		setRpaValide(CocktailConstantes.FAUX);
    	}
    }
    // Méthodes statiques
    /** recherche les adresses de type typeAdresse pour le persID passe en param&egrave;tre
     * @return tableaux d'adresses (EOGrhumAdresse) ou null si structure ou typeAdresse est nul */
    public static NSArray rechercherAdressesDeType(EOEditingContext editingContext,Number persID,String typeAdresse) {
		return (NSArray)rechercherRepartsDeType(editingContext,persID,typeAdresse).valueForKey("adresse");
    }
    /** Recherche les reparts d'adresse de type typeAdresse pour le persID passe en param&egrave;tre */
    public static NSArray rechercherRepartsDeType(EOEditingContext editingContext,Number persID,String typeAdresse) {
    	return rechercherRepartsDeType(editingContext, persID, null,typeAdresse);
    }  
    /** Recherche les reparts d'adresse de type typeAdresse pour l'adresse et le persID passe en param&egrave;tre */
    public static NSArray rechercherRepartsDeType(EOEditingContext editingContext,Number persID,EOGrhumAdresse adresse, String typeAdresse) {
    	NSMutableArray args = new NSMutableArray(persID);
		args.addObject(typeAdresse);
		String stringQualifier = "persId = %@ AND tadrCode = %@";
		if (adresse != null) {
			args.addObject(adresse);
			stringQualifier = stringQualifier + " AND adresse = %@";
		}
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification fs = new EOFetchSpecification("RepartPersonneAdresse",myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
    }  
    /** Recherche les adresses valides de type typeAdresse pour le persID passe en param&egrave;tre */
    public static NSArray rechercherAdressesValidesDeType(EOEditingContext editingContext,Number persID,String typeAdresse) {
    	NSMutableArray args = new NSMutableArray(persID);
		args.addObject(typeAdresse);
		String stringQualifier = "persId = %@ AND tadrCode = %@ AND rpaValide = 'O'";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification fs = new EOFetchSpecification("RepartPersonneAdresse",myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return (NSArray)editingContext.objectsWithFetchSpecification(fs).valueForKey("adresse");
    }  
    public static NSArray rechercherRpaPrincipalesEtValides(EOEditingContext editingContext,Number persID) {
    	NSMutableArray args = new NSMutableArray(persID);
		String stringQualifier = "persId = %@ AND rpaPrincipal = 'O' AND rpaValide = 'O'";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification fs = new EOFetchSpecification("RepartPersonneAdresse",myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
    }
}
