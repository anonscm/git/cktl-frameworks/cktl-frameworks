// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartPersonneAdresse.java instead.
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartPersonneAdresse extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RepartPersonneAdresse";

	// Attributes
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String E_MAIL_KEY = "eMail";
	public static final String PERS_ID_KEY = "persId";
	public static final String RPA_PRINCIPAL_KEY = "rpaPrincipal";
	public static final String RPA_VALIDE_KEY = "rpaValide";
	public static final String TADR_CODE_KEY = "tadrCode";

	// Relationships
	public static final String ADRESSE_KEY = "adresse";

  private static Logger LOG = Logger.getLogger(_EORepartPersonneAdresse.class);

  public EORepartPersonneAdresse localInstanceIn(EOEditingContext editingContext) {
    EORepartPersonneAdresse localInstance = (EORepartPersonneAdresse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey("adrOrdre");
  }

  public void setAdrOrdre(Integer value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating adrOrdre from " + adrOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "adrOrdre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String eMail() {
    return (String) storedValueForKey("eMail");
  }

  public void setEMail(String value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating eMail from " + eMail() + " to " + value);
    }
    takeStoredValueForKey(value, "eMail");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public String rpaPrincipal() {
    return (String) storedValueForKey("rpaPrincipal");
  }

  public void setRpaPrincipal(String value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating rpaPrincipal from " + rpaPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "rpaPrincipal");
  }

  public String rpaValide() {
    return (String) storedValueForKey("rpaValide");
  }

  public void setRpaValide(String value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating rpaValide from " + rpaValide() + " to " + value);
    }
    takeStoredValueForKey(value, "rpaValide");
  }

  public String tadrCode() {
    return (String) storedValueForKey("tadrCode");
  }

  public void setTadrCode(String value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
    	_EORepartPersonneAdresse.LOG.debug( "updating tadrCode from " + tadrCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tadrCode");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse adresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse)storedValueForKey("adresse");
  }

  public void setAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse value) {
    if (_EORepartPersonneAdresse.LOG.isDebugEnabled()) {
      _EORepartPersonneAdresse.LOG.debug("updating adresse from " + adresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "adresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "adresse");
    }
  }
  

  public static EORepartPersonneAdresse createRepartPersonneAdresse(EOEditingContext editingContext, Integer adrOrdre
, Integer persId
, String rpaPrincipal
, String tadrCode
) {
    EORepartPersonneAdresse eo = (EORepartPersonneAdresse) EOUtilities.createAndInsertInstance(editingContext, _EORepartPersonneAdresse.ENTITY_NAME);    
		eo.setAdrOrdre(adrOrdre);
		eo.setPersId(persId);
		eo.setRpaPrincipal(rpaPrincipal);
		eo.setTadrCode(tadrCode);
    return eo;
  }

  public static NSArray<EORepartPersonneAdresse> fetchAllRepartPersonneAdresses(EOEditingContext editingContext) {
    return _EORepartPersonneAdresse.fetchAllRepartPersonneAdresses(editingContext, null);
  }

  public static NSArray<EORepartPersonneAdresse> fetchAllRepartPersonneAdresses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartPersonneAdresse.fetchRepartPersonneAdresses(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartPersonneAdresse> fetchRepartPersonneAdresses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartPersonneAdresse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartPersonneAdresse> eoObjects = (NSArray<EORepartPersonneAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartPersonneAdresse fetchRepartPersonneAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartPersonneAdresse.fetchRepartPersonneAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartPersonneAdresse fetchRepartPersonneAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartPersonneAdresse> eoObjects = _EORepartPersonneAdresse.fetchRepartPersonneAdresses(editingContext, qualifier, null);
    EORepartPersonneAdresse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartPersonneAdresse)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RepartPersonneAdresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartPersonneAdresse fetchRequiredRepartPersonneAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartPersonneAdresse.fetchRequiredRepartPersonneAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartPersonneAdresse fetchRequiredRepartPersonneAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartPersonneAdresse eoObject = _EORepartPersonneAdresse.fetchRepartPersonneAdresse(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RepartPersonneAdresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartPersonneAdresse localInstanceIn(EOEditingContext editingContext, EORepartPersonneAdresse eo) {
    EORepartPersonneAdresse localInstance = (eo == null) ? null : (EORepartPersonneAdresse)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
