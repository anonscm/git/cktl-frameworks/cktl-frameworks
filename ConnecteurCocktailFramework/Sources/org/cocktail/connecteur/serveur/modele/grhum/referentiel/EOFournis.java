//EOFournis.java
//Created on Wed Jul 25 15:08:40 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOFournis extends _EOFournis {

	public EOFournis() {
		super();
	}

	// Méthodes ajoutées
	/** Initialisation d'un fournisseur */
	public void initFournis(EOGrhumAdresse adresse, Number persId, String nom) {
		setFouValide(CocktailConstantes.VRAI);
		setFouMarche("0");
		setFouType("F");
		setPersId(persId.intValue());	
		addObjectToBothSidesOfRelationshipWithKey(adresse, "adresse");		
		setFouCode(fouCodePourNom(editingContext(), nom));
		NSTimestamp today = new NSTimestamp();
		setFouDate(today);
		setDCreation(today);
		setDModification(today);
		setFouEtranger("N");
		//	le fournisseur valide est déjà créé (propagate primary key)
		EOValideFournis valideFournis = valideFournis();
		// responsable de la création du fournisseur
		valideFournis.init(this, AutomateImport.sharedInstance().responsableImport());
		editingContext().insertObject(valideFournis);
	}
	// Méthodes statiques
	/** retourne le fournisseur avec le persId passe en param&egrave;tre */
	public static EOFournis rechercherFournisAvecId(EOEditingContext editingContext,Number persId) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("persId = %@ AND fouValide = 'O'",new NSArray(persId));
		EOFetchSpecification myFetch = new EOFetchSpecification ("Fournis", myQualifier,null);
		try {
			return (EOFournis)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param nom
	 * @return
	 */
	public static String fouCodePourNom(EOEditingContext editingContext, String nom){
		String retour = StringCtrl.chaineClaire(nom, true).toUpperCase();
		retour = retour.replaceAll(" ", "");
		retour = retour.replaceAll("-", "");
		// On garde les 3 premiers caracteres du nom
		if (retour.length() >= 3) {
			retour = retour.substring(0,3);
		} else {
			retour = StringCtrl.stringCompletion(retour, 3,"0","D");
		}
		// Récupérer tous les codes fournisseurs correspondant à ces 3 caracteres de début
		NSArray mySort = new NSArray(new EOSortOrdering("fouCode",EOSortOrdering.CompareDescending));
		EOQualifier qual  = EOQualifier.qualifierWithQualifierFormat("fouCode like %@",new NSArray(retour+"*"));
		EOFetchSpecification fs = new EOFetchSpecification("Fournis", qual,mySort);
		fs.setRefreshesRefetchedObjects(true);
		NSArray<EOFournis> fournis = editingContext.objectsWithFetchSpecification(fs);

		// Construction du numero sur 5 caracteres.
		int numero = 0;
		for (int i = 0;i < fournis.count(); i++)	{
			String code = ((EOFournis)fournis.objectAtIndex(i)).fouCode();
			int currentNumero = new Integer(code.substring(3,code.length())).intValue();
			if (currentNumero > numero) {
				numero = currentNumero;
			}
		}
		String chaine = StringCtrl.stringCompletion(String.valueOf(++numero), 5, "0", "G");
		return (retour + chaine);
	}
}
