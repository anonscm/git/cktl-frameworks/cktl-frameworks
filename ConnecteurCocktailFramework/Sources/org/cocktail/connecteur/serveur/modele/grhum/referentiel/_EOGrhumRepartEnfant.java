// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumRepartEnfant.java instead.
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumRepartEnfant extends  EOGenericRecord {
	public static final String ENTITY_NAME = "GrhumRepartEnfant";

	// Attributes
	public static final String A_CHARGE_KEY = "aCharge";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String TEM_SFT_KEY = "temSft";

	// Relationships
	public static final String ENFANT_KEY = "enfant";
	public static final String LIEN_FILIATION_ENFANT_KEY = "lienFiliationEnfant";
	public static final String PARENT_KEY = "parent";

  private static Logger LOG = Logger.getLogger(_EOGrhumRepartEnfant.class);

  public EOGrhumRepartEnfant localInstanceIn(EOEditingContext editingContext) {
    EOGrhumRepartEnfant localInstance = (EOGrhumRepartEnfant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String aCharge() {
    return (String) storedValueForKey("aCharge");
  }

  public void setACharge(String value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumRepartEnfant.LOG.debug( "updating aCharge from " + aCharge() + " to " + value);
    }
    takeStoredValueForKey(value, "aCharge");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumRepartEnfant.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumRepartEnfant.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumRepartEnfant.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public String temSft() {
    return (String) storedValueForKey("temSft");
  }

  public void setTemSft(String value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumRepartEnfant.LOG.debug( "updating temSft from " + temSft() + " to " + value);
    }
    takeStoredValueForKey(value, "temSft");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant enfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("enfant");
  }

  public void setEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
      _EOGrhumRepartEnfant.LOG.debug("updating enfant from " + enfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfant");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOLienFiliationEnfant lienFiliationEnfant() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOLienFiliationEnfant)storedValueForKey("lienFiliationEnfant");
  }

  public void setLienFiliationEnfantRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOLienFiliationEnfant value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
      _EOGrhumRepartEnfant.LOG.debug("updating lienFiliationEnfant from " + lienFiliationEnfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOLienFiliationEnfant oldValue = lienFiliationEnfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "lienFiliationEnfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "lienFiliationEnfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu parent() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("parent");
  }

  public void setParentRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOGrhumRepartEnfant.LOG.isDebugEnabled()) {
      _EOGrhumRepartEnfant.LOG.debug("updating parent from " + parent() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = parent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "parent");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "parent");
    }
  }
  

  public static EOGrhumRepartEnfant createGrhumRepartEnfant(EOEditingContext editingContext, String aCharge
, Integer noDossierPers
, String temSft
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant enfant, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu parent) {
    EOGrhumRepartEnfant eo = (EOGrhumRepartEnfant) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumRepartEnfant.ENTITY_NAME);    
		eo.setACharge(aCharge);
		eo.setNoDossierPers(noDossierPers);
		eo.setTemSft(temSft);
    eo.setEnfantRelationship(enfant);
    eo.setParentRelationship(parent);
    return eo;
  }

  public static NSArray<EOGrhumRepartEnfant> fetchAllGrhumRepartEnfants(EOEditingContext editingContext) {
    return _EOGrhumRepartEnfant.fetchAllGrhumRepartEnfants(editingContext, null);
  }

  public static NSArray<EOGrhumRepartEnfant> fetchAllGrhumRepartEnfants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumRepartEnfant.fetchGrhumRepartEnfants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumRepartEnfant> fetchGrhumRepartEnfants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumRepartEnfant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumRepartEnfant> eoObjects = (NSArray<EOGrhumRepartEnfant>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumRepartEnfant fetchGrhumRepartEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumRepartEnfant.fetchGrhumRepartEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumRepartEnfant fetchGrhumRepartEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumRepartEnfant> eoObjects = _EOGrhumRepartEnfant.fetchGrhumRepartEnfants(editingContext, qualifier, null);
    EOGrhumRepartEnfant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumRepartEnfant)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumRepartEnfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumRepartEnfant fetchRequiredGrhumRepartEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumRepartEnfant.fetchRequiredGrhumRepartEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumRepartEnfant fetchRequiredGrhumRepartEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumRepartEnfant eoObject = _EOGrhumRepartEnfant.fetchGrhumRepartEnfant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumRepartEnfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumRepartEnfant localInstanceIn(EOEditingContext editingContext, EOGrhumRepartEnfant eo) {
    EOGrhumRepartEnfant localInstance = (eo == null) ? null : (EOGrhumRepartEnfant)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
