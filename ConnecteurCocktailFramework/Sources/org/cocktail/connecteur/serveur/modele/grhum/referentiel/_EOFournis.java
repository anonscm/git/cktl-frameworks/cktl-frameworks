// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOFournis.java instead.
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOFournis extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Fournis";

	// Attributes
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_DATE_KEY = "fouDate";
	public static final String FOU_ETRANGER_KEY = "fouEtranger";
	public static final String FOU_MARCHE_KEY = "fouMarche";
	public static final String FOU_TYPE_KEY = "fouType";
	public static final String FOU_VALIDE_KEY = "fouValide";
	public static final String PERS_ID_KEY = "persId";

	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String VALIDE_FOURNIS_KEY = "valideFournis";

  private static Logger LOG = Logger.getLogger(_EOFournis.class);

  public EOFournis localInstanceIn(EOEditingContext editingContext) {
    EOFournis localInstance = (EOFournis)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cptOrdre() {
    return (Integer) storedValueForKey("cptOrdre");
  }

  public void setCptOrdre(Integer value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating cptOrdre from " + cptOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "cptOrdre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String fouCode() {
    return (String) storedValueForKey("fouCode");
  }

  public void setFouCode(String value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating fouCode from " + fouCode() + " to " + value);
    }
    takeStoredValueForKey(value, "fouCode");
  }

  public NSTimestamp fouDate() {
    return (NSTimestamp) storedValueForKey("fouDate");
  }

  public void setFouDate(NSTimestamp value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating fouDate from " + fouDate() + " to " + value);
    }
    takeStoredValueForKey(value, "fouDate");
  }

  public String fouEtranger() {
    return (String) storedValueForKey("fouEtranger");
  }

  public void setFouEtranger(String value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating fouEtranger from " + fouEtranger() + " to " + value);
    }
    takeStoredValueForKey(value, "fouEtranger");
  }

  public String fouMarche() {
    return (String) storedValueForKey("fouMarche");
  }

  public void setFouMarche(String value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating fouMarche from " + fouMarche() + " to " + value);
    }
    takeStoredValueForKey(value, "fouMarche");
  }

  public String fouType() {
    return (String) storedValueForKey("fouType");
  }

  public void setFouType(String value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating fouType from " + fouType() + " to " + value);
    }
    takeStoredValueForKey(value, "fouType");
  }

  public String fouValide() {
    return (String) storedValueForKey("fouValide");
  }

  public void setFouValide(String value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating fouValide from " + fouValide() + " to " + value);
    }
    takeStoredValueForKey(value, "fouValide");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
    	_EOFournis.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse adresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse)storedValueForKey("adresse");
  }

  public void setAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
      _EOFournis.LOG.debug("updating adresse from " + adresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "adresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "adresse");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOValideFournis valideFournis() {
    return (org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOValideFournis)storedValueForKey("valideFournis");
  }

  public void setValideFournisRelationship(org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOValideFournis value) {
    if (_EOFournis.LOG.isDebugEnabled()) {
      _EOFournis.LOG.debug("updating valideFournis from " + valideFournis() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOValideFournis oldValue = valideFournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "valideFournis");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "valideFournis");
    }
  }
  

  public static EOFournis createFournis(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String fouCode
, String fouMarche
, Integer persId
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse adresse) {
    EOFournis eo = (EOFournis) EOUtilities.createAndInsertInstance(editingContext, _EOFournis.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setFouCode(fouCode);
		eo.setFouMarche(fouMarche);
		eo.setPersId(persId);
    eo.setAdresseRelationship(adresse);
    return eo;
  }

  public static NSArray<EOFournis> fetchAllFournises(EOEditingContext editingContext) {
    return _EOFournis.fetchAllFournises(editingContext, null);
  }

  public static NSArray<EOFournis> fetchAllFournises(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFournis.fetchFournises(editingContext, null, sortOrderings);
  }

  public static NSArray<EOFournis> fetchFournises(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFournis.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFournis> eoObjects = (NSArray<EOFournis>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFournis fetchFournis(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFournis.fetchFournis(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFournis fetchFournis(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFournis> eoObjects = _EOFournis.fetchFournises(editingContext, qualifier, null);
    EOFournis eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFournis)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fournis that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFournis fetchRequiredFournis(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFournis.fetchRequiredFournis(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFournis fetchRequiredFournis(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFournis eoObject = _EOFournis.fetchFournis(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fournis that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFournis localInstanceIn(EOEditingContext editingContext, EOFournis eo) {
    EOFournis localInstance = (eo == null) ? null : (EOFournis)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
