// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOValideFournis.java instead.
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOValideFournis extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ValideFournis";

	// Attributes
	public static final String VAL_CREATION_KEY = "valCreation";
	public static final String VAL_DATE_CREATE_KEY = "valDateCreate";
	public static final String VAL_DATE_VAL_KEY = "valDateVal";
	public static final String VAL_VALIDATION_KEY = "valValidation";

	// Relationships
	public static final String FOURNIS_KEY = "fournis";

  private static Logger LOG = Logger.getLogger(_EOValideFournis.class);

  public EOValideFournis localInstanceIn(EOEditingContext editingContext) {
    EOValideFournis localInstance = (EOValideFournis)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer valCreation() {
    return (Integer) storedValueForKey("valCreation");
  }

  public void setValCreation(Integer value) {
    if (_EOValideFournis.LOG.isDebugEnabled()) {
    	_EOValideFournis.LOG.debug( "updating valCreation from " + valCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "valCreation");
  }

  public NSTimestamp valDateCreate() {
    return (NSTimestamp) storedValueForKey("valDateCreate");
  }

  public void setValDateCreate(NSTimestamp value) {
    if (_EOValideFournis.LOG.isDebugEnabled()) {
    	_EOValideFournis.LOG.debug( "updating valDateCreate from " + valDateCreate() + " to " + value);
    }
    takeStoredValueForKey(value, "valDateCreate");
  }

  public NSTimestamp valDateVal() {
    return (NSTimestamp) storedValueForKey("valDateVal");
  }

  public void setValDateVal(NSTimestamp value) {
    if (_EOValideFournis.LOG.isDebugEnabled()) {
    	_EOValideFournis.LOG.debug( "updating valDateVal from " + valDateVal() + " to " + value);
    }
    takeStoredValueForKey(value, "valDateVal");
  }

  public Integer valValidation() {
    return (Integer) storedValueForKey("valValidation");
  }

  public void setValValidation(Integer value) {
    if (_EOValideFournis.LOG.isDebugEnabled()) {
    	_EOValideFournis.LOG.debug( "updating valValidation from " + valValidation() + " to " + value);
    }
    takeStoredValueForKey(value, "valValidation");
  }

  public org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis fournis() {
    return (org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis)storedValueForKey("fournis");
  }

  public void setFournisRelationship(org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis value) {
    if (_EOValideFournis.LOG.isDebugEnabled()) {
      _EOValideFournis.LOG.debug("updating fournis from " + fournis() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "fournis");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "fournis");
    }
  }
  

  public static EOValideFournis createValideFournis(EOEditingContext editingContext) {
    EOValideFournis eo = (EOValideFournis) EOUtilities.createAndInsertInstance(editingContext, _EOValideFournis.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOValideFournis> fetchAllValideFournises(EOEditingContext editingContext) {
    return _EOValideFournis.fetchAllValideFournises(editingContext, null);
  }

  public static NSArray<EOValideFournis> fetchAllValideFournises(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOValideFournis.fetchValideFournises(editingContext, null, sortOrderings);
  }

  public static NSArray<EOValideFournis> fetchValideFournises(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOValideFournis.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOValideFournis> eoObjects = (NSArray<EOValideFournis>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOValideFournis fetchValideFournis(EOEditingContext editingContext, String keyName, Object value) {
    return _EOValideFournis.fetchValideFournis(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOValideFournis fetchValideFournis(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOValideFournis> eoObjects = _EOValideFournis.fetchValideFournises(editingContext, qualifier, null);
    EOValideFournis eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOValideFournis)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ValideFournis that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOValideFournis fetchRequiredValideFournis(EOEditingContext editingContext, String keyName, Object value) {
    return _EOValideFournis.fetchRequiredValideFournis(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOValideFournis fetchRequiredValideFournis(EOEditingContext editingContext, EOQualifier qualifier) {
    EOValideFournis eoObject = _EOValideFournis.fetchValideFournis(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ValideFournis that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOValideFournis localInstanceIn(EOEditingContext editingContext, EOValideFournis eo) {
    EOValideFournis localInstance = (eo == null) ? null : (EOValideFournis)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
