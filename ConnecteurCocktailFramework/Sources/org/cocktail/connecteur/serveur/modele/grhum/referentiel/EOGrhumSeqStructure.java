// EOGrhumSeqStructure.java
// Created on Thu Aug 09 12:29:58 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrhumParametres;
import org.cocktail.connecteur.serveur.modele.SuperFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EOGrhumSeqStructure extends _EOGrhumSeqStructure {

    public EOGrhumSeqStructure() {
        super();
    }


    /** calcule la clé primaire en fonction de la base */
	public static Number clePrimairePour(EOEditingContext editingContext) {
		String valeur = EOGrhumParametres.parametrePourCle(editingContext,"BASE_DONNEES");
		if (valeur == null || valeur.equals("ORACLE")) {
				return SuperFinder.numeroSequenceOracle(editingContext,"GrhumSeqStructure",true);
		} else {
			// Pour les bases autres qu'Oracle :
			// cherche un enregistrement dans la table ayant le même nom que la table Oracle
			// incrémente le compteur et le stocke dans cette table en utilisant un nouveau record et retourne la valeur
			EOFetchSpecification fs = new EOFetchSpecification("GrhumSeqStructure",null,new NSArray(EOSortOrdering.sortOrderingWithKey("nextval",EOSortOrdering.CompareDescending)));
			fs.setRefreshesRefetchedObjects(true);
			NSArray compteurs = editingContext.objectsWithFetchSpecification(fs);	
			try {
				EOGrhumSeqStructure compteur = (EOGrhumSeqStructure)compteurs.objectAtIndex(0);
				Integer nouvelleValeur = new Integer(compteur.nextval().intValue() + 1);
				// ajouter la nouvelle valeur car on ne peut pas modifier les clés primaires ni les supprimer
				EOGrhumSeqStructure nouveauCompteur = new EOGrhumSeqStructure();
				nouveauCompteur.setNextval(nouvelleValeur);
				editingContext.insertObject(nouveauCompteur);
				editingContext.deleteObject(compteur);
				return nouvelleValeur;
			} catch (Exception e) {
				LogManager.logException(e);
				return null;
			}
		}
	}
}
