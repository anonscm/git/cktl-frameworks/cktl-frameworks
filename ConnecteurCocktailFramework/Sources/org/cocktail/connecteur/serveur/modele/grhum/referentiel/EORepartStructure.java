// EORepartStructure.java
// Created on Wed Jul 25 15:14:14 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EORepartStructure extends _EORepartStructure {

    public EORepartStructure() {
        super();
    }

	public void init(EOGrhumIndividu individu,EOGrhumStructure structure) {
		NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
		setPersId(individu.persId());
		setCStructure(structure.cStructure());
		addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
	}
	/** retourne true si un individu appartient a une structure
	 * @param editingContext
	 * @param individu
	 * @param structure
	 * @return boolean
	 */
	public static boolean estIndividuDansStructure(EOEditingContext editingContext, EOGrhumIndividu individu,EOGrhumStructure structure) {
		NSMutableArray args = new NSMutableArray(individu.persId());
		args.addObject(structure);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("persId = %@ AND structure  = %@",args);
		EOFetchSpecification fs = new EOFetchSpecification("RepartStructure",qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs).count() > 0;
	}
}
