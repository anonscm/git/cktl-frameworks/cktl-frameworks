// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartTypeGroupe.java instead.
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartTypeGroupe extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RepartTypeGroupe";

	// Attributes
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TGRP_CODE_KEY = "tgrpCode";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EORepartTypeGroupe.class);

  public EORepartTypeGroupe localInstanceIn(EOEditingContext editingContext) {
    EORepartTypeGroupe localInstance = (EORepartTypeGroupe)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String tgrpCode() {
    return (String) storedValueForKey("tgrpCode");
  }

  public void setTgrpCode(String value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating tgrpCode from " + tgrpCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tgrpCode");
  }


  public static EORepartTypeGroupe createRepartTypeGroupe(EOEditingContext editingContext, String cStructure
, NSTimestamp dCreation
, NSTimestamp dModification
, String tgrpCode
) {
    EORepartTypeGroupe eo = (EORepartTypeGroupe) EOUtilities.createAndInsertInstance(editingContext, _EORepartTypeGroupe.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTgrpCode(tgrpCode);
    return eo;
  }

  public static NSArray<EORepartTypeGroupe> fetchAllRepartTypeGroupes(EOEditingContext editingContext) {
    return _EORepartTypeGroupe.fetchAllRepartTypeGroupes(editingContext, null);
  }

  public static NSArray<EORepartTypeGroupe> fetchAllRepartTypeGroupes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartTypeGroupe.fetchRepartTypeGroupes(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartTypeGroupe> fetchRepartTypeGroupes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartTypeGroupe.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartTypeGroupe> eoObjects = (NSArray<EORepartTypeGroupe>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartTypeGroupe fetchRepartTypeGroupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartTypeGroupe.fetchRepartTypeGroupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartTypeGroupe fetchRepartTypeGroupe(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartTypeGroupe> eoObjects = _EORepartTypeGroupe.fetchRepartTypeGroupes(editingContext, qualifier, null);
    EORepartTypeGroupe eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartTypeGroupe)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RepartTypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartTypeGroupe fetchRequiredRepartTypeGroupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartTypeGroupe.fetchRequiredRepartTypeGroupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartTypeGroupe fetchRequiredRepartTypeGroupe(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartTypeGroupe eoObject = _EORepartTypeGroupe.fetchRepartTypeGroupe(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RepartTypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartTypeGroupe localInstanceIn(EOEditingContext editingContext, EORepartTypeGroupe eo) {
    EORepartTypeGroupe localInstance = (eo == null) ? null : (EORepartTypeGroupe)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
