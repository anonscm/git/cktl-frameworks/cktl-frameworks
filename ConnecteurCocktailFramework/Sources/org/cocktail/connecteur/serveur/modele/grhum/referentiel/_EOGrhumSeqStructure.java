// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumSeqStructure.java instead.
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumSeqStructure extends  EOGenericRecord {
	public static final String ENTITY_NAME = "GrhumSeqStructure";

	// Attributes
	public static final String NEXTVAL_KEY = "nextval";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOGrhumSeqStructure.class);

  public EOGrhumSeqStructure localInstanceIn(EOEditingContext editingContext) {
    EOGrhumSeqStructure localInstance = (EOGrhumSeqStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer nextval() {
    return (Integer) storedValueForKey("nextval");
  }

  public void setNextval(Integer value) {
    if (_EOGrhumSeqStructure.LOG.isDebugEnabled()) {
    	_EOGrhumSeqStructure.LOG.debug( "updating nextval from " + nextval() + " to " + value);
    }
    takeStoredValueForKey(value, "nextval");
  }


  public static EOGrhumSeqStructure createGrhumSeqStructure(EOEditingContext editingContext, Integer nextval
) {
    EOGrhumSeqStructure eo = (EOGrhumSeqStructure) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumSeqStructure.ENTITY_NAME);    
		eo.setNextval(nextval);
    return eo;
  }

  public static NSArray<EOGrhumSeqStructure> fetchAllGrhumSeqStructures(EOEditingContext editingContext) {
    return _EOGrhumSeqStructure.fetchAllGrhumSeqStructures(editingContext, null);
  }

  public static NSArray<EOGrhumSeqStructure> fetchAllGrhumSeqStructures(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumSeqStructure.fetchGrhumSeqStructures(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumSeqStructure> fetchGrhumSeqStructures(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumSeqStructure.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumSeqStructure> eoObjects = (NSArray<EOGrhumSeqStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumSeqStructure fetchGrhumSeqStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumSeqStructure.fetchGrhumSeqStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumSeqStructure fetchGrhumSeqStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumSeqStructure> eoObjects = _EOGrhumSeqStructure.fetchGrhumSeqStructures(editingContext, qualifier, null);
    EOGrhumSeqStructure eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumSeqStructure)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumSeqStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumSeqStructure fetchRequiredGrhumSeqStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumSeqStructure.fetchRequiredGrhumSeqStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumSeqStructure fetchRequiredGrhumSeqStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumSeqStructure eoObject = _EOGrhumSeqStructure.fetchGrhumSeqStructure(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumSeqStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumSeqStructure localInstanceIn(EOEditingContext editingContext, EOGrhumSeqStructure eo) {
    EOGrhumSeqStructure localInstance = (eo == null) ? null : (EOGrhumSeqStructure)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
