// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartStructure.java instead.
package org.cocktail.connecteur.serveur.modele.grhum.referentiel;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartStructure extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RepartStructure";

	// Attributes
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_KEY = "persId";

	// Relationships
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EORepartStructure.class);

  public EORepartStructure localInstanceIn(EOEditingContext editingContext) {
    EORepartStructure localInstance = (EORepartStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
      _EORepartStructure.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EORepartStructure createRepartStructure(EOEditingContext editingContext, String cStructure
, Integer persId
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure) {
    EORepartStructure eo = (EORepartStructure) EOUtilities.createAndInsertInstance(editingContext, _EORepartStructure.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setPersId(persId);
    eo.setStructureRelationship(structure);
    return eo;
  }

  public static NSArray<EORepartStructure> fetchAllRepartStructures(EOEditingContext editingContext) {
    return _EORepartStructure.fetchAllRepartStructures(editingContext, null);
  }

  public static NSArray<EORepartStructure> fetchAllRepartStructures(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartStructure.fetchRepartStructures(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartStructure> fetchRepartStructures(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartStructure.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartStructure> eoObjects = (NSArray<EORepartStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartStructure fetchRepartStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartStructure.fetchRepartStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartStructure fetchRepartStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartStructure> eoObjects = _EORepartStructure.fetchRepartStructures(editingContext, qualifier, null);
    EORepartStructure eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartStructure)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RepartStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartStructure fetchRequiredRepartStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartStructure.fetchRequiredRepartStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartStructure fetchRequiredRepartStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartStructure eoObject = _EORepartStructure.fetchRepartStructure(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RepartStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartStructure localInstanceIn(EOEditingContext editingContext, EORepartStructure eo) {
    EORepartStructure localInstance = (eo == null) ? null : (EORepartStructure)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
