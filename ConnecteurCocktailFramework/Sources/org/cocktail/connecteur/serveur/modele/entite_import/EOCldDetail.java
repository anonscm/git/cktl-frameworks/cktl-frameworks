/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.InterfaceRecordGenerique;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Modelise un detail de cld. Comporte les r&egrave;gles de validation
 * concernant les dates de debut et de fin qui doivent &ecirc;tre &grave;
 * l'interieur de celles du cld associe
 **/

public class EOCldDetail extends _EOCldDetail implements InterfaceRecordGenerique {
	/**
	 * 
	 */
	public void preparerRelations() {
		super.preparerRelations();
		if (editingContext() != null && cldSource() != null && idSource() != null) {
			EOCld cld = EOCldCorresp.cldPourSourceEtCgId(editingContext(), idSource(), cldSource());
			if (cld != null) {
				setCldRelationship(cld);
			}
		}
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		removeObjectFromBothSidesOfRelationshipWithKey(cld(), CLD_KEY);
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		LogManager.logDetail("Verification du record");
		if (cld() == null && idSource() != null && cldSource() != null) {
			EOCld cld = EOCldCorresp.cldPourSourceEtCgId(editingContext(), idSource(), cldSource());
			if (cld != null) {
				setCldRelationship(cld);
			}
		}
		String messageLog = null;
		if (cld() == null) {
			messageLog = "CLD_NON_IMPORTE";
		} else if (cld().temImport().equals(ObjetImport.A_TRANSFERER) && cld().statut().equals(ObjetImport.STATUT_ERREUR)) {
			messageLog = "CLD_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.setIdSource(new Integer(idSource().intValue()));
			log.setEimpSource(new Integer(eimpSource().intValue()));
			log.setEimp2Source(clddSource());
			log.setIndividuRelationship(individu());
			setStatut(STATUT_ERREUR);
			return log;
		} else {
			return null;
		}
	}

	/** Verification des dates par rapport &grave; celles du conge associe */
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log = super.verifierRecordApresOperation();
		if (log != null)
			return log;

		String message = verifierDatesDetailPourCld(cld());
		if (message != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), message);
			log.setIdSource(new Integer(idSource().intValue()));
			log.setEimpSource(new Integer(eimpSource().intValue()));
			log.setEimp2Source(clddSource());
			if (cld() != null && cld().individu() != null) {
				log.addObjectToBothSidesOfRelationshipWithKey(cld().individu(), "individu");
			}
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	/**
	 * On ne g&egrave;re pas les correspondances avec les details du SI
	 * Destinataire
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	public String nomRelationPourLog() {
		return EOLogImport.CLD_DETAIL_KEY;
	}

	public String nomRelationPourAttributComparaison(String nomAttribut) {
		String resultat = super.nomRelationPourAttributComparaison(nomAttribut);
		if (resultat == null && nomAttribut.equals(CLD_SOURCE_KEY)) {
			resultat = CLD_KEY;
		}
		return resultat;
	}

	// Méthodes privées
	private String verifierDatesDetailPourCld(EOCld cld) {
		if (dateFin() != null && cld.dateFin() != null) {
			if (DateCtrl.isBefore(cld.dateFin(), dateDebut())) {
				return "DATES_DETAIL_CLD";
			} else if (cld.dateFin() != null && DateCtrl.isBefore(cld.dateFin(), dateFin())) {
				return "DATES_DETAIL_CLD";
			}
		}
		if (DateCtrl.isBefore(dateDebut(), cld.dateDebut())) {
			return "DATES_DETAIL_CLD";
		} else if (cld.dateFin() != null && DateCtrl.isBefore(cld.dateFin(), dateDebut())) {
			return "DATES_DETAIL_CLD";
		}
		return null;
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);

		Number taux = (Number) valeursAttributs.objectForKey(NUM_TAUX_KEY);
		if (taux != null && (taux.intValue() < 0 || taux.intValue() > 100)) {
			resultat = resultat + "Num_Taux >>" + System.getProperty("line.separator");
		}
		taux = (Number) valeursAttributs.objectForKey(DEN_TAUX_KEY);
		if (taux != null && (taux.intValue() <= 0 || taux.intValue() > 100)) {
			resultat = resultat + "Den_Taux >>" + System.getProperty("line.separator");

		}

		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return clddSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setClddSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}
}
