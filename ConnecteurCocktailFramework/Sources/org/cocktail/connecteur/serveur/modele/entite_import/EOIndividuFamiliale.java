// EOIndividuFamiliale.java
// Created on Tue Mar 25 15:24:16 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSituationFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 24/06/2010 - changement de la super classe pour éviter l'import si un individu n'est pas personnel
public class EOIndividuFamiliale extends _EOIndividuFamiliale {

    public EOIndividuFamiliale() {
        super();
    }



    // Méthodes ajoutées
    /** pas de verificatation */
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}
	/** IndividuFamiliale du SI destinataire avec le m&ecirc;me individu et commencant a la m&ecirc;me date debut */ 	
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null) {
			if (estNouvelIndividu()) {
				return null;
			} else {
				return EOMangueIndividuFamiliale.individuFamilialeDestinationPourIndividuFamiliale(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}
	public String nomRelationPourLog() {
		return "individuFamiliale";
	}
	// Chaque sous-classe détermine les conditions pour effectuer un fetch pour déterminer le dernier objet importé qui pourrait être identique;
	// On ne teste pas nécessairement tous les champs. Voir la description de cette méthode dans les sous-classes
	// Il faut absolument implémenter cette méthode
	// Méthodes statiques
	/** Verifie les nomenclatures ainsi que les dates */
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number)valeursAttributs.objectForKey("idSource");
		if (idSource == null) {
			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
		}
	
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp)valeursAttributs.objectForKey("dFinSitFamiliale");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp)valeursAttributs.objectForKey("dDebSitFamiliale");
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		// Vérification des nomenclatures
		String valeur = (String)valeursAttributs.objectForKey("cSituationFamille");
		if (valeur != null) {
			EOSituationFamiliale record = EOSituationFamiliale.getFromCode(editingContext, valeur);
			if (record == null) {
				resultat = resultat + "Code_Situation_Inconnu >>" + valeur  + System.getProperty("line.separator");
			}
		}
		return resultat;
	}

}
