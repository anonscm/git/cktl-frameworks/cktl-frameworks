//EOIndividu.java
//Created on Wed Jul 25 15:05:30 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCivilite;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCommune;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EODepartement;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOPays;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSituationFamiliale;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSituationMilitaire;
import org.cocktail.connecteur.importer.moteur.erreurs.ErreurManager;
import org.cocktail.connecteur.serveur.TaskImportAutomatique;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IIndividuImport;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IPersonnelImport;
import org.cocktail.connecteur.serveur.modele.entite_import.services.IndividusImportServicesImpl;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Verifications effectuees sur les individus:<BR>
 * civilite : respect de la nomenclature<BR>
 * * Les verifications ci-dessous ne sont effectuees que si il s'agit d'un personnel<BR>
 * pays de naissance : <BR>
 * si France alors la commune de naissance et le departement de naissance doivent ectre fournis et valides<BR>
 * sinon verification de la nomenclature<BR>
 * pays de nationalite, situation militaire, situation familiale respect de la nomenclature<BR>
 * date de naissance : non posterieure a la date du jour<BR>
 * N°Insee :<BR>
 * Coherence avec la civilite<BR>
 * Coherence de l'annee et du mois avec la date de naissance<BR>
 * Validite des pays de naissance et de nationalite<BR>
 * Coherence du pays avec le pays de naissance si le N° Insee indique naissance a l'etranger<BR>
 * Coherence du departement avec le departement de naissance si le N° Insee indique naissance en France<BR>
 * Validite de la cle du N°Insee<BR>
 * Actions par defaut :<BR>
 * Nom et prenom en majuscules<BR>
 * Prenom d'affichage si il n'est pas defini = Prenom avec 1ere lettre majuscule<BR>
 * Prenom2 avec 1ere lettre majuscule<BR>
 * Pays de naissance force &grave; 100 (France) si le n°Insee indique que la personne est nee en France<BR>
 * Departement de naissance force &grave; la valeur de celui du n°Insee si ce dernier est fourni<BR>
 * 
 * 
 */
public class EOIndividu extends _EOIndividu implements InterfaceHomonymie, InterfaceInvalidationRecordAvecDependance, IIndividuImport {
	public static int LONGUEUR_NO_INSEE = 13;
	private static final String NO_INSEE_PROVISOIRE = "999999999999";
	private static int ANNEE_BASE_POUR_NAISSANCE = 1900;
	private final static String PRISE_EN_COMPTE_NO_INSEE_PROVISOIRE = "P";
	private final static String PRISE_EN_COMPTE_NO_INSEE = "R";

	private static String NOM_PARAM_CHECK_COMMUNE_NAISSANCE = "VERIFIER_INDIVIDU_COMMUNE_NAISSANCE";
	private static boolean PARAM_CHECK_COMMUNE_NAISSANCE_VALEUR_PAR_DEFAUT = true;
	private static boolean checkCommuneNaissance = PARAM_CHECK_COMMUNE_NAISSANCE_VALEUR_PAR_DEFAUT;
	private static boolean checkCommuneNaissanceInitialized = false;

	private static HashMap<Number, EOIndividu> idSourceCache = new HashMap<Number, EOIndividu>();

	private EOPersonnel personnel;
	private IndividusImportServicesImpl service = new IndividusImportServicesImpl();

	public EOIndividu() {
		super();
	}

	public boolean estHomme() {
		return civilite() != null && civilite().equals(CocktailConstantes.MONSIEUR);
	}

	public void preparerPourPersonnel() {
		personnel = new EOPersonnel();
		personnel.init();
	}

	public void awakeFromFetch(EOEditingContext ec) {
		// on récupère le personnel
		try {
			personnel = (EOPersonnel) Finder.rechercherAvecAttributEtValeurEgale(ec, "Personnel", "individu", this).objectAtIndex(0);
		} catch (Exception e) {
			// il ne s'agit pas d'un personnel
		}
	}

	public boolean estPersonnelEtablissement() {
		return temPersonnel().equals(CocktailConstantes.VRAI);
	}

	// Pour gérer les personnels
	public EOPersonnel personnel() {
		return personnel;
	}

	public IPersonnelImport getPersonnel() {
		return personnel();
	}

	protected IndividusImportServicesImpl getService() {
		return service;
	}

	protected void setService(IndividusImportServicesImpl service) {
		this.service = service;
	}

	public String temTitulaire() {
		if (personnel != null) {
			return personnel.temTitulaire();
		} else {
			return null;
		}
	}

	public void setTemTitulaire(String aStr) {
		if (personnel != null) {
			personnel.setTemTitulaire(aStr);
		}
	}

	public NSTimestamp cirDCertification() {
		if (personnel != null) {
			return personnel.cirDCertification();
		} else {
			return null;
		}
	}

	public void setCirDCertification(Date aStr) {
		if (personnel != null) {
			personnel.setCirDCertification(new NSTimestamp(aStr));
		}
	}

	public NSTimestamp cirDCompletude() {
		if (personnel != null) {
			return personnel.cirDCompletude();
		} else {
			return null;
		}
	}

	public void setCirDCompletude(Date aStr) {
		if (personnel != null) {
			personnel.setCirDCompletude(new NSTimestamp(aStr));
		}
	}

	public String numen() {
		if (personnel != null) {
			return personnel.numen();
		} else {
			return null;
		}
	}

	public void setNumen(String aStr) {
		if (personnel != null) {
			personnel.setNumen(aStr);
		}
	}

	public String noMatricule() {
		if (personnel != null) {
			return personnel.noMatricule();
		} else {
			return null;
		}
	}

	public void setNoMatricule(String aStr) {
		if (personnel != null) {
			personnel.setNoMatricule(aStr);
		}
	}

	public String txtLibre() {
		if (personnel != null) {
			return personnel.txtLibre();
		} else {
			return null;
		}
	}

	public void setTxtLibre(String aStr) {
		if (personnel != null) {
			personnel.setTxtLibre(aStr);
		}
	}

	public Number nbEnfants() {
		if (personnel != null) {
			return personnel.nbEnfants();
		} else {
			return null;
		}
	}

	public void setNbEnfants(Number value) {
		if (personnel != null) {
			personnel.setNbEnfants(new Integer(value.intValue()));
		}
	}

	public String cLoge() {
		if (personnel != null) {
			return personnel.cLoge();
		} else {
			return null;
		}
	}

	public void setCLoge(String aStr) {
		if (personnel != null) {
			personnel.setCLoge(aStr);
		}
	}

	public String affecteDefense() {
		if (personnel != null) {
			return personnel.affecteDefense();
		} else {
			return null;
		}
	}

	public void setAffecteDefense(String aStr) {
		if (personnel != null) {
			personnel.setAffecteDefense(aStr);
		}
	}

	// Autres
	/**
	 * Prepare la relation sur le personnel. A appeler apr&egrave;s avoir insere un objet dans l'editing context
	 */
	public void preparerRelations() {
		super.preparerRelations();
		if (personnel != null) {
			personnel.addObjectToBothSidesOfRelationshipWithKey(this, "individu");
		}
	}

	/** Pas de verification sur les relations */
	public EOLogImport verifierRecord() {
		return null; // Pas de vérification supplémentaire
	}

	/**
	 * Determine le statut et l'operation pour le personnel associe en verifiant si un record similaire a deja ete importe => update, existe deja dans le SI =>
	 * il manque une correspondance. Sinon c'est une insertion, verifie si il pourrait y avoir des homonymes dans le SI.<BR>
	 * Verifier si le personnel est OK
	 */
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log = service.verifierRecordApresOperation(this);

		if (EOImportParametres.cacheMemoireNiveauMoyenOuPlus())
			idSourceCache.put(idSource(), this);

		return log;
	}

	/**
	 * retourne un individu du SI Destinataire avec le m&ecirc;me nom, prenom et civilite et la m&ecirc;me date de naissance ou le m&ecirc;me numero insee
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return EOGrhumIndividu.individuDestinationPourIndividu(editingContextTravail(), this, false);
	}

	/** retourne le nom de la relation pour retrouver les logs lies au record */
	public String nomRelationPourLog() {
		return EOLogImport.INDIVIDU_KEY;
	}

	public void preparerPersonnel() {
		try {
			personnel = (EOPersonnel) Finder.rechercherAvecAttributEtValeurEgale(editingContext(), "Personnel", "individu", this).objectAtIndex(0);
		} catch (Exception e) {
			// Si l'individu n'est pas importé comme personnel
		}
	}

	// InterfaceSuppressionRecordsDependants
	/**
	 * Supprime les affectations, les occupations, les carri&grave;res, les contrats, les individuFamiliale, les enfants,les comptes, les telephones, les
	 * adresses, les ribs
	 */
	public String invaliderRecordsDependants() {
		try {
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Carriere", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Contrat", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Affectation", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Occupation", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("IndividuFamiliale", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Enfant", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Compte", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Adresse", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Telephone", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("Rib", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("CongeMaladie", this));
			if (personnel == null) {
				preparerPersonnel();
			}
			// 22/06/2010, le personnel n'existe pas nécessairement
			if (personnel() != null) {
				personnel().invalider();
			}
			editingContext().saveChanges();

			// Invalider aussi tous les objets rattachés à l'individu qui ne
			// seraient pas rattachés à une carrière
			// ou un contrat car carrière ou contrat inconnu
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("ElementCarriere", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("ChangementPosition", this));
			invaliderObjets(ObjetImportPourIndividu.rechercherObjetsImportCourantPourIndividu("ContratAvenant", this));

			return null;
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		}
	}

	// InterfaceHomonymie
	/**
	 * true si il existe un autre individu avec le m&ecirc;me nom, prenom (au sens de l'homonymie Grhum)
	 */
	public boolean aHomonymeDansSIDestinataire() {
		boolean resultat = EOGrhumIndividu.aHomonyme(editingContextTravail(), this);
		return resultat;
	}

	/**
	 * Il s'agit du même individu que si le nom et le prénom sont identiques, ainsi que le numéro insee, numen et date de naissance si renseignés
	 */
	public boolean memesDonneesQueRecord(ObjetImport record) {
		EOIndividu individu = (EOIndividu) record;

		if (individu.nomUsuel() != null && !individu.nomUsuel().equals(nomUsuel()))
			return false;
		if (individu.prenom() != null && !individu.prenom().equals(prenom()))
			return false;

		if (individu.noInsee() != null && noInsee() != null && !individu.noInsee().equals(noInsee()))
			return false;

		if (individu.numen() != null && numen() != null && !individu.numen().equals(numen()))
			return false;

		if (individu.dateNaissance() != null && dateNaissance() != null && !DateCtrl.isSameDay(individu.dateNaissance(), dateNaissance()))
			return false;

		return true;
	}

	/** Pas d'attribut de comparaison avec des relations */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}

	// Méthodes statiques
	/**
	 * Valide les donnees d'un individu.<BR>
	 * Verifie que le code pays de naissance, de departement de naissance (pour la France), de pays de nationalite, la commune, la situation familiale et
	 * militaire, le numero insee et le temoin de titularite sont corrects.<BR>
	 * Transforme noms et prenoms dans une capitalisation correcte.<BR>
	 * On ne verifie pas les champs obligatoires, cela a ete fait auparavant
	 * 
	 * @param editingContext
	 * @param valeursAttributs
	 *            dictionnaire des attributs a valider ou modifier
	 */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		// vérifier la civilité
		String civilite = (String) valeursAttributs.objectForKey("civilite");
		if (civilite != null) {
			EOCivilite civiliteRecord = EOCivilite.getFromCode(editingContext, civilite);
			if (civiliteRecord == null) {
				try {
					int num = Integer.parseInt(civilite);
					civiliteRecord = EOCivilite.getFromCodeOnp(editingContext, num);
					if (civiliteRecord != null) {
						// Transformer en civilité Grhum
						valeursAttributs.setObjectForKey(civiliteRecord.cCivilite(), "civilite");
					}
				} catch (Exception e) {
					civiliteRecord = null;
				}

				if (civiliteRecord == null)
					resultat = "Civilite_Inconnue >>" + civilite + System.getProperty("line.separator");
			}
		}
		// Mettre le nom en majuscules sans caractères accentués
		String nomAccentue = (String) valeursAttributs.objectForKey("nomUsuel");
		String nom = StringCtrl.chaineClaire(nomAccentue, false, true);
		nom = nom.toUpperCase();
		nomAccentue = StringCtrl.capitalizedString(nomAccentue);
		valeursAttributs.setObjectForKey(nom, "nomUsuel");
		// Mettre le prénom en majuscules sans caractères accentués
		String prenomAccentue = (String) valeursAttributs.objectForKey("prenom");
		if (prenomAccentue != null) {
			String prenom = StringCtrl.chaineClaire(prenomAccentue, false);
			valeursAttributs.setObjectForKey(prenom.toUpperCase(), "prenom");
		}
		// Mettre le nom patronymique en majuscules sans accent
		String nomPatronymique = (String) valeursAttributs.objectForKey("nomPatronymique");
		if (nomPatronymique == null) {
			nomPatronymique = StringCtrl.chaineClaire(nomAccentue, false);
			valeursAttributs.setObjectForKey(nomPatronymique.toUpperCase(), "nomPatronymique");
		}
		String nomPatronymiqueAffichage = (String) valeursAttributs.objectForKey("nomPatronymiqueAffichage");
		if (nomPatronymiqueAffichage != null) {
			valeursAttributs.setObjectForKey(StringCtrl.capitalizedString(nomPatronymiqueAffichage), "nomPatronymiqueAffichage");
		}
		// Mettre le prénom2 avec une majuscule en tête
		String prenom2 = (String) valeursAttributs.objectForKey("prenom2");
		if (prenom2 != null) {
			// valeursAttributs.setObjectForKey(StringCtrl.capitalizedString(prenom2),"prenom2");
			// Tout en majuscule : DT 5676
			valeursAttributs.setObjectForKey(prenom2.toUpperCase(), "prenom2");
		}
		// Vérifier la date de naissance
		NSTimestamp dNaissance = (NSTimestamp) valeursAttributs.objectForKey("dateNaissance");
		if (dNaissance != null && DateCtrl.isAfterEq(dNaissance, new NSTimestamp())) {
			resultat = resultat + "Date_Naissance >>" + System.getProperty("line.separator");
		}
		// vérifier la commune de naissance si le pays de naissance est la
		// France
		String paysNaissance = (String) valeursAttributs.objectForKey("paysNaissance");
		if (paysNaissance != null) {
			if (paysNaissance.equals(EOPays.CODE_PAYS_DEFAUT)) {
				if (!checkCommuneNaissanceInitialized) {
					String checkCommune = EOImportParametres.valeurParametrePourCle(editingContext, NOM_PARAM_CHECK_COMMUNE_NAISSANCE);
					if (checkCommune == null) {
						checkCommuneNaissance = PARAM_CHECK_COMMUNE_NAISSANCE_VALEUR_PAR_DEFAUT;
					} else {
						checkCommuneNaissance = checkCommune.equals(CocktailConstantes.VRAI);
					}
					checkCommuneNaissanceInitialized = true;
				}
				if (checkCommuneNaissance) {
					String communeDeNaissance = (String) valeursAttributs.objectForKey("villeNaissance");
					if (communeDeNaissance != null) {
						communeDeNaissance = communeDeNaissance.toUpperCase();
						EOCommune commune = EOCommune.rechercherCommune(editingContext, null, communeDeNaissance);
						if (commune == null) {
							resultat = resultat + "Commune_Inconnue >>" + communeDeNaissance + System.getProperty("line.separator");
						} else {
							valeursAttributs.setObjectForKey(commune.llCom(), "villeNaissance"); // pour
																									// l'avoir
																									// en
																									// majuscules
																									// et
																									// avec
																									// le
																									// libellé
																									// long
						}
					}
				}
				// Vérifier si le département est correct
				String dptNaissance = (String) valeursAttributs.objectForKey("dptNaissance");
				if (dptNaissance != null) {
					if (EODepartement.getFromCode(editingContext, dptNaissance) == null) {
						resultat = resultat + "Departement_Inconnu >>" + dptNaissance + System.getProperty("line.separator");
					}
				}
			} else {
				NSTimestamp date = dNaissance;
				if (date == null) {
					date = new NSTimestamp();
				}
				if (EOPays.estPaysValideADate(editingContext, paysNaissance, date) == false) {
					String message = "à la date de naissance";
					if (dNaissance == null) {
						message = "aujourd'hui";
					}
					resultat = resultat + "Pays_Inconnu >>" + paysNaissance + " : pays de naissance inconnu ou invalide " + message
							+ System.getProperty("line.separator");
				}
				// Vérifier si un département a été saisi et le supprimer
				String dptNaissance = (String) valeursAttributs.objectForKey("dptNaissance");
				if (dptNaissance != null) {
					valeursAttributs.removeObjectForKey("dptNaissance");
				}
			}
		}
		// vérifier si le pays de nationalité existe
		String paysNationalite = (String) valeursAttributs.objectForKey("paysNationalite");
		if (paysNationalite != null && EOPays.estPaysValideADate(editingContext, paysNationalite, new NSTimestamp()) == false) {
			resultat = resultat + "Pays_Inconnu >>" + paysNationalite + " : pays de nationalite inconnu" + System.getProperty("line.separator");
		}
		// Vérifier la situation militaire
		String sitMilitaire = (String) valeursAttributs.objectForKey("sitMilitaire");
		if (sitMilitaire != null && EOSituationMilitaire.getFromCode(editingContext, sitMilitaire) == null) {
			resultat = resultat + "Situation_Militaire_Inconnue >>" + sitMilitaire + System.getProperty("line.separator");
		}
		// Vérifier la situation familiale
		String sitFamiliale = (String) valeursAttributs.objectForKey("sitFamille");
		if (sitFamiliale != null && EOSituationFamiliale.getFromCode(editingContext, sitFamiliale) == null) {
			resultat = resultat + "Situation_Familiale_Inconnue >>" + sitFamiliale + System.getProperty("line.separator");
		}
		// Vérifier temTitulaire
		resultat = resultat + verifierTemoin(valeursAttributs, "temTitulaire");
		resultat = resultat + verifierTemoin(valeursAttributs, "temInseeProv");
		// Vérifier temPersonnel
		resultat = resultat + verifierTemoin(valeursAttributs, "temPersonnel");
		// Vérifier si le témoin priseCptInsee a les bonnes valeurs
		String numSS = (String) valeursAttributs.objectForKey("noInsee");
		String priseCptInsee = (String) valeursAttributs.objectForKey("priseCptInsee");
		if (priseCptInsee.equals(PRISE_EN_COMPTE_NO_INSEE) == false && priseCptInsee.equals(PRISE_EN_COMPTE_NO_INSEE_PROVISOIRE) == false) {
			resultat = resultat + "Prise_Cpt_Insee >>" + "uniquement R ou P" + System.getProperty("line.separator");

		}
		if (priseCptInsee.equals(PRISE_EN_COMPTE_NO_INSEE)) {
			if (numSS != null && numSS.equals("9" + NO_INSEE_PROVISOIRE) == false) {
				String resultatSS = verifierNoSS(valeursAttributs);
				if (resultatSS.length() > 0) {
					resultat = resultat + "Erreur_Numero_SS >>" + resultatSS + System.getProperty("line.separator");
				}
			}
		}
		return resultat;
	}

	/**
	 * 
	 * @param valeursAttributs
	 * @return
	 */
	private static String verifierNoSS(NSMutableDictionary valeursAttributs) {
		String numSS = (String) valeursAttributs.objectForKey("noInsee"); // La
																			// longueur
																			// a
																			// déjà
																			// été
																			// vérifiée

		String resultat = "";
		// vérifier si le numéro Insee est cohérent avec la civilité
		String civilite = (String) valeursAttributs.objectForKey("civilite");
		if (civilite != null) {
			if (civilite.equals(CocktailConstantes.MONSIEUR)) {
				if (numSS.substring(0, 1).equals("1") == false) {
					resultat += "Pour un homme, le numéro Insee commence par 1";
				}
			} else {
				if (numSS.substring(0, 1).equals("2") == false) {
					resultat += "Pour une femme, le numéro Insee commence par 2.";
				}
			}
		}
		// Vérifier si les autres données sont cohérentes si il ne s'agit pas
		// d'un numéro provisoire
		if (numSS.substring(1).equals(NO_INSEE_PROVISOIRE)) {
			return resultat;
		}
		// Vérifier l'année naissance en fonction de la date de naissance
		String temp = numSS.substring(1, 3);
		int anneeNaissance = -1;
		if (temp.equals("99") == false) { // Il ne s'agit pas d'un NoSS
											// provisoire
			try {
				anneeNaissance = new Integer(temp).intValue();
				anneeNaissance = ANNEE_BASE_POUR_NAISSANCE + anneeNaissance;
			} catch (Exception e) {
			}
			NSTimestamp dNaissance = (NSTimestamp) valeursAttributs.objectForKey("dateNaissance"); // les
																									// attributs
																									// ont
																									// déjà
																									// été
																									// transformés
			if (dNaissance != null) {
				if (DateCtrl.getYear(dNaissance) != anneeNaissance) {
					resultat += " L'année de naissance du numéro Insee ne correspond pas à l'année de naissance.";
				}
				// // Mois naissance
				// temp = numSS.substring(3,5);
				// if (temp.equals("99") == false) { // Il ne s'agit pas d'un
				// NoSS provisoire
				// try {
				// int mois = new Integer(temp).intValue();
				// // 04/03/2011 - Correction sur les n° Insee dont le mois est
				// > 20
				// if (mois <= 20 && DateCtrl.getMonth(dNaissance) + 1 != mois)
				// {
				// resultat +=
				// " Le mois de naissance du numéro Insee ne correspond pas au mois de naissance.";
				// }
				// } catch (Exception e) {
				// resultat +=
				// " Le mois de naissance du numéro Insee ne comporte pas que des chiffres.";
				// }
				// }
			}
		}
		boolean anneeNaissanceFournie = (anneeNaissance != -1);

		temp = numSS.substring(5, 7);
		String paysNaissance = (String) valeursAttributs.objectForKey("paysNaissance");
		if (paysNaissance == null) {
			// Vérifier si le pays de naissance peut être déterminé avec le n°
			// SS
			if (temp.equals("99") == false && temp.equals("96") == false) {
				if (anneeNaissanceFournie && anneeNaissance <= 1962
						&& (temp.equals("91") || temp.equals("92") || temp.equals("93") || temp.equals("94") || temp.equals("95"))) {
					// Il s'agit d'une personne nées sous un protectorat ou en
					// Algérie, ne pas forcer le code pays
				} else {
					// Il s'agit d'une personne née en France
					paysNaissance = EOPays.CODE_PAYS_DEFAUT;
					valeursAttributs.setObjectForKey(EOPays.CODE_PAYS_DEFAUT, "paysNaissance");
				}
			}
		} else {
			// Vérifier le pays de naissance du No SS si le pays a été fourni
			// vérifier la cohérence du département avec le département de
			// l'individu et avec le pays de naissance
			if (temp.equals("99") || temp.equals("96")) { // étranger ou ancien
															// protectorat
				if (paysNaissance.equals(EOPays.CODE_PAYS_DEFAUT)) {
					resultat += " Le numéro Insee indique que la personne est née à l'étranger.";
				} else {
					String temp1 = numSS.substring(7, 10);
					try {
						int valeur = new Integer(temp1).intValue();
						if (valeur == 0) {
							resultat += " Le code pays du numéro Insee pour les personnes nées à l'étranger doit être compris entre 000 et 999.";
						}
					} catch (Exception e) {
						resultat += " Le code pays du numéro Insee pour les personnes nées à l'étranger ne doit comporter que des chiffres.";
					}
				}
				if (anneeNaissanceFournie && temp.equals("96") && anneeNaissance > 1967) {
					resultat += " A partir de 1967, Le code département du numéro Insee ne peut être 96, ce doit être 99.";
				}
			} else {
				String departement = "";
				boolean ancienneColonie = false; // 01/10/2010 - Pour les
													// anciennes colonies
				departement = (String) valeursAttributs.objectForKey("dptNaissance");
				if (temp.equals("97") || temp.equals("98")) { // DOM - TOM
					if (departement == null) {
						if (temp.equals("98")) { // Les anciennes colonies ont
													// des codes à cheval sur
													// Mayotte, on se base donc
													// sur le fait que le
													// département n'est pas
													// fourni pour vérifier si
													// il s'agit d'anciennes
													// colonies
							try {
								String temp1 = numSS.substring(7, 10);
								int val = new Integer(temp1).intValue();
								ancienneColonie = (val >= 201 && val <= 506);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				// Caractères 5 à 7 = Numéro de département. Vérifier la
				// cohérence du département
				if (!ancienneColonie && departement != null) {
					if (temp.equals("97") || temp.equals("98")) { // DOM - TOM
						temp = numSS.substring(5, 8); // le département est codé
														// sur 3 caractères dans
														// le No Insee
						if (departement.equals("000")) {
							departement = temp;
							valeursAttributs.setObjectForKey(departement, "dptNaissance");
						}
					} else {
						if (departement.equals("000")) {
							departement = "0" + temp;
							valeursAttributs.setObjectForKey(departement, "dptNaissance");
						}
						departement = departement.substring(1); // les numéros
																// de
																// département
																// commencent
																// par 0 pour
																// les
																// départements
																// hors dom-tom
					}
					if (departement.equals(temp) == false && anneeNaissanceFournie) {
						// Vérification des gens nés sous protectorat
						if (anneeNaissance <= 1967 && (temp.equals("91") || temp.equals("92") || temp.equals("93") || temp.equals("94") || temp.equals("95"))) {
							if (temp.equals("91") || temp.equals("92") || temp.equals("93") || temp.equals("94")) {
								// Algérie
								if (paysNaissance.equals("352") == false) {
									resultat += " Le numéro Insee indique que la personne est née en Algérie.";
								}
							} else if (temp.equals("95")) {
								if (paysNaissance.equals("350") == false) {
									resultat += " Le numéro Insee indique que la personne est née au Maroc.";
								}
							}
						}
						if (anneeNaissance <= 1968 && (temp.equals("75") || temp.equals("78"))) {
							// vérification île de france pour les gens nés
							// avant 1968
							// le code département 75 peut avoir un département
							// de naissance égal à 75 ou 92 ou 93 ou 94
							if (temp.equals("75") && departement.equals("75") == false && departement.equals("92") == false
									&& departement.equals("93") == false && departement.equals("94") == false) {
								resultat += " Une personne née avant 1968 avec comme département du numéro Insee 75, ne peut avoir comme département de naissance que : 75, 92, 93 ou 94.";
							}
							// 09/03/2011- le code département 78 peut avoir un
							// département de naissance égal à 78 ou 91 ou 92 ou
							// 93 ou 94 ou 95
							if (temp.equals("78") && departement.equals("78") == false && departement.equals("91") == false
									&& departement.equals("92") == false && departement.equals("93") == false && departement.equals("94") == false
									&& departement.equals("95") == false) {
								resultat += "Une personne née avant 1968 avec comme département du numéro Insee 78, ne peut avoir comme département de naissance que : 78, 91, 92, 93, 94 ou 95";
							}
						} else if (anneeNaissance < 1976 && temp.equals("20")) { // Vérification
																					// Corse
							// Vérification des départements de naissance : ne
							// peut être que 2A ou 2B
							if (departement.equals("2A") == false && departement.equals("2B") == false) {
								resultat += " Le département de naissance sélectionné ne correspond pas à la Corse.";
							}
						} else {
							resultat += " Le département du numéro Insee ne correspond pas avec le département fourni";
						}
						if (anneeNaissance <= 1962 && (temp.equals("91") || temp.equals("92") || temp.equals("93") || temp.equals("94") || temp.equals("95"))) {
							// Ne pas vérifier le code pays
						} else if (paysNaissance.equals(EOPays.CODE_PAYS_DEFAUT) == false && paysNaissance.equals(EOPays.CODE_PAYS_INCONNU) == false) {
							resultat += " Le numéro Insee indique que la personne est née en France.";
						}
					}
				}
			}
		}
		// Vérifier que les numéros Insee pour la Corse sont cohérents
		if (anneeNaissanceFournie) {
			if ((temp.equals("2A") || temp.equals("2B")) && anneeNaissance < 1976) {
				resultat += " Pour un corse né avant 1976, le code département du numéro Insee ne peut être 2A ou 2B.";
			}
			if (temp.equals("20") && anneeNaissance >= 1976) {
				resultat += " Pour un corse né après 1976, le code département du numéro Insee doit être 2A ou 2B.";
			}
		}
		// Vérifier la clé
		Number cle = (Number) valeursAttributs.objectForKey("cleInsee"); // les
																			// attributs
																			// ont
																			// déjà
																			// été
																			// transformés
																			// dans
																			// le
																			// bon
																			// type
		if (cle != null) {
			String nouveauNumero = numSS.toUpperCase();
			String nombreASoustraire = null; // Pour la Corse
			if (nouveauNumero.indexOf("A") > 0) {
				nombreASoustraire = "100";
			} else if (nouveauNumero.indexOf("B") > 0) {
				nombreASoustraire = "200";
			}
			nouveauNumero = nouveauNumero.replaceAll("B", "0");
			nouveauNumero = nouveauNumero.replaceAll("A", "0");
			BigInteger bigInt1 = new BigInteger(nouveauNumero.substring(0, 9));
			if (nombreASoustraire != null) {
				bigInt1 = bigInt1.subtract(new BigInteger(nombreASoustraire));
			}
			String s = "" + ((bigInt1.mod(new BigInteger("97"))).intValue()) + (numSS.substring(9, 13));
			try {
				BigInteger bigInt2 = new BigInteger(s);
				Integer numCle = new Integer(97 - ((bigInt2.mod(new BigInteger("97"))).intValue()));
				if (numCle.intValue() != cle.intValue()) {
					resultat += " Cle SS invalide pour le numero SS.";
				}
			} catch (Exception e) {
				resultat += "" + s + " : Cle SS invalide pour le numero SS.";
			}
		}

		return resultat;
	}

	// Méthodes statiques
	public static EOIndividu individuPourSourceId(EOEditingContext editingContext, Number idSource) {
		if (idSource == null)
			return null;

		EOIndividu resultat = null;

		if (EOImportParametres.cacheMemoireNiveauMoyenOuPlus()) {
			resultat = idSourceCache.get(idSource);
			if (resultat != null)
				return resultat;
		}

		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, ID_SOURCE_KEY, (Integer)idSource);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOIndividu)premierObjetValide(resultats);

		if (EOImportParametres.cacheMemoireNiveauMoyenOuPlus() && resultat != null) {
			idSourceCache.put(idSource, resultat);
		}
		return resultat;
	}

	public static void resetCache() {
		idSourceCache.clear();
	}
}
