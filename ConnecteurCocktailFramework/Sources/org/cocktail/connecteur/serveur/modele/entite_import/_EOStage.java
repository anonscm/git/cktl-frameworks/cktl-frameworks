// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOStage.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStage extends ObjetImportPourCarriere {
	public static final String ENTITY_NAME = "Stage";

	// Attributes
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_TITULARISATION_KEY = "dateTitularisation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STA_SOURCE_KEY = "staSource";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_RENOUVELLEMENT_KEY = "temRenouvellement";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOStage.class);

  public EOStage localInstanceIn(EOEditingContext editingContext) {
    EOStage localInstance = (EOStage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dateTitularisation() {
    return (NSTimestamp) storedValueForKey("dateTitularisation");
  }

  public void setDateTitularisation(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dateTitularisation from " + dateTitularisation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateTitularisation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer staSource() {
    return (Integer) storedValueForKey("staSource");
  }

  public void setStaSource(Integer value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating staSource from " + staSource() + " to " + value);
    }
    takeStoredValueForKey(value, "staSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temRenouvellement() {
    return (String) storedValueForKey("temRenouvellement");
  }

  public void setTemRenouvellement(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating temRenouvellement from " + temRenouvellement() + " to " + value);
    }
    takeStoredValueForKey(value, "temRenouvellement");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOStage.LOG.isDebugEnabled()) {
      _EOStage.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOStage.LOG.isDebugEnabled()) {
      _EOStage.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOStage createStage(EOEditingContext editingContext, Integer carSource
, String cCorps
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temRenouvellement
) {
    EOStage eo = (EOStage) EOUtilities.createAndInsertInstance(editingContext, _EOStage.ENTITY_NAME);    
		eo.setCarSource(carSource);
		eo.setCCorps(cCorps);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemRenouvellement(temRenouvellement);
    return eo;
  }

  public static NSArray<EOStage> fetchAllStages(EOEditingContext editingContext) {
    return _EOStage.fetchAllStages(editingContext, null);
  }

  public static NSArray<EOStage> fetchAllStages(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStage.fetchStages(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStage> fetchStages(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStage> eoObjects = (NSArray<EOStage>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStage fetchStage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStage.fetchStage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStage fetchStage(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStage> eoObjects = _EOStage.fetchStages(editingContext, qualifier, null);
    EOStage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStage)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Stage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStage fetchRequiredStage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStage.fetchRequiredStage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStage fetchRequiredStage(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStage eoObject = _EOStage.fetchStage(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Stage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStage localInstanceIn(EOEditingContext editingContext, EOStage eo) {
    EOStage localInstance = (eo == null) ? null : (EOStage)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
