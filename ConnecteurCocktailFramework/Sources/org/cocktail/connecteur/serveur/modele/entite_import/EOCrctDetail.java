package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.InterfaceRecordGenerique;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCrctCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrctDetail;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**.
 * Classe gérant les détails d'un CRCT d'Adoption d'un individu
 * @author alainmalaplate
 *
 */
public class EOCrctDetail extends _EOCrctDetail implements InterfaceRecordGenerique {
	private static final long serialVersionUID = 1193287386913640504L;

	public void supprimerRelations() {
		super.supprimerRelations();
		removeObjectFromBothSidesOfRelationshipWithKey(crct(), CRCT_KEY);
	}
	
	public void preparerRelations() {
		super.preparerRelations();
		LogManager.logDetail("Preparation des relations pour les détails d'un CRCT");
		if (editingContext() != null && crctSource() != null && idSource() != null) {
			EOCrct crct = EOCrctCorresp.crctPourSourceEtCgId(editingContext(), idSource(),crctSource());
			if (crct != null) {
				addObjectToBothSidesOfRelationshipWithKey(crct,CRCT_KEY);
			}
		}
	}
	
	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttribut(String nomAttribut) {
		return nomRelationPourAttributComparaison(nomAttribut);
	}
	
	/**
	 * 
	 */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		if (nomAttribut.equals(ID_SOURCE_KEY)) {
			return CRCT_KEY+"."+EOCrct.INDIVIDU_KEY;
		} else if (nomAttribut.equals(CRCT_SOURCE_KEY)) {
			return CRCT_KEY;
		} else {
			return null;
		}	
	}
	
	public EOLogImport verifierRecord() {
		LogManager.logDetail("Verification du record");
		EOLogImport log=super.verifierRecord();
		if (log!=null) {
			log.setCrctDetailRelationship(this);
			return log;
		}
		
		if (crct() == null && idSource() != null && crctSource() != null) {
			EOCrct crct = EOCrctCorresp.crctPourSourceEtCgId(editingContext(), idSource(),crctSource());
			if (crct != null) {
				addObjectToBothSidesOfRelationshipWithKey(crct,CRCT_KEY);
			} 
		}
		String messageLog = null;
		if (crct() == null) {
			messageLog = "CRCT_NON_IMPORTE";
		} else if (crct().temImport().equals(ObjetImport.A_TRANSFERER) && crct().statut().equals(ObjetImport.STATUT_ERREUR)) {
			messageLog = "CRCT_INVALIDE";
		}
		// Vérifier si l'individu existe dans le cas où il y a eu une erreur pour préciser le message
		EOIndividu individu = null;
		if (idSource() != null) {
			individu = EOIndividu.individuPourSourceId(editingContextTravail(), idSource());
		}
		if (messageLog != null) {
			if (individu == null) {
				messageLog = "INDIVIDU_NON_IMPORTE";
			} else if (individu.temImport().equals(ObjetImport.A_TRANSFERER)) {
				if (individu.statut().equals(EOIndividu.STATUT_ERREUR)) {
					messageLog = "INDIVIDU_INVALIDE";
				}
			}
		}
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(),this.entityName(),messageLog);
			log.setIdSource(new Integer(idSource().intValue()));
			log.setEimpSource(new Integer(crctSource().intValue()));
			log.setEimp2Source(crctdSource());
			log.setCrctDetailRelationship(this);
			if (individu != null) {
				log.addObjectToBothSidesOfRelationshipWithKey(individu,"individu");
			}
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			return log;
		} else {
			return null;
		}
	}


	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log=super.verifierRecordApresOperation();
		if (log!=null) {
			log.setCrctDetailRelationship(this);
			return log;
		}
		String message =  verifierDatesDetailPourCrct(crct());
		if (message != null) {
			log = new EOLogImport(); 
			log.initAvecEditingContext(editingContextTravail(),this.entityName(), message);
			log.setIdSource(new Integer(idSource().intValue()));
			log.setEimpSource(new Integer(crctSource().intValue()));
			log.setEimp2Source(crctdSource());
			log.setCrctDetailRelationship(this);
			if (crct() != null && crct().individu() != null) {
				log.addObjectToBothSidesOfRelationshipWithKey(crct().individu(), EOLogImport.INDIVIDU_KEY);
			}
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			return log;
		}
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CRCT_DETAIL_KEY;
	}
	
	
	public String nomRelationPourAttributDansLog(String nomAttribut) {
		if (nomAttribut.equals(ID_SOURCE_KEY)) {
			return CRCT_KEY+"."+EOCrct.INDIVIDU_KEY;
		} else {
			return null;
		}	
	}
	
	public String verifierRecordPourClient(EOEditingContext editingContext) {
		LogManager.logDetail("verifierRecordPourClient");
		// Rajouter le cld qui n'est pas construit lorsqu'on effectue les vérifications client
		EOCrct crct = EOCrctCorresp.crctPourSourceEtCgId(editingContext, idSource(),crctSource());
		if (crct != null) {
			return verifierDatesDetailPourCrct(crct);
		}
		LogManager.logDetail("Crct non trouvé");
		return null;
	}
	
	// Méthodes privées
	private String verifierDatesDetailPourCrct(EOCrct crct) {
		if (dateFin() != null) {
			if (DateCtrl.isBefore(crct.dateFin(),dateDebut())) {
				return "DATES_DETAIL_CRCT";
			} else if (crct.dateFin() != null && DateCtrl.isBefore(crct.dateFin(),dateFin())) {
				return "DATES_DETAIL_CRCT";
			}
		}
		if (DateCtrl.isBefore(dateDebut(),crct.dateDebut())) {
			return "DATES_DETAIL_CRCT";
		} else if (crct.dateFin() != null && DateCtrl.isBefore(crct.dateFin(),dateDebut())) {
			return "DATES_DETAIL_CRCT";
		}
		return null;
	}
	
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		return ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
	}

	@Override
	public Integer eimpSource() {
		return crctdSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCrctdSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCrctDetail.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}
}
