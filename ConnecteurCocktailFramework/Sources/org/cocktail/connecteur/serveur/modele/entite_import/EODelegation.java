package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDelegation;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class EODelegation extends _EODelegation {

	private static final long serialVersionUID = -777297880656836737L;
	private static Logger log = Logger.getLogger(EODelegation.class);


	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueDelegation.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.DELEGATION_KEY;
	}
	

	private boolean estInclusDansChangementPositionOuContrat() {
		boolean changementPositionOuContratTrouve = false;
		// On cherche d'abord un changement de Position dans l'import
		NSArray resultats = EOChangementPosition.rechercherChangementsPositionsPourIndividuEtPeriode(editingContext(), idSource(), dateDebut(),
				dateFin());
		if (resultats.count() > 0)
			changementPositionOuContratTrouve = true;
		else {
			// Si rien trouvé, on cherche un contrat dans l'import
			resultats=EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(), idSource(), dateDebut(), dateFin());
			if (resultats.count() > 0)
				changementPositionOuContratTrouve = true;
			else {
				EOGrhumIndividu grhumIndividu = EOIndividuCorresp.individuGrhum(editingContext(), idSource());
				if (grhumIndividu != null) {
					// Si rien trouvé, on cherche un changement de position dans le SI destinataire
					resultats = EOMangueChangementPosition.rechercherChangementsPositionDestinPourIndividuValidesSurPeriode(editingContext(),
							grhumIndividu, dateDebut(), dateFin());
					if (resultats.count() > 0)
						changementPositionOuContratTrouve = true;
					else {
						// Si rien trouvé, on cherche un contrat dans le SI destinataire
						resultats = EOMangueContrat. rechercherContratsPourIndividuEtPeriode(editingContext(),
								grhumIndividu, dateDebut(), dateFin());
						changementPositionOuContratTrouve= (resultats.count()>0);
					}
				}
			}
		}
		return changementPositionOuContratTrouve;
	}

	@Override
	public EOLogImport verifierRecord() {
		EOLogImport logImport=super.verifierRecord();
		if (logImport!=null) {
			return logImport;
		}
		
		String messageLog=null;
		if (!estInclusDansChangementPositionOuContrat())
				messageLog="DELEG_INCLUS_DANS_CHANGEMENT_POS_OU_CONTRAT";
		
		if (messageLog != null) {
			logImport = new EOLogImport();
			logImport.initAvecEditingContext(editingContextTravail(),this.entityName(),messageLog);
			logImport.setIdSource(new Integer(idSource().intValue()));
			if (individu() != null) {
				logImport.addObjectToBothSidesOfRelationshipWithKey(individu(),"individu");
			}
			setStatut(STATUT_ERREUR);
		}

		return logImport;
	}

	
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat= "";
		resultat += ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		
		String cRne = (String)valeursAttributs.objectForKey(C_RNE_KEY);
		if (cRne != null) {
			EORne rne = EORne.getFromCode(editingContext, cRne);
			if (rne == null) {
				resultat += "Rne_Inconnu >>" + cRne + System.getProperty("line.separator");
			}
		}
		
		String cModDelegation = (String)valeursAttributs.objectForKey(C_MOD_DELEGATION_KEY);
		if (cModDelegation != null) {
			EOTypeModDelegation typeModDelegation = EOTypeModDelegation.rechercherPourCModDelegation(editingContext, cModDelegation);
			if (typeModDelegation == null) {
				resultat += "CModDelegation_Inconnu >>" + cModDelegation + System.getProperty("line.separator");
			}
		}
		
		String cMotDelegation = (String)valeursAttributs.objectForKey(C_MOT_DELEGATION_KEY);
		if (cMotDelegation != null) {
			EOTypeMotDelegation typeMotDelegation = EOTypeMotDelegation.rechercherPourCModDelegation(editingContext, cMotDelegation);
			if (typeMotDelegation == null) {
				resultat += "CMotDelegation_Inconnu >>" + cMotDelegation + System.getProperty("line.separator");
			}
		}
		
		Number quotiteService = (Number) valeursAttributs.objectForKey(NUM_QUOTITE_KEY);
		if (quotiteService==null)
			valeursAttributs.setObjectForKey(new Integer(100), NUM_QUOTITE_KEY);

		resultat = resultat + verifierTemoin(valeursAttributs, TEM_ANNUEL_DELEGATION_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);

		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return delegationSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setDelegationSource(value);
	}
}
