// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOOrgaBudget.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOOrgaBudget extends ObjetImport {
	public static final String ENTITY_NAME = "OrgaBudget";

	// Attributes
	public static final String OB_CR_KEY = "obCr";
	public static final String OB_DATE_CLOTURE_KEY = "obDateCloture";
	public static final String OB_DATE_OUVERTURE_KEY = "obDateOuverture";
	public static final String OB_ETAB_KEY = "obEtab";
	public static final String OB_LIB_KEY = "obLib";
	public static final String OB_LUCRATIVITE_KEY = "obLucrativite";
	public static final String OB_NIV_KEY = "obNiv";
	public static final String OB_SOURCE_KEY = "obSource";
	public static final String OB_SOURCE_PERE_KEY = "obSourcePere";
	public static final String OB_SOUS_CR_KEY = "obSousCr";
	public static final String OB_UB_KEY = "obUb";
	public static final String OB_UNIV_KEY = "obUniv";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TYOR_ID_KEY = "tyorId";

	// Relationships
	public static final String TO_ORGA_BUDGET_PERE_KEY = "toOrgaBudgetPere";

  private static Logger LOG = Logger.getLogger(_EOOrgaBudget.class);

  public EOOrgaBudget localInstanceIn(EOEditingContext editingContext) {
    EOOrgaBudget localInstance = (EOOrgaBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String obCr() {
    return (String) storedValueForKey("obCr");
  }

  public void setObCr(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obCr from " + obCr() + " to " + value);
    }
    takeStoredValueForKey(value, "obCr");
  }

  public NSTimestamp obDateCloture() {
    return (NSTimestamp) storedValueForKey("obDateCloture");
  }

  public void setObDateCloture(NSTimestamp value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obDateCloture from " + obDateCloture() + " to " + value);
    }
    takeStoredValueForKey(value, "obDateCloture");
  }

  public NSTimestamp obDateOuverture() {
    return (NSTimestamp) storedValueForKey("obDateOuverture");
  }

  public void setObDateOuverture(NSTimestamp value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obDateOuverture from " + obDateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "obDateOuverture");
  }

  public String obEtab() {
    return (String) storedValueForKey("obEtab");
  }

  public void setObEtab(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obEtab from " + obEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "obEtab");
  }

  public String obLib() {
    return (String) storedValueForKey("obLib");
  }

  public void setObLib(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obLib from " + obLib() + " to " + value);
    }
    takeStoredValueForKey(value, "obLib");
  }

  public Integer obLucrativite() {
    return (Integer) storedValueForKey("obLucrativite");
  }

  public void setObLucrativite(Integer value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obLucrativite from " + obLucrativite() + " to " + value);
    }
    takeStoredValueForKey(value, "obLucrativite");
  }

  public Integer obNiv() {
    return (Integer) storedValueForKey("obNiv");
  }

  public void setObNiv(Integer value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obNiv from " + obNiv() + " to " + value);
    }
    takeStoredValueForKey(value, "obNiv");
  }

  public Integer obSource() {
    return (Integer) storedValueForKey("obSource");
  }

  public void setObSource(Integer value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obSource from " + obSource() + " to " + value);
    }
    takeStoredValueForKey(value, "obSource");
  }

  public Integer obSourcePere() {
    return (Integer) storedValueForKey("obSourcePere");
  }

  public void setObSourcePere(Integer value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obSourcePere from " + obSourcePere() + " to " + value);
    }
    takeStoredValueForKey(value, "obSourcePere");
  }

  public String obSousCr() {
    return (String) storedValueForKey("obSousCr");
  }

  public void setObSousCr(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obSousCr from " + obSousCr() + " to " + value);
    }
    takeStoredValueForKey(value, "obSousCr");
  }

  public String obUb() {
    return (String) storedValueForKey("obUb");
  }

  public void setObUb(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obUb from " + obUb() + " to " + value);
    }
    takeStoredValueForKey(value, "obUb");
  }

  public String obUniv() {
    return (String) storedValueForKey("obUniv");
  }

  public void setObUniv(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating obUniv from " + obUniv() + " to " + value);
    }
    takeStoredValueForKey(value, "obUniv");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public Integer tyorId() {
    return (Integer) storedValueForKey("tyorId");
  }

  public void setTyorId(Integer value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
    	_EOOrgaBudget.LOG.debug( "updating tyorId from " + tyorId() + " to " + value);
    }
    takeStoredValueForKey(value, "tyorId");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget toOrgaBudgetPere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget)storedValueForKey("toOrgaBudgetPere");
  }

  public void setToOrgaBudgetPereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget value) {
    if (_EOOrgaBudget.LOG.isDebugEnabled()) {
      _EOOrgaBudget.LOG.debug("updating toOrgaBudgetPere from " + toOrgaBudgetPere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget oldValue = toOrgaBudgetPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toOrgaBudgetPere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toOrgaBudgetPere");
    }
  }
  

  public static EOOrgaBudget createOrgaBudget(EOEditingContext editingContext, NSTimestamp obDateOuverture
, String statut
, String temImport
, Integer tyorId
) {
    EOOrgaBudget eo = (EOOrgaBudget) EOUtilities.createAndInsertInstance(editingContext, _EOOrgaBudget.ENTITY_NAME);    
		eo.setObDateOuverture(obDateOuverture);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTyorId(tyorId);
    return eo;
  }

  public static NSArray<EOOrgaBudget> fetchAllOrgaBudgets(EOEditingContext editingContext) {
    return _EOOrgaBudget.fetchAllOrgaBudgets(editingContext, null);
  }

  public static NSArray<EOOrgaBudget> fetchAllOrgaBudgets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOOrgaBudget.fetchOrgaBudgets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOOrgaBudget> fetchOrgaBudgets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOOrgaBudget.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOOrgaBudget> eoObjects = (NSArray<EOOrgaBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOOrgaBudget fetchOrgaBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrgaBudget.fetchOrgaBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrgaBudget fetchOrgaBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOOrgaBudget> eoObjects = _EOOrgaBudget.fetchOrgaBudgets(editingContext, qualifier, null);
    EOOrgaBudget eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOOrgaBudget)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OrgaBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrgaBudget fetchRequiredOrgaBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrgaBudget.fetchRequiredOrgaBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrgaBudget fetchRequiredOrgaBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    EOOrgaBudget eoObject = _EOOrgaBudget.fetchOrgaBudget(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OrgaBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrgaBudget localInstanceIn(EOEditingContext editingContext, EOOrgaBudget eo) {
    EOOrgaBudget localInstance = (eo == null) ? null : (EOOrgaBudget)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
