//EOCompte.java
//Created on Tue Jun 03 14:13:53 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrhumParametres;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCompteCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumCompte;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

// 29/10/2010 - surcharge de la méthode verifierRecord pour le cas où il y a une erreur donc un log et pour vérifier si la
// longueur du login est > à un max car sinon il y a un problème plus tard lors de la destruction des logs
public class EOCompte extends _EOCompte {
	private static final String CPT_CRYPTE_VALEUR_PAR_DEFAUT = "N";
	private static final String CPT_PRINCIPAL_VALEUR_PAR_DEFAUT = "O";
	private static final String CPT_LISTE_ROUGE_VALEUR_PAR_DEFAUT = "N";
	private static String vlanLocal,vlanExterne,uidExterne;
	private int maxLogin = -1;	// pour éviter de le rechercher dans la base
	public EOCompte() {
		super();
	}


	public void awakeFromInsertion(EOEditingContext editingContext)  {
		vlanExterne = EOGrhumParametres.parametrePourCle(editingContext, "GRHUM_VLAN_EXTERNE");
		vlanLocal = EOGrhumParametres.parametrePourCle(editingContext, "GRHUM_VLAN_LOCAL");
		uidExterne = EOGrhumParametres.parametrePourCle(editingContext, "ANNUAIRE_EXTRANET_UID");
	}

	public void awakeFromFetch(EOEditingContext editingContext)  {
		vlanExterne = EOGrhumParametres.parametrePourCle(editingContext, "GRHUM_VLAN_EXTERNE");
		vlanLocal = EOGrhumParametres.parametrePourCle(editingContext, "GRHUM_VLAN_LOCAL");
		uidExterne = EOGrhumParametres.parametrePourCle(editingContext, "ANNUAIRE_EXTRANET_UID");
	}
	
	/** Initialise un compte */
	public void init() {
		super.init();
		if (vlanExterne == null) {
			vlanExterne = EOGrhumParametres.parametrePourCle(editingContextTravail(), "GRHUM_VLAN_EXTERNE");
		}
		setCptVlan(vlanExterne);
		setCptConnexion(CocktailConstantes.FAUX);
		setCptCharte(CocktailConstantes.FAUX);
		setCptGid(new Integer(0));
		setCptUid(new Integer(0));
		setCptCrypte(CPT_CRYPTE_VALEUR_PAR_DEFAUT);
		setCptPrincipal(CPT_PRINCIPAL_VALEUR_PAR_DEFAUT);
		setCptListeRouge(CPT_LISTE_ROUGE_VALEUR_PAR_DEFAUT);
	}
	
	/** Initialise un compte pour un individu en creant login et mot de passe et insere les donnees dans l'editing context
	 * @param individu
	 * @param uidGid
	 */
	public void initAvecIndividu(EOIndividu individu) {
		init();
		// Evaluer cptSource 
		int cptSource = lastCptSourcePourIndividu(individu.editingContext(), individu) + 1;
		setCptSource(new Integer(cptSource));
		setCptLogin(creerLogin(individu));
		setCptPasswd(creerMotDePasse(individu.editingContext()));
		preparerEmail(individu);
		setCptUid(new Integer(-1));
		setIdSource(individu.idSource());
		setIndividuRelationship(individu);
	}
	
	// 29/10/2010
	/** Surcharge pour le cas ou il y a une erreur donc un log et pour verifier si la
	// longueur du login est > à un max car sinon il y a un probleme plus tard lors de la destruction des logs */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null) {
			if (cptLogin().length() > maxLogin()) {
				setCptLogin(cptLogin().substring(0,maxLogin()));
			}
		}
		return log;
	}
	
	/** Verifie le format de l'email,la longueur du login,
	 * la presence du couple email/domaine (si l'un y est, l'autre aussi), l'unicite du login, de l'email/domaine et 
	 * du uid pour les vlans PER, la compatibilite du vlan avec le domaine, la presence du shell et gid pour les vlans
	 * autres que 'X' et l'absence des infos pour les autres, force l'uid si elle n'est pas fournie pour les vlans externes */
	public EOLogImport verifierRecordApresOperation() {
		String messageLog = null;
		// Vérifier sa longueur
		if (cptLogin().length() > maxLogin()) {
			messageLog = "LOGIN_TRONQUE";
			setStatut(ObjetImport.STATUT_TRONQUE);
			setCptLogin(cptLogin().substring(0,maxLogin()));
		}
		// Vérifier que le login est unique sur le vlan
		if (!estUniquePourTousComptes(cptLogin(), "cptLogin",cptVlan(),"cptVlan")) {
			messageLog = "LOGIN_UTILISE";
		} 
		if (messageLog == null) {
			// Vérifier que l'email a un format légal
			if (cptEmail() != null && cptEmail().length() > 0) {
				if (StringCtrl.isAcceptBasicString(cptEmail()) == false) {
					LogManager.logDetail("Insertion d'un log pour email avec caractères invalides");
					messageLog = "EMAIL_INVALIDE";
				} else if (cptDomaine() == null) {
					LogManager.logDetail("Insertion d'un log pour domaine manquant");
					messageLog = "DOMAINE_NECESSAIRE";
				} else {
					if (!estUniquePourTousComptes(cptEmail(),"cptEmail",cptDomaine(),"cptDomaine")) {
						LogManager.logDetail("Insertion d'un log pour email utilisé");
						messageLog = "EMAIL_UTILISE";
					}
				}
			}
		}

		if (messageLog == null) {
			// Vérifier le domaine, la présence du shell et du gid pour des comptes internes
			if (cptDomaine() != null) {
				// Vérifier la présence de l'email
				if (cptEmail() == null) {
					messageLog = "EMAIL_NECESSAIRE";
					LogManager.logDetail("Insertion d'un log pour email manquant");
				} else {
					String domainePrincipal = EOGrhumParametres.parametrePourCle(editingContextTravail(), "GRHUM_DOMAINE_PRINCIPAL");
					if (domainePrincipal != null) {
						if (vlanLocal.indexOf(cptVlan()) >= 0) {
							if (cptDomaine().equals(domainePrincipal) == false) {
								LogManager.logDetail("Insertion d'un log pour incompatibilite de domaine");
								messageLog = "DOMAINE_INCOMPATIBLE";
							} else {
								if (cptGid() == null) {
									if (cptShell() == null) {
										LogManager.logDetail("Insertion d'un log pour gid/shell nécessaires");
										messageLog = "GID_SHELL_NECESSAIRES";
									} else {
										LogManager.logDetail("Insertion d'un log pour gid nécessaire");
										messageLog = "GID_NECESSAIRE";
									}
								} else if (cptShell() == null) {
									LogManager.logDetail("Insertion d'un log pour shell nécessaire");
									messageLog = "SHELL_NECESSAIRE";
								}
							}
						} else if (cptVlan().equals(vlanExterne)) {
							if (cptDomaine().equals(domainePrincipal)) {
								LogManager.logDetail("Insertion d'un log pour incompatibilite de domaine");
								messageLog = "DOMAINE_INCOMPATIBLE";
							}
						} else {
							LogManager.logDetail("Insertion d'un log pour vlan non gere");
							messageLog = "VLAN_NON_GERE";
						}
					}
				}
			}
		}
		
		if (messageLog == null) {
			// Vérification de l'UID pour les vlans autres que X
			if (cptVlan().equals(vlanExterne) == false) {
				if (cptUid() != null) {
					if (cptUid().toString().equals(uidExterne) == false) {
//						if (estUniquePourTousComptes(cptUid(), "cptUid",null,null) == false) {
//							LogManager.logDetail("Insertion d'un log pour uid utilise - UID : " + cptUid());
//							messageLog = "UID_UTILISE";
//						}
					} else {
						LogManager.logDetail("Insertion d'un log pour incompatibilite vlan avec uid");
						messageLog = "VLAN_INCOMPATIBLE";
					}
				}
			} else {
				if (cptUid() != null) {
					String uid = cptUid().toString();
					if (uid.equals("0") == false && uid.equals(uidExterne) == false) {
						LogManager.logDetail("Insertion d'un log pour uid qui ne devrait pas etre fournie");
						messageLog = "INFOS_INVALIDES";
					}
				} else {
					setCptUid(new Integer(0));
				}
				if (cptShell() != null || cptHome() != null) {
					LogManager.logDetail("Insertion d'un log pour shell et home");
					messageLog = "INFOS_INVALIDES";
				}
			}
		}

		if (messageLog != null) {
			return logPourMessage(messageLog);
		}
		return null;
	}
	
	/** retourne un compte du SI destinataire avec le m&ecirc;me login, le meme password, le meme email et domaine, le meme vlan. 
	 * Si il n'existe pas retourne un compte avec le m&ecirc;me login (si il n'est pas nul) ou le m&ecirc;me email/domaines si ils ne sont pas nuls */ 
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() == null && structure() == null) {
			return null;
		}
		else {
			if (estNouvelIndividuOuStructure()) {
				return null;
			} else {
				return EOGrhumCompte.compteDestinationPourCompte(editingContextTravail(),this);
			}
		}
	}
	
	/**
	 * 
	 */
	public String nomRelationPourLog() {	
		return EOLogImport.COMPTE_KEY;
	}
	
	/** pour les comptes en mode de creation automatique, on choisit ce moment pour verifier l'unicite du login
	 * par rapport a la base d'import ou le SI Destinataire
	 */
	public void preparerRelations() {
		super.preparerRelations();
		String param = EOImportParametres.valeurParametrePourCle(editingContext(), "COMPTE_AUTO");
		if (param != null && param.equals(CocktailConstantes.VRAI) && cptUid() != null) {
			int uidgid = cptUid().intValue();
			if (uidgid == -1) {
				// Compte créé automatiquement, on vérifie son unicité pa rapport à GRhum en rappelant la méthode creerLogin
				//String login = creerLogin(individu(), EOGrhumParametres.maxLogin(editingContext()),true);
				// 04/06/09 - Utilisation des procédures stockées de GRHUM
				String login = creerLogin(individu());
				if (login.equals(cptLogin()) == false) {
					setCptLogin(login);
				}
				setCptUid(new Integer(0));
			}
		}
		
		if (cptVlan()!=null) {
			EOVlans vlan=EOVlans.fetchVlans(editingContext(), EOVlans.C_VLAN_KEY, cptVlan());
			setVLanRelationship(vlan);
		}
		
		if (tcryLibelle()!=null) {
			EOTypeCryptage typeCryptage=EOTypeCryptage.fetchTypeCryptage(editingContext(), EOTypeCryptage.TCRY_LIBELLE_KEY, tcryLibelle());
			setTypeCryptageRelationship(typeCryptage);
		}
	}

	/** Verifie qu'un compte n'a pas le m&circ;me login, email, uid qu'uan autre compte */
	public EOLogImport comparerAvecCompte(EOCompte compte) {
		String messageLog = null;
		if (cptLogin().equals(compte.cptLogin()) && cptVlan().equals(compte.cptVlan())) {
			messageLog = "LOGIN_UTILISE";
		} else if (cptEmail() != null && compte.cptEmail() != null && cptEmail().equals(compte.cptEmail()) && 
				cptDomaine() != null && compte.cptDomaine() != null && cptDomaine().equals(compte.cptDomaine())) {
			messageLog = "EMAIL_UTILISE";
		} 
//			else if (cptVlan().equals(vlanExterne) == false && compte.cptVlan().equals(vlanExterne)) {
//			if (cptUid() != null && compte.cptUid() != null && 
//					cptUid().toString().equals(uidExterne) == false && compte.cptUid().toString().equals(uidExterne) == false) {
//				if (cptUid().equals(compte.cptUid())) {
//					messageLog = "UID_UTILISE";
//				}
//			}
//		}
		if (messageLog != null) {
			return logPourMessage(messageLog);
		} else {
			return null;
		}
	}
	// Méthodes protégées
	/** Retourne false */
	protected boolean doitAvoirIndividuEtStructure() {
		return false;
	}
	// Méthodes privées
	private int maxLogin() {
		if (maxLogin < 0) {
			maxLogin = EOGrhumParametres.maxLogin(editingContext());
		}
		return maxLogin;
	}
	private String creerLogin(EOIndividu individu) {
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(individu.nomUsuel(), "nom");
		dict.setObjectForKey(individu.prenom(), "prenom");
		NSDictionary result = EOUtilities.executeStoredProcedureNamed(individu.editingContext(),"consLogin",dict);
        return (String)result.objectForKey("login");
	}
	private String creerMotDePasse(EOEditingContext editingContext) {
		NSDictionary result = EOUtilities.executeStoredProcedureNamed(editingContext,"consPasswd",new NSDictionary());
        return (String)result.objectForKey("password");
	}
	private void preparerEmail(EOIndividu individu) {
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(individu.nomUsuel(), "nom");
		dict.setObjectForKey(individu.prenom(), "prenom");
	
		NSDictionary result = EOUtilities.executeStoredProcedureNamed(individu.editingContext(),"consEmailLong",dict);
		String emailLong = (String)result.objectForKey("emailLong");
		if (emailLong != null) {
			int index = emailLong.indexOf("@");
			if (index >= 0) {
				setCptEmail(emailLong.substring(0,index));
				setCptDomaine(emailLong.substring(index + 1));
				modifierPourDomaineInterne(individu.editingContext());
			}
		}
	}
	private void modifierPourDomaineInterne(EOEditingContext editingContext) {
		setCptVlan("P");
		setCptShell("/bin/");
		setCptHome("/Utilisateurs/" + cptLogin());
		setCptGid(new Integer(0));
	}
	private int lastCptSourcePourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@",new NSArray(individu.idSource()));
		EOFetchSpecification fs = new EOFetchSpecification("Compte",qual,new NSArray(EOSortOrdering.sortOrderingWithKey("cptSource", EOSortOrdering.CompareDescending)));
		fs.setFetchLimit(1);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return ((EOCompte)results.objectAtIndex(0)).cptSource().intValue();
		} catch (Exception e) {
			return 0;
		}
	}
	
	private boolean estUniquePourTousComptes(Object valeur,String attribut,Object valeur1,String attribut1) {
		NSArray comptes;
		NSMutableArray args = new NSMutableArray(valeur);
		String stringQualifier = attribut + " = %@";
		if (attribut1 != null && valeur1 != null) {
			args.addObject(valeur1);
			stringQualifier += " AND " + attribut1 + " = %@";
		}
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier +  " AND temImport = %@", args);
		// On vérifie avec un refresh car un compte vient peut-être d'être créé avec les mêmes attributs
		comptes = Finder.rechercherAvecQualifier(editingContext(), "Compte", qualifier, false,true);
		if (comptes != null && comptes.count() > 0) {
			java.util.Enumeration e = comptes.objectEnumerator();
			while (e.hasMoreElements()) {
				EOCompte compte = (EOCompte)e.nextElement();
				if (compte != this && compte.individu() != individu()) {	// Si il s'agit du même individu, pas de problème
					return false;
				}
			}
			return true;
		}
		args.removeObject(ObjetImport.A_TRANSFERER);
		qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		comptes = Finder.rechercherAvecQualifier(editingContext(), "GrhumCompte", qualifier, false,false);
		if (comptes == null || comptes.count() == 0) {
			return true;
		}
		// Comme il ne peut y avoir qu'un seul compte avec les valeurs recherchées, on peut prendre le premier trouvé
		EOGrhumCompte compteGrhum = (EOGrhumCompte)comptes.objectAtIndex(0);
		if (operation().equals(ObjetImport.OPERATION_INSERTION)) {
			return false;
		} else if (operation().equals(ObjetImport.OPERATION_CORRESPONDANCE)) {
			return (compteGrhum == destinataireEquivalent());
		} else {
			// update
			return compteGrhum == ((EOCompteCorresp)correspondance()).compteGrhum();	
		}
	}
	private EOLogImport logPourMessage(String messageLog) {
		EOLogImport log = new EOLogImport();
		log.initAvecEditingContext(editingContextTravail(),this.entityName(),messageLog);
		if (individu() != null) {
			log.setIdSource(new Integer(idSource().intValue()));
			log.addObjectToBothSidesOfRelationshipWithKey(individu(),"individu");
		} else if (strSource() != null) {
			log.setStrSource(strSource());
			log.addObjectToBothSidesOfRelationshipWithKey(structure(),"structure");
		}
		log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
		if (statut().equals(ObjetImport.STATUT_TRONQUE) == false) {
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
		}
		return log;
	}

	// Méthodes statiques
	/** Cree un compte pour un agent si le parametrage de Mangue propose la creation
	 * automatique de compte et que l'individu n'a pas de compte externe et l'ins&egrave;re dans l'editing context ainsi
	 * que les donn&eeacute;es associees<BR>
	 * Retourne null si pas de creation automatique ou que l'individu a dej&agrav;e un compte
	 */
	public static EOCompte creerComptePourIndividu(EOIndividu individu) {
		LogManager.logDetail("Creation de compte pour " + individu.nomUsuel() + " " + individu.prenom());
		EOCompte compte = new EOCompte();
		compte.initAvecIndividu(individu);
		return compte;
	}

	/** Valide les donnees d'un compte.<BR>
	 *  Verifie que la source (individu ou structure est fournie), le vlan est correct et que 
	 *  la charte et la connexion ont bien la valeur 'O' ou 'N'<BR>
	 *  Supprime les caract&egrave;res accentues du password, du mail, du shell et du home<BR>
	 *  Met le login et le email en minuscule si necessaire
	 *  
	 *  @param editingContext
	 *  @param valeursAttributs dictionnaire des attributs a valider ou modifier
	 */
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number)valeursAttributs.objectForKey(ID_SOURCE_KEY);
		String strSource = (String)valeursAttributs.objectForKey(STR_SOURCE_KEY);
		if (idSource == null && strSource == null) {
			resultat = resultat + "ID_SOURCE_ou_STR_SOURCE >>" + System.getProperty("line.separator");
		} else if (idSource != null && strSource != null) {
			resultat = resultat + "UNE_SEULE_ID >>" + System.getProperty("line.separator");
		}
		String vlan = (String)valeursAttributs.objectForKey(CPT_VLAN_KEY);
		EOGenericRecord recordVlan = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOVlans.ENTITY_NAME, EOVlans.C_VLAN_KEY, vlan);
		if (recordVlan == null) {
			resultat = resultat + "Vlan_Inconnu >>" + vlan +System.getProperty("line.separator");
		}
		String cptLogin = (String)valeursAttributs.objectForKey(CPT_LOGIN_KEY);
		String loginMinuscule = cptLogin.toLowerCase();
		if (cptLogin.equals(loginMinuscule) == false) {
			// Mettre le login en minuscule
			valeursAttributs.setObjectForKey(loginMinuscule, CPT_LOGIN_KEY);
		}
		String cptCrypte= (String)valeursAttributs.objectForKey(CPT_CRYPTE_KEY);
		if (cptCrypte==null) {
			valeursAttributs.setObjectForKey(CPT_CRYPTE_VALEUR_PAR_DEFAUT, CPT_CRYPTE_KEY);
		}
		String cptPrincipal= (String)valeursAttributs.objectForKey(CPT_PRINCIPAL_KEY);
		if (cptPrincipal==null) {
			valeursAttributs.setObjectForKey(CPT_PRINCIPAL_VALEUR_PAR_DEFAUT, CPT_PRINCIPAL_KEY);
		}
		String cptListeRouge= (String)valeursAttributs.objectForKey(CPT_LISTE_ROUGE_KEY);
		if (cptListeRouge==null) {
			valeursAttributs.setObjectForKey(CPT_LISTE_ROUGE_VALEUR_PAR_DEFAUT, CPT_LISTE_ROUGE_KEY);
		}
		// Email en minuscule et sans caractères accentués
		modifierAttribut(valeursAttributs,CPT_EMAIL_KEY,true);
		// Password sans caractères accentués
		modifierAttribut(valeursAttributs,CPT_PASSWD_KEY,false);
		// Shell sans caractères accentués
		modifierAttribut(valeursAttributs,CPT_SHELL_KEY,false);
		// Home Directory sans caractères accentués
		modifierAttribut(valeursAttributs,CPT_HOME_KEY,false);

		resultat = resultat + verifierTemoin(valeursAttributs, CPT_CHARTE_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, CPT_CONNEXION_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, CPT_PRINCIPAL_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, CPT_LISTE_ROUGE_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, CPT_CRYPTE_KEY);

		return resultat;
	}
	// Méthodes privées statiques	
	private static void modifierAttribut(NSMutableDictionary valeursAttributs,String attribut,boolean useLowerCase) {
		String valeur = (String)valeursAttributs.objectForKey(attribut);
		if (valeur != null) {
			String valeurComparaison = valeur;
			if (useLowerCase) {
				valeurComparaison = valeurComparaison.toLowerCase();
			}
			String chaineClaire = StringCtrl.chaineNonAccentuee(valeurComparaison, true);
			// Si la chaîne comporte des caractères illégaux ou n'était pas en minuscules, la changer
			if (valeurComparaison.equals(chaineClaire) == false || (useLowerCase && valeur.equals(valeurComparaison) == false)) {
				valeursAttributs.setObjectForKey(chaineClaire, attribut);
			} 
		}
	}

}
