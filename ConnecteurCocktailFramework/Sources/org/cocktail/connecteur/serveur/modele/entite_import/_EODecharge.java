// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODecharge.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODecharge extends ObjetImportPourPersonnel {
	public static final String ENTITY_NAME = "Decharge";

	// Attributes
	public static final String C_TYPE_DECHARGE_KEY = "cTypeDecharge";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DECH_SOURCE_KEY = "dechSource";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NB_H_DECHARGE_KEY = "nbHDecharge";
	public static final String OPERATION_KEY = "operation";
	public static final String PERIODE_DECHARGE_KEY = "periodeDecharge";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EODecharge.class);

  public EODecharge localInstanceIn(EOEditingContext editingContext) {
    EODecharge localInstance = (EODecharge)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeDecharge() {
    return (String) storedValueForKey("cTypeDecharge");
  }

  public void setCTypeDecharge(String value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating cTypeDecharge from " + cTypeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeDecharge");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer dechSource() {
    return (Integer) storedValueForKey("dechSource");
  }

  public void setDechSource(Integer value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating dechSource from " + dechSource() + " to " + value);
    }
    takeStoredValueForKey(value, "dechSource");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public java.math.BigDecimal nbHDecharge() {
    return (java.math.BigDecimal) storedValueForKey("nbHDecharge");
  }

  public void setNbHDecharge(java.math.BigDecimal value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating nbHDecharge from " + nbHDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "nbHDecharge");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String periodeDecharge() {
    return (String) storedValueForKey("periodeDecharge");
  }

  public void setPeriodeDecharge(String value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating periodeDecharge from " + periodeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "periodeDecharge");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
      _EODecharge.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EODecharge createDecharge(EOEditingContext editingContext, String cTypeDecharge
, NSTimestamp dCreation
, Integer dechSource
, NSTimestamp dModification
, Integer idSource
, java.math.BigDecimal nbHDecharge
, String periodeDecharge
, String statut
, String temImport
) {
    EODecharge eo = (EODecharge) EOUtilities.createAndInsertInstance(editingContext, _EODecharge.ENTITY_NAME);    
		eo.setCTypeDecharge(cTypeDecharge);
		eo.setDCreation(dCreation);
		eo.setDechSource(dechSource);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setNbHDecharge(nbHDecharge);
		eo.setPeriodeDecharge(periodeDecharge);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EODecharge> fetchAllDecharges(EOEditingContext editingContext) {
    return _EODecharge.fetchAllDecharges(editingContext, null);
  }

  public static NSArray<EODecharge> fetchAllDecharges(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODecharge.fetchDecharges(editingContext, null, sortOrderings);
  }

  public static NSArray<EODecharge> fetchDecharges(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODecharge.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODecharge> eoObjects = (NSArray<EODecharge>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODecharge fetchDecharge(EOEditingContext editingContext, String keyName, Object value) {
    return _EODecharge.fetchDecharge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODecharge fetchDecharge(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODecharge> eoObjects = _EODecharge.fetchDecharges(editingContext, qualifier, null);
    EODecharge eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODecharge)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Decharge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODecharge fetchRequiredDecharge(EOEditingContext editingContext, String keyName, Object value) {
    return _EODecharge.fetchRequiredDecharge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODecharge fetchRequiredDecharge(EOEditingContext editingContext, EOQualifier qualifier) {
    EODecharge eoObject = _EODecharge.fetchDecharge(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Decharge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODecharge localInstanceIn(EOEditingContext editingContext, EODecharge eo) {
    EODecharge localInstance = (eo == null) ? null : (EODecharge)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
