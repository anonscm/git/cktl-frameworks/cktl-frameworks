package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAnciennete;
import org.cocktail.connecteur.serveur.modele.correspondance.EODeclarationAccidentCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCgntAccidentTrav;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOCgntAccidentTrav extends _EOCgntAccidentTrav {
	private static final long serialVersionUID = 6397231286276851123L;

	public void preparerRelations() {
		super.preparerRelations();
		if (editingContext() != null && daccSource() != null && individu() != null) {
			EODeclarationAccident declAccident = EODeclarationAccidentCorresp.declarationAccidentPourIndividuEtId(editingContext(), idSource(), daccSource());
			setDeclarationAccidentRelationship(declAccident);
		}
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setDeclarationAccidentRelationship(null);
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		
		String valeur = (String)valeursAttributs.objectForKey(C_ANCIENNETE_KEY);
		if (valeur != null) {
			EOAnciennete anciennete = EOAnciennete.fetchParamCgntAccTrav(editingContext, EOAnciennete.C_ANCIENNETE_KEY, valeur);
			if (anciennete == null) {
				resultat = resultat + "Type_Anciennete_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}

		resultat = resultat + verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);
		return resultat;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CGNT_ACCIDENT_TRAV_KEY;
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log == null) {
			if (declarationAccident() == null && daccSource() != null) {
				EODeclarationAccident declAccident = EODeclarationAccidentCorresp.declarationAccidentPourIndividuEtId(editingContext(), idSource(), daccSource());
				if (declAccident != null) {
					setDeclarationAccidentRelationship(declAccident);
				}
				String messageLog = null;
				if (declAccident == null) {
					messageLog = "DECLARATION_ACCIDENT_NON_IMPORTEE";
				} else if (declAccident.temImport().equals(ObjetImport.A_TRANSFERER)) {
					if (declAccident.statut().equals(EODeclarationAccident.STATUT_ERREUR)) {
						messageLog = "DECLARATION_ACCIDENT_INVALIDE";
					}
				}
				if (messageLog != null) {
					log = new EOLogImport();
					log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
					log.ajouterRelation(this);
					log.setEimpSource(eimpSource());
					if (declarationAccident() != null) {
						log.setDeclarationAccidentRelationship(declarationAccident());
					}
					setStatut(STATUT_ERREUR);
				}
			}
		}
		return log;
	}

	public EOLogImport verifierRecordApresOperation() {
		String message = null;
		EOLogImport log = super.verifierRecord();

		if (log == null) {

			message = verifierDatesAccidentTrav();

			if (message != null) {
				LogManager.logDetail("Insertion d'un log pour les erreurs sur les accidents de travail des non titulaires");
				log = new EOLogImport();
				log.initAvecEditingContext(editingContextTravail(),this.entityName(), message);
				log.ajouterRelation(this);
				log.setEimpSource(eimpSource());
				setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			}
		}

		if (log != null) {
			log.setEimpSource(catSource());
		}
		return log;
	}

	//Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCgntAccidentTrav.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}
	
	// Méthodes privées
	private String verifierDatesAccidentTrav() {
		if (dateFin() != null && DateCtrl.isBefore(dateFin(), dateDebut())) {
			return "DATES_DETAIL_ACC_TRAV";
		}
		if (dDebPleinTrait() != null) {
			if (dFinPleinTrait() != null && DateCtrl.isBefore(dFinPleinTrait(), dateDebut())) {
				return "DATES_PLEIN_TRAIT_ACC_TRAV";
			}
		}
		return null;
	}

	@Override
	public Integer eimpSource() {
		return catSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCatSource(value);
	}

}
