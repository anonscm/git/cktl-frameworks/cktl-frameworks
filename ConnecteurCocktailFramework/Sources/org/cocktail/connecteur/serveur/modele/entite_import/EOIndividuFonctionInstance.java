package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOFonctionRelevantInstance;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOQualiteIndividu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSousStatutIndividu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOStatutIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumInstance;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class EOIndividuFonctionInstance extends _EOIndividuFonctionInstance {
	@Override
	public Integer eimpSource() {
		return ifiSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setIfiSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.INDIVIDU_FONCTION_INSTANCE_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		Integer codeFonction = (Integer) valeursAttributs.objectForKey(C_FONCTION_KEY);
		if (codeFonction != null) {
			EOFonctionRelevantInstance fonction = EOFonctionRelevantInstance.getFromCode(editingContext, codeFonction);
			if (fonction == null) {
				resultat = resultat + "CodeFonction_Inconnu >>" + codeFonction + System.getProperty("line.separator");
			}
		}

		String codeInstance = (String) valeursAttributs.objectForKey(C_INSTANCE_KEY);
		if (codeInstance != null) {
			EOGrhumInstance instance = EOGrhumInstance.getFromCode(editingContext, codeInstance);
			if (instance == null) {
				resultat = resultat + "CodeInstance_Inconnu >>" + codeInstance + System.getProperty("line.separator");
			}
		}

		String codeQualite = (String) valeursAttributs.objectForKey(C_QUALITE_KEY);
		if (codeQualite != null) {
			EOQualiteIndividu qualite = EOQualiteIndividu.getFromCode(editingContext, codeQualite);
			if (qualite == null) {
				resultat = resultat + "CodeQualite_Inconnu >>" + codeQualite + System.getProperty("line.separator");
			}
		}

		String codeStatut = (String) valeursAttributs.objectForKey(C_STATUT_KEY);
		if (codeStatut != null) {
			EOStatutIndividu statut = EOStatutIndividu.getFromCode(editingContext, codeStatut);
			if (statut == null) {
				resultat = resultat + "CodeStatut_Inconnu >>" + codeStatut + System.getProperty("line.separator");
			}
		}

		String codeSsStatut = (String) valeursAttributs.objectForKey(C_SS_STATUT_KEY);
		if (codeSsStatut != null) {
			EOSousStatutIndividu fonction = EOSousStatutIndividu.getFromStatutSsStatut(editingContext, codeStatut, codeSsStatut);
			if (fonction == null) {
				resultat = resultat + "CodeSousStatut_Inconnu >>" + codeSsStatut + System.getProperty("line.separator");
			}
		}

		return resultat;
	}
}
