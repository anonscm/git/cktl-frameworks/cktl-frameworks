// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCrct.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCrct extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Crct";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_ORIGINE_DEMANDE_KEY = "cOrigineDemande";
	public static final String CRCT_SOURCE_KEY = "crctSource";
	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_SERVICE_KEY = "quotiteService";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_FRACTIONNEMENT_KEY = "temFractionnement";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCrct.class);

  public EOCrct localInstanceIn(EOEditingContext editingContext) {
    EOCrct localInstance = (EOCrct)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cOrigineDemande() {
    return (String) storedValueForKey("cOrigineDemande");
  }

  public void setCOrigineDemande(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating cOrigineDemande from " + cOrigineDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "cOrigineDemande");
  }

  public Integer crctSource() {
    return (Integer) storedValueForKey("crctSource");
  }

  public void setCrctSource(Integer value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating crctSource from " + crctSource() + " to " + value);
    }
    takeStoredValueForKey(value, "crctSource");
  }

  public String cSectionCnu() {
    return (String) storedValueForKey("cSectionCnu");
  }

  public void setCSectionCnu(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating cSectionCnu from " + cSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSectionCnu");
  }

  public String cSousSectionCnu() {
    return (String) storedValueForKey("cSousSectionCnu");
  }

  public void setCSousSectionCnu(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating cSousSectionCnu from " + cSousSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSousSectionCnu");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public java.math.BigDecimal quotiteService() {
    return (java.math.BigDecimal) storedValueForKey("quotiteService");
  }

  public void setQuotiteService(java.math.BigDecimal value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating quotiteService from " + quotiteService() + " to " + value);
    }
    takeStoredValueForKey(value, "quotiteService");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temFractionnement() {
    return (String) storedValueForKey("temFractionnement");
  }

  public void setTemFractionnement(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating temFractionnement from " + temFractionnement() + " to " + value);
    }
    takeStoredValueForKey(value, "temFractionnement");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
    	_EOCrct.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCrct.LOG.isDebugEnabled()) {
      _EOCrct.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCrct createCrct(EOEditingContext editingContext, String cOrigineDemande
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temConfirme
, String temFractionnement
, String temGestEtab
, String temImport
, String temValide
) {
    EOCrct eo = (EOCrct) EOUtilities.createAndInsertInstance(editingContext, _EOCrct.ENTITY_NAME);    
		eo.setCOrigineDemande(cOrigineDemande);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemFractionnement(temFractionnement);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCrct> fetchAllCrcts(EOEditingContext editingContext) {
    return _EOCrct.fetchAllCrcts(editingContext, null);
  }

  public static NSArray<EOCrct> fetchAllCrcts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCrct.fetchCrcts(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCrct> fetchCrcts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCrct.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCrct> eoObjects = (NSArray<EOCrct>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCrct fetchCrct(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrct.fetchCrct(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrct fetchCrct(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCrct> eoObjects = _EOCrct.fetchCrcts(editingContext, qualifier, null);
    EOCrct eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCrct)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Crct that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrct fetchRequiredCrct(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrct.fetchRequiredCrct(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrct fetchRequiredCrct(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCrct eoObject = _EOCrct.fetchCrct(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Crct that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrct localInstanceIn(EOEditingContext editingContext, EOCrct eo) {
    EOCrct localInstance = (eo == null) ? null : (EOCrct)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
