//EORib.java
//Created on Wed Jul 25 15:06:02 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import java.math.BigInteger;

import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRib;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Les banques sont automatiquement creees a la volee si elles n'existent pas et que le code banque-guichet ou le bic ainsi que la domiciliation sont fournis
 * 
 * @author christine
 * 
 */
// 22/06/2010 - modifié pour prendre en compte le bic et la validation de
// l'iban. Les domiciliations sont mises en majuscules, sans
// caractère accentué
public class EORib extends _EORib {
	private final static int LONGUEUR_BANQUE = 5;
	private final static int LONGUEUR_GUICHET = 5;
	private final static int LONGUEUR_COMPTE = 11;
	private final static int LONGUEUR_CLE = 2;

	public EORib() {
		super();
	}

	private void preparerAdresseFournisseur() {
		Integer persId = null;
		if (individu() != null) {
			EOGrhumIndividu grhumIndividu = EOIndividuCorresp.individuGrhum(editingContext(), individu());
			if (grhumIndividu != null)
				persId = grhumIndividu.persId();
		}
		if (structure() != null) {
			EOGrhumStructure grhumStructure = EOStructureCorresp.structureGrhum(editingContext(), strSource());
			if (grhumStructure != null)
				persId = grhumStructure.persId();
		}
		if (persId == null)
			return;

		EOGrhumAdresse adresseFournisseur = null;
		EORepartPersonneAdresse repartAdresseFournisseur = null;
		NSArray adresses = EORepartPersonneAdresse.rechercherRepartsDeType(editingContext(), persId, EORepartPersonneAdresse.TYPE_FACTURATION);
		if (adresses != null && adresses.count() > 0) {
			repartAdresseFournisseur = (EORepartPersonneAdresse) adresses.get(0);
			adresseFournisseur = repartAdresseFournisseur.adresse();
		} else {
			adresses = EORepartPersonneAdresse.rechercherRepartsDeType(editingContext(), persId, EORepartPersonneAdresse.TYPE_PERSO);
			if (adresses != null && adresses.count() > 0) {
				EORepartPersonneAdresse repartAdressePerso = (EORepartPersonneAdresse) adresses.get(0);
				EOGrhumAdresse adressePerso = repartAdressePerso.adresse();

				NSTimestamp dateActuelle = new NSTimestamp();
				// On crée un nouveau type FACTURE pour l'adresse perso
				repartAdresseFournisseur = new EORepartPersonneAdresse();
				repartAdresseFournisseur.setAdresseRelationship(repartAdressePerso.adresse());
				repartAdresseFournisseur.setAdrOrdre(repartAdressePerso.adrOrdre());
				repartAdresseFournisseur.setPersId(repartAdressePerso.persId());
				repartAdresseFournisseur.setTadrCode(EORepartPersonneAdresse.TYPE_FACTURATION);
				repartAdresseFournisseur.setEMail(repartAdressePerso.eMail());
				repartAdresseFournisseur.setRpaPrincipal(repartAdressePerso.rpaPrincipal());
				repartAdresseFournisseur.setEstValide(repartAdressePerso.estValide());
				repartAdresseFournisseur.setDCreation(dateActuelle);
				repartAdresseFournisseur.setDModification(dateActuelle);

				editingContext().insertObject(repartAdresseFournisseur);

				adresseFournisseur = repartAdresseFournisseur.adresse();
			}
		}
		setGrhumAdresseFournisseurRelationship(adresseFournisseur);
	}

	@Override
	public void preparerRelations() {
		super.preparerRelations();
		EOBanque banque = EOBanque.fetchBanquePourCodeBanqueGuichetIbanBic(editingContext(), cBanque(), cGuichet(), iban(), bic());
		setBanqueRelationship(banque);

		preparerAdresseFournisseur();
	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setBanqueRelationship(null);
		setGrhumAdresseFournisseurRelationship(null);
	}

	/** Pas de verification */
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log = null;

		if (idSource() != null && grhumAdresseFournisseur() == null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContext(), this.entityName(), "RIB_ADRESSE_FACT_PERSO");
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	/**
	 * retourne un rib du SI destinataire avec la m&ecirc;me banque, les m&ecirc;mes guichets et numero de compte
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null || structure() != null) {
			if (estNouvelIndividuOuStructure()) {
				return null;
			} else {
				return EOGrhumRib.ribDestinationPourRib(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	/** retourne le nom de la relation pour retrouver les logs lies au record */
	public String nomRelationPourLog() {
		return EOLogImport.RIB_KEY;
	}

	// Méthodes protégées
	protected boolean doitAvoirIndividuEtStructure() {
		return false;
	}

	// Méthodes statiques
	/**
	 * Valide les donnees d'un rib.<BR>
	 * Verifie que la banque, le guichet le numero de compte sont corrects et met le numero de compte en majuscules. Si les longueurs ne sont pas valides, padde
	 * avec des zeros a gauche et verifie la longueur totale.<BR>
	 * Verifie la cle rib en fonction de la banque, du guichet et du numero de compte.<BR>
	 * Si la banque et le guichet sont inconnus dans Grhum et que la domiciliation est fournie, on cree une nouvelle entre dans Banque qui est sauvee
	 * immediatement dans la base<BR>
	 * 
	 * @param editingContext
	 * @param valeursAttributs
	 *            dictionnaire des attributs a valider ou modifier
	 */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number) valeursAttributs.objectForKey(ID_SOURCE_KEY);
		String strSource = (String) valeursAttributs.objectForKey(STR_SOURCE_KEY);
		if (idSource == null && strSource == null) {
			resultat = resultat + "ID_SOURCE_ou_STR_SOURCE >>" + System.getProperty("line.separator");
		} else if (idSource != null && strSource != null) {
			resultat = resultat + "UNE_SEULE_ID >>" + System.getProperty("line.separator");
		}
		// Forcer le noCompte en majuscules
		String noCompte = (String) valeursAttributs.objectForKey(NO_COMPTE_KEY);
		if (noCompte != null) {
			noCompte = noCompte.toUpperCase();
			if (noCompte.length() != LONGUEUR_COMPTE) {
				noCompte = StringCtrl.stringCompletion(noCompte, LONGUEUR_COMPTE, "0", "G");
			}
			valeursAttributs.setObjectForKey(noCompte, NO_COMPTE_KEY);
		}
		String banque = (String) valeursAttributs.objectForKey(C_BANQUE_KEY);
		if (banque != null && banque.length() < LONGUEUR_BANQUE) {
			banque = StringCtrl.stringCompletion(banque, LONGUEUR_BANQUE, "0", "G");
			valeursAttributs.setObjectForKey(banque, C_BANQUE_KEY);
		}
		String guichet = (String) valeursAttributs.objectForKey(C_GUICHET_KEY);
		if (guichet != null && guichet.length() < LONGUEUR_GUICHET) {
			guichet = StringCtrl.stringCompletion(guichet, LONGUEUR_GUICHET, "0", "G");
			valeursAttributs.setObjectForKey(guichet, C_GUICHET_KEY);
		}
		String bic = (String) valeursAttributs.objectForKey(BIC_KEY);
		String iban = (String) valeursAttributs.objectForKey(IBAN_KEY);
		// Vérifier que code banque-guichet ou bic sont non nuls
		boolean couple_guichet_banque_ou_iban_bic_ok = (banque != null && guichet != null) || (bic != null && iban != null);
		if (!couple_guichet_banque_ou_iban_bic_ok) {
			resultat = resultat + "Ni_Banque/Guichet_Ni_IBAN/BIC >>" + System.getProperty("line.separator");
		}

		// 24/06/2010 - mise de la domiciliation en majuscule
		String domiciliation = (String) valeursAttributs.objectForKey(DOMICILIATION_KEY);
		if (domiciliation != null) {
			domiciliation = StringCtrl.chaineClaire(domiciliation, true).toUpperCase();
			valeursAttributs.setObjectForKey(domiciliation, DOMICILIATION_KEY);
		}
		if (couple_guichet_banque_ou_iban_bic_ok && !banqueValide(editingContext, banque, guichet, iban, bic)) {
			resultat = resultat + "Banque_Inconnue >>" + System.getProperty("line.separator");
		}
		String cleRib = (String) valeursAttributs.objectForKey(CLE_KEY);
		if (banque != null && guichet != null && noCompte != null && cleRib != null) {
			if (cleRib.length() == 1) {
				// Peut-être manque-t-il un zéro suite à une conversion
				cleRib = "0" + cleRib;
				valeursAttributs.setObjectForKey(cleRib, CLE_KEY);
			}
			if ((noCompte.length() + cleRib.length() + banque.length() + guichet.length()) != LONGUEUR_BANQUE + LONGUEUR_GUICHET + LONGUEUR_COMPTE
					+ LONGUEUR_CLE) {
				resultat = resultat + "Rib_Incomplet >>" + System.getProperty("line.separator");
			} else if (!EORib.donneesRibValides(banque, guichet, noCompte, cleRib)) {
				resultat = resultat + "Rib_Invalide >>" + System.getProperty("line.separator");
			}
		}
		if (iban != null && isIbanValide(iban) == false) {
			resultat = resultat + "Iban_Invalide >>" + System.getProperty("line.separator");
		}
		return resultat;
	}

	private static boolean banqueValide(EOEditingContext editingContext, String codeBanque, String codeGuichet, String iban, String bic) {
		if (iban != null && !EOBanque.estIbanFrancais(iban))
			return true;
		// On ne vérifie que les banques françaises.
		return EOBanque.fetchBanquePourCodeBanqueGuichetIbanBic(editingContext, codeBanque, codeGuichet, iban, bic) != null;

	}

	/**
	 * Validation du RIB en fonction du code banque, code guichet, N° compte et cle compte
	 */
	private static boolean donneesRibValides(String codeBanque, String codeGuichet, String noCompte, String cleCompte) {
		String chiffres = "";

		for (int i = 0; i < noCompte.length(); i++) {
			String car = noCompte.substring(i, i + 1);
			if (("A".equals(car)) || ("J".equals(car)))
				chiffres = chiffres + "1";
			else if (("B".equals(car)) || ("K".equals(car)) || ("S".equals(car)))
				chiffres = chiffres + "2";
			else if (("C".equals(car)) || ("L".equals(car)) || ("T".equals(car)))
				chiffres = chiffres + "3";
			else if (("D".equals(car)) || ("M".equals(car)) || ("U".equals(car)))
				chiffres = chiffres + "4";
			else if (("E".equals(car)) || ("N".equals(car)) || ("V".equals(car)))
				chiffres = chiffres + "5";
			else if (("F".equals(car)) || ("O".equals(car)) || ("W".equals(car)))
				chiffres = chiffres + "6";
			else if (("G".equals(car)) || ("P".equals(car)) || ("X".equals(car)))
				chiffres = chiffres + "7";
			else if (("H".equals(car)) || ("Q".equals(car)) || ("Y".equals(car)))
				chiffres = chiffres + "8";
			else if (("I".equals(car)) || ("R".equals(car)) || ("Z".equals(car)))
				chiffres = chiffres + "9";
			else
				chiffres = chiffres + car;
		}

		String s = codeBanque + codeGuichet + chiffres + cleCompte;
		BigInteger bigInt = new BigInteger(s);
		if ((bigInt.mod(new BigInteger("97"))).intValue() != 0)
			return false;

		return true;
	}

	private static boolean isIbanValide(String iban) {
		String temp = iban.toUpperCase().replaceAll(" ", "");
		// Conversion des lettres du code Pays en chiffre
		String codePays = conversionPourIban(temp.substring(0, 2));
		String clefIban = temp.substring(2, 4);
		String bban = conversionPourIban(temp.substring(4));
		String s = bban + codePays + "00";
		BigInteger bigInt = new BigInteger(s);

		String clefFinale = "";
		int clefCalculee = 98 - (bigInt.mod(new BigInteger("97"))).intValue();
		if (clefCalculee < 10) {
			clefFinale = "0" + new Integer(clefCalculee).toString();
		} else {
			clefFinale = new Integer(clefCalculee).toString();
		}
		return clefFinale.equals(clefIban);
	}

	private static String conversionPourIban(String aStr) {
		byte[] bytes = aStr.getBytes();
		String temp = "";
		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];
			if (b >= 65 && b <= 90) {
				int val = 10 + b - 65;
				temp += new Integer(val).toString();
			} else {
				temp += new String(new byte[] { b });
			}
		}
		return temp;
	}
}
