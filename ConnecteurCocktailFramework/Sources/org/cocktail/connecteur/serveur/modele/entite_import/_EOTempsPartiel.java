// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTempsPartiel.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTempsPartiel extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "TempsPartiel";

	// Attributes
	public static final String ARRETE_ANNULATION_SOURCE_KEY = "arreteAnnulationSource";
	public static final String C_MOTIF_TEMPS_PARTIEL_KEY = "cMotifTempsPartiel";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEN_QUOTITE_KEY = "denQuotite";
	public static final String D_FIN_EXECUTION_KEY = "dFinExecution";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ENF_SOURCE_KEY = "enfSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String OPERATION_KEY = "operation";
	public static final String PERIODICITE_KEY = "periodicite";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_REPRISE_TEMPS_PLEIN_KEY = "temRepriseTempsPlein";
	public static final String TEM_SURCOTISATION_KEY = "temSurcotisation";
	public static final String TP_SOURCE_KEY = "tpSource";

	// Relationships
	public static final String ARRETE_ANNULATION_KEY = "arreteAnnulation";
	public static final String ENFANT_KEY = "enfant";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOTempsPartiel.class);

  public EOTempsPartiel localInstanceIn(EOEditingContext editingContext) {
    EOTempsPartiel localInstance = (EOTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer arreteAnnulationSource() {
    return (Integer) storedValueForKey("arreteAnnulationSource");
  }

  public void setArreteAnnulationSource(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating arreteAnnulationSource from " + arreteAnnulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "arreteAnnulationSource");
  }

  public String cMotifTempsPartiel() {
    return (String) storedValueForKey("cMotifTempsPartiel");
  }

  public void setCMotifTempsPartiel(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating cMotifTempsPartiel from " + cMotifTempsPartiel() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifTempsPartiel");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Double denQuotite() {
    return (Double) storedValueForKey("denQuotite");
  }

  public void setDenQuotite(Double value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating denQuotite from " + denQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotite");
  }

  public NSTimestamp dFinExecution() {
    return (NSTimestamp) storedValueForKey("dFinExecution");
  }

  public void setDFinExecution(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dFinExecution from " + dFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinExecution");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer enfSource() {
    return (Integer) storedValueForKey("enfSource");
  }

  public void setEnfSource(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating enfSource from " + enfSource() + " to " + value);
    }
    takeStoredValueForKey(value, "enfSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public Double numQuotite() {
    return (Double) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Double value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer periodicite() {
    return (Integer) storedValueForKey("periodicite");
  }

  public void setPeriodicite(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating periodicite from " + periodicite() + " to " + value);
    }
    takeStoredValueForKey(value, "periodicite");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temRepriseTempsPlein() {
    return (String) storedValueForKey("temRepriseTempsPlein");
  }

  public void setTemRepriseTempsPlein(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temRepriseTempsPlein from " + temRepriseTempsPlein() + " to " + value);
    }
    takeStoredValueForKey(value, "temRepriseTempsPlein");
  }

  public String temSurcotisation() {
    return (String) storedValueForKey("temSurcotisation");
  }

  public void setTemSurcotisation(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temSurcotisation from " + temSurcotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "temSurcotisation");
  }

  public Integer tpSource() {
    return (Integer) storedValueForKey("tpSource");
  }

  public void setTpSource(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating tpSource from " + tpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "tpSource");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel arreteAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel)storedValueForKey("arreteAnnulation");
  }

  public void setArreteAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
      _EOTempsPartiel.LOG.debug("updating arreteAnnulation from " + arreteAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel oldValue = arreteAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "arreteAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "arreteAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant enfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("enfant");
  }

  public void setEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
      _EOTempsPartiel.LOG.debug("updating enfant from " + enfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
      _EOTempsPartiel.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOTempsPartiel createTempsPartiel(EOEditingContext editingContext, String cMotifTempsPartiel
, NSTimestamp dateDebut
, NSTimestamp dCreation
, Double denQuotite
, NSTimestamp dModification
, Integer idSource
, Double numQuotite
, String statut
, String temConfirme
, String temGestEtab
, String temImport
, String temRepriseTempsPlein
, String temSurcotisation
) {
    EOTempsPartiel eo = (EOTempsPartiel) EOUtilities.createAndInsertInstance(editingContext, _EOTempsPartiel.ENTITY_NAME);    
		eo.setCMotifTempsPartiel(cMotifTempsPartiel);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDenQuotite(denQuotite);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setNumQuotite(numQuotite);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemRepriseTempsPlein(temRepriseTempsPlein);
		eo.setTemSurcotisation(temSurcotisation);
    return eo;
  }

  public static NSArray<EOTempsPartiel> fetchAllTempsPartiels(EOEditingContext editingContext) {
    return _EOTempsPartiel.fetchAllTempsPartiels(editingContext, null);
  }

  public static NSArray<EOTempsPartiel> fetchAllTempsPartiels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTempsPartiel.fetchTempsPartiels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTempsPartiel> fetchTempsPartiels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTempsPartiel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTempsPartiel> eoObjects = (NSArray<EOTempsPartiel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTempsPartiel fetchTempsPartiel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartiel.fetchTempsPartiel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartiel fetchTempsPartiel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTempsPartiel> eoObjects = _EOTempsPartiel.fetchTempsPartiels(editingContext, qualifier, null);
    EOTempsPartiel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTempsPartiel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartiel fetchRequiredTempsPartiel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartiel.fetchRequiredTempsPartiel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartiel fetchRequiredTempsPartiel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTempsPartiel eoObject = _EOTempsPartiel.fetchTempsPartiel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartiel localInstanceIn(EOEditingContext editingContext, EOTempsPartiel eo) {
    EOTempsPartiel localInstance = (eo == null) ? null : (EOTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
