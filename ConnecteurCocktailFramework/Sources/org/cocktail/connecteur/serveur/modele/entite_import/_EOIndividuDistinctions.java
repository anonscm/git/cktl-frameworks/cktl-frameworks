// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuDistinctions.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuDistinctions extends ObjetImportPourIndividu {
	public static final String ENTITY_NAME = "IndividuDistinctions";

	// Attributes
	public static final String C_DISTINCTION_KEY = "cDistinction";
	public static final String C_DISTINCTION_NIVEAU_KEY = "cDistinctionNiveau";
	public static final String DATE_DISTINCTION_KEY = "dateDistinction";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DIST_SOURCE_KEY = "distSource";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_DISTINCTION_KEY = "toDistinction";
	public static final String TO_DISTINCTION_NIVEAU_KEY = "toDistinctionNiveau";

  private static Logger LOG = Logger.getLogger(_EOIndividuDistinctions.class);

  public EOIndividuDistinctions localInstanceIn(EOEditingContext editingContext) {
    EOIndividuDistinctions localInstance = (EOIndividuDistinctions)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDistinction() {
    return (String) storedValueForKey("cDistinction");
  }

  public void setCDistinction(String value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating cDistinction from " + cDistinction() + " to " + value);
    }
    takeStoredValueForKey(value, "cDistinction");
  }

  public Integer cDistinctionNiveau() {
    return (Integer) storedValueForKey("cDistinctionNiveau");
  }

  public void setCDistinctionNiveau(Integer value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating cDistinctionNiveau from " + cDistinctionNiveau() + " to " + value);
    }
    takeStoredValueForKey(value, "cDistinctionNiveau");
  }

  public NSTimestamp dateDistinction() {
    return (NSTimestamp) storedValueForKey("dateDistinction");
  }

  public void setDateDistinction(NSTimestamp value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating dateDistinction from " + dateDistinction() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDistinction");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer distSource() {
    return (Integer) storedValueForKey("distSource");
  }

  public void setDistSource(Integer value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating distSource from " + distSource() + " to " + value);
    }
    takeStoredValueForKey(value, "distSource");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctions.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
      _EOIndividuDistinctions.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction toDistinction() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction)storedValueForKey("toDistinction");
  }

  public void setToDistinctionRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
      _EOIndividuDistinctions.LOG.debug("updating toDistinction from " + toDistinction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction oldValue = toDistinction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDistinction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDistinction");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau toDistinctionNiveau() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau)storedValueForKey("toDistinctionNiveau");
  }

  public void setToDistinctionNiveauRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau value) {
    if (_EOIndividuDistinctions.LOG.isDebugEnabled()) {
      _EOIndividuDistinctions.LOG.debug("updating toDistinctionNiveau from " + toDistinctionNiveau() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau oldValue = toDistinctionNiveau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDistinctionNiveau");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDistinctionNiveau");
    }
  }
  

  public static EOIndividuDistinctions createIndividuDistinctions(EOEditingContext editingContext, NSTimestamp dateDistinction
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EOIndividuDistinctions eo = (EOIndividuDistinctions) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuDistinctions.ENTITY_NAME);    
		eo.setDateDistinction(dateDistinction);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOIndividuDistinctions> fetchAllIndividuDistinctionses(EOEditingContext editingContext) {
    return _EOIndividuDistinctions.fetchAllIndividuDistinctionses(editingContext, null);
  }

  public static NSArray<EOIndividuDistinctions> fetchAllIndividuDistinctionses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuDistinctions.fetchIndividuDistinctionses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuDistinctions> fetchIndividuDistinctionses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuDistinctions.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuDistinctions> eoObjects = (NSArray<EOIndividuDistinctions>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuDistinctions fetchIndividuDistinctions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDistinctions.fetchIndividuDistinctions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDistinctions fetchIndividuDistinctions(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuDistinctions> eoObjects = _EOIndividuDistinctions.fetchIndividuDistinctionses(editingContext, qualifier, null);
    EOIndividuDistinctions eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuDistinctions)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuDistinctions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDistinctions fetchRequiredIndividuDistinctions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDistinctions.fetchRequiredIndividuDistinctions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDistinctions fetchRequiredIndividuDistinctions(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuDistinctions eoObject = _EOIndividuDistinctions.fetchIndividuDistinctions(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuDistinctions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDistinctions localInstanceIn(EOEditingContext editingContext, EOIndividuDistinctions eo) {
    EOIndividuDistinctions localInstance = (eo == null) ? null : (EOIndividuDistinctions)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
