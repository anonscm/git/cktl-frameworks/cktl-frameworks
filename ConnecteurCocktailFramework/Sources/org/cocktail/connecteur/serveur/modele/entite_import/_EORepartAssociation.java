// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartAssociation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartAssociation extends ObjetImport {
	public static final String ENTITY_NAME = "RepartAssociation";

	// Attributes
	public static final String ASS_CODE_KEY = "assCode";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_KEY = "quotite";
	public static final String RANG_KEY = "rang";
	public static final String RAS_SOURCE_KEY = "rasSource";
	public static final String STATUT_KEY = "statut";
	public static final String STR2_SOURCE_KEY = "str2Source";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PRINCIPALE_KEY = "temPrincipale";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE2_KEY = "structure2";

  private static Logger LOG = Logger.getLogger(_EORepartAssociation.class);

  public EORepartAssociation localInstanceIn(EOEditingContext editingContext) {
    EORepartAssociation localInstance = (EORepartAssociation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String assCode() {
    return (String) storedValueForKey("assCode");
  }

  public void setAssCode(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating assCode from " + assCode() + " to " + value);
    }
    takeStoredValueForKey(value, "assCode");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey("dOuverture");
  }

  public void setDOuverture(NSTimestamp value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating dOuverture from " + dOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dOuverture");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Double quotite() {
    return (Double) storedValueForKey("quotite");
  }

  public void setQuotite(Double value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public Integer rang() {
    return (Integer) storedValueForKey("rang");
  }

  public void setRang(Integer value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating rang from " + rang() + " to " + value);
    }
    takeStoredValueForKey(value, "rang");
  }

  public Integer rasSource() {
    return (Integer) storedValueForKey("rasSource");
  }

  public void setRasSource(Integer value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating rasSource from " + rasSource() + " to " + value);
    }
    takeStoredValueForKey(value, "rasSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String str2Source() {
    return (String) storedValueForKey("str2Source");
  }

  public void setStr2Source(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating str2Source from " + str2Source() + " to " + value);
    }
    takeStoredValueForKey(value, "str2Source");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPrincipale() {
    return (String) storedValueForKey("temPrincipale");
  }

  public void setTemPrincipale(String value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
    	_EORepartAssociation.LOG.debug( "updating temPrincipale from " + temPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipale");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
      _EORepartAssociation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
      _EORepartAssociation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure2() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure2");
  }

  public void setStructure2Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EORepartAssociation.LOG.isDebugEnabled()) {
      _EORepartAssociation.LOG.debug("updating structure2 from " + structure2() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure2();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure2");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure2");
    }
  }
  

  public static EORepartAssociation createRepartAssociation(EOEditingContext editingContext, String assCode
, NSTimestamp dCreation
, NSTimestamp dModification
, String operation
, Integer rasSource
, String statut
, String strSource
, String temImport
, String temPrincipale
) {
    EORepartAssociation eo = (EORepartAssociation) EOUtilities.createAndInsertInstance(editingContext, _EORepartAssociation.ENTITY_NAME);    
		eo.setAssCode(assCode);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setOperation(operation);
		eo.setRasSource(rasSource);
		eo.setStatut(statut);
		eo.setStrSource(strSource);
		eo.setTemImport(temImport);
		eo.setTemPrincipale(temPrincipale);
    return eo;
  }

  public static NSArray<EORepartAssociation> fetchAllRepartAssociations(EOEditingContext editingContext) {
    return _EORepartAssociation.fetchAllRepartAssociations(editingContext, null);
  }

  public static NSArray<EORepartAssociation> fetchAllRepartAssociations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartAssociation.fetchRepartAssociations(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartAssociation> fetchRepartAssociations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartAssociation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartAssociation> eoObjects = (NSArray<EORepartAssociation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartAssociation fetchRepartAssociation(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAssociation.fetchRepartAssociation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartAssociation fetchRepartAssociation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartAssociation> eoObjects = _EORepartAssociation.fetchRepartAssociations(editingContext, qualifier, null);
    EORepartAssociation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartAssociation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RepartAssociation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartAssociation fetchRequiredRepartAssociation(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAssociation.fetchRequiredRepartAssociation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartAssociation fetchRequiredRepartAssociation(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartAssociation eoObject = _EORepartAssociation.fetchRepartAssociation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RepartAssociation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartAssociation localInstanceIn(EOEditingContext editingContext, EORepartAssociation eo) {
    EORepartAssociation localInstance = (eo == null) ? null : (EORepartAssociation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
