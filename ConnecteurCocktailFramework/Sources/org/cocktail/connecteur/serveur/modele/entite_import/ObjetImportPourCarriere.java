/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class ObjetImportPourCarriere extends ObjetImportPourPersonnel {

	public abstract EOCarriere toCarriere();

	public abstract void setToCarriereRelationship(EOCarriere value);

	public abstract Integer carSource();

	public void preparerRelations() {
		super.preparerRelations();

		EOCarriere carriere = EOCarriere.carrierePourSourceId(editingContext(), idSource(), carSource());
		setToCarriereRelationship(carriere);
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setToCarriereRelationship(null);
	}

	/**
	 * Verifie que les relations du record sont bien valides : il se peut que
	 * l'individu ait ete cree juste auparavant. Retourne un message de log ou
	 * null si pas d'erreur
	 */
	public EOLogImport verifierRecord() {

		EOLogImport logImport = super.verifierRecord();

		if (logImport != null) {
			return logImport;
		}

		if (carSource() == null)
			return null;

		String messageLog = null;

		if (toCarriere() == null) {
			messageLog = "CARRIERE_NON_IMPORTEE";
		} else if (toCarriere().temImport().equals(ObjetImport.A_TRANSFERER)) {
			if (toCarriere().statut().equals(EOIndividu.STATUT_ERREUR)) {
				messageLog = "CARRIERE_INVALIDE";
			}
		} else {
			// Carrière déjà importée, vérifier si la carrière du SI
			// Destinataire est valide
			EOCarriereCorresp correspondance = (EOCarriereCorresp) toCarriere().correspondance();
			if (correspondance != null && correspondance.carriereMangue().temValide().equals(CocktailConstantes.FAUX)) {
				messageLog = "CARRIERE_DESTINATAIRE_INVALIDE";
			}
		}
		if (messageLog != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			if (toCarriere() != null) {
				log.setCarriereRelationship(toCarriere());
			}
			setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une erreur
			return log;
		} else {
			return null;
		}
	}

	/** Retourne la relation sur la carriere */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		if (nomAttribut.equals("carSource")) {
			return "toCarriere";
		} else {
			return super.nomRelationPourAttributComparaison(nomAttribut);
		}
	} // Méthodes protégées

	protected boolean estNouvelleCarriere() {
		if (super.estNouvelIndividu()) {
			return true;
		}
		return (toCarriere() == null || toCarriere().operation().equals(ObjetImport.OPERATION_INSERTION));
	}

	/**
	 * Verifie que les dates de debut et fin sont bien a l'interieur de celles
	 * du segment de carriere
	 */
	protected EOLogImport verifierDatesParRapportCarriere(NSTimestamp dateDebut, NSTimestamp dateFin) {
		// Vérifier si on est dans les dates du segments de carrière
		boolean datesOK = true;
		if (toCarriere() == null)
			return null;

		if (DateCtrl.isBefore(dateDebut, toCarriere().dDebCarriere())) {
			datesOK = false;
		}
		if (toCarriere().dFinCarriere() != null) {
			if (dateFin == null) {
				datesOK = false;
			} else if (DateCtrl.isAfter(dateFin, toCarriere().dFinCarriere())) {
				datesOK = false;
			}
		}
		if (!datesOK) {
			LogManager.logDetail("Insertion d'un log pour les dates de " + entityName());
			String message = "debut  : " + DateCtrl.dateToString(dateDebut);
			if (dateFin != null) {
				message += ", fin :" + DateCtrl.dateToString(dateFin);
			}
			message = message + "\nCarriere - debut  : " + DateCtrl.dateToString(toCarriere().dDebCarriere());
			if (toCarriere().dFinCarriere() != null) {
				message += ", fin :" + DateCtrl.dateToString(toCarriere().dFinCarriere());
			}
			LogManager.logDetail(message);
			return preparerLog("DECALAGE_DATES");
		} else {
			return null;
		}
	}

	// Méthodes protégées
	protected EOLogImport preparerLog(String cleMessage) {
		EOLogImport log = new EOLogImport();
		log.initAvecEditingContext(editingContextTravail(), this.entityName(), cleMessage);
		log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
		log.setIdSource(individu().idSource());
		log.setIndividuRelationship(individu());
		log.setCarriereRelationship(toCarriere());
		setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une erreur
		return log;
	}

	// Méthodes statiques
	/** Retourne les objets de type nomEntite lies a une carriere */
	public static NSArray rechercherObjetsImportCourantPourCarriere(String nomEntite, EOCarriere carriere) {
		NSMutableArray args = new NSMutableArray(carriere);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("toCarriere = %@ AND temImport = %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite, qualifier, null);
		return carriere.editingContext().objectsWithFetchSpecification(myFetch);
	}
}
