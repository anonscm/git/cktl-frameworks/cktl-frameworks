package org.cocktail.connecteur.serveur.modele.entite_import.services;

import org.cocktail.common.LogManager;
import org.cocktail.connecteur.serveur.modele.entite_import.InterfaceHomonymie;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IObjetImport;

public class ObjetImportServicesImpl {

	public void determineStatutEtOperation(IObjetImport objImport, IObjetCorresp correspondance) {
		LogManager.logDetail("Début du passage dans ObjetImportServicesImpl.determineStatutServicesImpl");
		if (correspondance != null)	{ // si il existe une entrée dans la table de correspondance pointant sur des données équivalentes
			LogManager.logDetail("Il y a une correspondance donc on sette opération à UPDATE");
			objImport.setOperation(ObjetImport.OPERATION_UPDATE);
		} else if (objImport.dejaDansSIDestinataire()) {
			LogManager.logDetail("Pas de correspondance et présence dans le SI donc opération à CORRESPONDANCE");
			objImport.setOperation(ObjetImport.OPERATION_CORRESPONDANCE);
			determineStatutHomonyme(objImport);
		} else {
			LogManager.logDetail("Pas de correspondance, pas de présence dans le SI donc opération à INSERTION");
			objImport.setOperation(ObjetImport.OPERATION_INSERTION);
			determineStatutHomonyme(objImport);
		}
		LogManager.logDetail("Fin du passage dans ObjetImportServicesImpl.determineStatutServicesImpl");
	}
	
	public void determineStatutHomonyme(IObjetImport objImport) {
		if (objImport instanceof InterfaceHomonymie) {
			LogManager.logDetail("Héritage de l'interface d'Homonymie");
			InterfaceHomonymie objHomonyme = (InterfaceHomonymie)objImport;
			if (objHomonyme.testHomonymeActif() && objHomonyme.aHomonymeDansSIDestinataire()) {
				LogManager.logDetail("Si opération est INSERTION et enregistrement hérite de l'interface d'homonymie et qu'il y a un homonyme dans le SI destinataire alors : statut = H");
				objImport.setStatut(ObjetImport.STATUT_HOMONYME);
			}
		}		
	}

}
