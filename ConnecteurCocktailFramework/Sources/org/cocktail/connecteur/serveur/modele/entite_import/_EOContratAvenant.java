// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOContratAvenant.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOContratAvenant extends ObjetImportPourPersonnel {
	public static final String ENTITY_NAME = "ContratAvenant";

	// Attributes
	public static final String AV_SOURCE_KEY = "avSource";
	public static final String C_BAP_KEY = "cBap";
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_COND_RECRUT_KEY = "cCondRecrut";
	public static final String C_DEA_KEY = "cDea";
	public static final String C_DISC_SECOND_DEGRE_KEY = "cDiscSecondDegre";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String C_SPECIALITE_ITARF_KEY = "cSpecialiteItarf";
	public static final String CTRA_DUREE_VALIDEE_ANNEES_KEY = "ctraDureeValideeAnnees";
	public static final String CTRA_DUREE_VALIDEE_JOURS_KEY = "ctraDureeValideeJours";
	public static final String CTRA_DUREE_VALIDEE_MOIS_KEY = "ctraDureeValideeMois";
	public static final String CTRA_QUOTITE_COTISATION_KEY = "ctraQuotiteCotisation";
	public static final String CTR_SOURCE_KEY = "ctrSource";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CONTRAT_AV_KEY = "dDebContratAv";
	public static final String D_FIN_CONTRAT_AV_KEY = "dFinContratAv";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_REF_CTR_AVENANT_KEY = "dRefCtrAvenant";
	public static final String D_VAL_CONTRAT_AV_KEY = "dValContratAv";
	public static final String FONCTION_CTR_AV_KEY = "fonctionCtrAv";
	public static final String ID_COND_RECRUT_KEY = "idCondRecrut";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String INDICE_CONTRAT_KEY = "indiceContrat";
	public static final String MONTANT_KEY = "montant";
	public static final String NBR_UNITE_KEY = "nbrUnite";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_AVENANT_KEY = "noAvenant";
	public static final String NUM_QUOT_RECRUTEMENT_KEY = "numQuotRecrutement";
	public static final String OPERATION_KEY = "operation";
	public static final String POURCENT_SMIC_KEY = "pourcentSmic";
	public static final String REFERENCE_CONTRAT_KEY = "referenceContrat";
	public static final String STATUT_KEY = "statut";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String TEM_ARRETE_SIGNE_KEY = "temArreteSigne";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TYPE_MONTANT_KEY = "typeMontant";

	// Relationships
	public static final String CONTRAT_KEY = "contrat";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONDITION_RECRUTEMENT_KEY = "toConditionRecrutement";

  private static Logger LOG = Logger.getLogger(_EOContratAvenant.class);

  public EOContratAvenant localInstanceIn(EOEditingContext editingContext) {
    EOContratAvenant localInstance = (EOContratAvenant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer avSource() {
    return (Integer) storedValueForKey("avSource");
  }

  public void setAvSource(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating avSource from " + avSource() + " to " + value);
    }
    takeStoredValueForKey(value, "avSource");
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public String cCondRecrut() {
    return (String) storedValueForKey("cCondRecrut");
  }

  public void setCCondRecrut(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cCondRecrut from " + cCondRecrut() + " to " + value);
    }
    takeStoredValueForKey(value, "cCondRecrut");
  }

  public String cDea() {
    return (String) storedValueForKey("cDea");
  }

  public void setCDea(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cDea from " + cDea() + " to " + value);
    }
    takeStoredValueForKey(value, "cDea");
  }

  public String cDiscSecondDegre() {
    return (String) storedValueForKey("cDiscSecondDegre");
  }

  public void setCDiscSecondDegre(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cDiscSecondDegre from " + cDiscSecondDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiscSecondDegre");
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String codeemploi() {
    return (String) storedValueForKey("codeemploi");
  }

  public void setCodeemploi(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating codeemploi from " + codeemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeemploi");
  }

  public String cSectionCnu() {
    return (String) storedValueForKey("cSectionCnu");
  }

  public void setCSectionCnu(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cSectionCnu from " + cSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSectionCnu");
  }

  public String cSousSectionCnu() {
    return (String) storedValueForKey("cSousSectionCnu");
  }

  public void setCSousSectionCnu(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cSousSectionCnu from " + cSousSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSousSectionCnu");
  }

  public String cSpecialiteAtos() {
    return (String) storedValueForKey("cSpecialiteAtos");
  }

  public void setCSpecialiteAtos(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cSpecialiteAtos from " + cSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteAtos");
  }

  public String cSpecialiteItarf() {
    return (String) storedValueForKey("cSpecialiteItarf");
  }

  public void setCSpecialiteItarf(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cSpecialiteItarf from " + cSpecialiteItarf() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteItarf");
  }

  public Long ctraDureeValideeAnnees() {
    return (Long) storedValueForKey("ctraDureeValideeAnnees");
  }

  public void setCtraDureeValideeAnnees(Long value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraDureeValideeAnnees from " + ctraDureeValideeAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraDureeValideeAnnees");
  }

  public Long ctraDureeValideeJours() {
    return (Long) storedValueForKey("ctraDureeValideeJours");
  }

  public void setCtraDureeValideeJours(Long value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraDureeValideeJours from " + ctraDureeValideeJours() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraDureeValideeJours");
  }

  public Long ctraDureeValideeMois() {
    return (Long) storedValueForKey("ctraDureeValideeMois");
  }

  public void setCtraDureeValideeMois(Long value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraDureeValideeMois from " + ctraDureeValideeMois() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraDureeValideeMois");
  }

  public java.math.BigDecimal ctraQuotiteCotisation() {
    return (java.math.BigDecimal) storedValueForKey("ctraQuotiteCotisation");
  }

  public void setCtraQuotiteCotisation(java.math.BigDecimal value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraQuotiteCotisation from " + ctraQuotiteCotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraQuotiteCotisation");
  }

  public Integer ctrSource() {
    return (Integer) storedValueForKey("ctrSource");
  }

  public void setCtrSource(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctrSource from " + ctrSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ctrSource");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebContratAv() {
    return (NSTimestamp) storedValueForKey("dDebContratAv");
  }

  public void setDDebContratAv(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dDebContratAv from " + dDebContratAv() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebContratAv");
  }

  public NSTimestamp dFinContratAv() {
    return (NSTimestamp) storedValueForKey("dFinContratAv");
  }

  public void setDFinContratAv(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dFinContratAv from " + dFinContratAv() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinContratAv");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dRefCtrAvenant() {
    return (NSTimestamp) storedValueForKey("dRefCtrAvenant");
  }

  public void setDRefCtrAvenant(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dRefCtrAvenant from " + dRefCtrAvenant() + " to " + value);
    }
    takeStoredValueForKey(value, "dRefCtrAvenant");
  }

  public NSTimestamp dValContratAv() {
    return (NSTimestamp) storedValueForKey("dValContratAv");
  }

  public void setDValContratAv(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dValContratAv from " + dValContratAv() + " to " + value);
    }
    takeStoredValueForKey(value, "dValContratAv");
  }

  public String fonctionCtrAv() {
    return (String) storedValueForKey("fonctionCtrAv");
  }

  public void setFonctionCtrAv(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating fonctionCtrAv from " + fonctionCtrAv() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionCtrAv");
  }

  public Integer idCondRecrut() {
    return (Integer) storedValueForKey("idCondRecrut");
  }

  public void setIdCondRecrut(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating idCondRecrut from " + idCondRecrut() + " to " + value);
    }
    takeStoredValueForKey(value, "idCondRecrut");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String indiceContrat() {
    return (String) storedValueForKey("indiceContrat");
  }

  public void setIndiceContrat(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating indiceContrat from " + indiceContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "indiceContrat");
  }

  public Double montant() {
    return (Double) storedValueForKey("montant");
  }

  public void setMontant(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, "montant");
  }

  public Double nbrUnite() {
    return (Double) storedValueForKey("nbrUnite");
  }

  public void setNbrUnite(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating nbrUnite from " + nbrUnite() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrUnite");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noAvenant() {
    return (String) storedValueForKey("noAvenant");
  }

  public void setNoAvenant(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating noAvenant from " + noAvenant() + " to " + value);
    }
    takeStoredValueForKey(value, "noAvenant");
  }

  public Double numQuotRecrutement() {
    return (Double) storedValueForKey("numQuotRecrutement");
  }

  public void setNumQuotRecrutement(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating numQuotRecrutement from " + numQuotRecrutement() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotRecrutement");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Double pourcentSmic() {
    return (Double) storedValueForKey("pourcentSmic");
  }

  public void setPourcentSmic(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating pourcentSmic from " + pourcentSmic() + " to " + value);
    }
    takeStoredValueForKey(value, "pourcentSmic");
  }

  public String referenceContrat() {
    return (String) storedValueForKey("referenceContrat");
  }

  public void setReferenceContrat(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating referenceContrat from " + referenceContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "referenceContrat");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public java.math.BigDecimal tauxHoraire() {
    return (java.math.BigDecimal) storedValueForKey("tauxHoraire");
  }

  public void setTauxHoraire(java.math.BigDecimal value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating tauxHoraire from " + tauxHoraire() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraire");
  }

  public String temArreteSigne() {
    return (String) storedValueForKey("temArreteSigne");
  }

  public void setTemArreteSigne(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temArreteSigne from " + temArreteSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temArreteSigne");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey("temPaiementPonctuel");
  }

  public void setTemPaiementPonctuel(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temPaiementPonctuel from " + temPaiementPonctuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temPaiementPonctuel");
  }

  public String typeMontant() {
    return (String) storedValueForKey("typeMontant");
  }

  public void setTypeMontant(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating typeMontant from " + typeMontant() + " to " + value);
    }
    takeStoredValueForKey(value, "typeMontant");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOContrat contrat() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOContrat value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConditionRecrutement toConditionRecrutement() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConditionRecrutement)storedValueForKey("toConditionRecrutement");
  }

  public void setToConditionRecrutementRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConditionRecrutement value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toConditionRecrutement from " + toConditionRecrutement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConditionRecrutement oldValue = toConditionRecrutement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toConditionRecrutement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toConditionRecrutement");
    }
  }
  

  public static EOContratAvenant createContratAvenant(EOEditingContext editingContext, Integer avSource
, Integer ctrSource
, NSTimestamp dCreation
, NSTimestamp dDebContratAv
, NSTimestamp dModification
, Integer idSource
, Double numQuotRecrutement
, String statut
, String temGestEtab
, String temImport
) {
    EOContratAvenant eo = (EOContratAvenant) EOUtilities.createAndInsertInstance(editingContext, _EOContratAvenant.ENTITY_NAME);    
		eo.setAvSource(avSource);
		eo.setCtrSource(ctrSource);
		eo.setDCreation(dCreation);
		eo.setDDebContratAv(dDebContratAv);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setNumQuotRecrutement(numQuotRecrutement);
		eo.setStatut(statut);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOContratAvenant> fetchAllContratAvenants(EOEditingContext editingContext) {
    return _EOContratAvenant.fetchAllContratAvenants(editingContext, null);
  }

  public static NSArray<EOContratAvenant> fetchAllContratAvenants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOContratAvenant.fetchContratAvenants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOContratAvenant> fetchContratAvenants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOContratAvenant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOContratAvenant> eoObjects = (NSArray<EOContratAvenant>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOContratAvenant fetchContratAvenant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratAvenant.fetchContratAvenant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratAvenant fetchContratAvenant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOContratAvenant> eoObjects = _EOContratAvenant.fetchContratAvenants(editingContext, qualifier, null);
    EOContratAvenant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOContratAvenant)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ContratAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratAvenant fetchRequiredContratAvenant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratAvenant.fetchRequiredContratAvenant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratAvenant fetchRequiredContratAvenant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOContratAvenant eoObject = _EOContratAvenant.fetchContratAvenant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ContratAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratAvenant localInstanceIn(EOEditingContext editingContext, EOContratAvenant eo) {
    EOContratAvenant localInstance = (eo == null) ? null : (EOContratAvenant)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
