// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntAccidentTrav.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntAccidentTrav extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "CgntAccidentTrav";

	// Attributes
	public static final String C_ANCIENNETE_KEY = "cAnciennete";
	public static final String CAT_SOURCE_KEY = "catSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DACC_SOURCE_KEY = "daccSource";
	public static final String DATE_ACCIDENT_KEY = "dateAccident";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_PLEIN_TRAIT_KEY = "dDebPleinTrait";
	public static final String D_FIN_PLEIN_TRAIT_KEY = "dFinPleinTrait";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ANCIENNETE_KEY = "anciennete";
	public static final String DECLARATION_ACCIDENT_KEY = "declarationAccident";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCgntAccidentTrav.class);

  public EOCgntAccidentTrav localInstanceIn(EOEditingContext editingContext) {
    EOCgntAccidentTrav localInstance = (EOCgntAccidentTrav)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cAnciennete() {
    return (String) storedValueForKey("cAnciennete");
  }

  public void setCAnciennete(String value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating cAnciennete from " + cAnciennete() + " to " + value);
    }
    takeStoredValueForKey(value, "cAnciennete");
  }

  public Integer catSource() {
    return (Integer) storedValueForKey("catSource");
  }

  public void setCatSource(Integer value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating catSource from " + catSource() + " to " + value);
    }
    takeStoredValueForKey(value, "catSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Integer daccSource() {
    return (Integer) storedValueForKey("daccSource");
  }

  public void setDaccSource(Integer value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating daccSource from " + daccSource() + " to " + value);
    }
    takeStoredValueForKey(value, "daccSource");
  }

  public NSTimestamp dateAccident() {
    return (NSTimestamp) storedValueForKey("dateAccident");
  }

  public void setDateAccident(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dateAccident from " + dateAccident() + " to " + value);
    }
    takeStoredValueForKey(value, "dateAccident");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebPleinTrait() {
    return (NSTimestamp) storedValueForKey("dDebPleinTrait");
  }

  public void setDDebPleinTrait(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dDebPleinTrait from " + dDebPleinTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebPleinTrait");
  }

  public NSTimestamp dFinPleinTrait() {
    return (NSTimestamp) storedValueForKey("dFinPleinTrait");
  }

  public void setDFinPleinTrait(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dFinPleinTrait from " + dFinPleinTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinPleinTrait");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTrav.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAnciennete anciennete() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAnciennete)storedValueForKey("anciennete");
  }

  public void setAncienneteRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAnciennete value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
      _EOCgntAccidentTrav.LOG.debug("updating anciennete from " + anciennete() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAnciennete oldValue = anciennete();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "anciennete");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "anciennete");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident declarationAccident() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident)storedValueForKey("declarationAccident");
  }

  public void setDeclarationAccidentRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
      _EOCgntAccidentTrav.LOG.debug("updating declarationAccident from " + declarationAccident() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident oldValue = declarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "declarationAccident");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "declarationAccident");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCgntAccidentTrav.LOG.isDebugEnabled()) {
      _EOCgntAccidentTrav.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCgntAccidentTrav createCgntAccidentTrav(EOEditingContext editingContext, NSTimestamp dateAccident
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temValide
) {
    EOCgntAccidentTrav eo = (EOCgntAccidentTrav) EOUtilities.createAndInsertInstance(editingContext, _EOCgntAccidentTrav.ENTITY_NAME);    
		eo.setDateAccident(dateAccident);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCgntAccidentTrav> fetchAllCgntAccidentTravs(EOEditingContext editingContext) {
    return _EOCgntAccidentTrav.fetchAllCgntAccidentTravs(editingContext, null);
  }

  public static NSArray<EOCgntAccidentTrav> fetchAllCgntAccidentTravs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntAccidentTrav.fetchCgntAccidentTravs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntAccidentTrav> fetchCgntAccidentTravs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntAccidentTrav.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntAccidentTrav> eoObjects = (NSArray<EOCgntAccidentTrav>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntAccidentTrav fetchCgntAccidentTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntAccidentTrav.fetchCgntAccidentTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntAccidentTrav fetchCgntAccidentTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntAccidentTrav> eoObjects = _EOCgntAccidentTrav.fetchCgntAccidentTravs(editingContext, qualifier, null);
    EOCgntAccidentTrav eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntAccidentTrav)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntAccidentTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntAccidentTrav fetchRequiredCgntAccidentTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntAccidentTrav.fetchRequiredCgntAccidentTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntAccidentTrav fetchRequiredCgntAccidentTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntAccidentTrav eoObject = _EOCgntAccidentTrav.fetchCgntAccidentTrav(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntAccidentTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntAccidentTrav localInstanceIn(EOEditingContext editingContext, EOCgntAccidentTrav eo) {
    EOCgntAccidentTrav localInstance = (eo == null) ? null : (EOCgntAccidentTrav)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
