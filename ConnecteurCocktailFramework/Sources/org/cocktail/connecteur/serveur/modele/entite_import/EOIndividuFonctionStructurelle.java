package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOFonctionStructurelle;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOIndividuFonctionStructurelle extends _EOIndividuFonctionStructurelle {
	@Override
	public void preparerRelations() {
		super.preparerRelations();
		
		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);
	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setStructureRelationship(null);
	}
	
	@Override
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log!=null)
			return log;

		String messageLog = null;
		if (structure() == null && strSource() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure() != null && structure().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		} 
		
		return log;
	}


	@Override
	public Integer eimpSource() {
		return ifsSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setIfsSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.INDIVIDU_FONCTION_STRUCTURELLE_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		Integer codeFonction = (Integer) valeursAttributs.objectForKey(C_FONCTION_KEY);
		if (codeFonction != null) {
			EOFonctionStructurelle fonction = EOFonctionStructurelle.getFromCode(editingContext, codeFonction);
			if (fonction == null) {
				resultat = resultat + "CodeFonction_Inconnu >>" + codeFonction + System.getProperty("line.separator");
			}
		}

		return resultat;
	}
}
