// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongePaternite.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongePaternite extends CongeAvecArrete {
	public static final String ENTITY_NAME = "CongePaternite";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEMANDE_KEY = "dDemande";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAIS_PREV_KEY = "dNaisPrev";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NB_ENFANTS_DECL_KEY = "nbEnfantsDecl";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String PAT_SOURCE_KEY = "patSource";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CG_SANS_TRAIT_KEY = "temCgSansTrait";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_GROSSESSE_MULTIPLE_KEY = "temGrossesseMultiple";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongePaternite.class);

  public EOCongePaternite localInstanceIn(EOEditingContext editingContext) {
    EOCongePaternite localInstance = (EOCongePaternite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDemande() {
    return (NSTimestamp) storedValueForKey("dDemande");
  }

  public void setDDemande(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dDemande from " + dDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "dDemande");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dNaisPrev() {
    return (NSTimestamp) storedValueForKey("dNaisPrev");
  }

  public void setDNaisPrev(NSTimestamp value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating dNaisPrev from " + dNaisPrev() + " to " + value);
    }
    takeStoredValueForKey(value, "dNaisPrev");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer nbEnfantsDecl() {
    return (Integer) storedValueForKey("nbEnfantsDecl");
  }

  public void setNbEnfantsDecl(Integer value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating nbEnfantsDecl from " + nbEnfantsDecl() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfantsDecl");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer patSource() {
    return (Integer) storedValueForKey("patSource");
  }

  public void setPatSource(Integer value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating patSource from " + patSource() + " to " + value);
    }
    takeStoredValueForKey(value, "patSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temCgSansTrait() {
    return (String) storedValueForKey("temCgSansTrait");
  }

  public void setTemCgSansTrait(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating temCgSansTrait from " + temCgSansTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgSansTrait");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temGrossesseMultiple() {
    return (String) storedValueForKey("temGrossesseMultiple");
  }

  public void setTemGrossesseMultiple(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating temGrossesseMultiple from " + temGrossesseMultiple() + " to " + value);
    }
    takeStoredValueForKey(value, "temGrossesseMultiple");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
    	_EOCongePaternite.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
      _EOCongePaternite.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongePaternite.LOG.isDebugEnabled()) {
      _EOCongePaternite.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongePaternite createCongePaternite(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temCgSansTrait
, String temConfirme
, String temGestEtab
, String temImport
, String temValide
) {
    EOCongePaternite eo = (EOCongePaternite) EOUtilities.createAndInsertInstance(editingContext, _EOCongePaternite.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemCgSansTrait(temCgSansTrait);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCongePaternite> fetchAllCongePaternites(EOEditingContext editingContext) {
    return _EOCongePaternite.fetchAllCongePaternites(editingContext, null);
  }

  public static NSArray<EOCongePaternite> fetchAllCongePaternites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongePaternite.fetchCongePaternites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongePaternite> fetchCongePaternites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongePaternite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongePaternite> eoObjects = (NSArray<EOCongePaternite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongePaternite fetchCongePaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongePaternite.fetchCongePaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongePaternite fetchCongePaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongePaternite> eoObjects = _EOCongePaternite.fetchCongePaternites(editingContext, qualifier, null);
    EOCongePaternite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongePaternite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongePaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongePaternite fetchRequiredCongePaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongePaternite.fetchRequiredCongePaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongePaternite fetchRequiredCongePaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongePaternite eoObject = _EOCongePaternite.fetchCongePaternite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongePaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongePaternite localInstanceIn(EOEditingContext editingContext, EOCongePaternite eo) {
    EOCongePaternite localInstance = (eo == null) ? null : (EOCongePaternite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
