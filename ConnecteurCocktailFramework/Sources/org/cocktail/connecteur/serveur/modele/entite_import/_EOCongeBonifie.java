// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeBonifie.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeBonifie extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "CongeBonifie";

	// Attributes
	public static final String CGB_SOURCE_KEY = "cgbSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_METRO_DOM_KEY = "temMetroDom";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeBonifie.class);

  public EOCongeBonifie localInstanceIn(EOEditingContext editingContext) {
    EOCongeBonifie localInstance = (EOCongeBonifie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cgbSource() {
    return (Integer) storedValueForKey("cgbSource");
  }

  public void setCgbSource(Integer value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating cgbSource from " + cgbSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cgbSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temMetroDom() {
    return (String) storedValueForKey("temMetroDom");
  }

  public void setTemMetroDom(String value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating temMetroDom from " + temMetroDom() + " to " + value);
    }
    takeStoredValueForKey(value, "temMetroDom");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
    	_EOCongeBonifie.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeBonifie.LOG.isDebugEnabled()) {
      _EOCongeBonifie.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeBonifie createCongeBonifie(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temMetroDom
, String temValide
) {
    EOCongeBonifie eo = (EOCongeBonifie) EOUtilities.createAndInsertInstance(editingContext, _EOCongeBonifie.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemMetroDom(temMetroDom);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCongeBonifie> fetchAllCongeBonifies(EOEditingContext editingContext) {
    return _EOCongeBonifie.fetchAllCongeBonifies(editingContext, null);
  }

  public static NSArray<EOCongeBonifie> fetchAllCongeBonifies(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeBonifie.fetchCongeBonifies(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeBonifie> fetchCongeBonifies(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeBonifie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeBonifie> eoObjects = (NSArray<EOCongeBonifie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeBonifie fetchCongeBonifie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeBonifie.fetchCongeBonifie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeBonifie fetchCongeBonifie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeBonifie> eoObjects = _EOCongeBonifie.fetchCongeBonifies(editingContext, qualifier, null);
    EOCongeBonifie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeBonifie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeBonifie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeBonifie fetchRequiredCongeBonifie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeBonifie.fetchRequiredCongeBonifie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeBonifie fetchRequiredCongeBonifie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeBonifie eoObject = _EOCongeBonifie.fetchCongeBonifie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeBonifie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeBonifie localInstanceIn(EOEditingContext editingContext, EOCongeBonifie eo) {
    EOCongeBonifie localInstance = (eo == null) ? null : (EOCongeBonifie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
