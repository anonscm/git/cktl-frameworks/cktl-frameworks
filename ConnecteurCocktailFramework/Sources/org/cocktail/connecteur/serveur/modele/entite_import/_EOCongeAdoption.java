// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAdoption.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAdoption extends CongeAvecArrete {
	public static final String ENTITY_NAME = "CongeAdoption";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CADOP_SOURCE_KEY = "cadopSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRIVEE_FOYER_KEY = "dArriveeFoyer";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEMANDE_KEY = "dDemande";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ENF_SOURCE_KEY = "enfSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NB_ENFANT_A_CHARGE_KEY = "nbEnfantACharge";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_ADOPTION_MULTIPLE_KEY = "temAdoptionMultiple";
	public static final String TEM_CG_PARTAGE_KEY = "temCgPartage";
	public static final String TEM_CG_SANS_TRAIT_KEY = "temCgSansTrait";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String ENFANT_KEY = "enfant";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeAdoption.class);

  public EOCongeAdoption localInstanceIn(EOEditingContext editingContext) {
    EOCongeAdoption localInstance = (EOCongeAdoption)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer cadopSource() {
    return (Integer) storedValueForKey("cadopSource");
  }

  public void setCadopSource(Integer value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating cadopSource from " + cadopSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cadopSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArriveeFoyer() {
    return (NSTimestamp) storedValueForKey("dArriveeFoyer");
  }

  public void setDArriveeFoyer(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dArriveeFoyer from " + dArriveeFoyer() + " to " + value);
    }
    takeStoredValueForKey(value, "dArriveeFoyer");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDemande() {
    return (NSTimestamp) storedValueForKey("dDemande");
  }

  public void setDDemande(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dDemande from " + dDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "dDemande");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer enfSource() {
    return (Integer) storedValueForKey("enfSource");
  }

  public void setEnfSource(Integer value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating enfSource from " + enfSource() + " to " + value);
    }
    takeStoredValueForKey(value, "enfSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer nbEnfantACharge() {
    return (Integer) storedValueForKey("nbEnfantACharge");
  }

  public void setNbEnfantACharge(Integer value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating nbEnfantACharge from " + nbEnfantACharge() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfantACharge");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temAdoptionMultiple() {
    return (String) storedValueForKey("temAdoptionMultiple");
  }

  public void setTemAdoptionMultiple(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating temAdoptionMultiple from " + temAdoptionMultiple() + " to " + value);
    }
    takeStoredValueForKey(value, "temAdoptionMultiple");
  }

  public String temCgPartage() {
    return (String) storedValueForKey("temCgPartage");
  }

  public void setTemCgPartage(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating temCgPartage from " + temCgPartage() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgPartage");
  }

  public String temCgSansTrait() {
    return (String) storedValueForKey("temCgSansTrait");
  }

  public void setTemCgSansTrait(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating temCgSansTrait from " + temCgSansTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgSansTrait");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
    	_EOCongeAdoption.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
      _EOCongeAdoption.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant enfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("enfant");
  }

  public void setEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
      _EOCongeAdoption.LOG.debug("updating enfant from " + enfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeAdoption.LOG.isDebugEnabled()) {
      _EOCongeAdoption.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeAdoption createCongeAdoption(EOEditingContext editingContext, Integer cadopSource
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temAdoptionMultiple
, String temCgPartage
, String temCgSansTrait
, String temConfirme
, String temGestEtab
, String temImport
, String temValide
) {
    EOCongeAdoption eo = (EOCongeAdoption) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAdoption.ENTITY_NAME);    
		eo.setCadopSource(cadopSource);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemAdoptionMultiple(temAdoptionMultiple);
		eo.setTemCgPartage(temCgPartage);
		eo.setTemCgSansTrait(temCgSansTrait);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCongeAdoption> fetchAllCongeAdoptions(EOEditingContext editingContext) {
    return _EOCongeAdoption.fetchAllCongeAdoptions(editingContext, null);
  }

  public static NSArray<EOCongeAdoption> fetchAllCongeAdoptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAdoption.fetchCongeAdoptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAdoption> fetchCongeAdoptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAdoption.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAdoption> eoObjects = (NSArray<EOCongeAdoption>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAdoption fetchCongeAdoption(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAdoption.fetchCongeAdoption(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAdoption fetchCongeAdoption(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAdoption> eoObjects = _EOCongeAdoption.fetchCongeAdoptions(editingContext, qualifier, null);
    EOCongeAdoption eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAdoption)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAdoption that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAdoption fetchRequiredCongeAdoption(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAdoption.fetchRequiredCongeAdoption(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAdoption fetchRequiredCongeAdoption(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAdoption eoObject = _EOCongeAdoption.fetchCongeAdoption(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAdoption that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAdoption localInstanceIn(EOEditingContext editingContext, EOCongeAdoption eo) {
    EOCongeAdoption localInstance = (eo == null) ? null : (EOCongeAdoption)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
