package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOFonctionExpertConseil;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOIndividuAutreFonction extends _EOIndividuAutreFonction {
	@Override
	public Integer eimpSource() {
		return iafSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setIafSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.INDIVIDU_AUTRE_FONCTION_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		Integer codeFonction = (Integer) valeursAttributs.objectForKey(C_FONCTION_KEY);
		if (codeFonction != null) {
			EOFonctionExpertConseil fonction = EOFonctionExpertConseil.getFromCode(editingContext, codeFonction);
			if (fonction == null) {
				resultat = resultat + "CodeFonction_Inconnu >>" + codeFonction + System.getProperty("line.separator");
			}
		}

		return resultat;
	}
}
