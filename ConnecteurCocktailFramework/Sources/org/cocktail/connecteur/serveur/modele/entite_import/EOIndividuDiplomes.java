// EOIndividuDiplomes.java
// Created on Mon Mar 17 16:07:10  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;


import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.InterfaceRecordGenerique;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDiplomes;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** R&egrave;gles de validation<BR>
 	Verifie les nomenclatures
	Verifie que la date du dipl&ocirc;me n'est pas posterieure a la date de dec&egrave;s de l'individu<BR>
	Verifie que la date du dipl&ocirc;me n'est pas posterieure a la date du jour<BR>
	Verifie que la date du dipl&ocirc;me n'est pas anterieure a la date de naissance de l'individu<BR>
 */

public class EOIndividuDiplomes extends _EOIndividuDiplomes implements InterfaceRecordGenerique {

	public EOIndividuDiplomes() {
		super();
	}


	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null) {
			log.setEimpSource(eimpSource());
		}
		return log;
	}
	public EOLogImport verifierRecordApresOperation() {
		String message = verifierDateDiplome(editingContext(),individu());
		if (message != null) {
			LogManager.logDetail("Insertion d'un log pour "  + message);
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(),this.entityName(),message);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			return log;
		}
		return null;
	}
	// InterfaceValidationClient
	/** Verifie les donnees du record en provenance du client. Effectue les m&ecirc;mes verifications que verifierRecordApresOperation
	 * et retourne le message de log si il y a une erreur */
	public String verifierRecordPourClient(EOEditingContext editingContext) {
		// Rajouter l'individu qui n'est pas construit lorsqu'on effectue les vérifications client
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource());
		if (individu != null) {
			return verifierDateDiplome(editingContext,individu);
		} else {
			return null;
		}
	}
	/** Retourne l'individu diplome avec le m&ecirc;me code diplome si il est unique */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return EOMangueIndividuDiplomes.individuDiplomesDestinationPourIndividuDiplomes(editingContext(),this);
	}

	public String nomRelationPourLog() {
		return EOLogImport.INDIVIDU_DIPLOMES_KEY;
	}
	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttribut(String nomAttribut) {
		return super.nomRelationPourAttributComparaison(nomAttribut);
	}
	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttributDansLog(String nomAttribut) {
		return super.nomRelationPourAttributComparaison(nomAttribut);
	}
	// Méthodes privées
	private String verifierDateDiplome(EOEditingContext editingContext,EOIndividu individu) {
		if (dDiplome() != null) {
			if (individu.dateNaissance() != null && DateCtrl.isBefore(dDiplome(),individu.dateNaissance())) {
				return "DATE_DIPLOME_DATE_NAISSANCE";
			}
			if (individu.dDeces() != null) {
				if (DateCtrl.isAfter(dDiplome(),individu.dDeces())) {
					return "DATE_DIPLOME_DATE_DECES";
				}
			} else {
				EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
				if (individuGrhum != null && individuGrhum.dDeces() != null && DateCtrl.isAfter(dDiplome(),individuGrhum.dDeces())) {
					return "DATE_DIPLOME_DATE_DECES";
				}
			}
			//  pour ramener à une date sans prendre en compte les heures
			NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
			if (DateCtrl.isAfter(dDiplome(),today)) {
				return "DATE_DIPLOME_DATE_JOUR";
			}
		}
		return null;
	}
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = "";	
		String diplome = (String)valeursAttributs.objectForKey("cDiplome");
		EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "Diplomes","cDiplome", diplome);
		if (record == null) {
			resultat = "Diplome_Inconnu >>" + diplome + System.getProperty("line.separator");
		}
		String titulaire = (String)valeursAttributs.objectForKey("cTitulaireDiplome");
		record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TitulaireDiplome","cTitulaireDiplome", titulaire);
		if (record == null) {
			resultat += "Titulaire_Diplome_Inconnu >>" + titulaire + System.getProperty("line.separator");
		}
		return resultat;
	}
	
}
