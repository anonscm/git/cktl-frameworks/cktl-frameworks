/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.serveur.modele.InterfaceRecordGenerique;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Modelise un objet d'import generique (i.e affichage par l'interface generique) pour un individu avec une date debut et fin. Les periodes ne peuvent pas se
 * chevaucher. Tous les objets qui heritent de cette classe ont leur identifiant source appele eimpSource
 */
public abstract class ObjetImportPourIndividuEtPeriode extends ObjetImportPourIndividu implements InterfaceRecordGenerique {
	public abstract Integer eimpSource();

	public abstract void setEimpSource(Integer value);

	public NSTimestamp dateDebut() {
		return (NSTimestamp) storedValueForKey("dateDebut");
	}

	public void setDateDebut(NSTimestamp value) {
		takeStoredValueForKey(value, "dateDebut");
	}

	public NSTimestamp dateFin() {
		return (NSTimestamp) storedValueForKey("dateFin");
	}

	public void setDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, "dateFin");
	}

	/**
	 * Surcharge pour ajouter au log le eimpSource
	 */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null) {
			log.setEimpSource(eimpSource());
		}
		return log;
	}

	/**
	 * Verifie qu'il n'y a pas de chevauchement de periodes avec des objets de m&ecirc;me type dans l'import courant et dans le SI Destinataire
	 */
	public EOLogImport verifierRecordApresOperation() {

		String message = verifierChevauchement(editingContext());

		if (message != null) {
			LogManager.logDetail("Insertion d'un log pour le chevauchement des périodes");
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), message);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une erreur
			return log;
		} else {
			return null;
		}
	}

	/**
	 * Verifie les donnees du record en provenance du client. Effectue les m&ecirc;mes verifications que verifierRecordApresOperation et retourne le message de
	 * log si il y a une erreur
	 */
	public String verifierRecordPourClient(EOEditingContext editingContext) {
		return verifierChevauchement(editingContext);
	}

	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttribut(String nomAttribut) {
		return nomRelationPourAttributComparaison(nomAttribut);
	}

	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttributDansLog(String nomAttribut) {
		return nomRelationPourAttributComparaison(nomAttribut);
	}

	// Méthodes protégées
	protected abstract int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext);

	protected NSArray rechercherObjetDansImportPourChevauchement(EOEditingContext editingContext) {
		return rechercherObjetsImportPourIndividuEtPeriode(editingContext, this.entityName(), idSource(), dateDebut(), dateFin(), false);
	}

	protected String verifierChevauchement(EOEditingContext editingContext) {
		if (estAnnule())
			return null;

		// On recherche tous les objets de même type dans l'import courant
		NSArray objetsImport = rechercherObjetDansImportPourChevauchement(editingContext);
		// On compare sur les attributs de comparaison pour savoir si il s'agit du même objet car dans le cas de la vérification
		// sur le client, l'objet courant ne correspond pas nécessairement à l'objet modifié sur le client
		boolean chevauchementPeriodes = objetsImport.count() > 1
				|| (objetsImport.count() == 1 && getReglesImport().ontAttributsComparaisonIdentiques(
						(ObjetImportPourIndividuEtPeriode) objetsImport.objectAtIndex(0), this) == false);
		if (!chevauchementPeriodes) {
			// on recherche le nombre d'objets de même type dans le SI Destinataire (au plus 1 puisque les périodes ne peuvent pas se chevaucher)
			int nbObjets = nbObjetsPourPeriodeDansSIDestinataire(editingContext);
			LogManager.logDetail("nb objets dans SI Destinataire " + nbObjets);
			// Si il y en a déjà un et qu'on est en insertion => chevauchement, sinon on est nécessairement en update et c'est l'objet qui correspond à this
			if ((nbObjets == 1 && this.operation().equals(ObjetImport.OPERATION_INSERTION))) {
				LogManager.logDetail("chevauchement dans SI Destinataire");
				return "CHEVAUCHEMENT_PERIODE_DESTIN";
			} else {
				return null;
			}
		} else {
			// Chevauchement potentiel. On vérifie qu'il n'y a pas bien plus d'un élément non annulé.
			int cpt = 0;
			for (Object obj : objetsImport) {
				ObjetImportPourIndividuEtPeriode objImport = (ObjetImportPourIndividuEtPeriode) obj;
				if (!objImport.estAnnule()) {
					cpt++;
				}
			}
			if (cpt > 1) {
				LogManager.logDetail("chevauchement dans Import courant");
				return "CHEVAUCHEMENT_PERIODE_IMPORT";
			} else {
				return null;
			}
		}

	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey("dateFin");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp) valeursAttributs.objectForKey("dateDebut");
			if (dateDebut != null && DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		return resultat;
	}

	/**
	 * recherche les objets de type duree selon certains crit&egrave;res
	 * 
	 * @param editingContext
	 * @param entite
	 *            nom entite concernee
	 * @param qualifier
	 * @param shouldRefresh
	 *            true si le fetch doit raffraichir les objets
	 * @return tableau des objets trouves
	 */
	public static NSArray rechercherDureePourEntiteAvecCriteres(EOEditingContext editingContext, String entite, EOQualifier qualifier, boolean shouldRefresh) {
		EOFetchSpecification myFetch = new EOFetchSpecification(entite, qualifier, null);
		myFetch.setRefreshesRefetchedObjects(shouldRefresh);
		System.out.println("**************** rechercherDureePourEntiteAvecCriteres: editingContext="+editingContext+", myFetch="+myFetch);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * recherche les objets de type ObjetImportPourIndividuEtPeriode valide pendant la periode
	 * 
	 * @param editingContext
	 * @param individu
	 * @param nomEntite
	 *            nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode
	 *            peut &ecirc;tre nulle
	 * @param shouldRefresh
	 *            true si les donnees sont raffra&icirc;chies
	 * @return objets trouves
	 */
	public static NSArray rechercherObjetsImportPourIndividuEtPeriode(EOEditingContext editingContext, String nomEntite, Number idSource,
			NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean shouldRefresh) {
System.out.println("!!!!!!!!!!!!!!!! rechercherObjetsImportPourIndividuEtPeriode: editingContext="+editingContext+", nomEntite="+nomEntite+", idSource="+idSource+", debutPeriode="+debutPeriode+", finPeriode"+finPeriode);
		NSMutableArray qualifiers = new NSMutableArray();

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E' ", new NSArray(idSource));
		qualifiers.addObject(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));

		qualifier = SuperFinder.qualifierPourPeriode("dateDebut", debutPeriode, "dateFin", finPeriode);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}

		return rechercherDureePourEntiteAvecCriteres(editingContext, nomEntite, new EOAndQualifier(qualifiers), shouldRefresh);
	}

	// A dériver pour définir si l'objet est annulé ou non (ex: cf. CRCT)
	protected boolean estAnnule() {
		return false;
	}
}
