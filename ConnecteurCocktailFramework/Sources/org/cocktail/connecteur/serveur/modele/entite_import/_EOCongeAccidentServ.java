// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAccidentServ.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAccidentServ extends CongeAvecArrete {
	public static final String ENTITY_NAME = "CongeAccidentServ";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CAS_SOURCE_KEY = "casSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DACC_SOURCE_KEY = "daccSource";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ACCIDENT_KEY = "dateAccident";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_COM_MED_KEY = "dateComMed";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_REFORME_KEY = "dComReforme";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String DECLARATION_ACCIDENT_KEY = "declarationAccident";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeAccidentServ.class);

  public EOCongeAccidentServ localInstanceIn(EOEditingContext editingContext) {
    EOCongeAccidentServ localInstance = (EOCongeAccidentServ)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer casSource() {
    return (Integer) storedValueForKey("casSource");
  }

  public void setCasSource(Integer value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating casSource from " + casSource() + " to " + value);
    }
    takeStoredValueForKey(value, "casSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Integer daccSource() {
    return (Integer) storedValueForKey("daccSource");
  }

  public void setDaccSource(Integer value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating daccSource from " + daccSource() + " to " + value);
    }
    takeStoredValueForKey(value, "daccSource");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateAccident() {
    return (NSTimestamp) storedValueForKey("dateAccident");
  }

  public void setDateAccident(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dateAccident from " + dateAccident() + " to " + value);
    }
    takeStoredValueForKey(value, "dateAccident");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateComMed() {
    return (NSTimestamp) storedValueForKey("dateComMed");
  }

  public void setDateComMed(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dateComMed from " + dateComMed() + " to " + value);
    }
    takeStoredValueForKey(value, "dateComMed");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComReforme() {
    return (NSTimestamp) storedValueForKey("dComReforme");
  }

  public void setDComReforme(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dComReforme from " + dComReforme() + " to " + value);
    }
    takeStoredValueForKey(value, "dComReforme");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServ.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
      _EOCongeAccidentServ.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident declarationAccident() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident)storedValueForKey("declarationAccident");
  }

  public void setDeclarationAccidentRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
      _EOCongeAccidentServ.LOG.debug("updating declarationAccident from " + declarationAccident() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident oldValue = declarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "declarationAccident");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "declarationAccident");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeAccidentServ.LOG.isDebugEnabled()) {
      _EOCongeAccidentServ.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeAccidentServ createCongeAccidentServ(EOEditingContext editingContext, NSTimestamp dateAccident
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temConfirme
, String temGestEtab
, String temImport
, String temValide
) {
    EOCongeAccidentServ eo = (EOCongeAccidentServ) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAccidentServ.ENTITY_NAME);    
		eo.setDateAccident(dateAccident);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCongeAccidentServ> fetchAllCongeAccidentServs(EOEditingContext editingContext) {
    return _EOCongeAccidentServ.fetchAllCongeAccidentServs(editingContext, null);
  }

  public static NSArray<EOCongeAccidentServ> fetchAllCongeAccidentServs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAccidentServ.fetchCongeAccidentServs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAccidentServ> fetchCongeAccidentServs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAccidentServ.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAccidentServ> eoObjects = (NSArray<EOCongeAccidentServ>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAccidentServ fetchCongeAccidentServ(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAccidentServ.fetchCongeAccidentServ(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAccidentServ fetchCongeAccidentServ(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAccidentServ> eoObjects = _EOCongeAccidentServ.fetchCongeAccidentServs(editingContext, qualifier, null);
    EOCongeAccidentServ eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAccidentServ)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAccidentServ that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAccidentServ fetchRequiredCongeAccidentServ(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAccidentServ.fetchRequiredCongeAccidentServ(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAccidentServ fetchRequiredCongeAccidentServ(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAccidentServ eoObject = _EOCongeAccidentServ.fetchCongeAccidentServ(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAccidentServ that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAccidentServ localInstanceIn(EOEditingContext editingContext, EOCongeAccidentServ eo) {
    EOCongeAccidentServ localInstance = (eo == null) ? null : (EOCongeAccidentServ)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
