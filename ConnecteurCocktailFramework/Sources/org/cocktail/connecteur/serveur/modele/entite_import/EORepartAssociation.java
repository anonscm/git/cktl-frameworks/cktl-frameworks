package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EORepartAssociation extends _EORepartAssociation {
	@Override
	public void preparerRelations() {
		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);

		EOStructure structure2 = EOStructure.structurePourSourceId(editingContext(), str2Source());
		setStructure2Relationship(structure2);

		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext(), idSource());
		setIndividuRelationship(individu);
	}

	@Override
	public void supprimerRelations() {
		setStructureRelationship(null);
		setStructure2Relationship(null);
		setIndividuRelationship(null);
	}

	@Override
	public EOLogImport verifierRecord() {
		EOLogImport log = null;

		String messageLog = null;
		if (structure() == null && strSource() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure() != null && structure().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		} else if (structure2() == null && str2Source() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure2() != null && structure2().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		} else if (individu() == null && idSource() != null) {
			messageLog = "INDIVIDU_NON_IMPORTE";
		} else if (individu() != null && individu().statut().equals(STATUT_ERREUR)) {
			messageLog = "INDIVIDU_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
			return log;
		} else {
			return null;
		}
	}

	@Override
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.REPART_ASSOCIATION_KEY;
	}

	@Override
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		Integer idSource = (Integer) valeursAttributs.objectForKey(ID_SOURCE_KEY);
		String str2Source = (String) valeursAttributs.objectForKey(STR2_SOURCE_KEY);
		if (idSource == null && str2Source == null) {
			resultat = resultat + "IdSource_et_Str2Source_vides >>" + System.getProperty("line.separator");
		}
		if (idSource != null && str2Source != null) {
			resultat = resultat + "IdSource_et_Str2Source_non_vides >>" + System.getProperty("line.separator");
		}
		
		Integer rang = (Integer) valeursAttributs.objectForKey(RANG_KEY);
		if (rang==null)
			valeursAttributs.setObjectForKey(new Integer(1), RANG_KEY);

		String assCode = (String) valeursAttributs.objectForKey(ASS_CODE_KEY);
		if (assCode != null) {
			EOAssociation recordAssociation = EOAssociation.getFromCode(editingContext, assCode);
			if (recordAssociation == null) {
				resultat = resultat + "AssCode_Inconnu >>" + assCode + System.getProperty("line.separator");
			}
		}

		return resultat;
	}
}
