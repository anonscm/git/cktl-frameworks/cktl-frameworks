// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSituation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSituation extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Situation";

	// Attributes
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String ADR_SOURCE_KEY = "adrSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_KEY = "quotite";
	public static final String SIT_SOURCE_KEY = "sitSource";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOSituation.class);

  public EOSituation localInstanceIn(EOEditingContext editingContext) {
    EOSituation localInstance = (EOSituation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey("adrOrdre");
  }

  public void setAdrOrdre(Integer value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating adrOrdre from " + adrOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "adrOrdre");
  }

  public Integer adrSource() {
    return (Integer) storedValueForKey("adrSource");
  }

  public void setAdrSource(Integer value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating adrSource from " + adrSource() + " to " + value);
    }
    takeStoredValueForKey(value, "adrSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer quotite() {
    return (Integer) storedValueForKey("quotite");
  }

  public void setQuotite(Integer value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public Integer sitSource() {
    return (Integer) storedValueForKey("sitSource");
  }

  public void setSitSource(Integer value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating sitSource from " + sitSource() + " to " + value);
    }
    takeStoredValueForKey(value, "sitSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
    	_EOSituation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse adresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse)storedValueForKey("adresse");
  }

  public void setAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
      _EOSituation.LOG.debug("updating adresse from " + adresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "adresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "adresse");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
      _EOSituation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOSituation.LOG.isDebugEnabled()) {
      _EOSituation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOSituation createSituation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, Integer sitSource
, String statut
, String strSource
, String temImport
) {
    EOSituation eo = (EOSituation) EOUtilities.createAndInsertInstance(editingContext, _EOSituation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setSitSource(sitSource);
		eo.setStatut(statut);
		eo.setStrSource(strSource);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOSituation> fetchAllSituations(EOEditingContext editingContext) {
    return _EOSituation.fetchAllSituations(editingContext, null);
  }

  public static NSArray<EOSituation> fetchAllSituations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSituation.fetchSituations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSituation> fetchSituations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSituation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSituation> eoObjects = (NSArray<EOSituation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSituation fetchSituation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituation.fetchSituation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituation fetchSituation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSituation> eoObjects = _EOSituation.fetchSituations(editingContext, qualifier, null);
    EOSituation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSituation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Situation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituation fetchRequiredSituation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituation.fetchRequiredSituation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituation fetchRequiredSituation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSituation eoObject = _EOSituation.fetchSituation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Situation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituation localInstanceIn(EOEditingContext editingContext, EOSituation eo) {
    EOSituation localInstance = (eo == null) ? null : (EOSituation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
