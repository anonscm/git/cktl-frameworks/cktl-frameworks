package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.InterfaceRecordGenerique;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOTempsPartielCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * . Classe gérant les Reprise Temps Plein d'un individu
 * 
 * @author alainmalaplate
 * 
 */
public class EORepriseTempsPlein extends _EORepriseTempsPlein implements InterfaceRecordGenerique {
	private static final long serialVersionUID = 3679708435472962373L;

	public void preparerRelations() {
		LogManager.logDetail("Preparation des relations pour la Reprise à Temps Plein");
		super.preparerRelations();
		if (editingContext() != null && tpSource() != null && idSource() != null) {
			EOTempsPartiel tempsPartiel = EOTempsPartielCorresp.tpsPourSourceEtCgId(editingContext(), idSource(), tpSource());
			setTempsPartielRelationship(tempsPartiel);
		}
	}

	public void supprimerRelations() {
		setTempsPartielRelationship(null);
		setArreteAnnulationRelationship(null);
	}

	/**
	 * 
	 */
	public EOLogImport verifierRecord() {
		// Suivant l'ordre d'apparition des TempsPartiel dans le xml, l'objet lié peut ne pas encore avoir été rajouté dans la base lors de l'appel à preparerRelation
		// On attend donc verifierRecord pour faire le lien
		if (arreteAnnulationSource() != null) {
			EORepriseTempsPlein arreteAnnulation = rtpPourSourceId(editingContext(), arreteAnnulationSource());
			setArreteAnnulationRelationship(arreteAnnulation);
		}

		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		LogManager.logDetail("Verification du record");

		String messageLog = null;
		if (tempsPartiel() == null) {
			messageLog = "TEMPS_PARTIEL_NON_IMPORTE";
		} else if (tempsPartiel().temImport().equals(ObjetImport.A_TRANSFERER) && tempsPartiel().statut().equals(ObjetImport.STATUT_ERREUR)) {
			messageLog = "TEMPS_PARTIEL_INVALIDE";
		}

		if (arreteAnnulationSource() != null && arreteAnnulation() == null) {
			messageLog = "ARRETE_ANNULATION_NON_IMPORTE";
		} else if (arreteAnnulation() != null && arreteAnnulation().statut().equals(STATUT_ERREUR)) {
			messageLog = "ARRETE_ANNULATION_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();

			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.setIdSource(new Integer(idSource().intValue()));
			log.setEimpSource(new Integer(tpSource().intValue()));
			log.setEimp2Source(rtpSource());
			log.setIndividuRelationship(individu());
			log.setTempsPartielRelationship(tempsPartiel());
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttribut(String nomAttribut) {
		return nomRelationPourAttributComparaison(nomAttribut);
	}

	public String nomRelationPourAttributComparaison(String nomAttribut) {
		if (nomAttribut.equals(ID_SOURCE_KEY)) {
			return INDIVIDU_KEY;
		} else if (nomAttribut.equals(TP_SOURCE_KEY)) {
			return TEMPS_PARTIEL_KEY;
		} else {
			return null;
		}
	}

	public EOLogImport verifierRecordApresOperation() {

		String message = verifierDatesDetailPourTempsPartiel(tempsPartiel());
		if (message != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), message);
			log.setIdSource(new Integer(idSource().intValue()));
			log.setEimpSource(new Integer(tpSource().intValue()));
			log.setEimp2Source(rtpSource());
			log.setIndividuRelationship(individu());
			log.setTempsPartielRelationship(tempsPartiel());
			setStatut(STATUT_ERREUR);
			return log;
		}
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.REPRISE_TEMPS_PLEIN_KEY;
	}

	public String nomRelationPourAttributDansLog(String nomAttribut) {
		if (nomAttribut.equals(ID_SOURCE_KEY)) {
			return EOLogImport.INDIVIDU_KEY;
		} else if (nomAttribut.equals(TP_SOURCE_KEY)) {
			return EOLogImport.TEMPS_PARTIEL_KEY;
		} else {
			return null;
		}
	}

	public String verifierRecordPourClient(EOEditingContext editingContext) {
		LogManager.logDetail("verifierRecordPourClient");
		// Rajouter le cld qui n'est pas construit lorsqu'on effectue les
		// vérifications client
		EOTempsPartiel tempsPartiel = EOTempsPartielCorresp.tpsPourSourceEtCgId(editingContext, idSource(), tpSource());
		if (tempsPartiel != null) {
			return verifierDatesDetailPourTempsPartiel(tempsPartiel);
		}
		LogManager.logDetail("Temps partiel non trouve");
		return null;
	}

	// Méthodes privées
	private String verifierDatesDetailPourTempsPartiel(EOTempsPartiel tempsPartiel) {
		if (dRepriseTempsPlein() != null) {
			NSTimestamp dateFinTpsPartiel;
			if (tempsPartiel.dFinExecution() != null)
				dateFinTpsPartiel = tempsPartiel.dFinExecution();
			else
				dateFinTpsPartiel = tempsPartiel.dateFin();

			if (dateFinTpsPartiel != null && DateCtrl.isBefore(dRepriseTempsPlein(), dateFinTpsPartiel)) {
				return "DATES_DETAIL_TEMPS_PARTIEL";
			}
			if (DateCtrl.isBefore(dRepriseTempsPlein(), tempsPartiel.dateDebut())) {
				return "DATES_DETAIL_TEMPS_PARTIEL";
			}
		}
		return null;
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number) valeursAttributs.objectForKey(ID_SOURCE_KEY);
		if (idSource == null) {
			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
		}
		Number tpSource = (Number) valeursAttributs.objectForKey(TP_SOURCE_KEY);
		if (tpSource == null) {
			resultat = resultat + "Tp_Source >>" + System.getProperty("line.separator");
		}

		return resultat;
	}

	public static EORepriseTempsPlein rtpPourSourceId(EOEditingContext editingContext, Integer sourceId) {
		if (sourceId==null)
			return null;
		
		EORepriseTempsPlein resultat=null;
		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, RTP_SOURCE_KEY, sourceId);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EORepriseTempsPlein)premierObjetValide(resultats);

		return resultat;
	}
}
