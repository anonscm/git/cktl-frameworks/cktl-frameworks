/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;
import org.cocktail.connecteur.serveur.modele.importer.EOMessageErreur;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class ObjetImportPourIndividuOuStructure extends ObjetImportPourIndividu {
	public ObjetImportPourIndividuOuStructure() {
		super();
	}

	public abstract EOStructure structure();

	public abstract void setStructureRelationship(EOStructure value);

	public abstract String strSource();
	
	public abstract void setStrSource(String strSource);
	
	/** Prepare les relations. A appeler apr&egrave;s avoir insere un objet dans l'editing context */
	public void preparerRelations() {
		super.preparerRelations();
		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		setStructureRelationship(null);
	}
	
	/** Verifie que les relations du record sont bien valides : il se peut que l'individu ou/et la structure aient ete 
	 * crees juste auparavant. Retourne un message de log
	 * 
	 */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();

		if (log == null) {	// pas d'individu lié ou pas d'erreur
			// vérifier si il faut aussi une structure
			if (individu() == null || (individu() != null && doitAvoirIndividuEtStructure())) {
				if (structure() == null && strSource() != null) {
					EOStructure structure = EOStructure.structurePourSourceId(editingContextTravail(), strSource());
					if (structure != null) {
						addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
					}
					log = preparerLog(structure,log);
					if (individu() != null) {
						log.addObjectToBothSidesOfRelationshipWithKey(individu(), "individu");	
					}
					return log;
				} else if (structure() != null && structure().statut().equals(STATUT_ERREUR)) {
					log = new EOLogImport();
					log.initAvecEditingContext(editingContextTravail(), this.entityName(), "STRUCTURE_INVALIDE");
					setStatut(STATUT_ERREUR);
					return log;
				} else {
					return null;
				}
			} else {
				return null;	// Individu trouvé
			}
		} else {	// log non nul => problème sur les individus
			if (strSource() != null) {
				if (structure() == null) {
					EOStructure structure = EOStructure.structurePourSourceId(editingContextTravail(), strSource());
					if (structure != null) {
						addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
					}
					return preparerLog(structure,log);
				} else {	// Il y a une structure définie
					if (doitAvoirIndividuEtStructure()) {
						return log;		// Retourner le log pour les individus
					}  else {
						return null;	// Structure trouvée
					}
//					return log;
				}
			} else {	// Il n'y a pas de strSource
				if (doitAvoirIndividuEtStructure()) {
					EOMessageErreur message = EOMessageErreur.messagePourCle(editingContextTravail(), "STRUCTURE_INDIVIDU_INVALIDES");
					if (message != null) {
						log.addObjectToBothSidesOfRelationshipWithKey(message, "messageErreur");
					}
				}
				return log;		// Retourner le log pour les individus
			}
		}
	}
	/** Retourne la relation sur la structure ou l'individu */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		if (nomAttribut.equals("strSource")) {
			return "structure";
		} else {
			return super.nomRelationPourAttributComparaison(nomAttribut);
		}
	}
	// Méthodes protégées
	/** Retourne false si l'individu associe est en mode Insertion. A fortiori les objets qui en dependent le seront */
	protected boolean estNouvelIndividuOuStructure() {
		if (doitAvoirIndividuEtStructure()) {
			return (individu() == null || individu().operation().equals(ObjetImport.OPERATION_INSERTION) ||
					structure() == null || structure().operation().equals(ObjetImport.OPERATION_INSERTION));
		} else if (individu() == null) {
			return structure() == null || structure().operation().equals(ObjetImport.OPERATION_INSERTION);
		} else {
			return super.estNouvelIndividu();
		}
	}

	// Méthodes statiques
	/** Retourne les objets de type nomEntite lies a une structure */
	public static NSArray rechercherObjetsImportCourantPourStructure(String nomEntite,EOStructure structure) {
		NSMutableArray args = new NSMutableArray(structure);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("structure = %@ AND temImport = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (nomEntite, qualifier,null);
		return structure.editingContext().objectsWithFetchSpecification(myFetch);
	}
	// Méthodes abstraites
	protected abstract boolean doitAvoirIndividuEtStructure();
	// Méthodes privées
	private EOLogImport preparerLog(EOStructure structure,EOLogImport logOriginal) {
		String messageLog = null;
		if (logOriginal == null) {
			if (structure == null) {
				messageLog = "STRUCTURE_NON_IMPORTEE";
			} else if (structure.temImport().equals(ObjetImport.A_TRANSFERER)) {
				if (structure.statut().equals(EOIndividu.STATUT_ERREUR)) {
					messageLog = "STRUCTURE_INVALIDE";
				}
			}
			if (messageLog != null) {
				EOLogImport log = new EOLogImport();
				log.initAvecEditingContext(editingContextTravail(),this.entityName(),messageLog);
				log.ajouterRelation(this);
				if (structure != null) {
					log.addObjectToBothSidesOfRelationshipWithKey(structure, "structure");
				}
				setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
				return log;
			} else {
				return null;
			}
		} else {	// Il y a déjà un log pour les individus
			if (doitAvoirIndividuEtStructure()) {
				if (individu() == null) {
					if (structure == null) {
						messageLog = "INDIVIDU_STRUCTURE_NON_IMPORTES";
					} else if (structure.statut().equals(EOIndividu.STATUT_ERREUR)) {
						messageLog = "INDIVIDU_NON_IMPORTE_STRUCTURE_INVALIDE";
					}
				} else {
					if (structure == null) {
						messageLog = "INDIVIDU_INVALIDE_STRUCTURE_NON_IMPORTE";
					} else {
						messageLog = "INDIVIDU_STRUCTURE_INVALIDES";
					}
				}
			}
			if (messageLog != null) {
				logOriginal.setStrSource(strSource());
				logOriginal.ajouterRelation(this);
				if (structure != null) {
					logOriginal.addObjectToBothSidesOfRelationshipWithKey(structure, "structure");
				}
				EOMessageErreur message = EOMessageErreur.messagePourCle(editingContextTravail(), messageLog);
				if (message != null) {
					logOriginal.addObjectToBothSidesOfRelationshipWithKey(message, "messageErreur");
				}
			}
			return logOriginal;
		} 
	} 
}
