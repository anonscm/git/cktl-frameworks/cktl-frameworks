// EOCgntCgm.java
// Created on Tue Feb 14 09:17:51 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;


import org.cocktail.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.conges.PeriodeCongeAvecDateCommission;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** Le cong&eacue; de grave maladie ne concerne que les agents contractuels ou les contractuels assimiles qui ont une carri&egrave;re (TEM_fonctionnaire = N dans type_population).<BR>
 * L'individu doit avoir un contrat de travail ou une carri&egrave;re de non fonctionnaire en activite ou detachement avec+carri&egrave;re 
 * d'accueil pendant tout le conge.<BR>
 * La duree maximale du conge est de 6 mois<BR>
 * L'individu ne peut pas prendre de cgm consecutifs au-dela de trois ans : il doit alors reprendre son travail pendant 1 an
 * */
public class EOCgntCgm extends _EOCgntCgm {
	public EOCgntCgm() {
        super();
    }


    @Override
	public String nomRelationPourLog() {
		return EOLogImport.CGNT_CGM_KEY;
	}

    // Méthodes ajoutées
    /** Pas de date de commission */
	public NSTimestamp dateCommission() {
		return null;
	}
	protected String verifierConge(EOEditingContext editingContext) {
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource());
		if (individu != null) {
			if (estContractuelSurPeriodeComplete(editingContext,individu,dateDebut(),dateFin()) == false) {
				return "CONTRACTUEL_SUR_PERIODE";
			}
		} else {
			LogManager.logDetail("Pas d'individu pour idSource " + idSource());
		}
		double nbMois = dureeEnMois(dateDebut(),dateFin());
		// on ne vérifie pas les durées minimum car ce n'est pas bloquant
		int nbMoisMax = dureePourCle(editingContext,"cgmDuree2");
		if (nbMois > nbMoisMax) {
			return "DUREE_MAX_CGM"; 
		}
		// vérifier, si il existe des congés continus pour une période de 3 ans, qu'il existe plus d'une année écoulée avant la reprise d'un nouveau Clm
		LogManager.logDetail("Dates conge : [" + DateCtrl.dateToString(dateDebut()) + "," + DateCtrl.dateToString(dateFin()) + "]");
		NSArray periodesAnterieures = preparerPeriodesAnterieures(editingContext, dateDebut());
		if (periodesAnterieures.count() > 0) {
			// triés par ordre décroissant
			PeriodeCongeAvecDateCommission dernierCgm = (PeriodeCongeAvecDateCommission)periodesAnterieures.objectAtIndex(0);
			int dureePeriodeRef = dureePourCle(editingContext,"cgmDelai");	// durée fournie en année
			LogManager.logDetail("Evaluation de la duree entre clm précédent et clm courant");
			if (dureeEnMois(dernierCgm.dateFin(),dateDebut()) < dureePeriodeRef * 12) {
				// moins d'un an écoulé
				nbMoisMax = dureePourCle(editingContext,"cgmPleinTraitement") + dureePourCle(editingContext,"cgmDemiTraitement");
				NSArray periodesContigues = preparerPeriodesContigues(editingContext, periodesAnterieures, dateDebut(), dateFin());
				if (dureeEnMoisConges(periodesContigues) >= nbMoisMax) {	// 3 ans de congés continus
					return "REPRISE_TRAVAIL_CGM";
				}
			}
		}
		return null;
	}
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		return CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
	}

	@Override
	public Integer eimpSource() {
		return cgmSource();
	}


	@Override
	public void setEimpSource(Integer value) {
		setCgmSource(value);
	}

	@Override
	public String sourceKey() {
		return CGM_SOURCE_KEY;
	}


	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCgntCgm)conge);
	}
}
