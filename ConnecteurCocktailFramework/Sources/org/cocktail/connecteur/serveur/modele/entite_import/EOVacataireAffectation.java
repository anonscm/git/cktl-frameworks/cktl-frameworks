package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOVacataireAffectation extends _EOVacataireAffectation {

	@Override
	public void preparerRelations() {
		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);
		
		EOVacataires vacataires=EOVacataires.vacatairesPourSourceId(editingContext(),vacSource());
		setVacatairesRelationship(vacataires);
}

	@Override
	public void supprimerRelations() {
		setStructureRelationship(null);
		setVacatairesRelationship(null);
	}

	
	@Override
	public EOLogImport verifierRecord() {
		EOLogImport log = null;

		String messageLog = null;
		if (structure() == null && strSource() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure() != null && structure().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		} else if (vacataires() == null && vacSource() != null) {
			messageLog = "VACATAIRE_NON_IMPORTE";
		} else if (vacataires() != null && vacataires().statut().equals(STATUT_ERREUR)) {
			messageLog = "VACATAIRE_INVALIDE";
		} 
		
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		} 
		
		return log;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.VACATAIRE_AFFECTATION_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		return "";
	}

	@Override
	public EOLogImport verifierRecordApresOperation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}
}
