//EOContrat.java
//Created on Mon Jan 28 15:07:54 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import java.util.HashMap;

import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 16/03/2011 - vérification de la nomenclature des Rne
public class EOContrat extends _EOContrat implements InterfaceHomonymie, InterfaceInvalidationRecordAvecDependance {

	private static HashMap<Integer, EOContrat> idSourceCache = new HashMap<Integer, EOContrat>();

	public EOContrat() {
		super();
	}

	/**
	 * Verifie les donnees du contrat en fonction du contrat de travail. Verifie
	 * si il s'agit d'une insertion d'un contrat qui n'est pas une vacation et
	 * qui n'est pas annul&eacute, que la quotite totale des autres contrats qui
	 * ne sont pas des vacations est < 100%
	 */
	public EOLogImport verifierRecordApresOperation() {
		if (EOImportParametres.cacheMemoireNiveauHautOuPlus())
			idSourceCache.put(idSource() * 100 + ctrSource(), this);

		EOTypeContratTravail typeContrat = EOTypeContratTravail.getFromCode(editingContext(), cTypeContratTrav());
		String messageLog = null;
		if (!typeContrat.estCdi()) {
			if (dFinContratTrav() == null) {
				LogManager.logDetail("Insertion d'un log pour un contrat de CDD");
				messageLog = "CONTRAT_CDD";
			}
		}
		if (typeContrat.estVacataire()) {
			LogManager.logDetail("Insertion d'un log pour un contrat de type vacataire");
			messageLog = "CONTRAT_VACATAIRE";
		}
		if (messageLog == null) {
			Integer dureeContrat = DateCtrl.calculerDureeEnMois(dDebContratTrav(), dFinContratTrav(), true);

			if (typeContrat.dureeInitContrat() != null) {
				// vérifier si la durée du contrat est inférieure à la période
				// min. définie dans le type de contrat
				int dureeMin = typeContrat.dureeInitContrat().intValue();
				if (dureeContrat == null || dureeContrat.intValue() < dureeMin) {
					LogManager.logDetail("Insertion d'un log pour la durée min. du contrat");
					messageLog = "DUREE_MIN_CONTRAT";
				}
			}
			// vérifier si la durée du contrat dépasse la période max. définie
			// dans le type de contrat
			if (messageLog == null && typeContrat.dureeMaxContrat() != null) {
				int dureeMax = typeContrat.dureeMaxContrat().intValue();
				// ce type de contrat est forcément un cdd
				if (dureeContrat == null || dureeContrat.intValue() > dureeMax) {
					LogManager.logDetail("Insertion d'un log pour la durée max. du contrat");
					messageLog = "DUREE_MAX_CONTRAT";
				}
			}
		}
		// si il s'agit d'une insertion d'un contrat de type rémunération
		// principale,
		// vérifier la quotité des autres contrats sur la même période pour voir
		// si on autorise l'ajout du contrat
		if (messageLog == null && typeContrat.estRemunerationPrincipale() && individu() != null) {
			// vérifier la quotité des autres contrats de l'import pour voir si
			// on autorise un contrat
			// entre ces dates dans le cas où le contrat n'est pas annulé
			double quotiteTotale = EOContratAvenant.calculerQuotiteTotalePourPeriode(editingContextTravail(), individu(), null, dDebContratTrav(),
					dFinContratTrav());
			if (quotiteTotale > 100) {
				LogManager.logDetail("Insertion d'un log pour l'impossibilité d'ajouter un contrat, quotité totale " + quotiteTotale);
				messageLog = "AJOUT_CONTRAT_IMPOSSIBLE";
			}
		}
		if (messageLog != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.setIdSource(individu().idSource());
			if (individu() != null) {
				log.addObjectToBothSidesOfRelationshipWithKey(individu(), "individu");
			}
			log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
			setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une
										// erreur
			return log;
		}
		return null;
	}

	/**
	 * Contrat du SI destinataire avec le m&ecirc;me individu et commencant a la
	 * m&ecirc;me date debut et avec le m&circ;me type de contrat de travail
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null) {
			if (estNouvelIndividu()) {
				return null;
			} else {
				return EOMangueContrat.contratDestinationPourContrat(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	public String nomRelationPourLog() {
		return "contrat";
	}

	public boolean estNouveauContrat() {
		if (super.estNouvelIndividu()) {
			return true;
		}
		return operation().equals(ObjetImport.OPERATION_INSERTION);
	}

	// InterfaceHomonymie
	/**
	 * true si il existe plusieurs contrats avec la m&ecirc;me date debut et le
	 * m&ecirc;me type de contrat
	 */
	public boolean aHomonymeDansSIDestinataire() {
		return EOMangueContrat.aHomonyme(editingContextTravail(), this);
	}

	/**
	 * Retourne false car il peut y avoir plusieurs contrats avec les
	 * m&ecirc;mes caracteristiques
	 */
	public boolean memesDonneesQueRecord(ObjetImport record) {
		return false;
	}

	public String invaliderRecordsDependants() {
		try {
			// Rechercher les avenants dépendant de ce contrat
			invaliderObjets(EOContratAvenant.rechercherAvenantsImportCourantPourContrat(this));
			return null;
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		}
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number) valeursAttributs.objectForKey("idSource");
		if (idSource == null) {
			// On fait ici la vérification car il n'est pas marqué comme
			// obligatoire dans DescriptionImport.XML
			// afin de pouvoir générer des contrats pour les logs quand
			// l'individu est inconnu
			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
		}
		String typeContratTrav = (String) valeursAttributs.objectForKey("cTypeContratTrav");
		// Vérifier dans la nomenclature si ce type de contrat existe
		EOTypeContratTravail typeContrat = EOTypeContratTravail.getFromCode(editingContext, typeContratTrav);
		if (typeContrat == null) {
			resultat = resultat + "Type_Contrat_Inconnu >>" + typeContratTrav + System.getProperty("line.separator");
		}
		if (typeContratTrav.equals("VA") || typeContratTrav.equals("VF") || typeContratTrav.equals("VN")) {
			resultat = resultat + "Type_Contrat_Vacataire >>" + typeContratTrav + System.getProperty("line.separator");
		}
		// 16/03/2011 - vérifier les rne
		String rne = (String) valeursAttributs.objectForKey("cRne");
		if (rne != null) {
			// Vérifier dans la nomenclature si ce rene existe
			EORne rneRecord = EORne.getFromCode(editingContext, rne);
			if (rneRecord == null) {
				resultat = resultat + "Rne_Inconnu >>" + rne + System.getProperty("line.separator");
			}
		}
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey("dFinContratTrav");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp) valeursAttributs.objectForKey("dDebContratTrav");
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "temFonctionnaire");
		resultat = resultat + verifierTemoin(valeursAttributs, "temBudgetPropre");
		resultat = resultat + verifierTemoin(valeursAttributs, "temAnnulation");
		String temBudgetPropre = (String) valeursAttributs.objectForKey("temBudgetPropre");
		// Vérifier la cohérence des données entre type contrat travail et
		// temBudgetProtre
		if (typeContratTrav.equals(EOTypeContratTravail.CODE_CDD_BUDGET_PROPRE) && temBudgetPropre.equals(CocktailConstantes.FAUX)) {
			valeursAttributs.setObjectForKey(CocktailConstantes.VRAI, "temBudgetPropre");
		}
		return resultat;
	}

	/**
	 * Retourne true si l'individu a des contrats de remuneration principale
	 * dans l'import courant ou dans le SI Destinataire ou des segments de
	 * carri&egrave;re comme population non fonctionnaie dans l'import courant
	 * ou le SI Destinataire
	 * 
	 * @param editingContext
	 * @return
	 */
	public static boolean estContractuelSurPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSArray contrats = EOContrat.rechercherContratsRemunerationPrincipalePourIndividuEtPeriode(editingContext, individu, debutPeriode, finPeriode, false);
		if (contrats != null && contrats.count() > 0) {
			return true;
		}
		contrats = EOMangueContrat.rechercherContratsRemunerationPrincipalePourIndividuEtPeriode(editingContext, individu, debutPeriode, finPeriode);
		if (contrats != null && contrats.count() > 0) {
			return true;
		}
		NSArray intervalles = EOChangementPosition.intervallesPourChangementsEnActiviteEtNonFonctionnairePendantPeriode(editingContext, individu.idSource(),
				debutPeriode, finPeriode);
		return intervalles.count() > 0;
	}

	/** Retourne tous les contrats de l'import courant valides sur la periode */
	public static NSArray rechercherContratsRemunerationPrincipalePourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu,
			NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean shouldRefresh) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("statut <> 'A' AND statut <> 'E' AND individu = %@",
				new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode("dDebContratTrav", debutPeriode, "dFinContratTrav", finPeriode));

		EOFetchSpecification myFetch = new EOFetchSpecification("Contrat", new EOAndQualifier(qualifiers), null);
		myFetch.setRefreshesRefetchedObjects(shouldRefresh);
		// Rechercher les contrats de rémunération principale
		NSMutableArray contratsValides = new NSMutableArray();
		java.util.Enumeration e = editingContext.objectsWithFetchSpecification(myFetch).objectEnumerator();
		while (e.hasMoreElements()) {
			EOContrat contrat = (EOContrat) e.nextElement();
			String cTypeContrat = contrat.cTypeContratTrav();
			EOTypeContratTravail typeContrat = EOTypeContratTravail.getFromCode(editingContext, cTypeContrat);
			if (typeContrat != null && typeContrat.estRemunerationPrincipale()) {
				contratsValides.addObject(contrat);
			}
		}
		return contratsValides;
	}

	public static NSArray rechercherContratsPourIndividuEtPeriode(EOEditingContext editingContext, Number idSource, NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(ID_SOURCE_KEY + " = %@ AND " + STATUT_KEY + " <> 'A' AND " + STATUT_KEY + " <> 'E'",
				new NSArray(idSource));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(Finder.qualifierPourPeriode(D_DEB_CONTRAT_TRAV_KEY, dateDebut, D_FIN_CONTRAT_TRAV_KEY, dateFin));
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		NSArray contrats = editingContext.objectsWithFetchSpecification(myFetch);
		return contrats;
	}

	public static EOContrat contratPourSourceId(EOEditingContext editingContext, Integer idSource, Integer ctrSource) {
		if (ctrSource == null)
			return null;

		EOContrat resultat = null;
		if (EOImportParametres.cacheMemoireNiveauHautOuPlus()) {
			resultat = idSourceCache.get(idSource * 100 + ctrSource);
			if (resultat != null)
				return resultat;
		}

		if (editingContext == null || idSource == null)
			return null;

		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(ctrSource);
		args.addObject(STATUT_ANNULATION);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
				ID_SOURCE_KEY + " = %@ AND " + CTR_SOURCE_KEY + " = %@ AND " + STATUT_KEY + " <> %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, myQualifier, null);
		NSArray resultats = editingContext.objectsWithFetchSpecification(myFetch);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOContrat)premierObjetValide(resultats);

		if (EOImportParametres.cacheMemoireNiveauHautOuPlus() && resultat != null) {
			idSourceCache.put(idSource * 100 + ctrSource, resultat);
		}
		return resultat;
	}

	public static void resetCache() {
		idSourceCache.clear();
	}
}
