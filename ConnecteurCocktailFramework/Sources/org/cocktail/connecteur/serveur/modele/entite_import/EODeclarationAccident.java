package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOLieuAccident;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeAccidentTrav;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EODeclarationAccident extends _EODeclarationAccident {
	private static final long serialVersionUID = 3909845285369013080L;
	
	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.DECLARATION_ACCIDENT_KEY;
	}
	
	/**
	 * 
	 */
	public EOLogImport verifierRecord() {
		return super.verifierRecord();
	}

	
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log=null;
		
		String message =  verifierDatesDeclarationAccident();
		if (message != null) {
			log = new EOLogImport(); 
			log.initAvecEditingContext(editingContextTravail(),this.entityName(), message);
			log.setIdSource(new Integer(idSource().intValue()));
			log.setEimpSource(new Integer(daccSource().intValue()));
			log.setIndividuRelationship(individu());
			setStatut(STATUT_ERREUR);
		}
		return log;
	}
	
	// Méthodes privées
	private String verifierDatesDeclarationAccident() {
		
		if (daccHdebAm() != null && daccHfinAm() != null && heureDebutAnterieureHeureFin(daccHdebAm(), daccHfinAm()) == false) {
			return "DATES_MATIN_ERREUR_DECLARATION_ACCIDENT";
		}
		if (daccHdebPm() != null && daccHfinPm() != null && heureDebutAnterieureHeureFin(daccHdebPm(), daccHfinPm()) == false) {
			return "DATES_APREM_ERREUR_DECLARATION_ACCIDENT";
		}
		if (daccHdebPm() != null && daccHfinAm() != null && heureDebutAnterieureHeureFin(daccHfinAm(), daccHdebPm()) == false) {
			return "DATES_ERREUR_DECLARATION_ACCIDENT";
		}
		return null;
	}
	
	private boolean heureDebutAnterieureHeureFin(String heureDebutStr,String heureFinStr) {
		int heureDebut = new Integer(heureDebutStr.substring(0,2)).intValue();
		int heureFin = new Integer(heureFinStr.substring(0,2)).intValue();
		if (heureDebut > heureFin) {
			return false;
		} else if (heureDebut == heureFin) {
			int minutesDebut = new Integer(heureDebutStr.substring(3,5)).intValue();
			int minutesFin = new Integer(heureFinStr.substring(3,5)).intValue();
			if (minutesDebut > minutesFin) {
				return false;
			}
		}
		return true;
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = "";
		String valeur;
		
		valeur= (String)valeursAttributs.objectForKey(DACC_SUITE_KEY);
		if (valeur != null && !valeur.equals("S") && !valeur.equals("A") && !valeur.equals("D")) {
			resultat+= "Valeur_Temoin >>" + DACC_SUITE_KEY + " : " + valeur + System.getProperty("line.separator");
		}
		
		valeur = (String)valeursAttributs.objectForKey(C_TYPE_ACCIDENT_KEY);
		if (valeur != null) {
			EOTypeAccidentTrav typeAccTrav = EOTypeAccidentTrav.fetchTypeAccidentTrav(editingContext, EOTypeAccidentTrav.C_TYPE_ACC_TRAV_KEY, valeur);
			if (typeAccTrav == null) {
				resultat = resultat + "Type_AccidentTravail_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		
		Integer valeurInt = (Integer)valeursAttributs.objectForKey(LACC_ORDRE_KEY);
		if (valeurInt != null) {
			EOLieuAccident lieuAccTrav = EOLieuAccident.fetchLieuAccident(editingContext, EOLieuAccident.LACC_ORDRE_KEY, valeurInt);
			if (lieuAccTrav == null) {
				resultat = resultat + "Lieu_AccidentTravail_Inconnu >>" + valeurInt + System.getProperty("line.separator");
			}
		}

		return resultat;
	}

}
