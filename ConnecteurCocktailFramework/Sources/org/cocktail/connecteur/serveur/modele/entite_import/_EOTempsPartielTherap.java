// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTempsPartielTherap.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTempsPartielTherap extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "TempsPartielTherap";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_ARRETE_ANNULATION_KEY = "dateArreteAnnulation";
	public static final String DATE_COMITE_MEDICAL_KEY = "dateComiteMedical";
	public static final String DATE_COMITE_MEDICAL_SUP_KEY = "dateComiteMedicalSup";
	public static final String DATE_COMITE_REFORME_KEY = "dateComiteReforme";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_KEY = "quotite";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TPT_SOURCE_KEY = "tptSource";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOTempsPartielTherap.class);

  public EOTempsPartielTherap localInstanceIn(EOEditingContext editingContext) {
    EOTempsPartielTherap localInstance = (EOTempsPartielTherap)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateArreteAnnulation() {
    return (NSTimestamp) storedValueForKey("dateArreteAnnulation");
  }

  public void setDateArreteAnnulation(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dateArreteAnnulation from " + dateArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArreteAnnulation");
  }

  public NSTimestamp dateComiteMedical() {
    return (NSTimestamp) storedValueForKey("dateComiteMedical");
  }

  public void setDateComiteMedical(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dateComiteMedical from " + dateComiteMedical() + " to " + value);
    }
    takeStoredValueForKey(value, "dateComiteMedical");
  }

  public NSTimestamp dateComiteMedicalSup() {
    return (NSTimestamp) storedValueForKey("dateComiteMedicalSup");
  }

  public void setDateComiteMedicalSup(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dateComiteMedicalSup from " + dateComiteMedicalSup() + " to " + value);
    }
    takeStoredValueForKey(value, "dateComiteMedicalSup");
  }

  public NSTimestamp dateComiteReforme() {
    return (NSTimestamp) storedValueForKey("dateComiteReforme");
  }

  public void setDateComiteReforme(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dateComiteReforme from " + dateComiteReforme() + " to " + value);
    }
    takeStoredValueForKey(value, "dateComiteReforme");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Double quotite() {
    return (Double) storedValueForKey("quotite");
  }

  public void setQuotite(Double value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public Integer tptSource() {
    return (Integer) storedValueForKey("tptSource");
  }

  public void setTptSource(Integer value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherap.LOG.debug( "updating tptSource from " + tptSource() + " to " + value);
    }
    takeStoredValueForKey(value, "tptSource");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOTempsPartielTherap.LOG.isDebugEnabled()) {
      _EOTempsPartielTherap.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOTempsPartielTherap createTempsPartielTherap(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, Double quotite
, String statut
, String temImport
) {
    EOTempsPartielTherap eo = (EOTempsPartielTherap) EOUtilities.createAndInsertInstance(editingContext, _EOTempsPartielTherap.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setQuotite(quotite);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOTempsPartielTherap> fetchAllTempsPartielTheraps(EOEditingContext editingContext) {
    return _EOTempsPartielTherap.fetchAllTempsPartielTheraps(editingContext, null);
  }

  public static NSArray<EOTempsPartielTherap> fetchAllTempsPartielTheraps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTempsPartielTherap.fetchTempsPartielTheraps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTempsPartielTherap> fetchTempsPartielTheraps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTempsPartielTherap.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTempsPartielTherap> eoObjects = (NSArray<EOTempsPartielTherap>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTempsPartielTherap fetchTempsPartielTherap(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartielTherap.fetchTempsPartielTherap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartielTherap fetchTempsPartielTherap(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTempsPartielTherap> eoObjects = _EOTempsPartielTherap.fetchTempsPartielTheraps(editingContext, qualifier, null);
    EOTempsPartielTherap eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTempsPartielTherap)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TempsPartielTherap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartielTherap fetchRequiredTempsPartielTherap(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartielTherap.fetchRequiredTempsPartielTherap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartielTherap fetchRequiredTempsPartielTherap(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTempsPartielTherap eoObject = _EOTempsPartielTherap.fetchTempsPartielTherap(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TempsPartielTherap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartielTherap localInstanceIn(EOEditingContext editingContext, EOTempsPartielTherap eo) {
    EOTempsPartielTherap localInstance = (eo == null) ? null : (EOTempsPartielTherap)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
