// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCarriereSpecialisations.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCarriereSpecialisations extends ObjetImportPourCarriere {
	public static final String ENTITY_NAME = "CarriereSpecialisations";

	// Attributes
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String C_BAP_KEY = "cBap";
	public static final String C_DISC_SECOND_DEGRE_KEY = "cDiscSecondDegre";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String C_SPECIALITE_ITARF_KEY = "cSpecialiteItarf";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEBUT_KEY = "dDebut";
	public static final String D_FIN_KEY = "dFin";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String SPEC_SOURCE_KEY = "specSource";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOCarriereSpecialisations.class);

  public EOCarriereSpecialisations localInstanceIn(EOEditingContext editingContext) {
    EOCarriereSpecialisations localInstance = (EOCarriereSpecialisations)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public String cDiscSecondDegre() {
    return (String) storedValueForKey("cDiscSecondDegre");
  }

  public void setCDiscSecondDegre(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating cDiscSecondDegre from " + cDiscSecondDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiscSecondDegre");
  }

  public String codeemploi() {
    return (String) storedValueForKey("codeemploi");
  }

  public void setCodeemploi(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating codeemploi from " + codeemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeemploi");
  }

  public String cSectionCnu() {
    return (String) storedValueForKey("cSectionCnu");
  }

  public void setCSectionCnu(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating cSectionCnu from " + cSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSectionCnu");
  }

  public String cSousSectionCnu() {
    return (String) storedValueForKey("cSousSectionCnu");
  }

  public void setCSousSectionCnu(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating cSousSectionCnu from " + cSousSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSousSectionCnu");
  }

  public String cSpecialiteAtos() {
    return (String) storedValueForKey("cSpecialiteAtos");
  }

  public void setCSpecialiteAtos(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating cSpecialiteAtos from " + cSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteAtos");
  }

  public String cSpecialiteItarf() {
    return (String) storedValueForKey("cSpecialiteItarf");
  }

  public void setCSpecialiteItarf(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating cSpecialiteItarf from " + cSpecialiteItarf() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteItarf");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebut() {
    return (NSTimestamp) storedValueForKey("dDebut");
  }

  public void setDDebut(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating dDebut from " + dDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebut");
  }

  public NSTimestamp dFin() {
    return (NSTimestamp) storedValueForKey("dFin");
  }

  public void setDFin(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating dFin from " + dFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dFin");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer specSource() {
    return (Integer) storedValueForKey("specSource");
  }

  public void setSpecSource(Integer value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating specSource from " + specSource() + " to " + value);
    }
    takeStoredValueForKey(value, "specSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOCarriereSpecialisations createCarriereSpecialisations(EOEditingContext editingContext, Integer carSource
, NSTimestamp dCreation
, NSTimestamp dDebut
, NSTimestamp dModification
, Integer idSource
, Integer specSource
, String statut
, String temImport
) {
    EOCarriereSpecialisations eo = (EOCarriereSpecialisations) EOUtilities.createAndInsertInstance(editingContext, _EOCarriereSpecialisations.ENTITY_NAME);    
		eo.setCarSource(carSource);
		eo.setDCreation(dCreation);
		eo.setDDebut(dDebut);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setSpecSource(specSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCarriereSpecialisations> fetchAllCarriereSpecialisationses(EOEditingContext editingContext) {
    return _EOCarriereSpecialisations.fetchAllCarriereSpecialisationses(editingContext, null);
  }

  public static NSArray<EOCarriereSpecialisations> fetchAllCarriereSpecialisationses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCarriereSpecialisations.fetchCarriereSpecialisationses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCarriereSpecialisations> fetchCarriereSpecialisationses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCarriereSpecialisations.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCarriereSpecialisations> eoObjects = (NSArray<EOCarriereSpecialisations>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCarriereSpecialisations fetchCarriereSpecialisations(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereSpecialisations.fetchCarriereSpecialisations(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriereSpecialisations fetchCarriereSpecialisations(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCarriereSpecialisations> eoObjects = _EOCarriereSpecialisations.fetchCarriereSpecialisationses(editingContext, qualifier, null);
    EOCarriereSpecialisations eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCarriereSpecialisations)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CarriereSpecialisations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriereSpecialisations fetchRequiredCarriereSpecialisations(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereSpecialisations.fetchRequiredCarriereSpecialisations(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriereSpecialisations fetchRequiredCarriereSpecialisations(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCarriereSpecialisations eoObject = _EOCarriereSpecialisations.fetchCarriereSpecialisations(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CarriereSpecialisations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriereSpecialisations localInstanceIn(EOEditingContext editingContext, EOCarriereSpecialisations eo) {
    EOCarriereSpecialisations localInstance = (eo == null) ? null : (EOCarriereSpecialisations)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
