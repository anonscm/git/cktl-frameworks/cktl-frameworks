// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntMaladie.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntMaladie extends CongeMaladie {
	public static final String ENTITY_NAME = "CgntMaladie";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String C_ANCIENNETE_KEY = "cAnciennete";
	public static final String CGNT_SOURCE_KEY = "cgntSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRET_TRAVAIL_KEY = "dArretTravail";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_DUREE_CONTINUE_KEY = "temDureeContinue";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_JOUR_CARENCE_KEY = "temJourCarence";
	public static final String TEM_PROLONG_MALADIE_KEY = "temProlongMaladie";
	public static final String TEM_REQUALIF_MALADIE_KEY = "temRequalifMaladie";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCgntMaladie.class);

  public EOCgntMaladie localInstanceIn(EOEditingContext editingContext) {
    EOCgntMaladie localInstance = (EOCgntMaladie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public String cAnciennete() {
    return (String) storedValueForKey("cAnciennete");
  }

  public void setCAnciennete(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating cAnciennete from " + cAnciennete() + " to " + value);
    }
    takeStoredValueForKey(value, "cAnciennete");
  }

  public Integer cgntSource() {
    return (Integer) storedValueForKey("cgntSource");
  }

  public void setCgntSource(Integer value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating cgntSource from " + cgntSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cgntSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArretTravail() {
    return (NSTimestamp) storedValueForKey("dArretTravail");
  }

  public void setDArretTravail(NSTimestamp value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating dArretTravail from " + dArretTravail() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretTravail");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temDureeContinue() {
    return (String) storedValueForKey("temDureeContinue");
  }

  public void setTemDureeContinue(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating temDureeContinue from " + temDureeContinue() + " to " + value);
    }
    takeStoredValueForKey(value, "temDureeContinue");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temJourCarence() {
    return (String) storedValueForKey("temJourCarence");
  }

  public void setTemJourCarence(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating temJourCarence from " + temJourCarence() + " to " + value);
    }
    takeStoredValueForKey(value, "temJourCarence");
  }

  public String temProlongMaladie() {
    return (String) storedValueForKey("temProlongMaladie");
  }

  public void setTemProlongMaladie(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating temProlongMaladie from " + temProlongMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongMaladie");
  }

  public String temRequalifMaladie() {
    return (String) storedValueForKey("temRequalifMaladie");
  }

  public void setTemRequalifMaladie(String value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
    	_EOCgntMaladie.LOG.debug( "updating temRequalifMaladie from " + temRequalifMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalifMaladie");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
      _EOCgntMaladie.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCgntMaladie.LOG.isDebugEnabled()) {
      _EOCgntMaladie.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCgntMaladie createCgntMaladie(EOEditingContext editingContext, String cAnciennete
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temConfirme
, String temDureeContinue
, String temGestEtab
, String temImport
, String temProlongMaladie
, String temRequalifMaladie
) {
    EOCgntMaladie eo = (EOCgntMaladie) EOUtilities.createAndInsertInstance(editingContext, _EOCgntMaladie.ENTITY_NAME);    
		eo.setCAnciennete(cAnciennete);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemDureeContinue(temDureeContinue);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemProlongMaladie(temProlongMaladie);
		eo.setTemRequalifMaladie(temRequalifMaladie);
    return eo;
  }

  public static NSArray<EOCgntMaladie> fetchAllCgntMaladies(EOEditingContext editingContext) {
    return _EOCgntMaladie.fetchAllCgntMaladies(editingContext, null);
  }

  public static NSArray<EOCgntMaladie> fetchAllCgntMaladies(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntMaladie.fetchCgntMaladies(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntMaladie> fetchCgntMaladies(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntMaladie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntMaladie> eoObjects = (NSArray<EOCgntMaladie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntMaladie fetchCgntMaladie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntMaladie.fetchCgntMaladie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntMaladie fetchCgntMaladie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntMaladie> eoObjects = _EOCgntMaladie.fetchCgntMaladies(editingContext, qualifier, null);
    EOCgntMaladie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntMaladie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntMaladie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntMaladie fetchRequiredCgntMaladie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntMaladie.fetchRequiredCgntMaladie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntMaladie fetchRequiredCgntMaladie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntMaladie eoObject = _EOCgntMaladie.fetchCgntMaladie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntMaladie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntMaladie localInstanceIn(EOEditingContext editingContext, EOCgntMaladie eo) {
    EOCgntMaladie localInstance = (eo == null) ? null : (EOCgntMaladie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
