package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOStage extends _EOStage {
  private static Logger log = Logger.getLogger(EOStage.class);

  
  
  /** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttribut(String nomAttribut) {
		return super.nomRelationPourAttributComparaison(nomAttribut);
	}
  
  
  // Méthodes statiques
 	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
 		LogManager.logDetail("Début du pasage dans validerAttributs de EOStage");
 		String resultat = "";
 		Number idSource = (Number)valeursAttributs.objectForKey("idSource");
 		if (idSource == null) {
 			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
 		}
 		Number carSource = (Number)valeursAttributs.objectForKey("carSource");
 		if (carSource == null) {
 			resultat = resultat + "Car_Source >>" + System.getProperty("line.separator");
 		}
 
		String corps = (String) valeursAttributs.objectForKey(C_CORPS_KEY);
		if (corps != null) {
			// Vérifier dans la nomenclature si ce corps existe
			EOCorps recordCorps = (EOCorps) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOCorps.ENTITY_NAME, EOCorps.C_CORPS_KEY, corps);
			if (recordCorps == null) {
				resultat = resultat + "Corps_Inconnu >>" + corps + System.getProperty("line.separator");
			}
		}

 		// Vérifier que la date de début n'est pas postérieure à la date de fin
 		NSTimestamp dateFin = (NSTimestamp)valeursAttributs.objectForKey(DATE_FIN_KEY);
 		if (dateFin != null) {
 			NSTimestamp dateDebut = (NSTimestamp)valeursAttributs.objectForKey(DATE_DEBUT_KEY);
 			if (DateCtrl.isAfter(dateDebut, dateFin)) {
 				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
 			}
 		}
 		LogManager.logDetail("Fin du pasage dans validerAttributs de EOStage");
 		return resultat;
 	}
  
  /** Verifie que les relations du record sont bien valides : i.e que l'enfant existe bien 
   * cree juste auparavant. Retourne un message de log ou null si pas d'erreur
   */
  public EOLogImport verifierRecord() {
	  EOLogImport log = super.verifierRecord();
//	  log.setEimpSource(eimpSource());
	  if (log != null) {
		  log.setEimpSource(staSource());
	  }
	  
	  return log;
  }

  @Override
  public EOLogImport verifierRecordApresOperation() {
	  // TODO Auto-generated method stub
		return null;
  }

  @Override
  public ObjetPourSIDestinataire destinataireEquivalent() {
	  // TODO Auto-generated method stub
	  return null;
  }

  @Override
  public String nomRelationPourLog() {
	  return EOLogImport.STAGE_KEY;
  }

}
