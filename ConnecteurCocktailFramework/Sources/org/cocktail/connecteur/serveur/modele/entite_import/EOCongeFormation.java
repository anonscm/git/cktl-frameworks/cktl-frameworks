package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeFormation;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOCongeFormation extends _EOCongeFormation {

	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCongeFormation.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	public EOLogImport verifierRecordApresOperation() {
		return super.verifierRecordApresOperation();
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_FORMATION_KEY;
	}

	@Override
	public Integer eimpSource() {
		return cgfSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCgfSource(value);
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		
		Integer value = (Integer) valeursAttributs.objectForKey(C_CHAPITRE_KEY);
		if (value != null) {
			EOChapitre chapitre = EOChapitre.fetchChapitre(editingContext, EOChapitre.C_CHAPITRE_KEY, value);
			if (chapitre == null) {
				resultat += "Chapitre_Inconnu >>" + value + System.getProperty("line.separator");
			}
		}

		resultat+=verifierTemoin(valeursAttributs, TEM_FORM_AGREE_KEY);
		resultat+=verifierTemoin(valeursAttributs, TEM_FRACTIONNE_KEY);
		resultat+=verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);
		return resultat;
	}
}
