// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntCgm.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntCgm extends CongeAvecArrete {
	public static final String ENTITY_NAME = "CgntCgm";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CGM_SOURCE_KEY = "cgmSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCgntCgm.class);

  public EOCgntCgm localInstanceIn(EOEditingContext editingContext) {
    EOCgntCgm localInstance = (EOCgntCgm)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer cgmSource() {
    return (Integer) storedValueForKey("cgmSource");
  }

  public void setCgmSource(Integer value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating cgmSource from " + cgmSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cgmSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
    	_EOCgntCgm.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
      _EOCgntCgm.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCgntCgm.LOG.isDebugEnabled()) {
      _EOCgntCgm.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCgntCgm createCgntCgm(EOEditingContext editingContext, Integer cgmSource
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temConfirme
, String temGestEtab
, String temImport
) {
    EOCgntCgm eo = (EOCgntCgm) EOUtilities.createAndInsertInstance(editingContext, _EOCgntCgm.ENTITY_NAME);    
		eo.setCgmSource(cgmSource);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCgntCgm> fetchAllCgntCgms(EOEditingContext editingContext) {
    return _EOCgntCgm.fetchAllCgntCgms(editingContext, null);
  }

  public static NSArray<EOCgntCgm> fetchAllCgntCgms(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntCgm.fetchCgntCgms(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntCgm> fetchCgntCgms(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntCgm.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntCgm> eoObjects = (NSArray<EOCgntCgm>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntCgm fetchCgntCgm(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntCgm.fetchCgntCgm(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntCgm fetchCgntCgm(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntCgm> eoObjects = _EOCgntCgm.fetchCgntCgms(editingContext, qualifier, null);
    EOCgntCgm eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntCgm)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntCgm that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntCgm fetchRequiredCgntCgm(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntCgm.fetchRequiredCgntCgm(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntCgm fetchRequiredCgntCgm(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntCgm eoObject = _EOCgntCgm.fetchCgntCgm(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntCgm that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntCgm localInstanceIn(EOEditingContext editingContext, EOCgntCgm eo) {
    EOCgntCgm localInstance = (eo == null) ? null : (EOCgntCgm)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
