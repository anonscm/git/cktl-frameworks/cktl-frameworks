// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOOccupation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOOccupation extends ObjetImportPourPersonnel {
	public static final String ENTITY_NAME = "Occupation";

	// Attributes
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String CTR_SOURCE_KEY = "ctrSource";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_OCCUPATION_KEY = "dDebOccupation";
	public static final String D_FIN_OCCUPATION_KEY = "dFinOccupation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EMP_SOURCE_KEY = "empSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String MOTIF_FIN_KEY = "motifFin";
	public static final String NUM_MOYEN_UTILISE_KEY = "numMoyenUtilise";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String OCC_SOURCE_KEY = "occSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";

	// Relationships
	public static final String CONTRAT_KEY = "contrat";
	public static final String EMPLOI_KEY = "emploi";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOOccupation.class);

  public EOOccupation localInstanceIn(EOEditingContext editingContext) {
    EOOccupation localInstance = (EOOccupation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public Integer ctrSource() {
    return (Integer) storedValueForKey("ctrSource");
  }

  public void setCtrSource(Integer value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating ctrSource from " + ctrSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ctrSource");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebOccupation() {
    return (NSTimestamp) storedValueForKey("dDebOccupation");
  }

  public void setDDebOccupation(NSTimestamp value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating dDebOccupation from " + dDebOccupation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebOccupation");
  }

  public NSTimestamp dFinOccupation() {
    return (NSTimestamp) storedValueForKey("dFinOccupation");
  }

  public void setDFinOccupation(NSTimestamp value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating dFinOccupation from " + dFinOccupation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinOccupation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer empSource() {
    return (Integer) storedValueForKey("empSource");
  }

  public void setEmpSource(Integer value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating empSource from " + empSource() + " to " + value);
    }
    takeStoredValueForKey(value, "empSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String motifFin() {
    return (String) storedValueForKey("motifFin");
  }

  public void setMotifFin(String value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating motifFin from " + motifFin() + " to " + value);
    }
    takeStoredValueForKey(value, "motifFin");
  }

  public Double numMoyenUtilise() {
    return (Double) storedValueForKey("numMoyenUtilise");
  }

  public void setNumMoyenUtilise(Double value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating numMoyenUtilise from " + numMoyenUtilise() + " to " + value);
    }
    takeStoredValueForKey(value, "numMoyenUtilise");
  }

  public String observations() {
    return (String) storedValueForKey("observations");
  }

  public void setObservations(String value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating observations from " + observations() + " to " + value);
    }
    takeStoredValueForKey(value, "observations");
  }

  public Integer occSource() {
    return (Integer) storedValueForKey("occSource");
  }

  public void setOccSource(Integer value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating occSource from " + occSource() + " to " + value);
    }
    takeStoredValueForKey(value, "occSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
    	_EOOccupation.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOContrat contrat() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOContrat value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
      _EOOccupation.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi emploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi)storedValueForKey("emploi");
  }

  public void setEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
      _EOOccupation.LOG.debug("updating emploi from " + emploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi oldValue = emploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
      _EOOccupation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOOccupation.LOG.isDebugEnabled()) {
      _EOOccupation.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOOccupation createOccupation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebOccupation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temTitulaire
) {
    EOOccupation eo = (EOOccupation) EOUtilities.createAndInsertInstance(editingContext, _EOOccupation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebOccupation(dDebOccupation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemTitulaire(temTitulaire);
    return eo;
  }

  public static NSArray<EOOccupation> fetchAllOccupations(EOEditingContext editingContext) {
    return _EOOccupation.fetchAllOccupations(editingContext, null);
  }

  public static NSArray<EOOccupation> fetchAllOccupations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOOccupation.fetchOccupations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOOccupation> fetchOccupations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOOccupation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOOccupation> eoObjects = (NSArray<EOOccupation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOOccupation fetchOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOccupation.fetchOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOccupation fetchOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOOccupation> eoObjects = _EOOccupation.fetchOccupations(editingContext, qualifier, null);
    EOOccupation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOOccupation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Occupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOccupation fetchRequiredOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOccupation.fetchRequiredOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOccupation fetchRequiredOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOOccupation eoObject = _EOOccupation.fetchOccupation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Occupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOccupation localInstanceIn(EOEditingContext editingContext, EOOccupation eo) {
    EOOccupation localInstance = (eo == null) ? null : (EOOccupation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
