package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOCongePaternite extends _EOCongePaternite {

	/** Pour ajouter dans le log la reference sur le eimpSource */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null) {
			log.setEimpSource(eimpSource());
		}
		return log;
	}

	// méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
		return resultat;
	}


	//Méthodes protégées
	/** La verification des chevauchements est faite par un autre biais */
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCongePaternite.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}


	@Override
	public EOLogImport verifierRecordApresOperation() {
		return super.verifierRecordApresOperation();
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	public String nomRelationPourLog() {
		return EOLogImport.CONGE_PATERNITE_KEY;
	}

	@Override
	public NSTimestamp dateCommission() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String verifierConge(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eimpSource() {
		return patSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setPatSource(value);
	}

	@Override
	public String sourceKey() {
		return PAT_SOURCE_KEY;
	}

	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCongePaternite)conge);
	}


}
