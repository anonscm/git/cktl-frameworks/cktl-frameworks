// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPeriodesMilitaires.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPeriodesMilitaires extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "PeriodesMilitaires";

	// Attributes
	public static final String C_PERIODE_MILITAIRE_KEY = "cPeriodeMilitaire";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EIMP_SOURCE_KEY = "eimpSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOPeriodesMilitaires.class);

  public EOPeriodesMilitaires localInstanceIn(EOEditingContext editingContext) {
    EOPeriodesMilitaires localInstance = (EOPeriodesMilitaires)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cPeriodeMilitaire() {
    return (String) storedValueForKey("cPeriodeMilitaire");
  }

  public void setCPeriodeMilitaire(String value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating cPeriodeMilitaire from " + cPeriodeMilitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "cPeriodeMilitaire");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer eimpSource() {
    return (Integer) storedValueForKey("eimpSource");
  }

  public void setEimpSource(Integer value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating eimpSource from " + eimpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "eimpSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
      _EOPeriodesMilitaires.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOPeriodesMilitaires createPeriodesMilitaires(EOEditingContext editingContext, String cPeriodeMilitaire
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EOPeriodesMilitaires eo = (EOPeriodesMilitaires) EOUtilities.createAndInsertInstance(editingContext, _EOPeriodesMilitaires.ENTITY_NAME);    
		eo.setCPeriodeMilitaire(cPeriodeMilitaire);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOPeriodesMilitaires> fetchAllPeriodesMilitaireses(EOEditingContext editingContext) {
    return _EOPeriodesMilitaires.fetchAllPeriodesMilitaireses(editingContext, null);
  }

  public static NSArray<EOPeriodesMilitaires> fetchAllPeriodesMilitaireses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPeriodesMilitaires.fetchPeriodesMilitaireses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPeriodesMilitaires> fetchPeriodesMilitaireses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPeriodesMilitaires.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPeriodesMilitaires> eoObjects = (NSArray<EOPeriodesMilitaires>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPeriodesMilitaires fetchPeriodesMilitaires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodesMilitaires.fetchPeriodesMilitaires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriodesMilitaires fetchPeriodesMilitaires(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPeriodesMilitaires> eoObjects = _EOPeriodesMilitaires.fetchPeriodesMilitaireses(editingContext, qualifier, null);
    EOPeriodesMilitaires eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPeriodesMilitaires)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PeriodesMilitaires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriodesMilitaires fetchRequiredPeriodesMilitaires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodesMilitaires.fetchRequiredPeriodesMilitaires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriodesMilitaires fetchRequiredPeriodesMilitaires(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPeriodesMilitaires eoObject = _EOPeriodesMilitaires.fetchPeriodesMilitaires(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PeriodesMilitaires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriodesMilitaires localInstanceIn(EOEditingContext editingContext, EOPeriodesMilitaires eo) {
    EOPeriodesMilitaires localInstance = (eo == null) ? null : (EOPeriodesMilitaires)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
