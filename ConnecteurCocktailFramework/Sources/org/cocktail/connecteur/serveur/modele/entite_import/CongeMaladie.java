/*
 * Created on 13 févr. 2006
 *
 * Modélisation des congés de maladie fonctionnaires et contractuels
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;


import org.cocktail.common.Constantes;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.InterfaceRequalification;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


/** Classe abstraite : modelisation des conges de maladie fonctionnaires et contractuels
 * @author christine
 *
 */
public abstract class CongeMaladie extends CongeAvecArrete implements InterfaceRequalification {

	public CongeMaladie() {
		super();
	}

	public String temRequalifMaladie() {
		return (String)storedValueForKey("temRequalifMaladie");
	}

	public void setTemRequalifMaladie(String value) {
		takeStoredValueForKey(value, "temRequalifMaladie");
	}

	public String temProlongMaladie() {
		return (String)storedValueForKey("temProlongMaladie");
	}

	public void setTemProlongMaladie(String value) {
		takeStoredValueForKey(value, "temProlongMaladie");
	}
	public NSTimestamp dArretTravail() {
		return (NSTimestamp)storedValueForKey("dArretTravail");
	}

	public void setDArretTravail(NSTimestamp value) {
		takeStoredValueForKey(value, "dArretTravail");
	}
	// Méthodes ajoutées
	public void init() {
		super.init();
		setTemRequalifMaladie(Constantes.FAUX);
		setTemProlongMaladie(Constantes.FAUX);
	}
	   // InterfaceRequalification
    public boolean estRequalifie() {
    	return temRequalifMaladie() != null && temRequalifMaladie().equals(Constantes.VRAI);
    }
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
		resultat = resultat + verifierTemoin(valeursAttributs, "temRequalifMaladie");
		resultat = resultat + verifierTemoin(valeursAttributs, "temProlongMaladie");
		return resultat;
	}
	
}
