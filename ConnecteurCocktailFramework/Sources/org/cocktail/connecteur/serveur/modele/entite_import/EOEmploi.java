//EOEmploi.java
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import java.util.Enumeration;
import java.util.HashMap;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCategorieEmploi;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitreArticle;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EODiscSecondDegre;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBudget;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOParamTypeContrat;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProgramme;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProgrammeTitre;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOReferensEmplois;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteAtos;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEmploiCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 08/03/2011 - pour les emplois postérieurs au 01/01/2002 - on supprime aussi la référence Itarf de la spécialité si elle est fournie
public class EOEmploi extends _EOEmploi implements InterfaceHomonymie, InterfaceInvalidationRecordAvecDependance {
	private static HashMap<Integer, EOEmploi> idSourceCache = new HashMap<Integer, EOEmploi>();

	public EOEmploi() {
		super();
	}

	public void preparerRelations() {
		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);
	}

	public void supprimerRelations() {
		setStructureRelationship(null);
	}

	/** on verifie si la structure est fournie qu'elle existe */
	public EOLogImport verifierRecord() {
		String messageLog = null;
		if (structure() == null && strSource() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure() != null && structure().statut().equals(EOEmploi.STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		}
		
		if (messageLog != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.setStrSource(strSource());
			setStatut(STATUT_ERREUR); 
			
			return log;
		} else {
			return null;
		}
	}

	/**
	 * Verifie si le chapitre ou le programme sont fournis en fonction de la
	 * date de creation.
	 * 
	 */
	public EOLogImport verifierRecordApresOperation() {
		if (EOImportParametres.cacheMemoireNiveauTresHautOuPlus())
			idSourceCache.put(empSource(), this);

		if (DateCtrl.isBefore(dCreationEmploi(), DateCtrl.stringToDate("01/01/2006")) && cChapitre() == null && !cBudget().equals("E")
				&& !cBudget().equals("P")) {
			LogManager.logDetail("Insertion d'un log pour chapitre obligatoire");
			return preparerLog("CHAPITRE_OBLIGATOIRE");
		}
		if (DateCtrl.isAfterEq(dCreationEmploi(), DateCtrl.stringToDate("01/01/2006")) && cProgramme() == null && !cBudget().equals("E")
				&& !cBudget().equals("P")) {
			LogManager.logDetail("Insertion d'un log pour programme obligatoire");
			return preparerLog("PROGRAMME_OBLIGATOIRE");
		}
		return null;
	}

	public ObjetPourSIDestinataire destinataireEquivalent() {
		return EOMangueEmploi.emploiDestinatationPourEmploi(editingContextTravail(), this);
	}

	public String nomRelationPourLog() {
		return EOLogImport.EMPLOI_KEY;
	}

	// InterfaceHomonymie
	/** Si on trouve plusieurs emplois avec le m&ecirc;me numero national */
	public boolean aHomonymeDansSIDestinataire() {
		return EOMangueEmploi.aHomonyme(editingContextTravail(), this);
	}

	/**
	 * Retourne true si meme numero d'emploi national et meme date de
	 * publication
	 */
	public boolean memesDonneesQueRecord(ObjetImport record) {
		EOEmploi emploi = (EOEmploi) record;

		if (emploi.noEmploiNational() != null && !emploi.noEmploiNational().equals(this.noEmploiNational()))
			return false;

		if (emploi.numeroLocal() != null && !emploi.numeroLocal().equals(this.numeroLocal()))
			return false;

		if (emploi.noEmploiRectorat() != null && !emploi.noEmploiRectorat().equals(this.noEmploiRectorat()))
			return false;

		if (emploi.dPublicationEmploi() != null && this.dPublicationEmploi() != null
				&& !DateCtrl.isSameDay(emploi.dPublicationEmploi(), this.dPublicationEmploi()))
			return false;

		return true;
	}

	public String invaliderRecordsDependants() {
		try {
			// Rechercher les occupations dépendant de cet emploi
			invaliderObjets(EOOccupation.rechercherOccupationsImportCourantPourEmploi(editingContext(), this));
			return null;
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		}
	}

	/** Pas d'attribut de comparaison avec des relations */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}

	// Méthodes privées
	private EOLogImport preparerLog(String cleMessage) {
		EOLogImport log = new EOLogImport();
		log.initAvecEditingContext(editingContextTravail(), this.entityName(), cleMessage);
		log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
		setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une erreur
		return log;

	}

	// Méthodes statiques
	/**
	 * Verification des nomenclatures, dates et quotite Supprime la base si ils
	 * n'ont pas de sens a la date de creation ou de suppression
	 */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {

		try {
			String resultat = "";
			// Vérifier que la date de publication n'est pas postérieure à la
			// date de créaation
			// les dates de création et publication sont obligatoires
			NSTimestamp datePublication = (NSTimestamp) valeursAttributs.objectForKey(D_PUBLICATION_EMPLOI_KEY);
			NSTimestamp dateCreation = (NSTimestamp) valeursAttributs.objectForKey(D_CREATION_EMPLOI_KEY);
			NSTimestamp dateSuppression = (NSTimestamp) valeursAttributs.objectForKey(D_SUPPRESSION_EMPLOI_KEY);
			if (dateSuppression != null) {
				if (DateCtrl.isAfter(dateCreation, dateSuppression)) {
					resultat = resultat + "Date_Creation >>" + System.getProperty("line.separator");
				}
				if (DateCtrl.isAfter(datePublication, dateSuppression)) {
					resultat = resultat + "Date_Publication >>" + System.getProperty("line.separator");
				}
			}
			Number quotite = (Number) valeursAttributs.objectForKey(NUM_QUOTITE_CRE_EMP_KEY);
			if (quotite != null) {
				if (quotite.intValue() == 0) {
					resultat = resultat + "Quotite_Nulle >>" + System.getProperty("line.separator");

				} else if (quotite.intValue() > 100.00) {
					resultat += "Quotite_Invalide >>" + System.getProperty("line.separator");
				}
			}
			String valeur = (String) valeursAttributs.objectForKey(C_CATEGORIE_EMPLOI_KEY);
			// Vérifier dans la nomenclature si cette catégorie existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOCategorieEmploi.ENTITY_NAME,
					EOCategorieEmploi.C_CATEGORIE_EMPLOI_KEY, valeur);
			if (record == null) {
				resultat = resultat + "Categorie_Emploi_Invalide >>" + valeur + System.getProperty("line.separator");
			}
			Number nombre = (Number) valeursAttributs.objectForKey(C_CHAPITRE_KEY);
			if (nombre != null) {
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOChapitre.ENTITY_NAME, EOChapitre.C_CHAPITRE_KEY, nombre);
				if (record == null) {
					resultat = resultat + "Chapitre_Invalide >>" + valeur + System.getProperty("line.separator");
				}
			}
			nombre = (Number) valeursAttributs.objectForKey(C_ARTICLE_KEY);
			if (nombre != null) {
				record = Finder
						.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOChapitreArticle.ENTITY_NAME, EOChapitreArticle.C_ARTICLE_KEY, nombre);
				if (record == null) {
					resultat = resultat + "Article_Invalide >>" + valeur + System.getProperty("line.separator");
				}
			}
			valeur = (String) valeursAttributs.objectForKey(C_PROGRAMME_KEY);
			if (valeur != null) {
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOProgramme.ENTITY_NAME, EOProgramme.C_PROGRAMME_KEY, valeur);
				if (record == null) {
					resultat = resultat + "Programme_Invalide >>" + valeur + System.getProperty("line.separator");
				}
			}
			valeur = (String) valeursAttributs.objectForKey(C_BUDGET_KEY);
			if (valeur != null) {
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EONatureBudget.ENTITY_NAME, EONatureBudget.C_BUDGET_KEY, valeur);
				if (record == null) {
					resultat = resultat + "Budget_Invalide >>" + valeur + System.getProperty("line.separator");
				}
			}
			valeur = (String) valeursAttributs.objectForKey(C_TITRE_KEY);
			if (valeur != null) {
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOProgrammeTitre.ENTITY_NAME, EOProgrammeTitre.C_TITRE_KEY, valeur);
				if (record == null) {
					resultat = resultat + "Programme_Titre_Invalide >>" + valeur + System.getProperty("line.separator");
				}
			}
			valeur = (String) valeursAttributs.objectForKey(C_RNE_KEY);
			if (valeur != null) {
				// Vérifier le rne
				EORne rneRecord = EORne.getFromCode(editingContext, valeur);
				if (rneRecord == null) {
					resultat += "Rne_Inconnu >>" + valeur + System.getProperty("line.separator");
				}
			}
			int nbSpecialite = 0;
			valeur = (String) valeursAttributs.objectForKey(CODEEMPLOI_KEY);
			if (valeur != null) {
				// Vérifier dans la nomenclature si ce bap corps existe
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOReferensEmplois.ENTITY_NAME, EOReferensEmplois.CODEEMPLOI_KEY,
						valeur);
				if (record == null) {
					resultat = resultat + "Referens_Emploi_Inconnu >>" + valeur + System.getProperty("line.separator");
				}
				nbSpecialite++;
			}
			valeur = (String) valeursAttributs.objectForKey(C_BAP_KEY);
			if (valeur != null) {
				// Vérifier si la nomenclature est correcte par rapport aux
				// dates
				if (DateCtrl.isAfterEq(dateCreation, DateCtrl.stringToDate("01/01/2002"))) {
					valeursAttributs.removeObjectForKey(C_BAP_KEY);
					// 08/03/2011 - pour les emplois postérieurs au 01/01/2002 -
					// on supprime aussi la référence Itarf de la spécialité si
					// elle est fournie
					String specialiteItarf = (String) valeursAttributs.objectForKey(C_SPECIALITE_ITARF_KEY);
					if (specialiteItarf != null) {
						valeursAttributs.removeObjectForKey(C_SPECIALITE_ITARF_KEY);
					}
				} else {
					if (nbSpecialite == 0) { // On peut avoir à la fois bap et
												// referens emploi
						nbSpecialite++;
					}
					// Vérifier dans la nomenclature si cette Bap existe
					EOBap bap = EOBap.getFromCode(editingContext, valeur);
					if (bap == null) {
						resultat = resultat + "Bap_Inconnu >>" + valeur + System.getProperty("line.separator");
					}
				}
			}
			valeur = (String) valeursAttributs.objectForKey(C_SPECIALITE_ITARF_KEY);
			if (valeur != null) {
				nbSpecialite++;
				// Vérifier dans la nomenclature si cette spécialité Itarf
				// existe
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOSpecialiteItarf.ENTITY_NAME, C_SPECIALITE_ITARF_KEY, valeur);
				if (record == null) {
					resultat = resultat + "Specitalite_Itarf_Inconnue >>" + valeur + System.getProperty("line.separator");
				}
			}

			valeur = (String) valeursAttributs.objectForKey(C_DISC_SECOND_DEGRE_KEY);
			if (valeur != null) {
				nbSpecialite++;
				// Vérifier dans la nomenclature si ce disc existe
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EODiscSecondDegre.ENTITY_NAME,
						EODiscSecondDegre.C_DISC_SECOND_DEGRE_KEY, valeur);
				if (record == null) {
					resultat = resultat + "Disc2ndDegre_Inconnu >>" + valeur + System.getProperty("line.separator");
				}
			}
			valeur = (String) valeursAttributs.objectForKey(C_SECTION_CNU_KEY);
			if (valeur != null) {
				nbSpecialite++;
				// Vérifier dans la nomenclature si cette section cnu existe
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOCnu.ENTITY_NAME, EOCnu.C_SECTION_CNU_KEY, valeur);
				if (record == null) {
					resultat = resultat + "SectionCnu_Inconnue >>" + valeur + System.getProperty("line.separator");
				}
				String sousSection = (String) valeursAttributs.objectForKey(C_SOUS_SECTION_CNU_KEY);
				if (sousSection != null) {
					// Vérifier si la sous-section existe
					boolean found = false;
					NSArray sectionsCnu = Finder.rechercherAvecAttributEtValeurEgale(editingContext, EOCnu.ENTITY_NAME, EOCnu.C_SECTION_CNU_KEY, valeur);
					for (Enumeration<EOCnu> e = sectionsCnu.objectEnumerator(); e.hasMoreElements();) {
						EOCnu cnu = e.nextElement();
						if (cnu.cSousSectionCnu() != null && cnu.cSousSectionCnu().equals(sousSection)) {
							found = true;
							break;
						}
					}
					if (!found) {
						resultat = resultat + "SousSectionCnu_Inconnue >>" + sousSection + System.getProperty("line.separator");
					}
				}
			}
			valeur = (String) valeursAttributs.objectForKey(C_SPECIALITE_ATOS_KEY);
			if (valeur != null) {
				nbSpecialite++;
				// Vérifier dans la nomenclature si cette spécialité Atos existe
				record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOSpecialiteAtos.ENTITY_NAME, EOSpecialiteAtos.C_SPECIALITE_ATOS_KEY,
						valeur);
				if (record == null) {
					resultat = resultat + "Specitalite_Atos_Inconnue >>" + valeur + System.getProperty("line.separator");
				}
			}
			if (nbSpecialite > 2) {
				resultat = resultat + "Specialites_Trop_Nombreuses >>" + System.getProperty("line.separator");
			}
			resultat = resultat + verifierTemoin(valeursAttributs, TEM_ENSEIGNANT_KEY);
			resultat = resultat + verifierTemoin(valeursAttributs, TEMOIN_MIS_ARBITRAGE_KEY);
			resultat = resultat + verifierTemoin(valeursAttributs, TEMOIN_MIS_CONCOURS_KEY);

			return resultat;
		} catch (Exception e) {
			e.printStackTrace();
			return "ERREUR DE TRAITEMENT DES EMPLOIS !!!";
		}

	}

	/**
	 * retourne l'emploi d'import associe a l'id passee en param&egrave;tre. On
	 * commence par chercher dans l'import courant pour &ecirc;tre s&ucirc;r de
	 * retourner le dernier emploi, sinon on recherche dans les correspondances
	 */
	public static EOEmploi emploiPourSourceId(EOEditingContext editingContext, Integer empSource) {
		if (empSource == null)
			return null;

		EOEmploi resultat;
		if (EOImportParametres.cacheMemoireNiveauTresHautOuPlus()) {
			resultat = idSourceCache.get(empSource);
			if (resultat != null)
				return resultat;
		}

		if (editingContext == null)
			return null;

		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, EMP_SOURCE_KEY, empSource);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOEmploi)premierObjetValide(resultats);

		if (EOImportParametres.cacheMemoireNiveauTresHautOuPlus() && resultat != null)
			idSourceCache.put(empSource, resultat);

		return resultat;
	}

	public static void resetCache() {
		idSourceCache.clear();
	}
}
