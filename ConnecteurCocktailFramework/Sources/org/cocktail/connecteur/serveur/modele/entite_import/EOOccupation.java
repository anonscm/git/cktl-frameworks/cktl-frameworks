//EOOccupation.java
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.PeriodeAvecQuotite;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEmploiCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueOccupation;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

//24/06/2010 - changement de la superclasse
//08/02/2011 - correction d'un bug dans l'évaluation de la quotité d'occupation, il ne faut pas prendre en compte dans le
//calcul de la quotité des occupations en update suite à un changement de dates
public class EOOccupation extends _EOOccupation {
	public void preparerRelations() {
		super.preparerRelations();
		
		EOEmploi emploi = EOEmploi.emploiPourSourceId(editingContext(), empSource());
		setEmploiRelationship(emploi);
		
		EOContrat contrat = EOContrat.contratPourSourceId(editingContext(), idSource(), ctrSource());
		setContratRelationship(contrat);
		
		EOCarriere carriere = EOCarriere.carrierePourSourceId(editingContext(), idSource(), carSource());
		setToCarriereRelationship(carriere);
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setEmploiRelationship(null);
		setContratRelationship(null);
		setToCarriereRelationship(null);
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;
		if (emploi() == null && empSource() != null) {
			messageLog = "EMPLOI_NON_IMPORTE";
		} else if (emploi() != null && emploi().statut().equals(EOEmploi.STATUT_ERREUR)) {
			messageLog = "EMPLOI_INVALIDE";
		} else if (contrat() == null && ctrSource() != null) {
			messageLog = "CONTRAT_NON_IMPORTE";
		} else if (contrat() != null && contrat().statut().equals(EOContrat.STATUT_ERREUR)) {
			messageLog = "CONTRAT_IMPORT_INVALIDE";
		} else if (toCarriere() == null && carSource() != null) {
			messageLog = "CARRIERE_NON_IMPORTEE";
		} else if (toCarriere() != null && toCarriere().statut().equals(EOCarriere.STATUT_ERREUR)) {
			messageLog = "CARRIERE_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			log.setEmploiRelationship(emploi());
			setStatut(STATUT_ERREUR);
			return log;
		} else {
			return null;
		}
	}

	/**
	 * Verifie que :<BR>
	 * - les dates de l'occupation sont bien a l'interieur de celles de
	 * l'emploi.<BR>
	 * - l'emploi n'est pas deja occupe par cet individu pendant la periode<BR>
	 * - la quotite totale des occupations de cet individu pendant la periode
	 * est inferieure ou egale a 100
	 * 
	 */
	public EOLogImport verifierRecordApresOperation() {
		// Vérifier les dates par rapport a celles de l'emploi
		// Pour déterminer la quotité totale, il faut ajouter :
		// - la quotité de toutes les occupations de la période valides
		// importées en mode insertion
		// - la quotité de toutes les occupations de la période valides en mode
		// update ou correspondance
		// - la quotité de toutes les autres occupations de la période du SI
		// Destinataire qui ne sont pas dans l'import courant
		// Vérifier aussi les emplois
		// Rechercher toutes les occupations qui viennent d'être importées
		String messageLog = null;
		if (emploi() != null) {
			if (emploi().dSuppressionEmploi() != null && DateCtrl.isAfter(dDebOccupation(), emploi().dSuppressionEmploi())) {
				messageLog = "DATE_DEBUT_OCCUPATION_FIN_EMPLOI";
			} else if ((dFinOccupation() != null && emploi().dSuppressionEmploi() != null && DateCtrl.isAfter(dFinOccupation(), emploi().dSuppressionEmploi()))
					|| (dFinOccupation() == null && emploi().dSuppressionEmploi() != null)) {
				messageLog = "DATE_FIN_OCCUPATION_FIN_EMPLOI";
			}
		}
		if (messageLog == null) {
			NSArray occupationsImport = rechercherOccupationsValidesImportCourantPourIndividuEtPeriode();
			NSMutableArray occupationsDestin = new NSMutableArray();
			LogManager.logDetail("occupations import " + occupationsImport.count());
			NSMutableArray periodesAvecQuotite = new NSMutableArray();
			boolean aMemeEmploi = false;
			// On prend en compte toutes les périodes de l'import + toutes celles du SI Destinataire qui ne correspondent pas à des données de l'import
			periodesAvecQuotite.addObject(new PeriodeAvecQuotite(this.dDebOccupation(), this.dFinOccupation(), this.numMoyenUtilise()));
			ajouterOccupationDestinPourOccupation(occupationsDestin, this);

			java.util.Enumeration e = occupationsImport.objectEnumerator();
			while (e.hasMoreElements()) {
				EOOccupation occupation = (EOOccupation) e.nextElement();
				if (occupation != this) {
					if (occupation.empSource().equals(empSource())) {
						aMemeEmploi = true;
					}
					// Prendre en compte les emplois
					ajouterOccupationDestinPourOccupation(occupationsDestin, occupation);
					periodesAvecQuotite
							.addObject(new PeriodeAvecQuotite(occupation.dDebOccupation(), occupation.dFinOccupation(), occupation.numMoyenUtilise()));
				}
			}
			LogManager.logDetail("Occupations destin deja vues " + occupationsDestin.count());
			// Rechercher toutes les Occupations déjà dans le SI Destinataire
			NSArray occupations = EOMangueOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContextTravail(), this);
			if (occupations != null) { // Null si individu occupations pas
										// encore créé
				LogManager.logDetail("nb occupations SI dest" + occupations.count());
				// 08/02/2011
				NSArray occupationsImportValides = rechercherOccupationsValidesImportCourantPourIndividu();
				// On se base sur les occSource pour comparer les occupations
				occupationsImportValides = (NSArray) occupationsImportValides.valueForKey("occSource");
				e = occupations.objectEnumerator();
				while (e.hasMoreElements()) {
					EOMangueOccupation occupationMangue = (EOMangueOccupation) e.nextElement();
					LogManager.logDetail("occupation courante " + occupationMangue.noOccupation());
					if (occupationsDestin.containsObject(occupationMangue) == false) {
						// 08/02/2011 - Rechercher l'occupation d'import
						// associée à cette occupation
						EOOccupation occupationImport = (EOOccupation) ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext(),
								occupationMangue, "Occupation");
						if (occupationImport != null && occupationsImportValides.containsObject(occupationImport.occSource()) == false) {
							periodesAvecQuotite.addObject(new PeriodeAvecQuotite(occupationMangue.dDebOccupation(), occupationMangue.dFinOccupation(),
									occupationMangue.numMoyenUtilise()));
							EOOccupation occupation = (EOOccupation) ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContextTravail(),
									occupationMangue, "Occupation");
							if (occupation != null && occupation.emploi().empSource().equals(emploi().empSource())) {
								aMemeEmploi = true;
							}
						}
					}
				}
			}
			// Vérifier la quotité totale
			Number quotiteTotale = PeriodeAvecQuotite.calculerQuotiteTotale(periodesAvecQuotite);
			LogManager.logDetail("quotite finale " + quotiteTotale);
			if (quotiteTotale != null && quotiteTotale.doubleValue() > 100.00) {
				LogManager.logDetail("Insertion d'un log pour la quotite d'occupation");
				messageLog = "QUOTITE_OCCUPATION_INVALIDE";
			}
			if (aMemeEmploi) {
				LogManager.logDetail("Insertion d'un log pour meme emploi");
				messageLog = "EMPLOI_DEJA_OCCUPE";
			}
		}
		if (messageLog != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			if (emploi() != null) {
				log.setEmploiRelationship(emploi());
			}
			log.setIdSource(individu().idSource());
			if (individu() != null) {
				log.setIndividuRelationship(individu());
			}
			log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
			setStatut(STATUT_ERREUR);
			return log;
		} else {
			return null;
		}
	}

	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null && emploi() != null) {
			if (individu().operation().equals(ObjetImport.OPERATION_INSERTION)) {
				return null;
			}
			if (emploi().operation().equals(ObjetImport.OPERATION_INSERTION)) {
				return null;
			} else {
				return EOMangueOccupation.occupationDestinationPourOccupation(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	public String nomRelationPourLog() {
		return EOLogImport.OCCUPATION_KEY;
	}

	/** Retourne la relation sur l'emploi ou l'individu */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		if (nomAttribut.equals(EMP_SOURCE_KEY)) {
			return EMPLOI_KEY;
		} else {
			return super.nomRelationPourAttributComparaison(nomAttribut);
		}
	}

	// Méthodes privées
	/*
	 * retourne les occupations valides qui sont importées dans l'import courant
	 * pour l'individu de l'occupation et la même période.
	 */
	private NSArray rechercherOccupationsValidesImportCourantPourIndividuEtPeriode() {
		return rechercherOccupationsValidesImportCourantPourIndividu(true);
	}

	private NSArray rechercherOccupationsValidesImportCourantPourIndividu() {
		return rechercherOccupationsValidesImportCourantPourIndividu(false);
	}

	private NSArray rechercherOccupationsValidesImportCourantPourIndividu(boolean prendreEnComptePeriode) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E'", new NSArray(individu()
				.idSource()));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		if (prendreEnComptePeriode) {
			qualifiers.addObject(Finder.qualifierPourPeriode("dDebOccupation", dDebOccupation(), "dFinOccupation", dFinOccupation()));
		}
		EOFetchSpecification myFetch = new EOFetchSpecification("Occupation", new EOAndQualifier(qualifiers), null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContextTravail().objectsWithFetchSpecification(myFetch);
	}

	private void ajouterOccupationDestinPourOccupation(NSMutableArray occupationsDestin, EOOccupation occupation) {
		if (operation().equals(OPERATION_INSERTION) == false) {
			EOMangueOccupation occupationDestin = null;
			if (operation().equals(OPERATION_UPDATE)) {
				occupationDestin = (EOMangueOccupation) ObjetCorresp.rechercherDestinatairePrecedentPourRecordImport(editingContextTravail(), occupation);
			} else { // Correspondance
				occupationDestin = EOMangueOccupation.occupationDestinationPourOccupation(editingContextTravail(), occupation);
			}
			if (occupationDestin != null) {
				occupationsDestin.addObject(occupationDestin);
			}
		}
	}

	// Méthodes statiques
	public static NSArray rechercherOccupationsImportCourantPourEmploi(EOEditingContext editingContext, EOEmploi emploi) {
		NSMutableArray args = new NSMutableArray(emploi.empSource());
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("emploi.empSource = %@ AND temImport = %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification("Occupation", qualifier, null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/** Verification de la quotite (> 0 et <= 100%), des dates (debut avant fin) */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey("dFinOccupation");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp) valeursAttributs.objectForKey("dDebOccupation");
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		double quotite = ((Number) valeursAttributs.objectForKey("numMoyenUtilise")).doubleValue();
		if (quotite == 0.00) {
			resultat += "Quotite_Nulle >>" + System.getProperty("line.separator");
		} else if (quotite > 100.00) {
			resultat += "Quotite_Invalide >>" + System.getProperty("line.separator");
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "temTitulaire");

		return resultat;
	}

}
