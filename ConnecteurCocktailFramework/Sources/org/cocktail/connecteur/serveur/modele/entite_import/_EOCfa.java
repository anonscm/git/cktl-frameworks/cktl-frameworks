// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCfa.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCfa extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Cfa";

	// Attributes
	public static final String CFA_SOURCE_KEY = "cfaSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_FIN_EXECUTION_KEY = "dateFinExecution";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCfa.class);

  public EOCfa localInstanceIn(EOEditingContext editingContext) {
    EOCfa localInstance = (EOCfa)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cfaSource() {
    return (Integer) storedValueForKey("cfaSource");
  }

  public void setCfaSource(Integer value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating cfaSource from " + cfaSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cfaSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dateFinExecution() {
    return (NSTimestamp) storedValueForKey("dateFinExecution");
  }

  public void setDateFinExecution(NSTimestamp value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating dateFinExecution from " + dateFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFinExecution");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
    	_EOCfa.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCfa.LOG.isDebugEnabled()) {
      _EOCfa.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCfa createCfa(EOEditingContext editingContext, Integer cfaSource
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temValide
) {
    EOCfa eo = (EOCfa) EOUtilities.createAndInsertInstance(editingContext, _EOCfa.ENTITY_NAME);    
		eo.setCfaSource(cfaSource);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCfa> fetchAllCfas(EOEditingContext editingContext) {
    return _EOCfa.fetchAllCfas(editingContext, null);
  }

  public static NSArray<EOCfa> fetchAllCfas(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCfa.fetchCfas(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCfa> fetchCfas(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCfa.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCfa> eoObjects = (NSArray<EOCfa>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCfa fetchCfa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCfa.fetchCfa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCfa fetchCfa(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCfa> eoObjects = _EOCfa.fetchCfas(editingContext, qualifier, null);
    EOCfa eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCfa)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Cfa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCfa fetchRequiredCfa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCfa.fetchRequiredCfa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCfa fetchRequiredCfa(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCfa eoObject = _EOCfa.fetchCfa(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Cfa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCfa localInstanceIn(EOEditingContext editingContext, EOCfa eo) {
    EOCfa localInstance = (eo == null) ? null : (EOCfa)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
