package org.cocktail.connecteur.serveur.modele.entite_import;

import java.util.Date;
import java.util.List;

import org.cocktail.connecteur.common.metier.controles.IControleService;
import org.cocktail.connecteur.common.metier.controles.IManager;
import org.cocktail.connecteur.common.metier.controles.ResultatControle;
import org.cocktail.connecteur.common.metier.controles.periode.Periode;
import org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation.CarriereSpecialisationTypeControle;
import org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation.ICarriereSpecialisation;
import org.cocktail.connecteur.common.metier.controles.specialisations.carrierespecialisation.CarriereSpecialisationImpl;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IBap;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ICnu;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IDisciplineSecondDegre;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.IReferensEmploi;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteAtos;
import org.cocktail.connecteur.common.metier.controles.specialisations.nomenclatures.ISpecialiteItarf;
import org.cocktail.connecteur.common.metier.repositories.MainManager;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriereSpecialisations;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOCarriereSpecialisations extends _EOCarriereSpecialisations implements ICarriereSpecialisation {

	@Override
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport logImport = verifierDatesParRapportCarriere(this.dDebut(),this.dFin());
		return logImport;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (toCarriere() != null) {
			if (estNouvelleCarriere()) {
				return null;
			} else {
				return EOMangueCarriereSpecialisations.elementDestinationPourSpecialisation(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CARRIERE_SPECIALISATIONS_KEY;
	}

	// Méthodes statiques
	/** Verifie les nomenclatures ainsi que les dates */
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String message = "";
		
		if (valeursAttributs.objectForKey(C_BAP_KEY)!=null && valeursAttributs.objectForKey(CODEEMPLOI_KEY)!=null) {
			valeursAttributs.removeObjectForKey(C_BAP_KEY);
		}
		MainManager mainManager=MainManager.instance();
		IManager<ICarriereSpecialisation> manager=mainManager.getManagerCarriereSpecialisation();
		IControleService<ICarriereSpecialisation> controlService=manager.getControleService();
		
		// On crée pour le contrôler. Cet objet n'est pas celui qui sera stocké dans la base de donnée
		ICarriereSpecialisation carriereSpecialisation=new CarriereSpecialisationImpl();
		carriereSpecialisation.setCBap((String)valeursAttributs.objectForKey(C_BAP_KEY));
		carriereSpecialisation.setCDiscSecondDegre((String)valeursAttributs.objectForKey(C_DISC_SECOND_DEGRE_KEY));
		carriereSpecialisation.setCodeemploi((String)valeursAttributs.objectForKey(CODEEMPLOI_KEY));
		carriereSpecialisation.setCSectionCnu((String)valeursAttributs.objectForKey(C_SECTION_CNU_KEY));
		carriereSpecialisation.setCSousSectionCnu((String)valeursAttributs.objectForKey(C_SOUS_SECTION_CNU_KEY));
		carriereSpecialisation.setCSpecialiteAtos((String)valeursAttributs.objectForKey(C_SPECIALITE_ATOS_KEY));
		carriereSpecialisation.setCSpecialiteItarf((String)valeursAttributs.objectForKey(C_SPECIALITE_ITARF_KEY));
		carriereSpecialisation.setDDebut((Date)valeursAttributs.objectForKey("dDebut"));
		carriereSpecialisation.setDFin((Date)valeursAttributs.objectForKey("dFin"));;
		
		List<ResultatControle> resultats=controlService.appliqueControlesDeType(carriereSpecialisation,CarriereSpecialisationTypeControle.VALIDATION_ATTRIBUT,true);
		for (ResultatControle resultat : resultats) {
			if (!resultat.valide()) {
				message+=resultat.getMessage()+ System.getProperty("line.separator");
			}
		}
		
		return message;
	}

	public IBap getBap() {
		return MainManager.instance().getBapRepository().fetchCBap(cBap());
	}

	public IDisciplineSecondDegre getDisciplineSecondDegre() {
		return MainManager.instance().getDiscSecondDegreRepository().fetchCDiscSecondDegre(cDiscSecondDegre());
	}

	public ICnu getCnu() {
		return MainManager.instance().getCnuRepository().fetchSectionCnuSousSectionCnu(cSectionCnu(),cSousSectionCnu());
	}

	public IReferensEmploi getEmploi() {
		return MainManager.instance().getReferensEmploiRepository().fetchCodeEmploi(codeemploi());
	}

	public ISpecialiteItarf getSpecialiteItarf() {
		return MainManager.instance().getSpecialiteItarfRepository().fetchCSpecialiteItarf(cBap(),cSpecialiteItarf());
	}

	public ISpecialiteAtos getSpecialiteAtos() {
		return MainManager.instance().getSpecialiteAtosRepository().fetchCSpecialiteAtos(cSpecialiteAtos());
	}

	public void setDDebut(Date debut) {
		setDDebut(new NSTimestamp(debut));		
	}

	public void setDFin(Date fin) {
		setDFin(new NSTimestamp(fin));		
	}
	
	public Periode periodeEffet() {
		return new Periode(dDebut(), dFin());
	}
}
