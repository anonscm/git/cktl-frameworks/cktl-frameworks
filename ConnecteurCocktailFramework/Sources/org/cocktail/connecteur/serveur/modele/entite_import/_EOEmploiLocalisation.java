// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOEmploiLocalisation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOEmploiLocalisation extends ObjetImport {
	public static final String ENTITY_NAME = "EmploiLocalisation";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEBUT_KEY = "dDebut";
	public static final String D_FIN_KEY = "dFin";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ELOC_SOURCE_KEY = "elocSource";
	public static final String EMP_SOURCE_KEY = "empSource";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_KEY = "quotite";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String EMPLOI_KEY = "emploi";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOEmploiLocalisation.class);

  public EOEmploiLocalisation localInstanceIn(EOEditingContext editingContext) {
    EOEmploiLocalisation localInstance = (EOEmploiLocalisation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebut() {
    return (NSTimestamp) storedValueForKey("dDebut");
  }

  public void setDDebut(NSTimestamp value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating dDebut from " + dDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebut");
  }

  public NSTimestamp dFin() {
    return (NSTimestamp) storedValueForKey("dFin");
  }

  public void setDFin(NSTimestamp value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating dFin from " + dFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dFin");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer elocSource() {
    return (Integer) storedValueForKey("elocSource");
  }

  public void setElocSource(Integer value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating elocSource from " + elocSource() + " to " + value);
    }
    takeStoredValueForKey(value, "elocSource");
  }

  public Integer empSource() {
    return (Integer) storedValueForKey("empSource");
  }

  public void setEmpSource(Integer value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating empSource from " + empSource() + " to " + value);
    }
    takeStoredValueForKey(value, "empSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer quotite() {
    return (Integer) storedValueForKey("quotite");
  }

  public void setQuotite(Integer value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi emploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi)storedValueForKey("emploi");
  }

  public void setEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
      _EOEmploiLocalisation.LOG.debug("updating emploi from " + emploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi oldValue = emploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOEmploiLocalisation.LOG.isDebugEnabled()) {
      _EOEmploiLocalisation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOEmploiLocalisation createEmploiLocalisation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebut
, NSTimestamp dModification
, String statut
, String strSource
, String temImport
) {
    EOEmploiLocalisation eo = (EOEmploiLocalisation) EOUtilities.createAndInsertInstance(editingContext, _EOEmploiLocalisation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebut(dDebut);
		eo.setDModification(dModification);
		eo.setStatut(statut);
		eo.setStrSource(strSource);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOEmploiLocalisation> fetchAllEmploiLocalisations(EOEditingContext editingContext) {
    return _EOEmploiLocalisation.fetchAllEmploiLocalisations(editingContext, null);
  }

  public static NSArray<EOEmploiLocalisation> fetchAllEmploiLocalisations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEmploiLocalisation.fetchEmploiLocalisations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEmploiLocalisation> fetchEmploiLocalisations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEmploiLocalisation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEmploiLocalisation> eoObjects = (NSArray<EOEmploiLocalisation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEmploiLocalisation fetchEmploiLocalisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploiLocalisation.fetchEmploiLocalisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploiLocalisation fetchEmploiLocalisation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEmploiLocalisation> eoObjects = _EOEmploiLocalisation.fetchEmploiLocalisations(editingContext, qualifier, null);
    EOEmploiLocalisation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEmploiLocalisation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EmploiLocalisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploiLocalisation fetchRequiredEmploiLocalisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploiLocalisation.fetchRequiredEmploiLocalisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploiLocalisation fetchRequiredEmploiLocalisation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEmploiLocalisation eoObject = _EOEmploiLocalisation.fetchEmploiLocalisation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EmploiLocalisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploiLocalisation localInstanceIn(EOEditingContext editingContext, EOEmploiLocalisation eo) {
    EOEmploiLocalisation localInstance = (eo == null) ? null : (EOEmploiLocalisation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
