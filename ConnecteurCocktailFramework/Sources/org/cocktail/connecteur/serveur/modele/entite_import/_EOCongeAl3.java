// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAl3.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAl3 extends CongeAvecArrete {
	public static final String ENTITY_NAME = "CongeAl3";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CAL3_SOURCE_KEY = "cal3Source";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_KEY = "dComMed";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_EN_CAUSE_KEY = "temEnCause";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PROLONG_KEY = "temProlong";
	public static final String TEM_REQUALIF_KEY = "temRequalif";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeAl3.class);

  public EOCongeAl3 localInstanceIn(EOEditingContext editingContext) {
    EOCongeAl3 localInstance = (EOCongeAl3)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer cal3Source() {
    return (Integer) storedValueForKey("cal3Source");
  }

  public void setCal3Source(Integer value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating cal3Source from " + cal3Source() + " to " + value);
    }
    takeStoredValueForKey(value, "cal3Source");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMed() {
    return (NSTimestamp) storedValueForKey("dComMed");
  }

  public void setDComMed(NSTimestamp value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating dComMed from " + dComMed() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMed");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temEnCause() {
    return (String) storedValueForKey("temEnCause");
  }

  public void setTemEnCause(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating temEnCause from " + temEnCause() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnCause");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temProlong() {
    return (String) storedValueForKey("temProlong");
  }

  public void setTemProlong(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating temProlong from " + temProlong() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlong");
  }

  public String temRequalif() {
    return (String) storedValueForKey("temRequalif");
  }

  public void setTemRequalif(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating temRequalif from " + temRequalif() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalif");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
    	_EOCongeAl3.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
      _EOCongeAl3.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeAl3.LOG.isDebugEnabled()) {
      _EOCongeAl3.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeAl3 createCongeAl3(EOEditingContext editingContext, Integer cal3Source
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String operation
, String statut
, String temConfirme
, String temEnCause
, String temGestEtab
, String temImport
, String temProlong
, String temRequalif
, String temValide
) {
    EOCongeAl3 eo = (EOCongeAl3) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAl3.ENTITY_NAME);    
		eo.setCal3Source(cal3Source);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setOperation(operation);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemEnCause(temEnCause);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemProlong(temProlong);
		eo.setTemRequalif(temRequalif);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCongeAl3> fetchAllCongeAl3s(EOEditingContext editingContext) {
    return _EOCongeAl3.fetchAllCongeAl3s(editingContext, null);
  }

  public static NSArray<EOCongeAl3> fetchAllCongeAl3s(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAl3.fetchCongeAl3s(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAl3> fetchCongeAl3s(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAl3.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAl3> eoObjects = (NSArray<EOCongeAl3>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAl3 fetchCongeAl3(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl3.fetchCongeAl3(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl3 fetchCongeAl3(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAl3> eoObjects = _EOCongeAl3.fetchCongeAl3s(editingContext, qualifier, null);
    EOCongeAl3 eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAl3)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAl3 that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl3 fetchRequiredCongeAl3(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl3.fetchRequiredCongeAl3(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl3 fetchRequiredCongeAl3(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAl3 eoObject = _EOCongeAl3.fetchCongeAl3(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAl3 that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl3 localInstanceIn(EOEditingContext editingContext, EOCongeAl3 eo) {
    EOCongeAl3 localInstance = (eo == null) ? null : (EOCongeAl3)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
