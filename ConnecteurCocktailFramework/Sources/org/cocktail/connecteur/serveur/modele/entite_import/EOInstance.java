package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeInstance;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOInstance extends _EOInstance {
	@Override
	public void preparerRelations() {
	}

	@Override
	public void supprimerRelations() {
	}

	@Override
	public EOLogImport verifierRecord() {
		return null;
	}

	@Override
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.INSTANCE_KEY;
	}

	@Override
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		String typeInstance = (String) valeursAttributs.objectForKey(C_TYPE_INSTANCE_KEY);
		if (typeInstance != null) {
			EOTypeInstance fonction = EOTypeInstance.getFromCode(editingContext, typeInstance);
			if (fonction == null) {
				resultat = resultat + "TypeInstance_Inconnu >>" + typeInstance + System.getProperty("line.separator");
			}
		}

		return resultat;
	}
}
