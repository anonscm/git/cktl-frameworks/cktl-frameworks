//EOReliquatsAnciennete.java
//Created on Fri Feb 21 09:47:09  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOElementCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReliquatsAnciennete;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOReliquatsAnciennete extends _EOReliquatsAnciennete {
	private String motif;

	public EOReliquatsAnciennete() {
		super();
	}

	public String motif() {
		if (motifReduction() != null) {
			return (String) motifReduction().valueForKey("moreLibelle");
		} else {
			return motif;
		}
	}

	public String nomRelationPourLog() {
		return EOLogImport.RELIQUATS_ANCIENNETE_KEY;
	}

	public void setMotif(String value) {
		if (value == null) {
			if (motifReduction() != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(motifReduction(), "motifReduction");
			}
		} else {
			this.motif = value;
			if (editingContext() != null) {
				EOGenericRecord motif = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "MotifReduction", "moreLibelle", value);
				addObjectToBothSidesOfRelationshipWithKey(motif, "motifReduction");
			}
		}
	}

	// méthodes ajoutées
	/**
	 * Prepare la relation sur le motif de reduction. A appeler apr&egrave;s
	 * avoir insere un objet dans l'editing context
	 */
	public void preparerRelations() {
		super.preparerRelations();
		LogManager.logDetail("Preparation des relations");
		if (editingContext() != null && motif != null) {
			EOMotifReduction motifReduction = EOMotifReduction.fetchMotifReduction(editingContext(), EOMotifReduction.MORE_LIBELLE_KEY, motif);
			setMotifReductionRelationship(motifReduction);

		}
		if (editingContext() != null && carSource() != null && elSource() != null) {
			EOElementCarriere element = EOElementCarriereCorresp.elementCarrierePourIndividuEtCarriere(editingContext(), idSource(), carSource(), elSource());
			if (element != null) {
				setElementCarriereRelationship(element);
			}
		}
	}

	public void supprimerRelations() {
		LogManager.logDetail("Suppression des relations");
		if (motifReduction() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(motifReduction(), "motifReduction");
			this.motif = null;
		}
		setElementCarriereRelationship(null);
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		Integer temp = (Integer) valeursAttributs.objectForKey(ANC_NB_ANNEES_KEY);
		if (temp != null) {
			String anc = temp.toString();
			if (anc.length() > 2) {
				resultat = resultat + "Anc_Nb_Annees >>" + anc + System.getProperty("line.separator");
			}
		}
		temp = (Integer) valeursAttributs.objectForKey(ANC_NB_MOIS_KEY);
		if (temp != null) {
			String anc = temp.toString();
			if (anc.length() > 2) {
				resultat = resultat + "Anc_Nb_Mois >>" + anc + System.getProperty("line.separator");
			}
		}
		temp = (Integer) valeursAttributs.objectForKey(ANC_NB_JOURS_KEY);
		if (temp != null) {
			String anc = temp.toString();
			if (anc.length() > 2) {
				resultat = resultat + "Anc_Nb_Jours >>" + anc + System.getProperty("line.separator");
			}
		}
		temp = (Integer) valeursAttributs.objectForKey(ANC_ANNEE_KEY);
		if (temp != null) {
			String anc = temp.toString();
			if (anc.length() > 4) {
				resultat = resultat + "Anc_Annee >>" + anc + System.getProperty("line.separator");
			}
		}

		String valeur = (String) valeursAttributs.objectForKey("motif");
		// Vérifier dans la nomenclature si ce motif existe
		if (valeur != null) {
			EOMotifReduction motifReduction = EOMotifReduction.fetchMotifReduction(editingContext, EOMotifReduction.MORE_LIBELLE_KEY, valeur);
			if (motifReduction == null) {
				resultat = resultat + "Motif_Reduction_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "temUtilise");
		return resultat;
	}

	public EOLogImport verifierRecord() {
		EOLogImport logImport = super.verifierRecord();
		if (logImport != null)
			return logImport;

		String messageLog = null;
		if (carSource() != null && individu() != null) {
			EOCarriere carriere = EOCarriere.carrierePourSourceId(editingContextTravail(), idSource(), carSource());
			if (carriere == null)
				messageLog = "CARRIERE_NON_IMPORTEE";
			else if (carriere.temImport().equals(ObjetImport.A_TRANSFERER) && carriere.statut().equals(EOIndividu.STATUT_ERREUR))
				messageLog = "CARRIERE_INVALIDE";
			else if (elSource() != null) {
				// La carrière est OK, on vérifie l'élément de carrière
				EOElementCarriere element = EOElementCarriereCorresp.elementCarrierePourIndividuEtCarriere(editingContext(), idSource(), carSource(),
						elSource());
				if (element == null)
					messageLog = "ELEMENT_CARRIERE_NON_IMPORTE";
				else if (element.temImport().equals(ObjetImport.A_TRANSFERER) && element.statut().equals(EOIndividu.STATUT_ERREUR))
					messageLog = "ELEMENT_CARRIERE_IMPORT_INVALIDE";
			}

			if (messageLog != null) {
				logImport = new EOLogImport();
				logImport.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
				logImport.setEimpSource(eimpSource());
				logImport.ajouterRelation(this);
				logImport.setElementCarriereRelationship(elementCarriere());
				setStatut(STATUT_ERREUR);
			}
		}
		return logImport;
	}

	/**
	 * Verification des durees (ancNbAnnees, ancNbMois, ancNbJours) et de
	 * ancAnnee (sur 4 caract&egrave;res)
	 */
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log;
		log = super.verifierRecordApresOperation();
		if (log != null)
			return log;
		String message = verifierAnciennete();
		if (message != null) {
			LogManager.logDetail("Insertion d'un log pour erreur");
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), message);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			log.setElementCarriereRelationship(elementCarriere());
			setStatut(STATUT_ERREUR);
			return log;
		} else {
			return null;
		}
	}

	// On vérifie la cohérence des valeurs (au cas où elles sont modifiées dans
	// le client). Dans validerAttributs, on vérifie la longueur des valeurs
	private String verifierAnciennete() {
		if (ancNbAnnees() != null) {
			int anc = ancNbAnnees().intValue();
			if (anc < 0 || anc >= 100) {
				return "ANCIENNETE_NB_ANNEES";
			}
		}
		if (ancNbMois() != null) {
			int anc = ancNbMois().intValue();
			if (anc < 0 || anc > 12) {
				return "ANCIENNETE_NB_MOIS";
			}
		}
		if (ancNbJours() != null) {
			int anc = ancNbJours().intValue();
			if (anc < 0 || anc > 30) {
				return "ANCIENNETE_NB_JOURS";
			}
		}
		String temp = ancAnnee().toString();
		if (temp.length() != 4) {
			return "ANCIENNETE_ANNEE";
		}
		return null;
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueReliquatsAnciennete.findForIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		String resultat=null;
		if (nomAttribut.equals(EL_SOURCE_KEY)) {
			resultat= ELEMENT_CARRIERE_KEY;
		} else {
			resultat= super.nomRelationPourAttributComparaison(nomAttribut);
		}
		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return relSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setRelSource(value);
	}

}
