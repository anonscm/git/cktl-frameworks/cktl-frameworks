// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOEmploi.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOEmploi extends ObjetImport {
	public static final String ENTITY_NAME = "Emploi";

	// Attributes
	public static final String C_ARTICLE_KEY = "cArticle";
	public static final String C_BAP_KEY = "cBap";
	public static final String C_BUDGET_KEY = "cBudget";
	public static final String C_CATEGORIE_EMPLOI_KEY = "cCategorieEmploi";
	public static final String C_CHAPITRE_KEY = "cChapitre";
	public static final String C_DISC_SECOND_DEGRE_KEY = "cDiscSecondDegre";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String C_PROGRAMME_KEY = "cProgramme";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String C_SPECIALITE_ITARF_KEY = "cSpecialiteItarf";
	public static final String C_TITRE_KEY = "cTitre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_CREATION_EMPLOI_KEY = "dCreationEmploi";
	public static final String DEN_QUOTITE_CRE_EMP_KEY = "denQuotiteCreEmp";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_PUBLICATION_EMPLOI_KEY = "dPublicationEmploi";
	public static final String D_SUPPRESSION_EMPLOI_KEY = "dSuppressionEmploi";
	public static final String DURABILITE_EMPLOI_KEY = "durabiliteEmploi";
	public static final String EMP_SOURCE_KEY = "empSource";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String NO_EMPLOI_NATIONAL_KEY = "noEmploiNational";
	public static final String NO_EMPLOI_RECTORAT_KEY = "noEmploiRectorat";
	public static final String NUMERO_LOCAL_KEY = "numeroLocal";
	public static final String NUM_QUOTITE_CRE_EMP_KEY = "numQuotiteCreEmp";
	public static final String OPERATION_KEY = "operation";
	public static final String PROFIL_EMPLOI_KEY = "profilEmploi";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEMOIN_MIS_ARBITRAGE_KEY = "temoinMisArbitrage";
	public static final String TEMOIN_MIS_CONCOURS_KEY = "temoinMisConcours";

	// Relationships
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOEmploi.class);

  public EOEmploi localInstanceIn(EOEditingContext editingContext) {
    EOEmploi localInstance = (EOEmploi)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cArticle() {
    return (Integer) storedValueForKey("cArticle");
  }

  public void setCArticle(Integer value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cArticle from " + cArticle() + " to " + value);
    }
    takeStoredValueForKey(value, "cArticle");
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public String cBudget() {
    return (String) storedValueForKey("cBudget");
  }

  public void setCBudget(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cBudget from " + cBudget() + " to " + value);
    }
    takeStoredValueForKey(value, "cBudget");
  }

  public String cCategorieEmploi() {
    return (String) storedValueForKey("cCategorieEmploi");
  }

  public void setCCategorieEmploi(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cCategorieEmploi from " + cCategorieEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorieEmploi");
  }

  public Integer cChapitre() {
    return (Integer) storedValueForKey("cChapitre");
  }

  public void setCChapitre(Integer value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cChapitre from " + cChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cChapitre");
  }

  public String cDiscSecondDegre() {
    return (String) storedValueForKey("cDiscSecondDegre");
  }

  public void setCDiscSecondDegre(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cDiscSecondDegre from " + cDiscSecondDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiscSecondDegre");
  }

  public String codeemploi() {
    return (String) storedValueForKey("codeemploi");
  }

  public void setCodeemploi(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating codeemploi from " + codeemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeemploi");
  }

  public String cProgramme() {
    return (String) storedValueForKey("cProgramme");
  }

  public void setCProgramme(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cProgramme from " + cProgramme() + " to " + value);
    }
    takeStoredValueForKey(value, "cProgramme");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cSectionCnu() {
    return (String) storedValueForKey("cSectionCnu");
  }

  public void setCSectionCnu(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cSectionCnu from " + cSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSectionCnu");
  }

  public String cSousSectionCnu() {
    return (String) storedValueForKey("cSousSectionCnu");
  }

  public void setCSousSectionCnu(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cSousSectionCnu from " + cSousSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSousSectionCnu");
  }

  public String cSpecialiteAtos() {
    return (String) storedValueForKey("cSpecialiteAtos");
  }

  public void setCSpecialiteAtos(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cSpecialiteAtos from " + cSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteAtos");
  }

  public String cSpecialiteItarf() {
    return (String) storedValueForKey("cSpecialiteItarf");
  }

  public void setCSpecialiteItarf(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cSpecialiteItarf from " + cSpecialiteItarf() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteItarf");
  }

  public String cTitre() {
    return (String) storedValueForKey("cTitre");
  }

  public void setCTitre(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating cTitre from " + cTitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cTitre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dCreationEmploi() {
    return (NSTimestamp) storedValueForKey("dCreationEmploi");
  }

  public void setDCreationEmploi(NSTimestamp value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating dCreationEmploi from " + dCreationEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreationEmploi");
  }

  public Integer denQuotiteCreEmp() {
    return (Integer) storedValueForKey("denQuotiteCreEmp");
  }

  public void setDenQuotiteCreEmp(Integer value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating denQuotiteCreEmp from " + denQuotiteCreEmp() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotiteCreEmp");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dPublicationEmploi() {
    return (NSTimestamp) storedValueForKey("dPublicationEmploi");
  }

  public void setDPublicationEmploi(NSTimestamp value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating dPublicationEmploi from " + dPublicationEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dPublicationEmploi");
  }

  public NSTimestamp dSuppressionEmploi() {
    return (NSTimestamp) storedValueForKey("dSuppressionEmploi");
  }

  public void setDSuppressionEmploi(NSTimestamp value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating dSuppressionEmploi from " + dSuppressionEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dSuppressionEmploi");
  }

  public String durabiliteEmploi() {
    return (String) storedValueForKey("durabiliteEmploi");
  }

  public void setDurabiliteEmploi(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating durabiliteEmploi from " + durabiliteEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "durabiliteEmploi");
  }

  public Integer empSource() {
    return (Integer) storedValueForKey("empSource");
  }

  public void setEmpSource(Integer value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating empSource from " + empSource() + " to " + value);
    }
    takeStoredValueForKey(value, "empSource");
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey("noCnu");
  }

  public void setNoCnu(Integer value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "noCnu");
  }

  public String noEmploiNational() {
    return (String) storedValueForKey("noEmploiNational");
  }

  public void setNoEmploiNational(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating noEmploiNational from " + noEmploiNational() + " to " + value);
    }
    takeStoredValueForKey(value, "noEmploiNational");
  }

  public String noEmploiRectorat() {
    return (String) storedValueForKey("noEmploiRectorat");
  }

  public void setNoEmploiRectorat(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating noEmploiRectorat from " + noEmploiRectorat() + " to " + value);
    }
    takeStoredValueForKey(value, "noEmploiRectorat");
  }

  public String numeroLocal() {
    return (String) storedValueForKey("numeroLocal");
  }

  public void setNumeroLocal(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating numeroLocal from " + numeroLocal() + " to " + value);
    }
    takeStoredValueForKey(value, "numeroLocal");
  }

  public Integer numQuotiteCreEmp() {
    return (Integer) storedValueForKey("numQuotiteCreEmp");
  }

  public void setNumQuotiteCreEmp(Integer value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating numQuotiteCreEmp from " + numQuotiteCreEmp() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotiteCreEmp");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String profilEmploi() {
    return (String) storedValueForKey("profilEmploi");
  }

  public void setProfilEmploi(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating profilEmploi from " + profilEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "profilEmploi");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temEnseignant() {
    return (String) storedValueForKey("temEnseignant");
  }

  public void setTemEnseignant(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating temEnseignant from " + temEnseignant() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnseignant");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temoinMisArbitrage() {
    return (String) storedValueForKey("temoinMisArbitrage");
  }

  public void setTemoinMisArbitrage(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating temoinMisArbitrage from " + temoinMisArbitrage() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinMisArbitrage");
  }

  public String temoinMisConcours() {
    return (String) storedValueForKey("temoinMisConcours");
  }

  public void setTemoinMisConcours(String value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
    	_EOEmploi.LOG.debug( "updating temoinMisConcours from " + temoinMisConcours() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinMisConcours");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOEmploi.LOG.isDebugEnabled()) {
      _EOEmploi.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOEmploi createEmploi(EOEditingContext editingContext, String cBudget
, String cCategorieEmploi
, NSTimestamp dCreation
, NSTimestamp dCreationEmploi
, NSTimestamp dModification
, String statut
, String temImport
) {
    EOEmploi eo = (EOEmploi) EOUtilities.createAndInsertInstance(editingContext, _EOEmploi.ENTITY_NAME);    
		eo.setCBudget(cBudget);
		eo.setCCategorieEmploi(cCategorieEmploi);
		eo.setDCreation(dCreation);
		eo.setDCreationEmploi(dCreationEmploi);
		eo.setDModification(dModification);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOEmploi> fetchAllEmplois(EOEditingContext editingContext) {
    return _EOEmploi.fetchAllEmplois(editingContext, null);
  }

  public static NSArray<EOEmploi> fetchAllEmplois(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEmploi.fetchEmplois(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEmploi> fetchEmplois(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEmploi.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEmploi> eoObjects = (NSArray<EOEmploi>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEmploi fetchEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploi.fetchEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploi fetchEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEmploi> eoObjects = _EOEmploi.fetchEmplois(editingContext, qualifier, null);
    EOEmploi eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEmploi)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Emploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploi fetchRequiredEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploi.fetchRequiredEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploi fetchRequiredEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEmploi eoObject = _EOEmploi.fetchEmploi(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Emploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploi localInstanceIn(EOEditingContext editingContext, EOEmploi eo) {
    EOEmploi localInstance = (eo == null) ? null : (EOEmploi)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
