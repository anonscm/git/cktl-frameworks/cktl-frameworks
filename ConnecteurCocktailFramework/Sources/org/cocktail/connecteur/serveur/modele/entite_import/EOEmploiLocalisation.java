package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOEmploiLocalisation extends _EOEmploiLocalisation {

	@Override
	public void preparerRelations() {
		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);

		EOEmploi emploi = EOEmploi.emploiPourSourceId(editingContext(), empSource());
		setEmploiRelationship(emploi);
	}

	@Override
	public void supprimerRelations() {
		setStructureRelationship(null);
		setEmploiRelationship(null);
	}

	@Override
	public EOLogImport verifierRecord() {
		EOLogImport log = null;

		String messageLog = null;
		if (emploi() == null && empSource() != null) {
			messageLog = "EMPLOI_NON_IMPORTE";
		} else if (emploi() != null && emploi().statut().equals(EOEmploi.STATUT_ERREUR)) {
			messageLog = "EMPLOI_INVALIDE";
		} else if (structure() == null && strSource() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure() != null && structure().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			log.setEmploiLocalisationRelationship(this);
			setStatut(STATUT_ERREUR);
			return log;
		} else {
			return null;
		}
	}

	@Override
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.EMPLOI_LOCALISATION_KEY;
	}

	@Override
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		// Pas d'attribut de comparaison avec des relations
		return null;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		return "";
	}
}
