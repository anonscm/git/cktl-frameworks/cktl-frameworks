// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOEnfant.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOEnfant extends ObjetImportPourPersonnel {
	public static final String ENTITY_NAME = "Enfant";

	// Attributes
	public static final String A_CHARGE_KEY = "aCharge";
	public static final String D_ADOPTION_KEY = "dAdoption";
	public static final String D_ARRIVEE_FOYER_KEY = "dArriveeFoyer";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_SFT_KEY = "dDebSft";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_FIN_SFT_KEY = "dFinSft";
	public static final String D_MAX_A_CHARGE_KEY = "dMaxACharge";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String D_SEIZE_ANS_KEY = "dSeizeAns";
	public static final String D_VINGT_ANS_KEY = "dVingtAns";
	public static final String ENF_SOURCE_KEY = "enfSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LFEN_CODE_KEY = "lfenCode";
	public static final String LIEU_NAISSANCE_KEY = "lieuNaissance";
	public static final String MORT_POUR_LA_FRANCE_KEY = "mortPourLaFrance";
	public static final String NOM_KEY = "nom";
	public static final String NO_ORDRE_NAISSANCE_KEY = "noOrdreNaissance";
	public static final String OPERATION_KEY = "operation";
	public static final String PRENOM_KEY = "prenom";
	public static final String SEXE_KEY = "sexe";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_SFT_KEY = "temSft";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOEnfant.class);

  public EOEnfant localInstanceIn(EOEditingContext editingContext) {
    EOEnfant localInstance = (EOEnfant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String aCharge() {
    return (String) storedValueForKey("aCharge");
  }

  public void setACharge(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating aCharge from " + aCharge() + " to " + value);
    }
    takeStoredValueForKey(value, "aCharge");
  }

  public NSTimestamp dAdoption() {
    return (NSTimestamp) storedValueForKey("dAdoption");
  }

  public void setDAdoption(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dAdoption from " + dAdoption() + " to " + value);
    }
    takeStoredValueForKey(value, "dAdoption");
  }

  public NSTimestamp dArriveeFoyer() {
    return (NSTimestamp) storedValueForKey("dArriveeFoyer");
  }

  public void setDArriveeFoyer(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dArriveeFoyer from " + dArriveeFoyer() + " to " + value);
    }
    takeStoredValueForKey(value, "dArriveeFoyer");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebSft() {
    return (NSTimestamp) storedValueForKey("dDebSft");
  }

  public void setDDebSft(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dDebSft from " + dDebSft() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebSft");
  }

  public NSTimestamp dDeces() {
    return (NSTimestamp) storedValueForKey("dDeces");
  }

  public void setDDeces(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dDeces from " + dDeces() + " to " + value);
    }
    takeStoredValueForKey(value, "dDeces");
  }

  public NSTimestamp dFinSft() {
    return (NSTimestamp) storedValueForKey("dFinSft");
  }

  public void setDFinSft(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dFinSft from " + dFinSft() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinSft");
  }

  public NSTimestamp dMaxACharge() {
    return (NSTimestamp) storedValueForKey("dMaxACharge");
  }

  public void setDMaxACharge(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dMaxACharge from " + dMaxACharge() + " to " + value);
    }
    takeStoredValueForKey(value, "dMaxACharge");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey("dNaissance");
  }

  public void setDNaissance(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dNaissance from " + dNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "dNaissance");
  }

  public NSTimestamp dSeizeAns() {
    return (NSTimestamp) storedValueForKey("dSeizeAns");
  }

  public void setDSeizeAns(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dSeizeAns from " + dSeizeAns() + " to " + value);
    }
    takeStoredValueForKey(value, "dSeizeAns");
  }

  public NSTimestamp dVingtAns() {
    return (NSTimestamp) storedValueForKey("dVingtAns");
  }

  public void setDVingtAns(NSTimestamp value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating dVingtAns from " + dVingtAns() + " to " + value);
    }
    takeStoredValueForKey(value, "dVingtAns");
  }

  public Integer enfSource() {
    return (Integer) storedValueForKey("enfSource");
  }

  public void setEnfSource(Integer value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating enfSource from " + enfSource() + " to " + value);
    }
    takeStoredValueForKey(value, "enfSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String lfenCode() {
    return (String) storedValueForKey("lfenCode");
  }

  public void setLfenCode(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating lfenCode from " + lfenCode() + " to " + value);
    }
    takeStoredValueForKey(value, "lfenCode");
  }

  public String lieuNaissance() {
    return (String) storedValueForKey("lieuNaissance");
  }

  public void setLieuNaissance(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating lieuNaissance from " + lieuNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuNaissance");
  }

  public String mortPourLaFrance() {
    return (String) storedValueForKey("mortPourLaFrance");
  }

  public void setMortPourLaFrance(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating mortPourLaFrance from " + mortPourLaFrance() + " to " + value);
    }
    takeStoredValueForKey(value, "mortPourLaFrance");
  }

  public String nom() {
    return (String) storedValueForKey("nom");
  }

  public void setNom(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating nom from " + nom() + " to " + value);
    }
    takeStoredValueForKey(value, "nom");
  }

  public Integer noOrdreNaissance() {
    return (Integer) storedValueForKey("noOrdreNaissance");
  }

  public void setNoOrdreNaissance(Integer value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating noOrdreNaissance from " + noOrdreNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "noOrdreNaissance");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String prenom() {
    return (String) storedValueForKey("prenom");
  }

  public void setPrenom(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating prenom from " + prenom() + " to " + value);
    }
    takeStoredValueForKey(value, "prenom");
  }

  public String sexe() {
    return (String) storedValueForKey("sexe");
  }

  public void setSexe(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating sexe from " + sexe() + " to " + value);
    }
    takeStoredValueForKey(value, "sexe");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temSft() {
    return (String) storedValueForKey("temSft");
  }

  public void setTemSft(String value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
    	_EOEnfant.LOG.debug( "updating temSft from " + temSft() + " to " + value);
    }
    takeStoredValueForKey(value, "temSft");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOEnfant.LOG.isDebugEnabled()) {
      _EOEnfant.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOEnfant createEnfant(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp dNaissance
, Integer enfSource
, Integer idSource
, String nom
, String prenom
, String statut
, String temImport
) {
    EOEnfant eo = (EOEnfant) EOUtilities.createAndInsertInstance(editingContext, _EOEnfant.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setDNaissance(dNaissance);
		eo.setEnfSource(enfSource);
		eo.setIdSource(idSource);
		eo.setNom(nom);
		eo.setPrenom(prenom);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOEnfant> fetchAllEnfants(EOEditingContext editingContext) {
    return _EOEnfant.fetchAllEnfants(editingContext, null);
  }

  public static NSArray<EOEnfant> fetchAllEnfants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEnfant.fetchEnfants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEnfant> fetchEnfants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEnfant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEnfant> eoObjects = (NSArray<EOEnfant>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEnfant fetchEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnfant.fetchEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEnfant fetchEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEnfant> eoObjects = _EOEnfant.fetchEnfants(editingContext, qualifier, null);
    EOEnfant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEnfant)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Enfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEnfant fetchRequiredEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnfant.fetchRequiredEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEnfant fetchRequiredEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEnfant eoObject = _EOEnfant.fetchEnfant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Enfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEnfant localInstanceIn(EOEditingContext editingContext, EOEnfant eo) {
    EOEnfant localInstance = (eo == null) ? null : (EOEnfant)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
