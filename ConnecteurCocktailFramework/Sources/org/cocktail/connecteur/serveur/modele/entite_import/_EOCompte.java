// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCompte.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCompte extends ObjetImportPourIndividuOuStructure {
	public static final String ENTITY_NAME = "Compte";

	// Attributes
	public static final String CPT_CHARTE_KEY = "cptCharte";
	public static final String CPT_COMMENTAIRE_KEY = "cptCommentaire";
	public static final String CPT_CONNEXION_KEY = "cptConnexion";
	public static final String CPT_CRYPTE_KEY = "cptCrypte";
	public static final String CPT_DATE_CHARTE_KEY = "cptDateCharte";
	public static final String CPT_DEBUT_VALIDE_KEY = "cptDebutValide";
	public static final String CPT_DOMAINE_KEY = "cptDomaine";
	public static final String CPT_EMAIL_KEY = "cptEmail";
	public static final String CPT_FIN_VALIDE_KEY = "cptFinValide";
	public static final String CPT_GID_KEY = "cptGid";
	public static final String CPT_HOME_KEY = "cptHome";
	public static final String CPT_LISTE_ROUGE_KEY = "cptListeRouge";
	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String CPT_PASSWD_KEY = "cptPasswd";
	public static final String CPT_PRINCIPAL_KEY = "cptPrincipal";
	public static final String CPT_SHELL_KEY = "cptShell";
	public static final String CPT_SOURCE_KEY = "cptSource";
	public static final String CPT_UID_KEY = "cptUid";
	public static final String CPT_VLAN_KEY = "cptVlan";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TCRY_LIBELLE_KEY = "tcryLibelle";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TYPE_CRYPTAGE_KEY = "typeCryptage";
	public static final String V_LAN_KEY = "vLan";

  private static Logger LOG = Logger.getLogger(_EOCompte.class);

  public EOCompte localInstanceIn(EOEditingContext editingContext) {
    EOCompte localInstance = (EOCompte)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cptCharte() {
    return (String) storedValueForKey("cptCharte");
  }

  public void setCptCharte(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptCharte from " + cptCharte() + " to " + value);
    }
    takeStoredValueForKey(value, "cptCharte");
  }

  public String cptCommentaire() {
    return (String) storedValueForKey("cptCommentaire");
  }

  public void setCptCommentaire(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptCommentaire from " + cptCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "cptCommentaire");
  }

  public String cptConnexion() {
    return (String) storedValueForKey("cptConnexion");
  }

  public void setCptConnexion(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptConnexion from " + cptConnexion() + " to " + value);
    }
    takeStoredValueForKey(value, "cptConnexion");
  }

  public String cptCrypte() {
    return (String) storedValueForKey("cptCrypte");
  }

  public void setCptCrypte(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptCrypte from " + cptCrypte() + " to " + value);
    }
    takeStoredValueForKey(value, "cptCrypte");
  }

  public NSTimestamp cptDateCharte() {
    return (NSTimestamp) storedValueForKey("cptDateCharte");
  }

  public void setCptDateCharte(NSTimestamp value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptDateCharte from " + cptDateCharte() + " to " + value);
    }
    takeStoredValueForKey(value, "cptDateCharte");
  }

  public NSTimestamp cptDebutValide() {
    return (NSTimestamp) storedValueForKey("cptDebutValide");
  }

  public void setCptDebutValide(NSTimestamp value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptDebutValide from " + cptDebutValide() + " to " + value);
    }
    takeStoredValueForKey(value, "cptDebutValide");
  }

  public String cptDomaine() {
    return (String) storedValueForKey("cptDomaine");
  }

  public void setCptDomaine(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptDomaine from " + cptDomaine() + " to " + value);
    }
    takeStoredValueForKey(value, "cptDomaine");
  }

  public String cptEmail() {
    return (String) storedValueForKey("cptEmail");
  }

  public void setCptEmail(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptEmail from " + cptEmail() + " to " + value);
    }
    takeStoredValueForKey(value, "cptEmail");
  }

  public NSTimestamp cptFinValide() {
    return (NSTimestamp) storedValueForKey("cptFinValide");
  }

  public void setCptFinValide(NSTimestamp value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptFinValide from " + cptFinValide() + " to " + value);
    }
    takeStoredValueForKey(value, "cptFinValide");
  }

  public Integer cptGid() {
    return (Integer) storedValueForKey("cptGid");
  }

  public void setCptGid(Integer value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptGid from " + cptGid() + " to " + value);
    }
    takeStoredValueForKey(value, "cptGid");
  }

  public String cptHome() {
    return (String) storedValueForKey("cptHome");
  }

  public void setCptHome(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptHome from " + cptHome() + " to " + value);
    }
    takeStoredValueForKey(value, "cptHome");
  }

  public String cptListeRouge() {
    return (String) storedValueForKey("cptListeRouge");
  }

  public void setCptListeRouge(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptListeRouge from " + cptListeRouge() + " to " + value);
    }
    takeStoredValueForKey(value, "cptListeRouge");
  }

  public String cptLogin() {
    return (String) storedValueForKey("cptLogin");
  }

  public void setCptLogin(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptLogin from " + cptLogin() + " to " + value);
    }
    takeStoredValueForKey(value, "cptLogin");
  }

  public String cptPasswd() {
    return (String) storedValueForKey("cptPasswd");
  }

  public void setCptPasswd(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptPasswd from " + cptPasswd() + " to " + value);
    }
    takeStoredValueForKey(value, "cptPasswd");
  }

  public String cptPrincipal() {
    return (String) storedValueForKey("cptPrincipal");
  }

  public void setCptPrincipal(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptPrincipal from " + cptPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "cptPrincipal");
  }

  public String cptShell() {
    return (String) storedValueForKey("cptShell");
  }

  public void setCptShell(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptShell from " + cptShell() + " to " + value);
    }
    takeStoredValueForKey(value, "cptShell");
  }

  public Integer cptSource() {
    return (Integer) storedValueForKey("cptSource");
  }

  public void setCptSource(Integer value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptSource from " + cptSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cptSource");
  }

  public Integer cptUid() {
    return (Integer) storedValueForKey("cptUid");
  }

  public void setCptUid(Integer value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptUid from " + cptUid() + " to " + value);
    }
    takeStoredValueForKey(value, "cptUid");
  }

  public String cptVlan() {
    return (String) storedValueForKey("cptVlan");
  }

  public void setCptVlan(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating cptVlan from " + cptVlan() + " to " + value);
    }
    takeStoredValueForKey(value, "cptVlan");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String tcryLibelle() {
    return (String) storedValueForKey("tcryLibelle");
  }

  public void setTcryLibelle(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating tcryLibelle from " + tcryLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tcryLibelle");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
    	_EOCompte.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
      _EOCompte.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
      _EOCompte.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage typeCryptage() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage)storedValueForKey("typeCryptage");
  }

  public void setTypeCryptageRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
      _EOCompte.LOG.debug("updating typeCryptage from " + typeCryptage() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage oldValue = typeCryptage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeCryptage");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeCryptage");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans vLan() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans)storedValueForKey("vLan");
  }

  public void setVLanRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans value) {
    if (_EOCompte.LOG.isDebugEnabled()) {
      _EOCompte.LOG.debug("updating vLan from " + vLan() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans oldValue = vLan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vLan");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vLan");
    }
  }
  

  public static EOCompte createCompte(EOEditingContext editingContext, String cptCharte
, String cptConnexion
, Integer cptSource
, Integer cptUid
, NSTimestamp dCreation
, NSTimestamp dModification
, String statut
, String temImport
) {
    EOCompte eo = (EOCompte) EOUtilities.createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME);    
		eo.setCptCharte(cptCharte);
		eo.setCptConnexion(cptConnexion);
		eo.setCptSource(cptSource);
		eo.setCptUid(cptUid);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCompte> fetchAllComptes(EOEditingContext editingContext) {
    return _EOCompte.fetchAllComptes(editingContext, null);
  }

  public static NSArray<EOCompte> fetchAllComptes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCompte.fetchComptes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCompte> fetchComptes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCompte.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCompte> eoObjects = (NSArray<EOCompte>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCompte fetchCompte(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCompte.fetchCompte(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCompte fetchCompte(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCompte> eoObjects = _EOCompte.fetchComptes(editingContext, qualifier, null);
    EOCompte eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Compte that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCompte fetchRequiredCompte(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCompte.fetchRequiredCompte(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCompte fetchRequiredCompte(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCompte eoObject = _EOCompte.fetchCompte(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Compte that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCompte localInstanceIn(EOEditingContext editingContext, EOCompte eo) {
    EOCompte localInstance = (eo == null) ? null : (EOCompte)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
