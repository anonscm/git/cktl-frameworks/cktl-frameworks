//EOEnfant.java
//Created on Wed Jul 25 15:05:10 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 24/06/2010 - changement de la super classe pour éviter l'import si un individu n'est pas personnel
// 14/03/2011 - ajout des champs dArriveeFoyer, dAdoption, dMaxACharge et dDeces et des contrôles de validité sur les dates
public class EOEnfant extends _EOEnfant {

	public EOEnfant() {
		super();
	}

	@Override
	public EOLogImport verifierRecord() {
		EOLogImport logImport=super.verifierRecord();
		if (logImport!=null)
			return logImport;

		if (dMaxACharge()==null) {
			setDMaxACharge(DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(dNaissance(), 16)));
		}
		
		if (dNaissance() != null) {
			if (dSeizeAns() == null)
				setDSeizeAns(dNaissance().timestampByAddingGregorianUnits(16, 0, 0, 2, 0, 0));
			if (dVingtAns() == null)
				setDVingtAns(dNaissance().timestampByAddingGregorianUnits(20, 0, 0, 2, 0, 0));
		}

		return null; 
	}

	/** verification des dates de dec&egrave;s, d'arrivee au foyer, de naissance */
	public EOLogImport verifierRecordApresOperation() {
		// 14/03/2011 - ajout de vérification sur les champs de date
		String messageLog = null;
		NSTimestamp today = new NSTimestamp();
		// vérifier la cohérence des dates par rapport à la date du jour
		if (DateCtrl.isAfter(dNaissance(),today)) {
			messageLog = "DATE_NAISSANCE_ENFANT";
		}
		if (dDeces() != null) {
			// vérifier que la date de naissance n'est pas postérieure à la date de décès !!
			if (DateCtrl.isBefore(dDeces(),dNaissance())) {
				messageLog = "DATE_DECES_ENFANT_1";
			} else if (DateCtrl.isAfter(dDeces(),today)) {
				messageLog = "DATE_DECES_ENFANT_2";
			}
			if (messageLog == null) {
			// la date de fin de prise en charge ne peut être postérieure à la date de décès
				if (dMaxACharge() == null || DateCtrl.isAfter(dMaxACharge(), dDeces())) {
					setDMaxACharge(dDeces());
				}
				// vérifier que la date de décès n'est pas antérieure à la date d'arrivée au foyer !!
				if (dArriveeFoyer() != null && DateCtrl.isBefore(dDeces(),dArriveeFoyer())) {
					messageLog = "DATE_ARRIVEE_ENFANT_2";
				}
			}
		}
		//	vérifier que la date de naissance n'est pas postérieure à la date d'arrivée au foyer !!
		if (dArriveeFoyer() != null) {
			if (DateCtrl.isBefore(dArriveeFoyer(),dNaissance())) {
				messageLog = "DATE_ARRIVEE_ENFANT_1";
			} else if (DateCtrl.isAfter(dArriveeFoyer(),today)) {
				messageLog = "DATE_ARRIVEE_ENFANT_3";

			}
		}
		// Vérification des dates du SFT
		if (dDebSft() != null) {
			if (DateCtrl.isBefore(dDebSft(), dNaissance())) {
				messageLog = "DATE_DEBUT_SFT_1";
			}
			if (dFinSft() != null) {
				if (DateCtrl.isBefore(dFinSft(), dDebSft())) {
					messageLog = "DATE_DEBUT_SFT_2";
				}
			} else {
				messageLog = "DATE_FIN_SFT_1";
			}
		}
		if (dFinSft() != null && DateCtrl.isAfterEq(dFinSft(), DateCtrl.dateAvecAjoutAnnees(dNaissance(), 20))) {
			messageLog = "DATE_FIN_SFT_2";
		}
		if (dMaxACharge() != null) {
			// La date de prise en charge doit être postérieure à la date de naissance
			if (DateCtrl.isBefore(dMaxACharge(), dNaissance())) {
				messageLog = "DATE_MAX_A_CHARGE_1";
			}
			// Si l'enfant doit arrêter ses études pour cause de maladie, l'âge peut être repoussé jusqu'à la fin de l'année scolaire
			// qui comprend son 21ème anniversaire. L'année scolaire commence le 1er octobre et finit le 30 septembre suivant.
			NSTimestamp date21 = DateCtrl.dateAvecAjoutAnnees(dNaissance(), 21);
			int annee = DateCtrl.getYear(date21);
			NSTimestamp dateMax = DateCtrl.stringToDate("30/09/" + annee);
			if (DateCtrl.isAfter(dMaxACharge(),dateMax)) {
				messageLog = "DATE_MAX_A_CHARGE_2";
			}
		}
		if (dAdoption() != null) {
			if (DateCtrl.isAfter(dAdoption(),today)) {
				messageLog = "DATE_ADOPTION_ENFANT_1";
			} else if (DateCtrl.isBefore(dAdoption(),dNaissance())) {
				messageLog = "DATE_ADOPTION_ENFANT_2";
			}
		}
		if (messageLog != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(),this.entityName(),messageLog);
			if (individu() != null) {
				log.setIdSource(individu().idSource());
				log.addObjectToBothSidesOfRelationshipWithKey(individu(),"individu");
			} 
			log.addObjectToBothSidesOfRelationshipWithKey(this, this.nomRelationPourLog());
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			return log;
		}
		return null;
	}
	/** retourne un enfant dans le SI destinataire avec le m&ecirc;me parent, le m&ecirc;me prenom ou la m&ecirc;me date de naissance */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null) {
			if (estNouvelIndividu()) {
				return null;
			} else {
				return EOGrhumEnfant.enfantDestinationPourEnfant(editingContextTravail(),this);
			}
		} else {
			return null;
		}
	}
	public String nomRelationPourLog() {
		return EOLogImport.ENFANT_KEY;
	}
	// Méthodes statiques
	/** Valide les donnees d'un enfant.<BR>
	 *  Verifie que le sexe est bien M ou F.
	 *  @param editingContext
	 *  @param valeursAttributs dictionnaire des attributs a valider ou modifier
	 */
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number)valeursAttributs.objectForKey("idSource");
		if (idSource == null) {
			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
		}
		String sexe = (String)valeursAttributs.objectForKey("sexe");
		if (sexe != null && sexe.equals("F") == false && sexe.equals("M") == false) {
			resultat += "Sexe_Invalide >>" + sexe + System.getProperty("line.separator");
		}
		Integer noOrdreNaissance=(Integer)valeursAttributs.objectForKey(NO_ORDRE_NAISSANCE_KEY);
		if (noOrdreNaissance==null)
			valeursAttributs.setObjectForKey(new Integer(1), NO_ORDRE_NAISSANCE_KEY);
		
		resultat = resultat + verifierTemoin(valeursAttributs, "aCharge");
		resultat = resultat + verifierTemoin(valeursAttributs, "temSft");
		resultat = resultat + verifierTemoin(valeursAttributs, "mortPourLaFrance");

		return resultat;
	}
	/* retourne l'enfant dans les imports avec le m&ecirc;me parent, le m&ecirc;me prenom ou la m&ecirc;me date de naissance 
	public static EOEnfant enfantEquivalent(EOEditingContext editingContext,EOEnfant enfant) {
		if (enfant.individu() != null) {
			NSMutableArray args = new NSMutableArray();
			args.addObject("nom");
			args.addObject("prenom");
			args.addObject("dNaissance");
			args.addObject(enfant.individu().idSource());
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("nom caseInsensitiveLike %@ AND prenom caseInsensitiveLike %@ AND dNaissance = %@  AND individu.idSource = %@ AND temImport = 'O'",args);
			// On trie sur les dates de modification décroissantes de manière à être sûr d'avoir les modifications les plus récentes
			NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("dModification", EOSortOrdering.CompareDescending));
			EOFetchSpecification fs = new EOFetchSpecification("Enfant",myQualifier, sorts);
			fs.setFetchLimit(1);
			try {
				return (EOEnfant)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
			} catch (Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}*/

}
