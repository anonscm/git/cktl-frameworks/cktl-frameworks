package org.cocktail.connecteur.serveur.modele.entite_import.interfaces;

import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.InterfaceHomonymie;

public interface IObjetImport {

	public void setOperation(String value);
	public String operation();
	
	public void setStatut(String value);
	
	/** retourne la correspondance associee a un record d'import : on recherche une correspondance pointant
	 * sur un record d'import avec les m&circ;mes attributs source que le record courant */
	public ObjetCorresp correspondance();
	
	/** retourne true si il existe dans le SI destinataire un objet equivalent */
	public  boolean dejaDansSIDestinataire();
	
	public boolean testUpdateActif();
	
}
