package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOHeberge extends _EOHeberge {

	@Override
	public void preparerRelations() {
		super.preparerRelations();
		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);

		EOStructure structureOrigine = EOStructure.structurePourSourceId(editingContext(), strOrigineSource());
		setStructureOrigineRelationship(structureOrigine);
}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		
		setStructureRelationship(null);
		setStructureOrigineRelationship(null);
	}

	
	@Override
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;
		if (structure() == null && strSource() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure() != null && structure().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		} else if (structureOrigine() == null && strOrigineSource() != null) {
			messageLog = "STRUCTURE_ORIGINE_NON_IMPORTEE";
		} else if (structureOrigine() != null && structureOrigine().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_ORIGINE_INVALIDE";
		} 
		
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		} 
		
		return log;
	}

	@Override
	public Integer eimpSource() {
		return hebSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setHebSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.HEBERGE_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);

		String typeContrat = (String) valeursAttributs.objectForKey(C_TYPE_CONTRAT_TRAV_KEY);
		if (typeContrat != null) {
			EOTypeContratTravail recordTypeContrat = EOTypeContratTravail.getFromCode(editingContext, typeContrat);
			if (recordTypeContrat == null) {
				resultat = resultat + "TypeContratTravail_Inconnu >>" + typeContrat + System.getProperty("line.separator");
			}
		}

		String cRne = (String) valeursAttributs.objectForKey(C_RNE_KEY);
		String strSource = (String) valeursAttributs.objectForKey(STR_SOURCE_KEY);
		String strOrigineSource = (String) valeursAttributs.objectForKey(STR_ORIGINE_SOURCE_KEY);

		if (cRne == null && strSource == null && strOrigineSource == null) {
			resultat = resultat + "Rne_StrSource_Et_StrOrigineSource_Vides >>" + System.getProperty("line.separator");
		}

		if (cRne != null) {
			EORne rneRecord = EORne.getFromCode(editingContext, cRne);
			if (rneRecord == null) {
				resultat = resultat + "Rne_Inconnu >>" + cRne + System.getProperty("line.separator");
			}
		}

		String corps = (String) valeursAttributs.objectForKey(C_CORPS_KEY);
		EOCorps recordCorps = null;
		if (corps != null) {
			recordCorps = EOCorps.getFromCode(editingContext, corps);
			if (recordCorps == null) {
				resultat = resultat + "Corps_Inconnu >>" + corps + System.getProperty("line.separator");
			}
		}
		String grade = (String) valeursAttributs.objectForKey(C_GRADE_KEY);
		EOGrade recordGrade = null;
		if (grade != null) {
			recordGrade = EOGrade.getFromCode(editingContext, grade);
			if (recordGrade == null) {
				resultat = resultat + "Grade_Inconnu >>" + grade + System.getProperty("line.separator");
			}
		}
		// Vérifier que le grade est compatible avec le corps
		if (recordCorps != null && recordGrade != null && !recordGrade.toCorps().equals(recordCorps)) {
			resultat = resultat + "Corps_Grade_Incompatible >>" + grade + " incompatible avec " + corps + System.getProperty("line.separator");
		}

		resultat = resultat + verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);
		return resultat;
	}
}
