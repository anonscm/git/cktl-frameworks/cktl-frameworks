// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntRaisonFamPerso.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntRaisonFamPerso extends CongeAvecArrete {
	public static final String ENTITY_NAME = "CgntRaisonFamPerso";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String C_MOTIF_CG_RFP_KEY = "cMotifCgRfp";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CRFP_SOURCE_KEY = "crfpSource";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String IND_ORDRE_KEY = "indOrdre";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCgntRaisonFamPerso.class);

  public EOCgntRaisonFamPerso localInstanceIn(EOEditingContext editingContext) {
    EOCgntRaisonFamPerso localInstance = (EOCgntRaisonFamPerso)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public String cMotifCgRfp() {
    return (String) storedValueForKey("cMotifCgRfp");
  }

  public void setCMotifCgRfp(String value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating cMotifCgRfp from " + cMotifCgRfp() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifCgRfp");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Integer crfpSource() {
    return (Integer) storedValueForKey("crfpSource");
  }

  public void setCrfpSource(Integer value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating crfpSource from " + crfpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "crfpSource");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer indOrdre() {
    return (Integer) storedValueForKey("indOrdre");
  }

  public void setIndOrdre(Integer value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating indOrdre from " + indOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "indOrdre");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPerso.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
      _EOCgntRaisonFamPerso.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCgntRaisonFamPerso.LOG.isDebugEnabled()) {
      _EOCgntRaisonFamPerso.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCgntRaisonFamPerso createCgntRaisonFamPerso(EOEditingContext editingContext, String cMotifCgRfp
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temValide
) {
    EOCgntRaisonFamPerso eo = (EOCgntRaisonFamPerso) EOUtilities.createAndInsertInstance(editingContext, _EOCgntRaisonFamPerso.ENTITY_NAME);    
		eo.setCMotifCgRfp(cMotifCgRfp);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCgntRaisonFamPerso> fetchAllCgntRaisonFamPersos(EOEditingContext editingContext) {
    return _EOCgntRaisonFamPerso.fetchAllCgntRaisonFamPersos(editingContext, null);
  }

  public static NSArray<EOCgntRaisonFamPerso> fetchAllCgntRaisonFamPersos(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntRaisonFamPerso.fetchCgntRaisonFamPersos(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntRaisonFamPerso> fetchCgntRaisonFamPersos(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntRaisonFamPerso.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntRaisonFamPerso> eoObjects = (NSArray<EOCgntRaisonFamPerso>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntRaisonFamPerso fetchCgntRaisonFamPerso(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntRaisonFamPerso.fetchCgntRaisonFamPerso(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntRaisonFamPerso fetchCgntRaisonFamPerso(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntRaisonFamPerso> eoObjects = _EOCgntRaisonFamPerso.fetchCgntRaisonFamPersos(editingContext, qualifier, null);
    EOCgntRaisonFamPerso eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntRaisonFamPerso)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntRaisonFamPerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntRaisonFamPerso fetchRequiredCgntRaisonFamPerso(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntRaisonFamPerso.fetchRequiredCgntRaisonFamPerso(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntRaisonFamPerso fetchRequiredCgntRaisonFamPerso(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntRaisonFamPerso eoObject = _EOCgntRaisonFamPerso.fetchCgntRaisonFamPerso(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntRaisonFamPerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntRaisonFamPerso localInstanceIn(EOEditingContext editingContext, EOCgntRaisonFamPerso eo) {
    EOCgntRaisonFamPerso localInstance = (eo == null) ? null : (EOCgntRaisonFamPerso)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
