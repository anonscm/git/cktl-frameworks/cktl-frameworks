package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEnfantCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**.
 * Classe gérant les Congés d'Adoption d'un individu
 * @author alainmalaplate
 *
 */
public class EOCongeAdoption extends _EOCongeAdoption {

	private static final long serialVersionUID = 5731534476287462993L;
	
	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCongeAdoption.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log == null) {
			LogManager.logDetail("Verification de la relation sur l'enfant");
			if (enfant() == null && enfSource() != null) {
				EOEnfant enfant = EOEnfantCorresp.enfantPourIndividuEtId(editingContextTravail(), idSource(), enfSource());
				if (enfant != null) {
					setEnfantRelationship(enfant);
				}
				String messageLog = null;
				if (enfant == null) {
					messageLog = "ENFANT_NON_IMPORTE";
				} else if (enfant.temImport().equals(ObjetImport.A_TRANSFERER)) {
					if (enfant.statut().equals(EOEnfant.STATUT_ERREUR)) {
						messageLog = "ENFANT_INVALIDE";
					}
				}
				if (messageLog != null) {
					log = new EOLogImport();
					log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
					log.ajouterRelation(this);
					log.setEimpSource(eimpSource());
					if (enfant() != null) {
						log.setEnfantRelationship(enfant());
					}
					setStatut(STATUT_ERREUR);
				}
			}
		}
		return log;
	}

	public void preparerRelations() {
		super.preparerRelations();
		if (editingContext() != null && enfSource() != null && individu() != null) {
			EOEnfant enfant = EOEnfantCorresp.enfantPourIndividuEtId(editingContext(), individu().idSource(), enfSource());
			if (enfant != null) {
				setEnfantRelationship(enfant);
			}
		}
	}

	public void supprimerRelations() {
		setEnfantRelationship(null);
		super.supprimerRelations();
	}

	public EOLogImport verifierRecordApresOperation() {

		EOLogImport log = super.verifierRecord();
		if (log != null) {
			log.setEimpSource(eimpSource());
		}
		return log;
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
		return resultat;
	}


	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_ADOPTION_KEY;
	}

	@Override
	public Integer eimpSource() {
		return cadopSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCadopSource(value);
	}

	@Override
	public NSTimestamp dateCommission() {
		return null;
	}

	@Override
	protected String verifierConge(EOEditingContext editingContext) {
		return null;
	}

	@Override
	public String sourceKey() {
		return CADOP_SOURCE_KEY;
	}

	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCongeAdoption) conge);
	}
}
