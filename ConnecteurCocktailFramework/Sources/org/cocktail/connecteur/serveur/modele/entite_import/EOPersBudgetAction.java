package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOPersBudgetAction extends _EOPersBudgetAction {

	@Override
	public void preparerRelations() {
		EOPersBudget persBudget=EOPersBudget.persBudgetPourSourceId(editingContext(),pbudSource());
		setToPersBudgetRelationship(persBudget);
		
		EOLolfNomenclatureDepense lolf=EOLolfNomenclatureDepense.lolfFromCodeEtNiveau(editingContext(),lolfCode(),lolfNiveau());
		setToLolfNomenclatureDepenseRelationship(lolf);
	}

	@Override
	public void supprimerRelations() {
		setToPersBudgetRelationship(null);
		setToLolfNomenclatureDepenseRelationship(null);
	}

	@Override
	public EOLogImport verifierRecord() {
		String messageLog = null;
		if (toPersBudget() == null && pbudSource() != null) {
			messageLog = "PERS_BUDGET_NON_IMPORTE";
		} else if (toPersBudget() != null && toPersBudget().statut().equals(STATUT_ERREUR)) {
			messageLog = "PERS_BUDGET_INVALIDE";
		} else if (toLolfNomenclatureDepense() == null && lolfCode() != null) {
			messageLog = "LOLF_CODE_INCONNU";
		} 
		
		EOLogImport log=null;
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContext(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		} 

		return log;
	}

	@Override
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.PERS_BUDGET_ACTION_KEY;
	}

	@Override
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}
	
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		return resultat;
	}
}
