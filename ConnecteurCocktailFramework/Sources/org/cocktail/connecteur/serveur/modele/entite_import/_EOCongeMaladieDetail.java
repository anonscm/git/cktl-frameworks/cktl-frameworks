// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaladieDetail.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaladieDetail extends ObjetImport {
	public static final String ENTITY_NAME = "CongeMaladieDetail";

	// Attributes
	public static final String CMAL_SOURCE_KEY = "cmalSource";
	public static final String CMD_SOURCE_KEY = "cmdSource";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TAUX_KEY = "taux";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String CONGE_MALADIE_KEY = "congeMaladie";

  private static Logger LOG = Logger.getLogger(_EOCongeMaladieDetail.class);

  public EOCongeMaladieDetail localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaladieDetail localInstance = (EOCongeMaladieDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cmalSource() {
    return (Integer) storedValueForKey("cmalSource");
  }

  public void setCmalSource(Integer value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating cmalSource from " + cmalSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cmalSource");
  }

  public Integer cmdSource() {
    return (Integer) storedValueForKey("cmdSource");
  }

  public void setCmdSource(Integer value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating cmdSource from " + cmdSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cmdSource");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public Integer taux() {
    return (Integer) storedValueForKey("taux");
  }

  public void setTaux(Integer value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating taux from " + taux() + " to " + value);
    }
    takeStoredValueForKey(value, "taux");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetail.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie congeMaladie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie)storedValueForKey("congeMaladie");
  }

  public void setCongeMaladieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie value) {
    if (_EOCongeMaladieDetail.LOG.isDebugEnabled()) {
      _EOCongeMaladieDetail.LOG.debug("updating congeMaladie from " + congeMaladie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie oldValue = congeMaladie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladie");
    }
  }
  

  public static EOCongeMaladieDetail createCongeMaladieDetail(EOEditingContext editingContext, Integer cmalSource
, Integer cmdSource
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String operation
, String statut
, Integer taux
, String temImport
) {
    EOCongeMaladieDetail eo = (EOCongeMaladieDetail) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaladieDetail.ENTITY_NAME);    
		eo.setCmalSource(cmalSource);
		eo.setCmdSource(cmdSource);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setOperation(operation);
		eo.setStatut(statut);
		eo.setTaux(taux);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCongeMaladieDetail> fetchAllCongeMaladieDetails(EOEditingContext editingContext) {
    return _EOCongeMaladieDetail.fetchAllCongeMaladieDetails(editingContext, null);
  }

  public static NSArray<EOCongeMaladieDetail> fetchAllCongeMaladieDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaladieDetail.fetchCongeMaladieDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaladieDetail> fetchCongeMaladieDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaladieDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaladieDetail> eoObjects = (NSArray<EOCongeMaladieDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaladieDetail fetchCongeMaladieDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieDetail.fetchCongeMaladieDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieDetail fetchCongeMaladieDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaladieDetail> eoObjects = _EOCongeMaladieDetail.fetchCongeMaladieDetails(editingContext, qualifier, null);
    EOCongeMaladieDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaladieDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaladieDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieDetail fetchRequiredCongeMaladieDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieDetail.fetchRequiredCongeMaladieDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieDetail fetchRequiredCongeMaladieDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaladieDetail eoObject = _EOCongeMaladieDetail.fetchCongeMaladieDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaladieDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieDetail localInstanceIn(EOEditingContext editingContext, EOCongeMaladieDetail eo) {
    EOCongeMaladieDetail localInstance = (eo == null) ? null : (EOCongeMaladieDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
