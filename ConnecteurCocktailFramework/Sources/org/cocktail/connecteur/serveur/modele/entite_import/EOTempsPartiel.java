//EOTempsPartiel.java
//Created on Mon Nov 21 16:34:45 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.Outils;
import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifTempsPartiel;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEnfantCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * R&egrave;gles de validation des temps partiel :<BR>
 * Si la date de fin est fournie, celle de debut doit etre anterieure<BR>
 * Il doit y avoir au moins un segment de carri&egrave; ou un contrat pour cette periode<BR>
 * Le motif doit &ecirc;tre fourni<BR>
 * Il ne peut pas y avoir de chevauchement entre deux periodes de temps partiel<BR>
 * La quotite doit etre comprise entre 50% et 90%<BR>
 * Pour un agent a temps partiel de droit, la quotite doit &ecirc;tre inferieure &grave; 90%<BR>
 * La periodicite doit &ecirc;tre definie et inferieure a 12 (mois). Le contr&ocir;le sur la borne inferieure 6 mois n'est pas fait car dans certains cas de
 * terrain, il est inapplicable<BR>
 * Un contractuel recrute a temps incomplet ne peut pas beneficier d'un temps partiel.<BR>
 * Les enseignants hospitalo-universitaires ne peuvent pas beneficier d'un temps partiel.<BR>
 * Pour un comptable, la quotite ne peut etre que 80% ou 90%<BR>
 * Un temps partiel de droit pour handicap ne s'applique qu'aux titulaires<BR>
 * On ne peut definir un temps partiel de droit pour handicap que si les periodes de handicap chevauchent la periode de temps partiel et que la date d'avis
 * medical pour ces periodes est saisie.<BRW Certains types de contrat ne sont pas compatibles avec un temps partiel.<BR>
 * Un temps partiel de droit exige un contrat d'un an minimum a temps plein<BR>
 * Un temps partiel sur autorisation exige un contrat continu minimum d'un an a temps plein<BR>
 * Un temps partiel surcotise ne peut commencer qu'apr&egrave;s le 01/01/2004<BR>
 * sauf si il s'agit d'un temps partiel pour la naissance d'un enfant<BR>
 * La quotite de temps partiel pour un enseignant de second degre doit &ecirc;tre de 50%.<BR>
 * Pour un temps partiel d'un enseignant, la périodicite est de 12 mois.<BR>
 * Les periodes de temps partiel doivent &ecirc;tre compatibles avec les conges et modalites de service pendant la periode.<BR>
 * La declaration de l'enfant est obligatoire dans un temps partiel pour elever un enfant ou un enfant adopte apr&egrave;s le 01/01/2004.<BR>
 * Le motif de temps partiel doit &ecirc;tre coherent avec le lien de filiation de l'enfant choisi<BR>
 * Si un enfant est ajoute, il doit avoir moins de 3 ans ou &ecirc;tre arrive depuis moins de 3 ans au foyer pendant la duree du temps partiel<BR>
 * 
 * @author christine
 */
// 23/09/2011 - La gestion du temps partiel est à modifier lorsque la gestion du
// lien de filiation dans les enfants sera gérée
// 23/09/2011 - La gestion du temps partiel est à modifier lorsque les périodes
// de handicap seront importées (voir EOTempsPartiel dans Mangue)
// 23/09/2011 - La validation des quotités de temps partiel pour les enseignants
// du 1er degré est à modifier lorsqu'elle sera modifiée dans Mangue
public class EOTempsPartiel extends _EOTempsPartiel {
	private static String CORPS_POUR_COMPTABLE = "024";
	/**
	 * Date &grave; partir de laquelle la surcotisation est autorisee dans les temps partiels
	 */
	private final static NSTimestamp DATE_LIMITE_SURCOTISATION = DateCtrl.stringToDate("01/01/2004");
	/**
	 * Date &grave; partir de laquelle il faut declarer les enfants dans les temps partiels
	 */
	public final static NSTimestamp DATE_LIMITE_DECLARATION_ENFANT = DateCtrl.stringToDate("01/01/2004");

	public String typeEvenement() {
		return "TPAR";
	}

	public boolean surcotisePourRetraite() {
		return temSurcotisation() != null && temSurcotisation().equals(CocktailConstantes.VRAI);
	}

	public Integer eimpSource() {
		return tpSource();
	}

	public void setEimpSource(Integer value) {
		setTpSource(value);
	}

	/**
	 * Prepare les relations. A appeler apres avoir inse un objet dans l'editing context
	 */
	public void preparerRelations() {
		super.preparerRelations();
		if (enfSource() != null) {
			EOEnfant enfant = EOEnfantCorresp.enfantPourIndividuEtId(editingContext(), individu().idSource(), enfSource());
			setEnfantRelationship(enfant);
		}
	}

	public void supprimerRelations() {
		setEnfantRelationship(null);
		super.supprimerRelations();
	}

	/**
	 * Verifie que les relations du record sont bien valides : i.e que l'enfant existe bien cree juste auparavant. Retourne un message de log ou null si pas
	 * d'erreur
	 */
	public EOLogImport verifierRecord() {
		// Suivant l'ordre d'apparition des TempsPartiel dans le xml, l'objet lié peut ne pas encore avoir été rajouté dans la base lors de l'appel à preparerRelation
		// On attend donc verifierRecord pour faire le lien
		if (arreteAnnulationSource() != null) {
			EOTempsPartiel arreteAnnulation=tpPourSourceId(editingContext(), arreteAnnulationSource());
			setArreteAnnulationRelationship(arreteAnnulation);
		}
		
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;
		if (arreteAnnulationSource() != null && arreteAnnulation() == null) {
			messageLog = "ARRETE_ANNULATION_NON_IMPORTE";
		} else if (arreteAnnulation() != null && arreteAnnulation().statut().equals(STATUT_ERREUR)) {
			messageLog = "ARRETE_ANNULATION_INVALIDE";
		}

		if (enfSource() != null && enfant() == null) {
			messageLog = "ENFANT_NON_IMPORTE";
		} else if (enfant() != null && enfant().statut().equals(STATUT_ERREUR)) {
			messageLog = "ENFANT_INVALIDE";
		}
		
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			log.setEnfantRelationship(enfant());
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	/**
	 * Verifie qu'il n'y a pas de chevauchement de periodes avec des objets de eme type dans l'import courant et dans le SI Destinataire. Verifie la quotite de
	 * temps partiel, le temoin de surcotisation et les caracteristiques des temps partiels lies aux enfants. Verifie le temps partiel par rapport aux
	 * carrieres/contrats
	 */
	public EOLogImport verifierRecordApresOperation() {
		if (estAnnule())
			return null;
		
		EOLogImport log = null;

		String message = verifierChevauchement();
		
		if (message==null)
			message = verifierTempsPartiel(editingContext(), enfant(), individu());

		if (message != null) {
			LogManager.logDetail("Insertion d'un log pour le chevauchement des périodes");
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), message);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			log.setEnfantRelationship(enfant());
			setStatut(STATUT_ERREUR); 
			return log;
		}
		return log;
	}

	// Méthodes privées

	private String verifierChevauchement() {
		// On recherche tous les objets de même type dans l'import courant

		NSArray objetsImport = rechercherObjetsImportPourIndividuEtPeriode(editingContextTravail(), this.entityName(), idSource(), dateDebut(),
				dFinExecution(), false);
		int nb_elements_non_annules=0;
		
		for (Object obj : objetsImport) {
			EOTempsPartiel tempsPartiel = (EOTempsPartiel) obj;
			if (!tempsPartiel.estAnnule())
				nb_elements_non_annules++;
		}

		boolean chevauchementPeriodes = nb_elements_non_annules > 1;
		if (!chevauchementPeriodes) {
			// on recherche le nombre d'objets de même type dans le SI
			// Destinataire (au plus 1 puisque les périodes ne peuvent pas se
			// chevaucher)
			int nbObjets = nbObjetsPourPeriodeDansSIDestinataire(editingContextTravail());
			LogManager.logDetail("nb objets dans SI Destinataire " + nbObjets);
			// Si il y en a déjà un et qu'on est en insertion => chevauchement,
			// sinon on est nécessairement en update et c'est l'objet qui
			// correspond à this
			if ((nbObjets == 1 && this.operation().equals(ObjetImport.OPERATION_INSERTION))) {
				LogManager.logDetail("chevauchement dans SI Destinataire");
				return "CHEVAUCHEMENT_PERIODE_DESTIN";
			} else {
				return null;
			}
		} else {
			LogManager.logDetail("chevauchement dans Import courant");
			return "CHEVAUCHEMENT_PERIODE_IMPORT";
		}

	}

	/**
	 * recherche les objets de type ObjetImportPourIndividuEtPeriode valide pendant la periode
	 * 
	 * @param editingContext
	 * @param individu
	 * @param nomEntite
	 *            nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode
	 *            peut &ecirc;tre nulle
	 * @param shouldRefresh
	 *            true si les donnees sont raffra&icirc;chies
	 * @return objets trouves
	 */
	public static NSArray rechercherObjetsImportPourIndividuEtPeriode(EOEditingContext editingContext, String nomEntite, Number idSource,
			NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean shouldRefresh) {
		NSMutableArray qualifiers = new NSMutableArray();

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E' ", new NSArray(idSource));
		qualifiers.addObject(qualifier);

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_IMPORT_KEY + "=%@", new NSArray(ObjetImport.A_TRANSFERER)));

		NSMutableArray orQualifiers = new NSMutableArray();
		NSMutableArray andQualifiers = new NSMutableArray();

		andQualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FIN_EXECUTION_KEY + "=nil", null));

		orQualifiers.addObject(new EOAndQualifier(andQualifiers));

		andQualifiers = new NSMutableArray();

		andQualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, D_FIN_EXECUTION_KEY, finPeriode));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FIN_EXECUTION_KEY + " != nil", null));

		orQualifiers.addObject(new EOAndQualifier(andQualifiers));

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		return rechercherDureePourEntiteAvecCriteres(editingContext, nomEntite, new EOAndQualifier(qualifiers), shouldRefresh);
	}

	/**
	 * Verifie les donnes du record en provenance du client. Effectue les memes verifications que verifierRecordApresOperation et retourne le message de log si
	 * il y a une erreur
	 */
	public String verifierRecordPourClient(EOEditingContext editingContext) {
		String message = super.verifierRecordPourClient(editingContext);
		if (message != null) {
			return message;
		}
		// Rajouter l'individu qui n'est pas construit lorsqu'on effectue les
		// vérifications client et l'enfant
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource());
		EOEnfant enfant = null;
		if (enfSource() != null) {
			enfant = EOEnfantCorresp.enfantPourIndividuEtId(editingContext, idSource(), enfSource());
		}
		return verifierTempsPartiel(editingContext, enfant, individu);
	}

	/**
	 * Temps partiel avec la meme date de debut (pas de chevauchement des temps partiels)
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() == null || estNouvelIndividu()) { // A fortiori le temps
															// partiel est
															// nouveau
			return null;
		}
		// Rechercher les temps partiels qui commencent à la même date, il y en
		// a au plus un puisqu'il n'y a pas de chevauchement
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) individu().objetDestinataireAvecOuSansCorrespondance(editingContextTravail());
		if (individuGrhum != null) {
			NSArray tempsPartielsMangue = EOMangueTempsPartiel.rechercherTempsPartielManguePourIndividuEtDateDebut(editingContextTravail(), individuGrhum,
					dateDebut());
			// Si il y en a plus d'un, on ne sait pas lequel prendre mais ne
			// devrait pas se produire car pas de chevauchement des temps
			// partiels
			if (tempsPartielsMangue == null || (tempsPartielsMangue != null && tempsPartielsMangue.count() > 1)) {
				return null;
			}
			try {
				EOMangueTempsPartiel tempsPartiel = (EOMangueTempsPartiel) tempsPartielsMangue.objectAtIndex(0);
				// Vérifier si il n'est pas affecté à un autre temps partiel qui
				// est déjà importé
				ObjetCorresp correspondance = ObjetCorresp.rechercherObjetCorrespPourRecordDestinataire(editingContextTravail(), tempsPartiel, "TempsPartiel");
				if (correspondance != null) {
					return null; // le temps partiel destinataire correspond
									// déjà à un autre temps partiel
				} else {
					return tempsPartiel;
				}
			} catch (Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}

	/** Pas de relation dans le log */
	public String nomRelationPourLog() {
		return EOLogImport.TEMPS_PARTIEL_KEY;
	}

	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueTempsPartiel.rechercherTempsPartielsManguePourIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	// Méthodes privées
	private String verifierTempsPartiel(EOEditingContext editingContext, EOEnfant enfant, EOIndividu individu) {
		EOMotifTempsPartiel motif = (EOMotifTempsPartiel) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "MotifTempsPartiel",
				"cMotifTempsPartiel", cMotifTempsPartiel());
		if (motif.aQuotiteLimitee() && numQuotite().doubleValue() > 80) {
			return "QUOTITE_TP_DROIT";
		}
		if (surcotisePourRetraite() && DateCtrl.isBefore(dateDebut(), DATE_LIMITE_SURCOTISATION)) {
			return "TEMPS_PARTIEL_SURCOTISATION";
		}
		// vérifier pour les enfants
		boolean declarationEnfantObligatoire = dateDebut() != null && DateCtrl.isAfterEq(dateDebut(), DATE_LIMITE_DECLARATION_ENFANT)
				&& (motif.estPourEleverEnfant() || motif.estPourEleverEnfantAdopte());
		if (declarationEnfantObligatoire && enfant == null) {
			return "ENFANT_OBLIGATOIRE";
		}
		if (enfant != null) {
			if (motif.estPourEleverEnfant() == false && motif.estPourEleverEnfantAdopte() == false) {
				return "ENFANT_TP_INVALIDE";
			}
			// Vérifier si l'enfant choisi est cohérent avec le motif
			/*
			 * if (enfant().lienFiliation() != null) { if ((enfant().lienFiliation().estEnfantLegitime() && motif.estPourEleverEnfant() == false) ||
			 * (enfant().lienFiliation().estAdopte() && motif.estPourEleverEnfantAdopte() == false)) { return
			 * "Le lien de filiation de l'enfant ne correspond pas au motif choisi" ; } }
			 */
			if (motif.estPourEleverEnfant() || motif.estPourEleverEnfantAdopte()) {
				// vérifier qu'il dure au maximum 3 ans
				if (dateFin() == null || DateCtrl.calculerDureeEnMois(dateDebut(), dateFin()).intValue() > 36) {
					return "DUREE_MAX_ENFANT";
				}
			}
			// Vérifier si l'enfant a moins de 3 ans au début du temps partiel
			NSTimestamp date = enfant.dNaissance();
			if (motif.estPourEleverEnfantAdopte()) {
				date = enfant.dArriveeFoyer();
				if (date == null) {
					return "DATE_ARRIVEE_FOYER";
				}
			}
			NSTimestamp dateAnniversaireTroisAns = DateCtrl.dateAvecAjoutAnnees(date, 3);
			if (DateCtrl.isAfter(dateDebut(), dateAnniversaireTroisAns)) {
				return "DATE_TROIS_ANS";
			}
			if (dateFin() != null && DateCtrl.isAfter(dateFin(), dateAnniversaireTroisAns)) {
				return "DATE_MAX_TROIS_ANS";
			}

		}
		if (individu != null) {
			return validerPourCarriereEtContrat(editingContext, individu, motif);
		} else {
			return null;
		}
	}

	private String validerPourCarriereEtContrat(EOEditingContext editingContext, EOIndividu individu, EOMotifTempsPartiel motif)
			throws NSValidation.ValidationException {

		NSTimestamp myDateFin = dateFin();
		if (dFinExecution() != null)
			myDateFin = dFinExecution();

		// Vérifier la carrière dans l'import courant : valider les types de
		// population
		NSArray carrieres = EOCarriere.rechercherCarrieresValidesImportCourantPourIndividuEtPeriode(editingContext, individu, dateDebut(), myDateFin, false);
		boolean aCarriere = false;
		if (carrieres != null && carrieres.count() > 0) {
			aCarriere = true;
			java.util.Enumeration e = carrieres.objectEnumerator();
			while (e.hasMoreElements()) {
				EOCarriere carriere = (EOCarriere) e.nextElement();
				EOGenericRecord typePopulation = (EOGenericRecord) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePopulation",
						"cTypePopulation", carriere.cTypePopulation());
				String message = validerTempsPartielPourTypePopulation(typePopulation);
				if (message != null) {
					return message;
				}
			}
			// vérifier si il s'agit d'un comptable car la quotité est >= 80%
			NSArray elements = EOElementCarriere.rechercherElementsCarriereValidesImportCourantPourIndividuEtPeriode(editingContext, individu, dateDebut(),
					myDateFin);
			e = elements.objectEnumerator();
			while (e.hasMoreElements()) {
				EOElementCarriere element = (EOElementCarriere) e.nextElement();
				if (element.cCorps().equals(CORPS_POUR_COMPTABLE) && numQuotite().doubleValue() < 80) {
					return "QUOTITE_TP_COMPTABLE";
				}
			}
		}
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		// Vérifier la carrière dans le SI Destinataire : valider les types de
		// population
		if (individuGrhum != null) {
			carrieres = EOMangueCarriere.rechercherCarrieresDestinValidesSurPeriode(editingContext, individuGrhum, dateDebut(), myDateFin);
			if (carrieres != null && carrieres.count() > 0) {
				aCarriere = true;
				java.util.Enumeration e = carrieres.objectEnumerator();
				while (e.hasMoreElements()) {
					EOMangueCarriere carriere = (EOMangueCarriere) e.nextElement();
					EOGenericRecord typePopulation = (EOGenericRecord) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePopulation",
							"cTypePopulation", carriere.cTypePopulation());
					String message = validerTempsPartielPourTypePopulation(typePopulation);
					if (message != null) {
						return message;
					}
					// vérifier si il s'agit d'un comptable car la quotité est
					// >= 80%
					NSArray elements = EOMangueElementCarriere.rechercherElementsDestinValidesSurPeriode(editingContext, carriere, dateDebut(), myDateFin);
					e = elements.objectEnumerator();
					while (e.hasMoreElements()) {
						EOMangueElementCarriere element = (EOMangueElementCarriere) e.nextElement();
						if (element.cCorps().equals(CORPS_POUR_COMPTABLE) && numQuotite().doubleValue() < 80) {
							return "QUOTITE_TP_COMPTABLE";
						}
					}
				}
			}
		}
		if (!aCarriere) { // pas de carrière. Pour le motif
							// "Fonctionnaire handicapé", il faut être titulaire
			if (motif.estPourHandicape()) {
				return "MOTIF_HANDICAPE_INVALIDE";
			}
		}
		// Vérifier les contrats dans l'import courant : valider les types de
		// contrat
		NSArray contrats = EOContrat.rechercherContratsRemunerationPrincipalePourIndividuEtPeriode(editingContext, individu, dateDebut(), myDateFin, false);
		if (aCarriere && contrats != null && contrats.count() > 0) {
			// pas de temps partiel à cheval sur contrat et carrière
			return "TEMPS_PARTIEL_A_CHEVAL";
		}
		boolean aContrat = false;
		if (contrats != null && contrats.count() > 0) {
			aContrat = true;
			java.util.Enumeration e = contrats.objectEnumerator();
			while (e.hasMoreElements()) {
				EOContrat contrat = (EOContrat) e.nextElement();
				EOTypeContratTravail typeContrat = (EOTypeContratTravail) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeContratTravail",
						"cTypeContratTrav", contrat.cTypeContratTrav());
				String message = validerTypeContrat(typeContrat, motif);
				if (message != null) {
					return message;
				}
			}
		}
		// Vérifier les contrats dans le SI Destinataire : valider les types de
		// contrat
		if (individuGrhum != null) {

			contrats = EOMangueContrat.rechercherContratsPourIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), myDateFin);
			if (aCarriere && contrats != null && contrats.count() > 0) {
				// pas de temps partiel à cheval sur contrat et carrière
				return "TEMPS_PARTIEL_A_CHEVAL";
			}
			if (contrats != null && contrats.count() > 0) {
				aContrat = true;
				java.util.Enumeration e = contrats.objectEnumerator();
				while (e.hasMoreElements()) {
					EOMangueContrat contrat = (EOMangueContrat) e.nextElement();
					EOTypeContratTravail typeContrat = (EOTypeContratTravail) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext,
							"TypeContratTravail", "cTypeContratTrav", contrat.cTypeContratTrav());
					String message = validerTypeContrat(typeContrat, motif);
					if (message != null) {
						return message;
					}
				}
			}
		}
		if (!aCarriere && !aContrat) {
			// On ne peut définir les temps partiels que si il y a des segments
			// de carrière ou des contrats
			return "TEMPS_PARTIEL_INVALIDE";
		}
		if (aContrat) {
			double quotiteTotale = EOContratAvenant.calculerQuotiteTotalePourPeriode(editingContext, individu, null, dateDebut(), myDateFin);
			if (quotiteTotale <= 0) {
				return "TEMPS_PARTIEL_INVALIDE";
			} else if (quotiteTotale < 100) {
				return "TP_INVALIDE_CONTRACTUEL_INCOMPLET";
			}
		}

		return null;
	}

	private String validerTempsPartielPourTypePopulation(EOGenericRecord typePopulation) {
		String hospitalier = (String) typePopulation.valueForKey("temHospitalier");
		String enseignant = (String) typePopulation.valueForKey("temEnseignant");
		if (hospitalier != null && hospitalier.equals(CocktailConstantes.VRAI)) {
			return "TEMPS_PARTIEL_HOSPITALIER_INVALIDE";
		} else if (enseignant != null && enseignant.equals(CocktailConstantes.VRAI)) {
			if (periodicite().intValue() != 12) {
				return "PERIODICITE_TP_ENSEIGNANT";
			}
			/*
			 * 23/09/2011 A modifier quand cela sera fait dans Mangue String premierDegre = (String)typePopulation.valueForKey("tem1Degre");
			 * 
			 * if (premierDegre != null && premierDegre.equals(Constantes.VRAI) && numQuotite().intValue() != 50) { throw new NSValidation.ValidationException(
			 * "La quotité de temps partiel pour un enseignant du premier degré doit être de 50%" ); }
			 */
		}
		return null;
	}

	private String validerTypeContrat(EOTypeContratTravail typeContrat, EOMotifTempsPartiel motif) {
		if (typeContrat.tempsPartielPossible() == false) {
			return "TEMPS_PARTIEL_CONTRACTUEL_INVALIDE";
		} else if (typeContrat.estHospitalier()) {
			return "TEMPS_PARTIEL_HOSPITALIER_INVALIDE";
		} else if (typeContrat.estEnseignant()) {
			if (periodicite().intValue() != 12) {
				return "PERIODICITE_TP_ENSEIGNANT";
			}
			// Vérifier si cela correspond à une année universitaire
			if (dateFin() == null) {
				return "DATES_TP_ENSEIGNANT";
			} else {
				if (motif != null && motif.estPourEleverEnfant() == false && motif.estPourEleverEnfantAdopte() == false) {
					// un temps partiel pour un enseignant (sauf en cas de
					// naissance) doit être
					// calé sur l'année universitaire
					if (DateCtrl.isSameDay(dateDebut(), Outils.debutAnneeUniversitairePourDate(dateDebut())) == false
							|| DateCtrl.isSameDay(dateFin(), Outils.finAnneeUniversitairePourDate(dateDebut())) == false) {
						return "DATES_TP_ENSEIGNANT";
					}
				}
			}
		}
		return null;
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		String motif = (String) valeursAttributs.objectForKey("cMotifTempsPartiel");
		EOGenericRecord motifTp = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "MotifTempsPartiel", "cMotifTempsPartiel", motif);
		if (motifTp == null) {
			resultat = resultat + "Motif_Inconnu >>" + motif + System.getProperty("line.separator");
		}
		int periodicite = ((Integer) valeursAttributs.objectForKey("periodicite")).intValue(); // la périodicité est obligatoire
		if (periodicite < 6 || periodicite > 36) {
			resultat = resultat + "Periodicite_Invalide >>" + periodicite + System.getProperty("line.separator");
		}
		Double numQuotite = ((Double) valeursAttributs.objectForKey("numQuotite")); // la quotité est obligatoire
		Double denQuotite = ((Double) valeursAttributs.objectForKey("denQuotite"));
		// Vérifier que la quotité est bien définie dans la table GRHUM.QUOTITE
		if (quotiteInvalide(editingContext, numQuotite, denQuotite)) {
			resultat = resultat + "Quotite_Indefinie >>" + numQuotite + "/" + denQuotite + System.getProperty("line.separator");
		} else {
			double quotite = numQuotite.doubleValue();
			if (quotite < 50 || quotite > 90) {
				resultat = resultat + "Quotite_Invalide >>" + numQuotite + System.getProperty("line.separator");
			}
		}
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey(DATE_FIN_KEY);
		NSTimestamp dateFinExecution = (NSTimestamp) valeursAttributs.objectForKey(D_FIN_EXECUTION_KEY);
		if (dateFinExecution == null && dateFin != null) {
			valeursAttributs.setObjectForKey(dateFin, D_FIN_EXECUTION_KEY);
		}

		resultat = resultat + verifierTemoin(valeursAttributs, "temGestEtab");
		resultat = resultat + verifierTemoin(valeursAttributs, "temSurcotisation");
		resultat = resultat + verifierTemoin(valeursAttributs, "temConfirme");
		resultat = resultat + verifierTemoin(valeursAttributs, "temRepriseTempsPlein");

		return resultat;
	}

	public static EOTempsPartiel tpPourSourceId(EOEditingContext editingContext, Integer sourceId) {
		return fetchTempsPartiel(editingContext, TP_SOURCE_KEY, sourceId);
	}

	// Méthodes privées statiques
	private static boolean quotiteInvalide(EOEditingContext editingContext, Double numQuotite, Double denQuotite) {
		NSMutableArray args = new NSMutableArray(numQuotite);
		args.addObject(denQuotite);
		EOFetchSpecification fs = new EOFetchSpecification("Quotite", EOQualifier.qualifierWithQualifierFormat("numQuotite = %@ AND denQuotite = %@", args),
				null);
		return editingContext.objectsWithFetchSpecification(fs).count() == 0;
	}
	
	protected boolean estAnnule() {
		return arreteAnnulationSource()!=null || dAnnulation()!=null || noArreteAnnulation()!=null;
	}
}
