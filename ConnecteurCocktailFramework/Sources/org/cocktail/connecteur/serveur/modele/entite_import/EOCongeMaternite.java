//EOCongeMaternite.java
//Created on Sun Feb 23 18:41:29  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;


import java.text.SimpleDateFormat;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeCgMatern;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** R&egrave;gles de validation :<BR>
 * Le type de conge, la date debut et la date fin doivent etre fournis<BR>
 * Verification de dates<BR>
 * <UL>conge de type Etat pathologique du a la grossesse : la date de constat et date de naissance previsionnelle doivent &ecirc;tre fournies</UL>
 * <UL>conge de type Etat pathologique suite a l'accouchement : la date d'accouchement doit &ecirc;tre fournie</UL>
 * <UL>conge de type Maternite : la date de naissance previsionnelle doit &ecirc;tre fournie</UL>
1. Le conge de maternite de type ACCOU et GROSS ne concerne qu'un personnel feminin.<BR>
2. La declaration de maternite ne concerne qu'un personnel feminin. Sa saisie dans ce cas est obligatoire pour pouvoir saisir des conges.<BR>
3. Le conge de type GROSS Etat pathologique du a la grossesse est de 2 semaines et peut &ecirc;tre pris entre la date de constatation et la date presumee de l'accouchement. Il peut &ecirc;tre fractionne.<BR>
4. Le conge de type ACCOU Etat pathologique suite a l'accouchement est de 4 semaines apr&egrave;s le conge de maternite. Il peut &ecirc;tre fractionne.<BR>
Les r&egrave;gles ci-apr&egrave;s concernent le conge de maternite de type MATER<BR>
5. Le conge de maternite peut &ecirc;tre fractionne, en cas d'hospitalisation de l'enfant au-dela de 6 semaines.<BR>
6. Le conge de maternite ne peut &ecirc;tre d'une duree totale inferieure a 8 semaines avant et apr&egrave;s l'accouchement dont 6 semaines apr&egrave;s l'accouchement.<BR>
7. Pour la naissance du 1er ou du 2&egrave;me enfant, le conge de maternite est de 16 semaines maximum, 6 semaines avant la date presumee de l'accouchement et 10 semaines apr&egrave;s.<BR>
8.Il est possible de reporter une partie de la periode prenatale sur la periode postnatale<BR>
9. Pour la naissance du 3&egrave;me enfant et plus, le conge de maternite est de 26 semaines maximum, 8 semaines avant la date presumee de l'accouchement et 18 semaines apr&egrave;s.<BR>
10.Il est possible d'anticiper la periode prenatale, la periode postnatale etant reduite d'autant.<BR>
11. Pour la naissance de jumeaux, le conge de maternite est de 34 semaines, 12 semaines avant la date presumee de l'accouchement et 22 semaines apr&egrave;s.<BR>
12.Il est possible d'anticiper la periode prenatale, la periode postnatale etant reduite d'autant.<BR>
13. Pour la naissance de triples ou plus, le conge de maternite est de 46 semaines maximum, 24 semaines avant la date presumee de l'accouchement et 22 semaines apr&egrave;s.<BR>
14.Il ne devrait pas &ecirc;tre possible de d'anticiper la periode prenatale.<BR>
15. En cas d'accouchement apr&egrave;s la date presumee, la periode prenatale se trouve prolongee et la periode postnatale n'est pas reduite pour autant.<BR>
16. En cas d'accouchement premature, la duree totale du conge de maternite n'est pas modifiee.<BR>
17. En cas de grossesse non menee a terme, le conge de maternite est pour la duree du repos observe.<BR>
18. Le conge de maternite pour un agent masculin, en cas de dec&egrave;s de la m&egrave;re debute apr&egrave;s la date de naissance de l'enfant.<BR>
19.Le conge de maternite pour un agent masculin est de 10 semaines en cas de de naissance d'un seul enfant et si le p&egrave;re a moins de 2 enfants a charge, 
 18 semaines si le p&egrave;re a au moins duex enfants a charge, 22 semaines en cas de naissances multiples.<BR>
20. La case Conge sans traitement ne concerne que les agents non titulaires, les fonctionnaires perçoivent un plein traitement.<BR>
21. Le conge doit reposer soit sur un contrat, soit sur une position d'activite ou de detachement avec gestion de la carri&egrave;re d'accueil 
dans l'etablissement.<BR>
 * @author christine
 *
 */
public class EOCongeMaternite extends _EOCongeMaternite {
	public static int NB_JOURS_NORMAL_FEMME = 112;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_FEMME = 182;
	public static int NB_JOURS_JUMEAUX_FEMME = 238;
	public static int NB_JOURS_TRIPLES_FEMME = 322;
	public static int NB_JOURS_NORMAL_PRENATAL = 42;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_PRENATAL = 56;
	public static int NB_JOURS_JUMEAUX_PRENATAL = 84;
	public static int NB_JOURS_TRIPLES_PRENATAL = 168;
	public static int NB_JOURS_NORMAL_HOMME = 70;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL = 126;
	public static int NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL = 154;

	public EOCongeMaternite() {
		super();
	}

	public String nomRelationPourLog() {
		return EOLogImport.CONGE_MATERNITE_KEY;
	}

	public NSTimestamp dateCommission() {
		return null;
	}
	/** Pour ajouter dans le log la reference sur le eimp2Source */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null) {
			log.setEimp2Source(dmSource());
		}
		return log;
	}
	/** Pour ajouter dans le log la reference sur le eimp2Source */
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log = super.verifierRecordApresOperation();
		if (log != null) {
			log.setEimp2Source(dmSource());
		}
		return log;
	}
	// Méthodes protégées
	protected String verifierConge(EOEditingContext editingContext) {
		EOTypeCgMatern typeCgMatern = (EOTypeCgMatern)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeCgMatern", "cTypeCgMatern", cTypeCgMatern());
		boolean estCongeSansTraitement = temCgSansTrait() != null && temCgSansTrait().equals(CocktailConstantes.VRAI);
		// Le congé sans traitement n'est valable que pour les contractuels => il ne doit pas y avoir de segment de carrière
		if (EOCarriere.fonctionnaireEnActivitePendantPeriode(editingContext,idSource(),dateDebut(),dateFin()) && estCongeSansTraitement) {
			return "CONGE_SS_TT";
		}
		// Vérifier si les informations de déclaration sont cohérentes avec les autres congés pour le même dmSource
		EOIndividu individu = individu();
		if (individu == null) {		// Cas où il s'agit d'une vérification depuis le client
			individu = EOIndividu.individuPourSourceId(editingContext, idSource());
		}
		NSArray congesAssocies = rechercherCongesMaterniteImportCourantPourIndividuEtDeclaration(editingContext,idSource(),dmSource(),null);
		java.util.Enumeration e = congesAssocies.objectEnumerator();
		while (e.hasMoreElements()) {
			EOCongeMaternite conge = (EOCongeMaternite)e.nextElement();
			// on teste sur le cmatSource car si le congé a été modifié sur le client, ils ne sont pas identiques
			if (conge.cmatSource().intValue() != this.cmatSource().intValue() && aMemeDonneeDeclaration(conge) == false) {
				//System.out.println("ERREUR_DECLARATION: noArrete="+noArrete()+" cmatSource="+conge.cmatSource().intValue()+","+this.cmatSource().intValue());
				return "ERREUR_DECLARATION";
			}
		}
		boolean estTypeMaternite = typeCgMatern != null && typeCgMatern.estMaternite();
		if (estTypeMaternite && DateCtrl.isBefore(dateDebut(),dConstatMatern())) {
			return "DATE_DEBUT_CONGE";
		}
		if (individu != null) {
			if (individu.estHomme()) {
				if (!estTypeMaternite) {
					return "TYPE_MATERNITE_HOMME";
				}
				return verifierDureesCongeTypeMaternite(editingContext,false);
			}
		} else {
			LogManager.logDetail("Pas d'individu pour idSource " + idSource());
		}

		// cas d'une femme
		if (dNaisPrev() == null) {
			return "D_ACCOUCHEMENT_PREVIS";
		}
		if (estTypeMaternite) {
			// Vérifier si il existe des congés de type pathologie grossesse pour vérifier les dates
			NSArray congesPathologie = congesDeType(editingContext,EOTypeCgMatern.PATHOLOGIE_GROSSESSE,false);
			if (congesPathologie.count() > 0) {
				e = congesPathologie.objectEnumerator();
				while (e.hasMoreElements()) {
					CongeMaternite conge = (CongeMaternite)e.nextElement();
					if (DateCtrl.isAfterEq(conge.dateDebut(),dateDebut())) {
						return "DATES_CONGE_MATERNITE";
					}
					if (DateCtrl.isAfterEq(conge.dateFin(),dateDebut())) {
						return "DATES_CONGE_MATERNITE";
					}	
				}
			}
			String message = verifierDureesCongeTypeMaternite(editingContext,true);
			return message;
		} else {
			boolean estTypePathologieGrossesse = typeCgMatern != null && typeCgMatern.estPathologieGrossesse();
			boolean estTypePathologieAccouchement = typeCgMatern != null && typeCgMatern.estPathologieAccouchement();
			if (estTypePathologieGrossesse) {
				if (DateCtrl.isBefore(dateDebut(),dConstatMatern())) {
					return "DEBUT_CONGE_PATHOLOGIQUE";
				}
				if (DateCtrl.isAfter(dateFin(),dNaisPrev())) {
					return "FIN_CONGE_PATHOLOGIQUE";
				}	
			} else if (estTypePathologieAccouchement) {
				if (dAccouchement() == null) {
					return "DATE_ACCOUCHEMENT_OBLIGATOIRE";
				}
				NSArray congesMaternite = congesDeType(editingContext,EOTypeCgMatern.MATERNITE,false);
				if (congesMaternite.count() == 0) {
					return "CONGE_MATERNITE_OBLIGATOIRE";
				} else {
					CongeMaternite conge = (CongeMaternite)congesMaternite.objectAtIndex(0);
					if (DateCtrl.isBeforeEq(dateDebut(),conge.dateFin())) {
						return "DATE_DEBUT_CONGE_PATHOLOGIE_ACCOUCH";

					}
				}
			}
			int nbJoursLegaux = 0;
			if (typeCgMatern != null) {
				nbJoursLegaux = typeCgMatern.dureeCgMatern().intValue() * 7;	// la valeur fournie est en semaines
			}
			int nbJours = dureeTotaleCongeMemeType(editingContext,cTypeCgMatern());
			if (nbJours > nbJoursLegaux) {
				return "DUREE_CONGE";
			}
		}    	

		return null;
	}
	// méthodes privées
	private boolean estGrossesseInterrompue() {
		return temAnnule() != null && temAnnule().equals(CocktailConstantes.VRAI);
	}
	private boolean estGrossesseGemellaire() {
		return temGrossesseGemellaire() != null && temGrossesseGemellaire().equals(CocktailConstantes.VRAI);
	}
	private boolean estGrossesseTriple() {
		return temGrossesseTriple() != null && temGrossesseTriple().equals(CocktailConstantes.VRAI);
	}
	private boolean aMemeDonneeDeclaration(EOCongeMaternite conge) {
		if ((dConstatMatern() != null || conge.dConstatMatern() != null) && DateCtrl.isSameDay(dConstatMatern(), conge.dConstatMatern()) == false) {
			return false;
		}	
		if ((dNaisPrev() != null || conge.dNaisPrev() != null) && DateCtrl.isSameDay(dNaisPrev(), conge.dNaisPrev()) == false) {
			return false;
		}
		if ((dAccouchement() != null || conge.dAccouchement() != null) && DateCtrl.isSameDay(dAccouchement(), conge.dAccouchement()) == false) {
			return false;
		}
		if ((nbEnfantsDecl() == null && conge.nbEnfantsDecl() != null) || (nbEnfantsDecl() != null && conge.nbEnfantsDecl() == null) ||
				(nbEnfantsDecl() != null && conge.nbEnfantsDecl() != null && nbEnfantsDecl().intValue() != conge.nbEnfantsDecl().intValue())) {
			return false;
		}
		if (StringCtrl.chainesIdentiques(conge.temGrossesseGemellaire(), temGrossesseGemellaire()) == false) {
			return false;
		}
		if (StringCtrl.chainesIdentiques(conge.temGrossesseTriple(), temGrossesseTriple()) == false) {
			return false;
		}
		if (StringCtrl.chainesIdentiques(conge.temAnnule(), temAnnule()) == false) {
			return false;
		}
		return true;
	}

	// calcule la durée totale des congés de même type */
	private int dureeTotaleCongeMemeType(EOEditingContext editingContext,String typeCgMaternite) {
		int dureeTotale = 0;
		NSArray conges = congesDeType(editingContext,cTypeCgMatern(),true);
		java.util.Enumeration e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			CongeMaternite conge = (CongeMaternite)e.nextElement();
			dureeTotale += DateCtrl.nbJoursEntre(conge.dateDebut(), conge.dateFin(), true);
		}
		return dureeTotale;
	}


	/**
	 * @param editingContext
	 * @param estFemme
	 * @return
	 */
	private String verifierDureesCongeTypeMaternite(EOEditingContext editingContext,boolean estFemme) {
		if (estFemme) {
			if (estGrossesseInterrompue()) {
				// on vérifie que la date de début et de fin sont bien antérieures à la date de naissance prévisionnelle si elle est fournie
				if (dNaisPrev() != null) {
					if (DateCtrl.isAfter(dateDebut(),dNaisPrev())) {
						return "DATE_DEBUT_NAIS_PREV";
					}
					if (DateCtrl.isAfter(dateFin(),dNaisPrev())) {
						return "DATE_FIN_NAIS_PREV";
					}
					// on vérifier qu'il n'y a pas de date d'accouchement
					if (dAccouchement() != null) {
						return "SANS_ACCOUCHEMENT";
					}
				}
				return null;
			}
			if (DateCtrl.isBefore(dateFin(),dNaisPrev())) {
				return "DATE_FIN_MATERNITE";
			}
		} else {
			// date d'accouchement obligatoire pour les hommes
			if (dAccouchement() == null) {
				return "D_ACCOUCHEMENT_HOMME";
			}
		}
		if (dAccouchement() != null && DateCtrl.isBefore(dateFin(),dAccouchement())) {
			return "DATE_ACCOUCHEMENT";
		}
		int nbEnfants = 0;
		if (nbEnfantsDecl() != null) {
			nbEnfants = nbEnfantsDecl().intValue();
		}
		// Retourne la liste des congés de type CongeMaternite d'un certain type triés par ordre de date croissante
		NSArray congesTypeMaternite = congesDeType(editingContext, EOTypeCgMatern.MATERNITE,true);
		if (estFemme) {
			// le congé de maternité peut être fractionné en cas d'hospitalisation de l'enfant. En ce cas,
			// il faut que la date d'accouchement soit fournie pour pouvoir calculer la durée totale postMaternité
			if (congesTypeMaternite.count() > 1 && dAccouchement() == null) {	// il y a plusieurs congés
				return "DATE_ACCOUCHEMENT_OBLIGATOIRE";
			}
		} else {
			// Pour un hommre, un congé non fractionné doit débuter le même jour que la date d'accouchement
			if (congesTypeMaternite.count() == 1 && DateCtrl.isSameDay(dAccouchement(),dateDebut()) == false) {
				return "DEBUT_CONGE_HOMME";
			}
		}

		int nbJoursTotal = calculerDureeTotale(congesTypeMaternite);
		int deltaSupplementaire = 0;
		if (dNaisPrev() != null && dAccouchement() != null && DateCtrl.isBefore(dNaisPrev(),dAccouchement())) {
			deltaSupplementaire = DateCtrl.nbJoursEntre(dNaisPrev(),dAccouchement(),false);
		}
		LogManager.logDetail("CongeMaternite - verifier duree, nb Jours " + nbJoursTotal + " delta " + deltaSupplementaire);
		if (nbEnfants <= 1 && estGrossesseGemellaire() == false && estGrossesseTriple() == false) {
			LogManager.logDetail("Verification jours");

			if (estFemme == false) {
				if (nbJoursTotal != NB_JOURS_NORMAL_HOMME) {
					return "NB_JOURS_NORMAL_HOMME";
				}
			} else {
				if (nbJoursTotal < 56) {	// minimum 8 semaines
					//System.out.println("NB_JOURS_NORMAL_FEMME: noArrete="+noArrete()+", nbJoursTotal="+nbJoursTotal);
					return "NB_JOURS_NORMAL_FEMME";
				}
				if (nbJoursTotal - deltaSupplementaire > NB_JOURS_NORMAL_FEMME) {
					//SimpleDateFormat format=new SimpleDateFormat("dd.MM.yyyy");
					//System.out.println("NB_JOURS_NORMAL_FEMME: noArrete="+noArrete()+", nbJoursTotal="+nbJoursTotal
					//		+", deltaSupplementaire="+deltaSupplementaire+", NB_JOURS_NORMAL_FEMME="+NB_JOURS_NORMAL_FEMME
					//		+", dateDebut="+format.format(dateDebut())+", dateFin="+format.format(dateFin()));
					return "NB_JOURS_NORMAL_FEMME";
				}
			}
		} else {
			int nbJoursPostnatal = calculerDureePostNatale(congesTypeMaternite);
			int dureeTotale = NB_JOURS_2ENFANTS_OU_PLUS_FEMME; 							// 26 semaines
			int dureePostnatale = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL;	// 18 semaines
			String message = "NB_JOURS_2ENFANTS_OU_PLUS";
			if (estGrossesseGemellaire()) {
				dureeTotale = NB_JOURS_JUMEAUX_FEMME;										// 34 semaines
				dureePostnatale = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
				message = "NB_JOURS_JUMEAUX";
			} else if (estGrossesseTriple()) {
				dureeTotale = NB_JOURS_TRIPLES_FEMME;										// 46 semaines
				dureePostnatale = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
				message = "NB_JOURS_TRIPLES";
			} 
			// pour un homme, le congé de maternité a la durée postnatale
			if (estFemme == false) {
				if (nbJoursPostnatal != dureePostnatale) {
					return message + "_HOMME";
				}
			} else {		// pour une femme
				if (nbJoursPostnatal > dureeTotale + deltaSupplementaire) {	
//					SimpleDateFormat format=new SimpleDateFormat("dd.MM.yyyy");
//					System.out.println(message+"_FEMME: noArrete="+noArrete()+", nbJoursPostnatal="+nbJoursPostnatal
//							+", dureeTotale="+dureeTotale+", deltaSupplementaire="+deltaSupplementaire
//							+", dateDebut="+format.format(dateDebut())+", dateFin="+format.format(dateFin()));
					return message + "_FEMME";
				}
			}
		}
		return null;
	}
	// calcule la durée totale de tous les congés maternité de type maternité attachés à la déclaration de maternité
	private int calculerDureeTotale(NSArray conges) {
		int nbJours = 0;
		java.util.Enumeration e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			CongeMaternite conge = (CongeMaternite)e.nextElement();
			nbJours += DateCtrl.nbJoursEntre(conge.dateDebut(),conge.dateFin(),true);
		}
		return nbJours;
	}
	/*	 calcule la durée postnatale de tous les congés maternité passés en paramètre
	 * @param conges
	 * @param estPrenatale
	 * @return
	 */
	private int calculerDureePostNatale(NSArray conges) {
		if (conges.count() == 1 && dAccouchement() == null) {
			return DateCtrl.nbJoursEntre(dNaisPrev(),dateFin(),true);
		}
		// plusieurs congés (fractionnement pour enfant malade, la date d'accouchement doit être fournie). Il n'y en a qu'un
		// qui commence avant la date de naissance prévisionnelle
		int nbJours = 0;
		java.util.Enumeration e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			CongeMaternite conge = (CongeMaternite)e.nextElement();
			if (DateCtrl.isBefore(conge.dateDebut(),conge.dAccouchement())) {
				// congé qui comportait la durée prénatale
				nbJours += DateCtrl.nbJoursEntre(conge.dAccouchement(),conge.dateFin(),true);
			} else {
				nbJours += DateCtrl.nbJoursEntre(conge.dateDebut(),conge.dateFin(),true);
			}
		}
		return nbJours;
	}
	// retourne les congés du type passé en paramètre, associé à une même déclaration de l'import et de la destination sous la forme de CongeMaternite
	private NSArray congesDeType(EOEditingContext editingContext,String typeConge,boolean ajouterConge) {
		NSMutableArray congesAssocies = new NSMutableArray();
		NSMutableArray congesEnUpdate = new NSMutableArray();	// tableau des congesId en update/correspondance. Utilisé pour ne pas prendre en compte ceux du SI Destinataire 
		// On recherche d'abord les congés de l'import courant
		NSArray conges = rechercherCongesMaterniteImportCourantPourIndividuEtDeclaration(editingContext, idSource(), dmSource(),typeConge);
		// Les congés sont gardés comme des CongeMaternite (classe interne) afin de pouvoir trier et comparer les congés de l'import courant et du SI Destinataire
		java.util.Enumeration e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			EOCongeMaternite conge = (EOCongeMaternite)e.nextElement();
			// on teste sur l'eimp source car si le congé a été modifié sur le client, conge et this ne sont pas identiques
			if (conge.cmatSource().intValue() != this.cmatSource().intValue()) {
				congesAssocies.addObject(new CongeMaternite(conge));
				if (conge.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || conge.operation().equals(ObjetImport.OPERATION_UPDATE)) {
					congesEnUpdate.addObject(conge.cmatSource());
				}
			}
		}
		// on prend la valeur réelle du congé car il a pu être modifié sur le poste client
		if (ajouterConge) {
			congesAssocies.addObject(new CongeMaternite(this));
			if (this.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || this.operation().equals(ObjetImport.OPERATION_UPDATE)) {
				congesEnUpdate.addObject(this.cmatSource());
			}
		}
		NSTimestamp dNaissancePrevisionnelle = dNaisPrev();
		if (this.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || this.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			// Rechercher le congé dans le SI Destinataire pour récupérer l'ancienne date de déclaration et ainsi retrouver les autres congés associés
			EOMangueCongeMaternite congeDestin = (EOMangueCongeMaternite)objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (congeDestin != null) {
				dNaissancePrevisionnelle = congeDestin.declarationMaternite().dNaisPrev();
			}
		}
		// rechercher les congés liés à la même date de naissance prévisionnelle du SI Destinataire
		conges = rechercherCongesMaterniteDestinationPourIndividu(editingContext, idSource(), dNaissancePrevisionnelle,typeConge);
		e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			// il ne peut y en avoir qu'un, pas de superposition des congés
			EOMangueCongeMaternite conge = (EOMangueCongeMaternite)e.nextElement();
			EOCongeMaternite congeImport = (EOCongeMaternite)ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext, conge, entityName());
			// congeImport = nul, il s'agit d'un congé uniquement défini dans Mangue
			if (congeImport == null || congesEnUpdate.containsObject(congeImport.cmatSource()) == false) {
				congesAssocies.addObject(new CongeMaternite(conge));
			}
		}
		// On les trie par date croissante
		EOSortOrdering.sortArrayUsingKeyOrderArray(congesAssocies, new NSArray(EOSortOrdering.sortOrderingWithKey("dateFin",EOSortOrdering.CompareAscending)));
		return congesAssocies;
	}
	// Retourne tous les congés de maternité d'un invididu dans l'import courant qui ont le même dmSource
	private NSArray rechercherCongesMaterniteImportCourantPourIndividuEtDeclaration(EOEditingContext editingContext, Number idSource,Number dmSource,String typeCongeMaternite) {
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(dmSource);
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(ID_SOURCE_KEY+" = %@ AND "+DM_SOURCE_KEY+" = %@ AND "+STATUT_KEY+" <> 'A' AND "+STATUT_KEY+" <> 'E'",args));
		if (typeCongeMaternite != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_CG_MATERN_KEY+" = %@ ", new NSArray(typeCongeMaternite)));
		}
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_IMPORT_KEY+" = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	// Retourne les congés de maternité valides dans la destination qui correspondent à la même déclaration
	private NSArray rechercherCongesMaterniteDestinationPourIndividu(EOEditingContext editingContext,Number idSource, NSTimestamp dNaissancePrevisionnelle,String typeCongeMaternite) {
		//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext,idSource());
		if (individu != null) {
			// On recherche les congés valides
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individuGrhum)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_CG_MATERN_KEY + " = %@",new NSArray(typeCongeMaternite)));
				if (dNaissancePrevisionnelle != null) {
					NSMutableArray args = new NSMutableArray(DateCtrl.jourPrecedent(dNaissancePrevisionnelle));
					args.addObject(DateCtrl.jourSuivant(dNaissancePrevisionnelle));
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("declarationMaternite.dNaisPrev > %@ AND declarationMaternite.dNaisPrev < %@ AND (declarationMaternite.temAnnuleBis = nil OR declarationMaternite.temAnnuleBis = 'N')",args));
				}
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("noArreteAnnulation = nil AND dAnnulation = nil AND toCongeAnnulation = nil", null));
				EOFetchSpecification myFetch = new EOFetchSpecification (nomEntiteDestination(), new EOAndQualifier(qualifiers),null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			} else {
				return new NSArray();
			}
		} else {
			return new NSArray();
		}

	}
	// méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
		String valeur = (String)valeursAttributs.objectForKey("cTypeCgMatern");
		// Vérifier dans la nomenclature si ce type de congé de maternité existe
		EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeCgMatern", "cTypeCgMatern", valeur);
		if (record == null) {
			resultat = resultat + "Type_Conge_Matern_Inconnu >>" + valeur + System.getProperty("line.separator");
		}
		NSTimestamp dNaisPrev = (NSTimestamp)valeursAttributs.objectForKey("dNaisPrev");
		NSTimestamp dConstatMatern = (NSTimestamp)valeursAttributs.objectForKey("dConstatMatern");
		if (dNaisPrev != null && DateCtrl.isBefore(dNaisPrev,dConstatMatern)) {
			resultat = "Date_Naissance_Prev >>" + System.getProperty("line.separator"); 
		}
		NSTimestamp dAccouchement = (NSTimestamp)valeursAttributs.objectForKey("dAccouchement");
		if (dAccouchement != null && DateCtrl.isBefore(dAccouchement,dConstatMatern)) {
			resultat = "Date_Accouchement >>" + System.getProperty("line.separator"); 
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "temAnnule");
		resultat = resultat + verifierTemoin(valeursAttributs, "temCgSansTrait");
		resultat = resultat + verifierTemoin(valeursAttributs, "temGrossesseGemellaire");
		resultat = resultat + verifierTemoin(valeursAttributs, "temGrossesseTriple");

		return resultat;
	}

	public static class CongeMaternite {
		private NSTimestamp dateDebut;
		private NSTimestamp dateFin;
		private NSTimestamp dateNaissancePrevisionnelle;
		private NSTimestamp dateAccouchement;

		public CongeMaternite(EOCongeMaternite conge) {
			this.dateDebut = conge.dateDebut();
			this.dateFin = conge.dateFin();
			this.dateNaissancePrevisionnelle = conge.dNaisPrev();
			this.dateAccouchement = conge.dAccouchement();
		}
		public CongeMaternite(EOMangueCongeMaternite conge) {
			this.dateDebut = conge.dateDebut();
			this.dateFin = conge.dateFin();
			this.dateNaissancePrevisionnelle = conge.declarationMaternite().dNaisPrev();
		}
		public NSTimestamp dateDebut() {
			return dateDebut;
		}
		public NSTimestamp dateFin() {
			return dateFin;
		}
		public NSTimestamp dateNaissancePrevisionnelle() {
			return dateNaissancePrevisionnelle;
		}
		public NSTimestamp dAccouchement() {
			return dateAccouchement;
		}
		public String toString() {
			String tmp = "";
			if (dateFin() != null) {
				tmp = DateCtrl.dateToString(dateFin());
			}
			return "[" + DateCtrl.dateToString(dateDebut()) + "," + tmp + "]";
		}
	}

	@Override
	public Integer eimpSource() {
		return cmatSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCmatSource(value);
	}

	@Override
	public String sourceKey() {
		return CMAT_SOURCE_KEY;
	}

	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCongeMaternite)conge);
	}
}
