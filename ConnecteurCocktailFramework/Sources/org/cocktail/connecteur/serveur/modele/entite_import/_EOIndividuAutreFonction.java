// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuAutreFonction.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuAutreFonction extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "IndividuAutreFonction";

	// Attributes
	public static final String C_FONCTION_KEY = "cFonction";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String IAF_SOURCE_KEY = "iafSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOIndividuAutreFonction.class);

  public EOIndividuAutreFonction localInstanceIn(EOEditingContext editingContext) {
    EOIndividuAutreFonction localInstance = (EOIndividuAutreFonction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cFonction() {
    return (Integer) storedValueForKey("cFonction");
  }

  public void setCFonction(Integer value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating cFonction from " + cFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "cFonction");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer iafSource() {
    return (Integer) storedValueForKey("iafSource");
  }

  public void setIafSource(Integer value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating iafSource from " + iafSource() + " to " + value);
    }
    takeStoredValueForKey(value, "iafSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonction.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOIndividuAutreFonction.LOG.isDebugEnabled()) {
      _EOIndividuAutreFonction.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOIndividuAutreFonction createIndividuAutreFonction(EOEditingContext editingContext, Integer cFonction
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer iafSource
, Integer idSource
, String operation
, String statut
, String temImport
) {
    EOIndividuAutreFonction eo = (EOIndividuAutreFonction) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuAutreFonction.ENTITY_NAME);    
		eo.setCFonction(cFonction);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIafSource(iafSource);
		eo.setIdSource(idSource);
		eo.setOperation(operation);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOIndividuAutreFonction> fetchAllIndividuAutreFonctions(EOEditingContext editingContext) {
    return _EOIndividuAutreFonction.fetchAllIndividuAutreFonctions(editingContext, null);
  }

  public static NSArray<EOIndividuAutreFonction> fetchAllIndividuAutreFonctions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuAutreFonction.fetchIndividuAutreFonctions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuAutreFonction> fetchIndividuAutreFonctions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuAutreFonction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuAutreFonction> eoObjects = (NSArray<EOIndividuAutreFonction>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuAutreFonction fetchIndividuAutreFonction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuAutreFonction.fetchIndividuAutreFonction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuAutreFonction fetchIndividuAutreFonction(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuAutreFonction> eoObjects = _EOIndividuAutreFonction.fetchIndividuAutreFonctions(editingContext, qualifier, null);
    EOIndividuAutreFonction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuAutreFonction)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuAutreFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuAutreFonction fetchRequiredIndividuAutreFonction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuAutreFonction.fetchRequiredIndividuAutreFonction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuAutreFonction fetchRequiredIndividuAutreFonction(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuAutreFonction eoObject = _EOIndividuAutreFonction.fetchIndividuAutreFonction(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuAutreFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuAutreFonction localInstanceIn(EOEditingContext editingContext, EOIndividuAutreFonction eo) {
    EOIndividuAutreFonction localInstance = (eo == null) ? null : (EOIndividuAutreFonction)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
