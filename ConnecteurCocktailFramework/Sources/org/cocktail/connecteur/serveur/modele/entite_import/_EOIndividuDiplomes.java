// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuDiplomes.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuDiplomes extends ObjetImportPourIndividu {
	public static final String ENTITY_NAME = "IndividuDiplomes";

	// Attributes
	public static final String C_DIPLOME_KEY = "cDiplome";
	public static final String C_TITULAIRE_DIPLOME_KEY = "cTitulaireDiplome";
	public static final String C_UAI_OBTENTION_KEY = "cUaiObtention";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DIPLOME_KEY = "dDiplome";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EIMP_SOURCE_KEY = "eimpSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LIEU_DIPLOME_KEY = "lieuDiplome";
	public static final String OPERATION_KEY = "operation";
	public static final String SPECIALITE_KEY = "specialite";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOIndividuDiplomes.class);

  public EOIndividuDiplomes localInstanceIn(EOEditingContext editingContext) {
    EOIndividuDiplomes localInstance = (EOIndividuDiplomes)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDiplome() {
    return (String) storedValueForKey("cDiplome");
  }

  public void setCDiplome(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating cDiplome from " + cDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiplome");
  }

  public String cTitulaireDiplome() {
    return (String) storedValueForKey("cTitulaireDiplome");
  }

  public void setCTitulaireDiplome(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating cTitulaireDiplome from " + cTitulaireDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "cTitulaireDiplome");
  }

  public String cUaiObtention() {
    return (String) storedValueForKey("cUaiObtention");
  }

  public void setCUaiObtention(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating cUaiObtention from " + cUaiObtention() + " to " + value);
    }
    takeStoredValueForKey(value, "cUaiObtention");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDiplome() {
    return (NSTimestamp) storedValueForKey("dDiplome");
  }

  public void setDDiplome(NSTimestamp value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating dDiplome from " + dDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "dDiplome");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer eimpSource() {
    return (Integer) storedValueForKey("eimpSource");
  }

  public void setEimpSource(Integer value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating eimpSource from " + eimpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "eimpSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String lieuDiplome() {
    return (String) storedValueForKey("lieuDiplome");
  }

  public void setLieuDiplome(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating lieuDiplome from " + lieuDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDiplome");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String specialite() {
    return (String) storedValueForKey("specialite");
  }

  public void setSpecialite(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating specialite from " + specialite() + " to " + value);
    }
    takeStoredValueForKey(value, "specialite");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomes.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOIndividuDiplomes.LOG.isDebugEnabled()) {
      _EOIndividuDiplomes.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOIndividuDiplomes createIndividuDiplomes(EOEditingContext editingContext, String cDiplome
, String cTitulaireDiplome
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EOIndividuDiplomes eo = (EOIndividuDiplomes) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuDiplomes.ENTITY_NAME);    
		eo.setCDiplome(cDiplome);
		eo.setCTitulaireDiplome(cTitulaireDiplome);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOIndividuDiplomes> fetchAllIndividuDiplomeses(EOEditingContext editingContext) {
    return _EOIndividuDiplomes.fetchAllIndividuDiplomeses(editingContext, null);
  }

  public static NSArray<EOIndividuDiplomes> fetchAllIndividuDiplomeses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuDiplomes.fetchIndividuDiplomeses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuDiplomes> fetchIndividuDiplomeses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuDiplomes.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuDiplomes> eoObjects = (NSArray<EOIndividuDiplomes>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuDiplomes fetchIndividuDiplomes(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDiplomes.fetchIndividuDiplomes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDiplomes fetchIndividuDiplomes(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuDiplomes> eoObjects = _EOIndividuDiplomes.fetchIndividuDiplomeses(editingContext, qualifier, null);
    EOIndividuDiplomes eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuDiplomes)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuDiplomes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDiplomes fetchRequiredIndividuDiplomes(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDiplomes.fetchRequiredIndividuDiplomes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDiplomes fetchRequiredIndividuDiplomes(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuDiplomes eoObject = _EOIndividuDiplomes.fetchIndividuDiplomes(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuDiplomes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDiplomes localInstanceIn(EOEditingContext editingContext, EOIndividuDiplomes eo) {
    EOIndividuDiplomes localInstance = (eo == null) ? null : (EOIndividuDiplomes)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
