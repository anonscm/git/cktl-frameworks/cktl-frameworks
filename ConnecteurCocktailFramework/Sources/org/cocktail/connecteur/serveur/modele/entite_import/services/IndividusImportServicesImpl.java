package org.cocktail.connecteur.serveur.modele.entite_import.services;

import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IIndividuImport;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

public class IndividusImportServicesImpl {

	/** Determine le statut et l'operation pour le personnel associe en verifiant si un record 
	 * similaire a deja ete importe => update, existe deja dans le SI => il manque une correspondance.
	 * Sinon c'est une insertion, verifie si il pourrait y avoir des homonymes dans le SI.<BR>
	 * Verifier si le personnel est OK
	 */
	public EOLogImport verifierRecordApresOperation(IIndividuImport individu) {
		//TODO Homonymes
		if (individu.getPersonnel() == null) {
			return null;
		}

		if (estUnUpdate(individu)) {
			individu.getPersonnel().setOperation(ObjetImport.OPERATION_UPDATE);
		}
		if (estUneCorrespondanceSimple(individu)) {
			individu.getPersonnel().setOperation(ObjetImport.OPERATION_CORRESPONDANCE);
		}
		if (estUneInsertion(individu)) {
			individu.getPersonnel().setOperation(ObjetImport.OPERATION_INSERTION);
			if (estUnHomonyme(individu)) {
				individu.getPersonnel().setStatut(ObjetImport.STATUT_HOMONYME);
			}
		}

		// Partie commune
		if (individu.getPersonnel().statut().equals(ObjetImport.STATUT_HOMONYME) == false
				&& individu.getPersonnel().statut().equals(ObjetImport.STATUT_TRONQUE) == false) {
			individu.getPersonnel().setStatut(ObjetImport.STATUT_VALIDE);
		}
		EOLogImport log = individu.getPersonnel().verifierRecordApresOperation();
		if (log != null) {
			individu.setStatut(ObjetImport.STATUT_ERREUR);
		}
		return log;
	}

	private boolean estUnHomonyme(IIndividuImport individu) {
		return individu.statut().equals(ObjetImport.STATUT_HOMONYME);
	}

	private boolean estUneInsertion(IIndividuImport individu) {
		return individu.getPersonnel().correspondance() == null
				&& (!individu.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE)
						|| !individu.getPersonnel().dejaDansSIDestinataire());
	}

	private boolean estUneCorrespondanceSimple(IIndividuImport individu) {
		return individu.getPersonnel().correspondance() == null
				&& individu.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE)
				&& individu.getPersonnel().dejaDansSIDestinataire();
	}

	private boolean estUnUpdate(IIndividuImport individu) {
		return individu.getPersonnel().correspondance() != null;
	}
}	
//		//TODO Homonymes
//		if (individu.getPersonnel() != null) {
//			if (individu.getPersonnel().correspondance() != null) {
//				individu.getPersonnel().setOperation(ObjetImport.OPERATION_UPDATE);
//			} else if (individu.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) && individu.getPersonnel().dejaDansSIDestinataire()) {
//				individu.getPersonnel().setOperation(ObjetImport.OPERATION_CORRESPONDANCE);
//			} else {
//				individu.getPersonnel().setOperation(ObjetImport.OPERATION_INSERTION);
//				if (individu.statut().equals(ObjetImport.STATUT_HOMONYME)) {		
//					individu.getPersonnel().setStatut(ObjetImport.STATUT_HOMONYME);
//				}
//			}
//			if (individu.getPersonnel().statut().equals(ObjetImport.STATUT_HOMONYME) == false && individu.getPersonnel().statut().equals(ObjetImport.STATUT_TRONQUE) == false) {
//				individu.getPersonnel().setStatut(ObjetImport.STATUT_VALIDE);
//			}
//			EOLogImport log = individu.getPersonnel().verifierRecordApresOperation();
//			if (log != null) {
//				individu.setStatut(ObjetImport.STATUT_ERREUR);
//			}
//			return log;
//		}
//		return null;
//	}
//
//}
