// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPasse.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPasse extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Passe";

	// Attributes
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_TYPE_POPULATION_KEY = "cTypePopulation";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_VALIDEE_ANNEES_KEY = "dureeValideeAnnees";
	public static final String DUREE_VALIDEE_JOURS_KEY = "dureeValideeJours";
	public static final String DUREE_VALIDEE_MOIS_KEY = "dureeValideeMois";
	public static final String D_VALIDATION_SERVICE_KEY = "dValidationService";
	public static final String ETABLISSEMENT_PASSE_KEY = "etablissementPasse";
	public static final String FONCTION_PASSE_KEY = "fonctionPasse";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String PAS_MINISTERE_KEY = "pasMinistere";
	public static final String PAS_PC_ACQUITEE_KEY = "pasPcAcquitee";
	public static final String PAS_QUOTITE_KEY = "pasQuotite";
	public static final String PAS_QUOTITE_COTISATION_KEY = "pasQuotiteCotisation";
	public static final String PAS_SOURCE_KEY = "pasSource";
	public static final String PAS_TYPE_FCT_PUBLIQUE_KEY = "pasTypeFctPublique";
	public static final String PAS_TYPE_SERVICE_KEY = "pasTypeService";
	public static final String PAS_TYPE_TEMPS_KEY = "pasTypeTemps";
	public static final String SECTEUR_PASSE_KEY = "secteurPasse";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOPasse.class);

  public EOPasse localInstanceIn(EOEditingContext editingContext) {
    EOPasse localInstance = (EOPasse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public String cTypePopulation() {
    return (String) storedValueForKey("cTypePopulation");
  }

  public void setCTypePopulation(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating cTypePopulation from " + cTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypePopulation");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dureeValideeAnnees() {
    return (Integer) storedValueForKey("dureeValideeAnnees");
  }

  public void setDureeValideeAnnees(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dureeValideeAnnees from " + dureeValideeAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeAnnees");
  }

  public Integer dureeValideeJours() {
    return (Integer) storedValueForKey("dureeValideeJours");
  }

  public void setDureeValideeJours(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dureeValideeJours from " + dureeValideeJours() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeJours");
  }

  public Integer dureeValideeMois() {
    return (Integer) storedValueForKey("dureeValideeMois");
  }

  public void setDureeValideeMois(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dureeValideeMois from " + dureeValideeMois() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeMois");
  }

  public NSTimestamp dValidationService() {
    return (NSTimestamp) storedValueForKey("dValidationService");
  }

  public void setDValidationService(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dValidationService from " + dValidationService() + " to " + value);
    }
    takeStoredValueForKey(value, "dValidationService");
  }

  public String etablissementPasse() {
    return (String) storedValueForKey("etablissementPasse");
  }

  public void setEtablissementPasse(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating etablissementPasse from " + etablissementPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "etablissementPasse");
  }

  public String fonctionPasse() {
    return (String) storedValueForKey("fonctionPasse");
  }

  public void setFonctionPasse(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating fonctionPasse from " + fonctionPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionPasse");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String pasMinistere() {
    return (String) storedValueForKey("pasMinistere");
  }

  public void setPasMinistere(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasMinistere from " + pasMinistere() + " to " + value);
    }
    takeStoredValueForKey(value, "pasMinistere");
  }

  public String pasPcAcquitee() {
    return (String) storedValueForKey("pasPcAcquitee");
  }

  public void setPasPcAcquitee(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasPcAcquitee from " + pasPcAcquitee() + " to " + value);
    }
    takeStoredValueForKey(value, "pasPcAcquitee");
  }

  public Integer pasQuotite() {
    return (Integer) storedValueForKey("pasQuotite");
  }

  public void setPasQuotite(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasQuotite from " + pasQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "pasQuotite");
  }

  public Integer pasQuotiteCotisation() {
    return (Integer) storedValueForKey("pasQuotiteCotisation");
  }

  public void setPasQuotiteCotisation(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasQuotiteCotisation from " + pasQuotiteCotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "pasQuotiteCotisation");
  }

  public Integer pasSource() {
    return (Integer) storedValueForKey("pasSource");
  }

  public void setPasSource(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasSource from " + pasSource() + " to " + value);
    }
    takeStoredValueForKey(value, "pasSource");
  }

  public String pasTypeFctPublique() {
    return (String) storedValueForKey("pasTypeFctPublique");
  }

  public void setPasTypeFctPublique(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasTypeFctPublique from " + pasTypeFctPublique() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeFctPublique");
  }

  public String pasTypeService() {
    return (String) storedValueForKey("pasTypeService");
  }

  public void setPasTypeService(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasTypeService from " + pasTypeService() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeService");
  }

  public String pasTypeTemps() {
    return (String) storedValueForKey("pasTypeTemps");
  }

  public void setPasTypeTemps(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasTypeTemps from " + pasTypeTemps() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeTemps");
  }

  public String secteurPasse() {
    return (String) storedValueForKey("secteurPasse");
  }

  public void setSecteurPasse(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating secteurPasse from " + secteurPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "secteurPasse");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
      _EOPasse.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOPasse createPasse(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, Integer pasSource
, String secteurPasse
, String statut
, String temImport
, String temTitulaire
) {
    EOPasse eo = (EOPasse) EOUtilities.createAndInsertInstance(editingContext, _EOPasse.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setPasSource(pasSource);
		eo.setSecteurPasse(secteurPasse);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemTitulaire(temTitulaire);
    return eo;
  }

  public static NSArray<EOPasse> fetchAllPasses(EOEditingContext editingContext) {
    return _EOPasse.fetchAllPasses(editingContext, null);
  }

  public static NSArray<EOPasse> fetchAllPasses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPasse.fetchPasses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPasse> fetchPasses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPasse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPasse> eoObjects = (NSArray<EOPasse>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPasse fetchPasse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPasse.fetchPasse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPasse fetchPasse(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPasse> eoObjects = _EOPasse.fetchPasses(editingContext, qualifier, null);
    EOPasse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPasse)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Passe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPasse fetchRequiredPasse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPasse.fetchRequiredPasse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPasse fetchRequiredPasse(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPasse eoObject = _EOPasse.fetchPasse(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Passe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPasse localInstanceIn(EOEditingContext editingContext, EOPasse eo) {
    EOPasse localInstance = (eo == null) ? null : (EOPasse)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
