//EOPeriodesMilitaires.java
//Created on Wed Oct 12 14:23:44 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;


import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePeriodesMilitaires;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Verifie que le type de periode est declare et que les periodes ne se chevauchent pas
 * @author christine
 *
 */
public class EOPeriodesMilitaires extends _EOPeriodesMilitaires {

	public EOPeriodesMilitaires() {
		super();
	}


	// méthodes ajoutées
	/** Les periodes ne pouvant se chevaucher, le destinataire equivalent est le record du SI Destinataire
	 * valide sur la periode
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null) {
			if (estNouvelIndividu()) {
				return null;
			} else {
				return EOManguePeriodesMilitaires.periodeDestinationPourPeriode(editingContextTravail(), this);
			}
		} else {
			return null;
		}	
	}

	public String nomRelationPourLog() {
		return EOLogImport.PERIODES_MILITAIRES_KEY;
	}

	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOManguePeriodesMilitaires.rechercherPeriodesPourIndividuEtDates(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);	
		String type = (String)valeursAttributs.objectForKey("cPeriodeMilitaire");
		EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePeriodeMilit","cPeriodeMilitaire", type);
		if (record == null) {
			resultat += "Type_Periode_Inconnu >>" + type + System.getProperty("line.separator");
		}
		
		return resultat;
	}


	
	
}
