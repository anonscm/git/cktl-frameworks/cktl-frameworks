// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVacataires.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVacataires extends ObjetImportPourIndividuOuStructure {
	public static final String ENTITY_NAME = "Vacataires";

	// Attributes
	public static final String ADR_SOURCE_KEY = "adrSource";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String C_UAI_KEY = "cUai";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VACATION_KEY = "dDebVacation";
	public static final String D_FIN_VACATION_KEY = "dFinVacation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ENSEIGNEMENT_KEY = "enseignement";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NBR_HEURES_KEY = "nbrHeures";
	public static final String NBR_HEURES_REALISEES_KEY = "nbrHeuresRealisees";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String OPERATION_KEY = "operation";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String TAUX_HORAIRE_REALISE_KEY = "tauxHoraireRealise";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TEM_PERS_ETAB_KEY = "temPersEtab";
	public static final String TEM_SIGNE_KEY = "temSigne";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String VAC_SOURCE_KEY = "vacSource";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_PROFESSION_KEY = "toProfession";
	public static final String TO_TYPE_CONTRAT_TRAVAIL_KEY = "toTypeContratTravail";
	public static final String TO_UAI_KEY = "toUai";

  private static Logger LOG = Logger.getLogger(_EOVacataires.class);

  public EOVacataires localInstanceIn(EOEditingContext editingContext) {
    EOVacataires localInstance = (EOVacataires)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer adrSource() {
    return (Integer) storedValueForKey("adrSource");
  }

  public void setAdrSource(Integer value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating adrSource from " + adrSource() + " to " + value);
    }
    takeStoredValueForKey(value, "adrSource");
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String cSectionCnu() {
    return (String) storedValueForKey("cSectionCnu");
  }

  public void setCSectionCnu(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating cSectionCnu from " + cSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSectionCnu");
  }

  public String cSousSectionCnu() {
    return (String) storedValueForKey("cSousSectionCnu");
  }

  public void setCSousSectionCnu(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating cSousSectionCnu from " + cSousSectionCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "cSousSectionCnu");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public String cUai() {
    return (String) storedValueForKey("cUai");
  }

  public void setCUai(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating cUai from " + cUai() + " to " + value);
    }
    takeStoredValueForKey(value, "cUai");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVacation() {
    return (NSTimestamp) storedValueForKey("dDebVacation");
  }

  public void setDDebVacation(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dDebVacation from " + dDebVacation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVacation");
  }

  public NSTimestamp dFinVacation() {
    return (NSTimestamp) storedValueForKey("dFinVacation");
  }

  public void setDFinVacation(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dFinVacation from " + dFinVacation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVacation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String enseignement() {
    return (String) storedValueForKey("enseignement");
  }

  public void setEnseignement(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating enseignement from " + enseignement() + " to " + value);
    }
    takeStoredValueForKey(value, "enseignement");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public java.math.BigDecimal nbrHeures() {
    return (java.math.BigDecimal) storedValueForKey("nbrHeures");
  }

  public void setNbrHeures(java.math.BigDecimal value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating nbrHeures from " + nbrHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeures");
  }

  public java.math.BigDecimal nbrHeuresRealisees() {
    return (java.math.BigDecimal) storedValueForKey("nbrHeuresRealisees");
  }

  public void setNbrHeuresRealisees(java.math.BigDecimal value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating nbrHeuresRealisees from " + nbrHeuresRealisees() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeuresRealisees");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey("noCnu");
  }

  public void setNoCnu(Integer value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "noCnu");
  }

  public String observations() {
    return (String) storedValueForKey("observations");
  }

  public void setObservations(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating observations from " + observations() + " to " + value);
    }
    takeStoredValueForKey(value, "observations");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String proCode() {
    return (String) storedValueForKey("proCode");
  }

  public void setProCode(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating proCode from " + proCode() + " to " + value);
    }
    takeStoredValueForKey(value, "proCode");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public java.math.BigDecimal tauxHoraire() {
    return (java.math.BigDecimal) storedValueForKey("tauxHoraire");
  }

  public void setTauxHoraire(java.math.BigDecimal value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating tauxHoraire from " + tauxHoraire() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraire");
  }

  public java.math.BigDecimal tauxHoraireRealise() {
    return (java.math.BigDecimal) storedValueForKey("tauxHoraireRealise");
  }

  public void setTauxHoraireRealise(java.math.BigDecimal value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating tauxHoraireRealise from " + tauxHoraireRealise() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraireRealise");
  }

  public String temEnseignant() {
    return (String) storedValueForKey("temEnseignant");
  }

  public void setTemEnseignant(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temEnseignant from " + temEnseignant() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnseignant");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey("temPaiementPonctuel");
  }

  public void setTemPaiementPonctuel(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temPaiementPonctuel from " + temPaiementPonctuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temPaiementPonctuel");
  }

  public String temPersEtab() {
    return (String) storedValueForKey("temPersEtab");
  }

  public void setTemPersEtab(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temPersEtab from " + temPersEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temPersEtab");
  }

  public String temSigne() {
    return (String) storedValueForKey("temSigne");
  }

  public void setTemSigne(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temSigne from " + temSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temSigne");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public Integer vacSource() {
    return (Integer) storedValueForKey("vacSource");
  }

  public void setVacSource(Integer value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating vacSource from " + vacSource() + " to " + value);
    }
    takeStoredValueForKey(value, "vacSource");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse toAdresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse)storedValueForKey("toAdresse");
  }

  public void setToAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toAdresse from " + toAdresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAdresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAdresse");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu toCnu() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps toCorps() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps)storedValueForKey("toCorps");
  }

  public void setToCorpsRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toCorps from " + toCorps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCorps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCorps");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade toGrade() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade)storedValueForKey("toGrade");
  }

  public void setToGradeRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toGrade from " + toGrade() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrade");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrade");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession toProfession() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession)storedValueForKey("toProfession");
  }

  public void setToProfessionRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toProfession from " + toProfession() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession oldValue = toProfession();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toProfession");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toProfession");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail toTypeContratTravail() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail)storedValueForKey("toTypeContratTravail");
  }

  public void setToTypeContratTravailRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toTypeContratTravail from " + toTypeContratTravail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail oldValue = toTypeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeContratTravail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeContratTravail");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne toUai() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne)storedValueForKey("toUai");
  }

  public void setToUaiRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toUai from " + toUai() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne oldValue = toUai();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toUai");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toUai");
    }
  }
  

  public static EOVacataires createVacataires(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebVacation
, NSTimestamp dFinVacation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temEnseignant
, String temImport
, Integer vacSource
) {
    EOVacataires eo = (EOVacataires) EOUtilities.createAndInsertInstance(editingContext, _EOVacataires.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebVacation(dDebVacation);
		eo.setDFinVacation(dFinVacation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemEnseignant(temEnseignant);
		eo.setTemImport(temImport);
		eo.setVacSource(vacSource);
    return eo;
  }

  public static NSArray<EOVacataires> fetchAllVacataireses(EOEditingContext editingContext) {
    return _EOVacataires.fetchAllVacataireses(editingContext, null);
  }

  public static NSArray<EOVacataires> fetchAllVacataireses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVacataires.fetchVacataireses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVacataires> fetchVacataireses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVacataires.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVacataires> eoObjects = (NSArray<EOVacataires>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVacataires fetchVacataires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataires.fetchVacataires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacataires fetchVacataires(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVacataires> eoObjects = _EOVacataires.fetchVacataireses(editingContext, qualifier, null);
    EOVacataires eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVacataires)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Vacataires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacataires fetchRequiredVacataires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataires.fetchRequiredVacataires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacataires fetchRequiredVacataires(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVacataires eoObject = _EOVacataires.fetchVacataires(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Vacataires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacataires localInstanceIn(EOEditingContext editingContext, EOVacataires eo) {
    EOVacataires localInstance = (eo == null) ? null : (EOVacataires)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
