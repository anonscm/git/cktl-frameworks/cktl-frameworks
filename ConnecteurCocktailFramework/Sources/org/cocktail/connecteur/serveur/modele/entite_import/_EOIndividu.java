// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividu.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividu extends ObjetImportAvecSource {
	public static final String ENTITY_NAME = "Individu";

	// Attributes
	public static final String ACTIVITE_KEY = "activite";
	public static final String CIVILITE_KEY = "civilite";
	public static final String CLE_INSEE_KEY = "cleInsee";
	public static final String CLE_INSEE_PROV_KEY = "cleInseeProv";
	public static final String DATE_NAISSANCE_KEY = "dateNaissance";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DPT_NAISSANCE_KEY = "dptNaissance";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LIEU_DECES_KEY = "lieuDeces";
	public static final String NO_INSEE_KEY = "noInsee";
	public static final String NO_INSEE_PROV_KEY = "noInseeProv";
	public static final String NOM_AFFICHAGE_KEY = "nomAffichage";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_KEY = "nomPatronymiqueAffichage";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String OPERATION_KEY = "operation";
	public static final String PAYS_NAISSANCE_KEY = "paysNaissance";
	public static final String PAYS_NATIONALITE_KEY = "paysNationalite";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRENOM2_KEY = "prenom2";
	public static final String PRENOM_AFFICHAGE_KEY = "prenomAffichage";
	public static final String PRISE_CPT_INSEE_KEY = "priseCptInsee";
	public static final String QUALITE_KEY = "qualite";
	public static final String SIT_FAMILLE_KEY = "sitFamille";
	public static final String SIT_MILITAIRE_KEY = "sitMilitaire";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PERSONNEL_KEY = "temPersonnel";
	public static final String VILLE_NAISSANCE_KEY = "villeNaissance";

	// Relationships
	public static final String CIBLE_KEY = "cible";
	public static final String SOURCE_KEY = "source";

  private static Logger LOG = Logger.getLogger(_EOIndividu.class);

  public EOIndividu localInstanceIn(EOEditingContext editingContext) {
    EOIndividu localInstance = (EOIndividu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String activite() {
    return (String) storedValueForKey("activite");
  }

  public void setActivite(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating activite from " + activite() + " to " + value);
    }
    takeStoredValueForKey(value, "activite");
  }

  public String civilite() {
    return (String) storedValueForKey("civilite");
  }

  public void setCivilite(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating civilite from " + civilite() + " to " + value);
    }
    takeStoredValueForKey(value, "civilite");
  }

  public Integer cleInsee() {
    return (Integer) storedValueForKey("cleInsee");
  }

  public void setCleInsee(Integer value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating cleInsee from " + cleInsee() + " to " + value);
    }
    takeStoredValueForKey(value, "cleInsee");
  }

  public Integer cleInseeProv() {
    return (Integer) storedValueForKey("cleInseeProv");
  }

  public void setCleInseeProv(Integer value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating cleInseeProv from " + cleInseeProv() + " to " + value);
    }
    takeStoredValueForKey(value, "cleInseeProv");
  }

  public NSTimestamp dateNaissance() {
    return (NSTimestamp) storedValueForKey("dateNaissance");
  }

  public void setDateNaissance(NSTimestamp value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating dateNaissance from " + dateNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "dateNaissance");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDeces() {
    return (NSTimestamp) storedValueForKey("dDeces");
  }

  public void setDDeces(NSTimestamp value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating dDeces from " + dDeces() + " to " + value);
    }
    takeStoredValueForKey(value, "dDeces");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String dptNaissance() {
    return (String) storedValueForKey("dptNaissance");
  }

  public void setDptNaissance(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating dptNaissance from " + dptNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "dptNaissance");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String lieuDeces() {
    return (String) storedValueForKey("lieuDeces");
  }

  public void setLieuDeces(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating lieuDeces from " + lieuDeces() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDeces");
  }

  public String noInsee() {
    return (String) storedValueForKey("noInsee");
  }

  public void setNoInsee(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating noInsee from " + noInsee() + " to " + value);
    }
    takeStoredValueForKey(value, "noInsee");
  }

  public String noInseeProv() {
    return (String) storedValueForKey("noInseeProv");
  }

  public void setNoInseeProv(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating noInseeProv from " + noInseeProv() + " to " + value);
    }
    takeStoredValueForKey(value, "noInseeProv");
  }

  public String nomAffichage() {
    return (String) storedValueForKey("nomAffichage");
  }

  public void setNomAffichage(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating nomAffichage from " + nomAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "nomAffichage");
  }

  public String nomPatronymique() {
    return (String) storedValueForKey("nomPatronymique");
  }

  public void setNomPatronymique(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating nomPatronymique from " + nomPatronymique() + " to " + value);
    }
    takeStoredValueForKey(value, "nomPatronymique");
  }

  public String nomPatronymiqueAffichage() {
    return (String) storedValueForKey("nomPatronymiqueAffichage");
  }

  public void setNomPatronymiqueAffichage(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating nomPatronymiqueAffichage from " + nomPatronymiqueAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "nomPatronymiqueAffichage");
  }

  public String nomUsuel() {
    return (String) storedValueForKey("nomUsuel");
  }

  public void setNomUsuel(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating nomUsuel from " + nomUsuel() + " to " + value);
    }
    takeStoredValueForKey(value, "nomUsuel");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String paysNaissance() {
    return (String) storedValueForKey("paysNaissance");
  }

  public void setPaysNaissance(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating paysNaissance from " + paysNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "paysNaissance");
  }

  public String paysNationalite() {
    return (String) storedValueForKey("paysNationalite");
  }

  public void setPaysNationalite(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating paysNationalite from " + paysNationalite() + " to " + value);
    }
    takeStoredValueForKey(value, "paysNationalite");
  }

  public String prenom() {
    return (String) storedValueForKey("prenom");
  }

  public void setPrenom(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating prenom from " + prenom() + " to " + value);
    }
    takeStoredValueForKey(value, "prenom");
  }

  public String prenom2() {
    return (String) storedValueForKey("prenom2");
  }

  public void setPrenom2(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating prenom2 from " + prenom2() + " to " + value);
    }
    takeStoredValueForKey(value, "prenom2");
  }

  public String prenomAffichage() {
    return (String) storedValueForKey("prenomAffichage");
  }

  public void setPrenomAffichage(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating prenomAffichage from " + prenomAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "prenomAffichage");
  }

  public String priseCptInsee() {
    return (String) storedValueForKey("priseCptInsee");
  }

  public void setPriseCptInsee(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating priseCptInsee from " + priseCptInsee() + " to " + value);
    }
    takeStoredValueForKey(value, "priseCptInsee");
  }

  public String qualite() {
    return (String) storedValueForKey("qualite");
  }

  public void setQualite(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating qualite from " + qualite() + " to " + value);
    }
    takeStoredValueForKey(value, "qualite");
  }

  public String sitFamille() {
    return (String) storedValueForKey("sitFamille");
  }

  public void setSitFamille(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating sitFamille from " + sitFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "sitFamille");
  }

  public String sitMilitaire() {
    return (String) storedValueForKey("sitMilitaire");
  }

  public void setSitMilitaire(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating sitMilitaire from " + sitMilitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "sitMilitaire");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPersonnel() {
    return (String) storedValueForKey("temPersonnel");
  }

  public void setTemPersonnel(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating temPersonnel from " + temPersonnel() + " to " + value);
    }
    takeStoredValueForKey(value, "temPersonnel");
  }

  public String villeNaissance() {
    return (String) storedValueForKey("villeNaissance");
  }

  public void setVilleNaissance(String value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
    	_EOIndividu.LOG.debug( "updating villeNaissance from " + villeNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "villeNaissance");
  }

  public org.cocktail.connecteur.serveur.modele.importer.EOCible cible() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOCible)storedValueForKey("cible");
  }

  public void setCibleRelationship(org.cocktail.connecteur.serveur.modele.importer.EOCible value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
      _EOIndividu.LOG.debug("updating cible from " + cible() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOCible oldValue = cible();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cible");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cible");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.importer.EOSource source() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOSource)storedValueForKey("source");
  }

  public void setSourceRelationship(org.cocktail.connecteur.serveur.modele.importer.EOSource value) {
    if (_EOIndividu.LOG.isDebugEnabled()) {
      _EOIndividu.LOG.debug("updating source from " + source() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOSource oldValue = source();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "source");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "source");
    }
  }
  

  public static EOIndividu createIndividu(EOEditingContext editingContext, String civilite
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String nomUsuel
, String prenom
, String statut
, String temImport
, String temPersonnel
) {
    EOIndividu eo = (EOIndividu) EOUtilities.createAndInsertInstance(editingContext, _EOIndividu.ENTITY_NAME);    
		eo.setCivilite(civilite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setNomUsuel(nomUsuel);
		eo.setPrenom(prenom);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemPersonnel(temPersonnel);
    return eo;
  }

  public static NSArray<EOIndividu> fetchAllIndividus(EOEditingContext editingContext) {
    return _EOIndividu.fetchAllIndividus(editingContext, null);
  }

  public static NSArray<EOIndividu> fetchAllIndividus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividu.fetchIndividus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividu> fetchIndividus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividu> eoObjects = (NSArray<EOIndividu>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividu fetchIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividu.fetchIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividu fetchIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividu> eoObjects = _EOIndividu.fetchIndividus(editingContext, qualifier, null);
    EOIndividu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividu)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Individu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividu fetchRequiredIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividu.fetchRequiredIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividu fetchRequiredIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividu eoObject = _EOIndividu.fetchIndividu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Individu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividu localInstanceIn(EOEditingContext editingContext, EOIndividu eo) {
    EOIndividu localInstance = (eo == null) ? null : (EOIndividu)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
