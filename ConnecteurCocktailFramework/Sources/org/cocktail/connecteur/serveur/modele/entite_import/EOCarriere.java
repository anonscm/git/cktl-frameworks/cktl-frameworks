//EOCarriere.java
//Created on Mon Jan 07 16:28:01 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import java.util.HashMap;

import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

//24/06/2010 - changement de la superclasse
public class EOCarriere extends _EOCarriere implements InterfaceHomonymie, InterfaceInvalidationRecordAvecDependance {

	private static HashMap<Integer, EOCarriere> idSourceCache = new HashMap<Integer, EOCarriere>();

	public EOCarriere() {
		super();
	}

	/** Verifie qu'il n'y a pas de chevauchements de segments */
	public EOLogImport verifierRecordApresOperation() {
		if (EOImportParametres.cacheMemoireNiveauHautOuPlus())
			idSourceCache.put(idSource() * 100 + carSource(), this);
		return null;
	}

	/**
	 * Carriere du SI destinataire avec le meme individu et commencant a la meme
	 * date debut et avec le meme type de population
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() == null || estNouvelIndividu()) { // A fortiori la
															// carrière est
															// nouvelle
			return null;
		}
		// Rechercher les carrières qui commencent à la même date, pour le même
		// individu et type de population
		NSArray carrieresMangue = EOMangueCarriere.rechercherCarrieresManguePourCarriere(editingContextTravail(), this, false);
		// Si il y en a plus d'une, on ne sait pas laquelle prendre mais ne
		// devrait pas se produire car un segment par type de population et date
		if (carrieresMangue.count() > 1) {
			return null;
		}
		try {
			EOMangueCarriere carriereMangue = (EOMangueCarriere) carrieresMangue.objectAtIndex(0);
			// Vérifier si elle n'est pas affectée à un autre segment qui est
			// déjà importé suite à homonymie
			ObjetCorresp correspondance = ObjetCorresp.rechercherObjetCorrespPourRecordDestinataire(editingContextTravail(), carriereMangue,
					EOCarriere.ENTITY_NAME);
			// EOCarriereCorresp correspondance =
			// EOCarriereCorresp.correspondancePourCarriereMangue(editingContextTravail(),
			// carriereMangue);
			if (correspondance != null) {
				return null; // le segment destinataire correspond déjà à un
								// autre segment
			} else {
				return carriereMangue;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public String nomRelationPourLog() {
		return EOLogImport.CARRIERE_KEY;
	}

	// InterfaceHomonymie
	/**
	 * true si on trouve plusieurs segments de carriere commencant a la meme
	 * date dans le SI Destinataire
	 */
	public boolean aHomonymeDansSIDestinataire() {
		return EOMangueCarriere.aHomonyme(editingContextTravail(), this);
	}

	/** retourne true si meme individu, meme type population et date de debut */
	public boolean memesDonneesQueRecord(ObjetImport record) {
		EOCarriere carriere = (EOCarriere) record;
		// Se baser sur l'id source car à ce moment-là, le record n'est pas
		// encore dans la base
		return (carriere.cTypePopulation().equals(cTypePopulation()) && carriere.idSource().equals(idSource()) && DateCtrl.isSameDay(carriere.dDebCarriere(),
				this.dDebCarriere()));
	}

	/** Supprime les elements de carriere et les affectations */
	public String invaliderRecordsDependants() {
		try {
			invaliderObjets(ObjetImportPourCarriere.rechercherObjetsImportCourantPourCarriere("ChangementPosition", this));
			invaliderObjets(ObjetImportPourCarriere.rechercherObjetsImportCourantPourCarriere("ElementCarriere", this));
			return null;
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		}
	}

	// Méthodes privées
	/*
	 * retourne les segments de carrières valides qui sont importés dans
	 * l'import courant pour l'individu et la même période
	 */
	private NSArray rechercherCarrieresValidesImportCourantPourIndividuEtPeriode(NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return EOCarriere.rechercherCarrieresValidesImportCourantPourIndividuEtPeriode(editingContextTravail(), individu(), debutPeriode, finPeriode, true);
	}

	private void ajouterCarriereDestinPourCarriere(NSMutableArray carrieresDestin, EOCarriere carriere) {
		if (operation().equals(OPERATION_INSERTION) == false) {
			// Il existe une correspondance dans le si destinataire
			EOMangueCarriere carriereDestin = (EOMangueCarriere) this.objetDestinataireAvecOuSansCorrespondance(editingContextTravail());
			if (carriereDestin != null) {
				carrieresDestin.addObject(carriereDestin);
			}
		}
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		String valeur = (String) valeursAttributs.objectForKey("cTypePopulation");
		// Vérifier dans la nomenclature si ce type de population existe
		EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePopulation", "cTypePopulation", valeur);
		if (record == null) {
			resultat = resultat + "Type_Population_Inconnu >>" + valeur + System.getProperty("line.separator");
		}
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey("dFinCarriere");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp) valeursAttributs.objectForKey("dDebCarriere");
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		return resultat;
	}

	/**
	 * Retourne les segments de carri&egrave;res valides qui sont importes dans
	 * l'import courant pour l'individu et la m&ecirc;me periode
	 */
	public static NSArray rechercherCarrieresValidesImportCourantPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu,
			NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean shouldRefresh) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND statut <> 'A' AND statut <> 'E'", new NSArray(individu));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(Finder.qualifierPourPeriode("dDebCarriere", debutPeriode, "dFinCarriere", finPeriode));
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("Carriere", qualifier, null);
		myFetch.setPrefetchingRelationshipKeyPaths(new NSArray("individu"));
		myFetch.setRefreshesRefetchedObjects(shouldRefresh);
		return editingContext.objectsWithFetchSpecification(myFetch);

	}

	/**
	 * Retourne true si l'individu a un type de population fonctionnaire pendant
	 * la periode. Pour cela, on regarde les segments de carriere de l'import
	 * courant, puis ceux deja importes i.e dans le SI destinataire
	 */
	public static boolean fonctionnaireEnActivitePendantPeriode(EOEditingContext editingContext, Number idSource, NSTimestamp dateDebut, NSTimestamp dateFin) {
		// Recherche les carrières de l'individu dans l'import courant
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E'", new NSArray(idSource));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(Finder.qualifierPourPeriode("dDebCarriere", dateDebut, "dFinCarriere", dateFin));

		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("Carriere", qualifier, null);
		NSArray segments = editingContext.objectsWithFetchSpecification(myFetch);
		java.util.Enumeration e = segments.objectEnumerator();
		while (e.hasMoreElements()) {
			EOCarriere segment = (EOCarriere) e.nextElement();
			EOGenericRecord typePopulation = (EOGenericRecord) SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePopulation",
					"cTypePopulation", segment.cTypePopulation());
			if (typePopulation.valueForKey("temFonctionnaire").equals(CocktailConstantes.VRAI)) {
				return true;
			}
		}
		// Rechercher les carrières dans Mangue pour la période
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource);
		if (individu != null) {
			EOGrhumIndividu individuGRhum = EOIndividuCorresp.individuGrhum(editingContext, idSource);
			if (individuGRhum != null) {
				segments = EOMangueCarriere.rechercherCarrieresDestinValidesSurPeriode(editingContext, individuGRhum, dateDebut, dateFin);
				e = segments.objectEnumerator();
				while (e.hasMoreElements()) {
					EOMangueCarriere segment = (EOMangueCarriere) e.nextElement();
					EOGenericRecord typePopulation = (EOGenericRecord) SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePopulation",
							"cTypePopulation", segment.cTypePopulation());
					if (typePopulation.valueForKey("temFonctionnaire").equals(CocktailConstantes.VRAI)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Retourne true si l'individu a un type de population fonctionnaire avant
	 * la date passee en parametre. Pour cela, on regarde les segments de
	 * carriere de l'import courant, puis ceux deja importes i.e dans le SI
	 * destinataire
	 */
	public static boolean estTitulaireAvantDate(EOEditingContext editingContext, Number idSource, NSTimestamp date) {
		// Recherche les carrières de l'individu dans l'import courant
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + ".idSource = %@ AND statut <> 'A' AND statut <> 'E'", new NSArray(
				idSource));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_IMPORT_KEY + " = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DEB_CARRIERE_KEY + " < %@", new NSArray(date)));

		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		NSArray<EOCarriere> carrieres = editingContext.objectsWithFetchSpecification(myFetch);

		for (EOCarriere segment : carrieres) {
			EOGenericRecord typePopulation = (EOGenericRecord) SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext,
					EOTypePopulation.ENTITY_NAME, EOTypePopulation.C_TYPE_POPULATION_KEY, segment.cTypePopulation());
			if (typePopulation.valueForKey("temFonctionnaire").equals(CocktailConstantes.VRAI)) {
				return true;
			}
		}
		// Rechercher les carrières dans Mangue avant la date
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource);
		if (individu != null) {
			EOGrhumIndividu individuGRhum = EOIndividuCorresp.individuGrhum(editingContext, idSource);
			if (individuGRhum != null) {
				NSArray<EOMangueCarriere> carrieresMangue = EOMangueCarriere.rechercherCarrieresDestinValidesAvantDate(editingContext, individuGRhum, date);
				for (EOMangueCarriere segment : carrieresMangue) {
					EOGenericRecord typePopulation = (EOGenericRecord) SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext,
							EOTypePopulation.ENTITY_NAME, EOTypePopulation.C_TYPE_POPULATION_KEY, segment.cTypePopulation());
					if (typePopulation.valueForKey("temFonctionnaire").equals(CocktailConstantes.VRAI)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static EOCarriere carrierePourSourceId(EOEditingContext editingContext, Integer idSource, Integer carSource) {
		if (carSource == null)
			return null;

		EOCarriere resultat = null;

		if (EOImportParametres.cacheMemoireNiveauHautOuPlus()) {
			resultat = idSourceCache.get(idSource * 100 + carSource);
			if (resultat != null)
				return resultat;
		}

		if (editingContext == null || idSource == null)
			return null;

		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(carSource);
		args.addObject(STATUT_ANNULATION);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
				ID_SOURCE_KEY + " = %@ AND " + CAR_SOURCE_KEY + " = %@ AND " + STATUT_KEY + " <> %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(EOCarriere.ENTITY_NAME, myQualifier, null);
		NSArray resultats = editingContext.objectsWithFetchSpecification(myFetch);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOCarriere)premierObjetValide(resultats);

		if (EOImportParametres.cacheMemoireNiveauHautOuPlus() && resultat != null) {
			idSourceCache.put(idSource * 100 + carSource, resultat);
		}
		return resultat;
	}

	public static void resetCache() {
		idSourceCache.clear();
	}
}
