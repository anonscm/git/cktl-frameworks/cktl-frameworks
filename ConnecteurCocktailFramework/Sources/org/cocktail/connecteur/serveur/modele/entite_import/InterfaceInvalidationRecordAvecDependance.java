package org.cocktail.connecteur.serveur.modele.entite_import;

/** Interface de suppression pour les records dont dependent d'autres records. Par exemple, les occupations dependent
 * des emplois
 * @author christine
 *
 */
public interface InterfaceInvalidationRecordAvecDependance {
	/** Invalider les records dependants */
	public String invaliderRecordsDependants();
}
