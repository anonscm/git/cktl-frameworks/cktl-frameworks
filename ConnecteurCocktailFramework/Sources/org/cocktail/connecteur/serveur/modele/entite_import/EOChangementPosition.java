//EOChangementPosition.java
//Created on Mon Jan 07 16:28:20 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.IntervalleTemps;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifPosition;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOPosition;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEnfantCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 08/02/2011 - correction d'un bug dans l'évaluation de la quotité , il ne faut pas prendre en compte dans le
// calcul de la quotité des changements de position en update suite à un changement de dates
// 08/07/2011 - ajout de méthodes statiques de recherche des changements de position
public class EOChangementPosition extends _EOChangementPosition {

	public EOChangementPosition() {
		super();
	}

	@Override
	public void preparerRelations() {
		super.preparerRelations();
		
		EOCarriere carriere = EOCarriere.carrierePourSourceId(editingContext(), idSource(), carriereOrigine());
		setToCarriereOrigineRelationship(carriere);
		
		if (enfSource() != null) {
			EOEnfant enfant = EOEnfantCorresp.enfantPourIndividuEtId(editingContextTravail(), idSource(), enfSource());
			setToEnfantRelationship(enfant);
		}

	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setToCarriereOrigineRelationship(null);
		setToEnfantRelationship(null);
	}

	/**
	 * Verifie que les relations du record sont bien valides : i.e que l'enfant
	 * existe bien cree juste auparavant. Retourne un message de log ou null si
	 * pas d'erreur
	 */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;

		if (carSource()==null && carriereOrigine()==null) {
			messageLog = "CARRIERE_ACCUEIL_OU_CARRIERE_ORIGINE";
		} else if (enfSource() != null && toEnfant() == null) {
			messageLog = "ENFANT_NON_IMPORTE";
		} else if (toEnfant() != null && toEnfant().statut().equals(EOEnfant.STATUT_ERREUR)) {
			messageLog = "ENFANT_INVALIDE";
		} else if (carriereOrigine() != null && toCarriereOrigine() == null) {
			messageLog = "CARRIERE_NON_IMPORTEE";
		} else if (toCarriereOrigine() != null && toCarriereOrigine().statut().equals(EOCarriere.STATUT_ERREUR)) {
			messageLog = "CARRIERE_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			log.setChangementPositionRelationship(this);
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	/**
	 * Verifie qu'il n'y a pas de chevauchement de changements de position et
	 * que les dates correspondent a celles du segment de carriere
	 */
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport logImport = verifierDatesParRapportCarriere(this.dDebPosition(), this.dFinPosition());
		if (logImport != null) {
			return logImport;
		}

		return null;
	}

	/**
	 * Changement de position du SI destinataire commencant a la meme date debut
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (toCarriere() != null) {
			if (estNouvelleCarriere()) { 
				return null;
			} else {
				return EOMangueChangementPosition.changementDestinationPourChangement(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	public String nomRelationPourLog() {
		return EOLogImport.CHANGEMENT_POSITION_KEY;
	}

	// Méthodes privées
	/*
	 * retourne les changements de position valides qui sont importés dans
	 * l'import courant pour l'individu et la même période.
	 */
	private NSArray rechercherChangementsValidesImportCourantPourIndividuEtPeriode() {
		return rechercherChangementsValidesImportCourantPourIndividu(true);
	}

	private NSArray rechercherChangementsValidesImportCourantPourIndividu() {
		return rechercherChangementsValidesImportCourantPourIndividu(false);
	}

	private NSArray rechercherChangementsValidesImportCourantPourIndividu(boolean prendreEnComptePeriode) {
		NSMutableArray args = new NSMutableArray(individu());
		args.addObject(toCarriere());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND carriere = %@ AND statut <> 'A' AND statut <> 'E'", args);
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		if (prendreEnComptePeriode) {
			qualifiers.addObject(Finder.qualifierPourPeriode("dDebPosition", dDebPosition(), "dFinPosition", dFinPosition()));
		}
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("ChangementPosition", qualifier, null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContextTravail().objectsWithFetchSpecification(myFetch);
	}

	private void ajouterChangementDestinPourChangement(NSMutableArray changementsDestin, EOChangementPosition changement) {
		if (changement.operation().equals(OPERATION_INSERTION) == false) {
			EOMangueChangementPosition changementDestin = null;
			if (changement.operation().equals(OPERATION_UPDATE)) {
				changementDestin = (EOMangueChangementPosition) ObjetCorresp.rechercherDestinatairePrecedentPourRecordImport(editingContextTravail(),
						changement);
			} else { // Correspondance
				changementDestin = EOMangueChangementPosition.changementDestinationPourChangement(editingContextTravail(), changement);
			}
			if (changementDestin != null) {
				changementsDestin.addObject(changementDestin);
			}
		}
	}

	// Méthodes statiques
	/**
	 * Retourne true si l'individu a un changement de position en activie
	 * pendant la periode. Pour cela, on regarde les changements de position de
	 * l'import courant, puis ceux deja importes i.e dans le SI destinataire
	 */
	public static boolean individuEnActivitePendantPeriode(EOEditingContext editingContext, Number idSource, NSTimestamp dateDebut, NSTimestamp dateFin) {
		// Recherche des changements de position de l'individu dans l'import
		// courant
		NSArray positionsEnActivite = (NSArray) Finder.rechercherAvecAttributEtValeurEgale(editingContext, "Position", "temActivite", CocktailConstantes.VRAI)
				.valueForKey("cPosition");
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E'", new NSArray(idSource));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(Finder.qualifierPourPeriode("dDebPosition", dateDebut, "dFinPosition", dateFin));

		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("ChangementPosition", qualifier, null);
		NSArray changements = editingContext.objectsWithFetchSpecification(myFetch);
		java.util.Enumeration e = changements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOChangementPosition changement = (EOChangementPosition) e.nextElement();
			if (positionsEnActivite.containsObject(changement.cPosition())) {
				return true;
			}
		}
		// Rechercher les changements de position dans Mangue pour la période
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource);
		if (individu != null) {
			EOGrhumIndividu individuGRhum = EOIndividuCorresp.individuGrhum(editingContext, idSource);
			if (individuGRhum != null) {
				changements = EOMangueChangementPosition.rechercherChangementsPositionDestinPourIndividuValidesSurPeriode(editingContext, individuGRhum,
						dateDebut, dateFin);
				e = changements.objectEnumerator();
				while (e.hasMoreElements()) {
					EOMangueChangementPosition changement = (EOMangueChangementPosition) e.nextElement();
					if (positionsEnActivite.containsObject(changement.cPosition())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static NSArray rechercherChangementsPositionsPourIndividuEtPeriode(EOEditingContext editingContext, Number idSource, NSTimestamp dateDebut,
			NSTimestamp dateFin) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(ID_SOURCE_KEY + " = %@ AND " + STATUT_KEY + " <> 'A' AND " + STATUT_KEY + " <> 'E'",
				new NSArray(idSource));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(Finder.qualifierPourPeriode(D_DEB_POSITION_KEY, dateDebut, D_FIN_POSITION_KEY, dateFin));
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		NSArray changements = editingContext.objectsWithFetchSpecification(myFetch);
		return changements;
	}

	/**
	 * Retourne les intervalles de temps des changements en activie pendant la
	 * periode pour lesquels le type de population du segment n'est pas
	 * fonctionnaire. Pour cela, on regarde les changements de position de
	 * l'import courant, puis ceux deja importes i.e dans le SI destinataire
	 */
	public static NSArray intervallesPourChangementsEnActiviteEtNonFonctionnairePendantPeriode(EOEditingContext editingContext, Number idSource,
			NSTimestamp dateDebut, NSTimestamp dateFin) {
		// Recherche des changements de position de l'individu dans l'import
		// courant
		NSMutableArray intervalles = new NSMutableArray();
		NSMutableArray changementsEnUpdate = new NSMutableArray();
		NSArray positionsEnActivite = (NSArray) Finder.rechercherAvecAttributEtValeurEgale(editingContext, "Position", "temActivite", CocktailConstantes.VRAI)
				.valueForKey("cPosition");
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E'", new NSArray(idSource));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(Finder.qualifierPourPeriode("dDebPosition", dateDebut, "dFinPosition", dateFin));
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("ChangementPosition", qualifier, null);
		NSArray changements = editingContext.objectsWithFetchSpecification(myFetch);
		java.util.Enumeration e = changements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOChangementPosition changement = (EOChangementPosition) e.nextElement();
			// On ne garde que les changements qui ont une position en activité
			// et dont le type de population est non-fonctionnaire
			if (positionsEnActivite.containsObject(changement.cPosition())) {
				EOGenericRecord typePopulation = (EOGenericRecord) SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePopulation",
						"cTypePopulation", changement.toCarriere().cTypePopulation());
				if (typePopulation.valueForKey("temFonctionnaire").equals(CocktailConstantes.FAUX)) {
					intervalles.addObject(new IntervalleTemps(changement.dDebPosition(), changement.dFinPosition()));
					if (changement.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || changement.operation().equals(ObjetImport.OPERATION_UPDATE)) {
						changementsEnUpdate.addObject(changement);
					}
				}
			}
		}
		// Rechercher les changements de position dans Mangue pour la période
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource);
		if (individu != null) {
			EOGrhumIndividu individuGRhum = EOIndividuCorresp.individuGrhum(editingContext, idSource);
			if (individuGRhum != null) {
				changements = EOMangueChangementPosition.rechercherChangementsPositionDestinPourIndividuValidesSurPeriode(editingContext, individuGRhum,
						dateDebut, dateFin);
				e = changements.objectEnumerator();
				while (e.hasMoreElements()) {
					EOMangueChangementPosition changementMangue = (EOMangueChangementPosition) e.nextElement();
					EOChangementPosition changement = (EOChangementPosition) ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext,
							changementMangue, "ChangementPosition");
					// On ne garde que les changements qui ne sont pas en update
					// dans le SI Destinataire, qui ont une position en activité
					// et dont le type de population est non-fonctionnaire
					if ((changement == null || changementsEnUpdate.containsObject(changement) == false)
							&& positionsEnActivite.containsObject(changementMangue.cPosition()) && changementMangue.carriere()!=null) {
						EOGenericRecord typePopulation = (EOGenericRecord) SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext,
								"TypePopulation", "cTypePopulation", changementMangue.carriere().cTypePopulation());
						if (typePopulation.valueForKey("temFonctionnaire").equals(CocktailConstantes.FAUX)) {
							intervalles.addObject(new IntervalleTemps(changementMangue.dDebPosition(), changementMangue.dFinPosition()));
						}
					}
				}
			}
		}
		return intervalles;
	}

	/**
	 * Verifie les nomenclatures ainsi que les dates et durees des changements
	 * de position pour ceux qui sont limites dans le temps.<BR>
	 * Verifie que le motif de position est donne si il est requis par la
	 * position
	 */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		String cPostion = (String) valeursAttributs.objectForKey(C_POSITION_KEY);
		EOPosition position = EOPosition.fetchPosition(editingContext, EOPosition.C_POSITION_KEY, cPostion);
		if (position == null) {
			resultat = resultat + "Position_Inconnue >>" + cPostion + System.getProperty("line.separator");
		}

		String cMotifPostion = (String) valeursAttributs.objectForKey(C_MOTIF_POSITION_KEY);
		if (cMotifPostion != null) {
			EOMotifPosition motifPosition = EOMotifPosition.fetchMotifPosition(editingContext, EOMotifPosition.C_MOTIF_POSITION_KEY, cMotifPostion);
			if (motifPosition == null) {
				resultat = resultat + "MotifPosition_Inconnu >>" + cMotifPostion + System.getProperty("line.separator");
			} else if (!motifPosition.cPosition().equals(cPostion)) {
				resultat = resultat + "Couple_Position_Motif_Inconnu(Position,Motif) >>" + cPostion + "," + cMotifPostion
						+ System.getProperty("line.separator");
			}
		}

		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey("dFinPosition");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp) valeursAttributs.objectForKey("dDebPosition");
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
			// vérifier si la durée dépasse la durée max. définie dans la
			// position
			Number dureeMax = null;
			if (position != null) {
				dureeMax = (Number) position.valueForKey("dureeMaxPeriodPos");
			}
			if (dureeMax != null) {
				Integer dureeChangement = DateCtrl.calculerDureeEnMois(dateDebut, dateFin);
				if (dureeChangement == null || dureeChangement.intValue() > dureeMax.intValue()) {
					resultat = resultat + "Duree_Invalide >>" + dureeMax + " mois" + System.getProperty("line.separator");
				}
			}
		} else if (position != null) {
			// vérifier si la date de fin est obligatoire pour cette position
			if (((String) position.valueForKey("temDFinOblig")).equals(CocktailConstantes.VRAI)) {
				resultat = resultat + "Date_Fin_Obligatoire >>" + System.getProperty("line.separator");
			}
		}

		// Vérifier les rne
		String valeur = (String) valeursAttributs.objectForKey("cRne");
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette Rne existe
			EORne rneRecord = EORne.getFromCode(editingContext, valeur);
			if (rneRecord == null) {
				resultat = resultat + "Rne_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		valeur = (String) valeursAttributs.objectForKey("cRneOrig");
		if (valeur != null) {
			// Vérifier dans la nomenclature si ce disc existe
			EORne rneRecord = EORne.getFromCode(editingContext, valeur);
			if (rneRecord == null) {
				resultat = resultat + "Rne_Origine_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		Integer quotite = (Integer) valeursAttributs.objectForKey("quotitePosition");
		if (quotite.intValue() == 0) {
			resultat = resultat + "Quotite_Nulle >>" + System.getProperty("line.separator");
		} else if (quotite.intValue() > 100) {
			resultat = resultat + "Quotite_Invalide >>" + System.getProperty("line.separator");
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "temoinPositionPrev");

		return resultat;
	}

}
