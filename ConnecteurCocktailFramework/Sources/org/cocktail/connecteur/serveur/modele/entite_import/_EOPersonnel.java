// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPersonnel.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPersonnel extends ObjetImport {
	public static final String ENTITY_NAME = "Personnel";

	// Attributes
	public static final String AFFECTE_DEFENSE_KEY = "affecteDefense";
	public static final String CIR_D_CERTIFICATION_KEY = "cirDCertification";
	public static final String CIR_D_COMPLETUDE_KEY = "cirDCompletude";
	public static final String C_LOGE_KEY = "cLoge";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NB_ENFANTS_KEY = "nbEnfants";
	public static final String NO_MATRICULE_KEY = "noMatricule";
	public static final String NUMEN_KEY = "numen";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TXT_LIBRE_KEY = "txtLibre";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOPersonnel.class);

  public EOPersonnel localInstanceIn(EOEditingContext editingContext) {
    EOPersonnel localInstance = (EOPersonnel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String affecteDefense() {
    return (String) storedValueForKey("affecteDefense");
  }

  public void setAffecteDefense(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating affecteDefense from " + affecteDefense() + " to " + value);
    }
    takeStoredValueForKey(value, "affecteDefense");
  }

  public NSTimestamp cirDCertification() {
    return (NSTimestamp) storedValueForKey("cirDCertification");
  }

  public void setCirDCertification(NSTimestamp value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating cirDCertification from " + cirDCertification() + " to " + value);
    }
    takeStoredValueForKey(value, "cirDCertification");
  }

  public NSTimestamp cirDCompletude() {
    return (NSTimestamp) storedValueForKey("cirDCompletude");
  }

  public void setCirDCompletude(NSTimestamp value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating cirDCompletude from " + cirDCompletude() + " to " + value);
    }
    takeStoredValueForKey(value, "cirDCompletude");
  }

  public String cLoge() {
    return (String) storedValueForKey("cLoge");
  }

  public void setCLoge(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating cLoge from " + cLoge() + " to " + value);
    }
    takeStoredValueForKey(value, "cLoge");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer nbEnfants() {
    return (Integer) storedValueForKey("nbEnfants");
  }

  public void setNbEnfants(Integer value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating nbEnfants from " + nbEnfants() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfants");
  }

  public String noMatricule() {
    return (String) storedValueForKey("noMatricule");
  }

  public void setNoMatricule(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating noMatricule from " + noMatricule() + " to " + value);
    }
    takeStoredValueForKey(value, "noMatricule");
  }

  public String numen() {
    return (String) storedValueForKey("numen");
  }

  public void setNumen(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating numen from " + numen() + " to " + value);
    }
    takeStoredValueForKey(value, "numen");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String txtLibre() {
    return (String) storedValueForKey("txtLibre");
  }

  public void setTxtLibre(String value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
    	_EOPersonnel.LOG.debug( "updating txtLibre from " + txtLibre() + " to " + value);
    }
    takeStoredValueForKey(value, "txtLibre");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOPersonnel.LOG.isDebugEnabled()) {
      _EOPersonnel.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOPersonnel createPersonnel(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String statut
, String temImport
) {
    EOPersonnel eo = (EOPersonnel) EOUtilities.createAndInsertInstance(editingContext, _EOPersonnel.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOPersonnel> fetchAllPersonnels(EOEditingContext editingContext) {
    return _EOPersonnel.fetchAllPersonnels(editingContext, null);
  }

  public static NSArray<EOPersonnel> fetchAllPersonnels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPersonnel.fetchPersonnels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPersonnel> fetchPersonnels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPersonnel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPersonnel> eoObjects = (NSArray<EOPersonnel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPersonnel fetchPersonnel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersonnel.fetchPersonnel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersonnel fetchPersonnel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPersonnel> eoObjects = _EOPersonnel.fetchPersonnels(editingContext, qualifier, null);
    EOPersonnel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPersonnel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Personnel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersonnel fetchRequiredPersonnel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersonnel.fetchRequiredPersonnel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersonnel fetchRequiredPersonnel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPersonnel eoObject = _EOPersonnel.fetchPersonnel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Personnel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersonnel localInstanceIn(EOEditingContext editingContext, EOPersonnel eo) {
    EOPersonnel localInstance = (eo == null) ? null : (EOPersonnel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
