package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOOrigineDemande;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOCrct extends _EOCrct {
	public EOCrct() {
		super();
	}

	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCrct.findForIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CRCT_KEY;
	}

	@Override
	public EOLogImport verifierRecord() {
		EOLogImport logImport = super.verifierRecord();
		if (logImport != null) {
			return logImport;
		}

		String messageLog = null;
		if (cOrigineDemande().equals("C")) {
			if (cSectionCnu() == null || cSectionCnu().equals("")) {
				messageLog = "CRCT_ORIGINE_DEMANDE_C_MAIS_CNU_VIDE";
			}
			if (temFractionnement().equals(CocktailConstantes.VRAI)) {
				messageLog = "CRCT_ORIGINE_DEMANDE_C_MAIS_FRACTIONNEMENT";
			}
		}

		boolean changementPositionTrouve = false;
		NSArray changementPositions = EOChangementPosition.rechercherChangementsPositionsPourIndividuEtPeriode(editingContext(), idSource(), dateDebut(),
				dateFin());
		if (changementPositions.count() > 0)
			changementPositionTrouve = true;
		else {
			EOGrhumIndividu grhumIndividu = EOIndividuCorresp.individuGrhum(editingContext(), idSource());
			if (grhumIndividu != null) {
				changementPositions = EOMangueChangementPosition.rechercherChangementsPositionDestinPourIndividuValidesSurPeriode(editingContext(),
						grhumIndividu, dateDebut(), dateFin());
				changementPositionTrouve = changementPositions.count() > 0;
			}
		}
		if (!changementPositionTrouve) {
			messageLog = "CRCT_INCLUS_DANS_CHANGEMENT_POSITION";
		}

		if (messageLog != null) {
			logImport = new EOLogImport();
			logImport.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			logImport.setIdSource(new Integer(idSource().intValue()));
			if (individu() != null) {
				logImport.addObjectToBothSidesOfRelationshipWithKey(individu(), "individu");
			}
			setStatut(STATUT_ERREUR);
		}

		return logImport;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);

		String sectionCnu = (String) valeursAttributs.objectForKey(C_SECTION_CNU_KEY);
		String sousSectionCnu = (String) valeursAttributs.objectForKey(C_SOUS_SECTION_CNU_KEY);
		if (sectionCnu != null || sousSectionCnu != null) {
			// Vérifier dans la nomenclature si cette section cnu existe
			EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext, sectionCnu, sousSectionCnu);
			if (cnu == null) {
				resultat = resultat + "SectionCnu,SousSectionCnu_Inconnus >>" + sectionCnu + "," + sousSectionCnu + System.getProperty("line.separator");
			}
		}

		String cOrigineDemande = (String) valeursAttributs.objectForKey(C_ORIGINE_DEMANDE_KEY);
		if (cOrigineDemande != null) {
			EOOrigineDemande demande = EOOrigineDemande.rechercherPourCOrigineDemande(editingContext, cOrigineDemande);
			if (demande == null) {
				resultat = resultat + "OrigineDemande_Inconnue >>" + cOrigineDemande + System.getProperty("line.separator");
			}
		}

		Number quotiteService = (Number) valeursAttributs.objectForKey(QUOTITE_SERVICE_KEY);
		if (quotiteService == null)
			valeursAttributs.setObjectForKey(new Integer(100), QUOTITE_SERVICE_KEY);

		resultat = resultat + verifierTemoin(valeursAttributs, TEM_GEST_ETAB_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, TEM_FRACTIONNEMENT_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, TEM_CONFIRME_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);

		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return crctSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCrctSource(value);
	}

	@Override
	protected boolean estAnnule() {
		return dAnnulation()!=null || noArreteAnnulation()!=null;
	}
	
	
}
