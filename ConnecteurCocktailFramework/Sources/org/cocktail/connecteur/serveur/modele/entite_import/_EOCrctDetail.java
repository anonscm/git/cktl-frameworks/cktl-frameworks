// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCrctDetail.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCrctDetail extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "CrctDetail";

	// Attributes
	public static final String CRCTD_SOURCE_KEY = "crctdSource";
	public static final String CRCT_SOURCE_KEY = "crctSource";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String CRCT_KEY = "crct";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCrctDetail.class);

  public EOCrctDetail localInstanceIn(EOEditingContext editingContext) {
    EOCrctDetail localInstance = (EOCrctDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer crctdSource() {
    return (Integer) storedValueForKey("crctdSource");
  }

  public void setCrctdSource(Integer value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating crctdSource from " + crctdSource() + " to " + value);
    }
    takeStoredValueForKey(value, "crctdSource");
  }

  public Integer crctSource() {
    return (Integer) storedValueForKey("crctSource");
  }

  public void setCrctSource(Integer value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating crctSource from " + crctSource() + " to " + value);
    }
    takeStoredValueForKey(value, "crctSource");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
    	_EOCrctDetail.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCrct crct() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCrct)storedValueForKey("crct");
  }

  public void setCrctRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCrct value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
      _EOCrctDetail.LOG.debug("updating crct from " + crct() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCrct oldValue = crct();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "crct");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "crct");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCrctDetail.LOG.isDebugEnabled()) {
      _EOCrctDetail.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCrctDetail createCrctDetail(EOEditingContext editingContext, Integer crctdSource
, Integer crctSource
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EOCrctDetail eo = (EOCrctDetail) EOUtilities.createAndInsertInstance(editingContext, _EOCrctDetail.ENTITY_NAME);    
		eo.setCrctdSource(crctdSource);
		eo.setCrctSource(crctSource);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCrctDetail> fetchAllCrctDetails(EOEditingContext editingContext) {
    return _EOCrctDetail.fetchAllCrctDetails(editingContext, null);
  }

  public static NSArray<EOCrctDetail> fetchAllCrctDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCrctDetail.fetchCrctDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCrctDetail> fetchCrctDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCrctDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCrctDetail> eoObjects = (NSArray<EOCrctDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCrctDetail fetchCrctDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrctDetail.fetchCrctDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrctDetail fetchCrctDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCrctDetail> eoObjects = _EOCrctDetail.fetchCrctDetails(editingContext, qualifier, null);
    EOCrctDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCrctDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CrctDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrctDetail fetchRequiredCrctDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrctDetail.fetchRequiredCrctDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrctDetail fetchRequiredCrctDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCrctDetail eoObject = _EOCrctDetail.fetchCrctDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CrctDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrctDetail localInstanceIn(EOEditingContext editingContext, EOCrctDetail eo) {
    EOCrctDetail localInstance = (eo == null) ? null : (EOCrctDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
