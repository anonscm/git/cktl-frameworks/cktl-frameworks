// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOInstance.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOInstance extends ObjetImport {
	public static final String ENTITY_NAME = "Instance";

	// Attributes
	public static final String C_INSTANCE_KEY = "cInstance";
	public static final String C_TYPE_INSTANCE_KEY = "cTypeInstance";
	public static final String D_CALCUL_KEY = "dCalcul";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_REFERENCE_KEY = "dReference";
	public static final String D_SCRUTIN_KEY = "dScrutin";
	public static final String LC_INSTANCE_KEY = "lcInstance";
	public static final String LL_INSTANCE_KEY = "llInstance";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOInstance.class);

  public EOInstance localInstanceIn(EOEditingContext editingContext) {
    EOInstance localInstance = (EOInstance)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cInstance() {
    return (String) storedValueForKey("cInstance");
  }

  public void setCInstance(String value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating cInstance from " + cInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "cInstance");
  }

  public String cTypeInstance() {
    return (String) storedValueForKey("cTypeInstance");
  }

  public void setCTypeInstance(String value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating cTypeInstance from " + cTypeInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeInstance");
  }

  public NSTimestamp dCalcul() {
    return (NSTimestamp) storedValueForKey("dCalcul");
  }

  public void setDCalcul(NSTimestamp value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating dCalcul from " + dCalcul() + " to " + value);
    }
    takeStoredValueForKey(value, "dCalcul");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dReference() {
    return (NSTimestamp) storedValueForKey("dReference");
  }

  public void setDReference(NSTimestamp value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating dReference from " + dReference() + " to " + value);
    }
    takeStoredValueForKey(value, "dReference");
  }

  public NSTimestamp dScrutin() {
    return (NSTimestamp) storedValueForKey("dScrutin");
  }

  public void setDScrutin(NSTimestamp value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating dScrutin from " + dScrutin() + " to " + value);
    }
    takeStoredValueForKey(value, "dScrutin");
  }

  public String lcInstance() {
    return (String) storedValueForKey("lcInstance");
  }

  public void setLcInstance(String value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating lcInstance from " + lcInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "lcInstance");
  }

  public String llInstance() {
    return (String) storedValueForKey("llInstance");
  }

  public void setLlInstance(String value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating llInstance from " + llInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "llInstance");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOInstance.LOG.isDebugEnabled()) {
    	_EOInstance.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }


  public static EOInstance createInstance(EOEditingContext editingContext, String cInstance
, String cTypeInstance
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcInstance
, String llInstance
, String operation
, String statut
, String temImport
) {
    EOInstance eo = (EOInstance) EOUtilities.createAndInsertInstance(editingContext, _EOInstance.ENTITY_NAME);    
		eo.setCInstance(cInstance);
		eo.setCTypeInstance(cTypeInstance);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcInstance(lcInstance);
		eo.setLlInstance(llInstance);
		eo.setOperation(operation);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOInstance> fetchAllInstances(EOEditingContext editingContext) {
    return _EOInstance.fetchAllInstances(editingContext, null);
  }

  public static NSArray<EOInstance> fetchAllInstances(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOInstance.fetchInstances(editingContext, null, sortOrderings);
  }

  public static NSArray<EOInstance> fetchInstances(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOInstance.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOInstance> eoObjects = (NSArray<EOInstance>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOInstance fetchInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInstance.fetchInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInstance fetchInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOInstance> eoObjects = _EOInstance.fetchInstances(editingContext, qualifier, null);
    EOInstance eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOInstance)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Instance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInstance fetchRequiredInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInstance.fetchRequiredInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInstance fetchRequiredInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    EOInstance eoObject = _EOInstance.fetchInstance(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Instance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInstance localInstanceIn(EOEditingContext editingContext, EOInstance eo) {
    EOInstance localInstance = (eo == null) ? null : (EOInstance)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
