/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteAttribut;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.ObjetPourComparaison;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOPassageEchelon;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.importer.moteur.ReglesDestination;
import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.serveur.modele.correspondance.EOTelephoneCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonneTelephone;
import org.cocktail.connecteur.serveur.modele.entite_destination.InterfaceValiditePourDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.services.ObjetImportServicesImpl;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe generique pour les objets importes. Ont en commun : une operation d'import et un statut d'import. Toutes les sous-classes doivent etre rangees dans le
 * package org.cocktail.connecteur.serveur.modele.entite_import.<BR>
 * L'operation sur le record et le statut de celui-ci sont determines apres qu'on ait verifie que l'objet est bien importable.Tous les objets a importer doivent
 * sous-classer ObjetImport et implementer les methodes abstraites<BR>
 * 
 * Operations d'import :<BR>
 * Insertion -> Record inconnu dans la base destinataire<BR>
 * Update -> Record deja importe<BR>
 * Correspondance -> Record deja importe mais non reference dans la base d'import<BR>
 * Suppression -> Demande par l'utilisateur que l'import ne soit pas realise<BR>
 * Statut des records :<BR>
 * Non importe : au demarrage de l'import<BR>
 * Erreur : objet importable mais detection d'une erreur (references invalides )<BR>
 * Homonyme : homonyme detecte dans le SI destinataire. L'operation est une insertion, l'utilisateur doit prendre une decision<BR>
 * Tronque : des donnes ont ete tronquees pendant l'import<BR>
 * Termine : record importe dans le SI destinataire<BR>
 * Les sous-classes doivent definir une methode statique "validerAttributs" qui permet de valider les donnees et eventuellement adaptees les donnees lues dans
 * les fichiers relativement
 * 
 */
public abstract class ObjetImport extends EOGenericRecord implements IObjetImport {
	/** Statut au demarrage de l'import */
	public final static String STATUT_NON_IMPORTE = "N";
	/** Statut si annulation de l'import par suppression du record demandee par l'utilisateur, ces records ne seront jamais supprimes de l'import */
	public final static String STATUT_ANNULATION = "A";
	/** Statut si la r&egrave;gle de priorite est celle de la destination */
	public final static String STATUT_PRIORITE_DESTINATION = "P";
	/** Statut si erreur de l'import (par exemple donnee tronquee) */
	public final static String STATUT_ERREUR = "E";
	/** Statut OK */
	public final static String STATUT_VALIDE = "V";
	/** Statut tronque */
	public final static String STATUT_TRONQUE = "T";
	/** Statut Homonyme trouve dans la base cible */
	public final static String STATUT_HOMONYME = "H";
	/** Operation : Pas d'operation vers la base cible */
	public final static String PAS_OPERATION = "N";
	/** Operation : Insertion dans la base cible */
	public final static String OPERATION_INSERTION = "I";
	/** Operation : Mise a jour dans la base cible */
	public final static String OPERATION_UPDATE = "U";
	/** Operation : Correspondance a generer */
	public final static String OPERATION_CORRESPONDANCE = "C";
	/** Operation si le record existe deja dans le SI Destinataire lors d'un update et qu'il est absolument identique */
	public final static String OPERATION_A_OUBLIER = "O";
	/** Operation si le record est de la classe ObjetImporPourPersonnel et que l'individu n'est pas un personnel */
	public final static String OPERATION_A_OUBLIER_POUR_PERSONNEL = "H";
	/**
	 * Operation si le record existe deja dans le SI Destinataire et que la priorite du SI Destinataire l'emporte (il doit s'agir d'une priorite globale
	 */
	public final static String OPERATION_A_OUBLIER_POUR_PRIORITE = "P";
	/** Pas d'import demande */
	public final static String OPERATION_SUPPRIMER = "S";
	/** temoin d'import : record en cours d'import, non transfere dans le SI Destinataire */
	public final static String A_TRANSFERER = "N";
	/** temoin d'import : record transfere dans le SI Destinataire */
	public final static String TRANSFERE = "O";
	/** temoin d'import : record supprime (il ne sera pas transfere dans le SI Destinataire) */
	public final static String SUPPRIME = "A";
	/** import abandonne en cours de transfert */
	public final static String ABANDON_TRANSFERT = "T";

	private ReglesImport reglesImport;

	public final static String NOM_PARAM_CHECK_HOMONYMS = "VERIFIER_HOMONYMES";
	public final static String NOM_PARAM_CHECK_UPDATES = "VERIFIER_MISES_A_JOUR";

	private final static boolean PARAM_CHECK_HOMONYMS_VALEUR_PAR_DEFAUT = true;
	private final static boolean PARAM_CHECK_UPDATES_VALEUR_PAR_DEFAUT = true;

	private static boolean checkHomonyms = PARAM_CHECK_HOMONYMS_VALEUR_PAR_DEFAUT;
	private static boolean checkUpdates = PARAM_CHECK_UPDATES_VALEUR_PAR_DEFAUT;
	private static boolean checkHomonymsInitialized = false;
	private static boolean checkUpdatesInitialized = false;

	static public boolean litParametreCheckHomonyms(EOEditingContext edc) {
		if (!checkHomonymsInitialized) {
			String paramCheckHomonyms = EOImportParametres.valeurParametrePourCle(edc, NOM_PARAM_CHECK_HOMONYMS);
			if (paramCheckHomonyms == null)
				checkHomonyms = PARAM_CHECK_HOMONYMS_VALEUR_PAR_DEFAUT;
			else
				checkHomonyms = paramCheckHomonyms.equals(CocktailConstantes.VRAI);
			checkHomonymsInitialized = true;
		}
		return checkHomonyms;
	}

	static public boolean litParametreCheckUpdates(EOEditingContext edc) {
		if (!checkUpdatesInitialized) {
			String paramCheckUpdates = EOImportParametres.valeurParametrePourCle(edc, EOImportParametres.KEY_CONTROLE_MISES_A_JOUR);
			if (paramCheckUpdates == null)
				checkUpdates = PARAM_CHECK_UPDATES_VALEUR_PAR_DEFAUT;
			else
				checkUpdates = paramCheckUpdates.equals(CocktailConstantes.VRAI);
			checkUpdatesInitialized = true;
		}
		return checkUpdates;
	}

	/**
	 * @return Vrai si l'on teste les homonymes. Faux si le test n'est pas à effectuer sur l'entité (on considère qu'il n'y a pas d'homonymes)
	 */
	public boolean testHomonymeActif() {
		return litParametreCheckHomonyms(editingContext());
	}

	/**
	 * @return Vrai si l'on teste si l'élément a déjà été importé ou existe déjà dans Le SI Destinataire. Faux si le test n'est pas à effectuer sur l'entité (on
	 *         considère que la base import est vide et que les données n'existent pas déjà dans le SI destinataire)
	 */
	public boolean testUpdateActif() {
		return litParametreCheckUpdates(editingContext());
	}

	public ReglesImport getReglesImport() {
		return reglesImport;
	}

	public void setReglesImport(ReglesImport reglesImport) {
		this.reglesImport = reglesImport;
	}

	public String operation() {
		return (String) storedValueForKey("operation");
	}

	public void setOperation(String value) {
		takeStoredValueForKey(value, "operation");
	}

	public String temImport() {
		return (String) storedValueForKey("temImport");
	}

	public void setTemImport(String value) {
		takeStoredValueForKey(value, "temImport");
	}

	public String statut() {
		return (String) storedValueForKey("statut");
	}

	public void setStatut(String value) {
		takeStoredValueForKey(value, "statut");
	}

	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}

	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}

	// Méthodes ajoutées
	public void init() {
		setTemImport(ObjetImport.A_TRANSFERER);
		NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
		setStatut(STATUT_NON_IMPORTE);
		setOperation(PAS_OPERATION);
	}

	/**
	 * Determine le statut et l'operation en verifiant si un record similaire a deja ete importe => update, existe deja dans le SI => il manque une
	 * correspondance. Sinon c'est une insertion, verifie si il pourrait y avoir des homonymes dans le SI
	 */
	private ObjetImportServicesImpl service = new ObjetImportServicesImpl();

	public EOLogImport determinerStatutEtOperation(EOGrhumPrioriteEntite prioriteEntite) {

		LogManager.logDetail("Début du Passage dans ObjetImport.déterminerStatutEtOperation");

System.out.println("********** determinerStatutEtOperation");
		if (testUpdateActif()) {
			// Le fait que le statut soit tronqué est évalué auparavant lors de la copie des données dans le record
			ObjetCorresp correspondance = correspondance();
System.out.println("---------- correspondance="+correspondance);
			service.determineStatutEtOperation(this, correspondance);
			// Gestion des priorités
			if ((operation().equals(OPERATION_CORRESPONDANCE) || operation().equals(OPERATION_UPDATE)) && prioriteEntite != null) {
				setStatut(STATUT_PRIORITE_DESTINATION);
			}
			// 05/09/08 - pour gérer le problème de la clé primaire du téléphone dans GRHUM qui est composée du numéro de tél, du typeTel et typeNumero
			// Uniquement dans le cas des updates car dans le cas des correspondances, on a fait une vérification sur la clé primaire
			if (this instanceof EOTelephone && operation().equals(OPERATION_UPDATE)) {
				EOGrhumPersonneTelephone telGrhum = ((EOTelephoneCorresp) correspondance).telephoneGrhum();
				EOTelephone telImport = (EOTelephone) this;
				if (telGrhum.noTelephone().equals(telImport.noTelephone()) == false || telGrhum.typeNo().equals(telImport.typeNo()) == false
						|| telGrhum.typeTel().equals(telImport.typeTel()) == false) {
					this.setOperation(OPERATION_INSERTION);
				}
			}
			if (statut().equals(STATUT_HOMONYME) == false && statut().equals(STATUT_TRONQUE) == false && statut().equals(STATUT_PRIORITE_DESTINATION) == false) {
				setStatut(STATUT_VALIDE);
			}
		} else {
			this.setOperation(OPERATION_INSERTION);
			setStatut(STATUT_VALIDE);
		}


		LogManager.logDetail("Vérification du record après opération");

		// Réalisation d'opérations sur les sous-ensembles
		EOLogImport logImport = verifierRecordApresOperation(); // Vérifications une fois que l'objet est créé

		// 03/01/2010 - on ajoute un log pour les records tronqués
		if (logImport == null && statut().equals(STATUT_TRONQUE)) {
			logImport = new EOLogImport();
			logImport.initAvecEditingContext(editingContextTravail(), this.entityName(), "DONNEES_TRONQUEES");
		}
		LogManager.logDetail("Operation : " + operation() + ", statut :" + statut());
		return logImport;
	}

	/**
	 * retourne la correspondance associee a un record d'import : on recherche une correspondance pointant sur un record d'import avec les memes attributs
	 * source que le record courant
	 */
	public ObjetCorresp correspondance() {
		return ObjetCorresp.rechercherObjetCorrespPourRecordImport(editingContextTravail(), this, false);
	}

	/** retourne true si il existe dans le SI destinataire un objet equivalent */
	public boolean dejaDansSIDestinataire() {
		if (checkUpdates)
			return destinataireEquivalent() != null;
		else
			return false;
	}

	/**
	 * Retourne l'objet equivalent du SI Destinataire : il resulte soit d'une correspondance, soit on le trouve a partir du destinataire equivalent.
	 */
	public ObjetPourSIDestinataire objetDestinataireAvecOuSansCorrespondance(EOEditingContext editingContext) {
		ObjetCorresp correspondance = correspondance();
		if (correspondance != null) {
			return (ObjetPourSIDestinataire) correspondance.valueForKey(correspondance.nomRelationBaseDestinataire());
		} else {
			return destinataireEquivalent();
		}
	}

	public EOQualifier construireQualifierPourCorrespondance() {
		EOQualifier qualifier = construireQualifier(ObjetCorresp.nomRelationBaseImportPourEntite(entityName()));
		return qualifier;
	}

	public EOQualifier construireQualifierImportPourRecordPrecedent() {
		LogManager.logDetail("Début du pasage dans ObjetImport.construireQualifierImportpourRecordPrecedent");
		EOQualifier qualifier = construireQualifier("");
		LogManager.logDetail(entityName() + " : qualifier pour record prec " + qualifier.toString());
		LogManager.logDetail("Fin du pasage dans ObjetImport.construireQualifierImportpourRecordPrecedent");
		return qualifier;
	}

	// Méthodes abstraites
	/** Prepare les relations. A appeler apr&egrave;s avoir insere un objet dans l'editing context */
	public abstract void preparerRelations();

	/** Supprime les relations avant une destruction. */
	public abstract void supprimerRelations();

	/** Verifie que toutes les relations attendues sont definies */
	public abstract EOLogImport verifierRecord();

	/** Effectue des verifications complementaires une fois que l'operation est determinee */
	public abstract EOLogImport verifierRecordApresOperation();

	/** retourne l'objet &acute;quivalent dans le SI destinataire */
	public abstract ObjetPourSIDestinataire destinataireEquivalent();

	/** retourne le nom de la relation dans LogImport pour les logs lies au record */
	public abstract String nomRelationPourLog();

	// Autres méthodes
	public void invalider() {
		setStatut(STATUT_ANNULATION);
		setTemImport(SUPPRIME);
	}

	/** Supprime tous les records lies a ce record */
	public String invaliderRecordsDependants() {
		try {
			editingContext().lock();
			if (this instanceof InterfaceInvalidationRecordAvecDependance) {
				String message = ((InterfaceInvalidationRecordAvecDependance) this).invaliderRecordsDependants();
				if (message == null && editingContext().hasChanges()) {
					editingContext().saveChanges();
				}
				return message;
			}
			return null;
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		} finally {
			editingContext().unlock();
		}
	}

	/**
	 * Methode distante invoquee pour retourner un tableau contenant les informations sur les objets des 2 SI de maniere a pouvoir les comparer. La priorite des
	 * attributs est indiquee
	 * 
	 * @return NSArray de ObjetPourComparaison
	 */
	public NSArray clientSideRequestAttributsAvecComparaison() {
		return attributsComparaisonRecordImportRecordDestinataire(this.objetDestinataireAvecOuSansCorrespondance(editingContext()));
	}

	/**
	 * Methode distante invoquee pour retourner un tableau contenant les informations sur les objets des 2 SI de maniere a pouvoir les comparer. La priorite des
	 * attributs est indiquee. La globalID du record destinataire est passee en parametre
	 * 
	 * @return NSArray de ObjetPourComparaison
	 */
	public NSArray clientSideRequestAttributsAvecRecordDestinataire(EOGlobalID gidDestinataire) {
		if (gidDestinataire == null) {
			return new NSArray();
		}
		EOGenericRecord record = (EOGenericRecord) Finder.objetForGlobalIDDansEditingContext(gidDestinataire, editingContext());
		if (record instanceof ObjetPourSIDestinataire) {
			return attributsComparaisonRecordImportRecordDestinataire((ObjetPourSIDestinataire) record);
		} else {
			return new NSArray();
		}
	}

	/** Retourne le nom de la relation liee a un attribut de comparaison, null si pas de relation */
	public abstract String nomRelationPourAttributComparaison(String nomAttribut);

	// Méthodes protégées

	protected EOEditingContext editingContextTravail() {
		return AutomateImport.sharedInstance().editingContext();
	}

	/** Retourne le nom de l'entite de destination */
	protected String nomEntiteDestination() {
		return AutomateImport.sharedInstance().nomEntiteDestinationPourEntiteImport(entityName());
	}

	/** Invalide des objets de l'import courant et enregistre les modifications de l'editing context */
	protected void invaliderObjets(NSArray objetsAInvalider) {
		if (objetsAInvalider != null && objetsAInvalider.count() > 0) {
			java.util.Enumeration e = objetsAInvalider.objectEnumerator();
			while (e.hasMoreElements()) {
				ObjetImport record = (ObjetImport) e.nextElement();
				EOLogImport.invaliderLogsPourRecord(editingContext(), record);
				if (record instanceof InterfaceInvalidationRecordAvecDependance) {
					((InterfaceInvalidationRecordAvecDependance) record).invaliderRecordsDependants();
				}
				record.invalider();
				editingContext().saveChanges();
			}
		}
	}

	// Méthodes privées
	private NSArray attributsComparaisonRecordImportRecordDestinataire(ObjetPourSIDestinataire destinataire) {
		NSMutableArray attributs = new NSMutableArray();

		// Rechercher l'entité de destination et les priorités liées à cette entité
		String nomEntiteDestination = AutomateImport.sharedInstance().nomEntiteDestinationPourEntiteImport(this.entityName());
		Entite entite = ReglesDestination.sharedInstance().entiteAvecNomDestination(nomEntiteDestination);
		EOGrhumPrioriteEntite prioriteEntite = EOGrhumPrioriteEntite.rechercherPrioriteEntitePourEntite(editingContext(), nomEntiteDestination);
		String priorite = "", temValidite = "";
		if (destinataire != null) {
			// Ajouter au début un témoin de validité pour l'objet en indiquant rien dans la source
			temValidite = "O";
			if (destinataire instanceof InterfaceValiditePourDestinataire) {
				if (((InterfaceValiditePourDestinataire) destinataire).destinataireValidePourObjetImport(this) == false) {
					temValidite = "N";
				}
			} else if (!destinataire.estValide()) {
				temValidite = "N";
			}
			// par défaut on revalide les records destintaires invalidés sauf si dans la priorité de l'entité il est signifier
			// que le record n'est pas à revalider
			if (temValidite.equals("N") && (prioriteEntite == null || (prioriteEntite != null && prioriteEntite.estARevalider()))) {
				priorite = "R";
			}
		}
		attributs.addObject(new ObjetPourComparaison("Destinataire valide", "", temValidite, priorite));
		priorite = "";
		java.util.Enumeration e = entite.attributs().objectEnumerator();
		while (e.hasMoreElements()) {
			Attribut attribut = (Attribut) e.nextElement();
			Object valeurImport = this.valueForKey(attribut.nomSource());
			Object valeurDestination = null;
			if (destinataire != null) {
				// Il s'agit d'un attribut de l'entité
				valeurDestination = destinataire.valeurPourCleEtObjetImport(attribut.nomDestination(), this);
			}
			if (destinataire != null && prioriteEntite != null) {
				if (prioriteEntite.existeAttributsValides()) {
					EOGrhumPrioriteAttribut prioriteAttribut = prioriteEntite.attributPourLibelle(attribut.nomDestination(), true);
					if (prioriteAttribut != null) {
						priorite = "X";
					}
				} else {
					// tous les attributs du SI Destinataire l'emportent
					priorite = "X";
				}
			}
			attributs.addObject(new ObjetPourComparaison(attribut.nomSource(), valeurImport, valeurDestination, priorite));
		}
		return attributs;
	}

	/***
	 * 
	 * @param nomRelationImport
	 * @return
	 */
	private EOQualifier construireQualifier(String nomRelationImport) {
		LogManager.logDetail("Début du pasage dans ObjetImport.construireQualifier");
		if (nomRelationImport.length() > 0) {
			nomRelationImport += ".";
		}
		// Récupérer l'entité et ses attributs de la base d'import
		NSMutableArray qualifiers = new NSMutableArray();
		Entite entite = AutomateImport.sharedInstance().entiteImportPourNom(entityName());
		for (Attribut attribut : (NSArray<Attribut>) entite.attributs()) {

			if (attribut.estAttributComparaison()) {
				Object valeur = valueForKey(attribut.nomDestination());
				if (attribut.estObligatoire() && valeur == null) {
					return null;
				}
				if (attribut.estObligatoire() || valeur != null) {
					String nomRelationComparaison = nomRelationPourAttributComparaison(attribut.nomDestination());
					if (nomRelationComparaison == null) {
						nomRelationComparaison = "";
					} else {
						nomRelationComparaison += ".";
					}
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(nomRelationImport + nomRelationComparaison + attribut.nomDestination()
							+ " = %@", new NSArray(valeur)));
				}
			}
		}

		LogManager.logDetail("Fin du pasage dans ObjetImport.construireQualifier");
		return new EOAndQualifier(qualifiers);
	}

	// Méthodes statiques
	/** Retourne le nom d'une relation en fonction du nom d'entite (premier caract&egrave;re du nom en minuscule) */
	/*
	 * public static String nomRelationAPartirNomEntite(String nomEntite) { String nom = nomEntite; // les noms des relations sont les noms d'entité commençant
	 * par une minuscule String premierCar = nomEntite.substring(0,1); nom = nom.replaceFirst(premierCar,premierCar.toLowerCase()); return nom; }
	 */
	/** Recherche les objets qui n'ont pas encore ete importes dans le S.I. Destinataire et qui ne sont pas annules */
	public static NSArray rechercherObjetsPourImport(EOEditingContext editingContext, String nomEntite) {
		return rechercherObjetsPourImportEtStatut(editingContext, nomEntite, null);
	}

	/** Recherche les objets valides (en insertion, correspondance, update) qui n'ont pas encore ete importes dans le S.I. */
	public static NSArray rechercherObjetsPourImportValides(EOEditingContext editingContext, String nomEntite) {
		return rechercherObjetsPourImportEtStatut(editingContext, nomEntite, STATUT_VALIDE);
	}

	/**
	 * Recherche les objets qui doivent respecter les priorites du SI Destinataire, en insertion, correspondance, update, qui n'ont pas encore ete importes dans
	 * le S.I. destinataire
	 */
	public static NSArray rechercherObjetsPourImportAvecPrioriteDestination(EOEditingContext editingContext, String nomEntite) {
		return rechercherObjetsPourImportEtStatut(editingContext, nomEntite, STATUT_PRIORITE_DESTINATION);
	}

	/**
	 * Recherche les objets tronques (en insertion, correspondance, update) qui n'ont pas encore ete importes dans le S.I. Destinataire
	 */
	public static NSArray rechercherObjetsPourImportTronques(EOEditingContext editingContext, String nomEntite) {
		return rechercherObjetsPourImportEtStatut(editingContext, nomEntite, STATUT_TRONQUE);
	}

	/**
	 * Recherche les objets avec homonymes (en insertion necessairement )qui n'ont pas encore ete importes dans le S.I. Destinataire
	 */
	public static NSArray rechercherObjetsPourImportAvecHomonyme(EOEditingContext editingContext, String nomEntite) {
		return rechercherObjetsPourImportEtStatut(editingContext, nomEntite, STATUT_HOMONYME);
	}

	public static NSArray rechercherObjetsEnCorrespondancePourImportCourant(EOEditingContext editingContext, String nomEntite) {
		return rechercherObjetsPourOperationEtNomEntite(editingContext, nomEntite, OPERATION_CORRESPONDANCE);
	}

	public static NSArray rechercherObjetsEnUpdatePourImportCourant(EOEditingContext editingContext, String nomEntite) {
		return rechercherObjetsPourOperationEtNomEntite(editingContext, nomEntite, OPERATION_UPDATE);
	}

	/** Verifie si un temoins prend les valeurs O ou N */
	public static String verifierTemoin(NSDictionary valeursAttributs, String nomChamp) {
		String valeur = (String) valeursAttributs.objectForKey(nomChamp);
		if (valeur != null) {
			if (valeur.equals(CocktailConstantes.VRAI) == false && valeur.equals(CocktailConstantes.FAUX) == false) {
				return "Valeur_Temoin >>" + nomChamp + " : " + valeur + System.getProperty("line.separator");
			}
		}
		return "";
	}

	public static EOGenericRecord rechercherPassageEchelonPourGradeEtEchelon(EOEditingContext editingContext, String grade, String echelon) {
		NSMutableArray args = new NSMutableArray(grade);
		args.addObject(echelon);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cGrade = %@ AND cEchelon = %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(EOPassageEchelon.ENTITY_NAME, qualifier, null); // "PassageEchelon"
		EOGenericRecord resultat = null;
		try {
			NSArray passageEchelons = editingContext.objectsWithFetchSpecification(myFetch);
			if (passageEchelons.count() > 0)
				resultat = (EOGenericRecord) passageEchelons.get(0);
		} catch (Exception e) {
			return null;
		}
		return resultat;
	}

	/** Retourne true si il existe des homonymes dans l'import. */
	public static boolean existeHomonymes(EOEditingContext editingContext) {
		NSArray res = rechercherObjetsPourImportEtStatut(editingContext, EOIndividu.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, EOStructure.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, EOEnfant.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, EOContrat.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, EOContratAvenant.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, EOCarriere.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, EOElementCarriere.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, "ChangementPosition", STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		res = rechercherObjetsPourImportEtStatut(editingContext, EOAffectation.ENTITY_NAME, STATUT_HOMONYME);
		if (res != null && res.count() > 0) {
			return true;
		}
		return false;
	}

	// Méthodes privées
	/*
	 * Recherche les objets qui n'ont pas encore été importés dans le S.I., qui ont une opération d'insertion, de correspondance ou d'update et qui ont le
	 * statut passé en paramètre nomEntite nom de l'entité recherchée statut statut des records (peut être nul)
	 */
	private static NSArray rechercherObjetsPourImportEtStatut(EOEditingContext editingContext, String nomEntite, String statut) {
		if (nomEntite == null) {
			return null;
		}
		NSMutableArray args = new NSMutableArray(ObjetImport.A_TRANSFERER);
		String strQualifier = "temImport = %@";
		if (statut != null) {
			args.addObject(statut);
			strQualifier = strQualifier + " AND statut = %@";
		} else {
			strQualifier = strQualifier + " AND statut <> 'A'";
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(strQualifier, args);
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite, qualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	/**
	 * 
	 * @param editingContext
	 * @param nomEntite
	 * @param operation
	 * @return
	 */
	private static NSArray rechercherObjetsPourOperationEtNomEntite(EOEditingContext editingContext, String nomEntite, String operation) {
		LogManager.logDetail("Début du passage dans rechercherObjetsPourOperationEtNomEntite avec entité " + nomEntite + " et nom de l'opération " + operation);
		if (nomEntite == null) {
			return null;
		}
		NSMutableArray args = new NSMutableArray(ObjetImport.A_TRANSFERER);
		String strQualifier = "temImport = %@";
		if (operation != null) {
			args.addObject(operation);
			strQualifier = strQualifier + " AND operation = %@";
		}
		LogManager.logDetail("Log qualifier = " + strQualifier);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(strQualifier, args);
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite, qualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		NSArray res = editingContext.objectsWithFetchSpecification(fs);
		LogManager.logDetail("Nombre de résultats : " + res.count());
		LogManager.logDetail("Fin du passage dans rechercherObjetsPourOperationEtNomEntite");
		return res;
	}

	/**
	 * Retourne, si il existe, le premier élément dont le statut n'est ni annulation, ni erreur. Sinon, retourne le premier élément en erreur Sinon, retourne
	 * null
	 * 
	 * @param editingContext
	 * @param nomEntite
	 * @param operation
	 * @return le premier élément dont le statut n'est ni annulation, ni erreur
	 */
	public static ObjetImport premierObjetValide(NSArray<ObjetImport> objets) {
		if (objets == null)
			return null;

		// Le code n'est pas optimisé mais plus facile à lire
		// Retourne, s'il existe, le premier élément dont le statut est ni erreur ni annulation
		for (ObjetImport obj : objets) {
			if (!obj.statut().equals(STATUT_ERREUR) && !obj.statut().equals(STATUT_ANNULATION))
				return obj;
		}
		// Sinon, retourne le premier élément en erreur s'il existe
		for (ObjetImport obj : objets) {
			if (obj.statut().equals(STATUT_ERREUR))
				return obj;
		}
		return null;
	}
}
