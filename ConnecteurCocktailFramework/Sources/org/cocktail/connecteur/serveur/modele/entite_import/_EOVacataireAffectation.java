// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVacataireAffectation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVacataireAffectation extends ObjetImport {
	public static final String ENTITY_NAME = "VacataireAffectation";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NBR_HEURES_KEY = "nbrHeures";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PRINCIPALE_KEY = "temPrincipale";
	public static final String VAC_SOURCE_KEY = "vacSource";
	public static final String VAF_SOURCE_KEY = "vafSource";

	// Relationships
	public static final String STRUCTURE_KEY = "structure";
	public static final String VACATAIRES_KEY = "vacataires";

  private static Logger LOG = Logger.getLogger(_EOVacataireAffectation.class);

  public EOVacataireAffectation localInstanceIn(EOEditingContext editingContext) {
    EOVacataireAffectation localInstance = (EOVacataireAffectation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer nbrHeures() {
    return (Integer) storedValueForKey("nbrHeures");
  }

  public void setNbrHeures(Integer value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating nbrHeures from " + nbrHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeures");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPrincipale() {
    return (String) storedValueForKey("temPrincipale");
  }

  public void setTemPrincipale(String value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating temPrincipale from " + temPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipale");
  }

  public Integer vacSource() {
    return (Integer) storedValueForKey("vacSource");
  }

  public void setVacSource(Integer value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating vacSource from " + vacSource() + " to " + value);
    }
    takeStoredValueForKey(value, "vacSource");
  }

  public Integer vafSource() {
    return (Integer) storedValueForKey("vafSource");
  }

  public void setVafSource(Integer value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
    	_EOVacataireAffectation.LOG.debug( "updating vafSource from " + vafSource() + " to " + value);
    }
    takeStoredValueForKey(value, "vafSource");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
      _EOVacataireAffectation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires vacataires() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires)storedValueForKey("vacataires");
  }

  public void setVacatairesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires value) {
    if (_EOVacataireAffectation.LOG.isDebugEnabled()) {
      _EOVacataireAffectation.LOG.debug("updating vacataires from " + vacataires() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires oldValue = vacataires();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vacataires");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vacataires");
    }
  }
  

  public static EOVacataireAffectation createVacataireAffectation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer nbrHeures
, String statut
, String strSource
, String temImport
, String temPrincipale
, Integer vacSource
, Integer vafSource
) {
    EOVacataireAffectation eo = (EOVacataireAffectation) EOUtilities.createAndInsertInstance(editingContext, _EOVacataireAffectation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNbrHeures(nbrHeures);
		eo.setStatut(statut);
		eo.setStrSource(strSource);
		eo.setTemImport(temImport);
		eo.setTemPrincipale(temPrincipale);
		eo.setVacSource(vacSource);
		eo.setVafSource(vafSource);
    return eo;
  }

  public static NSArray<EOVacataireAffectation> fetchAllVacataireAffectations(EOEditingContext editingContext) {
    return _EOVacataireAffectation.fetchAllVacataireAffectations(editingContext, null);
  }

  public static NSArray<EOVacataireAffectation> fetchAllVacataireAffectations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVacataireAffectation.fetchVacataireAffectations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVacataireAffectation> fetchVacataireAffectations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVacataireAffectation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVacataireAffectation> eoObjects = (NSArray<EOVacataireAffectation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVacataireAffectation fetchVacataireAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataireAffectation.fetchVacataireAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacataireAffectation fetchVacataireAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVacataireAffectation> eoObjects = _EOVacataireAffectation.fetchVacataireAffectations(editingContext, qualifier, null);
    EOVacataireAffectation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVacataireAffectation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VacataireAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacataireAffectation fetchRequiredVacataireAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataireAffectation.fetchRequiredVacataireAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacataireAffectation fetchRequiredVacataireAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVacataireAffectation eoObject = _EOVacataireAffectation.fetchVacataireAffectation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VacataireAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacataireAffectation localInstanceIn(EOEditingContext editingContext, EOVacataireAffectation eo) {
    EOVacataireAffectation localInstance = (eo == null) ? null : (EOVacataireAffectation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
