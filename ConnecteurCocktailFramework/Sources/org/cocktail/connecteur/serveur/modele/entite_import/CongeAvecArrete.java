/*
 * 
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.Constantes;
import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.DateCtrl.IntRef;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.IntervalleTemps;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.correspondance.EODeclarationAccidentCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.InterfaceRequalification;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.MangueCongeAvecArreteAnnulation;
import org.cocktail.connecteur.serveur.modele.entite_import.conges.Absences;
import org.cocktail.connecteur.serveur.modele.entite_import.conges.PeriodeCongeAvecDateCommission;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** Classe abstraite : Modelise un conge legal avec un arrete associe et un commentaire.
 * Les conges legaux ne peuvent pas se chevaucher.
 * Pas de gestion des correspondances avec les conges du SI Destinataire : les conges sont en insertion ou update.
 * @author christine
 *
 *
 */
public abstract class CongeAvecArrete extends ObjetImportPourIndividuEtPeriode {
	private static EOGenericRecord parametresConges;
	private static String[] congesRequalifiants = {"Cld","Clm","CgntCgm"};
	public CongeAvecArrete() {
		super();
		parametresConges = null;
	}

	public abstract String noArrete();
	public abstract void setNoArrete(String value);

	public abstract NSTimestamp dateArrete();
	public abstract void setDateArrete(NSTimestamp value);

	public abstract String temConfirme();
	public abstract void setTemConfirme(String value);

	public abstract String temGestEtab();
	public abstract void setTemGestEtab(String value);

	public abstract String commentaire();
	public abstract void setCommentaire(String value);

	public abstract Integer annulationSource();
	public abstract void setAnnulationSource(Integer value);
	
	public abstract String sourceKey();

	public abstract NSTimestamp dAnnulation();
	public abstract void setDAnnulation(NSTimestamp value);

	public abstract String noArreteAnnulation();
	public abstract void setNoArreteAnnulation(String value);
	
	public abstract void setCongeAnnulationRelationship(CongeAvecArrete conge);
	public abstract CongeAvecArrete congeAnnulation();

	/** Surchargee par les sous-classes, retourne null si pas de date de commission */
	public abstract NSTimestamp dateCommission();

	// méthodes ajoutées
	public void init() {
		super.init();
		setTemConfirme(Constantes.FAUX);
		setTemGestEtab(Constantes.FAUX);
	}
	

	public void preparerRelations() {
		super.preparerRelations();
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setCongeAnnulationRelationship(null);
	}

	/** On ne gere pas les correspondances avec les conges du SI Destinataire */ 
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		String messageLog=null;
		// On fait la relation avec le congé annulation.
		// On ne pouvait pas le faire dans preparerRelation car le père n'était pas encore forcément sauvegarder dans la base.
		if (editingContext() != null && annulationSource() != null) {
			NSArray resultats=Finder.rechercherAvecAttribut(editingContext(), this.entityName(), sourceKey(), annulationSource());
			// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
			CongeAvecArrete conge = (CongeAvecArrete)premierObjetValide(resultats);
			setCongeAnnulationRelationship(conge);
		}
		if (annulationSource() != null) {
			if (congeAnnulation() == null) {
				messageLog = "CONGE_ANNULATION_NON_IMPORTE";
			} else if (congeAnnulation().temImport().equals(ObjetImport.A_TRANSFERER)) {
				if (congeAnnulation().statut().equals(STATUT_ERREUR)) {
					messageLog = "CONGE_ANNULATION_INVALIDE";
				}
			}
		}
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			setStatut(STATUT_ERREUR);
		}
		if (log != null) {
			log.setEimpSource(this.eimpSource());
		}
		return log;
	}
	/** Verifie qu'il n'y a pas de chevauchement de periodes avec d'autres conges legaux 
	 * dans l'import courant et d'autres conges legaux/modalites de service dans le SI Destinataire. 
	 * Retourne aussi un log d'avertissement si il existe un cong&e legal non gere par le 
	 * connecteur ou une modalite de service dans Mangue valide sur la periode  */
	public EOLogImport verifierRecordApresOperation() {
		if (estAnnule())
			return null;
		
		// On commence par effectuer les vérifications métier
		String message = verifierConge(editingContext());
		if (message != null) {
			EOLogImport log = new EOLogImport(); 
			log.initAvecEditingContext(editingContextTravail(),this.entityName(), message);
			log.setIdSource(individu().idSource());
			log.setEimpSource(eimpSource());
			log.addObjectToBothSidesOfRelationshipWithKey(individu(), "individu");
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			return log;
		}
		// On recherche tous les congés légaux dans l'import courant en se basant sur les absences importées
		NSArray conges = Absences.congesPourIndividuEtPeriode(individu(), dateDebut(), dateFin());
		message = null;
		boolean verifierAbsencesMangue = false;
		LogManager.logDetail("chevauchement dans import courant : " + (conges != null && conges.count() >= 1));
		if (conges != null && conges.count() >= 1) { // Le congé courant n'a pas encore été ajouté dans les Absences donc test >=1 et non >=2
			int cpt=0;
			// On vérifie que les congés liés ne sont pas tous annulés
			for (Object obj : conges) {
				CongeAvecArrete conge = (CongeAvecArrete) obj;
				if (!conge.estAnnule())
					cpt++;
			}
			if (cpt>=1) {
			message = "CHEVAUCHEMENT_PERIODE_IMPORT";
			verifierAbsencesMangue = estCongeRequalifiant();
			}
		} else {
			verifierAbsencesMangue = true;
		}
		String message1 = null;
////		if (verifierAbsencesMangue) {
////			// Vérifier si il existe dans Mangue des congés à la même période
////			message1 = verifierAbsencesMangue(editingContext());
////		}
//		if (message1 != null && message1.equals("CHEVAUCHEMENT_PERIODE_WARNING")) {
//			LogManager.logDetail("Chevauchement du conge avec des modalites de service du SI Destinataire : " + this);
//		}
		boolean verifierCongesImport = message != null && message.equals("CHEVAUCHEMENT_PERIODE_IMPORT") && estCongeRequalifiant();
		boolean verifierCongesDestin = message1 != null && message1.equals("CHEVAUCHEMENT_PERIODE_DESTIN") && estCongeRequalifiant();
		if (verifierCongesImport || verifierCongesDestin) {
			// Les congés requalifiants sont les cld, les clm et les cgntCgm
			message = verifierCongesRequalifies(editingContext(),conges,verifierCongesImport,verifierCongesDestin);	
		} else if (message == null) {
			message = message1;
		} 
		if (message != null) {
			LogManager.logDetail("Insertion d'un log pour "  + message);
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(),this.entityName(),message);
			if (nomRelationPourLog() != null) {
				log.addObjectToBothSidesOfRelationshipWithKey(this,nomRelationPourLog());
			}
			log.setIdSource(individu().idSource());
			log.setEimpSource(eimpSource());
			log.addObjectToBothSidesOfRelationshipWithKey(individu(),"individu");
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			return log;
		} 
		
		Absences.ajouterCongePourIndividu(this);
		return null;
	}
	/** Surcharge de la methode du parent */
	public String verifierRecordPourClient(EOEditingContext editingContext) {
		LogManager.logDetail("verifierRecordPourClient");
		// Effectuer les vérification métier avant de vérifier les chevauchements
		String message = verifierConge(editingContext);
		if (message != null) {
			return message;
		}
		// On ne fait pas l'appel à super car on vérifie globalement tous les congés légaux y compris les congés courants
		// On travaille dans un nouvel editing context car à cet instant l'objet courant n'est pas inséré dans un editing context
		NSArray conges = rechercherCongesLegauxPourIndividuEtPeriode(editingContext, idSource(), dateDebut(), dateFin());
		if (conges.count() > 1) {
			message = "CHEVAUCHEMENT_PERIODE_IMPORT";
		} else if (conges.count() == 1) {
			CongeAvecArrete conge = (CongeAvecArrete)conges.objectAtIndex(0);
			if (conge.getClass().getName().equals(this.getClass().getName()) == false) {
				// Il s'agit d'un congé d'un autre type
				message = "CHEVAUCHEMENT_PERIODE_IMPORT";
			} else if (getReglesImport().ontAttributsComparaisonIdentiques(this, conge) == false) {
				// Il s'agit d'un autre congé de ce type donc il y a un chevauchement, inutile d'aller plus loin
				return "CHEVAUCHEMENT_PERIODE_IMPORT";
			}
		}
		if (message != null && estCongeRequalifiant() == false) {
			// Chevauchement avec des congés qui ne requalifient pas ce congé
			return message;
		}
//		// Vérifier si il existe dans Mangue des congés à la même période
//		String message1 = verifierAbsencesMangue(editingContext);
//		if (message1 != null && message1.equals("CHEVAUCHEMENT_PERIODE_WARNING")) {
//			LogManager.logDetail("Chevauchement du conge avec des modalites de service du SI Destinataire : " + this);
//		}
//		boolean verifierCongesImport = message != null && message.equals("CHEVAUCHEMENT_PERIODE_IMPORT") && estCongeRequalifiant();
//		boolean verifierCongesDestin = message1 != null && message1.equals("CHEVAUCHEMENT_PERIODE_DESTIN") && estCongeRequalifiant();
//		if (verifierCongesImport || verifierCongesDestin) {
//			// Les congés requalifiants sont les cld, les clm et les cgntCgm
//			return verifierCongesRequalifies(editingContext,conges,verifierCongesImport,verifierCongesDestin);	
//		} else if (message == null) {
//			return message1;
//		} else {
			return message;
//		}
	}
	/** Retourne la duree entre le debut du premier conge et la fin du dernier (le nombre de jours correspond a la partie
	 * decimale du resultat
	 * @param periodes (tableau de PeriodeCongeAvecDateCommission)
	 * @return
	 */
	public double dureeEnMoisConges(NSArray periodes) {
		if (periodes == null || periodes.count() == 0) {
			return 0;
		}
		// Trier les congés par ordre de date croissante
		periodes = EOSortOrdering.sortedArrayUsingKeyOrderArray(periodes, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		// Récupérer la date de début du premier congé et la date de fin du dernier congé
		// Il y a au moins la période correspondant au congé courant
		NSTimestamp dateDebut = ((PeriodeCongeAvecDateCommission)periodes.objectAtIndex(0)).dateDebut();
		NSTimestamp dateFin = ((PeriodeCongeAvecDateCommission)periodes.lastObject()).dateFin();
		LogManager.logDetail("debut conge associe " + DateCtrl.dateToString(dateDebut));
		LogManager.logDetail("fin conge associe " + DateCtrl.dateToString(dateFin));
		IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
		DateCtrl.joursMoisAnneesEntre(dateDebut,dateFin,anneeRef,moisRef,jourRef,true);	// inclure les bornes
		LogManager.logDetail("duree : " + anneeRef.value + " an(s) " + moisRef.value + " mois " + jourRef.value + " jour(s)");

		return (12 * anneeRef.value) + moisRef.value + new Double(jourRef.value).doubleValue() /30;
	}
	/** Retourne la duree entre le debut du premier conge et la fin du dernier (le nombre de jours correspond a la partie
	 * decimale du resultat
	 * @param periodes (tableau de PeriodeCongeAvecDateCommission)
	 * @return
	 */
	public double dureeEnMois(NSTimestamp dateDebut, NSTimestamp dateFin) {
		LogManager.logDetail("debut " + DateCtrl.dateToString(dateDebut));
		LogManager.logDetail("fin " + DateCtrl.dateToString(dateFin));
		IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
		DateCtrl.joursMoisAnneesEntre(dateDebut,dateFin,anneeRef,moisRef,jourRef,true);	// inclure les bornes
		LogManager.logDetail("duree : " + anneeRef.value + " an(s) " + moisRef.value + " mois " + jourRef.value + " jour(s)");

		return (12 * anneeRef.value) + moisRef.value + new Double(jourRef.value).doubleValue() /30;

	}
	/** return true si l'individu a des contrats valides ou bien a un segment de carri&egrave;re de non fonctionnaire
	 * et un changement de position en activite. On regarde les donnees de l'import courant et celles du SI Destinataire.
	 * Pour simplifier le traitement, on ne regarde pas les avenants */

	public boolean estContractuelSurPeriodeComplete(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		// On récupère tous les contrats valides
		NSMutableArray intervalles = new NSMutableArray();
		NSMutableArray contratsEnUpdate = new NSMutableArray();
		NSArray contrats = EOContrat.rechercherContratsRemunerationPrincipalePourIndividuEtPeriode(editingContext,individu,debutPeriode,finPeriode,false);
		java.util.Enumeration e = contrats.objectEnumerator();
		while (e.hasMoreElements()) {
			EOContrat contrat = (EOContrat)e.nextElement();
			intervalles.addObject(new IntervalleTemps(contrat.dDebContratTrav(),contrat.dFinContratTrav()));
			if (contrat.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || contrat.operation().equals(ObjetImport.OPERATION_UPDATE)) {
				contratsEnUpdate.addObject(contrat);
			}
		}
		// Ajouter tous les contrats du SI Destinataire en se basant sur les avenants
		contrats = EOMangueContrat.rechercherContratsRemunerationPrincipalePourIndividuEtPeriode(editingContext,individu,debutPeriode,finPeriode);
		if (contrats != null && contrats.count() > 0) {
			e = contrats.objectEnumerator();
			while (e.hasMoreElements()) {
				EOMangueContrat contratMangue = (EOMangueContrat)e.nextElement();
				EOContrat contratAssocie = (EOContrat)ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext,contratMangue,"Contrat");
				if (contratAssocie == null || contratsEnUpdate.containsObject(contratAssocie) == false) {
					intervalles.addObject(new IntervalleTemps(contratMangue.dDebContratTrav(),contratMangue.dFinContratTrav()));
				}
			}
		}
		EOSortOrdering.sortArrayUsingKeyOrderArray(intervalles,new NSArray(EOSortOrdering.sortOrderingWithKey("debutPeriode",EOSortOrdering.CompareAscending)));
		LogManager.logDetail("Verification estContractuel");
		LogManager.logDetail("" + intervalles);
		if (intervalles.count() > 0) {
			LogManager.logDetail("est periode complete :" + couvrePeriodeComplete(intervalles,debutPeriode,finPeriode));
		}
		if (intervalles.count() > 0 && couvrePeriodeComplete(intervalles,debutPeriode,finPeriode)) {
			return true;
		}
		// sinon regarder si il y a des segments de carrière de non fonctionnaire et les ajouter aux intervalles
		intervalles.addObjectsFromArray(EOChangementPosition.intervallesPourChangementsEnActiviteEtNonFonctionnairePendantPeriode(editingContext, individu.idSource(), debutPeriode, finPeriode));
		LogManager.logDetail("Verification estContractuel");
		LogManager.logDetail("" + intervalles);
		if (intervalles.count() > 0) {
			boolean couvrePeriodeComplete = couvrePeriodeComplete(intervalles,debutPeriode,finPeriode);
			LogManager.logDetail("est periode complete :" + couvrePeriodeComplete);
			return couvrePeriodeComplete;
		} else {
			return false;
		}
	}
	// Méthodes protégées
	/** Retourne le resultat des verifications metier, null si pas d'erreur */
	protected abstract String verifierConge(EOEditingContext editingContext);
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOQualifier qualifierValidation = EOQualifier.qualifierWithQualifierFormat("noArreteAnnulation = nil AND dAnnulation = nil AND toCongeAnnulation = nil", null);
		NSArray objetsDestin = rechercherCongesPourIndividuEtPeriodeDansDestinatation(editingContext,nomEntiteDestination(),idSource(),dateDebut(),dateFin(),qualifierValidation);
		return objetsDestin.count();
	}
	/**  Retourne un tableau de PeriodeCongeAvecDateCommission des periodes anterieures a 
	 * classees par ordre de date fin decroissante
	 * @param editingContext
	 * @param date
	 * @return
	 */
	protected NSArray preparerPeriodesAnterieures(EOEditingContext editingContext,NSTimestamp date) {
		NSMutableArray periodesAnterieures = new NSMutableArray();
		NSMutableArray congesEnUpdate = new NSMutableArray();	// tableau des congesId en update/correspondance. Utilisé pour ne pas prendre en compte ceux du SI Destinataire 
		NSArray conges = rechercherCongesImportCourantPourIndividuAnterieursDate(editingContext, date);
		// On recherche d'abord les congés de l'import courant
		// Les congés sont gardés comme des périodes afin de pouvoir trier et comparer les congés de l'import courant et du SI Destinataire
		java.util.Enumeration e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			CongeAvecArrete conge = (CongeAvecArrete)e.nextElement();
			periodesAnterieures.addObject(new PeriodeCongeAvecDateCommission(conge.dateDebut(), conge.dateFin(), conge.dateCommission()));
			if (conge.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || conge.operation().equals(ObjetImport.OPERATION_UPDATE)) {
				congesEnUpdate.addObject(conge.eimpSource());
			}
		}
		// rechercher les congés antérieurs du SI Destinataire
		conges = rechercherCongesPourIndividuAnterieursDansDestinatation(editingContext, date);
		e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			// il ne peut y en avoir qu'un, pas de superposition des congés
			MangueCongeAvecArreteAnnulation conge = (MangueCongeAvecArreteAnnulation)e.nextElement();
			CongeAvecArrete congeImport = (CongeAvecArrete)ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext, conge, entityName());
			// congeImport = nul, il s'agit d'un congé uniquement défini dans Mangue
			if (congeImport == null || congesEnUpdate.containsObject(congeImport.eimpSource()) == false) {
				periodesAnterieures.addObject(new PeriodeCongeAvecDateCommission(conge.dateDebut(), conge.dateFin(), conge.dateCommission()));
			}
		}
		EOSortOrdering.sortArrayUsingKeyOrderArray(periodesAnterieures, new NSArray(EOSortOrdering.sortOrderingWithKey("dateFin",EOSortOrdering.CompareDescending)));
		return (NSArray)periodesAnterieures;
	}
	/** recherche recursivement tous les conges maladie valides qui sont associes au conge courant 
	 * (tous ceux tels que les dates de debut ou de fin soient contigues) : les conges sont recherches dans l'import
	 * courant et dans le SI Destinataire, on ne prend pas en compte les records du SI Destinataire qui correspondent a des conges 
	 * de l'import courant puisqu'ils seront modifies par l'import courant.
	 */
	protected NSArray preparerPeriodesContigues(EOEditingContext editingContext,NSArray periodesAnterieures,NSTimestamp dateDebut,NSTimestamp dateFin) {
		// On récupère tous les congés antérieurs et postérieurs pour en extraire les congés contigus à ce congé
		if (periodesAnterieures == null || periodesAnterieures.count() == 0) {
			periodesAnterieures = preparerPeriodesAnterieures(editingContext, dateDebut);
		}
		LogManager.logDetail("Periodes anterieures : " + periodesAnterieures);
		NSMutableArray periodesContigues = new NSMutableArray();
		java.util.Enumeration e = periodesAnterieures.objectEnumerator();
		NSTimestamp dateAComparer = DateCtrl.jourPrecedent(dateDebut);
		// Ne garder que les périodes antérieures qui sont contigues
		while (e.hasMoreElements()) {
			PeriodeCongeAvecDateCommission periode = (PeriodeCongeAvecDateCommission)e.nextElement();
			if (DateCtrl.isSameDay(periode.dateFin(), dateAComparer)) {
				periodesContigues.addObject(periode);
				dateAComparer = DateCtrl.jourPrecedent(periode.dateDebut());
			} else {
				break;	// on a trouvé une rupture dans la continuité
			}
		}
		// On ajoute le congé courant
		periodesContigues.addObject(new PeriodeCongeAvecDateCommission(dateDebut, dateFin, dateCommission()));
		if (dateFin != null) {
			NSArray periodesPosterieures = preparerPeriodesPosterieures(editingContext,dateFin);
			LogManager.logDetail("Periodes posterieures : " + periodesPosterieures);
			e = periodesPosterieures.objectEnumerator();
			dateAComparer = DateCtrl.jourSuivant(dateFin);
			// Ne garder que les périodes postérieures qui sont contigues, les périodes sont triées par ordre de date début croissante
			while (e.hasMoreElements()) {
				PeriodeCongeAvecDateCommission periode = (PeriodeCongeAvecDateCommission)e.nextElement();
				if (DateCtrl.isSameDay(periode.dateDebut(), dateAComparer)) {
					periodesContigues.addObject(periode);
					dateAComparer = DateCtrl.jourSuivant(periode.dateFin());
				} else {
					break;	// on a trouvé une rupture dans la continuité
				}
			}
		}
		// Trier les périodes par ordre de date dévut croissant
		EOSortOrdering.sortArrayUsingKeyOrderArray(periodesContigues, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		LogManager.logDetail("Periodes contigues : " + periodesContigues);

		return periodesContigues;
	}
	/* Retourne les congés de l'import courant postérieurs à la date qui ne sont pas annulés ou avec une erreur.
	 *  Si date est nulle, retourne tous les congés de l'import courant qui ne sont pas annulés ou avec 
	 *  une erreur.
	 */
	protected  NSArray rechercherCongesImportCourantPourIndividuPosterieursDate(EOEditingContext editingContext,NSTimestamp date) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E'",new NSArray(idSource()));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		if (date != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("dateDebut > %@",new NSArray(date));
			qualifiers.addObject(qualifier);
		}
		EOFetchSpecification myFetch = new EOFetchSpecification (this.entityName(), new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	// méthodes privées
	private boolean estCongeRequalifiant() {
		for (int i= 0; i < congesRequalifiants.length;i++) {
			if (this.entityName().equals(congesRequalifiants[i])) {
				return true;
			}
		}
		return false;
	}
	// conges contient tous les congés de l'import courant valides sur la période
	private String verifierCongesRequalifies(EOEditingContext editingContext,NSArray conges, boolean verifierImportCourant, boolean verifierDestination) {
		// Les congés requalifiants doivent couvrir complétement les congés qu'ils requalifient, sans interruption dans la continuité
		String entiteRequalifiee = null;
		String complementMessage = "CONGE_MALADIE";
		if (entityName().equals("Cld")) {
			entiteRequalifiee = "Clm";
			complementMessage = "CLM";
		} else if (entityName().equals("Clm")) {
			entiteRequalifiee = "CongeMaladie";
		} else if (entityName().equals("CgntCgm")) {
			entiteRequalifiee = "CgntMaladie";
		}
		if (entiteRequalifiee == null) {
			return null;	// par mesure de précaution
		}
		String message = verifierCongesAssocies(editingContext,entiteRequalifiee,conges,verifierImportCourant,verifierDestination);
		if (message == null && entityName().equals("Cld")) {
			message = verifierCongesAssocies(editingContext,"CongeMaladie",conges,verifierImportCourant,verifierDestination);
			complementMessage = "CONGE_MALADIE";
		}
		if (message != null) {
			message += "_" + complementMessage;
		}
		return message;
	}
	private String verifierCongesAssocies(EOEditingContext editingContext,String nomEntiteCongeAssocie,NSArray conges, boolean verifierImportCourant, boolean verifierDestination) {
		// On construit deux listes : celle des congés avec le témoin non requalifié, celle avec le témoin requalifié de manière
		// à pouvoir retourne le message qui va bien
		NSMutableArray periodesCongesQualifies = new NSMutableArray();
		NSMutableArray periodesCongesNonQualifies = new NSMutableArray();
		NSMutableArray congesEnUpdate = new NSMutableArray();
		if (verifierImportCourant) {
			// On ne s'occupe que des congés qui sont de l'entité associée
			java.util.Enumeration e = conges.objectEnumerator();
			CongeAvecArrete conge = (CongeAvecArrete)e.nextElement();
			if (conge.entityName().equals(nomEntiteCongeAssocie)) {
				if (((InterfaceRequalification)conge).estRequalifie()) {
					periodesCongesQualifies.addObject(new IntervalleTemps(conge.dateDebut(), conge.dateFin()));
				} else {
					periodesCongesNonQualifies.addObject(new IntervalleTemps(conge.dateDebut(), conge.dateFin()));
				}
				if (conge.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || conge.operation().equals(ObjetImport.OPERATION_UPDATE)) {
					congesEnUpdate.addObject(conge.eimpSource());
				}
			}
		}
		if (verifierDestination) {
			nomEntiteCongeAssocie = "Mangue" + nomEntiteCongeAssocie;
			NSArray congesAssocies = MangueCongeAvecArreteAnnulation.rechercherCongesValidesPourIndividuEtPeriode(editingContext, nomEntiteCongeAssocie,this.individu(),this.dateDebut(),this.dateFin());
			java.util.Enumeration e = congesAssocies.objectEnumerator();
			while (e.hasMoreElements()) {
				MangueCongeAvecArreteAnnulation conge = (MangueCongeAvecArreteAnnulation)e.nextElement();
				CongeAvecArrete congeImport = (CongeAvecArrete)ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext, conge, entityName());
				// congeImport = nul, il s'agit d'un congé uniquement défini dans Mangue
				if (congeImport == null || congesEnUpdate.containsObject(congeImport.eimpSource()) == false) {
					if (((InterfaceRequalification)conge).estRequalifie()) {
						periodesCongesQualifies.addObject(new IntervalleTemps(conge.dateDebut(), conge.dateFin()));
					} else {
						periodesCongesNonQualifies.addObject(new IntervalleTemps(conge.dateDebut(), conge.dateFin()));
					}
				}
			}
		}
		if (periodesCongesQualifies.count() == 0) {
			return null;		// pas de requalification
		}
		if (periodesCongesNonQualifies.count() > 0) {
			return "NON_REQUALIFIE";
		}
		// Vérifier si toutes les périodes sont bien contigues et couvrent bien toutes la période
		EOSortOrdering.sortArrayUsingKeyOrderArray(periodesCongesQualifies, new NSArray(EOSortOrdering.sortOrderingWithKey("debutPeriode", EOSortOrdering.CompareAscending)));
		LogManager.logDetail("Congés qualifiés " + periodesCongesQualifies);
		IntervalleTemps periode = (IntervalleTemps)periodesCongesQualifies.objectAtIndex(0);
		if (DateCtrl.isAfter(periode.debutPeriode(), this.dateDebut())) {
			return "CONGE_DISCONTINU";	// Toute la période n'est pas couverte
		}
		NSTimestamp dateDebut = null;
		if (DateCtrl.isBefore(periode.finPeriode(),this.dateFin())) {
			dateDebut = DateCtrl.jourSuivant(periode.finPeriode());
		} else {
			return null;	// on a passé la date de fin tous les congés requalifiés
		}
		for (int i = 1; i < periodesCongesQualifies.count(); i++) {
			periode = (IntervalleTemps)periodesCongesQualifies.objectAtIndex(i);
			if (DateCtrl.isSameDay(dateDebut,periode.debutPeriode()) == false) {
				return "CONGE_DISCONTINU";
			} else if (DateCtrl.isAfter(periode.finPeriode(),this.dateFin())) {
				return null;	// on a passé la date de fin tous les congés requalifiés
			} else {
				dateDebut = DateCtrl.jourSuivant(periode.finPeriode());
			}
		}
		return null;
	}
	/* Retourne les congés de l'import courant antérieurs à la date qui ne sont pas annulés ou avec une erreur.
	 *  Si date est nulle, retourne tous les congés de l'import courant qui ne sont pas annulés ou avec 
	 *  une erreur.
	 */
	private  NSArray rechercherCongesImportCourantPourIndividuAnterieursDate(EOEditingContext editingContext,NSTimestamp date) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND statut <> 'A' AND statut <> 'E'",new NSArray(idSource()));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		if (date != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("dateFin < %@",new NSArray(date));
			qualifiers.addObject(qualifier);
		}
		EOFetchSpecification myFetch = new EOFetchSpecification (this.entityName(), new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**  Retourne un tableau de PeriodeCongeAvecDateCommission des periodes posterieures &agrave 
	 * classees par ordre de date debut croissante
	 * @param editingContext
	 * @param date
	 * @return
	 */
	private NSArray preparerPeriodesPosterieures(EOEditingContext editingContext,NSTimestamp date) {
		NSMutableArray periodesPosterieures = new NSMutableArray();
		NSMutableArray congesEnUpdate = new NSMutableArray();	// tableau des congesId en update/correspondance. Utilisé pour ne pas prendre en compte ceux du SI Destinataire 
		NSArray conges = rechercherCongesImportCourantPourIndividuPosterieursDate(editingContext, date);
		// On recherche d'abord les congés de l'import courant
		// Les congés sont gardés comme des périodes afin de pouvoir trier et comparer les congés de l'import courant et du SI Destinataire
		java.util.Enumeration e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			CongeAvecArrete conge = (CongeAvecArrete)e.nextElement();
			periodesPosterieures.addObject(new PeriodeCongeAvecDateCommission(conge.dateDebut(), conge.dateFin(), conge.dateCommission()));
			if (conge.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || conge.operation().equals(ObjetImport.OPERATION_UPDATE)) {
				congesEnUpdate.addObject(conge.eimpSource());
			}
		}
		// rechercher les congés postérieurs du SI Destinataire
		conges = rechercherCongesPourIndividuPosterieursDansDestinatation(editingContext,date);
		e = conges.objectEnumerator();
		while (e.hasMoreElements()) {
			// il ne peut y en avoir qu'un, pas de superposition des congés
			MangueCongeAvecArreteAnnulation conge = (MangueCongeAvecArreteAnnulation)e.nextElement();
			CongeAvecArrete congeImport = (CongeAvecArrete)ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext, conge, entityName());
			// congeImport = nul, il s'agit d'un congé uniquement défini dans Mangue
			if (congeImport == null || congesEnUpdate.containsObject(congeImport.eimpSource()) == false) {
				periodesPosterieures.addObject(new PeriodeCongeAvecDateCommission(conge.dateDebut(), conge.dateFin(), conge.dateCommission()));
			}
		}
		EOSortOrdering.sortArrayUsingKeyOrderArray(periodesPosterieures, new NSArray(EOSortOrdering.sortOrderingWithKey("dateFin",EOSortOrdering.CompareDescending)));
		return (NSArray)periodesPosterieures;
	}
	/* Retourne les congés du SI Destinataire antérieurs à la date qui ne sont pas annulés.
	 *  Retourne null si il n'existe pas d'individu dans le SI Destinataire correspondant à l'individu
	 */
	private NSArray rechercherCongesPourIndividuAnterieursDansDestinatation(EOEditingContext editingContext,NSTimestamp date) {
		//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext,idSource());
		if (individu != null) {
			// On recherche les congés valides
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray qualifiers = new NSMutableArray();
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@",new NSArray(individuGrhum));
				qualifiers.addObject(qualifier);
				if (date != null) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateFin < %@",new NSArray(date)));
				}
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("noArreteAnnulation = nil AND dAnnulation = nil AND toCongeAnnulation = nil", null));
				EOFetchSpecification myFetch = new EOFetchSpecification (nomEntiteDestination(), new EOAndQualifier(qualifiers),null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			} else {
				return new NSArray();
			}
		} else {
			return new NSArray();
		}
	}
	/* Retourne les congés du SI Destinataire postérieurs à la date qui ne sont pas annulés.
	 *  Retourne null si il n'existe pas d'individu dans le SI Destinataire correspondant à l'individu
	 */
	private NSArray rechercherCongesPourIndividuPosterieursDansDestinatation(EOEditingContext editingContext,NSTimestamp date) {
		//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext,idSource());
		if (individu != null) {
			// On recherche les congés valides
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray qualifiers = new NSMutableArray();
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@",new NSArray(individuGrhum));
				qualifiers.addObject(qualifier);
				if (date != null) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateDebut > %@",new NSArray(date)));
				}
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("noArreteAnnulation = nil AND dAnnulation = nil AND toCongeAnnulation = nil", null));
				EOFetchSpecification myFetch = new EOFetchSpecification (nomEntiteDestination(), new EOAndQualifier(qualifiers),null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			} else {
				return new NSArray();
			}
		} else {
			return new NSArray();
		}
	}
//	private String verifierAbsencesMangue(EOEditingContext editingContext) {
//		String message = null;
//		EOIndividu individu = EOIndividuCorresp.individuPourSourceId(editingContext,idSource());
//		if (individu != null) {
//			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
//			if (individuGrhum != null) {
//				// on recherche les absences dans le SI Destinataire (au plus 1 puisque les périodes ne peuvent pas se chevaucher)
//				NSArray absences = EOMangueAbsences.rechercherAbsencesPourIndividuEtPeriode(editingContext, individuGrhum.noIndividu(), dateDebut(), dateFin());
//				// On scanne les absences pour savoir si elles correspondent au congé courant et pour vérifier dans le cas de congés légaux
//				// qu'ils sont valides
//				java.util.Enumeration e = absences.objectEnumerator();
//				while (e.hasMoreElements()) {
//					EOMangueAbsences absence = (EOMangueAbsences)e.nextElement();
//					if (absence.correspondCongeLegal()) {
//						MangueCongeLegal conge = absence.congePourAbsence(editingContext);
//						if (conge == null) {
//							// Il s'agit d'une congé légal qui n'est pas géré dans le connecteur
//							message = "CHEVAUCHEMENT_PERIODE_CONGE";
//						} else {
//							// Vérifier si le congé est valide
//							boolean congeValide = true;
//							if (conge instanceof MangueCongeAvecArreteAnnulation) {
//								congeValide = ((MangueCongeAvecArreteAnnulation)conge).congeValide();
//							}
//							if (congeValide) {
//								// Si il s'agit d'une insertion ou si il s'agit d'une correspondance ou d'un update 
//								// et qu'il ne s'agit pas du congé correspondant dans le SI Destinataire alors il y a chevauchement
//								if (this.operation().equals(ObjetImport.OPERATION_INSERTION)) {
//									return "CHEVAUCHEMENT_PERIODE_DESTIN";
//								} else {
//									MangueCongeLegal congeCorrespondant = (MangueCongeLegal)this.objetDestinataireAvecOuSansCorrespondance(editingContext);
//									if (congeCorrespondant != conge) {
//										return "CHEVAUCHEMENT_PERIODE_DESTIN";
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//		return message;
//	}
	private boolean couvrePeriodeComplete(NSMutableArray intervalles, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		// Trier les congés par ordre de date début croissante et date fin croissante
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("debutPeriode", EOSortOrdering.CompareAscending));
		EOSortOrdering.sortArrayUsingKeyOrderArray(intervalles, sorts);
		IntervalleTemps intervalle = (IntervalleTemps)intervalles.objectAtIndex(0);
		if (DateCtrl.isAfter(intervalle.debutPeriode(),debutPeriode)) {
			return false;
		}
		NSTimestamp dateFin = null;
		if (intervalle.finPeriode() != null && DateCtrl.isBefore(intervalle.finPeriode(), finPeriode)) {
			dateFin = intervalle.finPeriode();
		} else {
			return true;	// on couvre toute la période
		}
		for (int i = 1; i < intervalles.count();i++) {
			intervalle = (IntervalleTemps)intervalles.objectAtIndex(i);
			if (DateCtrl.isAfter(intervalle.debutPeriode(),dateFin)) {	// l'intervalle commence après le précédent
				if (DateCtrl.isSameDay(DateCtrl.jourSuivant(dateFin), intervalle.debutPeriode())) { // il commence le jour suivant
					dateFin = intervalle.finPeriode();
				} else {
					return false;	// il y a un trou;
				}
			} else {  // l'intervalle est à cheval avec le précédent
				if (intervalle.finPeriode() != null && DateCtrl.isBefore(dateFin, intervalle.finPeriode())) {
					dateFin = intervalle.finPeriode(); // on prend le max des dates de fin
				}
			}
			if (dateFin == null || DateCtrl.isAfter(dateFin, finPeriode)) {
				// on a terminé
				return true;
			}
		}
		return dateFin == null || DateCtrl.isAfter(dateFin, finPeriode);
	}
	// Retourne la liste de tous les congés légaux gérés pour un individu et une période */
	private NSArray rechercherCongesLegauxPourIndividuEtPeriode(EOEditingContext editingContext, Number idSource,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray conges = new NSMutableArray();
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "CongeMaladie", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "Clm", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "Cld", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "CongeMaladieSsTrt", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "CongeAccidentServ", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "CgntMaladie", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "CgntCgm", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "CgntAccidentTrav", idSource, debutPeriode, finPeriode, false));
		conges.addObjectsFromArray(ObjetImportPourIndividuEtPeriode.rechercherObjetsImportPourIndividuEtPeriode(editingContext, "CongeMaternite", idSource, debutPeriode, finPeriode, false));
		return conges;
	}
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		resultat = resultat + verifierTemoin(valeursAttributs, "temConfirme");
		resultat = resultat + verifierTemoin(valeursAttributs, "temGestEtab");
		return resultat;
	}
	/** Retourne les conges du SI Destinataire valides sur la periode
	 *  Si dateDebut est nulle, retourne tous les conges du SI Destinataire qui ne sont pas annules.
	 *  @param qualifierValiditeConge : qualifier de validite des conges dans le SI Destinataire. Peut
	 *  etre nul.
	 *  Retourne null si il n'existe pas d'individu dans le SI Destinataire correspondant a l'individu passe
	 *  en parametre
	 *
	 */
	public static NSArray rechercherCongesPourIndividuEtPeriodeDansDestinatation(EOEditingContext editingContext,String nomEntiteDestination,Number idSource, NSTimestamp dateDebut,NSTimestamp dateFin,EOQualifier qualifierValiditeConge) {
		//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext,idSource);
		if (individu != null) {
			// On recherche les congés valides
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray qualifiers = new NSMutableArray();
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@",new NSArray(individuGrhum));
				qualifiers.addObject(qualifier);
				qualifiers.addObject(SuperFinder.qualifierPourPeriode("dateDebut", dateDebut, "dateFin", dateFin));
				if (qualifierValiditeConge != null) {
					qualifiers.addObject(qualifierValiditeConge);
				}
				EOFetchSpecification myFetch = new EOFetchSpecification (nomEntiteDestination, new EOAndQualifier(qualifiers),null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/** Retourne les conges du SI Destinataire commencant a date qui ne sont pas annules.
	 *  Si date est nulle, retourne tous les conges du SI Destinataire qui ne sont pas annules.
	 *  @param qualifierValiditeConge : qualifier de validite des conges dans le SI Destinataire. Peut
	 *  etre nul.
	 *  Retourne null si il n'existe pas d'individu dans le SI Destinataire correspondant a l'individu passe
	 *  en parametre
	 *
	 */
	public static NSArray rechercherCongesPourIndividuEtDateDebutDansDestinatation(EOEditingContext editingContext,String nomEntiteDestination,Number idSource, NSTimestamp date,EOQualifier qualifierValiditeConge) {
		//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext,idSource);
		if (individu != null) {
			// On recherche les congés valides
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray qualifiers = new NSMutableArray();
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@",new NSArray(individuGrhum));
				qualifiers.addObject(qualifier);
				if (date != null) {
					// Vérifier que la date de début est la même : pour éviter les problèmes de comparaison
					// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
					NSMutableArray args = new NSMutableArray();
					args.addObject(DateCtrl.jourPrecedent(date));
					args.addObject(DateCtrl.jourSuivant(date));
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateDebut > %@ AND dateDebut < %@",args));
				}

				if (qualifierValiditeConge != null) {
					qualifiers.addObject(qualifierValiditeConge);
				}
				EOFetchSpecification myFetch = new EOFetchSpecification (nomEntiteDestination, new EOAndQualifier(qualifiers),null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** Retourne les conges du SI Destinataire se terminant a date qui ne sont pas annules.
	 *  Si date est nulle, retourne tous les conges du SI Destinataire qui ne sont pas annules.
	 *  @param qualifierValiditeConge : qualifier de validite des conges dans le SI Destinataire. Peut
	 *  etre nul.
	 *  Retourne null si il n'existe pas d'individu dans le SI Destinataire correspondant a l'individu passe
	 *  en param&egrave;tre
	 */
	public static NSArray rechercherCongesPourIndividuEtDateFinDansDestinatation(EOEditingContext editingContext,String nomEntiteDestination,Number idSource, NSTimestamp date,EOQualifier qualifierValiditeConge) {
		//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext,idSource);
		if (individu != null) {
			// On recherche les congés valides
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray qualifiers = new NSMutableArray();
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@",new NSArray(individuGrhum));
				qualifiers.addObject(qualifier);
				if (date != null) {
					// Vérifier que la date de début est la même : pour éviter les problèmes de comparaison
					// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
					NSMutableArray args = new NSMutableArray();
					args.addObject(DateCtrl.jourPrecedent(date));
					args.addObject(DateCtrl.jourSuivant(date));
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateFin > %@ AND dateFin < %@",args));
				}
				if (qualifierValiditeConge != null) {
					qualifiers.addObject(qualifierValiditeConge);
				}
				EOFetchSpecification myFetch = new EOFetchSpecification (nomEntiteDestination, new EOAndQualifier(qualifiers),null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** retourne le parametrage de duree pour la cle fournie en parametre de la table des parametres des absences */
	public static int dureePourCle(EOEditingContext editingContext,String cle) {
		if (parametresConges == null) {
			EOFetchSpecification fs = new EOFetchSpecification("ParamCongesAbsences",null,null);
			parametresConges = (EOGenericRecord)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		Integer num = (Integer)parametresConges.valueForKey(cle);
		if (num == null) {
			// erreur, clé fournie est erronnée
			return 0;
		} else {
			return num.intValue();
		}
	}

	@Override
	protected boolean estAnnule() {
		return dAnnulation()!=null || noArreteAnnulation()!=null;
	}
}
