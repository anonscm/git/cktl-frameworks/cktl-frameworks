// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODelegation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODelegation extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Delegation";

	// Attributes
	public static final String C_MOD_DELEGATION_KEY = "cModDelegation";
	public static final String C_MOT_DELEGATION_KEY = "cMotDelegation";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CONV_DELEGATION_KEY = "dConvDelegation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DELEGATION_SOURCE_KEY = "delegationSource";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LIEU_DELEGATION_KEY = "lieuDelegation";
	public static final String MONTANT_DELEGATION_KEY = "montantDelegation";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_ANNUEL_DELEGATION_KEY = "temAnnuelDelegation";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_TYPE_MOD_DELEGATION_KEY = "toTypeModDelegation";
	public static final String TO_TYPE_MOTIF_DELEGATION_KEY = "toTypeMotifDelegation";

  private static Logger LOG = Logger.getLogger(_EODelegation.class);

  public EODelegation localInstanceIn(EOEditingContext editingContext) {
    EODelegation localInstance = (EODelegation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cModDelegation() {
    return (String) storedValueForKey("cModDelegation");
  }

  public void setCModDelegation(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating cModDelegation from " + cModDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "cModDelegation");
  }

  public String cMotDelegation() {
    return (String) storedValueForKey("cMotDelegation");
  }

  public void setCMotDelegation(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating cMotDelegation from " + cMotDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotDelegation");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dConvDelegation() {
    return (NSTimestamp) storedValueForKey("dConvDelegation");
  }

  public void setDConvDelegation(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dConvDelegation from " + dConvDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "dConvDelegation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer delegationSource() {
    return (Integer) storedValueForKey("delegationSource");
  }

  public void setDelegationSource(Integer value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating delegationSource from " + delegationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "delegationSource");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String lieuDelegation() {
    return (String) storedValueForKey("lieuDelegation");
  }

  public void setLieuDelegation(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating lieuDelegation from " + lieuDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDelegation");
  }

  public java.math.BigDecimal montantDelegation() {
    return (java.math.BigDecimal) storedValueForKey("montantDelegation");
  }

  public void setMontantDelegation(java.math.BigDecimal value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating montantDelegation from " + montantDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "montantDelegation");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer numQuotite() {
    return (Integer) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Integer value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temAnnuelDelegation() {
    return (String) storedValueForKey("temAnnuelDelegation");
  }

  public void setTemAnnuelDelegation(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating temAnnuelDelegation from " + temAnnuelDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnuelDelegation");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne toRne() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation toTypeModDelegation() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation)storedValueForKey("toTypeModDelegation");
  }

  public void setToTypeModDelegationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toTypeModDelegation from " + toTypeModDelegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation oldValue = toTypeModDelegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeModDelegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeModDelegation");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation toTypeMotifDelegation() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation)storedValueForKey("toTypeMotifDelegation");
  }

  public void setToTypeMotifDelegationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toTypeMotifDelegation from " + toTypeMotifDelegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation oldValue = toTypeMotifDelegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeMotifDelegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeMotifDelegation");
    }
  }
  

  public static EODelegation createDelegation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temValide
) {
    EODelegation eo = (EODelegation) EOUtilities.createAndInsertInstance(editingContext, _EODelegation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EODelegation> fetchAllDelegations(EOEditingContext editingContext) {
    return _EODelegation.fetchAllDelegations(editingContext, null);
  }

  public static NSArray<EODelegation> fetchAllDelegations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODelegation.fetchDelegations(editingContext, null, sortOrderings);
  }

  public static NSArray<EODelegation> fetchDelegations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODelegation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODelegation> eoObjects = (NSArray<EODelegation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODelegation fetchDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EODelegation.fetchDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODelegation fetchDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODelegation> eoObjects = _EODelegation.fetchDelegations(editingContext, qualifier, null);
    EODelegation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODelegation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Delegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODelegation fetchRequiredDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EODelegation.fetchRequiredDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODelegation fetchRequiredDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    EODelegation eoObject = _EODelegation.fetchDelegation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Delegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODelegation localInstanceIn(EOEditingContext editingContext, EODelegation eo) {
    EODelegation localInstance = (eo == null) ? null : (EODelegation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
