// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOReculAge.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOReculAge extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "ReculAge";

	// Attributes
	public static final String C_MOTIF_RECUL_KEY = "cMotifRecul";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_FIN_EXECUTION_KEY = "dateFinExecution";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ENF_SOURCE1_KEY = "enfSource1";
	public static final String ENF_SOURCE2_KEY = "enfSource2";
	public static final String ENF_SOURCE3_KEY = "enfSource3";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String REC_SOURCE_KEY = "recSource";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_ENFANT1_KEY = "toEnfant1";
	public static final String TO_ENFANT2_KEY = "toEnfant2";
	public static final String TO_ENFANT3_KEY = "toEnfant3";

  private static Logger LOG = Logger.getLogger(_EOReculAge.class);

  public EOReculAge localInstanceIn(EOEditingContext editingContext) {
    EOReculAge localInstance = (EOReculAge)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifRecul() {
    return (String) storedValueForKey("cMotifRecul");
  }

  public void setCMotifRecul(String value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating cMotifRecul from " + cMotifRecul() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifRecul");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dateFinExecution() {
    return (NSTimestamp) storedValueForKey("dateFinExecution");
  }

  public void setDateFinExecution(NSTimestamp value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating dateFinExecution from " + dateFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFinExecution");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer enfSource1() {
    return (Integer) storedValueForKey("enfSource1");
  }

  public void setEnfSource1(Integer value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating enfSource1 from " + enfSource1() + " to " + value);
    }
    takeStoredValueForKey(value, "enfSource1");
  }

  public Integer enfSource2() {
    return (Integer) storedValueForKey("enfSource2");
  }

  public void setEnfSource2(Integer value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating enfSource2 from " + enfSource2() + " to " + value);
    }
    takeStoredValueForKey(value, "enfSource2");
  }

  public Integer enfSource3() {
    return (Integer) storedValueForKey("enfSource3");
  }

  public void setEnfSource3(Integer value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating enfSource3 from " + enfSource3() + " to " + value);
    }
    takeStoredValueForKey(value, "enfSource3");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer recSource() {
    return (Integer) storedValueForKey("recSource");
  }

  public void setRecSource(Integer value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating recSource from " + recSource() + " to " + value);
    }
    takeStoredValueForKey(value, "recSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
    	_EOReculAge.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
      _EOReculAge.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant toEnfant1() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("toEnfant1");
  }

  public void setToEnfant1Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
      _EOReculAge.LOG.debug("updating toEnfant1 from " + toEnfant1() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = toEnfant1();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant1");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant1");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant toEnfant2() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("toEnfant2");
  }

  public void setToEnfant2Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
      _EOReculAge.LOG.debug("updating toEnfant2 from " + toEnfant2() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = toEnfant2();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant2");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant2");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant toEnfant3() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("toEnfant3");
  }

  public void setToEnfant3Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOReculAge.LOG.isDebugEnabled()) {
      _EOReculAge.LOG.debug("updating toEnfant3 from " + toEnfant3() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = toEnfant3();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant3");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant3");
    }
  }
  

  public static EOReculAge createReculAge(EOEditingContext editingContext, String cMotifRecul
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, Integer recSource
, String statut
, String temImport
, String temValide
) {
    EOReculAge eo = (EOReculAge) EOUtilities.createAndInsertInstance(editingContext, _EOReculAge.ENTITY_NAME);    
		eo.setCMotifRecul(cMotifRecul);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setRecSource(recSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOReculAge> fetchAllReculAges(EOEditingContext editingContext) {
    return _EOReculAge.fetchAllReculAges(editingContext, null);
  }

  public static NSArray<EOReculAge> fetchAllReculAges(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOReculAge.fetchReculAges(editingContext, null, sortOrderings);
  }

  public static NSArray<EOReculAge> fetchReculAges(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOReculAge.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOReculAge> eoObjects = (NSArray<EOReculAge>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOReculAge fetchReculAge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReculAge.fetchReculAge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReculAge fetchReculAge(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOReculAge> eoObjects = _EOReculAge.fetchReculAges(editingContext, qualifier, null);
    EOReculAge eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOReculAge)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ReculAge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReculAge fetchRequiredReculAge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReculAge.fetchRequiredReculAge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReculAge fetchRequiredReculAge(EOEditingContext editingContext, EOQualifier qualifier) {
    EOReculAge eoObject = _EOReculAge.fetchReculAge(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ReculAge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReculAge localInstanceIn(EOEditingContext editingContext, EOReculAge eo) {
    EOReculAge localInstance = (eo == null) ? null : (EOReculAge)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
