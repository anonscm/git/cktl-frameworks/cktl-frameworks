package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOOrgaBudget extends _EOOrgaBudget {
	private static final long serialVersionUID = -552287435917963441L;
	private static final NSTimestamp MAINTENANT = new NSTimestamp();

	public EOOrgaBudget() {
		super();
	}

	@Override
	public NSTimestamp dCreation() {
		// Redéfini DCreation : ce champ n'existe pas pour cette entité
		return MAINTENANT;
	}

	@Override
	public void setDCreation(NSTimestamp value) {
		// Redéfini DCreation : ce champ n'existe pas pour cette entité
	}

	@Override
	public NSTimestamp dModification() {
		// Redéfini DModification : ce champ n'existe pas pour cette entité
		return MAINTENANT;
	}

	@Override
	public void setDModification(NSTimestamp value) {
		// Redéfini DModification : ce champ n'existe pas pour cette entité
	}

	@Override
	public void preparerRelations() {
	}

	@Override
	public void supprimerRelations() {
	}

	/** retourne le nom de la relation pour retrouver les logs lies au record */
	public String nomRelationPourLog() {
		return EOLogImport.ORGA_BUDGET_KEY;
	}

	// Méthodes protégées
	/** Pas d'attribut de comparaison avec des relations */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}

	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	/** Pas de verification sur les relations */
	public EOLogImport verifierRecord() {
		// On ne crée pas la relation avec le père dans preparerRelation mais dans verifierRecord.
		// Dans verifierRecord, on est sur que le père a été ajouté en base même s'il est après dans le XML
		EOOrgaBudget orgaBudgetPere = orgaBudgetPourSourceId(editingContext(), obSourcePere());
		setToOrgaBudgetPereRelationship(orgaBudgetPere);
		
		EOLogImport log = null;

		String message = verifierOrgan(editingContext(), this);
		if (message != null) {
			LogManager.logDetail("Insertion d'un log pour les erreurs sur l'oraganigramme budgétaire");
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), message);
			log.setObSource(obSource());
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une erreur
		}

		return log;
	}

	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	// Méthodes privées
	private String verifierOrgan(EOEditingContext editingContext, EOOrgaBudget organ) {

		if (obLib() == null) {
			return "OB_LIB_NON_NUL";
		}

		if (obDateOuverture() == null) {
			return "OB_DATE_OUVERTURE_NON_NUL";
		}

		if (obDateOuverture() != null && obDateCloture() != null && DateCtrl.isBefore(obDateCloture(), obDateOuverture())) {
			return "DATE_CLOTURE_ANTERIEURE";
		}

		if (obNiv() == null) {
			return "NIVEAU_INDEFINI";
		}

		if (toOrgaBudgetPere() == null && obSourcePere() != null) {
			return "ORGAN_PERE_NON_IMPORTE";
		} else if (toOrgaBudgetPere() != null && toOrgaBudgetPere().statut().equals(STATUT_ERREUR)) {
			return "ORGAN_PERE_INVALIDE";
		}
		return null;
	}

	private static Integer determineNiveau(String obEtab, String obUb, String obCr, String obSousCr) {
		Integer niveau = null;
		boolean hasEtab = false;
		boolean hasUb = false;
		boolean hasCr = false;
		boolean hasSousCr = false;

		hasEtab = obEtab != null && !obEtab.isEmpty();
		hasUb = obUb != null && !obUb.isEmpty();
		hasCr = obCr != null && !obCr.isEmpty();
		hasSousCr = obSousCr != null && !obSousCr.isEmpty();

		if (!hasEtab && !hasUb && !hasCr && !hasSousCr) {
			niveau = new Integer(0);
		}
		if (hasEtab && !hasUb && !hasCr && !hasSousCr) {
			niveau = new Integer(1);
		}
		if (hasEtab && hasUb && !hasCr && !hasSousCr) {
			niveau = new Integer(2);
		}
		if (hasEtab && hasUb && hasCr && !hasSousCr) {
			niveau = new Integer(3);
		}
		if (hasEtab && hasUb && hasCr && hasSousCr) {
			niveau = new Integer(4);
		}

		return niveau;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";

		// Initialisation des champs
		if (valeursAttributs.objectForKey(OB_UNIV_KEY) == null)
			valeursAttributs.setObjectForKey("UNIV", OB_UNIV_KEY);

		if (valeursAttributs.objectForKey(OB_LUCRATIVITE_KEY) == null)
			valeursAttributs.setObjectForKey(new Integer(0), OB_LUCRATIVITE_KEY);

		if (valeursAttributs.objectForKey(TYOR_ID_KEY) == null)
			valeursAttributs.setObjectForKey(new Integer(1), TYOR_ID_KEY);

		if (valeursAttributs.objectForKey(OB_NIV_KEY) == null) {
			Integer niveau = determineNiveau((String) valeursAttributs.objectForKey(OB_ETAB_KEY), (String) valeursAttributs.objectForKey(OB_UB_KEY),
					(String) valeursAttributs.objectForKey(OB_CR_KEY), (String) valeursAttributs.objectForKey(OB_SOUS_CR_KEY));
			valeursAttributs.setObjectForKey(niveau, OB_NIV_KEY);
		}

		return resultat;
	}

	public static EOOrgaBudget orgaBudgetPourSourceId(EOEditingContext editingContext, Integer obSource) {
		if (obSource == null || editingContext == null)
			return null;

		EOOrgaBudget resultat = null;
		
		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, OB_SOURCE_KEY, obSource);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOOrgaBudget)premierObjetValide(resultats);

		return resultat;
	}

}
