// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPersBudget.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPersBudget extends ObjetImportPourIndividu {
	public static final String ENTITY_NAME = "PersBudget";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EMP_SOURCE_KEY = "empSource";
	public static final String IDELT_KEY = "idelt";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String MOIS_CODE_DEBUT_KEY = "moisCodeDebut";
	public static final String MOIS_CODE_FIN_KEY = "moisCodeFin";
	public static final String OB_SOURCE_KEY = "obSource";
	public static final String OPERATION_KEY = "operation";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PBUD_SOURCE_KEY = "pbudSource";
	public static final String POURCENTAGE_KEY = "pourcentage";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String EMPLOI_KEY = "emploi";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_KX_ELEMENT_KEY = "toKxElement";
	public static final String TO_ORGA_BUDGET_KEY = "toOrgaBudget";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";

  private static Logger LOG = Logger.getLogger(_EOPersBudget.class);

  public EOPersBudget localInstanceIn(EOEditingContext editingContext) {
    EOPersBudget localInstance = (EOPersBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer empSource() {
    return (Integer) storedValueForKey("empSource");
  }

  public void setEmpSource(Integer value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating empSource from " + empSource() + " to " + value);
    }
    takeStoredValueForKey(value, "empSource");
  }

  public String idelt() {
    return (String) storedValueForKey("idelt");
  }

  public void setIdelt(String value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating idelt from " + idelt() + " to " + value);
    }
    takeStoredValueForKey(value, "idelt");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer moisCodeDebut() {
    return (Integer) storedValueForKey("moisCodeDebut");
  }

  public void setMoisCodeDebut(Integer value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating moisCodeDebut from " + moisCodeDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCodeDebut");
  }

  public Integer moisCodeFin() {
    return (Integer) storedValueForKey("moisCodeFin");
  }

  public void setMoisCodeFin(Integer value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating moisCodeFin from " + moisCodeFin() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCodeFin");
  }

  public Integer obSource() {
    return (Integer) storedValueForKey("obSource");
  }

  public void setObSource(Integer value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating obSource from " + obSource() + " to " + value);
    }
    takeStoredValueForKey(value, "obSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer orgId() {
    return (Integer) storedValueForKey("orgId");
  }

  public void setOrgId(Integer value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating orgId from " + orgId() + " to " + value);
    }
    takeStoredValueForKey(value, "orgId");
  }

  public Integer pbudSource() {
    return (Integer) storedValueForKey("pbudSource");
  }

  public void setPbudSource(Integer value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating pbudSource from " + pbudSource() + " to " + value);
    }
    takeStoredValueForKey(value, "pbudSource");
  }

  public Double pourcentage() {
    return (Double) storedValueForKey("pourcentage");
  }

  public void setPourcentage(Double value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating pourcentage from " + pourcentage() + " to " + value);
    }
    takeStoredValueForKey(value, "pourcentage");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
    	_EOPersBudget.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi emploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi)storedValueForKey("emploi");
  }

  public void setEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
      _EOPersBudget.LOG.debug("updating emploi from " + emploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi oldValue = emploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
      _EOPersBudget.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.jefy.EOKxElement toKxElement() {
    return (org.cocktail.connecteur.common.modele.jefy.EOKxElement)storedValueForKey("toKxElement");
  }

  public void setToKxElementRelationship(org.cocktail.connecteur.common.modele.jefy.EOKxElement value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
      _EOPersBudget.LOG.debug("updating toKxElement from " + toKxElement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.jefy.EOKxElement oldValue = toKxElement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toKxElement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toKxElement");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget toOrgaBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget)storedValueForKey("toOrgaBudget");
  }

  public void setToOrgaBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
      _EOPersBudget.LOG.debug("updating toOrgaBudget from " + toOrgaBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget oldValue = toOrgaBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toOrgaBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toOrgaBudget");
    }
  }
  
  public org.cocktail.connecteur.common.modele.jefy.EOTypeCredit toTypeCredit() {
    return (org.cocktail.connecteur.common.modele.jefy.EOTypeCredit)storedValueForKey("toTypeCredit");
  }

  public void setToTypeCreditRelationship(org.cocktail.connecteur.common.modele.jefy.EOTypeCredit value) {
    if (_EOPersBudget.LOG.isDebugEnabled()) {
      _EOPersBudget.LOG.debug("updating toTypeCredit from " + toTypeCredit() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.jefy.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeCredit");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeCredit");
    }
  }
  

  public static EOPersBudget createPersBudget(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer moisCodeDebut
, Integer pbudSource
, Double pourcentage
, String statut
, String temImport
, String temValide
, org.cocktail.connecteur.common.modele.jefy.EOTypeCredit toTypeCredit) {
    EOPersBudget eo = (EOPersBudget) EOUtilities.createAndInsertInstance(editingContext, _EOPersBudget.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setMoisCodeDebut(moisCodeDebut);
		eo.setPbudSource(pbudSource);
		eo.setPourcentage(pourcentage);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    eo.setToTypeCreditRelationship(toTypeCredit);
    return eo;
  }

  public static NSArray<EOPersBudget> fetchAllPersBudgets(EOEditingContext editingContext) {
    return _EOPersBudget.fetchAllPersBudgets(editingContext, null);
  }

  public static NSArray<EOPersBudget> fetchAllPersBudgets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPersBudget.fetchPersBudgets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPersBudget> fetchPersBudgets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPersBudget.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPersBudget> eoObjects = (NSArray<EOPersBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPersBudget fetchPersBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudget.fetchPersBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudget fetchPersBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPersBudget> eoObjects = _EOPersBudget.fetchPersBudgets(editingContext, qualifier, null);
    EOPersBudget eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPersBudget)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PersBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudget fetchRequiredPersBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudget.fetchRequiredPersBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudget fetchRequiredPersBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPersBudget eoObject = _EOPersBudget.fetchPersBudget(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PersBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudget localInstanceIn(EOEditingContext editingContext, EOPersBudget eo) {
    EOPersBudget localInstance = (eo == null) ? null : (EOPersBudget)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
