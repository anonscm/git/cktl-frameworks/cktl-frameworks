//EOAffectation.java
//Created on Wed Jul 25 15:04:40 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.PeriodeAvecQuotite;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConditionRecrutement;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEmploiCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueAffectation;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 07/02/2011 - correction d'un bug dans l'évaluation de la quotité d'affectation, il ne faut pas prendre en compte dans le
// calcul de la quotité des affectations en update suite à un changement de dates + correction d'un bug sur la recherche des affectations valides
public class EOAffectation extends _EOAffectation {

	public EOAffectation() {
		super();
	}

	/**
	 * 
	 */
	public void preparerRelations() {
		super.preparerRelations();

		EOContrat contrat = EOContrat.contratPourSourceId(editingContext(), idSource(), ctrSource());
		setContratRelationship(contrat);

		EOCarriere carriere = EOCarriere.carrierePourSourceId(editingContext(), idSource(), carSource());
		setToCarriereRelationship(carriere);
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		String messageLog = null;

		if (log == null) {
			if (ctrSource() != null && individu() != null) {
				if (contrat() == null) {
					messageLog = "CONTRAT_NON_IMPORTE";
				} else if (!contrat().statut().equals(ObjetImport.STATUT_VALIDE))
					messageLog = "CONTRAT_IMPORT_INVALIDE";
			}
			if (carSource() != null && individu() != null) {
				if (toCarriere() == null) {
					messageLog = "CARRIERE_NON_IMPORTEE";
				} else if (!toCarriere().statut().equals(ObjetImport.STATUT_VALIDE))
					messageLog = "CARRIERE_INVALIDE";
			}
			if (individu() != null && !individu().temPersonnel().equals(CocktailConstantes.VRAI))
				messageLog = "TYPE_PERSONNEL";

			if (messageLog != null) {
				log = new EOLogImport();
				log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
				log.ajouterRelation(this);

				setStatut(STATUT_ERREUR);
				return log;
			} else {
				return null;
			}
		} else {
			return log;
		}
	}

	/**
	 * Verifie que la quotite totale des affectations de cet individu pendant la periode est inferieure ou egale a 100
	 */
	public EOLogImport verifierRecordApresOperation() {
		// Pour déterminer la quotité totale, il faut ajouter :
		// - la quotité de toutes les affectations valides pour la période en
		// cours d'import
		// - la quotité de toutes les autres affectations valides pour la
		// période du SI Destinataire qui ne sont pas dans l'import courant
		// Rechercher toutes les affectations qui viennent d'être importées
		// 07/02/2011 - Il ne faut pas prendre en compte les affectations de la
		// destination qui sont updatées par l'import courant (cas des
		// changements de date d'affectations)
		NSArray affectationsImport = rechercherAffectationsValidesImportCourantPourIndividuEtPeriode();
		NSMutableArray affectationsDestin = new NSMutableArray();
		LogManager.logDetail("affectations import" + affectationsImport.count());
		NSMutableArray periodesAvecQuotite = new NSMutableArray();
		// l'affectation n'a pas encore été insérée dans la base, il faut donc la prendre en compte
		periodesAvecQuotite.addObject(new PeriodeAvecQuotite(this.dDebAffectation(), this.dFinAffectation(), this.numQuotAffectation()));
		// ne pas prendre en compte la quotité de la destination si elle existe
		ajouterAffectationDestinPourAffectation(affectationsDestin, this);

		java.util.Enumeration e = affectationsImport.objectEnumerator();
		while (e.hasMoreElements()) {
			EOAffectation affectation = (EOAffectation) e.nextElement();
			if (affectation != this) {
				LogManager.logDetail("affectation courante - individu : " + affectation.individu().idSource() + ", structure : "
						+ affectation.structure().strSource() + ", source : " + affectation.affSource());
				periodesAvecQuotite.addObject(new PeriodeAvecQuotite(affectation.dDebAffectation(), affectation.dFinAffectation(), affectation
						.numQuotAffectation()));
				// ne pas prendre en compte la quotité de la destination si elle existe
				ajouterAffectationDestinPourAffectation(affectationsDestin, affectation);
			}
		}
		LogManager.logDetail("Affectations destin deja vues " + affectationsDestin.count());
		// Rechercher toutes les affectations déjà dans le SI Destinataire
		NSArray affectations = EOMangueAffectation.rechercherAffectationsValidesPourIndividuEtPeriode(editingContextTravail(), this);
		if (affectations != null) { // Null si individu affectations pas encore créé
			LogManager.logDetail("nb affectations SI dest " + affectations.count());
			NSArray affectationsImportValides = rechercherAffectationsValidesImportCourantPourIndividu();
			// On se base sur les affSource pour comparer les affectations
			affectationsImportValides = (NSArray) affectationsImportValides.valueForKey("affSource");
			e = affectations.objectEnumerator();
			while (e.hasMoreElements()) {
				EOMangueAffectation affectationMangue = (EOMangueAffectation) e.nextElement();
				// LogManager.logDetail("affectation courante " + affectationMangue.noSeqAffectation());
				if (affectationsDestin.containsObject(affectationMangue) == false) {
					// 07/02/2011 - si il existe une affectation d'import qui n'est pas dans l'import courant
					EOAffectation affectationImport = (EOAffectation) ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext(),
							affectationMangue, "Affectation");
					// Il faut comparer les affSources car affectationImport correspond à une affectation déjà importée
					if (affectationImport != null && affectationsImportValides.containsObject(affectationImport.affSource()) == false) {
						periodesAvecQuotite.addObject(new PeriodeAvecQuotite(affectationMangue.dDebAffectation(), affectationMangue.dFinAffectation(),
								affectationMangue.numQuotAffectation()));
						affectationsDestin.addObject(affectationMangue);
					}
				}
			}
		}
		// Vérifier la quotité totale
		Number quotiteTotale = PeriodeAvecQuotite.calculerQuotiteTotale(periodesAvecQuotite);
		LogManager.logDetail("quotite finale " + quotiteTotale);
		if (quotiteTotale != null && quotiteTotale.doubleValue() > 100.00) {
			LogManager.logDetail("Insertion d'un log pour la quotite d'affectation");
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), "QUOTITE_AFFECTATION_INVALIDE");
			log.setStrSource(strSource());
			if (structure() != null) {
				log.setStructureRelationship(structure());
			}
			log.setIdSource(individu().idSource());
			if (individu() != null) {
				log.setIndividuRelationship(individu());
			}
			log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
			setStatut(STATUT_ERREUR);
			return log;
		}
		return null;
	}

	/**
	 * Affectation du SI destinataire avec la m&ecirc;me structure, le meme individu et une periode chevauchant la periode de cette affectation
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null && structure() != null) {
			if (estNouvelIndividuOuStructure()) { // A fortiori l'affectation est nouvelle
				return null;
			} else {
				return EOMangueAffectation.affectationDestinationPourAffectation(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	public String nomRelationPourLog() {
		return EOLogImport.AFFECTATION_KEY;
	}

	// Méthodes protégées
	protected boolean doitAvoirIndividuEtStructure() {
		return true;
	}

	// Méthodes privées
	/**
	 * retourne les affectations valides qui sont importées dans l'import courant pour l'individu de l'affectation et la même période.
	 */
	private NSArray rechercherAffectationsValidesImportCourantPourIndividuEtPeriode() {
		return rechercherAffectationsValidesImportCourantPourIndividu(true);

	}

	/**
	 * retourne les affectations valides qui sont importées dans l'import courant pour l'individu de l'affectation
	 */
	private NSArray rechercherAffectationsValidesImportCourantPourIndividu() {
		return rechercherAffectationsValidesImportCourantPourIndividu(false);

	}

	private NSArray rechercherAffectationsValidesImportCourantPourIndividu(boolean uniquementPourPeriode) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND statut <> 'A' AND statut <> 'E'", new NSArray(individu()));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		if (uniquementPourPeriode) {
			qualifiers.addObject(Finder.qualifierPourPeriode("dDebAffectation", dDebAffectation(), "dFinAffectation", dFinAffectation()));
		}
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("Affectation", qualifier, null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContextTravail().objectsWithFetchSpecification(myFetch);

	}

	private void ajouterAffectationDestinPourAffectation(NSMutableArray affectationsDestin, EOAffectation affectation) {
		if (operation().equals(OPERATION_INSERTION) == false) {
			EOMangueAffectation affectationDestin = (EOMangueAffectation) this.objetDestinataireAvecOuSansCorrespondance(editingContextTravail());
			if (affectationDestin != null) {
				affectationsDestin.addObject(affectationDestin);
			}
		}
	}

	// Méthodes statiques
	/** Verification que la quotite n'est pas plus grande que 100% */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey("dFinAffectation");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp) valeursAttributs.objectForKey("dDebAffectation");
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		double quotite = ((Number) valeursAttributs.objectForKey("numQuotAffectation")).doubleValue(); // déjà transformé en number et attribut obligatoire
		if (quotite == 0.00) {
			resultat += "Quotite_Nulle >>" + System.getProperty("line.separator");
		} else if (quotite > 100.00) {
			resultat += "Quotite_Invalide >>" + System.getProperty("line.separator");
		}
		return resultat;
	}
}