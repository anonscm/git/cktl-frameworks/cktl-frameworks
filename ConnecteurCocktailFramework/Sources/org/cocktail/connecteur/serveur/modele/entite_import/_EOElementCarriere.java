// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOElementCarriere.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOElementCarriere extends ObjetImportPourCarriere {
	public static final String ENTITY_NAME = "ElementCarriere";

	// Attributes
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String C_CHEVRON_KEY = "cChevron";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_TYPE_ACCES_KEY = "cTypeAcces";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRETE_CARRIERE_KEY = "dArreteCarriere";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_EFFET_ELEMENT_KEY = "dEffetElement";
	public static final String D_FIN_ELEMENT_KEY = "dFinElement";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ELC_ORDRE_KEY = "elcOrdre";
	public static final String EL_SOURCE_KEY = "elSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String INDICE_EFFECTIF_KEY = "indiceEffectif";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NO_ARRETE_CARRIERE_KEY = "noArreteCarriere";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_ELEMENT_KEY = "quotiteElement";
	public static final String REP_ANC_ECH_ANNEES_KEY = "repAncEchAnnees";
	public static final String REP_ANC_ECH_JOURS_KEY = "repAncEchJours";
	public static final String REP_ANC_ECH_MOIS_KEY = "repAncEchMois";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PROVISOIRE_KEY = "temProvisoire";
	public static final String TEM_SIGNE_KEY = "temSigne";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOElementCarriere.class);

  public EOElementCarriere localInstanceIn(EOEditingContext editingContext) {
    EOElementCarriere localInstance = (EOElementCarriere)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public String cChevron() {
    return (String) storedValueForKey("cChevron");
  }

  public void setCChevron(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating cChevron from " + cChevron() + " to " + value);
    }
    takeStoredValueForKey(value, "cChevron");
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String cTypeAcces() {
    return (String) storedValueForKey("cTypeAcces");
  }

  public void setCTypeAcces(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating cTypeAcces from " + cTypeAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAcces");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArreteCarriere() {
    return (NSTimestamp) storedValueForKey("dArreteCarriere");
  }

  public void setDArreteCarriere(NSTimestamp value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating dArreteCarriere from " + dArreteCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dArreteCarriere");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dEffetElement() {
    return (NSTimestamp) storedValueForKey("dEffetElement");
  }

  public void setDEffetElement(NSTimestamp value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating dEffetElement from " + dEffetElement() + " to " + value);
    }
    takeStoredValueForKey(value, "dEffetElement");
  }

  public NSTimestamp dFinElement() {
    return (NSTimestamp) storedValueForKey("dFinElement");
  }

  public void setDFinElement(NSTimestamp value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating dFinElement from " + dFinElement() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinElement");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer elcOrdre() {
    return (Integer) storedValueForKey("elcOrdre");
  }

  public void setElcOrdre(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating elcOrdre from " + elcOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "elcOrdre");
  }

  public Integer elSource() {
    return (Integer) storedValueForKey("elSource");
  }

  public void setElSource(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating elSource from " + elSource() + " to " + value);
    }
    takeStoredValueForKey(value, "elSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer indiceEffectif() {
    return (Integer) storedValueForKey("indiceEffectif");
  }

  public void setIndiceEffectif(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating indiceEffectif from " + indiceEffectif() + " to " + value);
    }
    takeStoredValueForKey(value, "indiceEffectif");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String noArreteCarriere() {
    return (String) storedValueForKey("noArreteCarriere");
  }

  public void setNoArreteCarriere(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating noArreteCarriere from " + noArreteCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteCarriere");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer quotiteElement() {
    return (Integer) storedValueForKey("quotiteElement");
  }

  public void setQuotiteElement(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating quotiteElement from " + quotiteElement() + " to " + value);
    }
    takeStoredValueForKey(value, "quotiteElement");
  }

  public Integer repAncEchAnnees() {
    return (Integer) storedValueForKey("repAncEchAnnees");
  }

  public void setRepAncEchAnnees(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating repAncEchAnnees from " + repAncEchAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchAnnees");
  }

  public Integer repAncEchJours() {
    return (Integer) storedValueForKey("repAncEchJours");
  }

  public void setRepAncEchJours(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating repAncEchJours from " + repAncEchJours() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchJours");
  }

  public Integer repAncEchMois() {
    return (Integer) storedValueForKey("repAncEchMois");
  }

  public void setRepAncEchMois(Integer value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating repAncEchMois from " + repAncEchMois() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchMois");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temProvisoire() {
    return (String) storedValueForKey("temProvisoire");
  }

  public void setTemProvisoire(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating temProvisoire from " + temProvisoire() + " to " + value);
    }
    takeStoredValueForKey(value, "temProvisoire");
  }

  public String temSigne() {
    return (String) storedValueForKey("temSigne");
  }

  public void setTemSigne(String value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
    	_EOElementCarriere.LOG.debug( "updating temSigne from " + temSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temSigne");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
      _EOElementCarriere.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOElementCarriere.LOG.isDebugEnabled()) {
      _EOElementCarriere.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOElementCarriere createElementCarriere(EOEditingContext editingContext, Integer carSource
, String cCorps
, String cGrade
, NSTimestamp dCreation
, NSTimestamp dEffetElement
, NSTimestamp dModification
, Integer elcOrdre
, Integer elSource
, Integer idSource
, String statut
, String temImport
, String temProvisoire
) {
    EOElementCarriere eo = (EOElementCarriere) EOUtilities.createAndInsertInstance(editingContext, _EOElementCarriere.ENTITY_NAME);    
		eo.setCarSource(carSource);
		eo.setCCorps(cCorps);
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDEffetElement(dEffetElement);
		eo.setDModification(dModification);
		eo.setElcOrdre(elcOrdre);
		eo.setElSource(elSource);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemProvisoire(temProvisoire);
    return eo;
  }

  public static NSArray<EOElementCarriere> fetchAllElementCarrieres(EOEditingContext editingContext) {
    return _EOElementCarriere.fetchAllElementCarrieres(editingContext, null);
  }

  public static NSArray<EOElementCarriere> fetchAllElementCarrieres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOElementCarriere.fetchElementCarrieres(editingContext, null, sortOrderings);
  }

  public static NSArray<EOElementCarriere> fetchElementCarrieres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOElementCarriere.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOElementCarriere> eoObjects = (NSArray<EOElementCarriere>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOElementCarriere fetchElementCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOElementCarriere.fetchElementCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOElementCarriere fetchElementCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOElementCarriere> eoObjects = _EOElementCarriere.fetchElementCarrieres(editingContext, qualifier, null);
    EOElementCarriere eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOElementCarriere)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ElementCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOElementCarriere fetchRequiredElementCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOElementCarriere.fetchRequiredElementCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOElementCarriere fetchRequiredElementCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    EOElementCarriere eoObject = _EOElementCarriere.fetchElementCarriere(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ElementCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOElementCarriere localInstanceIn(EOEditingContext editingContext, EOElementCarriere eo) {
    EOElementCarriere localInstance = (eo == null) ? null : (EOElementCarriere)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
