// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTelephone.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTelephone extends ObjetImportPourIndividuOuStructure {
	public static final String ENTITY_NAME = "Telephone";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String INDICATIF_KEY = "indicatif";
	public static final String LISTE_ROUGE_KEY = "listeRouge";
	public static final String NO_TELEPHONE_KEY = "noTelephone";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PRINCIPAL_KEY = "temPrincipal";
	public static final String TYPE_NO_KEY = "typeNo";
	public static final String TYPE_TEL_KEY = "typeTel";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOTelephone.class);

  public EOTelephone localInstanceIn(EOEditingContext editingContext) {
    EOTelephone localInstance = (EOTelephone)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer indicatif() {
    return (Integer) storedValueForKey("indicatif");
  }

  public void setIndicatif(Integer value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating indicatif from " + indicatif() + " to " + value);
    }
    takeStoredValueForKey(value, "indicatif");
  }

  public String listeRouge() {
    return (String) storedValueForKey("listeRouge");
  }

  public void setListeRouge(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating listeRouge from " + listeRouge() + " to " + value);
    }
    takeStoredValueForKey(value, "listeRouge");
  }

  public String noTelephone() {
    return (String) storedValueForKey("noTelephone");
  }

  public void setNoTelephone(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating noTelephone from " + noTelephone() + " to " + value);
    }
    takeStoredValueForKey(value, "noTelephone");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPrincipal() {
    return (String) storedValueForKey("temPrincipal");
  }

  public void setTemPrincipal(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating temPrincipal from " + temPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipal");
  }

  public String typeNo() {
    return (String) storedValueForKey("typeNo");
  }

  public void setTypeNo(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating typeNo from " + typeNo() + " to " + value);
    }
    takeStoredValueForKey(value, "typeNo");
  }

  public String typeTel() {
    return (String) storedValueForKey("typeTel");
  }

  public void setTypeTel(String value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
    	_EOTelephone.LOG.debug( "updating typeTel from " + typeTel() + " to " + value);
    }
    takeStoredValueForKey(value, "typeTel");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
      _EOTelephone.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOTelephone.LOG.isDebugEnabled()) {
      _EOTelephone.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOTelephone createTelephone(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String noTelephone
, String statut
, String temImport
, String temPrincipal
, String typeNo
, String typeTel
) {
    EOTelephone eo = (EOTelephone) EOUtilities.createAndInsertInstance(editingContext, _EOTelephone.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoTelephone(noTelephone);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemPrincipal(temPrincipal);
		eo.setTypeNo(typeNo);
		eo.setTypeTel(typeTel);
    return eo;
  }

  public static NSArray<EOTelephone> fetchAllTelephones(EOEditingContext editingContext) {
    return _EOTelephone.fetchAllTelephones(editingContext, null);
  }

  public static NSArray<EOTelephone> fetchAllTelephones(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTelephone.fetchTelephones(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTelephone> fetchTelephones(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTelephone.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTelephone> eoObjects = (NSArray<EOTelephone>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTelephone fetchTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTelephone.fetchTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTelephone fetchTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTelephone> eoObjects = _EOTelephone.fetchTelephones(editingContext, qualifier, null);
    EOTelephone eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTelephone)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Telephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTelephone fetchRequiredTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTelephone.fetchRequiredTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTelephone fetchRequiredTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTelephone eoObject = _EOTelephone.fetchTelephone(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Telephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTelephone localInstanceIn(EOEditingContext editingContext, EOTelephone eo) {
    EOTelephone localInstance = (eo == null) ? null : (EOTelephone)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
