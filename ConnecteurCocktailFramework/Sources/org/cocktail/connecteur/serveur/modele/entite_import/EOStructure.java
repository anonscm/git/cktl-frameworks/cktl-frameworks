//EOStructure.java
//Created on Wed Jul 25 15:06:29 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import java.util.HashMap;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrhumParametres;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeGroupe;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeStructure;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** Warning : a l'import, les structures parent doivent venir les premieres */
public class EOStructure extends _EOStructure implements InterfaceHomonymie, InterfaceInvalidationRecordAvecDependance {
	private static final String TYPE_GROUPE_VAL_DEFAUT = "S";
	private static HashMap<String, EOStructure> idSourceCache = new HashMap<String, EOStructure>();

	public EOStructure() {
		super();
	}

	@Override
	public void preparerRelations() {
		super.preparerRelations();
		if (idResponsableSource() != null) {
			EOIndividu individu = EOIndividu.individuPourSourceId(editingContext(), idResponsableSource());
			setResponsableRelationship(individu);
		}
	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setResponsableRelationship(null);
	}

	/**
	 * Verifie si la structure pere existe. On considere qu'il y a une structure pere si strSourcePere != null and strSource != strSourcePere
	 */
	public EOLogImport verifierRecord() {
		EOLogImport log = null;
		String message = null;

		if (strSourcePere() != null && strSource().equals(strSourcePere()) == false) {
			EOStructure structurePere = structurePourSourceId(editingContextTravail(), strSourcePere());
			if (structurePere == null) {
				message = "STRUCTURE_PERE_INVALIDE";
			}
		}

		if (idResponsableSource() != null && responsable() == null)
			message = "INDIVIDU_NON_IMPORTE";
		else if (responsable() != null && responsable().statut().equals(STATUT_ERREUR))
			message = "INDIVIDU_INVALIDE";

		if (message != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), message);
			log.setStrSource(strSource()); // On ne fait pas référence à la structure car elle n'est pas encore insérée
			setStatut(STATUT_ERREUR);
			return log;
		}
		return log;
	}

	/**
	 * Effectue des verifications complementaires sur la structure (unicite du siret) en cas d'insertion
	 */
	public EOLogImport verifierRecordApresOperation() {
		if (EOImportParametres.cacheMemoireNiveauHautOuPlus())
			idSourceCache.put(strSource(), this);

		if (operation().equals(ObjetImport.OPERATION_INSERTION)) {
			if (siret() != null) {
				EOGrhumStructure structure = (EOGrhumStructure) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContextTravail(), "GrhumStructure",
						"siret", siret());
				if (structure != null) {
					LogManager.logDetail("Insertion d'un log pour le siret");
					EOLogImport log = new EOLogImport();
					log.initAvecEditingContext(editingContextTravail(), this.entityName(), "STRUCTURE_SIRET_INVALIDE");
					log.setStrSource(strSource()); // On ne fait pas référence à la structure car elle n'est pas encore insérée
					setStatut(STATUT_ERREUR);
					return log;
				}
			}
		}
		return null;
	}

	/**
	 * Retourne une structure du SI destinataire avec le meme libelle long et ayant un père avec le même nom
	 * 
	 **/
	public ObjetPourSIDestinataire destinataireEquivalent() {
		NSArray structures = EOGrhumStructure.rechercherLibelleLong(editingContext(), llStructure());
		if (structures == null || structures.size() < 1)
			return null;

		EOStructure structurePere = structurePourSourceId(editingContextTravail(), strSourcePere());
		EOGrhumStructure destinataireEquivalent = null;
		int nb_succes = 0;
		for (Object obj : structures) {
			EOGrhumStructure structureGrhum = (EOGrhumStructure) obj;
			if (structurePere == null && structureGrhum.structurePere() == null) {
				nb_succes++;
				destinataireEquivalent = structureGrhum;
			} else if (structurePere != null && structureGrhum.structurePere() != null
					&& structurePere.llStructure().equals(structureGrhum.structurePere().llStructure())) {
				nb_succes++;
				destinataireEquivalent = structureGrhum;
			}
		}
		if (nb_succes == 1)
			return destinataireEquivalent;
		else
			return null;
	}

	/** retourne le nom de la relation pour retrouver les logs lies au record */
	public String nomRelationPourLog() {
		return EOLogImport.STRUCTURE_KEY;
	}

	/**
	 */
	public NSArray structuresFilles() {
		EOFetchSpecification fs = new EOFetchSpecification("Structure", EOQualifier.qualifierWithQualifierFormat("strSourcePere = %@ AND temImport = 'N'",
				new NSArray(strSource())), null);
		return editingContext().objectsWithFetchSpecification(fs);
	}

	/**
	 * Supprime les affectations, les occupations, les emplois, les telephones, les adresses, les ribs, les structures filles
	 */
	public String invaliderRecordsDependants() {
		try {
			invaliderObjets(ObjetImportPourIndividuOuStructure.rechercherObjetsImportCourantPourStructure("Emploi", this));
			invaliderObjets(ObjetImportPourIndividuOuStructure.rechercherObjetsImportCourantPourStructure("Affectation", this));
			invaliderObjets(ObjetImportPourIndividuOuStructure.rechercherObjetsImportCourantPourStructure("Adresse", this));
			invaliderObjets(ObjetImportPourIndividuOuStructure.rechercherObjetsImportCourantPourStructure("Telephone", this));
			invaliderObjets(ObjetImportPourIndividuOuStructure.rechercherObjetsImportCourantPourStructure("Rib", this));
			invaliderObjets(ObjetImportPourIndividuOuStructure.rechercherObjetsImportCourantPourStructure("Compte", this));
			invaliderStructuresFilles();
			return null;
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		}
	}

	public void invaliderStructuresFilles() {
		java.util.Enumeration e = structuresFilles().objectEnumerator();
		while (e.hasMoreElements()) {
			EOStructure structure = (EOStructure) e.nextElement();
			if (structure.strSourcePere() != null && structure.strSourcePere().equals(structure.strSource()) == false) {
				structure.invaliderRecordsDependants();
				EOLogImport.invaliderLogsPourRecord(editingContext(), structure);
				structure.invalider();
				editingContext().saveChanges();
			}
		}
	}

	// InterfaceHomonymie
	/**
	 * retourne true si trouve dans le SI des structures valides avec le m&ecirc;me libelle long
	 */
	public boolean aHomonymeDansSIDestinataire() {
		NSArray structures = EOGrhumStructure.rechercherLibelleLong(editingContextTravail(), llStructure());
		return structures != null && structures.count() > 0;
	}

	/** retourne true si les structures ont le m&ecirc;me libelle */
	public boolean memesDonneesQueRecord(ObjetImport record) {
		EOStructure structure = (EOStructure) record;
		if (!structure.llStructure().equals(llStructure()))
			return false;

		if (structure.strSourcePere() != null && !structure.strSourcePere().equals(strSourcePere()))
			return false;

		return true;
	}

	/** Pas d'attribut de comparaison avec des relations */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}

	// Méthodes statiques
	/**
	 * Valide les donnees d'une structure.<BR>
	 * Verifie que le type d'etablissement, l'academie, le statut juridique et le type de structure sont corrects. Verifie que le rne et le code Ape si ils sont
	 * fournis, sont valides et que le siret est valide, si c'est parametre dans Grhum. Verifie la coherence des donnees selon le type de structure : structure
	 * p&egrave;re obligatoire sauf pour les personnes morales et l'etablissement<BR>
	 * 
	 * @param editingContext
	 * @param valeursAttributs
	 *            dictionnaire des attributs a valider ou modifier
	 */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		
		String codeEtablissement = (String) valeursAttributs.objectForKey("typeEtablissement");
		if (codeEtablissement != null) {
			if (Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeEtablissement", "cTypeEtablissemen", codeEtablissement) == null) {
				resultat = "Type_Etablissement_Invalide >>" + codeEtablissement + System.getProperty("line.separator");
			}
		}
		String academie = (String) valeursAttributs.objectForKey("academie");
		if (academie != null) {
			if (Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "Academie", "cAcademie", academie) == null) {
				resultat += "Academie_Invalide >>" + academie + System.getProperty("line.separator");
			}
		}
		String statutJuridique = (String) valeursAttributs.objectForKey("statutJuridique");
		if (statutJuridique != null) {
			if (Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "StatutJuridiqueStr", "cStatutJuridique", statutJuridique) == null) {
				resultat += "Statut_Invalide >>" + statutJuridique + System.getProperty("line.separator");
			}
		}
		String typeStructure = (String) valeursAttributs.objectForKey("typeStructure"); // ne peut pas être nul, attribut obligatoire
		if (Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeStructure", "cTypeStructure", typeStructure) == null) {
			resultat += "Type_Structure_Invalide >>" + typeStructure + System.getProperty("line.separator");
		}
		String rne = (String) valeursAttributs.objectForKey("rne");
		if (rne != null) {
			// Vérifier le rne
			EORne rneRecord = EORne.getFromCode(editingContext, rne);
			if (rneRecord == null) {
				resultat += "Rne_Inconnu >>" + rne + System.getProperty("line.separator");
			}
		}
		// Vérifier la cohérence par rapport aux données fournies
		// Les autres types de structure sauf l'établissement et les personnes
		// morales doivent avoir une structure père
		String structurePere = (String) valeursAttributs.objectForKey("strSourcePere");
		if (typeStructure.equals(EOTypeStructure.AUTRE) == false && typeStructure.equals(EOTypeStructure.ETABLISSEMENT) == false) {
			if (structurePere == null) {
				resultat += "Structure_Parent >>" + System.getProperty("line.separator");
			}
		}

		// Vérifier le n° de Siret si c'est un paramétrage de Grhum
		String verifierSiret = EOGrhumParametres.parametrePourCle(editingContext, "GRHUM_CONTROLE_SIRET");
		if (verifierSiret != null && verifierSiret.startsWith(CocktailConstantes.VRAI)) {
			String siret = (String) valeursAttributs.objectForKey("siret");
			if (siret != null && estSiretValide(siret) == false) {
				resultat += "Siret_Invalide >>" + siret + System.getProperty("line.separator");
			}
		}
		// Vérifier le code ape
		String codeApe = (String) valeursAttributs.objectForKey("codeApe");
		if (codeApe != null) {
			// Vérifier que ce code est défini
			EOGenericRecord apeRecord = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "Ape", "apeCode", codeApe);
			if (apeRecord == null) {
				resultat += "Code_Ape_Inconnu >>" + codeApe + System.getProperty("line.separator");
			}
		}
		NSTimestamp dateOuverture = (NSTimestamp) valeursAttributs.objectForKey("dateOuverture");
		NSTimestamp dateFermeture = (NSTimestamp) valeursAttributs.objectForKey("dateFermeture");
		if (dateOuverture != null && dateFermeture != null && DateCtrl.isAfterEq(dateOuverture, dateFermeture)) {
			resultat += "Date_Ouverture >>" + System.getProperty("line.separator");
		}
		Number exportVal = (Number) valeursAttributs.objectForKey("export");
		if (exportVal != null && exportVal.doubleValue() > 100.00) {
			resultat += "Pourcentage_Export_Invalide >>" + System.getProperty("line.separator");
		}

		String codeTypeGroupe = (String) valeursAttributs.objectForKey(TYPE_GROUPE_KEY);
		if (codeTypeGroupe == null)
			valeursAttributs.setObjectForKey(TYPE_GROUPE_VAL_DEFAUT, TYPE_GROUPE_KEY);
		else {
			EOTypeGroupe typeGroupe=EOTypeGroupe.getFromCode(editingContext,codeTypeGroupe);
			if (typeGroupe == null) {
				resultat += "Type_Groupe_Inconnu >>" + codeTypeGroupe + System.getProperty("line.separator");
			}
		}
		return resultat;
	}

	/**
	 * 
	 * @param siret
	 * @return
	 */
	private static boolean estSiretValide(String siret) {
		// completion à gauche par des 0 pour avoir un numero SIRET de 14 caractères
		if (siret.length() < 14) {
			siret = StringCtrl.stringCompletion(siret, 14, "0", "D");
		}
		int somme = 0;
		for (int i = 0; i < siret.length(); i++) {
			int nombre = new Integer(siret.substring(i, i + 1)).intValue();
			// pour les chiffres impairs, on multiplie par 2. Comme on commence à zéro pour le premier caractère, on regarde le modulo 2
			if (i % 2 == 0) {
				try {
					nombre = nombre * 2;
					// si le nombre est supérieur à 9 on retranche 9
					if (nombre > 9) {
						nombre = nombre - 9;
					}
				} catch (Exception e) {
					return false;
				}
			}
			somme += nombre;
		}
		if (somme % 10 == 0) {
			return true;
		} else {
			return false;
		}
	}

	// Méthodes statiques
	public static EOStructure structurePourSourceId(EOEditingContext editingContext, String strSource) {
		if (strSource == null)
			return null;

		EOStructure resultat;

		if (EOImportParametres.cacheMemoireNiveauHautOuPlus()) {
			resultat = idSourceCache.get(strSource);
			if (resultat != null)
				return resultat;
		}

		if (editingContext == null)
			return null;

		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, STR_SOURCE_KEY, strSource);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOStructure)premierObjetValide(resultats);

		if (EOImportParametres.cacheMemoireNiveauHautOuPlus() && resultat != null)
			idSourceCache.put(strSource, resultat);

		return resultat;
	}

	public static void resetCache() {
		idSourceCache.clear();
	}
}
