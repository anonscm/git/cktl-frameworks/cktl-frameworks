// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOHeberge.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOHeberge extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Heberge";

	// Attributes
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DETAIL_ORIGINE_KEY = "detailOrigine";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HEB_SOURCE_KEY = "hebSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String INM_KEY = "inm";
	public static final String NBR_HEURES_KEY = "nbrHeures";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String STR_ORIGINE_SOURCE_KEY = "strOrigineSource";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_ORIGINE_KEY = "structureOrigine";

  private static Logger LOG = Logger.getLogger(_EOHeberge.class);

  public EOHeberge localInstanceIn(EOEditingContext editingContext) {
    EOHeberge localInstance = (EOHeberge)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public String detailOrigine() {
    return (String) storedValueForKey("detailOrigine");
  }

  public void setDetailOrigine(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating detailOrigine from " + detailOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, "detailOrigine");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer hebSource() {
    return (Integer) storedValueForKey("hebSource");
  }

  public void setHebSource(Integer value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating hebSource from " + hebSource() + " to " + value);
    }
    takeStoredValueForKey(value, "hebSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer inm() {
    return (Integer) storedValueForKey("inm");
  }

  public void setInm(Integer value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating inm from " + inm() + " to " + value);
    }
    takeStoredValueForKey(value, "inm");
  }

  public Integer nbrHeures() {
    return (Integer) storedValueForKey("nbrHeures");
  }

  public void setNbrHeures(Integer value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating nbrHeures from " + nbrHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeures");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strOrigineSource() {
    return (String) storedValueForKey("strOrigineSource");
  }

  public void setStrOrigineSource(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating strOrigineSource from " + strOrigineSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strOrigineSource");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
    	_EOHeberge.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
      _EOHeberge.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
      _EOHeberge.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structureOrigine() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structureOrigine");
  }

  public void setStructureOrigineRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOHeberge.LOG.isDebugEnabled()) {
      _EOHeberge.LOG.debug("updating structureOrigine from " + structureOrigine() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structureOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structureOrigine");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structureOrigine");
    }
  }
  

  public static EOHeberge createHeberge(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer hebSource
, Integer idSource
, String statut
, String temImport
, String temValide
) {
    EOHeberge eo = (EOHeberge) EOUtilities.createAndInsertInstance(editingContext, _EOHeberge.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setHebSource(hebSource);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOHeberge> fetchAllHeberges(EOEditingContext editingContext) {
    return _EOHeberge.fetchAllHeberges(editingContext, null);
  }

  public static NSArray<EOHeberge> fetchAllHeberges(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOHeberge.fetchHeberges(editingContext, null, sortOrderings);
  }

  public static NSArray<EOHeberge> fetchHeberges(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOHeberge.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOHeberge> eoObjects = (NSArray<EOHeberge>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOHeberge fetchHeberge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHeberge.fetchHeberge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHeberge fetchHeberge(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOHeberge> eoObjects = _EOHeberge.fetchHeberges(editingContext, qualifier, null);
    EOHeberge eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOHeberge)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Heberge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHeberge fetchRequiredHeberge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHeberge.fetchRequiredHeberge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHeberge fetchRequiredHeberge(EOEditingContext editingContext, EOQualifier qualifier) {
    EOHeberge eoObject = _EOHeberge.fetchHeberge(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Heberge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHeberge localInstanceIn(EOEditingContext editingContext, EOHeberge eo) {
    EOHeberge localInstance = (eo == null) ? null : (EOHeberge)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
