// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaladieSsTrt.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaladieSsTrt extends CongeMaladie {
	public static final String ENTITY_NAME = "CongeMaladieSsTrt";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CMST_SOURCE_KEY = "cmstSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRET_TRAVAIL_KEY = "dArretTravail";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_COM_KEY = "dComMedCom";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_ACC_SERV_KEY = "temAccServ";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PROLONG_MALADIE_KEY = "temProlongMaladie";
	public static final String TEM_REQUALIF_MALADIE_KEY = "temRequalifMaladie";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeMaladieSsTrt.class);

  public EOCongeMaladieSsTrt localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaladieSsTrt localInstance = (EOCongeMaladieSsTrt)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer cmstSource() {
    return (Integer) storedValueForKey("cmstSource");
  }

  public void setCmstSource(Integer value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating cmstSource from " + cmstSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cmstSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArretTravail() {
    return (NSTimestamp) storedValueForKey("dArretTravail");
  }

  public void setDArretTravail(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dArretTravail from " + dArretTravail() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretTravail");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedCom() {
    return (NSTimestamp) storedValueForKey("dComMedCom");
  }

  public void setDComMedCom(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dComMedCom from " + dComMedCom() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedCom");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temAccServ() {
    return (String) storedValueForKey("temAccServ");
  }

  public void setTemAccServ(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating temAccServ from " + temAccServ() + " to " + value);
    }
    takeStoredValueForKey(value, "temAccServ");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temProlongMaladie() {
    return (String) storedValueForKey("temProlongMaladie");
  }

  public void setTemProlongMaladie(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating temProlongMaladie from " + temProlongMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongMaladie");
  }

  public String temRequalifMaladie() {
    return (String) storedValueForKey("temRequalifMaladie");
  }

  public void setTemRequalifMaladie(String value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrt.LOG.debug( "updating temRequalifMaladie from " + temRequalifMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalifMaladie");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
      _EOCongeMaladieSsTrt.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeMaladieSsTrt.LOG.isDebugEnabled()) {
      _EOCongeMaladieSsTrt.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeMaladieSsTrt createCongeMaladieSsTrt(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temAccServ
, String temConfirme
, String temGestEtab
, String temImport
, String temProlongMaladie
, String temRequalifMaladie
) {
    EOCongeMaladieSsTrt eo = (EOCongeMaladieSsTrt) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaladieSsTrt.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemAccServ(temAccServ);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemProlongMaladie(temProlongMaladie);
		eo.setTemRequalifMaladie(temRequalifMaladie);
    return eo;
  }

  public static NSArray<EOCongeMaladieSsTrt> fetchAllCongeMaladieSsTrts(EOEditingContext editingContext) {
    return _EOCongeMaladieSsTrt.fetchAllCongeMaladieSsTrts(editingContext, null);
  }

  public static NSArray<EOCongeMaladieSsTrt> fetchAllCongeMaladieSsTrts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaladieSsTrt.fetchCongeMaladieSsTrts(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaladieSsTrt> fetchCongeMaladieSsTrts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaladieSsTrt.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaladieSsTrt> eoObjects = (NSArray<EOCongeMaladieSsTrt>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaladieSsTrt fetchCongeMaladieSsTrt(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieSsTrt.fetchCongeMaladieSsTrt(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieSsTrt fetchCongeMaladieSsTrt(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaladieSsTrt> eoObjects = _EOCongeMaladieSsTrt.fetchCongeMaladieSsTrts(editingContext, qualifier, null);
    EOCongeMaladieSsTrt eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaladieSsTrt)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaladieSsTrt that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieSsTrt fetchRequiredCongeMaladieSsTrt(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieSsTrt.fetchRequiredCongeMaladieSsTrt(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieSsTrt fetchRequiredCongeMaladieSsTrt(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaladieSsTrt eoObject = _EOCongeMaladieSsTrt.fetchCongeMaladieSsTrt(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaladieSsTrt that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieSsTrt localInstanceIn(EOEditingContext editingContext, EOCongeMaladieSsTrt eo) {
    EOCongeMaladieSsTrt localInstance = (eo == null) ? null : (EOCongeMaladieSsTrt)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
