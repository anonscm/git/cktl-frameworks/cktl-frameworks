/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// 08/11/2011 - Correction d'un bug pour les nouveaux individus, on doit vérifier qu'ils ne font pas partie de l'import courant
public abstract class ObjetImportPourIndividu extends ObjetImport{
	public ObjetImportPourIndividu() {
		super();
	}
	public EOIndividu individu() {
		return (EOIndividu)storedValueForKey("individu");
	}

	public void setIndividu(EOIndividu value) {
		takeStoredValueForKey(value, "individu");
	}

	public abstract Integer idSource();

	/** Prepare les relations. A appeler apres avoir insereun objet dans l'editing context */
	public void preparerRelations() {
		LogManager.logDetail("Preparation des relations");
		if (editingContext() != null && idSource() != null) {
			EOIndividu individu = EOIndividu.individuPourSourceId(editingContext(), idSource());
			if (individu != null) {
				addObjectToBothSidesOfRelationshipWithKey(individu,"individu");
			}
		}
	}
	public void supprimerRelations() {
		LogManager.logDetail("Suppression des relations");
		if (individu() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(individu(), "individu");
		}
	}
	/** Verifie que les relations du record sont bien valides : il se peut que l'individu ait ete cree juste auparavant. 
	 * Retourne un message de log ou null si pas d'erreur
	 */
	public EOLogImport verifierRecord() {
		LogManager.logDetail("Verification du record - ID_SOURCE : " + idSource() + "  , INDIVIDU " + individu());
		EOLogImport log = null;
		if (individu() == null && idSource() != null) {
			String messageLog = etablirCauseErreurPotentielle(individu());
			if (messageLog != null) {
				log = genererErreur(individu(), messageLog);
//				LogManager.logInformation("Log : " + log);
			}
		} 
		return log;
	}
	protected EOLogImport genererErreur(EOIndividu individu, String messageLog) {
		EOLogImport log;
		log = new EOLogImport();
		log.initAvecEditingContext(editingContextTravail(),this.entityName(),messageLog);
		log.setIdSource(new Integer(idSource().intValue()));
		log.ajouterRelation(this);
		if (individu != null) {
			log.setIndividuRelationship(individu);
		}
		setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
		return log;
	}
	private String etablirCauseErreurPotentielle(EOIndividu individu) {
		String messageLog = null;
		if (individu == null) {
			messageLog = "INDIVIDU_NON_IMPORTE";
		} else if (individu.temImport().equals(ObjetImport.A_TRANSFERER)) {
			if (individu.statut().equals(EOIndividu.STATUT_ERREUR)) {
				messageLog = "INDIVIDU_INVALIDE";
			}
		}
		return messageLog;
	}

	public String nomRelationPourAttributComparaison(String nomAttribut) {
		if (nomAttribut.equals("idSource")) {
			return "individu";
		} else {
			return null;
		}
	}

	// Méthodes protégées
	/** Retourne false si l'individu associe est en mode Insertion et vient d'etre cree. A fortiori les objets qui en dependent le seront */
	// 08/11/2011 - Correction pour vérifier qu'on est dans l'import courant
	protected boolean estNouvelIndividu() {
		return (individu() == null || (individu().operation().equals(ObjetImport.OPERATION_INSERTION) && individu().temImport().equals(ObjetImport.STATUT_NON_IMPORTE)));
	}

	// Méthodes statiques
	/** Retourne les objets de type nomEntite lies a un individu */
	public static NSArray rechercherObjetsImportCourantPourIndividu(String nomEntite,EOIndividu individu) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND temImport = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (nomEntite, qualifier,null);
		return individu.editingContext().objectsWithFetchSpecification(myFetch);
	}
}
