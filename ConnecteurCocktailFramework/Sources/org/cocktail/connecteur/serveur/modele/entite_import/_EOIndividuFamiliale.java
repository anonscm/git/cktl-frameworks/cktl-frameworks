// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuFamiliale.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuFamiliale extends ObjetImportPourPersonnel {
	public static final String ENTITY_NAME = "IndividuFamiliale";

	// Attributes
	public static final String C_SITUATION_FAMILLE_KEY = "cSituationFamille";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_SIT_FAMILIALE_KEY = "dDebSitFamiliale";
	public static final String D_FIN_SIT_FAMILIALE_KEY = "dFinSitFamiliale";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String IF_SOURCE_KEY = "ifSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOIndividuFamiliale.class);

  public EOIndividuFamiliale localInstanceIn(EOEditingContext editingContext) {
    EOIndividuFamiliale localInstance = (EOIndividuFamiliale)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cSituationFamille() {
    return (String) storedValueForKey("cSituationFamille");
  }

  public void setCSituationFamille(String value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating cSituationFamille from " + cSituationFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "cSituationFamille");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebSitFamiliale() {
    return (NSTimestamp) storedValueForKey("dDebSitFamiliale");
  }

  public void setDDebSitFamiliale(NSTimestamp value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating dDebSitFamiliale from " + dDebSitFamiliale() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebSitFamiliale");
  }

  public NSTimestamp dFinSitFamiliale() {
    return (NSTimestamp) storedValueForKey("dFinSitFamiliale");
  }

  public void setDFinSitFamiliale(NSTimestamp value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating dFinSitFamiliale from " + dFinSitFamiliale() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinSitFamiliale");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer ifSource() {
    return (Integer) storedValueForKey("ifSource");
  }

  public void setIfSource(Integer value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating ifSource from " + ifSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ifSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOIndividuFamiliale.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOIndividuFamiliale.LOG.isDebugEnabled()) {
      _EOIndividuFamiliale.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOIndividuFamiliale createIndividuFamiliale(EOEditingContext editingContext, String cSituationFamille
, NSTimestamp dCreation
, NSTimestamp dDebSitFamiliale
, NSTimestamp dModification
, Integer idSource
, Integer ifSource
, String statut
, String temImport
) {
    EOIndividuFamiliale eo = (EOIndividuFamiliale) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuFamiliale.ENTITY_NAME);    
		eo.setCSituationFamille(cSituationFamille);
		eo.setDCreation(dCreation);
		eo.setDDebSitFamiliale(dDebSitFamiliale);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setIfSource(ifSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOIndividuFamiliale> fetchAllIndividuFamiliales(EOEditingContext editingContext) {
    return _EOIndividuFamiliale.fetchAllIndividuFamiliales(editingContext, null);
  }

  public static NSArray<EOIndividuFamiliale> fetchAllIndividuFamiliales(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuFamiliale.fetchIndividuFamiliales(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuFamiliale> fetchIndividuFamiliales(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuFamiliale.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuFamiliale> eoObjects = (NSArray<EOIndividuFamiliale>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuFamiliale fetchIndividuFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFamiliale.fetchIndividuFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFamiliale fetchIndividuFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuFamiliale> eoObjects = _EOIndividuFamiliale.fetchIndividuFamiliales(editingContext, qualifier, null);
    EOIndividuFamiliale eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuFamiliale)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFamiliale fetchRequiredIndividuFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFamiliale.fetchRequiredIndividuFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFamiliale fetchRequiredIndividuFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuFamiliale eoObject = _EOIndividuFamiliale.fetchIndividuFamiliale(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFamiliale localInstanceIn(EOEditingContext editingContext, EOIndividuFamiliale eo) {
    EOIndividuFamiliale localInstance = (eo == null) ? null : (EOIndividuFamiliale)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
