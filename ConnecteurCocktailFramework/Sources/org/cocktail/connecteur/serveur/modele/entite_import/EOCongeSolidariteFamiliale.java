package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeCsf;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * . Classe gérant les Congés de Solidatité Familiale d'un individu
 * 
 * @author alainmalaplate
 * 
 */
public class EOCongeSolidariteFamiliale extends _EOCongeSolidariteFamiliale {

	private static final long serialVersionUID = 7963453695009690318L;

	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCongeSolidariteFamiliale.findForIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	public EOLogImport verifierRecordApresOperation() {
		return super.verifierRecordApresOperation();
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_SOLIDARITE_FAMILIALE_KEY;
	}

	@Override
	public Integer eimpSource() {
		return csfSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCsfSource(value);
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);

		Integer value = (Integer) valeursAttributs.objectForKey(TCSF_ORDRE_KEY);
		if (value != null) {
			EOTypeCsf typeCsf = EOTypeCsf.fetchTypeCsf(editingContext, EOTypeCsf.TCSF_ORDRE_KEY, value);
			if (typeCsf == null) {
				resultat += "TypeCSF_Inconnu >>" + value + System.getProperty("line.separator");
			}
		}

		resultat += verifierTemoin(valeursAttributs, TEM_RENOUVELE_KEY);
		resultat += verifierTemoin(valeursAttributs, TEM_CONFIRME_KEY);
		resultat += verifierTemoin(valeursAttributs, TEM_GEST_ETAB_KEY);
		resultat += verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);
		return resultat;
	}
}
