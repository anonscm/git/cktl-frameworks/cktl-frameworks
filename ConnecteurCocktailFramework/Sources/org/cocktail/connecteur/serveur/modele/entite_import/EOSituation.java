package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.serveur.modele.correspondance.EOAdresseCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOSituation extends _EOSituation {
	@Override
	public void preparerRelations() {
		super.preparerRelations();

		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);

		EOAdresse adresse = EOAdresse.adressePourSourceId(editingContext(), adrSource());
		setAdresseRelationship(adresse);
	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setAdresseRelationship(null);
		setStructureRelationship(null);
	}

	@Override
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log!=null)
			return log;

		String messageLog = null;
		if (adresse() == null && adrSource() != null) {
			messageLog = "ADRESSE_NON_IMPORTEE";
		} else if (adresse() != null && adresse().statut().equals(STATUT_ERREUR)) {
			messageLog = "ADRESSE_INVALIDE";
		} else if (structure() == null && strSource() != null) {
			messageLog = "STRUCTURE_NON_IMPORTEE";
		} else if (structure() != null && structure().statut().equals(STATUT_ERREUR)) {
			messageLog = "STRUCTURE_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	@Override
	protected String verifierChevauchement(EOEditingContext editingContext) {
		String resultat=null;
		NSArray objets = rechercherObjetsImportPourIndividuEtPeriode(editingContext, this.entityName(), idSource(), dateDebut(), dateFin(), false);
		if (objets==null)
			return resultat;
		
		int quotite=0;
		for (Object obj : objets) {
			EOSituation situation = (EOSituation) obj;
			quotite+=situation.quotite().intValue();
		}
		if (quotite>100)
			resultat="CHEVAUCHEMENT_PERIODE_DESTIN";
		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return sitSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setSitSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.SITUATION_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		return resultat;
	}
}
