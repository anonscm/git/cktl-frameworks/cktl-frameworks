// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOClm.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOClm extends CongeAvecArrete {
	public static final String ENTITY_NAME = "Clm";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CLM_SOURCE_KEY = "clmSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_CLM_KEY = "dComMedClm";
	public static final String D_COM_MED_CLM_SUP_KEY = "dComMedClmSup";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PROLONG_CLM_KEY = "temProlongClm";
	public static final String TEM_REQUALIF_CLM_KEY = "temRequalifClm";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOClm.class);

  public EOClm localInstanceIn(EOEditingContext editingContext) {
    EOClm localInstance = (EOClm)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer clmSource() {
    return (Integer) storedValueForKey("clmSource");
  }

  public void setClmSource(Integer value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating clmSource from " + clmSource() + " to " + value);
    }
    takeStoredValueForKey(value, "clmSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedClm() {
    return (NSTimestamp) storedValueForKey("dComMedClm");
  }

  public void setDComMedClm(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dComMedClm from " + dComMedClm() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedClm");
  }

  public NSTimestamp dComMedClmSup() {
    return (NSTimestamp) storedValueForKey("dComMedClmSup");
  }

  public void setDComMedClmSup(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dComMedClmSup from " + dComMedClmSup() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedClmSup");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temProlongClm() {
    return (String) storedValueForKey("temProlongClm");
  }

  public void setTemProlongClm(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating temProlongClm from " + temProlongClm() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongClm");
  }

  public String temRequalifClm() {
    return (String) storedValueForKey("temRequalifClm");
  }

  public void setTemRequalifClm(String value) {
    if (_EOClm.LOG.isDebugEnabled()) {
    	_EOClm.LOG.debug( "updating temRequalifClm from " + temRequalifClm() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalifClm");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOClm congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOClm)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOClm value) {
    if (_EOClm.LOG.isDebugEnabled()) {
      _EOClm.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOClm oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOClm.LOG.isDebugEnabled()) {
      _EOClm.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOClm createClm(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temConfirme
, String temGestEtab
, String temImport
, String temProlongClm
, String temRequalifClm
) {
    EOClm eo = (EOClm) EOUtilities.createAndInsertInstance(editingContext, _EOClm.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemProlongClm(temProlongClm);
		eo.setTemRequalifClm(temRequalifClm);
    return eo;
  }

  public static NSArray<EOClm> fetchAllClms(EOEditingContext editingContext) {
    return _EOClm.fetchAllClms(editingContext, null);
  }

  public static NSArray<EOClm> fetchAllClms(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOClm.fetchClms(editingContext, null, sortOrderings);
  }

  public static NSArray<EOClm> fetchClms(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOClm.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOClm> eoObjects = (NSArray<EOClm>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOClm fetchClm(EOEditingContext editingContext, String keyName, Object value) {
    return _EOClm.fetchClm(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOClm fetchClm(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOClm> eoObjects = _EOClm.fetchClms(editingContext, qualifier, null);
    EOClm eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOClm)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Clm that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOClm fetchRequiredClm(EOEditingContext editingContext, String keyName, Object value) {
    return _EOClm.fetchRequiredClm(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOClm fetchRequiredClm(EOEditingContext editingContext, EOQualifier qualifier) {
    EOClm eoObject = _EOClm.fetchClm(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Clm that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOClm localInstanceIn(EOEditingContext editingContext, EOClm eo) {
    EOClm localInstance = (eo == null) ? null : (EOClm)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
