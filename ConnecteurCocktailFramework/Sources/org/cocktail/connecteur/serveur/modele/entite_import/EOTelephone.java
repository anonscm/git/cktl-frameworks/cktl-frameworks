//EOTelephone.java
//Created on Wed Jul 25 15:06:49 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonneTelephone;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOTelephone extends _EOTelephone {
	final static int NUMERO_LONGUEUR_MAX = 14;

	public EOTelephone() {
		super();
	}

	// Méthodes ajoutées
	/** pas de verification complementaire */
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	/** retourne un telephone due SI destinataire identique (noTel, typeTel, typeNo, individu ou structure) */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null || structure() != null) {
			if (estNouvelIndividuOuStructure()) {
				return null;
			} else {
				return EOGrhumPersonneTelephone.telephoneDestinationPourTelephone(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	/** retourne le nom de la relation pour retrouver les logs lies au record */
	public String nomRelationPourLog() {
		return "telephone";
	}

	// Méthodes protégées
	protected boolean doitAvoirIndividuEtStructure() {
		return false;
	}

	// Méthodes statiques
	/**
	 * Valide les donnees d'un telephone.<BR>
	 * Verifie que le type de numero et le type de tel sont corrects.<BR>
	 * Formate correctement le numero de téléphone
	 * 
	 * @param editingContext
	 * @param valeursAttributs
	 *            dictionnaire des attributs a valider ou modifier
	 */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number) valeursAttributs.objectForKey("idSource");
		String strSource = (String) valeursAttributs.objectForKey("strSource");
		if (idSource == null && strSource == null) {
			resultat = resultat + "ID_SOURCE_ou_STR_SOURCE >>" + System.getProperty("line.separator");
		} else if (idSource != null && strSource != null) {
			resultat = resultat + "UNE_SEULE_ID >>" + System.getProperty("line.separator");
		}
		String typeNo = (String) valeursAttributs.objectForKey("typeNo");
		if (typeNo != null) {
			if (Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeNoTel", "cTypeNoTel", typeNo) == null) {
				resultat = resultat + "Type_No_Inconnu >>" + typeNo + System.getProperty("line.separator");
			}
		}
		String typeTel = (String) valeursAttributs.objectForKey("typeTel");
		if (typeTel != null) {
			if (Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeTel", "cTypeTel", typeTel) == null) {
				resultat = resultat + "Type_Tel_Inconnu >>" + typeTel + System.getProperty("line.separator");
			}
		}
		String noTel = (String) valeursAttributs.objectForKey("noTelephone");
		if (noTel != null) {
			if (noTel.length() > NUMERO_LONGUEUR_MAX)
				resultat = resultat + "Longueur_Numero_Supérieure_"+NUMERO_LONGUEUR_MAX+" >>" + valeursAttributs.objectForKey("noTelephone");
			else {
				// 19/12/07 - suppression des formatages de n° de téléphone
				// 04/09/08 - formatage des n° de téléphone mais ajout d'une méthode qui transforme toutes les correspondances/updates
				// qui transforment le numéro de téléphone en insertion
				noTel = nettoyerTelephone(noTel);
				if (noTel.length() == 0) {
					// Le numéro de téléphone ne comportait pas de chiffres
					resultat = resultat + "No_Tel_Invalide >>" + valeursAttributs.objectForKey("noTelephone");
				}
			}
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "listeRouge");

		return resultat;
	}

	// Méthodes privées
	private static String nettoyerTelephone(String unNumero) {
		String tempo = "";
		for (int i = 0; i < unNumero.length(); i++) {
			if (StringCtrl.isBasicDigit(unNumero.charAt(i)))
				tempo = tempo + unNumero.charAt(i);
		}
		return tempo;
	}

}
