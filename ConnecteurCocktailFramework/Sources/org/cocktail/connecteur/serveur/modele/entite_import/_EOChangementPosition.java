// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOChangementPosition.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOChangementPosition extends ObjetImportPourCarriere {
	public static final String ENTITY_NAME = "ChangementPosition";

	// Attributes
	public static final String CARRIERE_ORIGINE_KEY = "carriereOrigine";
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String C_MOTIF_POSITION_KEY = "cMotifPosition";
	public static final String C_POSITION_KEY = "cPosition";
	public static final String CP_SOURCE_KEY = "cpSource";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_RNE_ORIG_KEY = "cRneOrig";
	public static final String D_ARRETE_POSITION_KEY = "dArretePosition";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_POSITION_KEY = "dDebPosition";
	public static final String D_FIN_POSITION_KEY = "dFinPosition";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ENF_SOURCE_KEY = "enfSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LIEU_POSITION_KEY = "lieuPosition";
	public static final String LIEU_POSITION_ORIG_KEY = "lieuPositionOrig";
	public static final String NO_ARRETE_POSITION_KEY = "noArretePosition";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_POSITION_KEY = "quotitePosition";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEMOIN_POSITION_PREV_KEY = "temoinPositionPrev";
	public static final String TEM_PC_ACQUITEE_KEY = "temPcAcquitee";
	public static final String TXT_POSITION_KEY = "txtPosition";
	public static final String TXT_POSITION_ORIG_KEY = "txtPositionOrig";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_CARRIERE_ORIGINE_KEY = "toCarriereOrigine";
	public static final String TO_ENFANT_KEY = "toEnfant";

  private static Logger LOG = Logger.getLogger(_EOChangementPosition.class);

  public EOChangementPosition localInstanceIn(EOEditingContext editingContext) {
    EOChangementPosition localInstance = (EOChangementPosition)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer carriereOrigine() {
    return (Integer) storedValueForKey("carriereOrigine");
  }

  public void setCarriereOrigine(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating carriereOrigine from " + carriereOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, "carriereOrigine");
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public String cMotifPosition() {
    return (String) storedValueForKey("cMotifPosition");
  }

  public void setCMotifPosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating cMotifPosition from " + cMotifPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifPosition");
  }

  public String cPosition() {
    return (String) storedValueForKey("cPosition");
  }

  public void setCPosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating cPosition from " + cPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cPosition");
  }

  public Integer cpSource() {
    return (Integer) storedValueForKey("cpSource");
  }

  public void setCpSource(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating cpSource from " + cpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cpSource");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cRneOrig() {
    return (String) storedValueForKey("cRneOrig");
  }

  public void setCRneOrig(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating cRneOrig from " + cRneOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "cRneOrig");
  }

  public NSTimestamp dArretePosition() {
    return (NSTimestamp) storedValueForKey("dArretePosition");
  }

  public void setDArretePosition(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dArretePosition from " + dArretePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretePosition");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebPosition() {
    return (NSTimestamp) storedValueForKey("dDebPosition");
  }

  public void setDDebPosition(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dDebPosition from " + dDebPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebPosition");
  }

  public NSTimestamp dFinPosition() {
    return (NSTimestamp) storedValueForKey("dFinPosition");
  }

  public void setDFinPosition(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dFinPosition from " + dFinPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinPosition");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer enfSource() {
    return (Integer) storedValueForKey("enfSource");
  }

  public void setEnfSource(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating enfSource from " + enfSource() + " to " + value);
    }
    takeStoredValueForKey(value, "enfSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String lieuPosition() {
    return (String) storedValueForKey("lieuPosition");
  }

  public void setLieuPosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating lieuPosition from " + lieuPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuPosition");
  }

  public String lieuPositionOrig() {
    return (String) storedValueForKey("lieuPositionOrig");
  }

  public void setLieuPositionOrig(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating lieuPositionOrig from " + lieuPositionOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuPositionOrig");
  }

  public String noArretePosition() {
    return (String) storedValueForKey("noArretePosition");
  }

  public void setNoArretePosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating noArretePosition from " + noArretePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "noArretePosition");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer quotitePosition() {
    return (Integer) storedValueForKey("quotitePosition");
  }

  public void setQuotitePosition(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating quotitePosition from " + quotitePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "quotitePosition");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temoinPositionPrev() {
    return (String) storedValueForKey("temoinPositionPrev");
  }

  public void setTemoinPositionPrev(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating temoinPositionPrev from " + temoinPositionPrev() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinPositionPrev");
  }

  public String temPcAcquitee() {
    return (String) storedValueForKey("temPcAcquitee");
  }

  public void setTemPcAcquitee(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating temPcAcquitee from " + temPcAcquitee() + " to " + value);
    }
    takeStoredValueForKey(value, "temPcAcquitee");
  }

  public String txtPosition() {
    return (String) storedValueForKey("txtPosition");
  }

  public void setTxtPosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating txtPosition from " + txtPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "txtPosition");
  }

  public String txtPositionOrig() {
    return (String) storedValueForKey("txtPositionOrig");
  }

  public void setTxtPositionOrig(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating txtPositionOrig from " + txtPositionOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "txtPositionOrig");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriereOrigine() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriereOrigine");
  }

  public void setToCarriereOrigineRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toCarriereOrigine from " + toCarriereOrigine() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriereOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriereOrigine");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriereOrigine");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant toEnfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("toEnfant");
  }

  public void setToEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toEnfant from " + toEnfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = toEnfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant");
    }
  }
  

  public static EOChangementPosition createChangementPosition(EOEditingContext editingContext, String cPosition
, Integer cpSource
, NSTimestamp dCreation
, NSTimestamp dDebPosition
, NSTimestamp dModification
, Integer idSource
, Integer quotitePosition
, String statut
, String temImport
, String temoinPositionPrev
, String temPcAcquitee
) {
    EOChangementPosition eo = (EOChangementPosition) EOUtilities.createAndInsertInstance(editingContext, _EOChangementPosition.ENTITY_NAME);    
		eo.setCPosition(cPosition);
		eo.setCpSource(cpSource);
		eo.setDCreation(dCreation);
		eo.setDDebPosition(dDebPosition);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setQuotitePosition(quotitePosition);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemoinPositionPrev(temoinPositionPrev);
		eo.setTemPcAcquitee(temPcAcquitee);
    return eo;
  }

  public static NSArray<EOChangementPosition> fetchAllChangementPositions(EOEditingContext editingContext) {
    return _EOChangementPosition.fetchAllChangementPositions(editingContext, null);
  }

  public static NSArray<EOChangementPosition> fetchAllChangementPositions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChangementPosition.fetchChangementPositions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOChangementPosition> fetchChangementPositions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOChangementPosition.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChangementPosition> eoObjects = (NSArray<EOChangementPosition>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOChangementPosition fetchChangementPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChangementPosition.fetchChangementPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChangementPosition fetchChangementPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChangementPosition> eoObjects = _EOChangementPosition.fetchChangementPositions(editingContext, qualifier, null);
    EOChangementPosition eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOChangementPosition)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ChangementPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChangementPosition fetchRequiredChangementPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChangementPosition.fetchRequiredChangementPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChangementPosition fetchRequiredChangementPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChangementPosition eoObject = _EOChangementPosition.fetchChangementPosition(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ChangementPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChangementPosition localInstanceIn(EOEditingContext editingContext, EOChangementPosition eo) {
    EOChangementPosition localInstance = (eo == null) ? null : (EOChangementPosition)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
