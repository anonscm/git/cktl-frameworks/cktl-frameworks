package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifCgntRfp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOCgntRaisonFamPerso extends _EOCgntRaisonFamPerso {

	//Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCgntRaisonFamPerso.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	public EOLogImport verifierRecordApresOperation() {
		return super.verifierRecordApresOperation();
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CGNT_RAISON_FAM_PERSO_KEY;
	}

	@Override
	public Integer eimpSource() {
		return crfpSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCrfpSource(value);
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		
		String motif = (String)valeursAttributs.objectForKey(C_MOTIF_CG_RFP_KEY);
		EOMotifCgntRfp motifRfp = EOMotifCgntRfp.fetchMotifCgntRfp(editingContext, EOMotifCgntRfp.C_MOTIF_CG_RFP_KEY, motif);
		if (motifRfp == null) {
			resultat = resultat + C_MOTIF_CG_RFP_KEY+"_Inconnu >>" + motif + System.getProperty("line.separator");
		}

		resultat+=verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);
		return resultat;
	}

	public String temConfirme() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
		return null;
	}
	public void setTemConfirme(String value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
		return;
	}
	public String temGestEtab() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
		return null;
	}
	public void setTemGestEtab(String value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
	}

	@Override
	public NSTimestamp dAnnulation() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
		return null;
	}

	@Override
	public void setDAnnulation(NSTimestamp value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
	}

	@Override
	public String noArreteAnnulation() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
		return null;
	}

	@Override
	public void setNoArreteAnnulation(String value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour CongeAvecArrete
	}

	@Override
	public NSTimestamp dateCommission() {
		return null;
	}

	@Override
	protected String verifierConge(EOEditingContext editingContext) {
		return null;
	}

	@Override
	public String sourceKey() {
		return CRFP_SOURCE_KEY;
	}

	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCgntRaisonFamPerso) conge);
	}
}
