// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOAffectation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOAffectation extends ObjetImportPourIndividuOuStructure {
	public static final String ENTITY_NAME = "Affectation";

	// Attributes
	public static final String AFF_SOURCE_KEY = "affSource";
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String CTR_SOURCE_KEY = "ctrSource";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_AFFECTATION_KEY = "dDebAffectation";
	public static final String D_FIN_AFFECTATION_KEY = "dFinAffectation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NUM_QUOT_AFFECTATION_KEY = "numQuotAffectation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PRINCIPAL_KEY = "temPrincipal";

	// Relationships
	public static final String CONTRAT_KEY = "contrat";
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOAffectation.class);

  public EOAffectation localInstanceIn(EOEditingContext editingContext) {
    EOAffectation localInstance = (EOAffectation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer affSource() {
    return (Integer) storedValueForKey("affSource");
  }

  public void setAffSource(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating affSource from " + affSource() + " to " + value);
    }
    takeStoredValueForKey(value, "affSource");
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public Integer ctrSource() {
    return (Integer) storedValueForKey("ctrSource");
  }

  public void setCtrSource(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating ctrSource from " + ctrSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ctrSource");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebAffectation() {
    return (NSTimestamp) storedValueForKey("dDebAffectation");
  }

  public void setDDebAffectation(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dDebAffectation from " + dDebAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebAffectation");
  }

  public NSTimestamp dFinAffectation() {
    return (NSTimestamp) storedValueForKey("dFinAffectation");
  }

  public void setDFinAffectation(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dFinAffectation from " + dFinAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAffectation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Long numQuotAffectation() {
    return (Long) storedValueForKey("numQuotAffectation");
  }

  public void setNumQuotAffectation(Long value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating numQuotAffectation from " + numQuotAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotAffectation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPrincipal() {
    return (String) storedValueForKey("temPrincipal");
  }

  public void setTemPrincipal(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating temPrincipal from " + temPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipal");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOContrat contrat() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOContrat value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOAffectation createAffectation(EOEditingContext editingContext, Integer affSource
, NSTimestamp dCreation
, NSTimestamp dDebAffectation
, NSTimestamp dModification
, Integer idSource
, Long numQuotAffectation
, String statut
, String strSource
, String temImport
, String temPrincipal
) {
    EOAffectation eo = (EOAffectation) EOUtilities.createAndInsertInstance(editingContext, _EOAffectation.ENTITY_NAME);    
		eo.setAffSource(affSource);
		eo.setDCreation(dCreation);
		eo.setDDebAffectation(dDebAffectation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setNumQuotAffectation(numQuotAffectation);
		eo.setStatut(statut);
		eo.setStrSource(strSource);
		eo.setTemImport(temImport);
		eo.setTemPrincipal(temPrincipal);
    return eo;
  }

  public static NSArray<EOAffectation> fetchAllAffectations(EOEditingContext editingContext) {
    return _EOAffectation.fetchAllAffectations(editingContext, null);
  }

  public static NSArray<EOAffectation> fetchAllAffectations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAffectation.fetchAffectations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAffectation> fetchAffectations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAffectation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAffectation> eoObjects = (NSArray<EOAffectation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAffectation fetchAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectation.fetchAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAffectation fetchAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAffectation> eoObjects = _EOAffectation.fetchAffectations(editingContext, qualifier, null);
    EOAffectation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAffectation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Affectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAffectation fetchRequiredAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectation.fetchRequiredAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAffectation fetchRequiredAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAffectation eoObject = _EOAffectation.fetchAffectation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Affectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAffectation localInstanceIn(EOEditingContext editingContext, EOAffectation eo) {
    EOAffectation localInstance = (eo == null) ? null : (EOAffectation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
