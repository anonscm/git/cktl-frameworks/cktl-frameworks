package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotProlongation;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueProlongationActivite;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOProlongationActivite extends _EOProlongationActivite {

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		return log;
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		String valeur = (String) valeursAttributs.objectForKey(C_MOTIF_PROLONGATION_KEY);
		if (valeur != null) {
			EOTypeMotProlongation motifProlongation = EOTypeMotProlongation.fetchTypeMotProlongation(editingContext,
					EOTypeMotProlongation.C_TYPE_MOT_PROLONGATION_KEY, valeur);
			if (motifProlongation == null) {
				resultat = resultat + "Motif_Prolongation_Inconnu >>" + valeur + System.getProperty("line.separator");
			} else if (!motifProlongation.typeMotif().equals(EOTypeMotProlongation.MOTIF_PROLONGATION)) {
				resultat = resultat + "Motif_Prolongation_Non_Valide >>" + valeur + System.getProperty("line.separator");
			}
		}
		return resultat;
	}

	// Méthodes protégées

	/** La verification des chevauchements est faite par un autre biais */
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueProlongationActivite.findForIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.PROLONGATION_ACTIVITE_KEY;
	}

	@Override
	public Integer eimpSource() {
		return proSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setProSource(value);
	}

}
