package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMad;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class EOMad extends _EOMad {

	@Override
	public Integer eimpSource() {
		return madSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setMadSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.MAD_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);

		String rne = (String) valeursAttributs.objectForKey(C_RNE_KEY);
		if (rne != null) {
			EORne rneRecord = EORne.getFromCode(editingContext, rne);
			if (rneRecord == null)
				resultat = resultat + "Rne_Inconnu >>" + rne + System.getProperty("line.separator");
		}

		String typeMad = (String) valeursAttributs.objectForKey(C_TYPE_MAD_KEY);
		if (typeMad != null) {
			EOTypeMad typeMadRecord = EOTypeMad.getFromCode(editingContext, typeMad);
			if (typeMadRecord == null)
				resultat = resultat + "TypeMad_Inconnu >>" + typeMad + System.getProperty("line.separator");
		}

		resultat = resultat + verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);

		return resultat;
	}
}
