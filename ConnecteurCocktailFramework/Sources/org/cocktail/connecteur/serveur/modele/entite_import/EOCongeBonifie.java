package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeBonifie;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class EOCongeBonifie extends _EOCongeBonifie {
	private static Logger log = Logger.getLogger(EOCongeBonifie.class);

	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCongeBonifie.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	public EOLogImport verifierRecordApresOperation() {
		return super.verifierRecordApresOperation();
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_BONIFIE_KEY;
	}

	@Override
	public Integer eimpSource() {
		return cgbSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCgbSource(value);
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		
		String valeur = (String)valeursAttributs.objectForKey(TEM_METRO_DOM_KEY);
		if (valeur != null && !valeur.equals("D") && !valeur.equals("M")) {
			resultat+= "Valeur_Temoin >>" + TEM_METRO_DOM_KEY + " : " + valeur + System.getProperty("line.separator");
		}
		return resultat;
	}

}
