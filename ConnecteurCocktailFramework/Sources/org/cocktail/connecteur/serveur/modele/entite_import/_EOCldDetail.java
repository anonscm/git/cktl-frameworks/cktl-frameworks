// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCldDetail.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCldDetail extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "CldDetail";

	// Attributes
	public static final String CLDD_SOURCE_KEY = "clddSource";
	public static final String CLD_SOURCE_KEY = "cldSource";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEN_TAUX_KEY = "denTaux";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String IND_ORDRE_KEY = "indOrdre";
	public static final String NUM_TAUX_KEY = "numTaux";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String CLD_KEY = "cld";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCldDetail.class);

  public EOCldDetail localInstanceIn(EOEditingContext editingContext) {
    EOCldDetail localInstance = (EOCldDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer clddSource() {
    return (Integer) storedValueForKey("clddSource");
  }

  public void setClddSource(Integer value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating clddSource from " + clddSource() + " to " + value);
    }
    takeStoredValueForKey(value, "clddSource");
  }

  public Integer cldSource() {
    return (Integer) storedValueForKey("cldSource");
  }

  public void setCldSource(Integer value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating cldSource from " + cldSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cldSource");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Long denTaux() {
    return (Long) storedValueForKey("denTaux");
  }

  public void setDenTaux(Long value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating denTaux from " + denTaux() + " to " + value);
    }
    takeStoredValueForKey(value, "denTaux");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer indOrdre() {
    return (Integer) storedValueForKey("indOrdre");
  }

  public void setIndOrdre(Integer value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating indOrdre from " + indOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "indOrdre");
  }

  public Long numTaux() {
    return (Long) storedValueForKey("numTaux");
  }

  public void setNumTaux(Long value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating numTaux from " + numTaux() + " to " + value);
    }
    takeStoredValueForKey(value, "numTaux");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
    	_EOCldDetail.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCld cld() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCld)storedValueForKey("cld");
  }

  public void setCldRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCld value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
      _EOCldDetail.LOG.debug("updating cld from " + cld() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCld oldValue = cld();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cld");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cld");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCldDetail.LOG.isDebugEnabled()) {
      _EOCldDetail.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCldDetail createCldDetail(EOEditingContext editingContext, Integer clddSource
, Integer cldSource
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EOCldDetail eo = (EOCldDetail) EOUtilities.createAndInsertInstance(editingContext, _EOCldDetail.ENTITY_NAME);    
		eo.setClddSource(clddSource);
		eo.setCldSource(cldSource);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCldDetail> fetchAllCldDetails(EOEditingContext editingContext) {
    return _EOCldDetail.fetchAllCldDetails(editingContext, null);
  }

  public static NSArray<EOCldDetail> fetchAllCldDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCldDetail.fetchCldDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCldDetail> fetchCldDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCldDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCldDetail> eoObjects = (NSArray<EOCldDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCldDetail fetchCldDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCldDetail.fetchCldDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCldDetail fetchCldDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCldDetail> eoObjects = _EOCldDetail.fetchCldDetails(editingContext, qualifier, null);
    EOCldDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCldDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CldDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCldDetail fetchRequiredCldDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCldDetail.fetchRequiredCldDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCldDetail fetchRequiredCldDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCldDetail eoObject = _EOCldDetail.fetchCldDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CldDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCldDetail localInstanceIn(EOEditingContext editingContext, EOCldDetail eo) {
    EOCldDetail localInstance = (eo == null) ? null : (EOCldDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
