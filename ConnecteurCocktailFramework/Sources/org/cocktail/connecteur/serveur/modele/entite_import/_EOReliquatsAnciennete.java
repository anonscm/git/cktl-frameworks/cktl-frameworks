// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOReliquatsAnciennete.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOReliquatsAnciennete extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "ReliquatsAnciennete";

	// Attributes
	public static final String ANC_ANNEE_KEY = "ancAnnee";
	public static final String ANC_NB_ANNEES_KEY = "ancNbAnnees";
	public static final String ANC_NB_JOURS_KEY = "ancNbJours";
	public static final String ANC_NB_MOIS_KEY = "ancNbMois";
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EL_SOURCE_KEY = "elSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String REL_SOURCE_KEY = "relSource";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_UTILISE_KEY = "temUtilise";

	// Relationships
	public static final String ELEMENT_CARRIERE_KEY = "elementCarriere";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOTIF_REDUCTION_KEY = "motifReduction";

  private static Logger LOG = Logger.getLogger(_EOReliquatsAnciennete.class);

  public EOReliquatsAnciennete localInstanceIn(EOEditingContext editingContext) {
    EOReliquatsAnciennete localInstance = (EOReliquatsAnciennete)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer ancAnnee() {
    return (Integer) storedValueForKey("ancAnnee");
  }

  public void setAncAnnee(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancAnnee from " + ancAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, "ancAnnee");
  }

  public Integer ancNbAnnees() {
    return (Integer) storedValueForKey("ancNbAnnees");
  }

  public void setAncNbAnnees(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancNbAnnees from " + ancNbAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbAnnees");
  }

  public Integer ancNbJours() {
    return (Integer) storedValueForKey("ancNbJours");
  }

  public void setAncNbJours(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancNbJours from " + ancNbJours() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbJours");
  }

  public Integer ancNbMois() {
    return (Integer) storedValueForKey("ancNbMois");
  }

  public void setAncNbMois(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancNbMois from " + ancNbMois() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbMois");
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer elSource() {
    return (Integer) storedValueForKey("elSource");
  }

  public void setElSource(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating elSource from " + elSource() + " to " + value);
    }
    takeStoredValueForKey(value, "elSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer relSource() {
    return (Integer) storedValueForKey("relSource");
  }

  public void setRelSource(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating relSource from " + relSource() + " to " + value);
    }
    takeStoredValueForKey(value, "relSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temUtilise() {
    return (String) storedValueForKey("temUtilise");
  }

  public void setTemUtilise(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating temUtilise from " + temUtilise() + " to " + value);
    }
    takeStoredValueForKey(value, "temUtilise");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere elementCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere)storedValueForKey("elementCarriere");
  }

  public void setElementCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
      _EOReliquatsAnciennete.LOG.debug("updating elementCarriere from " + elementCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere oldValue = elementCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "elementCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "elementCarriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
      _EOReliquatsAnciennete.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction motifReduction() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction)storedValueForKey("motifReduction");
  }

  public void setMotifReductionRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
      _EOReliquatsAnciennete.LOG.debug("updating motifReduction from " + motifReduction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction oldValue = motifReduction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "motifReduction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "motifReduction");
    }
  }
  

  public static EOReliquatsAnciennete createReliquatsAnciennete(EOEditingContext editingContext, Integer carSource
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer elSource
, Integer idSource
, String statut
, String temImport
) {
    EOReliquatsAnciennete eo = (EOReliquatsAnciennete) EOUtilities.createAndInsertInstance(editingContext, _EOReliquatsAnciennete.ENTITY_NAME);    
		eo.setCarSource(carSource);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setElSource(elSource);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOReliquatsAnciennete> fetchAllReliquatsAnciennetes(EOEditingContext editingContext) {
    return _EOReliquatsAnciennete.fetchAllReliquatsAnciennetes(editingContext, null);
  }

  public static NSArray<EOReliquatsAnciennete> fetchAllReliquatsAnciennetes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOReliquatsAnciennete.fetchReliquatsAnciennetes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOReliquatsAnciennete> fetchReliquatsAnciennetes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOReliquatsAnciennete.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOReliquatsAnciennete> eoObjects = (NSArray<EOReliquatsAnciennete>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOReliquatsAnciennete fetchReliquatsAnciennete(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReliquatsAnciennete.fetchReliquatsAnciennete(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReliquatsAnciennete fetchReliquatsAnciennete(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOReliquatsAnciennete> eoObjects = _EOReliquatsAnciennete.fetchReliquatsAnciennetes(editingContext, qualifier, null);
    EOReliquatsAnciennete eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOReliquatsAnciennete)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ReliquatsAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReliquatsAnciennete fetchRequiredReliquatsAnciennete(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReliquatsAnciennete.fetchRequiredReliquatsAnciennete(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReliquatsAnciennete fetchRequiredReliquatsAnciennete(EOEditingContext editingContext, EOQualifier qualifier) {
    EOReliquatsAnciennete eoObject = _EOReliquatsAnciennete.fetchReliquatsAnciennete(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ReliquatsAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReliquatsAnciennete localInstanceIn(EOEditingContext editingContext, EOReliquatsAnciennete eo) {
    EOReliquatsAnciennete localInstance = (eo == null) ? null : (EOReliquatsAnciennete)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
