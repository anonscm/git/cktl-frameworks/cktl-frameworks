// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODepart.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODepart extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Depart";

	// Attributes
	public static final String C_MOTIF_DEPART_KEY = "cMotifDepart";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CESSATION_SERVICE_KEY = "dCessationService";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_EFFET_RADIATION_KEY = "dEffetRadiation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_RADIATION_EMPLOI_KEY = "dRadiationEmploi";
	public static final String EIMP_SOURCE_KEY = "eimpSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LIEU_DEPART_KEY = "lieuDepart";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_DEPART_PREVISIONNEL_KEY = "temDepartPrevisionnel";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EODepart.class);

  public EODepart localInstanceIn(EOEditingContext editingContext) {
    EODepart localInstance = (EODepart)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifDepart() {
    return (String) storedValueForKey("cMotifDepart");
  }

  public void setCMotifDepart(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating cMotifDepart from " + cMotifDepart() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifDepart");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCessationService() {
    return (NSTimestamp) storedValueForKey("dCessationService");
  }

  public void setDCessationService(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dCessationService from " + dCessationService() + " to " + value);
    }
    takeStoredValueForKey(value, "dCessationService");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dEffetRadiation() {
    return (NSTimestamp) storedValueForKey("dEffetRadiation");
  }

  public void setDEffetRadiation(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dEffetRadiation from " + dEffetRadiation() + " to " + value);
    }
    takeStoredValueForKey(value, "dEffetRadiation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dRadiationEmploi() {
    return (NSTimestamp) storedValueForKey("dRadiationEmploi");
  }

  public void setDRadiationEmploi(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dRadiationEmploi from " + dRadiationEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dRadiationEmploi");
  }

  public Integer eimpSource() {
    return (Integer) storedValueForKey("eimpSource");
  }

  public void setEimpSource(Integer value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating eimpSource from " + eimpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "eimpSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String lieuDepart() {
    return (String) storedValueForKey("lieuDepart");
  }

  public void setLieuDepart(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating lieuDepart from " + lieuDepart() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDepart");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temDepartPrevisionnel() {
    return (String) storedValueForKey("temDepartPrevisionnel");
  }

  public void setTemDepartPrevisionnel(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating temDepartPrevisionnel from " + temDepartPrevisionnel() + " to " + value);
    }
    takeStoredValueForKey(value, "temDepartPrevisionnel");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EODepart.LOG.isDebugEnabled()) {
      _EODepart.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EODepart createDepart(EOEditingContext editingContext, String cMotifDepart
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EODepart eo = (EODepart) EOUtilities.createAndInsertInstance(editingContext, _EODepart.ENTITY_NAME);    
		eo.setCMotifDepart(cMotifDepart);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EODepart> fetchAllDeparts(EOEditingContext editingContext) {
    return _EODepart.fetchAllDeparts(editingContext, null);
  }

  public static NSArray<EODepart> fetchAllDeparts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODepart.fetchDeparts(editingContext, null, sortOrderings);
  }

  public static NSArray<EODepart> fetchDeparts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODepart.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODepart> eoObjects = (NSArray<EODepart>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODepart fetchDepart(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepart.fetchDepart(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepart fetchDepart(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODepart> eoObjects = _EODepart.fetchDeparts(editingContext, qualifier, null);
    EODepart eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODepart)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Depart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepart fetchRequiredDepart(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepart.fetchRequiredDepart(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepart fetchRequiredDepart(EOEditingContext editingContext, EOQualifier qualifier) {
    EODepart eoObject = _EODepart.fetchDepart(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Depart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepart localInstanceIn(EOEditingContext editingContext, EODepart eo) {
    EODepart localInstance = (eo == null) ? null : (EODepart)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
