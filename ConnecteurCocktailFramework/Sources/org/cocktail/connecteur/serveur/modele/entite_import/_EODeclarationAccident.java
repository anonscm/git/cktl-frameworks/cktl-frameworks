// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODeclarationAccident.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODeclarationAccident extends ObjetImportPourIndividu {
	public static final String ENTITY_NAME = "DeclarationAccident";

	// Attributes
	public static final String C_TYPE_ACCIDENT_KEY = "cTypeAccident";
	public static final String DACC_CIRCONSTANCE_KEY = "daccCirconstance";
	public static final String DACC_DATE_KEY = "daccDate";
	public static final String DACC_HDEB_AM_KEY = "daccHdebAm";
	public static final String DACC_HDEB_PM_KEY = "daccHdebPm";
	public static final String DACC_HEURE_KEY = "daccHeure";
	public static final String DACC_HFIN_AM_KEY = "daccHfinAm";
	public static final String DACC_HFIN_PM_KEY = "daccHfinPm";
	public static final String DACC_JOUR_KEY = "daccJour";
	public static final String DACC_LOCALITE_KEY = "daccLocalite";
	public static final String DACC_SOURCE_KEY = "daccSource";
	public static final String DACC_SUITE_KEY = "daccSuite";
	public static final String DACC_TRANSPORT_KEY = "daccTransport";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LACC_ORDRE_KEY = "laccOrdre";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EODeclarationAccident.class);

  public EODeclarationAccident localInstanceIn(EOEditingContext editingContext) {
    EODeclarationAccident localInstance = (EODeclarationAccident)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeAccident() {
    return (String) storedValueForKey("cTypeAccident");
  }

  public void setCTypeAccident(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating cTypeAccident from " + cTypeAccident() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAccident");
  }

  public String daccCirconstance() {
    return (String) storedValueForKey("daccCirconstance");
  }

  public void setDaccCirconstance(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccCirconstance from " + daccCirconstance() + " to " + value);
    }
    takeStoredValueForKey(value, "daccCirconstance");
  }

  public NSTimestamp daccDate() {
    return (NSTimestamp) storedValueForKey("daccDate");
  }

  public void setDaccDate(NSTimestamp value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccDate from " + daccDate() + " to " + value);
    }
    takeStoredValueForKey(value, "daccDate");
  }

  public String daccHdebAm() {
    return (String) storedValueForKey("daccHdebAm");
  }

  public void setDaccHdebAm(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccHdebAm from " + daccHdebAm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHdebAm");
  }

  public String daccHdebPm() {
    return (String) storedValueForKey("daccHdebPm");
  }

  public void setDaccHdebPm(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccHdebPm from " + daccHdebPm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHdebPm");
  }

  public String daccHeure() {
    return (String) storedValueForKey("daccHeure");
  }

  public void setDaccHeure(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccHeure from " + daccHeure() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHeure");
  }

  public String daccHfinAm() {
    return (String) storedValueForKey("daccHfinAm");
  }

  public void setDaccHfinAm(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccHfinAm from " + daccHfinAm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHfinAm");
  }

  public String daccHfinPm() {
    return (String) storedValueForKey("daccHfinPm");
  }

  public void setDaccHfinPm(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccHfinPm from " + daccHfinPm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHfinPm");
  }

  public String daccJour() {
    return (String) storedValueForKey("daccJour");
  }

  public void setDaccJour(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccJour from " + daccJour() + " to " + value);
    }
    takeStoredValueForKey(value, "daccJour");
  }

  public String daccLocalite() {
    return (String) storedValueForKey("daccLocalite");
  }

  public void setDaccLocalite(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccLocalite from " + daccLocalite() + " to " + value);
    }
    takeStoredValueForKey(value, "daccLocalite");
  }

  public Integer daccSource() {
    return (Integer) storedValueForKey("daccSource");
  }

  public void setDaccSource(Integer value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccSource from " + daccSource() + " to " + value);
    }
    takeStoredValueForKey(value, "daccSource");
  }

  public String daccSuite() {
    return (String) storedValueForKey("daccSuite");
  }

  public void setDaccSuite(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccSuite from " + daccSuite() + " to " + value);
    }
    takeStoredValueForKey(value, "daccSuite");
  }

  public String daccTransport() {
    return (String) storedValueForKey("daccTransport");
  }

  public void setDaccTransport(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating daccTransport from " + daccTransport() + " to " + value);
    }
    takeStoredValueForKey(value, "daccTransport");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer laccOrdre() {
    return (Integer) storedValueForKey("laccOrdre");
  }

  public void setLaccOrdre(Integer value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating laccOrdre from " + laccOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "laccOrdre");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
    	_EODeclarationAccident.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EODeclarationAccident.LOG.isDebugEnabled()) {
      _EODeclarationAccident.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EODeclarationAccident createDeclarationAccident(EOEditingContext editingContext, String cTypeAccident
, NSTimestamp daccDate
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EODeclarationAccident eo = (EODeclarationAccident) EOUtilities.createAndInsertInstance(editingContext, _EODeclarationAccident.ENTITY_NAME);    
		eo.setCTypeAccident(cTypeAccident);
		eo.setDaccDate(daccDate);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EODeclarationAccident> fetchAllDeclarationAccidents(EOEditingContext editingContext) {
    return _EODeclarationAccident.fetchAllDeclarationAccidents(editingContext, null);
  }

  public static NSArray<EODeclarationAccident> fetchAllDeclarationAccidents(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODeclarationAccident.fetchDeclarationAccidents(editingContext, null, sortOrderings);
  }

  public static NSArray<EODeclarationAccident> fetchDeclarationAccidents(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODeclarationAccident.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODeclarationAccident> eoObjects = (NSArray<EODeclarationAccident>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODeclarationAccident fetchDeclarationAccident(EOEditingContext editingContext, String keyName, Object value) {
    return _EODeclarationAccident.fetchDeclarationAccident(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODeclarationAccident fetchDeclarationAccident(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODeclarationAccident> eoObjects = _EODeclarationAccident.fetchDeclarationAccidents(editingContext, qualifier, null);
    EODeclarationAccident eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODeclarationAccident)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one DeclarationAccident that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODeclarationAccident fetchRequiredDeclarationAccident(EOEditingContext editingContext, String keyName, Object value) {
    return _EODeclarationAccident.fetchRequiredDeclarationAccident(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODeclarationAccident fetchRequiredDeclarationAccident(EOEditingContext editingContext, EOQualifier qualifier) {
    EODeclarationAccident eoObject = _EODeclarationAccident.fetchDeclarationAccident(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no DeclarationAccident that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODeclarationAccident localInstanceIn(EOEditingContext editingContext, EODeclarationAccident eo) {
    EODeclarationAccident localInstance = (eo == null) ? null : (EODeclarationAccident)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
