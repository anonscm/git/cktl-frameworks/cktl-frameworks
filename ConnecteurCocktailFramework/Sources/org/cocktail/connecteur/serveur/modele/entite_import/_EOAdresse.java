// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOAdresse.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOAdresse extends ObjetImportPourIndividuOuStructure {
	public static final String ENTITY_NAME = "Adresse";

	// Attributes
	public static final String ADRESSE1_KEY = "adresse1";
	public static final String ADRESSE2_KEY = "adresse2";
	public static final String ADR_SOURCE_KEY = "adrSource";
	public static final String BIS_TER_KEY = "bisTer";
	public static final String BOITE_POSTALE_KEY = "boitePostale";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String C_VOIE_KEY = "cVoie";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String E_MAIL_KEY = "eMail";
	public static final String HABITANT_CHEZ_KEY = "habitantChez";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NOM_VOIE_KEY = "nomVoie";
	public static final String NO_VOIE_KEY = "noVoie";
	public static final String OPERATION_KEY = "operation";
	public static final String PAYS_KEY = "pays";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PRINCIPAL_KEY = "temPrincipal";
	public static final String TYPE_ADRESSE_KEY = "typeAdresse";
	public static final String VILLE_KEY = "ville";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TO_TYPE_VOIE_KEY = "toTypeVoie";

  private static Logger LOG = Logger.getLogger(_EOAdresse.class);

  public EOAdresse localInstanceIn(EOEditingContext editingContext) {
    EOAdresse localInstance = (EOAdresse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String adresse1() {
    return (String) storedValueForKey("adresse1");
  }

  public void setAdresse1(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating adresse1 from " + adresse1() + " to " + value);
    }
    takeStoredValueForKey(value, "adresse1");
  }

  public String adresse2() {
    return (String) storedValueForKey("adresse2");
  }

  public void setAdresse2(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating adresse2 from " + adresse2() + " to " + value);
    }
    takeStoredValueForKey(value, "adresse2");
  }

  public Integer adrSource() {
    return (Integer) storedValueForKey("adrSource");
  }

  public void setAdrSource(Integer value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating adrSource from " + adrSource() + " to " + value);
    }
    takeStoredValueForKey(value, "adrSource");
  }

  public String bisTer() {
    return (String) storedValueForKey("bisTer");
  }

  public void setBisTer(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating bisTer from " + bisTer() + " to " + value);
    }
    takeStoredValueForKey(value, "bisTer");
  }

  public String boitePostale() {
    return (String) storedValueForKey("boitePostale");
  }

  public void setBoitePostale(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating boitePostale from " + boitePostale() + " to " + value);
    }
    takeStoredValueForKey(value, "boitePostale");
  }

  public String codePostal() {
    return (String) storedValueForKey("codePostal");
  }

  public void setCodePostal(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating codePostal from " + codePostal() + " to " + value);
    }
    takeStoredValueForKey(value, "codePostal");
  }

  public String cpEtranger() {
    return (String) storedValueForKey("cpEtranger");
  }

  public void setCpEtranger(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating cpEtranger from " + cpEtranger() + " to " + value);
    }
    takeStoredValueForKey(value, "cpEtranger");
  }

  public String cVoie() {
    return (String) storedValueForKey("cVoie");
  }

  public void setCVoie(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating cVoie from " + cVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "cVoie");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String eMail() {
    return (String) storedValueForKey("eMail");
  }

  public void setEMail(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating eMail from " + eMail() + " to " + value);
    }
    takeStoredValueForKey(value, "eMail");
  }

  public String habitantChez() {
    return (String) storedValueForKey("habitantChez");
  }

  public void setHabitantChez(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating habitantChez from " + habitantChez() + " to " + value);
    }
    takeStoredValueForKey(value, "habitantChez");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String nomVoie() {
    return (String) storedValueForKey("nomVoie");
  }

  public void setNomVoie(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating nomVoie from " + nomVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "nomVoie");
  }

  public String noVoie() {
    return (String) storedValueForKey("noVoie");
  }

  public void setNoVoie(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating noVoie from " + noVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "noVoie");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String pays() {
    return (String) storedValueForKey("pays");
  }

  public void setPays(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating pays from " + pays() + " to " + value);
    }
    takeStoredValueForKey(value, "pays");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temPrincipal() {
    return (String) storedValueForKey("temPrincipal");
  }

  public void setTemPrincipal(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating temPrincipal from " + temPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipal");
  }

  public String typeAdresse() {
    return (String) storedValueForKey("typeAdresse");
  }

  public void setTypeAdresse(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating typeAdresse from " + typeAdresse() + " to " + value);
    }
    takeStoredValueForKey(value, "typeAdresse");
  }

  public String ville() {
    return (String) storedValueForKey("ville");
  }

  public void setVille(String value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
    	_EOAdresse.LOG.debug( "updating ville from " + ville() + " to " + value);
    }
    takeStoredValueForKey(value, "ville");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
      _EOAdresse.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
      _EOAdresse.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie toTypeVoie() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie)storedValueForKey("toTypeVoie");
  }

  public void setToTypeVoieRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie value) {
    if (_EOAdresse.LOG.isDebugEnabled()) {
      _EOAdresse.LOG.debug("updating toTypeVoie from " + toTypeVoie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie oldValue = toTypeVoie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeVoie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeVoie");
    }
  }
  

  public static EOAdresse createAdresse(EOEditingContext editingContext, Integer adrSource
, NSTimestamp dCreation
, NSTimestamp dModification
, String pays
, String statut
, String temImport
, String temPrincipal
, String typeAdresse
) {
    EOAdresse eo = (EOAdresse) EOUtilities.createAndInsertInstance(editingContext, _EOAdresse.ENTITY_NAME);    
		eo.setAdrSource(adrSource);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPays(pays);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemPrincipal(temPrincipal);
		eo.setTypeAdresse(typeAdresse);
    return eo;
  }

  public static NSArray<EOAdresse> fetchAllAdresses(EOEditingContext editingContext) {
    return _EOAdresse.fetchAllAdresses(editingContext, null);
  }

  public static NSArray<EOAdresse> fetchAllAdresses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAdresse.fetchAdresses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAdresse> fetchAdresses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAdresse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAdresse> eoObjects = (NSArray<EOAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAdresse fetchAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAdresse.fetchAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAdresse fetchAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAdresse> eoObjects = _EOAdresse.fetchAdresses(editingContext, qualifier, null);
    EOAdresse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAdresse)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Adresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAdresse fetchRequiredAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAdresse.fetchRequiredAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAdresse fetchRequiredAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAdresse eoObject = _EOAdresse.fetchAdresse(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Adresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAdresse localInstanceIn(EOEditingContext editingContext, EOAdresse eo) {
    EOAdresse localInstance = (eo == null) ? null : (EOAdresse)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
