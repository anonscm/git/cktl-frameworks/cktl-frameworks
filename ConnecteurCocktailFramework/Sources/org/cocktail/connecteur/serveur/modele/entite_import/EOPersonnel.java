//EOPersonnel.java
//Created on Wed Jul 25 15:05:45 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IPersonnelImport;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;



public class EOPersonnel extends _EOPersonnel implements IPersonnelImport {

	public EOPersonnel() {
		super();
		setTemTitulaire(CocktailConstantes.FAUX);
	}


	/** Pas de reparation, la relation avec l'individu est preparee par l'individu */
	public void preparerRelations() {
	}
	 public void supprimerRelations() {
		 if (individu() != null) {
			 removeObjectFromBothSidesOfRelationshipWithKey(individu(), "individu");
		 }
	 }
	/** Pas de verification */
	public EOLogImport verifierRecord() {
		return null;	// Le personnel est créé en même temps que l'individu, il n'y a pas de vérification à faire
	}
	/** Verifier l'unicite du numero de matricule */
	public  EOLogImport verifierRecordApresOperation() {
		// Vérifier que le login est unique si il n'y a pas d'homonyme
		if (individu().statut().equals(STATUT_HOMONYME)) {
			return null;
		}

		if (testUpdateActif()) {
			boolean estInsertion = operation().equals(ObjetImport.OPERATION_INSERTION);
			if (!estMatriculeUnique(estInsertion)) {
				EOLogImport log = new EOLogImport();
				log.initAvecEditingContext(editingContextTravail(),individu().entityName(),"MATRICULE_UTILISE");
				if (individu() != null) {
					log.setIdSource(new Integer(individu().idSource().intValue()));
					log.addObjectToBothSidesOfRelationshipWithKey(individu(),"individu");
				} 
				log.addObjectToBothSidesOfRelationshipWithKey(individu(), individu().nomRelationPourLog());
				setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
				return log;
			}
		}
		return null;
	}
	/** Retourne le qualifier pour rechercher les records dans la base avec les m&ecirc;mes caractéristiques
	 * Normalement n'est pas appeledirectement car les personnels ne sont pas dans les fichiers d'import */
	public EOQualifier construireQualifierImportPourRecordPrecedent() {
		return null;
	}
	/** M&ecirc;me sourceId de l'individu */
	public EOQualifier construireQualifierPourCorrespondance() {
		if (individu() != null) {
			return EOQualifier.qualifierWithQualifierFormat("personnel.individu.idSource = %@",new NSArray(individu().idSource()));
		} else {
			return null;
		}
	}
    /** retourne le personnel associe a l'individu dans le SI destinataire */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() != null) {
			if (individu().operation().equals(ObjetImport.OPERATION_INSERTION)) {
				return null;
			}
			//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
			// sur les correspondances car l'individu n'a peut-être pas encore été créé
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu().objetDestinataireAvecOuSansCorrespondance(editingContext());
			if (individuGrhum != null) {
				return individuGrhum.personnel();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** retourne le nom de la relation pour retrouver les logs lies au record */
	public String nomRelationPourLog() {
		return "personnel";
	}
	/** Pas d'attribut de comparaison avec des relations */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}
	// Méthodes privées
	private boolean estMatriculeUnique(boolean estInsertion) {
		if (noMatricule() == null) {
			return true;
		}
		boolean resultat;
System.out.println("/// estMatriculeUnique("+estInsertion+") : matricule "+noMatricule());
		NSArray personnels = EOGrhumPersonnel.rechercherPersonnelsPourMatricule(editingContextTravail(), noMatricule());
System.out.println("/// personnels.count(): "+personnels.count());
		if (estInsertion) {
			resultat=( personnels.count() == 0);
		} else {
			if (personnels.count() == 0) {	// On a trouvé une correspondance mais pas sur un personnel
				resultat= true;
			} else {
				EOGrhumPersonnel personnel = (EOGrhumPersonnel)personnels.objectAtIndex(0);
				EOGrhumPersonnel destinataireEquiv=(EOGrhumPersonnel)destinataireEquivalent();
				if (destinataireEquiv==null) {
					resultat= true;
					System.out.println("/// destinataireEquiv: "+destinataireEquiv);
				}
				else {
					resultat= personnel.noDossierPers().equals(destinataireEquiv.noDossierPers());
					System.out.println("/// personnel.noDossierPers() : "+personnel.noDossierPers()+", destinataireEquiv.noDossierPers() : "+destinataireEquiv.noDossierPers());
				}
				//return personnel == destinataireEquivalent();
				//return true;
			}
		}
		System.out.println("resultat: "+resultat);
		return resultat;
	}
	// Méthodes statiques
	/** Pas de contr&ocirc;le sur la validite des donnes */
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		return "";
	}
}
