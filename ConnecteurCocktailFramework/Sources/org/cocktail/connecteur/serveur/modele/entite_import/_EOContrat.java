// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOContrat.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOContrat extends ObjetImportPourPersonnel {
	public static final String ENTITY_NAME = "Contrat";

	// Attributes
	public static final String C_RNE_KEY = "cRne";
	public static final String CTR_SOURCE_KEY = "ctrSource";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CONTRAT_TRAV_KEY = "dDebContratTrav";
	public static final String D_FIN_ANTICIPEE_KEY = "dFinAnticipee";
	public static final String D_FIN_CONTRAT_TRAV_KEY = "dFinContratTrav";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_BUDGET_PROPRE_KEY = "temBudgetPropre";
	public static final String TEM_FONCTIONNAIRE_KEY = "temFonctionnaire";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_RECHERCHE_KEY = "temRecherche";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOContrat.class);

  public EOContrat localInstanceIn(EOEditingContext editingContext) {
    EOContrat localInstance = (EOContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public Integer ctrSource() {
    return (Integer) storedValueForKey("ctrSource");
  }

  public void setCtrSource(Integer value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating ctrSource from " + ctrSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ctrSource");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebContratTrav() {
    return (NSTimestamp) storedValueForKey("dDebContratTrav");
  }

  public void setDDebContratTrav(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dDebContratTrav from " + dDebContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebContratTrav");
  }

  public NSTimestamp dFinAnticipee() {
    return (NSTimestamp) storedValueForKey("dFinAnticipee");
  }

  public void setDFinAnticipee(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dFinAnticipee from " + dFinAnticipee() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAnticipee");
  }

  public NSTimestamp dFinContratTrav() {
    return (NSTimestamp) storedValueForKey("dFinContratTrav");
  }

  public void setDFinContratTrav(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dFinContratTrav from " + dFinContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinContratTrav");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temBudgetPropre() {
    return (String) storedValueForKey("temBudgetPropre");
  }

  public void setTemBudgetPropre(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temBudgetPropre from " + temBudgetPropre() + " to " + value);
    }
    takeStoredValueForKey(value, "temBudgetPropre");
  }

  public String temFonctionnaire() {
    return (String) storedValueForKey("temFonctionnaire");
  }

  public void setTemFonctionnaire(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temFonctionnaire from " + temFonctionnaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temFonctionnaire");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temRecherche() {
    return (String) storedValueForKey("temRecherche");
  }

  public void setTemRecherche(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temRecherche from " + temRecherche() + " to " + value);
    }
    takeStoredValueForKey(value, "temRecherche");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
      _EOContrat.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOContrat createContrat(EOEditingContext editingContext, Integer ctrSource
, String cTypeContratTrav
, NSTimestamp dCreation
, NSTimestamp dDebContratTrav
, NSTimestamp dModification
, Integer idSource
, String statut
, String temBudgetPropre
, String temFonctionnaire
, String temImport
) {
    EOContrat eo = (EOContrat) EOUtilities.createAndInsertInstance(editingContext, _EOContrat.ENTITY_NAME);    
		eo.setCtrSource(ctrSource);
		eo.setCTypeContratTrav(cTypeContratTrav);
		eo.setDCreation(dCreation);
		eo.setDDebContratTrav(dDebContratTrav);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemBudgetPropre(temBudgetPropre);
		eo.setTemFonctionnaire(temFonctionnaire);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOContrat> fetchAllContrats(EOEditingContext editingContext) {
    return _EOContrat.fetchAllContrats(editingContext, null);
  }

  public static NSArray<EOContrat> fetchAllContrats(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOContrat.fetchContrats(editingContext, null, sortOrderings);
  }

  public static NSArray<EOContrat> fetchContrats(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOContrat> eoObjects = (NSArray<EOContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOContrat fetchContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContrat.fetchContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContrat fetchContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOContrat> eoObjects = _EOContrat.fetchContrats(editingContext, qualifier, null);
    EOContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOContrat)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Contrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContrat fetchRequiredContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContrat.fetchRequiredContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContrat fetchRequiredContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    EOContrat eoObject = _EOContrat.fetchContrat(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Contrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContrat localInstanceIn(EOEditingContext editingContext, EOContrat eo) {
    EOContrat localInstance = (eo == null) ? null : (EOContrat)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
