// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepriseTempsPlein.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepriseTempsPlein extends ObjetImportPourIndividu {
	public static final String ENTITY_NAME = "RepriseTempsPlein";

	// Attributes
	public static final String ARRETE_ANNULATION_SOURCE_KEY = "arreteAnnulationSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_REPRISE_TEMPS_PLEIN_KEY = "dRepriseTempsPlein";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String RTP_SOURCE_KEY = "rtpSource";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String TP_SOURCE_KEY = "tpSource";

	// Relationships
	public static final String ARRETE_ANNULATION_KEY = "arreteAnnulation";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TEMPS_PARTIEL_KEY = "tempsPartiel";

  private static Logger LOG = Logger.getLogger(_EORepriseTempsPlein.class);

  public EORepriseTempsPlein localInstanceIn(EOEditingContext editingContext) {
    EORepriseTempsPlein localInstance = (EORepriseTempsPlein)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer arreteAnnulationSource() {
    return (Integer) storedValueForKey("arreteAnnulationSource");
  }

  public void setArreteAnnulationSource(Integer value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating arreteAnnulationSource from " + arreteAnnulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "arreteAnnulationSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dRepriseTempsPlein() {
    return (NSTimestamp) storedValueForKey("dRepriseTempsPlein");
  }

  public void setDRepriseTempsPlein(NSTimestamp value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating dRepriseTempsPlein from " + dRepriseTempsPlein() + " to " + value);
    }
    takeStoredValueForKey(value, "dRepriseTempsPlein");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer rtpSource() {
    return (Integer) storedValueForKey("rtpSource");
  }

  public void setRtpSource(Integer value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating rtpSource from " + rtpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "rtpSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public Integer tpSource() {
    return (Integer) storedValueForKey("tpSource");
  }

  public void setTpSource(Integer value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EORepriseTempsPlein.LOG.debug( "updating tpSource from " + tpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "tpSource");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein arreteAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein)storedValueForKey("arreteAnnulation");
  }

  public void setArreteAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
      _EORepriseTempsPlein.LOG.debug("updating arreteAnnulation from " + arreteAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein oldValue = arreteAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "arreteAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "arreteAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
      _EORepriseTempsPlein.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel tempsPartiel() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel)storedValueForKey("tempsPartiel");
  }

  public void setTempsPartielRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel value) {
    if (_EORepriseTempsPlein.LOG.isDebugEnabled()) {
      _EORepriseTempsPlein.LOG.debug("updating tempsPartiel from " + tempsPartiel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel oldValue = tempsPartiel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartiel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartiel");
    }
  }
  

  public static EORepriseTempsPlein createRepriseTempsPlein(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp dRepriseTempsPlein
, Integer idSource
, Integer rtpSource
, String statut
, String temConfirme
, String temGestEtab
, String temImport
, String temValide
) {
    EORepriseTempsPlein eo = (EORepriseTempsPlein) EOUtilities.createAndInsertInstance(editingContext, _EORepriseTempsPlein.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setDRepriseTempsPlein(dRepriseTempsPlein);
		eo.setIdSource(idSource);
		eo.setRtpSource(rtpSource);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EORepriseTempsPlein> fetchAllRepriseTempsPleins(EOEditingContext editingContext) {
    return _EORepriseTempsPlein.fetchAllRepriseTempsPleins(editingContext, null);
  }

  public static NSArray<EORepriseTempsPlein> fetchAllRepriseTempsPleins(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepriseTempsPlein.fetchRepriseTempsPleins(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepriseTempsPlein> fetchRepriseTempsPleins(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepriseTempsPlein.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepriseTempsPlein> eoObjects = (NSArray<EORepriseTempsPlein>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepriseTempsPlein fetchRepriseTempsPlein(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepriseTempsPlein.fetchRepriseTempsPlein(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepriseTempsPlein fetchRepriseTempsPlein(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepriseTempsPlein> eoObjects = _EORepriseTempsPlein.fetchRepriseTempsPleins(editingContext, qualifier, null);
    EORepriseTempsPlein eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepriseTempsPlein)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RepriseTempsPlein that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepriseTempsPlein fetchRequiredRepriseTempsPlein(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepriseTempsPlein.fetchRequiredRepriseTempsPlein(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepriseTempsPlein fetchRequiredRepriseTempsPlein(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepriseTempsPlein eoObject = _EORepriseTempsPlein.fetchRepriseTempsPlein(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RepriseTempsPlein that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepriseTempsPlein localInstanceIn(EOEditingContext editingContext, EORepriseTempsPlein eo) {
    EORepriseTempsPlein localInstance = (eo == null) ? null : (EORepriseTempsPlein)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
