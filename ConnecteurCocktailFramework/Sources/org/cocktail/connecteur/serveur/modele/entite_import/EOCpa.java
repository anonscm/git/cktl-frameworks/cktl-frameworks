package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCpa;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**.
 * Classe gérant les Cessation Progressives d'Activité d'un individu
 * @author alainmalaplate
 *
 */
public class EOCpa extends _EOCpa {

	private static final long serialVersionUID = -1665275686797234416L;
	
	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCpa.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CPA_KEY;
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat= ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return cpaSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCpaSource(value);
	}
}
