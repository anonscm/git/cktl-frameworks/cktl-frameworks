// EOCgntMaladie.java
// Created on Wed Feb 08 17:27:59 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;


import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** Classe metier pour les conges de maladie des contractuels<BR>
 * 1. Le conge de maladie non titulaire ne concerne que les agents contractuels ou les contractuels assimiles qui ont une carri&rgrave;re<BR>
 * 2. Le conge de maladie non titulaire doit &ecirc;tre recouvert enti&egrave;rement par un ou des contrats avenants ou une carrière d’assimile.<BR>
 */
public class EOCgntMaladie extends _EOCgntMaladie {

    public EOCgntMaladie() {
        super();
    }

    // méthodes ajoutées
	public void init() {
		super.init();
		setTemDureeContinue(CocktailConstantes.FAUX);
	}
	// Pas de date de commission
	public NSTimestamp dateCommission() {
		return null;
	}
	// méthodes protégées
	/** Verifie si l'individu est contractuel sur la periode compl&egrave;te */
	protected String verifierConge(EOEditingContext editingContext) {
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource());
		if (individu != null) {
			if (estContractuelSurPeriodeComplete(editingContext,individu,dateDebut(),dateFin()) == false) {
				return "CONTRACTUEL_SUR_PERIODE";
			}
		} else {
			LogManager.logDetail("Pas d'individu pour idSource " + idSource());
		}
		return null;
	}
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeMaladie.validerAttributs(editingContext, valeursAttributs);
		resultat = resultat + verifierTemoin(valeursAttributs, "temDureeContinue");
		// Vérification de la nomenclature
		String anciennete = (String)valeursAttributs.objectForKey("cAnciennete");
		if (anciennete != null) {
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "ParamCgntMal","cAnciennete", anciennete);
			if (record == null) {
				resultat = "Anciennete_Inconnue >>" + anciennete + System.getProperty("line.separator"); 
			}
		}
		return resultat;
	}
	
	public EOLogImport verifierRecord() {
		return super.verifierRecord();
	}

	@Override
	public Integer eimpSource() {
		return cgntSource();
	}
	@Override
	public void setEimpSource(Integer value) {
		setCgntSource(value);
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CGNT_MALADIE_KEY;
	}

	@Override
	public String sourceKey() {
		return CGNT_SOURCE_KEY;
	}

	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCgntMaladie)conge);
	}
}
