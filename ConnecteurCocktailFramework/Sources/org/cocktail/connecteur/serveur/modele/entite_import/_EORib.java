// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORib.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORib extends ObjetImportPourIndividuOuStructure {
	public static final String ENTITY_NAME = "Rib";

	// Attributes
	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String CLE_KEY = "cle";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DOMICILIATION_KEY = "domiciliation";
	public static final String IBAN_KEY = "iban";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_COMPTE_KEY = "noCompte";
	public static final String OPERATION_KEY = "operation";
	public static final String RIB_SOURCE_KEY = "ribSource";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TITULAIRE_KEY = "titulaire";

	// Relationships
	public static final String BANQUE_KEY = "banque";
	public static final String GRHUM_ADRESSE_FOURNISSEUR_KEY = "grhumAdresseFournisseur";
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EORib.class);

  public EORib localInstanceIn(EOEditingContext editingContext) {
    EORib localInstance = (EORib)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String bic() {
    return (String) storedValueForKey("bic");
  }

  public void setBic(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating bic from " + bic() + " to " + value);
    }
    takeStoredValueForKey(value, "bic");
  }

  public String cBanque() {
    return (String) storedValueForKey("cBanque");
  }

  public void setCBanque(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating cBanque from " + cBanque() + " to " + value);
    }
    takeStoredValueForKey(value, "cBanque");
  }

  public String cGuichet() {
    return (String) storedValueForKey("cGuichet");
  }

  public void setCGuichet(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating cGuichet from " + cGuichet() + " to " + value);
    }
    takeStoredValueForKey(value, "cGuichet");
  }

  public String cle() {
    return (String) storedValueForKey("cle");
  }

  public void setCle(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating cle from " + cle() + " to " + value);
    }
    takeStoredValueForKey(value, "cle");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String domiciliation() {
    return (String) storedValueForKey("domiciliation");
  }

  public void setDomiciliation(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating domiciliation from " + domiciliation() + " to " + value);
    }
    takeStoredValueForKey(value, "domiciliation");
  }

  public String iban() {
    return (String) storedValueForKey("iban");
  }

  public void setIban(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating iban from " + iban() + " to " + value);
    }
    takeStoredValueForKey(value, "iban");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noCompte() {
    return (String) storedValueForKey("noCompte");
  }

  public void setNoCompte(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating noCompte from " + noCompte() + " to " + value);
    }
    takeStoredValueForKey(value, "noCompte");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Double ribSource() {
    return (Double) storedValueForKey("ribSource");
  }

  public void setRibSource(Double value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating ribSource from " + ribSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ribSource");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String titulaire() {
    return (String) storedValueForKey("titulaire");
  }

  public void setTitulaire(String value) {
    if (_EORib.LOG.isDebugEnabled()) {
    	_EORib.LOG.debug( "updating titulaire from " + titulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "titulaire");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque banque() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque)storedValueForKey("banque");
  }

  public void setBanqueRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque value) {
    if (_EORib.LOG.isDebugEnabled()) {
      _EORib.LOG.debug("updating banque from " + banque() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque oldValue = banque();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "banque");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "banque");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse grhumAdresseFournisseur() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse)storedValueForKey("grhumAdresseFournisseur");
  }

  public void setGrhumAdresseFournisseurRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse value) {
    if (_EORib.LOG.isDebugEnabled()) {
      _EORib.LOG.debug("updating grhumAdresseFournisseur from " + grhumAdresseFournisseur() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse oldValue = grhumAdresseFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "grhumAdresseFournisseur");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "grhumAdresseFournisseur");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EORib.LOG.isDebugEnabled()) {
      _EORib.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EORib.LOG.isDebugEnabled()) {
      _EORib.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EORib createRib(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Double ribSource
, String statut
, String temImport
, String titulaire
) {
    EORib eo = (EORib) EOUtilities.createAndInsertInstance(editingContext, _EORib.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setRibSource(ribSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTitulaire(titulaire);
    return eo;
  }

  public static NSArray<EORib> fetchAllRibs(EOEditingContext editingContext) {
    return _EORib.fetchAllRibs(editingContext, null);
  }

  public static NSArray<EORib> fetchAllRibs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORib.fetchRibs(editingContext, null, sortOrderings);
  }

  public static NSArray<EORib> fetchRibs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORib.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORib> eoObjects = (NSArray<EORib>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORib fetchRib(EOEditingContext editingContext, String keyName, Object value) {
    return _EORib.fetchRib(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORib fetchRib(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORib> eoObjects = _EORib.fetchRibs(editingContext, qualifier, null);
    EORib eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORib)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Rib that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORib fetchRequiredRib(EOEditingContext editingContext, String keyName, Object value) {
    return _EORib.fetchRequiredRib(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORib fetchRequiredRib(EOEditingContext editingContext, EOQualifier qualifier) {
    EORib eoObject = _EORib.fetchRib(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Rib that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORib localInstanceIn(EOEditingContext editingContext, EORib eo) {
    EORib localInstance = (eo == null) ? null : (EORib)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
