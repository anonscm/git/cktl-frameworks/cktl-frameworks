package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOBonifications extends _EOBonifications {
	@Override
	public void preparerRelations() {
		super.preparerRelations();
		
		EONatureBonification nature = null;
		if (nboCode()!=null)
			nature=EONatureBonification.fetchNatureBonification(editingContextTravail(), EONatureBonification.NBO_CODE_KEY, nboCode());
		setNatureBonificationRelationship(nature);
		
		EONatureBonifTerritoire territoire = null;
		if (nbotCode()!=null)
			territoire=EONatureBonifTerritoire.fetchNatureBonifTerritoire(editingContextTravail(), EONatureBonifTerritoire.NBOT_CODE_KEY, nbotCode());
		setTerritoireRelationship(territoire);
		
		EONatureBonifTaux taux = null;
		if (nbtCode()!=null)
			taux=EONatureBonifTaux.fetchNatureBonifTaux(editingContextTravail(), EONatureBonifTaux.NBT_CODE_KEY, nbtCode());
		setTauxRelationship(taux);
	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setNatureBonificationRelationship(null);
		setTerritoireRelationship(null);
		setTauxRelationship(null);
	}

	@Override
	public Integer eimpSource() {
		return boniSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setBoniSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueBonifications.findForIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.BONIFICATIONS_KEY;
	}


	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		
		String valeur;
		valeur = (String) valeursAttributs.objectForKey(NBO_CODE_KEY);
		if (valeur != null) {
			EONatureBonification nature = EONatureBonification.fetchNatureBonification(editingContext, EONatureBonification.NBO_CODE_KEY, valeur);
			if (nature == null) {
				resultat = resultat + "NBO_Code_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		
		valeur = (String) valeursAttributs.objectForKey(NBOT_CODE_KEY);
		if (valeur != null) {
			EONatureBonifTerritoire territoire = EONatureBonifTerritoire.fetchNatureBonifTerritoire(editingContext, EONatureBonifTerritoire.NBOT_CODE_KEY, valeur);
			if (territoire == null) {
				resultat = resultat + "NBOT_Code_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}

		valeur = (String) valeursAttributs.objectForKey(NBT_CODE_KEY);
		if (valeur != null) {
			EONatureBonifTaux taux = EONatureBonifTaux.fetchNatureBonifTaux(editingContext, EONatureBonifTaux.NBT_CODE_KEY, valeur);
			if (taux == null) {
				resultat = resultat + "NBT_Code_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "temValide");

		return resultat;
	}

}
