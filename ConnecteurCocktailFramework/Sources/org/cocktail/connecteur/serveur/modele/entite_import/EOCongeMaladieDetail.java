package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class EOCongeMaladieDetail extends _EOCongeMaladieDetail {
	@Override
	public void preparerRelations() {
		EOCongeMaladie congeMaladie = EOCongeMaladie.congeMaladiePourId(editingContext(), cmalSource());
		setCongeMaladieRelationship(congeMaladie);
	}

	@Override
	public void supprimerRelations() {
		setCongeMaladieRelationship(null);
	}

	private String verifierDatesDetailPourCongeMaladie(EOCongeMaladie congeMaladie) {
		if (congeMaladie == null)
			return null;
		boolean estOK = true;

		estOK &= DateCtrl.isAfterEq(dateDebut(), congeMaladie.dateDebut());

		if (congeMaladie.dateFin() != null)
			estOK &= DateCtrl.isBeforeEq(dateDebut(), congeMaladie.dateFin());

		if (dateFin() != null)
			estOK &= DateCtrl.isAfterEq(dateFin(), congeMaladie.dateDebut());
		
		if (dateFin() != null && congeMaladie.dateFin() != null)
			estOK &= DateCtrl.isBeforeEq(dateFin(), congeMaladie.dateFin());
		
		if (estOK)
			return null;
		else
			return "DATES_DETAIL_CONGE";
	}

	@Override
	public EOLogImport verifierRecord() {
		EOLogImport log = null;

		String messageLog = verifierDatesDetailPourCongeMaladie(congeMaladie());
		if (congeMaladie() == null && cmalSource() != null) {
			messageLog = "CONGE_NON_IMPORTE";
		} else if (congeMaladie() != null && congeMaladie().statut().equals(STATUT_ERREUR)) {
			messageLog = "CONGE_INVALIDE";
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	@Override
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_MALADIE_DETAIL_KEY;
	}

	@Override
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		return null;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		return "";
	}
}
