// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCarriere.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCarriere extends ObjetImportPourPersonnel {
	public static final String ENTITY_NAME = "Carriere";

	// Attributes
	public static final String CAR_SOURCE_KEY = "carSource";
	public static final String C_TYPE_POPULATION_KEY = "cTypePopulation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CARRIERE_KEY = "dDebCarriere";
	public static final String D_FIN_CARRIERE_KEY = "dFinCarriere";
	public static final String D_MIN_ELEM_CAR_KEY = "dMinElemCar";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_TYPE_POPULATION_KEY = "toTypePopulation";

  private static Logger LOG = Logger.getLogger(_EOCarriere.class);

  public EOCarriere localInstanceIn(EOEditingContext editingContext) {
    EOCarriere localInstance = (EOCarriere)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer carSource() {
    return (Integer) storedValueForKey("carSource");
  }

  public void setCarSource(Integer value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating carSource from " + carSource() + " to " + value);
    }
    takeStoredValueForKey(value, "carSource");
  }

  public String cTypePopulation() {
    return (String) storedValueForKey("cTypePopulation");
  }

  public void setCTypePopulation(String value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating cTypePopulation from " + cTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypePopulation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebCarriere() {
    return (NSTimestamp) storedValueForKey("dDebCarriere");
  }

  public void setDDebCarriere(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dDebCarriere from " + dDebCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebCarriere");
  }

  public NSTimestamp dFinCarriere() {
    return (NSTimestamp) storedValueForKey("dFinCarriere");
  }

  public void setDFinCarriere(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dFinCarriere from " + dFinCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinCarriere");
  }

  public NSTimestamp dMinElemCar() {
    return (NSTimestamp) storedValueForKey("dMinElemCar");
  }

  public void setDMinElemCar(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dMinElemCar from " + dMinElemCar() + " to " + value);
    }
    takeStoredValueForKey(value, "dMinElemCar");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
      _EOCarriere.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation toTypePopulation() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation)storedValueForKey("toTypePopulation");
  }

  public void setToTypePopulationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
      _EOCarriere.LOG.debug("updating toTypePopulation from " + toTypePopulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypePopulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypePopulation");
    }
  }
  

  public static EOCarriere createCarriere(EOEditingContext editingContext, Integer carSource
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
) {
    EOCarriere eo = (EOCarriere) EOUtilities.createAndInsertInstance(editingContext, _EOCarriere.ENTITY_NAME);    
		eo.setCarSource(carSource);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCarriere> fetchAllCarrieres(EOEditingContext editingContext) {
    return _EOCarriere.fetchAllCarrieres(editingContext, null);
  }

  public static NSArray<EOCarriere> fetchAllCarrieres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCarriere.fetchCarrieres(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCarriere> fetchCarrieres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCarriere.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCarriere> eoObjects = (NSArray<EOCarriere>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCarriere fetchCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriere.fetchCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriere fetchCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCarriere> eoObjects = _EOCarriere.fetchCarrieres(editingContext, qualifier, null);
    EOCarriere eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCarriere)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Carriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriere fetchRequiredCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriere.fetchRequiredCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriere fetchRequiredCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCarriere eoObject = _EOCarriere.fetchCarriere(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Carriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriere localInstanceIn(EOEditingContext editingContext, EOCarriere eo) {
    EOCarriere localInstance = (eo == null) ? null : (EOCarriere)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
