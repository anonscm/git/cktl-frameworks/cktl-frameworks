// EOCongeMaladie.java
// Created on Wed Mar 12 15:04:02  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.Constantes;
import org.cocktail.connecteur.serveur.modele.entite_import.conges.PeriodeCongeAvecDateCommission;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
/** R&grave;gles de validation<BR>
 * 1. Le conge de maladie ordinaire ne concerne qu'un agent fonctionnaire titulaire ou stagiaire.<BR>
 * 2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la carri&egrave;re d'accueil dans l'etablissement.<BR>
 * 3. Un conge ordinaire de maladie qui dure plus de 6 mois doit avoir re&ccedil;u l'aval de la commission<BR>
 * @author christine
 */
public class EOCongeMaladie extends _EOCongeMaladie {
	public static double NB_MOIS_CONSECUTIFS_MAX = 12.00;
	public static int NB_MOIS_CONSECUTIFS_AVEC_AVIS = 6;
	public static int NB_MOIS_CONSECUTIFS_AVEC_INFO = 4;

	public EOCongeMaladie() {
		super();
	}


	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_MALADIE_KEY;
	}

	// méthodes ajoutées
	public void init() {
		super.init();
		setTemAccServ(Constantes.FAUX);
	}
	public NSTimestamp dateCommission() {
		return dComMedCom();
	}
	// Méthodes protégées
	protected String verifierConge(EOEditingContext editingContext) {
		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext, idSource(), dateDebut(), dateFin()) == false) {
			return "FONCTIONNAIRE_EN_ACTIVITE";
		} else {
			NSArray periodesContigues = preparerPeriodesContigues(editingContext,null,dateDebut(),dateFin());
			double nbMois = dureeEnMoisConges(periodesContigues);
			if (nbMois > EOCongeMaladie.NB_MOIS_CONSECUTIFS_MAX) {
				return "DUREE_CONGE_DEPASSEE";
			} else if (dComMedCom() == null) {
				if (verifierAvisComitePourDelaiSuperieur6Mois(periodesContigues) == false && nbMois > EOCongeMaladie.NB_MOIS_CONSECUTIFS_AVEC_AVIS) {
					//System.out.println("Erreur REQUIERT_AVIS_COMMISSION: idSource="+idSource()+", conSource="+dateDebut()+", noArrete="+noArrete()+", dateDeb="+dateDebut()+", dateFin="+dateFin()+", nbMois="+nbMois);
					return "REQUIERT_AVIS_COMMISSION";
				}
			}
		}
		return null;
	}
	// Méthodes privées
	// Vérifie si une des périodes de maladie associée comporte la date d'avis de la commission
	private boolean verifierAvisComitePourDelaiSuperieur6Mois(NSArray periodesContigues) {
		if (periodesContigues == null || periodesContigues.count() == 0) {
			return false;
		} else {
			java.util.Enumeration e = periodesContigues.objectEnumerator();
			while (e.hasMoreElements()) {
				PeriodeCongeAvecDateCommission periode = (PeriodeCongeAvecDateCommission)e.nextElement();
				if (periode.dateCommission() != null) {	// Si on en trouve une, c'est OK
					return true;
				}
			}
			return false;
		}
	}
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeMaladie.validerAttributs(editingContext, valeursAttributs);
		resultat = resultat + verifierTemoin(valeursAttributs, "temAccServ");
		return resultat;
	}


	@Override
	public Integer eimpSource() {
		return cmalSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCmalSource(value);
	}

	@Override
	public String sourceKey() {
		return CMAL_SOURCE_KEY;
	}

	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCongeMaladie)conge);
	}
	
	public static EOCongeMaladie congeMaladiePourId(EOEditingContext editingContext, Integer cmalSource) {
		if (cmalSource==null)
			return null;
		return fetchCongeMaladie(editingContext, CMAL_SOURCE_KEY, cmalSource);
	}
}
