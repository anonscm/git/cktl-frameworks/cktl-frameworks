package org.cocktail.connecteur.serveur.modele.entite_import.interfaces;


public interface IIndividuImport {
	

	// Pour gérer les personnels
	public IPersonnelImport getPersonnel();
	
	public String operation();
	
	public String statut();

	public void setStatut(String value);
	
}
