package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.correspondance.EODeclarationAccidentCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAccidentServ;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOCongeAccidentServ extends _EOCongeAccidentServ {
	private static final long serialVersionUID = 2812039135599314589L;

	public Integer eimpSource() {
		return daccSource();
	}

	public void setEimpSource(Integer value) {
		setDaccSource(value);
	}

	public void preparerRelations() {
		super.preparerRelations();
		if (editingContext() != null && daccSource() != null && individu() != null) {
			EODeclarationAccident declAccident = EODeclarationAccidentCorresp.declarationAccidentPourIndividuEtId(editingContext(), idSource(), daccSource());
			setDeclarationAccidentRelationship(declAccident);
		}
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setDeclarationAccidentRelationship(null);
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);

		resultat = resultat + verifierTemoin(valeursAttributs, TEM_CONFIRME_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, TEM_GEST_ETAB_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, TEM_VALIDE_KEY);
		return resultat;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_ACCIDENT_SERV_KEY;
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;

		if (daccSource() != null) {
			if (declarationAccident() == null) {
				messageLog = "DECLARATION_ACCIDENT_NON_IMPORTEE";
			} else if (declarationAccident().temImport().equals(ObjetImport.A_TRANSFERER)) {
				if (declarationAccident().statut().equals(EODeclarationAccident.STATUT_ERREUR)) {
					messageLog = "DECLARATION_ACCIDENT_INVALIDE";
				}
			}
		}

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			if (declarationAccident() != null) {
				log.setDeclarationAccidentRelationship(declarationAccident());
			}
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCongeAccidentServ.findForIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public NSTimestamp dateCommission() {
		return null;
	}

	@Override
	protected String verifierConge(EOEditingContext editingContext) {
		if (dateFin() != null && DateCtrl.isBefore(dateFin(), dateDebut())) {
			return "DATES_DETAIL_ACC_SERV";
		}
		return null;
	}

	@Override
	public String sourceKey() {
		return CAS_SOURCE_KEY;
	}

	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCongeAccidentServ)conge);
	}
}
