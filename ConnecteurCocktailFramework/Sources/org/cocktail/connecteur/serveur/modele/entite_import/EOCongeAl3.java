package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOCongeAl3 extends _EOCongeAl3 {
	@Override
	public String sourceKey() {
		return CAL3_SOURCE_KEY;
	}

	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCongeAl3) conge);

	}

	@Override
	public NSTimestamp dateCommission() {
		return dComMed();
	}

	@Override
	protected String verifierConge(EOEditingContext editingContext) {
		return null;
	}

	@Override
	public Integer eimpSource() {
		return cal3Source();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCal3Source(value);
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_AL3_KEY;
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String temValide = (String) valeursAttributs.objectForKey(TEM_VALIDE_KEY);
		if (temValide == null) {
			valeursAttributs.setObjectForKey("O", TEM_VALIDE_KEY);
		}

		String resultat = CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
		return resultat;
	}
}
