package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifCgStagiaire;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class EOCongeSsTraitement extends _EOCongeSsTraitement {
	private static Logger log = Logger.getLogger(EOCongeSsTraitement.class);

	@Override
	public Integer eimpSource() {
		return cstSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCstSource(value);
	}

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.CONGE_SS_TRAITEMENT_KEY;
	}
	
	@Override
	public EOLogImport verifierRecordApresOperation() {
		return super.verifierRecordApresOperation();
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);

		String value = (String) valeursAttributs.objectForKey(C_MOTIF_CG_STAGIAIRE_KEY);
		if (value != null) {
			EOMotifCgStagiaire motif=EOMotifCgStagiaire.fetchMotifCgStagiaire(editingContext, EOMotifCgStagiaire.C_MOTIF_CG_RFP_KEY, value);
			if (motif == null) {
				resultat += "MotifCgStagiare_Inconnu >>" + value + System.getProperty("line.separator");
			}
		}
		return resultat;
	}
}
