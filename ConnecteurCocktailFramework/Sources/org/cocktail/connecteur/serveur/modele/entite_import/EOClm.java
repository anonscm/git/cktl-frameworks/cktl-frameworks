/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.Constantes;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.InterfaceRequalification;
import org.cocktail.connecteur.serveur.modele.entite_import.conges.PeriodeCongeAvecDateCommission;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
/** R&grave;gles de validation<BR>
 * 1. Le conge de longue maladie  ne concerne qu'un agent fonctionnaire titulaire ou stagiaire.<BR>
 * 2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la carri&egrave;re d'accueil dans 
 * l'etablissement.<BR>
 * 3. Il est impossible de produire un projet d'arr&ecirc;te de conge longue maladie si une date d'avis du comite medical s
 * uperieur est renseignee<BR>
 * 4. La duree maximale du conge est de 6 mois<BR>
 * 5. Dans le cas de conges de longue maladie continus pendant 3 ans, l'agent doit reprendre son travail pendant 1 an pour beneficier 
 * d'un nouveau conge de longue maladie.<BR>
 * * @author christine
 */
public class EOClm extends _EOClm implements InterfaceRequalification {
	// Méthodes ajoutées
	public void init() {
		super.init();
		setTemRequalifClm(Constantes.FAUX);
		setTemProlongClm(Constantes.FAUX);
	}
	public String nomRelationPourLog() {
		return EOLogImport.CLM_KEY;
	}
	public NSTimestamp dateCommission() {
		return dComMedClm();
	}
	// InterfaceRequalification
	public boolean estRequalifie() {
		return temRequalifClm() != null && temRequalifClm().equals(Constantes.VRAI);
	}
	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueClm.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}
	protected String verifierConge(EOEditingContext editingContext) {
		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext, idSource(), dateDebut(), dateFin()) == false) {
			return "FONCTIONNAIRE_EN_ACTIVITE";
		} else if (EOCarriere.fonctionnaireEnActivitePendantPeriode(editingContext,idSource(),dateDebut(),dateFin()) == false) {
			return "POPULATION_FONCTIONNAIRE";
		} else if (dComMedClmSup() != null && (dateArrete() != null || noArrete() != null)) {
			return "DATE_COMMISSION_CLM";
		} else {
			double nbMois = dureeEnMois(dateDebut(),dateFin());
			int nbMoisMax = dureePourCle(editingContext,"clmDureeBloquant");
			if (nbMois > nbMoisMax) {
				return "DUREE_CONGE_DEPASSEE";
			}
			// vérifier, si il existe des congés continus pour une période de 3 ans, qu'il existe plus d'une année écoulée avant la reprise d'un nouveau Clm
			NSArray periodesAnterieures = preparerPeriodesAnterieures(editingContext, dateDebut());
			if (periodesAnterieures.count() > 0) {
				// triés par ordre décroissant
				PeriodeCongeAvecDateCommission dernierClm = (PeriodeCongeAvecDateCommission)periodesAnterieures.objectAtIndex(0);
				int dureePeriodeRef = dureePourCle(editingContext,"clmPeriodeReference");	// durée fournie en année
				LogManager.logDetail("Evaluation de la duree entre clm précédent et clm courant");
				if (dureeEnMois(dernierClm.dateFin(),dateDebut()) < dureePeriodeRef * 12) {
					// moins d'un an écoulé
					nbMoisMax = dureePourCle(editingContext,"clmPleinTraitement") + dureePourCle(editingContext,"clmDemiTraitement");
					NSArray periodesContigues = preparerPeriodesContigues(editingContext, periodesAnterieures, dateDebut(), dateFin());
					if (dureeEnMoisConges(periodesContigues) > nbMoisMax) {	// 3 ans de congés continus
						return "REPRISE_TRAVAIL_CLM";
					}
				}
			}
		}
		return null;
	}
	// Méthodes privées

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
		resultat = resultat + verifierTemoin(valeursAttributs, "temRequalifClm");
		resultat = resultat + verifierTemoin(valeursAttributs, "temProlongClm");
		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return clmSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setClmSource(value);
	}

	@Override
	public String sourceKey() {
		return CLM_SOURCE_KEY;
	}
	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOClm)conge);
	}
}
