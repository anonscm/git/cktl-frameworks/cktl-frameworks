// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCld.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCld extends CongeAvecArrete {
	public static final String ENTITY_NAME = "Cld";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CLD_SOURCE_KEY = "cldSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_CLD_KEY = "dComMedCld";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FIN_ANTICIPEE_KEY = "dFinAnticipee";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_FRACTIONNE_KEY = "temFractionne";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_PROLONG_CLD_KEY = "temProlongCld";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCld.class);

  public EOCld localInstanceIn(EOEditingContext editingContext) {
    EOCld localInstance = (EOCld)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer cldSource() {
    return (Integer) storedValueForKey("cldSource");
  }

  public void setCldSource(Integer value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating cldSource from " + cldSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cldSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedCld() {
    return (NSTimestamp) storedValueForKey("dComMedCld");
  }

  public void setDComMedCld(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dComMedCld from " + dComMedCld() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedCld");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFinAnticipee() {
    return (NSTimestamp) storedValueForKey("dFinAnticipee");
  }

  public void setDFinAnticipee(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dFinAnticipee from " + dFinAnticipee() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAnticipee");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temFractionne() {
    return (String) storedValueForKey("temFractionne");
  }

  public void setTemFractionne(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating temFractionne from " + temFractionne() + " to " + value);
    }
    takeStoredValueForKey(value, "temFractionne");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temProlongCld() {
    return (String) storedValueForKey("temProlongCld");
  }

  public void setTemProlongCld(String value) {
    if (_EOCld.LOG.isDebugEnabled()) {
    	_EOCld.LOG.debug( "updating temProlongCld from " + temProlongCld() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongCld");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCld congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCld)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCld value) {
    if (_EOCld.LOG.isDebugEnabled()) {
      _EOCld.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCld oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCld.LOG.isDebugEnabled()) {
      _EOCld.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCld createCld(EOEditingContext editingContext, Integer cldSource
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temConfirme
, String temFractionne
, String temGestEtab
, String temImport
, String temProlongCld
) {
    EOCld eo = (EOCld) EOUtilities.createAndInsertInstance(editingContext, _EOCld.ENTITY_NAME);    
		eo.setCldSource(cldSource);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemConfirme(temConfirme);
		eo.setTemFractionne(temFractionne);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemProlongCld(temProlongCld);
    return eo;
  }

  public static NSArray<EOCld> fetchAllClds(EOEditingContext editingContext) {
    return _EOCld.fetchAllClds(editingContext, null);
  }

  public static NSArray<EOCld> fetchAllClds(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCld.fetchClds(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCld> fetchClds(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCld.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCld> eoObjects = (NSArray<EOCld>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCld fetchCld(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCld.fetchCld(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCld fetchCld(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCld> eoObjects = _EOCld.fetchClds(editingContext, qualifier, null);
    EOCld eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCld)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Cld that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCld fetchRequiredCld(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCld.fetchRequiredCld(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCld fetchRequiredCld(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCld eoObject = _EOCld.fetchCld(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Cld that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCld localInstanceIn(EOEditingContext editingContext, EOCld eo) {
    EOCld localInstance = (eo == null) ? null : (EOCld)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
