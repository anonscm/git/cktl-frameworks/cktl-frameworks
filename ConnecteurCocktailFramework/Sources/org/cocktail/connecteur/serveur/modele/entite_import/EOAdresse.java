//EOAdresse.java
//Created on Wed Jul 25 15:04:17 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCommune;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOPays;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOAdresse extends _EOAdresse {

	public EOAdresse() {
		super();

	}

	public String adresse1() {
		if (super.adresse1() == null || super.adresse1().isEmpty()) {
			super.setAdresse1(recalculAdresse1());
		}
		return super.adresse1();
	}

	// Méthodes ajoutées
	/** pas de verification complementaire */
	public EOLogImport verifierRecordApresOperation() {
		return null;
	}

	/**
	 * retourne une adresse du SI destinataire avec le m&ecirc;me pays, le
	 * m&ecirc;me adresse1, le m&ecirc;me code postal ou la m&ecirc;me ville ou
	 * si il existe une adresse avec le m&ecirc;me pays et tous les autres
	 * champs nuls
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() == null && structure() == null) {
			return null;
		} else if (estNouvelIndividuOuStructure()) { // A fortiori l'adresse est
														// nouvelle
			return null;
		} else {
			return EOGrhumAdresse.adresseDestinationPourAdresse(editingContextTravail(), this);
		}
	}

	public String nomRelationPourLog() {
		return "adresse";
	}

	// Méthodes protégées
	protected boolean doitAvoirIndividuEtStructure() {
		return false;
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;

		if (!verifierAssociationCommuneCodePostal())
			messageLog = "COMMUNE_INCONNUE";

		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	private boolean verifierAssociationCommuneCodePostal() {
		boolean estOK = true;

		String checkCommune = EOImportParametres.valeurParametrePourCle(editingContext(), "CHECK_COMMNUNE_ADRESSE");

		if (pays() != null && ville() != null && codePostal() != null && pays().equals(EOPays.CODE_PAYS_DEFAUT)
				&& (checkCommune == null || checkCommune.equals("O"))) {
			estOK = EOCommune.rechercherCommune(editingContext(), codePostal(), ville()) != null;
			if (!estOK) {
				if (ville().equals("PARIS") || ville().equals("MARSEILLE") || ville().equals("LYON")) {
					NSMutableArray values = new NSMutableArray(ville() + " *");
					values.addObject(codePostal());
					String stringQualifier = EOCommune.LC_COM_KEY + " like %@ AND " + EOCommune.C_POSTAL_KEY + " = %@";
					EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, values);
					EOFetchSpecification fs = new EOFetchSpecification(EOCommune.ENTITY_NAME, qualifier, null);
					NSArray communes = editingContext().objectsWithFetchSpecification(fs);
					estOK = (communes.count() == 1);
					if (estOK) {
						EOCommune communeEnbase = ((EOCommune) communes.objectAtIndex(0));
						setVille(communeEnbase.lcCom());
					} else {
						values = new NSMutableArray(ville() + " *");
						values.addObject(codePostal());
						values.addObject(codePostal());
						stringQualifier = EOCommune.LC_COM_KEY + " like %@ AND (" + EOCommune.C_POSTAL_KEY + " = %@ or " + EOCommune.C_POSTAL_PRINCIPAL_KEY
								+ " = %@)";
						qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, values);
						fs = new EOFetchSpecification(EOCommune.ENTITY_NAME, qualifier, null);
						communes = editingContext().objectsWithFetchSpecification(fs);
						estOK = (communes.count() >= 1);
						if (estOK) {
							EOCommune communeEnbase = ((EOCommune) communes.objectAtIndex(0));
							setVille(communeEnbase.lcCom());
						}
					}
				}
			}
		}
		return estOK;
	}

	// Méthodes statiques
	/**
	 * Valide les donnees d'une adresse.<BR>
	 * Verifie que le pays, la commune et le code postal pour les adresses en
	 * France et le type d'adresse sont corrects. Force le pays a France si code
	 * postal et commnune sont fournis<BR>
	 * On ne verifie pas les champs obligatoires, cela a ete fait auparavant
	 * 
	 * @param editingContext
	 * @param valeursAttributs
	 *            dictionnaire des attributs a valider ou modifier
	 */
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number idSource = (Number) valeursAttributs.objectForKey("idSource");
		String strSource = (String) valeursAttributs.objectForKey("strSource");
		if (idSource == null && strSource == null) {
			resultat = resultat + "ID_SOURCE_ou_STR_SOURCE >>" + System.getProperty("line.separator");
		} else if (idSource != null && strSource != null) {
			resultat = resultat + "UNE_SEULE_ID >>" + System.getProperty("line.separator");
		}

		String ville = (String) valeursAttributs.objectForKey(VILLE_KEY);
		if (ville != null) {
			ville = ville.toUpperCase();
			valeursAttributs.setObjectForKey(ville, VILLE_KEY);
		}

		String typeAdresse = (String) valeursAttributs.objectForKey("typeAdresse");
		if (typeAdresse != null && Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeAdresse", "tadrCode", typeAdresse) == null) {
			resultat = resultat + "Type_Adresse_Inconnu >>" + typeAdresse + System.getProperty("line.separator");
		}

		String typeVoie = (String) valeursAttributs.objectForKey(C_VOIE_KEY);
		if (typeVoie != null && EOTypeVoie.fetchTypeVoie(editingContext, EOTypeVoie.CODE_VOIE_KEY, typeVoie) == null) {
			resultat = resultat + "Type_Voie_Inconnu >>" + typeVoie + System.getProperty("line.separator");
		}

		return resultat;
	}

	private String recalculAdresse1() {
		String chaineAdresse1 = "";
		chaineAdresse1 = chaineAdresse1 + noVoie();
		if (bisTer() != null && !bisTer().equals("---")) {
			chaineAdresse1 = chaineAdresse1 + " " + bisTer();
		}
		if (toTypeVoie() != null) {
			chaineAdresse1 = chaineAdresse1 + " " + toTypeVoie().libelleVoie();
		}
		if (nomVoie() != null) {
			chaineAdresse1 = chaineAdresse1 + " " + nomVoie();
		}
		return chaineAdresse1;
	}


	public static EOAdresse adressePourSourceId(EOEditingContext editingContext, Integer adrSource) {
		if (adrSource==null)
			return null;

		EOAdresse resultat=null;
		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, ADR_SOURCE_KEY, adrSource);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOAdresse)premierObjetValide(resultats);

		return resultat;
	}

}
