package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.serveur.modele.correspondance.EOAdresseCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOVacataires extends _EOVacataires {
	private static final long serialVersionUID = -2242971675992448424L;
	private static Logger log = Logger.getLogger(EOVacataires.class);

	public EOVacataires() {
		super();
	}

	/** Retourne le nom de la relation liee a un attribut */
	public String nomRelationPourAttribut(String nomAttribut) {
		return super.nomRelationPourAttributComparaison(nomAttribut);
	}

	/**
	 * 
	 */
	public void preparerRelations() {
		super.preparerRelations();

		EOStructure structure = EOStructure.structurePourSourceId(editingContext(), strSource());
		setStructureRelationship(structure);

		EORne rne = EORne.getFromCode(editingContext(), cUai());
		setToUaiRelationship(rne);

		EOTypeContratTravail typeContratTravail = EOTypeContratTravail.getFromCode(editingContext(), cTypeContratTrav());
		setToTypeContratTravailRelationship(typeContratTravail);

		EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext(), cSectionCnu(), cSousSectionCnu());
		setToCnuRelationship(cnu);

		if (editingContext() != null && cCorps() != null) {
			EOCorps corps = EOCorps.getFromCode(editingContext(), cCorps());
			if (corps != null) {
				setToCorpsRelationship(corps);
			}
		}
		if (editingContext() != null && cGrade() != null) {
			EOGrade grade = EOGrade.getFromCode(editingContext(), cGrade());
			if (grade != null) {
				setToGradeRelationship(grade);
			}
		}
		if (editingContext() != null && proCode() != null) {
			EOProfession profession = EOProfession.rechercherProfessionPourProCode(editingContext(), proCode());
			if (profession != null) {
				setToProfessionRelationship(profession);
			}
		}
		EOAdresse adresse = EOAdresse.adressePourSourceId(editingContext(), adrSource());
		setToAdresseRelationship(adresse);
	}

	public void supprimerRelations() {
		setStructureRelationship(null);
		setToAdresseRelationship(null);
		setToCnuRelationship(null);
	}

	public EOLogImport verifierRecord() {
		// EOLogImport log = super.verifierRecord();
		EOLogImport log = null;
		String messageLog = null;

		if (log == null) {

			if (editingContext() != null && idSource() != null) {
				EOIndividu individu = EOIndividu.individuPourSourceId(editingContextTravail(), idSource());
				if (individu == null) {
					messageLog = "INDIVIDU_NON_IMPORTE";
				}
			}

			if (editingContext() != null && strSource() != null) {
				EOStructure structure = EOStructure.structurePourSourceId(editingContextTravail(), strSource());
				if (structure == null) {
					messageLog = "STRUCTURE_NON_IMPORTEE";
				}
			}

			if (cSectionCnu() != null && toCnu()==null) {
				messageLog = "CNU_VACATAIRE";
			}

			EOTypeContratTravail typeContrat = null;

			if (cTypeContratTrav() != null)
				typeContrat = (EOTypeContratTravail) Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "TypeContratTravail",
						"cTypeContratTrav", cTypeContratTrav());

			if (typeContrat != null && !typeContrat.estVacataire()) {
				LogManager.logDetail("Insertion d'un log pour un contrat de type vacataire");
				messageLog = "CONTRAT_TRAV_VACATAIRE";
			}

			if (editingContext() != null && noCnu() != null) {
				EOCnu cnu = EOCnu.rechercherCnuPourNoCnu(editingContext(), noCnu());
				if (cnu == null) {
					LogManager.logDetail("Insertion d'un log pour un numéro de CNU pour un contrat de vacataire");
					messageLog = "CNU_VACATAIRE";
				}
			}

			if (editingContext() != null && cCorps() != null) {
				EOCorps corps = EOCorps.getFromCode(editingContext(), cCorps());
				if (corps == null) {
					LogManager.logDetail("Insertion d'un log pour un coprs pour un contrat de vacataire");
					messageLog = "CORPS_VACATAIRE";
				}
			}

			if (editingContext() != null && cGrade() != null) {
				EOGrade grade = EOGrade.getFromCode(editingContext(), cGrade());
				if (grade == null) {
					LogManager.logDetail("Insertion d'un log pour un grade pour un contrat de vacataire");
					messageLog = "GRADE_VACATAIRE";
				}
			}

			if (editingContext() != null && cUai() != null) {
				EORne rne = EORne.getFromCode(editingContext(), cUai());
				if (rne == null) {
					LogManager.logDetail("Insertion d'un log pour un numéro de UAI pour un contrat de vacataire");
					messageLog = "UAI_VACATAIRE";
				}
			}

			if (editingContext() != null && proCode() != null) {
				EOProfession profession = EOProfession.rechercherProfessionPourProCode(editingContext(), proCode());
				if (profession == null) {
					LogManager.logDetail("Insertion d'un log pour un code de profession pour un contrat de vacataire");
					messageLog = "PROFESSION_VACATAIRE";
				}
			}

			if (toAdresse() == null && adrSource() != null) {
				messageLog = "ADRESSE_NON_IMPORTEE";
			} else if (toAdresse() != null && toAdresse().statut().equals(STATUT_ERREUR)) {
				messageLog = "ADRESSE_INVALIDE";
			}
			
			if (messageLog != null) {
				log = new EOLogImport();
				log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
				log.ajouterRelation(this);
				log.setEimpSource(vacSource());
				log.setIdSource(idSource());
				log.setIndividuRelationship(individu());
				log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
				setStatut(STATUT_ERREUR); 
			}
		}
		return log;
	}

	/**
	 * On vérifie que les champs qui sont renseignés correspondent à une valeur
	 * en base
	 */
	public EOLogImport verifierRecordApresOperation() {
		EOTypeContratTravail typeContrat = null;
		if (cTypeContratTrav() != null)
			typeContrat = EOTypeContratTravail.getFromCode(editingContext(), cTypeContratTrav());

		String messageLog = null;

		if (strSource() != null) {
			EOStructure structure = EOStructure.structurePourSourceId(editingContextTravail(), strSource());
			if (structure == null) {
				LogManager.logDetail("Insertion d'un log pour une structure non importée");
				messageLog = "STRUCTURE_NON_IMPORTEE";
			}
		}

		if (typeContrat != null && !typeContrat.estVacataire()) {
			LogManager.logDetail("Insertion d'un log pour un contrat de type vacataire");
			messageLog = "CONTRAT_TRAV_VACATAIRE";
		}

		if (editingContext() != null && cCorps() != null) {
			EOCorps corps = EOCorps.getFromCode(editingContext(), cCorps());
			if (corps == null) {
				LogManager.logDetail("Insertion d'un log pour un coprs pour un contrat de vacataire");
				messageLog = "CORPS_VACATAIRE";
			}
		}

		if (editingContext() != null && cGrade() != null) {
			EOGrade grade = EOGrade.getFromCode(editingContext(), cGrade());
			if (grade == null) {
				LogManager.logDetail("Insertion d'un log pour un grade pour un contrat de vacataire");
				messageLog = "GRADE_VACATAIRE";
			}
		}

		if (editingContext() != null && cUai() != null) {
			EORne rne = EORne.getFromCode(editingContext(), cUai());
			if (rne == null) {
				LogManager.logDetail("Insertion d'un log pour un numéro de UAI pour un contrat de vacataire");
				messageLog = "UAI_VACATAIRE";
			}
		}

		if (editingContext() != null && proCode() != null) {
			EOProfession profession = EOProfession.rechercherProfessionPourProCode(editingContext(), proCode());
			if (profession == null) {
				LogManager.logDetail("Insertion d'un log pour un code de profession pour un contrat de vacataire");
				messageLog = "PROFESSION_VACATAIRE";
			}
		}

		if (messageLog != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.setIdSource(idSource());
			if (individu() != null) {
				log.addObjectToBothSidesOfRelationshipWithKey(individu(), "individu");
			}
			log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
			setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une erreur
			return log;
		}
		return null;
	}

	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	public String nomRelationPourLog() {
		return EOLogImport.VACATAIRES_KEY;
	}

	// Méthodes protégées
	protected boolean doitAvoirIndividuEtStructure() {
		return false;
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		LogManager.logDetail("Début du pasage dans validerAttributs de EOVacataires");

		String resultat = "";

		Integer noCnu = (Integer) valeursAttributs.objectForKey(NO_CNU_KEY);
		if (noCnu != null) {
			// Vérifier dans la nomenclature si cette section cnu existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOCnu.ENTITY_NAME, EOCnu.C_SECTION_CNU_KEY, noCnu);
			if (record == null) {
				resultat = resultat + "SectionCnu_Inconnue >>" + noCnu + System.getProperty("line.separator");
			}
		}

		Number idSource = (Number) valeursAttributs.objectForKey(ID_SOURCE_KEY);
		if (idSource == null) {
			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
		}
		LogManager.logDetail("Fin du pasage dans validerAttributs de EOVacataires");

		String typeContrat = (String) valeursAttributs.objectForKey(C_TYPE_CONTRAT_TRAV_KEY);
		if (typeContrat != null) {
			EOTypeContratTravail recordTypeContrat = EOTypeContratTravail.getFromCode(editingContext, typeContrat);
			if (recordTypeContrat == null) {
				resultat = resultat + "TypeContratTravail_Inconnu >>" + typeContrat + System.getProperty("line.separator");
			}
		}

		String proCode = (String) valeursAttributs.objectForKey(PRO_CODE_KEY);
		if (proCode != null) {
			EOGenericRecord recordProfession = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOProfession.ENTITY_NAME,
					EOProfession.PRO_CODE_KEY, proCode);
			if (recordProfession == null) {
				resultat = resultat + "ProCode_Inconnu >>" + proCode + System.getProperty("line.separator");
			}
		}

		String cUAI = (String) valeursAttributs.objectForKey(C_UAI_KEY);
		if (cUAI != null) {
			EORne recordRne = EORne.getFromCode(editingContext, cUAI);
			if (recordRne == null) {
				resultat = resultat + "UAI_Inconnu >>" + cUAI + System.getProperty("line.separator");
			}
		}

		String corps = (String) valeursAttributs.objectForKey(C_CORPS_KEY);
		EOCorps recordCorps = null;
		if (corps != null) {
			recordCorps = EOCorps.getFromCode(editingContext, corps);
			if (recordCorps == null) {
				resultat = resultat + "Corps_Inconnu >>" + corps + System.getProperty("line.separator");
			}
		}
		String grade = (String) valeursAttributs.objectForKey(C_GRADE_KEY);
		EOGrade recordGrade = null;
		if (grade != null) {
			recordGrade = EOGrade.getFromCode(editingContext, grade);
			if (recordGrade == null) {
				resultat = resultat + "Grade_Inconnu >>" + grade + System.getProperty("line.separator");
			}
		}
		// Vérifier que le grade est compatible avec le corps
		if (recordCorps != null && recordGrade != null && !recordGrade.toCorps().equals(recordCorps)) {
			resultat = resultat + "Corps_Grade_Incompatible >>" + grade + " incompatible avec " + corps + System.getProperty("line.separator");
		}

		String sectionCnu = (String) valeursAttributs.objectForKey(C_SECTION_CNU_KEY);
		String sousSectionCnu = (String) valeursAttributs.objectForKey(C_SOUS_SECTION_CNU_KEY);
		if (sectionCnu != null || sousSectionCnu != null) {
			// Vérifier dans la nomenclature si cette section cnu existe
			EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext, sectionCnu, sousSectionCnu);
			if (cnu == null) {
				resultat = resultat + "SectionCnu,SousSectionCnu_Inconnus >>" + sectionCnu + "," + sousSectionCnu + System.getProperty("line.separator");
			}
		}

		return resultat;
	}

	public static EOVacataires vacatairesPourSourceId(EOEditingContext editingContext, Integer vacSource) {
		if (vacSource==null)
			return null;

		EOVacataires resultat=null;
		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, VAC_SOURCE_KEY, vacSource);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOVacataires)premierObjetValide(resultats);

		return resultat;
	}
}
