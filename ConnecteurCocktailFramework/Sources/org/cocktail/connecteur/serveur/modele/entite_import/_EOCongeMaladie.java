// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaladie.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaladie extends CongeMaladie {
	public static final String ENTITY_NAME = "CongeMaladie";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CMAL_SOURCE_KEY = "cmalSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRET_TRAVAIL_KEY = "dArretTravail";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_COM_KEY = "dComMedCom";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_ACC_SERV_KEY = "temAccServ";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_EN_CAUSE_KEY = "temEnCause";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_JOUR_CARENCE_KEY = "temJourCarence";
	public static final String TEM_PROLONG_MALADIE_KEY = "temProlongMaladie";
	public static final String TEM_REQUALIF_MALADIE_KEY = "temRequalifMaladie";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeMaladie.class);

  public EOCongeMaladie localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaladie localInstance = (EOCongeMaladie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer cmalSource() {
    return (Integer) storedValueForKey("cmalSource");
  }

  public void setCmalSource(Integer value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating cmalSource from " + cmalSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cmalSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArretTravail() {
    return (NSTimestamp) storedValueForKey("dArretTravail");
  }

  public void setDArretTravail(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dArretTravail from " + dArretTravail() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretTravail");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedCom() {
    return (NSTimestamp) storedValueForKey("dComMedCom");
  }

  public void setDComMedCom(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dComMedCom from " + dComMedCom() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedCom");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temAccServ() {
    return (String) storedValueForKey("temAccServ");
  }

  public void setTemAccServ(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temAccServ from " + temAccServ() + " to " + value);
    }
    takeStoredValueForKey(value, "temAccServ");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temEnCause() {
    return (String) storedValueForKey("temEnCause");
  }

  public void setTemEnCause(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temEnCause from " + temEnCause() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnCause");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temJourCarence() {
    return (String) storedValueForKey("temJourCarence");
  }

  public void setTemJourCarence(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temJourCarence from " + temJourCarence() + " to " + value);
    }
    takeStoredValueForKey(value, "temJourCarence");
  }

  public String temProlongMaladie() {
    return (String) storedValueForKey("temProlongMaladie");
  }

  public void setTemProlongMaladie(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temProlongMaladie from " + temProlongMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongMaladie");
  }

  public String temRequalifMaladie() {
    return (String) storedValueForKey("temRequalifMaladie");
  }

  public void setTemRequalifMaladie(String value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
    	_EOCongeMaladie.LOG.debug( "updating temRequalifMaladie from " + temRequalifMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalifMaladie");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
      _EOCongeMaladie.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeMaladie.LOG.isDebugEnabled()) {
      _EOCongeMaladie.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeMaladie createCongeMaladie(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temAccServ
, String temConfirme
, String temEnCause
, String temGestEtab
, String temImport
, String temProlongMaladie
, String temRequalifMaladie
) {
    EOCongeMaladie eo = (EOCongeMaladie) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaladie.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemAccServ(temAccServ);
		eo.setTemConfirme(temConfirme);
		eo.setTemEnCause(temEnCause);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemImport(temImport);
		eo.setTemProlongMaladie(temProlongMaladie);
		eo.setTemRequalifMaladie(temRequalifMaladie);
    return eo;
  }

  public static NSArray<EOCongeMaladie> fetchAllCongeMaladies(EOEditingContext editingContext) {
    return _EOCongeMaladie.fetchAllCongeMaladies(editingContext, null);
  }

  public static NSArray<EOCongeMaladie> fetchAllCongeMaladies(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaladie.fetchCongeMaladies(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaladie> fetchCongeMaladies(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaladie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaladie> eoObjects = (NSArray<EOCongeMaladie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaladie fetchCongeMaladie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladie.fetchCongeMaladie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladie fetchCongeMaladie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaladie> eoObjects = _EOCongeMaladie.fetchCongeMaladies(editingContext, qualifier, null);
    EOCongeMaladie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaladie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaladie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladie fetchRequiredCongeMaladie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladie.fetchRequiredCongeMaladie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladie fetchRequiredCongeMaladie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaladie eoObject = _EOCongeMaladie.fetchCongeMaladie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaladie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladie localInstanceIn(EOEditingContext editingContext, EOCongeMaladie eo) {
    EOCongeMaladie localInstance = (eo == null) ? null : (EOCongeMaladie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
