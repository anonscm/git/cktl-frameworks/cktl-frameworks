// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuFonctionInstance.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuFonctionInstance extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "IndividuFonctionInstance";

	// Attributes
	public static final String C_FONCTION_KEY = "cFonction";
	public static final String C_INSTANCE_KEY = "cInstance";
	public static final String C_QUALITE_KEY = "cQualite";
	public static final String C_SS_STATUT_KEY = "cSsStatut";
	public static final String C_STATUT_KEY = "cStatut";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String IFI_SOURCE_KEY = "ifiSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOIndividuFonctionInstance.class);

  public EOIndividuFonctionInstance localInstanceIn(EOEditingContext editingContext) {
    EOIndividuFonctionInstance localInstance = (EOIndividuFonctionInstance)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cFonction() {
    return (Integer) storedValueForKey("cFonction");
  }

  public void setCFonction(Integer value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating cFonction from " + cFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "cFonction");
  }

  public String cInstance() {
    return (String) storedValueForKey("cInstance");
  }

  public void setCInstance(String value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating cInstance from " + cInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "cInstance");
  }

  public String cQualite() {
    return (String) storedValueForKey("cQualite");
  }

  public void setCQualite(String value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating cQualite from " + cQualite() + " to " + value);
    }
    takeStoredValueForKey(value, "cQualite");
  }

  public String cSsStatut() {
    return (String) storedValueForKey("cSsStatut");
  }

  public void setCSsStatut(String value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating cSsStatut from " + cSsStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "cSsStatut");
  }

  public String cStatut() {
    return (String) storedValueForKey("cStatut");
  }

  public void setCStatut(String value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating cStatut from " + cStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "cStatut");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer ifiSource() {
    return (Integer) storedValueForKey("ifiSource");
  }

  public void setIfiSource(Integer value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating ifiSource from " + ifiSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ifiSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstance.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOIndividuFonctionInstance.LOG.isDebugEnabled()) {
      _EOIndividuFonctionInstance.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOIndividuFonctionInstance createIndividuFonctionInstance(EOEditingContext editingContext, Integer cFonction
, String cInstance
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, Integer ifiSource
, String operation
, String statut
, String temImport
) {
    EOIndividuFonctionInstance eo = (EOIndividuFonctionInstance) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuFonctionInstance.ENTITY_NAME);    
		eo.setCFonction(cFonction);
		eo.setCInstance(cInstance);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setIfiSource(ifiSource);
		eo.setOperation(operation);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOIndividuFonctionInstance> fetchAllIndividuFonctionInstances(EOEditingContext editingContext) {
    return _EOIndividuFonctionInstance.fetchAllIndividuFonctionInstances(editingContext, null);
  }

  public static NSArray<EOIndividuFonctionInstance> fetchAllIndividuFonctionInstances(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuFonctionInstance.fetchIndividuFonctionInstances(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuFonctionInstance> fetchIndividuFonctionInstances(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuFonctionInstance.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuFonctionInstance> eoObjects = (NSArray<EOIndividuFonctionInstance>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuFonctionInstance fetchIndividuFonctionInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionInstance.fetchIndividuFonctionInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionInstance fetchIndividuFonctionInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuFonctionInstance> eoObjects = _EOIndividuFonctionInstance.fetchIndividuFonctionInstances(editingContext, qualifier, null);
    EOIndividuFonctionInstance eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuFonctionInstance)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuFonctionInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionInstance fetchRequiredIndividuFonctionInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionInstance.fetchRequiredIndividuFonctionInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionInstance fetchRequiredIndividuFonctionInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuFonctionInstance eoObject = _EOIndividuFonctionInstance.fetchIndividuFonctionInstance(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuFonctionInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionInstance localInstanceIn(EOEditingContext editingContext, EOIndividuFonctionInstance eo) {
    EOIndividuFonctionInstance localInstance = (eo == null) ? null : (EOIndividuFonctionInstance)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
