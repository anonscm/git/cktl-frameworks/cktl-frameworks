// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuFonctionStructurelle.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuFonctionStructurelle extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "IndividuFonctionStructurelle";

	// Attributes
	public static final String C_FONCTION_KEY = "cFonction";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String IFS_SOURCE_KEY = "ifsSource";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOIndividuFonctionStructurelle.class);

  public EOIndividuFonctionStructurelle localInstanceIn(EOEditingContext editingContext) {
    EOIndividuFonctionStructurelle localInstance = (EOIndividuFonctionStructurelle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cFonction() {
    return (Integer) storedValueForKey("cFonction");
  }

  public void setCFonction(Integer value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating cFonction from " + cFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "cFonction");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer ifsSource() {
    return (Integer) storedValueForKey("ifsSource");
  }

  public void setIfsSource(Integer value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating ifsSource from " + ifsSource() + " to " + value);
    }
    takeStoredValueForKey(value, "ifsSource");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelle.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
      _EOIndividuFonctionStructurelle.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
      _EOIndividuFonctionStructurelle.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOIndividuFonctionStructurelle createIndividuFonctionStructurelle(EOEditingContext editingContext, Integer cFonction
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, Integer ifsSource
, String operation
, String statut
, String strSource
, String temImport
) {
    EOIndividuFonctionStructurelle eo = (EOIndividuFonctionStructurelle) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuFonctionStructurelle.ENTITY_NAME);    
		eo.setCFonction(cFonction);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setIfsSource(ifsSource);
		eo.setOperation(operation);
		eo.setStatut(statut);
		eo.setStrSource(strSource);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOIndividuFonctionStructurelle> fetchAllIndividuFonctionStructurelles(EOEditingContext editingContext) {
    return _EOIndividuFonctionStructurelle.fetchAllIndividuFonctionStructurelles(editingContext, null);
  }

  public static NSArray<EOIndividuFonctionStructurelle> fetchAllIndividuFonctionStructurelles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuFonctionStructurelle.fetchIndividuFonctionStructurelles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuFonctionStructurelle> fetchIndividuFonctionStructurelles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuFonctionStructurelle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuFonctionStructurelle> eoObjects = (NSArray<EOIndividuFonctionStructurelle>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuFonctionStructurelle fetchIndividuFonctionStructurelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionStructurelle.fetchIndividuFonctionStructurelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionStructurelle fetchIndividuFonctionStructurelle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuFonctionStructurelle> eoObjects = _EOIndividuFonctionStructurelle.fetchIndividuFonctionStructurelles(editingContext, qualifier, null);
    EOIndividuFonctionStructurelle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuFonctionStructurelle)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuFonctionStructurelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionStructurelle fetchRequiredIndividuFonctionStructurelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionStructurelle.fetchRequiredIndividuFonctionStructurelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionStructurelle fetchRequiredIndividuFonctionStructurelle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuFonctionStructurelle eoObject = _EOIndividuFonctionStructurelle.fetchIndividuFonctionStructurelle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuFonctionStructurelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionStructurelle localInstanceIn(EOEditingContext editingContext, EOIndividuFonctionStructurelle eo) {
    EOIndividuFonctionStructurelle localInstance = (eo == null) ? null : (EOIndividuFonctionStructurelle)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
