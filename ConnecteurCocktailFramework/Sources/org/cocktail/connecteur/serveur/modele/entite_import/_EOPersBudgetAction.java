// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPersBudgetAction.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPersBudgetAction extends ObjetImport {
	public static final String ENTITY_NAME = "PersBudgetAction";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LOLF_CODE_KEY = "lolfCode";
	public static final String LOLF_NIVEAU_KEY = "lolfNiveau";
	public static final String OPERATION_KEY = "operation";
	public static final String PBAC_SOURCE_KEY = "pbacSource";
	public static final String PBUD_SOURCE_KEY = "pbudSource";
	public static final String POURCENTAGE_KEY = "pourcentage";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String TO_LOLF_NOMENCLATURE_DEPENSE_KEY = "toLolfNomenclatureDepense";
	public static final String TO_PERS_BUDGET_KEY = "toPersBudget";

  private static Logger LOG = Logger.getLogger(_EOPersBudgetAction.class);

  public EOPersBudgetAction localInstanceIn(EOEditingContext editingContext) {
    EOPersBudgetAction localInstance = (EOPersBudgetAction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lolfCode() {
    return (String) storedValueForKey("lolfCode");
  }

  public void setLolfCode(String value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating lolfCode from " + lolfCode() + " to " + value);
    }
    takeStoredValueForKey(value, "lolfCode");
  }

  public Integer lolfNiveau() {
    return (Integer) storedValueForKey("lolfNiveau");
  }

  public void setLolfNiveau(Integer value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating lolfNiveau from " + lolfNiveau() + " to " + value);
    }
    takeStoredValueForKey(value, "lolfNiveau");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Integer pbacSource() {
    return (Integer) storedValueForKey("pbacSource");
  }

  public void setPbacSource(Integer value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating pbacSource from " + pbacSource() + " to " + value);
    }
    takeStoredValueForKey(value, "pbacSource");
  }

  public Integer pbudSource() {
    return (Integer) storedValueForKey("pbudSource");
  }

  public void setPbudSource(Integer value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating pbudSource from " + pbudSource() + " to " + value);
    }
    takeStoredValueForKey(value, "pbudSource");
  }

  public Double pourcentage() {
    return (Double) storedValueForKey("pourcentage");
  }

  public void setPourcentage(Double value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating pourcentage from " + pourcentage() + " to " + value);
    }
    takeStoredValueForKey(value, "pourcentage");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
    	_EOPersBudgetAction.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense toLolfNomenclatureDepense() {
    return (org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense)storedValueForKey("toLolfNomenclatureDepense");
  }

  public void setToLolfNomenclatureDepenseRelationship(org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
      _EOPersBudgetAction.LOG.debug("updating toLolfNomenclatureDepense from " + toLolfNomenclatureDepense() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense oldValue = toLolfNomenclatureDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLolfNomenclatureDepense");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLolfNomenclatureDepense");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget toPersBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget)storedValueForKey("toPersBudget");
  }

  public void setToPersBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget value) {
    if (_EOPersBudgetAction.LOG.isDebugEnabled()) {
      _EOPersBudgetAction.LOG.debug("updating toPersBudget from " + toPersBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget oldValue = toPersBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersBudget");
    }
  }
  

  public static EOPersBudgetAction createPersBudgetAction(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String lolfCode
, Integer lolfNiveau
, String operation
, Integer pbacSource
, Integer pbudSource
, Double pourcentage
, String statut
, String temImport
) {
    EOPersBudgetAction eo = (EOPersBudgetAction) EOUtilities.createAndInsertInstance(editingContext, _EOPersBudgetAction.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLolfCode(lolfCode);
		eo.setLolfNiveau(lolfNiveau);
		eo.setOperation(operation);
		eo.setPbacSource(pbacSource);
		eo.setPbudSource(pbudSource);
		eo.setPourcentage(pourcentage);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOPersBudgetAction> fetchAllPersBudgetActions(EOEditingContext editingContext) {
    return _EOPersBudgetAction.fetchAllPersBudgetActions(editingContext, null);
  }

  public static NSArray<EOPersBudgetAction> fetchAllPersBudgetActions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPersBudgetAction.fetchPersBudgetActions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPersBudgetAction> fetchPersBudgetActions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPersBudgetAction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPersBudgetAction> eoObjects = (NSArray<EOPersBudgetAction>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPersBudgetAction fetchPersBudgetAction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudgetAction.fetchPersBudgetAction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudgetAction fetchPersBudgetAction(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPersBudgetAction> eoObjects = _EOPersBudgetAction.fetchPersBudgetActions(editingContext, qualifier, null);
    EOPersBudgetAction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPersBudgetAction)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PersBudgetAction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudgetAction fetchRequiredPersBudgetAction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudgetAction.fetchRequiredPersBudgetAction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudgetAction fetchRequiredPersBudgetAction(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPersBudgetAction eoObject = _EOPersBudgetAction.fetchPersBudgetAction(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PersBudgetAction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudgetAction localInstanceIn(EOEditingContext editingContext, EOPersBudgetAction eo) {
    EOPersBudgetAction localInstance = (eo == null) ? null : (EOPersBudgetAction)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
