//EOContratAvenant.java
//Created on Mon Jan 28 15:35:20 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.PeriodeAvecQuotite;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConditionRecrutement;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EODiscSecondDegre;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOParamTypeContrat;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOReferensEmplois;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteAtos;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratAvenant;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

//24/06/2010 - changement de la superclasse
//08/02/2011 - correction d'un bug dans l'évaluation de la quotité , il ne faut pas prendre en compte dans le
// calcul de la quotité des avenants en update suite à un changement de dates
// 09/03/2011 - les avenants peuvent se chevaucher à condition que la quotité totale soit <= 100%
// 15/03/2011 - ajout des informations concernant les services validés + validations de ces données
public class EOContratAvenant extends _EOContratAvenant {
	/** types de montant */
	public final static String[] TYPES_MONTANT = { "F", "M", "J", "H", "I", "O" };
	private final static String SANS_GRADE = "9990";

	public EOContratAvenant() {
		super();
	}

	/**
	 * Verifie que le grade n'est pas obligatoire, que la date de fin est valide
	 * par rapport a celle du contrat, que les conditions de recrutement sont
	 * compatibles avec le type de contrat, que l'indice est fourni en fonction
	 * de la table ParamTypeContrat et que la duree max. des conditions de
	 * recrutement est respectee, que les details de contrat non annules du
	 * contrat ne sont pas &grave; cheval pendant la periode et que la quotite
	 * totale pendant la periode de tous les details de contrat des contrats de
	 * remuneration principale est < 100
	 */
	public EOLogImport verifierRecordApresOperation() {
		String messageLog = null;
		// Vérifier si le grade est nécessaire en fonction du type de contrat
		EOTypeContratTravail typeContrat = EOTypeContratTravail.getFromCode(editingContext(), contrat().cTypeContratTrav());
		if (cGrade() == null && typeContrat != null && typeContrat.requiertGrade()) {
			LogManager.logDetail("Insertion d'un log pour le grade obligatoire");
			messageLog = "GRADE_OBLIGATOIRE";
		}
		EOParamTypeContrat paramType = null;
		if (typeContrat != null) {
			paramType = EOParamTypeContrat.rechercherParametrePourTypeContrat(editingContext(), contrat().cTypeContratTrav());
		}
		// Vérifier si l'indice est obligatoire
		if (messageLog == null && paramType != null) {
			if (paramType.fournirIndice() && indiceContrat() == null) {
				messageLog = "INDICE_OBLIGATOIRE";
			}
		}
		// Vérifier la date de début du détail
		if (messageLog == null) {
			// vérifier la cohérence de la date début par rapport au contrat
			if (DateCtrl.isBefore(dDebContratAv(), contrat().dDebContratTrav())
					|| (contrat().dFinContratTrav() != null && DateCtrl.isAfter(dDebContratAv(), contrat().dFinContratTrav()))) {
				LogManager.logDetail("Insertion d'un log pour la date début incompatible avec le contrat");
				messageLog = "DATE_DEBUT_INCOMPATIBLE";
			}
		}
		if (messageLog == null) {
			if (dFinContratAv() == null && contrat().dFinContratTrav() != null) {
				LogManager.logDetail("Insertion d'un log pour la date de fin obligatoire");
				messageLog = "DATE_FIN_AVENANT_OBLIGATOIRE";
			} else if (dFinContratAv() != null
					&& (DateCtrl.isBefore(dFinContratAv(), contrat().dDebContratTrav()) || (contrat().dFinContratTrav() != null && DateCtrl.isAfter(
							dFinContratAv(), contrat().dFinContratTrav())))) {
				// vérifier la cohérence de la date de fin par rapport au
				// contrat
				LogManager.logDetail("Insertion d'un log pour la date de fin incohérente");
				messageLog = "DATE_FIN_AVENANT";
			}
		}
		if (messageLog == null) {
			if (cCondRecrut() != null) {
				EOConditionRecrutement conditionRecrutement = EOConditionRecrutement.rechercherAvecTypeContratEtCodeCondition(editingContext(), contrat()
						.cTypeContratTrav(), cCondRecrut());

				if (conditionRecrutement == null) {
					LogManager.logDetail("Insertion d'un log pour la condition de recrutement et le type de contrat");
					messageLog = "COND_RECRUT_INVALIDE";
				} else {
					if (conditionRecrutement.dureeMaxCr() != null) {
						if (dFinContratAv() == null) {
							LogManager.logDetail("Insertion d'un log pour la condition de recrutement et la date de fin obligatoire");
							messageLog = "COND_RECRUT_DATE_FIN_OBLIGATOIRE";
						} else {
							// vérifier si les durées maximum ne sont pas
							// dépassées
							int dureeMax = conditionRecrutement.dureeMaxCr().intValue(); // durée
																							// en
																							// mois
							NSTimestamp dateFinMax = DateCtrl.dateAvecAjoutMois(dDebContratAv(), dureeMax);
							if (DateCtrl.isAfter(dFinContratAv(), dateFinMax)) {
								LogManager.logDetail("Insertion d'un log pour la condition de recrutement et la durée max.");
								messageLog = "COND_RECRUT_DUREE";
							}
						}
					}
				}
			}
		}
		/*
		 * if (messageLog == null && typeContrat.estRemunerationPrincipale()) {
		 * // 09/03/2011 - les avenants peuvent se chevaucher à condition que la
		 * quotité totale soit <= 100% NSArray avenants = EOContratAvenant.
		 * rechercherAvenantsNonAnnulesImportCourantPourContratEtPeriode
		 * (editingContextTravail(), contrat(), dDebContratAv(),
		 * dFinContratAv()); if ((avenants.count() == 1 &&
		 * avenants.containsObject(this) == false) || avenants.count() > 1) {
		 * messageLog = "CHEVAUCHEMENT_AVENANT"; } // Rechercher parmi les
		 * avenants déjà importés, ne pas prendre en compte le détail ancien //
		 * correspondant à l'avenant courant EOMangueContratAvenant
		 * avenantMangue =
		 * (EOMangueContratAvenant)objetDestinataireAvecOuSansCorrespondance
		 * (editingContextTravail()); NSArray avenantsMangue =
		 * EOMangueContratAvenant
		 * .rechercherAvenantsNonAnnulesPourContratEtPeriode
		 * (editingContextTravail(), contrat(), dDebContratAv(),
		 * dFinContratAv()); if ((avenantMangue != null && avenantsMangue !=
		 * null && avenantsMangue.count() == 1 &&
		 * avenantsMangue.containsObject(avenantMangue) == false) ||
		 * avenants.count() > 1) { messageLog = "CHEVAUCHEMENT_AVENANT"; } }
		 */
		// pour les contrats de rémunération principale dont la quotité doit
		// être contrôlée (quotité obligatoire dans paramTypeContrat),
		// vérifier la que la quotité ne dépasse pas 100 avec les autres
		// contrats sur la même période
		boolean controlerQuotite = paramType != null && paramType.fournirQuotite();
		if (messageLog == null && controlerQuotite && contrat().individu() != null && typeContrat != null && typeContrat.estRemunerationPrincipale()) {
			double quotiteTotale = calculerQuotiteTotalePourPeriode(editingContextTravail(), contrat().individu(), this, dDebContratAv(), dFinContratAv());
			if (quotiteTotale > 100) {
				LogManager.logDetail("Insertion d'un log pour la quotité " + quotiteTotale);
				messageLog = "QUOTITE_AVENANT_INVALIDE";
			}
		}
		if (messageLog == null) {
			// 15/03/2011 - vérification des informations de services validés
			if (dValContratAv() != null) {
				if (dFinContratAv() == null || DateCtrl.isAfter(dFinContratAv(), dValContratAv())) {
					messageLog = "DATE_FIN_SERVICE_VALIDE";
				}
			}
			if (messageLog == null && ctraQuotiteCotisation() != null) {
				double quotite = ctraQuotiteCotisation().doubleValue();
				if ((quotite != 0 && (quotite < 50.00 || quotite > 100.00)) || quotite < 0) {
					messageLog = "QUOTITE_COTISATION";
				}
			}
			if (messageLog == null) {
				if (dFinContratAv() != null) {
					if ((ctraDureeValideeAnnees() != null && ctraDureeValideeAnnees().intValue() > 0)
							|| (ctraDureeValideeMois() != null && ctraDureeValideeMois().intValue() > 0)
							|| (ctraDureeValideeJours() != null && ctraDureeValideeJours().intValue() > 0)) {
						int annees = 0, mois = 0, jours = 0;
						if (ctraDureeValideeAnnees() != null && ctraDureeValideeAnnees().intValue() > 0) {
							annees = ctraDureeValideeAnnees().intValue();
						}
						if (ctraDureeValideeMois() != null && ctraDureeValideeMois().intValue() > 0) {
							mois = ctraDureeValideeMois().intValue();
						}
						if (ctraDureeValideeJours() != null && ctraDureeValideeJours().intValue() > 0) {
							jours = ctraDureeValideeJours().intValue();
						}
						NSTimestamp dateCalculee = DateCtrl.timestampByAddingGregorianUnits(dDebContratAv(), annees, mois, jours, 0, 0, 0);
						int nbJours = DateCtrl.nbJoursEntre(dDebContratAv(), dFinContratAv(), true);
						int nbJoursCalcules = DateCtrl.nbJoursEntre(dDebContratAv(), dateCalculee, false); // on
																											// n'inclut
																											// pas
																											// les
																											// bornes
																											// car
																											// l'utilisateur
																											// indique
																											// un
																											// nombre
																											// de
																											// jours,
																											// mois,
																											// année
																											// et
																											// non
																											// pas
																											// une
																											// date
						if (nbJoursCalcules > nbJours) {
							LogManager.logDetail("date avec ajout duree validee " + dateCalculee);
							LogManager.logDetail("Nb jours avenant " + nbJours);
							LogManager.logDetail("nbJours valides " + nbJoursCalcules);
							messageLog = "DUREE_SERVICE_VALIDE";
						}
					}
				}
			}
		}
		if (messageLog != null) {
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);

			log.setIdSource(contrat().idSource());
			log.addObjectToBothSidesOfRelationshipWithKey(contrat(), "contrat");
			log.addObjectToBothSidesOfRelationshipWithKey(contrat().individu(), "individu");
			log.addObjectToBothSidesOfRelationshipWithKey(this, nomRelationPourLog());
			setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une
										// erreur
			return log;
		}
		return null;
	}

	/**
	 * Retourne le detail de contrat du SI destinataire equivalent si on en
	 * trouve un unique qui pourrait correspondre a ce detail de contrat sinon
	 * null (absence ou homonymie)
	 */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (contrat() != null) {
			if (contrat().estNouveauContrat()) {
				return null;
			} else {
				return EOMangueContratAvenant.avenantDestinationPourAvenant(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}

	public String nomRelationPourLog() {
		return EOLogImport.CONTRAT_AVENANT_KEY;
	}

	public void preparerRelations() {
		super.preparerRelations();

		EOContrat contrat = EOContrat.contratPourSourceId(editingContext(), idSource(), ctrSource());
		setContratRelationship(contrat);

		if (contrat != null && cCondRecrut() != null) {
			EOConditionRecrutement conditionRecrutement = EOConditionRecrutement.rechercherAvecTypeContratEtCodeCondition(editingContext(), contrat()
					.cTypeContratTrav(), cCondRecrut());
			if (conditionRecrutement != null) {
				setToConditionRecrutementRelationship(conditionRecrutement);
			}
		}
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setContratRelationship(null);
	}

	/**
	 * Verifie que les relations du record sont bien valides : il se peut que le
	 * contrat ait ete cree juste auparavant. Retourne un message de log ou null
	 * si pas d'erreur
	 */
	public EOLogImport verifierRecord() {
		LogManager.logDetail("Verification du record");
		EOLogImport log = super.verifierRecord();
		if (log != null) {
			return log;
		}

		String messageLog = null;
		if (contrat() == null) {
			messageLog = "CONTRAT_NON_IMPORTE";
		} else if (contrat().temImport().equals(ObjetImport.A_TRANSFERER)) {
			if (contrat().statut().equals(EOIndividu.STATUT_ERREUR)) {
				messageLog = "CONTRAT_IMPORT_INVALIDE";
			}
		} else {
			// Contrat déjà importé, vérifier si le contrat du SI Destinataire
			// est valide
			EOContratCorresp correspondance = (EOContratCorresp) contrat().correspondance();
			if (correspondance != null && correspondance.contratMangue().temAnnulation().equals(CocktailConstantes.VRAI)) {
				messageLog = "CONTRAT_DESTINATAIRE_INVALIDE";
			}
		}
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			if (contrat() != null) {
				log.setContratRelationship(contrat());
			}
			if (idSource() != null) {
				log.setIdSource(idSource());
			}
			setStatut(STATUT_ERREUR); // Indiquer dans l'objet qu'il y a une
										// erreur
			return log;
		} else {
			return null;
		}
	}

	/** Retourne la relation sur le contrat ou l'individu */
	public String nomRelationPourAttributComparaison(String nomAttribut) {
		if (nomAttribut.equals("ctrSource")) {
			return CONTRAT_KEY;
		} else if (nomAttribut.equals("idSource")) {
			return "contrat.individu";
		} else {
			return null;
		}
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		Number ctrSource = (Number) valeursAttributs.objectForKey("ctrSource");
		if (ctrSource == null) {
			// On fait ici la vérification car il n'est pas marqué comme
			// obligatoire dans DescriptionImport.XML
			// afin de pouvoir générer des détails de contrat pour les logs
			// quand le contrat est inconnu
			resultat = resultat + "Ctr_Source >>" + System.getProperty("line.separator");
		}
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp) valeursAttributs.objectForKey(D_FIN_CONTRAT_AV_KEY);
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp) valeursAttributs.objectForKey(D_DEB_CONTRAT_AV_KEY);
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		// Vérification des nomenclatures
		String valeur = (String) valeursAttributs.objectForKey(CODEEMPLOI_KEY);
		if (valeur != null) {
			// Vérifier dans la nomenclature si ce bap corps existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOReferensEmplois.ENTITY_NAME,
					EOReferensEmplois.CODEEMPLOI_KEY, valeur);
			if (record == null) {
				// record =
				// Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext,
				// EOReferensEmplois.ENTITY_NAME, EOReferensEmplois.CODEMEN_KEY,
				// valeur);
				// if (record == null) {
				resultat = resultat + "Referens_Emploi_Inconnu >>" + valeur + System.getProperty("line.separator");
				// }
			}
		}
		valeur = (String) valeursAttributs.objectForKey(C_BAP_KEY);
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette bap existe
			EOBap bap = EOBap.getFromCode(editingContext, valeur);
			if (bap == null) {
				resultat = resultat + "Bap_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		valeur = (String) valeursAttributs.objectForKey(C_DISC_SECOND_DEGRE_KEY);
		if (valeur != null) {
			// Vérifier dans la nomenclature si ce disc existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EODiscSecondDegre.ENTITY_NAME,
					EODiscSecondDegre.C_DISC_SECOND_DEGRE_KEY, valeur);
			if (record == null) {
				resultat = resultat + "Disc2ndDegre_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		valeur = (String) valeursAttributs.objectForKey(C_SECTION_CNU_KEY);
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette section cnu existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOCnu.ENTITY_NAME, EOCnu.C_SECTION_CNU_KEY, valeur);
			if (record == null) {
				resultat = resultat + "SectionCnu_Inconnue >>" + valeur + System.getProperty("line.separator");
			}
			String sousSection = (String) valeursAttributs.objectForKey(C_SOUS_SECTION_CNU_KEY);
			if (sousSection != null) {
				// Vérifier si la sous-section existe
				boolean found = false;
				java.util.Enumeration e = Finder.rechercherAvecAttributEtValeurEgale(editingContext, EOCnu.ENTITY_NAME, C_SECTION_CNU_KEY, valeur)
						.objectEnumerator();
				while (e.hasMoreElements()) {
					record = (EOGenericRecord) e.nextElement();
					if (((String) record.valueForKey(C_SOUS_SECTION_CNU_KEY)).equals(sousSection)) {
						found = true;
						break;
					}
				}
				if (!found) {
					resultat = resultat + "SousSectionCnu_Inconnue >>" + sousSection + System.getProperty("line.separator");
				}
			}
		}
		valeur = (String) valeursAttributs.objectForKey(C_SPECIALITE_ITARF_KEY);
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette spécialité Itarf existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOSpecialiteItarf.ENTITY_NAME, C_SPECIALITE_ITARF_KEY,
					valeur);
			if (record == null) {
				resultat = resultat + "Specitalite_Itarf_Inconnue >>" + valeur + System.getProperty("line.separator");
			}
		}
		valeur = (String) valeursAttributs.objectForKey(C_SPECIALITE_ATOS_KEY);
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette spécialité Atos existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOSpecialiteAtos.ENTITY_NAME, C_SPECIALITE_ATOS_KEY,
					valeur);
			if (record == null) {
				resultat = resultat + "Specitalite_Atos_Inconnue >>" + valeur + System.getProperty("line.separator");
			}
		}
		valeur = (String) valeursAttributs.objectForKey(C_CATEGORIE_KEY);
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette catégorie existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "Categorie", "cCategorie", valeur);
			if (record == null) {
				resultat = resultat + "Categorie_Inconnue >>" + valeur + System.getProperty("line.separator");
			}
		}
		valeur = (String) valeursAttributs.objectForKey("cCondRecrut");
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette condition de recrutement
			// existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "ConditionRecrutement", "cCondRecrut", valeur);
			if (record == null) {
				resultat = resultat + "Condition_Recrutement_Inconnue >>" + valeur + System.getProperty("line.separator");
			}
		}

		String grade = (String) valeursAttributs.objectForKey("cGrade");
		String echelon = (String) valeursAttributs.objectForKey("cEchelon");
		if (grade != null) {
			// Vérifier dans la nomenclature si ce grade existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "Grade", "cGrade", grade);
			if (record == null) {
				resultat = resultat + "Grade_Inconnu >>" + grade + System.getProperty("line.separator");
			} else if (echelon != null && grade.equals(SANS_GRADE) == false) {
				// Vérifier dans la nomenclature si cet échelon existe pour ce
				// grade
				record = rechercherPassageEchelonPourGradeEtEchelon(editingContext, grade, echelon);
				if (record == null) {
					resultat = resultat + "Echelon_Inconnu (" + grade + "," + echelon + ") >>" + echelon + System.getProperty("line.separator");
				}
			}
		} else if (echelon != null) {
			resultat = resultat + "Grade_Obligatoire >>" + System.getProperty("line.separator");
		}
		Double quotite = (Double) valeursAttributs.objectForKey("numQuotRecrutement");
		if (quotite != null) {
			int quot = quotite.intValue();
			if (quot == 0) {
				resultat = resultat + "Quotite_Nulle >>" + System.getProperty("line.separator");
			} else if (quot > 100) {
				resultat = resultat + "Quotite_Invalide >>" + System.getProperty("line.separator");
			}
		}
		// Vérifier le type de montant
		String typeMontant = (String) valeursAttributs.objectForKey("typeMontant");
		if (typeMontant != null) {
			boolean found = false;
			for (int i = 0; i < TYPES_MONTANT.length; i++) {
				// DT 5570 avec ce contrôle il ne devrait pas y avoir eu le
				// problème !!!
				if (typeMontant.equals(TYPES_MONTANT[i])) {
					found = true;
					break;
				}
			}
			if (!found) {
				String montantsPossibles = TYPES_MONTANT[0];
				for (int i = 1; i < TYPES_MONTANT.length; i++) {
					montantsPossibles = montantsPossibles + "," + TYPES_MONTANT[i];
				}
				resultat = resultat + "Type_Montant_Invalide >>" + typeMontant + ". Valeurs possibles : " + montantsPossibles
						+ System.getProperty("line.separator");
			}
		}
		resultat = resultat + verifierTemoin(valeursAttributs, C_DEA_KEY);
		resultat = resultat + verifierTemoin(valeursAttributs, TEM_GEST_ETAB_KEY);
		// resultat = resultat + verifierTemoin(valeursAttributs,
		// "temAnnulation");
		resultat = resultat + verifierTemoin(valeursAttributs, "temArreteSigne");
		resultat = resultat + verifierTemoin(valeursAttributs, "temPaiementPonctuel");

		return resultat;
	}

	/**
	 * retourne tous les details de contrat de remuneration principale d'un
	 * individu dont les dates de debut et fin sont a cheval sur deux dates
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 *            (peut &ecirc;tre nulle lorsqu'on recherche tous les avenants)
	 * @param finPeriode
	 *            (peut &ecirc;tre nulle)
	 * @return details de contrat trouv&eacutes;
	 */
	public static NSArray rechercherAvenantsRemunerationPrincipaleImportCourantPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu,
			NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		String stringQualifier = "statut <> 'A' AND statut <> 'E' AND contrat.individu = %@";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, new NSArray(individu));
		qualifiers.addObject(qualifier);
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_IMPORT_KEY + " = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		if (debutPeriode != null) {
			qualifier = Finder.qualifierPourPeriode(D_DEB_CONTRAT_AV_KEY, debutPeriode, D_FIN_CONTRAT_AV_KEY, finPeriode);
		}
		qualifiers.addObject(qualifier);
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("ContratAvenant", qualifier, null);
		// Rechercher les contrats de rémunération principale
		NSMutableArray avenantsValides = new NSMutableArray();
		java.util.Enumeration e = editingContext.objectsWithFetchSpecification(myFetch).objectEnumerator();
		while (e.hasMoreElements()) {
			EOContratAvenant avenant = (EOContratAvenant) e.nextElement();
			EOTypeContratTravail typeContrat = EOTypeContratTravail.getFromCode(editingContext, avenant.contrat().cTypeContratTrav());
			if (typeContrat != null) {
				EOParamTypeContrat param = EOParamTypeContrat.rechercherParametrePourTypeContrat(editingContext, typeContrat.cTypeContratTrav());
				// rajouter un test de contrôle de nullité de
				// param.fournirQuotie en fonction du cTypeContratTrav
				// ou
				// seulement rajouter un test de contrôle de nullité de
				// param.fournirQuotie ?
				if (typeContrat.estRemunerationPrincipale() && (param == null || param.fournirQuotite())) {
					avenantsValides.addObject(avenant);
				}
			}
		}
		return avenantsValides;
	}

	/**
	 * retourne tous les details de contrat d'un contrat non annules, d'un
	 * individu dont les dates de debut et fin sont a cheval sur deux dates
	 * 
	 * @param editingContext
	 * @param contrat
	 * @param debutPeriode
	 * @param finPeriode
	 *            (peut &ecirc;tre nulle)
	 * @return details de contrat trouv&eacutes;
	 */
	public static NSArray rechercherAvenantsNonAnnulesImportCourantPourContratEtPeriode(EOEditingContext editingContext, EOContrat contrat,
			NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		String stringQualifier = "statut <> 'A' AND statut <> 'E' AND contrat = %@";
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(stringQualifier, new NSArray(contrat)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(Finder.qualifierPourPeriode(D_DEB_CONTRAT_AV_KEY, debutPeriode, D_FIN_CONTRAT_AV_KEY, finPeriode));
		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/** Retourne les details de contrat lies a un contrat */
	public static NSArray rechercherAvenantsImportCourantPourContrat(EOContrat contrat) {
		NSMutableArray args = new NSMutableArray(contrat);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("contrat = %@ AND temImport = %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		return contrat.editingContext().objectsWithFetchSpecification(myFetch);
	}

	/**
	 * Calcule la quotite de tous les details de contrat de remuneration
	 * principale d'un individu pendant une periode
	 */
	public static double calculerQuotiteTotalePourPeriode(EOEditingContext editingContext, EOIndividu individu, EOContratAvenant avenantCourant,
			NSTimestamp dateDebut, NSTimestamp dateFin) {
		// vérifier la quotité des autres contrats de l'import pour voir si on
		// autorise un contrat
		// entre ces dates dans le cas où le contrat n'est pas annulé
		// détails de contrat valides non encore importés et de rémunération
		// principale :
		// on recherche tous les détails de contrat mais on prendra en compte
		// dans le calcul de la quotité
		// uniquement ceux non annulés et dont le contrat est non annulé
		LogManager.logDetail("Calcul quotite pour " + individu.nomUsuel());
		NSMutableArray periodesAvecQuotite = new NSMutableArray();
		NSArray avenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipaleImportCourantPourIndividuEtPeriode(editingContext, individu, dateDebut,
				dateFin);
		NSMutableArray avenantsDestinVus = new NSMutableArray();
		java.util.Enumeration e = avenants.objectEnumerator();
		LogManager.logDetail("nbAvenants imports trouves " + avenants.count());
		while (e.hasMoreElements()) {
			EOContratAvenant avenant = (EOContratAvenant) e.nextElement();
			// Rechercher le paramType selon le type de contrat pour vérifier si
			// la quotité doit être contrôlée
			EOParamTypeContrat paramType = EOParamTypeContrat.rechercherParametrePourTypeContrat(editingContext, avenant.contrat().cTypeContratTrav());
			if (paramType != null && paramType.fournirQuotite()) {
				periodesAvecQuotite.addObject(new PeriodeAvecQuotite(avenant.dDebContratAv(), avenant.dFinContratAv(), avenant.numQuotRecrutement()));
			}
			ajouterAvenantDestinPourAvenant(editingContext, avenantsDestinVus, avenant);
		}
		// Ajouter l'avenant courant si celui-ci n'a pas encore été pris en
		// compte
		// On ne vérifie pas paramType car la méthode n'aurait pas été appelée
		// si il ne fallait pas vérifier la quotité
		if (avenantCourant != null && avenants.containsObject(avenantCourant) == false) {
			periodesAvecQuotite.addObject(new PeriodeAvecQuotite(avenantCourant.dDebContratAv(), avenantCourant.dFinContratAv(), avenantCourant
					.numQuotRecrutement()));
			ajouterAvenantDestinPourAvenant(editingContext, avenantsDestinVus, avenantCourant);
		}
		LogManager.logDetail("nbDetails imports a prendre en compte " + periodesAvecQuotite.count());
		LogManager.logDetail("nb details a ignorer " + avenantsDestinVus.count());
		// Rechercher parmi les détails de contrat du SI Destinataire, ne pas
		// prendre en compte les détails de contrat dans l'import courant
		// qui sont dans le SI Destinataire car ils ont déjà été pris en
		// considération
		NSMutableArray avenantsMangue = new NSMutableArray(EOMangueContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtPeriode(editingContext,
				individu, dateDebut, dateFin));
		LogManager.logDetail("nb details Mangue " + avenantsMangue.count());
		// Supprimer les détails de contrat à ne pas prendre en compte
		avenantsMangue.removeObjectsInArray(avenantsDestinVus);
		// Il doit rester dans les détails de contrat de Mangue, les détails de
		// contrat qui n'ont pas été (ou pas encore) importés dans l'import
		// courant
		LogManager.logDetail("nb avenants Mangue restant a prendre en compte " + avenantsMangue.count());
		// 08/02/2011
		if (avenantsMangue.count() > 0) {
			// Rechercher tous les avenants valides dans l'import courant
			NSArray avenantsImportValides = rechercherAvenantsRemunerationPrincipaleImportCourantPourIndividuEtPeriode(editingContext, individu, null, null);
			// On se base sur les avSource pour comparer les avenants
			avenantsImportValides = (NSArray) avenantsImportValides.valueForKey(AV_SOURCE_KEY);
			e = avenantsMangue.objectEnumerator();
			while (e.hasMoreElements()) {
				EOMangueContratAvenant avenantMangue = (EOMangueContratAvenant) e.nextElement();
				// 08/02/2011 - Rechercher l'avenant d'import associé à cet
				// avenant
				EOContratAvenant avenantImport = (EOContratAvenant) ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext, avenantMangue,
						"ContratAvenant");
				// Vérifier si pour ce contrat il faut prendre en compte la
				// quotité
				EOParamTypeContrat paramType = EOParamTypeContrat
						.rechercherParametrePourTypeContrat(editingContext, avenantMangue.contrat().cTypeContratTrav());
				if (paramType != null && paramType.fournirQuotite()) {
					// vérifier si il faut prendre en compte la quotité de
					// l'avenant d'import
					boolean prendreEnCompteQuotiteImport = false;
					if (avenantImport != null) {
						paramType = EOParamTypeContrat.rechercherParametrePourTypeContrat(editingContext, avenantImport.contrat().cTypeContratTrav());
						if (paramType != null && paramType.fournirQuotite()) {
							prendreEnCompteQuotiteImport = true;
						}
					}
					if (!prendreEnCompteQuotiteImport || avenantImport != null && (avenantsImportValides.containsObject(avenantImport.avSource()) == false)) {
						periodesAvecQuotite.addObject(new PeriodeAvecQuotite(avenantMangue.dDebContratAv(), avenantMangue.dFinContratAv(), avenantMangue
								.numQuotRecrutement()));
					}
				}
			}
		}
		LogManager.logDetail("nbAvenants a considerer  " + periodesAvecQuotite.count());
		Number quotiteTotale = PeriodeAvecQuotite.calculerQuotiteTotale(periodesAvecQuotite);
		LogManager.logDetail("quotite finale " + quotiteTotale);
		if (quotiteTotale != null) {
			return quotiteTotale.doubleValue();
		} else {
			return 0.00;
		}
	}

	public static void ajouterAvenantDestinPourAvenant(EOEditingContext editingContext, NSMutableArray avenantsDestin, EOContratAvenant avenant) {
		if (avenant.operation().equals(OPERATION_INSERTION) == false) {
			EOMangueContratAvenant avenantDestin = (EOMangueContratAvenant) avenant.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (avenantsDestin != null) {
				avenantsDestin.addObject(avenantDestin);
			}
		}
	}
}
