package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.jefy.EOKxElement;
import org.cocktail.connecteur.common.modele.jefy.EOPafMois;
import org.cocktail.connecteur.common.modele.jefy.EOTypeCredit;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOPersBudget extends _EOPersBudget {
	@Override
	public void preparerRelations() {
		super.preparerRelations();

		EOEmploi emploi = EOEmploi.emploiPourSourceId(editingContext(), empSource());
		setEmploiRelationship(emploi);

		EOTypeCredit typeCredit = EOTypeCredit.typePersonnel(editingContext());
		setToTypeCreditRelationship(typeCredit);

		EOKxElement kxElement = EOKxElement.getFromIdeltEtMois(editingContext(), idelt(), moisCodeDebut());
		setToKxElementRelationship(kxElement);

		EOOrgaBudget orgaBudget = EOOrgaBudget.orgaBudgetPourSourceId(editingContext(), obSource());
		setToOrgaBudgetRelationship(orgaBudget);
	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setEmploiRelationship(null);
		setToTypeCreditRelationship(null);
		setToKxElementRelationship(null);
		setToOrgaBudgetRelationship(null);
	}

	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;
		if ((idSource() == null && empSource() == null) || (idSource() != null && empSource() != null)) {
			messageLog = "INDIVIDU_OU_EMPLOI";
		} else if (emploi() == null && empSource() != null) {
			messageLog = "EMPLOI_NON_IMPORTE";
		} else if (emploi() != null && emploi().statut().equals(EOEmploi.STATUT_ERREUR)) {
			messageLog = "EMPLOI_INVALIDE";
		} else if (toKxElement() == null && idelt() != null)
			messageLog = "IDELT_INCONNU";
		else if (toOrgaBudget() == null && obSource() != null) {
			messageLog = "ORGAN_NON_IMPORTE";
		} else if (toOrgaBudget() != null && toOrgaBudget().statut().equals(EOOrgaBudget.STATUT_ERREUR)) {
			messageLog = "ORGAN_INVALIDE";
		}
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);
			return log;
		} else {
			return null;
		}
	}

	@Override
	public EOLogImport verifierRecordApresOperation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.PERS_BUDGET_KEY;
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = "";
		if (valeursAttributs.objectForKey(TEM_VALIDE_KEY) == null)
			valeursAttributs.setObjectForKey(CocktailConstantes.VRAI, TEM_VALIDE_KEY);

		Integer orgId = (Integer) valeursAttributs.objectForKey(ORG_ID_KEY);
		Integer obSource = (Integer) valeursAttributs.objectForKey(OB_SOURCE_KEY);
		if ( orgId==null && obSource==null)
			resultat += "OrgId_et_ObSource_nuls >>" + System.getProperty("line.separator");
		if (orgId!=null && obSource!=null)
			resultat += "OrgId_et_ObSource_non_nuls >>" + System.getProperty("line.separator");
		
		if (orgId != null) {
			EOJefyOrgan organ = EOJefyOrgan.getFromId(editingContext, orgId);
			if (organ == null)
				resultat += "Organ_Inconnu (" + orgId + ") >>" + System.getProperty("line.separator");
		}

		EOTypeCredit typeCreditPersonnel = EOTypeCredit.typePersonnel(editingContext);
		if (typeCreditPersonnel == null)
			resultat += "TypeCreditPersonnel_non_trouve_dans_base >>" + System.getProperty("line.separator");

		String idelt = (String) valeursAttributs.objectForKey(IDELT_KEY);
		Integer idSource = (Integer) valeursAttributs.objectForKey(ID_SOURCE_KEY);
		if (idelt != null && idSource == null)
			resultat += "Idelt_non_nul_IdSource_nul >>" + System.getProperty("line.separator");

		Integer moisDebut = (Integer) valeursAttributs.objectForKey(MOIS_CODE_DEBUT_KEY);
		if (moisDebut != null) {
			EOPafMois mois = EOPafMois.fetchPafMois(editingContext, EOPafMois.MOIS_CODE_KEY, moisDebut);
			if (mois == null)
				resultat += "MoisCodeDebut_Inconnu (" + moisDebut + ") >>" + System.getProperty("line.separator");
			else
				valeursAttributs.setObjectForKey(mois.moisDebut(), DATE_DEBUT_KEY);
		}

		Integer moisFin = (Integer) valeursAttributs.objectForKey(MOIS_CODE_FIN_KEY);
		if (moisFin != null) {
			EOPafMois mois = EOPafMois.fetchPafMois(editingContext, EOPafMois.MOIS_CODE_KEY, moisFin);
			if (mois == null)
				resultat += "MoisCodeFin_Inconnu (" + moisFin + ") >>" + System.getProperty("line.separator");
			else
				valeursAttributs.setObjectForKey(mois.moisFin(), DATE_FIN_KEY);
		}
		return resultat;
	}

	public static EOPersBudget persBudgetPourSourceId(EOEditingContext editingContext, Integer id) {
		if (id == null)
			return null;

		EOPersBudget resultat=null;
		NSArray resultats=Finder.rechercherAvecAttribut(editingContext, ENTITY_NAME, PBUD_SOURCE_KEY, id);
		// Il est possible d'avoir plusieurs éléments dans la table avec le même sourceId dans le cas où la donnée a été mise à jour par exemple 
		resultat = (EOPersBudget)premierObjetValide(resultats);

		return resultat;
	}
}
