package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotProlongation;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEnfantCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReculAge;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOReculAge extends _EOReculAge {

	@Override
	public void preparerRelations() {
		super.preparerRelations();
		EOEnfant enfant1 = EOEnfantCorresp.enfantPourIndividuEtId(editingContextTravail(), idSource(), enfSource1());
		setToEnfant1Relationship(enfant1);
		EOEnfant enfant2 = EOEnfantCorresp.enfantPourIndividuEtId(editingContextTravail(), idSource(), enfSource2());
		setToEnfant2Relationship(enfant2);
		EOEnfant enfant3 = EOEnfantCorresp.enfantPourIndividuEtId(editingContextTravail(), idSource(), enfSource3());
		setToEnfant3Relationship(enfant3);
	}

	@Override
	public void supprimerRelations() {
		super.supprimerRelations();
		setToEnfant1Relationship(null);
		setToEnfant2Relationship(null);
		setToEnfant3Relationship(null);
	}

	/**
	 * Verifie que les relations du record sont bien valides : i.e que l'enfant
	 * existe bien cree juste auparavant. Retourne un message de log ou null si
	 * pas d'erreur
	 */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log != null)
			return log;

		String messageLog = null;
		if (toEnfant1() == null && enfSource1() != null) {
			messageLog = "ENFANT_NON_IMPORTE";
		} else if (toEnfant1() != null && toEnfant1().temImport().equals(ObjetImport.A_TRANSFERER) && toEnfant1().statut().equals(ObjetImport.STATUT_ERREUR)) {
			messageLog = "ENFANT_INVALIDE";
		}
		if (toEnfant2() == null && enfSource2() != null) {
			messageLog = "ENFANT_NON_IMPORTE";
		} else if (toEnfant2() != null && toEnfant2().temImport().equals(ObjetImport.A_TRANSFERER) && toEnfant2().statut().equals(ObjetImport.STATUT_ERREUR)) {
			messageLog = "ENFANT_INVALIDE";
		}
		if (toEnfant3() == null && enfSource3() != null) {
			messageLog = "ENFANT_NON_IMPORTE";
		} else if (toEnfant3() != null && toEnfant3().temImport().equals(ObjetImport.A_TRANSFERER) && toEnfant3().statut().equals(ObjetImport.STATUT_ERREUR)) {
			messageLog = "ENFANT_INVALIDE";
		}
		if (messageLog != null) {
			log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			log.ajouterRelation(this);
			log.setEimpSource(eimpSource());
			setStatut(STATUT_ERREUR);
		}
		return log;
	}

	// Méthodes protégées
	/** La verification des chevauchements est faite par un autre biais */
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueReculAge.findForIndividuEtPeriode(editingContext, individuGrhum, dateDebut(), dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.RECUL_AGE_KEY;
	}

	@Override
	public Integer eimpSource() {
		return recSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setRecSource(value);
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		String valeur = (String) valeursAttributs.objectForKey(C_MOTIF_RECUL_KEY);
		if (valeur != null) {
			EOTypeMotProlongation motifProlongation = EOTypeMotProlongation.fetchTypeMotProlongation(editingContext,
					EOTypeMotProlongation.C_TYPE_MOT_PROLONGATION_KEY, valeur);
			if (motifProlongation == null) {
				resultat = resultat + "Motif_Recul_Age_Inconnu >>" + valeur + System.getProperty("line.separator");
			} else if (!motifProlongation.typeMotif().equals(EOTypeMotProlongation.MOTIF_RECUL_AGE)) {
				resultat = resultat + "Motif_Recul_Age_Non_Valide >>" + valeur + System.getProperty("line.separator");
			}
		}
		return resultat;
	}
}
