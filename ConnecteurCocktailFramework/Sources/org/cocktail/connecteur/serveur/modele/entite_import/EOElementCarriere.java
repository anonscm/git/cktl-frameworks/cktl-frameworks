//EOElementCarriere.java
//Created on Mon Jan 07 16:28:30 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.PeriodeAvecQuotite;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypePopulation;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 16/09/2010 - correction du problème des anciennes bap qui ont des spécialités Itarf
// 08/02/2011 - correction d'un bug dans l'évaluation de la quotité, il ne faut pas prendre en compte dans le
// calcul de la quotité des éléments de carrière en update suite à un changement de dates
public class EOElementCarriere extends _EOElementCarriere  {
	private final static int ELEMENTS_VALIDES = 1;
	private final static int ELEMENTS_ANNULES = 2;
	private static final Object TEM_PROVISOIRE_VALEUR_PAR_DEFAUT = CocktailConstantes.FAUX;
	public EOElementCarriere() {
		super();
	}

	public boolean estProvisoire() {
		return temProvisoire().equals("O");
	}

	public EOLogImport verifierRecord() {
		EOLogImport logImport = super.verifierRecord();

		if (logImport != null) {
			return logImport;
		}
		
		String messageLog=null;
		
		if (individu()==null)
			messageLog="INDIVIDU_NON_IMPORTE";

		if (toCarriere()==null)
			messageLog="CARRIERE_NON_IMPORTEE";
		
		if (messageLog != null) {
			logImport = new EOLogImport();
			logImport.initAvecEditingContext(editingContextTravail(),this.entityName(),messageLog);
			logImport.setIdSource(new Integer(idSource().intValue()));
			setIndividuRelationship(individu());
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
		}
		return logImport;
	}

	/** Verifie que le corps est compatible avec la population du segment de carriere<BR>
	 *  Verifie que les dates correspondent a celles du segment de carriere<BR>
	 *  qu'il n'y a pas de chevauchement d'elements de carriere lorsqu'ils ne sont pas provisoires
	 * et qu'ils n'ont pas d'arrete d'annulation, */
	public EOLogImport verifierRecordApresOperation() {
		// Si carrière annulée, on ne fait pas de vérification
		if (dAnnulation()!=null)
			return null;

		// Vérifier la compatibilité du corps avec le type de population
		EOGenericRecord corps = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContextTravail(), EOCorps.ENTITY_NAME, C_CORPS_KEY, cCorps());
		if (corps != null) {
			EOGenericRecord typePopulationCorps = (EOGenericRecord)corps.valueForKey("toTypePopulation");
			EOTypePopulation typePopulationCarriere = toCarriere().toTypePopulation();
			
			if (typePopulationCorps!=null && typePopulationCarriere!=null) {
				if (typePopulationCarriere.dFermeture()==null && !typePopulationCorps.valueForKey("cTypePopulation").equals(typePopulationCarriere.cTypePopulation())) {
					LogManager.logDetail("Insertion d'un log pour l'incompatibilite du corps avec le type de population");
					return preparerLog("CORPS_INCOMPATIBLE");
				}
			}
		}	

		if (!this.estProvisoire()) {
			EOLogImport logImport = verifierDatesParRapportCarriere(this.dEffetElement(),this.dFinElement());
			if (logImport != null) {
				return logImport;
			}
		}

		// Pour les éléments de carrière non annulés et non provisoires
		if (temProvisoire() != null && temProvisoire().equals(CocktailConstantes.FAUX) && dAnnulation() == null && noArreteAnnulation() == null) {
			// Vérifier qu'il n'existe pas de chevauchement d'éléments de carrière dans l'import courant, en ne prenant en compte
			// que les éléments de carrière sans arrêté d'annulation et non provisoires
			NSMutableArray periodesAvecQuotite = new NSMutableArray();
			NSMutableArray elementsDestinVus = new NSMutableArray();

			// l'élément de carrière n'a pas encore été inséré dans la base, il faut donc la prendre en compte
			periodesAvecQuotite.addObject(new PeriodeAvecQuotite(this.dEffetElement(),this.dFinElement(),this.quotiteElement()));
			ajouterElementDestinPourElement(elementsDestinVus,this);

			NSArray elementsImport = rechercherElementsCarriereValidesImportCourantPourIndividuEtPeriode();
			LogManager.logDetail("Nb elements dans import : " + elementsImport.count());
			java.util.Enumeration e = elementsImport.objectEnumerator();
			while (e.hasMoreElements()) {
				EOElementCarriere element = (EOElementCarriere)e.nextElement();
				if (element != this) {
					LogManager.logDetail("element courant - individu : " + element.individu().idSource() + ", carriere : " + element.carSource() + ", element : " + element.elSource());
					periodesAvecQuotite.addObject(new PeriodeAvecQuotite(element.dEffetElement(),element.dFinElement(),element.quotiteElement()));
					ajouterElementDestinPourElement(elementsDestinVus,this);
				}
			}

			LogManager.logDetail("elements destins vus : " + elementsDestinVus.count());
			EOMangueCarriere carriereMangue = (EOMangueCarriere)toCarriere().objetDestinataireAvecOuSansCorrespondance(editingContextTravail());
			if (carriereMangue != null) {	// Null si pas encore créée dans le SI Destinataire
				// Vérifier si il y a des chevauchements de période avec le SI Destinataire
				// En recherchant tous les elements du SI destinataire qui ne sont pas annulés
				NSMutableArray elementsDestin = new NSMutableArray(EOMangueElementCarriere.rechercherElementsDestinValidesSurPeriode(editingContextTravail(),carriereMangue,dEffetElement(),dFinElement()));
				// 08/02/2011
				NSArray elementsImportValides = rechercherElementsCarriereValidesImportCourantPourIndividu();
				elementsImportValides = (NSArray)elementsImportValides.valueForKey("elSource");
				e = elementsDestin.objectEnumerator();
				while (e.hasMoreElements()) {
					EOMangueElementCarriere elementMangue = (EOMangueElementCarriere)e.nextElement();
					LogManager.logDetail("element courant destin : " + DateCtrl.dateToString(elementMangue.dEffetElement()));
					if (elementsDestinVus.containsObject(elementMangue) == false) {
						// 08/02/2011 - Rechercher l'élément d'import associée à cet élément
						EOElementCarriere elementImport = (EOElementCarriere)ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext(),elementMangue,"ElementCarriere");
						if (elementImport != null && elementsImportValides.containsObject(elementImport.elSource()) == false) {
							periodesAvecQuotite.addObject(new PeriodeAvecQuotite(elementMangue.dEffetElement(),elementMangue.dFinElement(),elementMangue.quotiteElement()));
						}
					}
				}
			}
			Number quotiteTotale = PeriodeAvecQuotite.calculerQuotiteTotale(periodesAvecQuotite);
			LogManager.logDetail("quotite finale " + quotiteTotale);
			if (quotiteTotale != null && quotiteTotale.doubleValue() > 100.00) {
				LogManager.logDetail("Insertion d'un log pour le chevauchement des elements de carriere importes");
				return preparerLog("CHEVAUCHEMENT_ELEMENT_CARRIERE");
			}
		}
		return null;
	}
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (toCarriere() != null) {
			if (estNouvelleCarriere()) {
				return null;
			} else {
				return EOMangueElementCarriere.elementDestinationPourElement(editingContextTravail(), this);
			}
		} else {
			return null;
		}
	}
	public String nomRelationPourLog() {
		return "elementCarriere";
	}
	// Méthodes privées
	/* retourne les éléments de carrière sans arrêté d'annulation, non provisoires et valides qui sont importés dans l'import courant 
	 * pour l'individu et la même période.	
	 */
	private NSArray rechercherElementsCarriereImportCourantPourIndividuEtPeriode(int type, boolean prendreEnComptePeriode) {
		NSMutableArray args = new NSMutableArray(individu());
		args.addObject(toCarriere());
		args.addObject(ObjetImport.A_TRANSFERER);
		String strQualifier = "individu = %@ AND toCarriere = %@ AND temImport = %@ AND statut <> 'A' AND statut <> 'E' AND temProvisoire = 'N'";
		if (type == ELEMENTS_VALIDES) {
			strQualifier = strQualifier + " AND noArreteAnnulation = nil AND dAnnulation = nil";
		} else if (type == ELEMENTS_ANNULES) {
			strQualifier = strQualifier + " AND (noArreteAnnulation <> nil OR dAnnulation <> nil)";
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(strQualifier,args);
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		if (prendreEnComptePeriode) {
			qualifiers.addObject(Finder.qualifierPourPeriode(D_EFFET_ELEMENT_KEY, dEffetElement(), D_FIN_ELEMENT_KEY, dFinElement()));
		}
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, qualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContextTravail().objectsWithFetchSpecification(myFetch);

	}	
	private NSArray rechercherElementsCarriereValidesImportCourantPourIndividuEtPeriode() {
		return rechercherElementsCarriereImportCourantPourIndividuEtPeriode(ELEMENTS_VALIDES,true);
	}
	private NSArray rechercherElementsCarriereValidesImportCourantPourIndividu() {
		return rechercherElementsCarriereImportCourantPourIndividuEtPeriode(ELEMENTS_VALIDES,false);
	}
	private void ajouterElementDestinPourElement(NSMutableArray elementsDestin,EOElementCarriere element) {
		if (element.operation().equals(OPERATION_INSERTION) == false) {
			EOMangueElementCarriere elementDestin = null;
			if (element.operation().equals(OPERATION_UPDATE)) {
				elementDestin = (EOMangueElementCarriere)ObjetCorresp.rechercherDestinatairePrecedentPourRecordImport(editingContextTravail(),element);
			} else {	// Correspondance
				elementDestin = EOMangueElementCarriere.elementDestinationPourElement(editingContextTravail(), element);
			}
			if (elementDestin != null) {
				elementsDestin.addObject(elementDestin);
			}
		}
	}
	// Méthodes statiques
	/** Verifie les nomenclatures ainsi que les dates */
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = "";
		String valeur="";
		Number idSource = (Number)valeursAttributs.objectForKey("idSource");
		if (idSource == null) {
			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
		}
		Number carSource = (Number)valeursAttributs.objectForKey("carSource");
		if (carSource == null) {
			resultat = resultat + "Car_Source >>" + System.getProperty("line.separator");
		}
		
		String temProvisoire=(String)valeursAttributs.objectForKey(TEM_PROVISOIRE_KEY);
		if (temProvisoire==null)
			valeursAttributs.setObjectForKey(TEM_PROVISOIRE_VALEUR_PAR_DEFAUT, TEM_PROVISOIRE_KEY);
		
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		NSTimestamp dateFin = (NSTimestamp)valeursAttributs.objectForKey("dFinElement");
		if (dateFin != null) {
			NSTimestamp dateDebut = (NSTimestamp)valeursAttributs.objectForKey("dEffetElement");
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				resultat = resultat + "Date_Invalide >>" + System.getProperty("line.separator");
			}
		}
		String corps = (String)valeursAttributs.objectForKey(C_CORPS_KEY);
		// Vérifier dans la nomenclature si ce corps existe
		EOGenericRecord recordCorps = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOCorps.ENTITY_NAME, EOCorps.C_CORPS_KEY, corps);
		if (recordCorps == null) {
			resultat = resultat + "Corps_Inconnu >>" + corps + System.getProperty("line.separator");
		}
		String grade = (String)valeursAttributs.objectForKey("cGrade");
		// Vérifier dans la nomenclature si ce grade existe
		EOGenericRecord recordGrade = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "Grade", "cGrade", grade);
		if (recordGrade == null) {
			resultat = resultat + "Grade_Inconnu >>" + grade + System.getProperty("line.separator");
		}
		// Vérifier que le grade est compatible avec le corps
		if (recordCorps != null && recordGrade != null && recordGrade.valueForKey("toCorps") != recordCorps) {
			resultat = resultat + "Corps_Grade_Incompatible >>" + grade + " incompatible avec " + corps + System.getProperty("line.separator");
		}
		String echelon = (String)valeursAttributs.objectForKey("cEchelon");
		if (echelon != null) {
			// Vérifier dans la nomenclature si cet échelon existe pour ce grade
			EOGenericRecord record = rechercherPassageEchelonPourGradeEtEchelon(editingContext, grade, echelon);
			if (record == null) {
				resultat = resultat + "Echelon_Inconnu >>" + echelon  + System.getProperty("line.separator");
			}
			valeur = (String)valeursAttributs.objectForKey("cChevron");
			if (valeur != null) {
				// Vérifier dans la nomenclature si ce chevron existe pour ce grade et cet échelon
				record = rechercherPassageChevronPourGradeEchelonEtChevron(editingContext, grade, echelon,valeur);
				if (record == null) {
					resultat = resultat + "Chevron_Inconnu >>" + valeur + System.getProperty("line.separator");
				}
			}
		}
		valeur = (String)valeursAttributs.objectForKey("cTypeAcces");
		if (valeur != null) {
			// Vérifier dans la nomenclature si cette spécialité Atos existe
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeAcces", "cTypeAcces", valeur);
			if (record == null) {
				resultat = resultat + "Type_Acces_Inconnu >>" + valeur + System.getProperty("line.separator");
			}
		}
		Integer quotite = (Integer)valeursAttributs.objectForKey("quotiteElement");
		if (quotite != null) {
			if (quotite.intValue() == 0) {
				resultat = resultat + "Quotite_Nulle >>" + System.getProperty("line.separator");
			} else if (quotite.intValue() > 100) {
				resultat = resultat + "Quotite_Invalide >>" + System.getProperty("line.separator");
			}
		}

		resultat = resultat + verifierTemoin(valeursAttributs, "temGestEtab");
		resultat = resultat + verifierTemoin(valeursAttributs, "temSigne");
		resultat = resultat + verifierTemoin(valeursAttributs, "temProvisoire");

		return resultat;
	}
	/** Retourne les elements de carri&egrave;re de l'import courant valides pour la periode et n'ayant pas
	 * d'arr&circ;te d'annumation
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray rechercherElementsCarriereValidesImportCourantPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(ObjetImport.A_TRANSFERER);
		String strQualifier = "individu = %@ AND temImport = %@ AND statut <> 'A' AND statut <> 'E' AND temProvisoire = 'N' AND noArreteAnnulation = nil AND dAnnulation = nil";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(strQualifier,args);
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(Finder.qualifierPourPeriode(D_EFFET_ELEMENT_KEY, debutPeriode, D_FIN_ELEMENT_KEY, finPeriode));
		EOFetchSpecification myFetch = new EOFetchSpecification ("ElementCarriere", new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}	
	// Méthodes privées statiques
	private static EOGenericRecord rechercherPassageChevronPourGradeEchelonEtChevron(EOEditingContext editingContext, String grade,String echelon,String chevron) {
		EOGenericRecord resultat=null;
		NSMutableArray args = new NSMutableArray(grade);
		args.addObject(echelon);
		args.addObject(chevron);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cGrade = %@ AND cEchelon = %@ AND cChevron = %@" ,args);
		EOFetchSpecification myFetch = new EOFetchSpecification("PassageChevron",qualifier,null);
		try {
			NSArray resultats=editingContext.objectsWithFetchSpecification(myFetch);
			if (resultats.count()>0)
				resultat=(EOGenericRecord)resultats.objectAtIndex(0);
			else
				resultat=null;
		} catch (Exception e) {
			e.printStackTrace();
			resultat=null;
		}
		return resultat;
	}

}
