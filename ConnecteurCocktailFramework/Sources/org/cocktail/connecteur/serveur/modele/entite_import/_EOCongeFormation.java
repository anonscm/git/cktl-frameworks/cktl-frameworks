// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeFormation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeFormation extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "CongeFormation";

	// Attributes
	public static final String C_CHAPITRE_KEY = "cChapitre";
	public static final String CGF_SOURCE_KEY = "cgfSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_INDEMNITE_CGF_KEY = "dDebIndemniteCgf";
	public static final String D_DEMANDE_CGF_KEY = "dDemandeCgf";
	public static final String D_FIN_INDEMNITE_CGF_KEY = "dFinIndemniteCgf";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_JJ_CGF_KEY = "dureeJjCgf";
	public static final String DUREE_MM_CGF_KEY = "dureeMmCgf";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_FORM_AGREE_KEY = "temFormAgree";
	public static final String TEM_FRACTIONNE_KEY = "temFractionne";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CHAPITRE_KEY = "chapitre";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeFormation.class);

  public EOCongeFormation localInstanceIn(EOEditingContext editingContext) {
    EOCongeFormation localInstance = (EOCongeFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cChapitre() {
    return (Integer) storedValueForKey("cChapitre");
  }

  public void setCChapitre(Integer value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating cChapitre from " + cChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cChapitre");
  }

  public Integer cgfSource() {
    return (Integer) storedValueForKey("cgfSource");
  }

  public void setCgfSource(Integer value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating cgfSource from " + cgfSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cgfSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebIndemniteCgf() {
    return (NSTimestamp) storedValueForKey("dDebIndemniteCgf");
  }

  public void setDDebIndemniteCgf(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dDebIndemniteCgf from " + dDebIndemniteCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebIndemniteCgf");
  }

  public NSTimestamp dDemandeCgf() {
    return (NSTimestamp) storedValueForKey("dDemandeCgf");
  }

  public void setDDemandeCgf(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dDemandeCgf from " + dDemandeCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dDemandeCgf");
  }

  public NSTimestamp dFinIndemniteCgf() {
    return (NSTimestamp) storedValueForKey("dFinIndemniteCgf");
  }

  public void setDFinIndemniteCgf(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dFinIndemniteCgf from " + dFinIndemniteCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinIndemniteCgf");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dureeJjCgf() {
    return (Integer) storedValueForKey("dureeJjCgf");
  }

  public void setDureeJjCgf(Integer value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dureeJjCgf from " + dureeJjCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeJjCgf");
  }

  public Integer dureeMmCgf() {
    return (Integer) storedValueForKey("dureeMmCgf");
  }

  public void setDureeMmCgf(Integer value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating dureeMmCgf from " + dureeMmCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeMmCgf");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temFormAgree() {
    return (String) storedValueForKey("temFormAgree");
  }

  public void setTemFormAgree(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating temFormAgree from " + temFormAgree() + " to " + value);
    }
    takeStoredValueForKey(value, "temFormAgree");
  }

  public String temFractionne() {
    return (String) storedValueForKey("temFractionne");
  }

  public void setTemFractionne(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating temFractionne from " + temFractionne() + " to " + value);
    }
    takeStoredValueForKey(value, "temFractionne");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
    	_EOCongeFormation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre chapitre() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre)storedValueForKey("chapitre");
  }

  public void setChapitreRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
      _EOCongeFormation.LOG.debug("updating chapitre from " + chapitre() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre oldValue = chapitre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "chapitre");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "chapitre");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeFormation.LOG.isDebugEnabled()) {
      _EOCongeFormation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeFormation createCongeFormation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temValide
) {
    EOCongeFormation eo = (EOCongeFormation) EOUtilities.createAndInsertInstance(editingContext, _EOCongeFormation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCongeFormation> fetchAllCongeFormations(EOEditingContext editingContext) {
    return _EOCongeFormation.fetchAllCongeFormations(editingContext, null);
  }

  public static NSArray<EOCongeFormation> fetchAllCongeFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeFormation.fetchCongeFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeFormation> fetchCongeFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeFormation> eoObjects = (NSArray<EOCongeFormation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeFormation fetchCongeFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeFormation.fetchCongeFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeFormation fetchCongeFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeFormation> eoObjects = _EOCongeFormation.fetchCongeFormations(editingContext, qualifier, null);
    EOCongeFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeFormation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeFormation fetchRequiredCongeFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeFormation.fetchRequiredCongeFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeFormation fetchRequiredCongeFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeFormation eoObject = _EOCongeFormation.fetchCongeFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeFormation localInstanceIn(EOEditingContext editingContext, EOCongeFormation eo) {
    EOCongeFormation localInstance = (eo == null) ? null : (EOCongeFormation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
