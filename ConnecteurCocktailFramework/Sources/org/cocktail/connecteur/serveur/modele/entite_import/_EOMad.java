// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMad.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMad extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Mad";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TYPE_MAD_KEY = "cTypeMad";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LIEU_MAD_KEY = "lieuMad";
	public static final String MAD_SOURCE_KEY = "madSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String QUOTITE_KEY = "quotite";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMad.class);

  public EOMad localInstanceIn(EOEditingContext editingContext) {
    EOMad localInstance = (EOMad)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cTypeMad() {
    return (String) storedValueForKey("cTypeMad");
  }

  public void setCTypeMad(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating cTypeMad from " + cTypeMad() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeMad");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String lieuMad() {
    return (String) storedValueForKey("lieuMad");
  }

  public void setLieuMad(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating lieuMad from " + lieuMad() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuMad");
  }

  public Integer madSource() {
    return (Integer) storedValueForKey("madSource");
  }

  public void setMadSource(Integer value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating madSource from " + madSource() + " to " + value);
    }
    takeStoredValueForKey(value, "madSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public Double quotite() {
    return (Double) storedValueForKey("quotite");
  }

  public void setQuotite(Double value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMad.LOG.isDebugEnabled()) {
    	_EOMad.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOMad.LOG.isDebugEnabled()) {
      _EOMad.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMad createMad(EOEditingContext editingContext, String cTypeMad
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, Integer madSource
, String statut
, String temImport
, String temValide
) {
    EOMad eo = (EOMad) EOUtilities.createAndInsertInstance(editingContext, _EOMad.ENTITY_NAME);    
		eo.setCTypeMad(cTypeMad);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setMadSource(madSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMad> fetchAllMads(EOEditingContext editingContext) {
    return _EOMad.fetchAllMads(editingContext, null);
  }

  public static NSArray<EOMad> fetchAllMads(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMad.fetchMads(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMad> fetchMads(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMad.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMad> eoObjects = (NSArray<EOMad>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMad fetchMad(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMad.fetchMad(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMad fetchMad(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMad> eoObjects = _EOMad.fetchMads(editingContext, qualifier, null);
    EOMad eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMad)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Mad that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMad fetchRequiredMad(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMad.fetchRequiredMad(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMad fetchRequiredMad(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMad eoObject = _EOMad.fetchMad(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Mad that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMad localInstanceIn(EOEditingContext editingContext, EOMad eo) {
    EOMad localInstance = (eo == null) ? null : (EOMad)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
