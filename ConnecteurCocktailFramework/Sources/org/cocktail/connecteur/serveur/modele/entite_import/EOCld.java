/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** R&egrave;gles de validation :<BR>
 * Verification de dates :<BR>
 * Un Cld dure au minimum 3 mois et au maximum 6 mois<BR>
 * Verifie que l'agent est titulaire <BR>
 * */
public class EOCld extends _EOCld implements InterfaceInvalidationRecordAvecDependance {

	// Méthodes ajoutées
	public void init() {
		super.init();
		setTemProlongCld(Constantes.FAUX);
		setTemFractionne(Constantes.FAUX);
	}
	public NSTimestamp dateCommission() {
		return dComMedCld();
	}
	public String nomRelationPourLog() {
		return EOLogImport.CLD_KEY;
	}
	// Interface InterfaceInvalidationRecordAvecDependance
	public String invaliderRecordsDependants() {
		try {
			invaliderObjets(rechercherDetailsPourCld());
			return null;
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		}
	}
	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueCld.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}

	}
	protected String verifierConge(EOEditingContext editingContext) {
		int nbMin = dureePourCle(editingContext,"cldDureeNonBloquant") ;
		int nbMax = dureePourCle(editingContext,"cldDureeBloquant") ;
		if (dateDebut() != null && dateFin() != null) {
			NSTimestamp limite = DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutMois(dateDebut(),nbMin));	// DT1636 - pour prendre en compte les bornes, on prend le jour précédent
			if (DateCtrl.isBefore(dateFin(),limite)) {
				return "DUREE_MIN_CLD";
			}
			limite = DateCtrl.dateAvecAjoutMois(dateDebut(),nbMax);
			if (DateCtrl.isAfter(dateFin(),limite)) {
				return "DUREE_MAX_CLD";
			}
		}
		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext, idSource(), dateDebut(), dateFin()) == false) {
			return "FONCTIONNAIRE_EN_ACTIVITE";
		}
		
		// Le congé doit reposer sur une carrière de fonctionnaire.
		if (EOCarriere.fonctionnaireEnActivitePendantPeriode(editingContext,idSource(),dateDebut(),dateFin()) == false) {
			return "POPULATION_FONCTIONNAIRE";
		}
		return null;
	}
	// Méthode privées
	private NSArray rechercherDetailsPourCld() {
		NSMutableArray args = new NSMutableArray(this);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cld = %@ AND temImport = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification ("CldDetail", qualifier,null);
		return editingContext().objectsWithFetchSpecification(myFetch);

	}
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = CongeAvecArrete.validerAttributs(editingContext, valeursAttributs);
		resultat = resultat + verifierTemoin(valeursAttributs, "temFractionne");
		resultat = resultat + verifierTemoin(valeursAttributs, "temProlongCld");
		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return cldSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setCldSource(value);
	}

	@Override
	public String sourceKey() {
		return CLD_SOURCE_KEY;
	}
	@Override
	public void setCongeAnnulationRelationship(CongeAvecArrete conge) {
		setCongeAnnulationRelationship((EOCld)conge); 
	}
}
