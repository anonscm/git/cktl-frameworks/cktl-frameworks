// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCpa.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCpa extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Cpa";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CPA_SOURCE_KEY = "cpaSource";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_CTA_KEY = "temCta";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_QUOT_DEGRESSIVE_KEY = "temQuotDegressive";
	public static final String TEM_SURCOTISATION_KEY = "temSurcotisation";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCpa.class);

  public EOCpa localInstanceIn(EOEditingContext editingContext) {
    EOCpa localInstance = (EOCpa)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Integer cpaSource() {
    return (Integer) storedValueForKey("cpaSource");
  }

  public void setCpaSource(Integer value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating cpaSource from " + cpaSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cpaSource");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temCta() {
    return (String) storedValueForKey("temCta");
  }

  public void setTemCta(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating temCta from " + temCta() + " to " + value);
    }
    takeStoredValueForKey(value, "temCta");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temQuotDegressive() {
    return (String) storedValueForKey("temQuotDegressive");
  }

  public void setTemQuotDegressive(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating temQuotDegressive from " + temQuotDegressive() + " to " + value);
    }
    takeStoredValueForKey(value, "temQuotDegressive");
  }

  public String temSurcotisation() {
    return (String) storedValueForKey("temSurcotisation");
  }

  public void setTemSurcotisation(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating temSurcotisation from " + temSurcotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "temSurcotisation");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
    	_EOCpa.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCpa.LOG.isDebugEnabled()) {
      _EOCpa.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCpa createCpa(EOEditingContext editingContext, Integer cpaSource
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temQuotDegressive
, String temSurcotisation
, String temValide
) {
    EOCpa eo = (EOCpa) EOUtilities.createAndInsertInstance(editingContext, _EOCpa.ENTITY_NAME);    
		eo.setCpaSource(cpaSource);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemQuotDegressive(temQuotDegressive);
		eo.setTemSurcotisation(temSurcotisation);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCpa> fetchAllCpas(EOEditingContext editingContext) {
    return _EOCpa.fetchAllCpas(editingContext, null);
  }

  public static NSArray<EOCpa> fetchAllCpas(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCpa.fetchCpas(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCpa> fetchCpas(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCpa.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCpa> eoObjects = (NSArray<EOCpa>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCpa fetchCpa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCpa.fetchCpa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCpa fetchCpa(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCpa> eoObjects = _EOCpa.fetchCpas(editingContext, qualifier, null);
    EOCpa eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCpa)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Cpa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCpa fetchRequiredCpa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCpa.fetchRequiredCpa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCpa fetchRequiredCpa(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCpa eoObject = _EOCpa.fetchCpa(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Cpa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCpa localInstanceIn(EOEditingContext editingContext, EOCpa eo) {
    EOCpa localInstance = (eo == null) ? null : (EOCpa)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
