package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeDechargeService;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotProlongation;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDecharge;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EODecharge extends _EODecharge {
	private static Logger log = Logger.getLogger(EODecharge.class);


	@Override
	public EOLogImport verifierRecordApresOperation() {
		EOLogImport log = null;

		return log;
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.DECHARGE_KEY;
	}
	
	public EOLogImport verifierRecord() {
		return super.verifierRecord();
	}
	
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = "";
		
		String valeur = (String) valeursAttributs.objectForKey(C_TYPE_DECHARGE_KEY);
		if (valeur != null) {
			EOTypeDechargeService typeDecharge = EOTypeDechargeService.getFromCode(editingContext, valeur);
			if (typeDecharge == null) {
				resultat = resultat + "Type_Decharge_Inconnu >>" + valeur + System.getProperty("line.separator");
			} 
			}

		return resultat;
	}
}
