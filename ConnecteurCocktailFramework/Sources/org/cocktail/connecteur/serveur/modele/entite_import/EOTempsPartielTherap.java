package org.cocktail.connecteur.serveur.modele.entite_import;

import org.apache.log4j.Logger;
import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.importer.moteur.ReglesImport;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartielTherap;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOTempsPartielTherap extends _EOTempsPartielTherap {
	private static Logger log = Logger.getLogger(EOTempsPartielTherap.class);

	@Override
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueTempsPartielTherap.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	@Override
	public ObjetPourSIDestinataire destinataireEquivalent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nomRelationPourLog() {
		return EOLogImport.TEMPS_PARTIEL_THERAP_KEY;
	}

	private boolean estInclusDansChangementPositionOuContrat() {
		boolean changementPositionOuContratTrouve = false;
		// On cherche d'abord un changement de Position dans l'import
		NSArray resultats = EOChangementPosition.rechercherChangementsPositionsPourIndividuEtPeriode(editingContext(), idSource(), dateDebut(),
				dateFin());
		if (resultats.count() > 0)
			changementPositionOuContratTrouve = true;
		else {
			// Si rien trouvé, on cherche un contrat dans l'import
			resultats=EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(), idSource(), dateDebut(), dateFin());
			if (resultats.count() > 0)
				changementPositionOuContratTrouve = true;
			else {
				EOGrhumIndividu grhumIndividu = EOIndividuCorresp.individuGrhum(editingContext(), idSource());
				if (grhumIndividu != null) {
					// Si rien trouvé, on cherche un changement de position dans le SI destinataire
					resultats = EOMangueChangementPosition.rechercherChangementsPositionDestinPourIndividuValidesSurPeriode(editingContext(),
							grhumIndividu, dateDebut(), dateFin());
					if (resultats.count() > 0)
						changementPositionOuContratTrouve = true;
					else {
						// Si rien trouvé, on cherche un contrat dans le SI destinataire
						resultats = EOMangueContrat. rechercherContratsPourIndividuEtPeriode(editingContext(),
								grhumIndividu, dateDebut(), dateFin());
						changementPositionOuContratTrouve= (resultats.count()>0);
					}
				}
			}
		}
		return changementPositionOuContratTrouve;
	}

	/**
	 * Verifie que les relations du record sont bien valides : i.e que l'enfant
	 * existe bien cree juste auparavant. Retourne un message de log ou null si
	 * pas d'erreur
	 */
	public EOLogImport verifierRecord() {
		EOLogImport logImport = super.verifierRecord();
		if (logImport != null) {
			return logImport;
		}

		String messageLog = null;

		if (!estInclusDansChangementPositionOuContrat())
			messageLog = "TPT_INCLUS_DANS_CHANGEMENT_POS_OU_CONTRAT";

		if (messageLog != null) {
			logImport = new EOLogImport();
			logImport.initAvecEditingContext(editingContextTravail(), this.entityName(), messageLog);
			logImport.setIdSource(new Integer(idSource().intValue()));
			if (individu() != null) {
				logImport.addObjectToBothSidesOfRelationshipWithKey(individu(), "individu");
			}
			setStatut(STATUT_ERREUR);
		}

		return logImport;
	}



	@Override
	protected NSArray rechercherObjetDansImportPourChevauchement(EOEditingContext editingContext) {
		// Si le tpt a été annulé, on retourne un tableau vide car il ne peut pas y avoir de chevauchement pour ce tpt
		if (dateArreteAnnulation()!=null)
			return new NSArray();
		// On surcharge la méthode pour appeler la bonne méthode statique
		return rechercherObjetsImportPourIndividuEtPeriode(editingContext, this.entityName(), idSource(), dateDebut(), dateFin(),false);
	}

	/**
	 * recherche les objets de type ObjetImportPourIndividuEtPeriode valide
	 * pendant la periode
	 * 
	 * @param editingContext
	 * @param individu
	 * @param nomEntite
	 *            nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode
	 *            peut etre nulle
	 * @param shouldRefresh
	 *            true si les donnees sont raffra&icirc;chies
	 * @return objets trouves
	 */
	public static NSArray rechercherObjetsImportPourIndividuEtPeriode(EOEditingContext editingContext, String nomEntite, Number idSource,
			NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean shouldRefresh) {
		NSMutableArray qualifiers = new NSMutableArray();

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(ID_SOURCE_KEY+" = %@ AND "+STATUT_KEY+" <> 'A' AND "+STATUT_KEY+" <> 'E' ", new NSArray(idSource));
		qualifiers.addObject(qualifier);

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_IMPORT_KEY + "=%@", new NSArray(ObjetImport.A_TRANSFERER)));
		// On retourne que les Tpt n'ayant pas été annulé (date annulation == null)
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_ARRETE_ANNULATION_KEY + " = null", null));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));

		return rechercherDureePourEntiteAvecCriteres(editingContext, nomEntite, new EOAndQualifier(qualifiers), shouldRefresh);
	}

	public static String validerAttributs(EOEditingContext editingContext, NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		Double quotite = (Double)valeursAttributs.objectForKey(QUOTITE_KEY);
		if (quotite != null) {
			int val=quotite.intValue();
			if (val!=50 && val!=60 && val!=70 && val!=80 && val!=90) {
				resultat += "Quotite_Invalide >>" + quotite + System.getProperty("line.separator");
			}
		}
		return resultat;
	}

	@Override
	public Integer eimpSource() {
		return tptSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setTptSource(value);
	}
}
