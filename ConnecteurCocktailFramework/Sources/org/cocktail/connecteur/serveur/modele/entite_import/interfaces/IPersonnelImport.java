package org.cocktail.connecteur.serveur.modele.entite_import.interfaces;

import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

public interface IPersonnelImport extends IObjetImport {

	public String statut();


	public  EOLogImport verifierRecordApresOperation();

}
