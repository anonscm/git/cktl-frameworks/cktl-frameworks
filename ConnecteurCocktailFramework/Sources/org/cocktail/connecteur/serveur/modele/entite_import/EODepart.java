//EODepart.java
//Created on Thu Dec 01 10:00:20 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** Le nom des entites pour lesquelles changer la date de fin en cas de depart est stocke dans un tableau static qu'il convient de modifier si
 * de nouvelles entites sont ajoutees<BR>
 * R&egrave;gles de validation :<BR>
 * 	Verifie les longueurs de strings<BR>
 * 	Le motif et la date debut doivent &ecirc;tre fournis<BR>
 *	Si la date de fin est fournie, celle de debut doit &ecirc;tre anterieure<BR> 
 *	Verifie en fonction du motif si la rne doit &ecirc;tre saisie <BR>
 *  Si le motif de depart est dec&egrave;s, il ne peut pas y avoir de depart posterieur<BR>
 *  Pour un depart en cas de dec&egrave;s, la date de fin ne doit pas &ecirc;tre fournie<BR>
 *  Si le motif de cessation est dec&egrave;s alors la date de dec&egrave;s de l'individu doit &ecirc;tre fournie et la date de debut du depart ne
 *  peut &ecirc;tre posterieure a la date de dec&egrave;s<BR>
 *	Si la date de dec&egrave;s est fournie alors la date de cessation doit &ecirc;tre inferieure ou egale a date de dec&egrave;s.<BR>
 * 	Si la cessation definitive de fonctions est prononcee pour un motif qui met fin a la carri&egrave;re autre que le dec&egrave;s alors la date d'effet de 
 *  radiation des cadres doit &ecirc;tre fournie. <BR>
 *  Correspondance avec les donnees du SI Destinataire : si le motif est 'décès' alors record du SI Destinataire dont le motif est 'décès' (on ne meurt qu'une fois !!).
 *  Dans tous les cas, depart commen&ccedil;ant le m&circ;me jour.

 * @author christine
 */
public class EODepart extends _EODepart {

	public EODepart() {
		super();
	}


	public boolean estDepartPrevisionnel() {
		return temDepartPrevisionnel() != null && temDepartPrevisionnel().equals(CocktailConstantes.VRAI);
	}
	/** Verifie qu'il n'y a pas de chevauchement de periodes avec des objets de m&ecirc;me type dans l'import courant et
	 * dans le SI Destinataire */
	public EOLogImport verifierRecord() {
		EOLogImport log = super.verifierRecord();
		if (log == null) {
			String message = verifierDepart(editingContext(),individu());
			if (message != null) {
				LogManager.logDetail("Insertion d'un log pour les erreurs sur le depart");
				log = new EOLogImport();
				log.initAvecEditingContext(editingContextTravail(),this.entityName(), message);
				log.ajouterRelation(this);
				log.setEimpSource(eimpSource());
				setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			}
		}

		return log;
	}
	/** Verifie les donnees du record en provenance du client. Effectue les m&ecirc;mes verifications que verifierRecordApresOperation
	 * et retourne le message de log si il y a une erreur */
	public String verifierRecordPourClient(EOEditingContext editingContext) {
		String message = super.verifierRecordPourClient(editingContext);
		if (message != null) {
			return message;
		}
		// Rajouter l'individu qui n'est pas construit lorsqu'on effectue les vérifications client
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource());
		return verifierDepart(editingContext,individu);
	}
	/** Depart avec la m&circ;me date de debut */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		if (individu() == null || estNouvelIndividu()) { // A fortiori le départ est nouveau	
			return null;
		}
		// Vérifier si il s'agit d'un depart pour dec&egrave;s
		if (cMotifDepart().equals(EOMotifDepart.TYPE_MOTIF_DECES)) {
			NSArray departsPourDeces = EOMangueDepart.rechercherDepartsPourDecesEtIndividu(editingContext(), individu());
			if (departsPourDeces != null && departsPourDeces.count() >= 1) {
				return (EOMangueDepart)departsPourDeces.objectAtIndex(0);
			}
		}
		// Rechercher les départs qui commencent à la même date, il y en a au plus un puisqu'il n'y a pas de chevauchement
		NSArray departsMangue = EOMangueDepart.rechercherDepartsManguePourIndividuEtDateDebut(editingContextTravail(), individu(),dateDebut());
		// Si il y en a plus d'un, on ne sait pas lequel prendre mais ne devrait pas se produire car pas de chevauchement des départs
		if (departsMangue == null || (departsMangue != null && departsMangue.count() > 1)) {
			return null;
		}
		try {
			EOMangueDepart departMangue = (EOMangueDepart)departsMangue.objectAtIndex(0);
			// Vérifier si il n'est pas affecté à un autre départ qui est déjà importé
			ObjetCorresp correspondance = ObjetCorresp.rechercherObjetCorrespPourRecordDestinataire(editingContextTravail(), departMangue, "Depart");
			if (correspondance != null) {
				return null;	// le départ destinataire correspond déjà à un autre départ
			} else {
				return departMangue;
			}
		} catch (Exception e) {
			return null;
		}	
	}
	/** Pas de relation dans les logs */
	public String nomRelationPourLog() {
		return EOLogImport.DEPART_KEY;
	}
	// Méthodes protégées
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOMangueDepart.rechercherDepartsManguePourIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}
	/** Pour mettre a jour un depart, la date de modification de service est modifiee, la date de fin
	 * est mise a nulle si elle existait */
	public void mettreAJourPourDeces(NSTimestamp dateDeces) {
		setDateDebut(dateDeces);
		setDCessationService(dateDeces);
		setDateFin(null);
		setCMotifDepart(EOMotifDepart.TYPE_MOTIF_DECES);
	}
	// Méthodes privées
	private String verifierDepart(EOEditingContext editingContext,EOIndividu individu) {
		EOMotifDepart motifDepart = (EOMotifDepart)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "MotifDepart", "cMotifDepart", cMotifDepart());
		EOGenericRecord autreDepartPourDeces = rechercherAutreDepartPourDeces(editingContext,individu);
		if (motifDepart.estDeces()) {
			if (dateFin() != null) {
				return "DATE_FIN_POUR_DECES";
			}
		} else if (autreDepartPourDeces != null && (DateCtrl.isAfter(dateDebut(), (NSTimestamp)autreDepartPourDeces.valueForKey("dateDebut")) || dateFin() == null || (dateFin() != null && DateCtrl.isAfter(dateFin(), (NSTimestamp)autreDepartPourDeces.valueForKey("dateDebut"))))) {
			return "DEPART_POSTERIEUR_DECES";
		} else if (motifDepart.doitFournirLieu() && cRne() == null && lieuDepart() == null) {
			return "RNE_ACCUEIL_OBLIGATOIRE";
		}
		NSTimestamp dateDeces = dateDecesIndividu(editingContext,individu);
		// Si le motif de cessation est décès alors la date de décès de l'individu doit être fournie et la date de départ ne peut être postérieure
		// à la date de départ
		if (motifDepart.estDeces()) {
			if (dateDeces == null) {
			return "DATE_DECES_INDIVIDU";
			} else if (DateCtrl.isAfter(dateDebut(), dateDeces)) {
				return "DATE_DEBUT_POUR_DECES";
			}
		} 
		if (estDepartPrevisionnel() == false) {		
			boolean estTitulaireAvantDepart = false;
			
			if (individu != null) {
				estTitulaireAvantDepart = EOCarriere.estTitulaireAvantDate(editingContext, individu.idSource(),dateDebut());
			} 
			
			
			
			if (motifDepart != null && motifDepart.metFinACarriere() && dCessationService() == null) {
				setDCessationService(DateCtrl.jourPrecedent(dateDebut()));
			}
			
			if (motifDepart != null && dateEffetRadiationObligatoire(motifDepart) && dEffetRadiation() == null) {
//				if (motifDepart != null && cMotifDepart().equals("RAD") && dEffetRadiation() == null) {
				setDEffetRadiation(dateDebut());
			}
			
			
			
			if (estTitulaireAvantDepart && dateEffetRadiationObligatoire(motifDepart) && dEffetRadiation() == null) {
				// Si la cessation définitive de fonctions est prononcée pour un motif qui met fin à la carrière autre que le décès alors la date d'effet de 
				// radiation des cadres et la date de cessation de service  doivent être fournies. 
				return "D_EFFET_RADIATION_OBLIGATOIRE";
			}
			if (motifDepart != null && motifDepart.metFinACarriere() && dCessationService() == null) {
				// la date de cessation de service doit être fournie
				return "D_CESSATION_SERVICE_OBLIGATOIRE";
			}
			// Si la date de décès est fournie alors la date de cessation de service doit être <= date de décès. 
			if (dateDeces != null && dCessationService() !=null && DateCtrl.isAfter(dCessationService(), dateDeces)) {
				return "D_CESSATION_SERVICE_INVALIDE";
			}
		}

		return null;

	}
	// Retourne un EODepart ou un EOMangueDepart si il n'existe pas d'import de ce dernier
	private EOGenericRecord rechercherAutreDepartPourDeces(EOEditingContext editingContext,EOIndividu individu) {
		return rechercherDepartPourDeces(editingContext,individu,eimpSource());
	}
	// Retourne la date de décès de l'individu dans la base d'import
	private NSTimestamp dateDecesIndividu(EOEditingContext editingContext,EOIndividu individu) {
		if (individu.dDeces() != null && individu.temImport().equals(A_TRANSFERER)) {
			return individu.dDeces();
		}
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			return individuGrhum.dDeces();
		}
		return null;
	}
	/* La date d'effet de radiation est obligatoire pour les départs de titulaires qui ne sont pas prévisionnels
	 * (le motif met fin à la carrièrere, ce n'est pas le motif de décès et c'est un motif ONP)
	 */
	private boolean dateEffetRadiationObligatoire(EOMotifDepart motif) {
		return estDepartPrevisionnel() == false && motif.estDeces() == false && motif.metFinACarriere() && motif.cMotifDepartOnp() != null;
	}

	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		// Vérifier que la date de début n'est pas postérieure à la date de fin
		String motif = (String)valeursAttributs.objectForKey("cMotifDepart");
		EOMotifDepart motifDepart = (EOMotifDepart)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "MotifDepart", "cMotifDepart", motif);
		if (motifDepart == null) {
			resultat = resultat + "Motif_Inconnu >>" + motif + System.getProperty("line.separator");
		}
		String rne = (String)valeursAttributs.objectForKey("cRne");
		if (rne != null) {
			if (EORne.getFromCode(editingContext, rne) == null) {
				resultat = resultat + "Rne_Inconnu >>" + rne + System.getProperty("line.separator");
			}
		}
		resultat = resultat + verifierTemoin(valeursAttributs, "temDepartPrevisionnel");

		return resultat;
	}
	/** Retourne un EODepart ou un EOMangueDepart si il n'existe pas d'import de ce dernier
	 * 
	 * @param editingContext
	 * @param individu
	 * @param eimpSourceDepart si non nul, retourne un depart pour dec&egrave;s avec un eimpSource different
	 * sinon retourne le premier depart pour dec&egrave;s trouve
	 * @return EODepart depart de l'import courant ou EOMangueDepart si pas de depart  dans l'import courant
	 */
	public static EOGenericRecord rechercherDepartPourDeces(EOEditingContext editingContext,EOIndividu individu,Number eimpSourceDepart) {
		// On recherche d'abord dans l'import courant
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@  AND statut <> 'A' AND statut <> 'E'", new NSArray(individu.idSource())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("cMotifDepart = %@", new NSArray(EOMotifDepart.TYPE_MOTIF_DECES)));
		if (eimpSourceDepart != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("eimpSource <> %@", new NSArray(eimpSourceDepart)));
		}
		NSArray departs = rechercherDureePourEntiteAvecCriteres(editingContext, "Depart", new EOAndQualifier(qualifiers),false);
		if (departs.count() > 0) {
			return (EODepart)departs.objectAtIndex(0);
		} else {
			// Rechercher dans le SI Destinataire
			departs = EOMangueDepart.rechercherDepartsPourDecesEtIndividu(editingContext,individu);
			if (departs != null && departs.count() > 0) {
				// on prend le premier car il y en a au plus un
				EOMangueDepart departMangue = (EOMangueDepart)departs.objectAtIndex(0);
				EODepart depart = (EODepart)ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext, departMangue, "Depart");
				if (depart == null) {
					// Il s'agit d'un départ uniquement défini dans Mangue
					return departMangue;
				} else if (eimpSourceDepart != null && depart.eimpSource().equals(eimpSourceDepart) == false) {
					// Il s'agit d'un autre départ
					return depart;
				}
			}
		}
		return null;
	}
	/** Retourne un EODepart ou un EOMangueDepart si il n'existe pas d'import de ce dernier
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateDebut non nul, date de debut du depart
	 * @return EODepart depart de l'import courant ou EOMangueDepart si pas de depart  dans l'import courant
	 */
	public static EOGenericRecord rechercherDepartPourDateDebut(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {
		// On recherche d'abord dans l'import courant
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@  AND statut <> 'A' AND statut <> 'E'", new NSArray(individu.idSource())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temImport = %@", new NSArray(ObjetImport.A_TRANSFERER)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("cMotifDepart = %@", new NSArray(EOMotifDepart.TYPE_MOTIF_DECES)));
		NSMutableArray args = new NSMutableArray(DateCtrl.jourPrecedent(date));
		args.addObject(DateCtrl.jourSuivant(date));
		EOQualifier qualifierDate = EOQualifier.qualifierWithQualifierFormat("dateDebut > %@ AND dateFin < %@", args);
		qualifiers.addObject(qualifierDate);
		NSArray departs = rechercherDureePourEntiteAvecCriteres(editingContext, "Depart", new EOAndQualifier(qualifiers),false);
		if (departs.count() > 0) {
			return (EODepart)departs.objectAtIndex(0);
		} else {
			// Rechercher dans le SI Destinataire
			departs = EOMangueDepart.rechercherDepartsManguePourIndividuEtDateDebut(editingContext, individu, date);
			if (departs != null && departs.count() > 0) {
				// on prend le premier car il y en a au plus un
				return (EOMangueDepart)departs.objectAtIndex(0);
			}
		}
		return null;
	}


}
