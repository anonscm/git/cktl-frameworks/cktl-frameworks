// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOBonifications.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOBonifications extends ObjetImportPourIndividuEtPeriode {
	public static final String ENTITY_NAME = "Bonifications";

	// Attributes
	public static final String BONI_DUREE_KEY = "boniDuree";
	public static final String BONI_SOURCE_KEY = "boniSource";
	public static final String D_ARRIVEE_KEY = "dArrivee";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NBO_CODE_KEY = "nboCode";
	public static final String NBOT_CODE_KEY = "nbotCode";
	public static final String NBT_CODE_KEY = "nbtCode";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String NATURE_BONIFICATION_KEY = "natureBonification";
	public static final String TAUX_KEY = "taux";
	public static final String TERRITOIRE_KEY = "territoire";

  private static Logger LOG = Logger.getLogger(_EOBonifications.class);

  public EOBonifications localInstanceIn(EOEditingContext editingContext) {
    EOBonifications localInstance = (EOBonifications)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String boniDuree() {
    return (String) storedValueForKey("boniDuree");
  }

  public void setBoniDuree(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating boniDuree from " + boniDuree() + " to " + value);
    }
    takeStoredValueForKey(value, "boniDuree");
  }

  public Integer boniSource() {
    return (Integer) storedValueForKey("boniSource");
  }

  public void setBoniSource(Integer value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating boniSource from " + boniSource() + " to " + value);
    }
    takeStoredValueForKey(value, "boniSource");
  }

  public NSTimestamp dArrivee() {
    return (NSTimestamp) storedValueForKey("dArrivee");
  }

  public void setDArrivee(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dArrivee from " + dArrivee() + " to " + value);
    }
    takeStoredValueForKey(value, "dArrivee");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String nboCode() {
    return (String) storedValueForKey("nboCode");
  }

  public void setNboCode(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating nboCode from " + nboCode() + " to " + value);
    }
    takeStoredValueForKey(value, "nboCode");
  }

  public String nbotCode() {
    return (String) storedValueForKey("nbotCode");
  }

  public void setNbotCode(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating nbotCode from " + nbotCode() + " to " + value);
    }
    takeStoredValueForKey(value, "nbotCode");
  }

  public String nbtCode() {
    return (String) storedValueForKey("nbtCode");
  }

  public void setNbtCode(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating nbtCode from " + nbtCode() + " to " + value);
    }
    takeStoredValueForKey(value, "nbtCode");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
      _EOBonifications.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification natureBonification() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification)storedValueForKey("natureBonification");
  }

  public void setNatureBonificationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
      _EOBonifications.LOG.debug("updating natureBonification from " + natureBonification() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification oldValue = natureBonification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "natureBonification");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "natureBonification");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux taux() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux)storedValueForKey("taux");
  }

  public void setTauxRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
      _EOBonifications.LOG.debug("updating taux from " + taux() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux oldValue = taux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "taux");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "taux");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire territoire() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire)storedValueForKey("territoire");
  }

  public void setTerritoireRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
      _EOBonifications.LOG.debug("updating territoire from " + territoire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire oldValue = territoire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "territoire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "territoire");
    }
  }
  

  public static EOBonifications createBonifications(EOEditingContext editingContext, Integer boniSource
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temImport
, String temValide
) {
    EOBonifications eo = (EOBonifications) EOUtilities.createAndInsertInstance(editingContext, _EOBonifications.ENTITY_NAME);    
		eo.setBoniSource(boniSource);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemImport(temImport);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOBonifications> fetchAllBonificationses(EOEditingContext editingContext) {
    return _EOBonifications.fetchAllBonificationses(editingContext, null);
  }

  public static NSArray<EOBonifications> fetchAllBonificationses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBonifications.fetchBonificationses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOBonifications> fetchBonificationses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOBonifications.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBonifications> eoObjects = (NSArray<EOBonifications>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOBonifications fetchBonifications(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBonifications.fetchBonifications(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBonifications fetchBonifications(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBonifications> eoObjects = _EOBonifications.fetchBonificationses(editingContext, qualifier, null);
    EOBonifications eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOBonifications)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Bonifications that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBonifications fetchRequiredBonifications(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBonifications.fetchRequiredBonifications(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBonifications fetchRequiredBonifications(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBonifications eoObject = _EOBonifications.fetchBonifications(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Bonifications that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBonifications localInstanceIn(EOEditingContext editingContext, EOBonifications eo) {
    EOBonifications localInstance = (eo == null) ? null : (EOBonifications)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
