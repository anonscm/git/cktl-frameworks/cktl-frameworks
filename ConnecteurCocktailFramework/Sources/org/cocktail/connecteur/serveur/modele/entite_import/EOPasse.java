// EOPasse.java
// Created on Wed Nov 30 16:41:45 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_import;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.DateCtrl.IntRef;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeFonctionPublique;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeService;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePasse;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation :<BR>
 *	La date debut doit etre anterieure a celle de fin<BR> 
 *	Le nombre d'annees, mois et jours de duree validee n'est pas superieur au nombre d'annees, mois et jours entre la date de debut et celle de fin
 *	Si la duree de services valides est fournie, la date de validation et la quotite de services doivent etre fournies
 **/
public class EOPasse extends _EOPasse {

	public static final String TYPE_SERVICE_VALIDES = "SV";
	public static final String TYPE_SERVICE_EAS = "EA";
	public static final String TYPE_SERVICE_ENGAGE = "EN";
	public static final String TYPE_SERVICE_ELEVE = "EL";
	public static final String TYPE_SERVICE_MILITAIRE = "MI";

	private static final String TYPE_FONCTION_PUBLIQUE_HOSPITALIERE = "H";
	private static final String TYPE_FONCTION_PUBLIQUE_TERRITOTIALE = "T";
	private static final String TYPE_FONCTION_PUBLIQUE_ETAT = "E";

	public static final String TYPE_TEMPS_COMPLET = "C";
	public static final String TYPE_TEMPS_INCOMPLET = "I";
	public static final String TYPE_TEMPS_PARTIEL = "P";

	public EOPasse() {
		super();
	}

	public boolean estTitulaire() {
		return temTitulaire() != null && temTitulaire().equals(CocktailConstantes.VRAI);
	}
	public boolean estSecteurPublic() {
		return secteurPasse() != null && secteurPasse().equals(CocktailConstantes.VRAI);
	}

	public boolean estTempsIncomplet() {
		return false;//pasTempsIncomplet() != null && pasTempsIncomplet().equals(Constantes.VRAI);
	}
	/** Pas de destinataire equivalent */
	public ObjetPourSIDestinataire destinataireEquivalent() {
		return null;
	}
	/** Pas de relation dans le log */
	public String nomRelationPourLog() {
		return EOLogImport.PASSE_KEY;
	}
	/** Verifie que les passes ne chevauchent pas des segments de carri&egrave;re ou des contrats principaux.
	 * Verifie que la duree de services valides ne depasse la duree du passe.
	 * Verifie la presence des champs obligatoires pour les passes en secteur public.
	 * Verifie les quotites des passes secteur public qui se chevauchent en prenant en compte les passes
	 * de l'import courant et du SI Destinataire */
	public EOLogImport verifierRecordApresOperation() {
		String message = verifierPasse(editingContext(),individu());
		if (message != null) {
			LogManager.logDetail("Insertion d'un log pour les erreurs sur le passé");
			EOLogImport log = new EOLogImport();
			log.initAvecEditingContext(editingContextTravail(),this.entityName(), message);
			log.setEimpSource(eimpSource());
			log.ajouterRelation(this);
			setStatut(STATUT_ERREUR);	// Indiquer dans l'objet qu'il y a une erreur
			return log;
		} else {
			return null;
		}
	}
	/** Verifie les donnees du record en provenance du client. Effectue les memes verifications que verifierRecordApresOperation
	 * et retourne le message de log si il y a une erreur */
	public String verifierRecordPourClient(EOEditingContext editingContext) {
		// Rajouter l'individu qui n'est pas construit lorsqu'on effectue les vérifications client
		EOIndividu individu = EOIndividu.individuPourSourceId(editingContext, idSource());
		return verifierPasse(editingContext,individu);

	}
	// Méthodes protégées
	/** La verification des chevauchements est faite par un autre biais */
	protected int nbObjetsPourPeriodeDansSIDestinataire(EOEditingContext editingContext) {
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext, idSource());
		if (individuGrhum != null) {
			NSArray periodes = EOManguePasse.findForIndividuEtPeriode(editingContext,individuGrhum,dateDebut(),dateFin());
			return periodes.count();
		} else {
			return 0;
		}
	}

	// Méthodes privées
	private String verifierPasse(EOEditingContext editingContext,EOIndividu individu) {
		if (estSecteurPublic()) {
			if (estTitulaire() == false) {
				if (dValidationService() != null && DateCtrl.isBefore(dValidationService(), dateFin())) {
					return "DATE_VALIDATION_ANTERIEURE";
				}
				if (pasQuotite() == null) {
					setPasQuotite(new Integer(100));
				}
			} 
		}

		return null;
	}
	
	private static void reformatTypeService(NSMutableDictionary valeursAttributs) {
		String typeService = (String)valeursAttributs.objectForKey(EOPasse.PAS_TYPE_SERVICE_KEY);
		if (typeService==null)
			return;
		
		if ("1".equals(typeService)) {
			valeursAttributs.setObjectForKey(TYPE_SERVICE_EAS, EOPasse.PAS_TYPE_SERVICE_KEY);
		}
		if ("2".equals(typeService)) {
			valeursAttributs.setObjectForKey(TYPE_SERVICE_VALIDES, EOPasse.PAS_TYPE_SERVICE_KEY);
		}
		if ("3".equals(typeService)) {
			valeursAttributs.setObjectForKey(TYPE_SERVICE_ELEVE, EOPasse.PAS_TYPE_SERVICE_KEY);
		}
		if ("5".equals(typeService)) {
			valeursAttributs.setObjectForKey(TYPE_SERVICE_MILITAIRE, EOPasse.PAS_TYPE_SERVICE_KEY);
		}
		if ("6".equals(typeService)) {
			valeursAttributs.setObjectForKey(TYPE_SERVICE_ENGAGE, EOPasse.PAS_TYPE_SERVICE_KEY);
		}
	}
	
	private static void reformatTypeFctPublique(NSMutableDictionary valeursAttributs) {
		String typeFonctionPublique = (String)valeursAttributs.objectForKey(EOPasse.PAS_TYPE_FCT_PUBLIQUE_KEY);
		if (typeFonctionPublique==null)
			return;
		if ("1".equals(typeFonctionPublique)) {
			valeursAttributs.setObjectForKey(TYPE_FONCTION_PUBLIQUE_ETAT, EOPasse.PAS_TYPE_FCT_PUBLIQUE_KEY);
		}
		if ("2".equals(typeFonctionPublique)) {
			valeursAttributs.setObjectForKey(TYPE_FONCTION_PUBLIQUE_TERRITOTIALE, EOPasse.PAS_TYPE_FCT_PUBLIQUE_KEY);
		}
		if ("3".equals(typeFonctionPublique)) {
			valeursAttributs.setObjectForKey(TYPE_FONCTION_PUBLIQUE_HOSPITALIERE, EOPasse.PAS_TYPE_FCT_PUBLIQUE_KEY);
		}
	}
	
	private static void reformatTypeTemps(NSMutableDictionary valeursAttributs) {
		String typeTemps = (String)valeursAttributs.objectForKey(EOPasse.PAS_TYPE_TEMPS_KEY);
		if (typeTemps != null) {
			if ("TC".equals(typeTemps)) {
				valeursAttributs.setObjectForKey(TYPE_TEMPS_COMPLET, EOPasse.PAS_TYPE_TEMPS_KEY);
			}
			if ("TI".equals(typeTemps)) {
				valeursAttributs.setObjectForKey(TYPE_TEMPS_INCOMPLET, EOPasse.PAS_TYPE_TEMPS_KEY);
			}
			if ("TP".equals(typeTemps)) {
				valeursAttributs.setObjectForKey(TYPE_TEMPS_PARTIEL, EOPasse.PAS_TYPE_TEMPS_KEY);
			}
			
		} else {
			valeursAttributs.setObjectForKey(TYPE_TEMPS_COMPLET, EOPasse.PAS_TYPE_TEMPS_KEY);
		}
		
	}
	
	private static void reformatAttributs(NSMutableDictionary valeursAttributs) {
		reformatTypeService(valeursAttributs);
		reformatTypeFctPublique(valeursAttributs);
		reformatTypeTemps(valeursAttributs);
	}

	
	// Méthodes statiques
	public static String validerAttributs(EOEditingContext editingContext,NSMutableDictionary valeursAttributs) {
		reformatAttributs(valeursAttributs);
		
		String resultat = ObjetImportPourIndividuEtPeriode.validerAttributs(editingContext, valeursAttributs);
		Number idSource = (Number)valeursAttributs.objectForKey("idSource");
		if (idSource == null) {
			resultat = resultat + "Id_Source >>" + System.getProperty("line.separator");
		}
		String categorie = (String)valeursAttributs.objectForKey("cCategorie");
		if (categorie != null) {
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "Categorie","cCategorie", categorie);
			if (record == null) {
				resultat += "Categorie_Inconnue >>" + categorie + System.getProperty("line.separator");
			}
		}
		String typePopulation = (String)valeursAttributs.objectForKey("cTypePopulation");
		if (typePopulation != null) {
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypePopulation","cTypePopulation", typePopulation);
			if (record == null) {
				resultat += "Type_Population_Inconnu >>" + typePopulation + System.getProperty("line.separator");
			}
		}
		String typeService = (String)valeursAttributs.objectForKey(EOPasse.PAS_TYPE_SERVICE_KEY);
		if (typeService != null) {
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOTypeService.ENTITY_NAME,EOTypeService.C_TYPE_SERVICE_KEY, typeService);
			if (record == null) {
				resultat += "Type_Service_Inconnu >>" + typeService + System.getProperty("line.separator");
			}
		}
		String secteurPasse = (String)valeursAttributs.objectForKey(EOPasse.SECTEUR_PASSE_KEY);
		boolean estTypePublique = (secteurPasse!=null && secteurPasse.equals(CocktailConstantes.VRAI));

		String typeFonctionPublique = (String)valeursAttributs.objectForKey(EOPasse.PAS_TYPE_FCT_PUBLIQUE_KEY);

		if (estTypePublique && typeFonctionPublique==null)
			resultat += "Type_Fonction_Publique_Vide >>" + System.getProperty("line.separator");
		
		if (typeFonctionPublique != null) {
			EOGenericRecord record = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOTypeFonctionPublique.ENTITY_NAME,EOTypeFonctionPublique.C_TYPE_FP_KEY, typeFonctionPublique);
			if (record == null) {
				resultat += "Type_Fonction_Publique_Inconnu >>" + typeFonctionPublique + System.getProperty("line.separator");
			}
		}
		String typeTemps = (String)valeursAttributs.objectForKey(PAS_TYPE_TEMPS_KEY);
		if (typeTemps != null) {
			if (!typeTemps.equals(TYPE_TEMPS_COMPLET) && !typeTemps.equals(TYPE_TEMPS_INCOMPLET) && !typeTemps.equals(TYPE_TEMPS_PARTIEL) ) {
				resultat += "Type_Temps_Inconnu >>" + typeTemps + System.getProperty("line.separator");
			}
		}
		// Vérifier la quotité de service (comprise en 50 et 100%)
		Number quot = (Number)valeursAttributs.objectForKey("pasQuotite");
		/*pas de contrôle sur le niveau de quotité afin que cela soit fait par la suite dans les contrôles CIR de Mangue*/
//		if (quot != null) {
//			double quotite = quot.doubleValue();
//			if (quotite != 0 && (quotite < 50.00 || quotite  > 100.00)) {
//				resultat += "Quotite_Invalide >>" + quotite + System.getProperty("line.separator");
//			}
//		}
		// Vérifier les témoins
		resultat = resultat + verifierTemoin(valeursAttributs, "temTitulaire");
		resultat = resultat + verifierTemoin(valeursAttributs, "pasTempsIncomplet");
		resultat = resultat + verifierTemoin(valeursAttributs, "pasCotisationTotale");
		resultat = resultat + verifierTemoin(valeursAttributs, "secteurPasse");

		return resultat;
	}
	/** Retourne la liste des passes hors EN non-titulaires concernant la fonction publique pour la periode
	 *  tries par ordre de date croissante 
	 * @param debutPeriode peut etre nul
	 */
	public static NSArray rechercherPassesImportCourantPourContractuelServicePublicEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND secteurPasse = 'O' AND temTitulaire = 'N' AND statut <> 'A' AND statut <> 'E'", new NSArray(individu.idSource())));
		if (debutPeriode != null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		}
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers), new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		return editingContext.objectsWithFetchSpecification(fs);
	}

	private class PasseAvecDureesCalculees {
		private Number quotite;
		private NSTimestamp dateDebut,dateFin;
		private int nbAnnees;
		private int nbMois;
		private int nbJours;
		public PasseAvecDureesCalculees(EOPasse passe,boolean estPourTempsIncomplet) {
			this.quotite = passe.pasQuotite();
			this.dateDebut = passe.dateDebut();
			this.dateFin = passe.dateFin();
			if (estPourTempsIncomplet) {
				if (passe.dureeValideeAnnees() != null || passe.dureeValideeMois() != null || passe.dureeValideeJours() != null) {
					if (passe.dureeValideeAnnees() != null) {
						nbAnnees = dureeValideeAnnees().intValue();
					}
					if (dureeValideeMois() != null) {
						nbMois = dureeValideeMois().intValue();
					}
					if (dureeValideeJours() != null) {
						nbJours = dureeValideeJours().intValue();
					}
				} else {
					IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
					DateCtrl.joursMoisAnneesEntre(passe.dateDebut(), passe.dateFin(), anneeRef, moisRef, jourRef, false,true);
					nbAnnees = anneeRef.value;
					nbMois = moisRef.value;
					nbJours = jourRef.value;
				}
			}
		}
		public PasseAvecDureesCalculees(EOManguePasse passe,boolean estPourTempsIncomplet) {
			this.quotite = passe.pasQuotite();
			this.dateDebut = passe.dateDebut();
			this.dateFin = passe.dateFin();
			if (estPourTempsIncomplet) {
				if (passe.dureeValideeAnnees() != null || passe.dureeValideeMois() != null || passe.dureeValideeJours() != null) {
					if (passe.dureeValideeAnnees() != null) {
						nbAnnees = dureeValideeAnnees().intValue();
					}
					if (dureeValideeMois() != null) {
						nbMois = dureeValideeMois().intValue();
					}
					if (dureeValideeJours() != null) {
						nbJours = dureeValideeJours().intValue();
					}
				} else {
					IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
					DateCtrl.joursMoisAnneesEntre(passe.dateDebut(), passe.dateFin(), anneeRef, moisRef, jourRef, false,true);
					nbAnnees = anneeRef.value;
					nbMois = moisRef.value;
					nbJours = jourRef.value;
				}
			}

		}
		public int nbAnnees() {
			return nbAnnees;
		}
		public int nbMois() {
			return nbMois;
		}
		public int nbJours() {
			return nbJours;
		}
		public Number quotite() {
			return quotite;
		}
		public NSTimestamp dateDebut() {
			return dateDebut;
		}	
		public NSTimestamp dateFin() {
			return dateFin;
		}
	}
	
	public void validateForSave() throws NSValidation.ValidationException {
		// Transformation des types de service
		if (pasPcAcquitee() == null) {
			setPasPcAcquitee("O");			
		}
	}

	@Override
	public Integer eimpSource() {
		return pasSource();
	}

	@Override
	public void setEimpSource(Integer value) {
		setPasSource(value);
	}


}
