// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaternite.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaternite extends CongeAvecArrete {
	public static final String ENTITY_NAME = "CongeMaternite";

	// Attributes
	public static final String ANNULATION_SOURCE_KEY = "annulationSource";
	public static final String CMAT_SOURCE_KEY = "cmatSource";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_TYPE_CG_MATERN_KEY = "cTypeCgMatern";
	public static final String D_ACCOUCHEMENT_KEY = "dAccouchement";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CONSTAT_MATERN_KEY = "dConstatMatern";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DM_SOURCE_KEY = "dmSource";
	public static final String D_NAIS_PREV_KEY = "dNaisPrev";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String NB_ENFANTS_DECL_KEY = "nbEnfantsDecl";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String OPERATION_KEY = "operation";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_ANNULE_KEY = "temAnnule";
	public static final String TEM_CG_SANS_TRAIT_KEY = "temCgSansTrait";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_GROSSESSE_GEMELLAIRE_KEY = "temGrossesseGemellaire";
	public static final String TEM_GROSSESSE_TRIPLE_KEY = "temGrossesseTriple";
	public static final String TEM_IMPORT_KEY = "temImport";

	// Relationships
	public static final String CONGE_ANNULATION_KEY = "congeAnnulation";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOCongeMaternite.class);

  public EOCongeMaternite localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaternite localInstance = (EOCongeMaternite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annulationSource() {
    return (Integer) storedValueForKey("annulationSource");
  }

  public void setAnnulationSource(Integer value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating annulationSource from " + annulationSource() + " to " + value);
    }
    takeStoredValueForKey(value, "annulationSource");
  }

  public Integer cmatSource() {
    return (Integer) storedValueForKey("cmatSource");
  }

  public void setCmatSource(Integer value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating cmatSource from " + cmatSource() + " to " + value);
    }
    takeStoredValueForKey(value, "cmatSource");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cTypeCgMatern() {
    return (String) storedValueForKey("cTypeCgMatern");
  }

  public void setCTypeCgMatern(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating cTypeCgMatern from " + cTypeCgMatern() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeCgMatern");
  }

  public NSTimestamp dAccouchement() {
    return (NSTimestamp) storedValueForKey("dAccouchement");
  }

  public void setDAccouchement(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dAccouchement from " + dAccouchement() + " to " + value);
    }
    takeStoredValueForKey(value, "dAccouchement");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dConstatMatern() {
    return (NSTimestamp) storedValueForKey("dConstatMatern");
  }

  public void setDConstatMatern(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dConstatMatern from " + dConstatMatern() + " to " + value);
    }
    takeStoredValueForKey(value, "dConstatMatern");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dmSource() {
    return (Integer) storedValueForKey("dmSource");
  }

  public void setDmSource(Integer value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dmSource from " + dmSource() + " to " + value);
    }
    takeStoredValueForKey(value, "dmSource");
  }

  public NSTimestamp dNaisPrev() {
    return (NSTimestamp) storedValueForKey("dNaisPrev");
  }

  public void setDNaisPrev(NSTimestamp value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating dNaisPrev from " + dNaisPrev() + " to " + value);
    }
    takeStoredValueForKey(value, "dNaisPrev");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public Integer nbEnfantsDecl() {
    return (Integer) storedValueForKey("nbEnfantsDecl");
  }

  public void setNbEnfantsDecl(Integer value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating nbEnfantsDecl from " + nbEnfantsDecl() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfantsDecl");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String temAnnule() {
    return (String) storedValueForKey("temAnnule");
  }

  public void setTemAnnule(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating temAnnule from " + temAnnule() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnule");
  }

  public String temCgSansTrait() {
    return (String) storedValueForKey("temCgSansTrait");
  }

  public void setTemCgSansTrait(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating temCgSansTrait from " + temCgSansTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgSansTrait");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temGrossesseGemellaire() {
    return (String) storedValueForKey("temGrossesseGemellaire");
  }

  public void setTemGrossesseGemellaire(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating temGrossesseGemellaire from " + temGrossesseGemellaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temGrossesseGemellaire");
  }

  public String temGrossesseTriple() {
    return (String) storedValueForKey("temGrossesseTriple");
  }

  public void setTemGrossesseTriple(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating temGrossesseTriple from " + temGrossesseTriple() + " to " + value);
    }
    takeStoredValueForKey(value, "temGrossesseTriple");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
    	_EOCongeMaternite.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite congeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite)storedValueForKey("congeAnnulation");
  }

  public void setCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
      _EOCongeMaternite.LOG.debug("updating congeAnnulation from " + congeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite oldValue = congeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOCongeMaternite.LOG.isDebugEnabled()) {
      _EOCongeMaternite.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOCongeMaternite createCongeMaternite(EOEditingContext editingContext, String cTypeCgMatern
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer idSource
, String statut
, String temAnnule
, String temCgSansTrait
, String temConfirme
, String temGestEtab
, String temGrossesseGemellaire
, String temGrossesseTriple
, String temImport
) {
    EOCongeMaternite eo = (EOCongeMaternite) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaternite.ENTITY_NAME);    
		eo.setCTypeCgMatern(cTypeCgMatern);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSource(idSource);
		eo.setStatut(statut);
		eo.setTemAnnule(temAnnule);
		eo.setTemCgSansTrait(temCgSansTrait);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemGrossesseGemellaire(temGrossesseGemellaire);
		eo.setTemGrossesseTriple(temGrossesseTriple);
		eo.setTemImport(temImport);
    return eo;
  }

  public static NSArray<EOCongeMaternite> fetchAllCongeMaternites(EOEditingContext editingContext) {
    return _EOCongeMaternite.fetchAllCongeMaternites(editingContext, null);
  }

  public static NSArray<EOCongeMaternite> fetchAllCongeMaternites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaternite.fetchCongeMaternites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaternite> fetchCongeMaternites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaternite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaternite> eoObjects = (NSArray<EOCongeMaternite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaternite fetchCongeMaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaternite.fetchCongeMaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaternite fetchCongeMaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaternite> eoObjects = _EOCongeMaternite.fetchCongeMaternites(editingContext, qualifier, null);
    EOCongeMaternite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaternite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaternite fetchRequiredCongeMaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaternite.fetchRequiredCongeMaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaternite fetchRequiredCongeMaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaternite eoObject = _EOCongeMaternite.fetchCongeMaternite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaternite localInstanceIn(EOEditingContext editingContext, EOCongeMaternite eo) {
    EOCongeMaternite localInstance = (eo == null) ? null : (EOCongeMaternite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
