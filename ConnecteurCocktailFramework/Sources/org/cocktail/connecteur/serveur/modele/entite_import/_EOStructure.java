// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOStructure.java instead.
package org.cocktail.connecteur.serveur.modele.entite_import;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStructure extends ObjetImportAvecSource {
	public static final String ENTITY_NAME = "Structure";

	// Attributes
	public static final String ACADEMIE_KEY = "academie";
	public static final String ACCES_KEY = "acces";
	public static final String ACCUEIL_KEY = "accueil";
	public static final String AFFICHAGE_KEY = "affichage";
	public static final String ALIAS_KEY = "alias";
	public static final String CA_KEY = "ca";
	public static final String CAPITAL_KEY = "capital";
	public static final String CODE_APE_KEY = "codeApe";
	public static final String CODE_URSSAF_KEY = "codeUrssaf";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EFFECTIFS_KEY = "effectifs";
	public static final String EXPORT_KEY = "export";
	public static final String FONCTION1_KEY = "fonction1";
	public static final String FONCTION2_KEY = "fonction2";
	public static final String GENCOD_KEY = "gencod";
	public static final String ID_RESPONSABLE_SOURCE_KEY = "idResponsableSource";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String MOTS_CLEFS_KEY = "motsClefs";
	public static final String MOYENNE_AGE_KEY = "moyenneAge";
	public static final String NAF_KEY = "naf";
	public static final String NUM_ASSEDIC_KEY = "numAssedic";
	public static final String NUM_CNRACL_KEY = "numCnracl";
	public static final String NUM_IRCANTEC_KEY = "numIrcantec";
	public static final String NUM_RAFP_KEY = "numRafp";
	public static final String NUM_URSSAF_KEY = "numUrssaf";
	public static final String OPERATION_KEY = "operation";
	public static final String RECHERCHE_KEY = "recherche";
	public static final String RNE_KEY = "rne";
	public static final String SIRET_KEY = "siret";
	public static final String STATUT_KEY = "statut";
	public static final String STATUT_JURIDIQUE_KEY = "statutJuridique";
	public static final String STR_AFFICHAGE_KEY = "strAffichage";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String STR_SOURCE_PERE_KEY = "strSourcePere";
	public static final String TEM_IMPORT_KEY = "temImport";
	public static final String TVA_INTRACOM_KEY = "tvaIntracom";
	public static final String TYPE_ETABLISSEMENT_KEY = "typeEtablissement";
	public static final String TYPE_GROUPE_KEY = "typeGroupe";
	public static final String TYPE_STRUCTURE_KEY = "typeStructure";

	// Relationships
	public static final String CIBLE_KEY = "cible";
	public static final String RESPONSABLE_KEY = "responsable";
	public static final String SOURCE_KEY = "source";

  private static Logger LOG = Logger.getLogger(_EOStructure.class);

  public EOStructure localInstanceIn(EOEditingContext editingContext) {
    EOStructure localInstance = (EOStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String academie() {
    return (String) storedValueForKey("academie");
  }

  public void setAcademie(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating academie from " + academie() + " to " + value);
    }
    takeStoredValueForKey(value, "academie");
  }

  public String acces() {
    return (String) storedValueForKey("acces");
  }

  public void setAcces(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating acces from " + acces() + " to " + value);
    }
    takeStoredValueForKey(value, "acces");
  }

  public String accueil() {
    return (String) storedValueForKey("accueil");
  }

  public void setAccueil(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating accueil from " + accueil() + " to " + value);
    }
    takeStoredValueForKey(value, "accueil");
  }

  public String affichage() {
    return (String) storedValueForKey("affichage");
  }

  public void setAffichage(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating affichage from " + affichage() + " to " + value);
    }
    takeStoredValueForKey(value, "affichage");
  }

  public String alias() {
    return (String) storedValueForKey("alias");
  }

  public void setAlias(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating alias from " + alias() + " to " + value);
    }
    takeStoredValueForKey(value, "alias");
  }

  public Double ca() {
    return (Double) storedValueForKey("ca");
  }

  public void setCa(Double value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating ca from " + ca() + " to " + value);
    }
    takeStoredValueForKey(value, "ca");
  }

  public Double capital() {
    return (Double) storedValueForKey("capital");
  }

  public void setCapital(Double value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating capital from " + capital() + " to " + value);
    }
    takeStoredValueForKey(value, "capital");
  }

  public String codeApe() {
    return (String) storedValueForKey("codeApe");
  }

  public void setCodeApe(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating codeApe from " + codeApe() + " to " + value);
    }
    takeStoredValueForKey(value, "codeApe");
  }

  public String codeUrssaf() {
    return (String) storedValueForKey("codeUrssaf");
  }

  public void setCodeUrssaf(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating codeUrssaf from " + codeUrssaf() + " to " + value);
    }
    takeStoredValueForKey(value, "codeUrssaf");
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey("dateFermeture");
  }

  public void setDateFermeture(NSTimestamp value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating dateFermeture from " + dateFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFermeture");
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey("dateOuverture");
  }

  public void setDateOuverture(NSTimestamp value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating dateOuverture from " + dateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateOuverture");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Double effectifs() {
    return (Double) storedValueForKey("effectifs");
  }

  public void setEffectifs(Double value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating effectifs from " + effectifs() + " to " + value);
    }
    takeStoredValueForKey(value, "effectifs");
  }

  public Double export() {
    return (Double) storedValueForKey("export");
  }

  public void setExport(Double value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating export from " + export() + " to " + value);
    }
    takeStoredValueForKey(value, "export");
  }

  public String fonction1() {
    return (String) storedValueForKey("fonction1");
  }

  public void setFonction1(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating fonction1 from " + fonction1() + " to " + value);
    }
    takeStoredValueForKey(value, "fonction1");
  }

  public String fonction2() {
    return (String) storedValueForKey("fonction2");
  }

  public void setFonction2(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating fonction2 from " + fonction2() + " to " + value);
    }
    takeStoredValueForKey(value, "fonction2");
  }

  public String gencod() {
    return (String) storedValueForKey("gencod");
  }

  public void setGencod(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating gencod from " + gencod() + " to " + value);
    }
    takeStoredValueForKey(value, "gencod");
  }

  public Integer idResponsableSource() {
    return (Integer) storedValueForKey("idResponsableSource");
  }

  public void setIdResponsableSource(Integer value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating idResponsableSource from " + idResponsableSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idResponsableSource");
  }

  public String lcStructure() {
    return (String) storedValueForKey("lcStructure");
  }

  public void setLcStructure(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating lcStructure from " + lcStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "lcStructure");
  }

  public String llStructure() {
    return (String) storedValueForKey("llStructure");
  }

  public void setLlStructure(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating llStructure from " + llStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "llStructure");
  }

  public String motsClefs() {
    return (String) storedValueForKey("motsClefs");
  }

  public void setMotsClefs(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating motsClefs from " + motsClefs() + " to " + value);
    }
    takeStoredValueForKey(value, "motsClefs");
  }

  public Double moyenneAge() {
    return (Double) storedValueForKey("moyenneAge");
  }

  public void setMoyenneAge(Double value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating moyenneAge from " + moyenneAge() + " to " + value);
    }
    takeStoredValueForKey(value, "moyenneAge");
  }

  public String naf() {
    return (String) storedValueForKey("naf");
  }

  public void setNaf(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating naf from " + naf() + " to " + value);
    }
    takeStoredValueForKey(value, "naf");
  }

  public String numAssedic() {
    return (String) storedValueForKey("numAssedic");
  }

  public void setNumAssedic(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating numAssedic from " + numAssedic() + " to " + value);
    }
    takeStoredValueForKey(value, "numAssedic");
  }

  public String numCnracl() {
    return (String) storedValueForKey("numCnracl");
  }

  public void setNumCnracl(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating numCnracl from " + numCnracl() + " to " + value);
    }
    takeStoredValueForKey(value, "numCnracl");
  }

  public String numIrcantec() {
    return (String) storedValueForKey("numIrcantec");
  }

  public void setNumIrcantec(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating numIrcantec from " + numIrcantec() + " to " + value);
    }
    takeStoredValueForKey(value, "numIrcantec");
  }

  public String numRafp() {
    return (String) storedValueForKey("numRafp");
  }

  public void setNumRafp(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating numRafp from " + numRafp() + " to " + value);
    }
    takeStoredValueForKey(value, "numRafp");
  }

  public String numUrssaf() {
    return (String) storedValueForKey("numUrssaf");
  }

  public void setNumUrssaf(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating numUrssaf from " + numUrssaf() + " to " + value);
    }
    takeStoredValueForKey(value, "numUrssaf");
  }

  public String operation() {
    return (String) storedValueForKey("operation");
  }

  public void setOperation(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating operation from " + operation() + " to " + value);
    }
    takeStoredValueForKey(value, "operation");
  }

  public String recherche() {
    return (String) storedValueForKey("recherche");
  }

  public void setRecherche(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating recherche from " + recherche() + " to " + value);
    }
    takeStoredValueForKey(value, "recherche");
  }

  public String rne() {
    return (String) storedValueForKey("rne");
  }

  public void setRne(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating rne from " + rne() + " to " + value);
    }
    takeStoredValueForKey(value, "rne");
  }

  public String siret() {
    return (String) storedValueForKey("siret");
  }

  public void setSiret(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating siret from " + siret() + " to " + value);
    }
    takeStoredValueForKey(value, "siret");
  }

  public String statut() {
    return (String) storedValueForKey("statut");
  }

  public void setStatut(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating statut from " + statut() + " to " + value);
    }
    takeStoredValueForKey(value, "statut");
  }

  public String statutJuridique() {
    return (String) storedValueForKey("statutJuridique");
  }

  public void setStatutJuridique(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating statutJuridique from " + statutJuridique() + " to " + value);
    }
    takeStoredValueForKey(value, "statutJuridique");
  }

  public String strAffichage() {
    return (String) storedValueForKey("strAffichage");
  }

  public void setStrAffichage(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating strAffichage from " + strAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "strAffichage");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String strSourcePere() {
    return (String) storedValueForKey("strSourcePere");
  }

  public void setStrSourcePere(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating strSourcePere from " + strSourcePere() + " to " + value);
    }
    takeStoredValueForKey(value, "strSourcePere");
  }

  public String temImport() {
    return (String) storedValueForKey("temImport");
  }

  public void setTemImport(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating temImport from " + temImport() + " to " + value);
    }
    takeStoredValueForKey(value, "temImport");
  }

  public String tvaIntracom() {
    return (String) storedValueForKey("tvaIntracom");
  }

  public void setTvaIntracom(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating tvaIntracom from " + tvaIntracom() + " to " + value);
    }
    takeStoredValueForKey(value, "tvaIntracom");
  }

  public String typeEtablissement() {
    return (String) storedValueForKey("typeEtablissement");
  }

  public void setTypeEtablissement(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating typeEtablissement from " + typeEtablissement() + " to " + value);
    }
    takeStoredValueForKey(value, "typeEtablissement");
  }

  public String typeGroupe() {
    return (String) storedValueForKey("typeGroupe");
  }

  public void setTypeGroupe(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating typeGroupe from " + typeGroupe() + " to " + value);
    }
    takeStoredValueForKey(value, "typeGroupe");
  }

  public String typeStructure() {
    return (String) storedValueForKey("typeStructure");
  }

  public void setTypeStructure(String value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
    	_EOStructure.LOG.debug( "updating typeStructure from " + typeStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "typeStructure");
  }

  public org.cocktail.connecteur.serveur.modele.importer.EOCible cible() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOCible)storedValueForKey("cible");
  }

  public void setCibleRelationship(org.cocktail.connecteur.serveur.modele.importer.EOCible value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
      _EOStructure.LOG.debug("updating cible from " + cible() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOCible oldValue = cible();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cible");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cible");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu responsable() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("responsable");
  }

  public void setResponsableRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
      _EOStructure.LOG.debug("updating responsable from " + responsable() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = responsable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "responsable");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "responsable");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.importer.EOSource source() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOSource)storedValueForKey("source");
  }

  public void setSourceRelationship(org.cocktail.connecteur.serveur.modele.importer.EOSource value) {
    if (_EOStructure.LOG.isDebugEnabled()) {
      _EOStructure.LOG.debug("updating source from " + source() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOSource oldValue = source();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "source");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "source");
    }
  }
  

  public static EOStructure createStructure(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String llStructure
, String statut
, String strSource
, String temImport
, String typeStructure
) {
    EOStructure eo = (EOStructure) EOUtilities.createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLlStructure(llStructure);
		eo.setStatut(statut);
		eo.setStrSource(strSource);
		eo.setTemImport(temImport);
		eo.setTypeStructure(typeStructure);
    return eo;
  }

  public static NSArray<EOStructure> fetchAllStructures(EOEditingContext editingContext) {
    return _EOStructure.fetchAllStructures(editingContext, null);
  }

  public static NSArray<EOStructure> fetchAllStructures(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStructure.fetchStructures(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStructure> fetchStructures(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStructure.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStructure> eoObjects = (NSArray<EOStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStructure fetchStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructure.fetchStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructure fetchStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStructure> eoObjects = _EOStructure.fetchStructures(editingContext, qualifier, null);
    EOStructure eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Structure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructure fetchRequiredStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructure.fetchRequiredStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructure fetchRequiredStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStructure eoObject = _EOStructure.fetchStructure(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Structure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructure localInstanceIn(EOEditingContext editingContext, EOStructure eo) {
    EOStructure localInstance = (eo == null) ? null : (EOStructure)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
