// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOErreurImport.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOErreurImport extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ErreurImport";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NOM_ERREUR_IMPORT_KEY = "nomErreurImport";
	public static final String NOM_ERREUR_TRANSFERT_KEY = "nomErreurTransfert";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String FICHIER_IMPORT_KEY = "fichierImport";

  private static Logger LOG = Logger.getLogger(_EOErreurImport.class);

  public EOErreurImport localInstanceIn(EOEditingContext editingContext) {
    EOErreurImport localInstance = (EOErreurImport)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOErreurImport.LOG.isDebugEnabled()) {
    	_EOErreurImport.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOErreurImport.LOG.isDebugEnabled()) {
    	_EOErreurImport.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String nomErreurImport() {
    return (String) storedValueForKey("nomErreurImport");
  }

  public void setNomErreurImport(String value) {
    if (_EOErreurImport.LOG.isDebugEnabled()) {
    	_EOErreurImport.LOG.debug( "updating nomErreurImport from " + nomErreurImport() + " to " + value);
    }
    takeStoredValueForKey(value, "nomErreurImport");
  }

  public String nomErreurTransfert() {
    return (String) storedValueForKey("nomErreurTransfert");
  }

  public void setNomErreurTransfert(String value) {
    if (_EOErreurImport.LOG.isDebugEnabled()) {
    	_EOErreurImport.LOG.debug( "updating nomErreurTransfert from " + nomErreurTransfert() + " to " + value);
    }
    takeStoredValueForKey(value, "nomErreurTransfert");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOErreurImport.LOG.isDebugEnabled()) {
    	_EOErreurImport.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.importer.EOFichierImport fichierImport() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOFichierImport)storedValueForKey("fichierImport");
  }

  public void setFichierImportRelationship(org.cocktail.connecteur.serveur.modele.importer.EOFichierImport value) {
    if (_EOErreurImport.LOG.isDebugEnabled()) {
      _EOErreurImport.LOG.debug("updating fichierImport from " + fichierImport() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOFichierImport oldValue = fichierImport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "fichierImport");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "fichierImport");
    }
  }
  

  public static EOErreurImport createErreurImport(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
) {
    EOErreurImport eo = (EOErreurImport) EOUtilities.createAndInsertInstance(editingContext, _EOErreurImport.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOErreurImport> fetchAllErreurImports(EOEditingContext editingContext) {
    return _EOErreurImport.fetchAllErreurImports(editingContext, null);
  }

  public static NSArray<EOErreurImport> fetchAllErreurImports(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOErreurImport.fetchErreurImports(editingContext, null, sortOrderings);
  }

  public static NSArray<EOErreurImport> fetchErreurImports(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOErreurImport.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOErreurImport> eoObjects = (NSArray<EOErreurImport>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOErreurImport fetchErreurImport(EOEditingContext editingContext, String keyName, Object value) {
    return _EOErreurImport.fetchErreurImport(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOErreurImport fetchErreurImport(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOErreurImport> eoObjects = _EOErreurImport.fetchErreurImports(editingContext, qualifier, null);
    EOErreurImport eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOErreurImport)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ErreurImport that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOErreurImport fetchRequiredErreurImport(EOEditingContext editingContext, String keyName, Object value) {
    return _EOErreurImport.fetchRequiredErreurImport(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOErreurImport fetchRequiredErreurImport(EOEditingContext editingContext, EOQualifier qualifier) {
    EOErreurImport eoObject = _EOErreurImport.fetchErreurImport(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ErreurImport that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOErreurImport localInstanceIn(EOEditingContext editingContext, EOErreurImport eo) {
    EOErreurImport localInstance = (eo == null) ? null : (EOErreurImport)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
