// EOFichierImport.java
// Created on Wed Aug 01 17:07:33 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.importer;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.LogManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOFichierImport extends _EOFichierImport {
	private final static String DEMARRAGE = "N";
	private final static String IMPORT_TERMINE = "I";
	private final static String TRANSFERT_EN_COURS = "T";
	private final static String TRANSFERT_TERMINE = "V";
	private final static String ANNULE = "A";

    public EOFichierImport() {
        super();
    }


	 /** Prepare les relations. A appeler apr&egrave;s avoir insere un objet dans l'editing context */
    public void preparerRelations(EOSource applicationSource,EOCible applicationCible) {
    	LogManager.logDetail("Preparation des relations");
    	// On n'ajoute la source et la cible qu'à ce moment-là car auparavant l'objet n'est pas associé à un editing context
		addObjectToBothSidesOfRelationshipWithKey(applicationSource, "source");
		addObjectToBothSidesOfRelationshipWithKey(applicationCible, "cible");
    }
    // Méthodes ajoutées
    public void initAvecNomFichier(String nomFichier) {
    	setNomFichier(nomFichier);
    	NSTimestamp today = new NSTimestamp();
    	setDCreation(today);
    	setDModification(today);
    	setTemValide(CocktailConstantes.VRAI);
    	setTemEtat(DEMARRAGE);
    	
    }
    public void invaliderImportCourant() {
    	setTemEtat(ANNULE);
    	setTemValide(CocktailConstantes.FAUX);
    }
    public boolean estImportValide() {
    	return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
    }
    public boolean donneesImportees() {
    	return estImportValide() && temEtat() != null && temEtat().equals(IMPORT_TERMINE);
    }
    public boolean donneesLuesOuImportees() {
		return estImportValide() && temEtat() != null && (temEtat().equals(DEMARRAGE) || temEtat().equals(IMPORT_TERMINE));
	}
    public boolean transfertEnCours() {
    	return estImportValide() && temEtat() != null && temEtat().equals(TRANSFERT_EN_COURS);
    }
    public boolean transfertTermine() {
    	return temEtat() != null && temEtat().equals(TRANSFERT_TERMINE);
    }
    public void terminerPhaseImport() {
    	setTemEtat(IMPORT_TERMINE);
    }
    public void demarrerPhaseTransfert() {
    	setTemEtat(TRANSFERT_EN_COURS);
    }
    public void terminerPhaseTransfert() {
    	setTemValide(CocktailConstantes.FAUX);
    	setTemEtat(TRANSFERT_TERMINE);
    }
    public void validateForSave() {
    	setDModification(new NSTimestamp());
    }
    // Méthodes statiques
    /** Retourne le record avec la date de modification la plus recente */
    public static EOFichierImport lastImport(EOEditingContext editingContext,String nomFichier) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("nomFichier = %@", new NSArray(nomFichier));
    	EOFetchSpecification fs = new EOFetchSpecification("FichierImport",qualifier, new NSArray(EOSortOrdering.sortOrderingWithKey("dModification",EOSortOrdering.CompareDescending)));
    	fs.setFetchLimit(1);
    	fs.setRefreshesRefetchedObjects(true);
    	try {
    		return (EOFichierImport)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
    
}
