// EOLogImport.java
// Created on Wed Jul 25 18:29:01 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.importer;

import org.cocktail.common.LogManager;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.InterfaceRecordGenerique;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuOuStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// 20/07/2011 - Ajout du eimpSource et vérification que le nom de la relation pour le log ne peut pas être nulle
public class EOLogImport extends _EOLogImport {
	private final static String TYPE_ERREUR = "E";
	private final static String TYPE_TRONQUE = "T";
	private final static String LOG_VALIDE = "O";
	private final static String LOG_SUPPRIME = "A";
	private final static String LOG_TERMINE = "N";
	public final static String RECORD_CORRIGE = "C";

	public EOLogImport() {
		super();
	}

	/**
	 * initialise un record de log
	 * 
	 * @param nomEntite
	 *            nom de l'entite provoquant le log
	 * @param cleMessage
	 *            cle du message d'erreur a ajouter
	 */
	public void initAvecEditingContext(EOEditingContext editingContext, String nomEntite, String cleMessage) {
		NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
		setTemValide(CocktailConstantes.VRAI);
		setLogEntite(nomEntite);
		EOFichierImport importCourant = (EOFichierImport) Finder.importCourant(editingContext);
		if (importCourant != null) {
			setFichierImportRelationship(importCourant);
		}
		if (cleMessage != null) {
			EOMessageErreur message = EOMessageErreur.messagePourCle(editingContext, cleMessage);
			if (message != null) {
				setMessageErreurRelationship(message);
			}
		}
	}

	public void ajouterRelation(ObjetImport record) {
		// addObjectToBothSidesOfRelationshipWithKey(record,ObjetImport.nomRelationAPartirNomEntite(record.entityName()));
		if (record.nomRelationPourLog() != null) {
			addObjectToBothSidesOfRelationshipWithKey(record, record.nomRelationPourLog());
		}
		if (record instanceof ObjetImportPourIndividu) {
			ObjetImportPourIndividu objetPourIndividu = (ObjetImportPourIndividu) record;
			if (objetPourIndividu.idSource() != null) {
				setIdSource(new Integer(objetPourIndividu.idSource().intValue()));
				if (objetPourIndividu.individu() != null) {
					setIndividuRelationship(objetPourIndividu.individu());
				}
			}
		}
		if (record instanceof ObjetImportPourIndividuOuStructure) {
			ObjetImportPourIndividuOuStructure objet = (ObjetImportPourIndividuOuStructure) record;
			setStrSource(objet.strSource());
			if (objet.structure() != null) {
				setStructureRelationship(objet.structure());
			}
		}
		if (record instanceof ObjetImportPourCarriere) {
			ObjetImportPourCarriere objetPourCarriere = (ObjetImportPourCarriere) record;
			if (objetPourCarriere.toCarriere() != null) {
				setCarriereRelationship(objetPourCarriere.toCarriere());
			}
		}
	}

	public void invalider() {
		setTemValide(LOG_SUPPRIME);
	}

	public void supprimerRelations() {
		LogManager.logDetail("Début de la méthode supprimerRelations");
		setIndividuRelationship(null);
		setPersonnelRelationship(null);
		setEnfantRelationship(null);
		setStructureRelationship(null);
		setAdresseRelationship(null);
		setTelephoneRelationship(null);
		setRibRelationship(null);
		setAffectationRelationship(null);
		setCarriereRelationship(null);
		setStageRelationship(null);
		setVacatairesRelationship(null);
		setElementCarriereRelationship(null);
		setPasseRelationship(null);
		setProlongationActiviteRelationship(null);
		setReculAgeRelationship(null);
		setTempsPartielTherapRelationship(null);
		setChangementPositionRelationship(null);
		setContratAvenantRelationship(null);
		setContratRelationship(null);
		setIndividuFamilialeRelationship(null);
		setCompteRelationship(null);
		setEmploiRelationship(null);
		setOccupationRelationship(null);
		setFichierImportRelationship(null);
		setMessageErreurRelationship(null);
		LogManager.logDetail("Fin de la méthode supprimerRelations");
	}

	// Méthodes statiques
	/** Retourne les logs qui ne sont pas annules */
	public static NSArray rechercherLogsValidesPourImport(EOEditingContext editingContext, EOFichierImport fichierImport) {
		NSMutableArray args = new NSMutableArray(fichierImport);
		args.addObject(LOG_SUPPRIME);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(FICHIER_IMPORT_KEY + " = %@ AND temValide <> %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	/** Retourne le nombre de logs d'erreurs pour un import donnes */
	public static int nbLogsErreursPourImport(EOEditingContext editingContext, EOFichierImport fichierImport) {
		NSMutableArray args = new NSMutableArray(fichierImport);
		args.addObject(TYPE_ERREUR);
		args.addObject(LOG_VALIDE);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(FICHIER_IMPORT_KEY + " = %@ AND messageErreur.mesType = %@ AND temValide = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs).count();
	}

	/** Retourne le nombre de logs de donnees tronquees pour un import donne */
	public static int nbLogsTronquesPourImport(EOEditingContext editingContext, EOFichierImport fichierImport) {
		NSMutableArray args = new NSMutableArray(fichierImport);
		args.addObject(TYPE_TRONQUE);
		args.addObject(LOG_VALIDE);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(FICHIER_IMPORT_KEY + " = %@ AND messageErreur.mesType = %@ AND temValide = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs).count();
	}

	/**
	 * Pour indiquer dans le log que le record a ete transfere dans le SI
	 * Destinataire
	 */
	public static void terminerLogsApresTransfertPourImportEtRecord(EOEditingContext editingContext, EOFichierImport fichierImport, ObjetImport recordImport) {
		NSMutableArray args = new NSMutableArray(fichierImport);
		args.addObject(recordImport.entityName());
		args.addObject(LOG_VALIDE);
		args.addObject(RECORD_CORRIGE);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
				FICHIER_IMPORT_KEY + " = %@ AND logEntite = %@ AND (temValide = %@ OR temValide = %@)", args);
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		String nomRelationPourLog = recordImport.nomRelationPourLog(); // Ajoute
																		// la
																		// relation
																		// sur
																		// le
																		// record
		if (nomRelationPourLog != null && nomRelationPourLog.length() > 0) {
			qualifier = EOQualifier.qualifierWithQualifierFormat(nomRelationPourLog + " = %@", new NSArray(recordImport));
			qualifiers.addObject(qualifier);
		} else {
			qualifier = qualifierPourRecordGenerique(recordImport);
			if (qualifier != null) {
				qualifiers.addObject(qualifier);
			}
		}
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		NSArray logs = editingContext.objectsWithFetchSpecification(fs);
		java.util.Enumeration e = logs.objectEnumerator();
		while (e.hasMoreElements()) {
			EOLogImport log = (EOLogImport) e.nextElement();
			log.setTemValide(LOG_TERMINE);
		}
	}

	/** Invalide les logs associes a un record */
	public static void invaliderLogsPourRecord(EOEditingContext editingContext, ObjetImport recordImport) {
		String nomRelationPourLog = recordImport.nomRelationPourLog(); // Ajoute
																		// la
																		// relation
																		// sur
																		// le
																		// record
		if (nomRelationPourLog != null && nomRelationPourLog.length() > 0) {
			NSMutableArray args = new NSMutableArray(recordImport);
			args.addObject(LOG_SUPPRIME);
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomRelationPourLog + " = %@ AND temValide <> %@", args);
			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
			NSArray logs = editingContext.objectsWithFetchSpecification(fs);
			java.util.Enumeration e = logs.objectEnumerator();
			while (e.hasMoreElements()) {
				EOLogImport log = (EOLogImport) e.nextElement();
				log.setTemValide(LOG_SUPPRIME);
			}
		} else {
			EOLogImport log = logPourRecordGenerique(editingContext, recordImport);
			if (log != null) {
				log.setTemValide(LOG_SUPPRIME);
			}
		}
	}

	/** Invalide tous les logs pour un import donne */
	public static void invaliderLogsPourImport(EOEditingContext editingContext, EOFichierImport fichierImport) {
		NSMutableArray args = new NSMutableArray(fichierImport);
		args.addObject(LOG_SUPPRIME);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(FICHIER_IMPORT_KEY + " = %@ AND temValide <> %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		NSArray logs = editingContext.objectsWithFetchSpecification(fs);
		java.util.Enumeration e = logs.objectEnumerator();
		while (e.hasMoreElements()) {
			EOLogImport log = (EOLogImport) e.nextElement();
			log.setTemValide(LOG_SUPPRIME);
		}
	}

	// Méthodes statiques privées
	private static EOLogImport logPourRecordGenerique(EOEditingContext editingContext, ObjetImport recordImport) {
		LogManager.logDetail("Début de la méthode logPourRecordGenerique");
		// Vérifier si il s'agit d'une entité générique et déterminer si on peut
		// la retrouver via les attributs de comparaison
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = qualifierPourRecordGenerique(recordImport);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
			// Rechercher les logs valides ou corrigés qui ont les attributs de
			// comparaison identiques et qui concernent l'entité
			NSMutableArray args = new NSMutableArray();
			args.addObject(recordImport.entityName());
			args.addObject(LOG_VALIDE);
			args.addObject(RECORD_CORRIGE);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("logEntite = %@ AND (temValide = %@ OR temValide = %@)", args));
			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
			try {
				LogManager.logDetail("Fin normale de la méthode logPourRecordGenerique");
				return (EOLogImport) editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
			} catch (Exception exc) {
				LogManager.logDetail("Fin de la méthode logPourRecordGenerique par exception du try-catch");
				return null;
			}
		} else {
			LogManager.logDetail("Fin de la méthode logPourRecordGenerique car aucun qualifier pour fetcher");
			return null;
		}
	}

	private static EOQualifier qualifierPourRecordGenerique(ObjetImport recordImport) {
		LogManager.logDetail("Début de la méthode qualifierPourRecordGenerique");
		if (AutomateImport.sharedInstance().entitesGeneriques().containsObject(recordImport.entityName())) {
			LogManager.logDetail("Log pour entite generique");
			// Récupérer l'entité et ses attributs de la base d'import
			// Les attributs de comparaison doivent être nécessairement dans le
			// log
			Entite entite = AutomateImport.sharedInstance().entiteImportPourNom(recordImport.entityName());
			NSMutableArray qualifiers = new NSMutableArray();
			java.util.Enumeration e = entite.attributs().objectEnumerator();
			while (e.hasMoreElements()) {
				Attribut attribut = (Attribut) e.nextElement();
				String nomAttribut = attribut.nomDestination();
				if (attribut.estAttributComparaison()) {
					String nomRelation = ((InterfaceRecordGenerique) recordImport).nomRelationPourAttributDansLog(nomAttribut);
					boolean estRelation = nomRelation != null && nomRelation.length() > 0;
					if (estRelation) {
						nomAttribut = nomRelation + "." + nomAttribut;
					}
					Object valeur = recordImport.valueForKey(attribut.nomDestination());
					if (valeur != null) {
						qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(nomAttribut + " = %@", new NSArray(valeur)));
					}
				}
			}
			LogManager.logDetail("Fin de la méthode qualifierPourRecordGenerique");
			return new EOAndQualifier(qualifiers);
		} else {
			return null;
		}
	}
}
