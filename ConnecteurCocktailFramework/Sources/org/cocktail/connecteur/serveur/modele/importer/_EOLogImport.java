// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOLogImport.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLogImport extends  EOGenericRecord {
	public static final String ENTITY_NAME = "LogImport";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EIMP2_SOURCE_KEY = "eimp2Source";
	public static final String EIMP_SOURCE_KEY = "eimpSource";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LOG_COMMENTAIRE_KEY = "logCommentaire";
	public static final String LOG_ENTITE_KEY = "logEntite";
	public static final String OB_SOURCE_KEY = "obSource";
	public static final String STR_SOURCE_KEY = "strSource";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String AFFECTATION_KEY = "affectation";
	public static final String BONIFICATIONS_KEY = "bonifications";
	public static final String CARRIERE_KEY = "carriere";
	public static final String CARRIERE_SPECIALISATIONS_KEY = "carriereSpecialisations";
	public static final String CFA_KEY = "cfa";
	public static final String CGNT_ACCIDENT_TRAV_KEY = "cgntAccidentTrav";
	public static final String CGNT_CGM_KEY = "cgntCgm";
	public static final String CGNT_MALADIE_KEY = "cgntMaladie";
	public static final String CGNT_RAISON_FAM_PERSO_KEY = "cgntRaisonFamPerso";
	public static final String CHANGEMENT_POSITION_KEY = "changementPosition";
	public static final String CLD_KEY = "cld";
	public static final String CLD_DETAIL_KEY = "cldDetail";
	public static final String CLM_KEY = "clm";
	public static final String COMPTE_KEY = "compte";
	public static final String CONGE_ACCIDENT_SERV_KEY = "congeAccidentServ";
	public static final String CONGE_ADOPTION_KEY = "congeAdoption";
	public static final String CONGE_AL3_KEY = "congeAl3";
	public static final String CONGE_AL4_KEY = "congeAl4";
	public static final String CONGE_AL5_KEY = "congeAl5";
	public static final String CONGE_AL6_KEY = "congeAl6";
	public static final String CONGE_BONIFIE_KEY = "congeBonifie";
	public static final String CONGE_FORMATION_KEY = "congeFormation";
	public static final String CONGE_MALADIE_KEY = "congeMaladie";
	public static final String CONGE_MALADIE_DETAIL_KEY = "congeMaladieDetail";
	public static final String CONGE_MALADIE_SS_TRT_KEY = "congeMaladieSsTrt";
	public static final String CONGE_MATERNITE_KEY = "congeMaternite";
	public static final String CONGE_PATERNITE_KEY = "congePaternite";
	public static final String CONGE_SOLIDARITE_FAMILIALE_KEY = "congeSolidariteFamiliale";
	public static final String CONGE_SS_TRAITEMENT_KEY = "congeSsTraitement";
	public static final String CONTRAT_KEY = "contrat";
	public static final String CONTRAT_AVENANT_KEY = "contratAvenant";
	public static final String CPA_KEY = "cpa";
	public static final String CRCT_KEY = "crct";
	public static final String CRCT_DETAIL_KEY = "crctDetail";
	public static final String DECHARGE_KEY = "decharge";
	public static final String DECLARATION_ACCIDENT_KEY = "declarationAccident";
	public static final String DELEGATION_KEY = "delegation";
	public static final String DEPART_KEY = "depart";
	public static final String ELEMENT_CARRIERE_KEY = "elementCarriere";
	public static final String EMPLOI_KEY = "emploi";
	public static final String EMPLOI_LOCALISATION_KEY = "emploiLocalisation";
	public static final String ENFANT_KEY = "enfant";
	public static final String FICHIER_IMPORT_KEY = "fichierImport";
	public static final String HEBERGE_KEY = "heberge";
	public static final String INDIVIDU_KEY = "individu";
	public static final String INDIVIDU_AUTRE_FONCTION_KEY = "individuAutreFonction";
	public static final String INDIVIDU_DIPLOMES_KEY = "individuDiplomes";
	public static final String INDIVIDU_DISTINCTIONS_KEY = "individuDistinctions";
	public static final String INDIVIDU_FAMILIALE_KEY = "individuFamiliale";
	public static final String INDIVIDU_FONCTION_INSTANCE_KEY = "individuFonctionInstance";
	public static final String INDIVIDU_FONCTION_STRUCTURELLE_KEY = "individuFonctionStructurelle";
	public static final String INSTANCE_KEY = "instance";
	public static final String MAD_KEY = "mad";
	public static final String MESSAGE_ERREUR_KEY = "messageErreur";
	public static final String OCCUPATION_KEY = "occupation";
	public static final String ORGA_BUDGET_KEY = "orgaBudget";
	public static final String PASSE_KEY = "passe";
	public static final String PERIODES_MILITAIRES_KEY = "periodesMilitaires";
	public static final String PERS_BUDGET_KEY = "persBudget";
	public static final String PERS_BUDGET_ACTION_KEY = "persBudgetAction";
	public static final String PERSONNEL_KEY = "personnel";
	public static final String PROLONGATION_ACTIVITE_KEY = "prolongationActivite";
	public static final String RECUL_AGE_KEY = "reculAge";
	public static final String RELIQUATS_ANCIENNETE_KEY = "reliquatsAnciennete";
	public static final String REPART_ASSOCIATION_KEY = "repartAssociation";
	public static final String REPRISE_TEMPS_PLEIN_KEY = "repriseTempsPlein";
	public static final String RIB_KEY = "rib";
	public static final String SITUATION_KEY = "situation";
	public static final String STAGE_KEY = "stage";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TELEPHONE_KEY = "telephone";
	public static final String TEMPS_PARTIEL_KEY = "tempsPartiel";
	public static final String TEMPS_PARTIEL_THERAP_KEY = "tempsPartielTherap";
	public static final String VACATAIRE_AFFECTATION_KEY = "vacataireAffectation";
	public static final String VACATAIRES_KEY = "vacataires";

  private static Logger LOG = Logger.getLogger(_EOLogImport.class);

  public EOLogImport localInstanceIn(EOEditingContext editingContext) {
    EOLogImport localInstance = (EOLogImport)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer eimp2Source() {
    return (Integer) storedValueForKey("eimp2Source");
  }

  public void setEimp2Source(Integer value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating eimp2Source from " + eimp2Source() + " to " + value);
    }
    takeStoredValueForKey(value, "eimp2Source");
  }

  public Integer eimpSource() {
    return (Integer) storedValueForKey("eimpSource");
  }

  public void setEimpSource(Integer value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating eimpSource from " + eimpSource() + " to " + value);
    }
    takeStoredValueForKey(value, "eimpSource");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String logCommentaire() {
    return (String) storedValueForKey("logCommentaire");
  }

  public void setLogCommentaire(String value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating logCommentaire from " + logCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "logCommentaire");
  }

  public String logEntite() {
    return (String) storedValueForKey("logEntite");
  }

  public void setLogEntite(String value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating logEntite from " + logEntite() + " to " + value);
    }
    takeStoredValueForKey(value, "logEntite");
  }

  public Integer obSource() {
    return (Integer) storedValueForKey("obSource");
  }

  public void setObSource(Integer value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating obSource from " + obSource() + " to " + value);
    }
    takeStoredValueForKey(value, "obSource");
  }

  public String strSource() {
    return (String) storedValueForKey("strSource");
  }

  public void setStrSource(String value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating strSource from " + strSource() + " to " + value);
    }
    takeStoredValueForKey(value, "strSource");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
    	_EOLogImport.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse adresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse)storedValueForKey("adresse");
  }

  public void setAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating adresse from " + adresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "adresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "adresse");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation affectation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation)storedValueForKey("affectation");
  }

  public void setAffectationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating affectation from " + affectation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation oldValue = affectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "affectation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "affectation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications bonifications() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications)storedValueForKey("bonifications");
  }

  public void setBonificationsRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating bonifications from " + bonifications() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications oldValue = bonifications();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "bonifications");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "bonifications");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere carriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("carriere");
  }

  public void setCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating carriere from " + carriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = carriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "carriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "carriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations carriereSpecialisations() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations)storedValueForKey("carriereSpecialisations");
  }

  public void setCarriereSpecialisationsRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating carriereSpecialisations from " + carriereSpecialisations() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations oldValue = carriereSpecialisations();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "carriereSpecialisations");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "carriereSpecialisations");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCfa cfa() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCfa)storedValueForKey("cfa");
  }

  public void setCfaRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCfa value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cfa from " + cfa() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCfa oldValue = cfa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cfa");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cfa");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav cgntAccidentTrav() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav)storedValueForKey("cgntAccidentTrav");
  }

  public void setCgntAccidentTravRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cgntAccidentTrav from " + cgntAccidentTrav() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav oldValue = cgntAccidentTrav();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntAccidentTrav");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntAccidentTrav");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm cgntCgm() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm)storedValueForKey("cgntCgm");
  }

  public void setCgntCgmRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cgntCgm from " + cgntCgm() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm oldValue = cgntCgm();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntCgm");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntCgm");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie cgntMaladie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie)storedValueForKey("cgntMaladie");
  }

  public void setCgntMaladieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cgntMaladie from " + cgntMaladie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie oldValue = cgntMaladie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntMaladie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntMaladie");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso cgntRaisonFamPerso() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso)storedValueForKey("cgntRaisonFamPerso");
  }

  public void setCgntRaisonFamPersoRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cgntRaisonFamPerso from " + cgntRaisonFamPerso() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso oldValue = cgntRaisonFamPerso();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntRaisonFamPerso");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntRaisonFamPerso");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition changementPosition() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition)storedValueForKey("changementPosition");
  }

  public void setChangementPositionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating changementPosition from " + changementPosition() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition oldValue = changementPosition();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "changementPosition");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "changementPosition");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCld cld() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCld)storedValueForKey("cld");
  }

  public void setCldRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCld value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cld from " + cld() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCld oldValue = cld();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cld");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cld");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail cldDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail)storedValueForKey("cldDetail");
  }

  public void setCldDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cldDetail from " + cldDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail oldValue = cldDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cldDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cldDetail");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOClm clm() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOClm)storedValueForKey("clm");
  }

  public void setClmRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOClm value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating clm from " + clm() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOClm oldValue = clm();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "clm");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "clm");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCompte compte() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCompte)storedValueForKey("compte");
  }

  public void setCompteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCompte value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating compte from " + compte() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCompte oldValue = compte();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "compte");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "compte");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ congeAccidentServ() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ)storedValueForKey("congeAccidentServ");
  }

  public void setCongeAccidentServRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeAccidentServ from " + congeAccidentServ() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ oldValue = congeAccidentServ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAccidentServ");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAccidentServ");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption congeAdoption() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption)storedValueForKey("congeAdoption");
  }

  public void setCongeAdoptionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeAdoption from " + congeAdoption() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption oldValue = congeAdoption();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAdoption");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAdoption");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 congeAl3() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3)storedValueForKey("congeAl3");
  }

  public void setCongeAl3Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeAl3 from " + congeAl3() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 oldValue = congeAl3();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl3");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl3");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4 congeAl4() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4)storedValueForKey("congeAl4");
  }

  public void setCongeAl4Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4 value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeAl4 from " + congeAl4() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4 oldValue = congeAl4();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl4");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl4");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5 congeAl5() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5)storedValueForKey("congeAl5");
  }

  public void setCongeAl5Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5 value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeAl5 from " + congeAl5() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5 oldValue = congeAl5();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl5");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl5");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6 congeAl6() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6)storedValueForKey("congeAl6");
  }

  public void setCongeAl6Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6 value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeAl6 from " + congeAl6() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6 oldValue = congeAl6();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl6");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl6");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie congeBonifie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie)storedValueForKey("congeBonifie");
  }

  public void setCongeBonifieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeBonifie from " + congeBonifie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie oldValue = congeBonifie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeBonifie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeBonifie");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation congeFormation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation)storedValueForKey("congeFormation");
  }

  public void setCongeFormationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeFormation from " + congeFormation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation oldValue = congeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeFormation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeFormation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie congeMaladie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie)storedValueForKey("congeMaladie");
  }

  public void setCongeMaladieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeMaladie from " + congeMaladie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie oldValue = congeMaladie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladie");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail congeMaladieDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail)storedValueForKey("congeMaladieDetail");
  }

  public void setCongeMaladieDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeMaladieDetail from " + congeMaladieDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail oldValue = congeMaladieDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladieDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladieDetail");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt congeMaladieSsTrt() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt)storedValueForKey("congeMaladieSsTrt");
  }

  public void setCongeMaladieSsTrtRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeMaladieSsTrt from " + congeMaladieSsTrt() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt oldValue = congeMaladieSsTrt();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladieSsTrt");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladieSsTrt");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite congeMaternite() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite)storedValueForKey("congeMaternite");
  }

  public void setCongeMaterniteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeMaternite from " + congeMaternite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite oldValue = congeMaternite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaternite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaternite");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite congePaternite() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite)storedValueForKey("congePaternite");
  }

  public void setCongePaterniteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congePaternite from " + congePaternite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite oldValue = congePaternite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congePaternite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congePaternite");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale congeSolidariteFamiliale() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale)storedValueForKey("congeSolidariteFamiliale");
  }

  public void setCongeSolidariteFamilialeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeSolidariteFamiliale from " + congeSolidariteFamiliale() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale oldValue = congeSolidariteFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeSolidariteFamiliale");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeSolidariteFamiliale");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement congeSsTraitement() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement)storedValueForKey("congeSsTraitement");
  }

  public void setCongeSsTraitementRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating congeSsTraitement from " + congeSsTraitement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement oldValue = congeSsTraitement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeSsTraitement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeSsTraitement");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOContrat contrat() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOContrat value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant contratAvenant() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant)storedValueForKey("contratAvenant");
  }

  public void setContratAvenantRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating contratAvenant from " + contratAvenant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant oldValue = contratAvenant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contratAvenant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contratAvenant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCpa cpa() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCpa)storedValueForKey("cpa");
  }

  public void setCpaRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCpa value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating cpa from " + cpa() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCpa oldValue = cpa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cpa");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cpa");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCrct crct() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCrct)storedValueForKey("crct");
  }

  public void setCrctRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCrct value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating crct from " + crct() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCrct oldValue = crct();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "crct");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "crct");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail crctDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail)storedValueForKey("crctDetail");
  }

  public void setCrctDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating crctDetail from " + crctDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail oldValue = crctDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "crctDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "crctDetail");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EODecharge decharge() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODecharge)storedValueForKey("decharge");
  }

  public void setDechargeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODecharge value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating decharge from " + decharge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODecharge oldValue = decharge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "decharge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "decharge");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident declarationAccident() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident)storedValueForKey("declarationAccident");
  }

  public void setDeclarationAccidentRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating declarationAccident from " + declarationAccident() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident oldValue = declarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "declarationAccident");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "declarationAccident");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EODelegation delegation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODelegation)storedValueForKey("delegation");
  }

  public void setDelegationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODelegation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating delegation from " + delegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODelegation oldValue = delegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "delegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "delegation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EODepart depart() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODepart)storedValueForKey("depart");
  }

  public void setDepartRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODepart value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating depart from " + depart() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODepart oldValue = depart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "depart");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "depart");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere elementCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere)storedValueForKey("elementCarriere");
  }

  public void setElementCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating elementCarriere from " + elementCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere oldValue = elementCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "elementCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "elementCarriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi emploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi)storedValueForKey("emploi");
  }

  public void setEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating emploi from " + emploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi oldValue = emploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation emploiLocalisation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation)storedValueForKey("emploiLocalisation");
  }

  public void setEmploiLocalisationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating emploiLocalisation from " + emploiLocalisation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation oldValue = emploiLocalisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploiLocalisation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploiLocalisation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant enfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("enfant");
  }

  public void setEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating enfant from " + enfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.importer.EOFichierImport fichierImport() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOFichierImport)storedValueForKey("fichierImport");
  }

  public void setFichierImportRelationship(org.cocktail.connecteur.serveur.modele.importer.EOFichierImport value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating fichierImport from " + fichierImport() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOFichierImport oldValue = fichierImport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "fichierImport");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "fichierImport");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge heberge() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge)storedValueForKey("heberge");
  }

  public void setHebergeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating heberge from " + heberge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge oldValue = heberge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "heberge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "heberge");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction individuAutreFonction() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction)storedValueForKey("individuAutreFonction");
  }

  public void setIndividuAutreFonctionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating individuAutreFonction from " + individuAutreFonction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction oldValue = individuAutreFonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuAutreFonction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuAutreFonction");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes individuDiplomes() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes)storedValueForKey("individuDiplomes");
  }

  public void setIndividuDiplomesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating individuDiplomes from " + individuDiplomes() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes oldValue = individuDiplomes();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuDiplomes");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuDiplomes");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions individuDistinctions() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions)storedValueForKey("individuDistinctions");
  }

  public void setIndividuDistinctionsRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating individuDistinctions from " + individuDistinctions() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions oldValue = individuDistinctions();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuDistinctions");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuDistinctions");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale individuFamiliale() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale)storedValueForKey("individuFamiliale");
  }

  public void setIndividuFamilialeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating individuFamiliale from " + individuFamiliale() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale oldValue = individuFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuFamiliale");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuFamiliale");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance individuFonctionInstance() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance)storedValueForKey("individuFonctionInstance");
  }

  public void setIndividuFonctionInstanceRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating individuFonctionInstance from " + individuFonctionInstance() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance oldValue = individuFonctionInstance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuFonctionInstance");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuFonctionInstance");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle individuFonctionStructurelle() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle)storedValueForKey("individuFonctionStructurelle");
  }

  public void setIndividuFonctionStructurelleRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating individuFonctionStructurelle from " + individuFonctionStructurelle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle oldValue = individuFonctionStructurelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuFonctionStructurelle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuFonctionStructurelle");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOInstance instance() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOInstance)storedValueForKey("instance");
  }

  public void setInstanceRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOInstance value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating instance from " + instance() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOInstance oldValue = instance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "instance");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "instance");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOMad mad() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOMad)storedValueForKey("mad");
  }

  public void setMadRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOMad value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating mad from " + mad() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOMad oldValue = mad();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mad");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mad");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.importer.EOMessageErreur messageErreur() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOMessageErreur)storedValueForKey("messageErreur");
  }

  public void setMessageErreurRelationship(org.cocktail.connecteur.serveur.modele.importer.EOMessageErreur value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating messageErreur from " + messageErreur() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOMessageErreur oldValue = messageErreur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "messageErreur");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "messageErreur");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation occupation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation)storedValueForKey("occupation");
  }

  public void setOccupationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating occupation from " + occupation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation oldValue = occupation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "occupation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "occupation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget orgaBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget)storedValueForKey("orgaBudget");
  }

  public void setOrgaBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating orgaBudget from " + orgaBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget oldValue = orgaBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "orgaBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "orgaBudget");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPasse passe() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPasse)storedValueForKey("passe");
  }

  public void setPasseRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPasse value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating passe from " + passe() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPasse oldValue = passe();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "passe");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "passe");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires periodesMilitaires() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires)storedValueForKey("periodesMilitaires");
  }

  public void setPeriodesMilitairesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating periodesMilitaires from " + periodesMilitaires() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires oldValue = periodesMilitaires();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "periodesMilitaires");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "periodesMilitaires");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget persBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget)storedValueForKey("persBudget");
  }

  public void setPersBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating persBudget from " + persBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget oldValue = persBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "persBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "persBudget");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction persBudgetAction() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction)storedValueForKey("persBudgetAction");
  }

  public void setPersBudgetActionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating persBudgetAction from " + persBudgetAction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction oldValue = persBudgetAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "persBudgetAction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "persBudgetAction");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel personnel() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel)storedValueForKey("personnel");
  }

  public void setPersonnelRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating personnel from " + personnel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel oldValue = personnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "personnel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "personnel");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite prolongationActivite() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite)storedValueForKey("prolongationActivite");
  }

  public void setProlongationActiviteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating prolongationActivite from " + prolongationActivite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite oldValue = prolongationActivite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "prolongationActivite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "prolongationActivite");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge reculAge() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge)storedValueForKey("reculAge");
  }

  public void setReculAgeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating reculAge from " + reculAge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge oldValue = reculAge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "reculAge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "reculAge");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete reliquatsAnciennete() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete)storedValueForKey("reliquatsAnciennete");
  }

  public void setReliquatsAncienneteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating reliquatsAnciennete from " + reliquatsAnciennete() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete oldValue = reliquatsAnciennete();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "reliquatsAnciennete");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "reliquatsAnciennete");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation repartAssociation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation)storedValueForKey("repartAssociation");
  }

  public void setRepartAssociationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating repartAssociation from " + repartAssociation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation oldValue = repartAssociation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "repartAssociation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "repartAssociation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein repriseTempsPlein() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein)storedValueForKey("repriseTempsPlein");
  }

  public void setRepriseTempsPleinRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating repriseTempsPlein from " + repriseTempsPlein() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein oldValue = repriseTempsPlein();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "repriseTempsPlein");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "repriseTempsPlein");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EORib rib() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EORib)storedValueForKey("rib");
  }

  public void setRibRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EORib value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating rib from " + rib() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "rib");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "rib");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOSituation situation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOSituation)storedValueForKey("situation");
  }

  public void setSituationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOSituation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating situation from " + situation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOSituation oldValue = situation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "situation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "situation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStage stage() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStage)storedValueForKey("stage");
  }

  public void setStageRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStage value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating stage from " + stage() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStage oldValue = stage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "stage");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "stage");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone telephone() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone)storedValueForKey("telephone");
  }

  public void setTelephoneRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating telephone from " + telephone() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone oldValue = telephone();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "telephone");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "telephone");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel tempsPartiel() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel)storedValueForKey("tempsPartiel");
  }

  public void setTempsPartielRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating tempsPartiel from " + tempsPartiel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel oldValue = tempsPartiel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartiel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartiel");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap tempsPartielTherap() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap)storedValueForKey("tempsPartielTherap");
  }

  public void setTempsPartielTherapRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating tempsPartielTherap from " + tempsPartielTherap() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap oldValue = tempsPartielTherap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartielTherap");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartielTherap");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation vacataireAffectation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation)storedValueForKey("vacataireAffectation");
  }

  public void setVacataireAffectationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating vacataireAffectation from " + vacataireAffectation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation oldValue = vacataireAffectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vacataireAffectation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vacataireAffectation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires vacataires() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires)storedValueForKey("vacataires");
  }

  public void setVacatairesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires value) {
    if (_EOLogImport.LOG.isDebugEnabled()) {
      _EOLogImport.LOG.debug("updating vacataires from " + vacataires() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires oldValue = vacataires();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vacataires");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vacataires");
    }
  }
  

  public static EOLogImport createLogImport(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String logEntite
, String temValide
, org.cocktail.connecteur.serveur.modele.importer.EOFichierImport fichierImport) {
    EOLogImport eo = (EOLogImport) EOUtilities.createAndInsertInstance(editingContext, _EOLogImport.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLogEntite(logEntite);
		eo.setTemValide(temValide);
    eo.setFichierImportRelationship(fichierImport);
    return eo;
  }

  public static NSArray<EOLogImport> fetchAllLogImports(EOEditingContext editingContext) {
    return _EOLogImport.fetchAllLogImports(editingContext, null);
  }

  public static NSArray<EOLogImport> fetchAllLogImports(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLogImport.fetchLogImports(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLogImport> fetchLogImports(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLogImport.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLogImport> eoObjects = (NSArray<EOLogImport>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLogImport fetchLogImport(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLogImport.fetchLogImport(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLogImport fetchLogImport(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLogImport> eoObjects = _EOLogImport.fetchLogImports(editingContext, qualifier, null);
    EOLogImport eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLogImport)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one LogImport that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLogImport fetchRequiredLogImport(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLogImport.fetchRequiredLogImport(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLogImport fetchRequiredLogImport(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLogImport eoObject = _EOLogImport.fetchLogImport(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no LogImport that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLogImport localInstanceIn(EOEditingContext editingContext, EOLogImport eo) {
    EOLogImport localInstance = (eo == null) ? null : (EOLogImport)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
