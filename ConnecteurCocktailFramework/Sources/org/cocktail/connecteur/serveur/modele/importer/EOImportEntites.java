package org.cocktail.connecteur.serveur.modele.importer;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOImportEntites extends _EOImportEntites {

	public static final String ETAT_IMPORTE = "I";
	public static final String ETAT_NON_IMPORTE = "N";

	public static EOSortOrdering SORT_PRIORITE = new EOSortOrdering(IMPE_PRIORITE_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_PRIORITE = new NSArray(SORT_PRIORITE);

	private static Logger log = Logger.getLogger(EOImportEntites.class);

	public static NSArray<EOImportEntites> find(EOEditingContext edc) {

		return fetchAll(edc, null, SORT_ARRAY_PRIORITE);

	}

	public boolean estPhaseImport() {
		return impeImport().equals("O");
	}

	public boolean estPhaseTransfert() {
		return impeTransfert().equals("O");
	}

	public boolean estPhaseTransfertPlsql() {
		return impeTransfertPlsql() != null && impeTransfertPlsql().equals("O");
	}

	/**
	 * 
	 * @return
	 */
	public boolean estImportee() {
		return impeEtat().equals(ETAT_IMPORTE);
	}

	public void setEstImportee(boolean value) {
		if (value)
			setImpeEtat(ETAT_IMPORTE);
		else
			setImpeEtat(ETAT_NON_IMPORTE);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		setDModification(new NSTimestamp());
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		if (prefetchs != null)
			fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
		NSArray eoObjects = (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

}
