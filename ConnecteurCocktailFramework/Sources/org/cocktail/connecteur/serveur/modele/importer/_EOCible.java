// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCible.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCible extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Cible";

	// Attributes
	public static final String CIB_LIBELLE_KEY = "cibLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOCible.class);

  public EOCible localInstanceIn(EOEditingContext editingContext) {
    EOCible localInstance = (EOCible)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cibLibelle() {
    return (String) storedValueForKey("cibLibelle");
  }

  public void setCibLibelle(String value) {
    if (_EOCible.LOG.isDebugEnabled()) {
    	_EOCible.LOG.debug( "updating cibLibelle from " + cibLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "cibLibelle");
  }


  public static EOCible createCible(EOEditingContext editingContext, String cibLibelle
) {
    EOCible eo = (EOCible) EOUtilities.createAndInsertInstance(editingContext, _EOCible.ENTITY_NAME);    
		eo.setCibLibelle(cibLibelle);
    return eo;
  }

  public static NSArray<EOCible> fetchAllCibles(EOEditingContext editingContext) {
    return _EOCible.fetchAllCibles(editingContext, null);
  }

  public static NSArray<EOCible> fetchAllCibles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCible.fetchCibles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCible> fetchCibles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCible.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCible> eoObjects = (NSArray<EOCible>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCible fetchCible(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCible.fetchCible(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCible fetchCible(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCible> eoObjects = _EOCible.fetchCibles(editingContext, qualifier, null);
    EOCible eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCible)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Cible that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCible fetchRequiredCible(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCible.fetchRequiredCible(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCible fetchRequiredCible(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCible eoObject = _EOCible.fetchCible(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Cible that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCible localInstanceIn(EOEditingContext editingContext, EOCible eo) {
    EOCible localInstance = (eo == null) ? null : (EOCible)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
