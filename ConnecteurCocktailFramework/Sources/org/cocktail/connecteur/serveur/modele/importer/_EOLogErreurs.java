// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOLogErreurs.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLogErreurs extends  EOGenericRecord {
	public static final String ENTITY_NAME = "LogErreurs";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SOURCE_KEY = "idSource";
	public static final String LOG_ENTITE_KEY = "logEntite";
	public static final String LOG_MESSAGE_KEY = "logMessage";
	public static final String NO_LIGNE_KEY = "noLigne";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOLogErreurs.class);

  public EOLogErreurs localInstanceIn(EOEditingContext editingContext) {
    EOLogErreurs localInstance = (EOLogErreurs)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOLogErreurs.LOG.isDebugEnabled()) {
    	_EOLogErreurs.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOLogErreurs.LOG.isDebugEnabled()) {
    	_EOLogErreurs.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idSource() {
    return (Integer) storedValueForKey("idSource");
  }

  public void setIdSource(Integer value) {
    if (_EOLogErreurs.LOG.isDebugEnabled()) {
    	_EOLogErreurs.LOG.debug( "updating idSource from " + idSource() + " to " + value);
    }
    takeStoredValueForKey(value, "idSource");
  }

  public String logEntite() {
    return (String) storedValueForKey("logEntite");
  }

  public void setLogEntite(String value) {
    if (_EOLogErreurs.LOG.isDebugEnabled()) {
    	_EOLogErreurs.LOG.debug( "updating logEntite from " + logEntite() + " to " + value);
    }
    takeStoredValueForKey(value, "logEntite");
  }

  public String logMessage() {
    return (String) storedValueForKey("logMessage");
  }

  public void setLogMessage(String value) {
    if (_EOLogErreurs.LOG.isDebugEnabled()) {
    	_EOLogErreurs.LOG.debug( "updating logMessage from " + logMessage() + " to " + value);
    }
    takeStoredValueForKey(value, "logMessage");
  }

  public Integer noLigne() {
    return (Integer) storedValueForKey("noLigne");
  }

  public void setNoLigne(Integer value) {
    if (_EOLogErreurs.LOG.isDebugEnabled()) {
    	_EOLogErreurs.LOG.debug( "updating noLigne from " + noLigne() + " to " + value);
    }
    takeStoredValueForKey(value, "noLigne");
  }


  public static EOLogErreurs createLogErreurs(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String logEntite
, String logMessage
) {
    EOLogErreurs eo = (EOLogErreurs) EOUtilities.createAndInsertInstance(editingContext, _EOLogErreurs.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLogEntite(logEntite);
		eo.setLogMessage(logMessage);
    return eo;
  }

  public static NSArray<EOLogErreurs> fetchAllLogErreurses(EOEditingContext editingContext) {
    return _EOLogErreurs.fetchAllLogErreurses(editingContext, null);
  }

  public static NSArray<EOLogErreurs> fetchAllLogErreurses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLogErreurs.fetchLogErreurses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLogErreurs> fetchLogErreurses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLogErreurs.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLogErreurs> eoObjects = (NSArray<EOLogErreurs>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLogErreurs fetchLogErreurs(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLogErreurs.fetchLogErreurs(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLogErreurs fetchLogErreurs(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLogErreurs> eoObjects = _EOLogErreurs.fetchLogErreurses(editingContext, qualifier, null);
    EOLogErreurs eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLogErreurs)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one LogErreurs that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLogErreurs fetchRequiredLogErreurs(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLogErreurs.fetchRequiredLogErreurs(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLogErreurs fetchRequiredLogErreurs(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLogErreurs eoObject = _EOLogErreurs.fetchLogErreurs(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no LogErreurs that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLogErreurs localInstanceIn(EOEditingContext editingContext, EOLogErreurs eo) {
    EOLogErreurs localInstance = (eo == null) ? null : (EOLogErreurs)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
