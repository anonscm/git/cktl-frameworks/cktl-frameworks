// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSource.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSource extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Source";

	// Attributes
	public static final String SRC_LIBELLE_KEY = "srcLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOSource.class);

  public EOSource localInstanceIn(EOEditingContext editingContext) {
    EOSource localInstance = (EOSource)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String srcLibelle() {
    return (String) storedValueForKey("srcLibelle");
  }

  public void setSrcLibelle(String value) {
    if (_EOSource.LOG.isDebugEnabled()) {
    	_EOSource.LOG.debug( "updating srcLibelle from " + srcLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "srcLibelle");
  }


  public static EOSource createSource(EOEditingContext editingContext, String srcLibelle
) {
    EOSource eo = (EOSource) EOUtilities.createAndInsertInstance(editingContext, _EOSource.ENTITY_NAME);    
		eo.setSrcLibelle(srcLibelle);
    return eo;
  }

  public static NSArray<EOSource> fetchAllSources(EOEditingContext editingContext) {
    return _EOSource.fetchAllSources(editingContext, null);
  }

  public static NSArray<EOSource> fetchAllSources(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSource.fetchSources(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSource> fetchSources(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSource.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSource> eoObjects = (NSArray<EOSource>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSource fetchSource(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSource.fetchSource(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSource fetchSource(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSource> eoObjects = _EOSource.fetchSources(editingContext, qualifier, null);
    EOSource eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSource)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Source that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSource fetchRequiredSource(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSource.fetchRequiredSource(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSource fetchRequiredSource(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSource eoObject = _EOSource.fetchSource(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Source that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSource localInstanceIn(EOEditingContext editingContext, EOSource eo) {
    EOSource localInstance = (eo == null) ? null : (EOSource)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
