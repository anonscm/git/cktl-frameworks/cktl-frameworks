// EOImportParametres.java
// Created on Tue Jul 31 15:26:57 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.importer;

import java.util.Date;
import java.util.HashMap;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOImportParametres extends _EOImportParametres {

	public final static String KEY_PATH_IMPORT = "PATH_IMPORT";
	public final static String KEY_PATH_IMPORT_CLIENT = "PATH_IMPORT_CLIENT";
	public final static String KEY_FORMAT_DATE = "FORMAT_DATE";
	public final static String KEY_COMPTE_AUTO = "COMPTE_AUTO";
	public final static String KEY_USE_IMPORT_AUTO = "USE_IMPORT_AUTO";
	public final static String KEY_IMPORT_AUTO_HEURE = "IMPORT_AUTO_HEURE";
	public final static String KEY_IMPORT_AUTO_CLEAN_IMPORT = "IMPORT_AUTO_CLEAN_IMPORT";
	public final static String KEY_IMPORT_AUTO_CLEAN_DEST = "IMPORT_AUTO_CLEAN_DEST";
	public final static String KEY_ID_RESPONSABLE = "ID_USER_DEFAUT";
	public final static String KEY_CONTROLE_MISES_A_JOUR = "VERIFIER_MISES_A_JOUR";
	public final static String KEY_NIVEAU_CACHE = "NIVEAU_CACHE_MEMOIRE";
	public final static String KEY_GENERATION_FICHIER_RESUME = "GENERATION_FICHIER_RESUME";
	public final static String KEY_HOMONYME_ATTRIBUTS_INDIVIDU = "HOMONYME_ATTRIBUTS_INDIVIDU";
	public final static String KEY_HOMONYME_MISE_A_JOUR_AUTO = "HOMONYME_MISE_A_JOUR_AUTO";

	public final static int CACHE_NON_INITIALISE = -1;
	public final static int CACHE_AUCUN = 0;
	public final static int CACHE_NIVEAU_BAS = 1;
	public final static int CACHE_NIVEAU_MOYEN = 2;
	public final static int CACHE_NIVEAU_HAUT = 3;
	public final static int CACHE_NIVEAU_TRES_HAUT = 4;
	public final static int CACHE_PAR_DEFAUT = CACHE_NIVEAU_TRES_HAUT;

	public final static boolean GENERATION_FICHIER_RESUME_PAR_DEFAUT = true;
	public final static boolean HOMONYME_MISE_A_JOUR_AUTO_PAR_DEFAUT = false;

	private static HashMap<String, String> parametreCache = new HashMap<String, String>();
	private static int niveauCache = CACHE_NON_INITIALISE;

	public EOImportParametres() {
		super();
	}

	/**
	 * 
	 * @param editingContext
	 * @param cle
	 * @return
	 */
	public static String valeurParametrePourCle(EOEditingContext edc, String cle) {
		if (EOImportParametres.cacheMemoireNiveauBasOuPlus() && parametreCache.containsKey(cle)) {
			return parametreCache.get(cle);
		}

		String resultat = null;
		try {
			EOImportParametres param = fetchImportParametres(edc, PARAM_KEY_KEY, cle);
			resultat = param.paramValue();
		} catch (Exception e) {
			resultat = null;
		}

		if (EOImportParametres.cacheMemoireNiveauBasOuPlus())
			parametreCache.put(cle, resultat);

		return resultat;
	}
	
	public static String valeurParametrePourCle(String cle) {
		return valeurParametrePourCle(new EOEditingContext(),cle);
	}

	/**
	 * 
	 * @return
	 */
	public static String formatDate() {
		String valeur = valeurParametrePourCle(new EOEditingContext(), "FORMAT_DATE");
		if (valeur == null) {
			return null;
		} else {
			return preparerFormatDate(valeur);
		}
	}

	public static String pathImport() {
		return EOImportParametres.valeurParametrePourCle(new EOEditingContext(), "PATH_IMPORT");
	}

	// Méthodes privées
	// Convertit une string en une string pour un NSTimestampFormatter
	private static String preparerFormatDate(String dateFormat) {
		String format = new String(dateFormat);
		if (dateFormat.indexOf("jj") >= 0) {
			format = format.replaceAll("jj", "%d");
		} else {
			return null;
		}
		if (dateFormat.indexOf("mm") >= 0) {
			format = format.replaceAll("mm", "%m");
		} else {
			return null;
		}
		if (dateFormat.indexOf("aaaa") >= 0) {
			format = format.replaceAll("aaaa", "%Y");
		} else if (dateFormat.indexOf("aa") >= 0) {
			format = format.replaceAll("aa", "%y");
		} else {
			return null;
		}
		if (dateFormat.indexOf("HH") >= 0) { // Heures
			format = format.replaceAll("HH", "%H");
		} else if (dateFormat.indexOf("H") >= 0) {
			format = format.replaceAll("H", "%H");
		}
		if (dateFormat.indexOf("MM") >= 0) { // Minutes
			format = format.replaceAll("MM", "%M");
		} else if (dateFormat.indexOf("M") >= 0) {
			format = format.replaceAll("M", "%M");
		}
		if (dateFormat.indexOf("SS") >= 0) { // Secondes
			format = format.replaceAll("SS", "%S");
		} else if (dateFormat.indexOf("S") >= 0) {
			format = format.replaceAll("S", "%S");
		}
		String date = DateCtrl.dateToString(new NSTimestamp(), format);
		if (date == null) {
			// format invalide
			return null;
		}
		return format;
	}

	private static void initCacheMemoire() {
		if (niveauCache == CACHE_NON_INITIALISE) {
			try {
				EOImportParametres param = fetchImportParametres(new EOEditingContext(), PARAM_KEY_KEY, KEY_NIVEAU_CACHE);
				if (param == null)
					niveauCache = CACHE_PAR_DEFAUT;
				else
					niveauCache = Integer.parseInt(param.paramValue());
			} catch (Exception e) {
				niveauCache = CACHE_PAR_DEFAUT;
			}
			LogManager.logInformation((new Date()) + ",   - Niveau de cache mémoire=" + niveauCache);
		}
	}

	public static boolean cacheMemoireNiveauBasOuPlus() {
		initCacheMemoire();
		return niveauCache >= CACHE_NIVEAU_BAS;
	}

	public static boolean cacheMemoireNiveauMoyenOuPlus() {
		initCacheMemoire();
		return niveauCache >= CACHE_NIVEAU_MOYEN;
	}

	public static boolean cacheMemoireNiveauHautOuPlus() {
		initCacheMemoire();
		return niveauCache >= CACHE_NIVEAU_HAUT;
	}

	public static boolean cacheMemoireNiveauTresHautOuPlus() {
		initCacheMemoire();
		return niveauCache >= CACHE_NIVEAU_TRES_HAUT;
	}
	
	public static void resetCache() {
		parametreCache.clear();
	}
	
	public static boolean generationFichierResumeActivee() {
		String generationString=valeurParametrePourCle(KEY_GENERATION_FICHIER_RESUME);
		if (generationString==null)
			return GENERATION_FICHIER_RESUME_PAR_DEFAUT;
		
		return generationString.equals(CocktailConstantes.VRAI);
	}

	public static String attributsHomonymeIndividu() {
		return valeurParametrePourCle(KEY_HOMONYME_ATTRIBUTS_INDIVIDU);
	}
	
	public static boolean homonymeMiseAJourAuto() {
		String val=valeurParametrePourCle(KEY_HOMONYME_MISE_A_JOUR_AUTO);
		if (val==null)
			return HOMONYME_MISE_A_JOUR_AUTO_PAR_DEFAUT;
		
		return val.equals(CocktailConstantes.VRAI);
	}
}
