/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.importer;

import org.cocktail.connecteur.serveur.modele.entite_import.EOStructure;

import com.webobjects.foundation.NSComparator;

/** Comparateur pour ranger les structures selon le type de structure: <BR>
 * L'ordre est le suivant : A (autre), E (etablissement), ES (etablissement secondaire), CS (composante statutaire), C composante, 
 * @author christine
 *
 */
public class StructureComparator extends NSComparator {
	public StructureComparator() {
		super();
	}
	public int compare(Object arg0, Object arg1) throws ComparisonException {
		if (arg0 instanceof EOStructure && arg1 instanceof EOStructure) {
			EOStructure structure0 = (EOStructure)arg0, structure1 = (EOStructure)arg1;
			if (structure0.strSourcePere() == null || structure0.strSourcePere().equals(structure0.strSource())) {	// structure racine
				if (structure1.strSourcePere() == null || structure1.strSourcePere().equals(structure1.strSource())) { // structure racine
					return NSComparator.OrderedSame;
				} else {
					return NSComparator.OrderedAscending;		// Structure racine en tête
				}
			} else {	// structure père non nulle
				if (structure1.strSourcePere() == null || structure1.strSourcePere().equals(structure1.strSource())) {
					return NSComparator.OrderedDescending;		// Structure racine en tête
				} else {
					return NSComparator.OrderedSame;
				}
			}
		} else {	// Pas de comparaison
			return NSComparator.OrderedSame;
		}
		/*if (arg0 instanceof EOStructure && arg1 instanceof EOStructure) {
			EOStructure structure0 = (EOStructure)arg0, structure1 = (EOStructure)arg1;
			if (structure0.typeStructure().equals(structure1.typeStructure())) {
				return NSComparator.OrderedSame;
			} else if (structure0.typeStructure().equals(EOTypeStructure.AUTRE)) {
				return NSComparator.OrderedDescending;	// autre en tête
			} else if (structure0.typeStructure().equals(EOTypeStructure.ETABLISSEMENT)) {
				if (structure1.typeStructure().equals(EOTypeStructure.AUTRE)) {
					return NSComparator.OrderedAscending;	// autre en tête
				} else {
					return NSComparator.OrderedDescending;	// autres types après
				}
			} else if (structure0.typeStructure().equals(EOTypeStructure.ETABLISSEMENT_SECONDAIRE)) {
				if (structure1.typeStructure().equals(EOTypeStructure.AUTRE) || structure1.typeStructure().equals(EOTypeStructure.ETABLISSEMENT)) {
					return NSComparator.OrderedAscending;	// autre et établissement en tête
				} else {
					return NSComparator.OrderedDescending;	// autres types après
				}
			} else if (structure0.typeStructure().equals(EOTypeStructure.COMPOSANTE_STATUTAIRE)) {
				if (structure1.typeStructure().equals(EOTypeStructure.COMPOSANTE) == false) {
					return NSComparator.OrderedDescending; 	// autre, établissement et établissement secondaire en tête
				} else {
					return NSComparator.OrderedAscending;	// autres types après
				}
			} else if (structure0.typeStructure().equals(EOTypeStructure.COMPOSANTE)) {
				return NSComparator.OrderedAscending;
			} else {	// Pas de comparaison
				return NSComparator.OrderedSame;
			}

		} else {	// Pas de comparaison
			return NSComparator.OrderedSame;
		}*/
	}
}