// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMessageErreur.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMessageErreur extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MessageErreur";

	// Attributes
	public static final String MES_KEY_KEY = "mesKey";
	public static final String MES_PRIORITE_KEY = "mesPriorite";
	public static final String MES_TEXTE_KEY = "mesTexte";
	public static final String MES_TYPE_KEY = "mesType";
	public static final String TEM_CORRECTION_KEY = "temCorrection";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMessageErreur.class);

  public EOMessageErreur localInstanceIn(EOEditingContext editingContext) {
    EOMessageErreur localInstance = (EOMessageErreur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String mesKey() {
    return (String) storedValueForKey("mesKey");
  }

  public void setMesKey(String value) {
    if (_EOMessageErreur.LOG.isDebugEnabled()) {
    	_EOMessageErreur.LOG.debug( "updating mesKey from " + mesKey() + " to " + value);
    }
    takeStoredValueForKey(value, "mesKey");
  }

  public Long mesPriorite() {
    return (Long) storedValueForKey("mesPriorite");
  }

  public void setMesPriorite(Long value) {
    if (_EOMessageErreur.LOG.isDebugEnabled()) {
    	_EOMessageErreur.LOG.debug( "updating mesPriorite from " + mesPriorite() + " to " + value);
    }
    takeStoredValueForKey(value, "mesPriorite");
  }

  public String mesTexte() {
    return (String) storedValueForKey("mesTexte");
  }

  public void setMesTexte(String value) {
    if (_EOMessageErreur.LOG.isDebugEnabled()) {
    	_EOMessageErreur.LOG.debug( "updating mesTexte from " + mesTexte() + " to " + value);
    }
    takeStoredValueForKey(value, "mesTexte");
  }

  public String mesType() {
    return (String) storedValueForKey("mesType");
  }

  public void setMesType(String value) {
    if (_EOMessageErreur.LOG.isDebugEnabled()) {
    	_EOMessageErreur.LOG.debug( "updating mesType from " + mesType() + " to " + value);
    }
    takeStoredValueForKey(value, "mesType");
  }

  public String temCorrection() {
    return (String) storedValueForKey("temCorrection");
  }

  public void setTemCorrection(String value) {
    if (_EOMessageErreur.LOG.isDebugEnabled()) {
    	_EOMessageErreur.LOG.debug( "updating temCorrection from " + temCorrection() + " to " + value);
    }
    takeStoredValueForKey(value, "temCorrection");
  }


  public static EOMessageErreur createMessageErreur(EOEditingContext editingContext, String mesKey
, Long mesPriorite
, String mesTexte
, String mesType
, String temCorrection
) {
    EOMessageErreur eo = (EOMessageErreur) EOUtilities.createAndInsertInstance(editingContext, _EOMessageErreur.ENTITY_NAME);    
		eo.setMesKey(mesKey);
		eo.setMesPriorite(mesPriorite);
		eo.setMesTexte(mesTexte);
		eo.setMesType(mesType);
		eo.setTemCorrection(temCorrection);
    return eo;
  }

  public static NSArray<EOMessageErreur> fetchAllMessageErreurs(EOEditingContext editingContext) {
    return _EOMessageErreur.fetchAllMessageErreurs(editingContext, null);
  }

  public static NSArray<EOMessageErreur> fetchAllMessageErreurs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMessageErreur.fetchMessageErreurs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMessageErreur> fetchMessageErreurs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMessageErreur.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMessageErreur> eoObjects = (NSArray<EOMessageErreur>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMessageErreur fetchMessageErreur(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMessageErreur.fetchMessageErreur(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMessageErreur fetchMessageErreur(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMessageErreur> eoObjects = _EOMessageErreur.fetchMessageErreurs(editingContext, qualifier, null);
    EOMessageErreur eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMessageErreur)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MessageErreur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMessageErreur fetchRequiredMessageErreur(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMessageErreur.fetchRequiredMessageErreur(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMessageErreur fetchRequiredMessageErreur(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMessageErreur eoObject = _EOMessageErreur.fetchMessageErreur(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MessageErreur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMessageErreur localInstanceIn(EOEditingContext editingContext, EOMessageErreur eo) {
    EOMessageErreur localInstance = (eo == null) ? null : (EOMessageErreur)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
