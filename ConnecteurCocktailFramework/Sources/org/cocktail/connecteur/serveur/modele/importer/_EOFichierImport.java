// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOFichierImport.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOFichierImport extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FichierImport";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NOM_FICHIER_KEY = "nomFichier";
	public static final String NOM_RAPPORT_KEY = "nomRapport";
	public static final String TEM_ETAT_KEY = "temEtat";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CIBLE_KEY = "cible";
	public static final String SOURCE_KEY = "source";

  private static Logger LOG = Logger.getLogger(_EOFichierImport.class);

  public EOFichierImport localInstanceIn(EOEditingContext editingContext) {
    EOFichierImport localInstance = (EOFichierImport)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
    	_EOFichierImport.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
    	_EOFichierImport.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String nomFichier() {
    return (String) storedValueForKey("nomFichier");
  }

  public void setNomFichier(String value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
    	_EOFichierImport.LOG.debug( "updating nomFichier from " + nomFichier() + " to " + value);
    }
    takeStoredValueForKey(value, "nomFichier");
  }

  public String nomRapport() {
    return (String) storedValueForKey("nomRapport");
  }

  public void setNomRapport(String value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
    	_EOFichierImport.LOG.debug( "updating nomRapport from " + nomRapport() + " to " + value);
    }
    takeStoredValueForKey(value, "nomRapport");
  }

  public String temEtat() {
    return (String) storedValueForKey("temEtat");
  }

  public void setTemEtat(String value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
    	_EOFichierImport.LOG.debug( "updating temEtat from " + temEtat() + " to " + value);
    }
    takeStoredValueForKey(value, "temEtat");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
    	_EOFichierImport.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.importer.EOCible cible() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOCible)storedValueForKey("cible");
  }

  public void setCibleRelationship(org.cocktail.connecteur.serveur.modele.importer.EOCible value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
      _EOFichierImport.LOG.debug("updating cible from " + cible() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOCible oldValue = cible();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cible");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cible");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.importer.EOSource source() {
    return (org.cocktail.connecteur.serveur.modele.importer.EOSource)storedValueForKey("source");
  }

  public void setSourceRelationship(org.cocktail.connecteur.serveur.modele.importer.EOSource value) {
    if (_EOFichierImport.LOG.isDebugEnabled()) {
      _EOFichierImport.LOG.debug("updating source from " + source() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.importer.EOSource oldValue = source();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "source");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "source");
    }
  }
  

  public static EOFichierImport createFichierImport(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String nomFichier
, String temEtat
, String temValide
, org.cocktail.connecteur.serveur.modele.importer.EOCible cible, org.cocktail.connecteur.serveur.modele.importer.EOSource source) {
    EOFichierImport eo = (EOFichierImport) EOUtilities.createAndInsertInstance(editingContext, _EOFichierImport.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNomFichier(nomFichier);
		eo.setTemEtat(temEtat);
		eo.setTemValide(temValide);
    eo.setCibleRelationship(cible);
    eo.setSourceRelationship(source);
    return eo;
  }

  public static NSArray<EOFichierImport> fetchAllFichierImports(EOEditingContext editingContext) {
    return _EOFichierImport.fetchAllFichierImports(editingContext, null);
  }

  public static NSArray<EOFichierImport> fetchAllFichierImports(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFichierImport.fetchFichierImports(editingContext, null, sortOrderings);
  }

  public static NSArray<EOFichierImport> fetchFichierImports(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFichierImport.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFichierImport> eoObjects = (NSArray<EOFichierImport>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFichierImport fetchFichierImport(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFichierImport.fetchFichierImport(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFichierImport fetchFichierImport(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFichierImport> eoObjects = _EOFichierImport.fetchFichierImports(editingContext, qualifier, null);
    EOFichierImport eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFichierImport)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FichierImport that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFichierImport fetchRequiredFichierImport(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFichierImport.fetchRequiredFichierImport(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFichierImport fetchRequiredFichierImport(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFichierImport eoObject = _EOFichierImport.fetchFichierImport(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FichierImport that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFichierImport localInstanceIn(EOEditingContext editingContext, EOFichierImport eo) {
    EOFichierImport localInstance = (eo == null) ? null : (EOFichierImport)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
