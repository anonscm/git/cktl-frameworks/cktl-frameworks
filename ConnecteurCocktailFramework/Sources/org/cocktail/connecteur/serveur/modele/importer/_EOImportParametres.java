// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOImportParametres.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOImportParametres extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ImportParametres";

	// Attributes
	public static final String PARAM_COMMENTAIRES_KEY = "paramCommentaires";
	public static final String PARAM_KEY_KEY = "paramKey";
	public static final String PARAM_VALUE_KEY = "paramValue";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOImportParametres.class);

  public EOImportParametres localInstanceIn(EOEditingContext editingContext) {
    EOImportParametres localInstance = (EOImportParametres)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String paramCommentaires() {
    return (String) storedValueForKey("paramCommentaires");
  }

  public void setParamCommentaires(String value) {
    if (_EOImportParametres.LOG.isDebugEnabled()) {
    	_EOImportParametres.LOG.debug( "updating paramCommentaires from " + paramCommentaires() + " to " + value);
    }
    takeStoredValueForKey(value, "paramCommentaires");
  }

  public String paramKey() {
    return (String) storedValueForKey("paramKey");
  }

  public void setParamKey(String value) {
    if (_EOImportParametres.LOG.isDebugEnabled()) {
    	_EOImportParametres.LOG.debug( "updating paramKey from " + paramKey() + " to " + value);
    }
    takeStoredValueForKey(value, "paramKey");
  }

  public String paramValue() {
    return (String) storedValueForKey("paramValue");
  }

  public void setParamValue(String value) {
    if (_EOImportParametres.LOG.isDebugEnabled()) {
    	_EOImportParametres.LOG.debug( "updating paramValue from " + paramValue() + " to " + value);
    }
    takeStoredValueForKey(value, "paramValue");
  }


  public static EOImportParametres createImportParametres(EOEditingContext editingContext) {
    EOImportParametres eo = (EOImportParametres) EOUtilities.createAndInsertInstance(editingContext, _EOImportParametres.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOImportParametres> fetchAllImportParametreses(EOEditingContext editingContext) {
    return _EOImportParametres.fetchAllImportParametreses(editingContext, null);
  }

  public static NSArray<EOImportParametres> fetchAllImportParametreses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOImportParametres.fetchImportParametreses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOImportParametres> fetchImportParametreses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOImportParametres.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOImportParametres> eoObjects = (NSArray<EOImportParametres>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOImportParametres fetchImportParametres(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImportParametres.fetchImportParametres(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImportParametres fetchImportParametres(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOImportParametres> eoObjects = _EOImportParametres.fetchImportParametreses(editingContext, qualifier, null);
    EOImportParametres eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOImportParametres)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ImportParametres that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImportParametres fetchRequiredImportParametres(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImportParametres.fetchRequiredImportParametres(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImportParametres fetchRequiredImportParametres(EOEditingContext editingContext, EOQualifier qualifier) {
    EOImportParametres eoObject = _EOImportParametres.fetchImportParametres(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ImportParametres that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImportParametres localInstanceIn(EOEditingContext editingContext, EOImportParametres eo) {
    EOImportParametres localInstance = (eo == null) ? null : (EOImportParametres)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
