// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOImportEntites.java instead.
package org.cocktail.connecteur.serveur.modele.importer;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOImportEntites extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ImportEntites";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String IMPE_DATE_IMPORT_KEY = "impeDateImport";
	public static final String IMPE_DATE_TRANSFERT_KEY = "impeDateTransfert";
	public static final String IMPE_DUREE_IMPORT_KEY = "impeDureeImport";
	public static final String IMPE_DUREE_TRANSFERT_KEY = "impeDureeTransfert";
	public static final String IMPE_ENTITE_KEY = "impeEntite";
	public static final String IMPE_ETAT_KEY = "impeEtat";
	public static final String IMPE_IMPORT_KEY = "impeImport";
	public static final String IMPE_NB_ERREURS_KEY = "impeNbErreurs";
	public static final String IMPE_NB_LOGS_KEY = "impeNbLogs";
	public static final String IMPE_NB_OBJETS_KEY = "impeNbObjets";
	public static final String IMPE_NOM_FICHIER_KEY = "impeNomFichier";
	public static final String IMPE_PRIORITE_KEY = "impePriorite";
	public static final String IMPE_PROC_IMPORT_KEY = "impeProcImport";
	public static final String IMPE_PROC_TRANSFERT_KEY = "impeProcTransfert";
	public static final String IMPE_TRANSFERT_KEY = "impeTransfert";
	public static final String IMPE_TRANSFERT_PLSQL_KEY = "impeTransfertPlsql";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOImportEntites.class);

  public EOImportEntites localInstanceIn(EOEditingContext editingContext) {
    EOImportEntites localInstance = (EOImportEntites)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp impeDateImport() {
    return (NSTimestamp) storedValueForKey("impeDateImport");
  }

  public void setImpeDateImport(NSTimestamp value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeDateImport from " + impeDateImport() + " to " + value);
    }
    takeStoredValueForKey(value, "impeDateImport");
  }

  public NSTimestamp impeDateTransfert() {
    return (NSTimestamp) storedValueForKey("impeDateTransfert");
  }

  public void setImpeDateTransfert(NSTimestamp value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeDateTransfert from " + impeDateTransfert() + " to " + value);
    }
    takeStoredValueForKey(value, "impeDateTransfert");
  }

  public Long impeDureeImport() {
    return (Long) storedValueForKey("impeDureeImport");
  }

  public void setImpeDureeImport(Long value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeDureeImport from " + impeDureeImport() + " to " + value);
    }
    takeStoredValueForKey(value, "impeDureeImport");
  }

  public Long impeDureeTransfert() {
    return (Long) storedValueForKey("impeDureeTransfert");
  }

  public void setImpeDureeTransfert(Long value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeDureeTransfert from " + impeDureeTransfert() + " to " + value);
    }
    takeStoredValueForKey(value, "impeDureeTransfert");
  }

  public String impeEntite() {
    return (String) storedValueForKey("impeEntite");
  }

  public void setImpeEntite(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeEntite from " + impeEntite() + " to " + value);
    }
    takeStoredValueForKey(value, "impeEntite");
  }

  public String impeEtat() {
    return (String) storedValueForKey("impeEtat");
  }

  public void setImpeEtat(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeEtat from " + impeEtat() + " to " + value);
    }
    takeStoredValueForKey(value, "impeEtat");
  }

  public String impeImport() {
    return (String) storedValueForKey("impeImport");
  }

  public void setImpeImport(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeImport from " + impeImport() + " to " + value);
    }
    takeStoredValueForKey(value, "impeImport");
  }

  public Integer impeNbErreurs() {
    return (Integer) storedValueForKey("impeNbErreurs");
  }

  public void setImpeNbErreurs(Integer value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeNbErreurs from " + impeNbErreurs() + " to " + value);
    }
    takeStoredValueForKey(value, "impeNbErreurs");
  }

  public Integer impeNbLogs() {
    return (Integer) storedValueForKey("impeNbLogs");
  }

  public void setImpeNbLogs(Integer value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeNbLogs from " + impeNbLogs() + " to " + value);
    }
    takeStoredValueForKey(value, "impeNbLogs");
  }

  public Integer impeNbObjets() {
    return (Integer) storedValueForKey("impeNbObjets");
  }

  public void setImpeNbObjets(Integer value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeNbObjets from " + impeNbObjets() + " to " + value);
    }
    takeStoredValueForKey(value, "impeNbObjets");
  }

  public String impeNomFichier() {
    return (String) storedValueForKey("impeNomFichier");
  }

  public void setImpeNomFichier(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeNomFichier from " + impeNomFichier() + " to " + value);
    }
    takeStoredValueForKey(value, "impeNomFichier");
  }

  public Integer impePriorite() {
    return (Integer) storedValueForKey("impePriorite");
  }

  public void setImpePriorite(Integer value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impePriorite from " + impePriorite() + " to " + value);
    }
    takeStoredValueForKey(value, "impePriorite");
  }

  public String impeProcImport() {
    return (String) storedValueForKey("impeProcImport");
  }

  public void setImpeProcImport(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeProcImport from " + impeProcImport() + " to " + value);
    }
    takeStoredValueForKey(value, "impeProcImport");
  }

  public String impeProcTransfert() {
    return (String) storedValueForKey("impeProcTransfert");
  }

  public void setImpeProcTransfert(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeProcTransfert from " + impeProcTransfert() + " to " + value);
    }
    takeStoredValueForKey(value, "impeProcTransfert");
  }

  public String impeTransfert() {
    return (String) storedValueForKey("impeTransfert");
  }

  public void setImpeTransfert(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeTransfert from " + impeTransfert() + " to " + value);
    }
    takeStoredValueForKey(value, "impeTransfert");
  }

  public String impeTransfertPlsql() {
    return (String) storedValueForKey("impeTransfertPlsql");
  }

  public void setImpeTransfertPlsql(String value) {
    if (_EOImportEntites.LOG.isDebugEnabled()) {
    	_EOImportEntites.LOG.debug( "updating impeTransfertPlsql from " + impeTransfertPlsql() + " to " + value);
    }
    takeStoredValueForKey(value, "impeTransfertPlsql");
  }


  public static EOImportEntites createImportEntites(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String impeEntite
, String impeEtat
, String impeImport
, Integer impeNbErreurs
, Integer impeNbLogs
, Integer impeNbObjets
, Integer impePriorite
, String impeTransfert
, String impeTransfertPlsql
) {
    EOImportEntites eo = (EOImportEntites) EOUtilities.createAndInsertInstance(editingContext, _EOImportEntites.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setImpeEntite(impeEntite);
		eo.setImpeEtat(impeEtat);
		eo.setImpeImport(impeImport);
		eo.setImpeNbErreurs(impeNbErreurs);
		eo.setImpeNbLogs(impeNbLogs);
		eo.setImpeNbObjets(impeNbObjets);
		eo.setImpePriorite(impePriorite);
		eo.setImpeTransfert(impeTransfert);
		eo.setImpeTransfertPlsql(impeTransfertPlsql);
    return eo;
  }

  public static NSArray<EOImportEntites> fetchAllImportEntiteses(EOEditingContext editingContext) {
    return _EOImportEntites.fetchAllImportEntiteses(editingContext, null);
  }

  public static NSArray<EOImportEntites> fetchAllImportEntiteses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOImportEntites.fetchImportEntiteses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOImportEntites> fetchImportEntiteses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOImportEntites.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOImportEntites> eoObjects = (NSArray<EOImportEntites>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOImportEntites fetchImportEntites(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImportEntites.fetchImportEntites(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImportEntites fetchImportEntites(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOImportEntites> eoObjects = _EOImportEntites.fetchImportEntiteses(editingContext, qualifier, null);
    EOImportEntites eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOImportEntites)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ImportEntites that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImportEntites fetchRequiredImportEntites(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImportEntites.fetchRequiredImportEntites(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImportEntites fetchRequiredImportEntites(EOEditingContext editingContext, EOQualifier qualifier) {
    EOImportEntites eoObject = _EOImportEntites.fetchImportEntites(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ImportEntites that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImportEntites localInstanceIn(EOEditingContext editingContext, EOImportEntites eo) {
    EOImportEntites localInstance = (eo == null) ? null : (EOImportEntites)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
