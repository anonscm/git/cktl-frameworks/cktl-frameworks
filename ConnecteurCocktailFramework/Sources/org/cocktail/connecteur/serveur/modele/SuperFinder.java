//
//  SuperFinder
//  ObjetsUniversite
//
//  Created by Christine Buttin on Fri Mar 11 2005.
//  Copyright (c) 2005 __MyCompanyName__. All rights reserved.
// contient toutes les méthodes de recherche sur des EOGenericRecord ou des méthodes générales
//
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele;
 
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrhumParametres;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuOuStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author christine<BR>
 *
 */
public class SuperFinder {

	/** Evalue la cle primaire pour une entite en cherchant dans la table de sequence pour une base Oracle
	 * ou en la calculant pour les autres bases
	 * @param editingContext
	 * @param nomEntite
	 * @param nomClePrimaire
	 * @param nomEntiteSequenceOracle null si Oracle g&egrave;re la sequence
	 * @param estUnLong true si la cle doit &ecirc;tre retournee comme un Long
	 * @return valeur de la cle primaire
	 * Pour que cette methode ait un sens il faut avoir defini dans la table GRHUM_PARAMETRES, un attribut "BASE_DONNEES" avec
	 * le nom de la base. Par defaut si aucun param&egrave;tre n'ait defini, on consid&egrave;re que la base est ORACLE
	 */
	public static Number clePrimairePour(EOEditingContext editingContext,String nomEntite,String nomClePrimaire,String nomEntiteSequenceOracle,boolean estUnLong) {
		String valeur = EOGrhumParametres.parametrePourCle(editingContext,"BASE_DONNEES");
		if (valeur == null || valeur.equals("ORACLE")) {
				return numeroSequenceOracle(editingContext,nomEntiteSequenceOracle,estUnLong);
		} else {
			return numeroSequenceAutreBase(editingContext,nomEntite,nomClePrimaire,estUnLong);
		}
	}
	/** Recherche le numero de sequence dans une table de sequence Oracle (doit comporter un attribut nextval)
	 * @param editingContext
	 * @param nomEntiteSequenceOracle
	 * @param estUnLong true si la cle doit &ecirc;tre retournee comme un Long
	 * @return valeur trouvee ou null
	 */
	public static Number numeroSequenceOracle(EOEditingContext editingContext,String nomEntiteSequenceOracle,boolean estUnLong) {
		if (nomEntiteSequenceOracle == null || nomEntiteSequenceOracle.equals("")) {
			return null;
		}
		EOFetchSpecification  myFetch = new EOFetchSpecification(nomEntiteSequenceOracle,null,null);
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			Number numero = (Number)((NSArray) result.valueForKey("nextval")).objectAtIndex(0);
			return new Integer(numero.intValue());
		} catch (Exception e) {
			return null;
		}
	}
	/** Calcule la cle primaire pour une base quelconque
	 * @param editingContext
	 * @param nomEntite
	 * @param nomClePrimaire (comme definie dans le mod&egrave;le)
	 * @param estUnLong true si la cle doit &ecirc;tre retournee comme un Long
	 * @return valeur trouvee ou null
	 */
	public static Number numeroSequenceAutreBase(EOEditingContext editingContext,String nomEntite,String nomClePrimaire,boolean estUnLong) {
		NSArray mySort = new NSArray(new EOSortOrdering(nomClePrimaire,EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,null,mySort);
		fs.setFetchLimit(1);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		if (estUnLong) {
			long noInd = 0;
			try {
				noInd = ((Number)((EOGenericRecord)results.objectAtIndex(0)).valueForKey(nomClePrimaire)).longValue();
			} catch (Exception e) {}
			return new Long(noInd + 1);
		} else {
			int noInd = 0;
			try {
				noInd = ((Number)((EOGenericRecord)results.objectAtIndex(0)).valueForKey(nomClePrimaire)).intValue();
			} catch (Exception e) {}
			return new Integer(noInd + 1);
		}
	}
	/** Creation manuelle d'un persId selon le type de base de données
	@param editingContext
	@return num du persID */
	public static Number construirePersId(EOEditingContext editingContext) {
		if (EOGrhumParametres.parametrePourCle(editingContext, "BASE_DONNEES").equals("ORACLE")) {
			return SuperFinder.numeroSequenceOracle(editingContext,"GrhumSeqPersonne",true);
		} else {
			NSArray mySort = new NSArray(new EOSortOrdering("persId",EOSortOrdering.CompareDescending));
			EOFetchSpecification fs = new EOFetchSpecification("GrhumStructure",null,mySort);
			fs.setFetchLimit(1);
			NSArray structures = editingContext.objectsWithFetchSpecification(fs);
			fs = new EOFetchSpecification("GrhumIndividu",null,mySort);
			fs.setFetchLimit(1);
			NSArray individus = editingContext.objectsWithFetchSpecification(fs);
			long persIdStr = 0, persIdInd = 0;
			try {
				persIdStr = ((EOGrhumStructure)structures.objectAtIndex(0)).persId().longValue();
				persIdInd = ((EOGrhumIndividu)individus.objectAtIndex(0)).persId().longValue();
			} catch (Exception e) {}
			long max = (persIdStr < persIdInd) ? persIdInd : persIdStr;
			return (Number)new Long(max + 1);
		}
	}
	public static NSDictionary recordAsDict(EOGenericRecord record) {
		NSMutableDictionary dictValeurs = new NSMutableDictionary();
		java.util.Enumeration e = record.attributeKeys().objectEnumerator();
		while (e.hasMoreElements()) {
			String cle = (String)e.nextElement();
			if (cle.equals("dCreation") == false && cle.equals("dModification") == false) {
				Object value = record.valueForKey(cle);
				if (value != null) {
					dictValeurs.setObjectForKey(value, cle);
				}
			}
		}
		if (record instanceof ObjetImportPourIndividuOuStructure) {
			ObjetImportPourIndividuOuStructure recordIndividuStructure = (ObjetImportPourIndividuOuStructure)record;
			if (recordIndividuStructure.structure() != null) {
				dictValeurs.setObjectForKey(recordIndividuStructure.structure().strSource(), "strSource");
			}
		}
		if (record instanceof ObjetImportPourIndividu) {
			ObjetImportPourIndividu recordIndividu = (ObjetImportPourIndividu)record;
			if (recordIndividu.individu() != null) {
				dictValeurs.setObjectForKey(recordIndividu.individu().idSource(), "idSource");
			}
		}
		if (record instanceof EOIndividu) {
			if (((EOIndividu)record).personnel() != null) {
				dictValeurs.addEntriesFromDictionary(recordAsDict(((EOIndividu)record).personnel()));
			}
		}
		return new NSDictionary(dictValeurs);
	}
}
