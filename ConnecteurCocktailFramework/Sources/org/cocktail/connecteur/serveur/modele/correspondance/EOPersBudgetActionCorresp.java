package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EOPersBudgetActionCorresp extends _EOPersBudgetActionCorresp {
	@Override
	public void supprimerRelations() {
		setToPersBudgetActionRelationship(null);
		setToManguePersBudgetActionRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return TO_PERS_BUDGET_ACTION_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_PERS_BUDGET_ACTION_KEY;
	}
}
