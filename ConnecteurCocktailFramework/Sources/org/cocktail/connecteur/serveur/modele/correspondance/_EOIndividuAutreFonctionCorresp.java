// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuAutreFonctionCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuAutreFonctionCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "IndividuAutreFonctionCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String GRHUM_INDIVIDU_AUTRE_FONCTION_KEY = "grhumIndividuAutreFonction";
	public static final String INDIVIDU_AUTRE_FONCTION_KEY = "individuAutreFonction";

  private static Logger LOG = Logger.getLogger(_EOIndividuAutreFonctionCorresp.class);

  public EOIndividuAutreFonctionCorresp localInstanceIn(EOEditingContext editingContext) {
    EOIndividuAutreFonctionCorresp localInstance = (EOIndividuAutreFonctionCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuAutreFonctionCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonctionCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuAutreFonctionCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuAutreFonctionCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuAutreFonction grhumIndividuAutreFonction() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuAutreFonction)storedValueForKey("grhumIndividuAutreFonction");
  }

  public void setGrhumIndividuAutreFonctionRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuAutreFonction value) {
    if (_EOIndividuAutreFonctionCorresp.LOG.isDebugEnabled()) {
      _EOIndividuAutreFonctionCorresp.LOG.debug("updating grhumIndividuAutreFonction from " + grhumIndividuAutreFonction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuAutreFonction oldValue = grhumIndividuAutreFonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "grhumIndividuAutreFonction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "grhumIndividuAutreFonction");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction individuAutreFonction() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction)storedValueForKey("individuAutreFonction");
  }

  public void setIndividuAutreFonctionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction value) {
    if (_EOIndividuAutreFonctionCorresp.LOG.isDebugEnabled()) {
      _EOIndividuAutreFonctionCorresp.LOG.debug("updating individuAutreFonction from " + individuAutreFonction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction oldValue = individuAutreFonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuAutreFonction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuAutreFonction");
    }
  }
  

  public static EOIndividuAutreFonctionCorresp createIndividuAutreFonctionCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuAutreFonction grhumIndividuAutreFonction, org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuAutreFonction individuAutreFonction) {
    EOIndividuAutreFonctionCorresp eo = (EOIndividuAutreFonctionCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuAutreFonctionCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setGrhumIndividuAutreFonctionRelationship(grhumIndividuAutreFonction);
    eo.setIndividuAutreFonctionRelationship(individuAutreFonction);
    return eo;
  }

  public static NSArray<EOIndividuAutreFonctionCorresp> fetchAllIndividuAutreFonctionCorresps(EOEditingContext editingContext) {
    return _EOIndividuAutreFonctionCorresp.fetchAllIndividuAutreFonctionCorresps(editingContext, null);
  }

  public static NSArray<EOIndividuAutreFonctionCorresp> fetchAllIndividuAutreFonctionCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuAutreFonctionCorresp.fetchIndividuAutreFonctionCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuAutreFonctionCorresp> fetchIndividuAutreFonctionCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuAutreFonctionCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuAutreFonctionCorresp> eoObjects = (NSArray<EOIndividuAutreFonctionCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuAutreFonctionCorresp fetchIndividuAutreFonctionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuAutreFonctionCorresp.fetchIndividuAutreFonctionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuAutreFonctionCorresp fetchIndividuAutreFonctionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuAutreFonctionCorresp> eoObjects = _EOIndividuAutreFonctionCorresp.fetchIndividuAutreFonctionCorresps(editingContext, qualifier, null);
    EOIndividuAutreFonctionCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuAutreFonctionCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuAutreFonctionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuAutreFonctionCorresp fetchRequiredIndividuAutreFonctionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuAutreFonctionCorresp.fetchRequiredIndividuAutreFonctionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuAutreFonctionCorresp fetchRequiredIndividuAutreFonctionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuAutreFonctionCorresp eoObject = _EOIndividuAutreFonctionCorresp.fetchIndividuAutreFonctionCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuAutreFonctionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuAutreFonctionCorresp localInstanceIn(EOEditingContext editingContext, EOIndividuAutreFonctionCorresp eo) {
    EOIndividuAutreFonctionCorresp localInstance = (eo == null) ? null : (EOIndividuAutreFonctionCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
