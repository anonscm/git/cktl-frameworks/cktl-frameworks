// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSituationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSituationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "SituationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String MANGUE_SITUATION_KEY = "mangueSituation";
	public static final String SITUATION_KEY = "situation";

  private static Logger LOG = Logger.getLogger(_EOSituationCorresp.class);

  public EOSituationCorresp localInstanceIn(EOEditingContext editingContext) {
    EOSituationCorresp localInstance = (EOSituationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOSituationCorresp.LOG.isDebugEnabled()) {
    	_EOSituationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOSituationCorresp.LOG.isDebugEnabled()) {
    	_EOSituationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueSituation mangueSituation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueSituation)storedValueForKey("mangueSituation");
  }

  public void setMangueSituationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueSituation value) {
    if (_EOSituationCorresp.LOG.isDebugEnabled()) {
      _EOSituationCorresp.LOG.debug("updating mangueSituation from " + mangueSituation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueSituation oldValue = mangueSituation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueSituation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueSituation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOSituation situation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOSituation)storedValueForKey("situation");
  }

  public void setSituationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOSituation value) {
    if (_EOSituationCorresp.LOG.isDebugEnabled()) {
      _EOSituationCorresp.LOG.debug("updating situation from " + situation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOSituation oldValue = situation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "situation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "situation");
    }
  }
  

  public static EOSituationCorresp createSituationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueSituation mangueSituation, org.cocktail.connecteur.serveur.modele.entite_import.EOSituation situation) {
    EOSituationCorresp eo = (EOSituationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOSituationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setMangueSituationRelationship(mangueSituation);
    eo.setSituationRelationship(situation);
    return eo;
  }

  public static NSArray<EOSituationCorresp> fetchAllSituationCorresps(EOEditingContext editingContext) {
    return _EOSituationCorresp.fetchAllSituationCorresps(editingContext, null);
  }

  public static NSArray<EOSituationCorresp> fetchAllSituationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSituationCorresp.fetchSituationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSituationCorresp> fetchSituationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSituationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSituationCorresp> eoObjects = (NSArray<EOSituationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSituationCorresp fetchSituationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationCorresp.fetchSituationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationCorresp fetchSituationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSituationCorresp> eoObjects = _EOSituationCorresp.fetchSituationCorresps(editingContext, qualifier, null);
    EOSituationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSituationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one SituationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationCorresp fetchRequiredSituationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationCorresp.fetchRequiredSituationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationCorresp fetchRequiredSituationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSituationCorresp eoObject = _EOSituationCorresp.fetchSituationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no SituationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationCorresp localInstanceIn(EOEditingContext editingContext, EOSituationCorresp eo) {
    EOSituationCorresp localInstance = (eo == null) ? null : (EOSituationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
