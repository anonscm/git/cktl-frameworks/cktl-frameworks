package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

import com.webobjects.foundation.NSDictionary;

public class EOCarriereSpecialisationsCorresp extends
		_EOCarriereSpecialisationsCorresp {
	private static Logger log = Logger
			.getLogger(EOCarriereSpecialisationsCorresp.class);

	@Override
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(
				carriereSpecialisations(), "carriereSpecialisations");
		removeObjectFromBothSidesOfRelationshipWithKey(
				carriereSpecialisationsMangue(),
				"carriereSpecialisationsMangue");
	}

	@Override
	public String nomRelationBaseImport() {
		return CARRIERE_SPECIALISATIONS_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return CARRIERE_SPECIALISATIONS_MANGUE_KEY;
	}
}
