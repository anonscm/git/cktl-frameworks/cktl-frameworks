// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCpaCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCpaCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CpaCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CPA_KEY = "cpa";
	public static final String MANGUE_CPA_KEY = "mangueCpa";

  private static Logger LOG = Logger.getLogger(_EOCpaCorresp.class);

  public EOCpaCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCpaCorresp localInstance = (EOCpaCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCpaCorresp.LOG.isDebugEnabled()) {
    	_EOCpaCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCpaCorresp.LOG.isDebugEnabled()) {
    	_EOCpaCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCpa cpa() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCpa)storedValueForKey("cpa");
  }

  public void setCpaRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCpa value) {
    if (_EOCpaCorresp.LOG.isDebugEnabled()) {
      _EOCpaCorresp.LOG.debug("updating cpa from " + cpa() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCpa oldValue = cpa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cpa");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cpa");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCpa mangueCpa() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCpa)storedValueForKey("mangueCpa");
  }

  public void setMangueCpaRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCpa value) {
    if (_EOCpaCorresp.LOG.isDebugEnabled()) {
      _EOCpaCorresp.LOG.debug("updating mangueCpa from " + mangueCpa() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCpa oldValue = mangueCpa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCpa");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCpa");
    }
  }
  

  public static EOCpaCorresp createCpaCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCpa cpa, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCpa mangueCpa) {
    EOCpaCorresp eo = (EOCpaCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCpaCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCpaRelationship(cpa);
    eo.setMangueCpaRelationship(mangueCpa);
    return eo;
  }

  public static NSArray<EOCpaCorresp> fetchAllCpaCorresps(EOEditingContext editingContext) {
    return _EOCpaCorresp.fetchAllCpaCorresps(editingContext, null);
  }

  public static NSArray<EOCpaCorresp> fetchAllCpaCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCpaCorresp.fetchCpaCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCpaCorresp> fetchCpaCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCpaCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCpaCorresp> eoObjects = (NSArray<EOCpaCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCpaCorresp fetchCpaCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCpaCorresp.fetchCpaCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCpaCorresp fetchCpaCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCpaCorresp> eoObjects = _EOCpaCorresp.fetchCpaCorresps(editingContext, qualifier, null);
    EOCpaCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCpaCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CpaCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCpaCorresp fetchRequiredCpaCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCpaCorresp.fetchRequiredCpaCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCpaCorresp fetchRequiredCpaCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCpaCorresp eoObject = _EOCpaCorresp.fetchCpaCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CpaCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCpaCorresp localInstanceIn(EOEditingContext editingContext, EOCpaCorresp eo) {
    EOCpaCorresp localInstance = (eo == null) ? null : (EOCpaCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
