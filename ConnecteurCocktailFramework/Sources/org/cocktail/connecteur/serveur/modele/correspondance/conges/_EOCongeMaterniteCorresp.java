// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaterniteCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaterniteCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongeMaterniteCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_MATERNITE_KEY = "congeMaternite";
	public static final String CONGE_MATERNITE_MANGUE_KEY = "congeMaterniteMangue";

  private static Logger LOG = Logger.getLogger(_EOCongeMaterniteCorresp.class);

  public EOCongeMaterniteCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaterniteCorresp localInstance = (EOCongeMaterniteCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaterniteCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaterniteCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaterniteCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaterniteCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite congeMaternite() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite)storedValueForKey("congeMaternite");
  }

  public void setCongeMaterniteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite value) {
    if (_EOCongeMaterniteCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaterniteCorresp.LOG.debug("updating congeMaternite from " + congeMaternite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite oldValue = congeMaternite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaternite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaternite");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite congeMaterniteMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite)storedValueForKey("congeMaterniteMangue");
  }

  public void setCongeMaterniteMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite value) {
    if (_EOCongeMaterniteCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaterniteCorresp.LOG.debug("updating congeMaterniteMangue from " + congeMaterniteMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite oldValue = congeMaterniteMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaterniteMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaterniteMangue");
    }
  }
  

  public static EOCongeMaterniteCorresp createCongeMaterniteCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite congeMaternite, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite congeMaterniteMangue) {
    EOCongeMaterniteCorresp eo = (EOCongeMaterniteCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaterniteCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeMaterniteRelationship(congeMaternite);
    eo.setCongeMaterniteMangueRelationship(congeMaterniteMangue);
    return eo;
  }

  public static NSArray<EOCongeMaterniteCorresp> fetchAllCongeMaterniteCorresps(EOEditingContext editingContext) {
    return _EOCongeMaterniteCorresp.fetchAllCongeMaterniteCorresps(editingContext, null);
  }

  public static NSArray<EOCongeMaterniteCorresp> fetchAllCongeMaterniteCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaterniteCorresp.fetchCongeMaterniteCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaterniteCorresp> fetchCongeMaterniteCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaterniteCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaterniteCorresp> eoObjects = (NSArray<EOCongeMaterniteCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaterniteCorresp fetchCongeMaterniteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaterniteCorresp.fetchCongeMaterniteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaterniteCorresp fetchCongeMaterniteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaterniteCorresp> eoObjects = _EOCongeMaterniteCorresp.fetchCongeMaterniteCorresps(editingContext, qualifier, null);
    EOCongeMaterniteCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaterniteCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaterniteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaterniteCorresp fetchRequiredCongeMaterniteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaterniteCorresp.fetchRequiredCongeMaterniteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaterniteCorresp fetchRequiredCongeMaterniteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaterniteCorresp eoObject = _EOCongeMaterniteCorresp.fetchCongeMaterniteCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaterniteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaterniteCorresp localInstanceIn(EOEditingContext editingContext, EOCongeMaterniteCorresp eo) {
    EOCongeMaterniteCorresp localInstance = (eo == null) ? null : (EOCongeMaterniteCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
