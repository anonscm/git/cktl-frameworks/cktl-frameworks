// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORibCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORibCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "RibCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String RIB_KEY = "rib";
	public static final String RIB_GRHUM_KEY = "ribGrhum";

  private static Logger LOG = Logger.getLogger(_EORibCorresp.class);

  public EORibCorresp localInstanceIn(EOEditingContext editingContext) {
    EORibCorresp localInstance = (EORibCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORibCorresp.LOG.isDebugEnabled()) {
    	_EORibCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORibCorresp.LOG.isDebugEnabled()) {
    	_EORibCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EORib rib() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EORib)storedValueForKey("rib");
  }

  public void setRibRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EORib value) {
    if (_EORibCorresp.LOG.isDebugEnabled()) {
      _EORibCorresp.LOG.debug("updating rib from " + rib() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "rib");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "rib");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRib ribGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRib)storedValueForKey("ribGrhum");
  }

  public void setRibGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRib value) {
    if (_EORibCorresp.LOG.isDebugEnabled()) {
      _EORibCorresp.LOG.debug("updating ribGrhum from " + ribGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRib oldValue = ribGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "ribGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "ribGrhum");
    }
  }
  

  public static EORibCorresp createRibCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EORib rib, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRib ribGrhum) {
    EORibCorresp eo = (EORibCorresp) EOUtilities.createAndInsertInstance(editingContext, _EORibCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setRibRelationship(rib);
    eo.setRibGrhumRelationship(ribGrhum);
    return eo;
  }

  public static NSArray<EORibCorresp> fetchAllRibCorresps(EOEditingContext editingContext) {
    return _EORibCorresp.fetchAllRibCorresps(editingContext, null);
  }

  public static NSArray<EORibCorresp> fetchAllRibCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORibCorresp.fetchRibCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EORibCorresp> fetchRibCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORibCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORibCorresp> eoObjects = (NSArray<EORibCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORibCorresp fetchRibCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EORibCorresp.fetchRibCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORibCorresp fetchRibCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORibCorresp> eoObjects = _EORibCorresp.fetchRibCorresps(editingContext, qualifier, null);
    EORibCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORibCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RibCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORibCorresp fetchRequiredRibCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EORibCorresp.fetchRequiredRibCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORibCorresp fetchRequiredRibCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EORibCorresp eoObject = _EORibCorresp.fetchRibCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RibCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORibCorresp localInstanceIn(EOEditingContext editingContext, EORibCorresp eo) {
    EORibCorresp localInstance = (eo == null) ? null : (EORibCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
