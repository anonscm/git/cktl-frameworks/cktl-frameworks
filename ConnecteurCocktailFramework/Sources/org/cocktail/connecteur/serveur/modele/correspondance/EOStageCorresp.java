package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueStage;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOStage;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOStageCorresp extends _EOStageCorresp {
	private static Logger log = Logger.getLogger(EOStageCorresp.class);

	public String nomRelationBaseImport() {
		return TO_STAGE_KEY;
	}
	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_STAGE_KEY;
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(toStage(), TO_STAGE_KEY);
		removeObjectFromBothSidesOfRelationshipWithKey(toMangueStage(), TO_MANGUE_STAGE_KEY);
	}

	/**
	 * 
	 */
	public static EOStageCorresp correspondancePourStageMangue(EOEditingContext editingContext,EOMangueStage stageMangue) {

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(TO_MANGUE_STAGE_KEY + " = %@ ",new NSArray(stageMangue));

		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOStageCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	public static EOStage stagePourIndividuEtCarriere(EOEditingContext editingContext,Number idSource,Number carSource,Number staSource) {


		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStage.INDIVIDU_KEY+"."+EOIndividu.ID_SOURCE_KEY + "=%@", new NSArray(idSource)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStage.TO_CARRIERE_KEY+"."+EOCarriere.CAR_SOURCE_KEY + "=%@", new NSArray(carSource)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStage.TEM_IMPORT_KEY + "=%@", new NSArray(ObjetImport.A_TRANSFERER)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStage.STATUT_KEY + " != %@", new NSArray("A")));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStage.STATUT_KEY + " != %@", new NSArray("E")));

		EOFetchSpecification myFetch = new EOFetchSpecification (EOStage.ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOStage)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e1) {

			// Rechercher dans les correspondances si le stage n'a pas déjà été importé
			qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_STAGE_KEY + "." + EOStage.INDIVIDU_KEY+"."+EOIndividu.ID_SOURCE_KEY + "=%@", new NSArray(idSource)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_STAGE_KEY + "." + EOStage.TO_CARRIERE_KEY+"."+EOCarriere.CAR_SOURCE_KEY + "=%@", new NSArray(carSource)));

			myFetch = new EOFetchSpecification (ENTITY_NAME, new EOAndQualifier(qualifiers),null);
			myFetch.setRefreshesRefetchedObjects(true);
			try {
				return ((EOStageCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0)).toStage();
			} catch (Exception e) {
				return null;

			}
		}
	}

}
