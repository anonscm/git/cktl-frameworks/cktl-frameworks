// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMadCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMadCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "MadCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String MAD_KEY = "mad";
	public static final String MANGUE_MAD_KEY = "mangueMad";

  private static Logger LOG = Logger.getLogger(_EOMadCorresp.class);

  public EOMadCorresp localInstanceIn(EOEditingContext editingContext) {
    EOMadCorresp localInstance = (EOMadCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMadCorresp.LOG.isDebugEnabled()) {
    	_EOMadCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMadCorresp.LOG.isDebugEnabled()) {
    	_EOMadCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOMad mad() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOMad)storedValueForKey("mad");
  }

  public void setMadRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOMad value) {
    if (_EOMadCorresp.LOG.isDebugEnabled()) {
      _EOMadCorresp.LOG.debug("updating mad from " + mad() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOMad oldValue = mad();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mad");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mad");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueMad mangueMad() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueMad)storedValueForKey("mangueMad");
  }

  public void setMangueMadRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueMad value) {
    if (_EOMadCorresp.LOG.isDebugEnabled()) {
      _EOMadCorresp.LOG.debug("updating mangueMad from " + mangueMad() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueMad oldValue = mangueMad();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueMad");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueMad");
    }
  }
  

  public static EOMadCorresp createMadCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOMad mad, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueMad mangueMad) {
    EOMadCorresp eo = (EOMadCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOMadCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setMadRelationship(mad);
    eo.setMangueMadRelationship(mangueMad);
    return eo;
  }

  public static NSArray<EOMadCorresp> fetchAllMadCorresps(EOEditingContext editingContext) {
    return _EOMadCorresp.fetchAllMadCorresps(editingContext, null);
  }

  public static NSArray<EOMadCorresp> fetchAllMadCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMadCorresp.fetchMadCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMadCorresp> fetchMadCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMadCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMadCorresp> eoObjects = (NSArray<EOMadCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMadCorresp fetchMadCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMadCorresp.fetchMadCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMadCorresp fetchMadCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMadCorresp> eoObjects = _EOMadCorresp.fetchMadCorresps(editingContext, qualifier, null);
    EOMadCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMadCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MadCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMadCorresp fetchRequiredMadCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMadCorresp.fetchRequiredMadCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMadCorresp fetchRequiredMadCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMadCorresp eoObject = _EOMadCorresp.fetchMadCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MadCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMadCorresp localInstanceIn(EOEditingContext editingContext, EOMadCorresp eo) {
    EOMadCorresp localInstance = (eo == null) ? null : (EOMadCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
