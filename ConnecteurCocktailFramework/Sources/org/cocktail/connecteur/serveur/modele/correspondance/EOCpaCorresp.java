package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

/**.
 * Classe gérant les correspondances
 * pour les Cessations Progressives d'Activité d'un individu
 * @author alainmalaplate
 *
 */
public class EOCpaCorresp extends _EOCpaCorresp {

	private static final long serialVersionUID = -8208489214602194568L;
	private static Logger log = Logger.getLogger(EOCpaCorresp.class);

	public EOCpaCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CPA_KEY;
	}

	public String nomRelationBaseImport() {
		return CPA_KEY;
	}

	public void supprimerRelations() {
		setCpaRelationship(null);
		setMangueCpaRelationship(null);
	}
}
