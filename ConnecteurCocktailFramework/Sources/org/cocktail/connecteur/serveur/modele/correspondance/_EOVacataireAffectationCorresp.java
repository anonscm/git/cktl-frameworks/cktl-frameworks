// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVacataireAffectationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVacataireAffectationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "VacataireAffectationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String MANGUE_VACATAIRES_AFFECTATION_KEY = "mangueVacatairesAffectation";
	public static final String VACATAIRE_AFFECTATION_KEY = "vacataireAffectation";

  private static Logger LOG = Logger.getLogger(_EOVacataireAffectationCorresp.class);

  public EOVacataireAffectationCorresp localInstanceIn(EOEditingContext editingContext) {
    EOVacataireAffectationCorresp localInstance = (EOVacataireAffectationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOVacataireAffectationCorresp.LOG.isDebugEnabled()) {
    	_EOVacataireAffectationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOVacataireAffectationCorresp.LOG.isDebugEnabled()) {
    	_EOVacataireAffectationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacatairesAffectation mangueVacatairesAffectation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacatairesAffectation)storedValueForKey("mangueVacatairesAffectation");
  }

  public void setMangueVacatairesAffectationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacatairesAffectation value) {
    if (_EOVacataireAffectationCorresp.LOG.isDebugEnabled()) {
      _EOVacataireAffectationCorresp.LOG.debug("updating mangueVacatairesAffectation from " + mangueVacatairesAffectation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacatairesAffectation oldValue = mangueVacatairesAffectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueVacatairesAffectation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueVacatairesAffectation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation vacataireAffectation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation)storedValueForKey("vacataireAffectation");
  }

  public void setVacataireAffectationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation value) {
    if (_EOVacataireAffectationCorresp.LOG.isDebugEnabled()) {
      _EOVacataireAffectationCorresp.LOG.debug("updating vacataireAffectation from " + vacataireAffectation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation oldValue = vacataireAffectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vacataireAffectation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vacataireAffectation");
    }
  }
  

  public static EOVacataireAffectationCorresp createVacataireAffectationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacatairesAffectation mangueVacatairesAffectation, org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation vacataireAffectation) {
    EOVacataireAffectationCorresp eo = (EOVacataireAffectationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOVacataireAffectationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setMangueVacatairesAffectationRelationship(mangueVacatairesAffectation);
    eo.setVacataireAffectationRelationship(vacataireAffectation);
    return eo;
  }

  public static NSArray<EOVacataireAffectationCorresp> fetchAllVacataireAffectationCorresps(EOEditingContext editingContext) {
    return _EOVacataireAffectationCorresp.fetchAllVacataireAffectationCorresps(editingContext, null);
  }

  public static NSArray<EOVacataireAffectationCorresp> fetchAllVacataireAffectationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVacataireAffectationCorresp.fetchVacataireAffectationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVacataireAffectationCorresp> fetchVacataireAffectationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVacataireAffectationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVacataireAffectationCorresp> eoObjects = (NSArray<EOVacataireAffectationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVacataireAffectationCorresp fetchVacataireAffectationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataireAffectationCorresp.fetchVacataireAffectationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacataireAffectationCorresp fetchVacataireAffectationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVacataireAffectationCorresp> eoObjects = _EOVacataireAffectationCorresp.fetchVacataireAffectationCorresps(editingContext, qualifier, null);
    EOVacataireAffectationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVacataireAffectationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VacataireAffectationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacataireAffectationCorresp fetchRequiredVacataireAffectationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataireAffectationCorresp.fetchRequiredVacataireAffectationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacataireAffectationCorresp fetchRequiredVacataireAffectationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVacataireAffectationCorresp eoObject = _EOVacataireAffectationCorresp.fetchVacataireAffectationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VacataireAffectationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacataireAffectationCorresp localInstanceIn(EOEditingContext editingContext, EOVacataireAffectationCorresp eo) {
    EOVacataireAffectationCorresp localInstance = (eo == null) ? null : (EOVacataireAffectationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
