// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOOccupationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOOccupationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "OccupationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String OCCUPATION_KEY = "occupation";
	public static final String OCCUPATION_MANGUE_KEY = "occupationMangue";

  private static Logger LOG = Logger.getLogger(_EOOccupationCorresp.class);

  public EOOccupationCorresp localInstanceIn(EOEditingContext editingContext) {
    EOOccupationCorresp localInstance = (EOOccupationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOOccupationCorresp.LOG.isDebugEnabled()) {
    	_EOOccupationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOOccupationCorresp.LOG.isDebugEnabled()) {
    	_EOOccupationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation occupation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation)storedValueForKey("occupation");
  }

  public void setOccupationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation value) {
    if (_EOOccupationCorresp.LOG.isDebugEnabled()) {
      _EOOccupationCorresp.LOG.debug("updating occupation from " + occupation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation oldValue = occupation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "occupation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "occupation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueOccupation occupationMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueOccupation)storedValueForKey("occupationMangue");
  }

  public void setOccupationMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueOccupation value) {
    if (_EOOccupationCorresp.LOG.isDebugEnabled()) {
      _EOOccupationCorresp.LOG.debug("updating occupationMangue from " + occupationMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueOccupation oldValue = occupationMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "occupationMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "occupationMangue");
    }
  }
  

  public static EOOccupationCorresp createOccupationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation occupation, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueOccupation occupationMangue) {
    EOOccupationCorresp eo = (EOOccupationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOOccupationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setOccupationRelationship(occupation);
    eo.setOccupationMangueRelationship(occupationMangue);
    return eo;
  }

  public static NSArray<EOOccupationCorresp> fetchAllOccupationCorresps(EOEditingContext editingContext) {
    return _EOOccupationCorresp.fetchAllOccupationCorresps(editingContext, null);
  }

  public static NSArray<EOOccupationCorresp> fetchAllOccupationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOOccupationCorresp.fetchOccupationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOOccupationCorresp> fetchOccupationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOOccupationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOOccupationCorresp> eoObjects = (NSArray<EOOccupationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOOccupationCorresp fetchOccupationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOccupationCorresp.fetchOccupationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOccupationCorresp fetchOccupationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOOccupationCorresp> eoObjects = _EOOccupationCorresp.fetchOccupationCorresps(editingContext, qualifier, null);
    EOOccupationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOOccupationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OccupationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOccupationCorresp fetchRequiredOccupationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOccupationCorresp.fetchRequiredOccupationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOccupationCorresp fetchRequiredOccupationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOOccupationCorresp eoObject = _EOOccupationCorresp.fetchOccupationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OccupationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOccupationCorresp localInstanceIn(EOEditingContext editingContext, EOOccupationCorresp eo) {
    EOOccupationCorresp localInstance = (eo == null) ? null : (EOOccupationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
