package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOMadCorresp extends _EOMadCorresp {
	@Override
	public void supprimerRelations() {
		setMadRelationship(null);
		setMangueMadRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return MAD_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_MAD_KEY;
	}
}
