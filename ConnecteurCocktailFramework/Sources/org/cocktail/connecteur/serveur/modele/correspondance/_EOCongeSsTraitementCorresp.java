// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeSsTraitementCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeSsTraitementCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CongeSsTraitementCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_CONGE_SS_TRAITEMENT_KEY = "toCongeSsTraitement";
	public static final String TO_MANGUE_CONGE_SS_TRAITEMENT_KEY = "toMangueCongeSsTraitement";

  private static Logger LOG = Logger.getLogger(_EOCongeSsTraitementCorresp.class);

  public EOCongeSsTraitementCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeSsTraitementCorresp localInstance = (EOCongeSsTraitementCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeSsTraitementCorresp.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitementCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeSsTraitementCorresp.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitementCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement toCongeSsTraitement() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement)storedValueForKey("toCongeSsTraitement");
  }

  public void setToCongeSsTraitementRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement value) {
    if (_EOCongeSsTraitementCorresp.LOG.isDebugEnabled()) {
      _EOCongeSsTraitementCorresp.LOG.debug("updating toCongeSsTraitement from " + toCongeSsTraitement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement oldValue = toCongeSsTraitement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeSsTraitement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeSsTraitement");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSsTraitement toMangueCongeSsTraitement() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSsTraitement)storedValueForKey("toMangueCongeSsTraitement");
  }

  public void setToMangueCongeSsTraitementRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSsTraitement value) {
    if (_EOCongeSsTraitementCorresp.LOG.isDebugEnabled()) {
      _EOCongeSsTraitementCorresp.LOG.debug("updating toMangueCongeSsTraitement from " + toMangueCongeSsTraitement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSsTraitement oldValue = toMangueCongeSsTraitement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueCongeSsTraitement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueCongeSsTraitement");
    }
  }
  

  public static EOCongeSsTraitementCorresp createCongeSsTraitementCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSsTraitement toCongeSsTraitement, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSsTraitement toMangueCongeSsTraitement) {
    EOCongeSsTraitementCorresp eo = (EOCongeSsTraitementCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeSsTraitementCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToCongeSsTraitementRelationship(toCongeSsTraitement);
    eo.setToMangueCongeSsTraitementRelationship(toMangueCongeSsTraitement);
    return eo;
  }

  public static NSArray<EOCongeSsTraitementCorresp> fetchAllCongeSsTraitementCorresps(EOEditingContext editingContext) {
    return _EOCongeSsTraitementCorresp.fetchAllCongeSsTraitementCorresps(editingContext, null);
  }

  public static NSArray<EOCongeSsTraitementCorresp> fetchAllCongeSsTraitementCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeSsTraitementCorresp.fetchCongeSsTraitementCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeSsTraitementCorresp> fetchCongeSsTraitementCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeSsTraitementCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeSsTraitementCorresp> eoObjects = (NSArray<EOCongeSsTraitementCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeSsTraitementCorresp fetchCongeSsTraitementCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeSsTraitementCorresp.fetchCongeSsTraitementCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeSsTraitementCorresp fetchCongeSsTraitementCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeSsTraitementCorresp> eoObjects = _EOCongeSsTraitementCorresp.fetchCongeSsTraitementCorresps(editingContext, qualifier, null);
    EOCongeSsTraitementCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeSsTraitementCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeSsTraitementCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeSsTraitementCorresp fetchRequiredCongeSsTraitementCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeSsTraitementCorresp.fetchRequiredCongeSsTraitementCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeSsTraitementCorresp fetchRequiredCongeSsTraitementCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeSsTraitementCorresp eoObject = _EOCongeSsTraitementCorresp.fetchCongeSsTraitementCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeSsTraitementCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeSsTraitementCorresp localInstanceIn(EOEditingContext editingContext, EOCongeSsTraitementCorresp eo) {
    EOCongeSsTraitementCorresp localInstance = (eo == null) ? null : (EOCongeSsTraitementCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
