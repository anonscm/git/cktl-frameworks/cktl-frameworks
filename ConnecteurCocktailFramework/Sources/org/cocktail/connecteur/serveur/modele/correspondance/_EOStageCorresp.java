// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOStageCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStageCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "StageCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_MANGUE_STAGE_KEY = "toMangueStage";
	public static final String TO_STAGE_KEY = "toStage";

  private static Logger LOG = Logger.getLogger(_EOStageCorresp.class);

  public EOStageCorresp localInstanceIn(EOEditingContext editingContext) {
    EOStageCorresp localInstance = (EOStageCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStageCorresp.LOG.isDebugEnabled()) {
    	_EOStageCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStageCorresp.LOG.isDebugEnabled()) {
    	_EOStageCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueStage toMangueStage() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueStage)storedValueForKey("toMangueStage");
  }

  public void setToMangueStageRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueStage value) {
    if (_EOStageCorresp.LOG.isDebugEnabled()) {
      _EOStageCorresp.LOG.debug("updating toMangueStage from " + toMangueStage() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueStage oldValue = toMangueStage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueStage");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueStage");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOStage toStage() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStage)storedValueForKey("toStage");
  }

  public void setToStageRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStage value) {
    if (_EOStageCorresp.LOG.isDebugEnabled()) {
      _EOStageCorresp.LOG.debug("updating toStage from " + toStage() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStage oldValue = toStage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStage");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStage");
    }
  }
  

  public static EOStageCorresp createStageCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueStage toMangueStage, org.cocktail.connecteur.serveur.modele.entite_import.EOStage toStage) {
    EOStageCorresp eo = (EOStageCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOStageCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToMangueStageRelationship(toMangueStage);
    eo.setToStageRelationship(toStage);
    return eo;
  }

  public static NSArray<EOStageCorresp> fetchAllStageCorresps(EOEditingContext editingContext) {
    return _EOStageCorresp.fetchAllStageCorresps(editingContext, null);
  }

  public static NSArray<EOStageCorresp> fetchAllStageCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStageCorresp.fetchStageCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStageCorresp> fetchStageCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStageCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStageCorresp> eoObjects = (NSArray<EOStageCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStageCorresp fetchStageCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStageCorresp.fetchStageCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStageCorresp fetchStageCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStageCorresp> eoObjects = _EOStageCorresp.fetchStageCorresps(editingContext, qualifier, null);
    EOStageCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStageCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one StageCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStageCorresp fetchRequiredStageCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStageCorresp.fetchRequiredStageCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStageCorresp fetchRequiredStageCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStageCorresp eoObject = _EOStageCorresp.fetchStageCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no StageCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStageCorresp localInstanceIn(EOEditingContext editingContext, EOStageCorresp eo) {
    EOStageCorresp localInstance = (eo == null) ? null : (EOStageCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
