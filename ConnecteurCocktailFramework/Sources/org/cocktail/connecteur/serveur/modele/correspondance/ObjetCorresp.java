/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IObjetCorresp;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Classe generique pour les objets de correspondance entre les bases d'import et destinataire.<BR>
	Tous les objets de correspondance doivent sous-classer ObjetCorresp et implementer les methodes
 * abstraites et doivent implementer la methode statique rechercherDestinatairePourRecord<BR>
 * 
 *  * 
 */
// 11/07/2011 - suppression du refresh dans la recherche des correspondances pour destinataire
public abstract class ObjetCorresp extends EOGenericRecord implements IObjetCorresp {
	public ObjetCorresp() {
		super();
	}
	public NSTimestamp dCreation() {
		return (NSTimestamp)storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}

	public NSTimestamp dModification() {
		return (NSTimestamp)storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	/** Initialise un objet de correspondance avec l'objet de la base d'import et l'objet de la base destination */
	public void initAvec(ObjetImport recordImport,ObjetPourSIDestinataire recordDestinataire) {
		NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
		addObjectToBothSidesOfRelationshipWithKey(recordImport, nomRelationBaseImport());
		addObjectToBothSidesOfRelationshipWithKey(recordDestinataire, nomRelationBaseDestinataire());
	}
	public void updaterAvecRecord(ObjetImport recordImport) {
		addObjectToBothSidesOfRelationshipWithKey(recordImport,nomRelationBaseImport());
		setDModification(new NSTimestamp());
	}
	public abstract void supprimerRelations();
	/** Retourne le destinataire associe a cette correspondance */
	public ObjetPourSIDestinataire destinataire() {
		return (ObjetPourSIDestinataire)valueForKey(nomRelationBaseDestinataire());
	}
	// Méthodes protégées
	/** Nom de la relation vers les donnees de la base d'import */
	public abstract String nomRelationBaseImport();
	/** Nom de la relation vers les donnees de la base du SI destinataire */
	public  abstract String nomRelationBaseDestinataire();
	// Méthodes statiques
	/** Retourne le nom de la relation de la correspondance vers l'objet de la base d'import pour une entite de la base d'import */
	public static String nomRelationBaseImportPourEntite(String nomEntite) {
		
		// On crée un objet de correspondance bidon pour avoir le nom de la relation d'import
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(nomEntite + "Corresp");
		ObjetCorresp correspondance = (ObjetCorresp)classDescription.createInstanceWithEditingContext(null,null);
		return correspondance.nomRelationBaseImport();
	}
	/** Recherche l'objet de correspondance pour le record passe en param&egrave:tre. Dans le cas d'une comparaison,
	 * on recherche une correspondance pointant sur le record.  
	 * @param recordImport ObjetImport
	 * @param estRechercheRecord true si on recherche la correspondance liee a ce record, false
	 * si on recherche un objet avec les m&ecirc;mes attributs de comparaison que le record */
	public static ObjetCorresp rechercherObjetCorrespPourRecordImport(EOEditingContext editingContext,ObjetImport recordImport, boolean estRechercheRecord) {
		//TODO Homonymie
		EOQualifier qualifier = null;
		NSArray sorts = null;
		if (estRechercheRecord) {
			qualifier = EOQualifier.qualifierWithQualifierFormat(nomRelationBaseImportPourEntite(recordImport.entityName()) + " = %@", new NSArray(recordImport));
		} else {
			qualifier = recordImport.construireQualifierPourCorrespondance();
			sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("dModification", EOSortOrdering.CompareDescending));
		}
		if (qualifier == null) {
			System.out.println("-------------- qualifier=null");
			return null;
		}
		System.out.println("-------------- qualifier="+qualifier.toString());
		EOFetchSpecification fs = new EOFetchSpecification(recordImport.entityName() + "Corresp",qualifier,sorts);
		fs.setFetchLimit(1);
		fs.setRefreshesRefetchedObjects(true);
		try {
			NSArray resultats=editingContext.objectsWithFetchSpecification(fs);
			System.out.println("-------------- resultats.count()="+resultats.count());
			return (ObjetCorresp) resultats.objectAtIndex(0);
		} catch (Exception e) {
			System.out.println("--------------     Exception dans rechercherObjetCorrespPourRecordImport");
			return null;
		}
	}
	/** Recherche l'objet destinataire correspondant au record passe en param&egrave:tre  comparaison,
	 * on recherche une correspondance pointant sur le record.  
	 * @param recordImport ObjetImport
	 * @param estSuppression true si il s'agit d'une suppression */
	public static ObjetPourSIDestinataire rechercherDestinatairePrecedentPourRecordImport(EOEditingContext editingContext,ObjetImport recordImport) {
		ObjetCorresp corresp = rechercherObjetCorrespPourRecordImport(editingContext, recordImport, false);
		if (corresp != null) {
			return (ObjetPourSIDestinataire)corresp.valueForKey(corresp.nomRelationBaseDestinataire());
		} else {
			return null;
		}
	}
	/** Recherche l'objet de correspondance pour le record du SI Destinataire passe en param&egrave:tre. 
	 * @param nomEntiteImport nom de l'entite d'import
	 * @param recordDestinataire ObjetPourSIDestinataire
	 * @return objetCorresp null si pas de correspondance trouvee
	 * */
	public static ObjetCorresp rechercherObjetCorrespPourRecordDestinataire(EOEditingContext editingContext,ObjetPourSIDestinataire recordDestinataire, String nomEntiteImport) {
		String nomEntiteCorresp = nomEntiteImport + "Corresp";
		// On crée un objet de correspondance bidon pour avoir le nom de la relation destinataire
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(nomEntiteCorresp);
		ObjetCorresp correspondance = (ObjetCorresp)classDescription.createInstanceWithEditingContext(null,null);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(correspondance.nomRelationBaseDestinataire() + " = %@", new NSArray(recordDestinataire));
		NSArray	sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("dModification", EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(nomEntiteCorresp,qualifier,sorts);
		fs.setFetchLimit(1);
		//fs.setRefreshesRefetchedObjects(true);
		try {
			return (ObjetCorresp)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Recherche l'objet import du SI Destinataire passe en param&egrave:tre en recherchant la correspondance la plus recente. 
	 * @param nomEntiteImport nom de l'entite d'import
	 * @param recordDestinataire ObjetPourSIDestinataire
	 * @return ObjetImport null si pas de correspondance trouvee

	 * */
	public static ObjetImport rechercherObjetImportPourRecordDestinataire(EOEditingContext editingContext,ObjetPourSIDestinataire recordDestinataire, String nomEntiteImport) {
		ObjetCorresp correspondance = rechercherObjetCorrespPourRecordDestinataire(editingContext, recordDestinataire, nomEntiteImport);
		if (correspondance == null) {
			return null;
		} else {
			return (ObjetImport)correspondance.valueForKey(correspondance.nomRelationBaseImport());
		}
	}
}
