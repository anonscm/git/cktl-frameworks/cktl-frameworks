// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuDiplomesCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuDiplomesCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "IndividuDiplomesCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_DIPLOMES_KEY = "individuDiplomes";
	public static final String INDIVIDU_DIPLOMES_MANGUE_KEY = "individuDiplomesMangue";

  private static Logger LOG = Logger.getLogger(_EOIndividuDiplomesCorresp.class);

  public EOIndividuDiplomesCorresp localInstanceIn(EOEditingContext editingContext) {
    EOIndividuDiplomesCorresp localInstance = (EOIndividuDiplomesCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuDiplomesCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomesCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuDiplomesCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuDiplomesCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes individuDiplomes() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes)storedValueForKey("individuDiplomes");
  }

  public void setIndividuDiplomesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes value) {
    if (_EOIndividuDiplomesCorresp.LOG.isDebugEnabled()) {
      _EOIndividuDiplomesCorresp.LOG.debug("updating individuDiplomes from " + individuDiplomes() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes oldValue = individuDiplomes();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuDiplomes");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuDiplomes");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDiplomes individuDiplomesMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDiplomes)storedValueForKey("individuDiplomesMangue");
  }

  public void setIndividuDiplomesMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDiplomes value) {
    if (_EOIndividuDiplomesCorresp.LOG.isDebugEnabled()) {
      _EOIndividuDiplomesCorresp.LOG.debug("updating individuDiplomesMangue from " + individuDiplomesMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDiplomes oldValue = individuDiplomesMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuDiplomesMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuDiplomesMangue");
    }
  }
  

  public static EOIndividuDiplomesCorresp createIndividuDiplomesCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes individuDiplomes, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDiplomes individuDiplomesMangue) {
    EOIndividuDiplomesCorresp eo = (EOIndividuDiplomesCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuDiplomesCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuDiplomesRelationship(individuDiplomes);
    eo.setIndividuDiplomesMangueRelationship(individuDiplomesMangue);
    return eo;
  }

  public static NSArray<EOIndividuDiplomesCorresp> fetchAllIndividuDiplomesCorresps(EOEditingContext editingContext) {
    return _EOIndividuDiplomesCorresp.fetchAllIndividuDiplomesCorresps(editingContext, null);
  }

  public static NSArray<EOIndividuDiplomesCorresp> fetchAllIndividuDiplomesCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuDiplomesCorresp.fetchIndividuDiplomesCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuDiplomesCorresp> fetchIndividuDiplomesCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuDiplomesCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuDiplomesCorresp> eoObjects = (NSArray<EOIndividuDiplomesCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuDiplomesCorresp fetchIndividuDiplomesCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDiplomesCorresp.fetchIndividuDiplomesCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDiplomesCorresp fetchIndividuDiplomesCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuDiplomesCorresp> eoObjects = _EOIndividuDiplomesCorresp.fetchIndividuDiplomesCorresps(editingContext, qualifier, null);
    EOIndividuDiplomesCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuDiplomesCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuDiplomesCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDiplomesCorresp fetchRequiredIndividuDiplomesCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDiplomesCorresp.fetchRequiredIndividuDiplomesCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDiplomesCorresp fetchRequiredIndividuDiplomesCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuDiplomesCorresp eoObject = _EOIndividuDiplomesCorresp.fetchIndividuDiplomesCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuDiplomesCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDiplomesCorresp localInstanceIn(EOEditingContext editingContext, EOIndividuDiplomesCorresp eo) {
    EOIndividuDiplomesCorresp localInstance = (eo == null) ? null : (EOIndividuDiplomesCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
