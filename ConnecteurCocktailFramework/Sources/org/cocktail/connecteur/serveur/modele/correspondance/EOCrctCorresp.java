package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCrct;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe gérant les correspondances pour les CRCT d'un individu
 * @author alainmalaplate
 *
 */
public class EOCrctCorresp extends _EOCrctCorresp {
	private static Logger log = Logger.getLogger(EOCrctCorresp.class);

	public EOCrctCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CRCT_KEY;
	}

	public String nomRelationBaseImport() {
		return CRCT_KEY;
	}

	public void supprimerRelations() {
		setCrctRelationship(null);
		setMangueCrctRelationship(null);
	}

	// Méthodes statiques
	/** retourne le crct d'import associe a l'id passee en parametre. On commence
	 * par chercher dans l'import courant pour etre sur de retourner le dernier crct, sinon
	 * on recherche dans les correspondances */
	public static EOCrct crctPourSourceEtCgId(EOEditingContext editingContext,Number idSource,Number crtSource) {
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(crtSource);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOCrct.ID_SOURCE_KEY+" = %@ AND "+EOCrct.CRCT_SOURCE_KEY+" = %@ AND temImport = %@ AND statut <> 'A'",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (EOCrct.ENTITY_NAME, myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOCrct)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e1) {
			// Rechercher dans les correspondances si le cld n'a pas déjà été importé
			args = new NSMutableArray(idSource);
			args.addObject(crtSource);
			myQualifier = EOQualifier.qualifierWithQualifierFormat(EOCrctCorresp.CRCT_KEY+"."+EOCrct.ID_SOURCE_KEY+" = %@ AND "+EOCrctCorresp.CRCT_KEY+"."+EOCrct.CRCT_SOURCE_KEY+" = %@",args);
			myFetch = new EOFetchSpecification (EOCrctCorresp.ENTITY_NAME, myQualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			try {
				return ((EOCrctCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0)).crct();
			} catch (Exception e) {
				return null;

			}
		}
	}
	/** retourne le cld de Mangue assoc&iee au cld d'import
	 * @param editingContext editing context 
	 * @param idSource identifiant de l'individu
	 * @param eimpSource identifiant du crct
	 **/
	public static EOMangueCrct crctMangue(EOEditingContext editingContext,Number crctSource, Number idSource) {
		// Rechercher dans les correspondances si le cld n'a pas déjà été importé
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(crctSource);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOCrctCorresp.CRCT_KEY+"."+EOCrct.ID_SOURCE_KEY+" = %@ AND "+EOCrctCorresp.CRCT_KEY+"."+EOCrct.CRCT_SOURCE_KEY+" = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (EOCrctCorresp.ENTITY_NAME, myQualifier,null);		
		try {
			EOCrctCorresp corresp = (EOCrctCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
			return corresp.mangueCrct();
		} catch (Exception e) {
			return null;
		}
	}


}
