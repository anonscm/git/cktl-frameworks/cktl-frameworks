// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAccidentServCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAccidentServCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CongeAccidentServCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_ACCIDENT_SERV_KEY = "congeAccidentServ";
	public static final String CONGE_ACCIDENT_SERV_MANGUE_KEY = "congeAccidentServMangue";

  private static Logger LOG = Logger.getLogger(_EOCongeAccidentServCorresp.class);

  public EOCongeAccidentServCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeAccidentServCorresp localInstance = (EOCongeAccidentServCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAccidentServCorresp.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAccidentServCorresp.LOG.isDebugEnabled()) {
    	_EOCongeAccidentServCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ congeAccidentServ() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ)storedValueForKey("congeAccidentServ");
  }

  public void setCongeAccidentServRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ value) {
    if (_EOCongeAccidentServCorresp.LOG.isDebugEnabled()) {
      _EOCongeAccidentServCorresp.LOG.debug("updating congeAccidentServ from " + congeAccidentServ() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ oldValue = congeAccidentServ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAccidentServ");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAccidentServ");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAccidentServ congeAccidentServMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAccidentServ)storedValueForKey("congeAccidentServMangue");
  }

  public void setCongeAccidentServMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAccidentServ value) {
    if (_EOCongeAccidentServCorresp.LOG.isDebugEnabled()) {
      _EOCongeAccidentServCorresp.LOG.debug("updating congeAccidentServMangue from " + congeAccidentServMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAccidentServ oldValue = congeAccidentServMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAccidentServMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAccidentServMangue");
    }
  }
  

  public static EOCongeAccidentServCorresp createCongeAccidentServCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ congeAccidentServ, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAccidentServ congeAccidentServMangue) {
    EOCongeAccidentServCorresp eo = (EOCongeAccidentServCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAccidentServCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeAccidentServRelationship(congeAccidentServ);
    eo.setCongeAccidentServMangueRelationship(congeAccidentServMangue);
    return eo;
  }

  public static NSArray<EOCongeAccidentServCorresp> fetchAllCongeAccidentServCorresps(EOEditingContext editingContext) {
    return _EOCongeAccidentServCorresp.fetchAllCongeAccidentServCorresps(editingContext, null);
  }

  public static NSArray<EOCongeAccidentServCorresp> fetchAllCongeAccidentServCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAccidentServCorresp.fetchCongeAccidentServCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAccidentServCorresp> fetchCongeAccidentServCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAccidentServCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAccidentServCorresp> eoObjects = (NSArray<EOCongeAccidentServCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAccidentServCorresp fetchCongeAccidentServCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAccidentServCorresp.fetchCongeAccidentServCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAccidentServCorresp fetchCongeAccidentServCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAccidentServCorresp> eoObjects = _EOCongeAccidentServCorresp.fetchCongeAccidentServCorresps(editingContext, qualifier, null);
    EOCongeAccidentServCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAccidentServCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAccidentServCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAccidentServCorresp fetchRequiredCongeAccidentServCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAccidentServCorresp.fetchRequiredCongeAccidentServCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAccidentServCorresp fetchRequiredCongeAccidentServCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAccidentServCorresp eoObject = _EOCongeAccidentServCorresp.fetchCongeAccidentServCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAccidentServCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAccidentServCorresp localInstanceIn(EOEditingContext editingContext, EOCongeAccidentServCorresp eo) {
    EOCongeAccidentServCorresp localInstance = (eo == null) ? null : (EOCongeAccidentServCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
