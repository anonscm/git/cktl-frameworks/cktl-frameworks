package org.cocktail.connecteur.serveur.modele.correspondance.conges;

public class EOCongeAl6Corresp extends _EOCongeAl6Corresp {
	@Override
	public void supprimerRelations() {
		setCongeAl6Relationship(null);
		setMangueCongeAl6Relationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return CONGE_AL6_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_AL6_KEY;
	}
}
