// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntAccidentTravCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntAccidentTravCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CgntAccidentTravCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CGNT_ACCIDENT_TRAV_KEY = "cgntAccidentTrav";
	public static final String CGNT_ACCIDENT_TRAV_MANGUE_KEY = "cgntAccidentTravMangue";

  private static Logger LOG = Logger.getLogger(_EOCgntAccidentTravCorresp.class);

  public EOCgntAccidentTravCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCgntAccidentTravCorresp localInstance = (EOCgntAccidentTravCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntAccidentTravCorresp.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTravCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntAccidentTravCorresp.LOG.isDebugEnabled()) {
    	_EOCgntAccidentTravCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav cgntAccidentTrav() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav)storedValueForKey("cgntAccidentTrav");
  }

  public void setCgntAccidentTravRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav value) {
    if (_EOCgntAccidentTravCorresp.LOG.isDebugEnabled()) {
      _EOCgntAccidentTravCorresp.LOG.debug("updating cgntAccidentTrav from " + cgntAccidentTrav() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav oldValue = cgntAccidentTrav();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntAccidentTrav");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntAccidentTrav");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCgntAccidentTrav cgntAccidentTravMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCgntAccidentTrav)storedValueForKey("cgntAccidentTravMangue");
  }

  public void setCgntAccidentTravMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCgntAccidentTrav value) {
    if (_EOCgntAccidentTravCorresp.LOG.isDebugEnabled()) {
      _EOCgntAccidentTravCorresp.LOG.debug("updating cgntAccidentTravMangue from " + cgntAccidentTravMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCgntAccidentTrav oldValue = cgntAccidentTravMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntAccidentTravMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntAccidentTravMangue");
    }
  }
  

  public static EOCgntAccidentTravCorresp createCgntAccidentTravCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCgntAccidentTrav cgntAccidentTrav, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCgntAccidentTrav cgntAccidentTravMangue) {
    EOCgntAccidentTravCorresp eo = (EOCgntAccidentTravCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCgntAccidentTravCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCgntAccidentTravRelationship(cgntAccidentTrav);
    eo.setCgntAccidentTravMangueRelationship(cgntAccidentTravMangue);
    return eo;
  }

  public static NSArray<EOCgntAccidentTravCorresp> fetchAllCgntAccidentTravCorresps(EOEditingContext editingContext) {
    return _EOCgntAccidentTravCorresp.fetchAllCgntAccidentTravCorresps(editingContext, null);
  }

  public static NSArray<EOCgntAccidentTravCorresp> fetchAllCgntAccidentTravCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntAccidentTravCorresp.fetchCgntAccidentTravCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntAccidentTravCorresp> fetchCgntAccidentTravCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntAccidentTravCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntAccidentTravCorresp> eoObjects = (NSArray<EOCgntAccidentTravCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntAccidentTravCorresp fetchCgntAccidentTravCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntAccidentTravCorresp.fetchCgntAccidentTravCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntAccidentTravCorresp fetchCgntAccidentTravCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntAccidentTravCorresp> eoObjects = _EOCgntAccidentTravCorresp.fetchCgntAccidentTravCorresps(editingContext, qualifier, null);
    EOCgntAccidentTravCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntAccidentTravCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntAccidentTravCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntAccidentTravCorresp fetchRequiredCgntAccidentTravCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntAccidentTravCorresp.fetchRequiredCgntAccidentTravCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntAccidentTravCorresp fetchRequiredCgntAccidentTravCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntAccidentTravCorresp eoObject = _EOCgntAccidentTravCorresp.fetchCgntAccidentTravCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntAccidentTravCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntAccidentTravCorresp localInstanceIn(EOEditingContext editingContext, EOCgntAccidentTravCorresp eo) {
    EOCgntAccidentTravCorresp localInstance = (eo == null) ? null : (EOCgntAccidentTravCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
