// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaladieSsTrtCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaladieSsTrtCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongeMaladieSsTrtCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_MALADIE_KEY = "congeMaladie";
	public static final String CONGE_MALADIE_MANGUE_KEY = "congeMaladieMangue";

  private static Logger LOG = Logger.getLogger(_EOCongeMaladieSsTrtCorresp.class);

  public EOCongeMaladieSsTrtCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaladieSsTrtCorresp localInstance = (EOCongeMaladieSsTrtCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaladieSsTrtCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrtCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaladieSsTrtCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaladieSsTrtCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt congeMaladie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt)storedValueForKey("congeMaladie");
  }

  public void setCongeMaladieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt value) {
    if (_EOCongeMaladieSsTrtCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaladieSsTrtCorresp.LOG.debug("updating congeMaladie from " + congeMaladie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt oldValue = congeMaladie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladie");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt congeMaladieMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt)storedValueForKey("congeMaladieMangue");
  }

  public void setCongeMaladieMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt value) {
    if (_EOCongeMaladieSsTrtCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaladieSsTrtCorresp.LOG.debug("updating congeMaladieMangue from " + congeMaladieMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt oldValue = congeMaladieMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladieMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladieMangue");
    }
  }
  

  public static EOCongeMaladieSsTrtCorresp createCongeMaladieSsTrtCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieSsTrt congeMaladie, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt congeMaladieMangue) {
    EOCongeMaladieSsTrtCorresp eo = (EOCongeMaladieSsTrtCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaladieSsTrtCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeMaladieRelationship(congeMaladie);
    eo.setCongeMaladieMangueRelationship(congeMaladieMangue);
    return eo;
  }

  public static NSArray<EOCongeMaladieSsTrtCorresp> fetchAllCongeMaladieSsTrtCorresps(EOEditingContext editingContext) {
    return _EOCongeMaladieSsTrtCorresp.fetchAllCongeMaladieSsTrtCorresps(editingContext, null);
  }

  public static NSArray<EOCongeMaladieSsTrtCorresp> fetchAllCongeMaladieSsTrtCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaladieSsTrtCorresp.fetchCongeMaladieSsTrtCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaladieSsTrtCorresp> fetchCongeMaladieSsTrtCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaladieSsTrtCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaladieSsTrtCorresp> eoObjects = (NSArray<EOCongeMaladieSsTrtCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaladieSsTrtCorresp fetchCongeMaladieSsTrtCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieSsTrtCorresp.fetchCongeMaladieSsTrtCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieSsTrtCorresp fetchCongeMaladieSsTrtCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaladieSsTrtCorresp> eoObjects = _EOCongeMaladieSsTrtCorresp.fetchCongeMaladieSsTrtCorresps(editingContext, qualifier, null);
    EOCongeMaladieSsTrtCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaladieSsTrtCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaladieSsTrtCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieSsTrtCorresp fetchRequiredCongeMaladieSsTrtCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieSsTrtCorresp.fetchRequiredCongeMaladieSsTrtCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieSsTrtCorresp fetchRequiredCongeMaladieSsTrtCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaladieSsTrtCorresp eoObject = _EOCongeMaladieSsTrtCorresp.fetchCongeMaladieSsTrtCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaladieSsTrtCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieSsTrtCorresp localInstanceIn(EOEditingContext editingContext, EOCongeMaladieSsTrtCorresp eo) {
    EOCongeMaladieSsTrtCorresp localInstance = (eo == null) ? null : (EOCongeMaladieSsTrtCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
