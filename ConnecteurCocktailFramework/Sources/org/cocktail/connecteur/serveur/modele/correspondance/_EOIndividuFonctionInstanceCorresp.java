// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuFonctionInstanceCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuFonctionInstanceCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "IndividuFonctionInstanceCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String GRHUM_INDIVIDU_FONCTION_INSTANCE_KEY = "grhumIndividuFonctionInstance";
	public static final String INDIVIDU_FONCTION_INSTANCE_KEY = "individuFonctionInstance";

  private static Logger LOG = Logger.getLogger(_EOIndividuFonctionInstanceCorresp.class);

  public EOIndividuFonctionInstanceCorresp localInstanceIn(EOEditingContext editingContext) {
    EOIndividuFonctionInstanceCorresp localInstance = (EOIndividuFonctionInstanceCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuFonctionInstanceCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstanceCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuFonctionInstanceCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionInstanceCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionInstance grhumIndividuFonctionInstance() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionInstance)storedValueForKey("grhumIndividuFonctionInstance");
  }

  public void setGrhumIndividuFonctionInstanceRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionInstance value) {
    if (_EOIndividuFonctionInstanceCorresp.LOG.isDebugEnabled()) {
      _EOIndividuFonctionInstanceCorresp.LOG.debug("updating grhumIndividuFonctionInstance from " + grhumIndividuFonctionInstance() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionInstance oldValue = grhumIndividuFonctionInstance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "grhumIndividuFonctionInstance");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "grhumIndividuFonctionInstance");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance individuFonctionInstance() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance)storedValueForKey("individuFonctionInstance");
  }

  public void setIndividuFonctionInstanceRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance value) {
    if (_EOIndividuFonctionInstanceCorresp.LOG.isDebugEnabled()) {
      _EOIndividuFonctionInstanceCorresp.LOG.debug("updating individuFonctionInstance from " + individuFonctionInstance() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance oldValue = individuFonctionInstance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuFonctionInstance");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuFonctionInstance");
    }
  }
  

  public static EOIndividuFonctionInstanceCorresp createIndividuFonctionInstanceCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionInstance grhumIndividuFonctionInstance, org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionInstance individuFonctionInstance) {
    EOIndividuFonctionInstanceCorresp eo = (EOIndividuFonctionInstanceCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuFonctionInstanceCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setGrhumIndividuFonctionInstanceRelationship(grhumIndividuFonctionInstance);
    eo.setIndividuFonctionInstanceRelationship(individuFonctionInstance);
    return eo;
  }

  public static NSArray<EOIndividuFonctionInstanceCorresp> fetchAllIndividuFonctionInstanceCorresps(EOEditingContext editingContext) {
    return _EOIndividuFonctionInstanceCorresp.fetchAllIndividuFonctionInstanceCorresps(editingContext, null);
  }

  public static NSArray<EOIndividuFonctionInstanceCorresp> fetchAllIndividuFonctionInstanceCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuFonctionInstanceCorresp.fetchIndividuFonctionInstanceCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuFonctionInstanceCorresp> fetchIndividuFonctionInstanceCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuFonctionInstanceCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuFonctionInstanceCorresp> eoObjects = (NSArray<EOIndividuFonctionInstanceCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuFonctionInstanceCorresp fetchIndividuFonctionInstanceCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionInstanceCorresp.fetchIndividuFonctionInstanceCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionInstanceCorresp fetchIndividuFonctionInstanceCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuFonctionInstanceCorresp> eoObjects = _EOIndividuFonctionInstanceCorresp.fetchIndividuFonctionInstanceCorresps(editingContext, qualifier, null);
    EOIndividuFonctionInstanceCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuFonctionInstanceCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuFonctionInstanceCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionInstanceCorresp fetchRequiredIndividuFonctionInstanceCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionInstanceCorresp.fetchRequiredIndividuFonctionInstanceCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionInstanceCorresp fetchRequiredIndividuFonctionInstanceCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuFonctionInstanceCorresp eoObject = _EOIndividuFonctionInstanceCorresp.fetchIndividuFonctionInstanceCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuFonctionInstanceCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionInstanceCorresp localInstanceIn(EOEditingContext editingContext, EOIndividuFonctionInstanceCorresp eo) {
    EOIndividuFonctionInstanceCorresp localInstance = (eo == null) ? null : (EOIndividuFonctionInstanceCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
