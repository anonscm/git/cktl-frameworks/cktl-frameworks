package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import org.apache.log4j.Logger;

public class EOCongePaterniteCorresp extends _EOCongePaterniteCorresp {
  private static Logger log = Logger.getLogger(EOCongePaterniteCorresp.class);


@Override
public void supprimerRelations() {
		setToCongePaterniteRelationship(null);
		setToMangueCongePaterniteRelationship(null);
}

public String nomRelationBaseDestinataire() {
	return TO_MANGUE_CONGE_PATERNITE_KEY;
}
public String nomRelationBaseImport() {
	return TO_CONGE_PATERNITE_KEY;
}

}
