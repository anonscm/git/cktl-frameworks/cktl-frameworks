// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "IndividuCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String INDIVIDU_GRHUM_KEY = "individuGrhum";

  private static Logger LOG = Logger.getLogger(_EOIndividuCorresp.class);

  public EOIndividuCorresp localInstanceIn(EOEditingContext editingContext) {
    EOIndividuCorresp localInstance = (EOIndividuCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu value) {
    if (_EOIndividuCorresp.LOG.isDebugEnabled()) {
      _EOIndividuCorresp.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individuGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individuGrhum");
  }

  public void setIndividuGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOIndividuCorresp.LOG.isDebugEnabled()) {
      _EOIndividuCorresp.LOG.debug("updating individuGrhum from " + individuGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individuGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuGrhum");
    }
  }
  

  public static EOIndividuCorresp createIndividuCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu individu, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individuGrhum) {
    EOIndividuCorresp eo = (EOIndividuCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    eo.setIndividuGrhumRelationship(individuGrhum);
    return eo;
  }

  public static NSArray<EOIndividuCorresp> fetchAllIndividuCorresps(EOEditingContext editingContext) {
    return _EOIndividuCorresp.fetchAllIndividuCorresps(editingContext, null);
  }

  public static NSArray<EOIndividuCorresp> fetchAllIndividuCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuCorresp.fetchIndividuCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuCorresp> fetchIndividuCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuCorresp> eoObjects = (NSArray<EOIndividuCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuCorresp fetchIndividuCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuCorresp.fetchIndividuCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuCorresp fetchIndividuCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuCorresp> eoObjects = _EOIndividuCorresp.fetchIndividuCorresps(editingContext, qualifier, null);
    EOIndividuCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuCorresp fetchRequiredIndividuCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuCorresp.fetchRequiredIndividuCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuCorresp fetchRequiredIndividuCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuCorresp eoObject = _EOIndividuCorresp.fetchIndividuCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuCorresp localInstanceIn(EOEditingContext editingContext, EOIndividuCorresp eo) {
    EOIndividuCorresp localInstance = (eo == null) ? null : (EOIndividuCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
