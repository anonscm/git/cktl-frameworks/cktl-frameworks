// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepriseTempsPleinCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepriseTempsPleinCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "RepriseTempsPleinCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String MANGUE_REPRISE_TEMPS_PLEIN_KEY = "mangueRepriseTempsPlein";
	public static final String REPRISE_TEMPS_PLEIN_KEY = "repriseTempsPlein";

  private static Logger LOG = Logger.getLogger(_EORepriseTempsPleinCorresp.class);

  public EORepriseTempsPleinCorresp localInstanceIn(EOEditingContext editingContext) {
    EORepriseTempsPleinCorresp localInstance = (EORepriseTempsPleinCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepriseTempsPleinCorresp.LOG.isDebugEnabled()) {
    	_EORepriseTempsPleinCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepriseTempsPleinCorresp.LOG.isDebugEnabled()) {
    	_EORepriseTempsPleinCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein mangueRepriseTempsPlein() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein)storedValueForKey("mangueRepriseTempsPlein");
  }

  public void setMangueRepriseTempsPleinRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein value) {
    if (_EORepriseTempsPleinCorresp.LOG.isDebugEnabled()) {
      _EORepriseTempsPleinCorresp.LOG.debug("updating mangueRepriseTempsPlein from " + mangueRepriseTempsPlein() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein oldValue = mangueRepriseTempsPlein();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueRepriseTempsPlein");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueRepriseTempsPlein");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein repriseTempsPlein() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein)storedValueForKey("repriseTempsPlein");
  }

  public void setRepriseTempsPleinRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein value) {
    if (_EORepriseTempsPleinCorresp.LOG.isDebugEnabled()) {
      _EORepriseTempsPleinCorresp.LOG.debug("updating repriseTempsPlein from " + repriseTempsPlein() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein oldValue = repriseTempsPlein();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "repriseTempsPlein");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "repriseTempsPlein");
    }
  }
  

  public static EORepriseTempsPleinCorresp createRepriseTempsPleinCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein mangueRepriseTempsPlein, org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein repriseTempsPlein) {
    EORepriseTempsPleinCorresp eo = (EORepriseTempsPleinCorresp) EOUtilities.createAndInsertInstance(editingContext, _EORepriseTempsPleinCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setMangueRepriseTempsPleinRelationship(mangueRepriseTempsPlein);
    eo.setRepriseTempsPleinRelationship(repriseTempsPlein);
    return eo;
  }

  public static NSArray<EORepriseTempsPleinCorresp> fetchAllRepriseTempsPleinCorresps(EOEditingContext editingContext) {
    return _EORepriseTempsPleinCorresp.fetchAllRepriseTempsPleinCorresps(editingContext, null);
  }

  public static NSArray<EORepriseTempsPleinCorresp> fetchAllRepriseTempsPleinCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepriseTempsPleinCorresp.fetchRepriseTempsPleinCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepriseTempsPleinCorresp> fetchRepriseTempsPleinCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepriseTempsPleinCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepriseTempsPleinCorresp> eoObjects = (NSArray<EORepriseTempsPleinCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepriseTempsPleinCorresp fetchRepriseTempsPleinCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepriseTempsPleinCorresp.fetchRepriseTempsPleinCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepriseTempsPleinCorresp fetchRepriseTempsPleinCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepriseTempsPleinCorresp> eoObjects = _EORepriseTempsPleinCorresp.fetchRepriseTempsPleinCorresps(editingContext, qualifier, null);
    EORepriseTempsPleinCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepriseTempsPleinCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RepriseTempsPleinCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepriseTempsPleinCorresp fetchRequiredRepriseTempsPleinCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepriseTempsPleinCorresp.fetchRequiredRepriseTempsPleinCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepriseTempsPleinCorresp fetchRequiredRepriseTempsPleinCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepriseTempsPleinCorresp eoObject = _EORepriseTempsPleinCorresp.fetchRepriseTempsPleinCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RepriseTempsPleinCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepriseTempsPleinCorresp localInstanceIn(EOEditingContext editingContext, EORepriseTempsPleinCorresp eo) {
    EORepriseTempsPleinCorresp localInstance = (eo == null) ? null : (EORepriseTempsPleinCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
