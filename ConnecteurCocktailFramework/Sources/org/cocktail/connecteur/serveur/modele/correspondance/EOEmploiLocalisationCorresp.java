package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOEmploiLocalisationCorresp extends _EOEmploiLocalisationCorresp {

@Override
public void supprimerRelations() {
	setToEmploiLocalisationRelationship(null);
	setToMangueEmploiLocalisationRelationship(null);
}

@Override
public String nomRelationBaseImport() {
	return TO_EMPLOI_LOCALISATION_KEY;
}

@Override
public String nomRelationBaseDestinataire() {
	return TO_MANGUE_EMPLOI_LOCALISATION_KEY;
}
}
