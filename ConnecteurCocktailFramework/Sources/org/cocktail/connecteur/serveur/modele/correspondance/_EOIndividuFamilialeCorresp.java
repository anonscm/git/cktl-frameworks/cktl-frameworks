// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuFamilialeCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuFamilialeCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "IndividuFamilialeCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_FAMILIALE_KEY = "individuFamiliale";
	public static final String INDIVIDU_FAMILIALE_MANGUE_KEY = "individuFamilialeMangue";

  private static Logger LOG = Logger.getLogger(_EOIndividuFamilialeCorresp.class);

  public EOIndividuFamilialeCorresp localInstanceIn(EOEditingContext editingContext) {
    EOIndividuFamilialeCorresp localInstance = (EOIndividuFamilialeCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuFamilialeCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuFamilialeCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuFamilialeCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuFamilialeCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale individuFamiliale() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale)storedValueForKey("individuFamiliale");
  }

  public void setIndividuFamilialeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale value) {
    if (_EOIndividuFamilialeCorresp.LOG.isDebugEnabled()) {
      _EOIndividuFamilialeCorresp.LOG.debug("updating individuFamiliale from " + individuFamiliale() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale oldValue = individuFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuFamiliale");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuFamiliale");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuFamiliale individuFamilialeMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuFamiliale)storedValueForKey("individuFamilialeMangue");
  }

  public void setIndividuFamilialeMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuFamiliale value) {
    if (_EOIndividuFamilialeCorresp.LOG.isDebugEnabled()) {
      _EOIndividuFamilialeCorresp.LOG.debug("updating individuFamilialeMangue from " + individuFamilialeMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuFamiliale oldValue = individuFamilialeMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuFamilialeMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuFamilialeMangue");
    }
  }
  

  public static EOIndividuFamilialeCorresp createIndividuFamilialeCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale individuFamiliale, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuFamiliale individuFamilialeMangue) {
    EOIndividuFamilialeCorresp eo = (EOIndividuFamilialeCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuFamilialeCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuFamilialeRelationship(individuFamiliale);
    eo.setIndividuFamilialeMangueRelationship(individuFamilialeMangue);
    return eo;
  }

  public static NSArray<EOIndividuFamilialeCorresp> fetchAllIndividuFamilialeCorresps(EOEditingContext editingContext) {
    return _EOIndividuFamilialeCorresp.fetchAllIndividuFamilialeCorresps(editingContext, null);
  }

  public static NSArray<EOIndividuFamilialeCorresp> fetchAllIndividuFamilialeCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuFamilialeCorresp.fetchIndividuFamilialeCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuFamilialeCorresp> fetchIndividuFamilialeCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuFamilialeCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuFamilialeCorresp> eoObjects = (NSArray<EOIndividuFamilialeCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuFamilialeCorresp fetchIndividuFamilialeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFamilialeCorresp.fetchIndividuFamilialeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFamilialeCorresp fetchIndividuFamilialeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuFamilialeCorresp> eoObjects = _EOIndividuFamilialeCorresp.fetchIndividuFamilialeCorresps(editingContext, qualifier, null);
    EOIndividuFamilialeCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuFamilialeCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuFamilialeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFamilialeCorresp fetchRequiredIndividuFamilialeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFamilialeCorresp.fetchRequiredIndividuFamilialeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFamilialeCorresp fetchRequiredIndividuFamilialeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuFamilialeCorresp eoObject = _EOIndividuFamilialeCorresp.fetchIndividuFamilialeCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuFamilialeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFamilialeCorresp localInstanceIn(EOEditingContext editingContext, EOIndividuFamilialeCorresp eo) {
    EOIndividuFamilialeCorresp localInstance = (eo == null) ? null : (EOIndividuFamilialeCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
