// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOClmCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOClmCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "ClmCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CLM_KEY = "clm";
	public static final String CLM_MANGUE_KEY = "clmMangue";

  private static Logger LOG = Logger.getLogger(_EOClmCorresp.class);

  public EOClmCorresp localInstanceIn(EOEditingContext editingContext) {
    EOClmCorresp localInstance = (EOClmCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOClmCorresp.LOG.isDebugEnabled()) {
    	_EOClmCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOClmCorresp.LOG.isDebugEnabled()) {
    	_EOClmCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOClm clm() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOClm)storedValueForKey("clm");
  }

  public void setClmRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOClm value) {
    if (_EOClmCorresp.LOG.isDebugEnabled()) {
      _EOClmCorresp.LOG.debug("updating clm from " + clm() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOClm oldValue = clm();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "clm");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "clm");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm clmMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm)storedValueForKey("clmMangue");
  }

  public void setClmMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm value) {
    if (_EOClmCorresp.LOG.isDebugEnabled()) {
      _EOClmCorresp.LOG.debug("updating clmMangue from " + clmMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm oldValue = clmMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "clmMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "clmMangue");
    }
  }
  

  public static EOClmCorresp createClmCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOClm clm, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm clmMangue) {
    EOClmCorresp eo = (EOClmCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOClmCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setClmRelationship(clm);
    eo.setClmMangueRelationship(clmMangue);
    return eo;
  }

  public static NSArray<EOClmCorresp> fetchAllClmCorresps(EOEditingContext editingContext) {
    return _EOClmCorresp.fetchAllClmCorresps(editingContext, null);
  }

  public static NSArray<EOClmCorresp> fetchAllClmCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOClmCorresp.fetchClmCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOClmCorresp> fetchClmCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOClmCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOClmCorresp> eoObjects = (NSArray<EOClmCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOClmCorresp fetchClmCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOClmCorresp.fetchClmCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOClmCorresp fetchClmCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOClmCorresp> eoObjects = _EOClmCorresp.fetchClmCorresps(editingContext, qualifier, null);
    EOClmCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOClmCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ClmCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOClmCorresp fetchRequiredClmCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOClmCorresp.fetchRequiredClmCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOClmCorresp fetchRequiredClmCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOClmCorresp eoObject = _EOClmCorresp.fetchClmCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ClmCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOClmCorresp localInstanceIn(EOEditingContext editingContext, EOClmCorresp eo) {
    EOClmCorresp localInstance = (eo == null) ? null : (EOClmCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
