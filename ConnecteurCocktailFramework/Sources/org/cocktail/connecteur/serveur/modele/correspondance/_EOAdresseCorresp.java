// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOAdresseCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOAdresseCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "AdresseCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String ADRESSE_GRHUM_KEY = "adresseGrhum";

  private static Logger LOG = Logger.getLogger(_EOAdresseCorresp.class);

  public EOAdresseCorresp localInstanceIn(EOEditingContext editingContext) {
    EOAdresseCorresp localInstance = (EOAdresseCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOAdresseCorresp.LOG.isDebugEnabled()) {
    	_EOAdresseCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOAdresseCorresp.LOG.isDebugEnabled()) {
    	_EOAdresseCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse adresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse)storedValueForKey("adresse");
  }

  public void setAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse value) {
    if (_EOAdresseCorresp.LOG.isDebugEnabled()) {
      _EOAdresseCorresp.LOG.debug("updating adresse from " + adresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "adresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "adresse");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse adresseGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse)storedValueForKey("adresseGrhum");
  }

  public void setAdresseGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse value) {
    if (_EOAdresseCorresp.LOG.isDebugEnabled()) {
      _EOAdresseCorresp.LOG.debug("updating adresseGrhum from " + adresseGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse oldValue = adresseGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "adresseGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "adresseGrhum");
    }
  }
  

  public static EOAdresseCorresp createAdresseCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse adresse, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse adresseGrhum) {
    EOAdresseCorresp eo = (EOAdresseCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOAdresseCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setAdresseRelationship(adresse);
    eo.setAdresseGrhumRelationship(adresseGrhum);
    return eo;
  }

  public static NSArray<EOAdresseCorresp> fetchAllAdresseCorresps(EOEditingContext editingContext) {
    return _EOAdresseCorresp.fetchAllAdresseCorresps(editingContext, null);
  }

  public static NSArray<EOAdresseCorresp> fetchAllAdresseCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAdresseCorresp.fetchAdresseCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAdresseCorresp> fetchAdresseCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAdresseCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAdresseCorresp> eoObjects = (NSArray<EOAdresseCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAdresseCorresp fetchAdresseCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAdresseCorresp.fetchAdresseCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAdresseCorresp fetchAdresseCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAdresseCorresp> eoObjects = _EOAdresseCorresp.fetchAdresseCorresps(editingContext, qualifier, null);
    EOAdresseCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAdresseCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one AdresseCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAdresseCorresp fetchRequiredAdresseCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAdresseCorresp.fetchRequiredAdresseCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAdresseCorresp fetchRequiredAdresseCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAdresseCorresp eoObject = _EOAdresseCorresp.fetchAdresseCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no AdresseCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAdresseCorresp localInstanceIn(EOEditingContext editingContext, EOAdresseCorresp eo) {
    EOAdresseCorresp localInstance = (eo == null) ? null : (EOAdresseCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
