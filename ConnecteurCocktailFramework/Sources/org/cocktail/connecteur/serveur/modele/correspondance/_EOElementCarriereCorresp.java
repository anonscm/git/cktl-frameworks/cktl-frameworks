// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOElementCarriereCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOElementCarriereCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "ElementCarriereCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String ELEMENT_CARRIERE_KEY = "elementCarriere";
	public static final String ELEMENT_CARRIERE_MANGUE_KEY = "elementCarriereMangue";

  private static Logger LOG = Logger.getLogger(_EOElementCarriereCorresp.class);

  public EOElementCarriereCorresp localInstanceIn(EOEditingContext editingContext) {
    EOElementCarriereCorresp localInstance = (EOElementCarriereCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOElementCarriereCorresp.LOG.isDebugEnabled()) {
    	_EOElementCarriereCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOElementCarriereCorresp.LOG.isDebugEnabled()) {
    	_EOElementCarriereCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere elementCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere)storedValueForKey("elementCarriere");
  }

  public void setElementCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere value) {
    if (_EOElementCarriereCorresp.LOG.isDebugEnabled()) {
      _EOElementCarriereCorresp.LOG.debug("updating elementCarriere from " + elementCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere oldValue = elementCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "elementCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "elementCarriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere elementCarriereMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere)storedValueForKey("elementCarriereMangue");
  }

  public void setElementCarriereMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere value) {
    if (_EOElementCarriereCorresp.LOG.isDebugEnabled()) {
      _EOElementCarriereCorresp.LOG.debug("updating elementCarriereMangue from " + elementCarriereMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere oldValue = elementCarriereMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "elementCarriereMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "elementCarriereMangue");
    }
  }
  

  public static EOElementCarriereCorresp createElementCarriereCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere elementCarriere, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere elementCarriereMangue) {
    EOElementCarriereCorresp eo = (EOElementCarriereCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOElementCarriereCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setElementCarriereRelationship(elementCarriere);
    eo.setElementCarriereMangueRelationship(elementCarriereMangue);
    return eo;
  }

  public static NSArray<EOElementCarriereCorresp> fetchAllElementCarriereCorresps(EOEditingContext editingContext) {
    return _EOElementCarriereCorresp.fetchAllElementCarriereCorresps(editingContext, null);
  }

  public static NSArray<EOElementCarriereCorresp> fetchAllElementCarriereCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOElementCarriereCorresp.fetchElementCarriereCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOElementCarriereCorresp> fetchElementCarriereCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOElementCarriereCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOElementCarriereCorresp> eoObjects = (NSArray<EOElementCarriereCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOElementCarriereCorresp fetchElementCarriereCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOElementCarriereCorresp.fetchElementCarriereCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOElementCarriereCorresp fetchElementCarriereCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOElementCarriereCorresp> eoObjects = _EOElementCarriereCorresp.fetchElementCarriereCorresps(editingContext, qualifier, null);
    EOElementCarriereCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOElementCarriereCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ElementCarriereCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOElementCarriereCorresp fetchRequiredElementCarriereCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOElementCarriereCorresp.fetchRequiredElementCarriereCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOElementCarriereCorresp fetchRequiredElementCarriereCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOElementCarriereCorresp eoObject = _EOElementCarriereCorresp.fetchElementCarriereCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ElementCarriereCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOElementCarriereCorresp localInstanceIn(EOEditingContext editingContext, EOElementCarriereCorresp eo) {
    EOElementCarriereCorresp localInstance = (eo == null) ? null : (EOElementCarriereCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
