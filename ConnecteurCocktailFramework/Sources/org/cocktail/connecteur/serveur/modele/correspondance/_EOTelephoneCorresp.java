// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTelephoneCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTelephoneCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "TelephoneCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TELEPHONE_KEY = "telephone";
	public static final String TELEPHONE_GRHUM_KEY = "telephoneGrhum";

  private static Logger LOG = Logger.getLogger(_EOTelephoneCorresp.class);

  public EOTelephoneCorresp localInstanceIn(EOEditingContext editingContext) {
    EOTelephoneCorresp localInstance = (EOTelephoneCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTelephoneCorresp.LOG.isDebugEnabled()) {
    	_EOTelephoneCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTelephoneCorresp.LOG.isDebugEnabled()) {
    	_EOTelephoneCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone telephone() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone)storedValueForKey("telephone");
  }

  public void setTelephoneRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone value) {
    if (_EOTelephoneCorresp.LOG.isDebugEnabled()) {
      _EOTelephoneCorresp.LOG.debug("updating telephone from " + telephone() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone oldValue = telephone();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "telephone");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "telephone");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonneTelephone telephoneGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonneTelephone)storedValueForKey("telephoneGrhum");
  }

  public void setTelephoneGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonneTelephone value) {
    if (_EOTelephoneCorresp.LOG.isDebugEnabled()) {
      _EOTelephoneCorresp.LOG.debug("updating telephoneGrhum from " + telephoneGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonneTelephone oldValue = telephoneGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "telephoneGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "telephoneGrhum");
    }
  }
  

  public static EOTelephoneCorresp createTelephoneCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone telephone, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonneTelephone telephoneGrhum) {
    EOTelephoneCorresp eo = (EOTelephoneCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOTelephoneCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setTelephoneRelationship(telephone);
    eo.setTelephoneGrhumRelationship(telephoneGrhum);
    return eo;
  }

  public static NSArray<EOTelephoneCorresp> fetchAllTelephoneCorresps(EOEditingContext editingContext) {
    return _EOTelephoneCorresp.fetchAllTelephoneCorresps(editingContext, null);
  }

  public static NSArray<EOTelephoneCorresp> fetchAllTelephoneCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTelephoneCorresp.fetchTelephoneCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTelephoneCorresp> fetchTelephoneCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTelephoneCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTelephoneCorresp> eoObjects = (NSArray<EOTelephoneCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTelephoneCorresp fetchTelephoneCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTelephoneCorresp.fetchTelephoneCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTelephoneCorresp fetchTelephoneCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTelephoneCorresp> eoObjects = _EOTelephoneCorresp.fetchTelephoneCorresps(editingContext, qualifier, null);
    EOTelephoneCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTelephoneCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TelephoneCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTelephoneCorresp fetchRequiredTelephoneCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTelephoneCorresp.fetchRequiredTelephoneCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTelephoneCorresp fetchRequiredTelephoneCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTelephoneCorresp eoObject = _EOTelephoneCorresp.fetchTelephoneCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TelephoneCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTelephoneCorresp localInstanceIn(EOEditingContext editingContext, EOTelephoneCorresp eo) {
    EOTelephoneCorresp localInstance = (eo == null) ? null : (EOTelephoneCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
