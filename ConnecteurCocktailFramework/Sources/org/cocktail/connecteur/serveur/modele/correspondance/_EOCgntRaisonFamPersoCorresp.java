// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntRaisonFamPersoCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntRaisonFamPersoCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CgntRaisonFamPersoCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CGNT_RAISON_FAM_PERSO_KEY = "cgntRaisonFamPerso";
	public static final String MANGUE_CGNT_RAISON_FAM_PERSO_KEY = "mangueCgntRaisonFamPerso";

  private static Logger LOG = Logger.getLogger(_EOCgntRaisonFamPersoCorresp.class);

  public EOCgntRaisonFamPersoCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCgntRaisonFamPersoCorresp localInstance = (EOCgntRaisonFamPersoCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntRaisonFamPersoCorresp.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPersoCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntRaisonFamPersoCorresp.LOG.isDebugEnabled()) {
    	_EOCgntRaisonFamPersoCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso cgntRaisonFamPerso() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso)storedValueForKey("cgntRaisonFamPerso");
  }

  public void setCgntRaisonFamPersoRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso value) {
    if (_EOCgntRaisonFamPersoCorresp.LOG.isDebugEnabled()) {
      _EOCgntRaisonFamPersoCorresp.LOG.debug("updating cgntRaisonFamPerso from " + cgntRaisonFamPerso() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso oldValue = cgntRaisonFamPerso();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntRaisonFamPerso");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntRaisonFamPerso");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso mangueCgntRaisonFamPerso() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso)storedValueForKey("mangueCgntRaisonFamPerso");
  }

  public void setMangueCgntRaisonFamPersoRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso value) {
    if (_EOCgntRaisonFamPersoCorresp.LOG.isDebugEnabled()) {
      _EOCgntRaisonFamPersoCorresp.LOG.debug("updating mangueCgntRaisonFamPerso from " + mangueCgntRaisonFamPerso() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso oldValue = mangueCgntRaisonFamPerso();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCgntRaisonFamPerso");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCgntRaisonFamPerso");
    }
  }
  

  public static EOCgntRaisonFamPersoCorresp createCgntRaisonFamPersoCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCgntRaisonFamPerso cgntRaisonFamPerso, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso mangueCgntRaisonFamPerso) {
    EOCgntRaisonFamPersoCorresp eo = (EOCgntRaisonFamPersoCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCgntRaisonFamPersoCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCgntRaisonFamPersoRelationship(cgntRaisonFamPerso);
    eo.setMangueCgntRaisonFamPersoRelationship(mangueCgntRaisonFamPerso);
    return eo;
  }

  public static NSArray<EOCgntRaisonFamPersoCorresp> fetchAllCgntRaisonFamPersoCorresps(EOEditingContext editingContext) {
    return _EOCgntRaisonFamPersoCorresp.fetchAllCgntRaisonFamPersoCorresps(editingContext, null);
  }

  public static NSArray<EOCgntRaisonFamPersoCorresp> fetchAllCgntRaisonFamPersoCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntRaisonFamPersoCorresp.fetchCgntRaisonFamPersoCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntRaisonFamPersoCorresp> fetchCgntRaisonFamPersoCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntRaisonFamPersoCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntRaisonFamPersoCorresp> eoObjects = (NSArray<EOCgntRaisonFamPersoCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntRaisonFamPersoCorresp fetchCgntRaisonFamPersoCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntRaisonFamPersoCorresp.fetchCgntRaisonFamPersoCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntRaisonFamPersoCorresp fetchCgntRaisonFamPersoCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntRaisonFamPersoCorresp> eoObjects = _EOCgntRaisonFamPersoCorresp.fetchCgntRaisonFamPersoCorresps(editingContext, qualifier, null);
    EOCgntRaisonFamPersoCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntRaisonFamPersoCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntRaisonFamPersoCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntRaisonFamPersoCorresp fetchRequiredCgntRaisonFamPersoCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntRaisonFamPersoCorresp.fetchRequiredCgntRaisonFamPersoCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntRaisonFamPersoCorresp fetchRequiredCgntRaisonFamPersoCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntRaisonFamPersoCorresp eoObject = _EOCgntRaisonFamPersoCorresp.fetchCgntRaisonFamPersoCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntRaisonFamPersoCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntRaisonFamPersoCorresp localInstanceIn(EOEditingContext editingContext, EOCgntRaisonFamPersoCorresp eo) {
    EOCgntRaisonFamPersoCorresp localInstance = (eo == null) ? null : (EOCgntRaisonFamPersoCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
