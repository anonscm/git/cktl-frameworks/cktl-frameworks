package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOCongeSsTraitementCorresp extends _EOCongeSsTraitementCorresp {

	@Override
	public void supprimerRelations() {
		setToCongeSsTraitementRelationship(null);
		setToMangueCongeSsTraitementRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return TO_CONGE_SS_TRAITEMENT_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_CONGE_SS_TRAITEMENT_KEY;
	}
}
