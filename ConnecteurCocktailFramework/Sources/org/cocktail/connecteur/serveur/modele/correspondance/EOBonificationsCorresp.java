package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EOBonificationsCorresp extends _EOBonificationsCorresp {
  private static Logger log = Logger.getLogger(EOBonificationsCorresp.class);
  
	public EOBonificationsCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_BONIFICATIONS_KEY;
	}

	public String nomRelationBaseImport() {
		return TO_BONIFICATIONS_KEY;
	}

	public void supprimerRelations() {
		setToBonificationsRelationship(null);
		setToMangueBonificationsRelationship(null);
	}
}
