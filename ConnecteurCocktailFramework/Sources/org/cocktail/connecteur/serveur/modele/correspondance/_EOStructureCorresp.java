// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOStructureCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStructureCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "StructureCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_GRHUM_KEY = "structureGrhum";

  private static Logger LOG = Logger.getLogger(_EOStructureCorresp.class);

  public EOStructureCorresp localInstanceIn(EOEditingContext editingContext) {
    EOStructureCorresp localInstance = (EOStructureCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStructureCorresp.LOG.isDebugEnabled()) {
    	_EOStructureCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStructureCorresp.LOG.isDebugEnabled()) {
    	_EOStructureCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOStructure value) {
    if (_EOStructureCorresp.LOG.isDebugEnabled()) {
      _EOStructureCorresp.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structureGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structureGrhum");
  }

  public void setStructureGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOStructureCorresp.LOG.isDebugEnabled()) {
      _EOStructureCorresp.LOG.debug("updating structureGrhum from " + structureGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structureGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structureGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structureGrhum");
    }
  }
  

  public static EOStructureCorresp createStructureCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOStructure structure, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structureGrhum) {
    EOStructureCorresp eo = (EOStructureCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOStructureCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setStructureRelationship(structure);
    eo.setStructureGrhumRelationship(structureGrhum);
    return eo;
  }

  public static NSArray<EOStructureCorresp> fetchAllStructureCorresps(EOEditingContext editingContext) {
    return _EOStructureCorresp.fetchAllStructureCorresps(editingContext, null);
  }

  public static NSArray<EOStructureCorresp> fetchAllStructureCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStructureCorresp.fetchStructureCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStructureCorresp> fetchStructureCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStructureCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStructureCorresp> eoObjects = (NSArray<EOStructureCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStructureCorresp fetchStructureCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructureCorresp.fetchStructureCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructureCorresp fetchStructureCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStructureCorresp> eoObjects = _EOStructureCorresp.fetchStructureCorresps(editingContext, qualifier, null);
    EOStructureCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStructureCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one StructureCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructureCorresp fetchRequiredStructureCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructureCorresp.fetchRequiredStructureCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructureCorresp fetchRequiredStructureCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStructureCorresp eoObject = _EOStructureCorresp.fetchStructureCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no StructureCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructureCorresp localInstanceIn(EOEditingContext editingContext, EOStructureCorresp eo) {
    EOStructureCorresp localInstance = (eo == null) ? null : (EOStructureCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
