package org.cocktail.connecteur.serveur.modele.correspondance;


public class EOCgntAccidentTravCorresp extends _EOCgntAccidentTravCorresp {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3263265077054785849L;

	public EOCgntAccidentTravCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return CGNT_ACCIDENT_TRAV_MANGUE_KEY;
	}

	public String nomRelationBaseImport() {
		return CGNT_ACCIDENT_TRAV_KEY;
	}

	public void supprimerRelations() {
		setCgntAccidentTravRelationship(null);
		setCgntAccidentTravMangueRelationship(null);
	}
}
