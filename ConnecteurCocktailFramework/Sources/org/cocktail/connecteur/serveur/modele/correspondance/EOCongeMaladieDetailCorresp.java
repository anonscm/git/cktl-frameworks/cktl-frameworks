package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOCongeMaladieDetailCorresp extends _EOCongeMaladieDetailCorresp {
	@Override
	public void supprimerRelations() {
		setCongeMaladieDetailRelationship(null);
		setMangueCongeMaladieDetailRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return CONGE_MALADIE_DETAIL_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_MALADIE_DETAIL_KEY;
	}
}
