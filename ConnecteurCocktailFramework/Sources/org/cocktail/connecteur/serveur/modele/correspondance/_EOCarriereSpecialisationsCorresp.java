// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCarriereSpecialisationsCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCarriereSpecialisationsCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CarriereSpecialisationsCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CARRIERE_SPECIALISATIONS_KEY = "carriereSpecialisations";
	public static final String CARRIERE_SPECIALISATIONS_MANGUE_KEY = "carriereSpecialisationsMangue";

  private static Logger LOG = Logger.getLogger(_EOCarriereSpecialisationsCorresp.class);

  public EOCarriereSpecialisationsCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCarriereSpecialisationsCorresp localInstance = (EOCarriereSpecialisationsCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCarriereSpecialisationsCorresp.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisationsCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCarriereSpecialisationsCorresp.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisationsCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations carriereSpecialisations() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations)storedValueForKey("carriereSpecialisations");
  }

  public void setCarriereSpecialisationsRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations value) {
    if (_EOCarriereSpecialisationsCorresp.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisationsCorresp.LOG.debug("updating carriereSpecialisations from " + carriereSpecialisations() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations oldValue = carriereSpecialisations();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "carriereSpecialisations");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "carriereSpecialisations");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriereSpecialisations carriereSpecialisationsMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriereSpecialisations)storedValueForKey("carriereSpecialisationsMangue");
  }

  public void setCarriereSpecialisationsMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriereSpecialisations value) {
    if (_EOCarriereSpecialisationsCorresp.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisationsCorresp.LOG.debug("updating carriereSpecialisationsMangue from " + carriereSpecialisationsMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriereSpecialisations oldValue = carriereSpecialisationsMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "carriereSpecialisationsMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "carriereSpecialisationsMangue");
    }
  }
  

  public static EOCarriereSpecialisationsCorresp createCarriereSpecialisationsCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations carriereSpecialisations, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriereSpecialisations carriereSpecialisationsMangue) {
    EOCarriereSpecialisationsCorresp eo = (EOCarriereSpecialisationsCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCarriereSpecialisationsCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCarriereSpecialisationsRelationship(carriereSpecialisations);
    eo.setCarriereSpecialisationsMangueRelationship(carriereSpecialisationsMangue);
    return eo;
  }

  public static NSArray<EOCarriereSpecialisationsCorresp> fetchAllCarriereSpecialisationsCorresps(EOEditingContext editingContext) {
    return _EOCarriereSpecialisationsCorresp.fetchAllCarriereSpecialisationsCorresps(editingContext, null);
  }

  public static NSArray<EOCarriereSpecialisationsCorresp> fetchAllCarriereSpecialisationsCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCarriereSpecialisationsCorresp.fetchCarriereSpecialisationsCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCarriereSpecialisationsCorresp> fetchCarriereSpecialisationsCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCarriereSpecialisationsCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCarriereSpecialisationsCorresp> eoObjects = (NSArray<EOCarriereSpecialisationsCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCarriereSpecialisationsCorresp fetchCarriereSpecialisationsCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereSpecialisationsCorresp.fetchCarriereSpecialisationsCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriereSpecialisationsCorresp fetchCarriereSpecialisationsCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCarriereSpecialisationsCorresp> eoObjects = _EOCarriereSpecialisationsCorresp.fetchCarriereSpecialisationsCorresps(editingContext, qualifier, null);
    EOCarriereSpecialisationsCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCarriereSpecialisationsCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CarriereSpecialisationsCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriereSpecialisationsCorresp fetchRequiredCarriereSpecialisationsCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereSpecialisationsCorresp.fetchRequiredCarriereSpecialisationsCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriereSpecialisationsCorresp fetchRequiredCarriereSpecialisationsCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCarriereSpecialisationsCorresp eoObject = _EOCarriereSpecialisationsCorresp.fetchCarriereSpecialisationsCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CarriereSpecialisationsCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriereSpecialisationsCorresp localInstanceIn(EOEditingContext editingContext, EOCarriereSpecialisationsCorresp eo) {
    EOCarriereSpecialisationsCorresp localInstance = (eo == null) ? null : (EOCarriereSpecialisationsCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
