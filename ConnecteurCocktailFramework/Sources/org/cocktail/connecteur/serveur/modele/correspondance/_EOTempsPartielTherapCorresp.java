// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTempsPartielTherapCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTempsPartielTherapCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "TempsPartielTherapCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TEMPS_PARTIEL_THERAP_KEY = "tempsPartielTherap";
	public static final String TEMPS_PARTIEL_THERAP_MANGUE_KEY = "tempsPartielTherapMangue";

  private static Logger LOG = Logger.getLogger(_EOTempsPartielTherapCorresp.class);

  public EOTempsPartielTherapCorresp localInstanceIn(EOEditingContext editingContext) {
    EOTempsPartielTherapCorresp localInstance = (EOTempsPartielTherapCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTempsPartielTherapCorresp.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherapCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTempsPartielTherapCorresp.LOG.isDebugEnabled()) {
    	_EOTempsPartielTherapCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap tempsPartielTherap() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap)storedValueForKey("tempsPartielTherap");
  }

  public void setTempsPartielTherapRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap value) {
    if (_EOTempsPartielTherapCorresp.LOG.isDebugEnabled()) {
      _EOTempsPartielTherapCorresp.LOG.debug("updating tempsPartielTherap from " + tempsPartielTherap() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap oldValue = tempsPartielTherap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartielTherap");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartielTherap");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartielTherap tempsPartielTherapMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartielTherap)storedValueForKey("tempsPartielTherapMangue");
  }

  public void setTempsPartielTherapMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartielTherap value) {
    if (_EOTempsPartielTherapCorresp.LOG.isDebugEnabled()) {
      _EOTempsPartielTherapCorresp.LOG.debug("updating tempsPartielTherapMangue from " + tempsPartielTherapMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartielTherap oldValue = tempsPartielTherapMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartielTherapMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartielTherapMangue");
    }
  }
  

  public static EOTempsPartielTherapCorresp createTempsPartielTherapCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap tempsPartielTherap, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartielTherap tempsPartielTherapMangue) {
    EOTempsPartielTherapCorresp eo = (EOTempsPartielTherapCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOTempsPartielTherapCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setTempsPartielTherapRelationship(tempsPartielTherap);
    eo.setTempsPartielTherapMangueRelationship(tempsPartielTherapMangue);
    return eo;
  }

  public static NSArray<EOTempsPartielTherapCorresp> fetchAllTempsPartielTherapCorresps(EOEditingContext editingContext) {
    return _EOTempsPartielTherapCorresp.fetchAllTempsPartielTherapCorresps(editingContext, null);
  }

  public static NSArray<EOTempsPartielTherapCorresp> fetchAllTempsPartielTherapCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTempsPartielTherapCorresp.fetchTempsPartielTherapCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTempsPartielTherapCorresp> fetchTempsPartielTherapCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTempsPartielTherapCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTempsPartielTherapCorresp> eoObjects = (NSArray<EOTempsPartielTherapCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTempsPartielTherapCorresp fetchTempsPartielTherapCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartielTherapCorresp.fetchTempsPartielTherapCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartielTherapCorresp fetchTempsPartielTherapCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTempsPartielTherapCorresp> eoObjects = _EOTempsPartielTherapCorresp.fetchTempsPartielTherapCorresps(editingContext, qualifier, null);
    EOTempsPartielTherapCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTempsPartielTherapCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TempsPartielTherapCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartielTherapCorresp fetchRequiredTempsPartielTherapCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartielTherapCorresp.fetchRequiredTempsPartielTherapCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartielTherapCorresp fetchRequiredTempsPartielTherapCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTempsPartielTherapCorresp eoObject = _EOTempsPartielTherapCorresp.fetchTempsPartielTherapCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TempsPartielTherapCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartielTherapCorresp localInstanceIn(EOEditingContext editingContext, EOTempsPartielTherapCorresp eo) {
    EOTempsPartielTherapCorresp localInstance = (eo == null) ? null : (EOTempsPartielTherapCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
