// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAdoptionCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAdoptionCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CongeAdoptionCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_ADOPTION_KEY = "congeAdoption";
	public static final String MANGUE_CONGE_ADOPTION_KEY = "mangueCongeAdoption";

  private static Logger LOG = Logger.getLogger(_EOCongeAdoptionCorresp.class);

  public EOCongeAdoptionCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeAdoptionCorresp localInstance = (EOCongeAdoptionCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAdoptionCorresp.LOG.isDebugEnabled()) {
    	_EOCongeAdoptionCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAdoptionCorresp.LOG.isDebugEnabled()) {
    	_EOCongeAdoptionCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption congeAdoption() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption)storedValueForKey("congeAdoption");
  }

  public void setCongeAdoptionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption value) {
    if (_EOCongeAdoptionCorresp.LOG.isDebugEnabled()) {
      _EOCongeAdoptionCorresp.LOG.debug("updating congeAdoption from " + congeAdoption() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption oldValue = congeAdoption();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAdoption");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAdoption");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption mangueCongeAdoption() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption)storedValueForKey("mangueCongeAdoption");
  }

  public void setMangueCongeAdoptionRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption value) {
    if (_EOCongeAdoptionCorresp.LOG.isDebugEnabled()) {
      _EOCongeAdoptionCorresp.LOG.debug("updating mangueCongeAdoption from " + mangueCongeAdoption() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption oldValue = mangueCongeAdoption();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeAdoption");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeAdoption");
    }
  }
  

  public static EOCongeAdoptionCorresp createCongeAdoptionCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption congeAdoption, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption mangueCongeAdoption) {
    EOCongeAdoptionCorresp eo = (EOCongeAdoptionCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAdoptionCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeAdoptionRelationship(congeAdoption);
    eo.setMangueCongeAdoptionRelationship(mangueCongeAdoption);
    return eo;
  }

  public static NSArray<EOCongeAdoptionCorresp> fetchAllCongeAdoptionCorresps(EOEditingContext editingContext) {
    return _EOCongeAdoptionCorresp.fetchAllCongeAdoptionCorresps(editingContext, null);
  }

  public static NSArray<EOCongeAdoptionCorresp> fetchAllCongeAdoptionCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAdoptionCorresp.fetchCongeAdoptionCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAdoptionCorresp> fetchCongeAdoptionCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAdoptionCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAdoptionCorresp> eoObjects = (NSArray<EOCongeAdoptionCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAdoptionCorresp fetchCongeAdoptionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAdoptionCorresp.fetchCongeAdoptionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAdoptionCorresp fetchCongeAdoptionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAdoptionCorresp> eoObjects = _EOCongeAdoptionCorresp.fetchCongeAdoptionCorresps(editingContext, qualifier, null);
    EOCongeAdoptionCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAdoptionCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAdoptionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAdoptionCorresp fetchRequiredCongeAdoptionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAdoptionCorresp.fetchRequiredCongeAdoptionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAdoptionCorresp fetchRequiredCongeAdoptionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAdoptionCorresp eoObject = _EOCongeAdoptionCorresp.fetchCongeAdoptionCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAdoptionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAdoptionCorresp localInstanceIn(EOEditingContext editingContext, EOCongeAdoptionCorresp eo) {
    EOCongeAdoptionCorresp localInstance = (eo == null) ? null : (EOCongeAdoptionCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
