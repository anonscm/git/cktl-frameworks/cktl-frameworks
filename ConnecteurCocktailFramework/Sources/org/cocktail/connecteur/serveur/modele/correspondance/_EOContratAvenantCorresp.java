// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOContratAvenantCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOContratAvenantCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "ContratAvenantCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONTRAT_AVENANT_KEY = "contratAvenant";
	public static final String CONTRAT_AVENANT_MANGUE_KEY = "contratAvenantMangue";

  private static Logger LOG = Logger.getLogger(_EOContratAvenantCorresp.class);

  public EOContratAvenantCorresp localInstanceIn(EOEditingContext editingContext) {
    EOContratAvenantCorresp localInstance = (EOContratAvenantCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOContratAvenantCorresp.LOG.isDebugEnabled()) {
    	_EOContratAvenantCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOContratAvenantCorresp.LOG.isDebugEnabled()) {
    	_EOContratAvenantCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant contratAvenant() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant)storedValueForKey("contratAvenant");
  }

  public void setContratAvenantRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant value) {
    if (_EOContratAvenantCorresp.LOG.isDebugEnabled()) {
      _EOContratAvenantCorresp.LOG.debug("updating contratAvenant from " + contratAvenant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant oldValue = contratAvenant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contratAvenant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contratAvenant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratAvenant contratAvenantMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratAvenant)storedValueForKey("contratAvenantMangue");
  }

  public void setContratAvenantMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratAvenant value) {
    if (_EOContratAvenantCorresp.LOG.isDebugEnabled()) {
      _EOContratAvenantCorresp.LOG.debug("updating contratAvenantMangue from " + contratAvenantMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratAvenant oldValue = contratAvenantMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contratAvenantMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contratAvenantMangue");
    }
  }
  

  public static EOContratAvenantCorresp createContratAvenantCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant contratAvenant, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratAvenant contratAvenantMangue) {
    EOContratAvenantCorresp eo = (EOContratAvenantCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOContratAvenantCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setContratAvenantRelationship(contratAvenant);
    eo.setContratAvenantMangueRelationship(contratAvenantMangue);
    return eo;
  }

  public static NSArray<EOContratAvenantCorresp> fetchAllContratAvenantCorresps(EOEditingContext editingContext) {
    return _EOContratAvenantCorresp.fetchAllContratAvenantCorresps(editingContext, null);
  }

  public static NSArray<EOContratAvenantCorresp> fetchAllContratAvenantCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOContratAvenantCorresp.fetchContratAvenantCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOContratAvenantCorresp> fetchContratAvenantCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOContratAvenantCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOContratAvenantCorresp> eoObjects = (NSArray<EOContratAvenantCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOContratAvenantCorresp fetchContratAvenantCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratAvenantCorresp.fetchContratAvenantCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratAvenantCorresp fetchContratAvenantCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOContratAvenantCorresp> eoObjects = _EOContratAvenantCorresp.fetchContratAvenantCorresps(editingContext, qualifier, null);
    EOContratAvenantCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOContratAvenantCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ContratAvenantCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratAvenantCorresp fetchRequiredContratAvenantCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratAvenantCorresp.fetchRequiredContratAvenantCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratAvenantCorresp fetchRequiredContratAvenantCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOContratAvenantCorresp eoObject = _EOContratAvenantCorresp.fetchContratAvenantCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ContratAvenantCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratAvenantCorresp localInstanceIn(EOEditingContext editingContext, EOContratAvenantCorresp eo) {
    EOContratAvenantCorresp localInstance = (eo == null) ? null : (EOContratAvenantCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
