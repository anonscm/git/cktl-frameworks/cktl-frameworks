package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOHebergeCorresp extends _EOHebergeCorresp {
	@Override
	public void supprimerRelations() {
		setHebergeRelationship(null);
		setMangueHebergeRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return HEBERGE_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_HEBERGE_KEY;
	}
}
