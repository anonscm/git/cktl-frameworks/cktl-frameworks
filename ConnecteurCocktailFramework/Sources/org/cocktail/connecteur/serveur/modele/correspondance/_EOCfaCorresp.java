// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCfaCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCfaCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CfaCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CFA_KEY = "cfa";
	public static final String MANGUE_CFA_KEY = "mangueCfa";

  private static Logger LOG = Logger.getLogger(_EOCfaCorresp.class);

  public EOCfaCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCfaCorresp localInstance = (EOCfaCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCfaCorresp.LOG.isDebugEnabled()) {
    	_EOCfaCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCfaCorresp.LOG.isDebugEnabled()) {
    	_EOCfaCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCfa cfa() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCfa)storedValueForKey("cfa");
  }

  public void setCfaRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCfa value) {
    if (_EOCfaCorresp.LOG.isDebugEnabled()) {
      _EOCfaCorresp.LOG.debug("updating cfa from " + cfa() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCfa oldValue = cfa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cfa");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cfa");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCfa mangueCfa() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCfa)storedValueForKey("mangueCfa");
  }

  public void setMangueCfaRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCfa value) {
    if (_EOCfaCorresp.LOG.isDebugEnabled()) {
      _EOCfaCorresp.LOG.debug("updating mangueCfa from " + mangueCfa() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCfa oldValue = mangueCfa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCfa");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCfa");
    }
  }
  

  public static EOCfaCorresp createCfaCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCfa cfa, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCfa mangueCfa) {
    EOCfaCorresp eo = (EOCfaCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCfaCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCfaRelationship(cfa);
    eo.setMangueCfaRelationship(mangueCfa);
    return eo;
  }

  public static NSArray<EOCfaCorresp> fetchAllCfaCorresps(EOEditingContext editingContext) {
    return _EOCfaCorresp.fetchAllCfaCorresps(editingContext, null);
  }

  public static NSArray<EOCfaCorresp> fetchAllCfaCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCfaCorresp.fetchCfaCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCfaCorresp> fetchCfaCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCfaCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCfaCorresp> eoObjects = (NSArray<EOCfaCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCfaCorresp fetchCfaCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCfaCorresp.fetchCfaCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCfaCorresp fetchCfaCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCfaCorresp> eoObjects = _EOCfaCorresp.fetchCfaCorresps(editingContext, qualifier, null);
    EOCfaCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCfaCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CfaCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCfaCorresp fetchRequiredCfaCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCfaCorresp.fetchRequiredCfaCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCfaCorresp fetchRequiredCfaCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCfaCorresp eoObject = _EOCfaCorresp.fetchCfaCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CfaCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCfaCorresp localInstanceIn(EOEditingContext editingContext, EOCfaCorresp eo) {
    EOCfaCorresp localInstance = (eo == null) ? null : (EOCfaCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
