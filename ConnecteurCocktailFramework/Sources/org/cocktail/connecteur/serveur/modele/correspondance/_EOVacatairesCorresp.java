// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVacatairesCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVacatairesCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "VacatairesCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String MANGUE_VACATAIRES_KEY = "mangueVacataires";
	public static final String VACATAIRES_KEY = "vacataires";

  private static Logger LOG = Logger.getLogger(_EOVacatairesCorresp.class);

  public EOVacatairesCorresp localInstanceIn(EOEditingContext editingContext) {
    EOVacatairesCorresp localInstance = (EOVacatairesCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOVacatairesCorresp.LOG.isDebugEnabled()) {
    	_EOVacatairesCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOVacatairesCorresp.LOG.isDebugEnabled()) {
    	_EOVacatairesCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires mangueVacataires() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires)storedValueForKey("mangueVacataires");
  }

  public void setMangueVacatairesRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires value) {
    if (_EOVacatairesCorresp.LOG.isDebugEnabled()) {
      _EOVacatairesCorresp.LOG.debug("updating mangueVacataires from " + mangueVacataires() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires oldValue = mangueVacataires();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueVacataires");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueVacataires");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires vacataires() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires)storedValueForKey("vacataires");
  }

  public void setVacatairesRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires value) {
    if (_EOVacatairesCorresp.LOG.isDebugEnabled()) {
      _EOVacatairesCorresp.LOG.debug("updating vacataires from " + vacataires() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires oldValue = vacataires();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vacataires");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vacataires");
    }
  }
  

  public static EOVacatairesCorresp createVacatairesCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires mangueVacataires, org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires vacataires) {
    EOVacatairesCorresp eo = (EOVacatairesCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOVacatairesCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setMangueVacatairesRelationship(mangueVacataires);
    eo.setVacatairesRelationship(vacataires);
    return eo;
  }

  public static NSArray<EOVacatairesCorresp> fetchAllVacatairesCorresps(EOEditingContext editingContext) {
    return _EOVacatairesCorresp.fetchAllVacatairesCorresps(editingContext, null);
  }

  public static NSArray<EOVacatairesCorresp> fetchAllVacatairesCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVacatairesCorresp.fetchVacatairesCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVacatairesCorresp> fetchVacatairesCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVacatairesCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVacatairesCorresp> eoObjects = (NSArray<EOVacatairesCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVacatairesCorresp fetchVacatairesCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacatairesCorresp.fetchVacatairesCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacatairesCorresp fetchVacatairesCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVacatairesCorresp> eoObjects = _EOVacatairesCorresp.fetchVacatairesCorresps(editingContext, qualifier, null);
    EOVacatairesCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVacatairesCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VacatairesCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacatairesCorresp fetchRequiredVacatairesCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacatairesCorresp.fetchRequiredVacatairesCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacatairesCorresp fetchRequiredVacatairesCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVacatairesCorresp eoObject = _EOVacatairesCorresp.fetchVacatairesCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VacatairesCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacatairesCorresp localInstanceIn(EOEditingContext editingContext, EOVacatairesCorresp eo) {
    EOVacatairesCorresp localInstance = (eo == null) ? null : (EOVacatairesCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
