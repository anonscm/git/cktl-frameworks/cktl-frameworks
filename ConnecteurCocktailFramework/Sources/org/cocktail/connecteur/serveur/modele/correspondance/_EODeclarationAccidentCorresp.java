// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODeclarationAccidentCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODeclarationAccidentCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "DeclarationAccidentCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_DECLARATION_ACCIDENT_KEY = "toDeclarationAccident";
	public static final String TO_MANGUE_DECLARATION_ACCIDENT_KEY = "toMangueDeclarationAccident";

  private static Logger LOG = Logger.getLogger(_EODeclarationAccidentCorresp.class);

  public EODeclarationAccidentCorresp localInstanceIn(EOEditingContext editingContext) {
    EODeclarationAccidentCorresp localInstance = (EODeclarationAccidentCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODeclarationAccidentCorresp.LOG.isDebugEnabled()) {
    	_EODeclarationAccidentCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODeclarationAccidentCorresp.LOG.isDebugEnabled()) {
    	_EODeclarationAccidentCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident toDeclarationAccident() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident)storedValueForKey("toDeclarationAccident");
  }

  public void setToDeclarationAccidentRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident value) {
    if (_EODeclarationAccidentCorresp.LOG.isDebugEnabled()) {
      _EODeclarationAccidentCorresp.LOG.debug("updating toDeclarationAccident from " + toDeclarationAccident() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident oldValue = toDeclarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDeclarationAccident");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDeclarationAccident");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident toMangueDeclarationAccident() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident)storedValueForKey("toMangueDeclarationAccident");
  }

  public void setToMangueDeclarationAccidentRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident value) {
    if (_EODeclarationAccidentCorresp.LOG.isDebugEnabled()) {
      _EODeclarationAccidentCorresp.LOG.debug("updating toMangueDeclarationAccident from " + toMangueDeclarationAccident() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident oldValue = toMangueDeclarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueDeclarationAccident");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueDeclarationAccident");
    }
  }
  

  public static EODeclarationAccidentCorresp createDeclarationAccidentCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident toDeclarationAccident, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident toMangueDeclarationAccident) {
    EODeclarationAccidentCorresp eo = (EODeclarationAccidentCorresp) EOUtilities.createAndInsertInstance(editingContext, _EODeclarationAccidentCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToDeclarationAccidentRelationship(toDeclarationAccident);
    eo.setToMangueDeclarationAccidentRelationship(toMangueDeclarationAccident);
    return eo;
  }

  public static NSArray<EODeclarationAccidentCorresp> fetchAllDeclarationAccidentCorresps(EOEditingContext editingContext) {
    return _EODeclarationAccidentCorresp.fetchAllDeclarationAccidentCorresps(editingContext, null);
  }

  public static NSArray<EODeclarationAccidentCorresp> fetchAllDeclarationAccidentCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODeclarationAccidentCorresp.fetchDeclarationAccidentCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EODeclarationAccidentCorresp> fetchDeclarationAccidentCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODeclarationAccidentCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODeclarationAccidentCorresp> eoObjects = (NSArray<EODeclarationAccidentCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODeclarationAccidentCorresp fetchDeclarationAccidentCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODeclarationAccidentCorresp.fetchDeclarationAccidentCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODeclarationAccidentCorresp fetchDeclarationAccidentCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODeclarationAccidentCorresp> eoObjects = _EODeclarationAccidentCorresp.fetchDeclarationAccidentCorresps(editingContext, qualifier, null);
    EODeclarationAccidentCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODeclarationAccidentCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one DeclarationAccidentCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODeclarationAccidentCorresp fetchRequiredDeclarationAccidentCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODeclarationAccidentCorresp.fetchRequiredDeclarationAccidentCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODeclarationAccidentCorresp fetchRequiredDeclarationAccidentCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EODeclarationAccidentCorresp eoObject = _EODeclarationAccidentCorresp.fetchDeclarationAccidentCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no DeclarationAccidentCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODeclarationAccidentCorresp localInstanceIn(EOEditingContext editingContext, EODeclarationAccidentCorresp eo) {
    EODeclarationAccidentCorresp localInstance = (eo == null) ? null : (EODeclarationAccidentCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
