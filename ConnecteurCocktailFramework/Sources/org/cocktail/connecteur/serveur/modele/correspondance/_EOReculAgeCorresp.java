// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOReculAgeCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOReculAgeCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "ReculAgeCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_MANGUE_RECUL_AGE_KEY = "toMangueReculAge";
	public static final String TO_RECUL_AGE_KEY = "toReculAge";

  private static Logger LOG = Logger.getLogger(_EOReculAgeCorresp.class);

  public EOReculAgeCorresp localInstanceIn(EOEditingContext editingContext) {
    EOReculAgeCorresp localInstance = (EOReculAgeCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOReculAgeCorresp.LOG.isDebugEnabled()) {
    	_EOReculAgeCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOReculAgeCorresp.LOG.isDebugEnabled()) {
    	_EOReculAgeCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReculAge toMangueReculAge() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReculAge)storedValueForKey("toMangueReculAge");
  }

  public void setToMangueReculAgeRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReculAge value) {
    if (_EOReculAgeCorresp.LOG.isDebugEnabled()) {
      _EOReculAgeCorresp.LOG.debug("updating toMangueReculAge from " + toMangueReculAge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReculAge oldValue = toMangueReculAge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueReculAge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueReculAge");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge toReculAge() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge)storedValueForKey("toReculAge");
  }

  public void setToReculAgeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge value) {
    if (_EOReculAgeCorresp.LOG.isDebugEnabled()) {
      _EOReculAgeCorresp.LOG.debug("updating toReculAge from " + toReculAge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge oldValue = toReculAge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReculAge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReculAge");
    }
  }
  

  public static EOReculAgeCorresp createReculAgeCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReculAge toMangueReculAge, org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge toReculAge) {
    EOReculAgeCorresp eo = (EOReculAgeCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOReculAgeCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToMangueReculAgeRelationship(toMangueReculAge);
    eo.setToReculAgeRelationship(toReculAge);
    return eo;
  }

  public static NSArray<EOReculAgeCorresp> fetchAllReculAgeCorresps(EOEditingContext editingContext) {
    return _EOReculAgeCorresp.fetchAllReculAgeCorresps(editingContext, null);
  }

  public static NSArray<EOReculAgeCorresp> fetchAllReculAgeCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOReculAgeCorresp.fetchReculAgeCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOReculAgeCorresp> fetchReculAgeCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOReculAgeCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOReculAgeCorresp> eoObjects = (NSArray<EOReculAgeCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOReculAgeCorresp fetchReculAgeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReculAgeCorresp.fetchReculAgeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReculAgeCorresp fetchReculAgeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOReculAgeCorresp> eoObjects = _EOReculAgeCorresp.fetchReculAgeCorresps(editingContext, qualifier, null);
    EOReculAgeCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOReculAgeCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ReculAgeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReculAgeCorresp fetchRequiredReculAgeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReculAgeCorresp.fetchRequiredReculAgeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReculAgeCorresp fetchRequiredReculAgeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOReculAgeCorresp eoObject = _EOReculAgeCorresp.fetchReculAgeCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ReculAgeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReculAgeCorresp localInstanceIn(EOEditingContext editingContext, EOReculAgeCorresp eo) {
    EOReculAgeCorresp localInstance = (eo == null) ? null : (EOReculAgeCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
