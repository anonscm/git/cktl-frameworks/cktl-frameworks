// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCrctDetailCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCrctDetailCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CrctDetailCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CRCT_DETAIL_KEY = "crctDetail";
	public static final String MANGUE_CRCT_DETAIL_KEY = "mangueCrctDetail";

  private static Logger LOG = Logger.getLogger(_EOCrctDetailCorresp.class);

  public EOCrctDetailCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCrctDetailCorresp localInstance = (EOCrctDetailCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCrctDetailCorresp.LOG.isDebugEnabled()) {
    	_EOCrctDetailCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCrctDetailCorresp.LOG.isDebugEnabled()) {
    	_EOCrctDetailCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail crctDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail)storedValueForKey("crctDetail");
  }

  public void setCrctDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail value) {
    if (_EOCrctDetailCorresp.LOG.isDebugEnabled()) {
      _EOCrctDetailCorresp.LOG.debug("updating crctDetail from " + crctDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail oldValue = crctDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "crctDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "crctDetail");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrctDetail mangueCrctDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrctDetail)storedValueForKey("mangueCrctDetail");
  }

  public void setMangueCrctDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrctDetail value) {
    if (_EOCrctDetailCorresp.LOG.isDebugEnabled()) {
      _EOCrctDetailCorresp.LOG.debug("updating mangueCrctDetail from " + mangueCrctDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrctDetail oldValue = mangueCrctDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCrctDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCrctDetail");
    }
  }
  

  public static EOCrctDetailCorresp createCrctDetailCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail crctDetail, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrctDetail mangueCrctDetail) {
    EOCrctDetailCorresp eo = (EOCrctDetailCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCrctDetailCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCrctDetailRelationship(crctDetail);
    eo.setMangueCrctDetailRelationship(mangueCrctDetail);
    return eo;
  }

  public static NSArray<EOCrctDetailCorresp> fetchAllCrctDetailCorresps(EOEditingContext editingContext) {
    return _EOCrctDetailCorresp.fetchAllCrctDetailCorresps(editingContext, null);
  }

  public static NSArray<EOCrctDetailCorresp> fetchAllCrctDetailCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCrctDetailCorresp.fetchCrctDetailCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCrctDetailCorresp> fetchCrctDetailCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCrctDetailCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCrctDetailCorresp> eoObjects = (NSArray<EOCrctDetailCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCrctDetailCorresp fetchCrctDetailCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrctDetailCorresp.fetchCrctDetailCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrctDetailCorresp fetchCrctDetailCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCrctDetailCorresp> eoObjects = _EOCrctDetailCorresp.fetchCrctDetailCorresps(editingContext, qualifier, null);
    EOCrctDetailCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCrctDetailCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CrctDetailCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrctDetailCorresp fetchRequiredCrctDetailCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrctDetailCorresp.fetchRequiredCrctDetailCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrctDetailCorresp fetchRequiredCrctDetailCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCrctDetailCorresp eoObject = _EOCrctDetailCorresp.fetchCrctDetailCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CrctDetailCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrctDetailCorresp localInstanceIn(EOEditingContext editingContext, EOCrctDetailCorresp eo) {
    EOCrctDetailCorresp localInstance = (eo == null) ? null : (EOCrctDetailCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
