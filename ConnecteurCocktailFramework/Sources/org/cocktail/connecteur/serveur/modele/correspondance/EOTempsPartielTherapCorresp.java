package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EOTempsPartielTherapCorresp extends _EOTempsPartielTherapCorresp {
  private static Logger log = Logger.getLogger(EOTempsPartielTherapCorresp.class);

@Override
public void supprimerRelations() {
	setTempsPartielTherapRelationship(null);
	setTempsPartielTherapMangueRelationship(null);
}

public String nomRelationBaseDestinataire() {
	return TEMPS_PARTIEL_THERAP_MANGUE_KEY;
}
public String nomRelationBaseImport() {
	return TEMPS_PARTIEL_THERAP_KEY;
}

}
