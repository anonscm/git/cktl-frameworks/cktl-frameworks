// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPasseCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPasseCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "PasseCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String PASSE_KEY = "passe";
	public static final String PASSE_MANGUE_KEY = "passeMangue";

  private static Logger LOG = Logger.getLogger(_EOPasseCorresp.class);

  public EOPasseCorresp localInstanceIn(EOEditingContext editingContext) {
    EOPasseCorresp localInstance = (EOPasseCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPasseCorresp.LOG.isDebugEnabled()) {
    	_EOPasseCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPasseCorresp.LOG.isDebugEnabled()) {
    	_EOPasseCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOPasse passe() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPasse)storedValueForKey("passe");
  }

  public void setPasseRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPasse value) {
    if (_EOPasseCorresp.LOG.isDebugEnabled()) {
      _EOPasseCorresp.LOG.debug("updating passe from " + passe() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPasse oldValue = passe();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "passe");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "passe");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePasse passeMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePasse)storedValueForKey("passeMangue");
  }

  public void setPasseMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePasse value) {
    if (_EOPasseCorresp.LOG.isDebugEnabled()) {
      _EOPasseCorresp.LOG.debug("updating passeMangue from " + passeMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePasse oldValue = passeMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "passeMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "passeMangue");
    }
  }
  

  public static EOPasseCorresp createPasseCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOPasse passe, org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePasse passeMangue) {
    EOPasseCorresp eo = (EOPasseCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOPasseCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setPasseRelationship(passe);
    eo.setPasseMangueRelationship(passeMangue);
    return eo;
  }

  public static NSArray<EOPasseCorresp> fetchAllPasseCorresps(EOEditingContext editingContext) {
    return _EOPasseCorresp.fetchAllPasseCorresps(editingContext, null);
  }

  public static NSArray<EOPasseCorresp> fetchAllPasseCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPasseCorresp.fetchPasseCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPasseCorresp> fetchPasseCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPasseCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPasseCorresp> eoObjects = (NSArray<EOPasseCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPasseCorresp fetchPasseCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPasseCorresp.fetchPasseCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPasseCorresp fetchPasseCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPasseCorresp> eoObjects = _EOPasseCorresp.fetchPasseCorresps(editingContext, qualifier, null);
    EOPasseCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPasseCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PasseCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPasseCorresp fetchRequiredPasseCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPasseCorresp.fetchRequiredPasseCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPasseCorresp fetchRequiredPasseCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPasseCorresp eoObject = _EOPasseCorresp.fetchPasseCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PasseCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPasseCorresp localInstanceIn(EOEditingContext editingContext, EOPasseCorresp eo) {
    EOPasseCorresp localInstance = (eo == null) ? null : (EOPasseCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
