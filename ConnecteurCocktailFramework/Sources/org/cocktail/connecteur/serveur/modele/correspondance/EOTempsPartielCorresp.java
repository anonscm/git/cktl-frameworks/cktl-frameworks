/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel;
import org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel;
import org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartielTherap;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;


public class EOTempsPartielCorresp extends _EOTempsPartielCorresp {

    public EOTempsPartielCorresp() {
        super();
    }


	public String nomRelationBaseDestinataire() {
		return TEMPS_PARTIEL_MANGUE_KEY;
	}
	public String nomRelationBaseImport() {
		return TEMPS_PARTIEL_KEY;
	}
	public void supprimerRelations() {
		setTempsPartielRelationship(null);
		setTempsPartielMangueRelationship(null);
	}
	
	// Méthodes statiques
	/** retourne le temps partiel d'import associe a l'id passee en parametre. On commence
	 * par chercher dans l'import courant pour etre sur de retourner le dernier temps partiel, sinon
	 * on recherche dans les correspondances */
	public static EOTempsPartiel tpsPourSourceEtCgId(EOEditingContext editingContext,Number idSource,Number tpSource) {
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(tpSource);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTempsPartiel.ID_SOURCE_KEY + " = %@ AND "+EOTempsPartiel.TP_SOURCE_KEY+ " = %@ AND temImport = %@ AND statut <> 'A'",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (EOTempsPartiel.ENTITY_NAME, myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOTempsPartiel)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e1) {
			// Rechercher dans les correspondances si le cld n'a pas déjà été importé
			args = new NSMutableArray(idSource);
			args.addObject(tpSource);
			myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTempsPartielCorresp.TEMPS_PARTIEL_KEY+"."+EOTempsPartiel.ID_SOURCE_KEY+" = %@ AND "+EOTempsPartielCorresp.TEMPS_PARTIEL_KEY+"."+EOTempsPartiel.TP_SOURCE_KEY+" = %@",args);
			myFetch = new EOFetchSpecification (EOTempsPartielCorresp.ENTITY_NAME, myQualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			try {
				return ((EOTempsPartielCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0)).tempsPartiel();
			} catch (Exception e) {
				return null;

			}
		}
	}
	/** retourne le temps partiel de Mangue assoc&iee au temps partiel d'import
	 * @param editingContext editing context 
	 * @param tpSource identifiant du temps partiel
	 * @param idSource identifiant de l'individu
	 **/
	public static EOMangueTempsPartiel tpsMangue(EOEditingContext editingContext,Number tpSource, Number idSource) {
		// Rechercher dans les correspondances si le cld n'a pas déjà été importé
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(tpSource);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTempsPartielCorresp.TEMPS_PARTIEL_KEY+"."+EOTempsPartiel.ID_SOURCE_KEY+ " = %@ AND "+EOTempsPartielCorresp.TEMPS_PARTIEL_KEY+"."+EOTempsPartiel.TP_SOURCE_KEY+" = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (EOTempsPartielCorresp.ENTITY_NAME, myQualifier,null);		
		try {
			EOTempsPartielCorresp corresp = (EOTempsPartielCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
			return corresp.tempsPartielMangue();
		} catch (Exception e) {
			return null;
		}
	}

	public static EOMangueTempsPartiel tpsMangue(EOEditingContext editingContext, EOTempsPartiel tempsPartielImport) {
		if (tempsPartielImport==null)
			return null;
		EOTempsPartielCorresp corresp=fetchTempsPartielCorresp(editingContext, TEMPS_PARTIEL_KEY, tempsPartielImport);
		if (corresp==null)
			return null;
		return corresp.tempsPartielMangue();
	}
}
