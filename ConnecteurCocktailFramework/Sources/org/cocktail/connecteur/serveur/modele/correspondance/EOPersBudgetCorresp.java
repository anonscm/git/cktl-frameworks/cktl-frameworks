package org.cocktail.connecteur.serveur.modele.correspondance;

import ognl.SetPropertyAccessor;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class EOPersBudgetCorresp extends _EOPersBudgetCorresp {
	@Override
	public void supprimerRelations() {
		setToPersBudgetRelationship(null);
		setToManguePersBudgetRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return TO_PERS_BUDGET_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_PERS_BUDGET_KEY;
	}

	public static EOManguePersBudget persBudgetMangue(EOEditingContext editingContext, EOPersBudget persBudget) {
		EOManguePersBudget resultat = null;

		EOPersBudgetCorresp corresp = fetchPersBudgetCorresp(editingContext, TO_PERS_BUDGET_KEY, persBudget);
		if (corresp != null)
			resultat = corresp.toManguePersBudget();

		return resultat;
	}
}
