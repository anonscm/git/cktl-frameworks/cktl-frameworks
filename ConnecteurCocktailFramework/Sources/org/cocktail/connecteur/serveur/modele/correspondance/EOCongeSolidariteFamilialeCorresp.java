package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

/**.
 * Classe gérant les correspondances
 * pour les Congés de Solidarité Familiale d'un individu
 * @author alainmalaplate
 *
 */
public class EOCongeSolidariteFamilialeCorresp extends _EOCongeSolidariteFamilialeCorresp {

	private static final long serialVersionUID = -8884677442149260231L;
	private static Logger log = Logger.getLogger(EOCongeSolidariteFamilialeCorresp.class);

	public EOCongeSolidariteFamilialeCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_SOLIDARITE_FAMILIALE_KEY;
	}

	public String nomRelationBaseImport() {
		return CONGE_SOLIDARITE_FAMILIALE_KEY;
	}

	public void supprimerRelations() {
		setCongeSolidariteFamilialeRelationship(null);
		setMangueCongeSolidariteFamilialeRelationship(null);
	}
}
