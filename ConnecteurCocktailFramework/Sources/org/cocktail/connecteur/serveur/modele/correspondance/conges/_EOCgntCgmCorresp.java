// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntCgmCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntCgmCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CgntCgmCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CGNT_CGM_KEY = "cgntCgm";
	public static final String CGNT_CGM_MANGUE_KEY = "cgntCgmMangue";

  private static Logger LOG = Logger.getLogger(_EOCgntCgmCorresp.class);

  public EOCgntCgmCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCgntCgmCorresp localInstance = (EOCgntCgmCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntCgmCorresp.LOG.isDebugEnabled()) {
    	_EOCgntCgmCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntCgmCorresp.LOG.isDebugEnabled()) {
    	_EOCgntCgmCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm cgntCgm() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm)storedValueForKey("cgntCgm");
  }

  public void setCgntCgmRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm value) {
    if (_EOCgntCgmCorresp.LOG.isDebugEnabled()) {
      _EOCgntCgmCorresp.LOG.debug("updating cgntCgm from " + cgntCgm() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm oldValue = cgntCgm();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntCgm");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntCgm");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntCgm cgntCgmMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntCgm)storedValueForKey("cgntCgmMangue");
  }

  public void setCgntCgmMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntCgm value) {
    if (_EOCgntCgmCorresp.LOG.isDebugEnabled()) {
      _EOCgntCgmCorresp.LOG.debug("updating cgntCgmMangue from " + cgntCgmMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntCgm oldValue = cgntCgmMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntCgmMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntCgmMangue");
    }
  }
  

  public static EOCgntCgmCorresp createCgntCgmCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCgntCgm cgntCgm, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntCgm cgntCgmMangue) {
    EOCgntCgmCorresp eo = (EOCgntCgmCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCgntCgmCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCgntCgmRelationship(cgntCgm);
    eo.setCgntCgmMangueRelationship(cgntCgmMangue);
    return eo;
  }

  public static NSArray<EOCgntCgmCorresp> fetchAllCgntCgmCorresps(EOEditingContext editingContext) {
    return _EOCgntCgmCorresp.fetchAllCgntCgmCorresps(editingContext, null);
  }

  public static NSArray<EOCgntCgmCorresp> fetchAllCgntCgmCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntCgmCorresp.fetchCgntCgmCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntCgmCorresp> fetchCgntCgmCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntCgmCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntCgmCorresp> eoObjects = (NSArray<EOCgntCgmCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntCgmCorresp fetchCgntCgmCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntCgmCorresp.fetchCgntCgmCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntCgmCorresp fetchCgntCgmCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntCgmCorresp> eoObjects = _EOCgntCgmCorresp.fetchCgntCgmCorresps(editingContext, qualifier, null);
    EOCgntCgmCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntCgmCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntCgmCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntCgmCorresp fetchRequiredCgntCgmCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntCgmCorresp.fetchRequiredCgntCgmCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntCgmCorresp fetchRequiredCgntCgmCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntCgmCorresp eoObject = _EOCgntCgmCorresp.fetchCgntCgmCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntCgmCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntCgmCorresp localInstanceIn(EOEditingContext editingContext, EOCgntCgmCorresp eo) {
    EOCgntCgmCorresp localInstance = (eo == null) ? null : (EOCgntCgmCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
