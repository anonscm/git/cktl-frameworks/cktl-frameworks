// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCompteCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCompteCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CompteCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String COMPTE_KEY = "compte";
	public static final String COMPTE_GRHUM_KEY = "compteGrhum";

  private static Logger LOG = Logger.getLogger(_EOCompteCorresp.class);

  public EOCompteCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCompteCorresp localInstance = (EOCompteCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCompteCorresp.LOG.isDebugEnabled()) {
    	_EOCompteCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCompteCorresp.LOG.isDebugEnabled()) {
    	_EOCompteCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCompte compte() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCompte)storedValueForKey("compte");
  }

  public void setCompteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCompte value) {
    if (_EOCompteCorresp.LOG.isDebugEnabled()) {
      _EOCompteCorresp.LOG.debug("updating compte from " + compte() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCompte oldValue = compte();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "compte");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "compte");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumCompte compteGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumCompte)storedValueForKey("compteGrhum");
  }

  public void setCompteGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumCompte value) {
    if (_EOCompteCorresp.LOG.isDebugEnabled()) {
      _EOCompteCorresp.LOG.debug("updating compteGrhum from " + compteGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumCompte oldValue = compteGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "compteGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "compteGrhum");
    }
  }
  

  public static EOCompteCorresp createCompteCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCompte compte, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumCompte compteGrhum) {
    EOCompteCorresp eo = (EOCompteCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCompteCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCompteRelationship(compte);
    eo.setCompteGrhumRelationship(compteGrhum);
    return eo;
  }

  public static NSArray<EOCompteCorresp> fetchAllCompteCorresps(EOEditingContext editingContext) {
    return _EOCompteCorresp.fetchAllCompteCorresps(editingContext, null);
  }

  public static NSArray<EOCompteCorresp> fetchAllCompteCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCompteCorresp.fetchCompteCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCompteCorresp> fetchCompteCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCompteCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCompteCorresp> eoObjects = (NSArray<EOCompteCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCompteCorresp fetchCompteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCompteCorresp.fetchCompteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCompteCorresp fetchCompteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCompteCorresp> eoObjects = _EOCompteCorresp.fetchCompteCorresps(editingContext, qualifier, null);
    EOCompteCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCompteCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CompteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCompteCorresp fetchRequiredCompteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCompteCorresp.fetchRequiredCompteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCompteCorresp fetchRequiredCompteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCompteCorresp eoObject = _EOCompteCorresp.fetchCompteCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CompteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCompteCorresp localInstanceIn(EOEditingContext editingContext, EOCompteCorresp eo) {
    EOCompteCorresp localInstance = (eo == null) ? null : (EOCompteCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
