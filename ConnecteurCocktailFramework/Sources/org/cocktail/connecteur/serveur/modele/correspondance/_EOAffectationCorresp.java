// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOAffectationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOAffectationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "AffectationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String AFFECTATION_KEY = "affectation";
	public static final String AFFECTATION_MANGUE_KEY = "affectationMangue";

  private static Logger LOG = Logger.getLogger(_EOAffectationCorresp.class);

  public EOAffectationCorresp localInstanceIn(EOEditingContext editingContext) {
    EOAffectationCorresp localInstance = (EOAffectationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOAffectationCorresp.LOG.isDebugEnabled()) {
    	_EOAffectationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOAffectationCorresp.LOG.isDebugEnabled()) {
    	_EOAffectationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation affectation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation)storedValueForKey("affectation");
  }

  public void setAffectationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation value) {
    if (_EOAffectationCorresp.LOG.isDebugEnabled()) {
      _EOAffectationCorresp.LOG.debug("updating affectation from " + affectation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation oldValue = affectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "affectation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "affectation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueAffectation affectationMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueAffectation)storedValueForKey("affectationMangue");
  }

  public void setAffectationMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueAffectation value) {
    if (_EOAffectationCorresp.LOG.isDebugEnabled()) {
      _EOAffectationCorresp.LOG.debug("updating affectationMangue from " + affectationMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueAffectation oldValue = affectationMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "affectationMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "affectationMangue");
    }
  }
  

  public static EOAffectationCorresp createAffectationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation affectation, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueAffectation affectationMangue) {
    EOAffectationCorresp eo = (EOAffectationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOAffectationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setAffectationRelationship(affectation);
    eo.setAffectationMangueRelationship(affectationMangue);
    return eo;
  }

  public static NSArray<EOAffectationCorresp> fetchAllAffectationCorresps(EOEditingContext editingContext) {
    return _EOAffectationCorresp.fetchAllAffectationCorresps(editingContext, null);
  }

  public static NSArray<EOAffectationCorresp> fetchAllAffectationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAffectationCorresp.fetchAffectationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAffectationCorresp> fetchAffectationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAffectationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAffectationCorresp> eoObjects = (NSArray<EOAffectationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAffectationCorresp fetchAffectationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectationCorresp.fetchAffectationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAffectationCorresp fetchAffectationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAffectationCorresp> eoObjects = _EOAffectationCorresp.fetchAffectationCorresps(editingContext, qualifier, null);
    EOAffectationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAffectationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one AffectationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAffectationCorresp fetchRequiredAffectationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectationCorresp.fetchRequiredAffectationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAffectationCorresp fetchRequiredAffectationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAffectationCorresp eoObject = _EOAffectationCorresp.fetchAffectationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no AffectationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAffectationCorresp localInstanceIn(EOEditingContext editingContext, EOAffectationCorresp eo) {
    EOAffectationCorresp localInstance = (eo == null) ? null : (EOAffectationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
