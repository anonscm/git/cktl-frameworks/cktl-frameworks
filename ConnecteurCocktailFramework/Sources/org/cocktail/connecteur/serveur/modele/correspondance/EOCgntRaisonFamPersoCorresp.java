package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EOCgntRaisonFamPersoCorresp extends _EOCgntRaisonFamPersoCorresp {
  private static Logger log = Logger.getLogger(EOCgntRaisonFamPersoCorresp.class);
  
  public EOCgntRaisonFamPersoCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CGNT_RAISON_FAM_PERSO_KEY;
	}

	public String nomRelationBaseImport() {
		return CGNT_RAISON_FAM_PERSO_KEY;
	}

	public void supprimerRelations() {
		setCgntRaisonFamPersoRelationship(null);
		setMangueCgntRaisonFamPersoRelationship(null);
	}
  
}
