// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAl4Corresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAl4Corresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongeAl4Corresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_AL4_KEY = "congeAl4";
	public static final String MANGUE_CONGE_AL4_KEY = "mangueCongeAl4";

  private static Logger LOG = Logger.getLogger(_EOCongeAl4Corresp.class);

  public EOCongeAl4Corresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeAl4Corresp localInstance = (EOCongeAl4Corresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAl4Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl4Corresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAl4Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl4Corresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4 congeAl4() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4)storedValueForKey("congeAl4");
  }

  public void setCongeAl4Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4 value) {
    if (_EOCongeAl4Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl4Corresp.LOG.debug("updating congeAl4 from " + congeAl4() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4 oldValue = congeAl4();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl4");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl4");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl4 mangueCongeAl4() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl4)storedValueForKey("mangueCongeAl4");
  }

  public void setMangueCongeAl4Relationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl4 value) {
    if (_EOCongeAl4Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl4Corresp.LOG.debug("updating mangueCongeAl4 from " + mangueCongeAl4() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl4 oldValue = mangueCongeAl4();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeAl4");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeAl4");
    }
  }
  

  public static EOCongeAl4Corresp createCongeAl4Corresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl4 congeAl4, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl4 mangueCongeAl4) {
    EOCongeAl4Corresp eo = (EOCongeAl4Corresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAl4Corresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeAl4Relationship(congeAl4);
    eo.setMangueCongeAl4Relationship(mangueCongeAl4);
    return eo;
  }

  public static NSArray<EOCongeAl4Corresp> fetchAllCongeAl4Corresps(EOEditingContext editingContext) {
    return _EOCongeAl4Corresp.fetchAllCongeAl4Corresps(editingContext, null);
  }

  public static NSArray<EOCongeAl4Corresp> fetchAllCongeAl4Corresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAl4Corresp.fetchCongeAl4Corresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAl4Corresp> fetchCongeAl4Corresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAl4Corresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAl4Corresp> eoObjects = (NSArray<EOCongeAl4Corresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAl4Corresp fetchCongeAl4Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl4Corresp.fetchCongeAl4Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl4Corresp fetchCongeAl4Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAl4Corresp> eoObjects = _EOCongeAl4Corresp.fetchCongeAl4Corresps(editingContext, qualifier, null);
    EOCongeAl4Corresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAl4Corresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAl4Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl4Corresp fetchRequiredCongeAl4Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl4Corresp.fetchRequiredCongeAl4Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl4Corresp fetchRequiredCongeAl4Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAl4Corresp eoObject = _EOCongeAl4Corresp.fetchCongeAl4Corresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAl4Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl4Corresp localInstanceIn(EOEditingContext editingContext, EOCongeAl4Corresp eo) {
    EOCongeAl4Corresp localInstance = (eo == null) ? null : (EOCongeAl4Corresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
