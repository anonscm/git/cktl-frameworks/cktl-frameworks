// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOReliquatsAncienneteCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOReliquatsAncienneteCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "ReliquatsAncienneteCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String RELIQUAT_ANCIENNETE_KEY = "reliquatAnciennete";
	public static final String RELIQUAT_MANGUE_KEY = "reliquatMangue";

  private static Logger LOG = Logger.getLogger(_EOReliquatsAncienneteCorresp.class);

  public EOReliquatsAncienneteCorresp localInstanceIn(EOEditingContext editingContext) {
    EOReliquatsAncienneteCorresp localInstance = (EOReliquatsAncienneteCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOReliquatsAncienneteCorresp.LOG.isDebugEnabled()) {
    	_EOReliquatsAncienneteCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOReliquatsAncienneteCorresp.LOG.isDebugEnabled()) {
    	_EOReliquatsAncienneteCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete reliquatAnciennete() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete)storedValueForKey("reliquatAnciennete");
  }

  public void setReliquatAncienneteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete value) {
    if (_EOReliquatsAncienneteCorresp.LOG.isDebugEnabled()) {
      _EOReliquatsAncienneteCorresp.LOG.debug("updating reliquatAnciennete from " + reliquatAnciennete() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete oldValue = reliquatAnciennete();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "reliquatAnciennete");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "reliquatAnciennete");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReliquatsAnciennete reliquatMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReliquatsAnciennete)storedValueForKey("reliquatMangue");
  }

  public void setReliquatMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReliquatsAnciennete value) {
    if (_EOReliquatsAncienneteCorresp.LOG.isDebugEnabled()) {
      _EOReliquatsAncienneteCorresp.LOG.debug("updating reliquatMangue from " + reliquatMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReliquatsAnciennete oldValue = reliquatMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "reliquatMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "reliquatMangue");
    }
  }
  

  public static EOReliquatsAncienneteCorresp createReliquatsAncienneteCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete reliquatAnciennete, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueReliquatsAnciennete reliquatMangue) {
    EOReliquatsAncienneteCorresp eo = (EOReliquatsAncienneteCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOReliquatsAncienneteCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setReliquatAncienneteRelationship(reliquatAnciennete);
    eo.setReliquatMangueRelationship(reliquatMangue);
    return eo;
  }

  public static NSArray<EOReliquatsAncienneteCorresp> fetchAllReliquatsAncienneteCorresps(EOEditingContext editingContext) {
    return _EOReliquatsAncienneteCorresp.fetchAllReliquatsAncienneteCorresps(editingContext, null);
  }

  public static NSArray<EOReliquatsAncienneteCorresp> fetchAllReliquatsAncienneteCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOReliquatsAncienneteCorresp.fetchReliquatsAncienneteCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOReliquatsAncienneteCorresp> fetchReliquatsAncienneteCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOReliquatsAncienneteCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOReliquatsAncienneteCorresp> eoObjects = (NSArray<EOReliquatsAncienneteCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOReliquatsAncienneteCorresp fetchReliquatsAncienneteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReliquatsAncienneteCorresp.fetchReliquatsAncienneteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReliquatsAncienneteCorresp fetchReliquatsAncienneteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOReliquatsAncienneteCorresp> eoObjects = _EOReliquatsAncienneteCorresp.fetchReliquatsAncienneteCorresps(editingContext, qualifier, null);
    EOReliquatsAncienneteCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOReliquatsAncienneteCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ReliquatsAncienneteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReliquatsAncienneteCorresp fetchRequiredReliquatsAncienneteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReliquatsAncienneteCorresp.fetchRequiredReliquatsAncienneteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReliquatsAncienneteCorresp fetchRequiredReliquatsAncienneteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOReliquatsAncienneteCorresp eoObject = _EOReliquatsAncienneteCorresp.fetchReliquatsAncienneteCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ReliquatsAncienneteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReliquatsAncienneteCorresp localInstanceIn(EOEditingContext editingContext, EOReliquatsAncienneteCorresp eo) {
    EOReliquatsAncienneteCorresp localInstance = (eo == null) ? null : (EOReliquatsAncienneteCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
