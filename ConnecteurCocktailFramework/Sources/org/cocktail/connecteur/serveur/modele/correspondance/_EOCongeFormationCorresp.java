// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeFormationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeFormationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CongeFormationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_FORMATION_KEY = "congeFormation";
	public static final String MANGUE_CONGE_FORMATION_KEY = "mangueCongeFormation";

  private static Logger LOG = Logger.getLogger(_EOCongeFormationCorresp.class);

  public EOCongeFormationCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeFormationCorresp localInstance = (EOCongeFormationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeFormationCorresp.LOG.isDebugEnabled()) {
    	_EOCongeFormationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeFormationCorresp.LOG.isDebugEnabled()) {
    	_EOCongeFormationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation congeFormation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation)storedValueForKey("congeFormation");
  }

  public void setCongeFormationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation value) {
    if (_EOCongeFormationCorresp.LOG.isDebugEnabled()) {
      _EOCongeFormationCorresp.LOG.debug("updating congeFormation from " + congeFormation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation oldValue = congeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeFormation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeFormation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeFormation mangueCongeFormation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeFormation)storedValueForKey("mangueCongeFormation");
  }

  public void setMangueCongeFormationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeFormation value) {
    if (_EOCongeFormationCorresp.LOG.isDebugEnabled()) {
      _EOCongeFormationCorresp.LOG.debug("updating mangueCongeFormation from " + mangueCongeFormation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeFormation oldValue = mangueCongeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeFormation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeFormation");
    }
  }
  

  public static EOCongeFormationCorresp createCongeFormationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeFormation congeFormation, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeFormation mangueCongeFormation) {
    EOCongeFormationCorresp eo = (EOCongeFormationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeFormationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeFormationRelationship(congeFormation);
    eo.setMangueCongeFormationRelationship(mangueCongeFormation);
    return eo;
  }

  public static NSArray<EOCongeFormationCorresp> fetchAllCongeFormationCorresps(EOEditingContext editingContext) {
    return _EOCongeFormationCorresp.fetchAllCongeFormationCorresps(editingContext, null);
  }

  public static NSArray<EOCongeFormationCorresp> fetchAllCongeFormationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeFormationCorresp.fetchCongeFormationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeFormationCorresp> fetchCongeFormationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeFormationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeFormationCorresp> eoObjects = (NSArray<EOCongeFormationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeFormationCorresp fetchCongeFormationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeFormationCorresp.fetchCongeFormationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeFormationCorresp fetchCongeFormationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeFormationCorresp> eoObjects = _EOCongeFormationCorresp.fetchCongeFormationCorresps(editingContext, qualifier, null);
    EOCongeFormationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeFormationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeFormationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeFormationCorresp fetchRequiredCongeFormationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeFormationCorresp.fetchRequiredCongeFormationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeFormationCorresp fetchRequiredCongeFormationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeFormationCorresp eoObject = _EOCongeFormationCorresp.fetchCongeFormationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeFormationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeFormationCorresp localInstanceIn(EOEditingContext editingContext, EOCongeFormationCorresp eo) {
    EOCongeFormationCorresp localInstance = (eo == null) ? null : (EOCongeFormationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
