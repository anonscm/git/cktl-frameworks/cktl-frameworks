package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOCfaCorresp extends _EOCfaCorresp {
	@Override
	public void supprimerRelations() {
		setCfaRelationship(null);
		setMangueCfaRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return CFA_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_CFA_KEY;
	}
}
