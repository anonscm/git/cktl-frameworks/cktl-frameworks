package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel;
import org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein;
import org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel;

import com.webobjects.eocontrol.EOEditingContext;

/**.
 * Classe gérant les correspondances
 * pour les Reprises à Temps Plein d'un individu
 * @author alainmalaplate
 *
 */
public class EORepriseTempsPleinCorresp extends _EORepriseTempsPleinCorresp {

	private static final long serialVersionUID = 2691041583895839000L;
	private static Logger log = Logger.getLogger(EORepriseTempsPleinCorresp.class);

	public EORepriseTempsPleinCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_REPRISE_TEMPS_PLEIN_KEY;
	}

	public String nomRelationBaseImport() {
		return REPRISE_TEMPS_PLEIN_KEY;
	}

	public void supprimerRelations() {
		setRepriseTempsPleinRelationship(null);
		setMangueRepriseTempsPleinRelationship(null);
	}

	public static EOMangueRepriseTempsPlein rtpMangue(EOEditingContext editingContext, EORepriseTempsPlein rtpImport) {
		if (rtpImport==null)
			return null;
		EORepriseTempsPleinCorresp corresp=fetchRepriseTempsPleinCorresp(editingContext, REPRISE_TEMPS_PLEIN_KEY, rtpImport);
		if (corresp==null)
			return null;
		return corresp.mangueRepriseTempsPlein();
	}
}
