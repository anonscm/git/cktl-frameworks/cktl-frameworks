// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODelegationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODelegationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "DelegationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String DELEGATION_KEY = "delegation";
	public static final String MANGUE_DELEGATION_KEY = "mangueDelegation";

  private static Logger LOG = Logger.getLogger(_EODelegationCorresp.class);

  public EODelegationCorresp localInstanceIn(EOEditingContext editingContext) {
    EODelegationCorresp localInstance = (EODelegationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODelegationCorresp.LOG.isDebugEnabled()) {
    	_EODelegationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODelegationCorresp.LOG.isDebugEnabled()) {
    	_EODelegationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EODelegation delegation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODelegation)storedValueForKey("delegation");
  }

  public void setDelegationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODelegation value) {
    if (_EODelegationCorresp.LOG.isDebugEnabled()) {
      _EODelegationCorresp.LOG.debug("updating delegation from " + delegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODelegation oldValue = delegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "delegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "delegation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDelegation mangueDelegation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDelegation)storedValueForKey("mangueDelegation");
  }

  public void setMangueDelegationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDelegation value) {
    if (_EODelegationCorresp.LOG.isDebugEnabled()) {
      _EODelegationCorresp.LOG.debug("updating mangueDelegation from " + mangueDelegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDelegation oldValue = mangueDelegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueDelegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueDelegation");
    }
  }
  

  public static EODelegationCorresp createDelegationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EODelegation delegation, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDelegation mangueDelegation) {
    EODelegationCorresp eo = (EODelegationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EODelegationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setDelegationRelationship(delegation);
    eo.setMangueDelegationRelationship(mangueDelegation);
    return eo;
  }

  public static NSArray<EODelegationCorresp> fetchAllDelegationCorresps(EOEditingContext editingContext) {
    return _EODelegationCorresp.fetchAllDelegationCorresps(editingContext, null);
  }

  public static NSArray<EODelegationCorresp> fetchAllDelegationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODelegationCorresp.fetchDelegationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EODelegationCorresp> fetchDelegationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODelegationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODelegationCorresp> eoObjects = (NSArray<EODelegationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODelegationCorresp fetchDelegationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODelegationCorresp.fetchDelegationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODelegationCorresp fetchDelegationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODelegationCorresp> eoObjects = _EODelegationCorresp.fetchDelegationCorresps(editingContext, qualifier, null);
    EODelegationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODelegationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one DelegationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODelegationCorresp fetchRequiredDelegationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODelegationCorresp.fetchRequiredDelegationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODelegationCorresp fetchRequiredDelegationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EODelegationCorresp eoObject = _EODelegationCorresp.fetchDelegationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no DelegationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODelegationCorresp localInstanceIn(EOEditingContext editingContext, EODelegationCorresp eo) {
    EODelegationCorresp localInstance = (eo == null) ? null : (EODelegationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
