package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

/**
 * Classe gérant les correspondances pour les Délégations d'un individu
 * @author alainmalaplate
 *
 */
public class EODelegationCorresp extends _EODelegationCorresp {

	private static final long serialVersionUID = 2472516706441291985L;
	private static Logger log = Logger.getLogger(EODelegationCorresp.class);
	
	public EODelegationCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_DELEGATION_KEY;
	}

	public String nomRelationBaseImport() {
		return DELEGATION_KEY;
	}

	public void supprimerRelations() {
		setDelegationRelationship(null);
		setMangueDelegationRelationship(null);
	}
}
