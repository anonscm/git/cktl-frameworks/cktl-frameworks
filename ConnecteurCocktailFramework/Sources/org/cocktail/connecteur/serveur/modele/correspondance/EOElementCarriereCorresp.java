// EOElementCarriereCorresp.java
// Created on Mon Jan 07 16:31:37 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOElementCarriereCorresp extends _EOElementCarriereCorresp {

    public EOElementCarriereCorresp() {
        super();
    }


	public String nomRelationBaseDestinataire() {
		return ELEMENT_CARRIERE_MANGUE_KEY;
	}
	public String nomRelationBaseImport() {
		return ELEMENT_CARRIERE_KEY;
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(elementCarriere(), ELEMENT_CARRIERE_KEY);
		removeObjectFromBothSidesOfRelationshipWithKey(elementCarriereMangue(),ELEMENT_CARRIERE_MANGUE_KEY);
	}
	//	 Méthodes statiques
	/** retourne la correspondance de l'element du SI Destinataire */
	public static EOElementCarriereCorresp correspondancePourElementMangue(EOEditingContext editingContext,EOMangueElementCarriere elementMangue) {
	EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("elementCarriereMangue = %@ ",new NSArray(elementMangue));
		EOFetchSpecification myFetch = new EOFetchSpecification ("ElementCarriereCorresp", myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOElementCarriereCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** retourne l'element de carri&egrave;re d'import associe a l'individu et au numero de segment 
	 * de carri&egrave;re et au numero d'element passes en param&egrave;tre. On commence par chercher dans l'import courant pour &ecirc;tre s&ucirc;r 
	 * de retourner le dernier element de carri&egrave;re, sinon on recherche dans les correspondances */
	public static EOElementCarriere elementCarrierePourIndividuEtCarriere(EOEditingContext editingContext,Number idSource,Number carSource,Number elSource) {
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(carSource);
		args.addObject(elSource);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND toCarriere.carSource = %@ AND elSource = %@ AND  temImport = %@ AND statut <> 'A' AND statut <> 'E'",args);
		EOFetchSpecification myFetch = new EOFetchSpecification ("ElementCarriere", myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOElementCarriere)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e1) {
			// Rechercher dans les correspondances si l'élément de carrière n'a pas déjà été importé
			myQualifier = EOQualifier.qualifierWithQualifierFormat("elementCarriere.individu.idSource = %@ AND elementCarriere.toCarriere.carSource = %@ AND elementCarriere.elSource = %@",args);
			myFetch = new EOFetchSpecification ("ElementCarriereCorresp", myQualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			try {
				return ((EOElementCarriereCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0)).elementCarriere();
			} catch (Exception e) {
				return null;
				
			}
		}
	}
	
	public static EOMangueElementCarriere mangueElementCarriere(EOEditingContext editingContext, EOElementCarriere elementCarriere) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(ELEMENT_CARRIERE_KEY +"." + EOElementCarriere.ELC_ORDRE_KEY+" = %@",new NSArray(elementCarriere.elcOrdre()));
		EOFetchSpecification myFetch = new EOFetchSpecification (EOElementCarriereCorresp.ENTITY_NAME, myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			NSArray results=editingContext.objectsWithFetchSpecification(myFetch);
			if (results.count()==1) {
				EOElementCarriereCorresp corresp = (EOElementCarriereCorresp)results.objectAtIndex(0);
				return corresp.elementCarriereMangue();
			}
		} catch (Exception e) {
		}
		return null;		
	}
}
