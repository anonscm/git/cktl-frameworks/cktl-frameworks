// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPersonnelCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPersonnelCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "PersonnelCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String PERSONNEL_KEY = "personnel";
	public static final String PERSONNEL_GRHUM_KEY = "personnelGrhum";

  private static Logger LOG = Logger.getLogger(_EOPersonnelCorresp.class);

  public EOPersonnelCorresp localInstanceIn(EOEditingContext editingContext) {
    EOPersonnelCorresp localInstance = (EOPersonnelCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPersonnelCorresp.LOG.isDebugEnabled()) {
    	_EOPersonnelCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPersonnelCorresp.LOG.isDebugEnabled()) {
    	_EOPersonnelCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel personnel() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel)storedValueForKey("personnel");
  }

  public void setPersonnelRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel value) {
    if (_EOPersonnelCorresp.LOG.isDebugEnabled()) {
      _EOPersonnelCorresp.LOG.debug("updating personnel from " + personnel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel oldValue = personnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "personnel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "personnel");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel personnelGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel)storedValueForKey("personnelGrhum");
  }

  public void setPersonnelGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel value) {
    if (_EOPersonnelCorresp.LOG.isDebugEnabled()) {
      _EOPersonnelCorresp.LOG.debug("updating personnelGrhum from " + personnelGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel oldValue = personnelGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "personnelGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "personnelGrhum");
    }
  }
  

  public static EOPersonnelCorresp createPersonnelCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel personnel, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel personnelGrhum) {
    EOPersonnelCorresp eo = (EOPersonnelCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOPersonnelCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setPersonnelRelationship(personnel);
    eo.setPersonnelGrhumRelationship(personnelGrhum);
    return eo;
  }

  public static NSArray<EOPersonnelCorresp> fetchAllPersonnelCorresps(EOEditingContext editingContext) {
    return _EOPersonnelCorresp.fetchAllPersonnelCorresps(editingContext, null);
  }

  public static NSArray<EOPersonnelCorresp> fetchAllPersonnelCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPersonnelCorresp.fetchPersonnelCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPersonnelCorresp> fetchPersonnelCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPersonnelCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPersonnelCorresp> eoObjects = (NSArray<EOPersonnelCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPersonnelCorresp fetchPersonnelCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersonnelCorresp.fetchPersonnelCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersonnelCorresp fetchPersonnelCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPersonnelCorresp> eoObjects = _EOPersonnelCorresp.fetchPersonnelCorresps(editingContext, qualifier, null);
    EOPersonnelCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPersonnelCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PersonnelCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersonnelCorresp fetchRequiredPersonnelCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersonnelCorresp.fetchRequiredPersonnelCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersonnelCorresp fetchRequiredPersonnelCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPersonnelCorresp eoObject = _EOPersonnelCorresp.fetchPersonnelCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PersonnelCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersonnelCorresp localInstanceIn(EOEditingContext editingContext, EOPersonnelCorresp eo) {
    EOPersonnelCorresp localInstance = (eo == null) ? null : (EOPersonnelCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
