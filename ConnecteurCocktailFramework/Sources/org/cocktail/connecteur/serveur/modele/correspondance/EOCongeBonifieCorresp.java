package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EOCongeBonifieCorresp extends _EOCongeBonifieCorresp {
  private static Logger log = Logger.getLogger(EOCongeBonifieCorresp.class);
  
  public EOCongeBonifieCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_BONIFIE_KEY;
	}

	public String nomRelationBaseImport() {
		return CONGE_BONIFIE_KEY;
	}

	public void supprimerRelations() {
		setCongeBonifieRelationship(null);
		setMangueCongeBonifieRelationship(null);
	}
}
