// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartAssociationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartAssociationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "RepartAssociationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String GRHUM_REPART_ASSOCIATION_KEY = "grhumRepartAssociation";
	public static final String REPART_ASSOCIATION_KEY = "repartAssociation";

  private static Logger LOG = Logger.getLogger(_EORepartAssociationCorresp.class);

  public EORepartAssociationCorresp localInstanceIn(EOEditingContext editingContext) {
    EORepartAssociationCorresp localInstance = (EORepartAssociationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartAssociationCorresp.LOG.isDebugEnabled()) {
    	_EORepartAssociationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartAssociationCorresp.LOG.isDebugEnabled()) {
    	_EORepartAssociationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRepartAssociation grhumRepartAssociation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRepartAssociation)storedValueForKey("grhumRepartAssociation");
  }

  public void setGrhumRepartAssociationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRepartAssociation value) {
    if (_EORepartAssociationCorresp.LOG.isDebugEnabled()) {
      _EORepartAssociationCorresp.LOG.debug("updating grhumRepartAssociation from " + grhumRepartAssociation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRepartAssociation oldValue = grhumRepartAssociation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "grhumRepartAssociation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "grhumRepartAssociation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation repartAssociation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation)storedValueForKey("repartAssociation");
  }

  public void setRepartAssociationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation value) {
    if (_EORepartAssociationCorresp.LOG.isDebugEnabled()) {
      _EORepartAssociationCorresp.LOG.debug("updating repartAssociation from " + repartAssociation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation oldValue = repartAssociation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "repartAssociation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "repartAssociation");
    }
  }
  

  public static EORepartAssociationCorresp createRepartAssociationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumRepartAssociation grhumRepartAssociation, org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation repartAssociation) {
    EORepartAssociationCorresp eo = (EORepartAssociationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EORepartAssociationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setGrhumRepartAssociationRelationship(grhumRepartAssociation);
    eo.setRepartAssociationRelationship(repartAssociation);
    return eo;
  }

  public static NSArray<EORepartAssociationCorresp> fetchAllRepartAssociationCorresps(EOEditingContext editingContext) {
    return _EORepartAssociationCorresp.fetchAllRepartAssociationCorresps(editingContext, null);
  }

  public static NSArray<EORepartAssociationCorresp> fetchAllRepartAssociationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartAssociationCorresp.fetchRepartAssociationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartAssociationCorresp> fetchRepartAssociationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartAssociationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartAssociationCorresp> eoObjects = (NSArray<EORepartAssociationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartAssociationCorresp fetchRepartAssociationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAssociationCorresp.fetchRepartAssociationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartAssociationCorresp fetchRepartAssociationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartAssociationCorresp> eoObjects = _EORepartAssociationCorresp.fetchRepartAssociationCorresps(editingContext, qualifier, null);
    EORepartAssociationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartAssociationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one RepartAssociationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartAssociationCorresp fetchRequiredRepartAssociationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAssociationCorresp.fetchRequiredRepartAssociationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartAssociationCorresp fetchRequiredRepartAssociationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartAssociationCorresp eoObject = _EORepartAssociationCorresp.fetchRepartAssociationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no RepartAssociationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartAssociationCorresp localInstanceIn(EOEditingContext editingContext, EORepartAssociationCorresp eo) {
    EORepartAssociationCorresp localInstance = (eo == null) ? null : (EORepartAssociationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
