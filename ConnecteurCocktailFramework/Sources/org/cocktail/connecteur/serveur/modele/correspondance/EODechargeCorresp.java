package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EODechargeCorresp extends _EODechargeCorresp {
  private static Logger log = Logger.getLogger(EODechargeCorresp.class);
  
	public EODechargeCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_DECHARGE_KEY;
	}

	public String nomRelationBaseImport() {
		return TO_DECHARGE_KEY;
	}

	public void supprimerRelations() {
		setToDechargeRelationship(null);
		setToMangueDechargeRelationship(null);
	}
}
