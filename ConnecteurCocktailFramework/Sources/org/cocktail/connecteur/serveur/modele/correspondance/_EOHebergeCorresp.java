// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOHebergeCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOHebergeCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "HebergeCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String HEBERGE_KEY = "heberge";
	public static final String MANGUE_HEBERGE_KEY = "mangueHeberge";

  private static Logger LOG = Logger.getLogger(_EOHebergeCorresp.class);

  public EOHebergeCorresp localInstanceIn(EOEditingContext editingContext) {
    EOHebergeCorresp localInstance = (EOHebergeCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOHebergeCorresp.LOG.isDebugEnabled()) {
    	_EOHebergeCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOHebergeCorresp.LOG.isDebugEnabled()) {
    	_EOHebergeCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge heberge() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge)storedValueForKey("heberge");
  }

  public void setHebergeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge value) {
    if (_EOHebergeCorresp.LOG.isDebugEnabled()) {
      _EOHebergeCorresp.LOG.debug("updating heberge from " + heberge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge oldValue = heberge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "heberge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "heberge");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratHeberges mangueHeberge() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratHeberges)storedValueForKey("mangueHeberge");
  }

  public void setMangueHebergeRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratHeberges value) {
    if (_EOHebergeCorresp.LOG.isDebugEnabled()) {
      _EOHebergeCorresp.LOG.debug("updating mangueHeberge from " + mangueHeberge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratHeberges oldValue = mangueHeberge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueHeberge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueHeberge");
    }
  }
  

  public static EOHebergeCorresp createHebergeCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge heberge, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContratHeberges mangueHeberge) {
    EOHebergeCorresp eo = (EOHebergeCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOHebergeCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setHebergeRelationship(heberge);
    eo.setMangueHebergeRelationship(mangueHeberge);
    return eo;
  }

  public static NSArray<EOHebergeCorresp> fetchAllHebergeCorresps(EOEditingContext editingContext) {
    return _EOHebergeCorresp.fetchAllHebergeCorresps(editingContext, null);
  }

  public static NSArray<EOHebergeCorresp> fetchAllHebergeCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOHebergeCorresp.fetchHebergeCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOHebergeCorresp> fetchHebergeCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOHebergeCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOHebergeCorresp> eoObjects = (NSArray<EOHebergeCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOHebergeCorresp fetchHebergeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHebergeCorresp.fetchHebergeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHebergeCorresp fetchHebergeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOHebergeCorresp> eoObjects = _EOHebergeCorresp.fetchHebergeCorresps(editingContext, qualifier, null);
    EOHebergeCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOHebergeCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one HebergeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHebergeCorresp fetchRequiredHebergeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHebergeCorresp.fetchRequiredHebergeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHebergeCorresp fetchRequiredHebergeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOHebergeCorresp eoObject = _EOHebergeCorresp.fetchHebergeCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no HebergeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHebergeCorresp localInstanceIn(EOEditingContext editingContext, EOHebergeCorresp eo) {
    EOHebergeCorresp localInstance = (eo == null) ? null : (EOHebergeCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
