package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOSituationCorresp extends _EOSituationCorresp {
	@Override
	public void supprimerRelations() {
		setSituationRelationship(null);
		setMangueSituationRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return SITUATION_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_SITUATION_KEY;
	}
}
