// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOProlongationActiviteCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOProlongationActiviteCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "ProlongationActiviteCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_MANGUE_PROLONGATION_ACTIVITE_KEY = "toMangueProlongationActivite";
	public static final String TO_PROLONGATION_ACTIVITE_KEY = "toProlongationActivite";

  private static Logger LOG = Logger.getLogger(_EOProlongationActiviteCorresp.class);

  public EOProlongationActiviteCorresp localInstanceIn(EOEditingContext editingContext) {
    EOProlongationActiviteCorresp localInstance = (EOProlongationActiviteCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOProlongationActiviteCorresp.LOG.isDebugEnabled()) {
    	_EOProlongationActiviteCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOProlongationActiviteCorresp.LOG.isDebugEnabled()) {
    	_EOProlongationActiviteCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueProlongationActivite toMangueProlongationActivite() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueProlongationActivite)storedValueForKey("toMangueProlongationActivite");
  }

  public void setToMangueProlongationActiviteRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueProlongationActivite value) {
    if (_EOProlongationActiviteCorresp.LOG.isDebugEnabled()) {
      _EOProlongationActiviteCorresp.LOG.debug("updating toMangueProlongationActivite from " + toMangueProlongationActivite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueProlongationActivite oldValue = toMangueProlongationActivite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueProlongationActivite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueProlongationActivite");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite toProlongationActivite() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite)storedValueForKey("toProlongationActivite");
  }

  public void setToProlongationActiviteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite value) {
    if (_EOProlongationActiviteCorresp.LOG.isDebugEnabled()) {
      _EOProlongationActiviteCorresp.LOG.debug("updating toProlongationActivite from " + toProlongationActivite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite oldValue = toProlongationActivite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toProlongationActivite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toProlongationActivite");
    }
  }
  

  public static EOProlongationActiviteCorresp createProlongationActiviteCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueProlongationActivite toMangueProlongationActivite, org.cocktail.connecteur.serveur.modele.entite_import.EOProlongationActivite toProlongationActivite) {
    EOProlongationActiviteCorresp eo = (EOProlongationActiviteCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOProlongationActiviteCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToMangueProlongationActiviteRelationship(toMangueProlongationActivite);
    eo.setToProlongationActiviteRelationship(toProlongationActivite);
    return eo;
  }

  public static NSArray<EOProlongationActiviteCorresp> fetchAllProlongationActiviteCorresps(EOEditingContext editingContext) {
    return _EOProlongationActiviteCorresp.fetchAllProlongationActiviteCorresps(editingContext, null);
  }

  public static NSArray<EOProlongationActiviteCorresp> fetchAllProlongationActiviteCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOProlongationActiviteCorresp.fetchProlongationActiviteCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOProlongationActiviteCorresp> fetchProlongationActiviteCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOProlongationActiviteCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOProlongationActiviteCorresp> eoObjects = (NSArray<EOProlongationActiviteCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOProlongationActiviteCorresp fetchProlongationActiviteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProlongationActiviteCorresp.fetchProlongationActiviteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProlongationActiviteCorresp fetchProlongationActiviteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOProlongationActiviteCorresp> eoObjects = _EOProlongationActiviteCorresp.fetchProlongationActiviteCorresps(editingContext, qualifier, null);
    EOProlongationActiviteCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOProlongationActiviteCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ProlongationActiviteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProlongationActiviteCorresp fetchRequiredProlongationActiviteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOProlongationActiviteCorresp.fetchRequiredProlongationActiviteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOProlongationActiviteCorresp fetchRequiredProlongationActiviteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOProlongationActiviteCorresp eoObject = _EOProlongationActiviteCorresp.fetchProlongationActiviteCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ProlongationActiviteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOProlongationActiviteCorresp localInstanceIn(EOEditingContext editingContext, EOProlongationActiviteCorresp eo) {
    EOProlongationActiviteCorresp localInstance = (eo == null) ? null : (EOProlongationActiviteCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
