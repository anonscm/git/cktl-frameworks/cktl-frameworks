// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODepartCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODepartCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "DepartCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String DEPART_KEY = "depart";
	public static final String DEPART_MANGUE_KEY = "departMangue";

  private static Logger LOG = Logger.getLogger(_EODepartCorresp.class);

  public EODepartCorresp localInstanceIn(EOEditingContext editingContext) {
    EODepartCorresp localInstance = (EODepartCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODepartCorresp.LOG.isDebugEnabled()) {
    	_EODepartCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODepartCorresp.LOG.isDebugEnabled()) {
    	_EODepartCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EODepart depart() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODepart)storedValueForKey("depart");
  }

  public void setDepartRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODepart value) {
    if (_EODepartCorresp.LOG.isDebugEnabled()) {
      _EODepartCorresp.LOG.debug("updating depart from " + depart() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODepart oldValue = depart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "depart");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "depart");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart departMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart)storedValueForKey("departMangue");
  }

  public void setDepartMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart value) {
    if (_EODepartCorresp.LOG.isDebugEnabled()) {
      _EODepartCorresp.LOG.debug("updating departMangue from " + departMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart oldValue = departMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "departMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "departMangue");
    }
  }
  

  public static EODepartCorresp createDepartCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EODepart depart, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDepart departMangue) {
    EODepartCorresp eo = (EODepartCorresp) EOUtilities.createAndInsertInstance(editingContext, _EODepartCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setDepartRelationship(depart);
    eo.setDepartMangueRelationship(departMangue);
    return eo;
  }

  public static NSArray<EODepartCorresp> fetchAllDepartCorresps(EOEditingContext editingContext) {
    return _EODepartCorresp.fetchAllDepartCorresps(editingContext, null);
  }

  public static NSArray<EODepartCorresp> fetchAllDepartCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODepartCorresp.fetchDepartCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EODepartCorresp> fetchDepartCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODepartCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODepartCorresp> eoObjects = (NSArray<EODepartCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODepartCorresp fetchDepartCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepartCorresp.fetchDepartCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepartCorresp fetchDepartCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODepartCorresp> eoObjects = _EODepartCorresp.fetchDepartCorresps(editingContext, qualifier, null);
    EODepartCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODepartCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one DepartCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepartCorresp fetchRequiredDepartCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepartCorresp.fetchRequiredDepartCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepartCorresp fetchRequiredDepartCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EODepartCorresp eoObject = _EODepartCorresp.fetchDepartCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no DepartCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepartCorresp localInstanceIn(EOEditingContext editingContext, EODepartCorresp eo) {
    EODepartCorresp localInstance = (eo == null) ? null : (EODepartCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
