// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPeriodesMilitairesCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPeriodesMilitairesCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "PeriodesMilitairesCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String PERIODE_MILITAIRE_KEY = "periodeMilitaire";
	public static final String PERIODE_MILITAIRE_MANGUE_KEY = "periodeMilitaireMangue";

  private static Logger LOG = Logger.getLogger(_EOPeriodesMilitairesCorresp.class);

  public EOPeriodesMilitairesCorresp localInstanceIn(EOEditingContext editingContext) {
    EOPeriodesMilitairesCorresp localInstance = (EOPeriodesMilitairesCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPeriodesMilitairesCorresp.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitairesCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPeriodesMilitairesCorresp.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitairesCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires periodeMilitaire() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires)storedValueForKey("periodeMilitaire");
  }

  public void setPeriodeMilitaireRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires value) {
    if (_EOPeriodesMilitairesCorresp.LOG.isDebugEnabled()) {
      _EOPeriodesMilitairesCorresp.LOG.debug("updating periodeMilitaire from " + periodeMilitaire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires oldValue = periodeMilitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "periodeMilitaire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "periodeMilitaire");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePeriodesMilitaires periodeMilitaireMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePeriodesMilitaires)storedValueForKey("periodeMilitaireMangue");
  }

  public void setPeriodeMilitaireMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePeriodesMilitaires value) {
    if (_EOPeriodesMilitairesCorresp.LOG.isDebugEnabled()) {
      _EOPeriodesMilitairesCorresp.LOG.debug("updating periodeMilitaireMangue from " + periodeMilitaireMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePeriodesMilitaires oldValue = periodeMilitaireMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "periodeMilitaireMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "periodeMilitaireMangue");
    }
  }
  

  public static EOPeriodesMilitairesCorresp createPeriodesMilitairesCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires periodeMilitaire, org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePeriodesMilitaires periodeMilitaireMangue) {
    EOPeriodesMilitairesCorresp eo = (EOPeriodesMilitairesCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOPeriodesMilitairesCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setPeriodeMilitaireRelationship(periodeMilitaire);
    eo.setPeriodeMilitaireMangueRelationship(periodeMilitaireMangue);
    return eo;
  }

  public static NSArray<EOPeriodesMilitairesCorresp> fetchAllPeriodesMilitairesCorresps(EOEditingContext editingContext) {
    return _EOPeriodesMilitairesCorresp.fetchAllPeriodesMilitairesCorresps(editingContext, null);
  }

  public static NSArray<EOPeriodesMilitairesCorresp> fetchAllPeriodesMilitairesCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPeriodesMilitairesCorresp.fetchPeriodesMilitairesCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPeriodesMilitairesCorresp> fetchPeriodesMilitairesCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPeriodesMilitairesCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPeriodesMilitairesCorresp> eoObjects = (NSArray<EOPeriodesMilitairesCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPeriodesMilitairesCorresp fetchPeriodesMilitairesCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodesMilitairesCorresp.fetchPeriodesMilitairesCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriodesMilitairesCorresp fetchPeriodesMilitairesCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPeriodesMilitairesCorresp> eoObjects = _EOPeriodesMilitairesCorresp.fetchPeriodesMilitairesCorresps(editingContext, qualifier, null);
    EOPeriodesMilitairesCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPeriodesMilitairesCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PeriodesMilitairesCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriodesMilitairesCorresp fetchRequiredPeriodesMilitairesCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodesMilitairesCorresp.fetchRequiredPeriodesMilitairesCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriodesMilitairesCorresp fetchRequiredPeriodesMilitairesCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPeriodesMilitairesCorresp eoObject = _EOPeriodesMilitairesCorresp.fetchPeriodesMilitairesCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PeriodesMilitairesCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriodesMilitairesCorresp localInstanceIn(EOEditingContext editingContext, EOPeriodesMilitairesCorresp eo) {
    EOPeriodesMilitairesCorresp localInstance = (eo == null) ? null : (EOPeriodesMilitairesCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
