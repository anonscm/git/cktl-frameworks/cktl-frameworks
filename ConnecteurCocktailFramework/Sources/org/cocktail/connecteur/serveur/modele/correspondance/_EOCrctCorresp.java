// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCrctCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCrctCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CrctCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CRCT_KEY = "crct";
	public static final String MANGUE_CRCT_KEY = "mangueCrct";

  private static Logger LOG = Logger.getLogger(_EOCrctCorresp.class);

  public EOCrctCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCrctCorresp localInstance = (EOCrctCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCrctCorresp.LOG.isDebugEnabled()) {
    	_EOCrctCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCrctCorresp.LOG.isDebugEnabled()) {
    	_EOCrctCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCrct crct() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCrct)storedValueForKey("crct");
  }

  public void setCrctRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCrct value) {
    if (_EOCrctCorresp.LOG.isDebugEnabled()) {
      _EOCrctCorresp.LOG.debug("updating crct from " + crct() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCrct oldValue = crct();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "crct");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "crct");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct mangueCrct() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct)storedValueForKey("mangueCrct");
  }

  public void setMangueCrctRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct value) {
    if (_EOCrctCorresp.LOG.isDebugEnabled()) {
      _EOCrctCorresp.LOG.debug("updating mangueCrct from " + mangueCrct() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct oldValue = mangueCrct();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCrct");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCrct");
    }
  }
  

  public static EOCrctCorresp createCrctCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCrct crct, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct mangueCrct) {
    EOCrctCorresp eo = (EOCrctCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCrctCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCrctRelationship(crct);
    eo.setMangueCrctRelationship(mangueCrct);
    return eo;
  }

  public static NSArray<EOCrctCorresp> fetchAllCrctCorresps(EOEditingContext editingContext) {
    return _EOCrctCorresp.fetchAllCrctCorresps(editingContext, null);
  }

  public static NSArray<EOCrctCorresp> fetchAllCrctCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCrctCorresp.fetchCrctCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCrctCorresp> fetchCrctCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCrctCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCrctCorresp> eoObjects = (NSArray<EOCrctCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCrctCorresp fetchCrctCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrctCorresp.fetchCrctCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrctCorresp fetchCrctCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCrctCorresp> eoObjects = _EOCrctCorresp.fetchCrctCorresps(editingContext, qualifier, null);
    EOCrctCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCrctCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CrctCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrctCorresp fetchRequiredCrctCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCrctCorresp.fetchRequiredCrctCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCrctCorresp fetchRequiredCrctCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCrctCorresp eoObject = _EOCrctCorresp.fetchCrctCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CrctCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCrctCorresp localInstanceIn(EOEditingContext editingContext, EOCrctCorresp eo) {
    EOCrctCorresp localInstance = (eo == null) ? null : (EOCrctCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
