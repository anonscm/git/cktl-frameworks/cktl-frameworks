// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCgntMaladieCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCgntMaladieCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CgntMaladieCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CGNT_MALADIE_KEY = "cgntMaladie";
	public static final String CGNT_MALADIE_MANGUE_KEY = "cgntMaladieMangue";

  private static Logger LOG = Logger.getLogger(_EOCgntMaladieCorresp.class);

  public EOCgntMaladieCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCgntMaladieCorresp localInstance = (EOCgntMaladieCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCgntMaladieCorresp.LOG.isDebugEnabled()) {
    	_EOCgntMaladieCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCgntMaladieCorresp.LOG.isDebugEnabled()) {
    	_EOCgntMaladieCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie cgntMaladie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie)storedValueForKey("cgntMaladie");
  }

  public void setCgntMaladieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie value) {
    if (_EOCgntMaladieCorresp.LOG.isDebugEnabled()) {
      _EOCgntMaladieCorresp.LOG.debug("updating cgntMaladie from " + cgntMaladie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie oldValue = cgntMaladie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntMaladie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntMaladie");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie cgntMaladieMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie)storedValueForKey("cgntMaladieMangue");
  }

  public void setCgntMaladieMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie value) {
    if (_EOCgntMaladieCorresp.LOG.isDebugEnabled()) {
      _EOCgntMaladieCorresp.LOG.debug("updating cgntMaladieMangue from " + cgntMaladieMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie oldValue = cgntMaladieMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cgntMaladieMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cgntMaladieMangue");
    }
  }
  

  public static EOCgntMaladieCorresp createCgntMaladieCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCgntMaladie cgntMaladie, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie cgntMaladieMangue) {
    EOCgntMaladieCorresp eo = (EOCgntMaladieCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCgntMaladieCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCgntMaladieRelationship(cgntMaladie);
    eo.setCgntMaladieMangueRelationship(cgntMaladieMangue);
    return eo;
  }

  public static NSArray<EOCgntMaladieCorresp> fetchAllCgntMaladieCorresps(EOEditingContext editingContext) {
    return _EOCgntMaladieCorresp.fetchAllCgntMaladieCorresps(editingContext, null);
  }

  public static NSArray<EOCgntMaladieCorresp> fetchAllCgntMaladieCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCgntMaladieCorresp.fetchCgntMaladieCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCgntMaladieCorresp> fetchCgntMaladieCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCgntMaladieCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCgntMaladieCorresp> eoObjects = (NSArray<EOCgntMaladieCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCgntMaladieCorresp fetchCgntMaladieCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntMaladieCorresp.fetchCgntMaladieCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntMaladieCorresp fetchCgntMaladieCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCgntMaladieCorresp> eoObjects = _EOCgntMaladieCorresp.fetchCgntMaladieCorresps(editingContext, qualifier, null);
    EOCgntMaladieCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCgntMaladieCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CgntMaladieCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntMaladieCorresp fetchRequiredCgntMaladieCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCgntMaladieCorresp.fetchRequiredCgntMaladieCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCgntMaladieCorresp fetchRequiredCgntMaladieCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCgntMaladieCorresp eoObject = _EOCgntMaladieCorresp.fetchCgntMaladieCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CgntMaladieCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCgntMaladieCorresp localInstanceIn(EOEditingContext editingContext, EOCgntMaladieCorresp eo) {
    EOCgntMaladieCorresp localInstance = (eo == null) ? null : (EOCgntMaladieCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
