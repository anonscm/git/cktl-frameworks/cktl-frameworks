package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan;
import org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget;

import com.webobjects.eocontrol.EOEditingContext;

public class EOOrgaBudgetCorresp extends _EOOrgaBudgetCorresp {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3556756091739425517L;

	public String nomRelationBaseImport() {
		return ORGA_BUDGET_KEY;
	}
	public String nomRelationBaseDestinataire() {
		return JEFY_ORGAN_KEY;
	}
	
	public void supprimerRelations() {
		setOrgaBudgetRelationship(null);
		setJefyOrganRelationship(null);
	}
	
	public static EOJefyOrgan jefyOrgan(EOEditingContext editingContext, EOOrgaBudget orgaBudget) {
		if (orgaBudget==null)
			return null;
		
		EOOrgaBudgetCorresp corresp=fetchOrgaBudgetCorresp(editingContext, ORGA_BUDGET_KEY, orgaBudget);
		if (corresp==null)
			return null;
		
		return corresp.jefyOrgan();
	}
}
