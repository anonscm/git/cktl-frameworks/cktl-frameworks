package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOIndividuFonctionInstanceCorresp extends _EOIndividuFonctionInstanceCorresp {
	@Override
	public void supprimerRelations() {
		setIndividuFonctionInstanceRelationship(null);
		setGrhumIndividuFonctionInstanceRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return INDIVIDU_FONCTION_INSTANCE_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return GRHUM_INDIVIDU_FONCTION_INSTANCE_KEY;
	}
}
