// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOChangementPositionCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOChangementPositionCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "ChangementPositionCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CHANGEMENT_POSITION_KEY = "changementPosition";
	public static final String CHANGEMENT_POSITION_MANGUE_KEY = "changementPositionMangue";

  private static Logger LOG = Logger.getLogger(_EOChangementPositionCorresp.class);

  public EOChangementPositionCorresp localInstanceIn(EOEditingContext editingContext) {
    EOChangementPositionCorresp localInstance = (EOChangementPositionCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOChangementPositionCorresp.LOG.isDebugEnabled()) {
    	_EOChangementPositionCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOChangementPositionCorresp.LOG.isDebugEnabled()) {
    	_EOChangementPositionCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition changementPosition() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition)storedValueForKey("changementPosition");
  }

  public void setChangementPositionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition value) {
    if (_EOChangementPositionCorresp.LOG.isDebugEnabled()) {
      _EOChangementPositionCorresp.LOG.debug("updating changementPosition from " + changementPosition() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition oldValue = changementPosition();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "changementPosition");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "changementPosition");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition changementPositionMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition)storedValueForKey("changementPositionMangue");
  }

  public void setChangementPositionMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition value) {
    if (_EOChangementPositionCorresp.LOG.isDebugEnabled()) {
      _EOChangementPositionCorresp.LOG.debug("updating changementPositionMangue from " + changementPositionMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition oldValue = changementPositionMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "changementPositionMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "changementPositionMangue");
    }
  }
  

  public static EOChangementPositionCorresp createChangementPositionCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition changementPosition, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition changementPositionMangue) {
    EOChangementPositionCorresp eo = (EOChangementPositionCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOChangementPositionCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setChangementPositionRelationship(changementPosition);
    eo.setChangementPositionMangueRelationship(changementPositionMangue);
    return eo;
  }

  public static NSArray<EOChangementPositionCorresp> fetchAllChangementPositionCorresps(EOEditingContext editingContext) {
    return _EOChangementPositionCorresp.fetchAllChangementPositionCorresps(editingContext, null);
  }

  public static NSArray<EOChangementPositionCorresp> fetchAllChangementPositionCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChangementPositionCorresp.fetchChangementPositionCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOChangementPositionCorresp> fetchChangementPositionCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOChangementPositionCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChangementPositionCorresp> eoObjects = (NSArray<EOChangementPositionCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOChangementPositionCorresp fetchChangementPositionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChangementPositionCorresp.fetchChangementPositionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChangementPositionCorresp fetchChangementPositionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChangementPositionCorresp> eoObjects = _EOChangementPositionCorresp.fetchChangementPositionCorresps(editingContext, qualifier, null);
    EOChangementPositionCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOChangementPositionCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ChangementPositionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChangementPositionCorresp fetchRequiredChangementPositionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChangementPositionCorresp.fetchRequiredChangementPositionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChangementPositionCorresp fetchRequiredChangementPositionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChangementPositionCorresp eoObject = _EOChangementPositionCorresp.fetchChangementPositionCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ChangementPositionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChangementPositionCorresp localInstanceIn(EOEditingContext editingContext, EOChangementPositionCorresp eo) {
    EOChangementPositionCorresp localInstance = (eo == null) ? null : (EOChangementPositionCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
