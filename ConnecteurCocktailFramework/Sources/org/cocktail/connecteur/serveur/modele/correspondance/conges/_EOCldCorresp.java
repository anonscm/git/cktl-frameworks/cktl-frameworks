// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCldCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCldCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CldCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CLD_KEY = "cld";
	public static final String CLD_MANGUE_KEY = "cldMangue";

  private static Logger LOG = Logger.getLogger(_EOCldCorresp.class);

  public EOCldCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCldCorresp localInstance = (EOCldCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCldCorresp.LOG.isDebugEnabled()) {
    	_EOCldCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCldCorresp.LOG.isDebugEnabled()) {
    	_EOCldCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCld cld() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCld)storedValueForKey("cld");
  }

  public void setCldRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCld value) {
    if (_EOCldCorresp.LOG.isDebugEnabled()) {
      _EOCldCorresp.LOG.debug("updating cld from " + cld() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCld oldValue = cld();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cld");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cld");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld cldMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld)storedValueForKey("cldMangue");
  }

  public void setCldMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld value) {
    if (_EOCldCorresp.LOG.isDebugEnabled()) {
      _EOCldCorresp.LOG.debug("updating cldMangue from " + cldMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld oldValue = cldMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cldMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cldMangue");
    }
  }
  

  public static EOCldCorresp createCldCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCld cld, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld cldMangue) {
    EOCldCorresp eo = (EOCldCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCldCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCldRelationship(cld);
    eo.setCldMangueRelationship(cldMangue);
    return eo;
  }

  public static NSArray<EOCldCorresp> fetchAllCldCorresps(EOEditingContext editingContext) {
    return _EOCldCorresp.fetchAllCldCorresps(editingContext, null);
  }

  public static NSArray<EOCldCorresp> fetchAllCldCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCldCorresp.fetchCldCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCldCorresp> fetchCldCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCldCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCldCorresp> eoObjects = (NSArray<EOCldCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCldCorresp fetchCldCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCldCorresp.fetchCldCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCldCorresp fetchCldCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCldCorresp> eoObjects = _EOCldCorresp.fetchCldCorresps(editingContext, qualifier, null);
    EOCldCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCldCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CldCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCldCorresp fetchRequiredCldCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCldCorresp.fetchRequiredCldCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCldCorresp fetchRequiredCldCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCldCorresp eoObject = _EOCldCorresp.fetchCldCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CldCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCldCorresp localInstanceIn(EOEditingContext editingContext, EOCldCorresp eo) {
    EOCldCorresp localInstance = (eo == null) ? null : (EOCldCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
