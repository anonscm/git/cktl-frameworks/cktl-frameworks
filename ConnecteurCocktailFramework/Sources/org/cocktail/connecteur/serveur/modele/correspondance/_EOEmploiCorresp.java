// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOEmploiCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOEmploiCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "EmploiCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String EMPLOI_KEY = "emploi";
	public static final String EMPLOI_MANGUE_KEY = "emploiMangue";

  private static Logger LOG = Logger.getLogger(_EOEmploiCorresp.class);

  public EOEmploiCorresp localInstanceIn(EOEditingContext editingContext) {
    EOEmploiCorresp localInstance = (EOEmploiCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEmploiCorresp.LOG.isDebugEnabled()) {
    	_EOEmploiCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEmploiCorresp.LOG.isDebugEnabled()) {
    	_EOEmploiCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi emploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi)storedValueForKey("emploi");
  }

  public void setEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi value) {
    if (_EOEmploiCorresp.LOG.isDebugEnabled()) {
      _EOEmploiCorresp.LOG.debug("updating emploi from " + emploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi oldValue = emploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi emploiMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi)storedValueForKey("emploiMangue");
  }

  public void setEmploiMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi value) {
    if (_EOEmploiCorresp.LOG.isDebugEnabled()) {
      _EOEmploiCorresp.LOG.debug("updating emploiMangue from " + emploiMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi oldValue = emploiMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploiMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploiMangue");
    }
  }
  

  public static EOEmploiCorresp createEmploiCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi emploi, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi emploiMangue) {
    EOEmploiCorresp eo = (EOEmploiCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOEmploiCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setEmploiRelationship(emploi);
    eo.setEmploiMangueRelationship(emploiMangue);
    return eo;
  }

  public static NSArray<EOEmploiCorresp> fetchAllEmploiCorresps(EOEditingContext editingContext) {
    return _EOEmploiCorresp.fetchAllEmploiCorresps(editingContext, null);
  }

  public static NSArray<EOEmploiCorresp> fetchAllEmploiCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEmploiCorresp.fetchEmploiCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEmploiCorresp> fetchEmploiCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEmploiCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEmploiCorresp> eoObjects = (NSArray<EOEmploiCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEmploiCorresp fetchEmploiCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploiCorresp.fetchEmploiCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploiCorresp fetchEmploiCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEmploiCorresp> eoObjects = _EOEmploiCorresp.fetchEmploiCorresps(editingContext, qualifier, null);
    EOEmploiCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEmploiCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EmploiCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploiCorresp fetchRequiredEmploiCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploiCorresp.fetchRequiredEmploiCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploiCorresp fetchRequiredEmploiCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEmploiCorresp eoObject = _EOEmploiCorresp.fetchEmploiCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EmploiCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploiCorresp localInstanceIn(EOEditingContext editingContext, EOEmploiCorresp eo) {
    EOEmploiCorresp localInstance = (eo == null) ? null : (EOEmploiCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
