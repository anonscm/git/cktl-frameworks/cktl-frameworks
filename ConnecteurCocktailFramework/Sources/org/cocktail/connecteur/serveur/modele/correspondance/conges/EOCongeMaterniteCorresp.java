/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOCongeMaterniteCorresp extends _EOCongeMaterniteCorresp {

	public String nomRelationBaseDestinataire() {
		return CONGE_MATERNITE_MANGUE_KEY;
	}

	public String nomRelationBaseImport() {
		return CONGE_MATERNITE_KEY;
	}

	public void supprimerRelations() {
		setCongeMaterniteRelationship(null);
		setCongeMaterniteMangueRelationship(null);
	}

	// Méthodes statiques
	/**
	 * Retourne le congede maternite de la destination le plus recent pour le
	 * m&circ;me individu et la m&ecirc;me declaration, null si non trouve
	 */
	public static EOMangueCongeMaternite congeMaterniteManguePourIndividuEtDeclaration(EOEditingContext editingContext, Number idSource, Number dmSource) {
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(dmSource);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(CONGE_MATERNITE_KEY + "."
				+ EOCongeMaternite.ID_SOURCE_KEY + " = %@ AND " + CONGE_MATERNITE_KEY + "." + EOCongeMaternite.DM_SOURCE_KEY + " = %@", args), new NSArray(
				EOSortOrdering.sortOrderingWithKey(D_MODIFICATION_KEY, EOSortOrdering.CompareDescending)));
		fs.setFetchLimit(1);
		try {
			return ((EOCongeMaterniteCorresp) editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0)).congeMaterniteMangue();
		} catch (Exception exc) {
			return null;
		}
	}
}
