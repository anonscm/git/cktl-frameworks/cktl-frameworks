package org.cocktail.connecteur.serveur.modele.correspondance.conges;

public class EOCongeAl4Corresp extends _EOCongeAl4Corresp {
	@Override
	public void supprimerRelations() {
		setCongeAl4Relationship(null);
		setMangueCongeAl4Relationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return CONGE_AL4_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_AL4_KEY;
	}
}
