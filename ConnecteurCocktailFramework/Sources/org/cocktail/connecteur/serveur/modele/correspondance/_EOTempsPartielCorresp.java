// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTempsPartielCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTempsPartielCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "TempsPartielCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TEMPS_PARTIEL_KEY = "tempsPartiel";
	public static final String TEMPS_PARTIEL_MANGUE_KEY = "tempsPartielMangue";

  private static Logger LOG = Logger.getLogger(_EOTempsPartielCorresp.class);

  public EOTempsPartielCorresp localInstanceIn(EOEditingContext editingContext) {
    EOTempsPartielCorresp localInstance = (EOTempsPartielCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTempsPartielCorresp.LOG.isDebugEnabled()) {
    	_EOTempsPartielCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTempsPartielCorresp.LOG.isDebugEnabled()) {
    	_EOTempsPartielCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel tempsPartiel() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel)storedValueForKey("tempsPartiel");
  }

  public void setTempsPartielRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel value) {
    if (_EOTempsPartielCorresp.LOG.isDebugEnabled()) {
      _EOTempsPartielCorresp.LOG.debug("updating tempsPartiel from " + tempsPartiel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel oldValue = tempsPartiel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartiel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartiel");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel tempsPartielMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel)storedValueForKey("tempsPartielMangue");
  }

  public void setTempsPartielMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel value) {
    if (_EOTempsPartielCorresp.LOG.isDebugEnabled()) {
      _EOTempsPartielCorresp.LOG.debug("updating tempsPartielMangue from " + tempsPartielMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel oldValue = tempsPartielMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartielMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartielMangue");
    }
  }
  

  public static EOTempsPartielCorresp createTempsPartielCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOTempsPartiel tempsPartiel, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel tempsPartielMangue) {
    EOTempsPartielCorresp eo = (EOTempsPartielCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOTempsPartielCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setTempsPartielRelationship(tempsPartiel);
    eo.setTempsPartielMangueRelationship(tempsPartielMangue);
    return eo;
  }

  public static NSArray<EOTempsPartielCorresp> fetchAllTempsPartielCorresps(EOEditingContext editingContext) {
    return _EOTempsPartielCorresp.fetchAllTempsPartielCorresps(editingContext, null);
  }

  public static NSArray<EOTempsPartielCorresp> fetchAllTempsPartielCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTempsPartielCorresp.fetchTempsPartielCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTempsPartielCorresp> fetchTempsPartielCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTempsPartielCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTempsPartielCorresp> eoObjects = (NSArray<EOTempsPartielCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTempsPartielCorresp fetchTempsPartielCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartielCorresp.fetchTempsPartielCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartielCorresp fetchTempsPartielCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTempsPartielCorresp> eoObjects = _EOTempsPartielCorresp.fetchTempsPartielCorresps(editingContext, qualifier, null);
    EOTempsPartielCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTempsPartielCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TempsPartielCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartielCorresp fetchRequiredTempsPartielCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartielCorresp.fetchRequiredTempsPartielCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartielCorresp fetchRequiredTempsPartielCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTempsPartielCorresp eoObject = _EOTempsPartielCorresp.fetchTempsPartielCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TempsPartielCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartielCorresp localInstanceIn(EOEditingContext editingContext, EOTempsPartielCorresp eo) {
    EOTempsPartielCorresp localInstance = (eo == null) ? null : (EOTempsPartielCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
