// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaladieCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaladieCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongeMaladieCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_MALADIE_KEY = "congeMaladie";
	public static final String CONGE_MALADIE_MANGUE_KEY = "congeMaladieMangue";

  private static Logger LOG = Logger.getLogger(_EOCongeMaladieCorresp.class);

  public EOCongeMaladieCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaladieCorresp localInstance = (EOCongeMaladieCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaladieCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaladieCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaladieCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaladieCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie congeMaladie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie)storedValueForKey("congeMaladie");
  }

  public void setCongeMaladieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie value) {
    if (_EOCongeMaladieCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaladieCorresp.LOG.debug("updating congeMaladie from " + congeMaladie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie oldValue = congeMaladie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladie");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie congeMaladieMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie)storedValueForKey("congeMaladieMangue");
  }

  public void setCongeMaladieMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie value) {
    if (_EOCongeMaladieCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaladieCorresp.LOG.debug("updating congeMaladieMangue from " + congeMaladieMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie oldValue = congeMaladieMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladieMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladieMangue");
    }
  }
  

  public static EOCongeMaladieCorresp createCongeMaladieCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie congeMaladie) {
    EOCongeMaladieCorresp eo = (EOCongeMaladieCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaladieCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeMaladieRelationship(congeMaladie);
    return eo;
  }

  public static NSArray<EOCongeMaladieCorresp> fetchAllCongeMaladieCorresps(EOEditingContext editingContext) {
    return _EOCongeMaladieCorresp.fetchAllCongeMaladieCorresps(editingContext, null);
  }

  public static NSArray<EOCongeMaladieCorresp> fetchAllCongeMaladieCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaladieCorresp.fetchCongeMaladieCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaladieCorresp> fetchCongeMaladieCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaladieCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaladieCorresp> eoObjects = (NSArray<EOCongeMaladieCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaladieCorresp fetchCongeMaladieCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieCorresp.fetchCongeMaladieCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieCorresp fetchCongeMaladieCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaladieCorresp> eoObjects = _EOCongeMaladieCorresp.fetchCongeMaladieCorresps(editingContext, qualifier, null);
    EOCongeMaladieCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaladieCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaladieCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieCorresp fetchRequiredCongeMaladieCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieCorresp.fetchRequiredCongeMaladieCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieCorresp fetchRequiredCongeMaladieCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaladieCorresp eoObject = _EOCongeMaladieCorresp.fetchCongeMaladieCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaladieCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieCorresp localInstanceIn(EOEditingContext editingContext, EOCongeMaladieCorresp eo) {
    EOCongeMaladieCorresp localInstance = (eo == null) ? null : (EOCongeMaladieCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
