// EOChangementPositionCorresp.java
// Created on Mon Jan 07 16:31:25 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueChangementPosition;
import org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOChangementPositionCorresp extends _EOChangementPositionCorresp {

    public EOChangementPositionCorresp() {
        super();
    }


	public String nomRelationBaseDestinataire() {
		return "changementPositionMangue";
	}
	public String nomRelationBaseImport() {
		return "changementPosition";
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(changementPosition(), "changementPosition");
		removeObjectFromBothSidesOfRelationshipWithKey(changementPositionMangue(),"changementPositionMangue");
	}
	// Méthodes statiques
	/** retourne dans les correspondances le changement de position d'import associe a ce segment, cet individu
		et ce numero de changement de position ou commen&ccedil;ant a la m&ecirc;me date   */
	public static EOChangementPositionCorresp correspondancePourChangementPosition(EOEditingContext editingContext,EOChangementPosition changement) {
		NSMutableArray args = new NSMutableArray(changement.toCarriere().carSource());
		args.addObject(changement.individu().idSource());
		args.addObject(changement.cpSource());
		args.addObject(DateCtrl.jourPrecedent(changement.dDebPosition()));
		args.addObject(DateCtrl.jourSuivant(changement.dDebPosition()));
		String stringQualifier = "changementPosition.toCarriere.carSource = %@ AND changementPosition.individu.idSource = %@ AND (changementPosition.cpSource = %@ OR (changementPosition.dDebPosition > %@ AND changementPosition.dDebPosition < %@))";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOChangementPositionCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** retourne la correspondance de l'affectation equivalente pour cet individu et qui commence le m&ecirc;me jour
	 * @param editingContext
	 * @param EOMangueChangement
	 * @return EOChangementPositionCorresp
	 */
	public static EOChangementPositionCorresp correspondancePourChangementMangue(EOEditingContext editingContext,EOMangueChangementPosition changementMangue) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("changementPositionMangue = %@",new NSArray(changementMangue));
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, qualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOChangementPositionCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
}
