// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOContratCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOContratCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "ContratCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONTRAT_KEY = "contrat";
	public static final String CONTRAT_MANGUE_KEY = "contratMangue";

  private static Logger LOG = Logger.getLogger(_EOContratCorresp.class);

  public EOContratCorresp localInstanceIn(EOEditingContext editingContext) {
    EOContratCorresp localInstance = (EOContratCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOContratCorresp.LOG.isDebugEnabled()) {
    	_EOContratCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOContratCorresp.LOG.isDebugEnabled()) {
    	_EOContratCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOContrat contrat() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOContrat value) {
    if (_EOContratCorresp.LOG.isDebugEnabled()) {
      _EOContratCorresp.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat contratMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat)storedValueForKey("contratMangue");
  }

  public void setContratMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat value) {
    if (_EOContratCorresp.LOG.isDebugEnabled()) {
      _EOContratCorresp.LOG.debug("updating contratMangue from " + contratMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat oldValue = contratMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contratMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contratMangue");
    }
  }
  

  public static EOContratCorresp createContratCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOContrat contrat, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat contratMangue) {
    EOContratCorresp eo = (EOContratCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOContratCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setContratRelationship(contrat);
    eo.setContratMangueRelationship(contratMangue);
    return eo;
  }

  public static NSArray<EOContratCorresp> fetchAllContratCorresps(EOEditingContext editingContext) {
    return _EOContratCorresp.fetchAllContratCorresps(editingContext, null);
  }

  public static NSArray<EOContratCorresp> fetchAllContratCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOContratCorresp.fetchContratCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOContratCorresp> fetchContratCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOContratCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOContratCorresp> eoObjects = (NSArray<EOContratCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOContratCorresp fetchContratCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratCorresp.fetchContratCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratCorresp fetchContratCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOContratCorresp> eoObjects = _EOContratCorresp.fetchContratCorresps(editingContext, qualifier, null);
    EOContratCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOContratCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ContratCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratCorresp fetchRequiredContratCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratCorresp.fetchRequiredContratCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratCorresp fetchRequiredContratCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOContratCorresp eoObject = _EOContratCorresp.fetchContratCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ContratCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratCorresp localInstanceIn(EOEditingContext editingContext, EOContratCorresp eo) {
    EOContratCorresp localInstance = (eo == null) ? null : (EOContratCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
