package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOInstanceCorresp extends _EOInstanceCorresp {
	@Override
	public void supprimerRelations() {
		setInstanceRelationship(null);
		setGrhumInstanceRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return INSTANCE_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return GRHUM_INSTANCE_KEY;
	}
}
