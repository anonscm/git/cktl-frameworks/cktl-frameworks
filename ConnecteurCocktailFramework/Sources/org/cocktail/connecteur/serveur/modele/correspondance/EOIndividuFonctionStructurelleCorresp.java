package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOIndividuFonctionStructurelleCorresp extends _EOIndividuFonctionStructurelleCorresp {
	@Override
	public void supprimerRelations() {
		setIndividuFonctionStructurelleRelationship(null);
		setGrhumIndividuFonctionStructurelleRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return INDIVIDU_FONCTION_STRUCTURELLE_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return GRHUM_INDIVIDU_FONCTION_STRUCTURELLE_KEY;
	}
}
