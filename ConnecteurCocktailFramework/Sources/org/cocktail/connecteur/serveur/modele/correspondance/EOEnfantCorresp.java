// EOEnfantCorresp.java
// Created on Wed Jul 25 15:05:21 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOEnfantCorresp extends _EOEnfantCorresp {

    public EOEnfantCorresp() {
        super();
    }


	public String nomRelationBaseImport() {
		return "enfant";
	}
	public String nomRelationBaseDestinataire() {
		return "enfantGrhum";
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(enfant(), "enfant");
		removeObjectFromBothSidesOfRelationshipWithKey(enfantGrhum(),"enfantGrhum");
	}
	// Méthodes statiques
	/** retourne l'enfant d'import associe a l'individu  et &grave; l'identifiant de l'enfant
	 * On commence par chercher dans l'import courant pour &ecirc;tre s&ucirc;r 
	 * de retourner le dernier enfant, sinon on recherche dans les correspondances */
	public static EOEnfant enfantPourIndividuEtId(EOEditingContext editingContext,Number idSource,Number enfSource) {
		if (editingContext==null || idSource==null || enfSource==null)
			return null;
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(enfSource);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@ AND enfSource = %@ AND temImport = %@ AND statut <> 'A' AND statut <> 'E'",args);
		EOFetchSpecification myFetch = new EOFetchSpecification ("Enfant", myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOEnfant)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e1) {
			// Rechercher dans les correspondances si l'élément de carrière n'a pas déjà été importé
			myQualifier = EOQualifier.qualifierWithQualifierFormat("enfant.individu.idSource = %@ AND enfant.enfSource = %@",args);
			myFetch = new EOFetchSpecification ("EnfantCorresp", myQualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			try {
				return ((EOEnfantCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0)).enfant();
			} catch (Exception e) {
				return null;
				
			}
		}
	}
	
	/** retourne l'enfant Grhum associe a l'enfant d'import 
	 * @param editingContext editing context 
	 * @param enfant enfant de la base d'import
	 **/
	public static EOGrhumEnfant enfantGrhum(EOEditingContext editingContext,EOEnfant enfant) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("enfant = %@",new NSArray(enfant));
		EOFetchSpecification myFetch = new EOFetchSpecification ("EnfantCorresp", myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			EOEnfantCorresp corresp = (EOEnfantCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
			return corresp.enfantGrhum();
		} catch (Exception e) {
			return null;
		}
	}
}
