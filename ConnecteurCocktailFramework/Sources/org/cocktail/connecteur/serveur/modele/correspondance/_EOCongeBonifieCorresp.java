// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeBonifieCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeBonifieCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CongeBonifieCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_BONIFIE_KEY = "congeBonifie";
	public static final String MANGUE_CONGE_BONIFIE_KEY = "mangueCongeBonifie";

  private static Logger LOG = Logger.getLogger(_EOCongeBonifieCorresp.class);

  public EOCongeBonifieCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeBonifieCorresp localInstance = (EOCongeBonifieCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeBonifieCorresp.LOG.isDebugEnabled()) {
    	_EOCongeBonifieCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeBonifieCorresp.LOG.isDebugEnabled()) {
    	_EOCongeBonifieCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie congeBonifie() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie)storedValueForKey("congeBonifie");
  }

  public void setCongeBonifieRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie value) {
    if (_EOCongeBonifieCorresp.LOG.isDebugEnabled()) {
      _EOCongeBonifieCorresp.LOG.debug("updating congeBonifie from " + congeBonifie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie oldValue = congeBonifie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeBonifie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeBonifie");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeBonifie mangueCongeBonifie() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeBonifie)storedValueForKey("mangueCongeBonifie");
  }

  public void setMangueCongeBonifieRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeBonifie value) {
    if (_EOCongeBonifieCorresp.LOG.isDebugEnabled()) {
      _EOCongeBonifieCorresp.LOG.debug("updating mangueCongeBonifie from " + mangueCongeBonifie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeBonifie oldValue = mangueCongeBonifie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeBonifie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeBonifie");
    }
  }
  

  public static EOCongeBonifieCorresp createCongeBonifieCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeBonifie congeBonifie, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeBonifie mangueCongeBonifie) {
    EOCongeBonifieCorresp eo = (EOCongeBonifieCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeBonifieCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeBonifieRelationship(congeBonifie);
    eo.setMangueCongeBonifieRelationship(mangueCongeBonifie);
    return eo;
  }

  public static NSArray<EOCongeBonifieCorresp> fetchAllCongeBonifieCorresps(EOEditingContext editingContext) {
    return _EOCongeBonifieCorresp.fetchAllCongeBonifieCorresps(editingContext, null);
  }

  public static NSArray<EOCongeBonifieCorresp> fetchAllCongeBonifieCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeBonifieCorresp.fetchCongeBonifieCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeBonifieCorresp> fetchCongeBonifieCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeBonifieCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeBonifieCorresp> eoObjects = (NSArray<EOCongeBonifieCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeBonifieCorresp fetchCongeBonifieCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeBonifieCorresp.fetchCongeBonifieCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeBonifieCorresp fetchCongeBonifieCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeBonifieCorresp> eoObjects = _EOCongeBonifieCorresp.fetchCongeBonifieCorresps(editingContext, qualifier, null);
    EOCongeBonifieCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeBonifieCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeBonifieCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeBonifieCorresp fetchRequiredCongeBonifieCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeBonifieCorresp.fetchRequiredCongeBonifieCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeBonifieCorresp fetchRequiredCongeBonifieCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeBonifieCorresp eoObject = _EOCongeBonifieCorresp.fetchCongeBonifieCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeBonifieCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeBonifieCorresp localInstanceIn(EOEditingContext editingContext, EOCongeBonifieCorresp eo) {
    EOCongeBonifieCorresp localInstance = (eo == null) ? null : (EOCongeBonifieCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
