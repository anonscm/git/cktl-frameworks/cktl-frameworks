// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAl3Corresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAl3Corresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongeAl3Corresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_AL3_KEY = "congeAl3";
	public static final String MANGUE_CONGE_AL3_KEY = "mangueCongeAl3";

  private static Logger LOG = Logger.getLogger(_EOCongeAl3Corresp.class);

  public EOCongeAl3Corresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeAl3Corresp localInstance = (EOCongeAl3Corresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAl3Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl3Corresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAl3Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl3Corresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 congeAl3() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3)storedValueForKey("congeAl3");
  }

  public void setCongeAl3Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 value) {
    if (_EOCongeAl3Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl3Corresp.LOG.debug("updating congeAl3 from " + congeAl3() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 oldValue = congeAl3();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl3");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl3");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3 mangueCongeAl3() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3)storedValueForKey("mangueCongeAl3");
  }

  public void setMangueCongeAl3Relationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3 value) {
    if (_EOCongeAl3Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl3Corresp.LOG.debug("updating mangueCongeAl3 from " + mangueCongeAl3() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3 oldValue = mangueCongeAl3();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeAl3");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeAl3");
    }
  }
  

  public static EOCongeAl3Corresp createCongeAl3Corresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl3 congeAl3, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3 mangueCongeAl3) {
    EOCongeAl3Corresp eo = (EOCongeAl3Corresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAl3Corresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeAl3Relationship(congeAl3);
    eo.setMangueCongeAl3Relationship(mangueCongeAl3);
    return eo;
  }

  public static NSArray<EOCongeAl3Corresp> fetchAllCongeAl3Corresps(EOEditingContext editingContext) {
    return _EOCongeAl3Corresp.fetchAllCongeAl3Corresps(editingContext, null);
  }

  public static NSArray<EOCongeAl3Corresp> fetchAllCongeAl3Corresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAl3Corresp.fetchCongeAl3Corresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAl3Corresp> fetchCongeAl3Corresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAl3Corresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAl3Corresp> eoObjects = (NSArray<EOCongeAl3Corresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAl3Corresp fetchCongeAl3Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl3Corresp.fetchCongeAl3Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl3Corresp fetchCongeAl3Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAl3Corresp> eoObjects = _EOCongeAl3Corresp.fetchCongeAl3Corresps(editingContext, qualifier, null);
    EOCongeAl3Corresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAl3Corresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAl3Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl3Corresp fetchRequiredCongeAl3Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl3Corresp.fetchRequiredCongeAl3Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl3Corresp fetchRequiredCongeAl3Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAl3Corresp eoObject = _EOCongeAl3Corresp.fetchCongeAl3Corresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAl3Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl3Corresp localInstanceIn(EOEditingContext editingContext, EOCongeAl3Corresp eo) {
    EOCongeAl3Corresp localInstance = (eo == null) ? null : (EOCongeAl3Corresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
