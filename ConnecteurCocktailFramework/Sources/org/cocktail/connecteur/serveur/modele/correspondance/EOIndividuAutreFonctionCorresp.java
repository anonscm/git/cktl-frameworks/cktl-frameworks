package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOIndividuAutreFonctionCorresp extends _EOIndividuAutreFonctionCorresp {
	@Override
	public void supprimerRelations() {
		setGrhumIndividuAutreFonctionRelationship(null);
		setIndividuAutreFonctionRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return INDIVIDU_AUTRE_FONCTION_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return GRHUM_INDIVIDU_AUTRE_FONCTION_KEY;
	}
}
