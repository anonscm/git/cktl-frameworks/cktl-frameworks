package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

/**.
 * Classe gérant les correspondances pour des Congés d'Adoption d'un individu
 * @author alainmalaplate
 *
 */
public class EOCongeAdoptionCorresp extends _EOCongeAdoptionCorresp {

	private static final long serialVersionUID = 5617439950278937400L;
	private static Logger log = Logger.getLogger(EOCongeAdoptionCorresp.class);

	public EOCongeAdoptionCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_ADOPTION_KEY;
	}

	public String nomRelationBaseImport() {
		return CONGE_ADOPTION_KEY;
	}

	public void supprimerRelations() {
		setCongeAdoptionRelationship(null);
		setMangueCongeAdoptionRelationship(null);
	}
}
