// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCldDetailCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCldDetailCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CldDetailCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CLD_DETAIL_KEY = "cldDetail";
	public static final String CLD_DETAIL_MANGUE_KEY = "cldDetailMangue";

  private static Logger LOG = Logger.getLogger(_EOCldDetailCorresp.class);

  public EOCldDetailCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCldDetailCorresp localInstance = (EOCldDetailCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCldDetailCorresp.LOG.isDebugEnabled()) {
    	_EOCldDetailCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCldDetailCorresp.LOG.isDebugEnabled()) {
    	_EOCldDetailCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail cldDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail)storedValueForKey("cldDetail");
  }

  public void setCldDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail value) {
    if (_EOCldDetailCorresp.LOG.isDebugEnabled()) {
      _EOCldDetailCorresp.LOG.debug("updating cldDetail from " + cldDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail oldValue = cldDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cldDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cldDetail");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCldDetail cldDetailMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCldDetail)storedValueForKey("cldDetailMangue");
  }

  public void setCldDetailMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCldDetail value) {
    if (_EOCldDetailCorresp.LOG.isDebugEnabled()) {
      _EOCldDetailCorresp.LOG.debug("updating cldDetailMangue from " + cldDetailMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCldDetail oldValue = cldDetailMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "cldDetailMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "cldDetailMangue");
    }
  }
  

  public static EOCldDetailCorresp createCldDetailCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail cldDetail, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCldDetail cldDetailMangue) {
    EOCldDetailCorresp eo = (EOCldDetailCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCldDetailCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCldDetailRelationship(cldDetail);
    eo.setCldDetailMangueRelationship(cldDetailMangue);
    return eo;
  }

  public static NSArray<EOCldDetailCorresp> fetchAllCldDetailCorresps(EOEditingContext editingContext) {
    return _EOCldDetailCorresp.fetchAllCldDetailCorresps(editingContext, null);
  }

  public static NSArray<EOCldDetailCorresp> fetchAllCldDetailCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCldDetailCorresp.fetchCldDetailCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCldDetailCorresp> fetchCldDetailCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCldDetailCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCldDetailCorresp> eoObjects = (NSArray<EOCldDetailCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCldDetailCorresp fetchCldDetailCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCldDetailCorresp.fetchCldDetailCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCldDetailCorresp fetchCldDetailCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCldDetailCorresp> eoObjects = _EOCldDetailCorresp.fetchCldDetailCorresps(editingContext, qualifier, null);
    EOCldDetailCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCldDetailCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CldDetailCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCldDetailCorresp fetchRequiredCldDetailCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCldDetailCorresp.fetchRequiredCldDetailCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCldDetailCorresp fetchRequiredCldDetailCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCldDetailCorresp eoObject = _EOCldDetailCorresp.fetchCldDetailCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CldDetailCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCldDetailCorresp localInstanceIn(EOEditingContext editingContext, EOCldDetailCorresp eo) {
    EOCldDetailCorresp localInstance = (eo == null) ? null : (EOCldDetailCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
