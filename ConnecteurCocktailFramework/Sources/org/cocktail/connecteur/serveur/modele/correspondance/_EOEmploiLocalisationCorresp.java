// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOEmploiLocalisationCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOEmploiLocalisationCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "EmploiLocalisationCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_EMPLOI_LOCALISATION_KEY = "toEmploiLocalisation";
	public static final String TO_MANGUE_EMPLOI_LOCALISATION_KEY = "toMangueEmploiLocalisation";

  private static Logger LOG = Logger.getLogger(_EOEmploiLocalisationCorresp.class);

  public EOEmploiLocalisationCorresp localInstanceIn(EOEditingContext editingContext) {
    EOEmploiLocalisationCorresp localInstance = (EOEmploiLocalisationCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEmploiLocalisationCorresp.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisationCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEmploiLocalisationCorresp.LOG.isDebugEnabled()) {
    	_EOEmploiLocalisationCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation toEmploiLocalisation() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation)storedValueForKey("toEmploiLocalisation");
  }

  public void setToEmploiLocalisationRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation value) {
    if (_EOEmploiLocalisationCorresp.LOG.isDebugEnabled()) {
      _EOEmploiLocalisationCorresp.LOG.debug("updating toEmploiLocalisation from " + toEmploiLocalisation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation oldValue = toEmploiLocalisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEmploiLocalisation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEmploiLocalisation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiLocalisation toMangueEmploiLocalisation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiLocalisation)storedValueForKey("toMangueEmploiLocalisation");
  }

  public void setToMangueEmploiLocalisationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiLocalisation value) {
    if (_EOEmploiLocalisationCorresp.LOG.isDebugEnabled()) {
      _EOEmploiLocalisationCorresp.LOG.debug("updating toMangueEmploiLocalisation from " + toMangueEmploiLocalisation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiLocalisation oldValue = toMangueEmploiLocalisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueEmploiLocalisation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueEmploiLocalisation");
    }
  }
  

  public static EOEmploiLocalisationCorresp createEmploiLocalisationCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation toEmploiLocalisation, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiLocalisation toMangueEmploiLocalisation) {
    EOEmploiLocalisationCorresp eo = (EOEmploiLocalisationCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOEmploiLocalisationCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToEmploiLocalisationRelationship(toEmploiLocalisation);
    eo.setToMangueEmploiLocalisationRelationship(toMangueEmploiLocalisation);
    return eo;
  }

  public static NSArray<EOEmploiLocalisationCorresp> fetchAllEmploiLocalisationCorresps(EOEditingContext editingContext) {
    return _EOEmploiLocalisationCorresp.fetchAllEmploiLocalisationCorresps(editingContext, null);
  }

  public static NSArray<EOEmploiLocalisationCorresp> fetchAllEmploiLocalisationCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEmploiLocalisationCorresp.fetchEmploiLocalisationCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEmploiLocalisationCorresp> fetchEmploiLocalisationCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEmploiLocalisationCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEmploiLocalisationCorresp> eoObjects = (NSArray<EOEmploiLocalisationCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEmploiLocalisationCorresp fetchEmploiLocalisationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploiLocalisationCorresp.fetchEmploiLocalisationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploiLocalisationCorresp fetchEmploiLocalisationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEmploiLocalisationCorresp> eoObjects = _EOEmploiLocalisationCorresp.fetchEmploiLocalisationCorresps(editingContext, qualifier, null);
    EOEmploiLocalisationCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEmploiLocalisationCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EmploiLocalisationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploiLocalisationCorresp fetchRequiredEmploiLocalisationCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEmploiLocalisationCorresp.fetchRequiredEmploiLocalisationCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEmploiLocalisationCorresp fetchRequiredEmploiLocalisationCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEmploiLocalisationCorresp eoObject = _EOEmploiLocalisationCorresp.fetchEmploiLocalisationCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EmploiLocalisationCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEmploiLocalisationCorresp localInstanceIn(EOEditingContext editingContext, EOEmploiLocalisationCorresp eo) {
    EOEmploiLocalisationCorresp localInstance = (eo == null) ? null : (EOEmploiLocalisationCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
