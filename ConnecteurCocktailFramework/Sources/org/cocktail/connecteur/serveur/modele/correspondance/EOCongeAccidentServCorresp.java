package org.cocktail.connecteur.serveur.modele.correspondance;


public class EOCongeAccidentServCorresp extends _EOCongeAccidentServCorresp {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2338156071406002796L;

	public EOCongeAccidentServCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return CONGE_ACCIDENT_SERV_MANGUE_KEY;
	}

	public String nomRelationBaseImport() {
		return CONGE_ACCIDENT_SERV_KEY;
	}

	public void supprimerRelations() {
		setCongeAccidentServRelationship(null);
		setCongeAccidentServMangueRelationship(null);
	}
}
