// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongePaterniteCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongePaterniteCorresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongePaterniteCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_CONGE_PATERNITE_KEY = "toCongePaternite";
	public static final String TO_MANGUE_CONGE_PATERNITE_KEY = "toMangueCongePaternite";

  private static Logger LOG = Logger.getLogger(_EOCongePaterniteCorresp.class);

  public EOCongePaterniteCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongePaterniteCorresp localInstance = (EOCongePaterniteCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongePaterniteCorresp.LOG.isDebugEnabled()) {
    	_EOCongePaterniteCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongePaterniteCorresp.LOG.isDebugEnabled()) {
    	_EOCongePaterniteCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite toCongePaternite() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite)storedValueForKey("toCongePaternite");
  }

  public void setToCongePaterniteRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite value) {
    if (_EOCongePaterniteCorresp.LOG.isDebugEnabled()) {
      _EOCongePaterniteCorresp.LOG.debug("updating toCongePaternite from " + toCongePaternite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite oldValue = toCongePaternite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongePaternite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongePaternite");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite toMangueCongePaternite() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite)storedValueForKey("toMangueCongePaternite");
  }

  public void setToMangueCongePaterniteRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite value) {
    if (_EOCongePaterniteCorresp.LOG.isDebugEnabled()) {
      _EOCongePaterniteCorresp.LOG.debug("updating toMangueCongePaternite from " + toMangueCongePaternite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite oldValue = toMangueCongePaternite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueCongePaternite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueCongePaternite");
    }
  }
  

  public static EOCongePaterniteCorresp createCongePaterniteCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongePaternite toCongePaternite, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite toMangueCongePaternite) {
    EOCongePaterniteCorresp eo = (EOCongePaterniteCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongePaterniteCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToCongePaterniteRelationship(toCongePaternite);
    eo.setToMangueCongePaterniteRelationship(toMangueCongePaternite);
    return eo;
  }

  public static NSArray<EOCongePaterniteCorresp> fetchAllCongePaterniteCorresps(EOEditingContext editingContext) {
    return _EOCongePaterniteCorresp.fetchAllCongePaterniteCorresps(editingContext, null);
  }

  public static NSArray<EOCongePaterniteCorresp> fetchAllCongePaterniteCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongePaterniteCorresp.fetchCongePaterniteCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongePaterniteCorresp> fetchCongePaterniteCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongePaterniteCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongePaterniteCorresp> eoObjects = (NSArray<EOCongePaterniteCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongePaterniteCorresp fetchCongePaterniteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongePaterniteCorresp.fetchCongePaterniteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongePaterniteCorresp fetchCongePaterniteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongePaterniteCorresp> eoObjects = _EOCongePaterniteCorresp.fetchCongePaterniteCorresps(editingContext, qualifier, null);
    EOCongePaterniteCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongePaterniteCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongePaterniteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongePaterniteCorresp fetchRequiredCongePaterniteCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongePaterniteCorresp.fetchRequiredCongePaterniteCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongePaterniteCorresp fetchRequiredCongePaterniteCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongePaterniteCorresp eoObject = _EOCongePaterniteCorresp.fetchCongePaterniteCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongePaterniteCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongePaterniteCorresp localInstanceIn(EOEditingContext editingContext, EOCongePaterniteCorresp eo) {
    EOCongePaterniteCorresp localInstance = (eo == null) ? null : (EOCongePaterniteCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
