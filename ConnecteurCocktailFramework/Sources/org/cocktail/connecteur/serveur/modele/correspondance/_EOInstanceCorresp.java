// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOInstanceCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOInstanceCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "InstanceCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String GRHUM_INSTANCE_KEY = "grhumInstance";
	public static final String INSTANCE_KEY = "instance";

  private static Logger LOG = Logger.getLogger(_EOInstanceCorresp.class);

  public EOInstanceCorresp localInstanceIn(EOEditingContext editingContext) {
    EOInstanceCorresp localInstance = (EOInstanceCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOInstanceCorresp.LOG.isDebugEnabled()) {
    	_EOInstanceCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOInstanceCorresp.LOG.isDebugEnabled()) {
    	_EOInstanceCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumInstance grhumInstance() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumInstance)storedValueForKey("grhumInstance");
  }

  public void setGrhumInstanceRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumInstance value) {
    if (_EOInstanceCorresp.LOG.isDebugEnabled()) {
      _EOInstanceCorresp.LOG.debug("updating grhumInstance from " + grhumInstance() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumInstance oldValue = grhumInstance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "grhumInstance");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "grhumInstance");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOInstance instance() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOInstance)storedValueForKey("instance");
  }

  public void setInstanceRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOInstance value) {
    if (_EOInstanceCorresp.LOG.isDebugEnabled()) {
      _EOInstanceCorresp.LOG.debug("updating instance from " + instance() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOInstance oldValue = instance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "instance");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "instance");
    }
  }
  

  public static EOInstanceCorresp createInstanceCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumInstance grhumInstance, org.cocktail.connecteur.serveur.modele.entite_import.EOInstance instance) {
    EOInstanceCorresp eo = (EOInstanceCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOInstanceCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setGrhumInstanceRelationship(grhumInstance);
    eo.setInstanceRelationship(instance);
    return eo;
  }

  public static NSArray<EOInstanceCorresp> fetchAllInstanceCorresps(EOEditingContext editingContext) {
    return _EOInstanceCorresp.fetchAllInstanceCorresps(editingContext, null);
  }

  public static NSArray<EOInstanceCorresp> fetchAllInstanceCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOInstanceCorresp.fetchInstanceCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOInstanceCorresp> fetchInstanceCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOInstanceCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOInstanceCorresp> eoObjects = (NSArray<EOInstanceCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOInstanceCorresp fetchInstanceCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInstanceCorresp.fetchInstanceCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInstanceCorresp fetchInstanceCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOInstanceCorresp> eoObjects = _EOInstanceCorresp.fetchInstanceCorresps(editingContext, qualifier, null);
    EOInstanceCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOInstanceCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one InstanceCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInstanceCorresp fetchRequiredInstanceCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInstanceCorresp.fetchRequiredInstanceCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInstanceCorresp fetchRequiredInstanceCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOInstanceCorresp eoObject = _EOInstanceCorresp.fetchInstanceCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no InstanceCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInstanceCorresp localInstanceIn(EOEditingContext editingContext, EOInstanceCorresp eo) {
    EOInstanceCorresp localInstance = (eo == null) ? null : (EOInstanceCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
