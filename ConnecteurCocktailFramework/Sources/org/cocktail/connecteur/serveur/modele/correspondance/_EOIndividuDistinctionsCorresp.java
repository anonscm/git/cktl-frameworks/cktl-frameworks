// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuDistinctionsCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuDistinctionsCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "IndividuDistinctionsCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_DISTINCTIONS_KEY = "individuDistinctions";
	public static final String MANGUE_INDIVIDU_DISTINCTIONS_KEY = "mangueIndividuDistinctions";

  private static Logger LOG = Logger.getLogger(_EOIndividuDistinctionsCorresp.class);

  public EOIndividuDistinctionsCorresp localInstanceIn(EOEditingContext editingContext) {
    EOIndividuDistinctionsCorresp localInstance = (EOIndividuDistinctionsCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuDistinctionsCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctionsCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuDistinctionsCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuDistinctionsCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions individuDistinctions() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions)storedValueForKey("individuDistinctions");
  }

  public void setIndividuDistinctionsRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions value) {
    if (_EOIndividuDistinctionsCorresp.LOG.isDebugEnabled()) {
      _EOIndividuDistinctionsCorresp.LOG.debug("updating individuDistinctions from " + individuDistinctions() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions oldValue = individuDistinctions();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuDistinctions");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuDistinctions");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDistinctions mangueIndividuDistinctions() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDistinctions)storedValueForKey("mangueIndividuDistinctions");
  }

  public void setMangueIndividuDistinctionsRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDistinctions value) {
    if (_EOIndividuDistinctionsCorresp.LOG.isDebugEnabled()) {
      _EOIndividuDistinctionsCorresp.LOG.debug("updating mangueIndividuDistinctions from " + mangueIndividuDistinctions() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDistinctions oldValue = mangueIndividuDistinctions();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueIndividuDistinctions");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueIndividuDistinctions");
    }
  }
  

  public static EOIndividuDistinctionsCorresp createIndividuDistinctionsCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDistinctions individuDistinctions, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueIndividuDistinctions mangueIndividuDistinctions) {
    EOIndividuDistinctionsCorresp eo = (EOIndividuDistinctionsCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuDistinctionsCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuDistinctionsRelationship(individuDistinctions);
    eo.setMangueIndividuDistinctionsRelationship(mangueIndividuDistinctions);
    return eo;
  }

  public static NSArray<EOIndividuDistinctionsCorresp> fetchAllIndividuDistinctionsCorresps(EOEditingContext editingContext) {
    return _EOIndividuDistinctionsCorresp.fetchAllIndividuDistinctionsCorresps(editingContext, null);
  }

  public static NSArray<EOIndividuDistinctionsCorresp> fetchAllIndividuDistinctionsCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuDistinctionsCorresp.fetchIndividuDistinctionsCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuDistinctionsCorresp> fetchIndividuDistinctionsCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuDistinctionsCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuDistinctionsCorresp> eoObjects = (NSArray<EOIndividuDistinctionsCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuDistinctionsCorresp fetchIndividuDistinctionsCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDistinctionsCorresp.fetchIndividuDistinctionsCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDistinctionsCorresp fetchIndividuDistinctionsCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuDistinctionsCorresp> eoObjects = _EOIndividuDistinctionsCorresp.fetchIndividuDistinctionsCorresps(editingContext, qualifier, null);
    EOIndividuDistinctionsCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuDistinctionsCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuDistinctionsCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDistinctionsCorresp fetchRequiredIndividuDistinctionsCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDistinctionsCorresp.fetchRequiredIndividuDistinctionsCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDistinctionsCorresp fetchRequiredIndividuDistinctionsCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuDistinctionsCorresp eoObject = _EOIndividuDistinctionsCorresp.fetchIndividuDistinctionsCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuDistinctionsCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDistinctionsCorresp localInstanceIn(EOEditingContext editingContext, EOIndividuDistinctionsCorresp eo) {
    EOIndividuDistinctionsCorresp localInstance = (eo == null) ? null : (EOIndividuDistinctionsCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
