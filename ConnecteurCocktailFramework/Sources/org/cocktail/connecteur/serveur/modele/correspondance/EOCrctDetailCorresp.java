package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

/**.
 * Classe gérant les correspondances pour les Détails d'un CRCT d'un individu
 * @author alainmalaplate
 *
 */
public class EOCrctDetailCorresp extends _EOCrctDetailCorresp {

	private static final long serialVersionUID = -3675414601741110223L;
	private static Logger log = Logger.getLogger(EOCrctDetailCorresp.class);

	public EOCrctDetailCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CRCT_DETAIL_KEY;
	}

	public String nomRelationBaseImport() {
		return CRCT_DETAIL_KEY;
	}

	public void supprimerRelations() {
		setCrctDetailRelationship(null);
		setMangueCrctDetailRelationship(null);
	}
	
	
	
}
