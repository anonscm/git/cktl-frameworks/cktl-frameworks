package org.cocktail.connecteur.serveur.modele.correspondance;

public class EOVacataireAffectationCorresp extends _EOVacataireAffectationCorresp {
	@Override
	public void supprimerRelations() {
		setVacataireAffectationRelationship(null);
		setMangueVacatairesAffectationRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return VACATAIRE_AFFECTATION_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_VACATAIRES_AFFECTATION_KEY;
	}
}
