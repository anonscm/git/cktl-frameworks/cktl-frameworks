package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EOCongeFormationCorresp extends _EOCongeFormationCorresp {
  private static Logger log = Logger.getLogger(EOCongeFormationCorresp.class);
  
  public EOCongeFormationCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_FORMATION_KEY;
	}

	public String nomRelationBaseImport() {
		return CONGE_FORMATION_KEY;
	}

	public void supprimerRelations() {
		setCongeFormationRelationship(null);
		setMangueCongeFormationRelationship(null);
	}
  
}
