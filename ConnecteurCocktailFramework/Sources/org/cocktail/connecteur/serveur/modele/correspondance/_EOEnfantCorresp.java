// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOEnfantCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOEnfantCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "EnfantCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String ENFANT_KEY = "enfant";
	public static final String ENFANT_GRHUM_KEY = "enfantGrhum";

  private static Logger LOG = Logger.getLogger(_EOEnfantCorresp.class);

  public EOEnfantCorresp localInstanceIn(EOEditingContext editingContext) {
    EOEnfantCorresp localInstance = (EOEnfantCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEnfantCorresp.LOG.isDebugEnabled()) {
    	_EOEnfantCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEnfantCorresp.LOG.isDebugEnabled()) {
    	_EOEnfantCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant enfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant)storedValueForKey("enfant");
  }

  public void setEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant value) {
    if (_EOEnfantCorresp.LOG.isDebugEnabled()) {
      _EOEnfantCorresp.LOG.debug("updating enfant from " + enfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant enfantGrhum() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("enfantGrhum");
  }

  public void setEnfantGrhumRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOEnfantCorresp.LOG.isDebugEnabled()) {
      _EOEnfantCorresp.LOG.debug("updating enfantGrhum from " + enfantGrhum() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = enfantGrhum();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfantGrhum");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfantGrhum");
    }
  }
  

  public static EOEnfantCorresp createEnfantCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant enfant, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant enfantGrhum) {
    EOEnfantCorresp eo = (EOEnfantCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOEnfantCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setEnfantRelationship(enfant);
    eo.setEnfantGrhumRelationship(enfantGrhum);
    return eo;
  }

  public static NSArray<EOEnfantCorresp> fetchAllEnfantCorresps(EOEditingContext editingContext) {
    return _EOEnfantCorresp.fetchAllEnfantCorresps(editingContext, null);
  }

  public static NSArray<EOEnfantCorresp> fetchAllEnfantCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEnfantCorresp.fetchEnfantCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEnfantCorresp> fetchEnfantCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEnfantCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEnfantCorresp> eoObjects = (NSArray<EOEnfantCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEnfantCorresp fetchEnfantCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnfantCorresp.fetchEnfantCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEnfantCorresp fetchEnfantCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEnfantCorresp> eoObjects = _EOEnfantCorresp.fetchEnfantCorresps(editingContext, qualifier, null);
    EOEnfantCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEnfantCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EnfantCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEnfantCorresp fetchRequiredEnfantCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnfantCorresp.fetchRequiredEnfantCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEnfantCorresp fetchRequiredEnfantCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEnfantCorresp eoObject = _EOEnfantCorresp.fetchEnfantCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EnfantCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEnfantCorresp localInstanceIn(EOEditingContext editingContext, EOEnfantCorresp eo) {
    EOEnfantCorresp localInstance = (eo == null) ? null : (EOEnfantCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
