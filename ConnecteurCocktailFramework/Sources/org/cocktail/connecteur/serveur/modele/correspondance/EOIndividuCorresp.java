//EOIndividuCorresp.java
//Created on Wed Jul 25 15:05:38 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOIndividuCorresp extends _EOIndividuCorresp {

	public EOIndividuCorresp() {
		super();
	}

	public String nomRelationBaseImport() {
		return "individu";
	}

	public String nomRelationBaseDestinataire() {
		return "individuGrhum";
	}

	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(individu(), "individu");
		removeObjectFromBothSidesOfRelationshipWithKey(individuGrhum(), "individuGrhum");
	}

	// Méthodes statiques
	/**
	 * retourne l'individu Grhum assoc&iee a l'individu d'import
	 * 
	 * @param editingContext
	 *            editing context
	 * @param idSource
	 *            identifiant de l'individu
	 **/
	public static EOGrhumIndividu individuGrhum(EOEditingContext editingContext, Number idSource) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("individu.idSource = %@", new NSArray(idSource));

		EOFetchSpecification myFetch = new EOFetchSpecification("IndividuCorresp", myQualifier, null);
		try {
			EOIndividuCorresp corresp = (EOIndividuCorresp) editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
			return corresp.individuGrhum();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * retourne l'individu Grhum associe a l'individu d'import
	 * 
	 * @param editingContext
	 *            editing context
	 * @param individu
	 *            individu de la base d'import
	 **/
	public static EOGrhumIndividu individuGrhum(EOEditingContext editingContext, EOIndividu individu) {
		if (individu==null)
			return null;
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu));
		EOFetchSpecification myFetch = new EOFetchSpecification("IndividuCorresp", myQualifier, null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			EOIndividuCorresp corresp = (EOIndividuCorresp) editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
			return corresp.individuGrhum();
		} catch (Exception e) {
			return null;
		}
	}

	/** retourne le persId de l'individu Grhum associe a l'individu d'import */
	public static Number persIdPourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			return individuGrhum.persId();
		} else {
			return null;
		}
	}

}
