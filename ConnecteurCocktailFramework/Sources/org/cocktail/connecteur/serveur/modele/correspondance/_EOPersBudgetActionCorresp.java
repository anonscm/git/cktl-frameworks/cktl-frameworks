// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPersBudgetActionCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPersBudgetActionCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "PersBudgetActionCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_MANGUE_PERS_BUDGET_ACTION_KEY = "toManguePersBudgetAction";
	public static final String TO_PERS_BUDGET_ACTION_KEY = "toPersBudgetAction";

  private static Logger LOG = Logger.getLogger(_EOPersBudgetActionCorresp.class);

  public EOPersBudgetActionCorresp localInstanceIn(EOEditingContext editingContext) {
    EOPersBudgetActionCorresp localInstance = (EOPersBudgetActionCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPersBudgetActionCorresp.LOG.isDebugEnabled()) {
    	_EOPersBudgetActionCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPersBudgetActionCorresp.LOG.isDebugEnabled()) {
    	_EOPersBudgetActionCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudgetAction toManguePersBudgetAction() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudgetAction)storedValueForKey("toManguePersBudgetAction");
  }

  public void setToManguePersBudgetActionRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudgetAction value) {
    if (_EOPersBudgetActionCorresp.LOG.isDebugEnabled()) {
      _EOPersBudgetActionCorresp.LOG.debug("updating toManguePersBudgetAction from " + toManguePersBudgetAction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudgetAction oldValue = toManguePersBudgetAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toManguePersBudgetAction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toManguePersBudgetAction");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction toPersBudgetAction() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction)storedValueForKey("toPersBudgetAction");
  }

  public void setToPersBudgetActionRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction value) {
    if (_EOPersBudgetActionCorresp.LOG.isDebugEnabled()) {
      _EOPersBudgetActionCorresp.LOG.debug("updating toPersBudgetAction from " + toPersBudgetAction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction oldValue = toPersBudgetAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersBudgetAction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersBudgetAction");
    }
  }
  

  public static EOPersBudgetActionCorresp createPersBudgetActionCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudgetAction toManguePersBudgetAction, org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction toPersBudgetAction) {
    EOPersBudgetActionCorresp eo = (EOPersBudgetActionCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOPersBudgetActionCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToManguePersBudgetActionRelationship(toManguePersBudgetAction);
    eo.setToPersBudgetActionRelationship(toPersBudgetAction);
    return eo;
  }

  public static NSArray<EOPersBudgetActionCorresp> fetchAllPersBudgetActionCorresps(EOEditingContext editingContext) {
    return _EOPersBudgetActionCorresp.fetchAllPersBudgetActionCorresps(editingContext, null);
  }

  public static NSArray<EOPersBudgetActionCorresp> fetchAllPersBudgetActionCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPersBudgetActionCorresp.fetchPersBudgetActionCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPersBudgetActionCorresp> fetchPersBudgetActionCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPersBudgetActionCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPersBudgetActionCorresp> eoObjects = (NSArray<EOPersBudgetActionCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPersBudgetActionCorresp fetchPersBudgetActionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudgetActionCorresp.fetchPersBudgetActionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudgetActionCorresp fetchPersBudgetActionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPersBudgetActionCorresp> eoObjects = _EOPersBudgetActionCorresp.fetchPersBudgetActionCorresps(editingContext, qualifier, null);
    EOPersBudgetActionCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPersBudgetActionCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PersBudgetActionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudgetActionCorresp fetchRequiredPersBudgetActionCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudgetActionCorresp.fetchRequiredPersBudgetActionCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudgetActionCorresp fetchRequiredPersBudgetActionCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPersBudgetActionCorresp eoObject = _EOPersBudgetActionCorresp.fetchPersBudgetActionCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PersBudgetActionCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudgetActionCorresp localInstanceIn(EOEditingContext editingContext, EOPersBudgetActionCorresp eo) {
    EOPersBudgetActionCorresp localInstance = (eo == null) ? null : (EOPersBudgetActionCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
