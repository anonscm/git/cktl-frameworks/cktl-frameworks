// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeMaladieDetailCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeMaladieDetailCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CongeMaladieDetailCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_MALADIE_DETAIL_KEY = "congeMaladieDetail";
	public static final String MANGUE_CONGE_MALADIE_DETAIL_KEY = "mangueCongeMaladieDetail";

  private static Logger LOG = Logger.getLogger(_EOCongeMaladieDetailCorresp.class);

  public EOCongeMaladieDetailCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeMaladieDetailCorresp localInstance = (EOCongeMaladieDetailCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeMaladieDetailCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetailCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeMaladieDetailCorresp.LOG.isDebugEnabled()) {
    	_EOCongeMaladieDetailCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail congeMaladieDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail)storedValueForKey("congeMaladieDetail");
  }

  public void setCongeMaladieDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail value) {
    if (_EOCongeMaladieDetailCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaladieDetailCorresp.LOG.debug("updating congeMaladieDetail from " + congeMaladieDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail oldValue = congeMaladieDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladieDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladieDetail");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeMaladieDetail mangueCongeMaladieDetail() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeMaladieDetail)storedValueForKey("mangueCongeMaladieDetail");
  }

  public void setMangueCongeMaladieDetailRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeMaladieDetail value) {
    if (_EOCongeMaladieDetailCorresp.LOG.isDebugEnabled()) {
      _EOCongeMaladieDetailCorresp.LOG.debug("updating mangueCongeMaladieDetail from " + mangueCongeMaladieDetail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeMaladieDetail oldValue = mangueCongeMaladieDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeMaladieDetail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeMaladieDetail");
    }
  }
  

  public static EOCongeMaladieDetailCorresp createCongeMaladieDetailCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail congeMaladieDetail, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeMaladieDetail mangueCongeMaladieDetail) {
    EOCongeMaladieDetailCorresp eo = (EOCongeMaladieDetailCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeMaladieDetailCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeMaladieDetailRelationship(congeMaladieDetail);
    eo.setMangueCongeMaladieDetailRelationship(mangueCongeMaladieDetail);
    return eo;
  }

  public static NSArray<EOCongeMaladieDetailCorresp> fetchAllCongeMaladieDetailCorresps(EOEditingContext editingContext) {
    return _EOCongeMaladieDetailCorresp.fetchAllCongeMaladieDetailCorresps(editingContext, null);
  }

  public static NSArray<EOCongeMaladieDetailCorresp> fetchAllCongeMaladieDetailCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeMaladieDetailCorresp.fetchCongeMaladieDetailCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeMaladieDetailCorresp> fetchCongeMaladieDetailCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeMaladieDetailCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeMaladieDetailCorresp> eoObjects = (NSArray<EOCongeMaladieDetailCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeMaladieDetailCorresp fetchCongeMaladieDetailCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieDetailCorresp.fetchCongeMaladieDetailCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieDetailCorresp fetchCongeMaladieDetailCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeMaladieDetailCorresp> eoObjects = _EOCongeMaladieDetailCorresp.fetchCongeMaladieDetailCorresps(editingContext, qualifier, null);
    EOCongeMaladieDetailCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeMaladieDetailCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeMaladieDetailCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieDetailCorresp fetchRequiredCongeMaladieDetailCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeMaladieDetailCorresp.fetchRequiredCongeMaladieDetailCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeMaladieDetailCorresp fetchRequiredCongeMaladieDetailCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeMaladieDetailCorresp eoObject = _EOCongeMaladieDetailCorresp.fetchCongeMaladieDetailCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeMaladieDetailCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeMaladieDetailCorresp localInstanceIn(EOEditingContext editingContext, EOCongeMaladieDetailCorresp eo) {
    EOCongeMaladieDetailCorresp localInstance = (eo == null) ? null : (EOCongeMaladieDetailCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
