// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPersBudgetCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPersBudgetCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "PersBudgetCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_MANGUE_PERS_BUDGET_KEY = "toManguePersBudget";
	public static final String TO_PERS_BUDGET_KEY = "toPersBudget";

  private static Logger LOG = Logger.getLogger(_EOPersBudgetCorresp.class);

  public EOPersBudgetCorresp localInstanceIn(EOEditingContext editingContext) {
    EOPersBudgetCorresp localInstance = (EOPersBudgetCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPersBudgetCorresp.LOG.isDebugEnabled()) {
    	_EOPersBudgetCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPersBudgetCorresp.LOG.isDebugEnabled()) {
    	_EOPersBudgetCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget toManguePersBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget)storedValueForKey("toManguePersBudget");
  }

  public void setToManguePersBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget value) {
    if (_EOPersBudgetCorresp.LOG.isDebugEnabled()) {
      _EOPersBudgetCorresp.LOG.debug("updating toManguePersBudget from " + toManguePersBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget oldValue = toManguePersBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toManguePersBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toManguePersBudget");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget toPersBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget)storedValueForKey("toPersBudget");
  }

  public void setToPersBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget value) {
    if (_EOPersBudgetCorresp.LOG.isDebugEnabled()) {
      _EOPersBudgetCorresp.LOG.debug("updating toPersBudget from " + toPersBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget oldValue = toPersBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersBudget");
    }
  }
  

  public static EOPersBudgetCorresp createPersBudgetCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget toManguePersBudget, org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget toPersBudget) {
    EOPersBudgetCorresp eo = (EOPersBudgetCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOPersBudgetCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToManguePersBudgetRelationship(toManguePersBudget);
    eo.setToPersBudgetRelationship(toPersBudget);
    return eo;
  }

  public static NSArray<EOPersBudgetCorresp> fetchAllPersBudgetCorresps(EOEditingContext editingContext) {
    return _EOPersBudgetCorresp.fetchAllPersBudgetCorresps(editingContext, null);
  }

  public static NSArray<EOPersBudgetCorresp> fetchAllPersBudgetCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPersBudgetCorresp.fetchPersBudgetCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPersBudgetCorresp> fetchPersBudgetCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPersBudgetCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPersBudgetCorresp> eoObjects = (NSArray<EOPersBudgetCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPersBudgetCorresp fetchPersBudgetCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudgetCorresp.fetchPersBudgetCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudgetCorresp fetchPersBudgetCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPersBudgetCorresp> eoObjects = _EOPersBudgetCorresp.fetchPersBudgetCorresps(editingContext, qualifier, null);
    EOPersBudgetCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPersBudgetCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one PersBudgetCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudgetCorresp fetchRequiredPersBudgetCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPersBudgetCorresp.fetchRequiredPersBudgetCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPersBudgetCorresp fetchRequiredPersBudgetCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPersBudgetCorresp eoObject = _EOPersBudgetCorresp.fetchPersBudgetCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no PersBudgetCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPersBudgetCorresp localInstanceIn(EOEditingContext editingContext, EOPersBudgetCorresp eo) {
    EOPersBudgetCorresp localInstance = (eo == null) ? null : (EOPersBudgetCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
