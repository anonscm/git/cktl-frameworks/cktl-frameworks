// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOOrgaBudgetCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOOrgaBudgetCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "OrgaBudgetCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String JEFY_ORGAN_KEY = "jefyOrgan";
	public static final String ORGA_BUDGET_KEY = "orgaBudget";

  private static Logger LOG = Logger.getLogger(_EOOrgaBudgetCorresp.class);

  public EOOrgaBudgetCorresp localInstanceIn(EOEditingContext editingContext) {
    EOOrgaBudgetCorresp localInstance = (EOOrgaBudgetCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOOrgaBudgetCorresp.LOG.isDebugEnabled()) {
    	_EOOrgaBudgetCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOOrgaBudgetCorresp.LOG.isDebugEnabled()) {
    	_EOOrgaBudgetCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan jefyOrgan() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan)storedValueForKey("jefyOrgan");
  }

  public void setJefyOrganRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan value) {
    if (_EOOrgaBudgetCorresp.LOG.isDebugEnabled()) {
      _EOOrgaBudgetCorresp.LOG.debug("updating jefyOrgan from " + jefyOrgan() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan oldValue = jefyOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "jefyOrgan");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "jefyOrgan");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget orgaBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget)storedValueForKey("orgaBudget");
  }

  public void setOrgaBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget value) {
    if (_EOOrgaBudgetCorresp.LOG.isDebugEnabled()) {
      _EOOrgaBudgetCorresp.LOG.debug("updating orgaBudget from " + orgaBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget oldValue = orgaBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "orgaBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "orgaBudget");
    }
  }
  

  public static EOOrgaBudgetCorresp createOrgaBudgetCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan jefyOrgan, org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget orgaBudget) {
    EOOrgaBudgetCorresp eo = (EOOrgaBudgetCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOOrgaBudgetCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setJefyOrganRelationship(jefyOrgan);
    eo.setOrgaBudgetRelationship(orgaBudget);
    return eo;
  }

  public static NSArray<EOOrgaBudgetCorresp> fetchAllOrgaBudgetCorresps(EOEditingContext editingContext) {
    return _EOOrgaBudgetCorresp.fetchAllOrgaBudgetCorresps(editingContext, null);
  }

  public static NSArray<EOOrgaBudgetCorresp> fetchAllOrgaBudgetCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOOrgaBudgetCorresp.fetchOrgaBudgetCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOOrgaBudgetCorresp> fetchOrgaBudgetCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOOrgaBudgetCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOOrgaBudgetCorresp> eoObjects = (NSArray<EOOrgaBudgetCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOOrgaBudgetCorresp fetchOrgaBudgetCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrgaBudgetCorresp.fetchOrgaBudgetCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrgaBudgetCorresp fetchOrgaBudgetCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOOrgaBudgetCorresp> eoObjects = _EOOrgaBudgetCorresp.fetchOrgaBudgetCorresps(editingContext, qualifier, null);
    EOOrgaBudgetCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOOrgaBudgetCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OrgaBudgetCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrgaBudgetCorresp fetchRequiredOrgaBudgetCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrgaBudgetCorresp.fetchRequiredOrgaBudgetCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrgaBudgetCorresp fetchRequiredOrgaBudgetCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOOrgaBudgetCorresp eoObject = _EOOrgaBudgetCorresp.fetchOrgaBudgetCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OrgaBudgetCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrgaBudgetCorresp localInstanceIn(EOEditingContext editingContext, EOOrgaBudgetCorresp eo) {
    EOOrgaBudgetCorresp localInstance = (eo == null) ? null : (EOOrgaBudgetCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
