/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCld;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;

public class EOCldCorresp extends _EOCldCorresp {

	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(cld(), CLD_KEY);
		removeObjectFromBothSidesOfRelationshipWithKey(cldMangue(),CLD_MANGUE_KEY);
	}
	public String nomRelationBaseImport() {
		return CLD_KEY;
	}

	public String nomRelationBaseDestinataire() {
		return CLD_MANGUE_KEY;
	}
	// Méthodes statiques
	/** retourne le cdl d'import associe a l'id passee en param&egrave;tre. On commence
	 * par chercher dans l'import courant pour &ecirc;tre s&ucirc;r de retourner le dernier cld, sinon
	 * on recherche dans les correspondances */
	public static EOCld cldPourSourceEtCgId(EOEditingContext editingContext,Number idSource,Number cldSource) {
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(cldSource);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOCld.ID_SOURCE_KEY+" = %@ AND "+EOCld.CLD_SOURCE_KEY+" = %@ AND "+EOCld.TEM_IMPORT_KEY+" = %@ AND "+EOCld.STATUT_KEY+" <> 'A'",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (EOCld.ENTITY_NAME, myQualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EOCld)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e1) {
			// Rechercher dans les correspondances si le cld n'a pas déjà été importé
			args = new NSMutableArray(idSource);
			args.addObject(cldSource);
			myQualifier = EOQualifier.qualifierWithQualifierFormat(CLD_KEY+"."+EOCld.ID_SOURCE_KEY+" = %@ AND "+CLD_KEY+"."+EOCld.CLD_SOURCE_KEY+" = %@",args);
			myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			try {
				return ((EOCldCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0)).cld();
			} catch (Exception e) {
				return null;

			}
		}
	}
	/** retourne le cld de Mangue assoc&iee au cld d'import
	 * @param editingContext editing context 
	 * @param idSource identifiant de l'individu
	 * @param cldSource identifiant du cld
	 **/
	public static EOMangueCld cldMangue(EOEditingContext editingContext,Number cldSource, Number idSource) {
		// Rechercher dans les correspondances si le cld n'a pas déjà été importé
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(cldSource);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(CLD_KEY+"."+EOCld.ID_SOURCE_KEY+" = %@ AND "+CLD_KEY+"."+EOCld.CLD_SOURCE_KEY+" = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);		
		try {
			EOCldCorresp corresp = (EOCldCorresp)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
			return corresp.cldMangue();
		} catch (Exception e) {
			return null;
		}
	}
}
