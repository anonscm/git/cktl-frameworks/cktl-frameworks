package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

public class EOReculAgeCorresp extends _EOReculAgeCorresp {
	private static Logger log = Logger.getLogger(EOReculAgeCorresp.class);


	public EOReculAgeCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_RECUL_AGE_KEY;
	}

	public String nomRelationBaseImport() {
		return TO_RECUL_AGE_KEY;
	}

	public void supprimerRelations() {
		setToReculAgeRelationship(null);
		setToMangueReculAgeRelationship(null);
	}


}
