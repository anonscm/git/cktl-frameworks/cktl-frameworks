// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAl5Corresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAl5Corresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongeAl5Corresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_AL5_KEY = "congeAl5";
	public static final String MANGUE_CONGE_AL5_KEY = "mangueCongeAl5";

  private static Logger LOG = Logger.getLogger(_EOCongeAl5Corresp.class);

  public EOCongeAl5Corresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeAl5Corresp localInstance = (EOCongeAl5Corresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAl5Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl5Corresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAl5Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl5Corresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5 congeAl5() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5)storedValueForKey("congeAl5");
  }

  public void setCongeAl5Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5 value) {
    if (_EOCongeAl5Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl5Corresp.LOG.debug("updating congeAl5 from " + congeAl5() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5 oldValue = congeAl5();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl5");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl5");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl5 mangueCongeAl5() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl5)storedValueForKey("mangueCongeAl5");
  }

  public void setMangueCongeAl5Relationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl5 value) {
    if (_EOCongeAl5Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl5Corresp.LOG.debug("updating mangueCongeAl5 from " + mangueCongeAl5() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl5 oldValue = mangueCongeAl5();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeAl5");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeAl5");
    }
  }
  

  public static EOCongeAl5Corresp createCongeAl5Corresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl5 congeAl5, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl5 mangueCongeAl5) {
    EOCongeAl5Corresp eo = (EOCongeAl5Corresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAl5Corresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeAl5Relationship(congeAl5);
    eo.setMangueCongeAl5Relationship(mangueCongeAl5);
    return eo;
  }

  public static NSArray<EOCongeAl5Corresp> fetchAllCongeAl5Corresps(EOEditingContext editingContext) {
    return _EOCongeAl5Corresp.fetchAllCongeAl5Corresps(editingContext, null);
  }

  public static NSArray<EOCongeAl5Corresp> fetchAllCongeAl5Corresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAl5Corresp.fetchCongeAl5Corresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAl5Corresp> fetchCongeAl5Corresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAl5Corresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAl5Corresp> eoObjects = (NSArray<EOCongeAl5Corresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAl5Corresp fetchCongeAl5Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl5Corresp.fetchCongeAl5Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl5Corresp fetchCongeAl5Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAl5Corresp> eoObjects = _EOCongeAl5Corresp.fetchCongeAl5Corresps(editingContext, qualifier, null);
    EOCongeAl5Corresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAl5Corresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAl5Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl5Corresp fetchRequiredCongeAl5Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl5Corresp.fetchRequiredCongeAl5Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl5Corresp fetchRequiredCongeAl5Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAl5Corresp eoObject = _EOCongeAl5Corresp.fetchCongeAl5Corresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAl5Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl5Corresp localInstanceIn(EOEditingContext editingContext, EOCongeAl5Corresp eo) {
    EOCongeAl5Corresp localInstance = (eo == null) ? null : (EOCongeAl5Corresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
