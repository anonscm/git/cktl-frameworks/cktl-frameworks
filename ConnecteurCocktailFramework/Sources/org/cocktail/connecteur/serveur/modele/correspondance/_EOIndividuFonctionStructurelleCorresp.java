// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOIndividuFonctionStructurelleCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOIndividuFonctionStructurelleCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "IndividuFonctionStructurelleCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String GRHUM_INDIVIDU_FONCTION_STRUCTURELLE_KEY = "grhumIndividuFonctionStructurelle";
	public static final String INDIVIDU_FONCTION_STRUCTURELLE_KEY = "individuFonctionStructurelle";

  private static Logger LOG = Logger.getLogger(_EOIndividuFonctionStructurelleCorresp.class);

  public EOIndividuFonctionStructurelleCorresp localInstanceIn(EOEditingContext editingContext) {
    EOIndividuFonctionStructurelleCorresp localInstance = (EOIndividuFonctionStructurelleCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuFonctionStructurelleCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelleCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuFonctionStructurelleCorresp.LOG.isDebugEnabled()) {
    	_EOIndividuFonctionStructurelleCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionStructurelle grhumIndividuFonctionStructurelle() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionStructurelle)storedValueForKey("grhumIndividuFonctionStructurelle");
  }

  public void setGrhumIndividuFonctionStructurelleRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionStructurelle value) {
    if (_EOIndividuFonctionStructurelleCorresp.LOG.isDebugEnabled()) {
      _EOIndividuFonctionStructurelleCorresp.LOG.debug("updating grhumIndividuFonctionStructurelle from " + grhumIndividuFonctionStructurelle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionStructurelle oldValue = grhumIndividuFonctionStructurelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "grhumIndividuFonctionStructurelle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "grhumIndividuFonctionStructurelle");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle individuFonctionStructurelle() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle)storedValueForKey("individuFonctionStructurelle");
  }

  public void setIndividuFonctionStructurelleRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle value) {
    if (_EOIndividuFonctionStructurelleCorresp.LOG.isDebugEnabled()) {
      _EOIndividuFonctionStructurelleCorresp.LOG.debug("updating individuFonctionStructurelle from " + individuFonctionStructurelle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle oldValue = individuFonctionStructurelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individuFonctionStructurelle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individuFonctionStructurelle");
    }
  }
  

  public static EOIndividuFonctionStructurelleCorresp createIndividuFonctionStructurelleCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividuFonctionStructurelle grhumIndividuFonctionStructurelle, org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle individuFonctionStructurelle) {
    EOIndividuFonctionStructurelleCorresp eo = (EOIndividuFonctionStructurelleCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuFonctionStructurelleCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setGrhumIndividuFonctionStructurelleRelationship(grhumIndividuFonctionStructurelle);
    eo.setIndividuFonctionStructurelleRelationship(individuFonctionStructurelle);
    return eo;
  }

  public static NSArray<EOIndividuFonctionStructurelleCorresp> fetchAllIndividuFonctionStructurelleCorresps(EOEditingContext editingContext) {
    return _EOIndividuFonctionStructurelleCorresp.fetchAllIndividuFonctionStructurelleCorresps(editingContext, null);
  }

  public static NSArray<EOIndividuFonctionStructurelleCorresp> fetchAllIndividuFonctionStructurelleCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuFonctionStructurelleCorresp.fetchIndividuFonctionStructurelleCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOIndividuFonctionStructurelleCorresp> fetchIndividuFonctionStructurelleCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuFonctionStructurelleCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuFonctionStructurelleCorresp> eoObjects = (NSArray<EOIndividuFonctionStructurelleCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuFonctionStructurelleCorresp fetchIndividuFonctionStructurelleCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionStructurelleCorresp.fetchIndividuFonctionStructurelleCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionStructurelleCorresp fetchIndividuFonctionStructurelleCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuFonctionStructurelleCorresp> eoObjects = _EOIndividuFonctionStructurelleCorresp.fetchIndividuFonctionStructurelleCorresps(editingContext, qualifier, null);
    EOIndividuFonctionStructurelleCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuFonctionStructurelleCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one IndividuFonctionStructurelleCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionStructurelleCorresp fetchRequiredIndividuFonctionStructurelleCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFonctionStructurelleCorresp.fetchRequiredIndividuFonctionStructurelleCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFonctionStructurelleCorresp fetchRequiredIndividuFonctionStructurelleCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuFonctionStructurelleCorresp eoObject = _EOIndividuFonctionStructurelleCorresp.fetchIndividuFonctionStructurelleCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no IndividuFonctionStructurelleCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFonctionStructurelleCorresp localInstanceIn(EOEditingContext editingContext, EOIndividuFonctionStructurelleCorresp eo) {
    EOIndividuFonctionStructurelleCorresp localInstance = (eo == null) ? null : (EOIndividuFonctionStructurelleCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
