// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOBonificationsCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOBonificationsCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "BonificationsCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_BONIFICATIONS_KEY = "toBonifications";
	public static final String TO_MANGUE_BONIFICATIONS_KEY = "toMangueBonifications";

  private static Logger LOG = Logger.getLogger(_EOBonificationsCorresp.class);

  public EOBonificationsCorresp localInstanceIn(EOEditingContext editingContext) {
    EOBonificationsCorresp localInstance = (EOBonificationsCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBonificationsCorresp.LOG.isDebugEnabled()) {
    	_EOBonificationsCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOBonificationsCorresp.LOG.isDebugEnabled()) {
    	_EOBonificationsCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications toBonifications() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications)storedValueForKey("toBonifications");
  }

  public void setToBonificationsRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications value) {
    if (_EOBonificationsCorresp.LOG.isDebugEnabled()) {
      _EOBonificationsCorresp.LOG.debug("updating toBonifications from " + toBonifications() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications oldValue = toBonifications();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBonifications");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toBonifications");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications toMangueBonifications() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications)storedValueForKey("toMangueBonifications");
  }

  public void setToMangueBonificationsRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications value) {
    if (_EOBonificationsCorresp.LOG.isDebugEnabled()) {
      _EOBonificationsCorresp.LOG.debug("updating toMangueBonifications from " + toMangueBonifications() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications oldValue = toMangueBonifications();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueBonifications");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueBonifications");
    }
  }
  

  public static EOBonificationsCorresp createBonificationsCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOBonifications toBonifications, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueBonifications toMangueBonifications) {
    EOBonificationsCorresp eo = (EOBonificationsCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOBonificationsCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToBonificationsRelationship(toBonifications);
    eo.setToMangueBonificationsRelationship(toMangueBonifications);
    return eo;
  }

  public static NSArray<EOBonificationsCorresp> fetchAllBonificationsCorresps(EOEditingContext editingContext) {
    return _EOBonificationsCorresp.fetchAllBonificationsCorresps(editingContext, null);
  }

  public static NSArray<EOBonificationsCorresp> fetchAllBonificationsCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBonificationsCorresp.fetchBonificationsCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOBonificationsCorresp> fetchBonificationsCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOBonificationsCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBonificationsCorresp> eoObjects = (NSArray<EOBonificationsCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOBonificationsCorresp fetchBonificationsCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBonificationsCorresp.fetchBonificationsCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBonificationsCorresp fetchBonificationsCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBonificationsCorresp> eoObjects = _EOBonificationsCorresp.fetchBonificationsCorresps(editingContext, qualifier, null);
    EOBonificationsCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOBonificationsCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one BonificationsCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBonificationsCorresp fetchRequiredBonificationsCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBonificationsCorresp.fetchRequiredBonificationsCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBonificationsCorresp fetchRequiredBonificationsCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBonificationsCorresp eoObject = _EOBonificationsCorresp.fetchBonificationsCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no BonificationsCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBonificationsCorresp localInstanceIn(EOEditingContext editingContext, EOBonificationsCorresp eo) {
    EOBonificationsCorresp localInstance = (eo == null) ? null : (EOBonificationsCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
