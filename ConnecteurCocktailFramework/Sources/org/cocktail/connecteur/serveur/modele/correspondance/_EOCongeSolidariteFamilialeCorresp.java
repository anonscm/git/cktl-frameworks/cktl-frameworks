// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeSolidariteFamilialeCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeSolidariteFamilialeCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CongeSolidariteFamilialeCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_SOLIDARITE_FAMILIALE_KEY = "congeSolidariteFamiliale";
	public static final String MANGUE_CONGE_SOLIDARITE_FAMILIALE_KEY = "mangueCongeSolidariteFamiliale";

  private static Logger LOG = Logger.getLogger(_EOCongeSolidariteFamilialeCorresp.class);

  public EOCongeSolidariteFamilialeCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeSolidariteFamilialeCorresp localInstance = (EOCongeSolidariteFamilialeCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeSolidariteFamilialeCorresp.LOG.isDebugEnabled()) {
    	_EOCongeSolidariteFamilialeCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeSolidariteFamilialeCorresp.LOG.isDebugEnabled()) {
    	_EOCongeSolidariteFamilialeCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale congeSolidariteFamiliale() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale)storedValueForKey("congeSolidariteFamiliale");
  }

  public void setCongeSolidariteFamilialeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale value) {
    if (_EOCongeSolidariteFamilialeCorresp.LOG.isDebugEnabled()) {
      _EOCongeSolidariteFamilialeCorresp.LOG.debug("updating congeSolidariteFamiliale from " + congeSolidariteFamiliale() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale oldValue = congeSolidariteFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeSolidariteFamiliale");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeSolidariteFamiliale");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale mangueCongeSolidariteFamiliale() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale)storedValueForKey("mangueCongeSolidariteFamiliale");
  }

  public void setMangueCongeSolidariteFamilialeRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale value) {
    if (_EOCongeSolidariteFamilialeCorresp.LOG.isDebugEnabled()) {
      _EOCongeSolidariteFamilialeCorresp.LOG.debug("updating mangueCongeSolidariteFamiliale from " + mangueCongeSolidariteFamiliale() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale oldValue = mangueCongeSolidariteFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeSolidariteFamiliale");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeSolidariteFamiliale");
    }
  }
  

  public static EOCongeSolidariteFamilialeCorresp createCongeSolidariteFamilialeCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeSolidariteFamiliale congeSolidariteFamiliale, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale mangueCongeSolidariteFamiliale) {
    EOCongeSolidariteFamilialeCorresp eo = (EOCongeSolidariteFamilialeCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeSolidariteFamilialeCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeSolidariteFamilialeRelationship(congeSolidariteFamiliale);
    eo.setMangueCongeSolidariteFamilialeRelationship(mangueCongeSolidariteFamiliale);
    return eo;
  }

  public static NSArray<EOCongeSolidariteFamilialeCorresp> fetchAllCongeSolidariteFamilialeCorresps(EOEditingContext editingContext) {
    return _EOCongeSolidariteFamilialeCorresp.fetchAllCongeSolidariteFamilialeCorresps(editingContext, null);
  }

  public static NSArray<EOCongeSolidariteFamilialeCorresp> fetchAllCongeSolidariteFamilialeCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeSolidariteFamilialeCorresp.fetchCongeSolidariteFamilialeCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeSolidariteFamilialeCorresp> fetchCongeSolidariteFamilialeCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeSolidariteFamilialeCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeSolidariteFamilialeCorresp> eoObjects = (NSArray<EOCongeSolidariteFamilialeCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeSolidariteFamilialeCorresp fetchCongeSolidariteFamilialeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeSolidariteFamilialeCorresp.fetchCongeSolidariteFamilialeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeSolidariteFamilialeCorresp fetchCongeSolidariteFamilialeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeSolidariteFamilialeCorresp> eoObjects = _EOCongeSolidariteFamilialeCorresp.fetchCongeSolidariteFamilialeCorresps(editingContext, qualifier, null);
    EOCongeSolidariteFamilialeCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeSolidariteFamilialeCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeSolidariteFamilialeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeSolidariteFamilialeCorresp fetchRequiredCongeSolidariteFamilialeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeSolidariteFamilialeCorresp.fetchRequiredCongeSolidariteFamilialeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeSolidariteFamilialeCorresp fetchRequiredCongeSolidariteFamilialeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeSolidariteFamilialeCorresp eoObject = _EOCongeSolidariteFamilialeCorresp.fetchCongeSolidariteFamilialeCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeSolidariteFamilialeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeSolidariteFamilialeCorresp localInstanceIn(EOEditingContext editingContext, EOCongeSolidariteFamilialeCorresp eo) {
    EOCongeSolidariteFamilialeCorresp localInstance = (eo == null) ? null : (EOCongeSolidariteFamilialeCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
