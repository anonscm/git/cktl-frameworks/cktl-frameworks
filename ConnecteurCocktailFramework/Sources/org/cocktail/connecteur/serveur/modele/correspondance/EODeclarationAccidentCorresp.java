package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;

public class EODeclarationAccidentCorresp extends _EODeclarationAccidentCorresp {

	public String nomRelationBaseDestinataire() {
		return TO_MANGUE_DECLARATION_ACCIDENT_KEY;
	}

	public String nomRelationBaseImport() {
		return TO_DECLARATION_ACCIDENT_KEY;
	}

	public void supprimerRelations() {
		setToDeclarationAccidentRelationship(null);
		setToMangueDeclarationAccidentRelationship(null);
	}

	/**
	 * retourne la déclaration d'import associe a l'individu et a l'identifiant
	 * de la declaration On commence par chercher dans l'import courant pour
	 * etre sur de retourner le dernier enfant, sinon on recherche dans les
	 * correspondances
	 */
	public static EODeclarationAccident declarationAccidentPourIndividuEtId(EOEditingContext editingContext, Number idSource, Number daccSource) {
		NSMutableArray args = new NSMutableArray(idSource);
		args.addObject(daccSource);
		args.addObject(ObjetImport.A_TRANSFERER);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EODeclarationAccident.ID_SOURCE_KEY + " = %@ AND "
				+ EODeclarationAccident.DACC_SOURCE_KEY + " = %@ AND " + EODeclarationAccident.TEM_IMPORT_KEY + " = %@ AND "
				+ EODeclarationAccident.STATUT_KEY + " <> 'A' AND " + EODeclarationAccident.STATUT_KEY + " <> 'E'", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(EODeclarationAccident.ENTITY_NAME, myQualifier, null);
		myFetch.setRefreshesRefetchedObjects(true);
		try {
			return (EODeclarationAccident) editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e1) {
			// Rechercher dans les correspondances
			myQualifier = EOQualifier.qualifierWithQualifierFormat(EODeclarationAccidentCorresp.TO_DECLARATION_ACCIDENT_KEY + "."
					+ EODeclarationAccident.ID_SOURCE_KEY + " = %@ AND " + EODeclarationAccidentCorresp.TO_DECLARATION_ACCIDENT_KEY + "."
					+ EODeclarationAccident.DACC_SOURCE_KEY + " = %@", args);
			myFetch = new EOFetchSpecification(EODeclarationAccidentCorresp.ENTITY_NAME, myQualifier, null);
			myFetch.setRefreshesRefetchedObjects(true);
			try {
				return ((EODeclarationAccidentCorresp) editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0)).toDeclarationAccident();
			} catch (Exception e) {
				return null;

			}
		}
	}

}
