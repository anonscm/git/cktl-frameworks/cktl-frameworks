package org.cocktail.connecteur.serveur.modele.correspondance;

import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires;
import org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires;

import com.webobjects.eocontrol.EOEditingContext;

public class EOVacatairesCorresp extends _EOVacatairesCorresp {
	private static final long serialVersionUID = -7664380913323729434L;

	public String nomRelationBaseDestinataire() {
		return MANGUE_VACATAIRES_KEY;
	}

	public String nomRelationBaseImport() {
		return VACATAIRES_KEY;
	}

	public void supprimerRelations() {
		setVacatairesRelationship(null);
		setMangueVacatairesRelationship(null);
	}

	public static EOMangueVacataires mangueVacataires(EOEditingContext editingContext, Integer vacSource) {
		if (vacSource == null)
			return null;

		EOVacatairesCorresp corresp = fetchVacatairesCorresp(editingContext, VACATAIRES_KEY + "." + EOVacataires.VAC_SOURCE_KEY, vacSource);
		if (corresp == null)
			return null;
		return corresp.mangueVacataires();
	}
}
