// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCarriereCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCarriereCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "CarriereCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CARRIERE_MANGUE_KEY = "carriereMangue";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOCarriereCorresp.class);

  public EOCarriereCorresp localInstanceIn(EOEditingContext editingContext) {
    EOCarriereCorresp localInstance = (EOCarriereCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCarriereCorresp.LOG.isDebugEnabled()) {
    	_EOCarriereCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCarriereCorresp.LOG.isDebugEnabled()) {
    	_EOCarriereCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere carriereMangue() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("carriereMangue");
  }

  public void setCarriereMangueRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOCarriereCorresp.LOG.isDebugEnabled()) {
      _EOCarriereCorresp.LOG.debug("updating carriereMangue from " + carriereMangue() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = carriereMangue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "carriereMangue");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "carriereMangue");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere value) {
    if (_EOCarriereCorresp.LOG.isDebugEnabled()) {
      _EOCarriereCorresp.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOCarriereCorresp createCarriereCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere carriereMangue, org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere toCarriere) {
    EOCarriereCorresp eo = (EOCarriereCorresp) EOUtilities.createAndInsertInstance(editingContext, _EOCarriereCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCarriereMangueRelationship(carriereMangue);
    eo.setToCarriereRelationship(toCarriere);
    return eo;
  }

  public static NSArray<EOCarriereCorresp> fetchAllCarriereCorresps(EOEditingContext editingContext) {
    return _EOCarriereCorresp.fetchAllCarriereCorresps(editingContext, null);
  }

  public static NSArray<EOCarriereCorresp> fetchAllCarriereCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCarriereCorresp.fetchCarriereCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCarriereCorresp> fetchCarriereCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCarriereCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCarriereCorresp> eoObjects = (NSArray<EOCarriereCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCarriereCorresp fetchCarriereCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereCorresp.fetchCarriereCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriereCorresp fetchCarriereCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCarriereCorresp> eoObjects = _EOCarriereCorresp.fetchCarriereCorresps(editingContext, qualifier, null);
    EOCarriereCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCarriereCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CarriereCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriereCorresp fetchRequiredCarriereCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereCorresp.fetchRequiredCarriereCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriereCorresp fetchRequiredCarriereCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCarriereCorresp eoObject = _EOCarriereCorresp.fetchCarriereCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CarriereCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriereCorresp localInstanceIn(EOEditingContext editingContext, EOCarriereCorresp eo) {
    EOCarriereCorresp localInstance = (eo == null) ? null : (EOCarriereCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
