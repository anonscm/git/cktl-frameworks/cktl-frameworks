package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.foundation.NSDictionary;

public class EOCongeAl3Corresp extends _EOCongeAl3Corresp {
	@Override
	public void supprimerRelations() {
		setCongeAl3Relationship(null);
		setMangueCongeAl3Relationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return CONGE_AL3_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_AL3_KEY;
	}
}
