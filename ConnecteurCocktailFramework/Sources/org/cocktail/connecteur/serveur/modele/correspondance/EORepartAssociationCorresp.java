package org.cocktail.connecteur.serveur.modele.correspondance;

public class EORepartAssociationCorresp extends _EORepartAssociationCorresp {
	@Override
	public void supprimerRelations() {
		setRepartAssociationRelationship(null);
		setGrhumRepartAssociationRelationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return REPART_ASSOCIATION_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return GRHUM_REPART_ASSOCIATION_KEY;
	}
}
