// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOCongeAl6Corresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOCongeAl6Corresp extends org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp {
	public static final String ENTITY_NAME = "CongeAl6Corresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONGE_AL6_KEY = "congeAl6";
	public static final String MANGUE_CONGE_AL6_KEY = "mangueCongeAl6";

  private static Logger LOG = Logger.getLogger(_EOCongeAl6Corresp.class);

  public EOCongeAl6Corresp localInstanceIn(EOEditingContext editingContext) {
    EOCongeAl6Corresp localInstance = (EOCongeAl6Corresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeAl6Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl6Corresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeAl6Corresp.LOG.isDebugEnabled()) {
    	_EOCongeAl6Corresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6 congeAl6() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6)storedValueForKey("congeAl6");
  }

  public void setCongeAl6Relationship(org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6 value) {
    if (_EOCongeAl6Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl6Corresp.LOG.debug("updating congeAl6 from " + congeAl6() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6 oldValue = congeAl6();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeAl6");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeAl6");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl6 mangueCongeAl6() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl6)storedValueForKey("mangueCongeAl6");
  }

  public void setMangueCongeAl6Relationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl6 value) {
    if (_EOCongeAl6Corresp.LOG.isDebugEnabled()) {
      _EOCongeAl6Corresp.LOG.debug("updating mangueCongeAl6 from " + mangueCongeAl6() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl6 oldValue = mangueCongeAl6();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCongeAl6");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCongeAl6");
    }
  }
  

  public static EOCongeAl6Corresp createCongeAl6Corresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAl6 congeAl6, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl6 mangueCongeAl6) {
    EOCongeAl6Corresp eo = (EOCongeAl6Corresp) EOUtilities.createAndInsertInstance(editingContext, _EOCongeAl6Corresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setCongeAl6Relationship(congeAl6);
    eo.setMangueCongeAl6Relationship(mangueCongeAl6);
    return eo;
  }

  public static NSArray<EOCongeAl6Corresp> fetchAllCongeAl6Corresps(EOEditingContext editingContext) {
    return _EOCongeAl6Corresp.fetchAllCongeAl6Corresps(editingContext, null);
  }

  public static NSArray<EOCongeAl6Corresp> fetchAllCongeAl6Corresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeAl6Corresp.fetchCongeAl6Corresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCongeAl6Corresp> fetchCongeAl6Corresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeAl6Corresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeAl6Corresp> eoObjects = (NSArray<EOCongeAl6Corresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeAl6Corresp fetchCongeAl6Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl6Corresp.fetchCongeAl6Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl6Corresp fetchCongeAl6Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeAl6Corresp> eoObjects = _EOCongeAl6Corresp.fetchCongeAl6Corresps(editingContext, qualifier, null);
    EOCongeAl6Corresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeAl6Corresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CongeAl6Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl6Corresp fetchRequiredCongeAl6Corresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeAl6Corresp.fetchRequiredCongeAl6Corresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeAl6Corresp fetchRequiredCongeAl6Corresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeAl6Corresp eoObject = _EOCongeAl6Corresp.fetchCongeAl6Corresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CongeAl6Corresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeAl6Corresp localInstanceIn(EOEditingContext editingContext, EOCongeAl6Corresp eo) {
    EOCongeAl6Corresp localInstance = (eo == null) ? null : (EOCongeAl6Corresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
