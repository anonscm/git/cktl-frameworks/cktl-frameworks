package org.cocktail.connecteur.serveur.modele.correspondance;

import org.apache.log4j.Logger;

/**.
 * Classe gérant les correspondances pour les Distinctions d'un individu
 * @author alainmalaplate
 *
 */
public class EOIndividuDistinctionsCorresp extends _EOIndividuDistinctionsCorresp {

	private static final long serialVersionUID = -1728057265988825254L;
	private static Logger log = Logger.getLogger(EOIndividuDistinctionsCorresp.class);

	public EOIndividuDistinctionsCorresp() {
		super();
	}


	public String nomRelationBaseDestinataire() {
		return MANGUE_INDIVIDU_DISTINCTIONS_KEY;
	}

	public String nomRelationBaseImport() {
		return INDIVIDU_DISTINCTIONS_KEY;
	}

	public void supprimerRelations() {
		setIndividuDistinctionsRelationship(null);
		setMangueIndividuDistinctionsRelationship(null);
	}

}
