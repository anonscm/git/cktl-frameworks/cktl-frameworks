// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODechargeCorresp.java instead.
package org.cocktail.connecteur.serveur.modele.correspondance;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODechargeCorresp extends ObjetCorresp {
	public static final String ENTITY_NAME = "DechargeCorresp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_DECHARGE_KEY = "toDecharge";
	public static final String TO_MANGUE_DECHARGE_KEY = "toMangueDecharge";

  private static Logger LOG = Logger.getLogger(_EODechargeCorresp.class);

  public EODechargeCorresp localInstanceIn(EOEditingContext editingContext) {
    EODechargeCorresp localInstance = (EODechargeCorresp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODechargeCorresp.LOG.isDebugEnabled()) {
    	_EODechargeCorresp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODechargeCorresp.LOG.isDebugEnabled()) {
    	_EODechargeCorresp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_import.EODecharge toDecharge() {
    return (org.cocktail.connecteur.serveur.modele.entite_import.EODecharge)storedValueForKey("toDecharge");
  }

  public void setToDechargeRelationship(org.cocktail.connecteur.serveur.modele.entite_import.EODecharge value) {
    if (_EODechargeCorresp.LOG.isDebugEnabled()) {
      _EODechargeCorresp.LOG.debug("updating toDecharge from " + toDecharge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_import.EODecharge oldValue = toDecharge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDecharge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDecharge");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDecharge toMangueDecharge() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDecharge)storedValueForKey("toMangueDecharge");
  }

  public void setToMangueDechargeRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDecharge value) {
    if (_EODechargeCorresp.LOG.isDebugEnabled()) {
      _EODechargeCorresp.LOG.debug("updating toMangueDecharge from " + toMangueDecharge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDecharge oldValue = toMangueDecharge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueDecharge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueDecharge");
    }
  }
  

  public static EODechargeCorresp createDechargeCorresp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_import.EODecharge toDecharge, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDecharge toMangueDecharge) {
    EODechargeCorresp eo = (EODechargeCorresp) EOUtilities.createAndInsertInstance(editingContext, _EODechargeCorresp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToDechargeRelationship(toDecharge);
    eo.setToMangueDechargeRelationship(toMangueDecharge);
    return eo;
  }

  public static NSArray<EODechargeCorresp> fetchAllDechargeCorresps(EOEditingContext editingContext) {
    return _EODechargeCorresp.fetchAllDechargeCorresps(editingContext, null);
  }

  public static NSArray<EODechargeCorresp> fetchAllDechargeCorresps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODechargeCorresp.fetchDechargeCorresps(editingContext, null, sortOrderings);
  }

  public static NSArray<EODechargeCorresp> fetchDechargeCorresps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODechargeCorresp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODechargeCorresp> eoObjects = (NSArray<EODechargeCorresp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODechargeCorresp fetchDechargeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODechargeCorresp.fetchDechargeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODechargeCorresp fetchDechargeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODechargeCorresp> eoObjects = _EODechargeCorresp.fetchDechargeCorresps(editingContext, qualifier, null);
    EODechargeCorresp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODechargeCorresp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one DechargeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODechargeCorresp fetchRequiredDechargeCorresp(EOEditingContext editingContext, String keyName, Object value) {
    return _EODechargeCorresp.fetchRequiredDechargeCorresp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODechargeCorresp fetchRequiredDechargeCorresp(EOEditingContext editingContext, EOQualifier qualifier) {
    EODechargeCorresp eoObject = _EODechargeCorresp.fetchDechargeCorresp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no DechargeCorresp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODechargeCorresp localInstanceIn(EOEditingContext editingContext, EODechargeCorresp eo) {
    EODechargeCorresp localInstance = (eo == null) ? null : (EODechargeCorresp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
