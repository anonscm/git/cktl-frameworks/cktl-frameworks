package org.cocktail.connecteur.serveur.modele.correspondance.conges;

public class EOCongeAl5Corresp extends _EOCongeAl5Corresp {
	@Override
	public void supprimerRelations() {
		setCongeAl5Relationship(null);
		setMangueCongeAl5Relationship(null);
	}

	@Override
	public String nomRelationBaseImport() {
		return CONGE_AL5_KEY;
	}

	@Override
	public String nomRelationBaseDestinataire() {
		return MANGUE_CONGE_AL5_KEY;
	}
}
