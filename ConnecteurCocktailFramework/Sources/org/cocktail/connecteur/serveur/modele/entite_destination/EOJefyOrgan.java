package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOOrgaBudgetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOOrgaBudget;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOJefyOrgan extends _EOJefyOrgan implements InterfaceGestionIdUtilisateur {
	@Override
	public void setTemValide(String value) {
		// Cette entité n'a pas de champ temValide donc on redéfini son comportement
	}
	
	public void setDCreation(NSTimestamp value) {
		// L'entité comporte une date de modification mais pas de création
	}

	protected void initialiserObjet(ObjetImport record) {
		EOOrgaBudget organ = (EOOrgaBudget) record;
		
		setOrgUniv(organ.obUniv());
		setOrgLucrativite(organ.obLucrativite());
		setTyorId(organ.tyorId());
		
		if (orgUniv() == null || orgUniv().isEmpty()) {
			setOrgUniv("UNIV");
		}
		if (orgLucrativite() == null) {
			setOrgLucrativite(new Integer(0));
		}
		if (tyorId() == null) {
			setTyorId(new Integer(1));
		}
		
		EOJefyOrgan jefyOrganPere=null;
		if (organ.toOrgaBudgetPere()!=null && organ.toOrgaBudgetPere().correspondance()!=null) {
			jefyOrganPere=((EOOrgaBudgetCorresp)organ.toOrgaBudgetPere().correspondance()).jefyOrgan();
			setToOrganPereRelationship(jefyOrganPere);
		}
	}
	
	public static EOJefyOrgan getFromId(EOEditingContext editingContext, Integer orgId) {
		try {
		return fetchJefyOrgan(editingContext, ORG_ID_KEY, orgId);
		} catch (Exception e) {
			return null;
		}
	}
	public Integer persIdCreation() {
		// Ce champs n'existe pas pour Organ. Récupère l'id de modification
		return persIdModification();
	}

	public void setPersIdCreation(Integer value) {
		// Ce champs n'existe pas pour Organ
	}
}
