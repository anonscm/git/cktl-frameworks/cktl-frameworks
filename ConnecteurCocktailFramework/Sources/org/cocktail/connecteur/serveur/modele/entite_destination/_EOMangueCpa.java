// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCpa.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCpa extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueCpa";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_CTA_KEY = "temCta";
	public static final String TEM_QUOT_DEGRESSIVE_KEY = "temQuotDegressive";
	public static final String TEM_SURCOTISATION_KEY = "temSurcotisation";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueCpa.class);

  public EOMangueCpa localInstanceIn(EOEditingContext editingContext) {
    EOMangueCpa localInstance = (EOMangueCpa)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temCta() {
    return (String) storedValueForKey("temCta");
  }

  public void setTemCta(String value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating temCta from " + temCta() + " to " + value);
    }
    takeStoredValueForKey(value, "temCta");
  }

  public String temQuotDegressive() {
    return (String) storedValueForKey("temQuotDegressive");
  }

  public void setTemQuotDegressive(String value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating temQuotDegressive from " + temQuotDegressive() + " to " + value);
    }
    takeStoredValueForKey(value, "temQuotDegressive");
  }

  public String temSurcotisation() {
    return (String) storedValueForKey("temSurcotisation");
  }

  public void setTemSurcotisation(String value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating temSurcotisation from " + temSurcotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "temSurcotisation");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
    	_EOMangueCpa.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCpa.LOG.isDebugEnabled()) {
      _EOMangueCpa.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueCpa createMangueCpa(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temQuotDegressive
, String temSurcotisation
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCpa eo = (EOMangueCpa) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCpa.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemQuotDegressive(temQuotDegressive);
		eo.setTemSurcotisation(temSurcotisation);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCpa> fetchAllMangueCpas(EOEditingContext editingContext) {
    return _EOMangueCpa.fetchAllMangueCpas(editingContext, null);
  }

  public static NSArray<EOMangueCpa> fetchAllMangueCpas(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCpa.fetchMangueCpas(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCpa> fetchMangueCpas(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCpa.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCpa> eoObjects = (NSArray<EOMangueCpa>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCpa fetchMangueCpa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCpa.fetchMangueCpa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCpa fetchMangueCpa(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCpa> eoObjects = _EOMangueCpa.fetchMangueCpas(editingContext, qualifier, null);
    EOMangueCpa eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCpa)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCpa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCpa fetchRequiredMangueCpa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCpa.fetchRequiredMangueCpa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCpa fetchRequiredMangueCpa(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCpa eoObject = _EOMangueCpa.fetchMangueCpa(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCpa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCpa localInstanceIn(EOEditingContext editingContext, EOMangueCpa eo) {
    EOMangueCpa localInstance = (eo == null) ? null : (EOMangueCpa)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
