package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCongeMaladieCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladie;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaladieDetail;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueCongeMaladieDetail extends _EOMangueCongeMaladieDetail {
	public String temValide() {
		// La table ne contient pas de TemValide (03/2015). On retourne donc systématiquement vrai
		return CocktailConstantes.VRAI;
	}

	public void setTemValide(String value) {
		// La table ne contient pas de TemValide (03/2015). On surcharge donc la méthode pour ne rien faire
	}
	
	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOCongeMaladieDetail congeDetailImport=(EOCongeMaladieDetail)recordImport;
		EOMangueCongeMaladie congeMaladie = EOCongeMaladieCorresp.congeMaladieMangue(editingContext(),congeDetailImport.congeMaladie());
		setCongeMaladieRelationship(congeMaladie);
	}
}
