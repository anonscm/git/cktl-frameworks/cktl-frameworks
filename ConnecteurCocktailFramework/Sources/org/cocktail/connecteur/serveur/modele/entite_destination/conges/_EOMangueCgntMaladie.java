// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCgntMaladie.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCgntMaladie extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCgntMaladie";

	// Attributes
	public static final String C_ANCIENNETE_KEY = "cAnciennete";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRET_TRAVAIL_KEY = "dArretTravail";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_DUREE_CONTINUE_KEY = "temDureeContinue";
	public static final String TEM_EN_CAUSE_KEY = "temEnCause";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_JOUR_CARENCE_KEY = "temJourCarence";
	public static final String TEM_PROLONG_MALADIE_KEY = "temProlongMaladie";
	public static final String TEM_REQUALIF_MALADIE_KEY = "temRequalifMaladie";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCgntMaladie.class);

  public EOMangueCgntMaladie localInstanceIn(EOEditingContext editingContext) {
    EOMangueCgntMaladie localInstance = (EOMangueCgntMaladie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cAnciennete() {
    return (String) storedValueForKey("cAnciennete");
  }

  public void setCAnciennete(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating cAnciennete from " + cAnciennete() + " to " + value);
    }
    takeStoredValueForKey(value, "cAnciennete");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArretTravail() {
    return (NSTimestamp) storedValueForKey("dArretTravail");
  }

  public void setDArretTravail(NSTimestamp value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating dArretTravail from " + dArretTravail() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretTravail");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temDureeContinue() {
    return (String) storedValueForKey("temDureeContinue");
  }

  public void setTemDureeContinue(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temDureeContinue from " + temDureeContinue() + " to " + value);
    }
    takeStoredValueForKey(value, "temDureeContinue");
  }

  public String temEnCause() {
    return (String) storedValueForKey("temEnCause");
  }

  public void setTemEnCause(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temEnCause from " + temEnCause() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnCause");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temJourCarence() {
    return (String) storedValueForKey("temJourCarence");
  }

  public void setTemJourCarence(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temJourCarence from " + temJourCarence() + " to " + value);
    }
    takeStoredValueForKey(value, "temJourCarence");
  }

  public String temProlongMaladie() {
    return (String) storedValueForKey("temProlongMaladie");
  }

  public void setTemProlongMaladie(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temProlongMaladie from " + temProlongMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongMaladie");
  }

  public String temRequalifMaladie() {
    return (String) storedValueForKey("temRequalifMaladie");
  }

  public void setTemRequalifMaladie(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temRequalifMaladie from " + temRequalifMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalifMaladie");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
    	_EOMangueCgntMaladie.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
      _EOMangueCgntMaladie.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie value) {
    if (_EOMangueCgntMaladie.LOG.isDebugEnabled()) {
      _EOMangueCgntMaladie.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntMaladie oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCgntMaladie createMangueCgntMaladie(EOEditingContext editingContext, String cAnciennete
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temConfirme
, String temDureeContinue
, String temEnCause
, String temGestEtab
, String temJourCarence
, String temProlongMaladie
, String temRequalifMaladie
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCgntMaladie eo = (EOMangueCgntMaladie) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCgntMaladie.ENTITY_NAME);    
		eo.setCAnciennete(cAnciennete);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemConfirme(temConfirme);
		eo.setTemDureeContinue(temDureeContinue);
		eo.setTemEnCause(temEnCause);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemJourCarence(temJourCarence);
		eo.setTemProlongMaladie(temProlongMaladie);
		eo.setTemRequalifMaladie(temRequalifMaladie);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCgntMaladie> fetchAllMangueCgntMaladies(EOEditingContext editingContext) {
    return _EOMangueCgntMaladie.fetchAllMangueCgntMaladies(editingContext, null);
  }

  public static NSArray<EOMangueCgntMaladie> fetchAllMangueCgntMaladies(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCgntMaladie.fetchMangueCgntMaladies(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCgntMaladie> fetchMangueCgntMaladies(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCgntMaladie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCgntMaladie> eoObjects = (NSArray<EOMangueCgntMaladie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCgntMaladie fetchMangueCgntMaladie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCgntMaladie.fetchMangueCgntMaladie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCgntMaladie fetchMangueCgntMaladie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCgntMaladie> eoObjects = _EOMangueCgntMaladie.fetchMangueCgntMaladies(editingContext, qualifier, null);
    EOMangueCgntMaladie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCgntMaladie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCgntMaladie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCgntMaladie fetchRequiredMangueCgntMaladie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCgntMaladie.fetchRequiredMangueCgntMaladie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCgntMaladie fetchRequiredMangueCgntMaladie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCgntMaladie eoObject = _EOMangueCgntMaladie.fetchMangueCgntMaladie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCgntMaladie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCgntMaladie localInstanceIn(EOEditingContext editingContext, EOMangueCgntMaladie eo) {
    EOMangueCgntMaladie localInstance = (eo == null) ? null : (EOMangueCgntMaladie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
