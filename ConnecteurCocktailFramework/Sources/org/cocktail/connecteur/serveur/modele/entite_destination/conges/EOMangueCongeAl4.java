package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCongeAl4Corresp;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;

import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongeAl4 extends _EOMangueCongeAl4 {
	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongeAl4) value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongeAl4Corresp congeCorresp = EOCongeAl4Corresp.fetchCongeAl4Corresp(editingContext(), EOCongeAl4Corresp.CONGE_AL4_KEY, congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.mangueCongeAl4();
	}

	@Override
	public NSTimestamp dateCommission() {
		return dComMed();
	}

	@Override
	public String typeEvenement() {
		return "CAL4";
	}
}
