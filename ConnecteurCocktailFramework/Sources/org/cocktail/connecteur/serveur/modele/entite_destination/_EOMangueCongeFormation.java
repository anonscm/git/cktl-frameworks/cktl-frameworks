// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongeFormation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongeFormation extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueCongeFormation";

	// Attributes
	public static final String C_CHAPITRE_KEY = "cChapitre";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_INDEMNITE_CGF_KEY = "dDebIndemniteCgf";
	public static final String D_DEMANDE_CGF_KEY = "dDemandeCgf";
	public static final String D_FIN_INDEMNITE_CGF_KEY = "dFinIndemniteCgf";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_JJ_CGF_KEY = "dureeJjCgf";
	public static final String DUREE_MM_CGF_KEY = "dureeMmCgf";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_FORM_AGREE_KEY = "temFormAgree";
	public static final String TEM_FRACTIONNE_KEY = "temFractionne";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CHAPITRE_KEY = "chapitre";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueCongeFormation.class);

  public EOMangueCongeFormation localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongeFormation localInstance = (EOMangueCongeFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cChapitre() {
    return (Integer) storedValueForKey("cChapitre");
  }

  public void setCChapitre(Integer value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating cChapitre from " + cChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cChapitre");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebIndemniteCgf() {
    return (NSTimestamp) storedValueForKey("dDebIndemniteCgf");
  }

  public void setDDebIndemniteCgf(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dDebIndemniteCgf from " + dDebIndemniteCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebIndemniteCgf");
  }

  public NSTimestamp dDemandeCgf() {
    return (NSTimestamp) storedValueForKey("dDemandeCgf");
  }

  public void setDDemandeCgf(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dDemandeCgf from " + dDemandeCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dDemandeCgf");
  }

  public NSTimestamp dFinIndemniteCgf() {
    return (NSTimestamp) storedValueForKey("dFinIndemniteCgf");
  }

  public void setDFinIndemniteCgf(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dFinIndemniteCgf from " + dFinIndemniteCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinIndemniteCgf");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dureeJjCgf() {
    return (Integer) storedValueForKey("dureeJjCgf");
  }

  public void setDureeJjCgf(Integer value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dureeJjCgf from " + dureeJjCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeJjCgf");
  }

  public Integer dureeMmCgf() {
    return (Integer) storedValueForKey("dureeMmCgf");
  }

  public void setDureeMmCgf(Integer value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating dureeMmCgf from " + dureeMmCgf() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeMmCgf");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temFormAgree() {
    return (String) storedValueForKey("temFormAgree");
  }

  public void setTemFormAgree(String value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating temFormAgree from " + temFormAgree() + " to " + value);
    }
    takeStoredValueForKey(value, "temFormAgree");
  }

  public String temFractionne() {
    return (String) storedValueForKey("temFractionne");
  }

  public void setTemFractionne(String value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating temFractionne from " + temFractionne() + " to " + value);
    }
    takeStoredValueForKey(value, "temFractionne");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
    	_EOMangueCongeFormation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre chapitre() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre)storedValueForKey("chapitre");
  }

  public void setChapitreRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
      _EOMangueCongeFormation.LOG.debug("updating chapitre from " + chapitre() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOChapitre oldValue = chapitre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "chapitre");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "chapitre");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCongeFormation.LOG.isDebugEnabled()) {
      _EOMangueCongeFormation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueCongeFormation createMangueCongeFormation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCongeFormation eo = (EOMangueCongeFormation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongeFormation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCongeFormation> fetchAllMangueCongeFormations(EOEditingContext editingContext) {
    return _EOMangueCongeFormation.fetchAllMangueCongeFormations(editingContext, null);
  }

  public static NSArray<EOMangueCongeFormation> fetchAllMangueCongeFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongeFormation.fetchMangueCongeFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongeFormation> fetchMangueCongeFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongeFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongeFormation> eoObjects = (NSArray<EOMangueCongeFormation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongeFormation fetchMangueCongeFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeFormation.fetchMangueCongeFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeFormation fetchMangueCongeFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongeFormation> eoObjects = _EOMangueCongeFormation.fetchMangueCongeFormations(editingContext, qualifier, null);
    EOMangueCongeFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongeFormation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongeFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeFormation fetchRequiredMangueCongeFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeFormation.fetchRequiredMangueCongeFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeFormation fetchRequiredMangueCongeFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongeFormation eoObject = _EOMangueCongeFormation.fetchMangueCongeFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongeFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeFormation localInstanceIn(EOEditingContext editingContext, EOMangueCongeFormation eo) {
    EOMangueCongeFormation localInstance = (eo == null) ? null : (EOMangueCongeFormation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
