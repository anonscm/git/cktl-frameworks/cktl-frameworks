package org.cocktail.connecteur.serveur.modele.entite_destination.services;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCategorieEmploi;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBudget;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiCategorie;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiLocalisation;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiNatureBudget;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploiSpecialisation;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

public class ServicesEmploi {
	
	private EOMangueEmploiCategorie mangueEmploiCategorie;
	private EOMangueEmploiNatureBudget mangueEmploiNatureBudget;
	private EOMangueEmploiSpecialisation mangueEmploiSpecialisation;
	private EOMangueEmploiLocalisation mangueEmploiLocalisation;
	
	
	public EOMangueEmploiCategorie getMangueEmploiCategorie() {
		return mangueEmploiCategorie;
	}

	public void setMangueEmploiCategorie(
			EOMangueEmploiCategorie mangueEmploiCategorie) {
		this.mangueEmploiCategorie = mangueEmploiCategorie;
	}

	public EOMangueEmploiNatureBudget getMangueEmploiNatureBudget() {
		return mangueEmploiNatureBudget;
	}

	public void setMangueEmploiNatureBudget(
			EOMangueEmploiNatureBudget mangueEmploiNatureBudget) {
		this.mangueEmploiNatureBudget = mangueEmploiNatureBudget;
	}

	public EOMangueEmploiSpecialisation getMangueEmploiSpecialisation() {
		return mangueEmploiSpecialisation;
	}

	public void setMangueEmploiSpecialisation(
			EOMangueEmploiSpecialisation mangueEmploiSpecialisation) {
		this.mangueEmploiSpecialisation = mangueEmploiSpecialisation;
	}

	public EOMangueEmploiLocalisation getMangueEmploiLocalisation() {
		return mangueEmploiLocalisation;
	}

	public void setMangueEmploiLocalisation(
			EOMangueEmploiLocalisation mangueEmploiLocalisation) {
		this.mangueEmploiLocalisation = mangueEmploiLocalisation;
	}

	

	public void intialisationServicesEmploi(EOMangueEmploi mangueEmploi, EOGrhumStructure structure, String cCategorie, String cBudget, EOEmploi emploiImport) {
		EOEditingContext edc = mangueEmploi.editingContext();
		Integer idEmploi = mangueEmploi.idEmploi();
//		LogManager.logInformation("idEmploi = " + idEmploi);

		NSTimestamp dateActuelle = new NSTimestamp();
		NSTimestamp dCreation = dateActuelle;
		NSTimestamp dModification = dateActuelle;

//		LogManager.logInformation("Date création : " + dCreation + " Date modification : " + dModification);
		NSTimestamp dDebut = mangueEmploi.dEffetEmploi();
		NSTimestamp dFin = null;
//		LogManager.logInformation("Date effet : " + dDebut + " Date fin : " + dFin);
		
		EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
		Integer persIdCreation = utilisateur.persId();
		Integer persIdModification = utilisateur.persId();

//		LogManager.logInformation("Pers ID création : " + persIdCreation + " Pers ID modification : " + persIdModification);
		
		EOCategorieEmploi categorieEmploi = rechercherCategorieEmploi(edc, cCategorie);
		
		if (categorieEmploi != null) {
			mangueEmploiCategorie = EOMangueEmploiCategorie
					.createMangueEmploiCategorie(edc, cCategorie, dCreation,
							dDebut, dModification, idEmploi, categorieEmploi,
							mangueEmploi);
			getMangueEmploiCategorie().setPersIdCreation(persIdCreation);
			getMangueEmploiCategorie()
					.setPersIdModification(persIdModification);
		}
		
		EONatureBudget natureBudget = rechercherNatureBudget(edc, cBudget);
		
		mangueEmploiNatureBudget = EOMangueEmploiNatureBudget.createMangueEmploiNatureBudget(edc, cBudget, dCreation, dDebut, dModification, idEmploi, mangueEmploi, natureBudget);
		getMangueEmploiNatureBudget().setPersIdCreation(persIdCreation);
		getMangueEmploiNatureBudget().setPersIdModification(persIdModification);
		
		if (determinationSpecialisationActif(emploiImport) ) {
			mangueEmploiSpecialisation = EOMangueEmploiSpecialisation
					.createMangueEmploiSpecialisation(edc, dCreation, dDebut,
							dModification, idEmploi, mangueEmploi);
			getMangueEmploiSpecialisation().setPersIdCreation(persIdCreation);
			getMangueEmploiSpecialisation().setPersIdModification(
					persIdModification);
			getMangueEmploiSpecialisation().setCBap(emploiImport.cBap());
			getMangueEmploiSpecialisation().setCDiscSecondDegre(
					emploiImport.cDiscSecondDegre());
			getMangueEmploiSpecialisation().setCodeemploi(
					emploiImport.codeemploi());
			getMangueEmploiSpecialisation().setCSpecialiteItarf(
					emploiImport.cSpecialiteItarf());
			getMangueEmploiSpecialisation().setCSpecialiteAtos(
					emploiImport.cSpecialiteAtos());
			determinerNoCnu(edc, emploiImport);
		}
	}
	
	private boolean determinationSpecialisationActif(EOEmploi emploiImport) {
		return emploiImport != null
				&& ((emploiImport.cBap() != null && !emploiImport.cBap().isEmpty()) 
					|| (emploiImport.cSpecialiteItarf() != null && !emploiImport.cSpecialiteItarf().isEmpty())
					|| (emploiImport.cDiscSecondDegre() != null && !emploiImport.cDiscSecondDegre().isEmpty())
					|| (emploiImport.codeemploi() != null && !emploiImport.codeemploi().isEmpty())
					|| (emploiImport.cSpecialiteAtos() != null && !emploiImport.cSpecialiteAtos().isEmpty())
					|| (emploiImport.cSectionCnu() != null && !emploiImport.cSectionCnu().isEmpty()));
	}

//	private boolean determinationSpecialisationActif(EOEmploi emploiImport) {
//		return !emploiImport.cBap().isEmpty() || !emploiImport.cSpecialiteItarf().isEmpty()
//				|| !emploiImport.cDiscSecondDegre().isEmpty() || !emploiImport.codeemploi().isEmpty()
//				|| !emploiImport.cSpecialiteAtos().isEmpty() || !emploiImport.cSectionCnu().isEmpty();
//	}
	
	
	
	
	private EONatureBudget rechercherNatureBudget(EOEditingContext edc, String cBudget) {
		
		EOQualifier qual = new EOKeyValueQualifier(EONatureBudget.C_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, cBudget);
		
		return EONatureBudget.fetchRequiredNatureBudget(edc, qual);
	}
	
	private EOCategorieEmploi rechercherCategorieEmploi(EOEditingContext edc, String cCategorie) {
		EOQualifier qual = new EOKeyValueQualifier(EOCategorieEmploi.C_CATEGORIE_EMPLOI_KEY, EOQualifier.QualifierOperatorEqual, cCategorie);
		
		return EOCategorieEmploi.fetchRequiredCategorieEmploi(edc, qual);
	}
	
	public void determinerNoCnu(EOEditingContext edc, EOEmploi emploi) {
		EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(edc, emploi.cSectionCnu(), emploi.cSousSectionCnu());
		if (cnu != null) {
			getMangueEmploiSpecialisation().setNoCnu(cnu.noCnu());
		} else {
			getMangueEmploiSpecialisation().setNoCnu(null);
		}
	}
	
}
