package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EORepartAssociation;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOGrhumRepartAssociation extends _EOGrhumRepartAssociation {
	@Override
	public void setTemValide(String value) {
		// Pas de champ TemValide. On surcharge l'affectation par défaut de la propriété
	}

	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EORepartAssociation repartAsso = (EORepartAssociation) recordImport;

		EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), repartAsso.strSource());
		setStructureRelationship(structure);

		if (repartAsso.str2Source() != null) {
			EOGrhumStructure structure2 = EOStructureCorresp.structureGrhum(editingContext(), repartAsso.str2Source());
			if (structure2 == null)
				throw new Exception("Pas de structure dans le SI correspondant a la structure " + repartAsso.str2Source());
			setPersId(structure2.persId());
		}

		if (repartAsso.idSource() != null) {
			EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), repartAsso.idSource());
			if (individu == null)
				throw new Exception("Pas d'individu dans le SI correspondant a l'individu " + repartAsso.idSource());
			setPersId(individu.persId());
		}

		EOAssociation association = EOAssociation.getFromCode(editingContext(), repartAsso.assCode());
		setAssociationRelationship(association);
		
		EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
		setPersIdCreation(utilisateur.persId());
		setPersIdModification(utilisateur.persId());
	}
}
