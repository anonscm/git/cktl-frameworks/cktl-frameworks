package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividu;

public class EOMangueIndividuDistinctions extends _EOMangueIndividuDistinctions {
	private static Logger log = Logger.getLogger(EOMangueIndividuDistinctions.class);

	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		ObjetImportPourIndividu objetImport = (ObjetImportPourIndividu)recordImport;
		EOGrhumIndividu individuGrhum = null;
		if (objetImport.individu()==null)
			throw new Exception("Pas d'individu dans objetImport");
		
		// A ce stade, l'individu grhum est dans le SI Destinataire
		individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), objetImport.individu().idSource());
		if (individuGrhum != null) {
			addObjectToBothSidesOfRelationshipWithKey(individuGrhum, INDIVIDU_KEY);
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + objetImport.individu().idSource());
		}

	}

	@Override
	public String temValide() {
		// La table Mangue ne contient pas de TemValide (12/2014). On retourne donc systématiquement vrai
		return CocktailConstantes.VRAI;
	}

	@Override
	public void setTemValide(String value) {
		// La table Mangue ne contient pas de TemValide (12/2014). On surcharge donc la méthode pour ne rien faire
	}
}
