// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCrctDetail.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCrctDetail extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueCrctDetail";

	// Attributes
	public static final String CRCT_ID_KEY = "crctId";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String MANGUE_CRCT_KEY = "mangueCrct";

  private static Logger LOG = Logger.getLogger(_EOMangueCrctDetail.class);

  public EOMangueCrctDetail localInstanceIn(EOEditingContext editingContext) {
    EOMangueCrctDetail localInstance = (EOMangueCrctDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer crctId() {
    return (Integer) storedValueForKey("crctId");
  }

  public void setCrctId(Integer value) {
    if (_EOMangueCrctDetail.LOG.isDebugEnabled()) {
    	_EOMangueCrctDetail.LOG.debug( "updating crctId from " + crctId() + " to " + value);
    }
    takeStoredValueForKey(value, "crctId");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCrctDetail.LOG.isDebugEnabled()) {
    	_EOMangueCrctDetail.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCrctDetail.LOG.isDebugEnabled()) {
    	_EOMangueCrctDetail.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCrctDetail.LOG.isDebugEnabled()) {
    	_EOMangueCrctDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCrctDetail.LOG.isDebugEnabled()) {
    	_EOMangueCrctDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct mangueCrct() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct)storedValueForKey("mangueCrct");
  }

  public void setMangueCrctRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct value) {
    if (_EOMangueCrctDetail.LOG.isDebugEnabled()) {
      _EOMangueCrctDetail.LOG.debug("updating mangueCrct from " + mangueCrct() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct oldValue = mangueCrct();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCrct");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCrct");
    }
  }
  

  public static EOMangueCrctDetail createMangueCrctDetail(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOMangueCrctDetail eo = (EOMangueCrctDetail) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCrctDetail.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOMangueCrctDetail> fetchAllMangueCrctDetails(EOEditingContext editingContext) {
    return _EOMangueCrctDetail.fetchAllMangueCrctDetails(editingContext, null);
  }

  public static NSArray<EOMangueCrctDetail> fetchAllMangueCrctDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCrctDetail.fetchMangueCrctDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCrctDetail> fetchMangueCrctDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCrctDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCrctDetail> eoObjects = (NSArray<EOMangueCrctDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCrctDetail fetchMangueCrctDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCrctDetail.fetchMangueCrctDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCrctDetail fetchMangueCrctDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCrctDetail> eoObjects = _EOMangueCrctDetail.fetchMangueCrctDetails(editingContext, qualifier, null);
    EOMangueCrctDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCrctDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCrctDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCrctDetail fetchRequiredMangueCrctDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCrctDetail.fetchRequiredMangueCrctDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCrctDetail fetchRequiredMangueCrctDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCrctDetail eoObject = _EOMangueCrctDetail.fetchMangueCrctDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCrctDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCrctDetail localInstanceIn(EOEditingContext editingContext, EOMangueCrctDetail eo) {
    EOMangueCrctDetail localInstance = (eo == null) ? null : (EOMangueCrctDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
