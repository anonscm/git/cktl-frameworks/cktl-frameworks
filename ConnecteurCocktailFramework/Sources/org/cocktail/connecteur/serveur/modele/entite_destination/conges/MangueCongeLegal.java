/*
 * Created on 25 janv. 2006
 *
 * Durée avec une relation sur la classe EOAbscences
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.InterfaceRecordAvecAutresAttributs;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetSIDestinatairePourPeriodeEtIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.foundation.NSTimestamp;
/**  Classe abstraite pour modeliser les conges legaux
 * Cette classe est a modifier lors de l'ajout de temoin de validite dans les conges de Mangue (estValide/setValide)

 */
public abstract class MangueCongeLegal extends ObjetSIDestinatairePourPeriodeEtIndividu implements InterfaceRecordAvecAutresAttributs {
	public String noArrete() {
		return (String)storedValueForKey("noArrete");
	}

	public void setNoArrete(String value) {
		takeStoredValueForKey(value, "noArrete");
	}

	public NSTimestamp dateArrete() {
		return (NSTimestamp)storedValueForKey("dateArrete");
	}

	public void setDateArrete(NSTimestamp value) {
		takeStoredValueForKey(value, "dateArrete");
	}

	public String commentaire() {
		return (String)storedValueForKey("commentaire");
	}

	public void setCommentaire(String value) {
		takeStoredValueForKey(value, "commentaire");
	}
//	public EOMangueAbsences absence() {
//		return (EOMangueAbsences)storedValueForKey("absence");
//	}
//	public void setAbsence(EOMangueAbsences value) {
//		takeStoredValueForKey(value, "absence");
//	}
	// Méthodes ajoutées
	/** Retourne le type de conge de la table TYPE_ABSENCE */
	public abstract String typeEvenement();

	/** Creation de l'absence si il s'agit d'un record en insertion*/
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport record) {
		boolean estModifie = false;
		CongeAvecArrete conge = (CongeAvecArrete)record;
//		EOMangueAbsences absenceCourante = null;
		// Pour les records en insertion, créer l'absence
		// Pour les records en correspondance ou update, il doit exister une absence dans Mangue
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), conge.individu().idSource());
		// A ce stade, l'individu GRhum est déjà dans le SI Destinataire sinon on ne pourrait pas transférer le congé
//		if (record.operation().equals(ObjetImport.OPERATION_INSERTION)) {
//			absenceCourante = new EOMangueAbsences();
//			editingContext().insertObject(absenceCourante);
//			this.addObjectToBothSidesOfRelationshipWithKey(absenceCourante, "absence");
//			absenceCourante.initAvecTypeEtIndividu(typeEvenement(), individuGrhum.noIndividu());
//		} else if (record.operation().equals(ObjetImport.OPERATION_UPDATE)) {
//			absenceCourante = absence();	// dans un update l'absence est déjà initialisée
//		} else {	// Normalement, il n'y a pas de correspondance
//			absenceCourante = EOMangueAbsences.rechercherAbsencePourIndividuDateEtTypeAbsence(editingContext(), individuGrhum.noIndividu(), conge.dateDebut(), conge.dateFin(), typeEvenement());
//		}
//		// Vérifier si il y a des règles de priorité sur les attributs qui fait qu'on ne doit pas y toucher
		EOGrhumPrioriteEntite priorite = null;
		String message = "";
		if (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || record.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			priorite = prioriteEntite();
		}
//		// Modifier les attributs de date de l'absence si il n'y a pas de règle de priorité sur ceux-ci
//		if ((priorite == null || priorite.attributPourLibelle("dateDebut",true) == null)) {
//			absenceCourante.setAbsDebut(conge.dateDebut());
//			message = "Absence date debut\n";
//			estModifie = true;
//		}
//		if ((priorite == null || priorite.attributPourLibelle("dateFin",true) == null)) {
//			absenceCourante.setAbsFin(conge.dateFin());
//			message += "Absence date fin\n";
//			estModifie = true;
//		}
//		if (estModifie) {
//			absenceCourante.setAbsDureeTotale("" + (float)DateCtrl.nbJoursEntre(conge.dateDebut(),conge.dateFin(),true));
//			absenceCourante.setDModification(new NSTimestamp());
//		}
		if (estModifie && (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || record.operation().equals(ObjetImport.OPERATION_UPDATE))) {
			message = "Operation de correspondance ou d'update, record : " + record + "\n" + message;
			LogManager.logDetail(message);
		}

		return estModifie;
	}
}
