// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCarriere.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCarriere extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueCarriere";

	// Attributes
	public static final String C_TYPE_POPULATION_KEY = "cTypePopulation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CARRIERE_KEY = "dDebCarriere";
	public static final String D_FIN_CARRIERE_KEY = "dFinCarriere";
	public static final String D_MIN_ELEM_CAR_KEY = "dMinElemCar";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueCarriere.class);

  public EOMangueCarriere localInstanceIn(EOEditingContext editingContext) {
    EOMangueCarriere localInstance = (EOMangueCarriere)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypePopulation() {
    return (String) storedValueForKey("cTypePopulation");
  }

  public void setCTypePopulation(String value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating cTypePopulation from " + cTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypePopulation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebCarriere() {
    return (NSTimestamp) storedValueForKey("dDebCarriere");
  }

  public void setDDebCarriere(NSTimestamp value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating dDebCarriere from " + dDebCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebCarriere");
  }

  public NSTimestamp dFinCarriere() {
    return (NSTimestamp) storedValueForKey("dFinCarriere");
  }

  public void setDFinCarriere(NSTimestamp value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating dFinCarriere from " + dFinCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinCarriere");
  }

  public NSTimestamp dMinElemCar() {
    return (NSTimestamp) storedValueForKey("dMinElemCar");
  }

  public void setDMinElemCar(NSTimestamp value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating dMinElemCar from " + dMinElemCar() + " to " + value);
    }
    takeStoredValueForKey(value, "dMinElemCar");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
    	_EOMangueCarriere.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCarriere.LOG.isDebugEnabled()) {
      _EOMangueCarriere.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueCarriere createMangueCarriere(EOEditingContext editingContext, String cTypePopulation
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCarriere eo = (EOMangueCarriere) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCarriere.ENTITY_NAME);    
		eo.setCTypePopulation(cTypePopulation);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCarriere> fetchAllMangueCarrieres(EOEditingContext editingContext) {
    return _EOMangueCarriere.fetchAllMangueCarrieres(editingContext, null);
  }

  public static NSArray<EOMangueCarriere> fetchAllMangueCarrieres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCarriere.fetchMangueCarrieres(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCarriere> fetchMangueCarrieres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCarriere.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCarriere> eoObjects = (NSArray<EOMangueCarriere>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCarriere fetchMangueCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCarriere.fetchMangueCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCarriere fetchMangueCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCarriere> eoObjects = _EOMangueCarriere.fetchMangueCarrieres(editingContext, qualifier, null);
    EOMangueCarriere eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCarriere)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCarriere fetchRequiredMangueCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCarriere.fetchRequiredMangueCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCarriere fetchRequiredMangueCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCarriere eoObject = _EOMangueCarriere.fetchMangueCarriere(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCarriere localInstanceIn(EOEditingContext editingContext, EOMangueCarriere eo) {
    EOMangueCarriere localInstance = (eo == null) ? null : (EOMangueCarriere)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
