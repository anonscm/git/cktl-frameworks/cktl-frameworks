// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCfa.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCfa extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueCfa";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_FIN_EXECUTION_KEY = "dateFinExecution";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueCfa.class);

  public EOMangueCfa localInstanceIn(EOEditingContext editingContext) {
    EOMangueCfa localInstance = (EOMangueCfa)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dateFinExecution() {
    return (NSTimestamp) storedValueForKey("dateFinExecution");
  }

  public void setDateFinExecution(NSTimestamp value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating dateFinExecution from " + dateFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFinExecution");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
    	_EOMangueCfa.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCfa.LOG.isDebugEnabled()) {
      _EOMangueCfa.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueCfa createMangueCfa(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCfa eo = (EOMangueCfa) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCfa.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCfa> fetchAllMangueCfas(EOEditingContext editingContext) {
    return _EOMangueCfa.fetchAllMangueCfas(editingContext, null);
  }

  public static NSArray<EOMangueCfa> fetchAllMangueCfas(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCfa.fetchMangueCfas(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCfa> fetchMangueCfas(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCfa.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCfa> eoObjects = (NSArray<EOMangueCfa>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCfa fetchMangueCfa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCfa.fetchMangueCfa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCfa fetchMangueCfa(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCfa> eoObjects = _EOMangueCfa.fetchMangueCfas(editingContext, qualifier, null);
    EOMangueCfa eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCfa)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCfa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCfa fetchRequiredMangueCfa(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCfa.fetchRequiredMangueCfa(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCfa fetchRequiredMangueCfa(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCfa eoObject = _EOMangueCfa.fetchMangueCfa(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCfa that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCfa localInstanceIn(EOEditingContext editingContext, EOMangueCfa eo) {
    EOMangueCfa localInstance = (eo == null) ? null : (EOMangueCfa)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
