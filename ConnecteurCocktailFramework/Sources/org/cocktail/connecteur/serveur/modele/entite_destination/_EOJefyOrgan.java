// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOJefyOrgan.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOJefyOrgan extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "JefyOrgan";

	// Attributes
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ORG_CANAL_OBLIGATOIRE_KEY = "orgCanalObligatoire";
	public static final String ORG_CR_KEY = "orgCr";
	public static final String ORG_DATE_CLOTURE_KEY = "orgDateCloture";
	public static final String ORG_DATE_OUVERTURE_KEY = "orgDateOuverture";
	public static final String ORG_ETAB_KEY = "orgEtab";
	public static final String ORG_ID_KEY = "orgId";
	public static final String ORG_LIB_KEY = "orgLib";
	public static final String ORG_LUCRATIVITE_KEY = "orgLucrativite";
	public static final String ORG_NIV_KEY = "orgNiv";
	public static final String ORG_PERE_KEY = "orgPere";
	public static final String ORG_SOUS_CR_KEY = "orgSousCr";
	public static final String ORG_UB_KEY = "orgUb";
	public static final String ORG_UNIV_KEY = "orgUniv";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TYOR_ID_KEY = "tyorId";

	// Relationships
	public static final String TO_ORGAN_PERE_KEY = "toOrganPere";

  private static Logger LOG = Logger.getLogger(_EOJefyOrgan.class);

  public EOJefyOrgan localInstanceIn(EOEditingContext editingContext) {
    EOJefyOrgan localInstance = (EOJefyOrgan)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer orgCanalObligatoire() {
    return (Integer) storedValueForKey("orgCanalObligatoire");
  }

  public void setOrgCanalObligatoire(Integer value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgCanalObligatoire from " + orgCanalObligatoire() + " to " + value);
    }
    takeStoredValueForKey(value, "orgCanalObligatoire");
  }

  public String orgCr() {
    return (String) storedValueForKey("orgCr");
  }

  public void setOrgCr(String value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgCr from " + orgCr() + " to " + value);
    }
    takeStoredValueForKey(value, "orgCr");
  }

  public NSTimestamp orgDateCloture() {
    return (NSTimestamp) storedValueForKey("orgDateCloture");
  }

  public void setOrgDateCloture(NSTimestamp value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgDateCloture from " + orgDateCloture() + " to " + value);
    }
    takeStoredValueForKey(value, "orgDateCloture");
  }

  public NSTimestamp orgDateOuverture() {
    return (NSTimestamp) storedValueForKey("orgDateOuverture");
  }

  public void setOrgDateOuverture(NSTimestamp value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgDateOuverture from " + orgDateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "orgDateOuverture");
  }

  public String orgEtab() {
    return (String) storedValueForKey("orgEtab");
  }

  public void setOrgEtab(String value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgEtab from " + orgEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "orgEtab");
  }

  public Integer orgId() {
    return (Integer) storedValueForKey("orgId");
  }

  public void setOrgId(Integer value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgId from " + orgId() + " to " + value);
    }
    takeStoredValueForKey(value, "orgId");
  }

  public String orgLib() {
    return (String) storedValueForKey("orgLib");
  }

  public void setOrgLib(String value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgLib from " + orgLib() + " to " + value);
    }
    takeStoredValueForKey(value, "orgLib");
  }

  public Integer orgLucrativite() {
    return (Integer) storedValueForKey("orgLucrativite");
  }

  public void setOrgLucrativite(Integer value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgLucrativite from " + orgLucrativite() + " to " + value);
    }
    takeStoredValueForKey(value, "orgLucrativite");
  }

  public Integer orgNiv() {
    return (Integer) storedValueForKey("orgNiv");
  }

  public void setOrgNiv(Integer value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgNiv from " + orgNiv() + " to " + value);
    }
    takeStoredValueForKey(value, "orgNiv");
  }

  public Integer orgPere() {
    return (Integer) storedValueForKey("orgPere");
  }

  public void setOrgPere(Integer value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgPere from " + orgPere() + " to " + value);
    }
    takeStoredValueForKey(value, "orgPere");
  }

  public String orgSousCr() {
    return (String) storedValueForKey("orgSousCr");
  }

  public void setOrgSousCr(String value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgSousCr from " + orgSousCr() + " to " + value);
    }
    takeStoredValueForKey(value, "orgSousCr");
  }

  public String orgUb() {
    return (String) storedValueForKey("orgUb");
  }

  public void setOrgUb(String value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgUb from " + orgUb() + " to " + value);
    }
    takeStoredValueForKey(value, "orgUb");
  }

  public String orgUniv() {
    return (String) storedValueForKey("orgUniv");
  }

  public void setOrgUniv(String value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating orgUniv from " + orgUniv() + " to " + value);
    }
    takeStoredValueForKey(value, "orgUniv");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public Integer tyorId() {
    return (Integer) storedValueForKey("tyorId");
  }

  public void setTyorId(Integer value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
    	_EOJefyOrgan.LOG.debug( "updating tyorId from " + tyorId() + " to " + value);
    }
    takeStoredValueForKey(value, "tyorId");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan toOrganPere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan)storedValueForKey("toOrganPere");
  }

  public void setToOrganPereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan value) {
    if (_EOJefyOrgan.LOG.isDebugEnabled()) {
      _EOJefyOrgan.LOG.debug("updating toOrganPere from " + toOrganPere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan oldValue = toOrganPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toOrganPere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toOrganPere");
    }
  }
  

  public static EOJefyOrgan createJefyOrgan(EOEditingContext editingContext, NSTimestamp orgDateOuverture
, Integer orgId
, String orgLib
, Integer orgLucrativite
, Integer orgNiv
, String orgUniv
, Integer persIdModification
, Integer tyorId
) {
    EOJefyOrgan eo = (EOJefyOrgan) EOUtilities.createAndInsertInstance(editingContext, _EOJefyOrgan.ENTITY_NAME);    
		eo.setOrgDateOuverture(orgDateOuverture);
		eo.setOrgId(orgId);
		eo.setOrgLib(orgLib);
		eo.setOrgLucrativite(orgLucrativite);
		eo.setOrgNiv(orgNiv);
		eo.setOrgUniv(orgUniv);
		eo.setPersIdModification(persIdModification);
		eo.setTyorId(tyorId);
    return eo;
  }

  public static NSArray<EOJefyOrgan> fetchAllJefyOrgans(EOEditingContext editingContext) {
    return _EOJefyOrgan.fetchAllJefyOrgans(editingContext, null);
  }

  public static NSArray<EOJefyOrgan> fetchAllJefyOrgans(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOJefyOrgan.fetchJefyOrgans(editingContext, null, sortOrderings);
  }

  public static NSArray<EOJefyOrgan> fetchJefyOrgans(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOJefyOrgan.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOJefyOrgan> eoObjects = (NSArray<EOJefyOrgan>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOJefyOrgan fetchJefyOrgan(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJefyOrgan.fetchJefyOrgan(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJefyOrgan fetchJefyOrgan(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOJefyOrgan> eoObjects = _EOJefyOrgan.fetchJefyOrgans(editingContext, qualifier, null);
    EOJefyOrgan eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOJefyOrgan)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one JefyOrgan that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJefyOrgan fetchRequiredJefyOrgan(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJefyOrgan.fetchRequiredJefyOrgan(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJefyOrgan fetchRequiredJefyOrgan(EOEditingContext editingContext, EOQualifier qualifier) {
    EOJefyOrgan eoObject = _EOJefyOrgan.fetchJefyOrgan(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no JefyOrgan that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJefyOrgan localInstanceIn(EOEditingContext editingContext, EOJefyOrgan eo) {
    EOJefyOrgan localInstance = (eo == null) ? null : (EOJefyOrgan)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
