//EOMangueContratAvenant.java
//Created on Mon Jan 28 13:52:36 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOContrat;
import org.cocktail.connecteur.serveur.modele.entite_import.EOContratAvenant;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// 15/03/2011 - ajout des données des services validés
public class EOMangueContratAvenant extends _EOMangueContratAvenant implements InterfaceRecordAvecAutresAttributs {
	private String cSectionCnu, cSousSectionCnu;
	private boolean cnuPreparee = false;
	
	
	public EOMangueContratAvenant() {
		super();
	}

	public String cSectionCnu() {
		if (!cnuPreparee) {
			preparerCnu();
		}
		return cSectionCnu;
	}

	public void setCSectionCnu(String value) {
		this.cSectionCnu = value;
		this.cSousSectionCnu = null;
	}

	public String cSousSectionCnu() {
		if (!cnuPreparee) {
			preparerCnu();
		}
		return cSousSectionCnu;
	}

	public void setCSousSectionCnu(String value) {
		this.cSousSectionCnu = value;
	}

	// Méthodes ajoutées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOContratAvenant avenant = (EOContratAvenant)recordImport;
		setTemAnnulation(CocktailConstantes.FAUX);
		setCtraTypeTemps("C");
		// A ce stade, le contrat est dans le SI Destinataire
		EOMangueContrat contratMangue = ((EOContratCorresp)avenant.contrat().correspondance()).contratMangue();
		addObjectToBothSidesOfRelationshipWithKey(contratMangue, "contrat");
	}
	/** return true si le contrat  n'est pas annule */
	public boolean estValide() {
		return temAnnulation() != null && temAnnulation().equals(CocktailConstantes.FAUX);
	}
	/** Surcharge pour gerer le temoin d'annulation */
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemAnnulation(CocktailConstantes.FAUX);
		} else {
			setTemAnnulation(CocktailConstantes.VRAI);
		}
	}
	// Méthodes privées
	private void preparerCnu() {
		if (!cnuPreparee) {
			if (noCnu() != null) {
				EOCnu cnu = EOCnu.rechercherCnuPourNoCnu(editingContext(), noCnu());
				if (cnu != null) {
					cSectionCnu = cnu.cSectionCnu();
					cSousSectionCnu = cnu.cSousSectionCnu();
				}
			}
			cnuPreparee = true;
		}
	}
	// InterfaceRecordAvecAutresAttributs
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport recordImport) {
		EOContratAvenant avenant = (EOContratAvenant)recordImport;
		EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext(), avenant.cSectionCnu(), avenant.cSousSectionCnu());
		if (cnu != null) {
			setNoCnu(cnu.noCnu());
		} else {
			setNoCnu(null);
		}
		return true;
	}
	//	 Méthodes statiques
	/** Retourne l'avenant de contrat equivalent pour ce contrat et qui commence a la m&ecirc;me date, on
	 * retourne null si non trouve.<BR>
	 * @param editingContext
	 * @param contrat
	 * @return EOMangueContratAvenant
	 */
	public static EOMangueContratAvenant avenantDestinationPourAvenant(EOEditingContext editingContext,EOContratAvenant avenant) {
		NSArray results = rechercherAvenantsManguePourAvenant(editingContext,avenant);
		// Si il y en a un, il est unique car pas de chevauchement des avenants
		if (results != null && results.count() == 1) {
			return (EOMangueContratAvenant)results.objectAtIndex(0);
		} else {
			return null;
		}

	}
	/** retourne tous les avenants de contrat non annules, de remuneration principale 
	 * d'un individu dont les dates de debut et fin sont a cheval sur deux dates dans le SI
	 * Destinataire
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode peut &ecirc;tre nulle
	 * @return avenants trouv&eacutes;
	 */
	public static NSArray rechercherAvenantsRemunerationPrincipalePourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		// Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray args = new NSMutableArray(individuGrhum);
			String stringQualifier = "temAnnulation <> 'O' AND contrat.individu = %@ AND contrat.temAnnulation <> 'O'";
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
			qualifiers.addObject(qualifier);
			qualifier = Finder.qualifierPourPeriode("dDebContratAv",debutPeriode,"dFinContratAv",finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
			EOFetchSpecification myFetch = new EOFetchSpecification("MangueContratAvenant",qualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			// Rechercher les contrats de rémunération principale
			NSMutableArray avenantsValides = new NSMutableArray();
			java.util.Enumeration e = editingContext.objectsWithFetchSpecification(myFetch).objectEnumerator();
			while (e.hasMoreElements()) {
				EOMangueContratAvenant avenant = (EOMangueContratAvenant)e.nextElement();
				String cTypeContrat = avenant.contrat().cTypeContratTrav();
				EOTypeContratTravail typeContrat = (EOTypeContratTravail)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeContratTravail", "cTypeContratTrav", cTypeContrat);
				if (typeContrat != null && typeContrat.estRemunerationPrincipale()) {
					avenantsValides.addObject(avenant);
				}
			}
			return avenantsValides;
		} else {
			return null;	// individu pas encore créé
		}
	}
	/** retourne tous les avenants de contrat non annules, de remuneration principale 
	 * d'un individu dont les dates de debut et fin sont a cheval sur deux dates dans le SI
	 * Destinataire
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode peut &ecirc;tre nulle
	 * @return avenants trouv&eacutes;
	 */
	public static NSArray rechercherAvenantsPourIndividuEtPeriode(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(individu);
		String stringQualifier = "temAnnulation <> 'O' AND contrat.individu = %@ AND contrat.temAnnulation <> 'O'";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		qualifier = Finder.qualifierPourPeriode("dDebContratAv",debutPeriode,"dFinContratAv",finPeriode);
		qualifiers.addObject(qualifier);
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("MangueContratAvenant",qualifier,null);
		return editingContext.objectsWithFetchSpecification(myFetch);	
	}
	/** retourne tous les avenants d'un contrat non annules,dont les dates de debut et fin 
	 * sont a cheval sur deux dates dans le SI
	 * Destinataire
	 * @param editingContext
	 * @param contrat
	 * @param debutPeriode
	 * @param finPeriode peut &ecirc;tre nulle
	 * @return avenants trouv&eacutes;
	 */
	public static NSArray rechercherAvenantsNonAnnulesPourContratEtPeriode(EOEditingContext editingContext,EOContrat contrat,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		EOMangueContrat contratMangue = EOMangueContrat.contratDestinationPourContrat(editingContext, contrat);
		if (contratMangue != null) {
			NSMutableArray args = new NSMutableArray(contratMangue);
			String stringQualifier = "temAnnulation <> 'O' AND contrat = %@ AND contrat.temAnnulation <> 'O'";
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
			qualifiers.addObject(qualifier);
			qualifier = Finder.qualifierPourPeriode("dDebContratAv",debutPeriode,"dFinContratAv",finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
			EOFetchSpecification myFetch = new EOFetchSpecification("MangueContratAvenant",qualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else {
			return null;	// contrat pas encore créé
		}
	}
	/** retourne tous les avenants d'un contrat non annules
	 * Destinataire
	 * @param editingContext
	 * @param contrat
	 * @param debutPeriode
	 * @param finPeriode peut &ecirc;tre nulle
	 * @return avenants trouv&eacutes;
	 */
	public static NSArray rechercherAvenantsNonAnnulesPourContrat(EOEditingContext editingContext,EOMangueContrat contrat) {
		NSMutableArray args = new NSMutableArray(contrat);
		String stringQualifier = "temAnnulation <> 'O' AND contrat = %@ AND contrat.temAnnulation <> 'O'";
		EOFetchSpecification myFetch = new EOFetchSpecification("MangueContratAvenant",EOQualifier.qualifierWithQualifierFormat(stringQualifier,args),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	
	// Méthodes privées
	private  static NSArray rechercherAvenantsManguePourAvenant(EOEditingContext editingContext,EOContratAvenant avenant) {
		// Vérifier si il existe un contrat équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car le contrat n'a peut-être pas encore été créé
		// On ne recherche que les avenants valides
		EOMangueContrat contratMangue = (EOMangueContrat)avenant.contrat().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (contratMangue != null) {
			NSMutableArray args = new NSMutableArray(contratMangue);
			// Vérifier que la date de début d'avenant est la même : pour éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(avenant.dDebContratAv()));
			args.addObject(DateCtrl.jourSuivant(avenant.dDebContratAv()));
			String qualifier = "contrat = %@ AND dDebContratAv > %@ AND dDebContratAv < %@ AND temAnnulation <> 'O'";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification ("MangueContratAvenant", myQualifier,null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else {
			return null;
		}

	}
}
