// EOMangueIndividuFamiliale.java
// Created on Tue Mar 25 11:13:42 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFamiliale;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;

public class EOMangueIndividuFamiliale extends _EOMangueIndividuFamiliale {

    public EOMangueIndividuFamiliale() {
        super();
    }

    // Méthodes ajoutées
	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), ((EOIndividuFamiliale)recordImport).idSource());
		if (individu != null) {	// Ne devrait pas car on crée le individus avant
			addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + ((EOIndividuFamiliale)recordImport).idSource());
		}	
	}
	// Méthodes statiques
	/** Retourne la situation familiale pour cet individu, qui est de m&circ;me type ou commence le m&circ;me jour<BR>
	 * @param editingContext
	 * @param affectation
	 * @return EOMangueAffectation
	 */
	public static EOMangueIndividuFamiliale individuFamilialeDestinationPourIndividuFamiliale(EOEditingContext editingContext,EOIndividuFamiliale individuFamiliale) {
		// Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individuFamiliale.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray args = new NSMutableArray(individuGrhum);
			// Vérifier que type est le même
			args.addObject(individuFamiliale.cSituationFamille());
			args.addObject(DateCtrl.jourSuivant(individuFamiliale.dDebSitFamiliale()));
			String qualifier = "individu = %@ AND cSituationFamille = %@ AND temValide = 'O'";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification ("MangueIndividuFamiliale", myQualifier,null);
			try {
				return (EOMangueIndividuFamiliale)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
			} catch (Exception e) {
				args = new NSMutableArray(individuGrhum);
				// Vérifier que la date de début est la même : pour éviter les problèmes de comparaison
				// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
				args.addObject(DateCtrl.jourPrecedent(individuFamiliale.dDebSitFamiliale()));
				args.addObject(DateCtrl.jourSuivant(individuFamiliale.dDebSitFamiliale()));
				qualifier = "individu = %@ AND temValide = 'O' AND dDebSitFamiliale > %@ AND dDebSitFamiliale < %@";
				if (individuFamiliale.dFinSitFamiliale() == null) {
					qualifier += " AND dFinSitFamiliale= NIL";
				} else {
					args.addObject(DateCtrl.jourPrecedent(individuFamiliale.dFinSitFamiliale()));
					args.addObject(DateCtrl.jourSuivant(individuFamiliale.dFinSitFamiliale()));
					qualifier += " AND dFinSitFamiliale > %@ AND dFinSitFamiliale < %@";
				}
				myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
				myFetch = new EOFetchSpecification ("MangueIndividuFamiliale", myQualifier,null);
				try {
					return (EOMangueIndividuFamiliale)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
				} catch (Exception exc) {
					return null;
				}
			}
		}
		return null;
	}

}
