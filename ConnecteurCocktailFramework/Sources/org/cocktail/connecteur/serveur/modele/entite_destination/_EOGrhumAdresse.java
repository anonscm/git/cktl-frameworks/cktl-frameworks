// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumAdresse.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumAdresse extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumAdresse";

	// Attributes
	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_BP_KEY = "adrBp";
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String BIS_TER_KEY = "bisTer";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String C_PAYS_KEY = "cPays";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String C_VOIE_KEY = "cVoie";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HABITANT_CHEZ_KEY = "habitantChez";
	public static final String LOCALITE_KEY = "localite";
	public static final String NOM_VOIE_KEY = "nomVoie";
	public static final String NO_VOIE_KEY = "noVoie";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";
	public static final String VILLE_KEY = "ville";

	// Relationships
	public static final String TO_TYPE_VOIE_KEY = "toTypeVoie";

  private static Logger LOG = Logger.getLogger(_EOGrhumAdresse.class);

  public EOGrhumAdresse localInstanceIn(EOEditingContext editingContext) {
    EOGrhumAdresse localInstance = (EOGrhumAdresse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String adrAdresse1() {
    return (String) storedValueForKey("adrAdresse1");
  }

  public void setAdrAdresse1(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating adrAdresse1 from " + adrAdresse1() + " to " + value);
    }
    takeStoredValueForKey(value, "adrAdresse1");
  }

  public String adrAdresse2() {
    return (String) storedValueForKey("adrAdresse2");
  }

  public void setAdrAdresse2(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating adrAdresse2 from " + adrAdresse2() + " to " + value);
    }
    takeStoredValueForKey(value, "adrAdresse2");
  }

  public String adrBp() {
    return (String) storedValueForKey("adrBp");
  }

  public void setAdrBp(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating adrBp from " + adrBp() + " to " + value);
    }
    takeStoredValueForKey(value, "adrBp");
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey("adrOrdre");
  }

  public void setAdrOrdre(Integer value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating adrOrdre from " + adrOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "adrOrdre");
  }

  public String bisTer() {
    return (String) storedValueForKey("bisTer");
  }

  public void setBisTer(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating bisTer from " + bisTer() + " to " + value);
    }
    takeStoredValueForKey(value, "bisTer");
  }

  public String codePostal() {
    return (String) storedValueForKey("codePostal");
  }

  public void setCodePostal(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating codePostal from " + codePostal() + " to " + value);
    }
    takeStoredValueForKey(value, "codePostal");
  }

  public String cPays() {
    return (String) storedValueForKey("cPays");
  }

  public void setCPays(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating cPays from " + cPays() + " to " + value);
    }
    takeStoredValueForKey(value, "cPays");
  }

  public String cpEtranger() {
    return (String) storedValueForKey("cpEtranger");
  }

  public void setCpEtranger(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating cpEtranger from " + cpEtranger() + " to " + value);
    }
    takeStoredValueForKey(value, "cpEtranger");
  }

  public String cVoie() {
    return (String) storedValueForKey("cVoie");
  }

  public void setCVoie(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating cVoie from " + cVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "cVoie");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String habitantChez() {
    return (String) storedValueForKey("habitantChez");
  }

  public void setHabitantChez(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating habitantChez from " + habitantChez() + " to " + value);
    }
    takeStoredValueForKey(value, "habitantChez");
  }

  public String localite() {
    return (String) storedValueForKey("localite");
  }

  public void setLocalite(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating localite from " + localite() + " to " + value);
    }
    takeStoredValueForKey(value, "localite");
  }

  public String nomVoie() {
    return (String) storedValueForKey("nomVoie");
  }

  public void setNomVoie(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating nomVoie from " + nomVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "nomVoie");
  }

  public String noVoie() {
    return (String) storedValueForKey("noVoie");
  }

  public void setNoVoie(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating noVoie from " + noVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "noVoie");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String temPayeUtil() {
    return (String) storedValueForKey("temPayeUtil");
  }

  public void setTemPayeUtil(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating temPayeUtil from " + temPayeUtil() + " to " + value);
    }
    takeStoredValueForKey(value, "temPayeUtil");
  }

  public String ville() {
    return (String) storedValueForKey("ville");
  }

  public void setVille(String value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
    	_EOGrhumAdresse.LOG.debug( "updating ville from " + ville() + " to " + value);
    }
    takeStoredValueForKey(value, "ville");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie toTypeVoie() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie)storedValueForKey("toTypeVoie");
  }

  public void setToTypeVoieRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie value) {
    if (_EOGrhumAdresse.LOG.isDebugEnabled()) {
      _EOGrhumAdresse.LOG.debug("updating toTypeVoie from " + toTypeVoie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeVoie oldValue = toTypeVoie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeVoie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeVoie");
    }
  }
  

  public static EOGrhumAdresse createGrhumAdresse(EOEditingContext editingContext, Integer adrOrdre
, String cPays
, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOGrhumAdresse eo = (EOGrhumAdresse) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumAdresse.ENTITY_NAME);    
		eo.setAdrOrdre(adrOrdre);
		eo.setCPays(cPays);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOGrhumAdresse> fetchAllGrhumAdresses(EOEditingContext editingContext) {
    return _EOGrhumAdresse.fetchAllGrhumAdresses(editingContext, null);
  }

  public static NSArray<EOGrhumAdresse> fetchAllGrhumAdresses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumAdresse.fetchGrhumAdresses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumAdresse> fetchGrhumAdresses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumAdresse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumAdresse> eoObjects = (NSArray<EOGrhumAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumAdresse fetchGrhumAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumAdresse.fetchGrhumAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumAdresse fetchGrhumAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumAdresse> eoObjects = _EOGrhumAdresse.fetchGrhumAdresses(editingContext, qualifier, null);
    EOGrhumAdresse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumAdresse)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumAdresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumAdresse fetchRequiredGrhumAdresse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumAdresse.fetchRequiredGrhumAdresse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumAdresse fetchRequiredGrhumAdresse(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumAdresse eoObject = _EOGrhumAdresse.fetchGrhumAdresse(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumAdresse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumAdresse localInstanceIn(EOEditingContext editingContext, EOGrhumAdresse eo) {
    EOGrhumAdresse localInstance = (eo == null) ? null : (EOGrhumAdresse)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
