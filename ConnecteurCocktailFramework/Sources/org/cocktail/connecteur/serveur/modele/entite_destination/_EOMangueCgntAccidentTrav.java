// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCgntAccidentTrav.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCgntAccidentTrav extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueCgntAccidentTrav";

	// Attributes
	public static final String C_ANCIENNETE_KEY = "cAnciennete";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ACCIDENT_KEY = "dateAccident";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_PLEIN_TRAIT_KEY = "dDebPleinTrait";
	public static final String D_FIN_PLEIN_TRAIT_KEY = "dFinPleinTrait";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String DECLARATION_KEY = "declaration";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueCgntAccidentTrav.class);

  public EOMangueCgntAccidentTrav localInstanceIn(EOEditingContext editingContext) {
    EOMangueCgntAccidentTrav localInstance = (EOMangueCgntAccidentTrav)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cAnciennete() {
    return (String) storedValueForKey("cAnciennete");
  }

  public void setCAnciennete(String value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating cAnciennete from " + cAnciennete() + " to " + value);
    }
    takeStoredValueForKey(value, "cAnciennete");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateAccident() {
    return (NSTimestamp) storedValueForKey("dateAccident");
  }

  public void setDateAccident(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dateAccident from " + dateAccident() + " to " + value);
    }
    takeStoredValueForKey(value, "dateAccident");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebPleinTrait() {
    return (NSTimestamp) storedValueForKey("dDebPleinTrait");
  }

  public void setDDebPleinTrait(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dDebPleinTrait from " + dDebPleinTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebPleinTrait");
  }

  public NSTimestamp dFinPleinTrait() {
    return (NSTimestamp) storedValueForKey("dFinPleinTrait");
  }

  public void setDFinPleinTrait(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dFinPleinTrait from " + dFinPleinTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinPleinTrait");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
    	_EOMangueCgntAccidentTrav.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident declaration() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident)storedValueForKey("declaration");
  }

  public void setDeclarationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
      _EOMangueCgntAccidentTrav.LOG.debug("updating declaration from " + declaration() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident oldValue = declaration();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "declaration");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "declaration");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCgntAccidentTrav.LOG.isDebugEnabled()) {
      _EOMangueCgntAccidentTrav.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueCgntAccidentTrav createMangueCgntAccidentTrav(EOEditingContext editingContext, String cAnciennete
, NSTimestamp dateAccident
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
) {
    EOMangueCgntAccidentTrav eo = (EOMangueCgntAccidentTrav) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCgntAccidentTrav.ENTITY_NAME);    
		eo.setCAnciennete(cAnciennete);
		eo.setDateAccident(dateAccident);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueCgntAccidentTrav> fetchAllMangueCgntAccidentTravs(EOEditingContext editingContext) {
    return _EOMangueCgntAccidentTrav.fetchAllMangueCgntAccidentTravs(editingContext, null);
  }

  public static NSArray<EOMangueCgntAccidentTrav> fetchAllMangueCgntAccidentTravs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCgntAccidentTrav.fetchMangueCgntAccidentTravs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCgntAccidentTrav> fetchMangueCgntAccidentTravs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCgntAccidentTrav.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCgntAccidentTrav> eoObjects = (NSArray<EOMangueCgntAccidentTrav>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCgntAccidentTrav fetchMangueCgntAccidentTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCgntAccidentTrav.fetchMangueCgntAccidentTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCgntAccidentTrav fetchMangueCgntAccidentTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCgntAccidentTrav> eoObjects = _EOMangueCgntAccidentTrav.fetchMangueCgntAccidentTravs(editingContext, qualifier, null);
    EOMangueCgntAccidentTrav eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCgntAccidentTrav)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCgntAccidentTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCgntAccidentTrav fetchRequiredMangueCgntAccidentTrav(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCgntAccidentTrav.fetchRequiredMangueCgntAccidentTrav(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCgntAccidentTrav fetchRequiredMangueCgntAccidentTrav(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCgntAccidentTrav eoObject = _EOMangueCgntAccidentTrav.fetchMangueCgntAccidentTrav(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCgntAccidentTrav that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCgntAccidentTrav localInstanceIn(EOEditingContext editingContext, EOMangueCgntAccidentTrav eo) {
    EOMangueCgntAccidentTrav localInstance = (eo == null) ? null : (EOMangueCgntAccidentTrav)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
