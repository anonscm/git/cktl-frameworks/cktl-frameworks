// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueIndividuDistinctions.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueIndividuDistinctions extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueIndividuDistinctions";

	// Attributes
	public static final String C_DISTINCTION_KEY = "cDistinction";
	public static final String C_DISTINCTION_NIVEAU_KEY = "cDistinctionNiveau";
	public static final String DATE_DISTINCTION_KEY = "dateDistinction";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_DISTINCTION_KEY = "toDistinction";
	public static final String TO_DISTINCTION_NIVEAU_KEY = "toDistinctionNiveau";

  private static Logger LOG = Logger.getLogger(_EOMangueIndividuDistinctions.class);

  public EOMangueIndividuDistinctions localInstanceIn(EOEditingContext editingContext) {
    EOMangueIndividuDistinctions localInstance = (EOMangueIndividuDistinctions)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDistinction() {
    return (String) storedValueForKey("cDistinction");
  }

  public void setCDistinction(String value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDistinctions.LOG.debug( "updating cDistinction from " + cDistinction() + " to " + value);
    }
    takeStoredValueForKey(value, "cDistinction");
  }

  public Integer cDistinctionNiveau() {
    return (Integer) storedValueForKey("cDistinctionNiveau");
  }

  public void setCDistinctionNiveau(Integer value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDistinctions.LOG.debug( "updating cDistinctionNiveau from " + cDistinctionNiveau() + " to " + value);
    }
    takeStoredValueForKey(value, "cDistinctionNiveau");
  }

  public NSTimestamp dateDistinction() {
    return (NSTimestamp) storedValueForKey("dateDistinction");
  }

  public void setDateDistinction(NSTimestamp value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDistinctions.LOG.debug( "updating dateDistinction from " + dateDistinction() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDistinction");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDistinctions.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDistinctions.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
      _EOMangueIndividuDistinctions.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction toDistinction() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction)storedValueForKey("toDistinction");
  }

  public void setToDistinctionRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
      _EOMangueIndividuDistinctions.LOG.debug("updating toDistinction from " + toDistinction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinction oldValue = toDistinction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDistinction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDistinction");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau toDistinctionNiveau() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau)storedValueForKey("toDistinctionNiveau");
  }

  public void setToDistinctionNiveauRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau value) {
    if (_EOMangueIndividuDistinctions.LOG.isDebugEnabled()) {
      _EOMangueIndividuDistinctions.LOG.debug("updating toDistinctionNiveau from " + toDistinctionNiveau() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EODistinctionNiveau oldValue = toDistinctionNiveau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDistinctionNiveau");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDistinctionNiveau");
    }
  }
  

  public static EOMangueIndividuDistinctions createMangueIndividuDistinctions(EOEditingContext editingContext, NSTimestamp dateDistinction
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueIndividuDistinctions eo = (EOMangueIndividuDistinctions) EOUtilities.createAndInsertInstance(editingContext, _EOMangueIndividuDistinctions.ENTITY_NAME);    
		eo.setDateDistinction(dateDistinction);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueIndividuDistinctions> fetchAllMangueIndividuDistinctionses(EOEditingContext editingContext) {
    return _EOMangueIndividuDistinctions.fetchAllMangueIndividuDistinctionses(editingContext, null);
  }

  public static NSArray<EOMangueIndividuDistinctions> fetchAllMangueIndividuDistinctionses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueIndividuDistinctions.fetchMangueIndividuDistinctionses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueIndividuDistinctions> fetchMangueIndividuDistinctionses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueIndividuDistinctions.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueIndividuDistinctions> eoObjects = (NSArray<EOMangueIndividuDistinctions>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueIndividuDistinctions fetchMangueIndividuDistinctions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueIndividuDistinctions.fetchMangueIndividuDistinctions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueIndividuDistinctions fetchMangueIndividuDistinctions(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueIndividuDistinctions> eoObjects = _EOMangueIndividuDistinctions.fetchMangueIndividuDistinctionses(editingContext, qualifier, null);
    EOMangueIndividuDistinctions eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueIndividuDistinctions)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueIndividuDistinctions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueIndividuDistinctions fetchRequiredMangueIndividuDistinctions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueIndividuDistinctions.fetchRequiredMangueIndividuDistinctions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueIndividuDistinctions fetchRequiredMangueIndividuDistinctions(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueIndividuDistinctions eoObject = _EOMangueIndividuDistinctions.fetchMangueIndividuDistinctions(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueIndividuDistinctions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueIndividuDistinctions localInstanceIn(EOEditingContext editingContext, EOMangueIndividuDistinctions eo) {
    EOMangueIndividuDistinctions localInstance = (eo == null) ? null : (EOMangueIndividuDistinctions)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
