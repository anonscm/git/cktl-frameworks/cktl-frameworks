// EOPasse.java
// Created on Wed Nov 30 16:41:45 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPasse;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * R&egrave;gles de validation :<BR>
 * 	Verification des longueurs de strings<BR>
 * 	La date de debut, la date de fin et l'etablissement doivent &ecirc;tre fournis<BR>
 *	La date debut doit &ecirc;tre anterieure a celle de fin<BR> 
 *	Le nombre d'annees, mois et jours de duree validee n'est pas superieur au nombre d'annees, mois et jours entre la date de debut et celle de fin
 *	Si la duree de services valides est fournie, la date de validation et la quotite de services doivent &circ;tre fournis
 **/
// 29/09/2011 - modifier les fetchs pour temValide lorsque cela sera implémenté dans Mangue
public class EOManguePasse extends _EOManguePasse {

	public static final EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray<EOSortOrdering> SORT_ARRAY_DATE_DESC = new NSArray<EOSortOrdering>(SORT_DATE_DESC);
	
	public EOManguePasse() {
		super();
	}

	/**
	 * 
	 */
	public void initialiserObjet(ObjetImport recordImport) throws Exception {
				
		setTemTitulaire(CocktailConstantes.FAUX);
		setSecteurPasse(CocktailConstantes.FAUX);
		setPasPcAcquitee("N");
		super.initialiserObjet(recordImport);
	}

	// Méthodes statiques
	/** Retourne la liste des passes hors EN non-titulaires concernant la fonction publique pour la periode
	 *  tries par ordre de date croissante 
	 * @param debutPeriode peut etre nul
	 */
	public static NSArray rechercherPassesDestinatairesPourContractuelEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ AND secteurPasse = 'O' AND temTitulaire = 'N'", new NSArray(individuGrhum)));
			if (debutPeriode != null) {
				qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
			}
			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers), new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
			return editingContext.objectsWithFetchSpecification(fs);
		} else {
			return null;
		}
	}

	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
}
