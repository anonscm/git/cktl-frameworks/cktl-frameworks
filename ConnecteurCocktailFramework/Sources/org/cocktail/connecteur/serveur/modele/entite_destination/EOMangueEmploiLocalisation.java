package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEmploiCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEmploiLocalisation;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;


public class EOMangueEmploiLocalisation extends _EOMangueEmploiLocalisation {

	@Override
	public void setEstValide(boolean aBool) {
		// Surcharge la méthode setEstValide et ne fait rien car l'objet Mangue Emploi Localisation n'a pas TemValide
	}

	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOEmploiLocalisation localisation =(EOEmploiLocalisation) recordImport;
		
		EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
		setPersIdCreation(utilisateur.persId());
		setPersIdModification(utilisateur.persId());

		EOEmploiCorresp emploiCorresp = (EOEmploiCorresp) ObjetCorresp.rechercherObjetCorrespPourRecordImport(localisation.editingContext(),
				localisation.emploi(), true);
		if (emploiCorresp != null) {
			setToMangueEmploiRelationship(emploiCorresp.emploiMangue());
		} else {
			throw new Exception("Pas d'emploi dans le SI correspondant a l'emploi " + localisation.emploi().empSource());
		}

		EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), localisation.strSource());
		if (structure != null) { 
			setToStructureRelationship(structure);
		} else {
			throw new Exception("Pas de structure dans le SI correspondant a la structure " + localisation.strSource());
		}
	}
}
