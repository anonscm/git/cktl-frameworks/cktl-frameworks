package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOHeberge;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueContratHeberges extends _EOMangueContratHeberges {

	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		
		EOHeberge herbergeImport=(EOHeberge)recordImport;
		
		EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), herbergeImport.strSource());
		setStructureRelationship(structure);

		EOGrhumStructure structureOrigine = EOStructureCorresp.structureGrhum(editingContext(), herbergeImport.strOrigineSource());
		setStructureOrigineRelationship(structureOrigine);
	}

}
