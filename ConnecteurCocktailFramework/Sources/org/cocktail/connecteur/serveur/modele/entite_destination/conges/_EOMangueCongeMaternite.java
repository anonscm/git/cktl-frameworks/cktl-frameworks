// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongeMaternite.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongeMaternite extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCongeMaternite";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_TYPE_CG_MATERN_KEY = "cTypeCgMatern";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CG_SANS_TRAIT_KEY = "temCgSansTrait";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String DECLARATION_MATERNITE_KEY = "declarationMaternite";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCongeMaternite.class);

  public EOMangueCongeMaternite localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongeMaternite localInstance = (EOMangueCongeMaternite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cTypeCgMatern() {
    return (String) storedValueForKey("cTypeCgMatern");
  }

  public void setCTypeCgMatern(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating cTypeCgMatern from " + cTypeCgMatern() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeCgMatern");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temCgSansTrait() {
    return (String) storedValueForKey("temCgSansTrait");
  }

  public void setTemCgSansTrait(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating temCgSansTrait from " + temCgSansTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgSansTrait");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaternite.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.mangue.EOMangueDeclarationMaternite declarationMaternite() {
    return (org.cocktail.connecteur.serveur.modele.mangue.EOMangueDeclarationMaternite)storedValueForKey("declarationMaternite");
  }

  public void setDeclarationMaterniteRelationship(org.cocktail.connecteur.serveur.modele.mangue.EOMangueDeclarationMaternite value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
      _EOMangueCongeMaternite.LOG.debug("updating declarationMaternite from " + declarationMaternite() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.mangue.EOMangueDeclarationMaternite oldValue = declarationMaternite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "declarationMaternite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "declarationMaternite");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
      _EOMangueCongeMaternite.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite value) {
    if (_EOMangueCongeMaternite.LOG.isDebugEnabled()) {
      _EOMangueCongeMaternite.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaternite oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCongeMaternite createMangueCongeMaternite(EOEditingContext editingContext, String cTypeCgMatern
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temCgSansTrait
, String temConfirme
, String temGestEtab
, String temValide
, org.cocktail.connecteur.serveur.modele.mangue.EOMangueDeclarationMaternite declarationMaternite, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCongeMaternite eo = (EOMangueCongeMaternite) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongeMaternite.ENTITY_NAME);    
		eo.setCTypeCgMatern(cTypeCgMatern);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemCgSansTrait(temCgSansTrait);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    eo.setDeclarationMaterniteRelationship(declarationMaternite);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCongeMaternite> fetchAllMangueCongeMaternites(EOEditingContext editingContext) {
    return _EOMangueCongeMaternite.fetchAllMangueCongeMaternites(editingContext, null);
  }

  public static NSArray<EOMangueCongeMaternite> fetchAllMangueCongeMaternites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongeMaternite.fetchMangueCongeMaternites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongeMaternite> fetchMangueCongeMaternites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongeMaternite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongeMaternite> eoObjects = (NSArray<EOMangueCongeMaternite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongeMaternite fetchMangueCongeMaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeMaternite.fetchMangueCongeMaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeMaternite fetchMangueCongeMaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongeMaternite> eoObjects = _EOMangueCongeMaternite.fetchMangueCongeMaternites(editingContext, qualifier, null);
    EOMangueCongeMaternite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongeMaternite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongeMaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeMaternite fetchRequiredMangueCongeMaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeMaternite.fetchRequiredMangueCongeMaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeMaternite fetchRequiredMangueCongeMaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongeMaternite eoObject = _EOMangueCongeMaternite.fetchMangueCongeMaternite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongeMaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeMaternite localInstanceIn(EOEditingContext editingContext, EOMangueCongeMaternite eo) {
    EOMangueCongeMaternite localInstance = (eo == null) ? null : (EOMangueCongeMaternite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
