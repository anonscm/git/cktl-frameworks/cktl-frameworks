// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueRepartLesions.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueRepartLesions extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueRepartLesions";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String MANGUE_DECLARATION_ACCIDENT_KEY = "mangueDeclarationAccident";

  private static Logger LOG = Logger.getLogger(_EOMangueRepartLesions.class);

  public EOMangueRepartLesions localInstanceIn(EOEditingContext editingContext) {
    EOMangueRepartLesions localInstance = (EOMangueRepartLesions)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueRepartLesions.LOG.isDebugEnabled()) {
    	_EOMangueRepartLesions.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueRepartLesions.LOG.isDebugEnabled()) {
    	_EOMangueRepartLesions.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident mangueDeclarationAccident() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident)storedValueForKey("mangueDeclarationAccident");
  }

  public void setMangueDeclarationAccidentRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident value) {
    if (_EOMangueRepartLesions.LOG.isDebugEnabled()) {
      _EOMangueRepartLesions.LOG.debug("updating mangueDeclarationAccident from " + mangueDeclarationAccident() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident oldValue = mangueDeclarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueDeclarationAccident");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueDeclarationAccident");
    }
  }
  

  public static EOMangueRepartLesions createMangueRepartLesions(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident mangueDeclarationAccident) {
    EOMangueRepartLesions eo = (EOMangueRepartLesions) EOUtilities.createAndInsertInstance(editingContext, _EOMangueRepartLesions.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setMangueDeclarationAccidentRelationship(mangueDeclarationAccident);
    return eo;
  }

  public static NSArray<EOMangueRepartLesions> fetchAllMangueRepartLesionses(EOEditingContext editingContext) {
    return _EOMangueRepartLesions.fetchAllMangueRepartLesionses(editingContext, null);
  }

  public static NSArray<EOMangueRepartLesions> fetchAllMangueRepartLesionses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueRepartLesions.fetchMangueRepartLesionses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueRepartLesions> fetchMangueRepartLesionses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueRepartLesions.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueRepartLesions> eoObjects = (NSArray<EOMangueRepartLesions>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueRepartLesions fetchMangueRepartLesions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueRepartLesions.fetchMangueRepartLesions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueRepartLesions fetchMangueRepartLesions(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueRepartLesions> eoObjects = _EOMangueRepartLesions.fetchMangueRepartLesionses(editingContext, qualifier, null);
    EOMangueRepartLesions eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueRepartLesions)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueRepartLesions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueRepartLesions fetchRequiredMangueRepartLesions(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueRepartLesions.fetchRequiredMangueRepartLesions(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueRepartLesions fetchRequiredMangueRepartLesions(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueRepartLesions eoObject = _EOMangueRepartLesions.fetchMangueRepartLesions(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueRepartLesions that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueRepartLesions localInstanceIn(EOEditingContext editingContext, EOMangueRepartLesions eo) {
    EOMangueRepartLesions localInstance = (eo == null) ? null : (EOMangueRepartLesions)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
