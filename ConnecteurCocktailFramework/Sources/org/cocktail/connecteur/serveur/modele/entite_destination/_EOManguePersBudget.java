// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOManguePersBudget.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOManguePersBudget extends ObjetPourSIDestinataire {
	public static final String ENTITY_NAME = "ManguePersBudget";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String MOIS_CODE_DEBUT_KEY = "moisCodeDebut";
	public static final String MOIS_CODE_FIN_KEY = "moisCodeFin";
	public static final String POURCENTAGE_KEY = "pourcentage";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String TO_EMPLOI_KEY = "toEmploi";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_JEFY_ORGAN_KEY = "toJefyOrgan";
	public static final String TO_KX_ELEMENT_KEY = "toKxElement";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";

  private static Logger LOG = Logger.getLogger(_EOManguePersBudget.class);

  public EOManguePersBudget localInstanceIn(EOEditingContext editingContext) {
    EOManguePersBudget localInstance = (EOManguePersBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
    	_EOManguePersBudget.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
    	_EOManguePersBudget.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public Integer moisCodeDebut() {
    return (Integer) storedValueForKey("moisCodeDebut");
  }

  public void setMoisCodeDebut(Integer value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
    	_EOManguePersBudget.LOG.debug( "updating moisCodeDebut from " + moisCodeDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCodeDebut");
  }

  public Integer moisCodeFin() {
    return (Integer) storedValueForKey("moisCodeFin");
  }

  public void setMoisCodeFin(Integer value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
    	_EOManguePersBudget.LOG.debug( "updating moisCodeFin from " + moisCodeFin() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCodeFin");
  }

  public Double pourcentage() {
    return (Double) storedValueForKey("pourcentage");
  }

  public void setPourcentage(Double value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
    	_EOManguePersBudget.LOG.debug( "updating pourcentage from " + pourcentage() + " to " + value);
    }
    takeStoredValueForKey(value, "pourcentage");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
    	_EOManguePersBudget.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toEmploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi)storedValueForKey("toEmploi");
  }

  public void setToEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
      _EOManguePersBudget.LOG.debug("updating toEmploi from " + toEmploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi oldValue = toEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEmploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEmploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu toIndividu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
      _EOManguePersBudget.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan toJefyOrgan() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan)storedValueForKey("toJefyOrgan");
  }

  public void setToJefyOrganRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
      _EOManguePersBudget.LOG.debug("updating toJefyOrgan from " + toJefyOrgan() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan oldValue = toJefyOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toJefyOrgan");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toJefyOrgan");
    }
  }
  
  public org.cocktail.connecteur.common.modele.jefy.EOKxElement toKxElement() {
    return (org.cocktail.connecteur.common.modele.jefy.EOKxElement)storedValueForKey("toKxElement");
  }

  public void setToKxElementRelationship(org.cocktail.connecteur.common.modele.jefy.EOKxElement value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
      _EOManguePersBudget.LOG.debug("updating toKxElement from " + toKxElement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.jefy.EOKxElement oldValue = toKxElement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toKxElement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toKxElement");
    }
  }
  
  public org.cocktail.connecteur.common.modele.jefy.EOTypeCredit toTypeCredit() {
    return (org.cocktail.connecteur.common.modele.jefy.EOTypeCredit)storedValueForKey("toTypeCredit");
  }

  public void setToTypeCreditRelationship(org.cocktail.connecteur.common.modele.jefy.EOTypeCredit value) {
    if (_EOManguePersBudget.LOG.isDebugEnabled()) {
      _EOManguePersBudget.LOG.debug("updating toTypeCredit from " + toTypeCredit() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.jefy.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeCredit");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeCredit");
    }
  }
  

  public static EOManguePersBudget createManguePersBudget(EOEditingContext editingContext, NSTimestamp dateDebut
, Integer moisCodeDebut
, Double pourcentage
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOJefyOrgan toJefyOrgan, org.cocktail.connecteur.common.modele.jefy.EOTypeCredit toTypeCredit) {
    EOManguePersBudget eo = (EOManguePersBudget) EOUtilities.createAndInsertInstance(editingContext, _EOManguePersBudget.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setMoisCodeDebut(moisCodeDebut);
		eo.setPourcentage(pourcentage);
		eo.setTemValide(temValide);
    eo.setToJefyOrganRelationship(toJefyOrgan);
    eo.setToTypeCreditRelationship(toTypeCredit);
    return eo;
  }

  public static NSArray<EOManguePersBudget> fetchAllManguePersBudgets(EOEditingContext editingContext) {
    return _EOManguePersBudget.fetchAllManguePersBudgets(editingContext, null);
  }

  public static NSArray<EOManguePersBudget> fetchAllManguePersBudgets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOManguePersBudget.fetchManguePersBudgets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOManguePersBudget> fetchManguePersBudgets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOManguePersBudget.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOManguePersBudget> eoObjects = (NSArray<EOManguePersBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOManguePersBudget fetchManguePersBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePersBudget.fetchManguePersBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePersBudget fetchManguePersBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOManguePersBudget> eoObjects = _EOManguePersBudget.fetchManguePersBudgets(editingContext, qualifier, null);
    EOManguePersBudget eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOManguePersBudget)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ManguePersBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePersBudget fetchRequiredManguePersBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePersBudget.fetchRequiredManguePersBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePersBudget fetchRequiredManguePersBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    EOManguePersBudget eoObject = _EOManguePersBudget.fetchManguePersBudget(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ManguePersBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePersBudget localInstanceIn(EOEditingContext editingContext, EOManguePersBudget eo) {
    EOManguePersBudget localInstance = (eo == null) ? null : (EOManguePersBudget)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
