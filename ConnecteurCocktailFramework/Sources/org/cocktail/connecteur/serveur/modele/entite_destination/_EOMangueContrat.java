// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueContrat.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueContrat extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueContrat";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CONTRAT_TRAV_KEY = "dDebContratTrav";
	public static final String D_FIN_ANTICIPEE_KEY = "dFinAnticipee";
	public static final String D_FIN_CONTRAT_TRAV_KEY = "dFinContratTrav";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_ANNULATION_KEY = "temAnnulation";
	public static final String TEM_BUDGET_PROPRE_KEY = "temBudgetPropre";
	public static final String TEM_FONCTIONNAIRE_KEY = "temFonctionnaire";
	public static final String TEM_RECHERCHE_KEY = "temRecherche";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueContrat.class);

  public EOMangueContrat localInstanceIn(EOEditingContext editingContext) {
    EOMangueContrat localInstance = (EOMangueContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebContratTrav() {
    return (NSTimestamp) storedValueForKey("dDebContratTrav");
  }

  public void setDDebContratTrav(NSTimestamp value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating dDebContratTrav from " + dDebContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebContratTrav");
  }

  public NSTimestamp dFinAnticipee() {
    return (NSTimestamp) storedValueForKey("dFinAnticipee");
  }

  public void setDFinAnticipee(NSTimestamp value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating dFinAnticipee from " + dFinAnticipee() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAnticipee");
  }

  public NSTimestamp dFinContratTrav() {
    return (NSTimestamp) storedValueForKey("dFinContratTrav");
  }

  public void setDFinContratTrav(NSTimestamp value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating dFinContratTrav from " + dFinContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinContratTrav");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temAnnulation() {
    return (String) storedValueForKey("temAnnulation");
  }

  public void setTemAnnulation(String value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating temAnnulation from " + temAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnulation");
  }

  public String temBudgetPropre() {
    return (String) storedValueForKey("temBudgetPropre");
  }

  public void setTemBudgetPropre(String value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating temBudgetPropre from " + temBudgetPropre() + " to " + value);
    }
    takeStoredValueForKey(value, "temBudgetPropre");
  }

  public String temFonctionnaire() {
    return (String) storedValueForKey("temFonctionnaire");
  }

  public void setTemFonctionnaire(String value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating temFonctionnaire from " + temFonctionnaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temFonctionnaire");
  }

  public String temRecherche() {
    return (String) storedValueForKey("temRecherche");
  }

  public void setTemRecherche(String value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
    	_EOMangueContrat.LOG.debug( "updating temRecherche from " + temRecherche() + " to " + value);
    }
    takeStoredValueForKey(value, "temRecherche");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueContrat.LOG.isDebugEnabled()) {
      _EOMangueContrat.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueContrat createMangueContrat(EOEditingContext editingContext, String cTypeContratTrav
, NSTimestamp dCreation
, NSTimestamp dDebContratTrav
, NSTimestamp dModification
, String temBudgetPropre
, String temFonctionnaire
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueContrat eo = (EOMangueContrat) EOUtilities.createAndInsertInstance(editingContext, _EOMangueContrat.ENTITY_NAME);    
		eo.setCTypeContratTrav(cTypeContratTrav);
		eo.setDCreation(dCreation);
		eo.setDDebContratTrav(dDebContratTrav);
		eo.setDModification(dModification);
		eo.setTemBudgetPropre(temBudgetPropre);
		eo.setTemFonctionnaire(temFonctionnaire);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueContrat> fetchAllMangueContrats(EOEditingContext editingContext) {
    return _EOMangueContrat.fetchAllMangueContrats(editingContext, null);
  }

  public static NSArray<EOMangueContrat> fetchAllMangueContrats(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueContrat.fetchMangueContrats(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueContrat> fetchMangueContrats(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueContrat> eoObjects = (NSArray<EOMangueContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueContrat fetchMangueContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueContrat.fetchMangueContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueContrat fetchMangueContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueContrat> eoObjects = _EOMangueContrat.fetchMangueContrats(editingContext, qualifier, null);
    EOMangueContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueContrat)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueContrat fetchRequiredMangueContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueContrat.fetchRequiredMangueContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueContrat fetchRequiredMangueContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueContrat eoObject = _EOMangueContrat.fetchMangueContrat(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueContrat localInstanceIn(EOEditingContext editingContext, EOMangueContrat eo) {
    EOMangueContrat localInstance = (eo == null) ? null : (EOMangueContrat)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
