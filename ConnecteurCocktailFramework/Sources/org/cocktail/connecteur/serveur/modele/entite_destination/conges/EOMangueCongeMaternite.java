//EOCongeMaternite.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCongeMaterniteCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.InterfaceRecordAvecAutresAttributs;
import org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataire;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeMaternite;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.mangue.EOMangueDeclarationMaternite;

import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongeMaternite extends _EOMangueCongeMaternite implements InterfaceRecordAvecAutresAttributs {
	public static int NB_JOURS_NORMAL_FEMME = 112;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_FEMME = 182;
	public static int NB_JOURS_JUMEAUX_FEMME = 238;
	public static int NB_JOURS_TRIPLES_FEMME = 322;
	public static int NB_JOURS_NORMAL_PRENATAL = 42;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_PRENATAL = 56;
	public static int NB_JOURS_JUMEAUX_PRENATAL = 84;
	public static int NB_JOURS_TRIPLES_PRENATAL = 168;
	public static int NB_JOURS_NORMAL_POSTNATAL = 70;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL = 126;
	public static int NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL = 154;
	public EOMangueCongeMaternite() {
		super();
	}


	public String typeEvenement() {
		return "CMATER";
	}
	public NSTimestamp dateCommission() {
		return null;
	}
	/** Compare les donnees de du conge maternite et de la declaration avec le conge d'import. Return true si elles sont identiques.
	@param source 
	@param destination
	 */
	public boolean aAttributsIdentiques(ObjetImport source,Entite entiteModele) {
		if (super.aAttributsIdentiques(source, entiteModele)) {
			EOCongeMaternite conge = (EOCongeMaternite)source;
			if (declarationMaternite() != null) {
				if (declarationMaternite().temAnnuleBis() != null && declarationMaternite().temAnnuleBis().equals(CocktailConstantes.VRAI)) {
					// La déclaration a été annulée
					return false;
				}
				if (memeValeur(conge.temAnnule(),declarationMaternite().temAnnule()) == false) {
					return false;
				}
				if (memeValeur(conge.temGrossesseGemellaire(),declarationMaternite().temGrossesseGemellaire()) == false) {
					return false;
				}
				if (memeValeur(conge.temGrossesseTriple(),declarationMaternite().temGrossesseTriple()) == false) {
					return false;
				}
				if (memeValeur(conge.dNaisPrev(),declarationMaternite().dNaisPrev()) == false) {
					return false;
				}
				if (memeValeur(conge.dAccouchement(),declarationMaternite().dAccouchement()) == false) {
					return false;
				}
				if (memeValeur(conge.dConstatMatern(),declarationMaternite().dConstatMatern()) == false) {
					return false;
				}
				if (memeValeur(conge.nbEnfantsDecl(),declarationMaternite().nbEnfantsDecl()) == false) {
					return false;
				}
				return true;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	/** Cr&acute;ation de la declaration  de maternite si il s'agit d'un record en insertion et qu'il n'y a pas de declaration 
	 * a la m&ecirc;me date pour un autre conge de l'individu ou d'un record en update qui a une nouvelle date de declaration
	 * et qu'il existe plusieurs conges rattaches a l'ancienne declaration. 
	 * Modification des donnees de la declaration sauf si priorite a la destination */
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport record) {
		boolean estModifie = super.modifierAutresAttributsAvecRecordImport(record);
		EOCongeMaternite conge = (EOCongeMaternite)record;
		EOMangueDeclarationMaternite declaration = null;
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), conge.individu().idSource());
		// Pour les records en insertion, créer la déclaration si elle n'existe pas déjà ou qu'elle n'est pas utilisée dans d'autres congé
		// Pour les records en correspondance ou update, il existe une déclaration dans Mangue associée au congé
		if (record.operation().equals(ObjetImport.OPERATION_INSERTION)) {
			// A ce stade, l'individu GRhum est déjà dans le SI Destinataire sinon on ne pourrait pas transférer le congé
			// Vérifier si il existe déjà une déclaration à la même date pour d'autres congés de cet individu en recherchant la correspondance
			// la plus récente
			EOMangueCongeMaternite congeMangue = EOCongeMaterniteCorresp.congeMaterniteManguePourIndividuEtDeclaration(editingContext(), conge.idSource(), conge.dmSource());
			if (congeMangue != null) {
				declaration = congeMangue.declarationMaternite();
			}
			if (declaration != null) {
				// Etablir un lien avec la déclaration trouvée
				addObjectToBothSidesOfRelationshipWithKey(declaration, "declarationMaternite");	
				estModifie = true;
			}
		}
		EOGrhumPrioriteEntite priorite = EOGrhumPrioriteEntite.rechercherPrioriteEntitePourEntite(editingContext(),"DeclarationMaternite");
		if (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || record.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			ObjetPourSIDestinataire objet = conge.objetDestinataireAvecOuSansCorrespondance(editingContext());
			if (objet != null) {
				declaration = ((EOMangueCongeMaternite)objet).declarationMaternite();
				// Vérifier si la déclaration a été invalidée dans Mangue, en quel cas selon la valeur de la priorité de la destinataion
				// il faudra la recréer ou non
				if (declaration.temAnnuleBis() != null && declaration.temAnnuleBis().equals(CocktailConstantes.VRAI)) {
					if (priorite.estARevalider()) {
						declaration.setTemAnnuleBis(CocktailConstantes.FAUX);
					} else {
						// il faut recréer la déclaration
						declaration = null;
					}
				}
			} 
		}
		if (declaration != null) {
			// Rechercher la priorité associée à l'entité DeclarationMaternite : cette entité est décrite dans ModeleDestination.XML et non dans ModeleImport.XML
			// uniquement pour gérer les priorités
			// Vérifier si il y a des règles de priorité sur les attributs qui fait qu'on ne doit pas y toucher
			// Modifier les attributs de date de déclaration si il n'y a pas de règle de priorité sur ceux-ci
			// et si il n'y a pas une nouvelle déclaration à créer
			modifierAttributDeclarationAvecValeur(priorite,declaration,"dConstatMatern",conge.dConstatMatern());
			modifierAttributDeclarationAvecValeur(priorite,declaration,"dNaisPrev",conge.dNaisPrev());
			modifierAttributDeclarationAvecValeur(priorite,declaration,"dAccouchement",conge.dAccouchement());
			modifierAttributDeclarationAvecValeur(priorite,declaration,"temAnnule",conge.temAnnule());
			modifierAttributDeclarationAvecValeur(priorite,declaration,"temGrossesseGemellaire",conge.temGrossesseGemellaire());
			modifierAttributDeclarationAvecValeur(priorite,declaration,"temGrossesseTriple",conge.temGrossesseTriple());
			modifierAttributDeclarationAvecValeur(priorite,declaration,"nbEnfantsDecl",conge.nbEnfantsDecl());
			boolean declarationModifiee = editingContext().updatedObjects().containsObject(declaration);
			if (declarationModifiee) {
				declaration.setDModification(new NSTimestamp());
				LogManager.logDetail("Operation de correspondance ou d'update : modification de la declaration : " + declaration + "\n");
			}
			return estModifie || declarationModifiee;
		} else  {
			// Créer une nouvelle déclaration
			declaration = new EOMangueDeclarationMaternite();
			editingContext().insertObject(declaration);
			declaration.initAvecCongeImportEtIndividuGRhum(conge,individuGrhum);
			addObjectToBothSidesOfRelationshipWithKey(declaration, "declarationMaternite");	
			return true;
			}
	}
	// Méthodes privées
	private void modifierAttributDeclarationAvecValeur(EOGrhumPrioriteEntite priorite,EOMangueDeclarationMaternite declaration,String attribut,Object valeur) {
		if ((priorite == null || priorite.attributPourLibelle(attribut,true) == null)) {
			declaration.takeValueForKey(valeur, attribut);
		}
	}
	private boolean memeValeur(Object object1,Object object2) {
		if ((object1 == null && object2 != null) || (object1 != null && object2 == null)) {
			return false;
		}
		if (object1 == null && object2 == null) {
			return true;
		}
		if (object1 instanceof String) {
			return object1.equals(object2);
		} else if (object1 instanceof Integer) {
			return ((Integer)object1).intValue() == ((Integer)object2).intValue();
		} else if (object1 instanceof NSTimestamp) {
			return DateCtrl.isSameDay((NSTimestamp)object1,(NSTimestamp)object2);
		}
		return false;
	}

	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongeMaternite)value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongeMaterniteCorresp congeCorresp = EOCongeMaterniteCorresp.fetchCongeMaterniteCorresp(editingContext(), EOCongeMaterniteCorresp.CONGE_MATERNITE_KEY,
				congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.congeMaterniteMangue();
	}
}
