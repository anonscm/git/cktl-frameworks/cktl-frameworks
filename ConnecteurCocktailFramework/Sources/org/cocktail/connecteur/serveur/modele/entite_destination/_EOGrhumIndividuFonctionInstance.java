// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumIndividuFonctionInstance.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumIndividuFonctionInstance extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "GrhumIndividuFonctionInstance";

	// Attributes
	public static final String C_FONCTION_KEY = "cFonction";
	public static final String C_INSTANCE_KEY = "cInstance";
	public static final String C_QUALITE_KEY = "cQualite";
	public static final String C_SS_STATUT_KEY = "cSsStatut";
	public static final String C_STATUT_KEY = "cStatut";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOGrhumIndividuFonctionInstance.class);

  public EOGrhumIndividuFonctionInstance localInstanceIn(EOEditingContext editingContext) {
    EOGrhumIndividuFonctionInstance localInstance = (EOGrhumIndividuFonctionInstance)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cFonction() {
    return (Integer) storedValueForKey("cFonction");
  }

  public void setCFonction(Integer value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating cFonction from " + cFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "cFonction");
  }

  public String cInstance() {
    return (String) storedValueForKey("cInstance");
  }

  public void setCInstance(String value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating cInstance from " + cInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "cInstance");
  }

  public String cQualite() {
    return (String) storedValueForKey("cQualite");
  }

  public void setCQualite(String value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating cQualite from " + cQualite() + " to " + value);
    }
    takeStoredValueForKey(value, "cQualite");
  }

  public String cSsStatut() {
    return (String) storedValueForKey("cSsStatut");
  }

  public void setCSsStatut(String value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating cSsStatut from " + cSsStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "cSsStatut");
  }

  public String cStatut() {
    return (String) storedValueForKey("cStatut");
  }

  public void setCStatut(String value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating cStatut from " + cStatut() + " to " + value);
    }
    takeStoredValueForKey(value, "cStatut");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionInstance.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOGrhumIndividuFonctionInstance.LOG.isDebugEnabled()) {
      _EOGrhumIndividuFonctionInstance.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOGrhumIndividuFonctionInstance createGrhumIndividuFonctionInstance(EOEditingContext editingContext, Integer cFonction
, String cInstance
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOGrhumIndividuFonctionInstance eo = (EOGrhumIndividuFonctionInstance) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumIndividuFonctionInstance.ENTITY_NAME);    
		eo.setCFonction(cFonction);
		eo.setCInstance(cInstance);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOGrhumIndividuFonctionInstance> fetchAllGrhumIndividuFonctionInstances(EOEditingContext editingContext) {
    return _EOGrhumIndividuFonctionInstance.fetchAllGrhumIndividuFonctionInstances(editingContext, null);
  }

  public static NSArray<EOGrhumIndividuFonctionInstance> fetchAllGrhumIndividuFonctionInstances(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumIndividuFonctionInstance.fetchGrhumIndividuFonctionInstances(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumIndividuFonctionInstance> fetchGrhumIndividuFonctionInstances(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumIndividuFonctionInstance.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumIndividuFonctionInstance> eoObjects = (NSArray<EOGrhumIndividuFonctionInstance>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumIndividuFonctionInstance fetchGrhumIndividuFonctionInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividuFonctionInstance.fetchGrhumIndividuFonctionInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividuFonctionInstance fetchGrhumIndividuFonctionInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumIndividuFonctionInstance> eoObjects = _EOGrhumIndividuFonctionInstance.fetchGrhumIndividuFonctionInstances(editingContext, qualifier, null);
    EOGrhumIndividuFonctionInstance eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumIndividuFonctionInstance)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumIndividuFonctionInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividuFonctionInstance fetchRequiredGrhumIndividuFonctionInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividuFonctionInstance.fetchRequiredGrhumIndividuFonctionInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividuFonctionInstance fetchRequiredGrhumIndividuFonctionInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumIndividuFonctionInstance eoObject = _EOGrhumIndividuFonctionInstance.fetchGrhumIndividuFonctionInstance(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumIndividuFonctionInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividuFonctionInstance localInstanceIn(EOEditingContext editingContext, EOGrhumIndividuFonctionInstance eo) {
    EOGrhumIndividuFonctionInstance localInstance = (eo == null) ? null : (EOGrhumIndividuFonctionInstance)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
