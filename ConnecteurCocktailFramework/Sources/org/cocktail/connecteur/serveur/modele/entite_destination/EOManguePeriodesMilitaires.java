//EOManguePeriodesMilitaires.java
//Created on Wed Oct 12 14:23:44 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPeriodesMilitaires;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Verifie que le type de periode est declare et que les periodes ne se chevauchent pas
 * @author christine
 *
 */
public class EOManguePeriodesMilitaires extends _EOManguePeriodesMilitaires {

	public EOManguePeriodesMilitaires() {
		super();
	}

	public static EOManguePeriodesMilitaires periodeDestinationPourPeriode(EOEditingContext editingContext,EOPeriodesMilitaires periode) {
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)periode.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSArray periodes = rechercherPeriodesPourIndividuEtDates(editingContext,individuGrhum,periode.dateDebut(),periode.dateFin());
			try {
				return (EOManguePeriodesMilitaires)periodes.objectAtIndex(0);
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
	/** Retourne la liste des periodes militaires triees par ordre de date croissant, toutes les periodes si dateDebut est nulle */
	public static NSArray rechercherPeriodesPourIndividuEtDates(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("individu.noIndividu = %@",new NSArray(individu.noIndividu())));
		if (debutPeriode != null) {
			qualifiers.addObject(Finder.qualifierPourPeriode("dateDebut",debutPeriode,"dateFin",finPeriode));
		}
		EOFetchSpecification fs = new EOFetchSpecification("ManguePeriodesMilitaires",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		return editingContext.objectsWithFetchSpecification(fs);

	}
	
	
}
