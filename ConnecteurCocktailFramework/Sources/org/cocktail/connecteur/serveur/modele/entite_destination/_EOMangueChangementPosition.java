// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueChangementPosition.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueChangementPosition extends ObjetPourSIDestinataireAvecCarriere {
	public static final String ENTITY_NAME = "MangueChangementPosition";

	// Attributes
	public static final String C_MOTIF_POSITION_KEY = "cMotifPosition";
	public static final String C_POSITION_KEY = "cPosition";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_RNE_ORIG_KEY = "cRneOrig";
	public static final String D_ARRETE_POSITION_KEY = "dArretePosition";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_POSITION_KEY = "dDebPosition";
	public static final String D_FIN_POSITION_KEY = "dFinPosition";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_POSITION_KEY = "lieuPosition";
	public static final String LIEU_POSITION_ORIG_KEY = "lieuPositionOrig";
	public static final String NO_ARRETE_POSITION_KEY = "noArretePosition";
	public static final String NO_ENFANT_KEY = "noEnfant";
	public static final String QUOTITE_POSITION_KEY = "quotitePosition";
	public static final String TEMOIN_POSITION_PREV_KEY = "temoinPositionPrev";
	public static final String TEM_PC_ACQUITEE_KEY = "temPcAcquitee";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String TXT_POSITION_KEY = "txtPosition";
	public static final String TXT_POSITION_ORIG_KEY = "txtPositionOrig";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_CARRIERE_ORIGINE_KEY = "toCarriereOrigine";
	public static final String TO_GRHUM_ENFANT_KEY = "toGrhumEnfant";

  private static Logger LOG = Logger.getLogger(_EOMangueChangementPosition.class);

  public EOMangueChangementPosition localInstanceIn(EOEditingContext editingContext) {
    EOMangueChangementPosition localInstance = (EOMangueChangementPosition)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifPosition() {
    return (String) storedValueForKey("cMotifPosition");
  }

  public void setCMotifPosition(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating cMotifPosition from " + cMotifPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifPosition");
  }

  public String cPosition() {
    return (String) storedValueForKey("cPosition");
  }

  public void setCPosition(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating cPosition from " + cPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cPosition");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cRneOrig() {
    return (String) storedValueForKey("cRneOrig");
  }

  public void setCRneOrig(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating cRneOrig from " + cRneOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "cRneOrig");
  }

  public NSTimestamp dArretePosition() {
    return (NSTimestamp) storedValueForKey("dArretePosition");
  }

  public void setDArretePosition(NSTimestamp value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating dArretePosition from " + dArretePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretePosition");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebPosition() {
    return (NSTimestamp) storedValueForKey("dDebPosition");
  }

  public void setDDebPosition(NSTimestamp value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating dDebPosition from " + dDebPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebPosition");
  }

  public NSTimestamp dFinPosition() {
    return (NSTimestamp) storedValueForKey("dFinPosition");
  }

  public void setDFinPosition(NSTimestamp value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating dFinPosition from " + dFinPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinPosition");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lieuPosition() {
    return (String) storedValueForKey("lieuPosition");
  }

  public void setLieuPosition(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating lieuPosition from " + lieuPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuPosition");
  }

  public String lieuPositionOrig() {
    return (String) storedValueForKey("lieuPositionOrig");
  }

  public void setLieuPositionOrig(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating lieuPositionOrig from " + lieuPositionOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuPositionOrig");
  }

  public String noArretePosition() {
    return (String) storedValueForKey("noArretePosition");
  }

  public void setNoArretePosition(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating noArretePosition from " + noArretePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "noArretePosition");
  }

  public Integer noEnfant() {
    return (Integer) storedValueForKey("noEnfant");
  }

  public void setNoEnfant(Integer value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating noEnfant from " + noEnfant() + " to " + value);
    }
    takeStoredValueForKey(value, "noEnfant");
  }

  public Integer quotitePosition() {
    return (Integer) storedValueForKey("quotitePosition");
  }

  public void setQuotitePosition(Integer value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating quotitePosition from " + quotitePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "quotitePosition");
  }

  public String temoinPositionPrev() {
    return (String) storedValueForKey("temoinPositionPrev");
  }

  public void setTemoinPositionPrev(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating temoinPositionPrev from " + temoinPositionPrev() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinPositionPrev");
  }

  public String temPcAcquitee() {
    return (String) storedValueForKey("temPcAcquitee");
  }

  public void setTemPcAcquitee(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating temPcAcquitee from " + temPcAcquitee() + " to " + value);
    }
    takeStoredValueForKey(value, "temPcAcquitee");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public String txtPosition() {
    return (String) storedValueForKey("txtPosition");
  }

  public void setTxtPosition(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating txtPosition from " + txtPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "txtPosition");
  }

  public String txtPositionOrig() {
    return (String) storedValueForKey("txtPositionOrig");
  }

  public void setTxtPositionOrig(String value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
    	_EOMangueChangementPosition.LOG.debug( "updating txtPositionOrig from " + txtPositionOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "txtPositionOrig");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
      _EOMangueChangementPosition.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
      _EOMangueChangementPosition.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere toCarriereOrigine() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("toCarriereOrigine");
  }

  public void setToCarriereOrigineRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
      _EOMangueChangementPosition.LOG.debug("updating toCarriereOrigine from " + toCarriereOrigine() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = toCarriereOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriereOrigine");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriereOrigine");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant toGrhumEnfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("toGrhumEnfant");
  }

  public void setToGrhumEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOMangueChangementPosition.LOG.isDebugEnabled()) {
      _EOMangueChangementPosition.LOG.debug("updating toGrhumEnfant from " + toGrhumEnfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = toGrhumEnfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrhumEnfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrhumEnfant");
    }
  }
  

  public static EOMangueChangementPosition createMangueChangementPosition(EOEditingContext editingContext, String cPosition
, NSTimestamp dCreation
, NSTimestamp dDebPosition
, NSTimestamp dModification
, Integer quotitePosition
, String temoinPositionPrev
, String temPcAcquitee
, String temValide
) {
    EOMangueChangementPosition eo = (EOMangueChangementPosition) EOUtilities.createAndInsertInstance(editingContext, _EOMangueChangementPosition.ENTITY_NAME);    
		eo.setCPosition(cPosition);
		eo.setDCreation(dCreation);
		eo.setDDebPosition(dDebPosition);
		eo.setDModification(dModification);
		eo.setQuotitePosition(quotitePosition);
		eo.setTemoinPositionPrev(temoinPositionPrev);
		eo.setTemPcAcquitee(temPcAcquitee);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueChangementPosition> fetchAllMangueChangementPositions(EOEditingContext editingContext) {
    return _EOMangueChangementPosition.fetchAllMangueChangementPositions(editingContext, null);
  }

  public static NSArray<EOMangueChangementPosition> fetchAllMangueChangementPositions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueChangementPosition.fetchMangueChangementPositions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueChangementPosition> fetchMangueChangementPositions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueChangementPosition.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueChangementPosition> eoObjects = (NSArray<EOMangueChangementPosition>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueChangementPosition fetchMangueChangementPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueChangementPosition.fetchMangueChangementPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueChangementPosition fetchMangueChangementPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueChangementPosition> eoObjects = _EOMangueChangementPosition.fetchMangueChangementPositions(editingContext, qualifier, null);
    EOMangueChangementPosition eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueChangementPosition)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueChangementPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueChangementPosition fetchRequiredMangueChangementPosition(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueChangementPosition.fetchRequiredMangueChangementPosition(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueChangementPosition fetchRequiredMangueChangementPosition(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueChangementPosition eoObject = _EOMangueChangementPosition.fetchMangueChangementPosition(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueChangementPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueChangementPosition localInstanceIn(EOEditingContext editingContext, EOMangueChangementPosition eo) {
    EOMangueChangementPosition localInstance = (eo == null) ? null : (EOMangueChangementPosition)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
