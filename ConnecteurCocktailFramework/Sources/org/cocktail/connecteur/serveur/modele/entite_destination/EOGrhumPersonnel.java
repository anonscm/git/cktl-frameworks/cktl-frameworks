// EOGrhumPersonnel.java
// Created on Wed Jul 25 15:13:48 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersonnel;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOGrhumPersonnel extends _EOGrhumPersonnel {

    public EOGrhumPersonnel() {
        super();
    }

    // Méthodes ajoutées
    /** toujours vrai, pas de notion de validite pour les personnels */
	public boolean estValide() {
		return true;
	}
	/** Pas d'action */
	public void setEstValide(boolean aBool) {
	}
    // Méthodes protégées
	protected void initialiserObjet(ObjetImport record) throws Exception {
		setTemPaieSecu(CocktailConstantes.VRAI);
		setTemImposable(CocktailConstantes.VRAI);
		setTemBudgetEtat(CocktailConstantes.FAUX);
		setTemTitulaire(CocktailConstantes.FAUX);	
		setNbEnfants(new Integer(0));
		setNbEnfantsACharge(new Integer(0));
		EOPersonnel personnel = (EOPersonnel)record;
		EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), personnel.individu());
		if (individu != null) {	// Ne devrait pas être nul car on crée les individus avant
			setNoDossierPers(individu.noIndividu());
			addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
		} else {
			throw new Exception("Pas d'individu dans le SI pour l'identifiant " + personnel.individu().idSource());
		}
	}
    // Méthodes statiques
    public static NSArray rechercherPersonnelsPourMatricule(EOEditingContext editingContext,String noMatricule) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("noMatricule = %@", new NSArray(noMatricule));
    	EOFetchSpecification fs = new EOFetchSpecification("GrhumPersonnel", qualifier, null);
    	return editingContext.objectsWithFetchSpecification(fs);
	}
    /**  Retourne un nouveau no matricule sur 6 caract&egrave;res */
	public static String getNewNoMatricule(EOEditingContext ec) {
		int anneeCourante = new GregorianCalendar().get(Calendar.YEAR);
		String chaine =  Integer.toString(anneeCourante).substring(2,4);
		
 		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("noMatricule like '" + chaine  + "*'" ,null);
 		EOFetchSpecification  fs = new EOFetchSpecification ("GrhumPersonnel", qual, null);
 		fs.setRefreshesRefetchedObjects(true);
		
		NSMutableArray mySort = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("noMatricule",EOSortOrdering.CompareDescending));
 		NSArray matricules = EOSortOrdering.sortedArrayUsingKeyOrderArray(ec.objectsWithFetchSpecification(fs),mySort);
		
		if (matricules.count() == 0)			
			return chaine + "0001";		
		
		String numero = (((EOGrhumPersonnel)matricules.objectAtIndex(0)).noMatricule()).substring(2,6);
		int newNumero = (new Integer(numero).intValue()) + 1;
		
		return chaine + formatteNoAvecZeros(Integer.toString(newNumero),4); 
	}
	// Méthodes privées
	/* Formatte un numero donne avec des 0 selon le nombre de caracteres voulu (Ex 3 sur 3 caracteres ==> '003')
	*/
	private static String formatteNoAvecZeros(String numero, int nbCars) {
		String chaine = "";

		for (int i = 0;i < (nbCars - numero.length());i++)
			chaine = chaine + "0";
	
		return chaine + numero;
	}

}
