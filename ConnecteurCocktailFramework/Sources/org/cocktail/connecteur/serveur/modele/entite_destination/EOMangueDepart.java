//EODepart.java
//Created on Thu Dec 01 10:00:20 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;


import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart;
import org.cocktail.connecteur.importer.moteur.LogDepart;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Ferme les informations de l'individu (carri&egrave;re, contrats, conges, occupations, affectations,...)
 */
public class EOMangueDepart extends _EOMangueDepart implements InterfaceRecordAvecAutresAttributs {

	public final static String TYPE_ABSENCE_DEPART = "DEPA";
	private final static String TEXTE_COMMENTAIRE = "Départ importé avec le connecteur Cocktail";

	private NSTimestamp oldDateDebut;	// pour éviter qu'on génère un log de départ en cas d'update alors que la date de début n'a pas été modifiée
	public EOMangueDepart() {
		super();
	}

	public void awakeFromFetch(EOEditingContext editingContext) {
		oldDateDebut = dateDebut();
	}
	
	/** Pour construire un depart a la volee */
	public void initAvecIndividu(EOGrhumIndividu individu) {
		setTemDepartPrevisionnel(CocktailConstantes.FAUX);
		setTemValide(CocktailConstantes.VRAI);

		oldDateDebut = null;
		setDateDebut(individu.dDeces());
		// Fournir la date de cessation de service qui doit être présente pour les départs
		setDCessationService(individu.dDeces());
		setDateFin(null);
		setCMotifDepart(EOMotifDepart.TYPE_MOTIF_DECES);
		NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
		setIndividuRelationship(individu);

		// Ajouter un log pour signaler qu'on a ajouté un départ
		creerLogPourDepart(ObjetImport.OPERATION_INSERTION);
	}
	/** Pour mettre a jour un depart, la date de modification de service est modifiee, la date de fin
	 * est mise a nulle si elle existait */
	public void mettreAJourPourDeces(NSTimestamp dateDebut) {
		setDateDebut(dateDebut);
		setDCessationService(dateDebut);
		setDateFin(null);
		setCMotifDepart(EOMotifDepart.TYPE_MOTIF_DECES);

		setDModification(new NSTimestamp());
		// Ajouter un log pour signaler qu'on a ajouté un départ
		creerLogPourDepart(ObjetImport.OPERATION_UPDATE);
	}

	
	
	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		setTemDepartPrevisionnel(CocktailConstantes.FAUX);
		oldDateDebut = null;
		super.initialiserObjet(recordImport);
	}

	private void creerLogPourDepart(String operationImportPourDepart) {
		LogManager.logDetail("Ajout d'un log d'information pour ce depart");
		// Modifier le commentaire de ce départ pour ajouter le texte "Départ modifié par le connecteur Cocktail";
		// Signaler ce départ dans le report des départs;
		String temp = commentaire();
		if (temp == null) {
			temp = "";
		}
		temp = TEXTE_COMMENTAIRE + temp;
		setCommentaire(temp);
		EOMotifDepart motifDepart = (EOMotifDepart)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOMotifDepart.ENTITY_NAME, "cMotifDepart", cMotifDepart());
		LogDepart.sharedInstance().ajouterDepart(this, operationImportPourDepart,motifDepart.metFinACarriere());
	}
	// Méthodes statiques
	/** Retourne tous les departs du SI Destinataire valides sur la periode. Si date debut est nulle, retourne tous
	 * les departs valides de l'individu. 
	 *	Retourne null si l'individu GRhum n'existe pas
	 */
	public static NSArray rechercherDepartsManguePourIndividuEtPeriode(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY  + " = %@ AND temValide = 'O'",new NSArray(individu)));
		if (dateDebut != null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));
		}
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** Retourne tous les departs du SI Destinataire qui commencent a date debut (au plus 1)
	 *  Retourne null si l'individu GRhum n'existe pas
	 */
	public static NSArray rechercherDepartsManguePourIndividuEtDateDebut(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut) {
		// Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		// On recherche uniquement les objets valides
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray args = new NSMutableArray(individuGrhum);
			// Vérifier que la date de début de départ est la même : pour éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(dateDebut));
			args.addObject(DateCtrl.jourSuivant(dateDebut));
			String qualifier = INDIVIDU_KEY + " = %@ AND dateDebut > %@ AND dateDebut < %@ AND temValide = 'O'";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		}
		return null;
	}
	public static NSArray rechercherDepartsPourDecesEtIndividu(EOEditingContext editingContext, EOIndividu individu) {
		// Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		// On recherche uniquement les objets valides
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individuGrhum)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_MOTIF_DEPART_KEY + " = %@", new NSArray(EOMotifDepart.TYPE_MOTIF_DECES)));
			EOFetchSpecification fs = new EOFetchSpecification (ENTITY_NAME, new EOAndQualifier(qualifiers),null);
			return editingContext.objectsWithFetchSpecification(fs);
		}
		return null;
	}

	public boolean modifierAutresAttributsAvecRecordImport(
			ObjetImport recordImport) {
		// TODO Auto-generated method stub
		return false;
	}

	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		NSTimestamp dateActuelle = new NSTimestamp();
		NSTimestamp dCreation = dateActuelle;
		NSTimestamp dModification = dateActuelle;
	}
}
