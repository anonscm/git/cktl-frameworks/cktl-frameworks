// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongeSolidariteFamiliale.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongeSolidariteFamiliale extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueCongeSolidariteFamiliale";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CERTIFICAT_MEDICAL_KEY = "dCertificatMedical";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FIN_ANTICIPEE_KEY = "dFinAnticipee";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_SEQ_ARRETE_RENOUV_KEY = "noSeqArreteRenouv";
	public static final String TCSF_ORDRE_KEY = "tcsfOrdre";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_RENOUVELE_KEY = "temRenouvele";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CONGE_RENOUVELE_KEY = "congeRenouvele";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TYPE_CSF_KEY = "typeCsf";

  private static Logger LOG = Logger.getLogger(_EOMangueCongeSolidariteFamiliale.class);

  public EOMangueCongeSolidariteFamiliale localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongeSolidariteFamiliale localInstance = (EOMangueCongeSolidariteFamiliale)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCertificatMedical() {
    return (NSTimestamp) storedValueForKey("dCertificatMedical");
  }

  public void setDCertificatMedical(NSTimestamp value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating dCertificatMedical from " + dCertificatMedical() + " to " + value);
    }
    takeStoredValueForKey(value, "dCertificatMedical");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFinAnticipee() {
    return (NSTimestamp) storedValueForKey("dFinAnticipee");
  }

  public void setDFinAnticipee(NSTimestamp value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating dFinAnticipee from " + dFinAnticipee() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAnticipee");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer noSeqArreteRenouv() {
    return (Integer) storedValueForKey("noSeqArreteRenouv");
  }

  public void setNoSeqArreteRenouv(Integer value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating noSeqArreteRenouv from " + noSeqArreteRenouv() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqArreteRenouv");
  }

  public Integer tcsfOrdre() {
    return (Integer) storedValueForKey("tcsfOrdre");
  }

  public void setTcsfOrdre(Integer value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating tcsfOrdre from " + tcsfOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "tcsfOrdre");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temRenouvele() {
    return (String) storedValueForKey("temRenouvele");
  }

  public void setTemRenouvele(String value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating temRenouvele from " + temRenouvele() + " to " + value);
    }
    takeStoredValueForKey(value, "temRenouvele");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueCongeSolidariteFamiliale.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale congeRenouvele() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale)storedValueForKey("congeRenouvele");
  }

  public void setCongeRenouveleRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
      _EOMangueCongeSolidariteFamiliale.LOG.debug("updating congeRenouvele from " + congeRenouvele() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCongeSolidariteFamiliale oldValue = congeRenouvele();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeRenouvele");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeRenouvele");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
      _EOMangueCongeSolidariteFamiliale.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeCsf typeCsf() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeCsf)storedValueForKey("typeCsf");
  }

  public void setTypeCsfRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeCsf value) {
    if (_EOMangueCongeSolidariteFamiliale.LOG.isDebugEnabled()) {
      _EOMangueCongeSolidariteFamiliale.LOG.debug("updating typeCsf from " + typeCsf() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeCsf oldValue = typeCsf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeCsf");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeCsf");
    }
  }
  

  public static EOMangueCongeSolidariteFamiliale createMangueCongeSolidariteFamiliale(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temConfirme
, String temGestEtab
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCongeSolidariteFamiliale eo = (EOMangueCongeSolidariteFamiliale) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongeSolidariteFamiliale.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCongeSolidariteFamiliale> fetchAllMangueCongeSolidariteFamiliales(EOEditingContext editingContext) {
    return _EOMangueCongeSolidariteFamiliale.fetchAllMangueCongeSolidariteFamiliales(editingContext, null);
  }

  public static NSArray<EOMangueCongeSolidariteFamiliale> fetchAllMangueCongeSolidariteFamiliales(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongeSolidariteFamiliale.fetchMangueCongeSolidariteFamiliales(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongeSolidariteFamiliale> fetchMangueCongeSolidariteFamiliales(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongeSolidariteFamiliale.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongeSolidariteFamiliale> eoObjects = (NSArray<EOMangueCongeSolidariteFamiliale>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongeSolidariteFamiliale fetchMangueCongeSolidariteFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeSolidariteFamiliale.fetchMangueCongeSolidariteFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeSolidariteFamiliale fetchMangueCongeSolidariteFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongeSolidariteFamiliale> eoObjects = _EOMangueCongeSolidariteFamiliale.fetchMangueCongeSolidariteFamiliales(editingContext, qualifier, null);
    EOMangueCongeSolidariteFamiliale eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongeSolidariteFamiliale)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongeSolidariteFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeSolidariteFamiliale fetchRequiredMangueCongeSolidariteFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeSolidariteFamiliale.fetchRequiredMangueCongeSolidariteFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeSolidariteFamiliale fetchRequiredMangueCongeSolidariteFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongeSolidariteFamiliale eoObject = _EOMangueCongeSolidariteFamiliale.fetchMangueCongeSolidariteFamiliale(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongeSolidariteFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeSolidariteFamiliale localInstanceIn(EOEditingContext editingContext, EOMangueCongeSolidariteFamiliale eo) {
    EOMangueCongeSolidariteFamiliale localInstance = (eo == null) ? null : (EOMangueCongeSolidariteFamiliale)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
