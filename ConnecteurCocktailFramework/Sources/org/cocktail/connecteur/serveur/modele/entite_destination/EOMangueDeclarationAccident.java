package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption;
import org.cocktail.connecteur.serveur.modele.entite_import.EODeclarationAccident;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueDeclarationAccident extends _EOMangueDeclarationAccident {
	private static Logger log = Logger.getLogger(EOMangueDeclarationAccident.class);

	public EOMangueDeclarationAccident() {
		super();
	}

	public void supprimerRelations() {
		setIndividuRelationship(null);
	}

	// Méthodes statiques
	/**
	 * Retourne la declaration &grave; la date passee en param&egrave;tre, null
	 * si non trouvee ou individu ou date sont nuls
	 */
	public static EOMangueDeclarationAccident rechercherDeclarationPourIndividuEtDate(EOEditingContext editingContext, EOGrhumIndividu individu,
			NSTimestamp dateDeclaration) {
		if (individu == null || dateDeclaration == null) {
			return null;
		}
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(DateCtrl.jourPrecedent(dateDeclaration));
		args.addObject(DateCtrl.jourSuivant(dateDeclaration));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ AND " + DACC_DATE_KEY + " > %@ AND " + DACC_DATE_KEY + " <  %@",
				args);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		try {
			return (EOMangueDeclarationAccident) editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EODeclarationAccident declarationAcc = (EODeclarationAccident) recordImport;
		
		EOGrhumIndividu grhumIndividu = EOIndividuCorresp.individuGrhum(editingContext(), declarationAcc.individu());
		setIndividuRelationship(grhumIndividu);
	}

	@Override
	public String temValide() {
		// La table Mangue ne contient pas de TemValide (12/2014). On retourne donc systématiquement vrai
		return CocktailConstantes.VRAI;
	}

	@Override
	public void setTemValide(String value) {
		// La table Mangue ne contient pas de TemValide (12/2014). On surcharge donc la méthode pour ne rien faire
	}
}
