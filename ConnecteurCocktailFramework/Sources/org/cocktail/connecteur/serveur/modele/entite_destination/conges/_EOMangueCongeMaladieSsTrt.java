// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongeMaladieSsTrt.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongeMaladieSsTrt extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCongeMaladieSsTrt";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRET_TRAVAIL_KEY = "dArretTravail";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_COM_KEY = "dComMedCom";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_ACC_SERV_KEY = "temAccServ";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PROLONG_MALADIE_KEY = "temProlongMaladie";
	public static final String TEM_REQUALIF_MALADIE_KEY = "temRequalifMaladie";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCongeMaladieSsTrt.class);

  public EOMangueCongeMaladieSsTrt localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongeMaladieSsTrt localInstance = (EOMangueCongeMaladieSsTrt)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArretTravail() {
    return (NSTimestamp) storedValueForKey("dArretTravail");
  }

  public void setDArretTravail(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dArretTravail from " + dArretTravail() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretTravail");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedCom() {
    return (NSTimestamp) storedValueForKey("dComMedCom");
  }

  public void setDComMedCom(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dComMedCom from " + dComMedCom() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedCom");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temAccServ() {
    return (String) storedValueForKey("temAccServ");
  }

  public void setTemAccServ(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating temAccServ from " + temAccServ() + " to " + value);
    }
    takeStoredValueForKey(value, "temAccServ");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temProlongMaladie() {
    return (String) storedValueForKey("temProlongMaladie");
  }

  public void setTemProlongMaladie(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating temProlongMaladie from " + temProlongMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongMaladie");
  }

  public String temRequalifMaladie() {
    return (String) storedValueForKey("temRequalifMaladie");
  }

  public void setTemRequalifMaladie(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating temRequalifMaladie from " + temRequalifMaladie() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalifMaladie");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieSsTrt.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
      _EOMangueCongeMaladieSsTrt.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt value) {
    if (_EOMangueCongeMaladieSsTrt.LOG.isDebugEnabled()) {
      _EOMangueCongeMaladieSsTrt.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladieSsTrt oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCongeMaladieSsTrt createMangueCongeMaladieSsTrt(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temAccServ
, String temConfirme
, String temGestEtab
, String temProlongMaladie
, String temRequalifMaladie
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCongeMaladieSsTrt eo = (EOMangueCongeMaladieSsTrt) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongeMaladieSsTrt.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemAccServ(temAccServ);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemProlongMaladie(temProlongMaladie);
		eo.setTemRequalifMaladie(temRequalifMaladie);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCongeMaladieSsTrt> fetchAllMangueCongeMaladieSsTrts(EOEditingContext editingContext) {
    return _EOMangueCongeMaladieSsTrt.fetchAllMangueCongeMaladieSsTrts(editingContext, null);
  }

  public static NSArray<EOMangueCongeMaladieSsTrt> fetchAllMangueCongeMaladieSsTrts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongeMaladieSsTrt.fetchMangueCongeMaladieSsTrts(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongeMaladieSsTrt> fetchMangueCongeMaladieSsTrts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongeMaladieSsTrt.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongeMaladieSsTrt> eoObjects = (NSArray<EOMangueCongeMaladieSsTrt>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongeMaladieSsTrt fetchMangueCongeMaladieSsTrt(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeMaladieSsTrt.fetchMangueCongeMaladieSsTrt(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeMaladieSsTrt fetchMangueCongeMaladieSsTrt(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongeMaladieSsTrt> eoObjects = _EOMangueCongeMaladieSsTrt.fetchMangueCongeMaladieSsTrts(editingContext, qualifier, null);
    EOMangueCongeMaladieSsTrt eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongeMaladieSsTrt)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongeMaladieSsTrt that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeMaladieSsTrt fetchRequiredMangueCongeMaladieSsTrt(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeMaladieSsTrt.fetchRequiredMangueCongeMaladieSsTrt(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeMaladieSsTrt fetchRequiredMangueCongeMaladieSsTrt(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongeMaladieSsTrt eoObject = _EOMangueCongeMaladieSsTrt.fetchMangueCongeMaladieSsTrt(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongeMaladieSsTrt that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeMaladieSsTrt localInstanceIn(EOEditingContext editingContext, EOMangueCongeMaladieSsTrt eo) {
    EOMangueCongeMaladieSsTrt localInstance = (eo == null) ? null : (EOMangueCongeMaladieSsTrt)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
