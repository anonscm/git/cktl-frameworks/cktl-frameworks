//EOMangueElementCarriere.java
//Created on Mon Jan 07 16:35:05 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.services.ServicesConsAnciennete;
import org.cocktail.connecteur.serveur.modele.entite_destination.services.ServicesEmploi;
import org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueElementCarriere extends _EOMangueElementCarriere {
	
	public ServicesConsAnciennete serviceConsAnciennete;
	
	public EOMangueElementCarriere() {
		super();
		serviceConsAnciennete = new ServicesConsAnciennete();
	}

	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		EOElementCarriere element = (EOElementCarriere)recordImport;
		// A ce stade, la carrière est dans le SI Destinataire donc la correspondance créée
		EOMangueCarriere carriereMangue = ((EOCarriereCorresp)element.toCarriere().correspondance()).carriereMangue();
		setNoDossierPers(carriereMangue.individu().noIndividu());
		setNoSeqCarriere(carriereMangue.noSeqCarriere());
		setNoSeqElement(new Integer(obtenirNumeroSequence(carriereMangue)));
		
		if (element.repAncEchAnnees() != null
				|| element.repAncEchMois() != null
				|| element.repAncEchJours() != null) {
			EOGrhumIndividu grhumIndividu = EOIndividuCorresp.individuGrhum(element.editingContext(), element.individu());
			serviceConsAnciennete.intialisationServicesConsAnciennete(this, grhumIndividu, element);
		}
	}
	
	// Méthodes privées
	private int obtenirNumeroSequence(EOMangueCarriere carriere) {
		NSArray elements = rechercherElementsPourCarriere(editingContext(),carriere);
		elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements,new NSArray(EOSortOrdering.sortOrderingWithKey("noSeqElement",EOSortOrdering.CompareDescending)));
		try {
			EOMangueElementCarriere element = (EOMangueElementCarriere)elements.objectAtIndex(0);
			return element.noSeqElement().intValue() + 1;
		} catch (Exception e) {
			return 1;
		}
	}

	// Méthodes statiques
	/** Retourne l'element de carriere equivalent pour cet individu, ce segment de carriere 
	 * qui commence a la meme date.<BR>
	 * @param editingContext
	 * @param elementCarriere
	 * @return EOMangueElementCarriere ou null si non trouve
	 */
	public static EOMangueElementCarriere elementDestinationPourElement(EOEditingContext editingContext,EOElementCarriere element) {
		NSArray results = rechercherElementsManguePourElement(editingContext,element);
		// Pas de chevauchement des éléments de carrière valide
		if (results != null && results.count() == 1) {
			return (EOMangueElementCarriere)results.objectAtIndex(0);
		}	 else {
			return null;
		}
	}
	/** retourne tous les elements associes a un segment de carriere d'un individu y compris les invalides
	 * @param editingContext
	 * @param carriere carriere Mangue
	 */
	public static NSArray rechercherElementsPourCarriere(EOEditingContext editingContext,EOMangueCarriere carriere) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_CARRIERE_KEY + " = %@", new NSArray(carriere));
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** retourne  les elements associes a un segment de carriere pendant la periode fournie
	 * non provisoires et sans arrete d'annulation
	 * @param editingContext
	 * @param carriere carriere Mangue
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode   peut etre nulle
	 */
	public static NSArray rechercherElementsDestinValidesSurPeriode(EOEditingContext editingContext,EOMangueCarriere carriere,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(carriere);
		String stringQualifier = TO_CARRIERE_KEY + " = %@ AND temProvisoire = 'N' AND dAnnulation = NIL AND noArreteAnnulation = NIL AND temValide = 'O'";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		if (debutPeriode != null) {
			qualifier = Finder.qualifierPourPeriode(D_EFFET_ELEMENT_KEY, debutPeriode, D_FIN_ELEMENT_KEY,finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
		}
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);

		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	// Méthodes privée
	private static NSArray rechercherElementsManguePourElement(EOEditingContext editingContext,EOElementCarriere element) {
		if (element.toCarriere() == null) {
			return null;
		}
		// On recherche uniquement les valides
		EOMangueCarriere carriereMangue = (EOMangueCarriere)element.toCarriere().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (carriereMangue != null) {
			NSMutableArray args = new NSMutableArray(carriereMangue);
			// Vérifier que la date de début est la même : pour éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(element.dEffetElement()));
			args.addObject(DateCtrl.jourSuivant(element.dEffetElement()));
			String qualifier = TO_CARRIERE_KEY + " = %@ AND dEffetElement > %@ AND dEffetElement < %@ AND temValide = 'O'";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else {
			return null;
		}
	}
}
