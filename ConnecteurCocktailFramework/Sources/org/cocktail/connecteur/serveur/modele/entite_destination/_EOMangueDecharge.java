// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueDecharge.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueDecharge extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueDecharge";

	// Attributes
	public static final String C_TYPE_DECHARGE_KEY = "cTypeDecharge";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NB_H_DECHARGE_KEY = "nbHDecharge";
	public static final String PERIODE_DECHARGE_KEY = "periodeDecharge";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TYPE_DECHARGE_KEY = "typeDecharge";

  private static Logger LOG = Logger.getLogger(_EOMangueDecharge.class);

  public EOMangueDecharge localInstanceIn(EOEditingContext editingContext) {
    EOMangueDecharge localInstance = (EOMangueDecharge)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeDecharge() {
    return (String) storedValueForKey("cTypeDecharge");
  }

  public void setCTypeDecharge(String value) {
    if (_EOMangueDecharge.LOG.isDebugEnabled()) {
    	_EOMangueDecharge.LOG.debug( "updating cTypeDecharge from " + cTypeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeDecharge");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueDecharge.LOG.isDebugEnabled()) {
    	_EOMangueDecharge.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueDecharge.LOG.isDebugEnabled()) {
    	_EOMangueDecharge.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public java.math.BigDecimal nbHDecharge() {
    return (java.math.BigDecimal) storedValueForKey("nbHDecharge");
  }

  public void setNbHDecharge(java.math.BigDecimal value) {
    if (_EOMangueDecharge.LOG.isDebugEnabled()) {
    	_EOMangueDecharge.LOG.debug( "updating nbHDecharge from " + nbHDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "nbHDecharge");
  }

  public String periodeDecharge() {
    return (String) storedValueForKey("periodeDecharge");
  }

  public void setPeriodeDecharge(String value) {
    if (_EOMangueDecharge.LOG.isDebugEnabled()) {
    	_EOMangueDecharge.LOG.debug( "updating periodeDecharge from " + periodeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "periodeDecharge");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueDecharge.LOG.isDebugEnabled()) {
      _EOMangueDecharge.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeDechargeService typeDecharge() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeDechargeService)storedValueForKey("typeDecharge");
  }

  public void setTypeDechargeRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeDechargeService value) {
    if (_EOMangueDecharge.LOG.isDebugEnabled()) {
      _EOMangueDecharge.LOG.debug("updating typeDecharge from " + typeDecharge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeDechargeService oldValue = typeDecharge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeDecharge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeDecharge");
    }
  }
  

  public static EOMangueDecharge createMangueDecharge(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, java.math.BigDecimal nbHDecharge
, String periodeDecharge
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueDecharge eo = (EOMangueDecharge) EOUtilities.createAndInsertInstance(editingContext, _EOMangueDecharge.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNbHDecharge(nbHDecharge);
		eo.setPeriodeDecharge(periodeDecharge);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueDecharge> fetchAllMangueDecharges(EOEditingContext editingContext) {
    return _EOMangueDecharge.fetchAllMangueDecharges(editingContext, null);
  }

  public static NSArray<EOMangueDecharge> fetchAllMangueDecharges(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueDecharge.fetchMangueDecharges(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueDecharge> fetchMangueDecharges(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueDecharge.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueDecharge> eoObjects = (NSArray<EOMangueDecharge>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueDecharge fetchMangueDecharge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDecharge.fetchMangueDecharge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDecharge fetchMangueDecharge(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueDecharge> eoObjects = _EOMangueDecharge.fetchMangueDecharges(editingContext, qualifier, null);
    EOMangueDecharge eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueDecharge)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueDecharge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDecharge fetchRequiredMangueDecharge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDecharge.fetchRequiredMangueDecharge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDecharge fetchRequiredMangueDecharge(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueDecharge eoObject = _EOMangueDecharge.fetchMangueDecharge(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueDecharge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDecharge localInstanceIn(EOEditingContext editingContext, EOMangueDecharge eo) {
    EOMangueDecharge localInstance = (eo == null) ? null : (EOMangueDecharge)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
