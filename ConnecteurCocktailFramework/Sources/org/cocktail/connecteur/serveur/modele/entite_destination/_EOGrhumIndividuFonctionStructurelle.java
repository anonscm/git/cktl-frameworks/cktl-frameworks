// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumIndividuFonctionStructurelle.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumIndividuFonctionStructurelle extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "GrhumIndividuFonctionStructurelle";

	// Attributes
	public static final String C_FONCTION_KEY = "cFonction";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOGrhumIndividuFonctionStructurelle.class);

  public EOGrhumIndividuFonctionStructurelle localInstanceIn(EOEditingContext editingContext) {
    EOGrhumIndividuFonctionStructurelle localInstance = (EOGrhumIndividuFonctionStructurelle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cFonction() {
    return (Integer) storedValueForKey("cFonction");
  }

  public void setCFonction(Integer value) {
    if (_EOGrhumIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionStructurelle.LOG.debug( "updating cFonction from " + cFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "cFonction");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionStructurelle.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionStructurelle.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionStructurelle.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuFonctionStructurelle.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOGrhumIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
      _EOGrhumIndividuFonctionStructurelle.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOGrhumIndividuFonctionStructurelle.LOG.isDebugEnabled()) {
      _EOGrhumIndividuFonctionStructurelle.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOGrhumIndividuFonctionStructurelle createGrhumIndividuFonctionStructurelle(EOEditingContext editingContext, Integer cFonction
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure) {
    EOGrhumIndividuFonctionStructurelle eo = (EOGrhumIndividuFonctionStructurelle) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumIndividuFonctionStructurelle.ENTITY_NAME);    
		eo.setCFonction(cFonction);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    eo.setStructureRelationship(structure);
    return eo;
  }

  public static NSArray<EOGrhumIndividuFonctionStructurelle> fetchAllGrhumIndividuFonctionStructurelles(EOEditingContext editingContext) {
    return _EOGrhumIndividuFonctionStructurelle.fetchAllGrhumIndividuFonctionStructurelles(editingContext, null);
  }

  public static NSArray<EOGrhumIndividuFonctionStructurelle> fetchAllGrhumIndividuFonctionStructurelles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumIndividuFonctionStructurelle.fetchGrhumIndividuFonctionStructurelles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumIndividuFonctionStructurelle> fetchGrhumIndividuFonctionStructurelles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumIndividuFonctionStructurelle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumIndividuFonctionStructurelle> eoObjects = (NSArray<EOGrhumIndividuFonctionStructurelle>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumIndividuFonctionStructurelle fetchGrhumIndividuFonctionStructurelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividuFonctionStructurelle.fetchGrhumIndividuFonctionStructurelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividuFonctionStructurelle fetchGrhumIndividuFonctionStructurelle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumIndividuFonctionStructurelle> eoObjects = _EOGrhumIndividuFonctionStructurelle.fetchGrhumIndividuFonctionStructurelles(editingContext, qualifier, null);
    EOGrhumIndividuFonctionStructurelle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumIndividuFonctionStructurelle)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumIndividuFonctionStructurelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividuFonctionStructurelle fetchRequiredGrhumIndividuFonctionStructurelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividuFonctionStructurelle.fetchRequiredGrhumIndividuFonctionStructurelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividuFonctionStructurelle fetchRequiredGrhumIndividuFonctionStructurelle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumIndividuFonctionStructurelle eoObject = _EOGrhumIndividuFonctionStructurelle.fetchGrhumIndividuFonctionStructurelle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumIndividuFonctionStructurelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividuFonctionStructurelle localInstanceIn(EOEditingContext editingContext, EOGrhumIndividuFonctionStructurelle eo) {
    EOGrhumIndividuFonctionStructurelle localInstance = (eo == null) ? null : (EOGrhumIndividuFonctionStructurelle)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
