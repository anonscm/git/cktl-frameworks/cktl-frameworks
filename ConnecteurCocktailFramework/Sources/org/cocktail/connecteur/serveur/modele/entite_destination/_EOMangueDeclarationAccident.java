// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueDeclarationAccident.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueDeclarationAccident extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueDeclarationAccident";

	// Attributes
	public static final String C_TYPE_ACCIDENT_KEY = "cTypeAccident";
	public static final String DACC_CIRCONSTANCE_KEY = "daccCirconstance";
	public static final String DACC_DATE_KEY = "daccDate";
	public static final String DACC_HDEB_AM_KEY = "daccHdebAm";
	public static final String DACC_HDEB_PM_KEY = "daccHdebPm";
	public static final String DACC_HEURE_KEY = "daccHeure";
	public static final String DACC_HFIN_AM_KEY = "daccHfinAm";
	public static final String DACC_HFIN_PM_KEY = "daccHfinPm";
	public static final String DACC_JOUR_KEY = "daccJour";
	public static final String DACC_LOCALITE_KEY = "daccLocalite";
	public static final String DACC_SUITE_KEY = "daccSuite";
	public static final String DACC_TRANSPORT_KEY = "daccTransport";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LACC_ORDRE_KEY = "laccOrdre";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueDeclarationAccident.class);

  public EOMangueDeclarationAccident localInstanceIn(EOEditingContext editingContext) {
    EOMangueDeclarationAccident localInstance = (EOMangueDeclarationAccident)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cTypeAccident() {
    return (String) storedValueForKey("cTypeAccident");
  }

  public void setCTypeAccident(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating cTypeAccident from " + cTypeAccident() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAccident");
  }

  public String daccCirconstance() {
    return (String) storedValueForKey("daccCirconstance");
  }

  public void setDaccCirconstance(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccCirconstance from " + daccCirconstance() + " to " + value);
    }
    takeStoredValueForKey(value, "daccCirconstance");
  }

  public NSTimestamp daccDate() {
    return (NSTimestamp) storedValueForKey("daccDate");
  }

  public void setDaccDate(NSTimestamp value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccDate from " + daccDate() + " to " + value);
    }
    takeStoredValueForKey(value, "daccDate");
  }

  public String daccHdebAm() {
    return (String) storedValueForKey("daccHdebAm");
  }

  public void setDaccHdebAm(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccHdebAm from " + daccHdebAm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHdebAm");
  }

  public String daccHdebPm() {
    return (String) storedValueForKey("daccHdebPm");
  }

  public void setDaccHdebPm(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccHdebPm from " + daccHdebPm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHdebPm");
  }

  public String daccHeure() {
    return (String) storedValueForKey("daccHeure");
  }

  public void setDaccHeure(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccHeure from " + daccHeure() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHeure");
  }

  public String daccHfinAm() {
    return (String) storedValueForKey("daccHfinAm");
  }

  public void setDaccHfinAm(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccHfinAm from " + daccHfinAm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHfinAm");
  }

  public String daccHfinPm() {
    return (String) storedValueForKey("daccHfinPm");
  }

  public void setDaccHfinPm(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccHfinPm from " + daccHfinPm() + " to " + value);
    }
    takeStoredValueForKey(value, "daccHfinPm");
  }

  public String daccJour() {
    return (String) storedValueForKey("daccJour");
  }

  public void setDaccJour(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccJour from " + daccJour() + " to " + value);
    }
    takeStoredValueForKey(value, "daccJour");
  }

  public String daccLocalite() {
    return (String) storedValueForKey("daccLocalite");
  }

  public void setDaccLocalite(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccLocalite from " + daccLocalite() + " to " + value);
    }
    takeStoredValueForKey(value, "daccLocalite");
  }

  public String daccSuite() {
    return (String) storedValueForKey("daccSuite");
  }

  public void setDaccSuite(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccSuite from " + daccSuite() + " to " + value);
    }
    takeStoredValueForKey(value, "daccSuite");
  }

  public String daccTransport() {
    return (String) storedValueForKey("daccTransport");
  }

  public void setDaccTransport(String value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating daccTransport from " + daccTransport() + " to " + value);
    }
    takeStoredValueForKey(value, "daccTransport");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer laccOrdre() {
    return (Integer) storedValueForKey("laccOrdre");
  }

  public void setLaccOrdre(Integer value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
    	_EOMangueDeclarationAccident.LOG.debug( "updating laccOrdre from " + laccOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "laccOrdre");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueDeclarationAccident.LOG.isDebugEnabled()) {
      _EOMangueDeclarationAccident.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueDeclarationAccident createMangueDeclarationAccident(EOEditingContext editingContext, String cTypeAccident
, NSTimestamp daccDate
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueDeclarationAccident eo = (EOMangueDeclarationAccident) EOUtilities.createAndInsertInstance(editingContext, _EOMangueDeclarationAccident.ENTITY_NAME);    
		eo.setCTypeAccident(cTypeAccident);
		eo.setDaccDate(daccDate);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueDeclarationAccident> fetchAllMangueDeclarationAccidents(EOEditingContext editingContext) {
    return _EOMangueDeclarationAccident.fetchAllMangueDeclarationAccidents(editingContext, null);
  }

  public static NSArray<EOMangueDeclarationAccident> fetchAllMangueDeclarationAccidents(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueDeclarationAccident.fetchMangueDeclarationAccidents(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueDeclarationAccident> fetchMangueDeclarationAccidents(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueDeclarationAccident.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueDeclarationAccident> eoObjects = (NSArray<EOMangueDeclarationAccident>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueDeclarationAccident fetchMangueDeclarationAccident(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDeclarationAccident.fetchMangueDeclarationAccident(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDeclarationAccident fetchMangueDeclarationAccident(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueDeclarationAccident> eoObjects = _EOMangueDeclarationAccident.fetchMangueDeclarationAccidents(editingContext, qualifier, null);
    EOMangueDeclarationAccident eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueDeclarationAccident)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueDeclarationAccident that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDeclarationAccident fetchRequiredMangueDeclarationAccident(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDeclarationAccident.fetchRequiredMangueDeclarationAccident(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDeclarationAccident fetchRequiredMangueDeclarationAccident(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueDeclarationAccident eoObject = _EOMangueDeclarationAccident.fetchMangueDeclarationAccident(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueDeclarationAccident that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDeclarationAccident localInstanceIn(EOEditingContext editingContext, EOMangueDeclarationAccident eo) {
    EOMangueDeclarationAccident localInstance = (eo == null) ? null : (EOMangueDeclarationAccident)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
