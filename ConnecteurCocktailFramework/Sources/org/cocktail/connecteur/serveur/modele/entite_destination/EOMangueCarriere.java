//EOMangueCarriere.java
//Created on Mon Jan 07 16:34:30 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueCarriere extends _EOMangueCarriere  {

	public EOMangueCarriere() {
		super();
	}
	
	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOCarriere carriereImport = (EOCarriere)recordImport;
		// A ce stade, l'individu grhum est dans le SI Destinataire
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), carriereImport.individu().idSource());
		if (individuGrhum != null && individuGrhum.personnel() != null) {
			addObjectToBothSidesOfRelationshipWithKey(individuGrhum, "individu");
			setNoDossierPers(individuGrhum.noIndividu());
			if (individuGrhum.personnel().temTitulaire() == null || individuGrhum.personnel().temTitulaire().equals(CocktailConstantes.FAUX)) {
				individuGrhum.personnel().setTemTitulaire(CocktailConstantes.VRAI);
			}
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + carriereImport.individu().idSource());
		}
		// On le fait après pour que la relation individu() soit initialisée
		//setNoSeqCarriere(new Integer(obtenirNumeroSeqCarriere()));
		setNoSeqCarriere(carriereImport.carSource());
	}
	// Méthodes privées
	private int obtenirNumeroSeqCarriere() {
		NSArray carrieres = EOMangueCarriere.rechercherCarrieresPourIndividu(editingContext(),individu(),false);
		// Les trier par orde de numéro de carrière décroissant
		carrieres = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres,new NSArray(EOSortOrdering.sortOrderingWithKey("noSeqCarriere",EOSortOrdering.CompareDescending)));
		try {
			EOMangueCarriere carriere = (EOMangueCarriere)carrieres.objectAtIndex(0);
			return carriere.noSeqCarriere().intValue() + 1;
		} catch (Exception e) {
			return 1;
		}
	}
	// Méthodes statiques
	/** Retourne true si on trouve plusieurs carrieres commencant a la meme date */
	public static boolean aHomonyme(EOEditingContext editingContext,EOCarriere carriere) {
		NSArray results = rechercherCarrieresManguePourCarriere(editingContext,carriere,true);
		return (results != null && results.count() > 1);
	}
	/** retourne les carrieres d'un individu
	 * @param editingContext
	 * @param individu individu Ghrum
	 * @param prefetch true si effectuer les prefetchs
	 */
	public static NSArray rechercherCarrieresPourIndividu(EOEditingContext editingContext,EOGrhumIndividu individu,boolean prefetch) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu));
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		if (prefetch) {
			NSMutableArray relations = new NSMutableArray("elements");
			relations.addObject("changementsPosition");
			myFetch.setPrefetchingRelationshipKeyPaths(relations);
		}
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** retourne les carri&egrave;res d'un individu pendant la periode fournie
	 * @param editingContext
	 * @param individu individu Ghrum
	 * @param debutPeriode peut &ecirc;tre nulle
	 * @param finPeriode	 peut &ecirc;tre nulle
	 * @param prefetch true si effectuer les prefetchs
	 */
	public static NSArray rechercherCarrieresDestinValidesSurPeriode(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOQualifier qualifier = qualifierPourPeriode(individu,debutPeriode,finPeriode,true);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** retourne les carri&egrave;res d'un individu valides avant la date fournie
	 * @param editingContext
	 * @param individu individu Ghrum
	 * @param date
	 */
	public static NSArray rechercherCarrieresDestinValidesAvantDate(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp date) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(CocktailConstantes.VRAI);
		args.addObject(date);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ AND temValide = %@ AND dDebCarriere < %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);

		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** retourne le qualifier pour determiner les segments de carriere sur une periode
	 * @param individu Grhum
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode	 peut etre nulle
	 * @param seulementCarrieresValides true si on ne recherche que les carrières valides
	 */
	public static EOQualifier qualifierPourPeriode(EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean seulementCarrieresValides) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(individu);
		String stringQualifier = INDIVIDU_KEY + " = %@";
		if (seulementCarrieresValides) {
			stringQualifier = stringQualifier + " AND temValide = 'O'";
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		if (debutPeriode != null) {
			qualifier = Finder.qualifierPourPeriode(D_DEB_CARRIERE_KEY,debutPeriode, D_FIN_CARRIERE_KEY,finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
		}
		return qualifier;
	}
	/** Recherche les segments de carri&grave;re valides de Mangue pour le m&rcirc;me individu, la m&ecirc;me date debut et le
	 * m&circ;me type de population (sauf dans le cas o&ugrave; on recherche des homonymes)
	 * @param editingContext
	 * @param carriere
	 * @param rechercherHomonymes true si on recherche les homonymes
	 * @return
	 */
	public static NSArray rechercherCarrieresManguePourCarriere(EOEditingContext editingContext,EOCarriere carriere,boolean rechercherHomonymes) {
		// Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		// On recherche uniquement les objets valides
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)carriere.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray args = new NSMutableArray(individuGrhum);
			// Vérifier que la date de début de carrière est la même : pour éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(carriere.dDebCarriere()));
			args.addObject(DateCtrl.jourSuivant(carriere.dDebCarriere()));
			String qualifier = INDIVIDU_KEY + " = %@ AND dDebCarriere > %@ AND dDebCarriere < %@ AND temValide = 'O'";
			if (rechercherHomonymes == false) {
				args.addObject(carriere.cTypePopulation());
				// on recherche sur le type de population
				qualifier = qualifier + " AND cTypePopulation = %@";
			}
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		}
		return null;
	}


}
