// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumPersonnel.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumPersonnel extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumPersonnel";

	// Attributes
	public static final String AFFECTE_DEFENSE_KEY = "affecteDefense";
	public static final String CIR_D_CERTIFICATION_KEY = "cirDCertification";
	public static final String CIR_D_COMPLETUDE_KEY = "cirDCompletude";
	public static final String CIR_D_VERIFICATION_KEY = "cirDVerification";
	public static final String C_LOGE_KEY = "cLoge";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_SITUATION_FAMILLE_KEY = "dSituationFamille";
	public static final String ID_MINISTERE_KEY = "idMinistere";
	public static final String NB_ENFANTS_KEY = "nbEnfants";
	public static final String NB_ENFANTS_A_CHARGE_KEY = "nbEnfantsACharge";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_EPICEA_KEY = "noEpicea";
	public static final String NO_MATRICULE_KEY = "noMatricule";
	public static final String NUMEN_KEY = "numen";
	public static final String TEM_BUDGET_ETAT_KEY = "temBudgetEtat";
	public static final String TEM_IMPOSABLE_KEY = "temImposable";
	public static final String TEM_PAIE_SECU_KEY = "temPaieSecu";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TXT_LIBRE_KEY = "txtLibre";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOGrhumPersonnel.class);

  public EOGrhumPersonnel localInstanceIn(EOEditingContext editingContext) {
    EOGrhumPersonnel localInstance = (EOGrhumPersonnel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String affecteDefense() {
    return (String) storedValueForKey("affecteDefense");
  }

  public void setAffecteDefense(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating affecteDefense from " + affecteDefense() + " to " + value);
    }
    takeStoredValueForKey(value, "affecteDefense");
  }

  public NSTimestamp cirDCertification() {
    return (NSTimestamp) storedValueForKey("cirDCertification");
  }

  public void setCirDCertification(NSTimestamp value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating cirDCertification from " + cirDCertification() + " to " + value);
    }
    takeStoredValueForKey(value, "cirDCertification");
  }

  public NSTimestamp cirDCompletude() {
    return (NSTimestamp) storedValueForKey("cirDCompletude");
  }

  public void setCirDCompletude(NSTimestamp value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating cirDCompletude from " + cirDCompletude() + " to " + value);
    }
    takeStoredValueForKey(value, "cirDCompletude");
  }

  public NSTimestamp cirDVerification() {
    return (NSTimestamp) storedValueForKey("cirDVerification");
  }

  public void setCirDVerification(NSTimestamp value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating cirDVerification from " + cirDVerification() + " to " + value);
    }
    takeStoredValueForKey(value, "cirDVerification");
  }

  public String cLoge() {
    return (String) storedValueForKey("cLoge");
  }

  public void setCLoge(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating cLoge from " + cLoge() + " to " + value);
    }
    takeStoredValueForKey(value, "cLoge");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dSituationFamille() {
    return (NSTimestamp) storedValueForKey("dSituationFamille");
  }

  public void setDSituationFamille(NSTimestamp value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating dSituationFamille from " + dSituationFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "dSituationFamille");
  }

  public Integer idMinistere() {
    return (Integer) storedValueForKey("idMinistere");
  }

  public void setIdMinistere(Integer value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating idMinistere from " + idMinistere() + " to " + value);
    }
    takeStoredValueForKey(value, "idMinistere");
  }

  public Integer nbEnfants() {
    return (Integer) storedValueForKey("nbEnfants");
  }

  public void setNbEnfants(Integer value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating nbEnfants from " + nbEnfants() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfants");
  }

  public Integer nbEnfantsACharge() {
    return (Integer) storedValueForKey("nbEnfantsACharge");
  }

  public void setNbEnfantsACharge(Integer value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating nbEnfantsACharge from " + nbEnfantsACharge() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfantsACharge");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public String noEpicea() {
    return (String) storedValueForKey("noEpicea");
  }

  public void setNoEpicea(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating noEpicea from " + noEpicea() + " to " + value);
    }
    takeStoredValueForKey(value, "noEpicea");
  }

  public String noMatricule() {
    return (String) storedValueForKey("noMatricule");
  }

  public void setNoMatricule(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating noMatricule from " + noMatricule() + " to " + value);
    }
    takeStoredValueForKey(value, "noMatricule");
  }

  public String numen() {
    return (String) storedValueForKey("numen");
  }

  public void setNumen(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating numen from " + numen() + " to " + value);
    }
    takeStoredValueForKey(value, "numen");
  }

  public String temBudgetEtat() {
    return (String) storedValueForKey("temBudgetEtat");
  }

  public void setTemBudgetEtat(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating temBudgetEtat from " + temBudgetEtat() + " to " + value);
    }
    takeStoredValueForKey(value, "temBudgetEtat");
  }

  public String temImposable() {
    return (String) storedValueForKey("temImposable");
  }

  public void setTemImposable(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating temImposable from " + temImposable() + " to " + value);
    }
    takeStoredValueForKey(value, "temImposable");
  }

  public String temPaieSecu() {
    return (String) storedValueForKey("temPaieSecu");
  }

  public void setTemPaieSecu(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating temPaieSecu from " + temPaieSecu() + " to " + value);
    }
    takeStoredValueForKey(value, "temPaieSecu");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String txtLibre() {
    return (String) storedValueForKey("txtLibre");
  }

  public void setTxtLibre(String value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
    	_EOGrhumPersonnel.LOG.debug( "updating txtLibre from " + txtLibre() + " to " + value);
    }
    takeStoredValueForKey(value, "txtLibre");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOGrhumPersonnel.LOG.isDebugEnabled()) {
      _EOGrhumPersonnel.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOGrhumPersonnel createGrhumPersonnel(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, String temImposable
, String temPaieSecu
, String temTitulaire
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOGrhumPersonnel eo = (EOGrhumPersonnel) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumPersonnel.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setTemImposable(temImposable);
		eo.setTemPaieSecu(temPaieSecu);
		eo.setTemTitulaire(temTitulaire);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOGrhumPersonnel> fetchAllGrhumPersonnels(EOEditingContext editingContext) {
    return _EOGrhumPersonnel.fetchAllGrhumPersonnels(editingContext, null);
  }

  public static NSArray<EOGrhumPersonnel> fetchAllGrhumPersonnels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumPersonnel.fetchGrhumPersonnels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumPersonnel> fetchGrhumPersonnels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumPersonnel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumPersonnel> eoObjects = (NSArray<EOGrhumPersonnel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumPersonnel fetchGrhumPersonnel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPersonnel.fetchGrhumPersonnel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPersonnel fetchGrhumPersonnel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumPersonnel> eoObjects = _EOGrhumPersonnel.fetchGrhumPersonnels(editingContext, qualifier, null);
    EOGrhumPersonnel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumPersonnel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumPersonnel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPersonnel fetchRequiredGrhumPersonnel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPersonnel.fetchRequiredGrhumPersonnel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPersonnel fetchRequiredGrhumPersonnel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumPersonnel eoObject = _EOGrhumPersonnel.fetchGrhumPersonnel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumPersonnel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPersonnel localInstanceIn(EOEditingContext editingContext, EOGrhumPersonnel eo) {
    EOGrhumPersonnel localInstance = (eo == null) ? null : (EOGrhumPersonnel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
