// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueBonifications.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueBonifications extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueBonifications";

	// Attributes
	public static final String BONI_DUREE_KEY = "boniDuree";
	public static final String DATE_ARRIVEE_KEY = "dateArrivee";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String NATURE_BONIFICATION_KEY = "natureBonification";
	public static final String TAUX_KEY = "taux";
	public static final String TERRITOIRE_KEY = "territoire";

  private static Logger LOG = Logger.getLogger(_EOMangueBonifications.class);

  public EOMangueBonifications localInstanceIn(EOEditingContext editingContext) {
    EOMangueBonifications localInstance = (EOMangueBonifications)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String boniDuree() {
    return (String) storedValueForKey("boniDuree");
  }

  public void setBoniDuree(String value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
    	_EOMangueBonifications.LOG.debug( "updating boniDuree from " + boniDuree() + " to " + value);
    }
    takeStoredValueForKey(value, "boniDuree");
  }

  public NSTimestamp dateArrivee() {
    return (NSTimestamp) storedValueForKey("dateArrivee");
  }

  public void setDateArrivee(NSTimestamp value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
    	_EOMangueBonifications.LOG.debug( "updating dateArrivee from " + dateArrivee() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrivee");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
    	_EOMangueBonifications.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
    	_EOMangueBonifications.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
    	_EOMangueBonifications.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
    	_EOMangueBonifications.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
    	_EOMangueBonifications.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
      _EOMangueBonifications.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification natureBonification() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification)storedValueForKey("natureBonification");
  }

  public void setNatureBonificationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
      _EOMangueBonifications.LOG.debug("updating natureBonification from " + natureBonification() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification oldValue = natureBonification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "natureBonification");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "natureBonification");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux taux() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux)storedValueForKey("taux");
  }

  public void setTauxRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
      _EOMangueBonifications.LOG.debug("updating taux from " + taux() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTaux oldValue = taux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "taux");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "taux");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire territoire() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire)storedValueForKey("territoire");
  }

  public void setTerritoireRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire value) {
    if (_EOMangueBonifications.LOG.isDebugEnabled()) {
      _EOMangueBonifications.LOG.debug("updating territoire from " + territoire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonifTerritoire oldValue = territoire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "territoire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "territoire");
    }
  }
  

  public static EOMangueBonifications createMangueBonifications(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBonification natureBonification) {
    EOMangueBonifications eo = (EOMangueBonifications) EOUtilities.createAndInsertInstance(editingContext, _EOMangueBonifications.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setNatureBonificationRelationship(natureBonification);
    return eo;
  }

  public static NSArray<EOMangueBonifications> fetchAllMangueBonificationses(EOEditingContext editingContext) {
    return _EOMangueBonifications.fetchAllMangueBonificationses(editingContext, null);
  }

  public static NSArray<EOMangueBonifications> fetchAllMangueBonificationses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueBonifications.fetchMangueBonificationses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueBonifications> fetchMangueBonificationses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueBonifications.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueBonifications> eoObjects = (NSArray<EOMangueBonifications>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueBonifications fetchMangueBonifications(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueBonifications.fetchMangueBonifications(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueBonifications fetchMangueBonifications(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueBonifications> eoObjects = _EOMangueBonifications.fetchMangueBonificationses(editingContext, qualifier, null);
    EOMangueBonifications eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueBonifications)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueBonifications that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueBonifications fetchRequiredMangueBonifications(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueBonifications.fetchRequiredMangueBonifications(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueBonifications fetchRequiredMangueBonifications(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueBonifications eoObject = _EOMangueBonifications.fetchMangueBonifications(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueBonifications that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueBonifications localInstanceIn(EOEditingContext editingContext, EOMangueBonifications eo) {
    EOMangueBonifications localInstance = (eo == null) ? null : (EOMangueBonifications)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
