// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueIndividuDiplomes.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueIndividuDiplomes extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueIndividuDiplomes";

	// Attributes
	public static final String C_DIPLOME_KEY = "cDiplome";
	public static final String C_TITULAIRE_DIPLOME_KEY = "cTitulaireDiplome";
	public static final String C_UAI_OBTENTION_KEY = "cUaiObtention";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DIPLOME_KEY = "dDiplome";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_DIPLOME_KEY = "lieuDiplome";
	public static final String SPECIALITE_KEY = "specialite";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueIndividuDiplomes.class);

  public EOMangueIndividuDiplomes localInstanceIn(EOEditingContext editingContext) {
    EOMangueIndividuDiplomes localInstance = (EOMangueIndividuDiplomes)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDiplome() {
    return (String) storedValueForKey("cDiplome");
  }

  public void setCDiplome(String value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating cDiplome from " + cDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiplome");
  }

  public String cTitulaireDiplome() {
    return (String) storedValueForKey("cTitulaireDiplome");
  }

  public void setCTitulaireDiplome(String value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating cTitulaireDiplome from " + cTitulaireDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "cTitulaireDiplome");
  }

  public String cUaiObtention() {
    return (String) storedValueForKey("cUaiObtention");
  }

  public void setCUaiObtention(String value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating cUaiObtention from " + cUaiObtention() + " to " + value);
    }
    takeStoredValueForKey(value, "cUaiObtention");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDiplome() {
    return (NSTimestamp) storedValueForKey("dDiplome");
  }

  public void setDDiplome(NSTimestamp value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating dDiplome from " + dDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "dDiplome");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lieuDiplome() {
    return (String) storedValueForKey("lieuDiplome");
  }

  public void setLieuDiplome(String value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating lieuDiplome from " + lieuDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDiplome");
  }

  public String specialite() {
    return (String) storedValueForKey("specialite");
  }

  public void setSpecialite(String value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating specialite from " + specialite() + " to " + value);
    }
    takeStoredValueForKey(value, "specialite");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
    	_EOMangueIndividuDiplomes.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueIndividuDiplomes.LOG.isDebugEnabled()) {
      _EOMangueIndividuDiplomes.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueIndividuDiplomes createMangueIndividuDiplomes(EOEditingContext editingContext, String cDiplome
, String cTitulaireDiplome
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueIndividuDiplomes eo = (EOMangueIndividuDiplomes) EOUtilities.createAndInsertInstance(editingContext, _EOMangueIndividuDiplomes.ENTITY_NAME);    
		eo.setCDiplome(cDiplome);
		eo.setCTitulaireDiplome(cTitulaireDiplome);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueIndividuDiplomes> fetchAllMangueIndividuDiplomeses(EOEditingContext editingContext) {
    return _EOMangueIndividuDiplomes.fetchAllMangueIndividuDiplomeses(editingContext, null);
  }

  public static NSArray<EOMangueIndividuDiplomes> fetchAllMangueIndividuDiplomeses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueIndividuDiplomes.fetchMangueIndividuDiplomeses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueIndividuDiplomes> fetchMangueIndividuDiplomeses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueIndividuDiplomes.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueIndividuDiplomes> eoObjects = (NSArray<EOMangueIndividuDiplomes>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueIndividuDiplomes fetchMangueIndividuDiplomes(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueIndividuDiplomes.fetchMangueIndividuDiplomes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueIndividuDiplomes fetchMangueIndividuDiplomes(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueIndividuDiplomes> eoObjects = _EOMangueIndividuDiplomes.fetchMangueIndividuDiplomeses(editingContext, qualifier, null);
    EOMangueIndividuDiplomes eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueIndividuDiplomes)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueIndividuDiplomes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueIndividuDiplomes fetchRequiredMangueIndividuDiplomes(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueIndividuDiplomes.fetchRequiredMangueIndividuDiplomes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueIndividuDiplomes fetchRequiredMangueIndividuDiplomes(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueIndividuDiplomes eoObject = _EOMangueIndividuDiplomes.fetchMangueIndividuDiplomes(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueIndividuDiplomes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueIndividuDiplomes localInstanceIn(EOEditingContext editingContext, EOMangueIndividuDiplomes eo) {
    EOMangueIndividuDiplomes localInstance = (eo == null) ? null : (EOMangueIndividuDiplomes)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
