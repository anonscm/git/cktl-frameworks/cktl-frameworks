package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.Finder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueDelegation extends _EOMangueDelegation {
  private static Logger log = Logger.getLogger(EOMangueDelegation.class);
  
  
	/** Recherche des delegations d'un individu pendant une periode donnee 
	 * @param individu Grhum
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 * @return retourne les temps partiels trouves et tous les tp si individu et debutPeriode sont nuls
	 */
	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}
		
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

}
