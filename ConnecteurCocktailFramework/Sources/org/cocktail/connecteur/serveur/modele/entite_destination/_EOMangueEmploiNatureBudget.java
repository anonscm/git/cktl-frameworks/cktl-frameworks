// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueEmploiNatureBudget.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueEmploiNatureBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueEmploiNatureBudget";

	// Attributes
	public static final String C_BUDGET_KEY = "cBudget";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEBUT_KEY = "dDebut";
	public static final String D_FIN_KEY = "dFin";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_EMPLOI_KEY = "idEmploi";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	// Relationships
	public static final String TO_MANGUE_EMPLOI_KEY = "toMangueEmploi";
	public static final String TO_NATURE_BUDGET_KEY = "toNatureBudget";

  private static Logger LOG = Logger.getLogger(_EOMangueEmploiNatureBudget.class);

  public EOMangueEmploiNatureBudget localInstanceIn(EOEditingContext editingContext) {
    EOMangueEmploiNatureBudget localInstance = (EOMangueEmploiNatureBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBudget() {
    return (String) storedValueForKey("cBudget");
  }

  public void setCBudget(String value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating cBudget from " + cBudget() + " to " + value);
    }
    takeStoredValueForKey(value, "cBudget");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebut() {
    return (NSTimestamp) storedValueForKey("dDebut");
  }

  public void setDDebut(NSTimestamp value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating dDebut from " + dDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebut");
  }

  public NSTimestamp dFin() {
    return (NSTimestamp) storedValueForKey("dFin");
  }

  public void setDFin(NSTimestamp value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating dFin from " + dFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dFin");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idEmploi() {
    return (Integer) storedValueForKey("idEmploi");
  }

  public void setIdEmploi(Integer value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating idEmploi from " + idEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "idEmploi");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
    	_EOMangueEmploiNatureBudget.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toMangueEmploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi)storedValueForKey("toMangueEmploi");
  }

  public void setToMangueEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
      _EOMangueEmploiNatureBudget.LOG.debug("updating toMangueEmploi from " + toMangueEmploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi oldValue = toMangueEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueEmploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueEmploi");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBudget toNatureBudget() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBudget)storedValueForKey("toNatureBudget");
  }

  public void setToNatureBudgetRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBudget value) {
    if (_EOMangueEmploiNatureBudget.LOG.isDebugEnabled()) {
      _EOMangueEmploiNatureBudget.LOG.debug("updating toNatureBudget from " + toNatureBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBudget oldValue = toNatureBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toNatureBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toNatureBudget");
    }
  }
  

  public static EOMangueEmploiNatureBudget createMangueEmploiNatureBudget(EOEditingContext editingContext, String cBudget
, NSTimestamp dCreation
, NSTimestamp dDebut
, NSTimestamp dModification
, Integer idEmploi
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toMangueEmploi, org.cocktail.connecteur.common.modele.grhum.nomenclature.EONatureBudget toNatureBudget) {
    EOMangueEmploiNatureBudget eo = (EOMangueEmploiNatureBudget) EOUtilities.createAndInsertInstance(editingContext, _EOMangueEmploiNatureBudget.ENTITY_NAME);    
		eo.setCBudget(cBudget);
		eo.setDCreation(dCreation);
		eo.setDDebut(dDebut);
		eo.setDModification(dModification);
		eo.setIdEmploi(idEmploi);
    eo.setToMangueEmploiRelationship(toMangueEmploi);
    eo.setToNatureBudgetRelationship(toNatureBudget);
    return eo;
  }

  public static NSArray<EOMangueEmploiNatureBudget> fetchAllMangueEmploiNatureBudgets(EOEditingContext editingContext) {
    return _EOMangueEmploiNatureBudget.fetchAllMangueEmploiNatureBudgets(editingContext, null);
  }

  public static NSArray<EOMangueEmploiNatureBudget> fetchAllMangueEmploiNatureBudgets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueEmploiNatureBudget.fetchMangueEmploiNatureBudgets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueEmploiNatureBudget> fetchMangueEmploiNatureBudgets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueEmploiNatureBudget.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueEmploiNatureBudget> eoObjects = (NSArray<EOMangueEmploiNatureBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueEmploiNatureBudget fetchMangueEmploiNatureBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiNatureBudget.fetchMangueEmploiNatureBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiNatureBudget fetchMangueEmploiNatureBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueEmploiNatureBudget> eoObjects = _EOMangueEmploiNatureBudget.fetchMangueEmploiNatureBudgets(editingContext, qualifier, null);
    EOMangueEmploiNatureBudget eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueEmploiNatureBudget)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueEmploiNatureBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiNatureBudget fetchRequiredMangueEmploiNatureBudget(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiNatureBudget.fetchRequiredMangueEmploiNatureBudget(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiNatureBudget fetchRequiredMangueEmploiNatureBudget(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueEmploiNatureBudget eoObject = _EOMangueEmploiNatureBudget.fetchMangueEmploiNatureBudget(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueEmploiNatureBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiNatureBudget localInstanceIn(EOEditingContext editingContext, EOMangueEmploiNatureBudget eo) {
    EOMangueEmploiNatureBudget localInstance = (eo == null) ? null : (EOMangueEmploiNatureBudget)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
