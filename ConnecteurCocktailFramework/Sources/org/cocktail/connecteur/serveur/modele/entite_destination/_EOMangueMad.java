// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueMad.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueMad extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueMad";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TYPE_MAD_KEY = "cTypeMad";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_MAD_KEY = "lieuMad";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String QUOTITE_KEY = "quotite";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueMad.class);

  public EOMangueMad localInstanceIn(EOEditingContext editingContext) {
    EOMangueMad localInstance = (EOMangueMad)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cTypeMad() {
    return (String) storedValueForKey("cTypeMad");
  }

  public void setCTypeMad(String value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating cTypeMad from " + cTypeMad() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeMad");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lieuMad() {
    return (String) storedValueForKey("lieuMad");
  }

  public void setLieuMad(String value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating lieuMad from " + lieuMad() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuMad");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Double quotite() {
    return (Double) storedValueForKey("quotite");
  }

  public void setQuotite(Double value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
    	_EOMangueMad.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueMad.LOG.isDebugEnabled()) {
      _EOMangueMad.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueMad createMangueMad(EOEditingContext editingContext, String cTypeMad
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueMad eo = (EOMangueMad) EOUtilities.createAndInsertInstance(editingContext, _EOMangueMad.ENTITY_NAME);    
		eo.setCTypeMad(cTypeMad);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueMad> fetchAllMangueMads(EOEditingContext editingContext) {
    return _EOMangueMad.fetchAllMangueMads(editingContext, null);
  }

  public static NSArray<EOMangueMad> fetchAllMangueMads(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueMad.fetchMangueMads(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueMad> fetchMangueMads(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueMad.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueMad> eoObjects = (NSArray<EOMangueMad>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueMad fetchMangueMad(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueMad.fetchMangueMad(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueMad fetchMangueMad(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueMad> eoObjects = _EOMangueMad.fetchMangueMads(editingContext, qualifier, null);
    EOMangueMad eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueMad)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueMad that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueMad fetchRequiredMangueMad(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueMad.fetchRequiredMangueMad(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueMad fetchRequiredMangueMad(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueMad eoObject = _EOMangueMad.fetchMangueMad(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueMad that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueMad localInstanceIn(EOEditingContext editingContext, EOMangueMad eo) {
    EOMangueMad localInstance = (eo == null) ? null : (EOMangueMad)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
