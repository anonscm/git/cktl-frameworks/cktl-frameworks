// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueTempsPartiel.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueTempsPartiel extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueTempsPartiel";

	// Attributes
	public static final String C_MOTIF_TEMPS_PARTIEL_KEY = "cMotifTempsPartiel";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEN_QUOTITE_KEY = "denQuotite";
	public static final String D_FIN_EXECUTION_KEY = "dFinExecution";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String PERIODICITE_KEY = "periodicite";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_REPRISE_TEMPS_PLEIN_KEY = "temRepriseTempsPlein";
	public static final String TEM_SURCOTISATION_KEY = "temSurcotisation";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ARRETE_ANNULATION_KEY = "arreteAnnulation";
	public static final String ENFANT_KEY = "enfant";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueTempsPartiel.class);

  public EOMangueTempsPartiel localInstanceIn(EOEditingContext editingContext) {
    EOMangueTempsPartiel localInstance = (EOMangueTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifTempsPartiel() {
    return (String) storedValueForKey("cMotifTempsPartiel");
  }

  public void setCMotifTempsPartiel(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating cMotifTempsPartiel from " + cMotifTempsPartiel() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifTempsPartiel");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer denQuotite() {
    return (Integer) storedValueForKey("denQuotite");
  }

  public void setDenQuotite(Integer value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating denQuotite from " + denQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotite");
  }

  public NSTimestamp dFinExecution() {
    return (NSTimestamp) storedValueForKey("dFinExecution");
  }

  public void setDFinExecution(NSTimestamp value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating dFinExecution from " + dFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinExecution");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public Integer numQuotite() {
    return (Integer) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Integer value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }

  public Integer periodicite() {
    return (Integer) storedValueForKey("periodicite");
  }

  public void setPeriodicite(Integer value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating periodicite from " + periodicite() + " to " + value);
    }
    takeStoredValueForKey(value, "periodicite");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temRepriseTempsPlein() {
    return (String) storedValueForKey("temRepriseTempsPlein");
  }

  public void setTemRepriseTempsPlein(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating temRepriseTempsPlein from " + temRepriseTempsPlein() + " to " + value);
    }
    takeStoredValueForKey(value, "temRepriseTempsPlein");
  }

  public String temSurcotisation() {
    return (String) storedValueForKey("temSurcotisation");
  }

  public void setTemSurcotisation(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating temSurcotisation from " + temSurcotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "temSurcotisation");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartiel.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel arreteAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel)storedValueForKey("arreteAnnulation");
  }

  public void setArreteAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
      _EOMangueTempsPartiel.LOG.debug("updating arreteAnnulation from " + arreteAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel oldValue = arreteAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "arreteAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "arreteAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant enfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("enfant");
  }

  public void setEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
      _EOMangueTempsPartiel.LOG.debug("updating enfant from " + enfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueTempsPartiel.LOG.isDebugEnabled()) {
      _EOMangueTempsPartiel.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueTempsPartiel createMangueTempsPartiel(EOEditingContext editingContext, String cMotifTempsPartiel
, NSTimestamp dateDebut
, NSTimestamp dCreation
, Integer denQuotite
, NSTimestamp dModification
, Integer numQuotite
, Integer periodicite
, String temConfirme
, String temGestEtab
, String temRepriseTempsPlein
, String temSurcotisation
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueTempsPartiel eo = (EOMangueTempsPartiel) EOUtilities.createAndInsertInstance(editingContext, _EOMangueTempsPartiel.ENTITY_NAME);    
		eo.setCMotifTempsPartiel(cMotifTempsPartiel);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDenQuotite(denQuotite);
		eo.setDModification(dModification);
		eo.setNumQuotite(numQuotite);
		eo.setPeriodicite(periodicite);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemRepriseTempsPlein(temRepriseTempsPlein);
		eo.setTemSurcotisation(temSurcotisation);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueTempsPartiel> fetchAllMangueTempsPartiels(EOEditingContext editingContext) {
    return _EOMangueTempsPartiel.fetchAllMangueTempsPartiels(editingContext, null);
  }

  public static NSArray<EOMangueTempsPartiel> fetchAllMangueTempsPartiels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueTempsPartiel.fetchMangueTempsPartiels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueTempsPartiel> fetchMangueTempsPartiels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueTempsPartiel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueTempsPartiel> eoObjects = (NSArray<EOMangueTempsPartiel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueTempsPartiel fetchMangueTempsPartiel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueTempsPartiel.fetchMangueTempsPartiel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueTempsPartiel fetchMangueTempsPartiel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueTempsPartiel> eoObjects = _EOMangueTempsPartiel.fetchMangueTempsPartiels(editingContext, qualifier, null);
    EOMangueTempsPartiel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueTempsPartiel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueTempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueTempsPartiel fetchRequiredMangueTempsPartiel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueTempsPartiel.fetchRequiredMangueTempsPartiel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueTempsPartiel fetchRequiredMangueTempsPartiel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueTempsPartiel eoObject = _EOMangueTempsPartiel.fetchMangueTempsPartiel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueTempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueTempsPartiel localInstanceIn(EOEditingContext editingContext, EOMangueTempsPartiel eo) {
    EOMangueTempsPartiel localInstance = (eo == null) ? null : (EOMangueTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
