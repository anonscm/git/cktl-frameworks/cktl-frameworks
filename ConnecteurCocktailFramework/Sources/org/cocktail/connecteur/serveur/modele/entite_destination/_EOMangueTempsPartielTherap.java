// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueTempsPartielTherap.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueTempsPartielTherap extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueTempsPartielTherap";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_MTTH_KEY = "dComMedMtth";
	public static final String D_COM_MED_SUP_MTTH_KEY = "dComMedSupMtth";
	public static final String D_COM_REFORME_KEY = "dComReforme";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String MTT_QUOTITE_KEY = "mttQuotite";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueTempsPartielTherap.class);

  public EOMangueTempsPartielTherap localInstanceIn(EOEditingContext editingContext) {
    EOMangueTempsPartielTherap localInstance = (EOMangueTempsPartielTherap)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedMtth() {
    return (NSTimestamp) storedValueForKey("dComMedMtth");
  }

  public void setDComMedMtth(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dComMedMtth from " + dComMedMtth() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedMtth");
  }

  public NSTimestamp dComMedSupMtth() {
    return (NSTimestamp) storedValueForKey("dComMedSupMtth");
  }

  public void setDComMedSupMtth(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dComMedSupMtth from " + dComMedSupMtth() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedSupMtth");
  }

  public NSTimestamp dComReforme() {
    return (NSTimestamp) storedValueForKey("dComReforme");
  }

  public void setDComReforme(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dComReforme from " + dComReforme() + " to " + value);
    }
    takeStoredValueForKey(value, "dComReforme");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer mttQuotite() {
    return (Integer) storedValueForKey("mttQuotite");
  }

  public void setMttQuotite(Integer value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating mttQuotite from " + mttQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "mttQuotite");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
    	_EOMangueTempsPartielTherap.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueTempsPartielTherap.LOG.isDebugEnabled()) {
      _EOMangueTempsPartielTherap.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueTempsPartielTherap createMangueTempsPartielTherap(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer mttQuotite
, String temConfirme
, String temGestEtab
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueTempsPartielTherap eo = (EOMangueTempsPartielTherap) EOUtilities.createAndInsertInstance(editingContext, _EOMangueTempsPartielTherap.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setMttQuotite(mttQuotite);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueTempsPartielTherap> fetchAllMangueTempsPartielTheraps(EOEditingContext editingContext) {
    return _EOMangueTempsPartielTherap.fetchAllMangueTempsPartielTheraps(editingContext, null);
  }

  public static NSArray<EOMangueTempsPartielTherap> fetchAllMangueTempsPartielTheraps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueTempsPartielTherap.fetchMangueTempsPartielTheraps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueTempsPartielTherap> fetchMangueTempsPartielTheraps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueTempsPartielTherap.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueTempsPartielTherap> eoObjects = (NSArray<EOMangueTempsPartielTherap>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueTempsPartielTherap fetchMangueTempsPartielTherap(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueTempsPartielTherap.fetchMangueTempsPartielTherap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueTempsPartielTherap fetchMangueTempsPartielTherap(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueTempsPartielTherap> eoObjects = _EOMangueTempsPartielTherap.fetchMangueTempsPartielTheraps(editingContext, qualifier, null);
    EOMangueTempsPartielTherap eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueTempsPartielTherap)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueTempsPartielTherap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueTempsPartielTherap fetchRequiredMangueTempsPartielTherap(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueTempsPartielTherap.fetchRequiredMangueTempsPartielTherap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueTempsPartielTherap fetchRequiredMangueTempsPartielTherap(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueTempsPartielTherap eoObject = _EOMangueTempsPartielTherap.fetchMangueTempsPartielTherap(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueTempsPartielTherap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueTempsPartielTherap localInstanceIn(EOEditingContext editingContext, EOMangueTempsPartielTherap eo) {
    EOMangueTempsPartielTherap localInstance = (eo == null) ? null : (EOMangueTempsPartielTherap)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
