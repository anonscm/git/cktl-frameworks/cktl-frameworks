// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueVacatairesAffectation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueVacatairesAffectation extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueVacatairesAffectation";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NBR_HEURES_KEY = "nbrHeures";
	public static final String TEM_PRINCIPALE_KEY = "temPrincipale";

	// Relationships
	public static final String STRUCTURE_KEY = "structure";
	public static final String VACATAIRES_KEY = "vacataires";

  private static Logger LOG = Logger.getLogger(_EOMangueVacatairesAffectation.class);

  public EOMangueVacatairesAffectation localInstanceIn(EOEditingContext editingContext) {
    EOMangueVacatairesAffectation localInstance = (EOMangueVacatairesAffectation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOMangueVacatairesAffectation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOMangueVacatairesAffectation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer nbrHeures() {
    return (Integer) storedValueForKey("nbrHeures");
  }

  public void setNbrHeures(Integer value) {
    if (_EOMangueVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOMangueVacatairesAffectation.LOG.debug( "updating nbrHeures from " + nbrHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeures");
  }

  public String temPrincipale() {
    return (String) storedValueForKey("temPrincipale");
  }

  public void setTemPrincipale(String value) {
    if (_EOMangueVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOMangueVacatairesAffectation.LOG.debug( "updating temPrincipale from " + temPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipale");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOMangueVacatairesAffectation.LOG.isDebugEnabled()) {
      _EOMangueVacatairesAffectation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires vacataires() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires)storedValueForKey("vacataires");
  }

  public void setVacatairesRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires value) {
    if (_EOMangueVacatairesAffectation.LOG.isDebugEnabled()) {
      _EOMangueVacatairesAffectation.LOG.debug("updating vacataires from " + vacataires() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires oldValue = vacataires();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vacataires");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vacataires");
    }
  }
  

  public static EOMangueVacatairesAffectation createMangueVacatairesAffectation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer nbrHeures
, String temPrincipale
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueVacataires vacataires) {
    EOMangueVacatairesAffectation eo = (EOMangueVacatairesAffectation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueVacatairesAffectation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNbrHeures(nbrHeures);
		eo.setTemPrincipale(temPrincipale);
    eo.setStructureRelationship(structure);
    eo.setVacatairesRelationship(vacataires);
    return eo;
  }

  public static NSArray<EOMangueVacatairesAffectation> fetchAllMangueVacatairesAffectations(EOEditingContext editingContext) {
    return _EOMangueVacatairesAffectation.fetchAllMangueVacatairesAffectations(editingContext, null);
  }

  public static NSArray<EOMangueVacatairesAffectation> fetchAllMangueVacatairesAffectations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueVacatairesAffectation.fetchMangueVacatairesAffectations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueVacatairesAffectation> fetchMangueVacatairesAffectations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueVacatairesAffectation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueVacatairesAffectation> eoObjects = (NSArray<EOMangueVacatairesAffectation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueVacatairesAffectation fetchMangueVacatairesAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueVacatairesAffectation.fetchMangueVacatairesAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueVacatairesAffectation fetchMangueVacatairesAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueVacatairesAffectation> eoObjects = _EOMangueVacatairesAffectation.fetchMangueVacatairesAffectations(editingContext, qualifier, null);
    EOMangueVacatairesAffectation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueVacatairesAffectation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueVacatairesAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueVacatairesAffectation fetchRequiredMangueVacatairesAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueVacatairesAffectation.fetchRequiredMangueVacatairesAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueVacatairesAffectation fetchRequiredMangueVacatairesAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueVacatairesAffectation eoObject = _EOMangueVacatairesAffectation.fetchMangueVacatairesAffectation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueVacatairesAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueVacatairesAffectation localInstanceIn(EOEditingContext editingContext, EOMangueVacatairesAffectation eo) {
    EOMangueVacatairesAffectation localInstance = (eo == null) ? null : (EOMangueVacatairesAffectation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
