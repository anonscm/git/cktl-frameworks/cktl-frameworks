//EOGrhumAdresse.java
//Created on Wed Jul 25 15:10:06 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOPays;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.importer.moteur.ReglesPourModele;
import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOAdresse;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EORepartPersonneAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

//24/06/2010 - ajout de persIdCreation et persIdModification
public class EOGrhumAdresse extends _EOGrhumAdresse implements InterfaceRecordAvecAutresAttributs,InterfaceGestionIdUtilisateur,InterfaceValiditePourDestinataire {
	private EORepartPersonneAdresse repartPourAdresse;

	public EOGrhumAdresse() {
		super();
		
	}


	/** Retourne la valeur correspondant &grave; la cle de destination. L'objet d'import est surcharge
	 * pour recuperer les infos de la repartPersonneAdresse */
	public Object valeurPourCleEtObjetImport(String cle,ObjetImport objet) {
		if (cle != null) {
			if (cle.equals("eMail") == false && cle.equals("tadrCode") == false) {
				return super.valeurPourCleEtObjetImport(cle, objet);
			} else {
				EORepartPersonneAdresse rpa = rpaPourAdresse((EOAdresse)objet);
				if (rpa == null) {
					return null;
				} else {
					return rpa.valueForKey(cle);
				}
			}
		} else {
			return null;
		}
	}
	/** Compare les donnees de l'adresse et de repartPersonneAdresse. Return true si elles sont identiques.
	@param source 
	@param destination
	 */
	public boolean aAttributsIdentiques(ObjetImport source,Entite entiteModele) {
		if (super.aAttributsIdentiques(source, entiteModele)) {
			EORepartPersonneAdresse repart = rpaPourAdresse((EOAdresse)source);
			if (repart == null) {
				return false;
			}
			NSArray attributs = entiteModele.attributs();
			java.util.Enumeration e = attributs.objectEnumerator();
			while (e.hasMoreElements()) {
				Attribut attribut = (Attribut)e.nextElement();
				Object valeurAttributSource = source.valueForKey(attribut.nomSource());
				if (valeurAttributSource != null) {		// On ne prend pas en compte les attributs nuls
					if (attribut.type() != null) {		// Sinon il s'agit de l'attribut de la repart personne adresse
						Object valeurAttributDestin = repart.valueForKey(attribut.nomDestination());
						if (valeurAttributDestin == null ||	ReglesPourModele.sontAttributsIdentiques(valeurAttributSource,valeurAttributDestin) == false) {  
							return false;
						}
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}
	/** retourne false si l'objet d'import n'est pas */
	public boolean estValide() {
		return repartPourAdresse != null && repartPourAdresse.estValide();
	}
	/** Modification de l'attribut de validite dans la RepartPersonAdresse */
	public void setEstValide(boolean aBool) {
		if (repartPourAdresse != null) {
			repartPourAdresse.setEstValide(aBool);
		}
	}
	public boolean updateAvecRecord(ObjetImport recordImport) throws Exception {
		if (recordImport instanceof EOAdresse) {
			repartPourAdresse = rpaPourAdresse((EOAdresse)recordImport);
		}
		return super.updateAvecRecord(recordImport);
	}
	// InterfaceValiditePourDestinataire
	/** Retourne true si il existe une rpa valide correspondant a cette adresse */
	public boolean destinataireValidePourObjetImport(ObjetImport recordImport) {
		EORepartPersonneAdresse rpa = rpaPourAdresse((EOAdresse)recordImport);
		return rpa != null && rpa.estValide();
	}
	// InterfaceRecordAvecAutresAttributs
	/** Modifie le RepartPersonneAdresse si il existe pour ce type d'adresse et le revalide si il avait ete invalide
	 * dans le SI Destinataire (l'import l'emporte sur le SI Destinataire), sinon en creer un nouveau */
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport recordImport) {
		EOAdresse adresseImport = (EOAdresse)recordImport;
		EORepartPersonneAdresse repart = rpaPourAdresse(adresseImport);
		boolean estModifie = false;
		// Vérifier si il y a des règles de priorité sur les attributs qui fait qu'on ne doit pas y toucher
		EOGrhumPrioriteEntite priorite = null;
		if (recordImport.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || recordImport.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			priorite = prioriteEntite();
		}
		// Modifier les attributs  si il n'y a pas de règle de priorité sur ceux-ci
		if (repart != null) {
			// Modifier éventuellement l'email si elle est fournie dans le record d'import
			if ((priorite == null || priorite.attributPourLibelle("eMail",true) == null) &&
					adresseImport.eMail() != null && (repart.eMail() == null || repart.eMail().equals(adresseImport.eMail()) == false)) {
				repart.setEMail(adresseImport.eMail());
				LogManager.logDetail("attribut : eMail, valeur import : " + adresseImport.eMail() + " remplace valeur SI Dest : " + repart.eMail());

				estModifie = true;
			}
		} else {
			if (recordImport.operation().equals(ObjetImport.OPERATION_INSERTION) == false) {
				LogManager.logDetail("Creation d'une repart pour adresse " + adresseImport);
			}
			preparerRepartPersonneAdresse(adresseImport);
			estModifie = true;
		}
		return estModifie;
	}	

	// Méthodes protégées
	protected void initialiserObjet(ObjetImport record) throws Exception {
		
		setTemPayeUtil(CocktailConstantes.FAUX);
		// Préparer la clé primaire
		Number noAdresse = SuperFinder.clePrimairePour(editingContext(),"GrhumAdresse","adrOrdre","GrhumSeqAdresse",true);
		setAdrOrdre(new Integer(noAdresse.intValue()));
		EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
		setPersIdCreation(utilisateur.persId());
		setPersIdModification(utilisateur.persId());
		
		NSTimestamp dateActuelle = new NSTimestamp();
		setDCreation(dateActuelle);
		setDModification(dateActuelle);
		
	}
	//	Méthodes statiques
	/** retourne l'adresse correspondante du SI Destinataire (recherchee parmi les adresses valides)
	 * avec le m&ecirc;me pays, la m&ecirc;me adresse1, le m&ecirc;me code postal ou la m&ecirc;me ville */ 
	public static EOGrhumAdresse adresseDestinationPourAdresse(EOEditingContext editingContext,EOAdresse adresse) {
		Number persID = null;
		if (adresse.individu() != null) {	
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)adresse.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				persID = individuGrhum.persId();
			}
		} else {
			// On vérifie si il existe une correspondance sinon (la structure n'a peut-être pas encore été créée)
			// on cherche directement dans la base destinataire
			EOGrhumStructure structureGrhum = (EOGrhumStructure)adresse.structure().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (structureGrhum != null) {
				persID = structureGrhum.persId();
			}
		}
		NSArray adresses = EORepartPersonneAdresse.rechercherAdressesValidesDeType(editingContext, persID, adresse.typeAdresse());
		if (adresses == null || adresses.count() == 0) {
			return null;
		}
		java.util.Enumeration e = adresses.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGrhumAdresse adresseGrhum = (EOGrhumAdresse)e.nextElement();
			// supprimer les blancs inutiles de adrAdresse1 car ils sont supprimés lors de l'import d'adresse
			if (adresseGrhum.cPays().equals(adresse.pays()) && 
					((adresseGrhum.adrAdresse1() == null && adresse.adresse1() == null) || 
							(adresseGrhum.adrAdresse1() != null && adresse.adresse1() != null && adresseGrhum.adrAdresse1().trim().toUpperCase().equals(adresse.adresse1().toUpperCase())))) {
				boolean memeVille = (adresseGrhum.ville() == null && adresse.ville() == null) || (adresseGrhum.ville() != null && adresse.ville() != null && adresseGrhum.ville().trim().equals(adresse.ville()));
				boolean memeCodePostal = (adresseGrhum.codePostal() == null && adresse.codePostal() == null) || (adresseGrhum.codePostal() != null && adresse.codePostal() != null && adresseGrhum.codePostal().equals(adresse.codePostal()));
				boolean memeCPEtranger = (adresseGrhum.cpEtranger() == null && adresse.cpEtranger() == null) || (adresseGrhum.cpEtranger() != null && adresse.cpEtranger() != null && adresseGrhum.cpEtranger().equals(adresse.cpEtranger()));
				if (memeVille || (adresseGrhum.cPays().equals(EOPays.CODE_PAYS_DEFAUT) && memeCodePostal) || (adresseGrhum.cPays().equals(EOPays.CODE_PAYS_DEFAUT) == false && memeCPEtranger)) {
					return adresseGrhum;
				}
			}
		}
		return null;
	}
	//	Méthodes privées
	private void preparerRepartPersonneAdresse(EOAdresse adresseImport) {
		Number persId = null;
		if (adresseImport.strSource() != null) {	// Il s'agit d'une adresse de structure
			EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), adresseImport.strSource());
			if (structure != null) {	// Ne devrait pas car on crée les structures avant
				persId = structure.persId();
			}
		} else if (adresseImport.idSource() != null) {	// Il s'agit d'une adresse d'individu
			EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), adresseImport.idSource());
			if (individu != null) {	// Ne devrait pas car on crée le individus avant
				persId = individu.persId();
			}
		}
		if (persId != null) {
//			String rpaPrincipale = CocktailConstantes.FAUX;
			// Vérifier si il y a déjà une adresse principale pour la personne
			NSArray reparts = EORepartPersonneAdresse.rechercherRpaPrincipalesEtValides(editingContext(), persId);
//			if (reparts == null || reparts.count() == 0) {
//				rpaPrincipale = CocktailConstantes.VRAI;
//			}
			EORepartPersonneAdresse repart = new EORepartPersonneAdresse();
			repart.init();
//			repart.setRpaPrincipal(rpaPrincipale);
			repart.setRpaPrincipal(adresseImport.temPrincipal());
			repart.setPersId(new Integer(persId.intValue()));
			repart.setAdrOrdre(this.adrOrdre());
			repart.addObjectToBothSidesOfRelationshipWithKey(this, "adresse");
			// Initialiser la repartition avec le reste des données qui sont dans l'adresse d'import
			repart.setTadrCode(adresseImport.typeAdresse());
			if (adresseImport.eMail() != null) {
				repart.setEMail(adresseImport.eMail());
			}
			editingContext().insertObject(repart);
		} 
	}
	private EORepartPersonneAdresse rpaPourAdresse(EOAdresse adresseImport) {
		Number persId = null;
		if (adresseImport.strSource() != null) {	// Il s'agit d'une adresse de structure
			EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), adresseImport.strSource());
			if (structure != null) {	// Ne devrait pas car on crée les structures avant
				persId = structure.persId();
			}
		} else if (adresseImport.idSource() != null) {	// Il s'agit d'une adresse d'individu
			EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), adresseImport.idSource());
			if (individu != null) {	// Ne devrait pas car on crée le individus avant
				persId = individu.persId();
			}
		}
		
		
		if (persId != null) {
			// Rechercher d'abord si on trouve une repart valide, sinon on retourne la première repart trouvée
			NSArray reparts = EORepartPersonneAdresse.rechercherRepartsDeType(editingContext(), persId, this,adresseImport.typeAdresse());
			if (reparts != null && reparts.count() > 0) {
				java.util.Enumeration e = reparts.objectEnumerator();
				while (e.hasMoreElements()) {
					EORepartPersonneAdresse repart = (EORepartPersonneAdresse)e.nextElement();
					if (repart.estValide()) {
						return repart;
					}
				}
				// On n'en a pas trouvé, on retourne la première avec la date de modification la plus récente
				reparts = EOSortOrdering.sortedArrayUsingKeyOrderArray(reparts, new NSArray(EOSortOrdering.sortOrderingWithKey("dModification", EOSortOrdering.CompareDescending)));
				return (EORepartPersonneAdresse)reparts.objectAtIndex(0);
			} else  {
				return null;	// Pas de repart trouvée
			}
		} else {
			return null;
		}
	}

	public NSDictionary changesFromSnapshot(NSDictionary arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	public void reapplyChangesFromDictionary(NSDictionary arg0) {
		// TODO Auto-generated method stub
	}

	public void setPersIdCreation(Number value) {
		// TODO Auto-generated method stub
	}

	public void setPersIdModification(Number value) {
		// TODO Auto-generated method stub
	}
	
}
