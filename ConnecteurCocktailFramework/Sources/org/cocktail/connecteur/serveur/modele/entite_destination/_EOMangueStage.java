// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueStage.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueStage extends ObjetPourSIDestinataireAvecCarriere {
	public static final String ENTITY_NAME = "MangueStage";

	// Attributes
	public static final String C_CORPS_KEY = "cCorps";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_TITULARISATION_KEY = "dateTitularisation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String TEM_RENOUVELLEMENT_KEY = "temRenouvellement";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOMangueStage.class);

  public EOMangueStage localInstanceIn(EOEditingContext editingContext) {
    EOMangueStage localInstance = (EOMangueStage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dateTitularisation() {
    return (NSTimestamp) storedValueForKey("dateTitularisation");
  }

  public void setDateTitularisation(NSTimestamp value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating dateTitularisation from " + dateTitularisation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateTitularisation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public String temRenouvellement() {
    return (String) storedValueForKey("temRenouvellement");
  }

  public void setTemRenouvellement(String value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating temRenouvellement from " + temRenouvellement() + " to " + value);
    }
    takeStoredValueForKey(value, "temRenouvellement");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
    	_EOMangueStage.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
      _EOMangueStage.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOMangueStage.LOG.isDebugEnabled()) {
      _EOMangueStage.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOMangueStage createMangueStage(EOEditingContext editingContext, String cCorps
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temRenouvellement
, String temValide
) {
    EOMangueStage eo = (EOMangueStage) EOUtilities.createAndInsertInstance(editingContext, _EOMangueStage.ENTITY_NAME);    
		eo.setCCorps(cCorps);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemRenouvellement(temRenouvellement);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueStage> fetchAllMangueStages(EOEditingContext editingContext) {
    return _EOMangueStage.fetchAllMangueStages(editingContext, null);
  }

  public static NSArray<EOMangueStage> fetchAllMangueStages(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueStage.fetchMangueStages(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueStage> fetchMangueStages(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueStage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueStage> eoObjects = (NSArray<EOMangueStage>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueStage fetchMangueStage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueStage.fetchMangueStage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueStage fetchMangueStage(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueStage> eoObjects = _EOMangueStage.fetchMangueStages(editingContext, qualifier, null);
    EOMangueStage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueStage)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueStage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueStage fetchRequiredMangueStage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueStage.fetchRequiredMangueStage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueStage fetchRequiredMangueStage(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueStage eoObject = _EOMangueStage.fetchMangueStage(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueStage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueStage localInstanceIn(EOEditingContext editingContext, EOMangueStage eo) {
    EOMangueStage localInstance = (eo == null) ? null : (EOMangueStage)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
