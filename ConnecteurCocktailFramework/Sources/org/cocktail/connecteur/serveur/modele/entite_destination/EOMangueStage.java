package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeAbsence;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOStage;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueStage extends _EOMangueStage {
	private static Logger log = Logger.getLogger(EOMangueStage.class);

	public String typeEvenement() {
		return EOTypeAbsence.TYPE_STAGE;
	}

	/**
	 * 
	 */
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		
		//super.initialiserObjet(recordImport);
		EOStage stage = (EOStage)recordImport;
		// A ce stade, la carrière est dans le SI Destinataire donc la correspondance créée
		EOMangueCarriere carriereMangue = ((EOCarriereCorresp)stage.toCarriere().correspondance()).carriereMangue();

		setIndividuRelationship(carriereMangue.individu());
		setNoDossierPers(carriereMangue.individu().noIndividu());
		setNoSeqCarriere(carriereMangue.noSeqCarriere());
	}
}
