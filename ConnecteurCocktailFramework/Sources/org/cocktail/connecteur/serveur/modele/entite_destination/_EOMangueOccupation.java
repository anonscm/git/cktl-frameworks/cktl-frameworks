// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueOccupation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueOccupation extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueOccupation";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_OCCUPATION_KEY = "dDebOccupation";
	public static final String DEN_MOYEN_UTILISE_KEY = "denMoyenUtilise";
	public static final String D_FIN_OCCUPATION_KEY = "dFinOccupation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String MOTIF_FIN_KEY = "motifFin";
	public static final String NO_OCCUPATION_KEY = "noOccupation";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String NO_SEQ_CONTRAT_KEY = "noSeqContrat";
	public static final String NUM_MOYEN_UTILISE_KEY = "numMoyenUtilise";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CARRIERE_KEY = "carriere";
	public static final String CONTRAT_KEY = "contrat";
	public static final String EMPLOI_KEY = "emploi";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueOccupation.class);

  public EOMangueOccupation localInstanceIn(EOEditingContext editingContext) {
    EOMangueOccupation localInstance = (EOMangueOccupation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebOccupation() {
    return (NSTimestamp) storedValueForKey("dDebOccupation");
  }

  public void setDDebOccupation(NSTimestamp value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating dDebOccupation from " + dDebOccupation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebOccupation");
  }

  public Integer denMoyenUtilise() {
    return (Integer) storedValueForKey("denMoyenUtilise");
  }

  public void setDenMoyenUtilise(Integer value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating denMoyenUtilise from " + denMoyenUtilise() + " to " + value);
    }
    takeStoredValueForKey(value, "denMoyenUtilise");
  }

  public NSTimestamp dFinOccupation() {
    return (NSTimestamp) storedValueForKey("dFinOccupation");
  }

  public void setDFinOccupation(NSTimestamp value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating dFinOccupation from " + dFinOccupation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinOccupation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String motifFin() {
    return (String) storedValueForKey("motifFin");
  }

  public void setMotifFin(String value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating motifFin from " + motifFin() + " to " + value);
    }
    takeStoredValueForKey(value, "motifFin");
  }

  public Integer noOccupation() {
    return (Integer) storedValueForKey("noOccupation");
  }

  public void setNoOccupation(Integer value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating noOccupation from " + noOccupation() + " to " + value);
    }
    takeStoredValueForKey(value, "noOccupation");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public Integer noSeqContrat() {
    return (Integer) storedValueForKey("noSeqContrat");
  }

  public void setNoSeqContrat(Integer value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating noSeqContrat from " + noSeqContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqContrat");
  }

  public Double numMoyenUtilise() {
    return (Double) storedValueForKey("numMoyenUtilise");
  }

  public void setNumMoyenUtilise(Double value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating numMoyenUtilise from " + numMoyenUtilise() + " to " + value);
    }
    takeStoredValueForKey(value, "numMoyenUtilise");
  }

  public String observations() {
    return (String) storedValueForKey("observations");
  }

  public void setObservations(String value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating observations from " + observations() + " to " + value);
    }
    takeStoredValueForKey(value, "observations");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
    	_EOMangueOccupation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere carriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("carriere");
  }

  public void setCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
      _EOMangueOccupation.LOG.debug("updating carriere from " + carriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = carriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "carriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "carriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat contrat() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
      _EOMangueOccupation.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi emploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi)storedValueForKey("emploi");
  }

  public void setEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
      _EOMangueOccupation.LOG.debug("updating emploi from " + emploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi oldValue = emploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "emploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "emploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueOccupation.LOG.isDebugEnabled()) {
      _EOMangueOccupation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueOccupation createMangueOccupation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebOccupation
, Integer denMoyenUtilise
, NSTimestamp dModification
, Integer noOccupation
, Double numMoyenUtilise
, String temTitulaire
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi emploi, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueOccupation eo = (EOMangueOccupation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueOccupation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebOccupation(dDebOccupation);
		eo.setDenMoyenUtilise(denMoyenUtilise);
		eo.setDModification(dModification);
		eo.setNoOccupation(noOccupation);
		eo.setNumMoyenUtilise(numMoyenUtilise);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    eo.setEmploiRelationship(emploi);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueOccupation> fetchAllMangueOccupations(EOEditingContext editingContext) {
    return _EOMangueOccupation.fetchAllMangueOccupations(editingContext, null);
  }

  public static NSArray<EOMangueOccupation> fetchAllMangueOccupations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueOccupation.fetchMangueOccupations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueOccupation> fetchMangueOccupations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueOccupation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueOccupation> eoObjects = (NSArray<EOMangueOccupation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueOccupation fetchMangueOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueOccupation.fetchMangueOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueOccupation fetchMangueOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueOccupation> eoObjects = _EOMangueOccupation.fetchMangueOccupations(editingContext, qualifier, null);
    EOMangueOccupation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueOccupation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueOccupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueOccupation fetchRequiredMangueOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueOccupation.fetchRequiredMangueOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueOccupation fetchRequiredMangueOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueOccupation eoObject = _EOMangueOccupation.fetchMangueOccupation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueOccupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueOccupation localInstanceIn(EOEditingContext editingContext, EOMangueOccupation eo) {
    EOMangueOccupation localInstance = (eo == null) ? null : (EOMangueOccupation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
