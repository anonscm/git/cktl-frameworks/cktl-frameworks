// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueContratHeberges.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueContratHeberges extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueContratHeberges";

	// Attributes
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_ORIGINE_KEY = "cStructureOrigine";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DETAIL_ORIGINE_KEY = "detailOrigine";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INM_KEY = "inm";
	public static final String NBR_HEURES_KEY = "nbrHeures";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_ORIGINE_KEY = "structureOrigine";

  private static Logger LOG = Logger.getLogger(_EOMangueContratHeberges.class);

  public EOMangueContratHeberges localInstanceIn(EOEditingContext editingContext) {
    EOMangueContratHeberges localInstance = (EOMangueContratHeberges)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public String cStructureOrigine() {
    return (String) storedValueForKey("cStructureOrigine");
  }

  public void setCStructureOrigine(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating cStructureOrigine from " + cStructureOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructureOrigine");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public String detailOrigine() {
    return (String) storedValueForKey("detailOrigine");
  }

  public void setDetailOrigine(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating detailOrigine from " + detailOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, "detailOrigine");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer inm() {
    return (Integer) storedValueForKey("inm");
  }

  public void setInm(Integer value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating inm from " + inm() + " to " + value);
    }
    takeStoredValueForKey(value, "inm");
  }

  public Integer nbrHeures() {
    return (Integer) storedValueForKey("nbrHeures");
  }

  public void setNbrHeures(Integer value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating nbrHeures from " + nbrHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeures");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
    	_EOMangueContratHeberges.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
      _EOMangueContratHeberges.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
      _EOMangueContratHeberges.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structureOrigine() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structureOrigine");
  }

  public void setStructureOrigineRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOMangueContratHeberges.LOG.isDebugEnabled()) {
      _EOMangueContratHeberges.LOG.debug("updating structureOrigine from " + structureOrigine() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structureOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structureOrigine");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structureOrigine");
    }
  }
  

  public static EOMangueContratHeberges createMangueContratHeberges(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueContratHeberges eo = (EOMangueContratHeberges) EOUtilities.createAndInsertInstance(editingContext, _EOMangueContratHeberges.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueContratHeberges> fetchAllMangueContratHebergeses(EOEditingContext editingContext) {
    return _EOMangueContratHeberges.fetchAllMangueContratHebergeses(editingContext, null);
  }

  public static NSArray<EOMangueContratHeberges> fetchAllMangueContratHebergeses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueContratHeberges.fetchMangueContratHebergeses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueContratHeberges> fetchMangueContratHebergeses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueContratHeberges.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueContratHeberges> eoObjects = (NSArray<EOMangueContratHeberges>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueContratHeberges fetchMangueContratHeberges(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueContratHeberges.fetchMangueContratHeberges(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueContratHeberges fetchMangueContratHeberges(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueContratHeberges> eoObjects = _EOMangueContratHeberges.fetchMangueContratHebergeses(editingContext, qualifier, null);
    EOMangueContratHeberges eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueContratHeberges)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueContratHeberges that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueContratHeberges fetchRequiredMangueContratHeberges(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueContratHeberges.fetchRequiredMangueContratHeberges(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueContratHeberges fetchRequiredMangueContratHeberges(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueContratHeberges eoObject = _EOMangueContratHeberges.fetchMangueContratHeberges(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueContratHeberges that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueContratHeberges localInstanceIn(EOEditingContext editingContext, EOMangueContratHeberges eo) {
    EOMangueContratHeberges localInstance = (eo == null) ? null : (EOMangueContratHeberges)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
