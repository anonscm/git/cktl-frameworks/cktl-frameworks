package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.entite_import.EOReculAge;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueReculAge extends _EOMangueReculAge {

	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		EOReculAge reculAge = (EOReculAge)recordImport;
		if (reculAge.toEnfant1() != null) {
			EOGrhumEnfant enfant = EOGrhumEnfant.enfantDestinationPourEnfant(editingContext(), reculAge.toEnfant1());
			if (enfant != null) {
				setToEnfantRelationship(enfant);
			} else {
				throw new Exception("Pas d'enfant dans le SI correspondant a l'identifiant toEnfant1 : " + reculAge.toEnfant1());
			}
		}
		if (reculAge.toEnfant2() != null) {
			EOGrhumEnfant enfant = EOGrhumEnfant.enfantDestinationPourEnfant(editingContext(), reculAge.toEnfant2());
			if (enfant != null) {
				setToEnfant2Relationship(enfant);
			} else {
				throw new Exception("Pas d'enfant dans le SI correspondant a l'identifiant toEnfant2 : " + reculAge.toEnfant2());
			}
		}
		if (reculAge.toEnfant3() != null) {
			EOGrhumEnfant enfant = EOGrhumEnfant.enfantDestinationPourEnfant(editingContext(), reculAge.toEnfant3());
			if (enfant != null) {
				setToEnfant3Relationship(enfant);
			} else {
				throw new Exception("Pas d'enfant dans le SI correspondant a l'identifiant toEnfant3 : " + reculAge.toEnfant3());
			}
		}
	}

	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
}
