// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCgntRaisonFamPerso.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCgntRaisonFamPerso extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCgntRaisonFamPerso";

	// Attributes
	public static final String C_MOTIF_CG_RFP_KEY = "cMotifCgRfp";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CONGE_ANNUL_ORDRE_KEY = "congeAnnulOrdre";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOTIF_KEY = "motif";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCgntRaisonFamPerso.class);

  public EOMangueCgntRaisonFamPerso localInstanceIn(EOEditingContext editingContext) {
    EOMangueCgntRaisonFamPerso localInstance = (EOMangueCgntRaisonFamPerso)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifCgRfp() {
    return (String) storedValueForKey("cMotifCgRfp");
  }

  public void setCMotifCgRfp(String value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating cMotifCgRfp from " + cMotifCgRfp() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifCgRfp");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Integer congeAnnulOrdre() {
    return (Integer) storedValueForKey("congeAnnulOrdre");
  }

  public void setCongeAnnulOrdre(Integer value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating congeAnnulOrdre from " + congeAnnulOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "congeAnnulOrdre");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
    	_EOMangueCgntRaisonFamPerso.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
      _EOMangueCgntRaisonFamPerso.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifCgntRfp motif() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifCgntRfp)storedValueForKey("motif");
  }

  public void setMotifRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifCgntRfp value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
      _EOMangueCgntRaisonFamPerso.LOG.debug("updating motif from " + motif() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifCgntRfp oldValue = motif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "motif");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "motif");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso value) {
    if (_EOMangueCgntRaisonFamPerso.LOG.isDebugEnabled()) {
      _EOMangueCgntRaisonFamPerso.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCgntRaisonFamPerso oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCgntRaisonFamPerso createMangueCgntRaisonFamPerso(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCgntRaisonFamPerso eo = (EOMangueCgntRaisonFamPerso) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCgntRaisonFamPerso.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCgntRaisonFamPerso> fetchAllMangueCgntRaisonFamPersos(EOEditingContext editingContext) {
    return _EOMangueCgntRaisonFamPerso.fetchAllMangueCgntRaisonFamPersos(editingContext, null);
  }

  public static NSArray<EOMangueCgntRaisonFamPerso> fetchAllMangueCgntRaisonFamPersos(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCgntRaisonFamPerso.fetchMangueCgntRaisonFamPersos(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCgntRaisonFamPerso> fetchMangueCgntRaisonFamPersos(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCgntRaisonFamPerso.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCgntRaisonFamPerso> eoObjects = (NSArray<EOMangueCgntRaisonFamPerso>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCgntRaisonFamPerso fetchMangueCgntRaisonFamPerso(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCgntRaisonFamPerso.fetchMangueCgntRaisonFamPerso(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCgntRaisonFamPerso fetchMangueCgntRaisonFamPerso(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCgntRaisonFamPerso> eoObjects = _EOMangueCgntRaisonFamPerso.fetchMangueCgntRaisonFamPersos(editingContext, qualifier, null);
    EOMangueCgntRaisonFamPerso eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCgntRaisonFamPerso)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCgntRaisonFamPerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCgntRaisonFamPerso fetchRequiredMangueCgntRaisonFamPerso(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCgntRaisonFamPerso.fetchRequiredMangueCgntRaisonFamPerso(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCgntRaisonFamPerso fetchRequiredMangueCgntRaisonFamPerso(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCgntRaisonFamPerso eoObject = _EOMangueCgntRaisonFamPerso.fetchMangueCgntRaisonFamPerso(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCgntRaisonFamPerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCgntRaisonFamPerso localInstanceIn(EOEditingContext editingContext, EOMangueCgntRaisonFamPerso eo) {
    EOMangueCgntRaisonFamPerso localInstance = (eo == null) ? null : (EOMangueCgntRaisonFamPerso)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
