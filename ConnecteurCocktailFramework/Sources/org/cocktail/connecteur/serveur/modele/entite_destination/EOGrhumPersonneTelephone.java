// EOGrhumPersonneTelephone.java
// Created on Wed Jul 25 15:13:25 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeTel;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOTelephone;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOGrhumPersonneTelephone extends _EOGrhumPersonneTelephone {

    public EOGrhumPersonneTelephone() {
        super();
    }


	protected void initialiserObjet(ObjetImport record) throws Exception {
		EOTelephone telephoneImport = (EOTelephone)record;
		Number persId = null;
		if (telephoneImport.strSource() != null) {	// Il s'agit du téléphone d'une structure
			EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), telephoneImport.strSource());
			if (structure != null) {	// Ne devrait pas car on crée les structures avant
				persId = structure.persId();
			}
		} else if (telephoneImport.idSource() != null) {	// Il s'agit ddu téléphone d'un individu
			EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), telephoneImport.idSource());
			if (individu != null) {	// Ne devrait pas car on crée les individus avant
				persId = individu.persId();
			}
		}
		if (persId != null) {
			setPersId(new Integer(persId.intValue()));
			setDDebVal(new NSTimestamp());
			setListeRouge(CocktailConstantes.FAUX);



		} else {
			throw new Exception("Pas de persId valable pour " + record);
		}
	}
	/** Temporaire pour simuler la presence d'un temoin de validite */
	public boolean estValide() {
		return true;
	}
	/** Temporaire pour simuler la presence d'un temoin de validite */
	public void setEstValide(boolean aBool) {

	}
    // Méthodes statiques
    /** retourne le telephone avec le m&ecirc;me numero de telephone avec les m&ecirc;mes types dans les imports
	 * @param editingContext
	 * @param telephone EOTelephone
	 * @return EOGrhumPersonneTelephone
	 */
	public static EOGrhumPersonneTelephone telephoneDestinationPourTelephone(EOEditingContext editingContext,EOTelephone telephone) {
		if (telephone.structure() == null && telephone.individu() == null) {
			return null;
		}
		NSMutableArray args = new NSMutableArray();
		Number persId;
		if (telephone.individu() != null) {
			//	 Vérifier si il existe un individu équivalent dans le SI Destinataire.
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)telephone.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum == null) {
				return null;
			} else {
				persId = individuGrhum.persId();
			}
		} else {
			//  Vérifier si il existe une structure équivalente dans le SI Destinataire. On ne se base pas à cet instant
			// sur les correspondances car la structure n'a peut-être pas encore été créée
			EOGrhumStructure structureGrhum = (EOGrhumStructure)telephone.structure().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (structureGrhum == null) {
				return null;
			} else {
				persId = structureGrhum.persId();
			}
		}
		args.addObject(persId);
		args.addObject(preparerNoTelephone(telephone.noTelephone()));
		args.addObject(telephone.typeTel());
		args.addObject(telephone.typeNo());
		String stringQualifier = "persId = %@ AND noTelephone caseinsensitivelike %@ AND typeTel = %@ AND typeNo = %@";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification myFetch = new EOFetchSpecification ("GrhumPersonneTelephone", myQualifier,null);
		try {
			// On ne vérifie pas si il peut être le correspondant car les numéros de téléphone sont uniques
			// pour une persid , noTel, typeNo, typeTel
			return (EOGrhumPersonneTelephone)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	// Méthodes privées statiques
	private static String preparerNoTelephone(String noTel) {
		String nouveauNumero = noTel.replaceAll("\\.", "*");
		// Ajouter des étoiles en fin et début de numéro pour les blancs en fin et début de numéro
		return "*" + nouveauNumero + "*";
	}


}
