/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.common.modele.ObjetDestinataireValide;
import org.cocktail.connecteur.importer.modele.Attribut;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.ReglesDestination;
import org.cocktail.connecteur.importer.moteur.ReglesPourModele;
import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe generique pour les objets importes dans le SI Destinataire. Tous les
 * objets a importer doivent sous-classer ObjetPourSIDestinataire et implementer
 * la methode abstraite d'initialisation<BR>
 * 
 * @author christine
 * 
 */
public abstract class ObjetPourSIDestinataire extends ObjetDestinataireValide {

	/** initialise l'objet */
	public void initAvecImport(ObjetImport recordImport) throws Exception {
		initialiserObjet(recordImport);
		modifierAvecRecord(recordImport);
		if (this instanceof InterfaceRecordAvecAutresAttributs) {
			((InterfaceRecordAvecAutresAttributs) this).modifierAutresAttributsAvecRecordImport(recordImport);
		}
		setEstValide(true);
	}

	/**
	 * modifie l'objet Retourne true si le record a été modifé
	 */
	public boolean updateAvecRecord(ObjetImport recordImport) throws Exception {
		try {
			boolean estModifie = modifierAvecRecord(recordImport);
			boolean autresAttributsModifies = false;
			if (this instanceof InterfaceRecordAvecAutresAttributs) {
				autresAttributsModifies = ((InterfaceRecordAvecAutresAttributs) this).modifierAutresAttributsAvecRecordImport(recordImport);
			}
			return estModifie || autresAttributsModifies;
		} catch (Exception e) {
			throw e;
		}
	}

	public void validateForSave() {
		try {
			super.validateForSave();
		} catch (Exception e) {
			String message = e.getMessage();
			message = message + " pour le record " + SuperFinder.recordAsDict(this);
			throw new NSValidation.ValidationException(message);
		}
	}

	/**
	 * Compare deux records l'un de la base d'import et l'autre du SI
	 * Destinataire et retourne true si les donnees des attributs a modifier
	 * sont identiques en comparant les attributs de l'entite qui n'ont pas de
	 * type. Pour les attributs avec un type, charge a la sous-classe
	 * d'implementer le traitement en faisant auparavant appel a
	 * super.aAttributsIdentiques()
	 * 
	 * @param source
	 * @param destination
	 */
	public boolean aAttributsIdentiques(ObjetImport source, Entite entiteModele) {
		String nomEntiteSource = source.entityName(), nomEntiteDestin = entityName();
		if (nomEntiteSource == null || nomEntiteDestin == null) {
			return false;
		}
		if (entiteModele.nomSource().equals(nomEntiteSource) == false) {
			return false; // Il ne s'agit pas d'entité qui correspondent
		}
		NSArray attributs = entiteModele.attributs();
		java.util.Enumeration e = attributs.objectEnumerator();
		while (e.hasMoreElements()) {
			Attribut attribut = (Attribut) e.nextElement();
			Object valeurAttributSource = source.valueForKey(attribut.nomSource());
			if (valeurAttributSource != null) { // On ne prend pas en compte les
												// attributs nuls
				if (attribut.type() == null) { // Sinon il s'agit de l'attribut
												// d'une autre entité
					Object valeurAttributDestin = valueForKey(attribut.nomDestination());
					if (valeurAttributDestin == null || ReglesPourModele.sontAttributsIdentiques(valeurAttributSource, valeurAttributDestin) == false) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Retourne la valeur correspondant &grave; la cle de destination. L'objet
	 * d'import n'est pas pris en compte. Il est passe en parametre pour que des
	 * sous-classes puissent l'utiliser en surchargeant la methode/ Cette
	 * methode permet pour des entites destination dont des attributs ont un
	 * type non nul de pouvoir effectuer les traitements ad-hoc pour recuperer
	 * les valeurs
	 */
	public Object valeurPourCleEtObjetImport(String cle, ObjetImport objet) {
		return valueForKey(cle);
	}

	/**
	 * Return true si il n'y a pas de priorite sur cette entite ou si cette
	 * priorite demande la revalidation des rcords
	 */
	public boolean importExigeRevalidation() {
		boolean doitRevalider = true;
		EOGrhumPrioriteEntite priorite = prioriteEntite();
		if (priorite != null) {
			doitRevalider = priorite.estARevalider();
		}
		return doitRevalider;
	}

	// Méthodes protégées
	/**
	 * Methode appelee pour l'initialisation de l'objet
	 * 
	 * @throws Exception
	 */
	protected abstract void initialiserObjet(ObjetImport recordImport) throws Exception;

	/** Retourne la priorite d'entite associe &agrave, un record */
	protected EOGrhumPrioriteEntite prioriteEntite() {
		return EOGrhumPrioriteEntite.rechercherPrioriteEntitePourEntite(this.editingContext(), this.entityName());
	}

	// Méthodes privées
	// Un record a un statut Valide/Tronqué ou Priorité Destination. Dans ce
	// dernier cas, on ne modifiera que les attributs
	// qui n'ont pas de priorité
	private boolean modifierAvecRecord(ObjetImport recordImport) throws Exception {
		try {
			boolean recordModifie = false;

			// Rechercher l'entité du modèle de données (décrit dans
			// ModeleDestination.XML)
			Entite entite = ReglesDestination.sharedInstance().entiteAvecNomDestination(entityName());
			EOGrhumPrioriteEntite prioriteEntite = null;
			boolean estInsertion = recordImport.operation().equals(ObjetImport.OPERATION_INSERTION);
			if (!estInsertion && recordImport.statut().equals(ObjetImport.STATUT_PRIORITE_DESTINATION)) {
				prioriteEntite = prioriteEntite();
			}
			// Vérifier si les règles de priorité s'appliquent pour tous les
			// attributs, en quel cas, on ne fait rien
			if (prioriteEntite != null && (prioriteEntite.existeAttributsValides() == false)) {
				return false; // on ne modifie pas le record
			}
			boolean shouldLog = !estInsertion;
			if (shouldLog) {
				LogManager.logDetail("Modification d'un record destinataire, opération en cours : " + recordImport.operation());
			}
			// Parcourir tous les attributs de l'entité et pour chaque attribut,
			// vérifier si il existe une priorité du SI Destinataire
			// et si ce n'est pas le cas, vérifier si c'est un attribut qui est
			// directement dans l'entité (type attribut == null).
			// Si c'est le cas et que les valeurs des attributs ne sont pas
			// égales, modifier la valeur de l'attribut de destination
			java.util.Enumeration e = entite.attributs().objectEnumerator();
			while (e.hasMoreElements()) {
				Attribut attribut = (Attribut) e.nextElement();
				// Vérifier ici les règles de priorité car si elles s'appliquent
				// ne rien faire
				if (estInsertion || prioriteEntite == null || prioriteEntite.attributPourLibelle(attribut.nomDestination(), true) == null) {
					// certains attributs du record ne sont pas définis dans le
					// modèle de destination car ils ne sont pas à transférer
					// (par exemple dCreation) ou bien parce qu'ils
					// appartiennent à un autre record (Compte)
					if (attribut.type()!=null)
						// Un attribut avec un type appartien à une autre entité destination
						continue;
					Object valeurAttributSource = recordImport.valueForKey(attribut.nomSource());
					Object valeurAttributDestination = this.valueForKey(attribut.nomDestination());
					// Si l'attribut source a une valeur nulle et qu'il s'agit
					// de la création d'un record
					// Si l'attribut a une valeur par défaut, prendre cette
					// valeur
					if (valeurAttributSource == null && estInsertion) {
						if (attribut.valeurParDefaut() != null) {
							// Récupérer la classe de l'attribut destination
							// pour savoir comment convertir la valeur par
							// défaut
							String classeAttribut = this.classDescription().classForAttributeKey(attribut.nomDestination()).getName();
							if (classeAttribut.equals("java.lang.Integer")) {
								valeurAttributSource = new Integer((String) attribut.valeurParDefaut());
							} else if (classeAttribut.equals("com.webobjects.foundation.NSTimestamp")) {
								valeurAttributSource = DateCtrl.stringToDate((String) attribut.valeurParDefaut());
							} else { // String
								valeurAttributSource = attribut.valeurParDefaut();
							}
						} else {
							// Si c'est un attribut obligatoire => exception
							if (attribut.estObligatoire()) {
								throw new Exception("Modele de donnees erronne, attribut obligatoire sans valeur par defaut : " + attribut.nomSource());
							}
						}
					}

					if (valeurAttributSource != null) {
						boolean shouldChange = false;
						if (valeurAttributSource instanceof String) {
							if (valeurAttributDestination == null || valeurAttributSource.equals(valeurAttributDestination) == false) {
								shouldChange = true;
							}
						} else if (valeurAttributSource instanceof Number) {
							if (valeurAttributDestination == null
									|| ((Number) valeurAttributSource).doubleValue() != ((Number) valeurAttributDestination).doubleValue()) {
								this.takeValueForKey(valeurAttributSource, attribut.nomDestination());
								shouldChange = true;
							}
						} else if (valeurAttributSource instanceof NSTimestamp) {
							if (valeurAttributDestination == null
									|| DateCtrl.dateToString((NSTimestamp) valeurAttributSource).equals(
											DateCtrl.dateToString((NSTimestamp) valeurAttributDestination)) == false) {
								shouldChange = true;
							}
						}
						// On ne modifie pas les numéros de téléphone des
						// correspondances/updates (le numéro est identique au
						// format près)
						if (attribut.nomDestination().equals("noTelephone") && estInsertion == false) {
							shouldChange = false;
						}
						if (shouldChange) {
							this.takeValueForKey(valeurAttributSource, attribut.nomDestination());
							if (shouldLog) {
								LogManager.logDetail("attribut : " + attribut.nomSource() + ", valeur import : " + valeurAttributSource
										+ " remplace valeur SI Dest : " + valeurAttributDestination);
							}
							recordModifie = true;
						}
					} else {
						/*
						 * // on modifie la valeur de l'attribut en remplaçant
						 * par null sauf pour les attributs avec valeur // par
						 * défaut qu'on laisse à leur ancienne valeur if
						 * (valeurAttributDestination != null &&
						 * attribut.valeurParDefaut() == null) { // Pas de
						 * valeur par défaut this.takeValueForKey(null,
						 * attribut.nomDestination()); recordModifie = true; }
						 */
						// On ne fait rien - pas d'update sur les attributs qui
						// ne sont pas fournis
					}
				}
			}
			return recordModifie;
		} catch (Exception exc) {
			throw exc;
		}
	}
	

	// Méthode dériver pour réaliser un comportement lors qu'un opération d'homonymie est effectuée.
	public void onHomonymieEffectuee() {
	}
}
