// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueProlongationActivite.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueProlongationActivite extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueProlongationActivite";

	// Attributes
	public static final String C_MOTIF_PROLONGATION_KEY = "cMotifProlongation";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FIN_EXECUTION_KEY = "dFinExecution";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_MOTIF_PROLONGATION_KEY = "toMotifProlongation";

  private static Logger LOG = Logger.getLogger(_EOMangueProlongationActivite.class);

  public EOMangueProlongationActivite localInstanceIn(EOEditingContext editingContext) {
    EOMangueProlongationActivite localInstance = (EOMangueProlongationActivite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifProlongation() {
    return (String) storedValueForKey("cMotifProlongation");
  }

  public void setCMotifProlongation(String value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating cMotifProlongation from " + cMotifProlongation() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifProlongation");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFinExecution() {
    return (NSTimestamp) storedValueForKey("dFinExecution");
  }

  public void setDFinExecution(NSTimestamp value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating dFinExecution from " + dFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinExecution");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
    	_EOMangueProlongationActivite.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
      _EOMangueProlongationActivite.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotProlongation toMotifProlongation() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotProlongation)storedValueForKey("toMotifProlongation");
  }

  public void setToMotifProlongationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotProlongation value) {
    if (_EOMangueProlongationActivite.LOG.isDebugEnabled()) {
      _EOMangueProlongationActivite.LOG.debug("updating toMotifProlongation from " + toMotifProlongation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotProlongation oldValue = toMotifProlongation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMotifProlongation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMotifProlongation");
    }
  }
  

  public static EOMangueProlongationActivite createMangueProlongationActivite(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
) {
    EOMangueProlongationActivite eo = (EOMangueProlongationActivite) EOUtilities.createAndInsertInstance(editingContext, _EOMangueProlongationActivite.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueProlongationActivite> fetchAllMangueProlongationActivites(EOEditingContext editingContext) {
    return _EOMangueProlongationActivite.fetchAllMangueProlongationActivites(editingContext, null);
  }

  public static NSArray<EOMangueProlongationActivite> fetchAllMangueProlongationActivites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueProlongationActivite.fetchMangueProlongationActivites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueProlongationActivite> fetchMangueProlongationActivites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueProlongationActivite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueProlongationActivite> eoObjects = (NSArray<EOMangueProlongationActivite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueProlongationActivite fetchMangueProlongationActivite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueProlongationActivite.fetchMangueProlongationActivite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueProlongationActivite fetchMangueProlongationActivite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueProlongationActivite> eoObjects = _EOMangueProlongationActivite.fetchMangueProlongationActivites(editingContext, qualifier, null);
    EOMangueProlongationActivite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueProlongationActivite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueProlongationActivite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueProlongationActivite fetchRequiredMangueProlongationActivite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueProlongationActivite.fetchRequiredMangueProlongationActivite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueProlongationActivite fetchRequiredMangueProlongationActivite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueProlongationActivite eoObject = _EOMangueProlongationActivite.fetchMangueProlongationActivite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueProlongationActivite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueProlongationActivite localInstanceIn(EOEditingContext editingContext, EOMangueProlongationActivite eo) {
    EOMangueProlongationActivite localInstance = (eo == null) ? null : (EOMangueProlongationActivite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
