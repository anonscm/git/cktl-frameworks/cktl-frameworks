//EOGrhumIndividu.java
//Created on Wed Jul 25 15:12:17 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.QualifierIndividuHelper;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart;
import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EODepart;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 24/06/2010 - ajout et gestion de persIdCreation et persIdModification
/**
 * Gestion de la date de dec&egrave;s : si elle est fournie, en mode insertion ou correspondance, on verifie si il existe une information de depart pour
 * dec&egrave;s (EOMangueDepart) dans l'import courant ou dans le SI Destinataire. Si ce n'est pas le cas, on cre une information de depart dans le SI
 * Destinataire et on ajoute un log sur cette information de depart pour signaler a l'utilisateur qu'il doit agir dans Mangue. Si on est en mode update et que
 * la date de dec&egrave; est modifiee, on recherche l'information de depart et on met &grave; jour la date de debut du depart.
 */
public class EOGrhumIndividu extends _EOGrhumIndividu implements InterfaceRecordAvecAutresAttributs, InterfaceGestionIdUtilisateur {

	private static QualifierIndividuHelper serviceQualifier = new QualifierIndividuHelper();

	private NSTimestamp oldDateDeces;

	public EOGrhumIndividu() {
		super();
	}

	// Méthodes ajoutées
	public void awakeFromFetch(EOEditingContext editingContext) {
		// Pour pouvoir gérer les dates de décès lors de la création du record dans le SI Destinataire
		oldDateDeces = dDeces();
	}

	public EOGrhumPersonnel personnel() {
		try {
			return (EOGrhumPersonnel) personnels().objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	public boolean estHomme() {
		return cCivilite() != null && cCivilite().equals(CocktailConstantes.MONSIEUR);
	}

	/** Retourne true si il n'existe pas de correspondance avec cet individu grhum ou qu'il s'agit du même individu (comparaison des idSource) */
	public boolean peutCorrespondreIndividuImport(EOIndividu individu) {
		EOIndividu autreIndividu = (EOIndividu) ObjetCorresp.rechercherObjetImportPourRecordDestinataire(editingContext(), this, "Individu");
		return autreIndividu == null || autreIndividu.idSource().equals(individu.idSource());
	}

	/**
	 * Retourne true si des informations de depart suite a un dec&egrave;s ont ete crees ou modifiees : <BR>
	 * - si pas d'information de depart dans l'import courant et dans le SI Destinataire : creation<BR>
	 * - si information de depart dans l'import courant et motif different : modification de cette information de depart<BR>
	 * - sinon si information de depart dans le SI Destinataire : modification de cette information de depart<BR>
	 * 
	 * @return true si il y a eu creation ou modification
	 */
	public boolean effectuerModificationDepartPourDeces(EOIndividu individu) {
		boolean departPourDecesModifie = false;
		// Vérifier si il y a des règles de priorité sur les attributs qui fait qu'on ne doit pas y toucher
		EOGrhumPrioriteEntite priorite = null;
		if (individu.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || individu.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			priorite = prioriteEntite();
		}
		// Gérer la date de décès
		if (dDeces() != null && (oldDateDeces == null || DateCtrl.isSameDay(dDeces(), oldDateDeces) == false)) {
			LogManager.logDetail("Recherche des informations de depart pour deces");
			// La date de décès est modifiée
			boolean verifierDepart = individu.operation().equals(ObjetImport.OPERATION_INSERTION);
			if (!verifierDepart && (priorite == null || priorite.attributPourLibelle("dDeces", true) == null)) {
				// Operation de correspondance ou d'update et pas de priorité sur la date de décès, elle sera donc modifiée
				verifierDepart = true;
			}
			if (verifierDepart) {
				// Vérifier si il existe des départs pour décès dans l'import courant ou dans le SI Destinataire
				EOGenericRecord depart = EODepart.rechercherDepartPourDeces(editingContext(), individu, null);
				boolean creerDepart = (depart == null);
				// Si il n'y a pas de départ, vérifier si il existe un départ avec la même date de début
				// en quel cas, on l'utilisera car il ne peut pas y avoir de chevauchement de départ
				if (creerDepart) {
					depart = EODepart.rechercherDepartPourDateDebut(editingContext(), individu, dDeces());
					creerDepart = (depart == null);
				}
				if (creerDepart) {
					LogManager.logDetail("Pas de depart trouve, creation d'un depart");
					EOMangueDepart departMangue = new EOMangueDepart();
					editingContext().insertObject(departMangue);
					departMangue.initAvecIndividu(this);
					departPourDecesModifie = true;
				} else {
					if (depart instanceof EODepart) {
						EODepart departImport = (EODepart) depart;
						// Ne modifier le départ que si il n'a pas le motif de décès
						if (departImport.cMotifDepart().equals(EOMotifDepart.TYPE_MOTIF_DECES) == false) {
							LogManager.logDetail("Modification d'un EODepart " + depart);
							// Le départ est dans l'import courant, il n'est pas encore traité, on va juste modifier ses dates et son motif
							departImport.mettreAJourPourDeces(dDeces());
							departPourDecesModifie = true;
						}
					} else {
						EOMangueDepart departMangue = (EOMangueDepart) depart;
						// Ne modifier le départ que si il n'a pas le motif de décès ou une date de début postérieure à la date de décès
						if (departMangue != null && departMangue.cMotifDepart().equals(EOMotifDepart.TYPE_MOTIF_DECES) == false || departMangue != null
								&& DateCtrl.isAfter(departMangue.dateDebut(), dDeces())) {
							LogManager.logDetail("Modification d'un EOMangueDepart " + depart);
							if (depart != null) {
								((EOMangueDepart) depart).mettreAJourPourDeces(dDeces());
							}
							departPourDecesModifie = true;
						}
					}
				}
			}
		}
		return departPourDecesModifie;
	}

	// InterfaceRecordAvecAutresAttributs
	/** Modification des attributs d'affichage et du nom patronymique des hommes si ils sont nuls. Gestion des dates de dec&egrave;s */
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport record) {
		boolean estModifie = false;
		// Vérifier si il y a des règles de priorité sur les attributs qui fait qu'on ne doit pas y toucher
		EOGrhumPrioriteEntite priorite = null;
		String message = "";
		if (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || record.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			priorite = prioriteEntite();
		}

		// Modifier les attributs si il n'y a pas de règle de priorité sur ceux-ci
		// Mettre comme nom d'affichage le nom usuel si cette valeur est nulle
		if ((priorite == null || priorite.attributPourLibelle("nomAffichage", true) == null) && nomAffichage() == null) {
			setNomAffichage(nomUsuel());
			message = "Nom affichage force a nom usuel\n";
			estModifie = true;
		}
		if ((priorite == null || priorite.attributPourLibelle("prenomAffichage", true) == null) && prenomAffichage() == null) {
			setPrenomAffichage(prenom());
			message += "Prenom affichage force a prenom\n";
			estModifie = true;
		}
		// Mettre comme nom patronymique le nom usuel pour les hommes dont le nom patronymique est nul
		if ((priorite == null || priorite.attributPourLibelle("nomPatronymique", true) == null) && nomPatronymique() == null
				&& cCivilite().equals(CocktailConstantes.MONSIEUR)) {
			setNomPatronymique(nomUsuel());
			estModifie = true;
			message += "Nom patronymique force a nom usuel pour un homme\n";
		}
		if ((priorite == null || priorite.attributPourLibelle("nomPatronymiqueAffichage", true) == null) && nomPatronymiqueAffichage() == null
				&& nomPatronymique() != null) {
			setNomPatronymiqueAffichage(nomPatronymique());
			estModifie = true;
			message += "Nom patronymique affichage force a nom patronymique\n";
		}
		if (estModifie && (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || record.operation().equals(ObjetImport.OPERATION_UPDATE))) {
			message = "Operation de correspondance ou d'update, record : " + record + "\n" + message;
			LogManager.logDetail(message);
		}
		return estModifie;
	}

	// Méthodes protégées
	protected void initialiserObjet(ObjetImport record) {
		Number noIndividu = SuperFinder.clePrimairePour(editingContext(), "GrhumIndividu", "noIndividu", "GrhumSeqIndividu", true);
		setNoIndividu(new Integer(noIndividu.intValue()));
		Number persId = SuperFinder.construirePersId(editingContext());
		setPersId(new Integer(persId.intValue()));
		NSTimestamp dateActuelle = new NSTimestamp();
		setDCreation(dateActuelle);
		setDModification(dateActuelle);
		if (Constantes.PERS_ID_UTILISATEUR != null) {
			setPersIdCreation(Constantes.PERS_ID_UTILISATEUR);
			setPersIdModification(Constantes.PERS_ID_UTILISATEUR);
		}
	}

	// Méthodes statiques
	public static EOGrhumIndividu individuAvecPersId(EOEditingContext editingContext, Number persId) {
		EOFetchSpecification fs = new EOFetchSpecification("GrhumIndividu", EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId)), null);
		try {
			return (EOGrhumIndividu) editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean aHomonyme(EOEditingContext editingContext, EOIndividu individu) {
		if (individu == null) {
			return false;
		}
		NSArray result = serviceQualifier.rechercherHomonymes(editingContext, individu);

		return result.count() > 0;
	}

	/**
	 * un individu existe dej&egrave; dans la base si on trouve un individu avec le m&ecirc;me nom et prenom et la m&ecirc;me date de naissance et le m&ecirc;me
	 * numero Insee ou bien juste le m&ecirc;me numero Insee On retournera en priorite un individu qui est un personnel et nul si on trouve plusieurs individus
	 * candidats
	 * 
	 * @param editingContext
	 * @param individu
	 *            de la base d'import
	 * @param uniquementPersonnel
	 *            true si on recherche uniquement les personnels
	 * @return individu trouve
	 */
	public static EOGrhumIndividu individuDestinationPourIndividu(EOEditingContext editingContext, EOIndividu individu, boolean uniquementPersonnel) {
		LogManager.logDetail("Début du passage dans EOGrhumIndividu.individuDestinationPourIndividu");
		NSArray results = null;
		// if (individu.dateNaissance() != null && individu.noInsee() != null) {
		// results = serviceQualifier.rechercherHomonymes(editingContext, individu.nomUsuel(), individu.prenom(), individu.dateNaissance(), individu.noInsee());
		// if (results.count() == 0) {
		// results = serviceQualifier.rechercherHomonymes(editingContext, individu.nomUsuel(), individu.prenom(), null,individu.noInsee());
		// }
		// } else if (individu.noInsee() != null) {
		// results = serviceQualifier.rechercherHomonymes(editingContext, individu.nomUsuel(), individu.prenom(), null,individu.noInsee());
		// }

		results = serviceQualifier.rechercherHomonymes(editingContext, individu);// .nomUsuel(), individu.prenom(), individu.dateNaissance(),
																					// individu.noInsee());
		// TODO Homonymes
		// Est-ce que l'ensemble des tests ici fait bien ce qui est prébu par la documentation de la méthode ?
		LogManager.logDetail("Le nombre d'individu trouvé par la recherche des homonymes est : " + results.count());

		if (results != null && results.count() == 1) {
			LogManager.logDetail("Traitement suite à des résultats d'homonymie et n'avoir qu'UN SEUL résultat");
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu) results.objectAtIndex(0);
			if (estElligibleADestination(uniquementPersonnel, individuGrhum) && individuGrhum.peutCorrespondreIndividuImport(individu)) {
				LogManager.logDetail("L'individu est élligible à destination et peut correspondre à un individu de l'import");
				LogManager.logDetail("Fin du passage dans EOGrhumIndividu.individuDestinationPourIndividu");
				return individuGrhum;
			} else {
				LogManager.logDetail("L'individu est élligible à destination et peut correspondre à un individu de l'import");
				LogManager.logDetail("Fin du passage dans EOGrhumIndividu.individuDestinationPourIndividu");
				return null;
			}
		} else {
			LogManager.logDetail("Traitement suite à des résultats d'homonymie null ou plus d'un seul");
			LogManager.logDetail("Fin du passage dans EOGrhumIndividu.individuDestinationPourIndividu");
			return null;
		}
	}

	private static boolean estElligibleADestination(boolean uniquementPersonnel, EOGrhumIndividu individuGrhum) {
		return (!uniquementPersonnel || individuGrhum.personnel() != null);
	}

	public NSDictionary changesFromSnapshot(NSDictionary arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public void reapplyChangesFromDictionary(NSDictionary arg0) {
		// TODO Auto-generated method stub

	}

	public void setPersIdCreation(Number value) {
		// super.setPersIdCreation(Integer.parseInt(value.toString()));

	}

	public void setPersIdModification(Number value) {
		// super.setPersIdModification(Integer.parseInt(value.toString()));

	}

	@Override
	public void onHomonymieEffectuee() {
		try {
			NSMutableDictionary dict = new NSMutableDictionary();
			Integer val = noIndividu();
			dict.setObjectForKey(val, "noIndividu");
			EOUtilities.executeStoredProcedureNamed(editingContext(), "on_operation_homonyme_individu", dict);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}