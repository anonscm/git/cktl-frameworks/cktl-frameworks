package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOPersBudgetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudgetAction;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOManguePersBudgetAction extends _EOManguePersBudgetAction {
	@Override
	public void setTemValide(String value) {
		// Pas de champ TemValide.  On surcharge l'affectation par défaut de la propriété
	}

	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOPersBudgetAction budgetActionImport = (EOPersBudgetAction) recordImport;
		if (budgetActionImport.toPersBudget() != null) {
			EOManguePersBudget persBudget = EOPersBudgetCorresp.persBudgetMangue(editingContext(), budgetActionImport.toPersBudget());
			if (persBudget != null) {
				setToPersBudgetRelationship(persBudget);
			} else {
				throw new Exception("Pas de PersBudget dans le SI correspondant a l'identifiant " + budgetActionImport.toPersBudget().pbudSource());
			}
		}
		
		setToLolfNomenclatureDepenseRelationship(budgetActionImport.toLolfNomenclatureDepense());
	}
}
