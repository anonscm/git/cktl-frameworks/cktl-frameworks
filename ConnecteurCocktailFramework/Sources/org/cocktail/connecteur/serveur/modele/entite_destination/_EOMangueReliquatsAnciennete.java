// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueReliquatsAnciennete.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueReliquatsAnciennete extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueReliquatsAnciennete";

	// Attributes
	public static final String ANC_ANNEE_KEY = "ancAnnee";
	public static final String ANC_NB_ANNEES_KEY = "ancNbAnnees";
	public static final String ANC_NB_JOURS_KEY = "ancNbJours";
	public static final String ANC_NB_MOIS_KEY = "ancNbMois";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_UTILISE_KEY = "temUtilise";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ELEMENT_CARRIERE_KEY = "elementCarriere";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOTIF_REDUCTION_KEY = "motifReduction";

  private static Logger LOG = Logger.getLogger(_EOMangueReliquatsAnciennete.class);

  public EOMangueReliquatsAnciennete localInstanceIn(EOEditingContext editingContext) {
    EOMangueReliquatsAnciennete localInstance = (EOMangueReliquatsAnciennete)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer ancAnnee() {
    return (Integer) storedValueForKey("ancAnnee");
  }

  public void setAncAnnee(Integer value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating ancAnnee from " + ancAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, "ancAnnee");
  }

  public Integer ancNbAnnees() {
    return (Integer) storedValueForKey("ancNbAnnees");
  }

  public void setAncNbAnnees(Integer value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating ancNbAnnees from " + ancNbAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbAnnees");
  }

  public Integer ancNbJours() {
    return (Integer) storedValueForKey("ancNbJours");
  }

  public void setAncNbJours(Integer value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating ancNbJours from " + ancNbJours() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbJours");
  }

  public Integer ancNbMois() {
    return (Integer) storedValueForKey("ancNbMois");
  }

  public void setAncNbMois(Integer value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating ancNbMois from " + ancNbMois() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbMois");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temUtilise() {
    return (String) storedValueForKey("temUtilise");
  }

  public void setTemUtilise(String value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating temUtilise from " + temUtilise() + " to " + value);
    }
    takeStoredValueForKey(value, "temUtilise");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueReliquatsAnciennete.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere elementCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere)storedValueForKey("elementCarriere");
  }

  public void setElementCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
      _EOMangueReliquatsAnciennete.LOG.debug("updating elementCarriere from " + elementCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere oldValue = elementCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "elementCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "elementCarriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
      _EOMangueReliquatsAnciennete.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction motifReduction() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction)storedValueForKey("motifReduction");
  }

  public void setMotifReductionRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction value) {
    if (_EOMangueReliquatsAnciennete.LOG.isDebugEnabled()) {
      _EOMangueReliquatsAnciennete.LOG.debug("updating motifReduction from " + motifReduction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction oldValue = motifReduction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "motifReduction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "motifReduction");
    }
  }
  

  public static EOMangueReliquatsAnciennete createMangueReliquatsAnciennete(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere elementCarriere, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueReliquatsAnciennete eo = (EOMangueReliquatsAnciennete) EOUtilities.createAndInsertInstance(editingContext, _EOMangueReliquatsAnciennete.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setElementCarriereRelationship(elementCarriere);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueReliquatsAnciennete> fetchAllMangueReliquatsAnciennetes(EOEditingContext editingContext) {
    return _EOMangueReliquatsAnciennete.fetchAllMangueReliquatsAnciennetes(editingContext, null);
  }

  public static NSArray<EOMangueReliquatsAnciennete> fetchAllMangueReliquatsAnciennetes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueReliquatsAnciennete.fetchMangueReliquatsAnciennetes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueReliquatsAnciennete> fetchMangueReliquatsAnciennetes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueReliquatsAnciennete.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueReliquatsAnciennete> eoObjects = (NSArray<EOMangueReliquatsAnciennete>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueReliquatsAnciennete fetchMangueReliquatsAnciennete(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueReliquatsAnciennete.fetchMangueReliquatsAnciennete(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueReliquatsAnciennete fetchMangueReliquatsAnciennete(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueReliquatsAnciennete> eoObjects = _EOMangueReliquatsAnciennete.fetchMangueReliquatsAnciennetes(editingContext, qualifier, null);
    EOMangueReliquatsAnciennete eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueReliquatsAnciennete)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueReliquatsAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueReliquatsAnciennete fetchRequiredMangueReliquatsAnciennete(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueReliquatsAnciennete.fetchRequiredMangueReliquatsAnciennete(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueReliquatsAnciennete fetchRequiredMangueReliquatsAnciennete(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueReliquatsAnciennete eoObject = _EOMangueReliquatsAnciennete.fetchMangueReliquatsAnciennete(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueReliquatsAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueReliquatsAnciennete localInstanceIn(EOEditingContext editingContext, EOMangueReliquatsAnciennete eo) {
    EOMangueReliquatsAnciennete localInstance = (eo == null) ? null : (EOMangueReliquatsAnciennete)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
