// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueElementCarriere.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueElementCarriere extends ObjetPourSIDestinataireAvecCarriere {
	public static final String ENTITY_NAME = "MangueElementCarriere";

	// Attributes
	public static final String C_CHEVRON_KEY = "cChevron";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_TYPE_ACCES_KEY = "cTypeAcces";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRETE_CARRIERE_KEY = "dArreteCarriere";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_EFFET_ELEMENT_KEY = "dEffetElement";
	public static final String D_FIN_ELEMENT_KEY = "dFinElement";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INM_EFFECTIF_KEY = "inmEffectif";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NO_ARRETE_CARRIERE_KEY = "noArreteCarriere";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String NO_SEQ_ELEMENT_KEY = "noSeqElement";
	public static final String QUOTITE_ELEMENT_KEY = "quotiteElement";
	public static final String REP_ANC_ECH_ANNEES_KEY = "repAncEchAnnees";
	public static final String REP_ANC_ECH_JOURS_KEY = "repAncEchJours";
	public static final String REP_ANC_ECH_MOIS_KEY = "repAncEchMois";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PROVISOIRE_KEY = "temProvisoire";
	public static final String TEM_SIGNE_KEY = "temSigne";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOMangueElementCarriere.class);

  public EOMangueElementCarriere localInstanceIn(EOEditingContext editingContext) {
    EOMangueElementCarriere localInstance = (EOMangueElementCarriere)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cChevron() {
    return (String) storedValueForKey("cChevron");
  }

  public void setCChevron(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating cChevron from " + cChevron() + " to " + value);
    }
    takeStoredValueForKey(value, "cChevron");
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String cTypeAcces() {
    return (String) storedValueForKey("cTypeAcces");
  }

  public void setCTypeAcces(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating cTypeAcces from " + cTypeAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAcces");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArreteCarriere() {
    return (NSTimestamp) storedValueForKey("dArreteCarriere");
  }

  public void setDArreteCarriere(NSTimestamp value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating dArreteCarriere from " + dArreteCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dArreteCarriere");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dEffetElement() {
    return (NSTimestamp) storedValueForKey("dEffetElement");
  }

  public void setDEffetElement(NSTimestamp value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating dEffetElement from " + dEffetElement() + " to " + value);
    }
    takeStoredValueForKey(value, "dEffetElement");
  }

  public NSTimestamp dFinElement() {
    return (NSTimestamp) storedValueForKey("dFinElement");
  }

  public void setDFinElement(NSTimestamp value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating dFinElement from " + dFinElement() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinElement");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer inmEffectif() {
    return (Integer) storedValueForKey("inmEffectif");
  }

  public void setInmEffectif(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating inmEffectif from " + inmEffectif() + " to " + value);
    }
    takeStoredValueForKey(value, "inmEffectif");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String noArreteCarriere() {
    return (String) storedValueForKey("noArreteCarriere");
  }

  public void setNoArreteCarriere(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating noArreteCarriere from " + noArreteCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteCarriere");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public Integer noSeqElement() {
    return (Integer) storedValueForKey("noSeqElement");
  }

  public void setNoSeqElement(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating noSeqElement from " + noSeqElement() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqElement");
  }

  public Integer quotiteElement() {
    return (Integer) storedValueForKey("quotiteElement");
  }

  public void setQuotiteElement(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating quotiteElement from " + quotiteElement() + " to " + value);
    }
    takeStoredValueForKey(value, "quotiteElement");
  }

  public Integer repAncEchAnnees() {
    return (Integer) storedValueForKey("repAncEchAnnees");
  }

  public void setRepAncEchAnnees(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating repAncEchAnnees from " + repAncEchAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchAnnees");
  }

  public Integer repAncEchJours() {
    return (Integer) storedValueForKey("repAncEchJours");
  }

  public void setRepAncEchJours(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating repAncEchJours from " + repAncEchJours() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchJours");
  }

  public Integer repAncEchMois() {
    return (Integer) storedValueForKey("repAncEchMois");
  }

  public void setRepAncEchMois(Integer value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating repAncEchMois from " + repAncEchMois() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchMois");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temProvisoire() {
    return (String) storedValueForKey("temProvisoire");
  }

  public void setTemProvisoire(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating temProvisoire from " + temProvisoire() + " to " + value);
    }
    takeStoredValueForKey(value, "temProvisoire");
  }

  public String temSigne() {
    return (String) storedValueForKey("temSigne");
  }

  public void setTemSigne(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating temSigne from " + temSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temSigne");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
    	_EOMangueElementCarriere.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
      _EOMangueElementCarriere.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOMangueElementCarriere.LOG.isDebugEnabled()) {
      _EOMangueElementCarriere.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOMangueElementCarriere createMangueElementCarriere(EOEditingContext editingContext, String cCorps
, String cGrade
, NSTimestamp dCreation
, NSTimestamp dEffetElement
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, Integer noSeqElement
, String temProvisoire
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere toCarriere) {
    EOMangueElementCarriere eo = (EOMangueElementCarriere) EOUtilities.createAndInsertInstance(editingContext, _EOMangueElementCarriere.ENTITY_NAME);    
		eo.setCCorps(cCorps);
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDEffetElement(dEffetElement);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setNoSeqElement(noSeqElement);
		eo.setTemProvisoire(temProvisoire);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setToCarriereRelationship(toCarriere);
    return eo;
  }

  public static NSArray<EOMangueElementCarriere> fetchAllMangueElementCarrieres(EOEditingContext editingContext) {
    return _EOMangueElementCarriere.fetchAllMangueElementCarrieres(editingContext, null);
  }

  public static NSArray<EOMangueElementCarriere> fetchAllMangueElementCarrieres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueElementCarriere.fetchMangueElementCarrieres(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueElementCarriere> fetchMangueElementCarrieres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueElementCarriere.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueElementCarriere> eoObjects = (NSArray<EOMangueElementCarriere>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueElementCarriere fetchMangueElementCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueElementCarriere.fetchMangueElementCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueElementCarriere fetchMangueElementCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueElementCarriere> eoObjects = _EOMangueElementCarriere.fetchMangueElementCarrieres(editingContext, qualifier, null);
    EOMangueElementCarriere eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueElementCarriere)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueElementCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueElementCarriere fetchRequiredMangueElementCarriere(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueElementCarriere.fetchRequiredMangueElementCarriere(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueElementCarriere fetchRequiredMangueElementCarriere(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueElementCarriere eoObject = _EOMangueElementCarriere.fetchMangueElementCarriere(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueElementCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueElementCarriere localInstanceIn(EOEditingContext editingContext, EOMangueElementCarriere eo) {
    EOMangueElementCarriere localInstance = (eo == null) ? null : (EOMangueElementCarriere)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
