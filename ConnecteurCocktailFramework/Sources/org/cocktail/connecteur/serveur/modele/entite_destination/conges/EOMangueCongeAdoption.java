package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCongeAdoptionCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEnfantCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAdoption;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongeAdoption extends _EOMangueCongeAdoption {

	/** Recherche des delegations d'un individu pendant une periode donnee 
	 * @param individu Grhum
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 * @return retourne les temps partiels trouves et tous les tp si individu et debutPeriode sont nuls
	 */
	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	
	// Méthodes protégées
	protected void initialiserObjet(ObjetImport record) throws Exception {
		EOCongeAdoption congeAdoption = (EOCongeAdoption) record;
		
		EOGrhumEnfant grhumEnfant = rechercherEnfantGrhum(congeAdoption);
		setEnfantRelationship(grhumEnfant);
		
		EOGrhumIndividu grhumIndividu = EOIndividuCorresp.individuGrhum(editingContext(), congeAdoption.individu());
		setIndividuRelationship(grhumIndividu);
		
		EOCongeAdoption congeImport = (EOCongeAdoption) record;
		if (congeImport.congeAnnulation() != null) {
			EOCongeAdoptionCorresp congeAnnulationCorresp = EOCongeAdoptionCorresp.fetchCongeAdoptionCorresp(editingContext(),
					EOCongeAdoptionCorresp.CONGE_ADOPTION_KEY, congeImport.congeAnnulation());
			if (congeAnnulationCorresp == null)
				throw new Exception("Pas de déclaration accident dans objetImport");
			setToCongeAnnulationRelationship(congeAnnulationCorresp.mangueCongeAdoption());
		}
	}
	
	private EOGrhumEnfant rechercherEnfantGrhum(EOCongeAdoption congeAdoption) {
		EOEnfantCorresp enfantCorresp = EOEnfantCorresp.fetchEnfantCorresp(editingContext(), EOEnfantCorresp.ENFANT_KEY, congeAdoption.enfant());
		if (enfantCorresp==null)
			return null;
		return enfantCorresp.enfantGrhum();
	}

	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongeAdoption)value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongeAdoptionCorresp congeCorresp = EOCongeAdoptionCorresp.fetchCongeAdoptionCorresp(editingContext(), EOCongeAdoptionCorresp.CONGE_ADOPTION_KEY,
				congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.mangueCongeAdoption();
	}

	@Override
	public NSTimestamp dateCommission() {
		return null;
	}

	@Override
	public String typeEvenement() {
		return null;
	}
}
