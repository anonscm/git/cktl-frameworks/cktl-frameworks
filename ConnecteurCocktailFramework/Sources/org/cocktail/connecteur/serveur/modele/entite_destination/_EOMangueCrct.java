// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCrct.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCrct extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueCrct";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CONGE_ANNUL_ORDRE_KEY = "congeAnnulOrdre";
	public static final String C_ORIGINE_DEMANDE_KEY = "cOrigineDemande";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String QUOTITE_SERVICE_KEY = "quotiteService";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_FRACTIONNEMENT_KEY = "temFractionnement";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_CONGE_DE_REMPLACEMENT_KEY = "toCongeDeRemplacement";
	public static final String TO_ORIGINE_DEMANDE_KEY = "toOrigineDemande";

  private static Logger LOG = Logger.getLogger(_EOMangueCrct.class);

  public EOMangueCrct localInstanceIn(EOEditingContext editingContext) {
    EOMangueCrct localInstance = (EOMangueCrct)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Integer congeAnnulOrdre() {
    return (Integer) storedValueForKey("congeAnnulOrdre");
  }

  public void setCongeAnnulOrdre(Integer value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating congeAnnulOrdre from " + congeAnnulOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "congeAnnulOrdre");
  }

  public String cOrigineDemande() {
    return (String) storedValueForKey("cOrigineDemande");
  }

  public void setCOrigineDemande(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating cOrigineDemande from " + cOrigineDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "cOrigineDemande");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey("noCnu");
  }

  public void setNoCnu(Integer value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "noCnu");
  }

  public java.math.BigDecimal quotiteService() {
    return (java.math.BigDecimal) storedValueForKey("quotiteService");
  }

  public void setQuotiteService(java.math.BigDecimal value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating quotiteService from " + quotiteService() + " to " + value);
    }
    takeStoredValueForKey(value, "quotiteService");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temFractionnement() {
    return (String) storedValueForKey("temFractionnement");
  }

  public void setTemFractionnement(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating temFractionnement from " + temFractionnement() + " to " + value);
    }
    takeStoredValueForKey(value, "temFractionnement");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
    	_EOMangueCrct.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
      _EOMangueCrct.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu toCnu() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
      _EOMangueCrct.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct toCongeDeRemplacement() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct)storedValueForKey("toCongeDeRemplacement");
  }

  public void setToCongeDeRemplacementRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
      _EOMangueCrct.LOG.debug("updating toCongeDeRemplacement from " + toCongeDeRemplacement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCrct oldValue = toCongeDeRemplacement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeDeRemplacement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeDeRemplacement");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOOrigineDemande toOrigineDemande() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOOrigineDemande)storedValueForKey("toOrigineDemande");
  }

  public void setToOrigineDemandeRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOOrigineDemande value) {
    if (_EOMangueCrct.LOG.isDebugEnabled()) {
      _EOMangueCrct.LOG.debug("updating toOrigineDemande from " + toOrigineDemande() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOOrigineDemande oldValue = toOrigineDemande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toOrigineDemande");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toOrigineDemande");
    }
  }
  

  public static EOMangueCrct createMangueCrct(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temConfirme
, String temFractionnement
, String temGestEtab
, String temValide
) {
    EOMangueCrct eo = (EOMangueCrct) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCrct.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemConfirme(temConfirme);
		eo.setTemFractionnement(temFractionnement);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueCrct> fetchAllMangueCrcts(EOEditingContext editingContext) {
    return _EOMangueCrct.fetchAllMangueCrcts(editingContext, null);
  }

  public static NSArray<EOMangueCrct> fetchAllMangueCrcts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCrct.fetchMangueCrcts(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCrct> fetchMangueCrcts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCrct.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCrct> eoObjects = (NSArray<EOMangueCrct>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCrct fetchMangueCrct(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCrct.fetchMangueCrct(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCrct fetchMangueCrct(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCrct> eoObjects = _EOMangueCrct.fetchMangueCrcts(editingContext, qualifier, null);
    EOMangueCrct eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCrct)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCrct that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCrct fetchRequiredMangueCrct(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCrct.fetchRequiredMangueCrct(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCrct fetchRequiredMangueCrct(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCrct eoObject = _EOMangueCrct.fetchMangueCrct(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCrct that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCrct localInstanceIn(EOEditingContext editingContext, EOMangueCrct eo) {
    EOMangueCrct localInstance = (eo == null) ? null : (EOMangueCrct)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
