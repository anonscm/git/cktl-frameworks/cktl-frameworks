// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOManguePersBudgetAction.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOManguePersBudgetAction extends ObjetPourSIDestinataire {
	public static final String ENTITY_NAME = "ManguePersBudgetAction";

	// Attributes
	public static final String POURCENTAGE_KEY = "pourcentage";

	// Relationships
	public static final String TO_LOLF_NOMENCLATURE_DEPENSE_KEY = "toLolfNomenclatureDepense";
	public static final String TO_PERS_BUDGET_KEY = "toPersBudget";

  private static Logger LOG = Logger.getLogger(_EOManguePersBudgetAction.class);

  public EOManguePersBudgetAction localInstanceIn(EOEditingContext editingContext) {
    EOManguePersBudgetAction localInstance = (EOManguePersBudgetAction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Double pourcentage() {
    return (Double) storedValueForKey("pourcentage");
  }

  public void setPourcentage(Double value) {
    if (_EOManguePersBudgetAction.LOG.isDebugEnabled()) {
    	_EOManguePersBudgetAction.LOG.debug( "updating pourcentage from " + pourcentage() + " to " + value);
    }
    takeStoredValueForKey(value, "pourcentage");
  }

  public org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense toLolfNomenclatureDepense() {
    return (org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense)storedValueForKey("toLolfNomenclatureDepense");
  }

  public void setToLolfNomenclatureDepenseRelationship(org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense value) {
    if (_EOManguePersBudgetAction.LOG.isDebugEnabled()) {
      _EOManguePersBudgetAction.LOG.debug("updating toLolfNomenclatureDepense from " + toLolfNomenclatureDepense() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense oldValue = toLolfNomenclatureDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLolfNomenclatureDepense");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLolfNomenclatureDepense");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget toPersBudget() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget)storedValueForKey("toPersBudget");
  }

  public void setToPersBudgetRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget value) {
    if (_EOManguePersBudgetAction.LOG.isDebugEnabled()) {
      _EOManguePersBudgetAction.LOG.debug("updating toPersBudget from " + toPersBudget() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget oldValue = toPersBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersBudget");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersBudget");
    }
  }
  

  public static EOManguePersBudgetAction createManguePersBudgetAction(EOEditingContext editingContext, Double pourcentage
, org.cocktail.connecteur.common.modele.jefy.EOLolfNomenclatureDepense toLolfNomenclatureDepense, org.cocktail.connecteur.serveur.modele.entite_destination.EOManguePersBudget toPersBudget) {
    EOManguePersBudgetAction eo = (EOManguePersBudgetAction) EOUtilities.createAndInsertInstance(editingContext, _EOManguePersBudgetAction.ENTITY_NAME);    
		eo.setPourcentage(pourcentage);
    eo.setToLolfNomenclatureDepenseRelationship(toLolfNomenclatureDepense);
    eo.setToPersBudgetRelationship(toPersBudget);
    return eo;
  }

  public static NSArray<EOManguePersBudgetAction> fetchAllManguePersBudgetActions(EOEditingContext editingContext) {
    return _EOManguePersBudgetAction.fetchAllManguePersBudgetActions(editingContext, null);
  }

  public static NSArray<EOManguePersBudgetAction> fetchAllManguePersBudgetActions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOManguePersBudgetAction.fetchManguePersBudgetActions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOManguePersBudgetAction> fetchManguePersBudgetActions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOManguePersBudgetAction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOManguePersBudgetAction> eoObjects = (NSArray<EOManguePersBudgetAction>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOManguePersBudgetAction fetchManguePersBudgetAction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePersBudgetAction.fetchManguePersBudgetAction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePersBudgetAction fetchManguePersBudgetAction(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOManguePersBudgetAction> eoObjects = _EOManguePersBudgetAction.fetchManguePersBudgetActions(editingContext, qualifier, null);
    EOManguePersBudgetAction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOManguePersBudgetAction)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ManguePersBudgetAction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePersBudgetAction fetchRequiredManguePersBudgetAction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePersBudgetAction.fetchRequiredManguePersBudgetAction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePersBudgetAction fetchRequiredManguePersBudgetAction(EOEditingContext editingContext, EOQualifier qualifier) {
    EOManguePersBudgetAction eoObject = _EOManguePersBudgetAction.fetchManguePersBudgetAction(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ManguePersBudgetAction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePersBudgetAction localInstanceIn(EOEditingContext editingContext, EOManguePersBudgetAction eo) {
    EOManguePersBudgetAction localInstance = (eo == null) ? null : (EOManguePersBudgetAction)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
