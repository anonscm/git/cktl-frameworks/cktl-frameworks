// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueAffectation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueAffectation extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueAffectation";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_AFFECTATION_KEY = "dDebAffectation";
	public static final String DEN_QUOT_AFFECTATION_KEY = "denQuotAffectation";
	public static final String D_FIN_AFFECTATION_KEY = "dFinAffectation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String NO_SEQ_CONTRAT_KEY = "noSeqContrat";
	public static final String NUM_QUOT_AFFECTATION_KEY = "numQuotAffectation";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TEM_PRINCIPALE_KEY = "temPrincipale";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CARRIERE_KEY = "carriere";
	public static final String CONTRAT_KEY = "contrat";
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOMangueAffectation.class);

  public EOMangueAffectation localInstanceIn(EOEditingContext editingContext) {
    EOMangueAffectation localInstance = (EOMangueAffectation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebAffectation() {
    return (NSTimestamp) storedValueForKey("dDebAffectation");
  }

  public void setDDebAffectation(NSTimestamp value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating dDebAffectation from " + dDebAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebAffectation");
  }

  public Integer denQuotAffectation() {
    return (Integer) storedValueForKey("denQuotAffectation");
  }

  public void setDenQuotAffectation(Integer value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating denQuotAffectation from " + denQuotAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotAffectation");
  }

  public NSTimestamp dFinAffectation() {
    return (NSTimestamp) storedValueForKey("dFinAffectation");
  }

  public void setDFinAffectation(NSTimestamp value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating dFinAffectation from " + dFinAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAffectation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public Integer noSeqContrat() {
    return (Integer) storedValueForKey("noSeqContrat");
  }

  public void setNoSeqContrat(Integer value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating noSeqContrat from " + noSeqContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqContrat");
  }

  public Integer numQuotAffectation() {
    return (Integer) storedValueForKey("numQuotAffectation");
  }

  public void setNumQuotAffectation(Integer value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating numQuotAffectation from " + numQuotAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotAffectation");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String temPrincipale() {
    return (String) storedValueForKey("temPrincipale");
  }

  public void setTemPrincipale(String value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating temPrincipale from " + temPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipale");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
    	_EOMangueAffectation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere carriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("carriere");
  }

  public void setCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
      _EOMangueAffectation.LOG.debug("updating carriere from " + carriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = carriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "carriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "carriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat contrat() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
      _EOMangueAffectation.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
      _EOMangueAffectation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOMangueAffectation.LOG.isDebugEnabled()) {
      _EOMangueAffectation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOMangueAffectation createMangueAffectation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebAffectation
, NSTimestamp dModification
, String temPrincipale
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure) {
    EOMangueAffectation eo = (EOMangueAffectation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueAffectation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebAffectation(dDebAffectation);
		eo.setDModification(dModification);
		eo.setTemPrincipale(temPrincipale);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setStructureRelationship(structure);
    return eo;
  }

  public static NSArray<EOMangueAffectation> fetchAllMangueAffectations(EOEditingContext editingContext) {
    return _EOMangueAffectation.fetchAllMangueAffectations(editingContext, null);
  }

  public static NSArray<EOMangueAffectation> fetchAllMangueAffectations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueAffectation.fetchMangueAffectations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueAffectation> fetchMangueAffectations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueAffectation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueAffectation> eoObjects = (NSArray<EOMangueAffectation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueAffectation fetchMangueAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueAffectation.fetchMangueAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueAffectation fetchMangueAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueAffectation> eoObjects = _EOMangueAffectation.fetchMangueAffectations(editingContext, qualifier, null);
    EOMangueAffectation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueAffectation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueAffectation fetchRequiredMangueAffectation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueAffectation.fetchRequiredMangueAffectation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueAffectation fetchRequiredMangueAffectation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueAffectation eoObject = _EOMangueAffectation.fetchMangueAffectation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueAffectation localInstanceIn(EOEditingContext editingContext, EOMangueAffectation eo) {
    EOMangueAffectation localInstance = (eo == null) ? null : (EOMangueAffectation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
