//EOGrhumStructure.java
//Created on Wed Jul 25 15:15:57 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOStructure;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOGrhumSeqStructure;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EORepartTypeGroupe;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOGrhumStructure extends _EOGrhumStructure implements InterfaceRecordAvecAutresAttributs, InterfaceGestionIdUtilisateur {

	public EOGrhumStructure() {
		super();
	}


	// InterfaceRecordAvecAutresAttributs
	/** Cretion de repart type groupe si ils n'existent pas */
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport record) {
		boolean estModifie = false;
		
		EOStructure structureImport=(EOStructure)record;
		// Vérifier si il y a des règles de priorité sur les attributs qui fait
		// qu'on ne doit pas y toucher
		EOGrhumPrioriteEntite priorite = null;
		if (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || record.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			priorite = prioriteEntite();
		}
		String message = "";
		
		// RepartTypeGroupe : S(Service) et G(roupe annuaire) sont à créer pour les structures autres que les personnes morales
		// Controle sur priorité, peut-être à revoir en fonction des attributs obligatoires ou non
		if ((priorite == null || priorite.attributPourLibelle("cTypeStructure", true) == null) && cTypeStructure() != null) {
			// && cTypeStructure().equals(EOTypeStructure.AUTRE) == false) {
			// Vérifier si il existe des repart type groupes pour cette structure de type groupe et service
			boolean creerGroupe = true, creerGroupeSpecife = true;
			NSArray repartsTg = Finder.rechercherAvecAttributEtValeurEgale(editingContext(), "RepartTypeGroupe", "cStructure", cStructure());
			if (repartsTg != null && repartsTg.count() > 0) {
				NSArray types = (NSArray) repartsTg.valueForKey("tgrpCode");
				creerGroupe = types.contains("G") == false;
				creerGroupeSpecife = types.contains(structureImport.typeGroupe()) == false;
			}
			if (creerGroupe) {
				EORepartTypeGroupe typeGroupe = new EORepartTypeGroupe();
				typeGroupe.initAvecStructureEtType(this, "G");
				editingContext().insertObject(typeGroupe);
				message = "creation repart type groupe pour le type Groupe\n";
				estModifie = true;
			}
			if (creerGroupeSpecife) {
				EORepartTypeGroupe typeGroupe; 
				typeGroupe = new EORepartTypeGroupe();
				typeGroupe.initAvecStructureEtType(this, structureImport.typeGroupe());
				editingContext().insertObject(typeGroupe);
				if (structureImport.typeGroupe().equals("S")) {
					typeGroupe = new EORepartTypeGroupe();
					typeGroupe.initAvecStructureEtType(this, "GI");
					editingContext().insertObject(typeGroupe);
				}
				message += "creation repart type groupe pour le type Service\n";
				estModifie = true;
			}
		}
		
		// Vérifier si la structure père a changé si il n'y a aucune règle de
		// priorité, sinon on ne peut rien faire
		if (priorite == null) {
			if (structureImport.strSourcePere() == null) { // Pas de structure père, supprimer la relation
				removeObjectFromBothSidesOfRelationshipWithKey(structurePere(), "structurePere");
				message += "suppression de la structure pere\n";
				estModifie = true;
			} else if (structureImport.strSourcePere().equals(structureImport.strSource()) == false) {
				// Rechercher la structure Grhum associée (elle existe forcément
				// car on crée les structures par ordre dans la hiérarchie)
				EOGrhumStructure structurePereCourante = EOStructureCorresp.structureGrhum(editingContext(), structureImport.strSourcePere());
				if (structurePere() != structurePereCourante) {
					// Changer la structure père
					addObjectToBothSidesOfRelationshipWithKey(structurePereCourante, "structurePere");
					message += "remplacement de la structure pere par " + structurePereCourante + "\n";

					estModifie = true;
				}
			} else { // la structure Grhum doit aussi être racine en pointant sur elle-même
				if (structurePere() != this) {
					addObjectToBothSidesOfRelationshipWithKey(this, "structurePere");
					message += "remplacement de la structure pere racine par " + this + "\n";
					estModifie = true;
				}
			}
		}
		if (estModifie && (record.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || record.operation().equals(ObjetImport.OPERATION_UPDATE))) {
			LogManager.logDetail(message);
		}
		return estModifie;
	}

	// Méthodes protégées
	protected void initialiserObjet(ObjetImport record) {
		EOStructure structureImport=(EOStructure) record;
		setCStructure(EOGrhumSeqStructure.clePrimairePour(editingContext()).toString());
		setPersId(new Integer(SuperFinder.construirePersId(editingContext()).intValue()));
		if (structureImport.strSourcePere() != null) {
			EOGrhumStructure structurePere = EOStructureCorresp.structureGrhum(editingContext(), structureImport.strSourcePere());
			setStructurePereRelationship(structurePere);
		}
		
		if (structureImport.responsable()!=null) {
			EOGrhumIndividu grpResponsable = EOIndividuCorresp.individuGrhum(editingContext(), structureImport.responsable().idSource());
			setToGrpResponsableRelationship(grpResponsable);
		}

		setGrpAcces("+");
	}

	// Méthodes statiques
	/** Retourne les structures valides qui ont le m&ecirc;me libelle long */
	public static NSArray rechercherLibelleLong(EOEditingContext editingContext, String libelle) {
		if (libelle==null)
			return null;
		if (editingContext==null)
			editingContext=new EOEditingContext();
		NSMutableArray args = new NSMutableArray(libelle);
		String stringQualifier = LL_STRUCTURE_KEY+" = %@ AND "+TEM_VALIDE_KEY+" = 'O'";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, myQualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);
	}


	public static EOGrhumStructure fetchRNE(String rne) {
		return EOGrhumStructure.fetchGrhumStructure(new EOEditingContext(), C_RNE_KEY, rne);
	}
}
