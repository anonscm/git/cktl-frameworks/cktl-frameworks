// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueEmploiSpecialisation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueEmploiSpecialisation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueEmploiSpecialisation";

	// Attributes
	public static final String C_BAP_KEY = "cBap";
	public static final String C_DISC_SECOND_DEGRE_KEY = "cDiscSecondDegre";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String C_SPECIALITE_ITARF_KEY = "cSpecialiteItarf";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEBUT_KEY = "dDebut";
	public static final String D_FIN_KEY = "dFin";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_EMPLOI_KEY = "idEmploi";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	// Relationships
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_DISC_SECOND_DEGRE_KEY = "toDiscSecondDegre";
	public static final String TO_MANGUE_EMPLOI_KEY = "toMangueEmploi";
	public static final String TO_REFERENS_EMPLOIS_KEY = "toReferensEmplois";
	public static final String TO_SPECIALITE_ATOS_KEY = "toSpecialiteAtos";
	public static final String TO_SPECIALITE_ITARF_KEY = "toSpecialiteItarf";

  private static Logger LOG = Logger.getLogger(_EOMangueEmploiSpecialisation.class);

  public EOMangueEmploiSpecialisation localInstanceIn(EOEditingContext editingContext) {
    EOMangueEmploiSpecialisation localInstance = (EOMangueEmploiSpecialisation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public String cDiscSecondDegre() {
    return (String) storedValueForKey("cDiscSecondDegre");
  }

  public void setCDiscSecondDegre(String value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating cDiscSecondDegre from " + cDiscSecondDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiscSecondDegre");
  }

  public String codeemploi() {
    return (String) storedValueForKey("codeemploi");
  }

  public void setCodeemploi(String value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating codeemploi from " + codeemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeemploi");
  }

  public String cSpecialiteAtos() {
    return (String) storedValueForKey("cSpecialiteAtos");
  }

  public void setCSpecialiteAtos(String value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating cSpecialiteAtos from " + cSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteAtos");
  }

  public String cSpecialiteItarf() {
    return (String) storedValueForKey("cSpecialiteItarf");
  }

  public void setCSpecialiteItarf(String value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating cSpecialiteItarf from " + cSpecialiteItarf() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteItarf");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebut() {
    return (NSTimestamp) storedValueForKey("dDebut");
  }

  public void setDDebut(NSTimestamp value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating dDebut from " + dDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebut");
  }

  public NSTimestamp dFin() {
    return (NSTimestamp) storedValueForKey("dFin");
  }

  public void setDFin(NSTimestamp value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating dFin from " + dFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dFin");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idEmploi() {
    return (Integer) storedValueForKey("idEmploi");
  }

  public void setIdEmploi(Integer value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating idEmploi from " + idEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "idEmploi");
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey("noCnu");
  }

  public void setNoCnu(Integer value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "noCnu");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiSpecialisation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu toCnu() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiSpecialisation.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EODiscSecondDegre toDiscSecondDegre() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EODiscSecondDegre)storedValueForKey("toDiscSecondDegre");
  }

  public void setToDiscSecondDegreRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EODiscSecondDegre value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiSpecialisation.LOG.debug("updating toDiscSecondDegre from " + toDiscSecondDegre() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EODiscSecondDegre oldValue = toDiscSecondDegre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDiscSecondDegre");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDiscSecondDegre");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toMangueEmploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi)storedValueForKey("toMangueEmploi");
  }

  public void setToMangueEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiSpecialisation.LOG.debug("updating toMangueEmploi from " + toMangueEmploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi oldValue = toMangueEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueEmploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueEmploi");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOReferensEmplois toReferensEmplois() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOReferensEmplois)storedValueForKey("toReferensEmplois");
  }

  public void setToReferensEmploisRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOReferensEmplois value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiSpecialisation.LOG.debug("updating toReferensEmplois from " + toReferensEmplois() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOReferensEmplois oldValue = toReferensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensEmplois");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensEmplois");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteAtos toSpecialiteAtos() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteAtos)storedValueForKey("toSpecialiteAtos");
  }

  public void setToSpecialiteAtosRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteAtos value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiSpecialisation.LOG.debug("updating toSpecialiteAtos from " + toSpecialiteAtos() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteAtos oldValue = toSpecialiteAtos();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSpecialiteAtos");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSpecialiteAtos");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf toSpecialiteItarf() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf)storedValueForKey("toSpecialiteItarf");
  }

  public void setToSpecialiteItarfRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf value) {
    if (_EOMangueEmploiSpecialisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiSpecialisation.LOG.debug("updating toSpecialiteItarf from " + toSpecialiteItarf() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSpecialiteItarf oldValue = toSpecialiteItarf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSpecialiteItarf");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSpecialiteItarf");
    }
  }
  

  public static EOMangueEmploiSpecialisation createMangueEmploiSpecialisation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebut
, NSTimestamp dModification
, Integer idEmploi
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toMangueEmploi) {
    EOMangueEmploiSpecialisation eo = (EOMangueEmploiSpecialisation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueEmploiSpecialisation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebut(dDebut);
		eo.setDModification(dModification);
		eo.setIdEmploi(idEmploi);
    eo.setToMangueEmploiRelationship(toMangueEmploi);
    return eo;
  }

  public static NSArray<EOMangueEmploiSpecialisation> fetchAllMangueEmploiSpecialisations(EOEditingContext editingContext) {
    return _EOMangueEmploiSpecialisation.fetchAllMangueEmploiSpecialisations(editingContext, null);
  }

  public static NSArray<EOMangueEmploiSpecialisation> fetchAllMangueEmploiSpecialisations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueEmploiSpecialisation.fetchMangueEmploiSpecialisations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueEmploiSpecialisation> fetchMangueEmploiSpecialisations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueEmploiSpecialisation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueEmploiSpecialisation> eoObjects = (NSArray<EOMangueEmploiSpecialisation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueEmploiSpecialisation fetchMangueEmploiSpecialisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiSpecialisation.fetchMangueEmploiSpecialisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiSpecialisation fetchMangueEmploiSpecialisation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueEmploiSpecialisation> eoObjects = _EOMangueEmploiSpecialisation.fetchMangueEmploiSpecialisations(editingContext, qualifier, null);
    EOMangueEmploiSpecialisation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueEmploiSpecialisation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueEmploiSpecialisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiSpecialisation fetchRequiredMangueEmploiSpecialisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiSpecialisation.fetchRequiredMangueEmploiSpecialisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiSpecialisation fetchRequiredMangueEmploiSpecialisation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueEmploiSpecialisation eoObject = _EOMangueEmploiSpecialisation.fetchMangueEmploiSpecialisation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueEmploiSpecialisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiSpecialisation localInstanceIn(EOEditingContext editingContext, EOMangueEmploiSpecialisation eo) {
    EOMangueEmploiSpecialisation localInstance = (eo == null) ? null : (EOMangueEmploiSpecialisation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
