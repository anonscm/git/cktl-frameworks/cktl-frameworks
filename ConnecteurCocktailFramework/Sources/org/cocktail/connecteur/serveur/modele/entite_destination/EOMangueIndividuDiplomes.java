// EOMangueIndividuDiplomes.java
// Created on Mon Mar 17 16:07:10  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;


import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuDiplomes;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// 29/09/2011 - modifier les méthodes temValide et setTemValide lorsqu'elles seront définies dans Mangue. Modifier aussi le fetch
public class EOMangueIndividuDiplomes extends _EOMangueIndividuDiplomes {

	public EOMangueIndividuDiplomes() {
		super();
	}

	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {

		EOIndividuDiplomes individuDiplome = (EOIndividuDiplomes)recordImport;
		// A ce stade, l'individu grhum est dans le SI Destinataire
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), individuDiplome.individu().idSource());
		setTemValide("O");
		if (individuGrhum != null) {
			addObjectToBothSidesOfRelationshipWithKey(individuGrhum, "individu");
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + individuDiplome.individu().idSource());
		}
	}
	// Méthodes statiques
	/** Retourne l'individuDiplomes trouve dans le SI Destinataire si il est unique, sinon null
	 * 
	 * @param editingContext
	 * @param individu EOIndividuDiplomes
	 * @return EOMangueIndividuDiplomes
	 */
	public static EOMangueIndividuDiplomes individuDiplomesDestinationPourIndividuDiplomes(EOEditingContext editingContext,EOIndividuDiplomes individuDiplome) {
		try {
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individuDiplome.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray args = new NSMutableArray(individuGrhum);
				args.addObject(individuDiplome.cDiplome());
				EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ AND cDiplome = %@",args);
								
				EOFetchSpecification fs = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
				NSArray<EOMangueIndividuDiplomes> individusDip = editingContext.objectsWithFetchSpecification(fs);
				if (individusDip.count() == 1) {
					return (EOMangueIndividuDiplomes)individusDip.objectAtIndex(0);
				}
			}
			
			return null;
		}
		catch (Exception e) {
			return null;
		}
	}

}
