// EOMangueChangementPosition.java
// Created on Mon Jan 07 16:34:51 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEnfantCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOChangementPosition;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// 08/07/2011 - Méthode ajoutée pour rechercher les changements de position d'un individu
public class EOMangueChangementPosition extends _EOMangueChangementPosition implements InterfaceRecordAvecAutresAttributs {

	public EOMangueChangementPosition() {
		super();
	}
	
	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		setTemPcAcquitee("N");
		EOChangementPosition chgtPositionImport = (EOChangementPosition)recordImport;
		EOEditingContext edc = this.editingContext();
		
		if (chgtPositionImport.toEnfant() != null) {

			EOGrhumEnfant enfant = EOGrhumEnfant.enfantDestinationPourEnfant(editingContext(), chgtPositionImport.toEnfant());
			if (enfant != null) {
				setToGrhumEnfantRelationship(enfant);
			} else {
				throw new Exception("Pas d'enfant dans le SI correspondant a l'identifiant enfSource : " + chgtPositionImport.enfSource());
			}
		}
		
		if (chgtPositionImport.toCarriereOrigine() != null) {
			EOMangueCarriere carriereMangue = ((EOCarriereCorresp) chgtPositionImport.toCarriereOrigine().correspondance()).carriereMangue();
			if (carriereMangue == null) {
				throw new Exception("Pas de segment de carriere pour cet individu dans le SI correspondant a l'identifiant "
						+ chgtPositionImport.toCarriereOrigine().carSource());
			}
			setToCarriereOrigineRelationship(carriereMangue);
		}

	}
	// Méthodes statiques
	/** Retourne le changement de position valide equivalent pour cet individu et ce segment de carriere 
	 * qui commence a la meme date.<BR>
	 * @param editingContext
	 * @param changement
	 * @return EOMangueChangementPosition ou null si non trouve
	 */
	public static EOMangueChangementPosition changementDestinationPourChangement(EOEditingContext editingContext,EOChangementPosition changement) {
		NSArray changements = rechercherChangementsManguePourChangement(editingContext,changement);
		// Si il y en a un, il est unique car pas de chevauchement des changements de position
		if (changements != null && changements.count() == 1) {
			return (EOMangueChangementPosition)changements.objectAtIndex(0);
		} else {
			return null;
		}
	}
	/** retourne les changements de position associes a un segment de carriere pendant la eriode fournie
	 * @param editingContext
	 * @param carriere carriere Mangue
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode	 peut etre nulle
	 */
	public static NSArray rechercherChangementsPositionDestinValidesSurPeriode(EOEditingContext editingContext,EOMangueCarriere carriere,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(carriere);
		String stringQualifier = TO_CARRIERE_KEY + " = %@ AND temValide = 'O'";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		if (debutPeriode != null) {
			qualifier = Finder.qualifierPourPeriode(D_DEB_POSITION_KEY,debutPeriode, D_FIN_POSITION_KEY,finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
		}
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);

		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** retourne les changements de position associes a un segment de carriere pendant la periode fournie
	 * @param editingContext
	 * @param carriere carriere Mangue
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode	 peut etre nulle
	 */
	public static NSArray rechercherChangementsPositionDestinPourIndividuValidesSurPeriode(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(individu);
		String stringQualifier = INDIVIDU_KEY+" = %@ AND "+TEM_VALIDE_KEY+" = 'O'";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		if (debutPeriode != null) {
			qualifier = Finder.qualifierPourPeriode(D_DEB_POSITION_KEY,debutPeriode,D_FIN_POSITION_KEY,finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
		}
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);

		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	// Méthodes privées
	/* Retourne les changements de position équivalents pour cet individu et ce segment de carrière 
	 * qui commence à la même date
	 * @param editingContext
	 * @param changement
	 * @return EOMangueChangementPosition
	 */
	private static NSArray rechercherChangementsManguePourChangement(EOEditingContext editingContext,EOChangementPosition changement) {
		if (changement.toCarriere() == null) {
			return null;
		}
		EOMangueCarriere carriereMangue = (EOMangueCarriere)changement.toCarriere().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (carriereMangue != null) {
			NSMutableArray args = new NSMutableArray(carriereMangue);
			// Vérifier que la date de début de position est la même : pour éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(changement.dDebPosition()));
			args.addObject(DateCtrl.jourSuivant(changement.dDebPosition()));
			String qualifier = TO_CARRIERE_KEY + " = %@ AND dDebPosition > %@ AND dDebPosition < %@ AND temValide ='O'";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		}
		return null;
	}

	public boolean modifierAutresAttributsAvecRecordImport(
			ObjetImport recordImport) {
		// TODO Auto-generated method stub
		return false;
	}
}
