/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** Cette classe est &grave; modifier le jour o&ugrave; le temoin temValide est gere dans Mangue */
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCldDetail;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueCldDetail extends _EOMangueCldDetail {
	public EOMangueCldDetail() {
		super();
	}


	// Méthodes ajoutées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		
		EOCldDetail detail = (EOCldDetail)recordImport;
		// A ce stade l'individu et le cld doivent être insérés dans le SI Destinataire
		EOMangueCld cldMangue = EOCldCorresp.cldMangue(editingContext(), detail.cld().eimpSource(), detail.cld().individu().idSource());
		if (cldMangue == null) {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant idSource : " + detail.cld().individu().idSource() + ", eimpSource : " + detail.cld().eimpSource());
		}
		addObjectToBothSidesOfRelationshipWithKey(cldMangue, "mangueCld");
	}

}
