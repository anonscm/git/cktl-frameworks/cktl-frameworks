// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumIndividu.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumIndividu extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumIndividu";

	// Attributes
	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String C_DEPT_NAISSANCE_KEY = "cDeptNaissance";
	public static final String C_PAYS_NAISSANCE_KEY = "cPaysNaissance";
	public static final String C_PAYS_NATIONALITE_KEY = "cPaysNationalite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_ACTIVITE_KEY = "indActivite";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_CLE_INSEE_PROV_KEY = "indCleInseeProv";
	public static final String IND_C_SIT_MILITAIRE_KEY = "indCSitMilitaire";
	public static final String IND_C_SITUATION_FAMILLE_KEY = "indCSituationFamille";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_NO_INSEE_PROV_KEY = "indNoInseeProv";
	public static final String IND_PHOTO_KEY = "indPhoto";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String LIEU_DECES_KEY = "lieuDeces";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_AFFICHAGE_KEY = "nomAffichage";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_KEY = "nomPatronymiqueAffichage";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRENOM2_KEY = "prenom2";
	public static final String PRENOM_AFFICHAGE_KEY = "prenomAffichage";
	public static final String PRISE_CPT_INSEE_KEY = "priseCptInsee";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String VILLE_DE_NAISSANCE_KEY = "villeDeNaissance";

	// Relationships
	public static final String PERSONNELS_KEY = "personnels";

  private static Logger LOG = Logger.getLogger(_EOGrhumIndividu.class);

  public EOGrhumIndividu localInstanceIn(EOEditingContext editingContext) {
    EOGrhumIndividu localInstance = (EOGrhumIndividu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCivilite() {
    return (String) storedValueForKey("cCivilite");
  }

  public void setCCivilite(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating cCivilite from " + cCivilite() + " to " + value);
    }
    takeStoredValueForKey(value, "cCivilite");
  }

  public String cDeptNaissance() {
    return (String) storedValueForKey("cDeptNaissance");
  }

  public void setCDeptNaissance(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating cDeptNaissance from " + cDeptNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "cDeptNaissance");
  }

  public String cPaysNaissance() {
    return (String) storedValueForKey("cPaysNaissance");
  }

  public void setCPaysNaissance(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating cPaysNaissance from " + cPaysNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "cPaysNaissance");
  }

  public String cPaysNationalite() {
    return (String) storedValueForKey("cPaysNationalite");
  }

  public void setCPaysNationalite(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating cPaysNationalite from " + cPaysNationalite() + " to " + value);
    }
    takeStoredValueForKey(value, "cPaysNationalite");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDeces() {
    return (NSTimestamp) storedValueForKey("dDeces");
  }

  public void setDDeces(NSTimestamp value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating dDeces from " + dDeces() + " to " + value);
    }
    takeStoredValueForKey(value, "dDeces");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey("dNaissance");
  }

  public void setDNaissance(NSTimestamp value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating dNaissance from " + dNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "dNaissance");
  }

  public String indActivite() {
    return (String) storedValueForKey("indActivite");
  }

  public void setIndActivite(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indActivite from " + indActivite() + " to " + value);
    }
    takeStoredValueForKey(value, "indActivite");
  }

  public Integer indCleInsee() {
    return (Integer) storedValueForKey("indCleInsee");
  }

  public void setIndCleInsee(Integer value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indCleInsee from " + indCleInsee() + " to " + value);
    }
    takeStoredValueForKey(value, "indCleInsee");
  }

  public Integer indCleInseeProv() {
    return (Integer) storedValueForKey("indCleInseeProv");
  }

  public void setIndCleInseeProv(Integer value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indCleInseeProv from " + indCleInseeProv() + " to " + value);
    }
    takeStoredValueForKey(value, "indCleInseeProv");
  }

  public String indCSitMilitaire() {
    return (String) storedValueForKey("indCSitMilitaire");
  }

  public void setIndCSitMilitaire(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indCSitMilitaire from " + indCSitMilitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "indCSitMilitaire");
  }

  public String indCSituationFamille() {
    return (String) storedValueForKey("indCSituationFamille");
  }

  public void setIndCSituationFamille(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indCSituationFamille from " + indCSituationFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "indCSituationFamille");
  }

  public String indNoInsee() {
    return (String) storedValueForKey("indNoInsee");
  }

  public void setIndNoInsee(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indNoInsee from " + indNoInsee() + " to " + value);
    }
    takeStoredValueForKey(value, "indNoInsee");
  }

  public String indNoInseeProv() {
    return (String) storedValueForKey("indNoInseeProv");
  }

  public void setIndNoInseeProv(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indNoInseeProv from " + indNoInseeProv() + " to " + value);
    }
    takeStoredValueForKey(value, "indNoInseeProv");
  }

  public String indPhoto() {
    return (String) storedValueForKey("indPhoto");
  }

  public void setIndPhoto(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indPhoto from " + indPhoto() + " to " + value);
    }
    takeStoredValueForKey(value, "indPhoto");
  }

  public String indQualite() {
    return (String) storedValueForKey("indQualite");
  }

  public void setIndQualite(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating indQualite from " + indQualite() + " to " + value);
    }
    takeStoredValueForKey(value, "indQualite");
  }

  public String lieuDeces() {
    return (String) storedValueForKey("lieuDeces");
  }

  public void setLieuDeces(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating lieuDeces from " + lieuDeces() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDeces");
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey("noIndividu");
  }

  public void setNoIndividu(Integer value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividu");
  }

  public String nomAffichage() {
    return (String) storedValueForKey("nomAffichage");
  }

  public void setNomAffichage(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating nomAffichage from " + nomAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "nomAffichage");
  }

  public String nomPatronymique() {
    return (String) storedValueForKey("nomPatronymique");
  }

  public void setNomPatronymique(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating nomPatronymique from " + nomPatronymique() + " to " + value);
    }
    takeStoredValueForKey(value, "nomPatronymique");
  }

  public String nomPatronymiqueAffichage() {
    return (String) storedValueForKey("nomPatronymiqueAffichage");
  }

  public void setNomPatronymiqueAffichage(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating nomPatronymiqueAffichage from " + nomPatronymiqueAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "nomPatronymiqueAffichage");
  }

  public String nomUsuel() {
    return (String) storedValueForKey("nomUsuel");
  }

  public void setNomUsuel(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating nomUsuel from " + nomUsuel() + " to " + value);
    }
    takeStoredValueForKey(value, "nomUsuel");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String prenom() {
    return (String) storedValueForKey("prenom");
  }

  public void setPrenom(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating prenom from " + prenom() + " to " + value);
    }
    takeStoredValueForKey(value, "prenom");
  }

  public String prenom2() {
    return (String) storedValueForKey("prenom2");
  }

  public void setPrenom2(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating prenom2 from " + prenom2() + " to " + value);
    }
    takeStoredValueForKey(value, "prenom2");
  }

  public String prenomAffichage() {
    return (String) storedValueForKey("prenomAffichage");
  }

  public void setPrenomAffichage(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating prenomAffichage from " + prenomAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "prenomAffichage");
  }

  public String priseCptInsee() {
    return (String) storedValueForKey("priseCptInsee");
  }

  public void setPriseCptInsee(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating priseCptInsee from " + priseCptInsee() + " to " + value);
    }
    takeStoredValueForKey(value, "priseCptInsee");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public String villeDeNaissance() {
    return (String) storedValueForKey("villeDeNaissance");
  }

  public void setVilleDeNaissance(String value) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
    	_EOGrhumIndividu.LOG.debug( "updating villeDeNaissance from " + villeDeNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "villeDeNaissance");
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel> personnels() {
    return (NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel>)storedValueForKey("personnels");
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel> personnels(EOQualifier qualifier) {
    return personnels(qualifier, null, false);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel> personnels(EOQualifier qualifier, boolean fetch) {
    return personnels(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel> personnels(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel.INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel.fetchGrhumPersonnels(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = personnels();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersonnelsRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel object) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
      _EOGrhumIndividu.LOG.debug("adding " + object + " to personnels relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "personnels");
  }

  public void removeFromPersonnelsRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel object) {
    if (_EOGrhumIndividu.LOG.isDebugEnabled()) {
      _EOGrhumIndividu.LOG.debug("removing " + object + " from personnels relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "personnels");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel createPersonnelsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GrhumPersonnel");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "personnels");
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel) eo;
  }

  public void deletePersonnelsRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "personnels");
    editingContext().deleteObject(object);
  }

  public void deleteAllPersonnelsRelationships() {
    Enumeration objects = personnels().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersonnelsRelationship((org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumPersonnel)objects.nextElement());
    }
  }


  public static EOGrhumIndividu createGrhumIndividu(EOEditingContext editingContext, String cCivilite
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noIndividu
, String nomUsuel
, Integer persId
, String prenom
) {
    EOGrhumIndividu eo = (EOGrhumIndividu) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumIndividu.ENTITY_NAME);    
		eo.setCCivilite(cCivilite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoIndividu(noIndividu);
		eo.setNomUsuel(nomUsuel);
		eo.setPersId(persId);
		eo.setPrenom(prenom);
    return eo;
  }

  public static NSArray<EOGrhumIndividu> fetchAllGrhumIndividus(EOEditingContext editingContext) {
    return _EOGrhumIndividu.fetchAllGrhumIndividus(editingContext, null);
  }

  public static NSArray<EOGrhumIndividu> fetchAllGrhumIndividus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumIndividu.fetchGrhumIndividus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumIndividu> fetchGrhumIndividus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumIndividu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumIndividu> eoObjects = (NSArray<EOGrhumIndividu>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumIndividu fetchGrhumIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividu.fetchGrhumIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividu fetchGrhumIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumIndividu> eoObjects = _EOGrhumIndividu.fetchGrhumIndividus(editingContext, qualifier, null);
    EOGrhumIndividu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumIndividu)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividu fetchRequiredGrhumIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividu.fetchRequiredGrhumIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividu fetchRequiredGrhumIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumIndividu eoObject = _EOGrhumIndividu.fetchGrhumIndividu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividu localInstanceIn(EOEditingContext editingContext, EOGrhumIndividu eo) {
    EOGrhumIndividu localInstance = (eo == null) ? null : (EOGrhumIndividu)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
