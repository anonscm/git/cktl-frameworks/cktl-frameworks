// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueDepart.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueDepart extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueDepart";

	// Attributes
	public static final String C_MOTIF_DEPART_KEY = "cMotifDepart";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CESSATION_SERVICE_KEY = "dCessationService";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_EFFET_RADIATION_KEY = "dEffetRadiation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_RADIATION_EMPLOI_KEY = "dRadiationEmploi";
	public static final String LIEU_DEPART_KEY = "lieuDepart";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_DEPART_PREVISIONNEL_KEY = "temDepartPrevisionnel";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_MOTIF_DEPART_KEY = "toMotifDepart";
	public static final String TO_RNE_KEY = "toRne";

  private static Logger LOG = Logger.getLogger(_EOMangueDepart.class);

  public EOMangueDepart localInstanceIn(EOEditingContext editingContext) {
    EOMangueDepart localInstance = (EOMangueDepart)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotifDepart() {
    return (String) storedValueForKey("cMotifDepart");
  }

  public void setCMotifDepart(String value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating cMotifDepart from " + cMotifDepart() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotifDepart");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCessationService() {
    return (NSTimestamp) storedValueForKey("dCessationService");
  }

  public void setDCessationService(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dCessationService from " + dCessationService() + " to " + value);
    }
    takeStoredValueForKey(value, "dCessationService");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dEffetRadiation() {
    return (NSTimestamp) storedValueForKey("dEffetRadiation");
  }

  public void setDEffetRadiation(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dEffetRadiation from " + dEffetRadiation() + " to " + value);
    }
    takeStoredValueForKey(value, "dEffetRadiation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dRadiationEmploi() {
    return (NSTimestamp) storedValueForKey("dRadiationEmploi");
  }

  public void setDRadiationEmploi(NSTimestamp value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating dRadiationEmploi from " + dRadiationEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dRadiationEmploi");
  }

  public String lieuDepart() {
    return (String) storedValueForKey("lieuDepart");
  }

  public void setLieuDepart(String value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating lieuDepart from " + lieuDepart() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDepart");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temDepartPrevisionnel() {
    return (String) storedValueForKey("temDepartPrevisionnel");
  }

  public void setTemDepartPrevisionnel(String value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating temDepartPrevisionnel from " + temDepartPrevisionnel() + " to " + value);
    }
    takeStoredValueForKey(value, "temDepartPrevisionnel");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
    	_EOMangueDepart.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
      _EOMangueDepart.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart toMotifDepart() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart)storedValueForKey("toMotifDepart");
  }

  public void setToMotifDepartRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
      _EOMangueDepart.LOG.debug("updating toMotifDepart from " + toMotifDepart() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart oldValue = toMotifDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMotifDepart");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMotifDepart");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne toRne() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne value) {
    if (_EOMangueDepart.LOG.isDebugEnabled()) {
      _EOMangueDepart.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  

  public static EOMangueDepart createMangueDepart(EOEditingContext editingContext, String cMotifDepart
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifDepart toMotifDepart) {
    EOMangueDepart eo = (EOMangueDepart) EOUtilities.createAndInsertInstance(editingContext, _EOMangueDepart.ENTITY_NAME);    
		eo.setCMotifDepart(cMotifDepart);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setToMotifDepartRelationship(toMotifDepart);
    return eo;
  }

  public static NSArray<EOMangueDepart> fetchAllMangueDeparts(EOEditingContext editingContext) {
    return _EOMangueDepart.fetchAllMangueDeparts(editingContext, null);
  }

  public static NSArray<EOMangueDepart> fetchAllMangueDeparts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueDepart.fetchMangueDeparts(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueDepart> fetchMangueDeparts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueDepart.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueDepart> eoObjects = (NSArray<EOMangueDepart>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueDepart fetchMangueDepart(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDepart.fetchMangueDepart(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDepart fetchMangueDepart(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueDepart> eoObjects = _EOMangueDepart.fetchMangueDeparts(editingContext, qualifier, null);
    EOMangueDepart eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueDepart)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueDepart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDepart fetchRequiredMangueDepart(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDepart.fetchRequiredMangueDepart(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDepart fetchRequiredMangueDepart(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueDepart eoObject = _EOMangueDepart.fetchMangueDepart(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueDepart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDepart localInstanceIn(EOEditingContext editingContext, EOMangueDepart eo) {
    EOMangueDepart localInstance = (eo == null) ? null : (EOMangueDepart)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
