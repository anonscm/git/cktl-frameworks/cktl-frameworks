package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;

public class EOGrhumInstance extends _EOGrhumInstance {
	public static EOGrhumInstance getFromCode(EOEditingContext editingContext, String codeInstance) {
		return fetchGrhumInstance(editingContext, C_INSTANCE_KEY, codeInstance);
	}
	
	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
	}

	@Override
	public String temValide() {
		// La table ne contient pas de TemValide (03/2015). On retourne donc systématiquement vrai
		return CocktailConstantes.VRAI;
	}

	@Override
	public void setTemValide(String value) {
		// La table ne contient pas de TemValide (03/2015). On surcharge donc la méthode pour ne rien faire
	}
}
