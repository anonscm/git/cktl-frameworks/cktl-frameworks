//EOMangueContrat.java
//Created on Mon Jan 28 13:52:21 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOContrat;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueContrat extends _EOMangueContrat {

	public EOMangueContrat() {
		super();
	}


	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOContrat contratImport = (EOContrat)recordImport;
		setTemAnnulation(CocktailConstantes.FAUX);
		// A ce stade, l'individu grhum est dans le SI Destinataire
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), contratImport.individu().idSource());
		if (individuGrhum != null) {
			addObjectToBothSidesOfRelationshipWithKey(individuGrhum, "individu");
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + contratImport.individu().idSource());
		}
	}
	/** return true si le contrat  n'est pas annule */
	public boolean estValide() {
		return temAnnulation() != null && temAnnulation().equals(CocktailConstantes.FAUX);
	}
	/** Surcharge pour gerer le temoin d'annulation */
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemAnnulation(CocktailConstantes.FAUX);
		} else {
			setTemAnnulation(CocktailConstantes.VRAI);
		}
	}
	// Méthodes statiques
	/** Retourne le contrat equivalent pour cet individu, qui commence a la m&ecirc;me date et
	 * a le m&ecirc;me type de contrat de travail. Si on trouve plusieurs contrats, on retourne null<BR>
	 * @param editingContext
	 * @param contrat
	 * @return EOMangueContrat
	 */
	public static EOMangueContrat contratDestinationPourContrat(EOEditingContext editingContext,EOContrat contrat) {
		NSArray results = rechercherContratsManguePourContrat(editingContext,contrat);
		if (results != null && results.count() == 1) {
			return (EOMangueContrat)results.objectAtIndex(0);
		} else {
			return null;
		}
	}
	/** Retourne true si on trouve plusieurs contrats de m&ecirc;me type commencant a la m&ecirc;me date */
	public static boolean aHomonyme(EOEditingContext editingContext,EOContrat contrat) {
		NSArray results = rechercherContratsManguePourContrat(editingContext,contrat);
		return (results != null && results.count() > 1);
	}
	/** retourne tous les contrats du SI Destinataire non annules, de remuneration principale 
	 * d'un individu dont les dates de debut et fin sont a cheval sur deux dates
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode peut &ecirc;tre nulle
	 * @return avenants trouv&eacutes;
	 */
	public static NSArray rechercherContratsRemunerationPrincipalePourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		// Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray args = new NSMutableArray(individuGrhum);
			String stringQualifier = "temAnnulation <> 'O' AND individu = %@";
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
			qualifiers.addObject(qualifier);
			qualifier = Finder.qualifierPourPeriode("dDebContratTrav",debutPeriode,"dFinContratTrav",finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
			EOFetchSpecification myFetch = new EOFetchSpecification("MangueContrat",qualifier,null);
			myFetch.setRefreshesRefetchedObjects(true);
			// Rechercher les contrats de rémunération principale
			NSMutableArray contratsValides = new NSMutableArray();
			java.util.Enumeration e = editingContext.objectsWithFetchSpecification(myFetch).objectEnumerator();
			while (e.hasMoreElements()) {
				EOMangueContrat contrat = (EOMangueContrat)e.nextElement();
				String cTypeContrat = contrat.cTypeContratTrav();
				EOTypeContratTravail typeContrat = (EOTypeContratTravail)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeContratTravail", "cTypeContratTrav", cTypeContrat);
				if (typeContrat != null && typeContrat.estRemunerationPrincipale()) {
					contratsValides.addObject(contrat);
				}
			}
			return contratsValides;
		} else {
			return null;	// individu pas encore créé
		}
	}
	public static NSArray rechercherContratsPourIndividuEtPeriode(EOEditingContext editingContext,EOGrhumIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(individu);
		String stringQualifier = "temAnnulation <> 'O' AND individu = %@";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		qualifier = Finder.qualifierPourPeriode("dDebContratTrav",debutPeriode,"dFinContratTrav",finPeriode);
		qualifiers.addObject(qualifier);
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("MangueContrat",qualifier,null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	//	Méthodes privées statiques
	private static NSArray rechercherContratsManguePourContrat(EOEditingContext editingContext,EOContrat contrat) {
		//	Vérifier si il existe un individu équivalent dans le SI Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été créé
		// On recherche les contrats valides
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu)contrat.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray args = new NSMutableArray(individuGrhum);
			// Vérifier que la date de début de contrat est la même : pour éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(contrat.dDebContratTrav()));
			args.addObject(DateCtrl.jourSuivant(contrat.dDebContratTrav()));
			args.addObject(contrat.cTypeContratTrav());
			String qualifier = "individu = %@ AND dDebContratTrav > %@ AND dDebContratTrav < %@ AND cTypeContratTrav = %@ AND (temAnnulation = NIL or temAnnulation ='N')";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification ("MangueContrat", myQualifier,null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else {
			return null;
		}
	}
}