package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCongeAl6Corresp;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;

import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongeAl6 extends _EOMangueCongeAl6 {
	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongeAl6) value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongeAl6Corresp congeCorresp = EOCongeAl6Corresp.fetchCongeAl6Corresp(editingContext(), EOCongeAl6Corresp.CONGE_AL6_KEY, congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.mangueCongeAl6();
	}

	@Override
	public NSTimestamp dateCommission() {
		return dComMed();
	}

	@Override
	public String typeEvenement() {
		return "CAL6";
	}
}
