package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCongeAl5Corresp;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;

import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongeAl5 extends _EOMangueCongeAl5 {
	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongeAl5) value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongeAl5Corresp congeCorresp = EOCongeAl5Corresp.fetchCongeAl5Corresp(editingContext(), EOCongeAl5Corresp.CONGE_AL5_KEY, congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.mangueCongeAl5();
	}

	@Override
	public NSTimestamp dateCommission() {
		return dComMed();
	}

	@Override
	public String typeEvenement() {
		return "CAL5";
	}
}
