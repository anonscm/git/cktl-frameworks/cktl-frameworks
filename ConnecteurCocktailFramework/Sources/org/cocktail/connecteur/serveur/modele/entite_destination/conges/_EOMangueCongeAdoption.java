// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongeAdoption.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongeAdoption extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCongeAdoption";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CONGE_ANNUL_ORDRE_KEY = "congeAnnulOrdre";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_ARRIVEE_FOYER_KEY = "dArriveeFoyer";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEMANDE_KEY = "dDemande";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NB_ENFANT_A_CHARGE_KEY = "nbEnfantACharge";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NO_ENFANT_KEY = "noEnfant";
	public static final String TEM_ADOPTION_MULTIPLE_KEY = "temAdoptionMultiple";
	public static final String TEM_CG_PARTAGE_KEY = "temCgPartage";
	public static final String TEM_CG_SANS_TRAIT_KEY = "temCgSansTrait";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ENFANT_KEY = "enfant";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCongeAdoption.class);

  public EOMangueCongeAdoption localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongeAdoption localInstance = (EOMangueCongeAdoption)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Integer congeAnnulOrdre() {
    return (Integer) storedValueForKey("congeAnnulOrdre");
  }

  public void setCongeAnnulOrdre(Integer value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating congeAnnulOrdre from " + congeAnnulOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "congeAnnulOrdre");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dArriveeFoyer() {
    return (NSTimestamp) storedValueForKey("dArriveeFoyer");
  }

  public void setDArriveeFoyer(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dArriveeFoyer from " + dArriveeFoyer() + " to " + value);
    }
    takeStoredValueForKey(value, "dArriveeFoyer");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDemande() {
    return (NSTimestamp) storedValueForKey("dDemande");
  }

  public void setDDemande(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dDemande from " + dDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "dDemande");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer nbEnfantACharge() {
    return (Integer) storedValueForKey("nbEnfantACharge");
  }

  public void setNbEnfantACharge(Integer value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating nbEnfantACharge from " + nbEnfantACharge() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfantACharge");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public Integer noEnfant() {
    return (Integer) storedValueForKey("noEnfant");
  }

  public void setNoEnfant(Integer value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating noEnfant from " + noEnfant() + " to " + value);
    }
    takeStoredValueForKey(value, "noEnfant");
  }

  public String temAdoptionMultiple() {
    return (String) storedValueForKey("temAdoptionMultiple");
  }

  public void setTemAdoptionMultiple(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating temAdoptionMultiple from " + temAdoptionMultiple() + " to " + value);
    }
    takeStoredValueForKey(value, "temAdoptionMultiple");
  }

  public String temCgPartage() {
    return (String) storedValueForKey("temCgPartage");
  }

  public void setTemCgPartage(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating temCgPartage from " + temCgPartage() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgPartage");
  }

  public String temCgSansTrait() {
    return (String) storedValueForKey("temCgSansTrait");
  }

  public void setTemCgSansTrait(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating temCgSansTrait from " + temCgSansTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgSansTrait");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
    	_EOMangueCongeAdoption.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant enfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("enfant");
  }

  public void setEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
      _EOMangueCongeAdoption.LOG.debug("updating enfant from " + enfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "enfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "enfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
      _EOMangueCongeAdoption.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption value) {
    if (_EOMangueCongeAdoption.LOG.isDebugEnabled()) {
      _EOMangueCongeAdoption.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAdoption oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCongeAdoption createMangueCongeAdoption(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temAdoptionMultiple
, String temCgPartage
, String temCgSansTrait
, String temConfirme
, String temGestEtab
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCongeAdoption eo = (EOMangueCongeAdoption) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongeAdoption.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemAdoptionMultiple(temAdoptionMultiple);
		eo.setTemCgPartage(temCgPartage);
		eo.setTemCgSansTrait(temCgSansTrait);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCongeAdoption> fetchAllMangueCongeAdoptions(EOEditingContext editingContext) {
    return _EOMangueCongeAdoption.fetchAllMangueCongeAdoptions(editingContext, null);
  }

  public static NSArray<EOMangueCongeAdoption> fetchAllMangueCongeAdoptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongeAdoption.fetchMangueCongeAdoptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongeAdoption> fetchMangueCongeAdoptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongeAdoption.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongeAdoption> eoObjects = (NSArray<EOMangueCongeAdoption>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongeAdoption fetchMangueCongeAdoption(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeAdoption.fetchMangueCongeAdoption(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeAdoption fetchMangueCongeAdoption(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongeAdoption> eoObjects = _EOMangueCongeAdoption.fetchMangueCongeAdoptions(editingContext, qualifier, null);
    EOMangueCongeAdoption eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongeAdoption)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongeAdoption that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeAdoption fetchRequiredMangueCongeAdoption(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeAdoption.fetchRequiredMangueCongeAdoption(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeAdoption fetchRequiredMangueCongeAdoption(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongeAdoption eoObject = _EOMangueCongeAdoption.fetchMangueCongeAdoption(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongeAdoption that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeAdoption localInstanceIn(EOEditingContext editingContext, EOMangueCongeAdoption eo) {
    EOMangueCongeAdoption localInstance = (eo == null) ? null : (EOMangueCongeAdoption)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
