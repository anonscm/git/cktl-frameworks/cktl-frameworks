package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCrctCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCrctDetail;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueCrctDetail extends _EOMangueCrctDetail {
	private boolean temValide = true;

	private static Logger log = Logger.getLogger(EOMangueCrctDetail.class);

	// Méthodes ajoutées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOCrctDetail detail = (EOCrctDetail) recordImport;
		// A ce stade l'individu et le crct doivent être insérés dans le SI
		// Destinataire
		EOMangueCrct crctMangue = EOCrctCorresp.crctMangue(editingContext(), detail.crct().crctSource(), detail.crct().individu().idSource());
		if (crctMangue == null) {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant idSource : " + detail.crct().individu().idSource()
					+ ", CRCT Source : " + detail.crct().eimpSource());
		}
		addObjectToBothSidesOfRelationshipWithKey(crctMangue, MANGUE_CRCT_KEY);
	}

	/** retourne false si l'objet d'import n'est pas */
	public boolean estValide() {
		return temValide;
	}

	/** Modification de l'attribut de validite dans la RepartPersonAdresse */
	public void setEstValide(boolean aBool) {

		temValide = aBool;
	}

	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MANGUE_CRCT_KEY+"."+EOMangueCrct.INDIVIDU_KEY + " = %@", new NSArray(individu)));
		}
		if (debutPeriode != null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

}
