package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividuFonctionStructurelle;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOGrhumIndividuFonctionStructurelle extends _EOGrhumIndividuFonctionStructurelle {
	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		EOIndividuFonctionStructurelle indFonctionStrucImport =(EOIndividuFonctionStructurelle) recordImport;
		
		EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), indFonctionStrucImport.strSource());
		if (structure != null) { 
			setStructureRelationship(structure);
		} else {
			throw new Exception("Pas de structure dans le SI correspondant a la structure " + indFonctionStrucImport.strSource());
		}
	}
}
