package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOVacatairesCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOVacataireAffectation;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueVacatairesAffectation extends _EOMangueVacatairesAffectation {
	@Override
	public void setEstValide(boolean aBool) {
		// Surcharge la méthode setEstValide et ne fait rien car l'objet Mangue n'a pas TemValide
	}
	
	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOVacataireAffectation vafImport=(EOVacataireAffectation)recordImport;
		
		EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), vafImport.strSource());
		setStructureRelationship(structure);

		EOMangueVacataires vacataires = EOVacatairesCorresp.mangueVacataires(editingContext(), vafImport.vacSource());
		setVacatairesRelationship(vacataires);
	}
}
