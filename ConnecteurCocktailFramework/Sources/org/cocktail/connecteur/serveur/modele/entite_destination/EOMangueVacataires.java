package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.correspondance.EOAdresseCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOVacataires;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOMangueVacataires extends _EOMangueVacataires {
	private static final long serialVersionUID = 6523888400045227007L;

	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOVacataires vacataireImport = (EOVacataires) recordImport;

		EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
		setPersIdCreation(utilisateur.persId());
		setPersIdModification(utilisateur.persId());

		// A ce stade, l'individu grhum est dans le SI Destinataire
		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), vacataireImport.individu().idSource());
		if (individuGrhum != null) {
			addObjectToBothSidesOfRelationshipWithKey(individuGrhum, INDIVIDU_KEY);
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + vacataireImport.individu().idSource());
		}

		if (vacataireImport.strSource() != null) {
			EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), vacataireImport.strSource());
			if (structure != null) { 
				addObjectToBothSidesOfRelationshipWithKey(structure, STRUCTURE_KEY);
			} else {
				throw new Exception("Pas de structure dans le SI correspondant a la structure " + vacataireImport.strSource());
			}
		}

		if (vacataireImport.adrSource() != null) {
			EOGrhumAdresse adresse = EOAdresseCorresp.adresseGrhum(editingContext(), vacataireImport.adrSource());
			if (adresse != null) {
				addObjectToBothSidesOfRelationshipWithKey(adresse, TO_ADRESSE_KEY);
			} else {
				throw new Exception("Pas d'adresse dans le SI correspondant a l'adresse " + vacataireImport.adrSource());
			}
		}
		
		setToCnuRelationship(vacataireImport.toCnu());
	}

	// Méthodes privées statiques
	private static NSArray rechercherVacatairessManguePourVacataires(EOEditingContext editingContext, EOVacataires vacataires) {
		// Vérifier si il existe un individu équivalent dans le SI Destinataire.
		// On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été
		// créé
		// On recherche les contrats valides
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) vacataires.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			NSMutableArray args = new NSMutableArray(individuGrhum);
			// Vérifier que la date de début de contrat est la même : pour
			// éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent
			// et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(vacataires.dDebVacation()));
			args.addObject(DateCtrl.jourSuivant(vacataires.dDebVacation()));
			args.addObject(vacataires.cTypeContratTrav());
			String qualifier = "individu = %@ AND dDebVacation > %@ AND dDebVacation < %@ AND cTypeContratTrav = %@ AND temValide ='O'";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier, args);
			EOFetchSpecification myFetch = new EOFetchSpecification("MangueVacataires", myQualifier, null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else {
			return null;
		}
	}
}
