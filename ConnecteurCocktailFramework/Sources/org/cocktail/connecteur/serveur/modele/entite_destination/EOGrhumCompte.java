//EOCompte.java
//Created on Mon Sep 24 11:47:29 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import java.util.Random;

import org.apache.log4j.Logger;
import org.cocktail.common.utilities.StringCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.EOGrhumPrioriteEntite;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrhumParametres;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCompte;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class EOGrhumCompte extends _EOGrhumCompte implements InterfaceRecordAvecAutresAttributs,  InterfaceGestionIdUtilisateur {

	private static final String CPT_VALIDE_VALEUR_PAR_DEFAUT = "O";

	private static Logger log = Logger.getLogger(EOGrhumCompte.class);

	private static String vlanExterne;

	public EOGrhumCompte() {
		super();
	}    
	
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	/**
	 * 
	 * @param editingContext
	 * @param persId
	 * @param vlan
	 * @return
	 */
	public static NSArray<EOGrhumCompte> rechercherComptesPourIdEtVlan(EOEditingContext editingContext, Number persId, String vlan) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(persId)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_VLAN_KEY + "=%@", new NSArray(vlan)));
		
		return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param persId
	 * @return
	 */
	public static EOGrhumCompte rechercherComptePourId(EOEditingContext editingContext, Number persId) {
		NSArray<EOGrhumCompte> comptes = rechercherComptesPourIdEtVlan(editingContext, persId, null);
		switch(comptes.count()) {
		case 0 : return null;
		case 1 : return (EOGrhumCompte)comptes.objectAtIndex(0);
		default :
			for (EOGrhumCompte compte : comptes) {
				if (compte.cptVlan().equals("P")) {
					return compte;
				}
			}
			// sinon, on retourne null
			return null;
		}
	}

	/**
	 * 
	 */
	public void awakeFromFetch(EOEditingContext editingContext) {
		if (vlanExterne == null) {
			vlanExterne = EOGrhumParametres.parametrePourCle(editingContext, "GRHUM_VLAN_EXTERNE");
		}
	}

	/** Compare les donnees de compte
	@param source 
	@param destination
	 */
	public boolean aAttributsIdentiques(ObjetImport source,Entite entiteModele) {
		if (super.aAttributsIdentiques(source, entiteModele)) {
			EOCompte compte = (EOCompte)source;
			// Si l'objet source a des valeurs nulles, elles ne sont pas à prendre en compte
			if (compte.cptGid() != null) {
				return false;
			}
			if (compte.cptHome() != null) {
				return false;
			}
			if (compte.cptShell() != null) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	}

	/** 
	 * Cree le password si il est inexistant  
	 * 
	*/
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport recordImport) {
		EOCompte compte = (EOCompte)recordImport;
		// Vérifier si il y a des règles de priorité sur les attributs qui fait qu'on ne doit pas y toucher
		EOGrhumPrioriteEntite priorite = null;
		if (recordImport.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || recordImport.operation().equals(ObjetImport.OPERATION_UPDATE)) {
			priorite = prioriteEntite();
		}
		if ((priorite == null || priorite.attributPourLibelle(CPT_PASSWD_KEY,true) == null) && cptPasswd() == null) {
			if (recordImport.operation().equals(ObjetImport.OPERATION_CORRESPONDANCE) || recordImport.operation().equals(ObjetImport.OPERATION_UPDATE)) {
				LogManager.logDetail("Operation de correspondance ou d'update : creation d'un password pour le compte " + compte);
			}
			setCptPasswd(creerMotPasse());
		}

		return true;

	}
	
	/** Temporaire pour simuler la presence d'un temoin de validite */
	public boolean estValide() {
		return true;
	}
	/** Temporaire pour simuler la presence d'un temoin de validite */
	public void setEstValide(boolean aBool) {
	}

	/**
	 * 
	 */
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
				
		EOCompte compte = (EOCompte)recordImport;
		Integer persId = new Integer(0);

		if (compte.structure() != null) {	
			EOGrhumStructure structureGrhum = (EOGrhumStructure)compte.structure().objetDestinataireAvecOuSansCorrespondance(editingContext());
			if (structureGrhum != null) {
				persId = structureGrhum.persId();
			}
		}

		if (compte.individu() != null) {	
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)compte.individu().objetDestinataireAvecOuSansCorrespondance(editingContext());
			if (individuGrhum != null) {
				persId = individuGrhum.persId();
			}
		}
 		setPersId(persId);

		setVLanRelationship(compte.vLan());
		setTypeCryptageRelationship(compte.typeCryptage());
		

		// Recuperation du GID par defaut. 
		String defaultGid = EOGrhumParametres.parametrePourCle(editingContext(), "GRHUM_DEFAULT_GID");
		if (defaultGid != null)
			setCptGid(new Integer(defaultGid));
		else
			setCptGid(new Integer(1000));
		setCptUid(new Integer(0));
		setCptValide(CPT_VALIDE_VALEUR_PAR_DEFAUT);
	}
	

	/**
	 * 
	 * @return
	 */
	private String creerMotPasse() {
		String passwd = "";
		Random random = new Random();
		for (int i = 0; i < 8;i++) {
			int value = random.nextInt(36);	// En se limitant à 35, on a forcément des caractères alpha numériques de 0 à 'z'
			char ch = Character.forDigit(value, Character.MAX_RADIX);
			if (i % 3 == 0 && Character.isDigit(ch) == false) {	// Pour mélanger majuscule et minuscuule
				ch = Character.toUpperCase(ch);
			}
			passwd +=  Character.toString(ch);
		}
		return(passwd);
	}
	

	/** retourne le compte correspondant du SI Destinataire du meme individu ou structure,
	 * avec le meme vlan, le meme login, le meme password ou le meme mail et le meme domaine */ 
	public static EOGrhumCompte compteDestinationPourCompte(EOEditingContext editingContext,EOCompte compte) {
		NSArray<EOGrhumCompte> comptes = null;
		Number persId = null;
		if (compte.individu() != null) {	
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)compte.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				persId = individuGrhum.persId();
			}
		} else {
			// On vérifie si il existe une correspondance sinon (la structure n'a peut-être pas encore été créée)
			// on cherche directement dans la base destinataire
			EOGrhumStructure structureGrhum = (EOGrhumStructure)compte.structure().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (structureGrhum != null) {
				persId = structureGrhum.persId();
			}
		}
		if (persId == null) {
			// individu pas encore créé
			return null;
		}
		comptes = EOGrhumCompte.rechercherComptesPourIdEtVlan(editingContext, persId, compte.cptVlan());
		switch (comptes.count()) {
		case 0 : return null;
		default : 
			// Si c'est un vlan interne, vérifier si il existe un compte avec le même GUid
			// Vérifier si il en existe un avec le même login
			EOGrhumCompte compteGrhum = null;
			if (compte.cptVlan().equals(vlanExterne) == false && compte.cptUid() != null) {

				for (EOGrhumCompte compteG : comptes) {
					if (compteG.cptUid() != null && compteG.cptUid().intValue() == compte.cptUid().intValue()) {
						// Il ne peut y avoir qu'un seul compte pour le vlan avec un même uidf
						compteGrhum = compteG;
						break;
					}
				}
			} else if (compte.cptLogin() != null) {
				for (EOGrhumCompte compteG : comptes) {
					if (compteG.cptLogin() != null && compteG.cptLogin().equals(compte.cptLogin().toLowerCase())) {
						// Il ne peut y avoir qu'un seul compte pour le vlan avec un même login
						compteGrhum = compteG;
						break;
					}
				}
			}
			// On n'a pas trouvé avec le login, on recommence avec le même email/domaine
			if (compteGrhum == null) {
				for (EOGrhumCompte compteG : comptes) {
					if ((compteG.cptEmail() == null && compte.cptEmail() == null) ||
							compteG.cptEmail() != null && compte.cptEmail() != null && compteG.cptEmail().equals(compte.cptEmail())) {
						if ((compteG.cptDomaine() == null && compte.cptEmail() == null) ||
								compteG.cptDomaine() != null && compte.cptDomaine() != null && compteG.cptDomaine().equals(compte.cptDomaine())) {
							// Il ne peut y avoir pour un vlan donné qu'un seul compte avec le même email/domaine
							compteGrhum = compteG;
							break;
						}
					}
				}

			}
			return compteGrhum;	
		}
	}

	// AJOUT POUR LE CRYPTAGE DES MOTS DE PASSE
	/**
	 * @param ec
	 * @param typeCryptage
	 * @param passwordClair
	 * @return la chaine cryptee
	 */
	private String cryptePassword(EOEditingContext ec, EOTypeCryptage typeCryptage, String passwordClair) throws Exception {
		String passwordCrypte = null;

		String javaMethode = typeCryptage.tcryJavaMethode();

		if (StringCtrl.chaineVide(javaMethode)) {
			throw new Exception("Classe Java non defini pour le type de cryptage " + typeCryptage.tcryLibelle());
		}
		if (log.isDebugEnabled()) {
			log.debug("Cryptage du mot de passe avec la methode " + typeCryptage.tcryLibelle() + "/ " + javaMethode);
		}

		try {
			passwordCrypte = CryptoCtrl.cryptPass(javaMethode, passwordClair);
		} catch (Exception e) {
			log.error("erreur lors de l'encryptage", e);
			throw new Exception("Erreur lors de l'encryptage du password", e);
		}

		return passwordCrypte;
	}
}
