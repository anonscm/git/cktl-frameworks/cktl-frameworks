// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongeAl3.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongeAl3 extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCongeAl3";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_KEY = "dComMed";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_EN_CAUSE_KEY = "temEnCause";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PROLONG_KEY = "temProlong";
	public static final String TEM_REQUALIF_KEY = "temRequalif";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCongeAl3.class);

  public EOMangueCongeAl3 localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongeAl3 localInstance = (EOMangueCongeAl3)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMed() {
    return (NSTimestamp) storedValueForKey("dComMed");
  }

  public void setDComMed(NSTimestamp value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating dComMed from " + dComMed() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMed");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temEnCause() {
    return (String) storedValueForKey("temEnCause");
  }

  public void setTemEnCause(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating temEnCause from " + temEnCause() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnCause");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temProlong() {
    return (String) storedValueForKey("temProlong");
  }

  public void setTemProlong(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating temProlong from " + temProlong() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlong");
  }

  public String temRequalif() {
    return (String) storedValueForKey("temRequalif");
  }

  public void setTemRequalif(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating temRequalif from " + temRequalif() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalif");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
    	_EOMangueCongeAl3.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
      _EOMangueCongeAl3.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3 toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3 value) {
    if (_EOMangueCongeAl3.LOG.isDebugEnabled()) {
      _EOMangueCongeAl3.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeAl3 oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCongeAl3 createMangueCongeAl3(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temConfirme
, String temEnCause
, String temGestEtab
, String temProlong
, String temRequalif
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCongeAl3 eo = (EOMangueCongeAl3) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongeAl3.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemConfirme(temConfirme);
		eo.setTemEnCause(temEnCause);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemProlong(temProlong);
		eo.setTemRequalif(temRequalif);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCongeAl3> fetchAllMangueCongeAl3s(EOEditingContext editingContext) {
    return _EOMangueCongeAl3.fetchAllMangueCongeAl3s(editingContext, null);
  }

  public static NSArray<EOMangueCongeAl3> fetchAllMangueCongeAl3s(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongeAl3.fetchMangueCongeAl3s(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongeAl3> fetchMangueCongeAl3s(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongeAl3.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongeAl3> eoObjects = (NSArray<EOMangueCongeAl3>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongeAl3 fetchMangueCongeAl3(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeAl3.fetchMangueCongeAl3(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeAl3 fetchMangueCongeAl3(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongeAl3> eoObjects = _EOMangueCongeAl3.fetchMangueCongeAl3s(editingContext, qualifier, null);
    EOMangueCongeAl3 eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongeAl3)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongeAl3 that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeAl3 fetchRequiredMangueCongeAl3(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeAl3.fetchRequiredMangueCongeAl3(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeAl3 fetchRequiredMangueCongeAl3(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongeAl3 eoObject = _EOMangueCongeAl3.fetchMangueCongeAl3(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongeAl3 that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeAl3 localInstanceIn(EOEditingContext editingContext, EOMangueCongeAl3 eo) {
    EOMangueCongeAl3 localInstance = (eo == null) ? null : (EOMangueCongeAl3)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
