// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongePaternite.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongePaternite extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCongePaternite";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEMANDE_KEY = "dDemande";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAIS_PREV_KEY = "dNaisPrev";
	public static final String NB_ENFANTS_DECL_KEY = "nbEnfantsDecl";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CG_SANS_TRAIT_KEY = "temCgSansTrait";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_GROSSESSE_MULTIPLE_KEY = "temGrossesseMultiple";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCongePaternite.class);

  public EOMangueCongePaternite localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongePaternite localInstance = (EOMangueCongePaternite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDemande() {
    return (NSTimestamp) storedValueForKey("dDemande");
  }

  public void setDDemande(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dDemande from " + dDemande() + " to " + value);
    }
    takeStoredValueForKey(value, "dDemande");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dNaisPrev() {
    return (NSTimestamp) storedValueForKey("dNaisPrev");
  }

  public void setDNaisPrev(NSTimestamp value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating dNaisPrev from " + dNaisPrev() + " to " + value);
    }
    takeStoredValueForKey(value, "dNaisPrev");
  }

  public Integer nbEnfantsDecl() {
    return (Integer) storedValueForKey("nbEnfantsDecl");
  }

  public void setNbEnfantsDecl(Integer value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating nbEnfantsDecl from " + nbEnfantsDecl() + " to " + value);
    }
    takeStoredValueForKey(value, "nbEnfantsDecl");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temCgSansTrait() {
    return (String) storedValueForKey("temCgSansTrait");
  }

  public void setTemCgSansTrait(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating temCgSansTrait from " + temCgSansTrait() + " to " + value);
    }
    takeStoredValueForKey(value, "temCgSansTrait");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temGrossesseMultiple() {
    return (String) storedValueForKey("temGrossesseMultiple");
  }

  public void setTemGrossesseMultiple(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating temGrossesseMultiple from " + temGrossesseMultiple() + " to " + value);
    }
    takeStoredValueForKey(value, "temGrossesseMultiple");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
    	_EOMangueCongePaternite.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
      _EOMangueCongePaternite.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite value) {
    if (_EOMangueCongePaternite.LOG.isDebugEnabled()) {
      _EOMangueCongePaternite.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongePaternite oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCongePaternite createMangueCongePaternite(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temCgSansTrait
, String temConfirme
, String temGestEtab
, String temValide
) {
    EOMangueCongePaternite eo = (EOMangueCongePaternite) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongePaternite.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemCgSansTrait(temCgSansTrait);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueCongePaternite> fetchAllMangueCongePaternites(EOEditingContext editingContext) {
    return _EOMangueCongePaternite.fetchAllMangueCongePaternites(editingContext, null);
  }

  public static NSArray<EOMangueCongePaternite> fetchAllMangueCongePaternites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongePaternite.fetchMangueCongePaternites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongePaternite> fetchMangueCongePaternites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongePaternite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongePaternite> eoObjects = (NSArray<EOMangueCongePaternite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongePaternite fetchMangueCongePaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongePaternite.fetchMangueCongePaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongePaternite fetchMangueCongePaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongePaternite> eoObjects = _EOMangueCongePaternite.fetchMangueCongePaternites(editingContext, qualifier, null);
    EOMangueCongePaternite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongePaternite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongePaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongePaternite fetchRequiredMangueCongePaternite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongePaternite.fetchRequiredMangueCongePaternite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongePaternite fetchRequiredMangueCongePaternite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongePaternite eoObject = _EOMangueCongePaternite.fetchMangueCongePaternite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongePaternite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongePaternite localInstanceIn(EOEditingContext editingContext, EOMangueCongePaternite eo) {
    EOMangueCongePaternite localInstance = (eo == null) ? null : (EOMangueCongePaternite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
