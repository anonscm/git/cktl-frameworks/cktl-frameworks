// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueEmploiCategorie.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueEmploiCategorie extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueEmploiCategorie";

	// Attributes
	public static final String C_CATEGORIE_EMPLOI_KEY = "cCategorieEmploi";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEBUT_KEY = "dDebut";
	public static final String D_FIN_KEY = "dFin";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_EMPLOI_KEY = "idEmploi";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	// Relationships
	public static final String TO_CATEGORIE_EMPLOI_KEY = "toCategorieEmploi";
	public static final String TO_MANGUE_EMPLOI_KEY = "toMangueEmploi";

  private static Logger LOG = Logger.getLogger(_EOMangueEmploiCategorie.class);

  public EOMangueEmploiCategorie localInstanceIn(EOEditingContext editingContext) {
    EOMangueEmploiCategorie localInstance = (EOMangueEmploiCategorie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorieEmploi() {
    return (String) storedValueForKey("cCategorieEmploi");
  }

  public void setCCategorieEmploi(String value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating cCategorieEmploi from " + cCategorieEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorieEmploi");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebut() {
    return (NSTimestamp) storedValueForKey("dDebut");
  }

  public void setDDebut(NSTimestamp value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating dDebut from " + dDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebut");
  }

  public NSTimestamp dFin() {
    return (NSTimestamp) storedValueForKey("dFin");
  }

  public void setDFin(NSTimestamp value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating dFin from " + dFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dFin");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idEmploi() {
    return (Integer) storedValueForKey("idEmploi");
  }

  public void setIdEmploi(Integer value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating idEmploi from " + idEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "idEmploi");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
    	_EOMangueEmploiCategorie.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCategorieEmploi toCategorieEmploi() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCategorieEmploi)storedValueForKey("toCategorieEmploi");
  }

  public void setToCategorieEmploiRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCategorieEmploi value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
      _EOMangueEmploiCategorie.LOG.debug("updating toCategorieEmploi from " + toCategorieEmploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCategorieEmploi oldValue = toCategorieEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCategorieEmploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCategorieEmploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toMangueEmploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi)storedValueForKey("toMangueEmploi");
  }

  public void setToMangueEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi value) {
    if (_EOMangueEmploiCategorie.LOG.isDebugEnabled()) {
      _EOMangueEmploiCategorie.LOG.debug("updating toMangueEmploi from " + toMangueEmploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi oldValue = toMangueEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueEmploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueEmploi");
    }
  }
  

  public static EOMangueEmploiCategorie createMangueEmploiCategorie(EOEditingContext editingContext, String cCategorieEmploi
, NSTimestamp dCreation
, NSTimestamp dDebut
, NSTimestamp dModification
, Integer idEmploi
, org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCategorieEmploi toCategorieEmploi, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toMangueEmploi) {
    EOMangueEmploiCategorie eo = (EOMangueEmploiCategorie) EOUtilities.createAndInsertInstance(editingContext, _EOMangueEmploiCategorie.ENTITY_NAME);    
		eo.setCCategorieEmploi(cCategorieEmploi);
		eo.setDCreation(dCreation);
		eo.setDDebut(dDebut);
		eo.setDModification(dModification);
		eo.setIdEmploi(idEmploi);
    eo.setToCategorieEmploiRelationship(toCategorieEmploi);
    eo.setToMangueEmploiRelationship(toMangueEmploi);
    return eo;
  }

  public static NSArray<EOMangueEmploiCategorie> fetchAllMangueEmploiCategories(EOEditingContext editingContext) {
    return _EOMangueEmploiCategorie.fetchAllMangueEmploiCategories(editingContext, null);
  }

  public static NSArray<EOMangueEmploiCategorie> fetchAllMangueEmploiCategories(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueEmploiCategorie.fetchMangueEmploiCategories(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueEmploiCategorie> fetchMangueEmploiCategories(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueEmploiCategorie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueEmploiCategorie> eoObjects = (NSArray<EOMangueEmploiCategorie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueEmploiCategorie fetchMangueEmploiCategorie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiCategorie.fetchMangueEmploiCategorie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiCategorie fetchMangueEmploiCategorie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueEmploiCategorie> eoObjects = _EOMangueEmploiCategorie.fetchMangueEmploiCategories(editingContext, qualifier, null);
    EOMangueEmploiCategorie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueEmploiCategorie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueEmploiCategorie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiCategorie fetchRequiredMangueEmploiCategorie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiCategorie.fetchRequiredMangueEmploiCategorie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiCategorie fetchRequiredMangueEmploiCategorie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueEmploiCategorie eoObject = _EOMangueEmploiCategorie.fetchMangueEmploiCategorie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueEmploiCategorie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiCategorie localInstanceIn(EOEditingContext editingContext, EOMangueEmploiCategorie eo) {
    EOMangueEmploiCategorie localInstance = (eo == null) ? null : (EOMangueEmploiCategorie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
