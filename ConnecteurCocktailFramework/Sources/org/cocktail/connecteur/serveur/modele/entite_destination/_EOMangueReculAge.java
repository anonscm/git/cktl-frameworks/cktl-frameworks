// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueReculAge.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueReculAge extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueReculAge";

	// Attributes
	public static final String C_MOTIF_KEY = "cMotif";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FIN_EXECUTION_KEY = "dFinExecution";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_ENFANT_KEY = "toEnfant";
	public static final String TO_ENFANT2_KEY = "toEnfant2";
	public static final String TO_ENFANT3_KEY = "toEnfant3";

  private static Logger LOG = Logger.getLogger(_EOMangueReculAge.class);

  public EOMangueReculAge localInstanceIn(EOEditingContext editingContext) {
    EOMangueReculAge localInstance = (EOMangueReculAge)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cMotif() {
    return (String) storedValueForKey("cMotif");
  }

  public void setCMotif(String value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating cMotif from " + cMotif() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotif");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFinExecution() {
    return (NSTimestamp) storedValueForKey("dFinExecution");
  }

  public void setDFinExecution(NSTimestamp value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating dFinExecution from " + dFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinExecution");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
    	_EOMangueReculAge.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
      _EOMangueReculAge.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant toEnfant() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("toEnfant");
  }

  public void setToEnfantRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
      _EOMangueReculAge.LOG.debug("updating toEnfant from " + toEnfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = toEnfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant toEnfant2() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("toEnfant2");
  }

  public void setToEnfant2Relationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
      _EOMangueReculAge.LOG.debug("updating toEnfant2 from " + toEnfant2() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = toEnfant2();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant2");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant2");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant toEnfant3() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant)storedValueForKey("toEnfant3");
  }

  public void setToEnfant3Relationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant value) {
    if (_EOMangueReculAge.LOG.isDebugEnabled()) {
      _EOMangueReculAge.LOG.debug("updating toEnfant3 from " + toEnfant3() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumEnfant oldValue = toEnfant3();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant3");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant3");
    }
  }
  

  public static EOMangueReculAge createMangueReculAge(EOEditingContext editingContext, String cMotif
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
) {
    EOMangueReculAge eo = (EOMangueReculAge) EOUtilities.createAndInsertInstance(editingContext, _EOMangueReculAge.ENTITY_NAME);    
		eo.setCMotif(cMotif);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueReculAge> fetchAllMangueReculAges(EOEditingContext editingContext) {
    return _EOMangueReculAge.fetchAllMangueReculAges(editingContext, null);
  }

  public static NSArray<EOMangueReculAge> fetchAllMangueReculAges(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueReculAge.fetchMangueReculAges(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueReculAge> fetchMangueReculAges(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueReculAge.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueReculAge> eoObjects = (NSArray<EOMangueReculAge>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueReculAge fetchMangueReculAge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueReculAge.fetchMangueReculAge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueReculAge fetchMangueReculAge(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueReculAge> eoObjects = _EOMangueReculAge.fetchMangueReculAges(editingContext, qualifier, null);
    EOMangueReculAge eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueReculAge)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueReculAge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueReculAge fetchRequiredMangueReculAge(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueReculAge.fetchRequiredMangueReculAge(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueReculAge fetchRequiredMangueReculAge(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueReculAge eoObject = _EOMangueReculAge.fetchMangueReculAge(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueReculAge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueReculAge localInstanceIn(EOEditingContext editingContext, EOMangueReculAge eo) {
    EOMangueReculAge localInstance = (eo == null) ? null : (EOMangueReculAge)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
