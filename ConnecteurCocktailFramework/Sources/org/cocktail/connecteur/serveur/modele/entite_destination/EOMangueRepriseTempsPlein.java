package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EORepriseTempsPleinCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOTempsPartielCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EORepriseTempsPlein;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueRepriseTempsPlein extends _EOMangueRepriseTempsPlein {
	private static Logger log = Logger.getLogger(EOMangueRepriseTempsPlein.class);

	// Méthodes ajoutées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		// super.initialiserObjet(recordImport);
		EORepriseTempsPlein repriseTpsPlein = (EORepriseTempsPlein) recordImport;

		if (repriseTpsPlein.individu() == null)
			throw new Exception("Pas d'individu dans objetImport. IdSource = " + repriseTpsPlein.idSource());

		EOGrhumIndividu individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), repriseTpsPlein.individu().idSource());
		if (individuGrhum == null)
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + repriseTpsPlein.individu().idSource());
		setIndividuRelationship(individuGrhum);

		EOMangueTempsPartiel tpsMangue = EOTempsPartielCorresp.tpsMangue(editingContext(), repriseTpsPlein.tempsPartiel().tpSource(), repriseTpsPlein
				.tempsPartiel().individu().idSource());
		if (tpsMangue == null) {
			throw new Exception("Pas de temps partiel dans le SI correspondant a l'identifiant " + repriseTpsPlein.tempsPartiel().tpSource());
		}
		setTempsPartielRelationship(tpsMangue);

		if (repriseTpsPlein.arreteAnnulation() != null) {
			EOMangueRepriseTempsPlein rtpAnnulation = EORepriseTempsPleinCorresp.rtpMangue(editingContext(),repriseTpsPlein.arreteAnnulation());
			if (rtpAnnulation == null)
				throw new Exception("Pas de repriseTempsPlein dans le SI correspondant a l'identifiant rtpSource : " + repriseTpsPlein.arreteAnnulation().rtpSource());
			setArreteAnnulationRelationship(rtpAnnulation);
		}
	}
}
