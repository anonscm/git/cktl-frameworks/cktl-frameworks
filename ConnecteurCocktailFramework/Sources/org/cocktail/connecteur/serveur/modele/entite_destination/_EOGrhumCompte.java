// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumCompte.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumCompte extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumCompte";

	// Attributes
	public static final String CPT_CHARTE_KEY = "cptCharte";
	public static final String CPT_COMMENTAIRE_KEY = "cptCommentaire";
	public static final String CPT_CONNEXION_KEY = "cptConnexion";
	public static final String CPT_CRYPTE_KEY = "cptCrypte";
	public static final String CPT_DATE_CHARTE_KEY = "cptDateCharte";
	public static final String CPT_DEBUT_VALIDE_KEY = "cptDebutValide";
	public static final String CPT_DOMAINE_KEY = "cptDomaine";
	public static final String CPT_EMAIL_KEY = "cptEmail";
	public static final String CPT_FIN_VALIDE_KEY = "cptFinValide";
	public static final String CPT_GID_KEY = "cptGid";
	public static final String CPT_HOME_KEY = "cptHome";
	public static final String CPT_LISTE_ROUGE_KEY = "cptListeRouge";
	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String CPT_PASSWD_KEY = "cptPasswd";
	public static final String CPT_PRINCIPAL_KEY = "cptPrincipal";
	public static final String CPT_SHELL_KEY = "cptShell";
	public static final String CPT_UID_KEY = "cptUid";
	public static final String CPT_VALIDE_KEY = "cptValide";
	public static final String CPT_VLAN_KEY = "cptVlan";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	// Relationships
	public static final String TYPE_CRYPTAGE_KEY = "typeCryptage";
	public static final String V_LAN_KEY = "vLan";

  private static Logger LOG = Logger.getLogger(_EOGrhumCompte.class);

  public EOGrhumCompte localInstanceIn(EOEditingContext editingContext) {
    EOGrhumCompte localInstance = (EOGrhumCompte)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cptCharte() {
    return (String) storedValueForKey("cptCharte");
  }

  public void setCptCharte(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptCharte from " + cptCharte() + " to " + value);
    }
    takeStoredValueForKey(value, "cptCharte");
  }

  public String cptCommentaire() {
    return (String) storedValueForKey("cptCommentaire");
  }

  public void setCptCommentaire(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptCommentaire from " + cptCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "cptCommentaire");
  }

  public String cptConnexion() {
    return (String) storedValueForKey("cptConnexion");
  }

  public void setCptConnexion(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptConnexion from " + cptConnexion() + " to " + value);
    }
    takeStoredValueForKey(value, "cptConnexion");
  }

  public String cptCrypte() {
    return (String) storedValueForKey("cptCrypte");
  }

  public void setCptCrypte(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptCrypte from " + cptCrypte() + " to " + value);
    }
    takeStoredValueForKey(value, "cptCrypte");
  }

  public NSTimestamp cptDateCharte() {
    return (NSTimestamp) storedValueForKey("cptDateCharte");
  }

  public void setCptDateCharte(NSTimestamp value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptDateCharte from " + cptDateCharte() + " to " + value);
    }
    takeStoredValueForKey(value, "cptDateCharte");
  }

  public NSTimestamp cptDebutValide() {
    return (NSTimestamp) storedValueForKey("cptDebutValide");
  }

  public void setCptDebutValide(NSTimestamp value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptDebutValide from " + cptDebutValide() + " to " + value);
    }
    takeStoredValueForKey(value, "cptDebutValide");
  }

  public String cptDomaine() {
    return (String) storedValueForKey("cptDomaine");
  }

  public void setCptDomaine(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptDomaine from " + cptDomaine() + " to " + value);
    }
    takeStoredValueForKey(value, "cptDomaine");
  }

  public String cptEmail() {
    return (String) storedValueForKey("cptEmail");
  }

  public void setCptEmail(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptEmail from " + cptEmail() + " to " + value);
    }
    takeStoredValueForKey(value, "cptEmail");
  }

  public NSTimestamp cptFinValide() {
    return (NSTimestamp) storedValueForKey("cptFinValide");
  }

  public void setCptFinValide(NSTimestamp value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptFinValide from " + cptFinValide() + " to " + value);
    }
    takeStoredValueForKey(value, "cptFinValide");
  }

  public Integer cptGid() {
    return (Integer) storedValueForKey("cptGid");
  }

  public void setCptGid(Integer value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptGid from " + cptGid() + " to " + value);
    }
    takeStoredValueForKey(value, "cptGid");
  }

  public String cptHome() {
    return (String) storedValueForKey("cptHome");
  }

  public void setCptHome(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptHome from " + cptHome() + " to " + value);
    }
    takeStoredValueForKey(value, "cptHome");
  }

  public String cptListeRouge() {
    return (String) storedValueForKey("cptListeRouge");
  }

  public void setCptListeRouge(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptListeRouge from " + cptListeRouge() + " to " + value);
    }
    takeStoredValueForKey(value, "cptListeRouge");
  }

  public String cptLogin() {
    return (String) storedValueForKey("cptLogin");
  }

  public void setCptLogin(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptLogin from " + cptLogin() + " to " + value);
    }
    takeStoredValueForKey(value, "cptLogin");
  }

  public String cptPasswd() {
    return (String) storedValueForKey("cptPasswd");
  }

  public void setCptPasswd(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptPasswd from " + cptPasswd() + " to " + value);
    }
    takeStoredValueForKey(value, "cptPasswd");
  }

  public String cptPrincipal() {
    return (String) storedValueForKey("cptPrincipal");
  }

  public void setCptPrincipal(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptPrincipal from " + cptPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "cptPrincipal");
  }

  public String cptShell() {
    return (String) storedValueForKey("cptShell");
  }

  public void setCptShell(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptShell from " + cptShell() + " to " + value);
    }
    takeStoredValueForKey(value, "cptShell");
  }

  public Integer cptUid() {
    return (Integer) storedValueForKey("cptUid");
  }

  public void setCptUid(Integer value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptUid from " + cptUid() + " to " + value);
    }
    takeStoredValueForKey(value, "cptUid");
  }

  public String cptValide() {
    return (String) storedValueForKey("cptValide");
  }

  public void setCptValide(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptValide from " + cptValide() + " to " + value);
    }
    takeStoredValueForKey(value, "cptValide");
  }

  public String cptVlan() {
    return (String) storedValueForKey("cptVlan");
  }

  public void setCptVlan(String value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating cptVlan from " + cptVlan() + " to " + value);
    }
    takeStoredValueForKey(value, "cptVlan");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
    	_EOGrhumCompte.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage typeCryptage() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage)storedValueForKey("typeCryptage");
  }

  public void setTypeCryptageRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
      _EOGrhumCompte.LOG.debug("updating typeCryptage from " + typeCryptage() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOTypeCryptage oldValue = typeCryptage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeCryptage");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeCryptage");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans vLan() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans)storedValueForKey("vLan");
  }

  public void setVLanRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans value) {
    if (_EOGrhumCompte.LOG.isDebugEnabled()) {
      _EOGrhumCompte.LOG.debug("updating vLan from " + vLan() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOVlans oldValue = vLan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "vLan");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "vLan");
    }
  }
  

  public static EOGrhumCompte createGrhumCompte(EOEditingContext editingContext, String cptCharte
, String cptConnexion
, String cptCrypte
, Integer cptGid
, Integer cptUid
, String cptValide
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdCreation
, Integer persIdModification
) {
    EOGrhumCompte eo = (EOGrhumCompte) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumCompte.ENTITY_NAME);    
		eo.setCptCharte(cptCharte);
		eo.setCptConnexion(cptConnexion);
		eo.setCptCrypte(cptCrypte);
		eo.setCptGid(cptGid);
		eo.setCptUid(cptUid);
		eo.setCptValide(cptValide);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    return eo;
  }

  public static NSArray<EOGrhumCompte> fetchAllGrhumComptes(EOEditingContext editingContext) {
    return _EOGrhumCompte.fetchAllGrhumComptes(editingContext, null);
  }

  public static NSArray<EOGrhumCompte> fetchAllGrhumComptes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumCompte.fetchGrhumComptes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumCompte> fetchGrhumComptes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumCompte.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumCompte> eoObjects = (NSArray<EOGrhumCompte>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumCompte fetchGrhumCompte(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumCompte.fetchGrhumCompte(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumCompte fetchGrhumCompte(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumCompte> eoObjects = _EOGrhumCompte.fetchGrhumComptes(editingContext, qualifier, null);
    EOGrhumCompte eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumCompte)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumCompte that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumCompte fetchRequiredGrhumCompte(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumCompte.fetchRequiredGrhumCompte(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumCompte fetchRequiredGrhumCompte(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumCompte eoObject = _EOGrhumCompte.fetchGrhumCompte(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumCompte that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumCompte localInstanceIn(EOEditingContext editingContext, EOGrhumCompte eo) {
    EOGrhumCompte localInstance = (eo == null) ? null : (EOGrhumCompte)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
