// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueConservationAnciennete.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueConservationAnciennete extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueConservationAnciennete";

	// Attributes
	public static final String ANC_ANNEE_KEY = "ancAnnee";
	public static final String ANC_NB_ANNEES_KEY = "ancNbAnnees";
	public static final String ANC_NB_JOURS_KEY = "ancNbJours";
	public static final String ANC_NB_MOIS_KEY = "ancNbMois";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_UTILISE_KEY = "temUtilise";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ELEMENT_CARRIERE_KEY = "elementCarriere";
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueConservationAnciennete.class);

  public EOMangueConservationAnciennete localInstanceIn(EOEditingContext editingContext) {
    EOMangueConservationAnciennete localInstance = (EOMangueConservationAnciennete)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer ancAnnee() {
    return (Integer) storedValueForKey("ancAnnee");
  }

  public void setAncAnnee(Integer value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating ancAnnee from " + ancAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, "ancAnnee");
  }

  public Integer ancNbAnnees() {
    return (Integer) storedValueForKey("ancNbAnnees");
  }

  public void setAncNbAnnees(Integer value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating ancNbAnnees from " + ancNbAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbAnnees");
  }

  public Integer ancNbJours() {
    return (Integer) storedValueForKey("ancNbJours");
  }

  public void setAncNbJours(Integer value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating ancNbJours from " + ancNbJours() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbJours");
  }

  public Integer ancNbMois() {
    return (Integer) storedValueForKey("ancNbMois");
  }

  public void setAncNbMois(Integer value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating ancNbMois from " + ancNbMois() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbMois");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temUtilise() {
    return (String) storedValueForKey("temUtilise");
  }

  public void setTemUtilise(String value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating temUtilise from " + temUtilise() + " to " + value);
    }
    takeStoredValueForKey(value, "temUtilise");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
    	_EOMangueConservationAnciennete.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere elementCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere)storedValueForKey("elementCarriere");
  }

  public void setElementCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
      _EOMangueConservationAnciennete.LOG.debug("updating elementCarriere from " + elementCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere oldValue = elementCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "elementCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "elementCarriere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueConservationAnciennete.LOG.isDebugEnabled()) {
      _EOMangueConservationAnciennete.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueConservationAnciennete createMangueConservationAnciennete(EOEditingContext editingContext, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere elementCarriere, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueConservationAnciennete eo = (EOMangueConservationAnciennete) EOUtilities.createAndInsertInstance(editingContext, _EOMangueConservationAnciennete.ENTITY_NAME);    
		eo.setTemValide(temValide);
    eo.setElementCarriereRelationship(elementCarriere);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueConservationAnciennete> fetchAllMangueConservationAnciennetes(EOEditingContext editingContext) {
    return _EOMangueConservationAnciennete.fetchAllMangueConservationAnciennetes(editingContext, null);
  }

  public static NSArray<EOMangueConservationAnciennete> fetchAllMangueConservationAnciennetes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueConservationAnciennete.fetchMangueConservationAnciennetes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueConservationAnciennete> fetchMangueConservationAnciennetes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueConservationAnciennete.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueConservationAnciennete> eoObjects = (NSArray<EOMangueConservationAnciennete>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueConservationAnciennete fetchMangueConservationAnciennete(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueConservationAnciennete.fetchMangueConservationAnciennete(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueConservationAnciennete fetchMangueConservationAnciennete(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueConservationAnciennete> eoObjects = _EOMangueConservationAnciennete.fetchMangueConservationAnciennetes(editingContext, qualifier, null);
    EOMangueConservationAnciennete eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueConservationAnciennete)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueConservationAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueConservationAnciennete fetchRequiredMangueConservationAnciennete(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueConservationAnciennete.fetchRequiredMangueConservationAnciennete(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueConservationAnciennete fetchRequiredMangueConservationAnciennete(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueConservationAnciennete eoObject = _EOMangueConservationAnciennete.fetchMangueConservationAnciennete(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueConservationAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueConservationAnciennete localInstanceIn(EOEditingContext editingContext, EOMangueConservationAnciennete eo) {
    EOMangueConservationAnciennete localInstance = (eo == null) ? null : (EOMangueConservationAnciennete)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
