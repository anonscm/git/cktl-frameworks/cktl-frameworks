package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCongeAccidentServCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EODeclarationAccidentCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCongeAccidentServ;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongeAccidentServ extends _EOMangueCongeAccidentServ {

	/**
	 * Recherche des delegations d'un individu pendant une periode donnee
	 * 
	 * @param individu
	 *            Grhum
	 * @param debutPeriode
	 *            debut periode
	 * @param finPeriode
	 *            fin periode (peut etre nulle)
	 * @return retourne les temps partiels trouves et tous les tp si individu et debutPeriode sont nuls
	 */
	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu)));
		}
		if (debutPeriode != null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		EOCongeAccidentServ congeAccidentServ = (EOCongeAccidentServ) recordImport;

		if (congeAccidentServ.declarationAccident() != null) {
			EODeclarationAccidentCorresp declAccidentCorresp = EODeclarationAccidentCorresp.fetchDeclarationAccidentCorresp(editingContext(),
					EODeclarationAccidentCorresp.TO_DECLARATION_ACCIDENT_KEY, congeAccidentServ.declarationAccident());
			setDeclarationRelationship(declAccidentCorresp.toMangueDeclarationAccident());
		}
	}

	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongeAccidentServ) value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongeAccidentServCorresp congeCorresp = EOCongeAccidentServCorresp.fetchCongeAccidentServCorresp(editingContext(),
				EOCongeAccidentServCorresp.CONGE_ACCIDENT_SERV_KEY, congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.congeAccidentServMangue();
	}

	@Override
	public NSTimestamp dateCommission() {
		return null;
	}

	@Override
	public String typeEvenement() {
		return null;
	}
}
