// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumIndividuAutreFonction.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumIndividuAutreFonction extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "GrhumIndividuAutreFonction";

	// Attributes
	public static final String C_FONCTION_KEY = "cFonction";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOGrhumIndividuAutreFonction.class);

  public EOGrhumIndividuAutreFonction localInstanceIn(EOEditingContext editingContext) {
    EOGrhumIndividuAutreFonction localInstance = (EOGrhumIndividuAutreFonction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cFonction() {
    return (Integer) storedValueForKey("cFonction");
  }

  public void setCFonction(Integer value) {
    if (_EOGrhumIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuAutreFonction.LOG.debug( "updating cFonction from " + cFonction() + " to " + value);
    }
    takeStoredValueForKey(value, "cFonction");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOGrhumIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuAutreFonction.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOGrhumIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuAutreFonction.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuAutreFonction.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumIndividuAutreFonction.LOG.isDebugEnabled()) {
    	_EOGrhumIndividuAutreFonction.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOGrhumIndividuAutreFonction.LOG.isDebugEnabled()) {
      _EOGrhumIndividuAutreFonction.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOGrhumIndividuAutreFonction createGrhumIndividuAutreFonction(EOEditingContext editingContext, Integer cFonction
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOGrhumIndividuAutreFonction eo = (EOGrhumIndividuAutreFonction) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumIndividuAutreFonction.ENTITY_NAME);    
		eo.setCFonction(cFonction);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOGrhumIndividuAutreFonction> fetchAllGrhumIndividuAutreFonctions(EOEditingContext editingContext) {
    return _EOGrhumIndividuAutreFonction.fetchAllGrhumIndividuAutreFonctions(editingContext, null);
  }

  public static NSArray<EOGrhumIndividuAutreFonction> fetchAllGrhumIndividuAutreFonctions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumIndividuAutreFonction.fetchGrhumIndividuAutreFonctions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumIndividuAutreFonction> fetchGrhumIndividuAutreFonctions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumIndividuAutreFonction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumIndividuAutreFonction> eoObjects = (NSArray<EOGrhumIndividuAutreFonction>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumIndividuAutreFonction fetchGrhumIndividuAutreFonction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividuAutreFonction.fetchGrhumIndividuAutreFonction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividuAutreFonction fetchGrhumIndividuAutreFonction(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumIndividuAutreFonction> eoObjects = _EOGrhumIndividuAutreFonction.fetchGrhumIndividuAutreFonctions(editingContext, qualifier, null);
    EOGrhumIndividuAutreFonction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumIndividuAutreFonction)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumIndividuAutreFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividuAutreFonction fetchRequiredGrhumIndividuAutreFonction(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumIndividuAutreFonction.fetchRequiredGrhumIndividuAutreFonction(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumIndividuAutreFonction fetchRequiredGrhumIndividuAutreFonction(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumIndividuAutreFonction eoObject = _EOGrhumIndividuAutreFonction.fetchGrhumIndividuAutreFonction(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumIndividuAutreFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumIndividuAutreFonction localInstanceIn(EOEditingContext editingContext, EOGrhumIndividuAutreFonction eo) {
    EOGrhumIndividuAutreFonction localInstance = (eo == null) ? null : (EOGrhumIndividuAutreFonction)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
