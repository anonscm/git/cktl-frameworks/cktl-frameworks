// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueVacataires.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueVacataires extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueVacataires";

	// Attributes
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String C_UAI_KEY = "cUai";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VACATION_KEY = "dDebVacation";
	public static final String D_FIN_VACATION_KEY = "dFinVacation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ENSEIGNEMENT_KEY = "enseignement";
	public static final String NBR_HEURES_KEY = "nbrHeures";
	public static final String NBR_HEURES_REALISEES_KEY = "nbrHeuresRealisees";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String SIT_ID_KEY = "sitId";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String TAUX_HORAIRE_REALISE_KEY = "tauxHoraireRealise";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TEM_PERS_ETAB_KEY = "temPersEtab";
	public static final String TEM_SIGNE_KEY = "temSigne";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_PROFESSION_KEY = "toProfession";
	public static final String TO_TYPE_CONTRAT_TRAVAIL_KEY = "toTypeContratTravail";
	public static final String TO_UAI_KEY = "toUai";

  private static Logger LOG = Logger.getLogger(_EOMangueVacataires.class);

  public EOMangueVacataires localInstanceIn(EOEditingContext editingContext) {
    EOMangueVacataires localInstance = (EOMangueVacataires)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey("adrOrdre");
  }

  public void setAdrOrdre(Integer value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating adrOrdre from " + adrOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "adrOrdre");
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public String cGrade() {
    return (String) storedValueForKey("cGrade");
  }

  public void setCGrade(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating cGrade from " + cGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "cGrade");
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey("cTypeContratTrav");
  }

  public void setCTypeContratTrav(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating cTypeContratTrav from " + cTypeContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeContratTrav");
  }

  public String cUai() {
    return (String) storedValueForKey("cUai");
  }

  public void setCUai(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating cUai from " + cUai() + " to " + value);
    }
    takeStoredValueForKey(value, "cUai");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVacation() {
    return (NSTimestamp) storedValueForKey("dDebVacation");
  }

  public void setDDebVacation(NSTimestamp value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating dDebVacation from " + dDebVacation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVacation");
  }

  public NSTimestamp dFinVacation() {
    return (NSTimestamp) storedValueForKey("dFinVacation");
  }

  public void setDFinVacation(NSTimestamp value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating dFinVacation from " + dFinVacation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVacation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String enseignement() {
    return (String) storedValueForKey("enseignement");
  }

  public void setEnseignement(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating enseignement from " + enseignement() + " to " + value);
    }
    takeStoredValueForKey(value, "enseignement");
  }

  public java.math.BigDecimal nbrHeures() {
    return (java.math.BigDecimal) storedValueForKey("nbrHeures");
  }

  public void setNbrHeures(java.math.BigDecimal value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating nbrHeures from " + nbrHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeures");
  }

  public java.math.BigDecimal nbrHeuresRealisees() {
    return (java.math.BigDecimal) storedValueForKey("nbrHeuresRealisees");
  }

  public void setNbrHeuresRealisees(java.math.BigDecimal value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating nbrHeuresRealisees from " + nbrHeuresRealisees() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrHeuresRealisees");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey("noCnu");
  }

  public void setNoCnu(Integer value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "noCnu");
  }

  public String observations() {
    return (String) storedValueForKey("observations");
  }

  public void setObservations(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating observations from " + observations() + " to " + value);
    }
    takeStoredValueForKey(value, "observations");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String proCode() {
    return (String) storedValueForKey("proCode");
  }

  public void setProCode(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating proCode from " + proCode() + " to " + value);
    }
    takeStoredValueForKey(value, "proCode");
  }

  public Integer sitId() {
    return (Integer) storedValueForKey("sitId");
  }

  public void setSitId(Integer value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating sitId from " + sitId() + " to " + value);
    }
    takeStoredValueForKey(value, "sitId");
  }

  public java.math.BigDecimal tauxHoraire() {
    return (java.math.BigDecimal) storedValueForKey("tauxHoraire");
  }

  public void setTauxHoraire(java.math.BigDecimal value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating tauxHoraire from " + tauxHoraire() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraire");
  }

  public java.math.BigDecimal tauxHoraireRealise() {
    return (java.math.BigDecimal) storedValueForKey("tauxHoraireRealise");
  }

  public void setTauxHoraireRealise(java.math.BigDecimal value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating tauxHoraireRealise from " + tauxHoraireRealise() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraireRealise");
  }

  public String temEnseignant() {
    return (String) storedValueForKey("temEnseignant");
  }

  public void setTemEnseignant(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating temEnseignant from " + temEnseignant() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnseignant");
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey("temPaiementPonctuel");
  }

  public void setTemPaiementPonctuel(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating temPaiementPonctuel from " + temPaiementPonctuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temPaiementPonctuel");
  }

  public String temPersEtab() {
    return (String) storedValueForKey("temPersEtab");
  }

  public void setTemPersEtab(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating temPersEtab from " + temPersEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temPersEtab");
  }

  public String temSigne() {
    return (String) storedValueForKey("temSigne");
  }

  public void setTemSigne(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating temSigne from " + temSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temSigne");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
    	_EOMangueVacataires.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse toAdresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse)storedValueForKey("toAdresse");
  }

  public void setToAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating toAdresse from " + toAdresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAdresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAdresse");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu toCnu() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps toCorps() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps)storedValueForKey("toCorps");
  }

  public void setToCorpsRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating toCorps from " + toCorps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCorps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCorps");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade toGrade() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade)storedValueForKey("toGrade");
  }

  public void setToGradeRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating toGrade from " + toGrade() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrade");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrade");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession toProfession() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession)storedValueForKey("toProfession");
  }

  public void setToProfessionRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating toProfession from " + toProfession() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOProfession oldValue = toProfession();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toProfession");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toProfession");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail toTypeContratTravail() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail)storedValueForKey("toTypeContratTravail");
  }

  public void setToTypeContratTravailRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating toTypeContratTravail from " + toTypeContratTravail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail oldValue = toTypeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeContratTravail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeContratTravail");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne toUai() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne)storedValueForKey("toUai");
  }

  public void setToUaiRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne value) {
    if (_EOMangueVacataires.LOG.isDebugEnabled()) {
      _EOMangueVacataires.LOG.debug("updating toUai from " + toUai() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne oldValue = toUai();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toUai");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toUai");
    }
  }
  

  public static EOMangueVacataires createMangueVacataires(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebVacation
, NSTimestamp dModification
, String temEnseignant
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueVacataires eo = (EOMangueVacataires) EOUtilities.createAndInsertInstance(editingContext, _EOMangueVacataires.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebVacation(dDebVacation);
		eo.setDModification(dModification);
		eo.setTemEnseignant(temEnseignant);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueVacataires> fetchAllMangueVacataireses(EOEditingContext editingContext) {
    return _EOMangueVacataires.fetchAllMangueVacataireses(editingContext, null);
  }

  public static NSArray<EOMangueVacataires> fetchAllMangueVacataireses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueVacataires.fetchMangueVacataireses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueVacataires> fetchMangueVacataireses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueVacataires.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueVacataires> eoObjects = (NSArray<EOMangueVacataires>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueVacataires fetchMangueVacataires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueVacataires.fetchMangueVacataires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueVacataires fetchMangueVacataires(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueVacataires> eoObjects = _EOMangueVacataires.fetchMangueVacataireses(editingContext, qualifier, null);
    EOMangueVacataires eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueVacataires)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueVacataires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueVacataires fetchRequiredMangueVacataires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueVacataires.fetchRequiredMangueVacataires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueVacataires fetchRequiredMangueVacataires(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueVacataires eoObject = _EOMangueVacataires.fetchMangueVacataires(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueVacataires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueVacataires localInstanceIn(EOEditingContext editingContext, EOMangueVacataires eo) {
    EOMangueVacataires localInstance = (eo == null) ? null : (EOMangueVacataires)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
