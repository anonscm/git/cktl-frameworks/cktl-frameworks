// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumRib.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumRib extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumRibfour";

	// Attributes
	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String CLE_RIB_KEY = "cleRib";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String IBAN_KEY = "iban";
	public static final String MOD_CODE_KEY = "modCode";
	public static final String NO_COMPTE_KEY = "noCompte";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String RIB_TITCO_KEY = "ribTitco";
	public static final String RIB_VALIDE_KEY = "ribValide";
	public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";

	// Relationships
	public static final String BANQUE_KEY = "banque";
	public static final String FOURNIS_KEY = "fournis";

  private static Logger LOG = Logger.getLogger(_EOGrhumRib.class);

  public EOGrhumRib localInstanceIn(EOEditingContext editingContext) {
    EOGrhumRib localInstance = (EOGrhumRib)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String bic() {
    return (String) storedValueForKey("bic");
  }

  public void setBic(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating bic from " + bic() + " to " + value);
    }
    takeStoredValueForKey(value, "bic");
  }

  public String cBanque() {
    return (String) storedValueForKey("cBanque");
  }

  public void setCBanque(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating cBanque from " + cBanque() + " to " + value);
    }
    takeStoredValueForKey(value, "cBanque");
  }

  public String cGuichet() {
    return (String) storedValueForKey("cGuichet");
  }

  public void setCGuichet(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating cGuichet from " + cGuichet() + " to " + value);
    }
    takeStoredValueForKey(value, "cGuichet");
  }

  public String cleRib() {
    return (String) storedValueForKey("cleRib");
  }

  public void setCleRib(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating cleRib from " + cleRib() + " to " + value);
    }
    takeStoredValueForKey(value, "cleRib");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String iban() {
    return (String) storedValueForKey("iban");
  }

  public void setIban(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating iban from " + iban() + " to " + value);
    }
    takeStoredValueForKey(value, "iban");
  }

  public String modCode() {
    return (String) storedValueForKey("modCode");
  }

  public void setModCode(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating modCode from " + modCode() + " to " + value);
    }
    takeStoredValueForKey(value, "modCode");
  }

  public String noCompte() {
    return (String) storedValueForKey("noCompte");
  }

  public void setNoCompte(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating noCompte from " + noCompte() + " to " + value);
    }
    takeStoredValueForKey(value, "noCompte");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String ribTitco() {
    return (String) storedValueForKey("ribTitco");
  }

  public void setRibTitco(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating ribTitco from " + ribTitco() + " to " + value);
    }
    takeStoredValueForKey(value, "ribTitco");
  }

  public String ribValide() {
    return (String) storedValueForKey("ribValide");
  }

  public void setRibValide(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating ribValide from " + ribValide() + " to " + value);
    }
    takeStoredValueForKey(value, "ribValide");
  }

  public String temPayeUtil() {
    return (String) storedValueForKey("temPayeUtil");
  }

  public void setTemPayeUtil(String value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
    	_EOGrhumRib.LOG.debug( "updating temPayeUtil from " + temPayeUtil() + " to " + value);
    }
    takeStoredValueForKey(value, "temPayeUtil");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque banque() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque)storedValueForKey("banque");
  }

  public void setBanqueRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
      _EOGrhumRib.LOG.debug("updating banque from " + banque() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBanque oldValue = banque();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "banque");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "banque");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis fournis() {
    return (org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis)storedValueForKey("fournis");
  }

  public void setFournisRelationship(org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis value) {
    if (_EOGrhumRib.LOG.isDebugEnabled()) {
      _EOGrhumRib.LOG.debug("updating fournis from " + fournis() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "fournis");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "fournis");
    }
  }
  

  public static EOGrhumRib createGrhumRibfour(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String modCode
, String ribTitco
, String ribValide
, org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis fournis) {
    EOGrhumRib eo = (EOGrhumRib) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumRib.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setModCode(modCode);
		eo.setRibTitco(ribTitco);
		eo.setRibValide(ribValide);
    eo.setFournisRelationship(fournis);
    return eo;
  }

  public static NSArray<EOGrhumRib> fetchAllGrhumRibfours(EOEditingContext editingContext) {
    return _EOGrhumRib.fetchAllGrhumRibfours(editingContext, null);
  }

  public static NSArray<EOGrhumRib> fetchAllGrhumRibfours(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumRib.fetchGrhumRibfours(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumRib> fetchGrhumRibfours(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumRib.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumRib> eoObjects = (NSArray<EOGrhumRib>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumRib fetchGrhumRibfour(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumRib.fetchGrhumRibfour(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumRib fetchGrhumRibfour(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumRib> eoObjects = _EOGrhumRib.fetchGrhumRibfours(editingContext, qualifier, null);
    EOGrhumRib eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumRib)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumRibfour that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumRib fetchRequiredGrhumRibfour(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumRib.fetchRequiredGrhumRibfour(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumRib fetchRequiredGrhumRibfour(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumRib eoObject = _EOGrhumRib.fetchGrhumRibfour(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumRibfour that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumRib localInstanceIn(EOEditingContext editingContext, EOGrhumRib eo) {
    EOGrhumRib localInstance = (eo == null) ? null : (EOGrhumRib)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
