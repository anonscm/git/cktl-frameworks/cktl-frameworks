// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueClm.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueClm extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueClm";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_CLM_KEY = "dComMedClm";
	public static final String D_COM_MED_CLM_SUP_KEY = "dComMedClmSup";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_EN_CAUSE_KEY = "temEnCause";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PROLONG_CLM_KEY = "temProlongClm";
	public static final String TEM_REQUALIF_CLM_KEY = "temRequalifClm";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueClm.class);

  public EOMangueClm localInstanceIn(EOEditingContext editingContext) {
    EOMangueClm localInstance = (EOMangueClm)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedClm() {
    return (NSTimestamp) storedValueForKey("dComMedClm");
  }

  public void setDComMedClm(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dComMedClm from " + dComMedClm() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedClm");
  }

  public NSTimestamp dComMedClmSup() {
    return (NSTimestamp) storedValueForKey("dComMedClmSup");
  }

  public void setDComMedClmSup(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dComMedClmSup from " + dComMedClmSup() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedClmSup");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temEnCause() {
    return (String) storedValueForKey("temEnCause");
  }

  public void setTemEnCause(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating temEnCause from " + temEnCause() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnCause");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temProlongClm() {
    return (String) storedValueForKey("temProlongClm");
  }

  public void setTemProlongClm(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating temProlongClm from " + temProlongClm() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongClm");
  }

  public String temRequalifClm() {
    return (String) storedValueForKey("temRequalifClm");
  }

  public void setTemRequalifClm(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating temRequalifClm from " + temRequalifClm() + " to " + value);
    }
    takeStoredValueForKey(value, "temRequalifClm");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
    	_EOMangueClm.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
      _EOMangueClm.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm value) {
    if (_EOMangueClm.LOG.isDebugEnabled()) {
      _EOMangueClm.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueClm oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueClm createMangueClm(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temConfirme
, String temEnCause
, String temGestEtab
, String temProlongClm
, String temRequalifClm
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueClm eo = (EOMangueClm) EOUtilities.createAndInsertInstance(editingContext, _EOMangueClm.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemConfirme(temConfirme);
		eo.setTemEnCause(temEnCause);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemProlongClm(temProlongClm);
		eo.setTemRequalifClm(temRequalifClm);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueClm> fetchAllMangueClms(EOEditingContext editingContext) {
    return _EOMangueClm.fetchAllMangueClms(editingContext, null);
  }

  public static NSArray<EOMangueClm> fetchAllMangueClms(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueClm.fetchMangueClms(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueClm> fetchMangueClms(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueClm.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueClm> eoObjects = (NSArray<EOMangueClm>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueClm fetchMangueClm(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueClm.fetchMangueClm(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueClm fetchMangueClm(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueClm> eoObjects = _EOMangueClm.fetchMangueClms(editingContext, qualifier, null);
    EOMangueClm eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueClm)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueClm that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueClm fetchRequiredMangueClm(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueClm.fetchRequiredMangueClm(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueClm fetchRequiredMangueClm(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueClm eoObject = _EOMangueClm.fetchMangueClm(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueClm that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueClm localInstanceIn(EOEditingContext editingContext, EOMangueClm eo) {
    EOMangueClm localInstance = (eo == null) ? null : (EOMangueClm)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
