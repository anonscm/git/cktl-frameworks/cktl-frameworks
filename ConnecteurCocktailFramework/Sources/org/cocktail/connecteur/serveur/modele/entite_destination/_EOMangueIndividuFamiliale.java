// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueIndividuFamiliale.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueIndividuFamiliale extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueIndividuFamiliale";

	// Attributes
	public static final String C_SITUATION_FAMILLE_KEY = "cSituationFamille";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_SIT_FAMILIALE_KEY = "dDebSitFamiliale";
	public static final String D_FIN_SIT_FAMILIALE_KEY = "dFinSitFamiliale";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOMangueIndividuFamiliale.class);

  public EOMangueIndividuFamiliale localInstanceIn(EOEditingContext editingContext) {
    EOMangueIndividuFamiliale localInstance = (EOMangueIndividuFamiliale)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cSituationFamille() {
    return (String) storedValueForKey("cSituationFamille");
  }

  public void setCSituationFamille(String value) {
    if (_EOMangueIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueIndividuFamiliale.LOG.debug( "updating cSituationFamille from " + cSituationFamille() + " to " + value);
    }
    takeStoredValueForKey(value, "cSituationFamille");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueIndividuFamiliale.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebSitFamiliale() {
    return (NSTimestamp) storedValueForKey("dDebSitFamiliale");
  }

  public void setDDebSitFamiliale(NSTimestamp value) {
    if (_EOMangueIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueIndividuFamiliale.LOG.debug( "updating dDebSitFamiliale from " + dDebSitFamiliale() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebSitFamiliale");
  }

  public NSTimestamp dFinSitFamiliale() {
    return (NSTimestamp) storedValueForKey("dFinSitFamiliale");
  }

  public void setDFinSitFamiliale(NSTimestamp value) {
    if (_EOMangueIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueIndividuFamiliale.LOG.debug( "updating dFinSitFamiliale from " + dFinSitFamiliale() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinSitFamiliale");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueIndividuFamiliale.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueIndividuFamiliale.LOG.isDebugEnabled()) {
    	_EOMangueIndividuFamiliale.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueIndividuFamiliale.LOG.isDebugEnabled()) {
      _EOMangueIndividuFamiliale.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOMangueIndividuFamiliale createMangueIndividuFamiliale(EOEditingContext editingContext, String cSituationFamille
, NSTimestamp dCreation
, NSTimestamp dDebSitFamiliale
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueIndividuFamiliale eo = (EOMangueIndividuFamiliale) EOUtilities.createAndInsertInstance(editingContext, _EOMangueIndividuFamiliale.ENTITY_NAME);    
		eo.setCSituationFamille(cSituationFamille);
		eo.setDCreation(dCreation);
		eo.setDDebSitFamiliale(dDebSitFamiliale);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueIndividuFamiliale> fetchAllMangueIndividuFamiliales(EOEditingContext editingContext) {
    return _EOMangueIndividuFamiliale.fetchAllMangueIndividuFamiliales(editingContext, null);
  }

  public static NSArray<EOMangueIndividuFamiliale> fetchAllMangueIndividuFamiliales(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueIndividuFamiliale.fetchMangueIndividuFamiliales(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueIndividuFamiliale> fetchMangueIndividuFamiliales(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueIndividuFamiliale.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueIndividuFamiliale> eoObjects = (NSArray<EOMangueIndividuFamiliale>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueIndividuFamiliale fetchMangueIndividuFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueIndividuFamiliale.fetchMangueIndividuFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueIndividuFamiliale fetchMangueIndividuFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueIndividuFamiliale> eoObjects = _EOMangueIndividuFamiliale.fetchMangueIndividuFamiliales(editingContext, qualifier, null);
    EOMangueIndividuFamiliale eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueIndividuFamiliale)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueIndividuFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueIndividuFamiliale fetchRequiredMangueIndividuFamiliale(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueIndividuFamiliale.fetchRequiredMangueIndividuFamiliale(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueIndividuFamiliale fetchRequiredMangueIndividuFamiliale(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueIndividuFamiliale eoObject = _EOMangueIndividuFamiliale.fetchMangueIndividuFamiliale(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueIndividuFamiliale that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueIndividuFamiliale localInstanceIn(EOEditingContext editingContext, EOMangueIndividuFamiliale eo) {
    EOMangueIndividuFamiliale localInstance = (eo == null) ? null : (EOMangueIndividuFamiliale)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
