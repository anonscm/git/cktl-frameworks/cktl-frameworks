//EOGrhumRib.java
//Created on Wed Jul 25 15:14:41 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOGrhumParametres;
import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EORib;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOFournis;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EORepartPersonneAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// 23/06/2010 - modifié pour évaluation de la foreign key sur la banque, une fois les attributs fournis + persIdCreation et modification
public class EOGrhumRib extends _EOGrhumRib implements InterfaceGestionIdUtilisateur {

	public EOGrhumRib() {
		super();
	}
	
	
	// Méthodes ajoutées
	/** return true si le contrat  n'est pas annule */
	public boolean estValide() {
		return ribValide() != null && ribValide().equals(CocktailConstantes.VRAI);
	}
	// Surcharge pour gerer le temoin d'annulation
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setRibValide(CocktailConstantes.VRAI);
		} else {
			setRibValide(CocktailConstantes.FAUX);
		}
	}

	protected void initialiserObjet(ObjetImport record) throws Exception {
		setTemPayeUtil(CocktailConstantes.FAUX);
		setModCode(EOGrhumParametres.parametrePourCle(editingContext(), "ANNUAIRE_MODE_PAIEMENT"));
		// Vérifier si il existe un fournisseur pour la structure ou l'individu
		EORib ribImport = (EORib)record;
		
		Number persId = null;
		String nom = null;
		if (ribImport.strSource() != null) {
			EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), ribImport.strSource());
			if (structure != null) {
				persId = structure.persId();
				nom = structure.llStructure();
			}
		} else if (ribImport.idSource() != null) {
			EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), ribImport.idSource());
			if (individu != null) {
				persId = individu.persId();
				nom = individu.nomUsuel();
			}
		}
		if (persId != null) {
			EOFournis fournis = EOFournis.rechercherFournisAvecId(editingContext(), persId);
			if (fournis == null) {
				try {
					fournis = creerFournisseur(persId,nom,ribImport.grhumAdresseFournisseur());
				} catch (Exception e) {
					String message = e.getMessage();
					message = "Erreur lors de la création du fournisseur: " + message + " pour " + SuperFinder.recordAsDict(record);
					throw new Exception(message);
				}
			} 
			addObjectToBothSidesOfRelationshipWithKey(fournis, "fournis");		
		} else {
			throw new Exception("Pas de persId valable pour " + record);
		}
		
		setBanqueRelationship(ribImport.banque());
	}
	// Méthodes statiques
	/** retourne  le rib valide du SI avec le m&ecirc;me persId, la m&egrave;me banque,guichet, et compte
	 * @param editingContext
	 * @param banque
	 * @param guichet
	 * @param noCompte
	 * @param persId
	 * @return EOGrhumRib
	 */
	public static EOGrhumRib ribDestinationPourRib(EOEditingContext editingContext,EORib rib) {
		Number persID = null;
		if (rib.individu() != null) {	
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)rib.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				persID = individuGrhum.persId();
			}
		} else {
			// On vérifie si il existe une correspondance sinon (la structure n'a peut-être pas encore été créée)
			// on cherche directement dans la base destinataire
			EOGrhumStructure structureGrhum = (EOGrhumStructure)rib.structure().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (structureGrhum != null) {
				persID = structureGrhum.persId();
			}
		}
		EOQualifier myQualifier = null;
		if (rib.cBanque()!=null && rib.cGuichet()!=null && rib.noCompte()!=null) {
		NSMutableArray args = new NSMutableArray(rib.cBanque());
		args.addObject(rib.cGuichet());
		args.addObject(rib.noCompte());
		args.addObject(persID);
		myQualifier = EOQualifier.qualifierWithQualifierFormat(C_BANQUE_KEY+" = %@ AND "+C_GUICHET_KEY+" = %@ AND "+NO_COMPTE_KEY+" = %@  AND "+FOURNIS_KEY+"."+EOFournis.PERS_ID_KEY+" = %@ AND "+RIB_VALIDE_KEY+" = 'O'",args);
		} else if (rib.iban()!=null && rib.bic()!=null){
			NSMutableArray args = new NSMutableArray(rib.iban());
			args.addObject(rib.bic());
			args.addObject(persID);
			myQualifier = EOQualifier.qualifierWithQualifierFormat(IBAN_KEY+" = %@ AND "+BIC_KEY+" = %@  AND "+FOURNIS_KEY+"."+EOFournis.PERS_ID_KEY+" = %@ AND "+RIB_VALIDE_KEY+" = 'O'",args);			
		}
		
		if (myQualifier==null)
			return null;
		
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
		try {
			// On ne vérifie pas si il peut être le correspondant car les ribs sont uniques
			// pour une persid , une banque, un guichet et un compte
			return (EOGrhumRib)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	}

	// Méthodes privées
	private EOFournis creerFournisseur(Number persId,String nom,EOGrhumAdresse adresse) throws Exception {
		// La création du fournisseur doit être faite dans la base avant la création du rib
		// Comme on ne contrôle pas l'ordre dans lequel sont faits les insert, il faut créer le fournisseur (et validFournis)
		// dans un autre editing context qu'on sauve immédiatement
		// créer le fournisseur
		EOEditingContext editingContextTemp = new EOEditingContext();
		EOFournis fournis =  new EOFournis();
		editingContextTemp.insertObject(fournis);
		fournis.initFournis(adresse.localInstanceIn(editingContextTemp),persId,nom);	// initialisation et création du valid fournis
		editingContextTemp.saveChanges();
		EOGlobalID gid = editingContextTemp.globalIDForObject(fournis);
		return (EOFournis)Finder.objetForGlobalIDDansEditingContext(gid, editingContext());	// retourner l'objet dans l'ed courant
	}



}
