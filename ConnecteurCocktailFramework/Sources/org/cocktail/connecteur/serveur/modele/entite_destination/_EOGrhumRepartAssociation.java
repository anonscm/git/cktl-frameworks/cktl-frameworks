// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumRepartAssociation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumRepartAssociation extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumRepartAssociation";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String QUOTITE_KEY = "quotite";
	public static final String RANG_KEY = "rang";

	// Relationships
	public static final String ASSOCIATION_KEY = "association";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOGrhumRepartAssociation.class);

  public EOGrhumRepartAssociation localInstanceIn(EOEditingContext editingContext) {
    EOGrhumRepartAssociation localInstance = (EOGrhumRepartAssociation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey("dFermeture");
  }

  public void setDFermeture(NSTimestamp value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating dFermeture from " + dFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermeture");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey("dOuverture");
  }

  public void setDOuverture(NSTimestamp value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating dOuverture from " + dOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dOuverture");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public Double quotite() {
    return (Double) storedValueForKey("quotite");
  }

  public void setQuotite(Double value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public Integer rang() {
    return (Integer) storedValueForKey("rang");
  }

  public void setRang(Integer value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
    	_EOGrhumRepartAssociation.LOG.debug( "updating rang from " + rang() + " to " + value);
    }
    takeStoredValueForKey(value, "rang");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation association() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation)storedValueForKey("association");
  }

  public void setAssociationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
      _EOGrhumRepartAssociation.LOG.debug("updating association from " + association() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation oldValue = association();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "association");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "association");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOGrhumRepartAssociation.LOG.isDebugEnabled()) {
      _EOGrhumRepartAssociation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOGrhumRepartAssociation createGrhumRepartAssociation(EOEditingContext editingContext, Integer persId
, Integer rang
, org.cocktail.connecteur.common.modele.grhum.nomenclature.EOAssociation association, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure) {
    EOGrhumRepartAssociation eo = (EOGrhumRepartAssociation) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumRepartAssociation.ENTITY_NAME);    
		eo.setPersId(persId);
		eo.setRang(rang);
    eo.setAssociationRelationship(association);
    eo.setStructureRelationship(structure);
    return eo;
  }

  public static NSArray<EOGrhumRepartAssociation> fetchAllGrhumRepartAssociations(EOEditingContext editingContext) {
    return _EOGrhumRepartAssociation.fetchAllGrhumRepartAssociations(editingContext, null);
  }

  public static NSArray<EOGrhumRepartAssociation> fetchAllGrhumRepartAssociations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumRepartAssociation.fetchGrhumRepartAssociations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumRepartAssociation> fetchGrhumRepartAssociations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumRepartAssociation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumRepartAssociation> eoObjects = (NSArray<EOGrhumRepartAssociation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumRepartAssociation fetchGrhumRepartAssociation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumRepartAssociation.fetchGrhumRepartAssociation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumRepartAssociation fetchGrhumRepartAssociation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumRepartAssociation> eoObjects = _EOGrhumRepartAssociation.fetchGrhumRepartAssociations(editingContext, qualifier, null);
    EOGrhumRepartAssociation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumRepartAssociation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumRepartAssociation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumRepartAssociation fetchRequiredGrhumRepartAssociation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumRepartAssociation.fetchRequiredGrhumRepartAssociation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumRepartAssociation fetchRequiredGrhumRepartAssociation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumRepartAssociation eoObject = _EOGrhumRepartAssociation.fetchGrhumRepartAssociation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumRepartAssociation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumRepartAssociation localInstanceIn(EOEditingContext editingContext, EOGrhumRepartAssociation eo) {
    EOGrhumRepartAssociation localInstance = (eo == null) ? null : (EOGrhumRepartAssociation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
