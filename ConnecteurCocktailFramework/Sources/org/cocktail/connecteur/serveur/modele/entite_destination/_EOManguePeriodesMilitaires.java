// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOManguePeriodesMilitaires.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOManguePeriodesMilitaires extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "ManguePeriodesMilitaires";

	// Attributes
	public static final String C_PERIODE_MILITAIRE_KEY = "cPeriodeMilitaire";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOManguePeriodesMilitaires.class);

  public EOManguePeriodesMilitaires localInstanceIn(EOEditingContext editingContext) {
    EOManguePeriodesMilitaires localInstance = (EOManguePeriodesMilitaires)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cPeriodeMilitaire() {
    return (String) storedValueForKey("cPeriodeMilitaire");
  }

  public void setCPeriodeMilitaire(String value) {
    if (_EOManguePeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOManguePeriodesMilitaires.LOG.debug( "updating cPeriodeMilitaire from " + cPeriodeMilitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "cPeriodeMilitaire");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOManguePeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOManguePeriodesMilitaires.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOManguePeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOManguePeriodesMilitaires.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOManguePeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOManguePeriodesMilitaires.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOManguePeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOManguePeriodesMilitaires.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOManguePeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOManguePeriodesMilitaires.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOManguePeriodesMilitaires.LOG.isDebugEnabled()) {
      _EOManguePeriodesMilitaires.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOManguePeriodesMilitaires createManguePeriodesMilitaires(EOEditingContext editingContext, String cPeriodeMilitaire
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOManguePeriodesMilitaires eo = (EOManguePeriodesMilitaires) EOUtilities.createAndInsertInstance(editingContext, _EOManguePeriodesMilitaires.ENTITY_NAME);    
		eo.setCPeriodeMilitaire(cPeriodeMilitaire);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOManguePeriodesMilitaires> fetchAllManguePeriodesMilitaireses(EOEditingContext editingContext) {
    return _EOManguePeriodesMilitaires.fetchAllManguePeriodesMilitaireses(editingContext, null);
  }

  public static NSArray<EOManguePeriodesMilitaires> fetchAllManguePeriodesMilitaireses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOManguePeriodesMilitaires.fetchManguePeriodesMilitaireses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOManguePeriodesMilitaires> fetchManguePeriodesMilitaireses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOManguePeriodesMilitaires.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOManguePeriodesMilitaires> eoObjects = (NSArray<EOManguePeriodesMilitaires>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOManguePeriodesMilitaires fetchManguePeriodesMilitaires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePeriodesMilitaires.fetchManguePeriodesMilitaires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePeriodesMilitaires fetchManguePeriodesMilitaires(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOManguePeriodesMilitaires> eoObjects = _EOManguePeriodesMilitaires.fetchManguePeriodesMilitaireses(editingContext, qualifier, null);
    EOManguePeriodesMilitaires eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOManguePeriodesMilitaires)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ManguePeriodesMilitaires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePeriodesMilitaires fetchRequiredManguePeriodesMilitaires(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePeriodesMilitaires.fetchRequiredManguePeriodesMilitaires(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePeriodesMilitaires fetchRequiredManguePeriodesMilitaires(EOEditingContext editingContext, EOQualifier qualifier) {
    EOManguePeriodesMilitaires eoObject = _EOManguePeriodesMilitaires.fetchManguePeriodesMilitaires(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ManguePeriodesMilitaires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePeriodesMilitaires localInstanceIn(EOEditingContext editingContext, EOManguePeriodesMilitaires eo) {
    EOManguePeriodesMilitaires localInstance = (eo == null) ? null : (EOManguePeriodesMilitaires)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
