// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumInstance.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumInstance extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumInstance";

	// Attributes
	public static final String C_INSTANCE_KEY = "cInstance";
	public static final String C_TYPE_INSTANCE_KEY = "cTypeInstance";
	public static final String D_CALCUL_KEY = "dCalcul";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_REFERENCE_KEY = "dReference";
	public static final String D_SCRUTIN_KEY = "dScrutin";
	public static final String LC_INSTANCE_KEY = "lcInstance";
	public static final String LL_INSTANCE_KEY = "llInstance";
	public static final String TEM_EDITION_COHÉRENTE_KEY = "temEditionCohérente";
	public static final String TEM_PARAMETRAGE_COHERENT_KEY = "temParametrageCoherent";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOGrhumInstance.class);

  public EOGrhumInstance localInstanceIn(EOEditingContext editingContext) {
    EOGrhumInstance localInstance = (EOGrhumInstance)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cInstance() {
    return (String) storedValueForKey("cInstance");
  }

  public void setCInstance(String value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating cInstance from " + cInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "cInstance");
  }

  public String cTypeInstance() {
    return (String) storedValueForKey("cTypeInstance");
  }

  public void setCTypeInstance(String value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating cTypeInstance from " + cTypeInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeInstance");
  }

  public NSTimestamp dCalcul() {
    return (NSTimestamp) storedValueForKey("dCalcul");
  }

  public void setDCalcul(NSTimestamp value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating dCalcul from " + dCalcul() + " to " + value);
    }
    takeStoredValueForKey(value, "dCalcul");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dReference() {
    return (NSTimestamp) storedValueForKey("dReference");
  }

  public void setDReference(NSTimestamp value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating dReference from " + dReference() + " to " + value);
    }
    takeStoredValueForKey(value, "dReference");
  }

  public NSTimestamp dScrutin() {
    return (NSTimestamp) storedValueForKey("dScrutin");
  }

  public void setDScrutin(NSTimestamp value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating dScrutin from " + dScrutin() + " to " + value);
    }
    takeStoredValueForKey(value, "dScrutin");
  }

  public String lcInstance() {
    return (String) storedValueForKey("lcInstance");
  }

  public void setLcInstance(String value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating lcInstance from " + lcInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "lcInstance");
  }

  public String llInstance() {
    return (String) storedValueForKey("llInstance");
  }

  public void setLlInstance(String value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating llInstance from " + llInstance() + " to " + value);
    }
    takeStoredValueForKey(value, "llInstance");
  }

  public String temEditionCohérente() {
    return (String) storedValueForKey("temEditionCohérente");
  }

  public void setTemEditionCohérente(String value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating temEditionCohérente from " + temEditionCohérente() + " to " + value);
    }
    takeStoredValueForKey(value, "temEditionCohérente");
  }

  public String temParametrageCoherent() {
    return (String) storedValueForKey("temParametrageCoherent");
  }

  public void setTemParametrageCoherent(String value) {
    if (_EOGrhumInstance.LOG.isDebugEnabled()) {
    	_EOGrhumInstance.LOG.debug( "updating temParametrageCoherent from " + temParametrageCoherent() + " to " + value);
    }
    takeStoredValueForKey(value, "temParametrageCoherent");
  }


  public static EOGrhumInstance createGrhumInstance(EOEditingContext editingContext, String cInstance
, String cTypeInstance
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcInstance
, String llInstance
) {
    EOGrhumInstance eo = (EOGrhumInstance) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumInstance.ENTITY_NAME);    
		eo.setCInstance(cInstance);
		eo.setCTypeInstance(cTypeInstance);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcInstance(lcInstance);
		eo.setLlInstance(llInstance);
    return eo;
  }

  public static NSArray<EOGrhumInstance> fetchAllGrhumInstances(EOEditingContext editingContext) {
    return _EOGrhumInstance.fetchAllGrhumInstances(editingContext, null);
  }

  public static NSArray<EOGrhumInstance> fetchAllGrhumInstances(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumInstance.fetchGrhumInstances(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumInstance> fetchGrhumInstances(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumInstance.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumInstance> eoObjects = (NSArray<EOGrhumInstance>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumInstance fetchGrhumInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumInstance.fetchGrhumInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumInstance fetchGrhumInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumInstance> eoObjects = _EOGrhumInstance.fetchGrhumInstances(editingContext, qualifier, null);
    EOGrhumInstance eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumInstance)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumInstance fetchRequiredGrhumInstance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumInstance.fetchRequiredGrhumInstance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumInstance fetchRequiredGrhumInstance(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumInstance eoObject = _EOGrhumInstance.fetchGrhumInstance(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumInstance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumInstance localInstanceIn(EOEditingContext editingContext, EOGrhumInstance eo) {
    EOGrhumInstance localInstance = (eo == null) ? null : (EOGrhumInstance)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
