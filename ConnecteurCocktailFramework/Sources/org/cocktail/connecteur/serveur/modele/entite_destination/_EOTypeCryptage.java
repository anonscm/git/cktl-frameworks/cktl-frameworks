// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeCryptage.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeCryptage extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeCryptage";

	// Attributes
	public static final String TCRY_JAVA_METHODE_KEY = "tcryJavaMethode";
	public static final String TCRY_LIBELLE_KEY = "tcryLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeCryptage.class);

  public EOTypeCryptage localInstanceIn(EOEditingContext editingContext) {
    EOTypeCryptage localInstance = (EOTypeCryptage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tcryJavaMethode() {
    return (String) storedValueForKey("tcryJavaMethode");
  }

  public void setTcryJavaMethode(String value) {
    if (_EOTypeCryptage.LOG.isDebugEnabled()) {
    	_EOTypeCryptage.LOG.debug( "updating tcryJavaMethode from " + tcryJavaMethode() + " to " + value);
    }
    takeStoredValueForKey(value, "tcryJavaMethode");
  }

  public String tcryLibelle() {
    return (String) storedValueForKey("tcryLibelle");
  }

  public void setTcryLibelle(String value) {
    if (_EOTypeCryptage.LOG.isDebugEnabled()) {
    	_EOTypeCryptage.LOG.debug( "updating tcryLibelle from " + tcryLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tcryLibelle");
  }


  public static EOTypeCryptage createTypeCryptage(EOEditingContext editingContext, String tcryJavaMethode
, String tcryLibelle
) {
    EOTypeCryptage eo = (EOTypeCryptage) EOUtilities.createAndInsertInstance(editingContext, _EOTypeCryptage.ENTITY_NAME);    
		eo.setTcryJavaMethode(tcryJavaMethode);
		eo.setTcryLibelle(tcryLibelle);
    return eo;
  }

  public static NSArray<EOTypeCryptage> fetchAllTypeCryptages(EOEditingContext editingContext) {
    return _EOTypeCryptage.fetchAllTypeCryptages(editingContext, null);
  }

  public static NSArray<EOTypeCryptage> fetchAllTypeCryptages(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeCryptage.fetchTypeCryptages(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeCryptage> fetchTypeCryptages(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeCryptage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeCryptage> eoObjects = (NSArray<EOTypeCryptage>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeCryptage fetchTypeCryptage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCryptage.fetchTypeCryptage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCryptage fetchTypeCryptage(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeCryptage> eoObjects = _EOTypeCryptage.fetchTypeCryptages(editingContext, qualifier, null);
    EOTypeCryptage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeCryptage)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeCryptage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCryptage fetchRequiredTypeCryptage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeCryptage.fetchRequiredTypeCryptage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeCryptage fetchRequiredTypeCryptage(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeCryptage eoObject = _EOTypeCryptage.fetchTypeCryptage(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeCryptage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeCryptage localInstanceIn(EOEditingContext editingContext, EOTypeCryptage eo) {
    EOTypeCryptage localInstance = (eo == null) ? null : (EOTypeCryptage)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
