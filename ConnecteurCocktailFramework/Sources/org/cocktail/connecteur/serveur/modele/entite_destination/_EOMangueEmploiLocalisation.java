// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueEmploiLocalisation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueEmploiLocalisation extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueEmploiLocalisation";

	// Attributes
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEBUT_KEY = "dDebut";
	public static final String D_FIN_KEY = "dFin";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_EMPLOI_KEY = "idEmploi";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String QUOTITE_KEY = "quotite";

	// Relationships
	public static final String TO_MANGUE_EMPLOI_KEY = "toMangueEmploi";
	public static final String TO_STRUCTURE_KEY = "toStructure";

  private static Logger LOG = Logger.getLogger(_EOMangueEmploiLocalisation.class);

  public EOMangueEmploiLocalisation localInstanceIn(EOEditingContext editingContext) {
    EOMangueEmploiLocalisation localInstance = (EOMangueEmploiLocalisation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebut() {
    return (NSTimestamp) storedValueForKey("dDebut");
  }

  public void setDDebut(NSTimestamp value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating dDebut from " + dDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebut");
  }

  public NSTimestamp dFin() {
    return (NSTimestamp) storedValueForKey("dFin");
  }

  public void setDFin(NSTimestamp value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating dFin from " + dFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dFin");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer idEmploi() {
    return (Integer) storedValueForKey("idEmploi");
  }

  public void setIdEmploi(Integer value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating idEmploi from " + idEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "idEmploi");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public Integer quotite() {
    return (Integer) storedValueForKey("quotite");
  }

  public void setQuotite(Integer value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
    	_EOMangueEmploiLocalisation.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi toMangueEmploi() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi)storedValueForKey("toMangueEmploi");
  }

  public void setToMangueEmploiRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiLocalisation.LOG.debug("updating toMangueEmploi from " + toMangueEmploi() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueEmploi oldValue = toMangueEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueEmploi");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueEmploi");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure toStructure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOMangueEmploiLocalisation.LOG.isDebugEnabled()) {
      _EOMangueEmploiLocalisation.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  

  public static EOMangueEmploiLocalisation createMangueEmploiLocalisation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebut
, NSTimestamp dModification
) {
    EOMangueEmploiLocalisation eo = (EOMangueEmploiLocalisation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueEmploiLocalisation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebut(dDebut);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOMangueEmploiLocalisation> fetchAllMangueEmploiLocalisations(EOEditingContext editingContext) {
    return _EOMangueEmploiLocalisation.fetchAllMangueEmploiLocalisations(editingContext, null);
  }

  public static NSArray<EOMangueEmploiLocalisation> fetchAllMangueEmploiLocalisations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueEmploiLocalisation.fetchMangueEmploiLocalisations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueEmploiLocalisation> fetchMangueEmploiLocalisations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueEmploiLocalisation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueEmploiLocalisation> eoObjects = (NSArray<EOMangueEmploiLocalisation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueEmploiLocalisation fetchMangueEmploiLocalisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiLocalisation.fetchMangueEmploiLocalisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiLocalisation fetchMangueEmploiLocalisation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueEmploiLocalisation> eoObjects = _EOMangueEmploiLocalisation.fetchMangueEmploiLocalisations(editingContext, qualifier, null);
    EOMangueEmploiLocalisation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueEmploiLocalisation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueEmploiLocalisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiLocalisation fetchRequiredMangueEmploiLocalisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploiLocalisation.fetchRequiredMangueEmploiLocalisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploiLocalisation fetchRequiredMangueEmploiLocalisation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueEmploiLocalisation eoObject = _EOMangueEmploiLocalisation.fetchMangueEmploiLocalisation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueEmploiLocalisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploiLocalisation localInstanceIn(EOEditingContext editingContext, EOMangueEmploiLocalisation eo) {
    EOMangueEmploiLocalisation localInstance = (eo == null) ? null : (EOMangueEmploiLocalisation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
