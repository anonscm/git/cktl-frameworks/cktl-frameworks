// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueRepriseTempsPlein.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueRepriseTempsPlein extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueRepriseTempsPlein";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_REPRISE_TEMPS_PLEIN_KEY = "dRepriseTempsPlein";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NO_SEQ_ARR_ANNUL_KEY = "noSeqArrAnnul";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ARRETE_ANNULATION_KEY = "arreteAnnulation";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TEMPS_PARTIEL_KEY = "tempsPartiel";

  private static Logger LOG = Logger.getLogger(_EOMangueRepriseTempsPlein.class);

  public EOMangueRepriseTempsPlein localInstanceIn(EOEditingContext editingContext) {
    EOMangueRepriseTempsPlein localInstance = (EOMangueRepriseTempsPlein)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dRepriseTempsPlein() {
    return (NSTimestamp) storedValueForKey("dRepriseTempsPlein");
  }

  public void setDRepriseTempsPlein(NSTimestamp value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating dRepriseTempsPlein from " + dRepriseTempsPlein() + " to " + value);
    }
    takeStoredValueForKey(value, "dRepriseTempsPlein");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public Integer noSeqArrAnnul() {
    return (Integer) storedValueForKey("noSeqArrAnnul");
  }

  public void setNoSeqArrAnnul(Integer value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating noSeqArrAnnul from " + noSeqArrAnnul() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqArrAnnul");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
    	_EOMangueRepriseTempsPlein.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein arreteAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein)storedValueForKey("arreteAnnulation");
  }

  public void setArreteAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
      _EOMangueRepriseTempsPlein.LOG.debug("updating arreteAnnulation from " + arreteAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueRepriseTempsPlein oldValue = arreteAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "arreteAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "arreteAnnulation");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
      _EOMangueRepriseTempsPlein.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel tempsPartiel() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel)storedValueForKey("tempsPartiel");
  }

  public void setTempsPartielRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel value) {
    if (_EOMangueRepriseTempsPlein.LOG.isDebugEnabled()) {
      _EOMangueRepriseTempsPlein.LOG.debug("updating tempsPartiel from " + tempsPartiel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel oldValue = tempsPartiel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "tempsPartiel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "tempsPartiel");
    }
  }
  

  public static EOMangueRepriseTempsPlein createMangueRepriseTempsPlein(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp dRepriseTempsPlein
, String temConfirme
, String temGestEtab
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueTempsPartiel tempsPartiel) {
    EOMangueRepriseTempsPlein eo = (EOMangueRepriseTempsPlein) EOUtilities.createAndInsertInstance(editingContext, _EOMangueRepriseTempsPlein.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setDRepriseTempsPlein(dRepriseTempsPlein);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setTempsPartielRelationship(tempsPartiel);
    return eo;
  }

  public static NSArray<EOMangueRepriseTempsPlein> fetchAllMangueRepriseTempsPleins(EOEditingContext editingContext) {
    return _EOMangueRepriseTempsPlein.fetchAllMangueRepriseTempsPleins(editingContext, null);
  }

  public static NSArray<EOMangueRepriseTempsPlein> fetchAllMangueRepriseTempsPleins(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueRepriseTempsPlein.fetchMangueRepriseTempsPleins(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueRepriseTempsPlein> fetchMangueRepriseTempsPleins(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueRepriseTempsPlein.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueRepriseTempsPlein> eoObjects = (NSArray<EOMangueRepriseTempsPlein>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueRepriseTempsPlein fetchMangueRepriseTempsPlein(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueRepriseTempsPlein.fetchMangueRepriseTempsPlein(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueRepriseTempsPlein fetchMangueRepriseTempsPlein(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueRepriseTempsPlein> eoObjects = _EOMangueRepriseTempsPlein.fetchMangueRepriseTempsPleins(editingContext, qualifier, null);
    EOMangueRepriseTempsPlein eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueRepriseTempsPlein)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueRepriseTempsPlein that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueRepriseTempsPlein fetchRequiredMangueRepriseTempsPlein(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueRepriseTempsPlein.fetchRequiredMangueRepriseTempsPlein(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueRepriseTempsPlein fetchRequiredMangueRepriseTempsPlein(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueRepriseTempsPlein eoObject = _EOMangueRepriseTempsPlein.fetchMangueRepriseTempsPlein(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueRepriseTempsPlein that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueRepriseTempsPlein localInstanceIn(EOEditingContext editingContext, EOMangueRepriseTempsPlein eo) {
    EOMangueRepriseTempsPlein localInstance = (eo == null) ? null : (EOMangueRepriseTempsPlein)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
