// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCertificatMedical.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCertificatMedical extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MangueCertificatMedical";

	// Attributes
	public static final String CMED_ARRET_KEY = "cmedArret";
	public static final String CMED_ARRET_DATE_KEY = "cmedArretDate";
	public static final String CMED_ATI_KEY = "cmedAti";
	public static final String CMED_CONCLUSION_DATE_KEY = "cmedConclusionDate";
	public static final String CMED_CONSTATATION_KEY = "cmedConstatation";
	public static final String CMED_DATE_KEY = "cmedDate";
	public static final String CMED_IPP_KEY = "cmedIpp";
	public static final String CMED_REPRISE_KEY = "cmedReprise";
	public static final String CMED_REPRISE_DATE_KEY = "cmedRepriseDate";
	public static final String CMED_SOINS_KEY = "cmedSoins";
	public static final String CMED_SOINS_DATE_KEY = "cmedSoinsDate";
	public static final String CMED_SORTIE_KEY = "cmedSortie";
	public static final String CMED_SORTIE_DEBUT_KEY = "cmedSortieDebut";
	public static final String CMED_SORTIE_FIN_KEY = "cmedSortieFin";
	public static final String CMED_TYPE_KEY = "cmedType";
	public static final String DACC_ORDRE_KEY = "daccOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String CONCLUSION_MEDICALE_KEY = "conclusionMedicale";
	public static final String TO_MANGUE_DECLARATION_ACCIDENT_KEY = "toMangueDeclarationAccident";

  private static Logger LOG = Logger.getLogger(_EOMangueCertificatMedical.class);

  public EOMangueCertificatMedical localInstanceIn(EOEditingContext editingContext) {
    EOMangueCertificatMedical localInstance = (EOMangueCertificatMedical)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cmedArret() {
    return (String) storedValueForKey("cmedArret");
  }

  public void setCmedArret(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedArret from " + cmedArret() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedArret");
  }

  public NSTimestamp cmedArretDate() {
    return (NSTimestamp) storedValueForKey("cmedArretDate");
  }

  public void setCmedArretDate(NSTimestamp value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedArretDate from " + cmedArretDate() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedArretDate");
  }

  public String cmedAti() {
    return (String) storedValueForKey("cmedAti");
  }

  public void setCmedAti(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedAti from " + cmedAti() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedAti");
  }

  public NSTimestamp cmedConclusionDate() {
    return (NSTimestamp) storedValueForKey("cmedConclusionDate");
  }

  public void setCmedConclusionDate(NSTimestamp value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedConclusionDate from " + cmedConclusionDate() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedConclusionDate");
  }

  public String cmedConstatation() {
    return (String) storedValueForKey("cmedConstatation");
  }

  public void setCmedConstatation(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedConstatation from " + cmedConstatation() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedConstatation");
  }

  public NSTimestamp cmedDate() {
    return (NSTimestamp) storedValueForKey("cmedDate");
  }

  public void setCmedDate(NSTimestamp value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedDate from " + cmedDate() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedDate");
  }

  public java.math.BigDecimal cmedIpp() {
    return (java.math.BigDecimal) storedValueForKey("cmedIpp");
  }

  public void setCmedIpp(java.math.BigDecimal value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedIpp from " + cmedIpp() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedIpp");
  }

  public String cmedReprise() {
    return (String) storedValueForKey("cmedReprise");
  }

  public void setCmedReprise(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedReprise from " + cmedReprise() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedReprise");
  }

  public NSTimestamp cmedRepriseDate() {
    return (NSTimestamp) storedValueForKey("cmedRepriseDate");
  }

  public void setCmedRepriseDate(NSTimestamp value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedRepriseDate from " + cmedRepriseDate() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedRepriseDate");
  }

  public String cmedSoins() {
    return (String) storedValueForKey("cmedSoins");
  }

  public void setCmedSoins(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedSoins from " + cmedSoins() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedSoins");
  }

  public NSTimestamp cmedSoinsDate() {
    return (NSTimestamp) storedValueForKey("cmedSoinsDate");
  }

  public void setCmedSoinsDate(NSTimestamp value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedSoinsDate from " + cmedSoinsDate() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedSoinsDate");
  }

  public String cmedSortie() {
    return (String) storedValueForKey("cmedSortie");
  }

  public void setCmedSortie(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedSortie from " + cmedSortie() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedSortie");
  }

  public String cmedSortieDebut() {
    return (String) storedValueForKey("cmedSortieDebut");
  }

  public void setCmedSortieDebut(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedSortieDebut from " + cmedSortieDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedSortieDebut");
  }

  public String cmedSortieFin() {
    return (String) storedValueForKey("cmedSortieFin");
  }

  public void setCmedSortieFin(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedSortieFin from " + cmedSortieFin() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedSortieFin");
  }

  public String cmedType() {
    return (String) storedValueForKey("cmedType");
  }

  public void setCmedType(String value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating cmedType from " + cmedType() + " to " + value);
    }
    takeStoredValueForKey(value, "cmedType");
  }

  public Integer daccOrdre() {
    return (Integer) storedValueForKey("daccOrdre");
  }

  public void setDaccOrdre(Integer value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating daccOrdre from " + daccOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "daccOrdre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
    	_EOMangueCertificatMedical.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConclusionMedicale conclusionMedicale() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConclusionMedicale)storedValueForKey("conclusionMedicale");
  }

  public void setConclusionMedicaleRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConclusionMedicale value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
      _EOMangueCertificatMedical.LOG.debug("updating conclusionMedicale from " + conclusionMedicale() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOConclusionMedicale oldValue = conclusionMedicale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "conclusionMedicale");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "conclusionMedicale");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident toMangueDeclarationAccident() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident)storedValueForKey("toMangueDeclarationAccident");
  }

  public void setToMangueDeclarationAccidentRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident value) {
    if (_EOMangueCertificatMedical.LOG.isDebugEnabled()) {
      _EOMangueCertificatMedical.LOG.debug("updating toMangueDeclarationAccident from " + toMangueDeclarationAccident() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident oldValue = toMangueDeclarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMangueDeclarationAccident");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMangueDeclarationAccident");
    }
  }
  

  public static EOMangueCertificatMedical createMangueCertificatMedical(EOEditingContext editingContext, String cmedType
, Integer daccOrdre
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueDeclarationAccident toMangueDeclarationAccident) {
    EOMangueCertificatMedical eo = (EOMangueCertificatMedical) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCertificatMedical.ENTITY_NAME);    
		eo.setCmedType(cmedType);
		eo.setDaccOrdre(daccOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToMangueDeclarationAccidentRelationship(toMangueDeclarationAccident);
    return eo;
  }

  public static NSArray<EOMangueCertificatMedical> fetchAllMangueCertificatMedicals(EOEditingContext editingContext) {
    return _EOMangueCertificatMedical.fetchAllMangueCertificatMedicals(editingContext, null);
  }

  public static NSArray<EOMangueCertificatMedical> fetchAllMangueCertificatMedicals(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCertificatMedical.fetchMangueCertificatMedicals(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCertificatMedical> fetchMangueCertificatMedicals(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCertificatMedical.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCertificatMedical> eoObjects = (NSArray<EOMangueCertificatMedical>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCertificatMedical fetchMangueCertificatMedical(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCertificatMedical.fetchMangueCertificatMedical(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCertificatMedical fetchMangueCertificatMedical(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCertificatMedical> eoObjects = _EOMangueCertificatMedical.fetchMangueCertificatMedicals(editingContext, qualifier, null);
    EOMangueCertificatMedical eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCertificatMedical)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCertificatMedical that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCertificatMedical fetchRequiredMangueCertificatMedical(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCertificatMedical.fetchRequiredMangueCertificatMedical(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCertificatMedical fetchRequiredMangueCertificatMedical(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCertificatMedical eoObject = _EOMangueCertificatMedical.fetchMangueCertificatMedical(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCertificatMedical that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCertificatMedical localInstanceIn(EOEditingContext editingContext, EOMangueCertificatMedical eo) {
    EOMangueCertificatMedical localInstance = (eo == null) ? null : (EOMangueCertificatMedical)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
