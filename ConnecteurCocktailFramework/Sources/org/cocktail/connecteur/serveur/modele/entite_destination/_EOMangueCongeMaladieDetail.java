// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCongeMaladieDetail.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCongeMaladieDetail extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueCongeMaladieDetail";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TAUX_KEY = "taux";

	// Relationships
	public static final String CONGE_MALADIE_KEY = "congeMaladie";

  private static Logger LOG = Logger.getLogger(_EOMangueCongeMaladieDetail.class);

  public EOMangueCongeMaladieDetail localInstanceIn(EOEditingContext editingContext) {
    EOMangueCongeMaladieDetail localInstance = (EOMangueCongeMaladieDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieDetail.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieDetail.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer taux() {
    return (Integer) storedValueForKey("taux");
  }

  public void setTaux(Integer value) {
    if (_EOMangueCongeMaladieDetail.LOG.isDebugEnabled()) {
    	_EOMangueCongeMaladieDetail.LOG.debug( "updating taux from " + taux() + " to " + value);
    }
    takeStoredValueForKey(value, "taux");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie congeMaladie() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie)storedValueForKey("congeMaladie");
  }

  public void setCongeMaladieRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie value) {
    if (_EOMangueCongeMaladieDetail.LOG.isDebugEnabled()) {
      _EOMangueCongeMaladieDetail.LOG.debug("updating congeMaladie from " + congeMaladie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie oldValue = congeMaladie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "congeMaladie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "congeMaladie");
    }
  }
  

  public static EOMangueCongeMaladieDetail createMangueCongeMaladieDetail(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer taux
, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCongeMaladie congeMaladie) {
    EOMangueCongeMaladieDetail eo = (EOMangueCongeMaladieDetail) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCongeMaladieDetail.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTaux(taux);
    eo.setCongeMaladieRelationship(congeMaladie);
    return eo;
  }

  public static NSArray<EOMangueCongeMaladieDetail> fetchAllMangueCongeMaladieDetails(EOEditingContext editingContext) {
    return _EOMangueCongeMaladieDetail.fetchAllMangueCongeMaladieDetails(editingContext, null);
  }

  public static NSArray<EOMangueCongeMaladieDetail> fetchAllMangueCongeMaladieDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCongeMaladieDetail.fetchMangueCongeMaladieDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCongeMaladieDetail> fetchMangueCongeMaladieDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCongeMaladieDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCongeMaladieDetail> eoObjects = (NSArray<EOMangueCongeMaladieDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCongeMaladieDetail fetchMangueCongeMaladieDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeMaladieDetail.fetchMangueCongeMaladieDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeMaladieDetail fetchMangueCongeMaladieDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCongeMaladieDetail> eoObjects = _EOMangueCongeMaladieDetail.fetchMangueCongeMaladieDetails(editingContext, qualifier, null);
    EOMangueCongeMaladieDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCongeMaladieDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCongeMaladieDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeMaladieDetail fetchRequiredMangueCongeMaladieDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCongeMaladieDetail.fetchRequiredMangueCongeMaladieDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCongeMaladieDetail fetchRequiredMangueCongeMaladieDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCongeMaladieDetail eoObject = _EOMangueCongeMaladieDetail.fetchMangueCongeMaladieDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCongeMaladieDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCongeMaladieDetail localInstanceIn(EOEditingContext editingContext, EOMangueCongeMaladieDetail eo) {
    EOMangueCongeMaladieDetail localInstance = (eo == null) ? null : (EOMangueCongeMaladieDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
