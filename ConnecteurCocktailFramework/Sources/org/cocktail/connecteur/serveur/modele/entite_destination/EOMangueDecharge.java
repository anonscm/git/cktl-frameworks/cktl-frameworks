package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EODecharge;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueDecharge extends _EOMangueDecharge {


	private static Logger log = Logger.getLogger(EOMangueDecharge.class);

	//Méthodes ajoutées
	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), ((EODecharge)recordImport).idSource());
		if (individu != null) {	// Ne devrait pas car on crée le individus avant
			addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + ((EODecharge)recordImport).idSource());
		}
	}

}
