//EOMangueAffectation.java
//Created on Wed Jul 25 15:07:18 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOAffectation;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EORepartStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// 09/03/2011 - Ajout de persIdCreation et persIdModification
public class EOMangueAffectation extends _EOMangueAffectation implements InterfaceGestionIdUtilisateur {

	public EOMangueAffectation() {
		super();
	}

	protected void initialiserObjet(ObjetImport record) throws Exception {
		setDenQuotAffectation(new Integer(100));
		EOAffectation affectationImport = (EOAffectation) record;
		EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), affectationImport.strSource());
		if (structure != null) { // Ne devrait pas car on crée le structures
									// avant
			addObjectToBothSidesOfRelationshipWithKey(structure, "structure");
		} else {
			throw new Exception("Pas de structure dans le SI correspondant a la structure " + affectationImport.strSource());
		}
		EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), affectationImport.idSource());
		if (individu != null) { // Ne devrait pas car on crée le individus avant
			addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + affectationImport.idSource());
		}
		// Ajouter la repart structure si elle n'est pas encore créée
		if (EORepartStructure.estIndividuDansStructure(editingContext(), individu, structure) == false) {
			// créer la repartStructure
			EORepartStructure repartStructure = new EORepartStructure();
			repartStructure.init(individu, structure);
			editingContext().insertObject(repartStructure);
		}

		EOMangueContrat contratMangue = null;
		if (affectationImport.contrat() != null && affectationImport.contrat().correspondance() != null)
			contratMangue = ((EOContratCorresp) affectationImport.contrat().correspondance()).contratMangue();
		setContratRelationship(contratMangue);

		EOMangueCarriere carriereMangue = null;
		if (affectationImport.toCarriere() != null && affectationImport.toCarriere().correspondance() != null)
			carriereMangue = ((EOCarriereCorresp) affectationImport.toCarriere().correspondance()).carriereMangue();
		setCarriereRelationship(carriereMangue);
	}

	// Méthodes statiques
	/**
	 * Retourne l'affectation equivalente pour cet individu, cette structure et
	 * qui commence a la m&ecirc;me date. Cette affectation doit &ecirc;tre
	 * valide<BR>
	 * Retourne l'affectation si il existe une seule sinon null
	 * 
	 * @param editingContext
	 * @param affectation
	 * @return EOMangueAffectation
	 */
	public static EOMangueAffectation affectationDestinationPourAffectation(EOEditingContext editingContext, EOAffectation affectation) {
		// Rechercher les affectations valides : pour une structure donnée et
		// une période donnée, il ne peut y en avoir qu'une
		// Ce n'est donc pas la peine de vérifier la correspondance
		NSArray affectations = rechercherMangueAffectationsPourAffectation(editingContext, affectation, true);
		if (affectations != null && affectations.count() == 1) {
			return (EOMangueAffectation) affectations.objectAtIndex(0);
		} else {
			return null;
		}
	}

	/**
	 * retourne les affectations pour l'individu de l'affectation et la
	 * m&ecirc;rme periode.<BR>
	 * 
	 * @param editingContext
	 * @param affectation
	 * @return NSArray
	 */
	public static NSArray rechercherAffectationsValidesPourIndividuEtPeriode(EOEditingContext editingContext, EOAffectation affectation) {
		// Vérifier si il existe un individu équivalent dans le SI Destinataire.
		// On ne se base pas
		// sur les correspondances car l'individu n'a peut-être pas encore été
		// créé
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) affectation.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND temValide = 'O'", new NSArray(individuGrhum));
			NSMutableArray qualifiers = new NSMutableArray(qualifier);
			qualifiers
					.addObject(Finder.qualifierPourPeriode("dDebAffectation", affectation.dDebAffectation(), "dFinAffectation", affectation.dFinAffectation()));
			qualifier = new EOAndQualifier(qualifiers);
			EOFetchSpecification myFetch = new EOFetchSpecification("MangueAffectation", qualifier, null);
			myFetch.setRefreshesRefetchedObjects(true);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else { // Individu pas encore créé
			return null;
		}
	}

	public static NSArray rechercherAffectationsValidesPourIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp dateDebut,
			NSTimestamp dateFin) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND temValide = 'O'", new NSArray(individu));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifiers.addObject(Finder.qualifierPourPeriode("dDebAffectation", dateDebut, "dFinAffectation", dateFin));
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification("MangueAffectation", qualifier, null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * recherche les affectations d'un individu commen&ccedil;ant apr&egrave;s
	 * la date fournie en param&egrave;tre
	 */
	public static NSArray affectationsPosterieuresADate(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp date) {
		NSMutableArray args = new NSMutableArray(individu.noIndividu());
		args.addObject(date);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("individu.noIndividu = %@ AND temValide = 'O' AND dDebAffectation > %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification("MangueAffectation", myQualifier, null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * ferme les affectations d'un agent valides a la date passee en
	 * param&egrave;tre
	 */
	public static void fermerAffectations(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp date) throws Exception {
		NSArray objets = rechercherAffectationsValidesPourIndividuEtPeriode(editingContext, individu, date, date);
		java.util.Enumeration e = objets.objectEnumerator();
		while (e.hasMoreElements()) {
			// fermer l'affectation
			EOMangueAffectation affectation = (EOMangueAffectation) e.nextElement();
			affectation.setDFinAffectation(date);
		}
		// supprimer les affectations postérieures à cette date
		objets = affectationsPosterieuresADate(editingContext, individu, date);
		e = objets.objectEnumerator();
		while (e.hasMoreElements()) {
			EOMangueAffectation affectation = (EOMangueAffectation) e.nextElement();
			affectation.setTemValide(CocktailConstantes.FAUX);
		}
	}

	// Méthodes privées
	private static NSArray rechercherMangueAffectationsPourAffectation(EOEditingContext editingContext, EOAffectation affectation, boolean validesUniquement) {
		// Vérifier si il existe une affectation équivalente dans le SI
		// Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu n'a peut-être pas encore été
		// créé
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) affectation.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			// Idem pour les structures
			EOGrhumStructure structureGrhum = (EOGrhumStructure) affectation.structure().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (structureGrhum != null) {
				NSMutableArray args = new NSMutableArray(individuGrhum);
				args.addObject(structureGrhum);
				// Vérifier que la date de début d'affectation est la même :
				// pour éviter les problèmes de comparaison
				// avec les heures, on vérifie qu'elle est entre le jour
				// précédent et le jour suivant
				args.addObject(DateCtrl.jourPrecedent(affectation.dDebAffectation()));
				args.addObject(DateCtrl.jourSuivant(affectation.dDebAffectation()));
				String qualifier = "individu = %@ AND structure = %@ AND dDebAffectation > %@ AND dDebAffectation < %@";
				if (validesUniquement) {
					qualifier = qualifier + " AND temValide = 'O'";
				}
				EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier, args);
				EOFetchSpecification myFetch = new EOFetchSpecification("MangueAffectation", myQualifier, null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			}
		}
		return null;
	}
}
