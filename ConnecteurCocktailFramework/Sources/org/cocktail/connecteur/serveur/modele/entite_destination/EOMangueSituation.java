package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.serveur.modele.correspondance.EOAdresseCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOSituation;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

public class EOMangueSituation extends _EOMangueSituation {
	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);

		EOSituation situationImport = (EOSituation) recordImport;
		setDenQuotite(100);

		EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), situationImport.strSource());
		setStructureRelationship(structure);
		if (structure == null)
			throw new Exception("Pas de structure dans le SI correspondant a la structure " + situationImport.strSource());

		if (situationImport.adrSource() != null) {
			EOGrhumAdresse adresse = EOAdresseCorresp.adresseGrhum(editingContext(), situationImport.adrSource());
			setAdresseRelationship(adresse);
			if (adresse == null)
				throw new Exception("Pas d'adresse dans le SI correspondant a l'adresse " + situationImport.adrSource());
		}

	}
}
