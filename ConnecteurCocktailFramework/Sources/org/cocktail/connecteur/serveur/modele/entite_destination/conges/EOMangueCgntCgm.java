// EOCgntCgm.java
// Created on Tue Feb 14 09:17:51 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;


import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCgntCgmCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.foundation.NSTimestamp;

/** Le cong&eacue; de grave maladie ne concerne que les agents contractuels ou les contractuels assimiles qui ont une carri&egrave;re (TEM_fonctionnaire = N dans type_population).<BR>
 * L'agent doit avoir un contrat de travail ou une carri&egrave;re de non fonctionnaire en activite ou detachement avec+carri&egrave;re 
 * d'accueil pendant tout le conge.<BR>
 * La duree maximale du conge est de 6 mois<BR>
 * */
public class EOMangueCgntCgm extends _EOMangueCgntCgm {

	public EOMangueCgntCgm() {
		super();
	}

	 public String temEnCause() {
		 return (String)storedValueForKey("temEnCause");
	 }

	 public void setTemEnCause(String value) {
		 takeStoredValueForKey(value, "temEnCause");
	 }
	 // méthodes ajoutées
	 public String typeEvenement() {
		 return "CGM";
	 }
	 /** pas de date de commission */
	 public NSTimestamp dateCommission() {
		 return null;
	 }
	 // Méthodes protégées
	 protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		 super.initialiserObjet(recordImport);
		 setTemEnCause(CocktailConstantes.FAUX);
	 }

	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCgntCgm)value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCgntCgmCorresp congeCorresp = EOCgntCgmCorresp.fetchCgntCgmCorresp(editingContext(), EOCgntCgmCorresp.CGNT_CGM_KEY, congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.cgntCgmMangue();
	}
}
