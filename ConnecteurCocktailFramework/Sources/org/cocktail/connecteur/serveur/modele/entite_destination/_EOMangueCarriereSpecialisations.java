// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCarriereSpecialisations.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCarriereSpecialisations extends ObjetPourSIDestinataireAvecCarriere {
	public static final String ENTITY_NAME = "MangueCarriereSpecialisations";

	// Attributes
	public static final String C_BAP_KEY = "cBap";
	public static final String C_DISC_SD_DEGRE_KEY = "cDiscSdDegre";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String C_SPECIALITE_KEY = "cSpecialite";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String SPEC_DEBUT_KEY = "specDebut";
	public static final String SPEC_FIN_KEY = "specFin";
	public static final String SPEC_ID_KEY = "specId";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";

  private static Logger LOG = Logger.getLogger(_EOMangueCarriereSpecialisations.class);

  public EOMangueCarriereSpecialisations localInstanceIn(EOEditingContext editingContext) {
    EOMangueCarriereSpecialisations localInstance = (EOMangueCarriereSpecialisations)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public String cDiscSdDegre() {
    return (String) storedValueForKey("cDiscSdDegre");
  }

  public void setCDiscSdDegre(String value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating cDiscSdDegre from " + cDiscSdDegre() + " to " + value);
    }
    takeStoredValueForKey(value, "cDiscSdDegre");
  }

  public String codeemploi() {
    return (String) storedValueForKey("codeemploi");
  }

  public void setCodeemploi(String value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating codeemploi from " + codeemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeemploi");
  }

  public String cSpecialite() {
    return (String) storedValueForKey("cSpecialite");
  }

  public void setCSpecialite(String value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating cSpecialite from " + cSpecialite() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialite");
  }

  public String cSpecialiteAtos() {
    return (String) storedValueForKey("cSpecialiteAtos");
  }

  public void setCSpecialiteAtos(String value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating cSpecialiteAtos from " + cSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteAtos");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey("noCnu");
  }

  public void setNoCnu(Integer value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, "noCnu");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public NSTimestamp specDebut() {
    return (NSTimestamp) storedValueForKey("specDebut");
  }

  public void setSpecDebut(NSTimestamp value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating specDebut from " + specDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "specDebut");
  }

  public NSTimestamp specFin() {
    return (NSTimestamp) storedValueForKey("specFin");
  }

  public void setSpecFin(NSTimestamp value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating specFin from " + specFin() + " to " + value);
    }
    takeStoredValueForKey(value, "specFin");
  }

  public Integer specId() {
    return (Integer) storedValueForKey("specId");
  }

  public void setSpecId(Integer value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOMangueCarriereSpecialisations.LOG.debug( "updating specId from " + specId() + " to " + value);
    }
    takeStoredValueForKey(value, "specId");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOMangueCarriereSpecialisations.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere toCarriere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere value) {
    if (_EOMangueCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOMangueCarriereSpecialisations.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  

  public static EOMangueCarriereSpecialisations createMangueCarriereSpecialisations(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, NSTimestamp specDebut
, Integer specId
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueCarriere toCarriere) {
    EOMangueCarriereSpecialisations eo = (EOMangueCarriereSpecialisations) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCarriereSpecialisations.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setSpecDebut(specDebut);
		eo.setSpecId(specId);
    eo.setIndividuRelationship(individu);
    eo.setToCarriereRelationship(toCarriere);
    return eo;
  }

  public static NSArray<EOMangueCarriereSpecialisations> fetchAllMangueCarriereSpecialisationses(EOEditingContext editingContext) {
    return _EOMangueCarriereSpecialisations.fetchAllMangueCarriereSpecialisationses(editingContext, null);
  }

  public static NSArray<EOMangueCarriereSpecialisations> fetchAllMangueCarriereSpecialisationses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCarriereSpecialisations.fetchMangueCarriereSpecialisationses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCarriereSpecialisations> fetchMangueCarriereSpecialisationses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCarriereSpecialisations.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCarriereSpecialisations> eoObjects = (NSArray<EOMangueCarriereSpecialisations>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCarriereSpecialisations fetchMangueCarriereSpecialisations(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCarriereSpecialisations.fetchMangueCarriereSpecialisations(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCarriereSpecialisations fetchMangueCarriereSpecialisations(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCarriereSpecialisations> eoObjects = _EOMangueCarriereSpecialisations.fetchMangueCarriereSpecialisationses(editingContext, qualifier, null);
    EOMangueCarriereSpecialisations eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCarriereSpecialisations)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCarriereSpecialisations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCarriereSpecialisations fetchRequiredMangueCarriereSpecialisations(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCarriereSpecialisations.fetchRequiredMangueCarriereSpecialisations(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCarriereSpecialisations fetchRequiredMangueCarriereSpecialisations(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCarriereSpecialisations eoObject = _EOMangueCarriereSpecialisations.fetchMangueCarriereSpecialisations(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCarriereSpecialisations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCarriereSpecialisations localInstanceIn(EOEditingContext editingContext, EOMangueCarriereSpecialisations eo) {
    EOMangueCarriereSpecialisations localInstance = (eo == null) ? null : (EOMangueCarriereSpecialisations)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
