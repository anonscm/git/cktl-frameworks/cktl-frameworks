// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueEmploi.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueEmploi extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueEmploi";

	// Attributes
	public static final String C_ARTICLE_KEY = "cArticle";
	public static final String C_CHAPITRE_KEY = "cChapitre";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_PROGRAMME_KEY = "cProgramme";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TITRE_KEY = "cTitre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_EFFET_EMPLOI_KEY = "dEffetEmploi";
	public static final String D_FERMETURE_EMPLOI_KEY = "dFermetureEmploi";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_PUBLICATION_EMPLOI_KEY = "dPublicationEmploi";
	public static final String ID_EMPLOI_KEY = "idEmploi";
	public static final String NO_EMPLOI_KEY = "noEmploi";
	public static final String NO_EMPLOI_FORMATTE_KEY = "noEmploiFormatte";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String QUOTITE_KEY = "quotite";
	public static final String TEM_ARBITRAGE_KEY = "temArbitrage";
	public static final String TEM_CONCOURS_KEY = "temConcours";
	public static final String TEM_CONTRACTUEL_KEY = "temContractuel";
	public static final String TEM_DURABILITE_KEY = "temDurabilite";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_NATIONAL_KEY = "temNational";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOMangueEmploi.class);

  public EOMangueEmploi localInstanceIn(EOEditingContext editingContext) {
    EOMangueEmploi localInstance = (EOMangueEmploi)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cArticle() {
    return (Integer) storedValueForKey("cArticle");
  }

  public void setCArticle(Integer value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating cArticle from " + cArticle() + " to " + value);
    }
    takeStoredValueForKey(value, "cArticle");
  }

  public Integer cChapitre() {
    return (Integer) storedValueForKey("cChapitre");
  }

  public void setCChapitre(Integer value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating cChapitre from " + cChapitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cChapitre");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cProgramme() {
    return (String) storedValueForKey("cProgramme");
  }

  public void setCProgramme(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating cProgramme from " + cProgramme() + " to " + value);
    }
    takeStoredValueForKey(value, "cProgramme");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cTitre() {
    return (String) storedValueForKey("cTitre");
  }

  public void setCTitre(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating cTitre from " + cTitre() + " to " + value);
    }
    takeStoredValueForKey(value, "cTitre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dEffetEmploi() {
    return (NSTimestamp) storedValueForKey("dEffetEmploi");
  }

  public void setDEffetEmploi(NSTimestamp value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating dEffetEmploi from " + dEffetEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dEffetEmploi");
  }

  public NSTimestamp dFermetureEmploi() {
    return (NSTimestamp) storedValueForKey("dFermetureEmploi");
  }

  public void setDFermetureEmploi(NSTimestamp value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating dFermetureEmploi from " + dFermetureEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dFermetureEmploi");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dPublicationEmploi() {
    return (NSTimestamp) storedValueForKey("dPublicationEmploi");
  }

  public void setDPublicationEmploi(NSTimestamp value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating dPublicationEmploi from " + dPublicationEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dPublicationEmploi");
  }

  public Integer idEmploi() {
    return (Integer) storedValueForKey("idEmploi");
  }

  public void setIdEmploi(Integer value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating idEmploi from " + idEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "idEmploi");
  }

  public String noEmploi() {
    return (String) storedValueForKey("noEmploi");
  }

  public void setNoEmploi(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating noEmploi from " + noEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "noEmploi");
  }

  public String noEmploiFormatte() {
    return (String) storedValueForKey("noEmploiFormatte");
  }

  public void setNoEmploiFormatte(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating noEmploiFormatte from " + noEmploiFormatte() + " to " + value);
    }
    takeStoredValueForKey(value, "noEmploiFormatte");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public Integer quotite() {
    return (Integer) storedValueForKey("quotite");
  }

  public void setQuotite(Integer value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public String temArbitrage() {
    return (String) storedValueForKey("temArbitrage");
  }

  public void setTemArbitrage(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating temArbitrage from " + temArbitrage() + " to " + value);
    }
    takeStoredValueForKey(value, "temArbitrage");
  }

  public String temConcours() {
    return (String) storedValueForKey("temConcours");
  }

  public void setTemConcours(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating temConcours from " + temConcours() + " to " + value);
    }
    takeStoredValueForKey(value, "temConcours");
  }

  public String temContractuel() {
    return (String) storedValueForKey("temContractuel");
  }

  public void setTemContractuel(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating temContractuel from " + temContractuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temContractuel");
  }

  public String temDurabilite() {
    return (String) storedValueForKey("temDurabilite");
  }

  public void setTemDurabilite(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating temDurabilite from " + temDurabilite() + " to " + value);
    }
    takeStoredValueForKey(value, "temDurabilite");
  }

  public String temEnseignant() {
    return (String) storedValueForKey("temEnseignant");
  }

  public void setTemEnseignant(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating temEnseignant from " + temEnseignant() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnseignant");
  }

  public String temNational() {
    return (String) storedValueForKey("temNational");
  }

  public void setTemNational(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating temNational from " + temNational() + " to " + value);
    }
    takeStoredValueForKey(value, "temNational");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueEmploi.LOG.isDebugEnabled()) {
    	_EOMangueEmploi.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOMangueEmploi createMangueEmploi(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dEffetEmploi
, NSTimestamp dModification
, Integer idEmploi
, String temArbitrage
, String temConcours
, String temContractuel
, String temDurabilite
, String temEnseignant
, String temValide
) {
    EOMangueEmploi eo = (EOMangueEmploi) EOUtilities.createAndInsertInstance(editingContext, _EOMangueEmploi.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDEffetEmploi(dEffetEmploi);
		eo.setDModification(dModification);
		eo.setIdEmploi(idEmploi);
		eo.setTemArbitrage(temArbitrage);
		eo.setTemConcours(temConcours);
		eo.setTemContractuel(temContractuel);
		eo.setTemDurabilite(temDurabilite);
		eo.setTemEnseignant(temEnseignant);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMangueEmploi> fetchAllMangueEmplois(EOEditingContext editingContext) {
    return _EOMangueEmploi.fetchAllMangueEmplois(editingContext, null);
  }

  public static NSArray<EOMangueEmploi> fetchAllMangueEmplois(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueEmploi.fetchMangueEmplois(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueEmploi> fetchMangueEmplois(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueEmploi.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueEmploi> eoObjects = (NSArray<EOMangueEmploi>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueEmploi fetchMangueEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploi.fetchMangueEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploi fetchMangueEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueEmploi> eoObjects = _EOMangueEmploi.fetchMangueEmplois(editingContext, qualifier, null);
    EOMangueEmploi eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueEmploi)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueEmploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploi fetchRequiredMangueEmploi(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueEmploi.fetchRequiredMangueEmploi(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueEmploi fetchRequiredMangueEmploi(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueEmploi eoObject = _EOMangueEmploi.fetchMangueEmploi(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueEmploi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueEmploi localInstanceIn(EOEditingContext editingContext, EOMangueEmploi eo) {
    EOMangueEmploi localInstance = (eo == null) ? null : (EOMangueEmploi)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
