package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.serveur.modele.correspondance.EOEmploiCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOOrgaBudgetCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOPersBudget;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;


public class EOManguePersBudget extends _EOManguePersBudget {
	@Override
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOPersBudget budgetImport = (EOPersBudget) recordImport;

		if (budgetImport.emploi() != null) {
			EOEmploiCorresp emploiCorresp = (EOEmploiCorresp) ObjetCorresp.rechercherObjetCorrespPourRecordImport(editingContext(),
					budgetImport.emploi(), true);
			if (emploiCorresp != null) {
				setToEmploiRelationship(emploiCorresp.emploiMangue());
			} else {
				throw new Exception("Pas d'emploi dans le SI correspondant a l'emploi " + budgetImport.emploi().empSource());
			}
		}
		if (budgetImport.individu() != null) {
			EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), budgetImport.individu());
			if (individu != null) {
				setToIndividuRelationship(individu);
			} else {
				throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + budgetImport.individu().idSource());
			}
		}
		
		EOJefyOrgan organ=null;
		if (budgetImport.toOrgaBudget()!=null) {
			organ = EOOrgaBudgetCorresp.jefyOrgan(editingContext(), budgetImport.toOrgaBudget());
		} else if (budgetImport.orgId()!=null) {
			organ = EOJefyOrgan.getFromId(editingContext(), budgetImport.orgId());
		} 
		else {
			throw new Exception("L'organ doit être défini par OB_SOURCE ou ORG_ID");
		}
		setToJefyOrganRelationship(organ);
			
		setToKxElementRelationship(budgetImport.toKxElement());
		setToTypeCreditRelationship(budgetImport.toTypeCredit());
	}
}
