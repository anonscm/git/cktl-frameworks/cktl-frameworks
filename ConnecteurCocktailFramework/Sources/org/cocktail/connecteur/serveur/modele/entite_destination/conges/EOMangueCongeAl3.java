package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCongeAl3Corresp;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;

import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongeAl3 extends _EOMangueCongeAl3 {
	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongeAl3) value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongeAl3Corresp congeCorresp = EOCongeAl3Corresp.fetchCongeAl3Corresp(editingContext(), EOCongeAl3Corresp.CONGE_AL3_KEY, congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.mangueCongeAl3();
	}

	@Override
	public NSTimestamp dateCommission() {
		return dComMed();
	}

	@Override
	public String typeEvenement() {
		return "CAL3";
	}
}
