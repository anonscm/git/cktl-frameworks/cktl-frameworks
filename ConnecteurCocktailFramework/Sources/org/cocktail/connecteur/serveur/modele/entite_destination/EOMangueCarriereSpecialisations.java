package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCarriereSpecialisations;
import org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class EOMangueCarriereSpecialisations extends _EOMangueCarriereSpecialisations implements InterfaceRecordAvecAutresAttributs,  InterfaceGestionIdUtilisateur {
	private String cSectionCnu, cSousSectionCnu;
	private boolean cnuPreparee = false;
	
	public String cSectionCnu() {
		if (!cnuPreparee) {
			preparerCnu();
		}
		return cSectionCnu;
	}

	public void setCSectionCnu(String value) {
		this.cSectionCnu = value;
	}

	public String cSousSectionCnu() {
		if (!cnuPreparee) {
			preparerCnu();
		}
		return cSousSectionCnu;
	}

	public void setCSousSectionCnu(String value) {
		this.cSousSectionCnu = value;
	}

	private void preparerCnu() {
		if (!cnuPreparee) {
			if (noCnu() != null) {
				EOCnu cnu = EOCnu.rechercherCnuPourNoCnu(editingContext(), noCnu());
				if (cnu != null) {
					cSectionCnu = cnu.cSectionCnu();
					cSousSectionCnu = cnu.cSousSectionCnu();
				}
			}
			cnuPreparee = true;
		}
	}
	
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport recordImport) {
		EOCarriereSpecialisations element = (EOCarriereSpecialisations)recordImport;
		EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext(), element.cSectionCnu(), element.cSousSectionCnu());
		if (cnu != null) {
			setNoCnu(cnu.noCnu());
		} else {
			setNoCnu(null);
		}
		return true;
	}
	
	@Override
	public void setEstValide(boolean aBool) {
		// Surcharge la méthode setEstValide et ne fait rien car l'objet Mangue Carriere Specialisation n'a pas TemValide
	}
	
	/** Retourne la spécialisation de carrière equivalents pour cet individu, ce segment de carriere 
	 * qui commence a la meme date.<BR>
	 * @param editingContext
	 * @param spec
	 * @return EOMangueCarriereSpecialisations ou null si non trouve
	 */
	public static EOMangueCarriereSpecialisations elementDestinationPourSpecialisation(EOEditingContext editingContext,EOCarriereSpecialisations spec) {
		NSArray results = rechercherElementsManguePourSpecialisation(editingContext,spec);
		// Pas de chevauchement des éléments de carrière valide
		if (results != null && results.count() == 1) {
			return (EOMangueCarriereSpecialisations)results.objectAtIndex(0);
		}	 else {
			return null;
		}
	}
	// Méthodes privée
	private static NSArray rechercherElementsManguePourSpecialisation(EOEditingContext editingContext,EOCarriereSpecialisations element) {
		if (element.toCarriere() == null) {
			return null;
		}
		// On recherche uniquement les valides
		EOMangueCarriere carriereMangue = (EOMangueCarriere)element.toCarriere().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (carriereMangue != null) {
			NSMutableArray args = new NSMutableArray(carriereMangue);
			// Vérifier que la date de début est la même : pour éviter les problèmes de comparaison
			// avec les heures, on vérifie qu'elle est entre le jour précédent et le jour suivant
			args.addObject(DateCtrl.jourPrecedent(element.dDebut()));
			args.addObject(DateCtrl.jourSuivant(element.dDebut()));
			String qualifier = TO_CARRIERE_KEY + " = %@ AND specDebut > %@ AND specDebut < %@";
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
			EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else {
			return null;
		}
	}

	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		EOCarriereSpecialisations element = (EOCarriereSpecialisations)recordImport;
		// A ce stade, la carrière est dans le SI Destinataire donc la correspondance créée
		EOMangueCarriere carriereMangue = ((EOCarriereCorresp)element.toCarriere().correspondance()).carriereMangue();
		setNoDossierPers(carriereMangue.individu().noIndividu());
		setNoSeqCarriere(carriereMangue.noSeqCarriere());
	}

}
