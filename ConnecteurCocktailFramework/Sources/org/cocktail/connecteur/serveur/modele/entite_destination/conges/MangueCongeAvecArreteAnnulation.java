/*
 * Created on 26 janv. 2006
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;


import org.cocktail.common.Constantes;
import org.cocktail.common.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe abstraite pour les conges legaux avec des arretes d'annulation
 */
public abstract class MangueCongeAvecArreteAnnulation extends MangueCongeLegal {
	public MangueCongeAvecArreteAnnulation() {
		super();
	}
	// signature
	 public abstract String temConfirme();
    public abstract void setTemConfirme(String value);

    public abstract String temGestEtab();
    public abstract void setTemGestEtab(String value);
   
	// arrêté d'annulation
    public abstract NSTimestamp dAnnulation();
    public abstract void setDAnnulation(NSTimestamp value);

    public abstract String noArreteAnnulation();
    public abstract void setNoArreteAnnulation(String value);
    
    public abstract MangueCongeAvecArreteAnnulation toCongeAnnulation();
    public abstract void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value);
    
    public abstract MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport);
    
    // Méthodes ajoutées
    public boolean congeValide() {
    	return dAnnulation() == null && noArreteAnnulation() == null && toCongeAnnulation() == null;
    }
	/** Surchargee par les sous-classes, retourne null si pas de date de commission */
	public abstract NSTimestamp dateCommission();
    // Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		setTemConfirme(Constantes.FAUX);
		setTemGestEtab(Constantes.FAUX);
		
		CongeAvecArrete conge = (CongeAvecArrete)recordImport ;
		if (conge.congeAnnulation() != null) {
			MangueCongeAvecArreteAnnulation congeMangueAnnulation=fetchCorrespondanceMangue(conge.congeAnnulation());
			if (congeMangueAnnulation==null)
				throw new Exception("Pas de déclaration accident dans objetImport");
			setToCongeAnnulationRelationship(congeMangueAnnulation);
		}
	}
	// Méthodes statiques
	/** recherche les congee valides pendant la periode
	 * @param editingContext
	 * @param individu d'import
	 * @param entite	nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode peut etre nulle
	 * @return conges trouves, null si l'individu (GRHum) n'existe pas ou que individu est nul
	 */
	public static NSArray rechercherCongesValidesPourIndividuEtPeriode(EOEditingContext editingContext,String nomEntite,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (individu != null) {
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu)individu.objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("individu = %@",new NSArray(individuGrhum)));
				if (debutPeriode != null) {
					qualifiers.addObject(SuperFinder.qualifierPourPeriode("dateDebut",debutPeriode,"dateFin",finPeriode));
				}
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("noArreteAnnulation = nil AND dAnnulation = nil AND toCongeAnnulation = nil", null));
				EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite, new EOAndQualifier(qualifiers),null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
