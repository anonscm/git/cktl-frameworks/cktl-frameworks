// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumStructure.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumStructure extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumStructure";

	// Attributes
	public static final String C_ACADEMIE_KEY = "cAcademie";
	public static final String C_NAF_KEY = "cNaf";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_STATUT_JURIDIQUE_KEY = "cStatutJuridique";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_TYPE_DECISION_STR_KEY = "cTypeDecisionStr";
	public static final String C_TYPE_ETABLISSEMEN_KEY = "cTypeEtablissemen";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String DATE_DECISION_KEY = "dateDecision";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETAB_CODURSSAF_KEY = "etabCodurssaf";
	public static final String ETAB_NUMURSSAF_KEY = "etabNumurssaf";
	public static final String EXPORT_KEY = "export";
	public static final String GENCOD_KEY = "gencod";
	public static final String GRP_ACCES_KEY = "grpAcces";
	public static final String GRP_ALIAS_KEY = "grpAlias";
	public static final String GRP_APE_CODE_KEY = "grpApeCode";
	public static final String GRP_APE_CODE_BIS_KEY = "grpApeCodeBis";
	public static final String GRP_APE_CODE_COMP_KEY = "grpApeCodeComp";
	public static final String GRP_CA_KEY = "grpCa";
	public static final String GRP_CAPITAL_KEY = "grpCapital";
	public static final String GRP_CENTRE_DECISION_KEY = "grpCentreDecision";
	public static final String GRP_EFFECTIFS_KEY = "grpEffectifs";
	public static final String GRP_FONCTION1_KEY = "grpFonction1";
	public static final String GRP_FONCTION2_KEY = "grpFonction2";
	public static final String GRP_FORME_JURIDIQUE_KEY = "grpFormeJuridique";
	public static final String GRP_MOTS_CLEFS_KEY = "grpMotsClefs";
	public static final String GRP_OWNER_KEY = "grpOwner";
	public static final String GRP_RESPONSABILITE_KEY = "grpResponsabilite";
	public static final String GRP_RESPONSABLE_KEY = "grpResponsable";
	public static final String GRP_TRADEMARQUE_KEY = "grpTrademarque";
	public static final String GRP_WEBMESTRE_KEY = "grpWebmestre";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String MOYENNE_AGE_KEY = "moyenneAge";
	public static final String NUM_ASSEDIC_KEY = "numAssedic";
	public static final String NUM_CNRACL_KEY = "numCnracl";
	public static final String NUM_IRCANTEC_KEY = "numIrcantec";
	public static final String NUM_RAFP_KEY = "numRafp";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String REF_DECISION_KEY = "refDecision";
	public static final String REF_EXT_COMP_KEY = "refExtComp";
	public static final String REF_EXT_CR_KEY = "refExtCr";
	public static final String REF_EXT_ETAB_KEY = "refExtEtab";
	public static final String SIREN_KEY = "siren";
	public static final String SIRET_KEY = "siret";
	public static final String STR_ACCUEIL_KEY = "strAccueil";
	public static final String STR_ACTIVITE_KEY = "strActivite";
	public static final String STR_AFFICHAGE_KEY = "strAffichage";
	public static final String STR_ORIGINE_KEY = "strOrigine";
	public static final String STR_PHOTO_KEY = "strPhoto";
	public static final String STR_RECHERCHE_KEY = "strRecherche";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String TVA_INTRACOM_KEY = "tvaIntracom";

	// Relationships
	public static final String STRUCTURE_PERE_KEY = "structurePere";
	public static final String TO_GRP_RESPONSABLE_KEY = "toGrpResponsable";

  private static Logger LOG = Logger.getLogger(_EOGrhumStructure.class);

  public EOGrhumStructure localInstanceIn(EOEditingContext editingContext) {
    EOGrhumStructure localInstance = (EOGrhumStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cAcademie() {
    return (String) storedValueForKey("cAcademie");
  }

  public void setCAcademie(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cAcademie from " + cAcademie() + " to " + value);
    }
    takeStoredValueForKey(value, "cAcademie");
  }

  public String cNaf() {
    return (String) storedValueForKey("cNaf");
  }

  public void setCNaf(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cNaf from " + cNaf() + " to " + value);
    }
    takeStoredValueForKey(value, "cNaf");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cStatutJuridique() {
    return (String) storedValueForKey("cStatutJuridique");
  }

  public void setCStatutJuridique(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cStatutJuridique from " + cStatutJuridique() + " to " + value);
    }
    takeStoredValueForKey(value, "cStatutJuridique");
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public String cTypeDecisionStr() {
    return (String) storedValueForKey("cTypeDecisionStr");
  }

  public void setCTypeDecisionStr(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cTypeDecisionStr from " + cTypeDecisionStr() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeDecisionStr");
  }

  public String cTypeEtablissemen() {
    return (String) storedValueForKey("cTypeEtablissemen");
  }

  public void setCTypeEtablissemen(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cTypeEtablissemen from " + cTypeEtablissemen() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeEtablissemen");
  }

  public String cTypeStructure() {
    return (String) storedValueForKey("cTypeStructure");
  }

  public void setCTypeStructure(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating cTypeStructure from " + cTypeStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeStructure");
  }

  public NSTimestamp dateDecision() {
    return (NSTimestamp) storedValueForKey("dateDecision");
  }

  public void setDateDecision(NSTimestamp value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating dateDecision from " + dateDecision() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDecision");
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey("dateFermeture");
  }

  public void setDateFermeture(NSTimestamp value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating dateFermeture from " + dateFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFermeture");
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey("dateOuverture");
  }

  public void setDateOuverture(NSTimestamp value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating dateOuverture from " + dateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateOuverture");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String etabCodurssaf() {
    return (String) storedValueForKey("etabCodurssaf");
  }

  public void setEtabCodurssaf(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating etabCodurssaf from " + etabCodurssaf() + " to " + value);
    }
    takeStoredValueForKey(value, "etabCodurssaf");
  }

  public String etabNumurssaf() {
    return (String) storedValueForKey("etabNumurssaf");
  }

  public void setEtabNumurssaf(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating etabNumurssaf from " + etabNumurssaf() + " to " + value);
    }
    takeStoredValueForKey(value, "etabNumurssaf");
  }

  public Double export() {
    return (Double) storedValueForKey("export");
  }

  public void setExport(Double value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating export from " + export() + " to " + value);
    }
    takeStoredValueForKey(value, "export");
  }

  public String gencod() {
    return (String) storedValueForKey("gencod");
  }

  public void setGencod(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating gencod from " + gencod() + " to " + value);
    }
    takeStoredValueForKey(value, "gencod");
  }

  public String grpAcces() {
    return (String) storedValueForKey("grpAcces");
  }

  public void setGrpAcces(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpAcces from " + grpAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "grpAcces");
  }

  public String grpAlias() {
    return (String) storedValueForKey("grpAlias");
  }

  public void setGrpAlias(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpAlias from " + grpAlias() + " to " + value);
    }
    takeStoredValueForKey(value, "grpAlias");
  }

  public String grpApeCode() {
    return (String) storedValueForKey("grpApeCode");
  }

  public void setGrpApeCode(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpApeCode from " + grpApeCode() + " to " + value);
    }
    takeStoredValueForKey(value, "grpApeCode");
  }

  public String grpApeCodeBis() {
    return (String) storedValueForKey("grpApeCodeBis");
  }

  public void setGrpApeCodeBis(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpApeCodeBis from " + grpApeCodeBis() + " to " + value);
    }
    takeStoredValueForKey(value, "grpApeCodeBis");
  }

  public String grpApeCodeComp() {
    return (String) storedValueForKey("grpApeCodeComp");
  }

  public void setGrpApeCodeComp(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpApeCodeComp from " + grpApeCodeComp() + " to " + value);
    }
    takeStoredValueForKey(value, "grpApeCodeComp");
  }

  public Integer grpCa() {
    return (Integer) storedValueForKey("grpCa");
  }

  public void setGrpCa(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpCa from " + grpCa() + " to " + value);
    }
    takeStoredValueForKey(value, "grpCa");
  }

  public Integer grpCapital() {
    return (Integer) storedValueForKey("grpCapital");
  }

  public void setGrpCapital(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpCapital from " + grpCapital() + " to " + value);
    }
    takeStoredValueForKey(value, "grpCapital");
  }

  public String grpCentreDecision() {
    return (String) storedValueForKey("grpCentreDecision");
  }

  public void setGrpCentreDecision(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpCentreDecision from " + grpCentreDecision() + " to " + value);
    }
    takeStoredValueForKey(value, "grpCentreDecision");
  }

  public Integer grpEffectifs() {
    return (Integer) storedValueForKey("grpEffectifs");
  }

  public void setGrpEffectifs(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpEffectifs from " + grpEffectifs() + " to " + value);
    }
    takeStoredValueForKey(value, "grpEffectifs");
  }

  public String grpFonction1() {
    return (String) storedValueForKey("grpFonction1");
  }

  public void setGrpFonction1(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpFonction1 from " + grpFonction1() + " to " + value);
    }
    takeStoredValueForKey(value, "grpFonction1");
  }

  public String grpFonction2() {
    return (String) storedValueForKey("grpFonction2");
  }

  public void setGrpFonction2(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpFonction2 from " + grpFonction2() + " to " + value);
    }
    takeStoredValueForKey(value, "grpFonction2");
  }

  public String grpFormeJuridique() {
    return (String) storedValueForKey("grpFormeJuridique");
  }

  public void setGrpFormeJuridique(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpFormeJuridique from " + grpFormeJuridique() + " to " + value);
    }
    takeStoredValueForKey(value, "grpFormeJuridique");
  }

  public String grpMotsClefs() {
    return (String) storedValueForKey("grpMotsClefs");
  }

  public void setGrpMotsClefs(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpMotsClefs from " + grpMotsClefs() + " to " + value);
    }
    takeStoredValueForKey(value, "grpMotsClefs");
  }

  public Integer grpOwner() {
    return (Integer) storedValueForKey("grpOwner");
  }

  public void setGrpOwner(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpOwner from " + grpOwner() + " to " + value);
    }
    takeStoredValueForKey(value, "grpOwner");
  }

  public String grpResponsabilite() {
    return (String) storedValueForKey("grpResponsabilite");
  }

  public void setGrpResponsabilite(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpResponsabilite from " + grpResponsabilite() + " to " + value);
    }
    takeStoredValueForKey(value, "grpResponsabilite");
  }

  public Integer grpResponsable() {
    return (Integer) storedValueForKey("grpResponsable");
  }

  public void setGrpResponsable(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpResponsable from " + grpResponsable() + " to " + value);
    }
    takeStoredValueForKey(value, "grpResponsable");
  }

  public String grpTrademarque() {
    return (String) storedValueForKey("grpTrademarque");
  }

  public void setGrpTrademarque(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpTrademarque from " + grpTrademarque() + " to " + value);
    }
    takeStoredValueForKey(value, "grpTrademarque");
  }

  public String grpWebmestre() {
    return (String) storedValueForKey("grpWebmestre");
  }

  public void setGrpWebmestre(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating grpWebmestre from " + grpWebmestre() + " to " + value);
    }
    takeStoredValueForKey(value, "grpWebmestre");
  }

  public String lcStructure() {
    return (String) storedValueForKey("lcStructure");
  }

  public void setLcStructure(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating lcStructure from " + lcStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "lcStructure");
  }

  public String llStructure() {
    return (String) storedValueForKey("llStructure");
  }

  public void setLlStructure(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating llStructure from " + llStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "llStructure");
  }

  public Double moyenneAge() {
    return (Double) storedValueForKey("moyenneAge");
  }

  public void setMoyenneAge(Double value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating moyenneAge from " + moyenneAge() + " to " + value);
    }
    takeStoredValueForKey(value, "moyenneAge");
  }

  public String numAssedic() {
    return (String) storedValueForKey("numAssedic");
  }

  public void setNumAssedic(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating numAssedic from " + numAssedic() + " to " + value);
    }
    takeStoredValueForKey(value, "numAssedic");
  }

  public String numCnracl() {
    return (String) storedValueForKey("numCnracl");
  }

  public void setNumCnracl(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating numCnracl from " + numCnracl() + " to " + value);
    }
    takeStoredValueForKey(value, "numCnracl");
  }

  public String numIrcantec() {
    return (String) storedValueForKey("numIrcantec");
  }

  public void setNumIrcantec(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating numIrcantec from " + numIrcantec() + " to " + value);
    }
    takeStoredValueForKey(value, "numIrcantec");
  }

  public String numRafp() {
    return (String) storedValueForKey("numRafp");
  }

  public void setNumRafp(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating numRafp from " + numRafp() + " to " + value);
    }
    takeStoredValueForKey(value, "numRafp");
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey("orgOrdre");
  }

  public void setOrgOrdre(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating orgOrdre from " + orgOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "orgOrdre");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String refDecision() {
    return (String) storedValueForKey("refDecision");
  }

  public void setRefDecision(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating refDecision from " + refDecision() + " to " + value);
    }
    takeStoredValueForKey(value, "refDecision");
  }

  public String refExtComp() {
    return (String) storedValueForKey("refExtComp");
  }

  public void setRefExtComp(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating refExtComp from " + refExtComp() + " to " + value);
    }
    takeStoredValueForKey(value, "refExtComp");
  }

  public String refExtCr() {
    return (String) storedValueForKey("refExtCr");
  }

  public void setRefExtCr(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating refExtCr from " + refExtCr() + " to " + value);
    }
    takeStoredValueForKey(value, "refExtCr");
  }

  public String refExtEtab() {
    return (String) storedValueForKey("refExtEtab");
  }

  public void setRefExtEtab(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating refExtEtab from " + refExtEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "refExtEtab");
  }

  public String siren() {
    return (String) storedValueForKey("siren");
  }

  public void setSiren(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating siren from " + siren() + " to " + value);
    }
    takeStoredValueForKey(value, "siren");
  }

  public String siret() {
    return (String) storedValueForKey("siret");
  }

  public void setSiret(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating siret from " + siret() + " to " + value);
    }
    takeStoredValueForKey(value, "siret");
  }

  public String strAccueil() {
    return (String) storedValueForKey("strAccueil");
  }

  public void setStrAccueil(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating strAccueil from " + strAccueil() + " to " + value);
    }
    takeStoredValueForKey(value, "strAccueil");
  }

  public String strActivite() {
    return (String) storedValueForKey("strActivite");
  }

  public void setStrActivite(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating strActivite from " + strActivite() + " to " + value);
    }
    takeStoredValueForKey(value, "strActivite");
  }

  public String strAffichage() {
    return (String) storedValueForKey("strAffichage");
  }

  public void setStrAffichage(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating strAffichage from " + strAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "strAffichage");
  }

  public String strOrigine() {
    return (String) storedValueForKey("strOrigine");
  }

  public void setStrOrigine(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating strOrigine from " + strOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, "strOrigine");
  }

  public String strPhoto() {
    return (String) storedValueForKey("strPhoto");
  }

  public void setStrPhoto(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating strPhoto from " + strPhoto() + " to " + value);
    }
    takeStoredValueForKey(value, "strPhoto");
  }

  public String strRecherche() {
    return (String) storedValueForKey("strRecherche");
  }

  public void setStrRecherche(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating strRecherche from " + strRecherche() + " to " + value);
    }
    takeStoredValueForKey(value, "strRecherche");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public String tvaIntracom() {
    return (String) storedValueForKey("tvaIntracom");
  }

  public void setTvaIntracom(String value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
    	_EOGrhumStructure.LOG.debug( "updating tvaIntracom from " + tvaIntracom() + " to " + value);
    }
    takeStoredValueForKey(value, "tvaIntracom");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structurePere() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structurePere");
  }

  public void setStructurePereRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
      _EOGrhumStructure.LOG.debug("updating structurePere from " + structurePere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structurePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structurePere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structurePere");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu toGrpResponsable() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("toGrpResponsable");
  }

  public void setToGrpResponsableRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOGrhumStructure.LOG.isDebugEnabled()) {
      _EOGrhumStructure.LOG.debug("updating toGrpResponsable from " + toGrpResponsable() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = toGrpResponsable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrpResponsable");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrpResponsable");
    }
  }
  

  public static EOGrhumStructure createGrhumStructure(EOEditingContext editingContext, String cStructure
, String cTypeStructure
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persId
, Integer persIdCreation
, Integer persIdModification
, String temValide
) {
    EOGrhumStructure eo = (EOGrhumStructure) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumStructure.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setCTypeStructure(cTypeStructure);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersId(persId);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOGrhumStructure> fetchAllGrhumStructures(EOEditingContext editingContext) {
    return _EOGrhumStructure.fetchAllGrhumStructures(editingContext, null);
  }

  public static NSArray<EOGrhumStructure> fetchAllGrhumStructures(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumStructure.fetchGrhumStructures(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumStructure> fetchGrhumStructures(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumStructure.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumStructure> eoObjects = (NSArray<EOGrhumStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumStructure fetchGrhumStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumStructure.fetchGrhumStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumStructure fetchGrhumStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumStructure> eoObjects = _EOGrhumStructure.fetchGrhumStructures(editingContext, qualifier, null);
    EOGrhumStructure eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumStructure)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumStructure fetchRequiredGrhumStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumStructure.fetchRequiredGrhumStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumStructure fetchRequiredGrhumStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumStructure eoObject = _EOGrhumStructure.fetchGrhumStructure(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumStructure localInstanceIn(EOEditingContext editingContext, EOGrhumStructure eo) {
    EOGrhumStructure localInstance = (eo == null) ? null : (EOGrhumStructure)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
