package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueCrct extends _EOMangueCrct {
	private static Logger log = Logger.getLogger(EOMangueCrct.class);
	private String cSectionCnu=null;
	private String cSousSectionCnu=null;
	private boolean cnuPreparee = false;

	public String cSectionCnu() {
		if (!cnuPreparee) {
			preparerCnu();
		}
		return cSectionCnu;
	}

	public void setCSectionCnu(String value) {
		this.cSectionCnu = value;
		EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext(), cSectionCnu, cSousSectionCnu);
		if (cnu != null) {
			setNoCnu(cnu.noCnu());
		} else {
			setNoCnu(null);
		}
	}

	public String cSousSectionCnu() {
		if (!cnuPreparee) {
			preparerCnu();
		}
		return cSousSectionCnu;
	}

	public void setCSousSectionCnu(String value) {
		this.cSousSectionCnu = value;
		EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext(), cSectionCnu, cSousSectionCnu);
		if (cnu != null) {
			setNoCnu(cnu.noCnu());
		} else {
			setNoCnu(null);
		}
	}

	private void preparerCnu() {
		if (!cnuPreparee) {
			if (noCnu() != null) {
				EOCnu cnu = EOCnu.rechercherCnuPourNoCnu(editingContext(), noCnu());
				if (cnu != null) {
					cSectionCnu = cnu.cSectionCnu();
					cSousSectionCnu = cnu.cSousSectionCnu();
				}
			}
			cnuPreparee = true;
		}
	}

	/** Recherche des delegations d'un individu pendant une periode donnee 
	 * @param individu Grhum
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 * @return retourne les temps partiels trouves et tous les tp si individu et debutPeriode sont nuls
	 */
	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

}
