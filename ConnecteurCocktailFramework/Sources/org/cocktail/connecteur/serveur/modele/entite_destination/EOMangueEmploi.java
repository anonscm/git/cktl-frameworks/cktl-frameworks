// EOMangueEmploi.java
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCnu;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.SuperFinder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEmploiCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOStructureCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.services.ServicesEmploi;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueEmploi extends _EOMangueEmploi implements InterfaceRecordAvecAutresAttributs {
	private String cSectionCnu, cSousSectionCnu;
	private boolean cnuPreparee = false;
	
	public ServicesEmploi servicesEmploi;
	
	
    public EOMangueEmploi() {
        super();
        servicesEmploi = new ServicesEmploi();
    }

	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		EOEmploi emploiImport = (EOEmploi)recordImport;
		
		Number idEmploi = SuperFinder.clePrimairePour(editingContext(),"MangueEmploi","idEmploi","MangueSeqEmploi",true);
		setIdEmploi(new Integer(idEmploi.intValue()));
		
		EOGrhumIndividu utilisateur = AutomateImport.sharedInstance().responsableImport();
		setPersIdCreation(utilisateur.persId());
		setPersIdModification(utilisateur.persId());
		setDEffetEmploi(emploiImport.dCreationEmploi());
		
		if (emploiImport.structure() != null) {
			EOGrhumStructure structure = EOStructureCorresp.structureGrhum(editingContext(), emploiImport.strSource());

			if (structure == null) {	// Ne devrait pas car on crée le structures avant
				throw new Exception("Pas de structure dans le SI correspondant a la structure " + emploiImport.strSource());
			} else if (emploiImport.cBudget().isEmpty()) {
				throw new Exception("Pas de budget asocié à cet emploi. Emploi ID source : " + emploiImport.empSource());
			} else if (emploiImport.cCategorieEmploi().isEmpty()) {
				throw new Exception("Pas de catégorie d'emploi associé à cet emploi. Emploi ID source : " + emploiImport.empSource());
			} else {
				
				servicesEmploi.intialisationServicesEmploi(this, structure, emploiImport.cCategorieEmploi(), emploiImport.cBudget(), emploiImport);
			}
		}
	}
	
	// InterfaceRecordAvecAutresAttributs
	public boolean modifierAutresAttributsAvecRecordImport(ObjetImport recordImport) {
		EOEmploi emploi = (EOEmploi)recordImport;
		EOCnu cnu = EOCnu.rechercherCnuPourSectionEtSousSection(editingContext(), emploi.cSectionCnu(), emploi.cSousSectionCnu());
		
		return true;
	}
	// Méthodes statiques
	/** Retourne l'emploi equivalent pour le m&ecir;me numero national et la m&cir;me date de publication.<BR>
	 * Retourne l'emploi si il en existe un seul qui n'est pas en correspondance avec un autre objet sinon null
	 * @param editingContext
	 * @param emploi
	 * @return EOMangueEmploi
	 */
	public static EOMangueEmploi emploiDestinatationPourEmploi(EOEditingContext editingContext,EOEmploi emploi) {
		NSArray emplois = rechercherMangueEmploisPourEmploi(editingContext,emploi);
		if (emplois != null && emplois.count() == 1) {
			return (EOMangueEmploi)emplois.objectAtIndex(0);
		} else {
			return null;
		}
	}
	/** Retourne true si on trouve plusieurs emplois pour le m&ecir;me numero national publies a la m&ecirc;me date */
	public static boolean aHomonyme(EOEditingContext editingContext,EOEmploi emploi) {
		NSArray results = rechercherMangueEmploisPourEmploi(editingContext,emploi);
		return results != null && results.count() > 1;
	}
	// Méthodes privées
	private static NSArray rechercherMangueEmploisPourEmploi(EOEditingContext editingContext, EOEmploi emploi) {
		NSMutableArray args = new NSMutableArray(emploi.noEmploiNational());
		// On cherche une date entre le jour précédent et le jour suivant pour ne pas avoir de pb d'heures
		args.addObject(DateCtrl.jourPrecedent(emploi.dPublicationEmploi()));
		args.addObject(DateCtrl.jourSuivant(emploi.dPublicationEmploi()));
//		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("noEmploiNational = %@ AND temValide = 'O' AND dPublicationEmploi > %@ AND dPublicationEmploi < %@",args);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("noEmploi = %@ AND temValide = 'O' AND dPublicationEmploi > %@ AND dPublicationEmploi < %@",args);
		EOFetchSpecification fs = new EOFetchSpecification("MangueEmploi",qualifier,null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		// Il ne faut retenir dans ces emplois que ceux qui ne sont pas déjà associés à des correspondances
		// autres que l'emploi courant
		NSMutableArray emplois = new NSMutableArray();
		java.util.Enumeration e = results.objectEnumerator();
		while (e.hasMoreElements()) {
			EOMangueEmploi mangueEmploi = (EOMangueEmploi)e.nextElement();
			EOEmploiCorresp corresp = EOEmploiCorresp.correspondancePourEmploiMangue(editingContext, mangueEmploi);
			// Si on ne trouve pas de correspondance ou qu'on en trouve une qui correspond à cet emploi
			if (corresp == null || corresp.emploi().empSource() == emploi.empSource()) {
				emplois.addObject(mangueEmploi);
			}
		}
		return emplois;
	}
	
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		NSTimestamp dateActuelle = new NSTimestamp();
		NSTimestamp dCreation = dateActuelle;
		NSTimestamp dModification = dateActuelle;
		
		setTemContractuel("N");
		setTemNational("O");
		setTemValide("O");
		if (temArbitrage() == null || temArbitrage().isEmpty()) {
			setTemArbitrage("N");
		}
		if (temConcours() == null || temConcours().isEmpty()) {
			setTemConcours("N");
		}
		if (temDurabilite() == null || temDurabilite().isEmpty()) {
			setTemDurabilite("P");
		}
		if (temEnseignant() == null || temEnseignant().isEmpty()) {
			setTemEnseignant("N");
		}
	}
}
