// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumEnfant.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumEnfant extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumEnfant";

	// Attributes
	public static final String D_ADOPTION_KEY = "dAdoption";
	public static final String D_ARRIVEE_FOYER_KEY = "dArriveeFoyer";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_SFT_KEY = "dDebSft";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_FIN_SFT_KEY = "dFinSft";
	public static final String D_MAX_A_CHARGE_KEY = "dMaxACharge";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String D_SEIZE_ANS_KEY = "dSeizeAns";
	public static final String D_VINGT_ANS_KEY = "dVingtAns";
	public static final String LIEU_NAISSANCE_KEY = "lieuNaissance";
	public static final String MORT_POUR_LA_FRANCE_KEY = "mortPourLaFrance";
	public static final String NOM_KEY = "nom";
	public static final String NO_ORDRE_NAISSANCE_KEY = "noOrdreNaissance";
	public static final String PRENOM_KEY = "prenom";
	public static final String SEXE_KEY = "sexe";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOGrhumEnfant.class);

  public EOGrhumEnfant localInstanceIn(EOEditingContext editingContext) {
    EOGrhumEnfant localInstance = (EOGrhumEnfant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dAdoption() {
    return (NSTimestamp) storedValueForKey("dAdoption");
  }

  public void setDAdoption(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dAdoption from " + dAdoption() + " to " + value);
    }
    takeStoredValueForKey(value, "dAdoption");
  }

  public NSTimestamp dArriveeFoyer() {
    return (NSTimestamp) storedValueForKey("dArriveeFoyer");
  }

  public void setDArriveeFoyer(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dArriveeFoyer from " + dArriveeFoyer() + " to " + value);
    }
    takeStoredValueForKey(value, "dArriveeFoyer");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebSft() {
    return (NSTimestamp) storedValueForKey("dDebSft");
  }

  public void setDDebSft(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dDebSft from " + dDebSft() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebSft");
  }

  public NSTimestamp dDeces() {
    return (NSTimestamp) storedValueForKey("dDeces");
  }

  public void setDDeces(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dDeces from " + dDeces() + " to " + value);
    }
    takeStoredValueForKey(value, "dDeces");
  }

  public NSTimestamp dFinSft() {
    return (NSTimestamp) storedValueForKey("dFinSft");
  }

  public void setDFinSft(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dFinSft from " + dFinSft() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinSft");
  }

  public NSTimestamp dMaxACharge() {
    return (NSTimestamp) storedValueForKey("dMaxACharge");
  }

  public void setDMaxACharge(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dMaxACharge from " + dMaxACharge() + " to " + value);
    }
    takeStoredValueForKey(value, "dMaxACharge");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey("dNaissance");
  }

  public void setDNaissance(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dNaissance from " + dNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "dNaissance");
  }

  public NSTimestamp dSeizeAns() {
    return (NSTimestamp) storedValueForKey("dSeizeAns");
  }

  public void setDSeizeAns(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dSeizeAns from " + dSeizeAns() + " to " + value);
    }
    takeStoredValueForKey(value, "dSeizeAns");
  }

  public NSTimestamp dVingtAns() {
    return (NSTimestamp) storedValueForKey("dVingtAns");
  }

  public void setDVingtAns(NSTimestamp value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating dVingtAns from " + dVingtAns() + " to " + value);
    }
    takeStoredValueForKey(value, "dVingtAns");
  }

  public String lieuNaissance() {
    return (String) storedValueForKey("lieuNaissance");
  }

  public void setLieuNaissance(String value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating lieuNaissance from " + lieuNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuNaissance");
  }

  public String mortPourLaFrance() {
    return (String) storedValueForKey("mortPourLaFrance");
  }

  public void setMortPourLaFrance(String value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating mortPourLaFrance from " + mortPourLaFrance() + " to " + value);
    }
    takeStoredValueForKey(value, "mortPourLaFrance");
  }

  public String nom() {
    return (String) storedValueForKey("nom");
  }

  public void setNom(String value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating nom from " + nom() + " to " + value);
    }
    takeStoredValueForKey(value, "nom");
  }

  public Integer noOrdreNaissance() {
    return (Integer) storedValueForKey("noOrdreNaissance");
  }

  public void setNoOrdreNaissance(Integer value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating noOrdreNaissance from " + noOrdreNaissance() + " to " + value);
    }
    takeStoredValueForKey(value, "noOrdreNaissance");
  }

  public String prenom() {
    return (String) storedValueForKey("prenom");
  }

  public void setPrenom(String value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating prenom from " + prenom() + " to " + value);
    }
    takeStoredValueForKey(value, "prenom");
  }

  public String sexe() {
    return (String) storedValueForKey("sexe");
  }

  public void setSexe(String value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating sexe from " + sexe() + " to " + value);
    }
    takeStoredValueForKey(value, "sexe");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOGrhumEnfant.LOG.isDebugEnabled()) {
    	_EOGrhumEnfant.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOGrhumEnfant createGrhumEnfant(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noOrdreNaissance
, String temValide
) {
    EOGrhumEnfant eo = (EOGrhumEnfant) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumEnfant.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoOrdreNaissance(noOrdreNaissance);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOGrhumEnfant> fetchAllGrhumEnfants(EOEditingContext editingContext) {
    return _EOGrhumEnfant.fetchAllGrhumEnfants(editingContext, null);
  }

  public static NSArray<EOGrhumEnfant> fetchAllGrhumEnfants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumEnfant.fetchGrhumEnfants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumEnfant> fetchGrhumEnfants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumEnfant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumEnfant> eoObjects = (NSArray<EOGrhumEnfant>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumEnfant fetchGrhumEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumEnfant.fetchGrhumEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumEnfant fetchGrhumEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumEnfant> eoObjects = _EOGrhumEnfant.fetchGrhumEnfants(editingContext, qualifier, null);
    EOGrhumEnfant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumEnfant)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumEnfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumEnfant fetchRequiredGrhumEnfant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumEnfant.fetchRequiredGrhumEnfant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumEnfant fetchRequiredGrhumEnfant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumEnfant eoObject = _EOGrhumEnfant.fetchGrhumEnfant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumEnfant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumEnfant localInstanceIn(EOEditingContext editingContext, EOGrhumEnfant eo) {
    EOGrhumEnfant localInstance = (eo == null) ? null : (EOGrhumEnfant)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
