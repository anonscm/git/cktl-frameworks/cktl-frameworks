//EOGrhumEnfant.java
//Created on Wed Jul 25 15:12:07 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import java.util.GregorianCalendar;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOLienFiliationEnfant;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEnfant;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.grhum.referentiel.EOGrhumRepartEnfant;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// 22/06/2010 - ajout du témoin mortPourLaFrance. Prise en compte du fait que l'individu importé n'est pas nécessairement un personnel
// 14/03/2011 - ajout des champs dArriveeFoyer, dAdoption, dMaxACharge et dDeces
public class EOGrhumEnfant extends _EOGrhumEnfant {
	private String sftPere, sftMere; // Utilisé au cas où un enfant est associé à deux parents
	private EOGrhumIndividu parentCourant;

	public EOGrhumEnfant() {
		super();
	}

	// Méthodes publiques
	public boolean gererRepartEnfant(EOEnfant enfantImport) {
		LogManager.logDetail("Gestion de RepartEnfant");
		// Rechercher l'individu Grhum qui est le parent. A cet instant le parent est nécessairement créé
		parentCourant = EOIndividuCorresp.individuGrhum(editingContext(), enfantImport.individu());
		// Lien de filiation
		EOLienFiliationEnfant lienFiliationEnfant = null;

		// Déterminer le sexe du parent pour savoir si on ajoute comme relation le père ou la mère
		if (parentCourant.estHomme()) {
			sftPere = enfantImport.temSft();
			sftMere = null;
		} else {
			sftMere = enfantImport.temSft();
			sftPere = null;
		}
		LogManager.logDetail(">>>> Individu " + parentCourant.nomUsuel() + ", sftPere " + sftPere + ", sftMere " + sftMere);
		NSArray reparts = EOGrhumRepartEnfant.rechercherRepartsPourEnfant(editingContext(), this);
		if (reparts.count() > 0) {
			determinerSftAutreParent(reparts);
		}
		LogManager.logDetail(">>>> Individu " + parentCourant.nomUsuel() + ", sftPere " + sftPere + ", sftMere " + sftMere);
		boolean repartModifiee = false;
		EOGrhumRepartEnfant repartPourPere = null;
		if (parentCourant.estHomme()) {
			// Rechercher la repart pour le père
			java.util.Enumeration e = reparts.objectEnumerator();
			while (e.hasMoreElements()) {
				EOGrhumRepartEnfant repart = (EOGrhumRepartEnfant) e.nextElement();
				if (repart.parent() == parentCourant) {
					repartPourPere = repart;
					break;
				}
			}
			if (repartPourPere == null) {
				repartPourPere = creerEtInsererRepart(enfantImport.aCharge());
				repartModifiee = true;
			}
			// On modifie le sft pour le père dans tous les cas sauf si le père est la mère ont droit
			// en même temps au sft, en quel cas le père n'a pas le droit au sft
			if (sftPere != null && sftMere != null && sftPere.equals(CocktailConstantes.VRAI) && sftMere.equals(CocktailConstantes.VRAI)) {
				if (repartPourPere.temSft().equals(CocktailConstantes.VRAI)) {
					repartPourPere.setTemSft(CocktailConstantes.FAUX);
					repartModifiee = true;
				}
			} else if (sftPere != null && sftPere.equals(repartPourPere.temSft()) == false) {
				repartPourPere.setTemSft(sftPere);
				repartModifiee = true;
			}
		}
		EOGrhumRepartEnfant repartPourMere = null;
		if (parentCourant.estHomme() == false) {
			// Rechercher la repart pour la mere
			java.util.Enumeration e = reparts.objectEnumerator();
			while (e.hasMoreElements()) {
				EOGrhumRepartEnfant repart = (EOGrhumRepartEnfant) e.nextElement();
				if (repart.parent() == parentCourant) {
					repartPourMere = repart;
					break;
				}
			}
			if (repartPourMere == null) {
				repartPourMere = creerEtInsererRepart(enfantImport.aCharge());
				repartModifiee = true;
			}
			// On modifie le sft pour la mère dans tous les cas si le sftMere est fourni
			// en même temps au sft
			if (sftMere != null && sftMere.equals(repartPourMere.temSft()) == false) {
				repartPourMere.setTemSft(sftMere);
				repartModifiee = true;
			}
		}

		if (enfantImport.lfenCode() != null) {
			EOQualifier qual = new EOKeyValueQualifier(EOLienFiliationEnfant.LFEN_CODE_KEY, EOQualifier.QualifierOperatorEqual, enfantImport.lfenCode());
			lienFiliationEnfant = EOLienFiliationEnfant.fetchRequiredLienFiliationEnfant(editingContext(), qual);
		}

		if (parentCourant.estHomme()) {
			if (repartPourPere != null) {
				repartPourPere.setLienFiliationEnfantRelationship(lienFiliationEnfant);
			}
		} else {
			if (repartPourMere != null) {
				repartPourMere.setLienFiliationEnfantRelationship(lienFiliationEnfant);
			}
		}

		return repartModifiee;
	}

	// Méthodes protégées
	protected void initialiserObjet(ObjetImport record) {
		EOEnfant enfant = (EOEnfant) record;

		// Rechercher l'individu Grhum qui est le parent. A cet instant le parent est nécessairement créé
		parentCourant = EOIndividuCorresp.individuGrhum(editingContext(), enfant.individu());
		// 24/06/2010 - l'individu n'est pas nécessairement un personnel
		if (parentCourant.personnel() != null) {
			// Modifier le nombre d'enfants du parent
			int nbEnfants = 0;
			if (parentCourant.personnel().nbEnfants() != null) {
				nbEnfants = parentCourant.personnel().nbEnfants().intValue();
			}
			parentCourant.personnel().setNbEnfants(new Integer(nbEnfants + 1));
		}
	}

	// Méthodes statiques
	/**
	 * retourne l'enfant du SI Destinataire avec le m&ecirc;me parent, le m&ecirc;me prenom et la m&circ;me date de naissance si elle est fournie
	 */
	public static EOGrhumEnfant enfantDestinationPourEnfant(EOEditingContext editingContext, EOEnfant enfant) {
		if (editingContext == null || enfant == null)
			return null;
		if (enfant.individu() != null) {
			EOGrhumIndividu individuGrhum = (EOGrhumIndividu) enfant.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (individuGrhum != null) {
				NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("parent = %@ AND enfant.temValide = 'O'", new NSArray(
						individuGrhum)));
				qualifiers
						.addObject(EOQualifier.qualifierWithQualifierFormat("enfant.prenom caseInsensitiveLike %@", new NSArray("*" + enfant.prenom() + "*")));
				// A cause du problème des heures on recherche sur date comprise entre jour précédent et jour suivant
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("enfant.dNaissance > %@",
						new NSArray(DateCtrl.jourPrecedent(enfant.dNaissance()))));
				qualifiers
						.addObject(EOQualifier.qualifierWithQualifierFormat("enfant.dNaissance < %@", new NSArray(DateCtrl.jourSuivant(enfant.dNaissance()))));

				EOFetchSpecification fs = new EOFetchSpecification("GrhumRepartEnfant", new EOAndQualifier(qualifiers), null);
				NSArray result = editingContext.objectsWithFetchSpecification(fs);
				if (result.count() == 1) {
					return ((EOGrhumRepartEnfant) result.objectAtIndex(0)).enfant();
				} else {
					return null;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static EOGrhumEnfant enfantPourNomPrenomDateNaissance(EOEditingContext editingContext, String nom, String prenom, NSTimestamp dateNaissance) {
		NSMutableArray args = new NSMutableArray();
		args.addObject(nom);
		args.addObject(prenom);
		args.addObject(dateNaissance);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
				"nom caseInsensitiveLike %@ AND prenom caseInsensitiveLike %@ AND dNaissance = %@ AND temValide = 'O'", args);
		EOFetchSpecification fs = new EOFetchSpecification("GrhumEnfant", myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOGrhumEnfant) editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	// Méthodes privées
	private void determinerSftAutreParent(NSArray reparts) {
		if (parentCourant.estHomme()) {
			// vérifier si il n'y aurait pas un sft pour la mère
			java.util.Enumeration e = reparts.objectEnumerator();
			while (e.hasMoreElements()) {
				EOGrhumRepartEnfant repart = (EOGrhumRepartEnfant) e.nextElement();
				if (repart.parent().estHomme() == false) {
					sftMere = repart.temSft();
					break;
				}
			}
		} else {
			// vérifier si il n'y aurait pas un sft pour le père
			java.util.Enumeration e = reparts.objectEnumerator();
			while (e.hasMoreElements()) {
				EOGrhumRepartEnfant repart = (EOGrhumRepartEnfant) e.nextElement();
				if (repart.parent().estHomme()) {
					sftPere = repart.temSft();
					break;
				}
			}
		}
	}

	private EOGrhumRepartEnfant creerEtInsererRepart(String temACharge) {
		LogManager.logDetail("Création d'entrées dans RepartEnfant");
		EOGrhumRepartEnfant repart = new EOGrhumRepartEnfant();
		editingContext().insertObject(repart);
		repart.initAvecEnfantEtParent(this, parentCourant, temACharge);
		return repart;
	}
}
