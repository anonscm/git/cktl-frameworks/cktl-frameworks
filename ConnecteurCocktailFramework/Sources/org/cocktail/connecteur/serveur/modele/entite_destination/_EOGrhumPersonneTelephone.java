// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGrhumPersonneTelephone.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGrhumPersonneTelephone extends ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "GrhumPersonneTelephone";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDICATIF_KEY = "indicatif";
	public static final String LISTE_ROUGE_KEY = "listeRouge";
	public static final String NO_TELEPHONE_KEY = "noTelephone";
	public static final String PERS_ID_KEY = "persId";
	public static final String TEL_PRINCIPAL_KEY = "telPrincipal";
	public static final String TYPE_NO_KEY = "typeNo";
	public static final String TYPE_TEL_KEY = "typeTel";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOGrhumPersonneTelephone.class);

  public EOGrhumPersonneTelephone localInstanceIn(EOEditingContext editingContext) {
    EOGrhumPersonneTelephone localInstance = (EOGrhumPersonneTelephone)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer indicatif() {
    return (Integer) storedValueForKey("indicatif");
  }

  public void setIndicatif(Integer value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating indicatif from " + indicatif() + " to " + value);
    }
    takeStoredValueForKey(value, "indicatif");
  }

  public String listeRouge() {
    return (String) storedValueForKey("listeRouge");
  }

  public void setListeRouge(String value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating listeRouge from " + listeRouge() + " to " + value);
    }
    takeStoredValueForKey(value, "listeRouge");
  }

  public String noTelephone() {
    return (String) storedValueForKey("noTelephone");
  }

  public void setNoTelephone(String value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating noTelephone from " + noTelephone() + " to " + value);
    }
    takeStoredValueForKey(value, "noTelephone");
  }

  public Integer persId() {
    return (Integer) storedValueForKey("persId");
  }

  public void setPersId(Integer value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public String telPrincipal() {
    return (String) storedValueForKey("telPrincipal");
  }

  public void setTelPrincipal(String value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating telPrincipal from " + telPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "telPrincipal");
  }

  public String typeNo() {
    return (String) storedValueForKey("typeNo");
  }

  public void setTypeNo(String value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating typeNo from " + typeNo() + " to " + value);
    }
    takeStoredValueForKey(value, "typeNo");
  }

  public String typeTel() {
    return (String) storedValueForKey("typeTel");
  }

  public void setTypeTel(String value) {
    if (_EOGrhumPersonneTelephone.LOG.isDebugEnabled()) {
    	_EOGrhumPersonneTelephone.LOG.debug( "updating typeTel from " + typeTel() + " to " + value);
    }
    takeStoredValueForKey(value, "typeTel");
  }


  public static EOGrhumPersonneTelephone createGrhumPersonneTelephone(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String noTelephone
, Integer persId
, String telPrincipal
, String typeNo
, String typeTel
) {
    EOGrhumPersonneTelephone eo = (EOGrhumPersonneTelephone) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumPersonneTelephone.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoTelephone(noTelephone);
		eo.setPersId(persId);
		eo.setTelPrincipal(telPrincipal);
		eo.setTypeNo(typeNo);
		eo.setTypeTel(typeTel);
    return eo;
  }

  public static NSArray<EOGrhumPersonneTelephone> fetchAllGrhumPersonneTelephones(EOEditingContext editingContext) {
    return _EOGrhumPersonneTelephone.fetchAllGrhumPersonneTelephones(editingContext, null);
  }

  public static NSArray<EOGrhumPersonneTelephone> fetchAllGrhumPersonneTelephones(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGrhumPersonneTelephone.fetchGrhumPersonneTelephones(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGrhumPersonneTelephone> fetchGrhumPersonneTelephones(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGrhumPersonneTelephone.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGrhumPersonneTelephone> eoObjects = (NSArray<EOGrhumPersonneTelephone>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGrhumPersonneTelephone fetchGrhumPersonneTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPersonneTelephone.fetchGrhumPersonneTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPersonneTelephone fetchGrhumPersonneTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGrhumPersonneTelephone> eoObjects = _EOGrhumPersonneTelephone.fetchGrhumPersonneTelephones(editingContext, qualifier, null);
    EOGrhumPersonneTelephone eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGrhumPersonneTelephone)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GrhumPersonneTelephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPersonneTelephone fetchRequiredGrhumPersonneTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGrhumPersonneTelephone.fetchRequiredGrhumPersonneTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGrhumPersonneTelephone fetchRequiredGrhumPersonneTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGrhumPersonneTelephone eoObject = _EOGrhumPersonneTelephone.fetchGrhumPersonneTelephone(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GrhumPersonneTelephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGrhumPersonneTelephone localInstanceIn(EOEditingContext editingContext, EOGrhumPersonneTelephone eo) {
    EOGrhumPersonneTelephone localInstance = (eo == null) ? null : (EOGrhumPersonneTelephone)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
