// EOMangueOccupation.java
/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOContratCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOEmploiCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.EOOccupation;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueOccupation extends _EOMangueOccupation {

	public EOMangueOccupation() {
		super();
	}

	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		setDenMoyenUtilise(new Integer(100));
		EOOccupation occupationImport = (EOOccupation) recordImport;
		// Rechercher la correspondance sur l'emploi, à ce stade elle doit avoir
		// été créée
		EOEmploiCorresp emploiCorresp = (EOEmploiCorresp) ObjetCorresp.rechercherObjetCorrespPourRecordImport(occupationImport.editingContext(),
				occupationImport.emploi(), true);
		if (emploiCorresp != null) {
			setEmploiRelationship(emploiCorresp.emploiMangue());
		} else {
			throw new Exception("Pas d'emploi dans le SI correspondant a l'emploi " + occupationImport.emploi().empSource());
		}
		EOGrhumIndividu individu = EOIndividuCorresp.individuGrhum(editingContext(), occupationImport.idSource());
		if (individu != null) {
			setIndividuRelationship(individu);
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + occupationImport.idSource());
		}
		if (occupationImport.contrat() != null) {
			EOMangueContrat contratMangue = ((EOContratCorresp) occupationImport.contrat().correspondance()).contratMangue();
			setContratRelationship(contratMangue);
		}
		if (occupationImport.toCarriere() != null) {
			EOMangueCarriere carriereMangue = ((EOCarriereCorresp) occupationImport.toCarriere().correspondance()).carriereMangue();
			setCarriereRelationship(carriereMangue);
		}
	}

	// Méthodes statiques
	/**
	 * Retourne l'occupation equivalente pour cet individu et cet emploi et qui
	 * commence a la m&ecirc;me date. <BR>
	 * Retourne l'occupation si il existe une seule sinon null
	 * 
	 * @param editingContext
	 * @param occupation
	 * @return EOMangueOccupation
	 */
	public static EOMangueOccupation occupationDestinationPourOccupation(EOEditingContext editingContext, EOOccupation occupation) {
		// Rechercher les occupations valides : pour une structure donnée et une
		// période donnée, il ne peut y en avoir qu'une
		// Ce n'est donc pas la peine de vérifier la correspondance
		NSArray<EOMangueOccupation> occupations = rechercherMangueOccupationsPourOccupation(editingContext, occupation);
		if (occupations != null && occupations.count() == 1) {
			return occupations.objectAtIndex(0);
		} else {
			return null;
		}
	}

	/**
	 * retourne les occupations pour l'individu de l'occupation et la
	 * m&ecirc;rme periode.<BR>
	 * 
	 * @param editingContext
	 * @param occupation
	 * @return NSArray
	 */
	public static NSArray rechercherOccupationsPourIndividuEtPeriode(EOEditingContext editingContext, EOOccupation occupation) {
		// Vérifier si il existe un individu équivalent dans le SI Destinataire.
		// On ne se base pas
		// sur les correspondances car l'individu n'a peut-être pas encore été
		// créé
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) occupation.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individuGrhum));
			NSMutableArray qualifiers = new NSMutableArray(qualifier);
			qualifiers.addObject(Finder.qualifierPourPeriode(D_DEB_OCCUPATION_KEY, occupation.dDebOccupation(), D_FIN_OCCUPATION_KEY,
					occupation.dFinOccupation()));
			qualifier = new EOAndQualifier(qualifiers);
			EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
			myFetch.setRefreshesRefetchedObjects(true);
			return editingContext.objectsWithFetchSpecification(myFetch);
		} else { // Individu pas encore créé
			return null;
		}
	}

	/**
	 * recherche les occupations d'un individu commen&ccedil;ant apr&egrave;s la
	 * date fournie en param&egrave;tre
	 */
	public static NSArray<EOMangueOccupation> occupationsPosterieuresADate(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp date) {
		NSMutableArray args = new NSMutableArray(individu.noIndividu());
		args.addObject(date);
		EOQualifier myQualifier = EOQualifier
				.qualifierWithQualifierFormat(INDIVIDU_KEY + ".noIndividu = %@ AND dDebOccupation > %@  AND temValide = 'O'", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, myQualifier, null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * Recherche des occupations d'un individu pendant une periode donnee
	 * 
	 * @param individu
	 *            (peut &ecirc;tre nul)
	 * @param debutPeriode
	 *            debut periode
	 * @param finPeriode
	 *            fin periode (peut &ecirc;tre nulle)
	 */
	public static NSArray<EOMangueOccupation> rechercherOccupationsPourIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu,
			NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = 'O'", null));
		EOQualifier qualifier;
		if (individu != null) {
			NSMutableArray args = new NSMutableArray(individu.noIndividu());
			String stringQualifier = INDIVIDU_KEY + ".noIndividu = %@";
			qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
			qualifiers.addObject(qualifier);
		}
		qualifier = Finder.qualifierPourPeriode(D_DEB_OCCUPATION_KEY, debutPeriode, D_FIN_OCCUPATION_KEY, finPeriode);
		qualifiers.addObject(qualifier);
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/** ferme les occupations d'un agent valides a la date passee en parametre */
	public static void fermerOccupations(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp date) {
		NSArray<EOMangueOccupation> objets = rechercherOccupationsPourIndividuEtPeriode(editingContext, individu, date, date);
		for (EOMangueOccupation occupation : objets) {
			occupation.setDFinOccupation(date);
		}

		// supprimer les occupations postérieures à cette date
		objets = occupationsPosterieuresADate(editingContext, individu, date);
		for (EOMangueOccupation occupation : objets) {
			occupation.setTemValide(CocktailConstantes.FAUX);
		}
	}

	// Méthodes privées
	private static NSArray<EOMangueOccupation> rechercherMangueOccupationsPourOccupation(EOEditingContext editingContext, EOOccupation occupation) {
		// Vérifier si il existe une occupation équivalente dans le SI
		// Destinataire. On ne se base pas à cet instant
		// sur les correspondances car l'individu ou l'emploi n'ont peut-être
		// pas encore été créés
		EOGrhumIndividu individuGrhum = (EOGrhumIndividu) occupation.individu().objetDestinataireAvecOuSansCorrespondance(editingContext);
		if (individuGrhum != null) {
			// Idem pour les emplois
			EOMangueEmploi emploiMangue = (EOMangueEmploi) occupation.emploi().objetDestinataireAvecOuSansCorrespondance(editingContext);
			if (emploiMangue != null) {
				NSMutableArray args = new NSMutableArray(individuGrhum);
				args.addObject(emploiMangue);
				// Vérifier que la date de début d'occupation est la même : pour
				// éviter les problèmes de comparaison
				// avec les heures, on vérifie qu'elle est entre le jour
				// précédent et le jour suivant
				args.addObject(DateCtrl.jourPrecedent(occupation.dDebOccupation()));
				args.addObject(DateCtrl.jourSuivant(occupation.dDebOccupation()));
				String qualifier = INDIVIDU_KEY + " = %@ AND emploi = %@ AND temValide = 'O' AND dDebOccupation > %@ AND dDebOccupation < %@";

				EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(qualifier, args);
				EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, myQualifier, null);
				return editingContext.objectsWithFetchSpecification(myFetch);
			}
		}
		return null;
	}

}
