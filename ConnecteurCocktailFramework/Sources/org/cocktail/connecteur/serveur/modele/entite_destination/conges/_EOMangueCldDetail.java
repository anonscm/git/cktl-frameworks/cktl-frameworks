// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCldDetail.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCldDetail extends org.cocktail.connecteur.serveur.modele.entite_destination.ObjetPourSIDestinataireAvecDates {
	public static final String ENTITY_NAME = "MangueCldDetail";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEN_TAUX_KEY = "denTaux";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NUM_TAUX_KEY = "numTaux";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String CLD_DETAIL_CORRESPS_KEY = "cldDetailCorresps";
	public static final String MANGUE_CLD_KEY = "mangueCld";

  private static Logger LOG = Logger.getLogger(_EOMangueCldDetail.class);

  public EOMangueCldDetail localInstanceIn(EOEditingContext editingContext) {
    EOMangueCldDetail localInstance = (EOMangueCldDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
    	_EOMangueCldDetail.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
    	_EOMangueCldDetail.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
    	_EOMangueCldDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Long denTaux() {
    return (Long) storedValueForKey("denTaux");
  }

  public void setDenTaux(Long value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
    	_EOMangueCldDetail.LOG.debug( "updating denTaux from " + denTaux() + " to " + value);
    }
    takeStoredValueForKey(value, "denTaux");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
    	_EOMangueCldDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Long numTaux() {
    return (Long) storedValueForKey("numTaux");
  }

  public void setNumTaux(Long value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
    	_EOMangueCldDetail.LOG.debug( "updating numTaux from " + numTaux() + " to " + value);
    }
    takeStoredValueForKey(value, "numTaux");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
    	_EOMangueCldDetail.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld mangueCld() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld)storedValueForKey("mangueCld");
  }

  public void setMangueCldRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld value) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
      _EOMangueCldDetail.LOG.debug("updating mangueCld from " + mangueCld() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld oldValue = mangueCld();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mangueCld");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mangueCld");
    }
  }
  
  public NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp> cldDetailCorresps() {
    return (NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp>)storedValueForKey("cldDetailCorresps");
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp> cldDetailCorresps(EOQualifier qualifier) {
    return cldDetailCorresps(qualifier, null, false);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp> cldDetailCorresps(EOQualifier qualifier, boolean fetch) {
    return cldDetailCorresps(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp> cldDetailCorresps(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp.CLD_DETAIL_MANGUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp.fetchCldDetailCorresps(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = cldDetailCorresps();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCldDetailCorrespsRelationship(org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp object) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
      _EOMangueCldDetail.LOG.debug("adding " + object + " to cldDetailCorresps relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "cldDetailCorresps");
  }

  public void removeFromCldDetailCorrespsRelationship(org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp object) {
    if (_EOMangueCldDetail.LOG.isDebugEnabled()) {
      _EOMangueCldDetail.LOG.debug("removing " + object + " from cldDetailCorresps relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "cldDetailCorresps");
  }

  public org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp createCldDetailCorrespsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CldDetailCorresp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "cldDetailCorresps");
    return (org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp) eo;
  }

  public void deleteCldDetailCorrespsRelationship(org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "cldDetailCorresps");
    editingContext().deleteObject(object);
  }

  public void deleteAllCldDetailCorrespsRelationships() {
    Enumeration objects = cldDetailCorresps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCldDetailCorrespsRelationship((org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCldDetailCorresp)objects.nextElement());
    }
  }


  public static EOMangueCldDetail createMangueCldDetail(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld mangueCld) {
    EOMangueCldDetail eo = (EOMangueCldDetail) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCldDetail.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setMangueCldRelationship(mangueCld);
    return eo;
  }

  public static NSArray<EOMangueCldDetail> fetchAllMangueCldDetails(EOEditingContext editingContext) {
    return _EOMangueCldDetail.fetchAllMangueCldDetails(editingContext, null);
  }

  public static NSArray<EOMangueCldDetail> fetchAllMangueCldDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCldDetail.fetchMangueCldDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCldDetail> fetchMangueCldDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCldDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCldDetail> eoObjects = (NSArray<EOMangueCldDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCldDetail fetchMangueCldDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCldDetail.fetchMangueCldDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCldDetail fetchMangueCldDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCldDetail> eoObjects = _EOMangueCldDetail.fetchMangueCldDetails(editingContext, qualifier, null);
    EOMangueCldDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCldDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCldDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCldDetail fetchRequiredMangueCldDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCldDetail.fetchRequiredMangueCldDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCldDetail fetchRequiredMangueCldDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCldDetail eoObject = _EOMangueCldDetail.fetchMangueCldDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCldDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCldDetail localInstanceIn(EOEditingContext editingContext, EOMangueCldDetail eo) {
    EOMangueCldDetail localInstance = (eo == null) ? null : (EOMangueCldDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
