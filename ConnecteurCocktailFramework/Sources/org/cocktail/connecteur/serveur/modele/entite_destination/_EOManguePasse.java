// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOManguePasse.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOManguePasse extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "ManguePasse";

	// Attributes
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_TYPE_POPULATION_KEY = "cTypePopulation";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_VALIDEE_ANNEES_KEY = "dureeValideeAnnees";
	public static final String DUREE_VALIDEE_JOURS_KEY = "dureeValideeJours";
	public static final String DUREE_VALIDEE_MOIS_KEY = "dureeValideeMois";
	public static final String D_VALIDATION_SERVICE_KEY = "dValidationService";
	public static final String ETABLISSEMENT_PASSE_KEY = "etablissementPasse";
	public static final String FONCTION_PASSE_KEY = "fonctionPasse";
	public static final String PAS_MINISTERE_KEY = "pasMinistere";
	public static final String PAS_PC_ACQUITEE_KEY = "pasPcAcquitee";
	public static final String PAS_QUOTITE_KEY = "pasQuotite";
	public static final String PAS_QUOTITE_COTISATION_KEY = "pasQuotiteCotisation";
	public static final String PAS_TYPE_FCT_PUBLIQUE_KEY = "pasTypeFctPublique";
	public static final String PAS_TYPE_SERVICE_KEY = "pasTypeService";
	public static final String PAS_TYPE_TEMPS_KEY = "pasTypeTemps";
	public static final String SECTEUR_PASSE_KEY = "secteurPasse";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";

  private static Logger LOG = Logger.getLogger(_EOManguePasse.class);

  public EOManguePasse localInstanceIn(EOEditingContext editingContext) {
    EOManguePasse localInstance = (EOManguePasse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCategorie() {
    return (String) storedValueForKey("cCategorie");
  }

  public void setCCategorie(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating cCategorie from " + cCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, "cCategorie");
  }

  public String cTypePopulation() {
    return (String) storedValueForKey("cTypePopulation");
  }

  public void setCTypePopulation(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating cTypePopulation from " + cTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypePopulation");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dureeValideeAnnees() {
    return (Integer) storedValueForKey("dureeValideeAnnees");
  }

  public void setDureeValideeAnnees(Integer value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dureeValideeAnnees from " + dureeValideeAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeAnnees");
  }

  public Integer dureeValideeJours() {
    return (Integer) storedValueForKey("dureeValideeJours");
  }

  public void setDureeValideeJours(Integer value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dureeValideeJours from " + dureeValideeJours() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeJours");
  }

  public Integer dureeValideeMois() {
    return (Integer) storedValueForKey("dureeValideeMois");
  }

  public void setDureeValideeMois(Integer value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dureeValideeMois from " + dureeValideeMois() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeMois");
  }

  public NSTimestamp dValidationService() {
    return (NSTimestamp) storedValueForKey("dValidationService");
  }

  public void setDValidationService(NSTimestamp value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating dValidationService from " + dValidationService() + " to " + value);
    }
    takeStoredValueForKey(value, "dValidationService");
  }

  public String etablissementPasse() {
    return (String) storedValueForKey("etablissementPasse");
  }

  public void setEtablissementPasse(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating etablissementPasse from " + etablissementPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "etablissementPasse");
  }

  public String fonctionPasse() {
    return (String) storedValueForKey("fonctionPasse");
  }

  public void setFonctionPasse(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating fonctionPasse from " + fonctionPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionPasse");
  }

  public String pasMinistere() {
    return (String) storedValueForKey("pasMinistere");
  }

  public void setPasMinistere(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating pasMinistere from " + pasMinistere() + " to " + value);
    }
    takeStoredValueForKey(value, "pasMinistere");
  }

  public String pasPcAcquitee() {
    return (String) storedValueForKey("pasPcAcquitee");
  }

  public void setPasPcAcquitee(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating pasPcAcquitee from " + pasPcAcquitee() + " to " + value);
    }
    takeStoredValueForKey(value, "pasPcAcquitee");
  }

  public Integer pasQuotite() {
    return (Integer) storedValueForKey("pasQuotite");
  }

  public void setPasQuotite(Integer value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating pasQuotite from " + pasQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "pasQuotite");
  }

  public Integer pasQuotiteCotisation() {
    return (Integer) storedValueForKey("pasQuotiteCotisation");
  }

  public void setPasQuotiteCotisation(Integer value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating pasQuotiteCotisation from " + pasQuotiteCotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "pasQuotiteCotisation");
  }

  public String pasTypeFctPublique() {
    return (String) storedValueForKey("pasTypeFctPublique");
  }

  public void setPasTypeFctPublique(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating pasTypeFctPublique from " + pasTypeFctPublique() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeFctPublique");
  }

  public String pasTypeService() {
    return (String) storedValueForKey("pasTypeService");
  }

  public void setPasTypeService(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating pasTypeService from " + pasTypeService() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeService");
  }

  public String pasTypeTemps() {
    return (String) storedValueForKey("pasTypeTemps");
  }

  public void setPasTypeTemps(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating pasTypeTemps from " + pasTypeTemps() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeTemps");
  }

  public String secteurPasse() {
    return (String) storedValueForKey("secteurPasse");
  }

  public void setSecteurPasse(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating secteurPasse from " + secteurPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "secteurPasse");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
    	_EOManguePasse.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOManguePasse.LOG.isDebugEnabled()) {
      _EOManguePasse.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  

  public static EOManguePasse createManguePasse(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String secteurPasse
, String temTitulaire
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOManguePasse eo = (EOManguePasse) EOUtilities.createAndInsertInstance(editingContext, _EOManguePasse.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setSecteurPasse(secteurPasse);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOManguePasse> fetchAllManguePasses(EOEditingContext editingContext) {
    return _EOManguePasse.fetchAllManguePasses(editingContext, null);
  }

  public static NSArray<EOManguePasse> fetchAllManguePasses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOManguePasse.fetchManguePasses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOManguePasse> fetchManguePasses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOManguePasse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOManguePasse> eoObjects = (NSArray<EOManguePasse>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOManguePasse fetchManguePasse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePasse.fetchManguePasse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePasse fetchManguePasse(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOManguePasse> eoObjects = _EOManguePasse.fetchManguePasses(editingContext, qualifier, null);
    EOManguePasse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOManguePasse)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ManguePasse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePasse fetchRequiredManguePasse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOManguePasse.fetchRequiredManguePasse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOManguePasse fetchRequiredManguePasse(EOEditingContext editingContext, EOQualifier qualifier) {
    EOManguePasse eoObject = _EOManguePasse.fetchManguePasse(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ManguePasse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOManguePasse localInstanceIn(EOEditingContext editingContext, EOManguePasse eo) {
    EOManguePasse localInstance = (eo == null) ? null : (EOManguePasse)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
