package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.EOCgntRaisonFamPersoCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueCgntRaisonFamPerso extends _EOMangueCgntRaisonFamPerso {

	/** Recherche des delegations d'un individu pendant une periode donnee 
	 * @param individu Grhum
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 * @return retourne les temps partiels trouves et tous les tp si individu et debutPeriode sont nuls
	 */
	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	@Override
	public String temConfirme() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
		return null;
	}

	@Override
	public void setTemConfirme(String value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
	}

	@Override
	public String temGestEtab() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
		return null;
	}

	@Override
	public void setTemGestEtab(String value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
	}

	@Override
	public NSTimestamp dAnnulation() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
		return null;
	}

	@Override
	public void setDAnnulation(NSTimestamp value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
	}

	@Override
	public String noArreteAnnulation() {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
		return null;
	}

	@Override
	public void setNoArreteAnnulation(String value) {
		// Cet attribut n'existe pas sur ce congé donc on rajoute une implémentation pour répondre à la méthode abstraite
	}

	@Override
	public NSTimestamp dateCommission() {
		return null;
	}

	@Override
	public String typeEvenement() {
		return null;
	}

	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCgntRaisonFamPerso)value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCgntRaisonFamPersoCorresp congeCorresp = EOCgntRaisonFamPersoCorresp.fetchCgntRaisonFamPersoCorresp(editingContext(), EOCgntRaisonFamPersoCorresp.CGNT_RAISON_FAM_PERSO_KEY,
				congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.mangueCgntRaisonFamPerso();
	}
}
