package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import org.apache.log4j.Logger;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.serveur.modele.correspondance.conges.EOCongePaterniteCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.CongeAvecArrete;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueCongePaternite extends _EOMangueCongePaternite {
  private static Logger log = Logger.getLogger(EOMangueCongePaternite.class);
  
  
  public String typeEvenement() {
		return "CPATER";
	}
	public NSTimestamp dateCommission() {
		return null;
	}
	
	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	@Override
	public void setToCongeAnnulationRelationship(MangueCongeAvecArreteAnnulation value) {
		setToCongeAnnulationRelationship((EOMangueCongePaternite)value);
	}

	@Override
	public MangueCongeAvecArreteAnnulation fetchCorrespondanceMangue(CongeAvecArrete congeImport) {
		EOCongePaterniteCorresp congeCorresp = EOCongePaterniteCorresp.fetchCongePaterniteCorresp(editingContext(), EOCongePaterniteCorresp.TO_CONGE_PATERNITE_KEY,
				congeImport);
		if (congeCorresp == null)
			return null;
		return congeCorresp.toMangueCongePaternite();
	}
  
}
