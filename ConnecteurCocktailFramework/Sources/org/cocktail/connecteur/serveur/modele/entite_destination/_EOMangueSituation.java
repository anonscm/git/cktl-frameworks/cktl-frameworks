// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueSituation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueSituation extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueSituation";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEN_QUOTITE_KEY = "denQuotite";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";

  private static Logger LOG = Logger.getLogger(_EOMangueSituation.class);

  public EOMangueSituation localInstanceIn(EOEditingContext editingContext) {
    EOMangueSituation localInstance = (EOMangueSituation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer denQuotite() {
    return (Integer) storedValueForKey("denQuotite");
  }

  public void setDenQuotite(Integer value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating denQuotite from " + denQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotite");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer numQuotite() {
    return (Integer) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Integer value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
    	_EOMangueSituation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse adresse() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse)storedValueForKey("adresse");
  }

  public void setAdresseRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
      _EOMangueSituation.LOG.debug("updating adresse from " + adresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "adresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "adresse");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
      _EOMangueSituation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure value) {
    if (_EOMangueSituation.LOG.isDebugEnabled()) {
      _EOMangueSituation.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  

  public static EOMangueSituation createMangueSituation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer numQuotite
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumStructure structure) {
    EOMangueSituation eo = (EOMangueSituation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueSituation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNumQuotite(numQuotite);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setStructureRelationship(structure);
    return eo;
  }

  public static NSArray<EOMangueSituation> fetchAllMangueSituations(EOEditingContext editingContext) {
    return _EOMangueSituation.fetchAllMangueSituations(editingContext, null);
  }

  public static NSArray<EOMangueSituation> fetchAllMangueSituations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueSituation.fetchMangueSituations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueSituation> fetchMangueSituations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueSituation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueSituation> eoObjects = (NSArray<EOMangueSituation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueSituation fetchMangueSituation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueSituation.fetchMangueSituation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueSituation fetchMangueSituation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueSituation> eoObjects = _EOMangueSituation.fetchMangueSituations(editingContext, qualifier, null);
    EOMangueSituation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueSituation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueSituation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueSituation fetchRequiredMangueSituation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueSituation.fetchRequiredMangueSituation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueSituation fetchRequiredMangueSituation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueSituation eoObject = _EOMangueSituation.fetchMangueSituation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueSituation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueSituation localInstanceIn(EOEditingContext editingContext, EOMangueSituation eo) {
    EOMangueSituation localInstance = (eo == null) ? null : (EOMangueSituation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
