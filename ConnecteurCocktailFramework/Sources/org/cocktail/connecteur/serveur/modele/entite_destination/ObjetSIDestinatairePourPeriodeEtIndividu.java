/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.Constantes;
import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImportPourIndividuEtPeriode;

import com.webobjects.foundation.NSTimestamp;

/** Modelise un objet avec une date de debut et de fin et un individu. Les methodes de validite
 * des objets n'etant pas implementees dans la version courante de Mangue, elles sont surchargees.
 * Cette classe est &grave; modifier apr&egrave;s evolution de Mangue.
 * @author christine
 *
 */
/** 23/09/2011 - Cette classe est &grave; modifier le jour o&ugrave; le temoin temValide est gere dans Mangue.
 * Les fetchs de toutes les sous-classes sont à modifier pour prendre en compte le temoin temValide */
public abstract class ObjetSIDestinatairePourPeriodeEtIndividu extends ObjetPourSIDestinataireAvecDates {
	public String temValide() {
		return CocktailConstantes.VRAI;
	}
	public void setTemValide(String value) {
	}
	
	public NSTimestamp dateDebut() {
		return (NSTimestamp)storedValueForKey("dateDebut");
	}

	public void setDateDebut(NSTimestamp value) {
		takeStoredValueForKey(value, "dateDebut");
	}

	public NSTimestamp dateFin() {
		return (NSTimestamp)storedValueForKey("dateFin");
	}

	public void setDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, "dateFin");
	}
	
	public EOGrhumIndividu individu() {
		return (EOGrhumIndividu)storedValueForKey("individu");
	}

	public void setIndividu(EOGrhumIndividu value) {
		takeStoredValueForKey(value, "individu");
	}
	// méthodes ajoutées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		setTemValide(CocktailConstantes.VRAI);

		ObjetImportPourIndividuEtPeriode objetImport = (ObjetImportPourIndividuEtPeriode)recordImport;
		EOGrhumIndividu individuGrhum = null;
		if (objetImport.individu()==null)
			throw new Exception("Pas d'individu dans objetImport");
		
		// A ce stade, l'individu grhum est dans le SI Destinataire
		individuGrhum = EOIndividuCorresp.individuGrhum(editingContext(), objetImport.individu().idSource());
		if (individuGrhum != null) {
			addObjectToBothSidesOfRelationshipWithKey(individuGrhum, "individu");
		} else {
			throw new Exception("Pas d'individu dans le SI correspondant a l'identifiant " + objetImport.individu().idSource());
		}
	}
}
