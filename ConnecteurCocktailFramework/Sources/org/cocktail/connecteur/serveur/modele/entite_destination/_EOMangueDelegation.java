// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueDelegation.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueDelegation extends ObjetSIDestinatairePourPeriodeEtIndividu {
	public static final String ENTITY_NAME = "MangueDelegation";

	// Attributes
	public static final String C_MOD_DELEGATION_KEY = "cModDelegation";
	public static final String C_MOT_DELEGATION_KEY = "cMotDelegation";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TYPE_MODAL_DELEGATION_KEY = "cTypeModalDelegation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CONV_DELEGATION_KEY = "dConvDelegation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_DELEGATION_KEY = "lieuDelegation";
	public static final String MONTANT_DELEGATION_KEY = "montantDelegation";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String TEM_ANNUEL_DELEGATION_KEY = "temAnnuelDelegation";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_TYPE_MOD_DELEGATION_KEY = "toTypeModDelegation";
	public static final String TO_TYPE_MOTIF_DELEGATION_KEY = "toTypeMotifDelegation";

  private static Logger LOG = Logger.getLogger(_EOMangueDelegation.class);

  public EOMangueDelegation localInstanceIn(EOEditingContext editingContext) {
    EOMangueDelegation localInstance = (EOMangueDelegation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cModDelegation() {
    return (String) storedValueForKey("cModDelegation");
  }

  public void setCModDelegation(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating cModDelegation from " + cModDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "cModDelegation");
  }

  public String cMotDelegation() {
    return (String) storedValueForKey("cMotDelegation");
  }

  public void setCMotDelegation(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating cMotDelegation from " + cMotDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "cMotDelegation");
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cTypeModalDelegation() {
    return (String) storedValueForKey("cTypeModalDelegation");
  }

  public void setCTypeModalDelegation(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating cTypeModalDelegation from " + cTypeModalDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeModalDelegation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dConvDelegation() {
    return (NSTimestamp) storedValueForKey("dConvDelegation");
  }

  public void setDConvDelegation(NSTimestamp value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating dConvDelegation from " + dConvDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "dConvDelegation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lieuDelegation() {
    return (String) storedValueForKey("lieuDelegation");
  }

  public void setLieuDelegation(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating lieuDelegation from " + lieuDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDelegation");
  }

  public java.math.BigDecimal montantDelegation() {
    return (java.math.BigDecimal) storedValueForKey("montantDelegation");
  }

  public void setMontantDelegation(java.math.BigDecimal value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating montantDelegation from " + montantDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "montantDelegation");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer numQuotite() {
    return (Integer) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Integer value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }

  public String temAnnuelDelegation() {
    return (String) storedValueForKey("temAnnuelDelegation");
  }

  public void setTemAnnuelDelegation(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating temAnnuelDelegation from " + temAnnuelDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnuelDelegation");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
    	_EOMangueDelegation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
      _EOMangueDelegation.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne toRne() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
      _EOMangueDelegation.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation toTypeModDelegation() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation)storedValueForKey("toTypeModDelegation");
  }

  public void setToTypeModDelegationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
      _EOMangueDelegation.LOG.debug("updating toTypeModDelegation from " + toTypeModDelegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeModDelegation oldValue = toTypeModDelegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeModDelegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeModDelegation");
    }
  }
  
  public org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation toTypeMotifDelegation() {
    return (org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation)storedValueForKey("toTypeMotifDelegation");
  }

  public void setToTypeMotifDelegationRelationship(org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation value) {
    if (_EOMangueDelegation.LOG.isDebugEnabled()) {
      _EOMangueDelegation.LOG.debug("updating toTypeMotifDelegation from " + toTypeMotifDelegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeMotDelegation oldValue = toTypeMotifDelegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeMotifDelegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeMotifDelegation");
    }
  }
  

  public static EOMangueDelegation createMangueDelegation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueDelegation eo = (EOMangueDelegation) EOUtilities.createAndInsertInstance(editingContext, _EOMangueDelegation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueDelegation> fetchAllMangueDelegations(EOEditingContext editingContext) {
    return _EOMangueDelegation.fetchAllMangueDelegations(editingContext, null);
  }

  public static NSArray<EOMangueDelegation> fetchAllMangueDelegations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueDelegation.fetchMangueDelegations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueDelegation> fetchMangueDelegations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueDelegation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueDelegation> eoObjects = (NSArray<EOMangueDelegation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueDelegation fetchMangueDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDelegation.fetchMangueDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDelegation fetchMangueDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueDelegation> eoObjects = _EOMangueDelegation.fetchMangueDelegations(editingContext, qualifier, null);
    EOMangueDelegation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueDelegation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueDelegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDelegation fetchRequiredMangueDelegation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueDelegation.fetchRequiredMangueDelegation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueDelegation fetchRequiredMangueDelegation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueDelegation eoObject = _EOMangueDelegation.fetchMangueDelegation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueDelegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueDelegation localInstanceIn(EOEditingContext editingContext, EOMangueDelegation eo) {
    EOMangueDelegation localInstance = (eo == null) ? null : (EOMangueDelegation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
