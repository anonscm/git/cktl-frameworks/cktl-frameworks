// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOMangueCld.java instead.
package org.cocktail.connecteur.serveur.modele.entite_destination.conges;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOMangueCld extends MangueCongeAvecArreteAnnulation {
	public static final String ENTITY_NAME = "MangueCld";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_CLD_KEY = "dComMedCld";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FIN_ANTICIPEE_KEY = "dFinAnticipee";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_EN_CAUSE_KEY = "temEnCause";
	public static final String TEM_FRACTIONNE_KEY = "temFractionne";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PROLONG_CLD_KEY = "temProlongCld";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONGE_ANNULATION_KEY = "toCongeAnnulation";

  private static Logger LOG = Logger.getLogger(_EOMangueCld.class);

  public EOMangueCld localInstanceIn(EOEditingContext editingContext) {
    EOMangueCld localInstance = (EOMangueCld)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedCld() {
    return (NSTimestamp) storedValueForKey("dComMedCld");
  }

  public void setDComMedCld(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dComMedCld from " + dComMedCld() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedCld");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dFinAnticipee() {
    return (NSTimestamp) storedValueForKey("dFinAnticipee");
  }

  public void setDFinAnticipee(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dFinAnticipee from " + dFinAnticipee() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAnticipee");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temEnCause() {
    return (String) storedValueForKey("temEnCause");
  }

  public void setTemEnCause(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating temEnCause from " + temEnCause() + " to " + value);
    }
    takeStoredValueForKey(value, "temEnCause");
  }

  public String temFractionne() {
    return (String) storedValueForKey("temFractionne");
  }

  public void setTemFractionne(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating temFractionne from " + temFractionne() + " to " + value);
    }
    takeStoredValueForKey(value, "temFractionne");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temProlongCld() {
    return (String) storedValueForKey("temProlongCld");
  }

  public void setTemProlongCld(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating temProlongCld from " + temProlongCld() + " to " + value);
    }
    takeStoredValueForKey(value, "temProlongCld");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
    	_EOMangueCld.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
      _EOMangueCld.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld toCongeAnnulation() {
    return (org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld)storedValueForKey("toCongeAnnulation");
  }

  public void setToCongeAnnulationRelationship(org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld value) {
    if (_EOMangueCld.LOG.isDebugEnabled()) {
      _EOMangueCld.LOG.debug("updating toCongeAnnulation from " + toCongeAnnulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.connecteur.serveur.modele.entite_destination.conges.EOMangueCld oldValue = toCongeAnnulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCongeAnnulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCongeAnnulation");
    }
  }
  

  public static EOMangueCld createMangueCld(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temConfirme
, String temEnCause
, String temFractionne
, String temGestEtab
, String temProlongCld
, String temValide
, org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu individu) {
    EOMangueCld eo = (EOMangueCld) EOUtilities.createAndInsertInstance(editingContext, _EOMangueCld.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemConfirme(temConfirme);
		eo.setTemEnCause(temEnCause);
		eo.setTemFractionne(temFractionne);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemProlongCld(temProlongCld);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOMangueCld> fetchAllMangueClds(EOEditingContext editingContext) {
    return _EOMangueCld.fetchAllMangueClds(editingContext, null);
  }

  public static NSArray<EOMangueCld> fetchAllMangueClds(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMangueCld.fetchMangueClds(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMangueCld> fetchMangueClds(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMangueCld.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMangueCld> eoObjects = (NSArray<EOMangueCld>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMangueCld fetchMangueCld(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCld.fetchMangueCld(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCld fetchMangueCld(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMangueCld> eoObjects = _EOMangueCld.fetchMangueClds(editingContext, qualifier, null);
    EOMangueCld eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMangueCld)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MangueCld that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCld fetchRequiredMangueCld(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMangueCld.fetchRequiredMangueCld(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMangueCld fetchRequiredMangueCld(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMangueCld eoObject = _EOMangueCld.fetchMangueCld(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MangueCld that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMangueCld localInstanceIn(EOEditingContext editingContext, EOMangueCld eo) {
    EOMangueCld localInstance = (eo == null) ? null : (EOMangueCld)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
