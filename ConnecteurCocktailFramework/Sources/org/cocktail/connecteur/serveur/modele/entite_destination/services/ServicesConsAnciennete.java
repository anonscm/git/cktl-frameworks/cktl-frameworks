package org.cocktail.connecteur.serveur.modele.entite_destination.services;

import org.cocktail.connecteur.serveur.modele.correspondance.EOIndividuCorresp;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueConservationAnciennete;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOMangueElementCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class ServicesConsAnciennete {

	private EOMangueConservationAnciennete conservationAnciennete;

	public EOMangueConservationAnciennete getConservationAnciennete() {
		return conservationAnciennete;
	}

	public void setConservationAnciennete(
			EOMangueConservationAnciennete conservationAnciennete) {
		this.conservationAnciennete = conservationAnciennete;
	}
	
	public void intialisationServicesConsAnciennete(EOMangueElementCarriere eltCarriere, EOGrhumIndividu individu, EOElementCarriere element) {
		EOEditingContext edc = eltCarriere.editingContext();
		
		NSTimestamp dateActuelle = new NSTimestamp();
		NSTimestamp dCreation = dateActuelle;
		NSTimestamp dModification = dateActuelle;
		
		conservationAnciennete = EOMangueConservationAnciennete.createMangueConservationAnciennete(edc, "O", eltCarriere, individu);
		if (element.repAncEchAnnees() != null) {
			conservationAnciennete.setAncNbAnnees(element.repAncEchAnnees());
		} else {
			conservationAnciennete.setAncNbAnnees(new Integer(0));
		}
		if (element.repAncEchMois() != null) {
			conservationAnciennete.setAncNbMois(element.repAncEchMois());
		} else {
			conservationAnciennete.setAncNbMois(new Integer(0));
		}
		if (element.repAncEchJours() != null) {
			conservationAnciennete.setAncNbJours(element.repAncEchJours());
		} else {
			conservationAnciennete.setAncNbJours(new Integer(0));
		}
		
		conservationAnciennete.setTemValide("O");
		conservationAnciennete.setTemUtilise("O");
		
		conservationAnciennete.setDCreation(dCreation);
		conservationAnciennete.setDModification(dModification);
	}
}
