//EOReliquatsAnciennete.java
//Created on Fri Feb 21 09:47:09  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur.modele.entite_destination;

import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOMotifReduction;
import org.cocktail.connecteur.serveur.modele.correspondance.EOElementCarriereCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.EOReliquatsAnciennete;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOMangueReliquatsAnciennete extends _EOMangueReliquatsAnciennete {

	public EOMangueReliquatsAnciennete() {
		super();
	}

	// méthodes ajoutées
	public String motif() {
		
		if (motifReduction() != null) {
			return (String)motifReduction().valueForKey(EOMotifReduction.MORE_LIBELLE_KEY);
		} else {
			return null;
		}
	}
	public void setMotif(String value) {
		if (value == null) {
			if (motifReduction() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(motifReduction(), MOTIF_REDUCTION_KEY);
			}
		} else {
			EOGenericRecord motif = Finder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOMotifReduction.ENTITY_NAME, EOMotifReduction.MORE_LIBELLE_KEY, value);
			addObjectToBothSidesOfRelationshipWithKey(motif, MOTIF_REDUCTION_KEY);
		}
	}
	public void supprimerRelations() {
		if (motifReduction() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(motifReduction(), MOTIF_REDUCTION_KEY);
		}
	}
	// Méthodes protégées
	protected void initialiserObjet(ObjetImport recordImport) throws Exception {
		super.initialiserObjet(recordImport);
		EOReliquatsAnciennete anciennete = (EOReliquatsAnciennete)recordImport;
		
		EOMangueElementCarriere elementCarriere = EOElementCarriereCorresp.mangueElementCarriere(editingContext(), anciennete.elementCarriere());
		if (elementCarriere != null) {
			addObjectToBothSidesOfRelationshipWithKey(elementCarriere, ELEMENT_CARRIERE_KEY);
		} else {
			throw new Exception("Pas d'element carriere dans le SI correspondant a l'identifiant idSource : " + anciennete.elementCarriere().idSource() + ", carSource : " + anciennete.elementCarriere().carSource() + ", elSource : " + anciennete.elementCarriere().elSource());
		}

		if (anciennete.motifReduction() != null) {
			addObjectToBothSidesOfRelationshipWithKey(anciennete.motifReduction(), MOTIF_REDUCTION_KEY);
		}
	}
	
	
	
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		
		setTemValide("O");
	}

	public static NSArray findForIndividuEtPeriode(EOEditingContext editingContext, EOGrhumIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		}
		if (debutPeriode !=  null) {
			qualifiers.addObject(Finder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
}
