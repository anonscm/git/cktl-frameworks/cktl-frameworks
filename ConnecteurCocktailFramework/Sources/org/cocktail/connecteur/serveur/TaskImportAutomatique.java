package org.cocktail.connecteur.serveur;

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

import java.io.File;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.cocktail.common.LogManager;
import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.CocktailUtilities;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.StringCtrl;
import org.cocktail.connecteur.common.metier.repositories.MainManager;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOBap;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOCivilite;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EODepartement;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EORne;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSituationFamiliale;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOSituationMilitaire;
import org.cocktail.connecteur.common.modele.grhum.nomenclature.EOTypeContratTravail;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.importer.moteur.erreurs.ErreurManager;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOCarriere;
import org.cocktail.connecteur.serveur.modele.entite_import.EOContrat;
import org.cocktail.connecteur.serveur.modele.entite_import.EOEmploi;
import org.cocktail.connecteur.serveur.modele.entite_import.EOIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.EOStructure;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.importer.EOImportEntites;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXUtilities;

public class TaskImportAutomatique {

	private final static String PATH_IMPORT = "PATH_IMPORT";
	public final static String NOTIFICATION_MESSAGE_PREPARATION = "NotificationMessage";

	private static TaskImportAutomatique sharedInstance;

	private static EOEditingContext editingContextPermanent = new EOEditingContext();;

	private Timer currentTimer;
	private ServerThreadManager threadCourant;

	public TaskImportAutomatique(EOEditingContext edc) {
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TaskImportAutomatique sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null)
			sharedInstance = new TaskImportAutomatique(edc);
		return sharedInstance;
	}

	public static EOEditingContext editingContextPermanent() {
		return editingContextPermanent;
	}

	/**
	 * 
	 * @return
	 */
	public Timer getTimer() {
		return currentTimer;
	}

	/**
	 * 
	 * @param aTimer
	 */
	public void setTimer(Timer aTimer) {
		currentTimer = aTimer;
	}

	/**
	 * 
	 * @param message
	 */
	private void informerThread(String message) {

		if (message != null) {
			if (threadCourant != null)
				threadCourant.setMessage(message);
			else
				LogManager.logInformation((new Date()) + ",   - " + message);
		}
	}

	public ResultatImport importerFichier(String repertoire, String nomFichier, boolean effectueImport, boolean effectueTransfert, String plSQLProc) {
		String idUser = EOImportParametres.valeurParametrePourCle(editingContextPermanent, "ID_USER_DEFAUT");
		if (idUser == null) {
			LogManager.logInformation("Pas de responsable defini pour l'import automatique");
			return null;
		}
		EOGrhumIndividu individu = EOGrhumIndividu.individuAvecPersId(editingContextPermanent, new Integer(idUser));
		if (individu == null) {
			LogManager.logInformation("Responsable invalide pour l'import automatique");
			return null;
		}

		AutomateImport.sharedInstance().setTraitementAutomatiqueActif(true);

		ResultatImport resultat = importerFichier(editingContextPermanent, repertoire, nomFichier, editingContextPermanent.globalIDForObject(individu), true,
				effectueImport, effectueTransfert, plSQLProc);

		return resultat;
	}

	/**
	 * 
	 */
	public void importerEntite(EOImportEntites myEntite) {
		LogManager.logInformation((new Date()) + ",   - Début TaskImportAutomatique.importerEntite");

		if (myEntite.estImportee()) {
			LogManager.logInformation((new Date()) + ",   - Entité " + myEntite.entityName() + " déjà importée.");
			return;
		}

		informerThread("Traitement ENTITE : " + myEntite.impeEntite());

		try {
			String resultatGlobal = "";

			if (myEntite.impeNomFichier() == null) {
				LogManager.logInformation("Nom de fichier import vide");
				return;
			}
			String pathFichier = EOImportParametres.valeurParametrePourCle(editingContextPermanent, PATH_IMPORT);
			;
			String nomFichier = myEntite.impeNomFichier();

			if (!nomFichier.endsWith("xml")) {
				nomFichier = nomFichier.concat(".xml");
			}

			String plSQLTransfertProc = "";
			if (myEntite.estPhaseTransfertPlsql())
				plSQLTransfertProc = myEntite.impeProcTransfert();

			ResultatImport resultat = importerFichier(pathFichier, nomFichier, myEntite.estPhaseImport(), myEntite.estPhaseTransfert(), plSQLTransfertProc);
			if (resultat != null) {
				resultatGlobal += myEntite.impeEntite() + " : " + resultat.resultatOperation() + "\n" + resultat.diagnostic() + "\n";
				LogManager.logInformation((new Date()) + ",   - " + resultatGlobal);

				if (myEntite.estPhaseImport()) {
					myEntite.setImpeDureeTransfert(resultat.getDureeImport());
					myEntite.setImpeDateTransfert(new NSTimestamp());
				}
				if (myEntite.estPhaseTransfert()) {
					myEntite.setImpeDureeImport(resultat.getDureeTransfert());
					myEntite.setImpeDateImport(new NSTimestamp());
				}
				// myEntite.setEstImportee(true);
				myEntite.editingContext().saveChanges();
			}
		} catch (Exception e) {
			LogManager.logException(e);
			e.printStackTrace();
		}
		LogManager.logInformation((new Date()) + ",   - Fin TaskImportAutomatique.importerFichierSimple");
	}

	/**
	 * 
	 */
	public void stopProgrammation() {

		try {
			if (getTimer() != null) {
				getTimer().cancel();
				setTimer(null);
			}
			LogManager.logInformation("-----------------------------------------------");
			LogManager.logInformation("  Arrêt de la programmation de l'import automatique");
			LogManager.logInformation("-----------------------------------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void startImport() {

		LogManager.logInformation((new Date()) + " Début TaskImportAutomatique.startImport()");
		ImportAutomatiqueTask myTask = new ImportAutomatiqueTask();
		myTask.run();
		LogManager.logInformation((new Date()) + " Fin TaskImportAutomatique.startImport()");

	}

	/**
	 * 
	 * @param threader
	 */
	public void startImportManuel(ServerThreadManager threadCourant) {

		this.threadCourant = threadCourant;
		informerThread("TEST START IMPORT MANUEL");
		importerEntites();

	}

	/**
	 * 
	 * @param jours
	 */
	public void programmer(String heureCalcul) {

		NSTimestamp dateJour = new NSTimestamp();

		LogManager.logInformation("/**********************************************");
		LogManager.logInformation("	Programmation import automatique - " + DateCtrl.dateToString(dateJour, "%d/%m/%Y %H:%M:%S"));
		LogManager.logInformation("	L'import automatique se lancera à " + heureCalcul);

		if (getTimer() == null)
			currentTimer = new Timer(true);

		// Recuperation du delai entre l'heure actuelle et l'heure de programmation
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.setTime(dateJour);
		long flag1 = calendar1.getTimeInMillis();

		GregorianCalendar calendar2 = new GregorianCalendar();
		NSTimestamp dateProg = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp(), "%d/%m/%Y") + " " + heureCalcul, "%d/%m/%Y %H:%M");
		calendar2.setTime(dateProg);
		long flag2 = calendar2.getTimeInMillis();

		long delay = flag2 - flag1;

		if (delay < 0) {
			delay = 86400000 + delay;
		}
		LogManager.logInformation("	Demarrage dans " + ((delay / 1000) / 60) + " minutes ...");
		LogManager.logInformation(" *********************************************/");

		long period = 86400000; // Toutes les 24 Heures

		currentTimer.scheduleAtFixedRate(new ImportAutomatiqueTask(), delay, period);

	}

	private static boolean pathExiste(String unPath) {
		if (unPath == null || unPath.length() == 0) {
			return false;
		}
		File file = new File(unPath);
		return file.exists();
	}

	/**
	 * 
	 * @author cpinsard
	 * 
	 */
	public class ImportAutomatiqueTask extends TimerTask {

		public ImportAutomatiqueTask() {
		}

		public void run() {

			LogManager.logInformation((new Date()) + " Début ImportAutomatiqueTask.run()");
			importerEntites();
			try {
				LogManager.logInformation((new Date()) + " Appel à la procédure stockée  OnImportTermine");
				EOUtilities.executeStoredProcedureNamed(editingContextPermanent, "onImportTermine",new NSDictionary());
				LogManager.logInformation((new Date()) + " Fin de la procédure stockée  OnImportTermine");
			} catch (Exception e) {
				LogManager.logErreur("Erreur lors de l'exécution de la procédure stockée OnImportTermine");
				LogManager.logException(e);
			}
			LogManager.logInformation((new Date()) + " Fin ImportAutomatiqueTask.run()");

		}

	}

	public void nettoyerBases() {
		try {
			String cleanImport = EOImportParametres.valeurParametrePourCle(editingContextPermanent, EOImportParametres.KEY_IMPORT_AUTO_CLEAN_IMPORT);
			if (cleanImport != null && cleanImport.equals("O")) {
				LogManager.logInformation("NETTOYAGE DE LA BASE IMPORT ... ");
				EOUtilities.executeStoredProcedureNamed(editingContextPermanent, "nettoyerImport", new NSDictionary());
			}
			String cleanDestination = EOImportParametres.valeurParametrePourCle(editingContextPermanent, EOImportParametres.KEY_IMPORT_AUTO_CLEAN_DEST);
			if (cleanDestination != null && cleanDestination.equals("O")) {
				LogManager.logInformation("NETTOYAGE DE LA BASE DESTINATION ... ");
				EOUtilities.executeStoredProcedureNamed(editingContextPermanent, "nettoyerDestination", new NSDictionary());
			}
		} catch (Exception e) {
			LogManager.logInformation("IMPORT AUTOMATIQUE - ERREUR DE NETTOYADE DE BASE ! - PAS DE TRAITEMENT !");
			LogManager.logInformation(e.getMessage());
			e.printStackTrace();
			return;
		}
	}

	/**
	 * 
	 */
	private void importerEntites() {

		LogManager.logInformation("-----------------------------------------------------");
		LogManager.logInformation("	 >>>>> DEMARRAGE IMPORT AUTOMATIQUE <<<<<");
		LogManager.logInformation("-----------------------------------------------------");

		String idUser = EOImportParametres.valeurParametrePourCle(editingContextPermanent, "ID_USER_DEFAUT");
		if (idUser == null) {
			LogManager.logInformation("STOP - Pas de responsable defini pour l'import automatique");
			return;
		}
		EOGrhumIndividu individu = EOGrhumIndividu.individuAvecPersId(editingContextPermanent, new Integer(idUser));
		if (individu == null) {
			LogManager.logInformation("STOP - Responsable invalide pour l'import automatique");
			return;
		}

		try {
			String useImportAuto = EOImportParametres.valeurParametrePourCle(editingContextPermanent, EOImportParametres.KEY_USE_IMPORT_AUTO);
			if (useImportAuto != null && useImportAuto.equals("N")) {
				LogManager.logInformation("IMPORT AUTOMATIQUE DESACTIVE - PAS DE TRAITEMENT !");
				return;
			}

			nettoyerBases();

			// Path import
			String pathImportAuto = EOImportParametres.valeurParametrePourCle(editingContextPermanent, PATH_IMPORT);

			// Chemin pour les homonymes
			String pathHomomnyme = pathImportAuto + "/";
			pathHomomnyme = StringCtrl.replace(pathHomomnyme, "//", "/") + "Homonymes";
			CocktailUtilities.verifierPathEtCreer(pathHomomnyme);

			// Recuperation des entites
			NSArray<EOImportEntites> entites = EOImportEntites.find(editingContextPermanent);

			ErreurManager.instance().reset();
			resetCache();
			
			for (EOImportEntites myEntite : entites) {
				importerEntite(myEntite);
			}

			ErreurManager.instance().save();

			LogManager.logInformation("-----------------------------------------------------");
			LogManager.logInformation("	 >>>>> IMPORT AUTOMATIQUE TERMINE <<<<<");
			LogManager.logInformation("-----------------------------------------------------");

		} catch (Exception e) {
			LogManager.logException(e);
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param editingContext
	 * @param nomFichierImport
	 * @param responsableID
	 * @param prendreEnCompteEntitesTronquees
	 * @param phaseImport
	 *            Booleen indiquant si l'import doit être lancé
	 * @param phaseTransfert
	 *            Booleen indiquant si le transfert doit être lancé
	 * @param plSQLTranfertProc
	 *            Si non vide, indique que le transfert doit être lancé par la procédure SQL indiquée
	 * @return
	 */
	private static ResultatImport importerFichier(EOEditingContext editingContext, String repertoire, String nomFichierImport, EOGlobalID responsableID,
			boolean prendreEnCompteEntitesTronquees, boolean phaseImport, boolean phaseTransfert, String plSQLTranfertProc) {

		if (!pathExiste(repertoire + '/' + nomFichierImport)) {
			LogManager.logInformation(" \tFichier non trouvé : " + repertoire + '/' + nomFichierImport);
			return null;
		}

		int etape = AutomateImport.ETAPE_PARSING;

		NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_MESSAGE_PREPARATION, "Préparation de l'import"); // on les affiche par 10

		AutomateImport.sharedInstance().init(repertoire, nomFichierImport, null, null);

		MainManager.init(editingContext);

		ResultatImport resultat = new ResultatImport(editingContext, nomFichierImport);

		try {
			if (phaseImport)
				resultat.setResultatParsing(AutomateImport.sharedInstance().importerDonnees());

			if (phaseImport && resultat.continuerTraitement()) {
				LogManager.logInformation((new Date()) + ",   - Phase Import ...");

				NSTimestamp dateDebut = new NSTimestamp();
				etape = AutomateImport.ETAPE_IMPORT;
				resultat.setResultatImport(AutomateImport.sharedInstance().enregistrerDonneesImport(null));
				resultat.setDureeImport(CocktailUtilities.ecartSecondesEntre(dateDebut, new NSTimestamp()));
			}

			if (resultat.continuerTraitement() && phaseTransfert) {
				LogManager.logInformation((new Date()) + ",   - Phase Transfert ...");

				NSTimestamp dateDebut = new NSTimestamp();

				if (plSQLTranfertProc != null && !plSQLTranfertProc.equals("")) {
					EOUtilities.executeStoredProcedureNamed(editingContext, plSQLTranfertProc, null);
				} else {
					etape = AutomateImport.ETAPE_TRANSFERT;
					resultat.setComporteHomonymes(ObjetImport.existeHomonymes(editingContext));
					resultat.setResultatTransfert(AutomateImport.sharedInstance().enregistrerImportDansBaseDestinataire(responsableID,
							new Boolean(prendreEnCompteEntitesTronquees), new Boolean(resultat.comporteHomonymes()), null));
				}

				resultat.setDureeTransfert(CocktailUtilities.ecartSecondesEntre(dateDebut, new NSTimestamp()));
				LogManager.logInformation((new Date()) + ",   - Fin du transfert");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultat.setException(e.getMessage());
		} finally {
			if (resultat.resultatParsing() != null && resultat.resultatParsing().equals(AutomateImport.DEJA_IMPORTE) == false) {
				resultat.setDiagnostic(AutomateImport.sharedInstance().rapatrierRapport(nomFichierImport, etape));
			}
		}

		return resultat;

	}

	// Classe privée pour stocker les résultats des imports afin de pouvoir déplacer les fichiers dans les directories ad-hoc
	private static class ResultatImport {
		private EOEditingContext editingContext;
		private boolean comporteErreurs;
		private boolean comporteHomonymes;
		private boolean continuerTraitement;
		private String nomFichierImport;
		private String resultatParsing;
		private String resultatImport;
		private String resultatTransfert;
		private String exception;
		private String diagnostic;
		private long dureeImport;
		private long dureeTransfert;

		public ResultatImport(EOEditingContext editingContext, String nomFichierImport) {
			this.editingContext = editingContext;
			this.nomFichierImport = nomFichierImport;
			this.continuerTraitement = true;
			diagnostic = "";
		}

		public long getDureeImport() {
			return dureeImport;
		}

		public void setDureeImport(long dureeImport) {
			this.dureeImport = dureeImport;
		}

		public long getDureeTransfert() {
			return dureeTransfert;
		}

		public void setDureeTransfert(long dureeTransfert) {
			this.dureeTransfert = dureeTransfert;
		}

		public String nomFichierImport() {
			return nomFichierImport;
		}

		public boolean comporteErreurs() {
			return comporteErreurs;
		}

		public void setComporteErreurs(boolean comporteErreurs) {
			this.comporteErreurs = comporteErreurs;
		}

		public boolean comporteHomonymes() {
			return comporteHomonymes;
		}

		public void setComporteHomonymes(boolean comporteHomonymes) {
			this.comporteHomonymes = comporteHomonymes;
		}

		public boolean continuerTraitement() {
			return continuerTraitement;
		}

		public void setContinuerTraitement(boolean continuerTraitement) {
			this.continuerTraitement = continuerTraitement;
		}

		public String resultatParsing() {
			return resultatParsing;
		}

		public void setResultatParsing(String diagnosticParsing) {
			this.resultatParsing = diagnosticParsing;
			if (diagnosticParsing.equals(AutomateImport.FIN_PARSING) == false && diagnosticParsing.equals(AutomateImport.DEJA_IMPORTE) == false) {
				// Il s'est produit un problème
				setComporteErreurs(true);
				String continuer = EOImportParametres.valeurParametrePourCle(editingContext, "CONTINUER_APRES_ERREUR");
				setContinuerTraitement(continuer != null && continuer.equals(CocktailConstantes.VRAI));
			} else if (diagnosticParsing.equals(AutomateImport.DEJA_IMPORTE)) {
				setContinuerTraitement(false); // pas la peine d'importer les données
			} else {
				setContinuerTraitement(true);
			}
		}

		public String resultatImport() {
			return resultatImport;
		}

		public void setResultatImport(String resultat) {
			this.resultatImport = resultat;
			if (resultatImport.equals(AutomateImport.FIN_IMPORT)) {
				setContinuerTraitement(true);
			} else if (resultatImport.equals(AutomateImport.FIN_IMPORT_AVEC_ERREUR)) {
				// Il s'est produit un problème
				setComporteErreurs(true);
				String continuer = EOImportParametres.valeurParametrePourCle(editingContext, "CONTINUER_APRES_ERREUR");
				setContinuerTraitement(continuer != null && continuer.equals(CocktailConstantes.VRAI));
			} else {
				setContinuerTraitement(false);
			}
		}

		public String resultatTransfert() {
			return resultatTransfert;
		}

		public void setResultatTransfert(String resultat) {
			this.resultatTransfert = resultat;
		}

		public String exception() {
			return exception;
		}

		public void setException(String exception) {
			this.exception = exception;
		}

		public String diagnostic() {
			return diagnostic;
		}

		public void setDiagnostic(String diagnostic) {
			this.diagnostic = diagnostic;
		}

		public String resultatOperation() {
			String temp = "";
			if (resultatTransfert() != null) {
				temp = resultatTransfert();
			} else if (resultatImport() != null) {
				temp = resultatImport();
			} else if (resultatParsing() != null) {
				temp = resultatParsing();
			}
			if (exception() != null) {
				temp += "avec erreur : " + exception() + ". Vérifier le log de l'application";
			}
			return temp;
		}

		public String toString() {
			String temp = "Fichier : " + nomFichierImport + "\nParsing : " + resultatParsing;
			if (resultatImport != null) {
				temp += ", " + resultatImport;
				if (resultatTransfert != null) { // On le fait là car pour qu'il y ait transfert, il faut qu'il y ait import
					temp += ", " + resultatTransfert;
				}
			}
			if (exception != null) {
				temp += "\nException pendant le traitement : " + exception;
			}
			if (diagnostic != null) {
				temp += "\nDetail des traitements\n" + diagnostic;
			}
			return temp;
		}

	}

	/**
	 * 
	 * @param nomFichier
	 * @param clePath
	 */
	private static void transfererFichier(String nomFichier, String clePath) {
		String ancienPath = EOImportParametres.valeurParametrePourCle(editingContextPermanent, "PATH_IMPORT");
		String path = EOImportParametres.valeurParametrePourCle(editingContextPermanent, clePath);
		if (ancienPath == null || path == null) {
			return;
		}
		if (ancienPath.substring(ancienPath.length() - 1).equals(File.separator) == false) {
			ancienPath = ancienPath + File.separator;
		}
		if (path.substring(path.length() - 1).equals(File.separator) == false) {
			path = path + File.separator;
		}
		File ancienFile = new File(ancienPath + nomFichier);
		ancienFile.renameTo(new File(path + nomFichier));
	}

	private void resetCache() {
		if (EOImportParametres.cacheMemoireNiveauBasOuPlus()) {
			LogManager.logInformation((new Date()) + ",   - Reset cache");
			EOBap.resetCache();
			EOCivilite.resetCache();
			EODepartement.resetCache();
			EOImportParametres.resetCache();
			EOSituationFamiliale.resetCache();
			EOSituationMilitaire.resetCache();
			EOTypeContratTravail.resetCache();
			EOIndividu.resetCache();
			EORne.resetCache();
			EOContrat.resetCache();
			EOCarriere.resetCache();
			EOStructure.resetCache();
			EOEmploi.resetCache();
		}
	}

}