/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur;

import java.io.File;
import java.io.FilenameFilter;

import org.cocktail.connecteur.common.CocktailConstantes;
import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.common.LogManager;
import org.cocktail.connecteur.common.modele.Finder;
import org.cocktail.connecteur.importer.moteur.AutomateImport;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumCompte;
import org.cocktail.connecteur.serveur.modele.entite_destination.EOGrhumIndividu;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.importer.EOCible;
import org.cocktail.connecteur.serveur.modele.importer.EOFichierImport;
import org.cocktail.connecteur.serveur.modele.importer.EOImportParametres;
import org.cocktail.connecteur.serveur.modele.importer.EOSource;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Classe utilitaire pour gerer l'import automatique des donnees */
public class ImportAutomatique {
	
	private final static String PATH_FICHIER_APRES_ERREUR = "PATH_FICHIER_ERREUR";
	private final static String PATH_FICHIER_APRES_IMPORT = "PATH_FICHIER_APRES_IMPORT";
	private final static String PATH_FICHIER_POUR_HOMONYME = "PATH_HOMONYME";
	private static final String MESSAGE_TEST_IMPORT = ">>>>> Service Import automatique disponible";

	/** Import automatique des donnees : scan le directory d'import et importe successivement
	 * les fichiers correspondant a des entites de ce fichier
	 * 
	 * @param nomApplicationSource
	 * @param nomApplicationDestin
	 * @param prendreEnCompteEntitesTronquees
	 * @return
	 */ 
	public static String importerDonnees(String nomApplicationSource,String nomApplicationDestin,boolean prendreEnCompteEntitesTronquees) {
		LogManager.resetPourModeAutomatique();
		String resultat = importer(nomApplicationSource,nomApplicationDestin,prendreEnCompteEntitesTronquees);
		envoyerMail("Import de " + nomApplicationSource + " vers " + nomApplicationDestin, resultat);
		LogManager.logDetail(resultat);
		return resultat;
	}
	/** Pour verifier que la communication via les WebServices fonctionne bien */
	public static String testImportAutomatique() {
		envoyerMail("Test import automatique",MESSAGE_TEST_IMPORT);
		LogManager.logDetail(MESSAGE_TEST_IMPORT);
		return MESSAGE_TEST_IMPORT;
	}
	// Méthodes privées
	private static String importer(String nomApplicationSource,String nomApplicationDestin,boolean prendreEnCompteEntitesTronquees) {
		// Pour ne pas avoir de problème de saturation mémoire, on réalloue un editingContext pour chaque fichier traité
		EOEditingContext editingContextPermanent = new EOEditingContext();
		EOSource source = (EOSource)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContextPermanent, "Source", "srcLibelle", nomApplicationSource);
		if (source == null) {
			return nomApplicationSource + " application source inconnue";
		}
		EOCible cible = (EOCible)Finder.rechercherObjetAvecAttributEtValeurEgale(editingContextPermanent, "Cible", "cibLibelle", nomApplicationDestin);
		if (cible == null) {
			return nomApplicationDestin + " application destination inconnue";
		}
		// Vérifier si le responsable est défini
		String idUser = EOImportParametres.valeurParametrePourCle(editingContextPermanent, "ID_USER_DEFAUT");
		if (idUser == null) {
			return "pas de responsable defini pour l'import automatique";
		}
		EOGrhumIndividu individu = EOGrhumIndividu.individuAvecPersId(editingContextPermanent, new Integer(idUser));
		if (individu == null) {
			return "responsable invalide pour l'import automatique";
		}
		// Vérifier si le mode automatique est possible
		String mode = EOImportParametres.valeurParametrePourCle(editingContextPermanent, "MODE_AUTOMATIQUE");
		if (mode == null || mode.equals(CocktailConstantes.FAUX)) {
			return "l'import ne peut être lancé en mode automatique";
		}
		String message = AutomateImport.sharedInstance().verifierPathPourCle(PATH_FICHIER_APRES_ERREUR);
		message += AutomateImport.sharedInstance().verifierPathPourCle(PATH_FICHIER_APRES_IMPORT);
		message += AutomateImport.sharedInstance().verifierPathPourCle(PATH_FICHIER_POUR_HOMONYME);
		if (message.length() > 0) {
			return message;
		}
		if (AutomateImport.sharedInstance().peutImporter()) {	// Si il s'est produit une erreur et qu'un import précédent a été arrêté, on ne pourra pas continuer
			NSArray listeFichiers = recupererNomsFichiers();
			if (listeFichiers == null) {
				return "Path import invalide ou erreur dans la comparaison des fichiers";
			} 
			AutomateImport.sharedInstance().setTraitementAutomatiqueActif(true);
			String resultatGlobal = "";
			boolean continuer = true;
			java.util.Enumeration e = listeFichiers.objectEnumerator();
			// Selon les choix de l'utilisateur, on continue le traitement en supprimant les données de l'import
			// ou on arrête le traitement en laissant en l'état de manière à ce que l'utilisateur puisse faire
			// des corrections manuelles
			while (e.hasMoreElements() && continuer) {
				String nomFichierImport = (String)e.nextElement();
				EOEditingContext editingContext = new EOEditingContext();
				ResultatImport resultat = importerFichier(editingContext, nomFichierImport, editingContextPermanent.globalIDForObject(source), editingContextPermanent.globalIDForObject(cible), editingContextPermanent.globalIDForObject(individu), prendreEnCompteEntitesTronquees);
				resultatGlobal += nomFichierImport + " : " + resultat.resultatOperation() + "\n" + resultat.diagnostic() + "\n";

				if (resultat.comporteHomonymes() || resultat.comporteErreurs()) {
					if (resultat.comporteErreurs()) {
						// Transférer le fichier dans le directory des erreurs
						transfererFichier(nomFichierImport,PATH_FICHIER_APRES_ERREUR);
					} else if (resultat.comporteHomonymes()) {
						// Le transfert s'est terminé mais il restait des homonymes
						transfererFichier(nomFichierImport,PATH_FICHIER_POUR_HOMONYME);
					}
					// Vérifier si on continue ou non le traitement
					continuer = continuerImportFichiers(editingContext);
				} else {
					transfererFichier(nomFichierImport,PATH_FICHIER_APRES_IMPORT);
				}
			}
			AutomateImport.sharedInstance().setTraitementAutomatiqueActif(false);
			if (resultatGlobal.length() == 0) {
				return "Pas de fichier à importer";
			} else {
				return resultatGlobal;
			}
		} else {
			return "Autre import en cours";
		}
	}
	// Lance l'import d'un fichier
	private static ResultatImport importerFichier(EOEditingContext editingContext,String nomFichierImport,EOGlobalID sourceID,EOGlobalID cibleID,EOGlobalID responsableID,boolean prendreEnCompteEntitesTronquees) {
		int etape = AutomateImport.ETAPE_PARSING;
		AutomateImport.sharedInstance().init(null,nomFichierImport,sourceID, cibleID);
		ResultatImport resultat = new ResultatImport(editingContext,nomFichierImport);
		try {
			resultat.setResultatParsing(AutomateImport.sharedInstance().importerDonnees());
			if (resultat.continuerTraitement()) {	
				etape = AutomateImport.ETAPE_IMPORT;
				resultat.setResultatImport(AutomateImport.sharedInstance().enregistrerDonneesImport(null));
			} 
			if (resultat.continuerTraitement()) {
				etape = AutomateImport.ETAPE_TRANSFERT;
				resultat.setComporteHomonymes(ObjetImport.existeHomonymes(editingContext));
				resultat.setResultatTransfert(AutomateImport.sharedInstance().enregistrerImportDansBaseDestinataire(responsableID, new Boolean(prendreEnCompteEntitesTronquees), new Boolean(resultat.comporteHomonymes()), null));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultat.setException(e.getMessage());
		} finally {
			if (resultat.resultatParsing() != null && resultat.resultatParsing().equals(AutomateImport.DEJA_IMPORTE) == false) {
				resultat.setDiagnostic(AutomateImport.sharedInstance().rapatrierRapport(nomFichierImport, etape));
			}
		}
		
		return resultat;

	}
	
	/**
	 * 
	 * @return
	 */
	private static NSArray recupererNomsFichiers() {
		String path = EOImportParametres.valeurParametrePourCle(new EOEditingContext(), EOImportParametres.KEY_PATH_IMPORT);
		if (path != null) {
			if (path.substring(path.length() - 1).equals(File.separator) == false) {
				path = path + File.separator;
			}
			File file = new File(path);
			String[] listeFichiers = file.list(new ImportAutomatique.FileFilter());
			// Commencer par ne garder que les fichiers qui correspondent à la nomenclature d'import automatique
			NSMutableArray fichiers = new NSMutableArray();
			FichierComparator comparator = new FichierComparator();
			for (int i = 0;i < listeFichiers.length;i++) {
				String nomFichier = listeFichiers[i];
				if (comparator.estNomFichierValide(nomFichier)) {
					fichiers.addObject(nomFichier);
				}
			}
			try {
				return fichiers.sortedArrayUsingComparator(comparator);
			} catch (Exception e) {
				//LogManager.logException(e);
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	
	/**
	 * 
	 * @param nomFichier
	 * @param clePath
	 */
	private static void transfererFichier(String nomFichier,String clePath) {
		String ancienPath = EOImportParametres.valeurParametrePourCle(new EOEditingContext(), "PATH_IMPORT");
		String path = EOImportParametres.valeurParametrePourCle(new EOEditingContext(), clePath);
		if (ancienPath == null || path == null) {
			return;
		}
		if (ancienPath.substring(ancienPath.length() - 1).equals(File.separator) == false) {
			ancienPath = ancienPath + File.separator;
		}
		if (path.substring(path.length() - 1).equals(File.separator) == false) {
			path = path + File.separator;
		}
		File ancienFile = new File(ancienPath+nomFichier);
		ancienFile.renameTo(new File(path+nomFichier));
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	private static boolean continuerImportFichiers(EOEditingContext editingContext) {
		String continuer = EOImportParametres.valeurParametrePourCle(editingContext, "CONTINUER_AUTRES_IMPORTS_APRES_ERREUR");
		if (continuer != null && continuer.equals(CocktailConstantes.VRAI)) {
			// Si nécessaire, supprimer les données  de l'import pour pouvoir continuer
			EOFichierImport importCourant = (EOFichierImport)Finder.importCourant(editingContext);
			if (importCourant != null) {
				if (importCourant.donneesImportees()) {
					AutomateImport.sharedInstance().supprimerImportCourant(null);
				} else if (importCourant.transfertEnCours()) {
					AutomateImport.sharedInstance().abandonnerTransfertImportCourant(null);
				}
			}
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param texte
	 * @param message
	 */
	private static void envoyerMail(String texte,String message) {
		try {
			EOEditingContext editingContext = new EOEditingContext();
			String persId = EOImportParametres.valeurParametrePourCle(editingContext, "ID_USER_DEFAUT");
			if (persId == null) {
				return;
			}
			String sujet  = " Rapport d'import automatique (" + DateCtrl.dateToString(new NSTimestamp(),"%d/%m/%Y %H:%M") + ")";
			StringBuffer mail = new StringBuffer();	
			mail.append(texte + "\n");
			mail.append(message);
			EOGrhumCompte compte = EOGrhumCompte.rechercherComptePourId(editingContext, new Integer(persId)); 
			String from = "";
			if (compte.cptEmail() != null && compte.cptEmail().length() > 0
					&&	compte.cptDomaine() != null && compte.cptDomaine().length() > 0) {
				from = compte.cptEmail() + "@" + compte.cptDomaine();
			}
			String destinataire = from;
			CktlWebApplication application = (CktlWebApplication)WOApplication.application();
			if (application.config() != null) {
				String emetteur = application.config().stringForKey("APP_ADMIN_MAIL");
				/*if (emetteur != null) {
					from = emetteur;
				}*/
			} else {
				LogManager.logInformation("pas de config");
			}
			application.mailBus().sendMail(from,destinataire,null, sujet, message);
		} catch (Exception e) {
			LogManager.logException(e);
		}

	}
	// Classe Filtre
	public static class FileFilter implements FilenameFilter {
		public FileFilter() {
		}
		public boolean accept(File f,String name) {
			return (name.indexOf(".XML") > 0 || name.indexOf(".xml") > 0 || name.indexOf(".txt") > 0 || name.indexOf(".cvs") > 0);
		}
	}
	// Classe privée pour stocker les résultats des imports afin de pouvoir déplacer les fichiers dans les directories ad-hoc
	private static class ResultatImport  {
		private EOEditingContext editingContext;
		private boolean comporteErreurs;
		private boolean comporteHomonymes;
		private boolean continuerTraitement;
		private String nomFichierImport;
		private String resultatParsing;
		private String resultatImport;
		private String resultatTransfert;
		private String exception;
		private String diagnostic;

		public ResultatImport(EOEditingContext editingContext,String nomFichierImport) {
			this.editingContext  = editingContext;
			this.nomFichierImport = nomFichierImport;
		}
		public String nomFichierImport() {
			return nomFichierImport;
		}
		public boolean comporteErreurs() {
			return comporteErreurs;
		}
		public void setComporteErreurs(boolean comporteErreurs) {
			this.comporteErreurs = comporteErreurs;
		}
		public boolean comporteHomonymes() {
			return comporteHomonymes;
		}
		public void setComporteHomonymes(boolean comporteHomonymes) {
			this.comporteHomonymes = comporteHomonymes;
		}
		public boolean continuerTraitement() {
			return continuerTraitement;
		}
		public void setContinuerTraitement(boolean continuerTraitement) {
			this.continuerTraitement = continuerTraitement;
		}
		public String resultatParsing() {
			return resultatParsing;
		}
		public void setResultatParsing(String diagnosticParsing) {
			this.resultatParsing = diagnosticParsing;
			if (diagnosticParsing.equals(AutomateImport.FIN_PARSING) == false && diagnosticParsing.equals(AutomateImport.DEJA_IMPORTE) == false) {
				// Il s'est produit un problème
				setComporteErreurs(true);
				String continuer = EOImportParametres.valeurParametrePourCle(editingContext, "CONTINUER_APRES_ERREUR");
				setContinuerTraitement(continuer != null && continuer.equals(CocktailConstantes.VRAI));
			} else if (diagnosticParsing.equals(AutomateImport.DEJA_IMPORTE)) {
				setContinuerTraitement(false);	// pas la peine d'importer les données
			} else {
				setContinuerTraitement(true);
			}
		}

		public String resultatImport() {
			return resultatImport;
		}

		public void setResultatImport(String resultat) {
			this.resultatImport = resultat;
			if (resultatImport.equals(AutomateImport.FIN_IMPORT)) {
				setContinuerTraitement(true);
			} else if (resultatImport.equals(AutomateImport.FIN_IMPORT_AVEC_ERREUR)) {
				// Il s'est produit un problème
				setComporteErreurs(true);
				String continuer = EOImportParametres.valeurParametrePourCle(editingContext, "CONTINUER_APRES_ERREUR");
				setContinuerTraitement(continuer != null && continuer.equals(CocktailConstantes.VRAI));
			} else {
				setContinuerTraitement(false);
			}		
		}
		public String resultatTransfert() {
			return resultatTransfert;
		}
		public void setResultatTransfert(String resultat) {
			this.resultatTransfert = resultat;
		}
		public String exception() {
			return exception;
		}
		public void setException(String exception) {
			this.exception = exception;
		}
		public String diagnostic() {
			return diagnostic;
		}
		public void setDiagnostic(String diagnostic) {
			this.diagnostic = diagnostic;
		}
		public String resultatOperation() {
			String temp = "";
			if (resultatTransfert() != null) {
				temp = resultatTransfert();
			} else if (resultatImport() != null) {
				temp = resultatImport();
			} else if (resultatParsing() != null) {
				temp = resultatParsing();
			}
			if (exception() != null) {
				temp += "avec erreur : " + exception() + ". Vérifier le log de l'application";
			}
			return temp;
		}
		public String toString() {
			String temp = "Fichier : " + nomFichierImport + "\nParsing : " + resultatParsing;
			if (resultatImport != null) {
				temp += ", " + resultatImport;
				if (resultatTransfert != null) {	// On le fait là car pour qu'il y ait transfert, il faut qu'il y ait import
					temp +=  ", " + resultatTransfert;
				}
			}
			if (exception != null) {
				temp += "\nException pendant le traitement : " + exception;
			}
			if (diagnostic != null) {
				temp += "\nDetail des traitements\n" + diagnostic;
			}
			return temp;
		}

	}

}
