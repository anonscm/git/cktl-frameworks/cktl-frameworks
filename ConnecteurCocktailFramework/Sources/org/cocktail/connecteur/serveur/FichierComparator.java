/*
 * Copyright Consortium Cocktail
 *
 * connecteur_cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur;

import org.cocktail.connecteur.common.DateCtrl;
import org.cocktail.connecteur.importer.modele.Entite;
import org.cocktail.connecteur.importer.moteur.AutomateImport;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSTimestamp;

/** Pour comparer des noms de fichier commen&ccedil;ant par un nom d'entite suivi d'un _ et d'une date sous
 * la forme : JJMMAA ou JJMMAAAA ou JJMMAAAHHmm */
public class FichierComparator extends NSComparator {
	private NSArray entitesTriees;
	public FichierComparator() {
		super();
		entitesTriees = AutomateImport.sharedInstance().nomEntitesImportTrieesParPriorite();
	}

	public int compare(Object object1, Object object2) throws ComparisonException {
		if (object1 instanceof String && object2 instanceof String) {
			String nomFichier1 = (String)object1, nomFichier2 = (String)object2;
			// Extraire des noms de fichier, les noms de table
			String nom1 = extraireNomEntite(nomFichier1);
			String nom2 = extraireNomEntite(nomFichier2);
			if (nom1 == null) {
				throw new ComparisonException(nomFichier1 + " : nom de fichier invalide, pas de nom de table");
			}
			if (nom2 == null) {
				throw new ComparisonException(nomFichier2 + " : nom de fichier invalide, pas de nom de table");
			}
			if (nom1.equals(nom2)) {
				NSTimestamp date1 = extraireDate(nomFichier1);
				NSTimestamp date2 = extraireDate(nomFichier2);
				if (date1 == null) {
					throw new ComparisonException(nomFichier1 + " : nom de fichier invalide, pas de date (JJMMAAAA ou JJMMAAAAHHmm ou JJMMAAAAHHmmSS)");
				}
				if (date2 == null) {
					throw new ComparisonException(nomFichier2 + " : nom de fichier invalide, pas de date (JJMMAAAA ou JJMMAAAAHHmm ou JJMMAAAAHHmmSS)");
				}
				if (DateCtrl.isBefore(date1, date2)) {
					return NSComparator.OrderedAscending;
				} else if (DateCtrl.isAfter(date1, date2)) {
					return NSComparator.OrderedDescending;
				} else {
					return NSComparator.OrderedSame;
				}
			} else {
				// nom1 et nom2 correspondent nécessairement à des entités
				java.util.Enumeration e = entitesTriees.objectEnumerator();
				while (e.hasMoreElements()) {
					String nomEntite = (String)e.nextElement();
					if (nomEntite.equals(nom1)) {
						return NSComparator.OrderedAscending;
					} else if (nomEntite.equals(nom2)) {
						return NSComparator.OrderedDescending;
					}
				}
				// ne devrait pas se produire car l'extraction du nom d'entité aurait retourné null
				return NSComparator.OrderedSame;
			}
		} else {	// Pas de comparaison
			return NSComparator.OrderedSame;
		}
	}
	/** Retourne true si le nom de fichier correspond &grave; la nomenclature i.e 
	 * NOM_JJMMAAAA ou NOM_JJMMAAAAHHmm et NOM est le nom source d'une table définie dans ModeleImport.XML
	 * @param nomFichier
	 * @return
	 */
	public boolean estNomFichierValide(String nomFichier) {
		if (extraireNomEntite(nomFichier) == null) {
			return false;
		}
		if (extraireDate(nomFichier) == null) {
			return false;
		}
		return true;
	}
	// Méthodes privées
	private String extraireNomEntite(String nom) {
		int index = nom.lastIndexOf("_");
		if (index == -1) {
			return null;
		}
		String nomTable = nom.substring(0,index);
		Entite entite =  AutomateImport.sharedInstance().entitePourNomTable(nomTable);
		if (entite == null) {
			return null;
		} else {
			return entite.nomDestination();
		}
	}
	private NSTimestamp extraireDate(String nom) {
		int index = nom.lastIndexOf("_");
		if (index == -1) {
			return null;
		}
		int indexExtension = nom.lastIndexOf(".");
		String date = null;
		if (indexExtension == -1) {
			// Il peut s'agir d'un fichier CVS
			date = nom.substring(index + 1);

		} else {
			date = nom.substring(index + 1, indexExtension);
		}
		if (date.length() == 6) {
			return DateCtrl.stringToDate(date, "%d%m%y");
		}
		if (date.length() == 8) {
			return DateCtrl.stringToDate(date, "%d%m%Y");
		} else if (date.length() == 12) {
			return DateCtrl.stringToDate(date, "%d%m%Y%H%M");
		} else if (date.length() == 14) {
			return DateCtrl.stringToDate(date, "%d%m%Y%H%M%S");
		} else  {
			return null;
		}
		
	}
}
