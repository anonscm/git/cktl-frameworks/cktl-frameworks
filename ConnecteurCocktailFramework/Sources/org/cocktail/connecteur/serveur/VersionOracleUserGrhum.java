package org.cocktail.connecteur.serveur;

import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser;

public class VersionOracleUserGrhum extends CktlVersionOracleUser {
	public String dbUserTableName() {
		return "GRHUM.DB_VERSION";
	}
	public String dbVersionDateColumnName() {
		return "DBV_DATE";
	}
	public String dbVersionIdColumnName() {
		return "DBV_LIBELLE";
	}
	public CktlVersionRequirements[] dependencies() {
		return null;
	}
	public String name() {
		return "Grhum User";
	}

}
