/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.connecteur.serveur;

import org.cocktail.connecteur.common.LogManager;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

/** Recup&egrave;re les invocations distantes concernant les Logs */
public class RemoteLogManager {
	/** Constructeur */
	public RemoteLogManager() {
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("ajouterReceveur", new Class[] { NSNotification.class }), EODistributionContext.RemoteMethodReceiverNeededNotification, null);
	}
	public void finalize() {
		try {
			NSNotificationCenter.defaultCenter().removeObserver(this);
			super.finalize();
		} catch (Throwable e) {}
	}
	// Notifications
	public void ajouterReceveur(NSNotification aNotif) {
		EODistributionContext distributionContext = (EODistributionContext)aNotif.object();
		distributionContext.addRemoteMethodReceiver(this);
	}
	// Méthodes distantes
	/** retourne le texte du log du serveur
	 */
	public String clientSideRequestTexteLog() {
		return LogManager.texteLog();
	}
	/**
	 * Demande le reset du log du serveur
	 */
	public void clientSideRequestReset() {
		LogManager.reset();
	}
	
	/** Permet d'envoyer un mail à partir du client. L'emetteur est l'administrateur de l'application.

	 * @param mailTo
	 * @param sujet
	 * @param texte
	 */
	public String clientSideRequestEnvoyerMail(String from,String mailTo,String sujet, String texte) throws Exception {
		CktlWebApplication application = (CktlWebApplication)WOApplication.application();
		if (application.config() != null) {
			String emetteur = application.config().stringForKey("APP_ADMIN_MAIL");
			if (emetteur != null) {
				from = emetteur;
			}
		}
		try {
			application.mailBus().sendMail(from,mailTo,null, sujet, texte);
			return "Envoi Mail OK";
		} catch (Exception e) {
			LogManager.logException(e);
			return e.getMessage();
		}
	}
	
	// Méthodes statiques
	public static String envoyerMail(String from,String mailTo,String sujet, String texte)  {
		CktlWebApplication application = (CktlWebApplication)WOApplication.application();
		try {
			application.mailBus().sendMail(from,mailTo,null, sujet, texte);
			return "";
		} catch (Exception e) {
			LogManager.logException(e);
			return e.getMessage();
		}
	}
}
