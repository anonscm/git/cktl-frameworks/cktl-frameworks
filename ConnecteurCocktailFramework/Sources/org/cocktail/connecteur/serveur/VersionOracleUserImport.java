package org.cocktail.connecteur.serveur;

import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser;

public class VersionOracleUserImport extends CktlVersionOracleUser {

	public String dbUserTableName() {
		return "IMPORT.DB_VERSION";
	}
	public String dbVersionDateColumnName() {
		return "DB_VERSION_DATE";
	}
	public String dbVersionIdColumnName() {
		return "DB_VERSION_LIBELLE";
	}
	public CktlVersionRequirements[] dependencies() {
		return null;
	}
	public String name() {
		return "Import User";
	}


}
