Information sur les choses à ne pas oublier pour les logs :
************************************************************

Quand on met une nouvelle entité, une fois réglé les problèmes d'import et de transfert,
il peut arriver que l'on obtienne des logs.
L'affichage nous dit d'où vient le souci mais la suppression freeze l'application.

Pour résoudre cela, il est alors nécessaire de rajouter dans Utilitaires.java, qui se
trouve dans le package org.cocktail.connecteurcocktail.serveur, au niveau de la méthode
evaluerRelationPourEntiteEtAttribut des tests pour retourner le nom de la relation adaptée
au type d'objet.

Exemple :
/**
	 * 
	 * @param nomEntite
	 * @param nomAttribut
	 * @return
	 */
	private static String evaluerRelationPourEntiteEtAttribut(String nomEntite, String nomAttribut) {
		try {
			// Rechercher la classe de l'entité du modèle (elle est définie par nomDestination dans le modèle d'import)
			String nomClasse = EOUtilities.entityNamed(AutomateImport.sharedInstance().editingContext(), nomEntite).className();
			Object object = Class.forName(nomClasse).newInstance();

			if (object instanceof ObjetImportPourIndividuEtPeriode) {
				return ((ObjetImportPourIndividuEtPeriode)object).nomRelationPourAttribut(nomAttribut);
			} else if (object instanceof EOCldDetail) {
				return ((EOCldDetail)object).nomRelationPourAttribut(nomAttribut);			
			} else if (object instanceof EOCrctDetail) {
				return ((EOCrctDetail)object).nomRelationPourAttribut(nomAttribut);
			} else if (object instanceof EORepriseTempsPlein) {
				return ((EORepriseTempsPlein)object).nomRelationPourAttribut(nomAttribut);
			}  else if (object instanceof EOIndividuDiplomes) {
				return ((EOIndividuDiplomes)object).nomRelationPourAttribut(nomAttribut);
			} else {
				return null;
			}
		} catch (Exception exc) {
			return null;
		}
	}
