spool e:\cocktail\Mangue\import_bx4\xml\occupation.xml
set heading off
set long 1000
set linesize 150
set pagesize 0


PROMPT <?xml version="1.0" encoding="iso-8859-1" ?><IMPORT>
PROMPT <OCCUPATIONS>

SELECT XMLElement("OCCUPATION",
			XMLFOREST(
				 P.NO_DOSSIER_PERS "ID_SOURCE",
				 NO_POSTE "EMP_SOURCE",
				 NO_OCCUPATION "OCC_SOURCE",
				 D_DEB_OCCUPATION "D_DEB_OCCUPATION",
				 D_FIN_OCCUPATION "D_FIN_OCCUPATION",
				 NUM_MOYEN_UTILISE "NUM_MOYEN_UTILISE",
				 null "MOTIF_FIN",
				 null "OBSERVATIONS",
				 'N' "TEM_TITULAIRE"))
   FROM occupation O, INDIVIDU I, PERSONNEL P, CODE_INSEE CI
   WHERE I.NO_INDIVIDU = O.NO_DOSSIER_PERS
   AND I.NO_INDIVIDU = P.NO_DOSSIER_PERS
   AND CI.NO_DOSSIER_PERS = P.NO_DOSSIER_PERS;

PROMPT </OCCUPATIONS>
PROMPT </IMPORT>
spool off

