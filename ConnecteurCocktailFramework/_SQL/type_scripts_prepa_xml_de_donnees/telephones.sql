spool e:\cocktail\Mangue\import_bx4\xml\telephones.xml
set heading off
set long 1000
set pagesize 0

PROMPT <?xml version="1.0" encoding="iso-8859-1" ?><IMPORT>
PROMPT <TELEPHONES>

SELECT XMLElement("TELEPHONE",
			XMLFOREST(
				I.NO_INDIVIDU "ID_SOURCE",
				NO_TELEPHONE "NO_TELEPHONE",
				'PRV' "TYPE_TEL",
				DECODE(TEM_TEL,'O','TEL','FAX') "TYPE_NO"))
   FROM INDIVIDU I, INDIVIDU_TELEPHONE T, PERSONNEL P, CODE_INSEE CI 
   WHERE I.NO_INDIVIDU = T.NO_INDIVIDU
   AND I.NO_INDIVIDU = P.NO_DOSSIER_PERS
   AND CI.NO_DOSSIER_PERS = P.NO_DOSSIER_PERS
   AND (TEM_TEL='O' OR TEM_FAX='O');

PROMPT </TELEPHONES>
PROMPT </IMPORT>

spool off