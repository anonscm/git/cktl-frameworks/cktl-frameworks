REM Select dans GRHUM

spool e:\cocktail\Mangue\import_bx4\xml\structures.xml
set heading off
set long 1000
set pagesize 0

PROMPT <?xml version="1.0" encoding="iso-8859-1" ?><IMPORT>
PROMPT <STRUCTURES>

SELECT XMLELEMENT("STRUCTURE", 
	XMLFOREST(
		S.C_STRUCTURE "STR_SOURCE",
		LL_STRUCTURE "LL_STRUCTURE",
		LC_STRUCTURE "LC_STRUCTURE",
		DECODE (C_STRUCTURE_PERE,NULL,'INSA',C_STRUCTURE_PERE) "STR_SOURCE_PERE",
		C_TYPE_ETABLISSEMEN "TYPE_ETABLISSEMENT",
		C_TYPE_STRUCTURE "TYPE_STRUCTURE",
		C_ACADEMIE "ACADEMIE",
		C_STATUT_JURIDIQUE "STATUT_JURIDIQUE",
		C_RNE "RNE",
		SIRET "SIRET",
		C_NAF "NAF",
		DATE_OUVERTURE "DATE_OUVERTURE",
		DATE_FERMETURE "DATE_FERMETURE",
		null "CAPITAL",
		null "CA",
		null "EFFECTIFS",
		null "CODE_APE",
		null "ACCES",
		null "ALIAS",
		null "FONCTION1",
		null "FONCTION2",
		null "MOTS_CLEFS",
		null "MOYENNE_AGE",
		null "EXPORT",
		null "TVA_INTRACOM",
		null "ACCUEIL",
		null "RECHERCHE",
		null "AFFICHAGE",
		null "GENCOD",
		null "CODE_URSSAF",
		null "NUM_ASSEDIC",
		null "NUM_IRCANTEC",
		null "NUM_URSSAF",
		null "NUM_CNRACL",
		null "NUM_RAFP"))
  FROM STRUCTURE S;

PROMPT </STRUCTURES>
PROMPT </IMPORT>

spool off