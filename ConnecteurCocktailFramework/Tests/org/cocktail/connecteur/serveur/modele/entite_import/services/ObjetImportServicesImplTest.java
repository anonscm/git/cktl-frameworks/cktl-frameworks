package org.cocktail.connecteur.serveur.modele.entite_import.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IObjetImport;
import org.junit.Test;

public class ObjetImportServicesImplTest {

	@Test
	public void insertionUnObjetAyantAucuneCorrespondance() {
		IObjetImport objImport = new IObjetImport() {

			public void setStatut(String value) {
				assertEquals("", value);
			}

			public void setOperation(String value) {
				assertEquals(ObjetImport.OPERATION_INSERTION, value);
			}

			public String operation() {
				return ObjetImport.OPERATION_INSERTION;
			}
			
			public boolean testUpdateActif() {
				return true;
			}

			public boolean dejaDansSIDestinataire() {
				return false;
			}

			public ObjetCorresp correspondance() {
				return null;
			}
		};

		ObjetImportServicesImpl service = new ObjetImportServicesImpl();
		service.determineStatutEtOperation(objImport, null);

	}

	@Test
	public void correspUnObjetAyantAucuneCorrespondanceDansSI() {
		IObjetImport objImport = new IObjetImport() {

			public void setStatut(String value) {
				assertEquals("", value);
			}

			public void setOperation(String value) {
				assertEquals(ObjetImport.OPERATION_CORRESPONDANCE, value);
			}

			public String operation() {
				return ObjetImport.OPERATION_CORRESPONDANCE;
			}
			
			public boolean testUpdateActif() {
				return true;
			}

			public boolean dejaDansSIDestinataire() {
				return true;
			}

			public ObjetCorresp correspondance() {
				return null;
			}
		};

		ObjetImportServicesImpl service = new ObjetImportServicesImpl();
		service.determineStatutEtOperation(objImport, null);

	}

	@Test
	public void updateUnObjetAyantUneCorrespondance() {
		ObjetCorresp corresp = mock(ObjetCorresp.class);
		
		IObjetImport objImport = new IObjetImport() {

			ObjetCorresp corresp = mock(ObjetCorresp.class);
			public void setStatut(String value) {
				assertEquals("", value);
			}

			public void setOperation(String value) {
				assertEquals(ObjetImport.OPERATION_UPDATE, value);
			}

			public String operation() {
				return ObjetImport.OPERATION_UPDATE;
			}
			
			public boolean testUpdateActif() {
				return true;
			}

			public boolean dejaDansSIDestinataire() {
				return false;
			}

			public ObjetCorresp correspondance() {
				return corresp;
			}
		};

		ObjetImportServicesImpl service = new ObjetImportServicesImpl();
		service.determineStatutEtOperation(objImport, corresp);

	}

}
