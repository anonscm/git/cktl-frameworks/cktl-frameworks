package org.cocktail.connecteur.serveur.modele.entite_import.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.connecteur.serveur.modele.correspondance.ObjetCorresp;
import org.cocktail.connecteur.serveur.modele.entite_import.ObjetImport;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IIndividuImport;
import org.cocktail.connecteur.serveur.modele.entite_import.interfaces.IPersonnelImport;
import org.cocktail.connecteur.serveur.modele.importer.EOLogImport;
import org.junit.Test;
import org.mockito.Mockito;


public class IndividusImportServicesImplTest {

	@Test
	public void individuNonPersonnel() {

		IIndividuImport individu = mock(IIndividuImport.class);
		when(individu.getPersonnel()).thenReturn(null);

		IndividusImportServicesImpl service = new IndividusImportServicesImpl();
		assertEquals(service.verifierRecordApresOperation(individu), null);
		Mockito.verify(individu).getPersonnel();
	}


	@Test
	public void insertionUnObjetAyantUneCorrespondance() {

		IIndividuImport individu = mock(IIndividuImport.class);
		IPersonnelImport personnel = mock(IPersonnelImport.class);
		ObjetCorresp corresp = mock(ObjetCorresp.class);
		when(individu.getPersonnel()).thenReturn(personnel);
		when(individu.getPersonnel().correspondance()).thenReturn(corresp);

		when(individu.operation()).thenReturn(ObjetImport.OPERATION_CORRESPONDANCE);
		when(individu.statut()).thenReturn(ObjetImport.STATUT_VALIDE);

		when(individu.getPersonnel().statut()).thenReturn(ObjetImport.STATUT_VALIDE);


		IndividusImportServicesImpl service = new IndividusImportServicesImpl();
		assertEquals(service.verifierRecordApresOperation(individu), null);
		Mockito.verify(individu.getPersonnel()).setOperation(ObjetImport.OPERATION_UPDATE);
		Mockito.verify(individu.getPersonnel()).setStatut(ObjetImport.STATUT_VALIDE);
	}

	@Test
	public void insertionUnObjetAyantUneCorrespondanceDansSi() {

		IIndividuImport individu = mock(IIndividuImport.class);
		IPersonnelImport personnel = mock(IPersonnelImport.class);
		when(individu.getPersonnel()).thenReturn(personnel);
		when(individu.getPersonnel().correspondance()).thenReturn(null);

		when(individu.operation()).thenReturn(ObjetImport.OPERATION_CORRESPONDANCE);
		when(individu.statut()).thenReturn(ObjetImport.STATUT_VALIDE);
		
		when(individu.getPersonnel().statut()).thenReturn(ObjetImport.STATUT_VALIDE);
		when(individu.getPersonnel().dejaDansSIDestinataire()).thenReturn(true);


		IndividusImportServicesImpl service = new IndividusImportServicesImpl();
		assertEquals(service.verifierRecordApresOperation(individu), null);
		Mockito.verify(individu.getPersonnel()).setOperation(ObjetImport.OPERATION_CORRESPONDANCE);
		Mockito.verify(individu.getPersonnel()).setStatut(ObjetImport.STATUT_VALIDE);
	}

	@Test
	public void insertionUnObjetAyantAucuneCorrespondanceDansSi() {

		IIndividuImport individu = mock(IIndividuImport.class);
		IPersonnelImport personnel = mock(IPersonnelImport.class);
		when(individu.getPersonnel()).thenReturn(personnel);
		when(individu.getPersonnel().correspondance()).thenReturn(null);

		when(individu.operation()).thenReturn(ObjetImport.OPERATION_CORRESPONDANCE);
		when(individu.statut()).thenReturn(ObjetImport.OPERATION_INSERTION);

		when(individu.getPersonnel().statut()).thenReturn(ObjetImport.STATUT_VALIDE);
		when(individu.getPersonnel().dejaDansSIDestinataire()).thenReturn(false);


		IndividusImportServicesImpl service = new IndividusImportServicesImpl();
		assertEquals(service.verifierRecordApresOperation(individu), null);
		Mockito.verify(individu.getPersonnel()).setOperation(ObjetImport.OPERATION_INSERTION);
		Mockito.verify(individu.getPersonnel()).setStatut(ObjetImport.STATUT_VALIDE);
	}

	@Test
	public void insertionUnObjetAyantAucuneCorrespondanceDansSiAvecHomonymes() {

		IIndividuImport individu = mock(IIndividuImport.class);
		IPersonnelImport personnel = mock(IPersonnelImport.class);
		when(individu.getPersonnel()).thenReturn(personnel);
		when(individu.getPersonnel().correspondance()).thenReturn(null);

		when(individu.operation()).thenReturn(ObjetImport.OPERATION_CORRESPONDANCE);
		when(individu.statut()).thenReturn(ObjetImport.STATUT_HOMONYME);

		when(individu.getPersonnel().statut()).thenReturn(ObjetImport.STATUT_HOMONYME);
		when(individu.getPersonnel().dejaDansSIDestinataire()).thenReturn(false);


		IndividusImportServicesImpl service = new IndividusImportServicesImpl();
		assertEquals(service.verifierRecordApresOperation(individu), null);
		Mockito.verify(individu.getPersonnel()).setOperation(ObjetImport.OPERATION_INSERTION);
		Mockito.verify(individu.getPersonnel()).setStatut(ObjetImport.STATUT_HOMONYME);
		
	}

	
	@Test
	public void insertionUnObjetAyantUneCorrespondanceAvecUnLog() {

		IIndividuImport individu = mock(IIndividuImport.class);
		IPersonnelImport personnel = mock(IPersonnelImport.class);
		ObjetCorresp corresp = mock(ObjetCorresp.class);
		EOLogImport log = mock(EOLogImport.class);
		
		when(individu.getPersonnel()).thenReturn(personnel);
		when(individu.getPersonnel().correspondance()).thenReturn(corresp);

		when(individu.operation()).thenReturn(ObjetImport.OPERATION_CORRESPONDANCE);
		when(individu.statut()).thenReturn(ObjetImport.STATUT_VALIDE);

		when(individu.getPersonnel().verifierRecordApresOperation()).thenReturn(log);

		when(individu.getPersonnel().statut()).thenReturn(ObjetImport.STATUT_VALIDE);

		IndividusImportServicesImpl service = new IndividusImportServicesImpl();
		assertEquals(service.verifierRecordApresOperation(individu), log);
		Mockito.verify(individu.getPersonnel()).setOperation(ObjetImport.OPERATION_UPDATE);
		Mockito.verify(individu.getPersonnel()).setStatut(ObjetImport.STATUT_VALIDE);
		Mockito.verify(individu).setStatut(ObjetImport.STATUT_ERREUR);
	}
}
