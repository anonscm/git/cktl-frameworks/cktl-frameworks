/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.controles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSValidation;
import com.wounit.rules.MockEditingContext;

public class ControleBICTest {

	@Rule
	public MockEditingContext edc = new MockEditingContext("FwkCktlPersonne");

	@Test(expected = NSValidation.ValidationException.class)
	public void bicInvalide() {

		(new ControleBIC("BADBIC")).checkValide(edc);
	}

	@Test
	public void bicValide() {

		final String BIC = "BANKFREMBRA";
		EOPays pays = new EOPays();
		pays.setCPays("100");
		pays.setLcPays("FRANCE");
		pays.setCodeIso("FR");
		edc.insertObject(pays);

		ControleBIC bic = new ControleBIC(BIC);

		bic.checkValide(edc);
		assertEquals(BIC, bic.getBic());
		assertEquals("BANK", bic.getCodeBanque());
		assertEquals("FR", bic.getCodePays());
		assertEquals("EM", bic.getCodeEmplacement());
		assertEquals("BRA", bic.getCodeBranche());
	}

	@Test
	public void bicVideValide() {
		final String BIC = null;
		ControleBIC bic = new ControleBIC(BIC);
		bic.checkValide(edc);
		assertEquals(BIC, bic.getBic());
	}

	@Test
	public void checkCodeBanqueValide() {
		assertTrue("Un code banque est composé de quatre caractères majuscules.",
				encapsulateCheckCodeBanqueValide("BANK"));
		assertFalse("Un code banque null est invalide", encapsulateCheckCodeBanqueValide(null));
		assertFalse("Un code banque ne peux contenir de minuscule", encapsulateCheckCodeBanqueValide("BANk"));
		assertFalse("Un code banque ne peux contenir de chiffre", encapsulateCheckCodeBanqueValide("BAN1"));
		assertFalse("Un code banque ne peux contenir de majuscule accentué", encapsulateCheckCodeBanqueValide("BÄNK"));

		assertFalse("Un code banque ne peux faire moins de quatre caractère", encapsulateCheckCodeBanqueValide("BAN"));
		assertFalse("Un code banque ne peux faire plus de quatre caractère", encapsulateCheckCodeBanqueValide("BANQU"));
	}

	private boolean encapsulateCheckCodeBanqueValide(final String codeBanque) {
		try {
			ControleBIC.checkCodeBanqueValide(codeBanque);
		} catch (NSValidation.ValidationException e) {
			return false;
		}
		return true;
	}
}
