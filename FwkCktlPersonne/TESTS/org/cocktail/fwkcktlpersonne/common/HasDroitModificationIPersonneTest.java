/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wounit.annotations.UnderTest;
import com.wounit.rules.MockEditingContext;

@RunWith(MockitoJUnitRunner.class)
public class HasDroitModificationIPersonneTest {

	
	private static Integer PERSID = new Integer("12");
	
	@Rule
	public MockEditingContext edc = new MockEditingContext("FwkCktlPersonne");
	
	@UnderTest @Spy
	private EOIndividu individu;
	
	@Test
	public void siPersonneNullPasDeDroit(){
		EOUtilisateur utilisateur = mock(EOUtilisateur.class);
		when (utilisateur.getLogin()).thenReturn("toto");
		PersonneApplicationUser application = new PersonneApplicationUser(edc, utilisateur);
		assertFalse(application.hasDroitModificationIPersonne(null));
	}

	@Test
	public void siPersonneEdcNullPasDeDroit(){
		EOUtilisateur utilisateur = mock(EOUtilisateur.class);
		//EOIndividu individu = mock(EOIndividu.class);
		when(individu.editingContext()).thenReturn(null);
		PersonneApplicationUser application = new PersonneApplicationUser(edc, utilisateur);
		assertFalse(application.hasDroitModificationIPersonne(individu));
	}
	 
	
	@Test
	public void siTempGobalIdTrueDroit(){
		EOUtilisateur utilisateur = mock(EOUtilisateur.class);
		//EOIndividu individu = mock(EOIndividu.class);
		when (individu.hasTemporaryGlobalID()).thenReturn(Boolean.TRUE);
		PersonneApplicationUser application = new PersonneApplicationUser(edc, utilisateur);
		assertTrue(application.hasDroitModificationIPersonne(individu));
	}
	
}
