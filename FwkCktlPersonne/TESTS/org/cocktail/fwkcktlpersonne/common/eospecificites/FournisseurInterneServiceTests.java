package org.cocktail.fwkcktlpersonne.common.eospecificites;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.doReturn;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IValideFournis;
import org.junit.Test;

public class FournisseurInterneServiceTests {

	@Test
	public void isStructureFournisseurInterne() {
		// Arrange
		EOStructure structure = mock(EOStructure.class);
		IFournis fournisseur = mock(IFournis.class);
		IValideFournis valideFournisseur = mock(IValideFournis.class);
		when(fournisseur.toValideFournis()).thenReturn(valideFournisseur);
		when(structure.toFournis()).thenReturn(fournisseur);
		FournisseurInterneServices structureFournisseurService = spy(new FournisseurInterneServices());
		doReturn("structureFouValideInterne").when(structureFournisseurService).getCStructureFouValideInterne();
		doReturn("structureFouValideMorale").when(structureFournisseurService).getCStructureFouValideMorale();
		doReturn(true).when(structureFournisseurService).estDansGroupe(structure, "structureFouValideInterne");
		doReturn(true).when(structureFournisseurService).estDansGroupe(structure, "structureFouValideMorale");

		// Assert ready to test
		// Act

		// Assert
		assertNotNull(structureFournisseurService.isStructureFournisseurInterne(structure));
		assertTrue(structureFournisseurService.isStructureFournisseurInterne(structure));
	}
}
