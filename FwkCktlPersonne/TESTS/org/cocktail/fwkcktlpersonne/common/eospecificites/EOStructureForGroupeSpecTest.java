package org.cocktail.fwkcktlpersonne.common.eospecificites;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonne.common.metier.providers.CstructureProviderTestImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class EOStructureForGroupeSpecTest {

	
	@Rule
	public MockEditingContext edc = new MockEditingContext("FwkCktlPersonne");
	
	
	@Before
	public void setUp() {
	}

	private void creerStructure(String cStructure, String cStructurePere, String cTypeStructure) {
		EOStructure.setCstructureProvider(new CstructureProviderTestImpl());
		EOStructure s = edc.createSavedObject(EOStructure.ENTITY_NAME);
		s.setCStructure(cStructure);
		s.setCStructurePere(cStructurePere);
		s.setCTypeStructure(cTypeStructure);
	}
	
	@Test
	public void testGetGroupesRacineE() {
		creerStructure("42", "42", EOTypeStructure.TYPE_STRUCTURE_E);		
		List<EOStructure> groupesRacines = EOStructureForGroupeSpec.getGroupesRacine(edc);
		assertEquals(1, groupesRacines.size());
		
	}
	
	@Test
	public void testGetGroupesRacineES() {
		creerStructure("42", "42", EOTypeStructure.TYPE_STRUCTURE_ES);		
		List<EOStructure> groupesRacines = EOStructureForGroupeSpec.getGroupesRacine(edc);
		assertEquals(1, groupesRacines.size());
		
	}
	
	@Test
	public void testGetGroupesRacineA() {
		creerStructure("42", "42", EOTypeStructure.TYPE_STRUCTURE_A);		
		List<EOStructure> groupesRacines = EOStructureForGroupeSpec.getGroupesRacine(edc);
		assertEquals(0, groupesRacines.size());
		
	}
	
	@Test
	public void testGetGroupesRacineEPasRacine() {
		creerStructure("42", "43", EOTypeStructure.TYPE_STRUCTURE_E);		
		List<EOStructure> groupesRacines = EOStructureForGroupeSpec.getGroupesRacine(edc);
		assertEquals(0, groupesRacines.size());
		
	}

}
