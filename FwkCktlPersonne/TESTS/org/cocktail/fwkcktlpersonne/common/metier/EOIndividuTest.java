/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;
import com.wounit.rules.MockEditingContext;

public class EOIndividuTest {

	public static SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
	public static NSTimestamp DATE_DU_JOUR; 
	static{
		try {
			DATE_DU_JOUR = new NSTimestamp(SDF.parse("01/05/2000"));
		} catch (ParseException e) {
		}
	}

	@Rule
	public MockEditingContext mockEditingContext = new MockEditingContext("FwkCktlPersonne");
	
    @Test
	public void checkDatesQuandDateNaissanceNull() throws Exception {
		
		EOIndividu.checkDatesNaissance(null, null, null);
		// Rien ne se passe
		
		// Cas normal
		EOIndividu.checkDatesNaissance(new NSTimestamp(SDF.parse("14/07/1990")), null, DATE_DU_JOUR);
		
	}
	
	@Test(expected=NullPointerException.class)
	public void checkDatesExplorationDateJourNull() throws ParseException{
		EOIndividu.checkDatesNaissance(new NSTimestamp(SDF.parse("14/07/1990")), null, null);
	}
	
	@Test(expected=ValidationException.class)
	public void checkDatesApresDateDuJour() throws ValidationException, ParseException {
		EOIndividu.checkDatesNaissance(new NSTimestamp(SDF.parse("01/01/2025")), null, DATE_DU_JOUR);
	}
	
	@Test(expected=ValidationException.class)
	public void checkDatesDecesApresAujourdhui() throws ValidationException, ParseException {
		EOIndividu.checkDatesNaissance(new NSTimestamp(SDF.parse("01/01/1990")), new NSTimestamp(SDF.parse("01/01/2010")), DATE_DU_JOUR);
	}
	
	@Test(expected=ValidationException.class)
	public void checkDatesNaissanceApresDeces() throws ValidationException, ParseException {
		EOIndividu.checkDatesNaissance(new NSTimestamp(SDF.parse("01/01/1990")), new NSTimestamp(SDF.parse("01/01/1980")), DATE_DU_JOUR);
	}
	
	@Test 
	public  void checkIndividuWithEtudNumeroEqualsVoid () {
		EOEditingContext context = mock(EOEditingContext.class);
		EOIndividu res = EOIndividu.individuWithEtudNumeroEquals(context, "");	
		assertNull(res);
	}
	
	@Test 
	public  void checkIndividuWithEtudNumeroEqualsNull () {
		EOEditingContext context = mock(EOEditingContext.class);
		EOIndividu res = EOIndividu.individuWithEtudNumeroEquals(context, null);	
		assertNull(res);
	}
	
	@Test 
	public  void checkIndividuWithEtudNumeroEqualsBig () {
		EOEditingContext context = mock(EOEditingContext.class);
		EOIndividu res = EOIndividu.individuWithEtudNumeroEquals(context, "125631463322566256688522");	
		assertNull(res);
	}
	
	@Test 
	public  void checkIndividuWithEtudNumeroEqualsInvalid () {
		EOEditingContext context = mock(EOEditingContext.class);
		EOIndividu res = EOIndividu.individuWithEtudNumeroEquals(context, "12A135");	
		assertNull(res);
	}
	
	@Test
	public void testIsEtranger() {
	    EOIndividu individu = mockEditingContext.createSavedObject(EOIndividu.class);
	    EOPays pays = mock(EOPays.class);
	    when(pays.isPaysDefaut()).thenReturn(false);
	    individu.setToPaysNationaliteRelationship(pays);
	    
	    assertTrue(individu.isEtranger());
	    
	}
	
	@Test
	public void testAdresseFacturation() {
        EOIndividu individu = mockEditingContext.createSavedObject(EOIndividu.class);
        EOAdresse adresse = mockEditingContext.createSavedObject(EOAdresse.class);
        EOTypeAdresse typeFacturation = mockEditingContext.createSavedObject(EOTypeAdresse.class);
        typeFacturation.setTadrCode(EOTypeAdresse.TADR_CODE_FACT);
        EORepartPersonneAdresse repart = mockEditingContext.createSavedObject(EORepartPersonneAdresse.class);
        repart.setToTypeAdresseRelationship(typeFacturation);
        repart.setToAdresseRelationship(adresse);
        repart.setToPersonneRelationship(individu);
        
        IAdresse adresseFacturation = individu.adresseFacturation();
        
        assertNotNull(adresseFacturation);
        assertTrue(adresseFacturation.equals(adresse));
	}
	
}
