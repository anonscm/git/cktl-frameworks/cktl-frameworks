package org.cocktail.fwkcktlpersonne.common.metier;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

/**
 * Classe de test de RepartStructure
 * @author Chama LAATIK
 *
 */
public class EORepartStructureTest {
	
	@Rule
    public MockEditingContext editingContext = new MockEditingContext("FwkCktlPersonne");

	@Dummy
    private EORepartAssociation repartAssociationExistant;
    
    @Dummy
    private EORepartStructure repartStructureExistant;
    
    @Dummy
    private EOAssociation association;
  
    
    @Before
	public void setup() {
		NSTimestamp dateCreation = new NSTimestamp(1376539200000l);  //Date du 15/08/2013
		
		repartStructureExistant.setPersIdCreation(1);
		repartStructureExistant.setDCreation(dateCreation);
		repartStructureExistant.setPersIdModification(1);	
		repartStructureExistant.setDModification(dateCreation);

		repartAssociationExistant.setToAssociationRelationship(association);
		repartAssociationExistant.setPersIdCreation(1);
		repartAssociationExistant.setDCreation(dateCreation);
		repartAssociationExistant.setPersIdModification(1);
		repartAssociationExistant.setDModification(dateCreation);		
		
		repartStructureExistant.addToToRepartAssociationsRelationship(repartAssociationExistant);
    }
    
	@Test
	public void testMajModificationPourRepartStructureEtAssociations() {
		Integer persIdConnecte = 4;
		
		String now = DateCtrl.dateToString(new NSTimestamp());
		
		Assert.assertEquals("15/08/2013", DateCtrl.dateToString(repartStructureExistant.dModification()));
		Assert.assertEquals(Integer.valueOf(1), repartStructureExistant.persIdModification());

		Assert.assertEquals(1, repartStructureExistant.toRepartAssociations().size());
		Assert.assertEquals("15/08/2013", DateCtrl.dateToString(repartStructureExistant.toRepartAssociations().get(0).dModification()));
		Assert.assertEquals(Integer.valueOf(1), repartStructureExistant.toRepartAssociations().get(0).persIdModification());
		
		repartStructureExistant.majModificationPourRepartStructureEtAssociations(editingContext, persIdConnecte);
		
		//On vérifie que la répartition a bien été mis à jour
		Assert.assertEquals("15/08/2013", DateCtrl.dateToString(repartStructureExistant.dCreation()));
		Assert.assertEquals(now, DateCtrl.dateToString(repartStructureExistant.dModification()));
		Assert.assertEquals(Integer.valueOf(1), repartStructureExistant.persIdCreation());
		Assert.assertEquals(persIdConnecte, repartStructureExistant.persIdModification());

		//On vérifie que les associations correspondantes ont bien été mises à jour
		Assert.assertEquals(1, repartStructureExistant.toRepartAssociations().size());
		Assert.assertEquals("15/08/2013", DateCtrl.dateToString(repartStructureExistant.toRepartAssociations().get(0).dCreation()));
		Assert.assertEquals(now, DateCtrl.dateToString(repartStructureExistant.toRepartAssociations().get(0).dModification()));
		Assert.assertEquals(Integer.valueOf(1), repartStructureExistant.toRepartAssociations().get(0).persIdCreation());
		Assert.assertEquals(persIdConnecte, repartStructureExistant.toRepartAssociations().get(0).persIdModification());
	}
	
	private EORepartStructure creerRepartStructure(MockEditingContext editingContext) {
		EORepartStructure repartStruct = new EORepartStructure();
		editingContext.insertSavedObject(repartStruct);
		return repartStruct;
	}
	
	private void ajouteAssociation(MockEditingContext editingContext,EORepartStructure repartStructure, String libelleAssociation)
	{
		EOAssociation association=new EOAssociation();
		editingContext.insertSavedObject(association);
		association.setAssLibelle(libelleAssociation);
		
		EORepartAssociation repartAssociation=new EORepartAssociation();
		editingContext.insertSavedObject(repartAssociation);
		
		repartAssociation.setToAssociationRelationship(association);
		repartStructure.addToToRepartAssociationsRelationship(repartAssociation);
	}
	

	/**
	 * US 848 - DT 6650 Afficher les rôles des membres d'un groupe 
	 */
	@Test
	public void testRolesAucuneAssociation() {
		EORepartStructure repartStructure=creerRepartStructure(editingContext);
		
		Assert.assertEquals("",repartStructure.roles());
	}

	/**
	 * US 848 - DT 6650 Afficher les rôles des membres d'un groupe 
	 */
	@Test
	public void testRolesUneAssociation() {
		EORepartStructure repartStructure=creerRepartStructure(editingContext);
		ajouteAssociation(editingContext,repartStructure,"Role 1");
		Assert.assertEquals("Role 1",repartStructure.roles());
	}

	/**
	 * US 848 - DT 6650 Afficher les rôles des membres d'un groupe 
	 */
	@Test
	public void testRolesPlusieursAssociations() {
		EORepartStructure repartStructure=creerRepartStructure(editingContext);
		ajouteAssociation(editingContext,repartStructure,"Role 1");
		ajouteAssociation(editingContext,repartStructure,"Role 2");
		ajouteAssociation(editingContext,repartStructure,"Role 3");
		Assert.assertEquals("Role 1, Role 2, Role 3",repartStructure.roles());
	}
}
