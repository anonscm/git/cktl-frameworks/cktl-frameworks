/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

public class InseeSexeTest {

    @Test
    public void checkable() throws ParseException {

        IIndividuControle controle = new InseeSexe();

        assertFalse("Le code Insee ne doit pas être null",
                        controle.checkable(Helper.initHomme(null, "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, "02A")));

        assertFalse("Le code Insee ne doit pas être vide",
                        controle.checkable(Helper.initHomme("", "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, "02A")));

        assertFalse("Le genre ne doit pas être null",
                        controle.checkable(Helper.initIndividu(null, "17605753500001", null, 
                                        IPays.CODE_PAYS_FRANCE, "075")));

        assertTrue(controle.checkable(Helper.initHomme("17605753500001", "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, "075")));
    }
    
    @Test
    public void checkInseeSexeHommeOk() {
        
        IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(mock(ICivilite.class));
        when(individu.estHomme()).thenReturn(true);
        when(individu.getCodeInsee()).thenReturn(new CodeInsee("1234567890123"));
        
        IIndividuControle controle = new InseeSexe();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
        
    }
    
    @Test
    public void checkInseeSexeHommeKo() {
        
        IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(mock(ICivilite.class));
        when(individu.estHomme()).thenReturn(true);
        when(individu.getCodeInsee()).thenReturn(new CodeInsee("2234567890123"));
        
        IIndividuControle controle = new InseeSexe();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeSexe.REGLE_HOMME, resultat.getCode());
    }
    
    @Test
    public void checkInseeSexeFemmeOk() {
        
        IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(mock(ICivilite.class));
        when(individu.estHomme()).thenReturn(false);
        when(individu.getCodeInsee()).thenReturn(new CodeInsee("2234567890123"));
        
        IIndividuControle controle = new InseeSexe();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
        
    }
    
    @Test
    public void checkInseeSexeFemmeKo() {
        
        IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(mock(ICivilite.class));
        when(individu.estHomme()).thenReturn(false);
        when(individu.getCodeInsee()).thenReturn(new CodeInsee("1234567890123"));
        
        IIndividuControle controle = new InseeSexe();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeSexe.REGLE_FEMME, resultat.getCode());
        
    }

    
    
    @Test
    public void checkInseeSexeNonControlable() {
        
        IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(null);
        
        IIndividuControle controle = new InseeSexe();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(ResultatControle.NON_CHECKABLE, resultat);
        
    }

}
