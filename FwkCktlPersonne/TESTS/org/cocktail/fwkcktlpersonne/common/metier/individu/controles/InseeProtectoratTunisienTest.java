/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

public class InseeProtectoratTunisienTest {

    public static SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void checkable() throws ParseException {

        IIndividuControle controle = new InseeProtectoratTunisien();

        assertFalse("Le code Insee ne doit pas être null",
                        controle.checkable(Helper.initHomme(null, "01/05/1967", 
                                        InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null)));
        assertFalse("La date de naissance ne doit pas être null",
                        controle.checkable(Helper.initHomme("15005963510001", null, 
                                        InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null)));
    }

    @Test
    public void checkProtectorat() throws ParseException {
        
        IIndividu individu = Helper.initHomme("16705963510001", "01/05/1967", InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null);
        
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
        
    }
    
    @Test
    public void checkProtectoratDateNull() throws ParseException {
        
        IIndividu individu = Helper.initHomme("16705963510001", null, InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null);
        
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(ResultatControle.NON_CHECKABLE, resultat);
        
    }

    @Test
    public void checkProtectoratDateNaissanceApres68() throws ParseException {
        
        IIndividu individu = Helper.initHomme("16805963510001", "01/05/1968", InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null);
       
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeProtectoratTunisien.REGLE_ANNEE_NAISSANCE_INF_68, resultat.getCode());
        
    }

    @Test
    public void checkProtectoratDateNaissanceAvant1912() throws ParseException {
        
        IIndividu individu = Helper.initHomme("10805963510001", "01/05/1908", InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null);
       
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
        
    }

    @Test
    public void checkProtectoratDateNaissanceApres2000() throws ParseException {
        
        IIndividu individu = Helper.initHomme("10805963510001", "01/05/2008", InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null);
       
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeProtectoratTunisien.REGLE_ANNEE_NAISSANCE_INF_68, resultat.getCode());
        
    }

    @Test
    public void checkProtectoratDateNaissanceInvalide() throws ParseException {
        
        IIndividu individu = Helper.initHomme("16705963510001", "01/05/1968", InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null);
       
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeProtectoratTunisien.REGLE_ANNEE_NAISSANCE_INF_68, resultat.getCode());
        
    }
    
    @Test
    public void checkProtectoratPaysDifferentTunisie() throws ParseException {
        
        IPays pays = mock(IPays.class);
        when(pays.getCode()).thenReturn("100");
        
        IIndividu individu = Helper.initHomme("15005963510001", "01/05/1967", IPays.CODE_PAYS_FRANCE, null);
       
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeProtectoratTunisien.REGLE_PAYS_DIFFERENT_FRANCE_POUR_PROTECTORAT, resultat.getCode());
        
    }


    @Test
    public void checkCodePaysInseeInvalide() throws ParseException {
        
        IPays pays = mock(IPays.class);
        when(pays.getCode()).thenReturn("012");
        
        IIndividu individu = Helper.initHomme("15005960120001", "01/05/1967", InseeProtectoratTunisien.CODE_INSEE_TUNISIE, null);
        
        IIndividuControle controle = new InseeProtectoratTunisien();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeProtectoratTunisien.REGLE_CODE_PAYS_INSEE_TUNISIE, resultat.getCode());
        
    }

}
