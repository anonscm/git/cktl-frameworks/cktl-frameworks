/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

public class InseeSeineEtSeineEtOiseTest {

    @Test
    public void checkable() throws ParseException {

        IIndividuControle controle = new InseeSeineEtSeineEtOise();

        assertFalse("Le code Insee ne doit pas être null",
                        controle.checkable(Helper.initHomme(null, "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, "091")));
        assertFalse("La date de naissance ne doit pas être null",
                        controle.checkable(Helper.initHomme("17205753500001", null, 
                                        IPays.CODE_PAYS_FRANCE, "091")));
        assertFalse("La date de naissance doit être antérieur à 1969",
                        controle.checkable(Helper.initHomme("16905753500001", "01/05/1969", 
                                        IPays.CODE_PAYS_FRANCE, null)));
        assertFalse("Le département de naissance ne doit pas être null",
                        controle.checkable(Helper.initHomme("16805753500001", "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, null)));
        assertTrue("Le département de naissance Insee peut être 75",
                        controle.checkable(Helper.initHomme("16805753500001", "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, "092")));
        assertTrue("Le département de naissance Insee peut être 78",
                        controle.checkable(Helper.initHomme("16805783500001", "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, "091")));
        assertFalse("Le département de naissance Insse ne peut être 77l",
                        controle.checkable(Helper.initHomme("16805773500001", "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, "077")));
    }

    @Test
    public void checkSeine() throws ParseException {

        IIndividuControle controle = new InseeSeineEtSeineEtOise();
        assertFalse("Le 78 ne faisait pas partie de la seine", 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "078")).valide());
        assertFalse("Le 91 ne faisait pas partie de la seine", 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "091")).valide());
        assertTrue("Une partie du 92 faisait partie de la seine", 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "092")).valide());
        assertTrue("Une partie du 93 faisait partie de la seine", 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "093")).valide());
        assertTrue("Une partie du 94 faisait partie de la seine", 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "094")).valide());
        assertFalse("Le 95 ne faisait pas partie de la seine", 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "095")).valide());
        assertTrue("Paris faisait partie de la seine", 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "075")).valide());
        assertEquals(InseeSeineEtSeineEtOise.REGLE_COHERENCE_75_IDF_KO, 
                        controle.check(Helper.initHomme("16805753500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "095")).getCode());

    }

    @Test
    public void checkSeineEtOise() throws ParseException {

        IIndividuControle controle = new InseeSeineEtSeineEtOise();
        assertEquals("", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "078")).getMessage());
        assertTrue("Le 78 faisait partie de la seine et oise", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "078")).valide());
        assertTrue("Le 91 faisait partie de la seine et oise", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "091")).valide());
        assertTrue("Une partie du 92 faisait partie de la seine et oise", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "092")).valide());
        assertTrue("Une partie du 93 faisait partie de la seine et oise", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "093")).valide());
        assertTrue("Une partie du 94 faisait partie de la seine et oise", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "094")).valide());
        assertTrue("Le 95 faisait partie de la seine et oise", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "095")).valide());
        assertFalse("Paris ne faisait pas partie de la seine et oise", 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "075")).valide());
        assertEquals(InseeSeineEtSeineEtOise.REGLE_COHERENCE_78_IDF_KO, 
                        controle.check(Helper.initHomme("16805783500001", "01/05/1968", IPays.CODE_PAYS_FRANCE, "075")).getCode());

    }

}
