package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

public class InseeMayotteTest {
	
	 @Test
	    public void checkable() throws ParseException {

	        IIndividuControle controle = new InseeMayotte();

	        assertFalse("Le code Insee ne doit pas être null",
	                        controle.checkable(Helper.initHomme(null, "01/05/1976", 
	                                        IPays.CODE_PAYS_FRANCE, "985")));
	        assertFalse("La date de naissance ne doit pas être null",
	                        controle.checkable(Helper.initHomme("17605753500001", null, 
	                                        IPays.CODE_PAYS_FRANCE, "985")));
	        assertFalse("Le département de naissance ne doit pas être null",
	                        controle.checkable(Helper.initHomme("17605753500001", "01/05/1976", 
	                                        IPays.CODE_PAYS_FRANCE, null)));
	        
	        assertTrue("Le département de naissance Insee peut être 985",
	                        controle.checkable(Helper.initHomme("1680598510001", "01/05/1968", 
	                                        IPays.CODE_PAYS_FRANCE, "985")));
	        assertTrue("Le département de naissance Insee peut être 976",
	                        controle.checkable(Helper.initHomme("1090597610001", "01/05/2009", 
	                                        IPays.CODE_PAYS_FRANCE, "976")));
	        assertFalse("Le département de naissance Insee ne peut être 75",
	                        controle.checkable(Helper.initHomme("16805753500001", "01/05/1968", 
	                                        IPays.CODE_PAYS_FRANCE, "075")));
	    }

	 @Test
	    public void checkMayotte() throws ParseException {

	        IIndividuControle controle = new InseeMayotte();
	        assertFalse("Un individu né après le 29/03/2009 ne peut être nés dans le département insee 985", 
	                        controle.check(Helper.initHomme("1090598510001", "01/05/2009", IPays.CODE_PAYS_FRANCE, "985")).valide());
	        assertFalse("Un individu né avant le 29/03/2009 ne peut être nés dans le département insee 976", 
	                        controle.check(Helper.initHomme("1750597610001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "976")).valide());
	        
	        assertTrue("Un individu né avant le 29/03/2009 peut être nés dans le département insee 985", 
	                        controle.check(Helper.initHomme("1750598550001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "985")).valide());
	        assertTrue("Un individu né après le 29/03/2009 peut être nés dans le département insee 976", 
	                        controle.check(Helper.initHomme("1090597610001", "01/05/2009", IPays.CODE_PAYS_FRANCE, "976")).valide());

	        assertEquals(InseeMayotte.REGLE_COHERENCE_MAYOTTE, 
	                        controle.check(Helper.initHomme("1750598550001", "01/05/1975", "100", "075")).getCode());
	        assertEquals(InseeMayotte.REGLE_COHERENCE_MAYOTTE, 
	        		controle.check(Helper.initHomme("1090598510001", "01/05/2009", "100", "075")).getCode());
	    }
	 
}
