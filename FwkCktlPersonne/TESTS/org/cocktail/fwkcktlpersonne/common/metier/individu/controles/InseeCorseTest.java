/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

public class InseeCorseTest {

    @Test
    public void checkable() throws ParseException {

        IIndividuControle controle = new InseeCorse();

        assertFalse("Le code Insee ne doit pas être null",
                        controle.checkable(Helper.initHomme(null, "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, "02A")));
        assertFalse("La date de naissance ne doit pas être null",
                        controle.checkable(Helper.initHomme("17605753500001", null, 
                                        IPays.CODE_PAYS_FRANCE, "02A")));
        assertFalse("Le département de naissance ne doit pas être null",
                        controle.checkable(Helper.initHomme("17605753500001", "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, null)));
        assertTrue("Le département de naissance Insee peut être 20",
                        controle.checkable(Helper.initHomme("17605203500001", "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, "020")));
        assertTrue("Le département de naissance Insee peut être 2A",
                        controle.checkable(Helper.initHomme("168052A3500001", "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, "02A")));
        assertTrue("Le département de naissance Insee peut être 2B",
                        controle.checkable(Helper.initHomme("168052B3500001", "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, "02B")));
        assertFalse("Le département de naissance Insee ne peut être 75",
                        controle.checkable(Helper.initHomme("16805753500001", "01/05/1968", 
                                        IPays.CODE_PAYS_FRANCE, "075")));
    }

    @Test
    public void checkCorseAvantDivision() throws ParseException {

        IIndividuControle controle = new InseeCorse();
        assertFalse("Un individu né avant 1976 ne peut être nés dans le département insee 2A", 
                        controle.check(Helper.initHomme("175052A3500001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "02A")).valide());
        assertFalse("Un individu né avant 1976 ne peut être nés dans le département insee 2B", 
                        controle.check(Helper.initHomme("175052B3500001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "02B")).valide());
        assertEquals(InseeCorse.REGLE_NAISSANCE_CORSE_AVANT_76, 
                        controle.check(Helper.initHomme("175052B3500001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "02B")).getCode());

        assertTrue("Un individu né avant 1976 peut être nés dans le département insee 20 et dans le département 2A", 
                        controle.check(Helper.initHomme("17505203500001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "02A")).valide());
        assertTrue("Un individu né avant 1976 peut être nés dans le département insee 20 et dans le département 2B", 
                        controle.check(Helper.initHomme("17505203500001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "02B")).valide());
        assertTrue("Un individu né avant 1976 peut être nés dans le département insee 20 et dens le département 20", 
                        controle.check(Helper.initHomme("17505203500001", "01/05/1975", IPays.CODE_PAYS_FRANCE, "020")).valide());
        assertFalse("Un individu né avant 1976 peut être nés dans le département insee 20 et dens le département 20", 
                        controle.check(Helper.initHomme("1750520190123", "01/05/1975", "100", "075")).valide());
        assertEquals(InseeCorse.REGLE_COHERENCE_20_CORSE, 
                        controle.check(Helper.initHomme("1750520190123", "01/05/1975", "100", "075")).getCode());
    }

    @Test
    public void checkCorseApresDivision() throws ParseException {

        IIndividuControle controle = new InseeCorse();
        assertFalse("Un individu né à partir de 1976 ne peut être nés dans le département insee 20", 
                        controle.check(Helper.initHomme("17605203500001", "01/05/1976", IPays.CODE_PAYS_FRANCE, "02A")).valide());
        assertEquals(InseeCorse.REGLE_NAISSANCE_CORSE_APRES_76, 
                        controle.check(Helper.initHomme("17605203500001", "01/05/1976", IPays.CODE_PAYS_FRANCE, "02B")).getCode());

        assertTrue("Un individu né à partir de 1976 peut être nés dans le département insee 2A", 
                        controle.check(Helper.initHomme("176052A3500001", "01/05/1976", IPays.CODE_PAYS_FRANCE, "02B")).valide());
        assertTrue("Un individu né à partir de 1976 peut être nés dans le département insee 2B", 
                        controle.check(Helper.initHomme("176052B3500001", "01/05/1976", IPays.CODE_PAYS_FRANCE, "02B")).valide());
    }
}
