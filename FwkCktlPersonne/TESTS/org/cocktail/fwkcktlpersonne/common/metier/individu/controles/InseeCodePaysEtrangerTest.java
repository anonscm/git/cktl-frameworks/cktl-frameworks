/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

public class InseeCodePaysEtrangerTest {

    @Test
    public void checkable() throws ParseException {

        IIndividuControle controle = new InseeCodePaysEtranger();

        assertFalse("Le code Insee ne doit pas être null",
                        controle.checkable(Helper.initHomme(null, "01/05/1968", 
                                        "234", null)));
        assertFalse("Le code insee doit représenter un pays étranger",
                        controle.checkable(Helper.initHomme("16805753500001", "01/05/1968", 
                                        "234", null)));
        assertTrue(controle.checkable(Helper.initHomme("16805993500001", "01/05/1968", "234", null)));
    }

    @Test
    public void checkEtranger() throws ParseException {
        
        IIndividu individu = Helper.initHomme("15005992340001", "01/05/1950", "234", null);
        
        IIndividuControle controle = new InseeCodePaysEtranger();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
        
    }

    @Test
    public void checkEtrangerInseeFrance() throws ParseException {
        
        IIndividu individu = Helper.initHomme("15005991000001", "01/05/1950", "234", null);
        
        IIndividuControle controle = new InseeCodePaysEtranger();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeCodePaysEtranger.REGLE_CODE_PAYS_INSEE_FRANCE_INVALIDE, resultat.getCode());
        
    }

    @Test
    public void checkEtrangerPaysFrance() throws ParseException {
        
        IIndividu individu = Helper.initHomme("15005991000001", "01/05/1950", 
                        IPays.CODE_PAYS_FRANCE, null);

        
        IIndividuControle controle = new InseeCodePaysEtranger();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeCodePaysEtranger.REGLE_PAYS_DIFFERENT_FRANCE_POUR_ETRANGER, resultat.getCode());
        
    }

    @Test
    public void checkEtrangerCodePaysInseeAvecLettre() throws ParseException {
        
        IIndividu individu = Helper.initHomme("15005992A40001", "01/05/1950", 
                        "234", null);
        
        IIndividuControle controle = new InseeCodePaysEtranger();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeCodePaysEtranger.REGLE_CODE_PAYS_INSEE_INVALIDE, resultat.getCode());
        
    }

    @Test
    public void checkEtrangerCodePaysInseeInvalide() throws ParseException {
        
        IIndividu individu = Helper.initHomme("15005990000001", "01/05/1950", 
                        "234", null);
        
        IIndividuControle controle = new InseeCodePaysEtranger();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeCodePaysEtranger.REGLE_ETRANGER_PAYS_NAISSANCE_001_999, resultat.getCode());
        
    }

}
