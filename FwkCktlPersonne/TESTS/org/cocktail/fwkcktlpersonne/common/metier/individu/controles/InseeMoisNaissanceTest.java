/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

public class InseeMoisNaissanceTest {

    public static SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
    
    @Test
    public void checkable() throws ParseException {

        IIndividuControle controle = new InseeMoisNaissance();

        assertFalse("Le code Insee ne doit pas être null",
                        controle.checkable(Helper.initHomme(null, "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, "02A")));
        assertFalse("La date de naissance ne doit pas être null",
                        controle.checkable(Helper.initHomme("17605753500001", null, 
                                        IPays.CODE_PAYS_FRANCE, "075")));

        assertTrue(controle.checkable(Helper.initHomme("17605753500001", "01/05/1976", 
                                        IPays.CODE_PAYS_FRANCE, "075")));
    }

    @Test
    public void checkInseeDateNaissanceMoisCorrect() throws ParseException {

        IIndividu individu = Helper.initHomme("1800567890123", "01/05/1980", IPays.CODE_PAYS_FRANCE, "67"); 
        
        IIndividuControle controle = new InseeMoisNaissance();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
        
    }

    @Test
    public void checkInseeDateNaissanceMoisIndetermine() throws ParseException {

        IIndividu individu = Helper.initHomme("1809967890123", "01/05/1980", org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays.CODE_PAYS_FRANCE, "67"); 

        IIndividuControle controle = new InseeMoisNaissance();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
        
    }

    @Test
    public void checkInseeDateNaissanceMauvaisMois() throws ParseException {

        IIndividu individu = Helper.initHomme("1800667890123", "01/05/1980", org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays.CODE_PAYS_FRANCE, "67"); 
        
        IIndividuControle controle = new InseeMoisNaissance();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeMoisNaissance.REGLE_MOIS_COHERENT, resultat.getCode());
        
    }

}
