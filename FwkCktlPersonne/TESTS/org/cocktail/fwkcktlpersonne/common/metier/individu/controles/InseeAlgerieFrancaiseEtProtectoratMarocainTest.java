/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.junit.Test;

public class InseeAlgerieFrancaiseEtProtectoratMarocainTest {

    public static SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void checkableDepartementsNull() throws ParseException {

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();

        assertFalse("Le code Insee ne doit pas être null",
                        controle.checkable(Helper.initHomme(null, "01/05/1962", 
                                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null)));
        assertFalse("La date de naissance ne doit pas être null",
                        controle.checkable(Helper.initHomme("16205913500001", null, 
                                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null)));
        assertFalse("Les individus nées après 1962 ne sont pas concerné",
                        controle.checkable(Helper.initHomme("16305913500001", "01/05/1963", 
                                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null)));
        assertFalse("Les individus nées avant 1963 hors des département 91 à 95 ne sont pas concerné",
                        controle.checkable(Helper.initHomme("16205903500001", "01/05/1962", 
                                        IPays.CODE_PAYS_FRANCE, null)));
        assertTrue("Les individus nées avant 1963 dans les départements 91 à 94 (Algérie Française) et 95 (Maroc) sont concerné",
                        controle.checkable(Helper.initHomme("16205913500001", "01/05/1962", 
                                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null)));
        assertTrue("Les individus nées avant 1910 dans les départements 91 à 94 (Algérie Française) et 95 (Maroc) sont concerné",
                        controle.checkable(Helper.initHomme("10305913500001", "01/05/1903", 
                                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null)));
        assertFalse("Les individus nées après 1999 ne sont pas concerné",
                        controle.checkable(Helper.initHomme("10305913500001", "01/05/2003", 
                                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null)));
    }

    @Test
    public void checkAlgerieFrancaise() throws ParseException {

        IIndividu individu = Helper.initHomme("16205913500001", "01/05/1962", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null);

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
    }

    @Test
    public void checkAlgerieFrancaiseDepartementNonNullAvant63() throws ParseException {

        IIndividu individu = Helper.initHomme("16205913500001", "01/05/1962", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, "091");

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeAlgerieFrancaiseEtProtectoratMarocain.REGLE_PROTECTORAT_ALGERIE_PAS_DEPARTEMENT,
                        resultat.getCode());
    }

    @Test
    public void checkAlgerieFrancaiseDepartementNonNullApres63() throws ParseException {

        IIndividu individu = Helper.initHomme("16305913500001", "01/05/1963", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, "091");

         IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(ResultatControle.NON_CHECKABLE, resultat);
    }

    @Test
    public void checkAlgerieFrancaisePayNaissanceAlgerieAvant63() throws ParseException {
        IIndividu individu = Helper.initHomme("16205913500001", "01/05/1962", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null);

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
    }

    @Test
    public void checkAlgerieFrancaisePayNaissanceAlgerieApres2000() throws ParseException {
        IIndividu individu = Helper.initHomme("10205913500001", "01/05/2002", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null);

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
    }

    @Test
    public void checkAlgerieFrancaisePayNaissanceAlgerieAvant1910() throws ParseException {
        IIndividu individu = Helper.initHomme("10205913500001", "01/05/1902", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_ALGERIE, null);

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
    }

    @Test
    public void checkAlgerieFrancaisePayNaissanceFranceAvant63() throws ParseException {
        IIndividu individu = Helper.initHomme("16205913500001", "01/05/1962", 
                        IPays.CODE_PAYS_FRANCE, null);
      
        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeAlgerieFrancaiseEtProtectoratMarocain.REGLE_PAYS_NAISSANCE_ALGERIE_KO,
                        resultat.getCode());
    }
    @Test
    public void checkAlgerieFrancaisePaysNaissanceFranceApres63() throws ParseException {

        IIndividu individu = Helper.initHomme("16305913500001", "01/05/1963", 
                        IPays.CODE_PAYS_FRANCE, null);

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(ResultatControle.NON_CHECKABLE, resultat);
    }

    @Test
    public void checkMarocPayNaissanceMarocAvant63() throws ParseException {

        IIndividu individu = Helper.initHomme("16205953500001", "01/05/1962", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_MAROC, null);
        
        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
    }

    @Test
    public void checkMarocPayNaissanceMarocApres2000() throws ParseException {

        IIndividu individu = Helper.initHomme("10205953500001", "01/05/2002", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_MAROC, null);
        
        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
    }

    @Test
    public void checkMarocPayNaissanceMarocAvant1910() throws ParseException {

        IIndividu individu = Helper.initHomme("10205953500001", "01/05/1902", 
                        InseeAlgerieFrancaiseEtProtectoratMarocain.CODE_INSEE_MAROC, null);
        
        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertTrue(resultat.valide());
    }

    @Test
    public void checkMarocPayNaissanceFranceAvant63() throws ParseException {

        IIndividu individu = Helper.initHomme("16205953500001", "01/05/1962", 
                        IPays.CODE_PAYS_FRANCE, null);
        
        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(InseeAlgerieFrancaiseEtProtectoratMarocain.REGLE_PAYS_NAISSANCE_MAROC_KO,
                        resultat.getCode());
    }
    @Test
    public void checkMarocPaysNaissanceFranceApres63() throws ParseException {

        IIndividu individu = Helper.initHomme("16305953500001", "01/05/1963", 
                        IPays.CODE_PAYS_FRANCE, null);

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);
        
        assertFalse(resultat.valide());
        assertEquals(ResultatControle.NON_CHECKABLE, resultat);
    }
    
    @Test
    public void problemTest() throws ParseException {
        
        IIndividu individu = Helper.initHomme("1500592012345", "01/05/1950", "99", "92");

        IIndividuControle controle = new InseeAlgerieFrancaiseEtProtectoratMarocain();
        ResultatControle resultat = controle.check(individu);

        assertFalse(resultat.valide());
        assertEquals(InseeAlgerieFrancaiseEtProtectoratMarocain.REGLE_PROTECTORAT_ALGERIE_PAS_DEPARTEMENT,
                        resultat.getCode());

    }
}
