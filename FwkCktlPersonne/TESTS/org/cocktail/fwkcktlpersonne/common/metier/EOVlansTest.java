/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.wounit.rules.MockEditingContext;

import er.extensions.foundation.ERXArrayUtilities;

@RunWith(MockitoJUnitRunner.class)
public class EOVlansTest {
	
    @Rule
    public MockEditingContext editingContext = new MockEditingContext("FwkCktlPersonne");

    private NSArray<EOVlans> arrayOfDummyVlans = new NSMutableArray<EOVlans>();
	
    private  NSArray<String> vlanTypesPourIndividu = 
    		new NSMutableArray<String>(EOVlans.VLAN_P, EOVlans.VLAN_R, EOVlans.VLAN_X);
    
    
	@Before
	public void setup() {

	    initVlans(EOVlans.VLAN_P, "O");
		initVlans(EOVlans.VLAN_R, "O");
		initVlans(EOVlans.VLAN_X, "O");
		initVlans(EOVlans.VLAN_E, "N");
		initVlans(EOVlans.VLAN_G, "N");
		initVlans(EOVlans.VLAN_D, "D");
		
	}
	
	
	@Test
	public void testFetchAllVlansPourIndividu() {

	    EOIndividu individu = mock(EOIndividu.class);
        doReturn(true).when(individu).isIndividu();

		NSArray<EOVlans> vlansPourPersonne = EOVlans.fetchAllVlansPourPersonne(individu, editingContext);
		assertEquals("La liste doit contenir au moins " + vlanTypesPourIndividu.size() + " vlan(s)", vlanTypesPourIndividu.size(), vlansPourPersonne.size());
		
		NSArray<String> cVlansPourPersonne = (NSArray<String>) vlansPourPersonne.valueForKey(EOVlans.C_VLAN_KEY);
		assertTrue("La liste doit contenir tous les vlans pour un individus",
				ERXArrayUtilities.arrayContainsArray(cVlansPourPersonne, vlanTypesPourIndividu));
	}

	@Test
	public void testFetchAllVlansPourGroupeUnix() {

	   EOStructure groupeUnix = initStructure("UNIX", true, false);

	    
		NSArray<EOVlans> vlans = EOVlans.fetchAllVlansPourPersonne(groupeUnix, editingContext);
		assertEquals("La liste doit contenir un et un seul vlan", 1, vlans.size());
		
		NSArray<String> cVlansPourPersonne = (NSArray<String>) vlans.valueForKey(EOVlans.C_VLAN_KEY);
		assertTrue("La liste doit contenir tous les vlans pour un individus", cVlansPourPersonne.contains(EOVlans.VLAN_G));
	
	}
 
	@Test
	public void testFetchAllVlansPourGroupeDiplome() {

	   EOStructure groupe = initStructure("DIPLOME", false, true);

	    
		NSArray<EOVlans> vlans = EOVlans.fetchAllVlansPourPersonne(groupe, editingContext);
		
		assertEquals("La liste doit contenir un et un seul vlan", 1, vlans.size());
		
		NSArray<String> cVlansPourPersonne = (NSArray<String>) vlans.valueForKey(EOVlans.C_VLAN_KEY);
		assertTrue("La liste doit contenir tous les vlans pour un individus", cVlansPourPersonne.contains(EOVlans.VLAN_D));
	
	}

	private EOStructure initStructure(String cStructure, boolean unix, boolean diplome) {
        
        EOStructure structure = mock(EOStructure.class);
        doReturn(EOStructure.ENTITY_NAME).when(structure).entityName();
        doReturn(cStructure).when(structure).construireCStructure();
        doReturn(false).when(structure).isIndividu();
        doReturn(true).when(structure).isStructure();
        doReturn(unix).when(structure).laStructurePossedeUnTypeGroupeUnix();
        doReturn(diplome).when(structure).laStructurePossedeUnTypeGroupeDiplome();

        editingContext.insertSavedObject(structure);
        
        return structure;
    }
    
    private void initVlans(String vlansType, String priseEnCompte) {

        EOVlans vlans = editingContext.createSavedObject(EOVlans.class);
        vlans.setCVlan(vlansType);
        vlans.setPrise_Compte(priseEnCompte);
        arrayOfDummyVlans.add(vlans);
    }
    
    
}
