package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.TemporaryEditingContext;

import er.extensions.eof.ERXQ;

/**
 * 
 * 
 * @author Alexis Tual
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EOStructureForGroupeTest {

	private EOAssociation associationPartenaire;
	private EOIndividu individu;
	private EOStructure structure;
	private NSTimestamp now = new NSTimestamp();
	
	@Rule
	public TemporaryEditingContext editingContext = new TemporaryEditingContext("FwkCktlPersonne");
	private EOAssociation associationDirecteur;
	private EOAssociation associationPresident;
	
	private void creerIndividuStub() {
		EOCivilite civilite = EOCivilite.createEOCivilite(editingContext, "M.", new NSTimestamp(), new NSTimestamp());
		individu = new EOIndividu();
		individu = spy(individu);
		doNothing().when(individu).validateForInsert();
		doReturn(false).when(individu).objectHasChanged();
		editingContext.insertObject(individu);
		individu.setNoIndividu(999);
		individu.setPersId(99);
		individu.setNomUsuel("Nom");
		individu.setPrenom("Prenom");
		individu.setDModification(now);
		individu.setDCreation(now);
		individu.setToCiviliteRelationship(civilite);
		// Gros hack des familles pour désactiver le read-only sur Personne -_-
		EOEntity entity = EOUtilities.entityForClass(editingContext, EOPersonne.class);
		entity.setReadOnly(false);
		EOPersonne.createEOPersonne(editingContext, 99, "Nom Prenom", 999, "I");
		final PersonneDelegateCacheManager cacheManager = mock(PersonneDelegateCacheManager.class);
		when(cacheManager.getGroupesAdministres(editingContext)).thenReturn(new NSArray<EOStructure>(structure));
		PersonneDelegate personneDelegate = new PersonneDelegate(individu) {
			@Override
			protected PersonneDelegateCacheManager getCacheManager() {
				return cacheManager;
			}
			
		};
		individu.setPersonneDelegate(personneDelegate);
	}
	
	private void creerStructureStub() {
		structure = new EOStructure();
		structure = spy(structure);
		doReturn("888").when(structure).construireCStructure();
		doReturn(false).when(structure).objectHasChanged();
		editingContext.insertObject(structure);
		structure.setLlStructure("Groupe");
		structure.setPersId(88);
		EOTypeStructure typeStructure = EOTypeStructure.createEOTypeStructure(editingContext, EOTypeStructure.TYPE_STRUCTURE_E, now, now);
		EOTypeGroupe typeGroupe = EOTypeGroupe.createEOTypeGroupe(editingContext, now, now, "G", "O", "N");
		EORepartTypeGroupe repartTypeGroupe = EORepartTypeGroupe.createEORepartTypeGroupe(
				editingContext, "888", now, now, "G", typeGroupe);
		repartTypeGroupe.setPersIdCreation(99);
		repartTypeGroupe.setPersIdModification(99);
		structure.addToToRepartTypeGroupesRelationship(repartTypeGroupe);
		structure.setCTypeStructure(EOTypeStructure.TYPE_STRUCTURE_E);
		structure.setToTypeStructureRelationship(typeStructure);
		structure.setToStructurePereRelationship(structure);
	}
	
	@Before
	public void setUp() {
		creerStructureStub();
		creerIndividuStub();
		associationPartenaire = EOAssociation.createEOAssociation(editingContext, "PART", "Partenaire");
		associationDirecteur = EOAssociation.createEOAssociation(editingContext, "DIR", "Directeur");
		associationPresident = EOAssociation.createEOAssociation(editingContext, "PRES", "President");
		editingContext.saveChanges();
	}
	
	@Test
	public void testSupprimerUnRoleEnMemoire() {
		EOStructureForGroupeSpec.definitLesRoles(
				editingContext, individu, new NSArray<EOAssociation>(associationPartenaire), 
				structure, 99, null, null, null, null, null, true);
		EOStructureForGroupeSpec.sharedInstance().supprimerUnRole(
				editingContext, individu, associationPartenaire, structure, 99, null);
		editingContext.saveChanges();
		assertTrue(true);
	}
	
	@Test
	public void testSupprimerUnRole() {
		EOStructureForGroupeSpec.definitLesRoles(
				editingContext, individu, new NSArray<EOAssociation>(associationPartenaire), 
				structure, 99, null, null, null, null, null, true);
		editingContext.saveChanges();
		EOStructureForGroupeSpec.sharedInstance().supprimerUnRole(
				editingContext, individu, associationPartenaire, structure, 99, null);
		editingContext.saveChanges();
		EOQualifier qual = 
				EORepartAssociation.TO_STRUCTURE.eq(structure)
					.and(EORepartAssociation.PERS_ID.eq(individu.persId()))
					.and(EORepartAssociation.TO_ASSOCIATION.eq(associationPartenaire));
		NSArray<EORepartAssociation> results = EORepartAssociation.fetchAll(editingContext, qual);
		assertTrue(results.isEmpty());
	}
	
	@Test
	public void testAjouterUnRole() {
		EOQualifier qual = 
				EORepartAssociation.TO_STRUCTURE.eq(structure)
					.and(EORepartAssociation.PERS_ID.eq(individu.persId()));
		
		NSArray<EORepartAssociation> results = EORepartAssociation.fetchAll(editingContext, qual);
		assertTrue(results.isEmpty());
		
		EOStructureForGroupeSpec.ajouterUnRole(
				editingContext, individu, associationPartenaire, 
				structure, 99, null, null, null, null, null, true);
		editingContext.saveChanges();
		results = EORepartAssociation.fetchAll(editingContext, qual);
		assertEquals(1, results.count());
		
		EOStructureForGroupeSpec.ajouterUnRole(
				editingContext, individu, associationDirecteur, 
				structure, 99, null, null, null, null, null, true);
		editingContext.saveChanges();
		results = EORepartAssociation.fetchAll(editingContext, qual);
		assertEquals(2, results.count());
		
		EOStructureForGroupeSpec.ajouterUnRole(
				editingContext, individu, associationDirecteur, 
				structure, 99, null, null, null, null, null, true);
		editingContext.saveChanges();
		results = EORepartAssociation.fetchAll(editingContext, qual);
		assertEquals(2, results.count());



	}
	
	
}
