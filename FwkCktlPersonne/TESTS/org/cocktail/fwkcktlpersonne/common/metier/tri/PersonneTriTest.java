package org.cocktail.fwkcktlpersonne.common.metier.tri;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.junit.Before;
import org.junit.Test;

public class PersonneTriTest {

	
	private List<IPersonne> listePersonnes = new ArrayList<IPersonne>();
	private PersonneTri personneTri = new PersonneTri();

	@Before
	public void setUp() throws Exception {
		listePersonnes.add(initPersonne("1", "C_1"));
		listePersonnes.add(initPersonne("2", "A_1"));
	}

	@Test
	public void testTrierParLibelleAffichage() {
		personneTri.trierParLibelleAffichage(listePersonnes);
		assertEquals("A_1", listePersonnes.get(0).getNomCompletAffichage());
		assertEquals("C_1", listePersonnes.get(1).getNomCompletAffichage());
		
	}
	
	/**
    * Initialisation d'une IPersonne
    * @param persId : persId
    * @param libelleAffichage : libellé d'affichage
    * @return une structure
    */
	public IPersonne initPersonne(String persId, String libelleAffichage) {
		IPersonne personne = mock(IPersonne.class);
       
		when(personne.getNomCompletAffichage()).thenReturn(libelleAffichage);
		
		return personne;
   }
	
}
