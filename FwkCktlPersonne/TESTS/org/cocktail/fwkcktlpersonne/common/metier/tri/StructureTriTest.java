package org.cocktail.fwkcktlpersonne.common.metier.tri;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.junit.Before;
import org.junit.Test;

/**
 * Test de la classe StructureTri
 * @author Chama LAATIK
 *
 */
public class StructureTriTest {
	
	private List<IStructure> listeStructures = new ArrayList<IStructure>();
	private StructureTri structureTri = new StructureTri();

	@Before
	public void setUp() throws Exception {
		listeStructures.add(initStructure("1", "A_1", "B_1", "C_1"));
		listeStructures.add(initStructure("2", "C_2", "B_2", "A_2"));
		listeStructures.add(initStructure("3", "B_3", "A_3", "C_3"));
		listeStructures.add(initStructure("4", "A_1", "A_3", "A_2"));
	}

	@Test
	public void testTrierParLibelleAffichage() {
		structureTri.trierParLibelleAffichage(listeStructures);
		assertEquals("A_2", listeStructures.get(0).getNomCompletAffichage());
		assertEquals("2", listeStructures.get(0).cStructure());
	}

	@Test
	public void testTrierParLibelleLong() {
		structureTri.trierParLibelleLong(listeStructures);
		assertEquals("A_3", listeStructures.get(0).llStructure());
		assertEquals("3", listeStructures.get(0).cStructure());
	}

	@Test
	public void testTrierParLibelleCourt() {
		structureTri.trierParLibelleCourt(listeStructures);
		assertEquals("A_1", listeStructures.get(0).lcStructure());
		assertEquals("1", listeStructures.get(0).cStructure());
	}
	
	/**
    * Initialisation d'une structure
    * @param id : cStructure
    * @param libelleCourt : libellé court
    * @param libelleLong : libellé long
    * @param libelleAffichage : libellé d'affichage
    * @return une structure
    */
	public IStructure initStructure(String id, String libelleCourt, String libelleLong, String libelleAffichage) {
		EOStructure structure = mock(EOStructure.class);
       
		when(structure.cStructure()).thenReturn(String.valueOf(id));
		when(structure.lcStructure()).thenReturn(libelleCourt);
		when(structure.llStructure()).thenReturn(libelleLong);
		when(structure.getNomCompletAffichage()).thenReturn(libelleAffichage);
		
		return structure;
   }

}
