package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TypageException;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.Validateur;
import org.cocktail.fwkcktlwebapp.server.CktlEOEditingContextFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.wounit.rules.TemporaryEditingContext;

import er.extensions.eof.ERXEC;

public class NestedEditingContextTest {

    @Rule
    public TemporaryEditingContext editingContext = new TemporaryEditingContext("FwkCktlPersonne");
    
    /**
     * Classe validateur bidon pour les tests
     */
    public static class ValidateurBidon implements Validateur {
        
        public void valider(String valeurParam) throws TypageException {
            
        }
    };
    
    @Before
    public void setUp() {
        EOGrhumParametres.choixVerifications.setObjectForKey(ValidateurBidon.class, "BIDON");
    }
    
    private void manipuleNested() {
        EOGrhumParametresType paramType = EOGrhumParametresType.createEOGrhumParametresType(editingContext, 999);
        paramType.setTypeIdInterne("BIDON");
        editingContext.saveChanges();
        // Création d'une grande hiérarchie d'ec nested
        EOEditingContext nested1 = ERXEC.newEditingContext(editingContext);
        EOEditingContext nested2 = ERXEC.newEditingContext(nested1);
        EOEditingContext nested3 = ERXEC.newEditingContext(nested2);
        EOEditingContext nested4 = ERXEC.newEditingContext(nested3);
        EOEditingContext nested5 = ERXEC.newEditingContext(nested4);
        EOEditingContext nested6 = ERXEC.newEditingContext(nested5);
        EOEditingContext nested7 = ERXEC.newEditingContext(nested6);
        EOGrhumParametresType nestedParamType = paramType.localInstanceIn(nested7);
        // Création du param dans un nested assez loin
        for (int i = 0; i < 400; i++) {
            EOGrhumParametres param = EOGrhumParametres.createEOGrhumParametres(nested7);
            param.setParamKey("Test");
            param.setParamValue("test");
            param.setToParametresTypeRelationship(nestedParamType);
            nested7.saveChanges();
            nested6.saveChanges();
            nested5.saveChanges();
            nested4.saveChanges();
            nested3.saveChanges();
            nested2.saveChanges();
            nested1.saveChanges();
            editingContext.saveChanges();
        }
        editingContext.deleteObject(paramType);
        System.gc();
        editingContext.saveChanges();
    }
    
    /**
     * Test très particulier reproduisant le fameux bug sur les nested ecs.
     * Ce test est très fortement inspiré de :
     * 
     * https://github.com/wocommunity/wonder/blob/bed35de926d0fe53c207b2fea7f5e3d1922d0ddb/Tests/ERXTest/Sources/er/extensions/eof/ERXECTest.java
     *  
     *  
     */
    @Test
    public void testNestedECsSansFix() {
        try {
            // On insère l'eo père dans l'ec de base
            manipuleNested();
        } catch (Exception e) {
            // L'erreur contient bien quelque chose du genre "no database snapshot"
            Assert.assertTrue(e instanceof IllegalStateException);
            Assert.assertTrue(e.getMessage().contains("snapshot"));
            e.printStackTrace();
        }
    }
    
    @Test
    public void testNestedEcAvecFix() {
        ERXEC.setFactory(new CktlEOEditingContextFactory());
        try {
            // On insère l'eo père dans l'ec de base
            manipuleNested();
        } catch (Exception e) {
            Assert.fail("Il ne doit plus y avoir de pb d'eos fantomes !");
            e.printStackTrace();
        }
    }
    
}
