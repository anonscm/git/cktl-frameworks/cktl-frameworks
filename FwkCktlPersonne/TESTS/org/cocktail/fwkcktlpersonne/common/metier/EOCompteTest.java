package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSArray;
import com.wounit.rules.MockEditingContext;

public class EOCompteTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("FwkCktlPersonne");
    
    private EOCompte compte;
    
    @Before
    public void setUp() {
        compte = ec.createSavedObject(EOCompte.class);
    }
    
    @Test
    public void testObjectHasNotChanged() {
        NSArray<String> changedKeys = new NSArray<String>(EOCompte.CPT_PRINCIPAL_KEY);
        
        boolean hasChanged = compte.sommeKeyHasChanged(changedKeys, EOCompte.CPT_PRINCIPAL_KEY);
        
        assertFalse("Le compte ne doit pas être détecté comme modifié, l'attribut cptPrincipal devant être ignoré", hasChanged);
    }
    
    @Test
    public void someKeyHasChangedKeyNull() {
        boolean hasChanged = compte.sommeKeyHasChanged(null, EOCompte.CPT_PRINCIPAL_KEY);
        
        assertFalse(hasChanged);
    }
    
    @Test
    public void someKeyHasChangedKeyEmpty() {
        boolean hasChanged = compte.sommeKeyHasChanged(NSArray.<String>emptyArray(), EOCompte.CPT_PRINCIPAL_KEY);
        
        assertFalse(hasChanged);
    }
    
        
    @Test
    public void testObjectHasChanged() {
        NSArray<String> changedKeys = new NSArray<String>(EOCompte.CPT_PRINCIPAL_KEY, EOCompte.CPT_CHARTE_OUI);

        boolean hasChanged = compte.sommeKeyHasChanged(changedKeys, EOCompte.CPT_PRINCIPAL_KEY);
        
        assertTrue("Le compte doit être détecté comme modifié", hasChanged);
    }
    
    @Test
    public void testSupprimerCompteEmail() {
		EOCompteEmail unCompteEmail = EOCompteEmail.creerInstance(ec);
		unCompteEmail.setEmailFormatte("stephen.king@dsi-cocktail.fr");
		unCompteEmail.setToCompteRelationship(compte);
		
		Assert.assertEquals("stephen.king", compte.toCompteEmail().cemEmail());
		Assert.assertEquals("dsi-cocktail.fr", compte.toCompteEmail().cemDomaine());

    	//A la création d'un email, le trigger GRHUM.TRG_COMPTE initialise le champ COMPTE.CPT_DOMAINE
		compte.setCptDomaine("dsi-cocktail.fr");
		
		compte.supprimerCompteEmail(unCompteEmail);
		
		Assert.assertNull("L'email doit être null après la suppression", compte.toCompteEmail());
		Assert.assertNull("Le domaine du compte doit être null après la suppression de l'email", compte.cptDomaine());
    }
    
}
