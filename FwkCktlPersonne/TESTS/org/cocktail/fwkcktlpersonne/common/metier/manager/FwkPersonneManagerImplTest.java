package org.cocktail.fwkcktlpersonne.common.metier.manager;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuControle;
import org.cocktail.fwkcktlpersonne.common.metier.services.IIndividuService;
import org.cocktail.fwkcktlpersonne.common.metier.services.IndividuServiceImpl;
import org.junit.Test;

import com.thoughtworks.xstream.converters.collections.PropertiesConverter;

import er.extensions.foundation.ERXProperties;

public class FwkPersonneManagerImplTest {

	@Test
	public void getIndividuService() {
		
		IFwkPersonneManager manager = new FwkPersonneManagerImpl();
		IIndividuService service = manager.getIndividuService();
		
		
		
		assertEquals(IndividuServiceImpl.class, service.getClass());
		assertEquals(10, service.getListeControles().size());
		assertEquals(true, service.getListeControles().contains("InseeSexe"));
		assertEquals(true, service.getListeControles().contains("InseeAnneeNaissance"));
		assertEquals(true, service.getListeControles().contains("InseeMoisNaissance"));
		assertEquals(true, service.getListeControles().contains("InseeCodePaysEtranger"));
		assertEquals(true, service.getListeControles().contains("InseeProtectoratTunisien"));
		assertEquals(true, service.getListeControles().contains("InseeAlgerieFrancaiseEtProtectoratMarocain"));
		assertEquals(true, service.getListeControles().contains("InseeSeineEtSeineEtOise"));
		assertEquals(true, service.getListeControles().contains("InseeCorse"));
		assertEquals(true, service.getListeControles().contains("InseePaysNaissanceFrance"));
		assertEquals(true, service.getListeControles().contains("InseeDepartementNaissanceFrance"));
//		assertEquals("InseeSexe", service.getListeControles().get(0));
//		assertEquals("InseeCodePaysEtranger", service.getListeControles().get(1));
//		assertEquals("InseeProtectoratTunisien", service.getListeControles().get(2));
//		assertEquals("InseeAlgerieFrancaiseEtProtectoratMarocain", service.getListeControles().get(3));
//		assertEquals("InseeSeineEtSeineEtOise", service.getListeControles().get(4));
//		assertEquals("InseeCorse", service.getListeControles().get(5));
//		assertEquals("InseePaysNaissanceFrance", service.getListeControles().get(6));
//		assertEquals("InseeDepartementNaissanceFrance", service.getListeControles().get(7));
//		assertEquals("InseeAnneeNaissance", service.getListeControles().get(8));
//		assertEquals("InseeMoisNaissance", service.getListeControles().get(9));
		
	}

	@Test
	public void getIndividuServiceProperties() throws Exception {

		IFwkPersonneManager manager = new FwkPersonneManagerImpl("FwkPersonne.properties.test");
		IIndividuService service = manager.getIndividuService();
		
		assertEquals(IndividuServiceImpl.class, service.getClass());
		assertEquals(5, service.getListeControles().size());
		assertEquals(true, service.getListeControles().contains("InseeSexe"));
		assertEquals(true, service.getListeControles().contains("InseeAnneeNaissance"));
		assertEquals(true, service.getListeControles().contains("InseeMoisNaissance"));
		assertEquals(true, service.getListeControles().contains("InseeCodePaysEtranger"));
		assertEquals(true, service.getListeControles().contains("InseeCorse"));
//		assertEquals("InseeSexe", service.getListeControles().get(0));
//		assertEquals("InseeCodePaysEtranger", service.getListeControles().get(1));
//		assertEquals("InseeCorse", service.getListeControles().get(2));
//		assertEquals("InseeAnneeNaissance", service.getListeControles().get(3));
//		assertEquals("InseeMoisNaissance", service.getListeControles().get(4));
		
	}

}
