package org.cocktail.fwkcktlpersonne.common.metier.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.Helper;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuCondition;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuControle;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeAnneeNaissance;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeSexe;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.junit.Before;
import org.junit.Test;

public class IndividuServiceImplTest {

	IndividuServiceImpl service;
	
	@Before
	public void setupControles() {

		List<IIndividuControle> controles = new ArrayList<IIndividuControle>();
	
		controles.add(new InseeSexe());
		controles.add(new InseeAnneeNaissance());
		
		Map<IIndividuCondition,List<IIndividuControle>> conditions = new HashMap<IIndividuCondition,List<IIndividuControle>>();
		conditions.put(IndividuServiceImpl.CONDITION_TJRS_VRAIE, controles);
		
    	service = new IndividuServiceImpl();
		service.setControles(conditions);
		
	}
	
	@Test
    public void controleInvalide() {
        
    	IndividuServiceImpl service = new IndividuServiceImpl();

    	IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(mock(ICivilite.class));
        when(individu.estHomme()).thenReturn(true);
        when(individu.getCodeInsee()).thenReturn(new CodeInsee("2234567890123"));

        assertFalse(service.controle(individu, new InseeSexe()).valide());
        
    }
    
    @Test
	public void controleValide() {
	    
    	IndividuServiceImpl service = new IndividuServiceImpl();

    	IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(mock(ICivilite.class));
	    when(individu.estHomme()).thenReturn(true);
        when(individu.getCodeInsee()).thenReturn(new CodeInsee("1234567890123"));
	    
        assertEquals(ResultatControle.RESULTAT_OK, service.controle(individu, new InseeSexe()));
	    
	}
	
    @Test
    public void checkNonCheckable() {
        
    	IndividuServiceImpl service = new IndividuServiceImpl();

    	IIndividu individu = mock(IIndividu.class);
        when(individu.getCivilite()).thenReturn(null);
        when(individu.estHomme()).thenReturn(true);
        
        assertEquals(ResultatControle.RESULTAT_OK, service.controle(individu, new InseeSexe()));
        
    }


	
    @Test
    public void appliqueControles() throws ParseException {
        
        assertInseeException("Sexe KO", Helper.initHomme("2500596351123", "01/05/1950", "99", null), InseeSexe.REGLE_HOMME);
        assertInseeException("Annnée KO", Helper.initHomme("1500596351123", "01/05/1940", "99", null),
        		InseeAnneeNaissance.ANNEE_NAISSANCE);
        assertInseeNoException("Sexe OK", Helper.initIndividu(null,"2500596351123", "01/05/1950", "99", null));
        
    }


    protected void assertInseeNoException(String message, IIndividu individu) {
        
        List<ResultatControle> resultats = service.appliqueControles(individu);
        assertEquals(0, resultats.size());
    }

    protected void assertInseeException(String message, IIndividu individu, String erreur) {
        List<ResultatControle> resultats = service.appliqueControles(individu);
        assertTrue(resultats.size() > 0);
        assertEquals(erreur, resultats.get(0).getCode());
    }


    @Test
    public void appliqueControlesConditionFalse() throws ParseException {
        
    	List<IIndividuControle> controles = new ArrayList<IIndividuControle>();
    	
		controles.add(new InseeSexe());
		controles.add(new InseeAnneeNaissance());
		
		Map<IIndividuCondition,List<IIndividuControle>> conditions = new HashMap<IIndividuCondition,List<IIndividuControle>>();
		conditions.put(IndividuServiceImpl.CONDITION_TJRS_FAUSSE, controles);
		
		service.setControles(conditions);
    	
    	
        assertInseeNoException("Aucun contrôle", Helper.initHomme("2500596351123", "01/05/1950", "99", null));
        assertInseeNoException("Aucun contrôle même sur Individu OK", Helper.initIndividu(null,"2500596351123", "01/05/1950", "99", null));
        
    }
}
