package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

import er.extensions.eof.ERXModelGroup;

/**
 * Tests assez génériques sur nos classes EO de base.
 *
 * @author Alexis Tual
 *
 */
public class AfwkPersRecordTest {

	@Rule
	public MockEditingContext editingContext = new MockEditingContext("FwkCktlPersonne", "FwkCktlDroits");

	/**
	 * Test pour s'assurer que les méthodes suivantes sont appelées dans l'ordre et une seule fois :
	 * - {@link AfwkPersRecord#validateObjectMetier()}
	 * - {@link AfwkPersRecord#validateBeforeTransactionSave()}
	 * pour tous les EO des modèles du FwkPersonne insérés.
	 */
	@Test
	public void testInvocationMethodesValidationOnInsert() {
		NSArray<EOModel> eomodels = ERXModelGroup.defaultGroup().models();
		for (EOModel eomodel : eomodels) {
			for (EOEntity entity : eomodel.entities()) {
				EOGenericRecord persGr =
						(EOGenericRecord) entity.classDescriptionForInstances().createInstanceWithEditingContext(
								editingContext, null);
				if (persGr instanceof AfwkPersRecord) {
					AfwkPersRecord pers = (AfwkPersRecord)persGr;
					pers = spy(pers);
					doNothing().when(pers).awakeFromInsertion(editingContext);
					doNothing().when(pers).validateObjectMetier();
					doNothing().when(pers).validateBeforeTransactionSave();
					editingContext.insertObject(pers);
					try {
						mockerLesMethodesAvantValidation(pers);
						editingContext.saveChanges();
					} catch (Exception e) {
						// Surement tout un gros tas d'exception dont on se fout
						// Sauf si ça concerne un mock
						if (e.getMessage() != null && e.getMessage().contains("mock")) {
							throw new RuntimeException(e);
						}
					} finally {
						editingContext.revert();
					}
					System.out.println("Entité " + entity.name());
					InOrder orderedExec = inOrder(pers);
					orderedExec.verify(pers, times(1)).validateForInsert();
					orderedExec.verify(pers, times(1)).validateForSave();
					orderedExec.verify(pers, times(1)).validateObjectMetier();
					orderedExec.verify(pers, times(1)).validateBeforeTransactionSave();
					orderedExec.verify(pers, times(0)).validateForUpdate();
					orderedExec.verify(pers, times(0)).validateForDelete();
				}
			}
		}
		assertTrue(true);
	}

	/**
	 * Hélas on ne peut pas tester aussi simplement, l'update,
	 * car le MockEc ne track pas les changements sur les mock EO...
	 * Il faudrait donc que nous insérions des eos (donc renseigner des valeurs)
	 * pour pouvoir les modifier et tester le comportement...
	 */
	@Test
	public void testInvocationMethodesValidationOnUpdate() {
		assertTrue(true);
	}

	/**
	 * Doit mocker les méthodes de <code>genericRecord</code> qui sont appelées
	 * juste avant {@link AfwkPersRecord#validateObjectMetier()}.
	 *
	 * @param genericRecord l'eo à mocker
	 */
	public void mockerLesMethodesAvantValidation(AfwkPersRecord genericRecord) {
		if (genericRecord instanceof EOIndividu) {
			EOIndividu individu = (EOIndividu)genericRecord;
			doNothing().when(individu).checkCreateur();
			doReturn(false).when(individu).registerDetectedSpec();
		} else if (genericRecord instanceof EORepartTypeGroupe) {
			EORepartTypeGroupe rptTypeGroupe = (EORepartTypeGroupe)genericRecord;
			doNothing().when(rptTypeGroupe).checkUsers();
		}
	}

	@Test
	public void testHasDirtyAttributes() throws Exception {
		EOAcademie academie = EOAcademie.creerInstance(editingContext);
		academie.setCAcademie("nvlleValeur");
		assertTrue(academie.hasDirtyAttributes());
	}

	@Test
	public void testHasDirtyAttributesWhenNoChange() throws Exception {
		EOAcademie academie = EOAcademie.creerInstance(editingContext);
		assertFalse(academie.hasDirtyAttributes());
	}

	@Test
	public void testHasDirtyAttributesWhenNoToOneRelation() throws Exception {
		EOAcademie academie = EOAcademie.creerInstance(editingContext);
		assertFalse(academie.hasDirtyAttributes());
	}

	@Test
	public void testHasDirtyAttributesWhenToOneRelationDirty() {
		EOCorps corps = EOCorps.creerInstance(editingContext);
		corps.setToTypePopulationRelationship(EOTypePopulation.creerInstance(editingContext));
		assertTrue(corps.hasDirtyAttributes());
	}

	@Test
	public void testHasDirtyAttributesWhenToManyRelationDirty() {
		EOCorps corps = EOCorps.creerInstance(editingContext);
		corps.addToToGradesRelationship(EOGrade.creerInstance(editingContext));
		assertFalse(corps.hasDirtyAttributes());
	}

}
