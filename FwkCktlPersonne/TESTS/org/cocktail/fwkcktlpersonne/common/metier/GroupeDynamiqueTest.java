/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique;
import org.cocktail.fwkcktlpersonne.common.metier.EORegle;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEC;

/**
 * Classe de test des groupes dynamiques avec règles.
 * Les règles ne sont pas sauvegardés, on teste juste le calcul, nécessitant bien sûr que les données de test soient
 * présentes.
 * On teste seulement l'enregistrement d'un groupe dynamique et de son calcul.
 * 
 * @see AllTests
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class GroupeDynamiqueTest {

    private NSMutableArray<EOEnterpriseObject> eosToDelete;
    private EOEditingContext editingContext;
    
    @Before
    public void setUp() {
        editingContext = ERXEC.newEditingContext();
        eosToDelete = new NSMutableArray<EOEnterpriseObject>();
    }
    
    public void testGroupeDynamiqueRegleSimpleMembreDe() {
        
        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        String cStructure = AllTests.CStructureTest;
        // Création d'un groupe dynamique
        EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(editingContext);
        groupeDynamique.setGrpdLc("Groupe dynamique de test 1");
        // Création d'une règle simple : utilisateur PERS_ID_DEFAULT membre de divers
        EORegle regleRacine = EORegle.creerRegleSimple(editingContext, utilisateurPersId);
        regleRacine.setRValue(cStructure);
        regleRacine.setToRegleOperateurRelationship(EORegleOperateur.getRegleOperateurMembre());
        groupeDynamique.setToRegleRelationship(regleRacine);
        // Calcul des persId et vérification que PERS_ID_DEFAULT s'y trouve
        NSArray<Integer> persIds = groupeDynamique.calculerPersIds();
        assertTrue(persIds.containsObject(utilisateurPersId));
    }
    
    public void testGroupeDynamiqueRegleSimpleAPourRole() {
        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        String assId = AllTests.AssIdDirecteur.toString();
        // Création d'un groupe dynamique
        EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(editingContext);
        groupeDynamique.setGrpdLc("Groupe dynamique de test 2");
        // Création d'une règle simple : utilisateur PERS_ID_DEFAULT membre de divers
        EORegle regleRacine = EORegle.creerRegleSimple(editingContext, utilisateurPersId);
        regleRacine.setRValue(assId);
        regleRacine.setToRegleOperateurRelationship(EORegleOperateur.getRegleOperateurRole());
        groupeDynamique.setToRegleRelationship(regleRacine);
        // Calcul des persId et vérification que PERS_ID_DEFAULT s'y trouve
        NSArray<Integer> persIds = groupeDynamique.calculerPersIds();
        assertTrue(persIds.containsObject(utilisateurPersId));
    }
    
    public void testGroupeDynamiqueRegleSimpleAPourRoleDansGroupe() {
        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        String cStructure = AllTests.CStructureTest;
        String assId = AllTests.AssIdDirecteur.toString();
        // Création d'un groupe dynamique
        EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(editingContext);
        groupeDynamique.setGrpdLc("Groupe dynamique de test 3");
        // Création d'une règle simple : utilisateur PERS_ID_DEFAULT membre de divers
        EORegle regleRacine = EORegle.creerRegleSimple(editingContext, utilisateurPersId);
        regleRacine.setRValue(assId);
        regleRacine.setRValue2(cStructure);
        regleRacine.setToRegleOperateurRelationship(EORegleOperateur.getRegleOperateurRoleDansGroupe());
        groupeDynamique.setToRegleRelationship(regleRacine);
        // Calcul des persId et vérification que PERS_ID_DEFAULT s'y trouve
        NSArray<Integer> persIds = groupeDynamique.calculerPersIds();
        assertTrue(persIds.containsObject(utilisateurPersId));
    }
    
    public void testGroupeDynamiqueRegleComplexe1() {
        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        String cStructure = AllTests.CStructureTest;
        String assId = AllTests.AssIdDirecteur.toString();
        // Création d'un groupe dynamique
        EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(editingContext);
        groupeDynamique.setGrpdLc("Groupe dynamique de test 4");
        // Création d'une règle simple : utilisateur PERS_ID_DEFAULT membre de divers
        EORegle regleRacine = EORegle.creerRegleSimple(editingContext, utilisateurPersId);
        regleRacine.setRValue(cStructure);
        EORegle regleRole = EORegle.creerRegleSimple(editingContext, utilisateurPersId);
        regleRole.setRValue(assId);
        regleRole.setToRegleOperateurRelationship(EORegleOperateur.getRegleOperateurRole());
        regleRacine = regleRacine.and(regleRole, utilisateurPersId);
        groupeDynamique.setToRegleRelationship(regleRacine);
        // Calcul des persId et vérification que PERS_ID_DEFAULT s'y trouve
        NSArray<Integer> persIds = groupeDynamique.calculerPersIds();
        assertTrue(persIds.containsObject(utilisateurPersId));
    }
    
    public void testCalculGroupeDynamique() {
        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        String cStructure = AllTests.CStructureTest;
        // Création d'un groupe dynamique
        EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(editingContext);
        groupeDynamique.setGrpdLc("Groupe dynamique de test 1");
        groupeDynamique.setGrpdDescription("Groupe dynamique de test 1");
        // Création d'une règle simple
        EORegle regleRacine = EORegle.creerRegleSimple(editingContext, utilisateurPersId);
        regleRacine.setRValue(cStructure);
        regleRacine.setToRegleOperateurRelationship(EORegleOperateur.getRegleOperateurMembre());
        groupeDynamique.setToRegleRelationship(regleRacine);
        // Sauvegarder le groupe
        editingContext.saveChanges();
        eosToDelete.addObject(groupeDynamique);
        eosToDelete.addObject(regleRacine);
    }
    
    @After
    public void tearDown() {
        if (!eosToDelete.isEmpty()) {
            for (EOEnterpriseObject eo : eosToDelete) {
                editingContext.deleteObject(eo);
            }
            editingContext.saveChanges();
        }
    }

}
