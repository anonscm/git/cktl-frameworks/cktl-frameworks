/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.insee;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CodeInseeTest {

    @Test
    public void isHorsFrance() {
        
        assertFalse((new CodeInsee("1510375012345")).isHorsFrance());
        assertTrue((new CodeInsee("1510396012345")).isHorsFrance());
        assertTrue((new CodeInsee("1510399012345")).isHorsFrance());
        assertTrue((new CodeInsee("1510391012345")).isHorsFrance());
        assertTrue((new CodeInsee("1510392012345")).isHorsFrance());
        assertTrue((new CodeInsee("1510393012345")).isHorsFrance());
        assertTrue((new CodeInsee("1510394012345")).isHorsFrance());
        assertTrue((new CodeInsee("1510395012345")).isHorsFrance());
 
        assertTrue((new CodeInsee("1630391012345")).isHorsFrance());
        assertTrue((new CodeInsee("1620391012345")).isHorsFrance());

    }

    @Test
    public void isAlgerieFrancaiseEtProtectoratMarocain() {
        
        assertFalse((new CodeInsee("1510375012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertFalse((new CodeInsee("1510396012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertFalse((new CodeInsee("1510399012345")).isAlgerieFrancaiseEtProtectoratMarocain());

        assertTrue((new CodeInsee("1510391012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertTrue((new CodeInsee("1510392012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertTrue((new CodeInsee("1510393012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertTrue((new CodeInsee("1510394012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertTrue((new CodeInsee("1510395012345")).isAlgerieFrancaiseEtProtectoratMarocain());
 
        assertTrue((new CodeInsee("1630391012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertTrue((new CodeInsee("1620391012345")).isAlgerieFrancaiseEtProtectoratMarocain());

    }
    
    @Test
    public void isAlgerieFrancaise() {
        
        assertFalse((new CodeInsee("1510375012345")).isAlgerieFrancaise());
        assertFalse((new CodeInsee("1510396012345")).isAlgerieFrancaise());
        assertFalse((new CodeInsee("1510399012345")).isAlgerieFrancaise());

        assertTrue((new CodeInsee("1510391012345")).isAlgerieFrancaise());
        assertTrue((new CodeInsee("1510392012345")).isAlgerieFrancaise());
        assertTrue((new CodeInsee("1510393012345")).isAlgerieFrancaise());
        assertTrue((new CodeInsee("1510394012345")).isAlgerieFrancaise());
        assertFalse((new CodeInsee("1510395012345")).isAlgerieFrancaise());
 
        assertTrue((new CodeInsee("1630391012345")).isAlgerieFrancaise());
        assertTrue((new CodeInsee("1620391012345")).isAlgerieFrancaise());

    }

    @Test
    public void isProtectoratMarocain() {
        
        assertFalse((new CodeInsee("1510375012345")).isProtectoratMarocain());
        assertFalse((new CodeInsee("1510396012345")).isProtectoratMarocain());
        assertFalse((new CodeInsee("1510399012345")).isProtectoratMarocain());

        assertFalse((new CodeInsee("1510391012345")).isProtectoratMarocain());
        assertFalse((new CodeInsee("1510392012345")).isProtectoratMarocain());
        assertFalse((new CodeInsee("1510393012345")).isProtectoratMarocain());
        assertFalse((new CodeInsee("1510394012345")).isProtectoratMarocain());
        assertTrue((new CodeInsee("1510395012345")).isProtectoratMarocain());
 
        assertTrue((new CodeInsee("1630395012345")).isAlgerieFrancaiseEtProtectoratMarocain());
        assertTrue((new CodeInsee("1620395012345")).isAlgerieFrancaiseEtProtectoratMarocain());

    }
    
    @Test
    public void isCorseApresDivision() {
        assertTrue((new CodeInsee("175032A012345")).isCorseApresDivision());
        assertTrue((new CodeInsee("176032A012345")).isCorseApresDivision());
        assertTrue((new CodeInsee("175032B012345")).isCorseApresDivision());
        assertTrue((new CodeInsee("176032B012345")).isCorseApresDivision());
        assertFalse((new CodeInsee("1760320012345")).isCorseApresDivision());
    }

    @Test
    public void isCorseAvantDivision() {
        assertTrue((new CodeInsee("1750320012345")).isCorseAvantDivision());
        assertTrue((new CodeInsee("1760320012345")).isCorseAvantDivision());
        assertFalse((new CodeInsee("175032A012345")).isCorseAvantDivision());
        assertFalse((new CodeInsee("175032B012345")).isCorseAvantDivision());
    }

    @Test
    public void isMoisIndetermine() {
        assertFalse((new CodeInsee("1510375012345")).isMoisIndetermine());
        assertFalse((new CodeInsee("1511375012345")).isMoisIndetermine()); // TODO a éclaircir
        assertTrue((new CodeInsee("1519975012345")).isMoisIndetermine());
    }
    
    @Test
    public void isProtectorat() {
        assertTrue((new CodeInsee("1670596012345")).isProtectorat());
        assertTrue((new CodeInsee("1680596012345")).isProtectorat()); // TODO A voir
    }
    
    @Test
    public void getDepartement() {
        assertEquals("75",(new CodeInsee("1510575112345")).getDepartement());
        assertEquals("971",(new CodeInsee("1510597112345")).getDepartement());
//        assertEquals("97",(new CodeInsee("1510597112345")).getDepartement());
        assertEquals("981",(new CodeInsee("1510598112345")).getDepartement());
//        assertEquals("98",(new CodeInsee("1510598112345")).getDepartement());
    }
    
    @Test
    public void isDomTom() {
        assertTrue((new CodeInsee("1510597112345")).isDomTom());
        assertTrue((new CodeInsee("1510598212345")).isDomTom());
        assertFalse((new CodeInsee("1510575112345")).isDomTom());
    }
    
    @Test
    public void isSeineEtOise() {
        assertTrue((new CodeInsee("1680578112345")).isSeineEtOise());
        assertFalse((new CodeInsee("1680575112345")).isSeineEtOise());
    }

    @Test
    public void isSeine() {
        assertFalse((new CodeInsee("1680578112345")).isSeine());
        assertTrue((new CodeInsee("1680575112345")).isSeine());
    }

    @Test
    public void isDepartementSeineEtOise() {
        assertFalse(CodeInsee.isDepartementSeineEtOise(1968, CodeInsee.DEPARTEMENT_PARIS));
        assertTrue(CodeInsee.isDepartementSeineEtOise(1968, CodeInsee.DEPARTEMENT_YVELINES));
        assertTrue(CodeInsee.isDepartementSeineEtOise(1968, CodeInsee.DEPARTEMENT_ESSONE));
        assertTrue(CodeInsee.isDepartementSeineEtOise(1968, CodeInsee.DEPARTEMENT_HAUTS_DE_SEINE));
        assertTrue(CodeInsee.isDepartementSeineEtOise(1968, CodeInsee.DEPARTEMENT_SEINE_ST_DENIS));
        assertTrue(CodeInsee.isDepartementSeineEtOise(1968, CodeInsee.DEPARTEMENT_VAL_DE_MARNE));
        assertTrue(CodeInsee.isDepartementSeineEtOise(1968, CodeInsee.DEPARTEMENT_VAL_D_OISE));

        assertFalse(CodeInsee.isDepartementSeineEtOise(1969, CodeInsee.DEPARTEMENT_PARIS));
        assertFalse(CodeInsee.isDepartementSeineEtOise(1969, CodeInsee.DEPARTEMENT_YVELINES));
        assertFalse(CodeInsee.isDepartementSeineEtOise(1969, CodeInsee.DEPARTEMENT_ESSONE));
        assertFalse(CodeInsee.isDepartementSeineEtOise(1969, CodeInsee.DEPARTEMENT_HAUTS_DE_SEINE));
        assertFalse(CodeInsee.isDepartementSeineEtOise(1969, CodeInsee.DEPARTEMENT_SEINE_ST_DENIS));
        assertFalse(CodeInsee.isDepartementSeineEtOise(1969, CodeInsee.DEPARTEMENT_VAL_DE_MARNE));
        assertFalse(CodeInsee.isDepartementSeineEtOise(1969, CodeInsee.DEPARTEMENT_VAL_D_OISE));
    }

    @Test
    public void isDepartementSeine() {
        assertTrue(CodeInsee.isDepartementSeine(1968,CodeInsee.DEPARTEMENT_PARIS));
        assertFalse(CodeInsee.isDepartementSeine(1968, CodeInsee.DEPARTEMENT_YVELINES));
        assertFalse(CodeInsee.isDepartementSeine(1968, CodeInsee.DEPARTEMENT_ESSONE));
        assertTrue(CodeInsee.isDepartementSeine(1968, CodeInsee.DEPARTEMENT_HAUTS_DE_SEINE));
        assertTrue(CodeInsee.isDepartementSeine(1968, CodeInsee.DEPARTEMENT_SEINE_ST_DENIS));
        assertTrue(CodeInsee.isDepartementSeine(1968, CodeInsee.DEPARTEMENT_VAL_DE_MARNE));
        assertFalse(CodeInsee.isDepartementSeine(1968, CodeInsee.DEPARTEMENT_VAL_D_OISE));

        assertFalse(CodeInsee.isDepartementSeine(1969, CodeInsee.DEPARTEMENT_PARIS));
        assertFalse(CodeInsee.isDepartementSeine(1969, CodeInsee.DEPARTEMENT_YVELINES));
        assertFalse(CodeInsee.isDepartementSeine(1969, CodeInsee.DEPARTEMENT_ESSONE));
        assertFalse(CodeInsee.isDepartementSeine(1969, CodeInsee.DEPARTEMENT_HAUTS_DE_SEINE));
        assertFalse(CodeInsee.isDepartementSeine(1969, CodeInsee.DEPARTEMENT_SEINE_ST_DENIS));
        assertFalse(CodeInsee.isDepartementSeine(1969, CodeInsee.DEPARTEMENT_VAL_DE_MARNE));
        assertFalse(CodeInsee.isDepartementSeine(1969, CodeInsee.DEPARTEMENT_VAL_D_OISE));
    }

    @Test
    public void isDepartementCorseAvantDivision() {
        assertTrue(CodeInsee.isDepartementCorseAvantDivision(1975, CodeInsee.DEPARTEMENT_CORSE));
        assertFalse(CodeInsee.isDepartementCorseAvantDivision(1975, CodeInsee.DEPARTEMENT_HAUTE_CORSE));
        assertFalse(CodeInsee.isDepartementCorseAvantDivision(1975, CodeInsee.DEPARTEMENT_CORSE_DU_SUD));

        assertFalse(CodeInsee.isDepartementCorseAvantDivision(1976, CodeInsee.DEPARTEMENT_CORSE));
        assertFalse(CodeInsee.isDepartementCorseAvantDivision(1976, CodeInsee.DEPARTEMENT_HAUTE_CORSE));
        assertFalse(CodeInsee.isDepartementCorseAvantDivision(1976, CodeInsee.DEPARTEMENT_CORSE_DU_SUD));
    }

    @Test
    public void isDepartementCorseApresDivision() {
        assertFalse(CodeInsee.isDepartementCorseApresDivision(1975, CodeInsee.DEPARTEMENT_CORSE));
        assertFalse(CodeInsee.isDepartementCorseApresDivision(1975, CodeInsee.DEPARTEMENT_HAUTE_CORSE));
        assertFalse(CodeInsee.isDepartementCorseApresDivision(1975, CodeInsee.DEPARTEMENT_CORSE_DU_SUD));

        assertFalse(CodeInsee.isDepartementCorseApresDivision(1976, CodeInsee.DEPARTEMENT_CORSE));
        assertTrue(CodeInsee.isDepartementCorseApresDivision(1976, CodeInsee.DEPARTEMENT_HAUTE_CORSE));
        assertTrue(CodeInsee.isDepartementCorseApresDivision(1976, CodeInsee.DEPARTEMENT_CORSE_DU_SUD));
    }
    
    @Test
    public void calculeClefinsee() {
        
        assertEquals(11,CodeInsee.calculeClefinsee("1234567890123"));

        assertEquals(25,CodeInsee.calculeClefinsee("1750320012345"));
        assertEquals(52,CodeInsee.calculeClefinsee("175032A012345"));
        assertEquals(79,CodeInsee.calculeClefinsee("175032B012345"));
        assertEquals(52,CodeInsee.calculeClefinsee("175032a012345"));
        
    }
}
