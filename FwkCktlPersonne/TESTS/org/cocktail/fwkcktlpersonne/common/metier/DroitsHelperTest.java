/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.*;

import org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique;
import org.cocktail.fwkcktlpersonne.common.metier.EORegle;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur;
import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * 
 * Tests des fonctions basiques des droits.
 * Ces tests crée pour leur besoin deux profils, deux périmètres de donnée.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class DroitsHelperTest {
    
    private static EOEditingContext EditingContext = ERXEC.newEditingContext();
    private static EOGdProfil ProfilDirecteurServiceTest;
    private static EOGdProfil ProfilDirecteurDeService;
    private static EOGdDonnee Donnee;
    private static EOGdDonnee Donnee2;
    private static EOGdFonction Fonction;
    private static EOGdFonction Fonction2;
    private static NSMutableArray<EOEnterpriseObject> EosToDelete = new NSMutableArray<EOEnterpriseObject>();
    private static AutorisationsCache ApplicationAutorisationCache;

    @Rule
    public MockEditingContext contexte = new MockEditingContext("FwkCktlPersonne");

    public void init() {
        // On crée un profil "Directeur du service test"
        creerProfilDirecteurDeServiceTest();
        // On crée un profil "Directeur de service", correspondant aux individus avec rôle "Directeur de departement"
        creerProfilDirecteurDeService();
        EditingContext.saveChanges();
        // On crée le cache des autorisations pour COCONUT et le persId 
        ApplicationAutorisationCache = new AutorisationsCache("COCONUT", AllTests.PersIdIndividuTest);
    }
    
    
    public void testProfilsForPersonne() {

        EOGdProfil profil = mock(EOGdProfil.class);
        contexte.insertObject(profil);
        
        EOIndividu individu = mock(EOIndividu.class);
        contexte.insertObject(individu);
        //when(contexte.objectsWithFetchSpecification(eq(specification))).thenReturn(EosToDelete);
        //when(individu.persId()).thenReturn(1);EOFetchSpecification
        
        /**indiv.setNomAffichage("Indiv Test 1");
        indiv.setPrenomAffichage("Indiv Test 1");
        indiv.setPersIdCreation(grhumCreateur.persId());
        indiv.setPersIdModification(grhumCreateur.persId());
        indiv.setNoIndividuCreateur(grhumCreateur.noIndividu());
        EOCivilite civ = 
            EOCivilite.fetchFirstByQualifier(EditingContext, ERXQ.equals(EOCivilite.C_CIVILITE_KEY, EOCivilite.C_CIVILITE_MONSIEUR));
        indiv.setToCiviliteRelationship(civ);**/

        
        Integer utilisateurPersId = 1;
        
        NSArray<EOGdProfil> profils = DroitsHelper.profilsForPersonne(contexte, utilisateurPersId);
        // L'utilisateur de test doit avoir les 3 profils
        assertTrue(profils.containsObject(ProfilDirecteurDeService));
        assertTrue(profils.containsObject(ProfilDirecteurServiceTest));
    }
    
    public void testDroitsDonneeForPersonne() {
        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        NSArray<EOGdProfilDroitDonnee> droitsDonnee = 
            DroitsHelper.droitsDonneeForPersonne(EditingContext, "COCONUT", utilisateurPersId);
        assertTrue(!droitsDonnee.isEmpty());
    }
    
    public void testHasDroitLectureOnDonnee() {
        boolean hasDroit = DroitsHelper.personneHasDroitLectureOnDonnee(EditingContext, AllTests.PersIdIndividuTest, Donnee);
        assertTrue(hasDroit);
    }
    
    public void testHasDroitLectureOnDonneeFromCache() {
        boolean hasDroit = ApplicationAutorisationCache.hasDroitLectureOnDonnee(Donnee.donIdInterne());
        assertTrue(hasDroit);
    }
    
    public void testHasDroitUtilisationOnFonction() {
        boolean hasDroit = DroitsHelper.personneHasDroitUtilisationOnFonction(EditingContext, AllTests.PersIdIndividuTest, Fonction);
        assertTrue(hasDroit);
    }
    
    public void testHasDroitUtilisationOnFonctionFromCache() {
        boolean hasDroit = ApplicationAutorisationCache.hasDroitUtilisationOnFonction(Fonction.fonIdInterne());
        assertTrue(hasDroit);
    }
    
    public void testInterdictionOnDonnee() {
        boolean hasNoDroit = DroitsHelper.personneHasNoDroitOnDonnee(EditingContext, AllTests.PersIdIndividuTest, Donnee2);
        assertTrue(hasNoDroit);
    }
    
    public void testInterdictionOnFonction() {
        boolean hasNoDroit = DroitsHelper.personneHasNoDroitOnFonction(EditingContext, AllTests.PersIdIndividuTest, Fonction2);
        assertTrue(hasNoDroit);
    }
    
    public void testPersonnesWithDroitOnDonnee() {
        NSArray<Integer> persIds = DroitsHelper.personnesWithDroitOnDonnee(EditingContext, EOGdTypeDroitDonnee.STR_ID_R, Donnee);
        assertTrue(!persIds.isEmpty());
    }
    
    public void testPersonnesWithDroitOnFonction() {
        long begin = System.currentTimeMillis();
        NSArray<Integer> persIds = DroitsHelper.personnesWithDroitOnFonction(EditingContext, EOGdTypeDroitFonction.STR_ID_U, Fonction);
        long end = System.currentTimeMillis() - begin;
        System.out.println("*** TEMPS : *** " + Long.toString(end));
        assertTrue(!persIds.isEmpty());
    }
    
    public void testPersonnesWithProfil() {
        NSArray<Integer> persIds = DroitsHelper.personnesWithProfil(EditingContext, ProfilDirecteurDeService);
        assertTrue(!persIds.isEmpty());
    }
    
    @AfterClass
    public static void tearDown() {
        if (!EosToDelete.isEmpty()) {
            for (EOEnterpriseObject eo : EosToDelete) {
                EditingContext.deleteObject(eo);
            }
            EditingContext.saveChanges();
        }
    }
    
    private void creerProfilDirecteurDeServiceTest() {

        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        Integer persIdCreateur = AllTests.PersIdGrhumCreateur;
        String cStructure = AllTests.CStructureTest;
        String assId = AllTests.AssIdDirecteur.toString();
        // Création d'un groupe dynamique
        EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(EditingContext);
        groupeDynamique.setGrpdLc("Groupe dynamique pour profil 'Directeur du service test'");
        groupeDynamique.setGrpdDescription("Groupe dynamique pour profil 'Directeur du service test'");
        // Création d'une règle simple
        EORegle regleRacine = EORegle.creerRegleSimple(EditingContext, utilisateurPersId);
        regleRacine.setRValue(assId);
        regleRacine.setRValue2(cStructure);
        regleRacine.setToRegleOperateurRelationship(EORegleOperateur.getRegleOperateurRoleDansGroupe());
        groupeDynamique.setToRegleRelationship(regleRacine);
        // Création du profil
        ProfilDirecteurServiceTest = EOGdProfil.creerInstance(EditingContext);
        ProfilDirecteurServiceTest.setToGroupeDynamiqueRelationship(groupeDynamique);
        ProfilDirecteurServiceTest.setPrLc("Directeur du service test");
        ProfilDirecteurServiceTest.setPersIdCreation(persIdCreateur);
        ProfilDirecteurServiceTest.setPersIdModification(persIdCreateur);
        // On crée le périmètre de données "Documents de toutes les conventions"
        Donnee = EOGdDonnee.creerInstance(EditingContext);
        Donnee.setDonCategorie("Documents");
        Donnee.setDonDescription("Les documents de toutes les conventions");
        Donnee.setDonIdInterne("DOCCONV");
        Donnee.setDonLc("Documents des conventions");
        EOGdApplication app = EOGdApplication.fetchFirstByQualifier(EditingContext, ERXQ.equals(EOGdApplication.APP_STR_ID_KEY, "COCONUT"));
        Donnee.setToGdApplicationRelationship(app);
        ProfilDirecteurServiceTest.addDroitOnDonnee(Donnee, EOGdTypeDroitDonnee.getTypeDroitLecture(), persIdCreateur);
        // On crée le périmètre de données "Documents des conventions de mon service"
        Donnee2 = EOGdDonnee.creerInstance(EditingContext);
        Donnee2.setDonCategorie("Documents");
        Donnee2.setDonDescription("Les documents des conventions de mon service");
        Donnee2.setDonIdInterne("DOCCONVSERVICE");
        Donnee2.setDonLc("Documents des conventions de mon service");
        Donnee2.setToGdApplicationRelationship(app);
        ProfilDirecteurServiceTest.addDroitOnDonnee(Donnee2, EOGdTypeDroitDonnee.getTypeDroitLecture(), persIdCreateur);
        // On récupère la fonction TRAVAL pour donner le droit d'utilisation dessus
        Fonction = EOGdFonction.fetchFirstByQualifier(EditingContext, ERXQ.equals(EOGdFonction.FON_ID_INTERNE_KEY, "TRAVAL"));
        ProfilDirecteurServiceTest.addDroitOnFonction(Fonction, EOGdTypeDroitFonction.getTypeDroitUtilisation(), persIdCreateur);
        // On récupère la fonction TRAM pour donner le droit d'utilisation dessus
        Fonction2 = EOGdFonction.fetchFirstByQualifier(EditingContext, ERXQ.equals(EOGdFonction.FON_ID_INTERNE_KEY, "TRAM"));
        ProfilDirecteurServiceTest.addDroitOnFonction(Fonction2, EOGdTypeDroitFonction.getTypeDroitUtilisation(), persIdCreateur);
        // On marque à supprimer les objets crées
        EosToDelete.addObjects(ProfilDirecteurServiceTest, regleRacine, groupeDynamique, Donnee, Donnee2);
    }
    
    private void creerProfilDirecteurDeService() {
        
        Integer utilisateurPersId = AllTests.PersIdIndividuTest;
        Integer persIdCreateur = AllTests.PersIdGrhumCreateur;
        String assId = AllTests.AssIdDirecteur.toString();
        // Création d'un groupe dynamique
        EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(EditingContext);
        groupeDynamique.setGrpdLc("Groupe dynamique pour profil 'Directeur de service'");
        groupeDynamique.setGrpdDescription("Groupe dynamique pour profil 'Directeur de service'");
        // Création d'une règle simple
        EORegle regleRacine = EORegle.creerRegleSimple(EditingContext, utilisateurPersId);
        regleRacine.setRValue(assId);
        regleRacine.setToRegleOperateurRelationship(EORegleOperateur.getRegleOperateurRole());
        groupeDynamique.setToRegleRelationship(regleRacine);
        // Création du profil
        ProfilDirecteurDeService = EOGdProfil.creerInstance(EditingContext);
        ProfilDirecteurDeService.setToGroupeDynamiqueRelationship(groupeDynamique);
        ProfilDirecteurDeService.setPrLc("Directeur de service");
        ProfilDirecteurDeService.setPersIdCreation(persIdCreateur);
        ProfilDirecteurDeService.setPersIdModification(persIdCreateur);
        // On rajoute un droit d'interdiction sur le périmètre 
        ProfilDirecteurDeService.addDroitOnDonnee(Donnee2, EOGdTypeDroitDonnee.getTypeDroitInterdiction(), persIdCreateur);
        EosToDelete.addObjects(ProfilDirecteurDeService, regleRacine, groupeDynamique);
        // On interdit sur Fonction2...
        ProfilDirecteurDeService.addDroitOnFonction(Fonction2, EOGdTypeDroitFonction.getTypeDroitInterdiction(), persIdCreateur);
    }
}
