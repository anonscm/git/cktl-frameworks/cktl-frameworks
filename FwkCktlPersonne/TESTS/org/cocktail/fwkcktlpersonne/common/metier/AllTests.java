/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.ERXExtensions;
import er.extensions.appserver.ERXApplication;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * 
 * Suite de tests sur les droits.
 * Des données de test sont insérées avant l'exécution de tous les tests et nettoyées après l'exécution des tests.
 * On insère un individu et une structure, on met l'individu dans la structure avec le rôle directeur de département.
 * 
 * Attention vous devez lancer obligatoirement AllTests plutôt que chaque test individuellement, pour bénéficier
 * des données de test.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class AllTests {

    private static EOEditingContext EditingContext;
    private static NSMutableArray<Integer> PersIdsToDelete = new NSMutableArray<Integer>();
    public static Integer PersIdIndividuTest;
    public static String CStructureTest;
    public static Integer AssIdDirecteur;
    public static Integer PersIdGrhumCreateur;
    
    @BeforeClass
    public static void init() throws URISyntaxException {
        String[] args = new String[1];
        args[0] = "-Der.extensions.ERXProperties.OptionalConfigurationFiles=(Properties.test)";
        ERXApplication.setup(args);
        URL url = NSBundle.bundleForClass(FwkCktlPersonne.class).bundlePathURL();
        File bundlePersonne = new File(url.toURI());
        ERXExtensions.initEOF(bundlePersonne, args, true, false, true);
        // insertion des données de test
        insertDonneesTest();
    }
    
    private static EOIndividu grhumCreateur(EOEditingContext ec) {
        String loginGrhumCreateur = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CREATEUR);
        EOCompte compteGrhumCreateur = EOCompte.fetchFirstByQualifier(ec, ERXQ.equals(EOCompte.CPT_LOGIN_KEY, loginGrhumCreateur));
        return EOIndividu.fetchFirstByQualifier(ec, ERXQ.equals(EOIndividu.PERS_ID_KEY, compteGrhumCreateur.persId()));
    }
    
    public static void insertDonneesTest() {
        // On insère un individu "Indiv test 1" dans une autre stack EOF
        EditingContext = ERXEC.newEditingContext(new EOObjectStoreCoordinator());
        // Persid grhum createur pour creer des indivs, structures...
        EOIndividu grhumCreateur = grhumCreateur(EditingContext);
        PersIdGrhumCreateur = grhumCreateur.persId();
        EOIndividu indiv = EOIndividu.creerInstance(EditingContext);
        indiv.setNomAffichage("Indiv Test 1");
        indiv.setPrenomAffichage("Indiv Test 1");
        indiv.setPersIdCreation(grhumCreateur.persId());
        indiv.setPersIdModification(grhumCreateur.persId());
        indiv.setNoIndividuCreateur(grhumCreateur.noIndividu());
        EOCivilite civ = 
            EOCivilite.fetchFirstByQualifier(EditingContext, ERXQ.equals(EOCivilite.C_CIVILITE_KEY, EOCivilite.C_CIVILITE_MONSIEUR));
        indiv.setToCiviliteRelationship(civ);
        // On insère une structure "Structure test 1"
        EOStructure structure = EOStructure.creerInstance(EditingContext);
        structure.setStrAffichage("Structure de Test 1");
        structure.setPersIdCreation(grhumCreateur.persId());
        structure.setPersIdModification(grhumCreateur.persId());
        // On associe l'individu dans la structure avec le role "CONTACT"
        EORepartStructure repart = EORepartStructure.creerInstance(EditingContext);
        repart.setToPersonneElt(indiv);
        repart.setToStructureGroupeRelationship(structure);
        repart.setPersIdCreation(grhumCreateur.persId());
        repart.setPersIdModification(grhumCreateur.persId());
        EORepartAssociation repartAsso = EORepartAssociation.creerInstance(EditingContext);
        EOAssociation association = EOAssociation.fetchFirstByQualifier(EditingContext, ERXQ.equals(EOAssociation.ASS_CODE_KEY, "DIRDEPARTEME"));
        repartAsso.setToAssociationRelationship(association);
        repartAsso.setToPersonne(indiv);
        repartAsso.setToStructureRelationship(structure);
        repartAsso.setPersIdCreation(grhumCreateur.persId());
        repartAsso.setPersIdModification(grhumCreateur.persId());
        EditingContext.saveChanges();
        PersIdIndividuTest = indiv.persId();
        CStructureTest = structure.cStructure();
        AssIdDirecteur = (Integer)ERXEOControlUtilities.primaryKeyObjectForObject(association);
        PersIdsToDelete.addObjects(PersIdIndividuTest, structure.persId());
    }
    
    private static void desactivateTriggers() {
        String sql = "ALTER TRIGGER GRHUM.LOG_DELETE_PERSONNE DISABLE";
        String sql2 = "ALTER TRIGGER GRHUM.LOG_DELETE_INDIVIDU_ULR DISABLE";
        EOUtilities.rawRowsForSQL(EditingContext, "FwkCktlPersonne", sql, null);
        EOUtilities.rawRowsForSQL(EditingContext, "FwkCktlPersonne", sql2, null);
    }
    
    private static void reactivateTriggers() {
        String sql = "ALTER TRIGGER GRHUM.LOG_DELETE_PERSONNE ENABLE";
        String sql2 = "ALTER TRIGGER GRHUM.LOG_DELETE_INDIVIDU_ULR ENABLE";
        EOUtilities.rawRowsForSQL(EditingContext, "FwkCktlPersonne", sql, null);
        EOUtilities.rawRowsForSQL(EditingContext, "FwkCktlPersonne", sql2, null);
    }
    
    public static void deleteDonneesTest() {
        try {
            // désactivation des triggers de suppression des personnes
            desactivateTriggers();
            for (Integer persId : PersIdsToDelete) {
                NSMutableDictionary<String, Integer> procStockParams = new NSMutableDictionary<String, Integer>();
                procStockParams.put("persid", persId);
                EOUtilities.executeStoredProcedureNamed(EditingContext, "FwkPers_prcDetruirePersonne", procStockParams);
            }
            EditingContext.saveChanges();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new IllegalStateException("Une erreur s'est produite lors de la suppression des données de test, " +
            		                        " votre db comporte donc des données de test et doit être nettoyée.", e);
        } finally {
            reactivateTriggers();
        }
    }
    
    @AfterClass
    public static void end() {
        // suppression des données de test
        deleteDonneesTest();
    }
    
}
