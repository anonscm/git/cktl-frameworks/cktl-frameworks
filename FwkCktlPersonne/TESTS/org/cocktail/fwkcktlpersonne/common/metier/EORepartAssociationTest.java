package org.cocktail.fwkcktlpersonne.common.metier;

import static com.wounit.matchers.EOAssert.cannotBeSaved;
import static com.wounit.matchers.EOAssert.cannotBeSavedBecause;
import static com.wounit.matchers.EOAssert.confirm;

import org.cocktail.fwkcktlpersonne.common.metier.providers.CstructureProviderTestImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;
import com.wounit.annotations.UnderTest;
import com.wounit.rules.MockEditingContext;
/**
 * 
 * Classe de test de ReparAssociation
 * 
 * @author jlafourc
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EORepartAssociationTest {

	
    @Rule
    public MockEditingContext editingContext = new MockEditingContext("FwkCktlPersonne");

    @Dummy
    private EOAssociation association;
    
    @Dummy
    private EOAssociation affectation;
    
    @Dummy
    private EORepartAssociation repartAssociationExistant;
    
    @Dummy
    private EORepartStructure repartStructureExistant;
    
    @Dummy
    private EOStructure groupe;

    @UnderTest
    private EORepartAssociation nouveauRepartAssociation;
	
    
    private NSTimestamp now = new NSTimestamp();

    @BeforeClass
    public static void setupClass() {
    	EOStructure.setCstructureProvider(new CstructureProviderTestImpl());
    }
    
	@Before
	public void setup() {
    	EOAssociation.Liste.setEditingContext(editingContext);
		affectation.setAssCode(EOAssociation.Liste.AFFECT.code());    	
		association.setAssCode(EOAssociation.Liste.FONCTION.code());
		
		groupe.setPersId(1);
		
		repartStructureExistant.setToStructureGroupeRelationship(groupe);
		repartStructureExistant.setPersId(1);		

		groupe.addToToRepartStructuresEltsRelationship(repartStructureExistant);

		
		repartAssociationExistant.setToAssociationRelationship(association);
		repartAssociationExistant.setToStructureRelationship(groupe);
		repartAssociationExistant.setPersId(1);		
		repartAssociationExistant.setRasDOuverture(now);
		
		repartStructureExistant.addToToRepartAssociationsRelationship(repartAssociationExistant);
	}
	
	@Test
	public void checkDoublonsTest() {
		nouveauRepartAssociation.setToAssociationRelationship(association);
		nouveauRepartAssociation.setToStructureRelationship(groupe);
		groupe.addToToRepartAssociationsRelationship(nouveauRepartAssociation);
		nouveauRepartAssociation.setToPersonne(groupe);	
		nouveauRepartAssociation.setPersId(1);
		nouveauRepartAssociation.setRasDOuverture(now);
		nouveauRepartAssociation.setDCreation(now);
		nouveauRepartAssociation.setDModification(now);
		nouveauRepartAssociation.setPersIdCreation(1);
		nouveauRepartAssociation.setPersIdModification(1);
		confirm(nouveauRepartAssociation, cannotBeSaved());
	}

	@Test
	public void checkReserveesTest() {
		nouveauRepartAssociation.setToAssociationRelationship(affectation);
		nouveauRepartAssociation.setToStructureRelationship(groupe);
		nouveauRepartAssociation.setToPersonne(groupe);	
		nouveauRepartAssociation.setPersId(1);
		nouveauRepartAssociation.setRasDOuverture(now);
		nouveauRepartAssociation.setDCreation(now);
		nouveauRepartAssociation.setDModification(now);
		nouveauRepartAssociation.setPersIdCreation(1);
		nouveauRepartAssociation.setPersIdModification(1);
		confirm(nouveauRepartAssociation, cannotBeSavedBecause("La modification des affectations est interdite en dehors de l'application Mangue"));

	}
	
}
