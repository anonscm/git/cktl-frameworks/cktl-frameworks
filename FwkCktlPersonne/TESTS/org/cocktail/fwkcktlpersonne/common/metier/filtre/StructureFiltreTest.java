package org.cocktail.fwkcktlpersonne.common.metier.filtre;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.junit.Before;
import org.junit.Test;

public class StructureFiltreTest {

	private List<IStructure> listeStructure = new ArrayList<IStructure>();
	private StructureFiltre structureFiltre = new StructureFiltre();
	
	private String C_STRUCTURE_1 = "cStructure1";
	private String C_STRUCTURE_2 = "cStructure2";
	private String C_STRUCTURE_3 = "cStructure3";
	private String C_STRUCTURE_4 = "cStructure4";
	private String C_STRUCTURE_5 = "cStructure5";
	private String C_STRUCTURE_6 = "cStructure6";
	
	private String NOM_1 = "UFR 1";
	private String NOM_2 = "Labo 1";
	private String NOM_3 = "UFR 2";
	private String NOM_4 = "UFR 3";
	private String NOM_5 = "UFR 4";
	private String NOM_6 = "UFR 5";
	
	
	@Before
	public void initListeStructure() throws ParseException {
		
		listeStructure.add(initStructure(C_STRUCTURE_1, NOM_1, false));
		listeStructure.add(initStructure(C_STRUCTURE_2, NOM_2, false));
		listeStructure.add(initStructure(C_STRUCTURE_3, NOM_3, false));
		listeStructure.add(initStructure(C_STRUCTURE_4, NOM_4, true));
		listeStructure.add(initStructure(C_STRUCTURE_5, NOM_5, true));
		listeStructure.add(initStructure(C_STRUCTURE_6, NOM_6, false));
		
	}
	
	
	@Test
	public void filtrerParNomCompletAffichage() throws ParseException {
		
		assertEquals(5, structureFiltre.filtrerParNomCompletAffichage(listeStructure, "UFR").size());
		assertEquals(1, structureFiltre.filtrerParNomCompletAffichage(listeStructure, "abo").size());
		
	}
	
	@Test
	public void filtreParCodeStructure() throws ParseException {
		
		IStructure structure = structureFiltre.filtreParCodeStructure(listeStructure, C_STRUCTURE_1);
		assertEquals(NOM_1, structure.getNomCompletAffichage());
		
		structure = structureFiltre.filtreParCodeStructure(listeStructure, C_STRUCTURE_5);
		assertEquals(NOM_5, structure.getNomCompletAffichage());
		
	}
	
	@Test
	public void filtrerParIsArchivee() throws ParseException {
		
		assertEquals(4, structureFiltre.filtrerParIsArchivee(listeStructure).size());
		
	}
	
	

	 /**
    * 
    * @param cStructure codeStructure
    * @param nomCompletAffichage nom
    * @param isArchivee boolean
    * @return @IStructure structure
    * @throws ParseException exception
    */
	public IStructure initStructure(String cStructure, String nomCompletAffichage, boolean isArchivee) throws ParseException {
       
		IStructure individu = mock(IStructure.class);
       
		when(individu.cStructure()).thenReturn(cStructure);
		when(individu.getNomCompletAffichage()).thenReturn(nomCompletAffichage);
		when(individu.isArchivee()).thenReturn(isArchivee);
       
		return individu;
   }
	
	
}
