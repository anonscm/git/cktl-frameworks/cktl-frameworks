package org.cocktail.fwkcktlpersonne.common.metier.filtre;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.junit.Before;
import org.junit.Test;

public class GdProfilFiltreTest {

	private List<EOGdProfil> listeProfil = new ArrayList<EOGdProfil>();
	private GdProfilFiltre structureFiltre = new GdProfilFiltre();
	
	@Before
	public void initListeProfil() throws ParseException {
		
		listeProfil.add(initProfil(1, "profil 1"));
		listeProfil.add(initProfil(2, "profil 2"));
		listeProfil.add(initProfil(3, "profil 3"));
		
	}
	
	
	@Test
	public void filtrerParIds() throws ParseException {
		assertEquals(1, structureFiltre.filtrerParIds(listeProfil, Arrays.asList(1, 2)).size());
		assertEquals(2, structureFiltre.filtrerParIds(listeProfil, Arrays.asList(2)).size());
	}

	 /**
    * 
    * @param id primary Key
    * @param prLc libellé court
    * @return @EOGdProfil
    * @throws ParseException 
    */
	public EOGdProfil initProfil(Integer id, String prLc) throws ParseException {
       
		EOGdProfil profil = mock(EOGdProfil.class);
       
		when(profil.primaryKey()).thenReturn(String.valueOf(id));
		when(profil.prLc()).thenReturn(prLc);
		
		return profil;
   }
	
	
}
