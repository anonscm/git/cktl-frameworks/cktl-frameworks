package org.cocktail.fwkcktlpersonne.common.metier.droits;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

import junit.framework.TestCase;

/**
 * @author lilia
 * Classe de base aux test unitaires sur les droits
 */
public class EOGdTestBase extends TestCase {
	protected EOGdDomaine domaine;
	protected EOGdApplication application;
	
	@Rule
	protected MockEditingContext editingContext = new MockEditingContext("FwkCktlPersonne", "FwkCktlDroits");

	@Before
	public void setUp() {
		domaine = EOGdTestsHelper.creerGdDomaine(editingContext, "domaine");
		application = EOGdTestsHelper.creerGdApplication(editingContext, "application", "1", domaine);
	}
	
	@Test
    public void test_dummy() {
		assertTrue(true);
	}
}
