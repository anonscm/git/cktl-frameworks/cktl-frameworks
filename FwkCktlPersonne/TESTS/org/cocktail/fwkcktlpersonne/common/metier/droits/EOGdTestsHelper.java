package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.wounit.rules.MockEditingContext;

/**
 * @author lilia
 *         Classe utilitaires qui fournit des briques pour les test unitaire sur la partie gestion des droits
 */
public class EOGdTestsHelper {

	public static EOGdDomaine creerGdDomaine(MockEditingContext editingContext, String libelle) {
		EOGdDomaine domaine = new EOGdDomaine();
		domaine.setDomLc(libelle);
		editingContext.insertSavedObject(domaine);

		return domaine;
	}

	public static EOGdApplication creerGdApplication(MockEditingContext editingContext, String libelle, String strId, EOGdDomaine domaine) {
		EOGdApplication application = new EOGdApplication();
		application.setAppLc(libelle);
		application.setAppStrId(strId);
		editingContext.insertSavedObject(application);
		application.setToGdDomaineRelationship(domaine);
		editingContext.saveChanges();

		return application;
	}

	public static EOGdPerimetre creerGdPerimetre(MockEditingContext editingContext, String libelle, Integer appId, EOGdApplication application) {

		EOGdPerimetre perimetre = new EOGdPerimetre();
		perimetre.setAppId(appId);
		perimetre.setLibelle(libelle);
		perimetre.setPersIdCreation(1);
		perimetre.setPersIdModification(1);
		editingContext.insertSavedObject(perimetre);
		perimetre.setApplicationRelationship(application);

		return perimetre;
	}

	public static EOGdProfil creerGdProfil(MockEditingContext editingContext, String libelle) {
		EOGdProfil profil = new EOGdProfil();
		profil.setPersIdCreation(1);
		profil.setPersIdModification(1);
		profil.setPrLc(libelle);
		editingContext.insertSavedObject(profil);
		return profil;
	}
}
