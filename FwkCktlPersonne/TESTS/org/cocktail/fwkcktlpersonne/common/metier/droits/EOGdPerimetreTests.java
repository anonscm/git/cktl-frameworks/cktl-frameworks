package org.cocktail.fwkcktlpersonne.common.metier.droits;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

import junit.framework.TestCase;

/**
 * @author lilia
 *         Tests de la classe EOGPerimetre
 */
public class EOGdPerimetreTests extends EOGdTestBase {

	@Test
	public void test_creerPerimetre() {
		// Arrange		
		EOGdPerimetre perimetre = new EOGdPerimetre();
		perimetre.setAppId(1);
		perimetre.setLibelle("perimetre");
		perimetre.setPersIdCreation(1);
		perimetre.setPersIdModification(1);
		editingContext.insertObject(perimetre);
		perimetre.setApplicationRelationship(application);
		// Assert ready to test

		// Act
		editingContext.saveChanges();

		// Assert
		assertEquals(1, EOGdPerimetre.fetchAll(editingContext).count());
	}
}
