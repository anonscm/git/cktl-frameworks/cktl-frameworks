package org.cocktail.fwkcktlpersonne.common.metier.droits;

import org.junit.Test;

/**
 * Tests de la classe qui gere les profils GD
 */
public class EOGdProfilTests extends EOGdTestBase {

	@Test
	public void test_creerProfil() {
		// Arrange
		EOGdProfil profil = createGDProfil("Nouveau profil");

		// Assert ready to test

		// Act

		editingContext.saveChanges();
		// Assert		
		assertEquals(1, EOGdProfil.fetchAll(editingContext).count());
	}

	@Test
	public void test_associerPerimetre() {
		// Arrange
		EOGdProfil profil = createGDProfil("Nouveau profil");
		EOGdPerimetre perimetre = EOGdTestsHelper.creerGdPerimetre(editingContext, "perimetre", 1, application);

		// Assert ready to test
		assertEquals(0, EOGdProfilPerimetre.fetchAll(editingContext).count());

		// Act
		profil.associerPerimetre(perimetre);
		editingContext.saveChanges();

		// Assert		
		assertEquals(1, profil.perimetres().count());

		assertEquals(perimetre, profil.perimetres().get(0));
	}

	@Test
	public void test_allPerimetres() {
		// Arrange
		EOGdProfil profil = createGDProfil("profil");
		EOGdProfil profilPere = createGDProfil("profilPere");
		EOGdProfil profilGrandPere = createGDProfil("profilPere");
		profilGrandPere.addToToGdProfilsEnfantsRelationship(profilPere);
		profilPere.addToToGdProfilsEnfantsRelationship(profil);

		EOGdPerimetre perimetre = EOGdTestsHelper.creerGdPerimetre(editingContext, "perimetre", 1, application);
		EOGdPerimetre perimetrePere = EOGdTestsHelper.creerGdPerimetre(editingContext, "perimetrePere", 1, application);
		EOGdPerimetre perimetreGrandPere = EOGdTestsHelper.creerGdPerimetre(editingContext, "perimetreGrandPere", 1, application);
		EOGdPerimetre perimetreGrandPere1 = EOGdTestsHelper.creerGdPerimetre(editingContext, "perimetreGrandPere", 1, application);

		profil.associerPerimetre(perimetre);
		profilPere.associerPerimetre(perimetrePere);
		profilGrandPere.associerPerimetre(perimetreGrandPere);
		profilGrandPere.associerPerimetre(perimetreGrandPere1);
		editingContext.saveChanges();

		// Assert ready to test

		// Act

		// Assert
		assertEquals(4, profil.allPerimetres(application).count());
	}

	private EOGdProfil createGDProfil(String libelle) {
		EOGdProfil profil = new EOGdProfil();
		profil.setPersIdCreation(1);
		profil.setPersIdModification(1);
		profil.setPrLc(libelle);
		editingContext.insertObject(profil);
		return profil;
	}
}
