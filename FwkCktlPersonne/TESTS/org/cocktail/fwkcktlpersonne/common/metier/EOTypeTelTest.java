package org.cocktail.fwkcktlpersonne.common.metier;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class EOTypeTelTest {

	@Rule
	public MockEditingContext editingContext = new MockEditingContext("FwkCktlPersonne");

	@Test
	public void testEstTypeInterne() {

		EOTypeTel typeTel = editingContext.createSavedObject(EOTypeTel.class);
		
		typeTel.setCTypeTel(EOTypeTel.C_TYPE_TEL_INT);

		assertTrue(typeTel.isTypeInterne());
	}

	@Test
	public void testEstPasTypeInterne() {

		EOTypeTel typeTel = editingContext.createSavedObject(EOTypeTel.class);
		
		typeTel.setCTypeTel(EOTypeTel.C_TYPE_TEL_PRF);

		assertFalse(typeTel.isTypeInterne());
	}
	
	

}
