package org.cocktail.fwkcktlpersonne.common.metier.providers;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Implémentation pour tests JUnit
 * @author jlafourc
 *
 */
public class CstructureProviderTestImpl implements CstructureProvider {

	private static Integer nextValue = 0;
	
	
	/**
	 * {@inheritDoc}
	 */
	public String construireCStructure(EOEditingContext editingContext) {
		nextValue = nextValue + 1;
		return nextValue.toString();
	}

}
