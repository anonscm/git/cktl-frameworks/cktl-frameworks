/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IDepartement;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;

import com.webobjects.foundation.NSTimestamp;

public class Helper {

    /**
     * 
     * @param civilite
     * @param insee
     * @param dtNaissance
     * @param codePays
     * @param codeDepartement
     * @return
     * @throws ParseException
     */
    public static IIndividu initIndividu(String codeCivilite, String insee, String dtNaissance, String codePays, String codeDepartement) throws ParseException {

        ICivilite civilite = mock(ICivilite.class);
        when(civilite.getCode()).thenReturn(codeCivilite);
        
        IPays pays = mock(IPays.class);
        when(pays.getCode()).thenReturn(codePays);
        
        IDepartement departement = mock(IDepartement.class);
        when(departement.getCode()).thenReturn(codeDepartement);
        if(codeDepartement == null)
            when(departement.getDepartementFrance()).thenReturn(codeDepartement);
        else if(codeDepartement.startsWith("0")) {
            when(departement.getDepartementFrance()).thenReturn(codeDepartement.substring(1));
        } else {
            when(departement.getDepartementFrance()).thenReturn(codeDepartement);
        }
        
        IIndividu individu = mock(IIndividu.class);
        if(codeCivilite != null) {
            when(individu.getCivilite()).thenReturn(civilite);
            when(individu.estHomme()).thenReturn(codeCivilite.equals(ICivilite.C_CIVILITE_MONSIEUR));
        } else {
            when(individu.getCivilite()).thenReturn(null);
            when(individu.estHomme()).thenReturn(false);
        }
        if(MyStringCtrl.isEmpty(insee)) {
            when(individu.getCodeInsee()).thenReturn(null);
        } else {
            when(individu.getCodeInsee()).thenReturn(new CodeInsee(insee));
        }
        if(dtNaissance != null) {
            when(individu.getDtNaissance()).thenReturn(new NSTimestamp(EOIndividuTest.SDF.parse(dtNaissance)));
        } else {
            when(individu.getDtNaissance()).thenReturn(null);
        }
        if(codePays != null) {
            when(individu.getPaysNaissance()).thenReturn(pays);
        } else {
            when(individu.getPaysNaissance()).thenReturn(null);
        }
        if(codeDepartement != null) {
            when(individu.getDepartement()).thenReturn(departement);
        } else {
            when(individu.getDepartement()).thenReturn(null);
        }
        return individu;
    }

    /**
     * 
     * @param insee
     * @param dtNaissance
     * @param codePays
     * @param codeDepartement
     * @return
     * @throws ParseException
     */
    public static IIndividu initHomme(String insee, String dtNaissance, String codePays, String codeDepartement) throws ParseException {
    
        return initIndividu(ICivilite.C_CIVILITE_MONSIEUR, insee, dtNaissance, codePays, codeDepartement);
    }

    public static IIndividu initFemme(String insee, String dtNaissance, String codePays, String codeDepartement) throws ParseException {
        
        return initIndividu(ICivilite.C_CIVILITE_MADAME, insee, dtNaissance, codePays, codeDepartement);
    }
}
