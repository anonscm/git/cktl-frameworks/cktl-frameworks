/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.CStructureValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TypageException;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;

public class CStructureValidateurTest {
	@Test
	public void testValider() {
        EOStructure structure = mock(EOStructure.class);
        NSArray<EOStructure> structures = new NSArray<EOStructure>(structure);
	    
	    CStructureValidateur validateur = getStubStructureValidateur(structures);
	    
		try{
		    validateur.valider("Structure existante");
		}
		catch (TypageException e) {
			fail("La validation ne devrait pas lever d'exception pour un élément valide");
		}

	}

	@Test(expected = TypageException.class)
	public void testNonValiderVide() {
	    CStructureValidateur validateur = new CStructureValidateur();
	    validateur.valider("");
	}

	@Test(expected = TypageException.class)
	public void testNonValiderBlanc() {
        NSArray<EOStructure> structures = new NSArray<EOStructure>();
        CStructureValidateur validateur = getStubStructureValidateur(structures);
        validateur.valider(" ");
	}

	@Test(expected = TypageException.class)
	public void testNonValiderNull() {
        CStructureValidateur validateur = new CStructureValidateur();
        validateur.valider(null);
	}

	@Test(expected = TypageException.class)
	public void structureInexistante() {
        NSArray<EOStructure> structures = new NSArray<EOStructure>();
        CStructureValidateur validateur = getStubStructureValidateur(structures);
        validateur.valider("Structure Inexistante");
	}

    /**
     * Instancie une sous-classe anonyme de PersIdValidateur avec un fake EOContext.
     * 
     * @param comptes
     * @return
     */
    private CStructureValidateur getStubStructureValidateur(NSArray<EOStructure> structures) {
        final EOEditingContext context = mock(EOEditingContext.class);
        when(context.objectsWithFetchSpecification((EOFetchSpecification) anyObject(), eq(context))).thenReturn(structures);
        CStructureValidateur validateur = new CStructureValidateur() {
            
            @Override
            public EOEditingContext editingContext() {
                return context;
            }
            
        };
        return validateur;
    }

}
