/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import static org.junit.Assert.fail;

import org.cocktail.fwkcktlpersonne.common.metier.validateurs.HtmlValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TypageException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HtmlValidateurTest {
	private HtmlValidateur baliseHTML = null;

	@Before
	public void setUp() throws Exception {
		baliseHTML = new HtmlValidateur();	
	}

	@After
	public void tearDown() throws Exception {
		baliseHTML = null;
	}

	@Test
	public void testValider() {
		try{
			baliseHTML.valider("<link rel=\"stylesheet\" href=\"http://host.univ.fr/Templates/assa.css\" type=\"text/css\">");
			baliseHTML.valider("<link REL=\"icon\" HREF=\"http://host.univ.fr/image_partagee/logo_univ_16.png\" TYPE=\"image/png\">");
			baliseHTML.valider("<link REL=\"icon\" HREF=\"http://host.univ.fr/image_partagee/logo_univ_16.png\" TYPE=\"image/png\"><link REL=\"SHORTCUT ICON\" HREF=\"http://host.univ.fr/image_partagee/ulr.ico\">");
		} catch (TypageException e) {
			fail("La validation ne devrait pas lever d'exception pour un élément valide");
		}
	}
	
	@Test(expected=TypageException.class)
	public void testNonValiderFauteEcriture() {
		baliseHTML.valider("link rel=\"stylesheet\" href=\"http://host.univ.fr/Templates/assa.css\" type=\"text/css\"");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderVide() {
		baliseHTML.valider("");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderBlanc() {
		baliseHTML.valider(" ");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderNull() {
		baliseHTML.valider(null);	
	}

}
