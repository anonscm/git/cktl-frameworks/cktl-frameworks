/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.net.URL;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.PersIdValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TypageException;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;

public class PersIdValidateurTest {

	@Test
	public void unIndividuValide() {
        EOIndividu individu = mock(EOIndividu.class);
        NSArray<EOIndividu> individus = new NSArray<EOIndividu>(individu);
        
        PersIdValidateur validateur = getStubPersIdValidateur(individus);
        try{
            validateur.valider("1");
		} catch (TypageException e) {
			fail("La validation ne devrait pas lever d'exception pour un élément valide");
		}
	}

    @Test
    public void uneStructureValide() {
        EOStructure structure = mock(EOStructure.class);
        NSArray<EOStructure> structures = new NSArray<EOStructure>(structure);
        
        PersIdValidateur validateur = getStubPersIdValidateurStructure(structures);
        try{
            validateur.valider("1");
        } catch (TypageException e) {
            fail("La validation ne devrait pas lever d'exception pour un élément valide");
        }
    }

    @Test(expected = TypageException.class)
	public void testNonValiderVide() {
        PersIdValidateur validateur = new PersIdValidateur();
        validateur.valider("");
	}

	@Test(expected = TypageException.class)
	public void testNonValiderBlanc() {
        PersIdValidateur validateur = new PersIdValidateur();
	    validateur.valider(" ");
	}

	@Test(expected = TypageException.class)
	public void testNonValiderNull() {
        PersIdValidateur validateur = new PersIdValidateur();
	    validateur.valider(null);
	}

	@Test(expected = TypageException.class)
	public void nistructureNiIndividu() {
        NSArray<EOIndividu> individus = new NSArray<EOIndividu>();
        
        PersIdValidateur validateur = getStubPersIdValidateur(individus);
	    validateur.valider("999");
	}

	@Test(expected = TypageException.class)
	public void testNonValiderLibelleFaux() {
        PersIdValidateur validateur = new PersIdValidateur();
		validateur.valider("c_structure du groupe \"Referentiel Applicatifs\"");
	}

	
    /**
     * Instancie une sous-classe anonyme de PersIdValidateur avec un fake EOContext.
     * 
     * @param comptes
     * @return
     */
    private PersIdValidateur getStubPersIdValidateur(NSArray<EOIndividu> individus) {
        final EOEditingContext context = mock(EOEditingContext.class);
        when(context.objectsWithFetchSpecification((EOFetchSpecification) anyObject(), eq(context))).thenReturn(individus);
        PersIdValidateur validateur = new PersIdValidateur() {

           @Override
           protected boolean isStructureKey(String valeurParam, EOEditingContext ec) {
               return false;
           }
            @Override
            public EOEditingContext editingContext() {
                return context;
            }
            
        };
        return validateur;
    }

    /**
     * Instancie une sous-classe anonyme de PersIdValidateur avec un fake EOContext.
     * 
     * @param comptes
     * @return
     */
    private PersIdValidateur getStubPersIdValidateurStructure(NSArray<EOStructure> structures) {
        final EOEditingContext context = mock(EOEditingContext.class);
        when(context.objectsWithFetchSpecification((EOFetchSpecification) anyObject(), eq(context))).thenReturn(structures);
        PersIdValidateur validateur = new PersIdValidateur() {
            
            @Override
            protected boolean isIndividuPersId(String valeurParam, EOEditingContext ec) {
                return false;
            }
            
            @Override
            public EOEditingContext editingContext() {
                return context;
            }
            
        };
        return validateur;
    }

}
