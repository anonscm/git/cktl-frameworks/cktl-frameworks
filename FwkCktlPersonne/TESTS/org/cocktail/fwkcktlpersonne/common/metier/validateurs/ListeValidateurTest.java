/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import static org.junit.Assert.fail;

import org.cocktail.fwkcktlpersonne.common.metier.validateurs.ListeValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TypageException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListeValidateurTest {
	private ListeValidateur listeValidateur = null;

	@Before
	public void setUp() throws Exception {
		listeValidateur = new ListeValidateur();
	}

	@After
	public void tearDown() throws Exception {
		listeValidateur = null;
	}

	@Test
	public void testValider() {
		try{
			listeValidateur.valider("X");
			listeValidateur.valider("P");
			listeValidateur.valider("R");
			listeValidateur.valider("P,R,E");
			listeValidateur.valider("R,E");
		} catch (TypageException e) {
			fail("La validation ne devrait pas lever d'exception pour un élément valide");
		}
	}
	
	@Test(expected=TypageException.class)
	public void testNonValiderVide() {
		listeValidateur.valider("");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderMiniscule() {
		listeValidateur.valider("x");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderBlanc() {
		listeValidateur.valider(" ");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderNull() {
		listeValidateur.valider(null);	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderValeurNumerique() {
		listeValidateur.valider("2010");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderOnlyExtension() {
		listeValidateur.valider(".com");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderOnlyExtensionTropLongue() {
		listeValidateur.valider("cocktail.gouv");	
	}
	@Test(expected=TypageException.class)
	public void testNonValiderListe() {
		listeValidateur.valider("a,b,c,d");	
	}

}
