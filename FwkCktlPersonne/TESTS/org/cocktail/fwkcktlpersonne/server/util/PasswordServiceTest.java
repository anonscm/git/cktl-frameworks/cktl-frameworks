package org.cocktail.fwkcktlpersonne.server.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.wounit.rules.MockEditingContext;
public class PasswordServiceTest {

	private PasswordService service;
	private final static String AUTHORIZED_CHARS_DEFAULT = "0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	
	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlPersonne");
	
	@Before
	public void setUp() {
		service = new PasswordService(ec) {

			@Override
			public String getPasswordAuthorizedChars(EOEditingContext ec) throws Exception {
				return AUTHORIZED_CHARS_DEFAULT;
			}

		};
	}
	
	@Test
	public void unMauvaisMotDePasseNestPasValide() {
		
		assertFalse("Un mauvais MdP ne doit pas être valide !", service.checkPasswordAuthorizedChar(ec, "&"));
		
		assertFalse("Un mauvais MdP même contenant des caractères autorisés ne doit pas être valide !", service.checkPasswordAuthorizedChar(ec, "a&"));
	}
	
	@Test
	public void unBonMotDePasseEstValide() {
		
		assertTrue("Un bon MdP passe à tous les coups !", service.checkPasswordAuthorizedChar(ec, "BabarALEcole"));
		
		assertTrue("Un bon MdP avec des caractères autorisés est accepté !", service.checkPasswordAuthorizedChar(ec, "12345@"));
	}
	
//	@Test
//	public void unMotDePasseDansHistorique() {
//		EOPasswordHistory pwdHistory = mock(EOPasswordHistory.class);
//		when
//	}
	
		
	
}
