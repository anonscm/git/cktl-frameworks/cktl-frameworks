package org.cocktail.fwkcktlpersonne.server.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;



/**
 * Classe de Tests de la génération des MdP
 * @author alainmalaplate
 *
 */
public class GenerationMotDePasseTest {

	
	
	private String limitedList = "$%&*+-123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
	private String fullList = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
	
	
	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlPersonne");
	
	
	@Dummy
	private EOVlans vlanX;
	
	

	public String getLimitedList() {
		return limitedList;
	}

	public void setLimitedList(String limitedList) {
		this.limitedList = limitedList;
	}

	public String getFullList() {
		return fullList;
	}

	public void setFullList(String fullList) {
		this.fullList = fullList;
	}

	public EOVlans getVlanX() {
		return vlanX;
	}

	public void setVlanX(EOVlans vlanX) {
		this.vlanX = vlanX;
	}

	/**
	 * Method to test if a password do not include a forbidden character
	 * @param passwordClair  not crypted password to test
	 * @param listChars is the pool of authorized characters 
	 * @return false if an used character is in the list of forbidden characters
	 */
	public boolean checkPasswordAuthorizedChar(String passwordClair, String listChars)  {
		String authChars = listChars;

		for (char c : passwordClair.toCharArray()) {
			if (authChars.indexOf(c) == -1) {
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * Pour le lancement des tests, on pseudo-initialise les Vlans
	 */
	@Before
	public void setVlan() {
		
		vlanX.setCVlan("X");
		
	}
	
	@Test
	public void reproductionFausseValidation() throws Exception {
		
		PasswordService pwdService = mock(PasswordService.class);
		when(pwdService.getRandomPasswordWithPrivateRules(ec, getVlanX().cVlan())).thenReturn("Xo0azer1");
		
		String motDePasse;
		motDePasse = pwdService.getRandomPasswordWithPrivateRules(ec, getVlanX().cVlan());
//		System.out.print(motDePasse);
		assertFalse(checkPasswordAuthorizedChar(motDePasse, getLimitedList()));
		assertTrue(checkPasswordAuthorizedChar(motDePasse, getFullList()));
	}
	

}
