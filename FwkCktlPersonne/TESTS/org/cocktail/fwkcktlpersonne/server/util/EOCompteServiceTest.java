package org.cocktail.fwkcktlpersonne.server.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;


public class EOCompteServiceTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlPersonne");
	
	@Dummy
	EOVlans vlanE;
	
	@Dummy
	EOVlans vlanG;
	
	@Dummy
	EOVlans vlanP;
	
	@Dummy
	EOVlans vlanR;
	
	@Dummy
	EOVlans vlanD;
	
	
	@Before
	public void setVlan(){
		vlanE.setCVlan("E");
		vlanE.setDomaine("Easso-cocktail.fr");
		vlanE.setPriorite(4);
		
		vlanG.setCVlan("G");
		vlanG.setDomaine(null);
		vlanG.setPriorite(3);
		
		vlanD.setCVlan("D");
		vlanD.setDomaine(null);
		vlanD.setPriorite(2);
		
		vlanP.setCVlan("P");
		vlanP.setDomaine("asso-cocktail.fr");
		vlanP.setPriorite(1);
		
		vlanR.setCVlan("R");
		vlanR.setDomaine("asso-cocktail.fr");
		vlanP.setPriorite(0);
		
		
	}
	
	@Test
	public void determinationDomaineVlans(){
		
		CompteService service = new CompteService();
		assertEquals("(toVlans.cVlan = 'E')", service.determinationDomaineVlans(ec, "E").toString());
		assertEquals("(toVlans.cVlan = 'G')", service.determinationDomaineVlans(ec, "G").toString());
		assertEquals("(toVlans.cVlan = 'D')", service.determinationDomaineVlans(ec, "D").toString());
		
		assertEquals("((toVlans.cVlan = 'R') or (toVlans.cVlan = 'P'))", service.determinationDomaineVlans(ec, "R").toString());
		
		assertEquals("((toVlans.cVlan = 'P') or (toVlans.cVlan = 'R'))", service.determinationDomaineVlans(ec, "P").toString());
		
	}
	
	@Test()
	public void retourneUnParamNumerique() throws Exception {
		
		CompteService service = new CompteService(){
			@Override
			protected String getGrhumParameter(EOEditingContext ec, String nomParam) {
				return "3";
			}
		};
		
		assertEquals("Si le paramètre est déjà renseigné, la fonctionne retourne la valeur ", "4", service.retourneUnParamNumerique(ec, CompteService.PARAM_PASSWORD_LENGTH_MIN, new Integer("4")).toString());
		assertEquals("Si le paramètre n'est pas encore renseigné, la fonction va récupérer la valeur du paramètre en base.", "3", service.retourneUnParamNumerique(ec, CompteService.PARAM_PASSWORD_LENGTH_MIN, null).toString());
	}
	
	@Test(expected=Exception.class)
	public void retourneUnParamNumeriqueRenvoieUneException() throws Exception {
		CompteService service = new CompteService() {
			@Override
			protected String getGrhumParameter(EOEditingContext ec, String nomParam) {
				return null;
			}
		};
		
		service.retourneUnParamNumerique(ec, CompteService.PARAM_PASSWORD_LENGTH_MIN, null);
	}
	
	
	@Test
	public void isPasswordHistoryDisabled() throws Exception {
		
		CompteService service = getStubCompteService(true, "N");
		
		assertFalse(service.isHistorisationActivePourVlanParticulier(ec, "X"));
	}
	
	@Test
	public void isPasswordHistoryForVlanEnabled() throws Exception {
		
		CompteService service = getStubCompteService(true, "O");
		
		assertTrue(service.isHistorisationActivePourVlanParticulier(ec, "X"));
	}
	
	
	@Test
	public void isPasswordHistoryForVlanDisabled() throws Exception {
		
		CompteService service = getStubCompteService(false, "O");
		
		assertFalse(service.isHistorisationActivePourVlanParticulier(ec, "X"));
	}

	
	@Test
	public void isPasswordHistoryForVlanInexistant() throws Exception {
		
		CompteService service = new CompteService() {
			@Override
			protected boolean isCodeActivationActif(String nomParam) {
				fail("Je n'ai aucune raison de passer par ici !");
				return false;
			}
		};
		service.setPasswordService(new PasswordService() {
			@Override
			protected String getHistorisationParametre(EOEditingContext ec) {
				return "O";
			}
		});
		
		assertFalse("En cas de Vlan inconnu, il n'y a pas d'HistoryPassword", service.isHistorisationActivePourVlanParticulier(ec, "W"));
	}
	
	@Test
	public void testUnicite() {
		UnicitePwd unicitePwd = new UnicitePwd(null);
		List<String> pwds = Arrays.asList("pwd1", "pwd2", "pwd3");
		Assert.assertTrue(unicitePwd.checkUnicite(pwds, "pwd4"));
	}

	@Test
	public void testUniciteFailure() {
		UnicitePwd unicitePwd = new UnicitePwd(null);
		List<String> pwds = Arrays.asList("pwd1", "pwd2", "pwd3");
		Assert.assertFalse(unicitePwd.checkUnicite(pwds, "pwd2"));
	}

	@Test
	public void testGenererPwdUnique() {
		PwdGenerator generator = mock(PwdGenerator.class);
		when(generator.generate()).thenReturn("pwd1", "pwd2", "pwd");

		UnicitePwd unicitePwd = new UnicitePwd(generator);

		List<String> pwds = Arrays.asList("pwd1", "pwd2", "pwd3");
		String pwd = unicitePwd.genererPwdUnique(pwds);
		Assert.assertNotNull(pwd);
		Assert.assertEquals("pwd", pwd);
	}

	@Test
	public void testGenererPwdUniqueFailure() {
		PwdGenerator generator = mock(PwdGenerator.class);
		when(generator.generate()).thenReturn("pwd1", "pwd1", "pwd1", "pwd1", "pwd1", "pwd1", "pwd");

		UnicitePwd unicitePwd = new UnicitePwd(generator);

		List<String> pwds = Arrays.asList("pwd1", "pwd2", "pwd3");
		try {
			unicitePwd.genererPwdUnique(pwds);
			Assert.fail("Runtime exception expected!");
		} catch (Throwable t) {
			Assert.assertTrue(t instanceof RuntimeException);
		}
	}
	
	@Test
	public void testClasserParPriorite() {
	    CompteService service = new CompteService();
	    EOCompte compteVlanE = compte(vlanE);
	    EOCompte compteVlanG = compte(vlanG);
	    EOCompte compteVlanP = compte(vlanP);
	    
	    NSArray<EOCompte> comptes = new NSArray<EOCompte>(compteVlanE, compteVlanP, compteVlanG);
	    NSArray<EOCompte> comptesClasses = service.classerParPriorite(comptes);
	    
	    assertTrue(comptesClasses.size() == 3);
	    assertTrue(comptesClasses.objectAtIndex(0) == compteVlanP);
	    assertTrue(comptesClasses.objectAtIndex(1) == compteVlanG);
	    assertTrue(comptesClasses.objectAtIndex(2) == compteVlanE);
	}
	
	@Test
	public void testDeterminationCptePrincipal() {
        CompteService service = new CompteService();
        EOCompte compteVlanE = compte(vlanE);
        EOCompte compteVlanG = compte(vlanG);
        EOCompte compteVlanP = compte(vlanP);
        
        service.determinationCptePrincipal(ec, 999);
        
        assertTrue(compteVlanP.cptPrincipal() == "O");
        assertTrue(compteVlanG.cptPrincipal() == "N");
        assertTrue(compteVlanE.cptPrincipal() == "N");
	}
	
	@Test
	public void testDeterminationCptePrincipalCompteNonValide() {
	    CompteService service = new CompteService();
	    EOCompte compteVlanE = compte(vlanE);
	    EOCompte compteVlanG = compte(vlanG);
	    EOCompte compteVlanP = compte(vlanP);
	    compteVlanP.setCptValide("N");
	    
	    service.determinationCptePrincipal(ec, 999);
	    
	    assertTrue(compteVlanP.cptPrincipal() == "N");
	    assertTrue(compteVlanG.cptPrincipal() == "O");
	    assertTrue(compteVlanE.cptPrincipal() == "N");
	}
	
	@Test
	public void testDeterminationCptePrincipalNeChangePasLesObjets() {
	    CompteService service = new CompteService();
	    EOCompte compteVlanE = compteSpy(vlanE);
	    EOCompte compteVlanG = compteSpy(vlanG);
	    EOCompte compteVlanP = compteSpy(vlanP);

	    service.determinationCptePrincipal(ec, 999);
	    
        Mockito.verify(compteVlanP, Mockito.times(1)).setCptPrincipal("O");
        // Ceux là sont appelés une fois dans le awakeFromInsertion
        // mais pas plus !
        Mockito.verify(compteVlanG, Mockito.times(1)).setCptPrincipal("N");
        Mockito.verify(compteVlanE, Mockito.times(1)).setCptPrincipal("N");
	}

    private EOCompte compte(EOVlans vlan) {
        EOCompte compte = ec.createSavedObject(EOCompte.class);
        compte.setToVlansRelationship(vlan);
        compte.setPersId(999);
        return compte;
    }
	
    private EOCompte compteSpy(EOVlans vlan) {
        EOCompte compte = Mockito.spy(new EOCompte());
        ec.insertSavedObject(compte);
        compte.setToVlansRelationship(vlan);
        compte.setPersId(999);
        return compte;
    }
    
    
    
    
    protected CompteService getStubCompteService(final boolean historisationActiveePourVlan, final String historisationGlobaleActivee) {
		CompteService service = new CompteService() {
			@Override
			protected boolean isCodeActivationActif(String nomParam) {
				assertEquals(FwkCktlPersonneParamManager.COMPTE_PASSWORD_HISTORY_VLAN_X, nomParam);
				return historisationActiveePourVlan;
			}
		};
		service.setPasswordService(new PasswordService() {
			@Override
			protected String getHistorisationParametre(EOEditingContext ec) {
				return historisationGlobaleActivee;
			}
		});
		return service;
	}
}
