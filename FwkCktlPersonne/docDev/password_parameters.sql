--
-- Ajout des parametres relatifs au changement des mots de passe
--

-- Longueur du mot de passe
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1100, 'PASSWORD_LENGTH_MIN', '8', 'Longueur minimum du mot de passe');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1101, 'PASSWORD_LENGTH_MAX', '8', 'Longueur maximum du mot de passe');
-- Caracteres autorises
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1102, 'PASSWORD_AUTHORIZED_CHARS', '!"#$%&''()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~','Liste des caracteres autorises dans le mot de passe');
-- Regles sur la complexite
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1103, 'PASSWORD_RULE_NB_UPPERCASE', '1','Nombre minimum de majuscules dans le mot de passe (0 si non actif)');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1104, 'PASSWORD_RULE_NB_DIGITS', '1','Nombre minimum de chiffres dans le mot de passe (0 si non actif)');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1105, 'PASSWORD_RULE_NB_ALPHABETICAL', '1','Nombre minimum de lettres dans le mot de passe (0 si non actif)');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1106, 'PASSWORD_RULE_NB_LOWERCASE', '1','Nombre minimum de minuscules dans le mot de passe (0 si non actif)');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1107, 'PASSWORD_RULE_NB_NONALPHANUMERIC', '1','Nombre minimum de caracteres non alphanumeriques dans le mot de passe (0 si non actif)');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1108, 'PASSWORD_RULES_MIN', '3','Nombre minimum de regles a respecter pour valider le mot de passe');
-- Dictionnaires
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1109, 'PASSWORD_DICTIONARY_SEARCH', 'O','Booleen indiquant si la recherche par dictionnaires est activee ( O | N )');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1110, 'PASSWORD_DICTIONARY_LOCATION', '/tmp/dico','Dossier contenant les dictionnaires');
-- Historisation
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1111, 'PASSWORD_HISTORY', 'O','Booleen indiquant si l''historisation des mots de passe est activee ( O | N )');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1112, 'PASSWORD_VALID_PERIOD_CREATE', '90','Duree de validite du mot de passe lors de la creation du compte (en jours)');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1113, 'PASSWORD_VALID_PERIOD_MODIFY', '365','Duree de validite du mot de passe lors de la modification par l''utilisateur (en jours)');
insert into grhum.grhum_parametres (param_ordre, param_key, param_value, param_commentaires) values (1114, 'PASSWORD_VALID_PERIOD_MODIFY_ADMIN', '7','Duree de validite du mot de passe lors de la modification par un administrateur (en jours)');