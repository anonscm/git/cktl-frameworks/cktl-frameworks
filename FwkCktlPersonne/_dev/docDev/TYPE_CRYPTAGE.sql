



declare laseq number;

begin



select GRHUM.TYPE_CRYPTAGE_SEQ.nextval into laseq from dual;


INSERT INTO GRHUM.TYPE_CRYPTAGE (
   TCRY_ORDRE, TCRY_LIBELLE, TCRY_JAVA_METHODE, 
   D_DEB_VAL, D_FIN_VAL) 
VALUES (laseq, 
        'UNIX', 
        'org.cocktail.crypto.jcrypt.Jcrypt.crypt', 
        TO_Date( '07/18/2008 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
        null);
        
update grhum.vlans set TCRY_ORDRE = laseq;         
        
select GRHUM.TYPE_CRYPTAGE_SEQ.nextval into laseq from dual;

INSERT INTO GRHUM.TYPE_CRYPTAGE (
   TCRY_ORDRE, TCRY_LIBELLE, TCRY_JAVA_METHODE, 
   D_DEB_VAL, D_FIN_VAL) 
VALUES (laseq, 
        'MD5', 
        'org.cocktail.crypto.md5.MD5Crypt.crypt', 
        TO_Date( '07/18/2008 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
        null);
        
update grhum.vlans set TCRY_ORDRE = laseq where  C_VLAN='X';       
update grhum.vlans set TVPN_CODE='NO'  where  C_VLAN='X'; 


commit;
end;