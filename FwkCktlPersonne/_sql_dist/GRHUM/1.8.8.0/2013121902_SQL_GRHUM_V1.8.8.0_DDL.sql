--
-- Patch DDL de GRHUM du 19/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.8.0
-- Date de publication : 19/12/2013
-- Auteur(s) : Chama LAATIK

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.7.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.8.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.8.0 a deja ete passe !');
    end if;

end;
/

---------------------V20131209.120215__DDL_modification_contrainte_Etudiant.sql---------------------

ALTER TABLE GRHUM.ETUDIANT DROP CONSTRAINT ETUDIANT_INDIVIDU_ULR;

ALTER TABLE GRHUM.ETUDIANT ADD CONSTRAINT ETUDIANT_INDIVIDU_ULR FOREIGN KEY (NO_INDIVIDU) REFERENCES GRHUM.INDIVIDU_ULR DEFERRABLE INITIALLY DEFERRED;
--------------------V20131218.092317__DDL_Maj_table_nature_bonif_territoire.sql---------------------
ALTER TABLE GRHUM.NATURE_BONIF_TERRITOIRE MODIFY NBOT_ORDRE NUMBER;

ALTER TABLE GRHUM.NATURE_BONIF_TERRITOIRE ADD (NBT_ORDRE NUMBER);



