--
-- Patch DDL de GRHUM du 03/10/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.0.0
-- Date de publication : 03/10/2011
-- Auteur(s) : Philippe BERGER
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- GRANTs
--
GRANT SELECT, REFERENCES ON GRHUM.PERSONNE TO PHOTO;
GRANT SELECT ON GRHUM.INDIVIDU_ULR TO PHOTO;
GRANT SELECT ON JEFY_ADMIN.SIGNATURE TO PHOTO;
GRANT SELECT, INSERT, UPDATE, DELETE ON GRHUM.REGROUPEMENT_SECTION TO MANGUE;

-- Rem :  GRHUM a normalement le rôle DBA et donc tout les GRANT DROP ANY mais visiblement ce n'est pas suffisant pour 
GRANT DROP ANY DIRECTORY to GRHUM;
GRANT DROP ANY INDEX to GRHUM;
GRANT DROP ANY INDEXTYPE to GRHUM; 
GRANT DROP ANY PROCEDURE to GRHUM;
GRANT DROP ANY ROLE to GRHUM;
GRANT DROP ANY SEQUENCE to GRHUM;
GRANT DROP ANY SYNONYM to GRHUM;
GRANT DROP ANY TABLE to GRHUM;
GRANT DROP ANY TRIGGER to GRHUM; 
GRANT DROP ANY TYPE to GRHUM; 
GRANT DROP ANY VIEW to GRHUM; 
GRANT DROP PUBLIC DATABASE LINK to GRHUM; 
GRANT DROP PUBLIC SYNONYM to GRHUM;



--
-- Tables de l'application Typage (Validation des paramètres du référentiel) 
--
CREATE TABLE GRHUM.GRHUM_PARAMETRES_TYPE (
  TYPE_ID          	number(8) NOT NULL, 
  TYPE_ID_INTERNE	varchar2(32), 
  TYPE_LIBELLE 		varchar2(64), 
  TYPE_DESCRIPTION 	varchar2(1024), 
  
  CONSTRAINT PK_GRHUM_PARAMETRES_TYPES 
    PRIMARY KEY (TYPE_ID));
COMMENT ON TABLE GRHUM.GRHUM_PARAMETRES_TYPE IS 'Table listant les types de paramètres du reférentiel.';
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES_TYPE.TYPE_ID IS 'Clé primaire des types de paramètres du referentiel.';
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES_TYPE.TYPE_ID_INTERNE IS 'Valeur du code de typage des paramètres.';
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES_TYPE.TYPE_LIBELLE IS 'Libellé du typage des paramètres.';
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES_TYPE.TYPE_DESCRIPTION IS 'Informations sur le typage des paramètres.';

-- Sequences GRHUM_PARAMETRES_TYPES
CREATE SEQUENCE GRHUM.GRHUM_PARAMETRES_TYPE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCACHE NOCYCLE NOORDER;




--
-- GRHUM_PARAMETRES
--
ALTER TABLE GRHUM.GRHUM_PARAMETRES MODIFY PARAM_KEY VARCHAR2(2000);

ALTER TABLE GRHUM.GRHUM_PARAMETRES MODIFY PARAM_VALUE VARCHAR2(200);

COMMENT ON TABLE GRHUM.GRHUM_PARAMETRES IS 'Les paramètres de GRHUM';

COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_ORDRE IS 'Clef primaire de la table GRHUM_PARAMETRES';

COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_KEY IS 'Clef du paramètre';

COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_VALUE IS 'Valeur du paramètre';

COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_COMMENTAIRES IS 'Description du paramètre';

COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_ORDRE IS 'Clef primaire de la table GRHUM_PARAMETRES.';
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_KEY IS 'Paramètre de la table GRHUM_PARAMETRES.';
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_VALUE IS 'Valeur du paramètre de la table GRHUM_PARAMETRES.';
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_COMMENTAIRES IS 'Commentaire du paramètre de la table GRHUM_PARAMETRES.';

ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD PERS_ID_CREATION NUMBER;
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PERS_ID_CREATION IS 'Pers_id de la personne à l''origine de la création de l''enregistrement.';
ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD (CONSTRAINT GRHUM_PARAMETRES_REF_CREATEUR FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID));

ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD PERS_ID_MODIFICATION NUMBER;
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PERS_ID_MODIFICATION IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement.';
ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD (CONSTRAINT GRHUM_PARAMETRES_REF_MODIFICAT FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID));

ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD D_CREATION DATE;
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.D_CREATION IS 'Date de création de l''enregistrement.';

ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD D_MODIFICATION DATE;
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement.';

ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD PARAM_TYPE_ID NUMBER;
COMMENT ON COLUMN GRHUM.GRHUM_PARAMETRES.PARAM_TYPE_ID IS 'Clef étrangère du type du paramètre Cf. GRHUM_PARAMETRES_TYPE.';
ALTER TABLE GRHUM.GRHUM_PARAMETRES ADD CONSTRAINT GRHUM_PARAMETRES_REF_TYPE FOREIGN KEY (PARAM_TYPE_ID) REFERENCES GRHUM.GRHUM_PARAMETRES_TYPE (TYPE_ID);



--
-- BANQUE
--
ALTER TABLE GRHUM.BANQUE ADD TEM_LOCAL CHAR(1) DEFAULT'N';

COMMENT ON COLUMN GRHUM.BANQUE.TEM_LOCAL IS 'Témoin indiquant s''il s''agit d''une nomenclature locale ( ''O''ui ou ''N''on )';

ALTER TABLE GRHUM.BANQUE ADD (CONSTRAINT CHK_BANQUE_TEM_LOCAL CHECK ((TEM_LOCAL IN ('O','N'))));

ALTER TABLE GRHUM.BANQUE ADD PERS_ID_CREATION NUMBER;

COMMENT ON COLUMN GRHUM.BANQUE.PERS_ID_CREATION IS 'Pers_id de la personne à l''origine de la création de l''enregistrement';

ALTER TABLE GRHUM.BANQUE ADD (CONSTRAINT BANQUE_REF_PERS_CREAT FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.BANQUE ADD PERS_ID_MODIFICATION NUMBER;

COMMENT ON COLUMN GRHUM.BANQUE.PERS_ID_MODIFICATION IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';

ALTER TABLE GRHUM.BANQUE ADD (CONSTRAINT BANQUE_REF_PERS_MODIF FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);



--
-- INCLUSION_CORPS
--
ALTER TABLE GRHUM.INCLUSION_CORPS DROP CONSTRAINT UK_INCLUSION_CORPS;

DROP INDEX GRHUM.UK_INCLUSION_CORPS;

CREATE UNIQUE INDEX GRHUM.UK_INCLUSION_CORPS ON GRHUM.INCLUSION_CORPS(NO_SEQ_COLLEGE, C_CORPS, C_CATEGORIE, C_GROUPE_EXCLUSION);

ALTER TABLE GRHUM.INCLUSION_CORPS ADD (CONSTRAINT UK_INCLUSION_CORPS UNIQUE (NO_SEQ_COLLEGE, C_CORPS, C_CATEGORIE, C_GROUPE_EXCLUSION));



--
-- LANGUE
--
ALTER TABLE GRHUM.LANGUE ADD TEM_LOCAL CHAR(1) DEFAULT('N');

COMMENT ON COLUMN GRHUM.LANGUE.TEM_LOCAL IS 'Témoin indiquant s''il s''agit d''une nomenclature locale ( ''O''ui OU ''N''on )';

ALTER TABLE GRHUM.LANGUE ADD (CONSTRAINT CHK_LANGUE_TEM_LOCAL CHECK ((TEM_LOCAL IN ('O','N'))));



--
-- ASSOCIATION
--
ALTER TABLE GRHUM.ASSOCIATION MODIFY ASS_CODE NOT NULL; 



--
-- REPART_ASSOCIATION
--
ALTER TABLE GRHUM.REPART_ASSOCIATION ENABLE CONSTRAINT RASS_REF_PERSONNE;
ALTER TABLE GRHUM.REPART_ASSOCIATION ENABLE CONSTRAINT REPART_ASS_REF_PERS_CREAT;
ALTER TABLE GRHUM.REPART_ASSOCIATION ENABLE CONSTRAINT REPART_ASS_REF_PERS_MODIF;



--
-- PERSONNE_TELEPHONE
--
ALTER TABLE GRHUM.PERSONNE_TELEPHONE MODIFY NO_TELEPHONE VARCHAR2(20);



--
-- EVT_TACHE
--
ALTER TABLE GRHUM.EVT_TACHE ADD (CONSTRAINT TACHE_EVT_REF_PERSID_CREATE FOREIGN KEY (PERSID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID));
ALTER TABLE GRHUM.EVT_TACHE ADD (CONSTRAINT TACHE_EVT_REF_PERSID_MODIF FOREIGN KEY (PERSID_MODIF) REFERENCES GRHUM.PERSONNE (PERS_ID));



--
-- ZONE_MONDE
--
ALTER TABLE GRHUM.ZONE_MONDE ADD ( CONSTRAINT CHK_TYPE_ZONE CHECK ( TYPE_ZONE IN ('GEO','POL','ECO')));

COMMENT ON COLUMN GRHUM.ZONE_MONDE.TYPE_ZONE IS 'Type de zone (GEO = géographique, POL = politique, ECO = Economique)';



--
-- PAYS_ZONE
--
CREATE TABLE GRHUM.PAYS_ZONE
(
  PAZO_ORDRE            NUMBER(4)                NOT NULL,
  PAZO_CODE             VARCHAR2(3)				 NOT NULL,
  C_ISO					VARCHAR2(2),
  C_ZONE_MONDE          VARCHAR2(5)              NOT NULL,
  D_CREATION            DATE                     NOT NULL,
  D_MODIFICATION        DATE                     NOT NULL
);

COMMENT ON TABLE GRHUM.PAYS_ZONE IS 'Table de répartition des pays avec les différentes zones géographiques, politiques, économiques.';

COMMENT ON COLUMN GRHUM.PAYS_ZONE.PAZO_ORDRE IS 'Clef primaire de la table PAYS_ZONE.';

COMMENT ON COLUMN GRHUM.PAYS_ZONE.PAZO_CODE IS 'Code du pays ou code du département.';

COMMENT ON COLUMN GRHUM.PAYS_ZONE.C_ISO IS 'Code ISO sur deux caractères à la norme ISO 3166.';

COMMENT ON COLUMN GRHUM.PAYS_ZONE.C_ZONE_MONDE IS 'Code de la zone. Clef étrangère vers ZONE_MONDE.';

COMMENT ON COLUMN GRHUM.PAYS_ZONE.D_CREATION IS 'Date de création de l''enregistrement.';

COMMENT ON COLUMN GRHUM.PAYS_ZONE.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement.';

CREATE UNIQUE INDEX GRHUM.PK_PAYS_ZONE ON GRHUM.PAYS_ZONE(PAZO_ORDRE) LOGGING NOPARALLEL;

ALTER TABLE GRHUM.PAYS_ZONE ADD CONSTRAINT UK_PAYS_ZONE UNIQUE (PAZO_CODE,C_ZONE_MONDE);

ALTER TABLE GRHUM.PAYS_ZONE ADD (CONSTRAINT PK_PAYS_ZONE PRIMARY KEY(PAZO_ORDRE));

--ALTER TABLE GRHUM.PAYS_ZONE ADD (CONSTRAINT PAYS_ZONE_REF_PAYS FOREIGN KEY (C_PAYS) REFERENCES GRHUM.PAYS (C_PAYS));

ALTER TABLE GRHUM.PAYS_ZONE ADD (CONSTRAINT PAYS_ZONE_REF_ZONE_MONDE FOREIGN KEY (C_ZONE_MONDE) REFERENCES GRHUM.ZONE_MONDE (C_ZONE_MONDE));

-- Séquence
CREATE SEQUENCE GRHUM.PAYS_ZONE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCACHE NOCYCLE NOORDER; 



--
-- V_PAYS_AFRIQUE
--
CREATE OR REPLACE FORCE VIEW GRHUM.V_PAYS_AFRIQUE(C_PAYS, LL_PAYS, LC_PAYS, L_NATIONALITE, D_DEB_VAL,D_FIN_VAL, D_CREATION, D_MODIFICATION, CODE_ISO)
AS
SELECT P.C_PAYS,LL_PAYS,LC_PAYS,L_NATIONALITE,D_DEB_VAL,D_FIN_VAL,P.D_CREATION,P.D_MODIFICATION,CODE_ISO
FROM grhum.pays P, grhum.PAYS_ZONE PZ
WHERE P.C_PAYS = PZ.PAZO_CODE
AND PZ.c_zone_monde = 'AF';
/



--
-- V_PERSONNEL_ACTUEL_ENS
--
CREATE OR REPLACE FORCE VIEW GRHUM.V_PERSONNEL_ACTUEL_ENS
(NO_DOSSIER_PERS)
AS 
SELECT c.no_dossier_pers
     FROM element_carriere c, corps co, type_population tp
    WHERE (    c.d_effet_element <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_element IS NULL
                OR c.d_fin_element >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_valide = 'O'
      AND c.c_corps = co.c_corps
      AND co.c_type_corps = tp.c_type_population
      AND (   tp.tem_enseignant = 'O'
           OR tp.tem_2degre = 'O'
           OR tp.tem_ens_sup = 'O'
          )
   UNION
   SELECT c.no_dossier_pers
     FROM contrat c, type_contrat_travail tct
    WHERE (    c.d_deb_contrat_trav <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_contrat_trav IS NULL
                OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND tct.droit_conges_contrat = 'O'
      AND c.tem_annulation = 'N'
      AND tct.tem_enseignant = 'O'
      AND c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND tct.tem_remuneration_accessoire != 'O'
      AND tct.tem_titulaire != 'O'
      AND tct.tem_invite_associe != 'O'
   UNION
   SELECT c.no_dossier_pers
     FROM element_carriere c, corps co
    WHERE c.c_corps = co.c_corps
      AND co.lc_corps IN ('M.A.', 'DCIO ET CONS.ORIENT.')
      AND (    c.d_effet_element <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_element IS NULL
                OR c.d_fin_element >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_valide = 'O'
   UNION
   -- inclusion des contrats non enseignant typés par le
   -- typepopulation enseignant
   SELECT no_dossier_pers
     FROM contrat c,
          type_contrat_travail tct,
          mangue.contrat_avenant ca,
          grade g,
          corps c,
          type_population tp
    WHERE (    c.d_deb_contrat_trav <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_contrat_trav IS NULL
                OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND tct.droit_conges_contrat = 'O'
      AND c.tem_annulation = 'N'
      AND tct.tem_enseignant = 'N'
      AND c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND tct.tem_remuneration_accessoire != 'O'
      AND tct.tem_titulaire != 'O'
      AND c.no_seq_contrat = ca.no_seq_contrat
      AND ca.c_grade = g.c_grade
      AND g.c_corps = c.c_corps
      AND c.c_type_corps = tp.c_type_population
      AND tp.tem_enseignant = 'O'
      AND ca.tem_annulation = 'N'
      AND ca.d_deb_contrat_av <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      AND (   ca.d_fin_contrat_av IS NULL
           OR ADD_MONTHS (ca.d_fin_contrat_av, 1) >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
          )
   UNION
   -- Prise en compte des Professeurs émérites
    select no_dossier_pers from mangue.emeritat E
    WHERE E.D_DEB_EMERITAT <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
    AND (E.D_FIN_EMERITAT IS NULL OR E.D_FIN_EMERITAT >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY') )
    AND NO_ARRETE_EMERITAT is not null;
/

GRANT SELECT ON GRHUM.V_PERSONNEL_ACTUEL_ENS TO MANGUE;



--
-- format_Iban
--
CREATE OR REPLACE FUNCTION GRHUM.format_Iban(iban varchar)
RETURN varchar
-- Fonction qui renvoie un iban formate (representation FR)
-- le paremetre doit avoir une longueur de 27 caracteres pour etre formate
is
begin
    if (length(iban) <> 27) then
        return iban;
    end if;

    return substr(iban, 1,4) || ' ' ||
           substr(iban, 5,4) || ' ' ||
           substr(iban, 9,4) || ' ' ||
           substr(iban, 13,4) || ' ' ||
           substr(iban, 17,4) || ' ' ||
           substr(iban, 21,4) || ' ' ||
           substr(iban, 25) ;

--FR14 2004 1010 0505 0001 3M02 606
end;
/



--
-- V_RIBFOUR_GRHUM
--
CREATE OR REPLACE FORCE VIEW GRHUM.V_RIBFOUR_GRHUM
(
 RIB_ETAB, RIB_GUICH, RIB_CLE, FOU_ORDRE, MOD_CODE,RIB_NUM, RIB_ORDRE, RIB_TITCO, RIB_DOMICIL,
 MOD_DOM,MOD_LIB,
 BIC, IBAN
)
AS 
SELECT r.c_banque, r.c_guichet, cle_rib,r.fou_ordre, r.mod_code, no_compte,rib_ordre, rib_titco, b.DOMICILIATION,
  m.MOD_DOM, m.MOD_LIB,
  r.BIC, r.IBAN
FROM GRHUM.RIBFOUR_ULR r, GRHUM.BANQUE b, GRHUM.V_MODE_PAIEMENT m
WHERE r.C_BANQUE = b.C_BANQUE
AND r.C_GUICHET = b.C_GUICHET
AND m.MOD_CODE = r.MOD_CODE (+);
/



--
-- V_SUIVIFOUR_PAYE
--
CREATE OR REPLACE FORCE VIEW GRHUM.V_SUIVIFOUR_PAYE
(
    ORIGINE,
    SUIVI_CLE,
    NO_INDIVIDU,
    PERS_ID,
    IND_NO_INSEE,
    MOIS,
    EXE_ORDRE,
    D_PAIE,
    PAYE_INDICE,
    PAYE_QUOTITE,
    PAYE_NB_HEURES,
    PAYE_NET,
    C_MINISTERE,
    NO_DOSSIER
)
AS
    SELECT 'PAPAYE' AS origine,
           ('PAPAYE_' || ph.paye_ordre) AS suivi_cle,
           i.no_individu,
           i.pers_id,
           i.IND_NO_INSEE,
           TO_CHAR (m.mois_debut, 'MM') AS MOIS,
           TO_CHAR (ph.exe_ordre),
           TO_CHAR (m.mois_fin, 'dd/mm/yyyy') d_paie,
           NVL (PAYE_INDICE, 'NC') AS PAYE_INDICE,
           PAYE_QUOTITE,
           PAYE_NB_HEURES,
           PAYE_NET,
           'XXX' C_ministere,
           'XX' NO_dossier
      FROM grhum.individu_ulr i,
           jefy_paye.paye_contrat pctr,
           jefy_paye.paye_statut ps,
           jefy_paye.paye_histo ph,
           jefy_paye.paye_mois m
     WHERE     pctr.no_individu = i.no_individu
           AND pctr.pctr_ordre = ph.pctr_ordre
           AND ph.psta_ordre = ps.psta_ordre
           AND ph.mois_ordre = m.mois_ordre
    UNION ALL
    SELECT 'TGKA' AS origine,
           ('TGKA_' || k.idka05) AS suivi_cle,
           i.no_individu,
           i.pers_id,
           no_insee,
           TO_CHAR (d_paie, 'MM') MOIS,
           TO_CHAR (d_paie, 'YYYY') AS exe_ordre,
           TO_CHAR (d_paie, 'dd/mm/yyyy') d_paie,
           TO_CHAR (INDICE_MAJORE),
           DECODE (num_temps_partiel, 0, 100, num_temps_partiel)
              AS PAYE_QUOTIT,
           NB_HEURES_REMUN,
           MONTANT_NET_A_PAYER,
           C_ministere,
           NO_dossier
      FROM jefy_paye.ka_05 k, grhum.individu_ulr i
     WHERE i.IND_NO_INSEE = k.NO_INSEE
    UNION ALL
    SELECT 'TGKX' AS origine,
           ('TGKX_' || k.idkx05) AS suivi_cle,
           i.no_individu,
           i.pers_id,
           no_insee,
           TO_CHAR (d_paie, 'MM') MOIS,
           TO_CHAR (d_paie, 'YYYY') AS exe_ordre,
           TO_CHAR (d_paie, 'dd/mm/yyyy') d_paie,
           TO_CHAR (INDICE_MAJORE),
           DECODE (num_temps_partiel, 0, 100, num_temps_partiel)
              AS PAYE_QUOTIT,
           NB_HEURES_REMUN,
           MONTANT_NET_A_PAYER,
           C_ministere,
           NO_dossier
      FROM jefy_paf.kx_05 k, grhum.individu_ulr i
     WHERE i.IND_NO_INSEE = k.NO_INSEE;
/

	 
	 
--
-- V_AGENT_FOURNISSEUR
--
CREATE OR REPLACE FORCE VIEW GRHUM.V_AGENT_FOURNISSEUR
(AGT_ORDRE, AGT_NOM, AGT_PRENOM, AGT_LOGIN, AGT_MAIL, 
 AGT_FOUCONS, AGT_FOUCR, AGT_FOUVAL, AGT_ANNEE, UTL_OUVERTURE, 
 UTL_FERMETURE)
AS 
SELECT A.AGT_ORDRE, A.AGT_NOM, A.AGT_PRENOM,
	   LOWER(A.AGT_LOGIN) "AGT_LOGIN", LOWER(A.AGT_MAIL) "AGT_MAIL",
	   TO_CHAR(1) "AGT_FOUCONS", TO_CHAR(1) "AGT_FOUCR",NVL(A.AGT_FOURNIS,0) "AGT_FOUVAL",
	   '2006' "AGT_ANNEE", TO_DATE('01/01/2006','dd/mm/yyyy') UTL_OUVERTURE, TO_DATE('31/12/2006','dd/mm/yyyy') UTL_FERMETURE
FROM JEFY.AGENT A
UNION
SELECT U.UTL_ORDRE "AGT_ORDRE", P.PERS_LIBELLE "AGT_NOM", P.PERS_LC "AGT_PRENOM",
	   C.CPT_LOGIN "AGT_LOGIN", C.CPT_EMAIL||'@'||C.CPT_DOMAINE "AGT_MAIL",
	   TO_CHAR(MAX(DECODE(F.FON_ID_INTERNE,'ANFOUCS',1,0))) "AGT_FOUCONS",
	   TO_CHAR(MAX(DECODE(F.FON_ID_INTERNE,'ANFOUCR',1,0))) "AGT_FOUCR",
	   TO_CHAR(MAX(DECODE(F.FON_ID_INTERNE,'ANFOUVAL',1,0))) "AGT_FOUVAL",
	   '2007' "AGT_ANNEE", U.UTL_OUVERTURE, U.UTL_FERMETURE
FROM   JEFY_ADMIN.UTILISATEUR U, JEFY_ADMIN.UTILISATEUR_FONCT UF, JEFY_ADMIN.FONCTION F,
	   GRHUM.PERSONNE P, GRHUM.REPART_COMPTE RC, GRHUM.COMPTE C, GRHUM.VLANS V
WHERE U.PERS_ID = P.PERS_ID
AND	  U.UTL_ORDRE = UF.UTL_ORDRE
AND	  UF.FON_ORDRE = F.FON_ORDRE
AND	  F.FON_ID_INTERNE IN ('ANFOUCS','ANFOUCR','ANFOUVAL')
AND	  P.PERS_ID = RC.PERS_ID
AND	  RC.CPT_ORDRE = C.CPT_ORDRE
AND	  C.CPT_VLAN = V.C_VLAN
AND   (U.UTL_FERMETURE >= SYSDATE OR U.UTL_FERMETURE IS NULL) 
AND	  (P.PERS_ID,V.PRIORITE) IN
	  (SELECT pers_id,MIN(priorite) FROM
	  (SELECT rcp0.pers_id,cpt0.cpt_ordre,vls0.priorite
	   FROM   REPART_COMPTE rcp0,COMPTE cpt0,VLANS vls0
	   WHERE  rcp0.cpt_ordre = cpt0.cpt_ordre
	   AND    cpt0.cpt_vlan = vls0.c_vlan
	  )
	  GROUP BY pers_id
	  )
GROUP BY U.UTL_ORDRE ,P.PERS_LIBELLE ,P.PERS_LC ,C.CPT_LOGIN ,C.CPT_EMAIL||'@'||C.CPT_DOMAINE , '2007' ,U.UTL_OUVERTURE ,U.UTL_FERMETURE;
/


	 
BEGIN 
  GRHUM.DROP_OBJECT ( 'GRHUM', 'ECHANGER_PERSONNE_BLOBS', 'PROCEDURE' ); 
END;
/



--
-- Echanger_Personne_Photo
--
CREATE OR REPLACE PROCEDURE GRHUM.Echanger_Personne_Photo
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
BEGIN
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   GRHUM.photos_employes_grhum
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
	           'update GRHUM.PHOTOS_EMPLOYES_GRHUM set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   GRHUM.photos_etudiants_grhum
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
	           'update GRHUM.PHOTOS_ETUDIANTS_GRHUM set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   GRHUM.photos_etudiants_old_grhum
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
	           'update GRHUM.PHOTOS_ETUDIANTS_OLD_GRHUM set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
  
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   GRHUM.photos_sav
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
	           'update GRHUM.PHOTOS_SAV set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   GRHUM.signature
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
	           'update GRHUM.SIGNATURE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;
  
END;
/



--
-- Vues obsolètes
--
DROP VIEW GRHUM.V_USER_TAB_COMMENTS ;
DROP VIEW GRHUM.V_USER_TAB_COLUMNS ;
DROP VIEW GRHUM.V_USER_COL_COMMENTS ;


--
-- UID_LIBRE
--
CREATE OR REPLACE FUNCTION GRHUM.Uid_Libre
RETURN NUMBER
IS
  uidLibre INTEGER;
  uidMin INTEGER;
  uidMax INTEGER;
BEGIN

    select nvl(to_number(param_value),1000) into uidMin from grhum.grhum_parametres where param_key='GRHUM_UID_MIN';
    select nvl(to_number(param_value),65000) into uidMax from grhum.grhum_parametres where param_key='GRHUM_UID_MAX';

    select cpt_uid into uidLibre from
        (select cpt_uid from
            (select rownum cpt_uid from dual connect by level < uidMax)
            where cpt_uid >= uidMin
            minus
            select cpt_uid from compte
        )
    where rownum=1;

    RETURN uidLibre;

END Uid_Libre;
/



--
-- COMPTE
--
BEGIN 
  GRHUM.DROP_OBJECT ( 'GRHUM', 'CPT_NEW_LOGIN', 'INDEX' ); 
END;
/



--
-- FOURNIS_ULR
--
UPDATE GRHUM.FOURNIS_ULR SET CPT_ORDRE=NULL, D_MODIFICATION=SYSDATE WHERE FOU_ORDRE IN (SELECT FOU_ORDRE FROM FOURNIS_ULR WHERE CPT_ORDRE NOT IN (SELECT CPT_ORDRE FROM COMPTE));

ALTER TABLE GRHUM.FOURNIS_ULR ADD (CONSTRAINT FOURNIS_REF_COMPTE FOREIGN KEY (CPT_ORDRE) REFERENCES GRHUM.COMPTE(CPT_ORDRE) DEFERRABLE INITIALLY DEFERRED);




-- select cpt_login,count(*) from compte group by cpt_login having count(*) > 1
-- procedure Purge_Compte_Double (disponible si besoin et à adapter)
CREATE UNIQUE INDEX GRHUM.UK_CPT_LOGIN ON GRHUM.COMPTE(CPT_LOGIN);



--
-- TRG_COMPTE_EMAIL
--
CREATE OR REPLACE TRIGGER GRHUM.TRG_COMPTE_EMAIL
AFTER INSERT OR UPDATE OR DELETE ON GRHUM.COMPTE_EMAIL FOR EACH ROW
-- auteur : Cocktail
-- creation : 31/05/2011
-- modification : 31/05/2011
-- Mise à jour des attributs CPT_EMAIL et CPT_DOMAINE de COMPTE à partir de COMPTE_EMAIL
DECLARE

    cpt         INTEGER;
    cptEmail    GRHUM.COMPTE.CPT_EMAIL%TYPE;
    cptDomaine  GRHUM.COMPTE.CPT_DOMAINE%TYPE;
    
BEGIN

    cpt := 0;
    cptEmail := NULL;
    cptDomaine := NULL;
    
    select count(*) into cpt from GRHUM.COMPTE where cpt_ordre = :NEW.cpt_ordre;
    if (cpt != 0) then
        select cpt_email,cpt_domaine into cptEmail,cptDomaine from GRHUM.COMPTE where cpt_ordre = :NEW.cpt_ordre;
        if (cptEmail is null or cptDomaine is null) then
            update GRHUM.COMPTE set cpt_email = :NEW.cem_email, cpt_domaine = :NEW.cem_domaine, d_modification = sysdate where cpt_ordre = :NEW.cpt_ordre; 
        end if; 
    end if;

END;
/


--
-- Admin_GRHUM
--
-- rem : ajout de ETABLISSEMENTS ETRANGERS
CREATE OR REPLACE PACKAGE GRHUM.Admin_GRHUM
IS
    FUNCTION persId_For_Login(login GRHUM.COMPTE.CPT_LOGIN%TYPE) RETURN NUMBER;
    PROCEDURE Install_Params_Annuaire(ins_nom GRHUM.INDIVIDU_ULR.nom_usuel%TYPE,ins_prenom GRHUM.INDIVIDU_ULR.prenom%TYPE,ins_login GRHUM.COMPTE.CPT_LOGIN%TYPE);
END Admin_GRHUM;
/

CREATE OR REPLACE PACKAGE BODY GRHUM.Admin_GRHUM
IS
    
FUNCTION persId_For_Login(login GRHUM.COMPTE.CPT_LOGIN%TYPE) RETURN NUMBER
IS
nbr     INTEGER;
persId  NUMBER;
BEGIN
    select count(*) into nbr from GRHUM.COMPTE where cpt_login=login;
    if (nbr=1) then
        select pers_id into persId from GRHUM.COMPTE where cpt_login=login;
        return persId;
    else
        return NULL;
    end if;
END persId_For_Login;

PROCEDURE Install_Params_Annuaire
(
 ins_nom         GRHUM.INDIVIDU_ULR.nom_usuel%TYPE,
 ins_prenom      GRHUM.INDIVIDU_ULR.prenom%TYPE,
 ins_login       GRHUM.COMPTE.cpt_login%TYPE
)
IS
-- Auteur : Cocktail
-- creation : 2000
-- modification : 25/03/2011 
-- Procédure de mise en place de l'arborescence des groupes du référentiel

localPersIdStructure        GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;
localCStructure             GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;

localPersIdIndividu         GRHUM.INDIVIDU_ULR.PERS_ID%TYPE;
localNoIndividu             GRHUM.INDIVIDU_ULR.NO_INDIVIDU%TYPE;

localPersIdStructureDivers  GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;
localCStructureDivers       GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;

localPersIdSousStructure    GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;
localCSousStructure         GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;

localCStructureRacine       GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;

localCptOrdre               GRHUM.COMPTE.CPT_ORDRE%TYPE;

BEGIN

     -- Insertion d'une structure de départ : Ce sera la racine du navigateur de l'application Annuaire
     -- Le libelle de cette structure sera modifiable par l'application annuaire . Il est mis par défaut à 'UNIVERSITE' .
       GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'RACINE ETABLISSEMENT', 'RAC.ETAB.', 'E', NULL, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL );

     -- La racine de l'etablissement : c_structure = c_structure_pere and c_type_structure = 'E'
     UPDATE GRHUM.STRUCTURE_ULR SET c_structure_pere = localCStructure WHERE c_structure = localCStructure;
     localCStructureRacine := localCStructure;

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureRacine, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureRacine, 'S','O');

     -- Un individu doit etre cree au depart. Ce sera la personne qui installe l'applications.
     -- Cet individu est cree en fonction des parametres passes a la procedure.
     GRHUM.Ins_Individu_Ulr ( ins_nom, ins_prenom, 'M.', ins_nom, NULL, NULL, NULL, NULL, NULL,
                             NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, localPersIdIndividu, localNoIndividu );

     -- Mise a jour de la table GRHUM_PARAMETRES : on definit un super_user vis à vis du login passe en parametre
     UPDATE GRHUM_PARAMETRES SET param_value = ins_login WHERE param_key = 'GRHUM_CREATEUR';

     -- Un compte est cree et associe à la personne creee.
     --Cela permettra de faire le lien entre le login windows et la personne qui utilise l'application.
     GRHUM.Ins_Compte ( localPersIdIndividu, 999999, ins_login, 'achanger!', 'N', 'P', NULL, NULL, 'N', localCptOrdre );

     -- On cree une structure par defaut qui permettra d'afficher les fiches des personnes qui n'ont pas de groupes d'affectation.
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'DIVERS', NULL, 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- Mise a jour de la table GRHUM_PARAMETRES. On renseigne la structure par defaut ainsi que le nom du groupe qui contiendra les fournisseurs
     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_STRUCTURE_DEFAUT';
     UPDATE GRHUM_PARAMETRES SET param_value = 'NON' WHERE param_key = 'GRHUM_FORUMS';
     UPDATE GRHUM_PARAMETRES SET param_value = 'NON' WHERE param_key = 'GRHUM_PHOTO';
     UPDATE GRHUM_PARAMETRES SET param_value = 'FOURNISSEUR' WHERE param_key = 'ANNUAIRE_LIBELLE_FOURNISSEURS';

     -- Insertion du groupe pere des SERVICE SCOLARITE
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'SERVICE SCOLARITE', 'SCOL', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'S','O');

     -- Insertion du groupe pere des DOCTORANTS
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'DOCTORANTS', 'DOCT', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- Insertion du groupe pere des DIPLOMES
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'DIPLOMES', 'DIPL', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- Insertion du groupe pere des PARCOURS CLASSIQUES
     GRHUM.Ins_Structure_Ulr ( localCSousStructure, localPersIdSousStructure, 'PARCOURS CLASSIQUES', 'CLASSIQUES', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCSousStructure, 'G','O');

     -- Insertion du groupe pere des PARCOURS LMD
     GRHUM.Ins_Structure_Ulr ( localCSousStructure, localPersIdSousStructure, 'PARCOURS LMD', 'LMD', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCSousStructure, 'G','O');

     -- Insertion du groupe pere des fournisseurs
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEUR', NULL, 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'FO','O');     

     -- Fournisseurs annulés
     GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'FOURNISSEURS ANNULES', NULL, 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructure WHERE param_key = 'ANNUAIRE_FOU_ARCHIVES';

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'FO','O');     

     -- Fournisseurs en cours de validation
     GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'FOURNISSEURS EN COURS DE VALIDATION', NULL, 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructure WHERE param_key = 'ANNUAIRE_FOU_ENCOURS_VALIDE';

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'FO','O');     

     -- Fournisseurs valides
     GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'FOURNISSEURS VALIDES', NULL, 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'FO','O');     

     -- Insertion du groupe des fournisseurs valides (individus)
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (INDIVIDUS)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_PHYSIQUE';

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'FO','O');     

     -- Insertion du groupe des fournisseurs valides (structures)
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (STRUCTURES)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_MORALE';

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'FO','O');     

     -- Insertion du groupe des fournisseurs valides (étudiants)
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (ETUDIANTS)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_ETUDIANT';

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'FO','O');     

     -- Insertion du groupe des fournisseurs valides (internes)
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (INTERNES)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_INTERNE';

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'FO','O');     

     -- Insertion du groupe des fournisseurs valides (externes)
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (EXTERNES)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_EXTERNE';

     -- Tous les groupes affichés dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'FO','O');     
 
     -- création de la structure 'ENTREPRISES'
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'ENTREPRISES', 'ENTREPRISES', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_ENTREPRISE';

     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- création de la structure 'ENTREPRISES FOURNISSEURS'
     GRHUM.Ins_Structure_Ulr ( localCSousStructure, localPersIdSousStructure, 'ENTREPRISES FOURNISSEURS', 'ENT FOU', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCSousStructure WHERE param_key = 'ANNUAIRE_FOU_ENTREPRISE';

     GRHUM.Maj_Repart_Type_Groupe(localCSousStructure, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCSousStructure, 'FO','O');     

    -- Référentiel Applicatifs
    GRHUM.Ins_Structure_Ulr (localCStructure, localPersIdStructure, 'REFERENTIEL APPLICATIFS', 'REFERENTIEL_APP', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, localNoIndividu, localNoIndividu, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');
    GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'RE','O');

    insert into GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    select GRHUM.GRHUM_PARAMETRES_SEQ.nextval,'ANNUAIRE_REFAPP',localCStructure,'c_structure du groupe "Référentiel Applicatifs"' from dual;

     -- création de la structure 'PARTENARIAT' (sous-groupe de Référentiel Applicatifs)
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'PARTENARIAT', 'PARTENARIAT', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_PARTENARIAT';

     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     -- ce groupe est typé 'Partenariat'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'PN','O');

     -- création de la structure 'PERSONNELS'
     GRHUM.Ins_Structure_Ulr (localCStructureDivers, localPersIdStructureDivers, 'PERSONNELS', 'PERSONNELS', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_PERSONNELS';

    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- création de la structure 'EMPLOYEUR'
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'EMPLOYEUR', 'EMPLOYEUR', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_EMPLOYEUR';

     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

    -- création de l'arborescence 'WORKFLOW' et de ses sou-groupes HEBERGES et WIFI   
    GRHUM.Ins_Structure_Ulr (localCStructureDivers, localPersIdStructureDivers, 'WORKFLOW', 'WORKFLOW', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'RE','O');

    -- WiFi
    GRHUM.Ins_Structure_Ulr (localCStructure, localPersIdStructure, 'COMPTES TICKET WIFI', 'CPT TICKET WIFI', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_TICKET_WIFI', localCStructure, 'c_structure du groupe COMPTES TICKET WIFI' FROM DUAL;

    GRHUM.Ins_Structure_Ulr (localCStructure, localPersIdStructure, 'COMPTES INVITES WIFI', 'CPT INVITES WIFI', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_INVITE_WIFI', localCStructure, 'c_structure du groupe COMPTES INVITES WIFI' FROM DUAL;

    GRHUM.Ins_Structure_Ulr (localCStructure, localPersIdStructure, 'ALIAS VALIDATION TICKET WIFI', 'ALIAS TICKET WIFI', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', 'validticketwifi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_VALID_TICKET_WIFI', localCStructure, 'c_structure du groupe de l''alias de Validation des Tickets WiFi' FROM DUAL;

    -- Hébergés
    GRHUM.Ins_Structure_Ulr (localCStructure, localPersIdStructure, 'HEBERGES', 'HEBERGES', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_HEBERGES', localCStructure, 'c_structure du groupe de workflow des hébergés' FROM DUAL;
    
    GRHUM.Ins_Structure_Ulr (localCStructureDivers, localPersIdStructureDivers, '1 - HEBERGES VALIDES', 'HEBERGES VALIDES', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_HEBERGE_VALIDE', localCStructureDivers, 'c_structure du groupe "Hébergés valides"' FROM DUAL;

    GRHUM.Ins_Structure_Ulr (localCStructureDivers, localPersIdStructureDivers, '2 - HEBERGES EN INSTANCE DE VALIDATION', 'HEBERGES A VALIDER', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_HEBERGE_ENCOURS_VALIDE', localCStructureDivers, 'c_structure du groupe "Hébergés en cours de validation"' FROM DUAL;

    GRHUM.Ins_Structure_Ulr (localCStructureDivers, localPersIdStructureDivers, '3 - HEBERGES ANNULES', 'HEBERGES ANNULES', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_HEBERGE_ARCHIVES', localCStructureDivers, 'c_structure du groupe "Hébergés annulés"' FROM DUAL;

    -- création de l'arborescence 'CONTACT'    
    GRHUM.Ins_Structure_Ulr (localCStructureDivers, localPersIdStructureDivers, 'CONTACTS', 'CONTACTS', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'RE','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'ANNUAIRE_CONTACTS', localCStructureDivers, 'c_structure du groupe "Contacts" pour AGhrum' FROM DUAL;

    -- création de l'arborescence 'ARCHIVES'
    GRHUM.Ins_Structure_Ulr (localCStructureDivers, localPersIdStructureDivers, 'ARCHIVES', 'ARCHIVES', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
    GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'RE','O');

    INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES)
    SELECT GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'ANNUAIRE_ARCHIVES', localCStructureDivers, 'c_structure du groupe des archives' FROM DUAL;

     -- création de la structure 'ETABLISSEMENTS ETRANGERS'
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'ETABLISSEMENTS ETRANGERS', 'ETABS_ETRANGERS', 'A', localCStructureRacine, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_ETABLISSEMENTS_ETRANGERS';

     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'PN','O');
    
END Install_Params_Annuaire;
    
END Admin_GRHUM;
/



COMMIT;
