--
-- Patch DML de GRHUM du 25/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 25/01/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.24';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.25';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.25 a deja ete passe !');
    end if;

end;
/

--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.25', TO_DATE('25/01/2013', 'DD/MM/YYYY'),NULL,'Ajout d un RNE et Suppression de vieilles tables inutilisées et de séquences; Actualisation de ECHANGER_PERSONNE');
-- commit;

--
-- Refonte de GRHUM.ECHANGER_PERSONNE
--
DROP PROCEDURE GRHUM.ECHANGER_PERSONNE_GRHUM;

CREATE OR REPLACE PROCEDURE GRHUM.Echanger_Personne_Grhum
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
  ligne1 VARCHAR2(200);
  ligne2 VARCHAR2(200);
  cursor c1 (oldid number) is select c_structure from grhum.repart_structure where pers_id = oldid;
  cursor c2 (oldid number) is select no_telephone from grhum.personne_telephone where pers_id = oldid;
  cstruct varchar2(10);
  notel varchar2(20);
BEGIN
  ligne1 := NULL;
  ligne2 := NULL;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_individu_urgence = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNEL_ULR set no_individu_urgence = '||newno||' where no_individu_urgence = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0) then
    SELECT COUNT(*)
    INTO   nbenr1
    FROM   grhum.PERSONNEL_ULR
    WHERE  no_dossier_pers = newno;
    IF (nbenr1 = 0) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'create table TEMP00 as select * from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update TEMP00 set no_dossier_pers = '||newno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'insert into GRHUM.PERSONNEL_ULR select * from TEMP00;');

       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
       ligne2 := 'drop table TEMP00;';
    else
       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
    end if;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_PERSONNEL
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_PERSONNEL set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR_EDITION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR_EDITION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DIPLOME
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DIPLOME set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DOSSIER
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DOSSIER set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_HISTORIQUE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_HISTORIQUE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_INDIVIDU_SITUATION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_INDIVIDU_SITUATION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BAP_ELEMENT_CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BAP_ELEMENT_CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BLOC_NOTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BLOC_NOTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_ECHELON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_ECHELON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_INDICIAIRE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_INDICIAIRE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CARRIERE_OLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CARRIERE_OLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CGE_MOD_AGT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CGE_MOD_AGT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CHERCHEUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CHERCHEUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  --

  /*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE_QUOTA
  WHERE  par_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE_QUOTA set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DELEGATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DELEGATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DEPOSITAIRE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DEPOSITAIRE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;


  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ENQUETES_REPONSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ENQUETES_REPONSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ETUDIANT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ETUDIANT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.EVOLUTION_CHEVRON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.EVOLUTION_CHEVRON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.FOURNIS_ULR
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.FOURNIS_ULR set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_DISTINCTIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_DISTINCTIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_FORMATIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_FORMATIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_PSEUDO
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_PSEUDO set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_LA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_ELECTEUR set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_LA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_LA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_TA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_TA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_TA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_TA_STRUCTURE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MAD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MAD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MI_TPS_THERAP
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MI_TPS_THERAP set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_auteur = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_auteur = '||newno||' where no_auteur = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NOTES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NOTES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PAIEMENT
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PAIEMENT set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_ALIAS
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_ALIAS set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_TELEPHONE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) then
    open c2(oldid);
    loop
        fetch c2 into notel;
        exit when c2%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.PERSONNE_TELEPHONE WHERE PERS_ID = newid and no_telephone = notel;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_TELEPHONE set pers_id = '||newid||' where pers_id = '||oldid||' and no_telephone='''||notel||''';');
        end if;
    end loop;
    close c2;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_PERSONNEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_PERSONNEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME_HISTO
  WHERE  pers_id_createur = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME_HISTO set pers_id_createur = '||newid||' where pers_id_createur = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.RELIQUATS_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.RELIQUATS_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ASSOCIATION
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
       SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_ASSOCIATION WHERE PERS_ID = newid;
       IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ASSOCIATION set pers_id = '||newid||' where pers_id = '||oldid||'
               and c_structure not in (select c_structure from GRHUM.REPART_ASSOCIATION where pers_id = '||newid||');');
       END IF;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_BUREAU
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_BUREAU set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_EMPLOI
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldno||','||newno,SYSDATE,
               'update GRHUM.REPART_EMPLOI set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_PERSONNE_ADRESSE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_PERSONNE_ADRESSE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_STRUCTURE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
    open c1(oldid);
    loop
        fetch c1 into cstruct;
        exit when c1%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_STRUCTURE WHERE PERS_ID = newid and c_structure = cstruct;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_STRUCTURE set pers_id = '||newid||' where pers_id = '||oldid||' and c_structure = '||cstruct||';');
        END IF;
    end loop;
    close c1;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SECRETARIAT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SECRETARIAT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STAGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STAGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_owner = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_owner = '||newno||' where grp_owner = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_responsable = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_responsable = '||newno||' where grp_responsable = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_ROLE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_ROLE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_CATEGORIE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_CATEGORIE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.TRAVAUX_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.TRAVAUX_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  IF (ligne1 IS NOT NULL)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne1);
       IF (ligne2 IS NOT NULL) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne2);
       end if;
  END IF;
END;
/

-- Suppression de plusieurs tables de GRHUM

-- MANGUE - CONTRATS  / AFFECTATIONS / CARRIERES
-- DROP TABLE GRHUM.AFFECTATION_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.OCCUPATION_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONTRAT_VACATAIRES CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONTRAT_LBUDS CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONSERVATION_ANCIENNETE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CESS_PROG_ACTIVITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONTRAT_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.NBI CASCADE CONSTRAINTS;

-- MANGUE / CONGES
-- DROP TABLE GRHUM.ABSENCES_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CGNT_ACCIDENT_TRAV CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CG_FIN_ACTIVITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_ACCIDENT_SERV CASCADE CONSTRAINT;
-- DROP TABLE GRHUM.CONGE_ADOPTION CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_FORMATION CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_GARDE_ENFANT CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.DETAIL_CG_MALADIE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_MALADIE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.DECLARATION_MATERNITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_MATERNITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_PATERNITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CLM CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CLD CASCADE CONSTRAINTS;


declare

   c int;

begin

-- MANGUE - CONTRATS  / AFFECTATIONS / CARRIERES

  select count(*) into c from user_procedures where procedure_name = upper('INS_AFFECTATION');

   if c <> 0 then
      execute immediate '
      DROP PROCEDURE GRHUM.INS_AFFECTATION
      ';
   end if;
   select count(*) into c from user_views where view_name = upper('V_DOSSIER_PERSONNEL_INTERVALLE');

   if c <> 0 then
      execute immediate '
      DROP VIEW GRHUM.V_DOSSIER_PERSONNEL_INTERVALLE
      ';
   end if;


  select count(*) into c from user_sequences where sequence_name = upper('AFFECTATION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.AFFECTATION_SEQ
      ';
   end if;
  select count(*) into c from user_sequences where sequence_name = upper('ABSENCES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ABSENCES_SEQ
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONTRAT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONTRAT_SEQ
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONTRAT_LUBDS_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONTRAT_LUBDS_SEQ
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('OCCUPATION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.OCCUPATION_SEQ
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('POSTE_ULR_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.POSTE_ULR_SEQ
      ';
   end if;


   select count(*) into c from user_tables where table_name = upper('AFFECTATION_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.AFFECTATION_OLD CASCADE CONSTRAINTS
      ';
   end if;
  select count(*) into c from user_sequences where sequence_name = upper('AFFECTATION_OLD_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.AFFECTATION_OLD_SEQ
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('AFFECTATION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.AFFECTATION_SEQ
      ';
   end if;


   select count(*) into c from user_tables where table_name = upper('OCCUPATION_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.OCCUPATION_OLD CASCADE CONSTRAINTS 
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('OCCUPATION_OLD_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.OCCUPATION_OLD_SEQ
      ';
   end if;



   select count(*) into c from user_tables where table_name = upper('CONTRAT_VACATAIRES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONTRAT_VACATAIRES CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONTRAT_VACATAIRES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONTRAT_VACATAIRES_SEQ
      ';
   end if;



   select count(*) into c from user_tables where table_name = upper('CONTRAT_LBUDS');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONTRAT_LBUDS CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONTRAT_LBUDS_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONTRAT_LBUDS_SEQ
      ';
   end if;
   
  select count(*) into c from user_tables where table_name = upper('CONSERVATION_ANCIENNETE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONSERVATION_ANCIENNETE CASCADE CONSTRAINTS    
      ';     
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONSERVATION_ANCIENNETE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONSERVATION_ANCIENNETE_SEQ
      ';
   end if;
   
   select count(*) into c from user_tables where table_name = upper('CESS_PROG_ACTIVITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CESS_PROG_ACTIVITE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CESS_PROG_ACTIVITE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CESS_PROG_ACTIVITE_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONTRAT_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONTRAT_OLD CASCADE CONSTRAINTS   
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONTRAT_OLD_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONTRAT_OLD_SEQ
      ';
   end if;
   
   select count(*) into c from user_tables where table_name = upper('NBI');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.NBI CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('NBI_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.NBI_SEQ
      ';
   end if;

-- MANGUE / CONGES   
   select count(*) into c from user_tables where table_name = upper('ABSENCES_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ABSENCES_OLD CASCADE CONSTRAINTS    
      ';     
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ABSENCES_OLD_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ABSENCES_OLD_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CGNT_ACCIDENT_TRAV');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CGNT_ACCIDENT_TRAV CASCADE CONSTRAINTS   
      ';  
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CGNT_ACCIDENT_TRAV_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CGNT_ACCIDENT_TRAV_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CG_FIN_ACTIVITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CG_FIN_ACTIVITE CASCADE CONSTRAINTS   
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CG_FIN_ACTIVITE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CG_FIN_ACTIVITE_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_ACCIDENT_SERV');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_ACCIDENT_SERV CASCADE CONSTRAINT  
      ';   
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONGE_ACCIDENT_SERV_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONGE_ACCIDENT_SERV_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_ADOPTION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_ADOPTION CASCADE CONSTRAINTS  
      '; 
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONGE_ADOPTION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONGE_ADOPTION_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_FORMATION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_FORMATION CASCADE CONSTRAINTS 
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONGE_FORMATION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONGE_FORMATION_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_GARDE_ENFANT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_GARDE_ENFANT CASCADE CONSTRAINTS
      ';  
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONGE_GARDE_ENFANT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONGE_GARDE_ENFANT_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('DETAIL_CG_MALADIE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.DETAIL_CG_MALADIE CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('DETAIL_CG_MALADIE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.DETAIL_CG_MALADIE_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_MALADIE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_MALADIE CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONGE_MALADIE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONGE_MALADIE_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('DECLARATION_MATERNITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.DECLARATION_MATERNITE CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('DECLARATION_MATERNITE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.DECLARATION_MATERNITE_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_MATERNITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_MATERNITE CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONGE_MATERNITE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONGE_MATERNITE_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_PATERNITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_PATERNITE CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CONGE_PATERNITE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CONGE_PATERNITE_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CLM');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CLM CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CLM_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CLM_SEQ
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CLD CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CLD_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CLD_SEQ
      ';
   end if;
   
   select count(*) into c from user_tables where table_name = upper('CHANGEMENT_POSITION_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CHANGEMENT_POSITION_OLD CASCADE CONSTRAINTS
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('CHANGEMENT_POSITION_OLD_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.CHANGEMENT_POSITION_OLD_SEQ
      ';
   end if;

end;
/

--
-- Redirection de V_PERSONNEL_ACTUEL_VAC_ENS sur le User MANGUE
--
CREATE or replace VIEW V_PERSONNEL_ACTUEL_VAC_ENS AS SELECT c.no_dossier_pers FROM mangue.CONTRAT_VACATAIRES v, mangue.CONTRAT c , grhum.TYPE_CONTRAT_TRAVAIL tct
WHERE c.no_seq_contrat = v.no_seq_contrat
AND v.tem_enseignant='O'
AND (c.d_deb_contrat_trav <= SYSDATE  AND c.d_fin_contrat_trav IS NULL OR c.d_fin_contrat_trav >= SYSDATE)
AND c.c_type_contrat_trav = tct.c_type_contrat_trav
AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
AND tct.TEM_TITULAIRE != 'O';
/

--
-- Redirection de V_PERSONNEL_ACTUEL_NON_ENS sur le User MANGUE
--
CREATE or replace VIEW V_PERSONNEL_ACTUEL_VAC_NON_ENS AS SELECT c.no_dossier_pers FROM mangue.CONTRAT_VACATAIRES v, mangue.CONTRAT c , grhum.TYPE_CONTRAT_TRAVAIL tct
WHERE c.no_seq_contrat = v.no_seq_contrat
AND v.tem_enseignant='N'
AND (c.d_deb_contrat_trav <= SYSDATE  AND c.d_fin_contrat_trav IS NULL OR c.d_fin_contrat_trav >= SYSDATE)
AND c.c_type_contrat_trav = tct.c_type_contrat_trav
AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
AND tct.TEM_TITULAIRE != 'O';
/

COMMIT;
