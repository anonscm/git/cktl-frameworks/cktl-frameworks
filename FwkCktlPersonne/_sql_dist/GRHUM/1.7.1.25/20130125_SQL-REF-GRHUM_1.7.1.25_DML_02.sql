--
-- Patch DML de GRHUM du 25/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 25/01/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- Ajout d'une donnée dans GRHUM.RNE
--
-- ajout adresse pour un établissement
-- http://www.education.gouv.fr/annuaire/71-saone-et-loire/chalon-sur-saone/lycee/lycee-nicephore-niepce.html

-- modification effectuée 
update grhum.rne 
set ll_rne = upper('Lycée Nicéphore Niepce'), adresse = upper('141 avenue Boucicaut - BP 99'), code_postal=71100, ville=upper('Chalon-sur-Saône')
where c_rne='0710012C';


--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.25';


COMMIT;