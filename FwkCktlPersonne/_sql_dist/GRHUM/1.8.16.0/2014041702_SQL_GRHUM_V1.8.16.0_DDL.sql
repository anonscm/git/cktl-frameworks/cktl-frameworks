--
-- Patch DDL de GRHUM du 17/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.16.0
-- Date de publication : 17/04/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.15.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.16.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.16.0 a deja ete passe !');
    end if;

end;
/


---------------------------V20140417.142534__DDL_MAJ_CIR_ET_MINISTERE.sql---------------------------
declare
begin

grhum.adm_add_column('GRHUM', 'PERSONNEL_ULR', 'CIR_D_VERIFICATION', 'DATE', NULL, NULL);
grhum.adm_add_column('GRHUM', 'PERSONNEL_ULR', 'CIR_D_CERTIFICATION', 'DATE', NULL, NULL);
grhum.adm_add_column('GRHUM', 'PERSONNEL_ULR', 'CIR_D_COMPLETUDE', 'DATE', NULL, NULL);
grhum.adm_add_column('GRHUM', 'PERSONNEL_ULR', 'ID_MINISTERE', 'NUMBER', NULL, NULL);
grhum.adm_add_column('GRHUM', 'PERSONNEL_ULR', 'NO_EPICEA', 'VARCHAR2(15)', NULL, NULL);
grhum.adm_add_column('GRHUM', 'CORPS', 'ID_MINISTERE', 'NUMBER', NULL, NULL);

end;
/

ALTER TABLE GRHUM.PERSONNEL_ULR MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX GRHUM.PK_PERSONNEL REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.UK_NO_MATRICULE REBUILD TABLESPACE INDX_GRHUM;


ALTER TABLE GRHUM.CORPS MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX GRHUM.PK_CORPS REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_CORPS_BUR_GES REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_CORPS_CATEGORIE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_CORPS_FILIERE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_CORPS_TYPE_POPUL REBUILD TABLESPACE INDX_GRHUM;


------------------------------V20140417.142726__DDL_TROUVER_LOGIN.sql-------------------------------
create or replace FUNCTION TROUVER_LOGIN
( prenom CHAR,
  nom CHAR )
RETURN CHAR
IS
  login   COMPTE.cpt_login%TYPE;
  lglogin INTEGER;
  nbenr   INTEGER;
  nbmax   INTEGER;
  nbmin   INTEGER;
  i       INTEGER;
BEGIN
-- recuperation des valeurs MAX et MIN, sinon par defaut : 8 et 4
  select nvl(max(decode(param_key,'ANNUAIRE_LOGIN_MAX', param_value, null)),8),
         nvl(max(decode(param_key,'ANNUAIRE_LOGIN_MIN', param_value, null)),4)
         into nbmax, nbmin
   from  grhum.GRHUM_PARAMETRES;

-- construction du login
  login := SUBSTR(LOWER(prenom),1,1) || SUBSTR(LOWER(nom),1,(nbmax-1));
  
-- si trop court il est complete par les caracteres du prenom
-- et, eventuellement, par un certain nombre des zeros
  if length(login) < nbmin
     then
        login := rpad(SUBSTR(LOWER(prenom),1,nbmin - length(SUBSTR(LOWER(nom),1,(nbmax-1)))) || SUBSTR(LOWER(nom),1,(nbmax-1)),nbmin,'0');
  end if;

-- gestion des doublons
  SELECT COUNT(*)
    INTO nbenr
    FROM grhum.COMPTE
   WHERE cpt_login = login;
  
  -- si un doublon est detecte on force deux places ...
  IF (nbenr > 0)
    THEN login := substr(login,1,nbmax-2);
  
    -- ... pour ajouter deux chiffres : de 01 au 99
    i := 0;
    WHILE nbenr > 0
     LOOP
        i := i + 1;
        login := login||ltrim(rtrim(to_char(i,'09')));
        SELECT COUNT(*) INTO nbenr
          FROM grhum.COMPTE
         WHERE cpt_login = login;
     END LOOP;
  END IF;
  RETURN login;
END;
/

