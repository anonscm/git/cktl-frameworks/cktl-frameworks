--
-- Patch DML de GRHUM du 17/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.16.0
-- Date de publication : 17/04/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------------V20140417.142841__DML_MAJ_TYPE_GROUPE.sql------------------------------
-- MAJ du témoin utilisé
UPDATE grhum.type_groupe SET tgrp_tem_util = 'N' WHERE tgrp_code = 'I';

-- MAJ du témoin de visibilité
-- sur Agenda
UPDATE grhum.type_groupe SET tgrp_tem_web = 'O' WHERE tgrp_code = 'AG';

-- sur Annuaire
UPDATE grhum.type_groupe SET tgrp_tem_web = 'O' WHERE tgrp_code = 'A';

-- sur Elus Etudiants
UPDATE grhum.type_groupe SET tgrp_tem_web = 'N' WHERE tgrp_code = 'EL';

-- sur Format d'échange LDAP
UPDATE grhum.type_groupe SET tgrp_tem_web = 'N' WHERE tgrp_code = 'L';

-- sur Forum
UPDATE grhum.type_groupe SET tgrp_tem_web = 'N' WHERE tgrp_code = 'F';

-- sur Responsable prestations créées par DT
UPDATE grhum.type_groupe SET tgrp_tem_web = 'N' WHERE tgrp_code = 'PD';
-------------------------------V20140417.143016__DML_PARAM_PECHE.sql--------------------------------
--Insertion du parametre definissant si l'on doit afficher l'année d'exercice au format d'une année civile ou universitaire 
declare 
    persIdCreateur  integer;
    keyFormatAnneeExercice VARCHAR2(512);
    keyIdParametresType VARCHAR2(512);   
    itemCount integer; 
begin
    -- persIdCreation
    SELECT PERS_ID into persIdCreateur
               FROM GRHUM.COMPTE
        WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES
                                                WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));
                                                
    
    keyFormatAnneeExercice := 'org.cocktail.fwkcktlscolpeda.formatAnneeExercice_AnneeCivile';
	
	SELECT TYPE_ID into keyIdParametresType
		FROM GRHUM.GRHUM_PARAMETRES_TYPE
			WHERE TYPE_ID_INTERNE = 'CODE_ACTIVATION'; 

    select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where  PARAM_KEY = keyFormatAnneeExercice;
    if itemCount =0 then                                           
      INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) 
        values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,keyFormatAnneeExercice,'N','Indique si l''on doit afficher l''année d''exercice au format d''une année civile (ex: 2014 valeur O) ou universitaire (ex: 2014/2015  valeur N)',persIdCreateur,persIdCreateur,sysdate, sysdate, keyIdParametresType);
    end if;
end;
/
---------------------------------V20140417.143038__DML_MAJ_RNE.sql----------------------------------
--------------------------------------------------------
--  RNE fourni par l'UPPA
--------------------------------------------------------

-- Collège d'ASPE

-- http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640016V
 UPDATE GRHUM.RNE
 	SET ville = 'BEDOUS',
 		tetab_code = 'CLG',
 		lc_rne = 'CLG ASPE',
 		adresse = 'ROUTE D''ESPAGNE BP 14'
 	WHERE c_rne = '0640016V';
 
 
 
--------------------------------------------------
-- MAJ pour les établissements du département 40 
--------------------------------------------------

--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0401025X
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400093J
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400097N
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400751Z
update grhum.rne set ville='MORCENX' where c_rne in ('0401025X','0400093J','0400097N','0400751Z');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400034V
update grhum.rne set ville='ROQUEFORT' where c_rne = '0400034V';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400026L
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400046H
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400056U
update grhum.rne set ville='PARENTIS EN BORS' where c_rne in ('0400026L','0400046H','0400056U');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400750Y
update grhum.rne set ville='HEUGAS' where c_rne = '0400750Y';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400780F
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0401007C
update grhum.rne set ville='SEYRESSE' where c_rne in ('0400780F','0401007C');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0401011G
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400898J
update grhum.rne set ville='OEYRELUY' where c_rne in ('0401011G','0400898J');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0401005A
update grhum.rne set ville='DAX' where c_rne = '0401005A';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400783J
update grhum.rne set ville='ST PANDELON' where c_rne = '0400783J';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400062A
update grhum.rne set ville='MIMIZAN' where c_rne = '0400062A';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400930U
update grhum.rne set ville='LABOUHEYRE' where c_rne = '0400930U';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400059X
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0401013J
update grhum.rne set ville='ST VINCENT DE TYROSSE' where c_rne in ( '0400059X','0401013J');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400784K
update grhum.rne set ville='SAUBRIGUES' where c_rne = '0400784K';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400025K
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400139J
update grhum.rne set ville='MUGRON' where c_rne in ('0400025K','0400139J');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400080V
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400103V
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400094K
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400067F
update grhum.rne set ville='ST PIERRE DU MONT' where c_rne in ('0400080V','0400103V','0400094K','0400067F');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400883T
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400072L


-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400010U
update grhum.rne set ville='GABARRET' where c_rne in( '0400883T','0400072L','0400010U');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400011V
update grhum.rne set ville='GEAUNE' where c_rne = '0400011V';
 
 
 
 
 -- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400077S
update grhum.rne set ville='TARTAS' where c_rne = '0400077S';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400075P
update grhum.rne set ville='ST SEVER' where c_rne = '0400075P';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400141L
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400918F
update grhum.rne set ville='SABRES' where c_rne in('0400141L','0400918F');


-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400068G


-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400916D
update grhum.rne set ville='ST VINCENT DE PAUL' where c_rne in('0400068G','0400916D');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0400096M
update grhum.rne set ville='ST PAUL LES DAX' where c_rne = '0400096M';


--------------------------------------------------
-- MAJ pour les établissements du département 64 
--------------------------------------------------

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641405E
update grhum.rne set ville='JURANCON' where c_rne = '0641405E';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641520E
update grhum.rne set ville='DOMEZAIN BERRAUTE' where c_rne = '0641520E';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641927X
update grhum.rne set ville='ETCHARRY' where c_rne = '0641927X';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641545G
update grhum.rne set ville='ST PALAIS' where c_rne = '0641545G';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640040W
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641948V
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640124M
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641656C
update grhum.rne set ville='MAULEON SOULE' where c_rne in ('0640040W','0641948V','0640124M','0641656C');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641538Z
update grhum.rne set ville='BERROGAIN LARUNS' where c_rne = '0641538Z';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640042Y
update grhum.rne set ville='MORLAAS' where c_rne = '0640042Y';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641408H
update grhum.rne set ville='ANDAUX' where c_rne = '0641408H';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640155W
update grhum.rne set ville='NAVARRENX' where c_rne = '0640155W';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0642007J
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640146L
update grhum.rne set ville='BIARRITZ' where c_rne in ('0642007J','0640146L');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640165G
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641542D
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640069C
update grhum.rne set ville='ST JEAN PIED DE PORT' where c_rne in ('0640165G','0641542D','0640069C');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640121J
update grhum.rne set ville='LESCAR' where c_rne  = '0640121J';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641913G
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640181Z
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641535W
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640119G
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641752G
update grhum.rne set ville='HASPARREN' where c_rne in ('0641913G','0640181Z','0641535W','0640119G','0641752G');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641926W
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641702C
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640147M
update grhum.rne set ville='CAMBO LES BAINS' where c_rne in ('0641926W','0641702C','0640147M');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640170M
update grhum.rne set ville='SALIES DE BEARN' where c_rne  = '0640170M';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640036S
update grhum.rne set ville='LASSEUBE' where c_rne  = '0640036S';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641659F
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640253C
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640128S
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641594K
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640127R
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640086W
update grhum.rne set ville='ORTHEZ' where c_rne in ('0641659F','0640253C','0640128S','0641594K','0640127R','0640086W');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640169L
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641547J
update grhum.rne set ville='ST PEE SUR NIVELLE' where c_rne in ('0640169L','0641547J');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0642036R
update grhum.rne set ville='BIZANOS' where c_rne  = '0642036R';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640171N
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641933D
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641548K
update grhum.rne set ville='SAUVETERRE DE BEARN' where c_rne in ('0640171N','0641933D','0641548K');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640126P
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641658E
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640106T
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640087X
update grhum.rne set ville='OLORON STE MARIE' where c_rne in ('0640126P','0641658E','0640106T','0640087X');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640008L
update grhum.rne set ville='ARZACQ ARRAZIGUET' where c_rne  = '0640008L';



-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640162D
update grhum.rne set ville='ST ETIENNE DE BAIGORRY' where c_rne  = '0640162D';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640074H
update grhum.rne set ville='TARDETS SORHOLUS' where c_rne  = '0640074H';
 
 
 
-- RNE  suite et fin 64, 65, 40

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641705F
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640137B
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641874P
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640136A
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640108V
update grhum.rne set ville='USTARITZ' where c_rne in ('0641705F','0640137B','0641874P','0640136A','0640108V','');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640134Y
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641661H
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640163E
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641945S
update grhum.rne set ville='ST JEAN DE LUZ' where c_rne in ('0640134Y','0641661H','0640163E','0641945S');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641409J
update grhum.rne set ville='ASSAT' where c_rne = '0641409J';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640161C
update grhum.rne set ville='PONTACQ' where c_rne = '0640161C';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640004G
update grhum.rne set ville='ARETTE' where c_rne = '0640004G';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0641928Y
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0640151S
update grhum.rne set ville='HENDAYE' where c_rne in ( '0641928Y','0640151S');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650877A
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650058K
update grhum.rne set ville='LOURDES' where c_rne in ( '0650877A','0650058K');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650018S
update grhum.rne set ville='LUZ ST SAUVEUR' where c_rne = '0650018S';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0651024K
update grhum.rne set ville='CANTAOUS' where c_rne = '0651024K';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650022W
update grhum.rne set ville='ST LAURENT DE NESTE' where c_rne = '0650022W';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650036L
update grhum.rne set ville='TOURNAY' where c_rne = '0650036L';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650787C
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650738Z
update grhum.rne set ville='BAGNERES DE BIGORRE' where c_rne in ( '0650787C','0650738Z');

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650037M
update grhum.rne set ville='TRIE SUR BAISE' where c_rne = '0650037M';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650003A
update grhum.rne set ville='ARREAU' where c_rne = '0650003A';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650020U
update grhum.rne set ville='PIERREFITTE NESTALAS' where c_rne = '0650020U';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650950E
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0651021G
-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650054F
update grhum.rne set ville='LANNEMEZAN' where c_rne in ( '0650950E','0651021G','0650054F');



-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650088T
update grhum.rne set ville='SEMEAC' where c_rne = '0650088T';

-- 
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0650019T
update grhum.rne set ville='MAUBOURGUET' where c_rne = '0650019T';
 
 
--------------------------------------------------------
--  RNE DT de l'ENS Cachan
--------------------------------------------------------
 
 --0750654D	HENRI IV
 UPDATE GRHUM.RNE
 	SET lc_rne = 'LG HENRI IV',
 		tetab_code = 'LG',
 		d_deb_val = to_date('01/05/65','DD/MM/RR')
 	WHERE c_rne = '0750654D';
 	
--0750655E	LOUIS LE GRAND
 UPDATE GRHUM.RNE
 	SET lc_rne = 'LG LOUIS LE GRAND',
 		tetab_code = 'LG',
 		d_deb_val = to_date('01/05/65','DD/MM/RR')
 	WHERE c_rne = '0750655E';
 
 	

--------------------------------------------------------
--  RNE fourni par Bordeaux
--------------------------------------------------------

-----------------------------------
-----------------------------------
--MAJ de RNE

--0331766R UNIVERSITE BORDEAUX MONTAIGNE
UPDATE GRHUM.RNE
 	SET ll_rne = 'UNIVERSITE BORDEAUX MONTAIGNE',
 		lc_rne = 'U BORDEAUX 3'
 	WHERE c_rne = '0331766R';
 	
--0331420P IUT BORDEAUX MONTAIGNE
UPDATE GRHUM.RNE
 	SET ll_rne = 'IUT BORDEAUX MONTAIGNE',
 		lc_rne = 'IUT BORDEAUX 3',
 		adresse = '1 RUE JACQUES ELLUL',
 		code_postal = '33080',
 		ville = 'BORDEAUX'
 	WHERE c_rne = '0331420P';




--0333178A COMMUNAUTE D'UNIVERSITES ET D'ETABLISSEMENTS D'AQUITAINE
UPDATE GRHUM.RNE
 	SET ll_rne = 'COMMUNAUTE D''UNIVERSITES ET D''ETABLISSEMENTS D''AQUITAINE',
 		lc_rne = 'COMMM UNIV ETAB',
 		d_deb_val = to_date('01/09/07','DD/MM/RR'),
 		adresse = '166 COURS DE L ARGONNE',
 		code_postal = '33000'
 	WHERE c_rne = '0333178A';


-----------------------------------
----------------------------------- 	
-- AJOUT d'un RNE Université de Bordeaux
DECLARE
	compteur integer;
BEGIN		
	SELECT count(*) into compteur
	FROM GRHUM.RNE
	WHERE c_rne = '0333298F';
	
	IF compteur = 0 THEN                                           
      Insert into GRHUM.RNE (C_RNE,LL_RNE,ADRESSE,CODE_POSTAL,C_RNE_PERE,D_DEB_VAL,D_FIN_VAL,D_CREATION,D_MODIFICATION,LC_RNE,ACAD_CODE,ETAB_STATUT,VILLE,SIRET)
		values ('0333298F','UNIVERSITE DE BORDEAUX','35 PLACE PEY BERLAND','33000',null,
			to_date('01/01/14','DD/MM/RR'),null,to_date('25/03/14','DD/MM/RR'),to_date('25/03/14','DD/MM/RR'),'UNIV BDX','004','PU','BORDEAUX','13001835100010');
			
    END IF;


END;
/
			
			
-----------------------------------
-- Demande de l'UHA
----------------------------------- 
-- UAI Agrosup Dijon: 0212198A
--http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0212198A
	
DECLARE
	compteur integer;
BEGIN		
	SELECT count(*) into compteur
	FROM GRHUM.RNE
	WHERE c_rne = '0212198A';
	
	IF compteur = 0 THEN                                           
      Insert into GRHUM.RNE (C_RNE,LL_RNE,ADRESSE,CODE_POSTAL,C_RNE_PERE,D_DEB_VAL,D_FIN_VAL,D_CREATION,D_MODIFICATION,LC_RNE,ACAD_CODE,ETAB_STATUT,VILLE,SIRET)
		values ('0212198A','AGROSUP DIJON','26 BOULEVARD DR PETITJEAN','21079',null,
			to_date('01/03/2009','DD/MM/RR'),null,to_date('01/03/2009','DD/MM/RR'),to_date('01/12/2010','DD/MM/RR'),'AGROSUP DIJON','007','PU','DIJON','13000604200019');
			
    END IF;


END;
/
---------------------------------V20140417.143101__DML_MAJ_ROME.sql---------------------------------
-- CODES ROME

Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('662','A1101','Conduite d''engins d''exploitation agricole et forestière',null,null,sysdate,sysdate,'2');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('132','A1201','Bûcheronnage et élagage',null,null,sysdate,sysdate,'3');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('133','A1202','Entretien des espaces naturels',null,null,sysdate,sysdate,'3');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('134','A1203','Entretien des espaces verts',null,null,sysdate,sysdate,'3');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('135','A1204','Protection du patrimoine naturel',null,null,sysdate,sysdate,'3');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('136','A1205','Sylviculture',null,null,sysdate,sysdate,'3');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('137','A1301','Conseil et assistance technique en agriculture',null,null,sysdate,sysdate,'4');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('138','A1302','Contrôle et diagnostic technique en agriculture',null,null,sysdate,sysdate,'4');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('139','A1303','Ingénierie en agriculture et environnement naturel',null,null,sysdate,sysdate,'4');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('140','A1401','Aide agricole de production fruitière ou viticole',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('141','A1402','Aide agricole de production légumière ou végétale',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('142','A1403','Aide d''élevage agricole et aquacole',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('143','A1404','Aquaculture',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('144','A1405','Arboriculture et viticulture',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('145','A1406','Encadrement équipage de la pêche',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('146','A1407','Élevage bovin ou équin',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('147','A1408','Élevage d''animaux sauvages ou de compagnie',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('148','A1409','Élevage de lapins et volailles',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('149','A1410','Élevage ovin ou caprin',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('150','A1411','Élevage porcin',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('151','A1412','Fabrication et affinage de fromages',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('152','A1413','Fermentation de boissons alcoolisées',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('153','A1414','Horticulture et maraîchage',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('154','A1415','Equipage de la pêche',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('155','A1416','Polyculture, élevage',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('156','A1417','Saliculture',null,null,sysdate,sysdate,'5');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('157','A1501','Aide aux soins animaux',null,null,sysdate,sysdate,'6');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('158','A1502','Podologie animale',null,null,sysdate,sysdate,'6');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('159','A1503','Toilettage des animaux',null,null,sysdate,sysdate,'6');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('160','A1504','Santé animale',null,null,sysdate,sysdate,'6');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('161','B1101','Création en arts plastiques',null,null,sysdate,sysdate,'8');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('162','B1201','Réalisation d''objets décoratifs et utilitaires en céramique et matériaux de synthèse',null,null,sysdate,sysdate,'9');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('163','B1301','Décoration d''espaces de vente',null,null,sysdate,sysdate,'10');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('164','B1302','Décoration d''objets d''art et artisanaux',null,null,sysdate,sysdate,'10');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('165','B1303','Gravure - ciselure',null,null,sysdate,sysdate,'10');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('166','B1401','Réalisation d''objets en lianes, fibres et brins végétaux',null,null,sysdate,sysdate,'11');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('167','B1402','Reliure et restauration de livres et archives',null,null,sysdate,sysdate,'11');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('168','B1501','Fabrication et réparation d''instruments de musique',null,null,sysdate,sysdate,'12');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('169','B1601','Métallerie d''art',null,null,sysdate,sysdate,'13');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('170','B1602','Réalisation d''objets artistiques et fonctionnels en verre',null,null,sysdate,sysdate,'13');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('171','B1603','Réalisation d''ouvrages en bijouterie, joaillerie et orfèvrerie',null,null,sysdate,sysdate,'13');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('172','B1604','Réparation - montage en systèmes horlogers',null,null,sysdate,sysdate,'13');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('173','B1701','Conservation et reconstitution d''espèces animales',null,null,sysdate,sysdate,'14');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('174','B1801','Réalisation d''articles de chapellerie',null,null,sysdate,sysdate,'15');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('175','B1802','Réalisation d''articles en cuir et matériaux souples (hors vêtement)',null,null,sysdate,sysdate,'15');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('176','B1803','Réalisation de vêtements sur mesure ou en petite série',null,null,sysdate,sysdate,'15');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('177','B1804','Réalisation d''ouvrages d''art en fils',null,null,sysdate,sysdate,'15');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('178','B1805','Stylisme',null,null,sysdate,sysdate,'15');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('179','B1806','Tapisserie - décoration en ameublement',null,null,sysdate,sysdate,'15');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('180','C1101','Conception - développement produits d''assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('181','C1102','Conseil clientèle en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('182','C1103','Courtage en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('183','C1104','Direction d''exploitation en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('184','C1105','Études actuarielles en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('185','C1106','Expertise risques en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('186','C1107','Indemnisations en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('187','C1108','Management de groupe et de service en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('188','C1109','Rédaction et gestion en assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('189','C1110','Souscription d''assurances',null,null,sysdate,sysdate,'17');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('190','C1201','Accueil et services bancaires',null,null,sysdate,sysdate,'18');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('191','C1202','Analyse de crédits et risques bancaires',null,null,sysdate,sysdate,'18');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('192','C1203','Relation clients banque/finance',null,null,sysdate,sysdate,'18');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('193','C1204','Conception et expertise produits bancaires et financiers',null,null,sysdate,sysdate,'18');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('194','C1205','Conseil en gestion de patrimoine financier',null,null,sysdate,sysdate,'18');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('195','C1206','Gestion de clientèle bancaire',null,null,sysdate,sysdate,'18');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('196','C1207','Management en exploitation bancaire',null,null,sysdate,sysdate,'18');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('197','C1301','Front office marchés financiers',null,null,sysdate,sysdate,'19');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('198','C1302','Gestion back et middle-office marchés financiers',null,null,sysdate,sysdate,'19');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('199','C1303','Gestion de portefeuilles sur les marchés financiers',null,null,sysdate,sysdate,'19');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('200','C1401','Gestion en banque et assurance',null,null,sysdate,sysdate,'20');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('201','C1501','Gérance immobilière',null,null,sysdate,sysdate,'21');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('202','C1502','Gestion locative immobilière',null,null,sysdate,sysdate,'21');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('203','C1503','Management de projet immobilier',null,null,sysdate,sysdate,'21');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('204','C1504','Transaction immobilière',null,null,sysdate,sysdate,'21');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('205','D1101','Boucherie',null,null,sysdate,sysdate,'23');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('206','D1102','Boulangerie - viennoiserie',null,null,sysdate,sysdate,'23');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('207','D1103','Charcuterie - traiteur',null,null,sysdate,sysdate,'23');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('208','D1104','Pâtisserie, confiserie, chocolaterie et glacerie',null,null,sysdate,sysdate,'23');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('209','D1105','Poissonnerie',null,null,sysdate,sysdate,'23');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('210','D1106','Vente en alimentation',null,null,sysdate,sysdate,'23');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('211','D1107','Vente en gros de produits frais',null,null,sysdate,sysdate,'23');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('212','D1201','Achat vente d''objets d''art, anciens ou d''occasion',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('213','D1202','Coiffure',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('214','D1203','Hydrothérapie',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('215','D1204','Location de véhicules ou de matériel de loisirs',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('216','D1205','Nettoyage d''articles textiles ou cuirs',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('217','D1206','Réparation d''articles en cuir et matériaux souples',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('218','D1207','Retouches en habillement',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('219','D1208','Soins esthétiques et corporels',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('220','D1209','Vente de végétaux',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('221','D1210','Vente en animalerie',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('222','D1211','Vente en articles de sport et loisirs',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('223','D1212','Vente en décoration et équipement du foyer',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('224','D1213','Vente en gros de matériel et équipement',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('225','D1214','Vente en habillement et accessoires de la personne',null,null,sysdate,sysdate,'24');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('226','D1301','Management  de magasin de détail',null,null,sysdate,sysdate,'25');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('227','D1401','Assistanat commercial',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('228','D1402','Relation commerciale grands comptes et entreprises',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('229','D1403','Relation commerciale auprès de particuliers',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('230','D1404','Relation commerciale en vente de véhicules',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('231','D1405','Conseil en information médicale',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('232','D1406','Management en force de vente',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('233','D1407','Relation technico-commerciale',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('234','D1408','Téléconseil et télévente',null,null,sysdate,sysdate,'26');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('235','D1501','Animation de vente',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('236','D1502','Management/gestion de rayon produits alimentaires',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('237','D1503','Management/gestion de rayon produits non alimentaires',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('238','D1504','Direction de magasin de grande distribution',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('239','D1505','Personnel de caisse',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('240','D1506','Marchandisage',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('241','D1507','Mise en rayon libre-service',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('242','D1508','Encadrement du personnel de caisses',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('243','D1509','Management de département en grande distribution',null,null,sysdate,sysdate,'27');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('244','E1101','Animation de site multimédia',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('245','E1102','Ecriture d''ouvrages, de livres',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('246','E1103','Communication',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('247','E1104','Conception de contenus multimédias',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('248','E1105','Coordination d''édition',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('249','E1106','Journalisme et information média',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('250','E1107','Organisation d''événementiel',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('251','E1108','Traduction, interprétariat',null,null,sysdate,sysdate,'29');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('252','E1201','Photographie',null,null,sysdate,sysdate,'30');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('253','E1202','Production en laboratoire cinématographique',null,null,sysdate,sysdate,'30');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('254','E1203','Production en laboratoire photographique',null,null,sysdate,sysdate,'30');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('255','E1204','Projection cinéma',null,null,sysdate,sysdate,'30');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('256','E1205','Réalisation de contenus multimédias',null,null,sysdate,sysdate,'30');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('257','E1301','Conduite de machines d''impression',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('258','E1302','Conduite de machines de façonnage routage',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('259','E1303','Encadrement des industries graphiques',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('260','E1304','Façonnage et routage',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('261','E1305','Préparation et correction en édition et presse',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('262','E1306','Prépresse',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('263','E1307','Reprographie',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('264','E1308','Intervention technique en industrie graphique',null,null,sysdate,sysdate,'31');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('265','E1401','Développement et promotion publicitaire',null,null,sysdate,sysdate,'32');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('266','E1402','Élaboration de plan média',null,null,sysdate,sysdate,'32');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('267','F1101','Architecture du BTP',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('268','F1102','Conception - aménagement d''espaces intérieurs',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('269','F1103','Contrôle et diagnostic technique du bâtiment',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('270','F1104','Dessin BTP',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('271','F1105','Études géologiques',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('272','F1106','Ingénierie et études du BTP',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('273','F1107','Mesures topographiques',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('274','F1108','Métré de la construction',null,null,sysdate,sysdate,'34');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('275','F1201','Conduite de travaux du BTP',null,null,sysdate,sysdate,'35');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('276','F1202','Direction de chantier du BTP',null,null,sysdate,sysdate,'35');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('277','F1203','Direction et ingénierie d''exploitation de gisements et de carrières',null,null,sysdate,sysdate,'35');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('278','F1204','Sécurité et protection santé du BTP',null,null,sysdate,sysdate,'35');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('279','F1301','Conduite de grue',null,null,sysdate,sysdate,'36');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('280','F1302','Conduite d''engins de terrassement et de carrière',null,null,sysdate,sysdate,'36');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('281','F1401','Extraction liquide et gazeuse',null,null,sysdate,sysdate,'37');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('282','F1402','Extraction solide',null,null,sysdate,sysdate,'37');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('283','F1501','Montage de structures et de charpentes bois',null,null,sysdate,sysdate,'38');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('284','F1502','Montage de structures métalliques',null,null,sysdate,sysdate,'38');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('285','F1503','Réalisation - installation d''ossatures bois',null,null,sysdate,sysdate,'38');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('286','F1601','Application et décoration en plâtre, stuc et staff',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('287','F1602','Électricité bâtiment',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('288','F1603','Installation d''équipements sanitaires et thermiques',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('289','F1604','Montage d''agencements',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('290','F1605','Montage de réseaux électriques et télécoms',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('291','F1606','Peinture en bâtiment',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('292','F1607','Pose de fermetures menuisées',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('293','F1608','Pose de revêtements rigides',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('294','F1609','Pose de revêtements souples',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('295','F1610','Pose et restauration de couvertures',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('296','F1611','Réalisation et restauration de façades',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('297','F1612','Taille et décoration de pierres',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('298','F1613','Travaux d''étanchéité et d''isolation',null,null,sysdate,sysdate,'39');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('299','F1701','Construction en béton',null,null,sysdate,sysdate,'40');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('300','F1702','Construction de routes et voies',null,null,sysdate,sysdate,'40');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('301','F1703','Maçonnerie',null,null,sysdate,sysdate,'40');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('302','F1704','Préparation du gros œuvre et des travaux publics',null,null,sysdate,sysdate,'40');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('303','F1705','Pose de canalisations',null,null,sysdate,sysdate,'40');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('304','F1706','Préfabrication en béton industriel',null,null,sysdate,sysdate,'40');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('305','G1101','Accueil touristique',null,null,sysdate,sysdate,'42');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('306','G1102','Promotion du tourisme local',null,null,sysdate,sysdate,'42');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('307','G1201','Accompagnement de voyages, d''activités culturelles ou sportives',null,null,sysdate,sysdate,'43');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('308','G1202','Animation d''activités culturelles ou ludiques',null,null,sysdate,sysdate,'43');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('309','G1203','Animation de loisirs auprès d''enfants ou d''adolescents',null,null,sysdate,sysdate,'43');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('310','G1204','Éducation en activités sportives',null,null,sysdate,sysdate,'43');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('311','G1205','Personnel d''attractions ou de structures de loisirs',null,null,sysdate,sysdate,'43');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('312','G1206','Personnel technique des jeux',null,null,sysdate,sysdate,'43');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('313','G1301','Conception de produits touristiques',null,null,sysdate,sysdate,'44');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('314','G1302','Optimisation de produits touristiques',null,null,sysdate,sysdate,'44');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('315','G1303','Vente de voyages',null,null,sysdate,sysdate,'44');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('316','G1401','Assistance de direction d''hôtel-restaurant',null,null,sysdate,sysdate,'45');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('317','G1402','Management d''hôtel-restaurant',null,null,sysdate,sysdate,'45');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('318','G1403','Gestion de structure de loisirs ou d''hébergement touristique',null,null,sysdate,sysdate,'45');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('319','G1404','Management d''établissement de restauration collective',null,null,sysdate,sysdate,'45');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('320','G1501','Personnel d''étage',null,null,sysdate,sysdate,'46');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('321','G1502','Personnel polyvalent d''hôtellerie',null,null,sysdate,sysdate,'46');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('322','G1503','Management du personnel d''étage',null,null,sysdate,sysdate,'46');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('323','G1601','Management du personnel de cuisine',null,null,sysdate,sysdate,'47');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('324','G1602','Personnel de cuisine',null,null,sysdate,sysdate,'47');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('325','G1603','Personnel polyvalent en restauration',null,null,sysdate,sysdate,'47');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('326','G1604','Fabrication de crêpes ou pizzas',null,null,sysdate,sysdate,'47');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('327','G1605','Plonge en restauration',null,null,sysdate,sysdate,'47');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('328','G1701','Conciergerie en hôtellerie',null,null,sysdate,sysdate,'48');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('329','G1702','Personnel du hall',null,null,sysdate,sysdate,'48');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('330','G1703','Réception en hôtellerie',null,null,sysdate,sysdate,'48');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('331','G1801','Café, bar brasserie',null,null,sysdate,sysdate,'49');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('332','G1802','Management du service en restauration',null,null,sysdate,sysdate,'49');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('333','G1803','Service en restauration',null,null,sysdate,sysdate,'49');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('334','G1804','Sommellerie',null,null,sysdate,sysdate,'49');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('335','H1101','Assistance et support technique client',null,null,sysdate,sysdate,'52');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('336','H1102','Management et ingénierie d''affaires',null,null,sysdate,sysdate,'52');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('337','H1201','Expertise technique couleur en industrie',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('338','H1202','Conception et dessin de produits électriques et électroniques',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('339','H1203','Conception et dessin produits mécaniques',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('340','H1204','Design industriel',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('341','H1205','Études - modèles en industrie des matériaux souples',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('342','H1206','Management et ingénierie études, recherche et développement industriel',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('343','H1207','Rédaction technique',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('344','H1208','Intervention technique en études et conception en automatisme',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('345','H1209','Intervention technique en études et développement électronique',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('346','H1210','Intervention technique en études, recherche et développement',null,null,sysdate,sysdate,'53');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('347','H1301','Inspection de conformité',null,null,sysdate,sysdate,'54');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('348','H1302','Management et ingénierie Hygiène Sécurité Environnement -HSE- industriels',null,null,sysdate,sysdate,'54');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('349','H1303','Intervention technique en Hygiène Sécurité Environnement  -HSE- industriel',null,null,sysdate,sysdate,'54');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('350','H1401','Management et ingénierie gestion industrielle et logistique',null,null,sysdate,sysdate,'55');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('351','H1402','Management et ingénierie méthodes et industrialisation',null,null,sysdate,sysdate,'55');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('352','H1403','Intervention technique en gestion industrielle et logistique',null,null,sysdate,sysdate,'55');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('353','H1404','Intervention technique en méthodes et industrialisation',null,null,sysdate,sysdate,'55');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('354','H1501','Direction de laboratoire d''analyse industrielle',null,null,sysdate,sysdate,'56');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('355','H1502','Management et ingénierie qualité industrielle',null,null,sysdate,sysdate,'56');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('356','H1503','Intervention technique en laboratoire d''analyse industrielle',null,null,sysdate,sysdate,'56');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('357','H1504','Intervention technique en contrôle essai qualité en électricité et électronique',null,null,sysdate,sysdate,'56');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('358','H1505','Intervention technique en formulation et analyse sensorielle',null,null,sysdate,sysdate,'56');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('359','H1506','Intervention technique qualité en mécanique et travail des métaux',null,null,sysdate,sysdate,'56');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('360','H2101','Abattage et découpe des viandes',null,null,sysdate,sysdate,'58');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('361','H2102','Conduite d''équipement de production alimentaire',null,null,sysdate,sysdate,'58');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('362','H2201','Assemblage d''ouvrages en bois',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('363','H2202','Conduite d''équipement de fabrication de l''ameublement et du bois',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('364','H2203','Conduite d''installation de production de panneaux bois',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('365','H2204','Encadrement des industries de l''ameublement et du bois',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('366','H2205','Première transformation de bois d''œuvre',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('367','H2206','Réalisation de menuiserie bois et tonnellerie',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('368','H2207','Réalisation de meubles en bois',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('369','H2208','Réalisation d''ouvrages décoratifs en bois',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('370','H2209','Intervention technique en ameublement et bois',null,null,sysdate,sysdate,'59');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('371','H2301','Conduite d''équipement de production chimique ou pharmaceutique',null,null,sysdate,sysdate,'60');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('372','H2401','Assemblage - montage d''articles en cuirs, peaux',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('373','H2402','Assemblage - montage de vêtements et produits textiles',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('374','H2403','Conduite de machine de fabrication de produits textiles',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('375','H2404','Conduite de machine de production et transformation des fils',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('376','H2405','Conduite de machine de textiles nontissés',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('377','H2406','Conduite de machine de traitement textile',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('378','H2407','Conduite de machine de transformation et de finition des cuirs et peaux',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('379','H2408','Conduite de machine d''impression textile',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('380','H2409','Coupe cuir, textile et matériaux souples',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('381','H2410','Mise en forme, repassage et finitions en industrie textile',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('382','H2411','Montage de prototype cuir et matériaux souples',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('383','H2412','Patronnage - gradation',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('384','H2413','Préparation de fils, montage de métiers textiles',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('385','H2414','Préparation et finition d''articles en cuir et matériaux souples',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('386','H2415','Contrôle en industrie du cuir et du textile',null,null,sysdate,sysdate,'61');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('387','H2501','Encadrement de production de matériel électrique et électronique',null,null,sysdate,sysdate,'62');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('388','H2502','Management et ingénierie de production',null,null,sysdate,sysdate,'62');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('389','H2503','Pilotage d''unité élémentaire de production mécanique',null,null,sysdate,sysdate,'62');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('390','H2504','Encadrement d''équipe en industrie de transformation',null,null,sysdate,sysdate,'62');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('391','H2505','Encadrement d''équipe ou d''atelier en matériaux souples',null,null,sysdate,sysdate,'62');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('392','H2601','Bobinage électrique',null,null,sysdate,sysdate,'63');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('393','H2602','Câblage électrique et électromécanique',null,null,sysdate,sysdate,'63');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('394','H2603','Conduite d''installation automatisée de production électrique, électronique et microélectronique',null,null,sysdate,sysdate,'63');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('395','H2604','Montage de produits électriques et électroniques',null,null,sysdate,sysdate,'63');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('396','H2605','Montage et câblage électronique',null,null,sysdate,sysdate,'63');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('397','H2701','Pilotage d''installation énergétique et pétrochimique',null,null,sysdate,sysdate,'64');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('398','H2801','Conduite d''équipement de transformation du verre',null,null,sysdate,sysdate,'65');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('399','H2802','Conduite d''installation de production de matériaux de construction',null,null,sysdate,sysdate,'65');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('400','H2803','Façonnage et émaillage en industrie céramique',null,null,sysdate,sysdate,'65');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('401','H2804','Pilotage de centrale à béton prêt à l''emploi, ciment, enrobés et granulats',null,null,sysdate,sysdate,'65');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('402','H2805','Pilotage d''installation de production verrière',null,null,sysdate,sysdate,'65');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('403','H2901','Ajustement et montage de fabrication',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('404','H2902','Chaudronnerie - tôlerie',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('405','H2903','Conduite d''équipement d''usinage',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('406','H2904','Conduite d''équipement de déformation des métaux',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('407','H2905','Conduite d''équipement de formage et découpage des matériaux',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('408','H2906','Conduite d''installation automatisée ou robotisée de fabrication mécanique',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('409','H2907','Conduite d''installation de production des métaux',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('410','H2908','Modelage de matériaux non métalliques',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('411','H2909','Montage-assemblage mécanique',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('412','H2910','Moulage sable',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('413','H2911','Réalisation de structures métalliques',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('414','H2912','Réglage d''équipement de production industrielle',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('415','H2913','Soudage manuel',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('416','H2914','Réalisation et montage en tuyauterie',null,null,sysdate,sysdate,'66');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('417','H3101','Conduite d''équipement de fabrication de papier ou de carton',null,null,sysdate,sysdate,'67');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('418','H3102','Conduite d''installation de pâte à papier',null,null,sysdate,sysdate,'67');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('419','H3201','Conduite d''équipement de formage des plastiques et caoutchoucs',null,null,sysdate,sysdate,'68');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('420','H3202','Réglage d''équipement de formage des plastiques et caoutchoucs',null,null,sysdate,sysdate,'68');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('421','H3203','Fabrication de pièces en matériaux composites',null,null,sysdate,sysdate,'68');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('422','H3301','Conduite d''équipement de conditionnement',null,null,sysdate,sysdate,'69');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('423','H3302','Opérations manuelles d''assemblage, tri ou emballage',null,null,sysdate,sysdate,'69');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('424','H3303','Préparation de matières et produits industriels (broyage, mélange, ¿)',null,null,sysdate,sysdate,'69');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('425','H3401','Conduite de traitement d''abrasion de surface',null,null,sysdate,sysdate,'70');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('426','H3402','Conduite de traitement par dépôt de surface',null,null,sysdate,sysdate,'70');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('427','H3403','Conduite de traitement thermique',null,null,sysdate,sysdate,'70');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('428','H3404','Peinture industrielle',null,null,sysdate,sysdate,'70');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('429','I1101','Direction et ingénierie en entretien infrastructure et bâti',null,null,sysdate,sysdate,'72');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('430','I1102','Management et ingénierie de maintenance industrielle',null,null,sysdate,sysdate,'72');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('431','I1103','Supervision d''entretien et gestion de véhicules',null,null,sysdate,sysdate,'72');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('432','I1201','Entretien d''affichage et mobilier urbain',null,null,sysdate,sysdate,'73');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('433','I1202','Entretien et surveillance du tracé routier',null,null,sysdate,sysdate,'73');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('434','I1203','Maintenance des bâtiments et des locaux',null,null,sysdate,sysdate,'73');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('435','I1301','Installation et maintenance d''ascenseurs',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('436','I1302','Installation et maintenance d''automatismes',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('437','I1303','Installation et maintenance de distributeurs automatiques',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('438','I1304','Installation et maintenance d''équipements industriels et d''exploitation',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('439','I1305','Installation et maintenance électronique',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('440','I1306','Installation et maintenance en froid, conditionnement d''air',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('441','I1307','Installation et maintenance télécoms et courants faibles',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('442','I1308','Maintenance d''installation de chauffage',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('443','I1309','Maintenance électrique',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('444','I1310','Maintenance mécanique industrielle',null,null,sysdate,sysdate,'74');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('445','I1401','Maintenance informatique et bureautique',null,null,sysdate,sysdate,'75');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('446','I1402','Réparation de biens électrodomestiques',null,null,sysdate,sysdate,'75');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('447','I1501','Intervention en grande hauteur',null,null,sysdate,sysdate,'76');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('448','I1502','Intervention en milieu subaquatique',null,null,sysdate,sysdate,'76');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('449','I1503','Intervention en milieux et produits nocifs',null,null,sysdate,sysdate,'76');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('450','I1601','Installation et maintenance en nautisme',null,null,sysdate,sysdate,'77');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('451','I1602','Maintenance d''aéronefs',null,null,sysdate,sysdate,'77');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('452','I1603','Maintenance d''engins de chantier, levage, manutention et de machines agricoles',null,null,sysdate,sysdate,'77');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('453','I1604','Mécanique automobile',null,null,sysdate,sysdate,'77');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('454','I1605','Mécanique de marine',null,null,sysdate,sysdate,'77');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('455','I1606','Réparation de carrosserie',null,null,sysdate,sysdate,'77');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('456','I1607','Réparation de cycles, motocycles et motoculteurs de loisirs',null,null,sysdate,sysdate,'77');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('457','J1101','Médecine de prévention',null,null,sysdate,sysdate,'79');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('458','J1102','Médecine généraliste et spécialisée',null,null,sysdate,sysdate,'79');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('459','J1103','Médecine dentaire',null,null,sysdate,sysdate,'79');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('460','J1104','Suivi de la grossesse et de l''accouchement',null,null,sysdate,sysdate,'79');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('461','J1201','Biologie médicale',null,null,sysdate,sysdate,'80');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('462','J1202','Pharmacie',null,null,sysdate,sysdate,'80');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('463','J1301','Personnel polyvalent des services hospitaliers',null,null,sysdate,sysdate,'81');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('464','J1302','Analyses médicales',null,null,sysdate,sysdate,'81');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('465','J1303','Assistance médico-technique',null,null,sysdate,sysdate,'81');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('466','J1304','Aide en puériculture',null,null,sysdate,sysdate,'81');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('467','J1305','Conduite de véhicules sanitaires',null,null,sysdate,sysdate,'81');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('468','J1306','Imagerie médicale',null,null,sysdate,sysdate,'81');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('469','J1307','Préparation en pharmacie',null,null,sysdate,sysdate,'81');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('470','J1401','Audioprothèses',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('471','J1402','Diététique',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('472','J1403','Ergothérapie',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('473','J1404','Kinésithérapie',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('474','J1405','Optique - lunetterie',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('475','J1406','Orthophonie',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('476','J1407','Orthoptique',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('477','J1408','Ostéopathie et chiropraxie',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('478','J1409','Pédicurie et podologie',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('479','J1410','Prothèses dentaires',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('480','J1411','Prothèses et orthèses',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('481','J1412','Rééducation en psychomotricité',null,null,sysdate,sysdate,'82');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('482','J1501','Soins d''hygiène, de confort du patient',null,null,sysdate,sysdate,'83');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('483','J1502','Coordination de services médicaux ou paramédicaux',null,null,sysdate,sysdate,'83');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('484','J1503','Soins infirmiers spécialisés en anesthésie',null,null,sysdate,sysdate,'83');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('485','J1504','Soins infirmiers spécialisés en bloc opératoire',null,null,sysdate,sysdate,'83');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('486','J1505','Soins infirmiers spécialisés en prévention',null,null,sysdate,sysdate,'83');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('487','J1506','Soins infirmiers généralistes',null,null,sysdate,sysdate,'83');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('488','J1507','Soins infirmiers spécialisés en puériculture',null,null,sysdate,sysdate,'83');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('489','K1101','Accompagnement et médiation familiale',null,null,sysdate,sysdate,'85');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('490','K1102','Aide aux bénéficiaires d''une mesure de protection juridique',null,null,sysdate,sysdate,'85');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('491','K1103','Développement personnel et bien-être de la personne',null,null,sysdate,sysdate,'85');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('492','K1104','Psychologie',null,null,sysdate,sysdate,'85');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('493','K1201','Action sociale',null,null,sysdate,sysdate,'86');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('494','K1202','Éducation de jeunes enfants',null,null,sysdate,sysdate,'86');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('495','K1203','Encadrement technique en insertion professionnelle',null,null,sysdate,sysdate,'86');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('496','K1204','Facilitation de la vie sociale',null,null,sysdate,sysdate,'86');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('497','K1205','Information et médiation sociale',null,null,sysdate,sysdate,'86');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('498','K1206','Intervention socioculturelle',null,null,sysdate,sysdate,'86');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('499','K1207','Intervention socioéducative',null,null,sysdate,sysdate,'86');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('500','K1301','Accompagnement médicosocial',null,null,sysdate,sysdate,'87');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('501','K1302','Assistance auprès d''adultes',null,null,sysdate,sysdate,'87');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('502','K1303','Assistance auprès d''enfants',null,null,sysdate,sysdate,'87');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('503','K1304','Services domestiques',null,null,sysdate,sysdate,'87');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('504','K1305','Intervention sociale et familiale',null,null,sysdate,sysdate,'87');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('505','K1401','Conception et pilotage de la politique des pouvoirs publics',null,null,sysdate,sysdate,'88');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('506','K1402','Conseil en Santé Publique',null,null,sysdate,sysdate,'88');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('507','K1403','Management de structure de santé, sociale ou pénitentiaire',null,null,sysdate,sysdate,'88');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('508','K1404','Mise en œuvre et pilotage de la politique des pouvoirs publics',null,null,sysdate,sysdate,'88');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('509','K1405','Représentation de l''Etat sur le territoire national ou international',null,null,sysdate,sysdate,'88');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('510','K1501','Application des règles financières publiques',null,null,sysdate,sysdate,'89');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('511','K1502','Contrôle et inspection des Affaires Sociales',null,null,sysdate,sysdate,'89');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('512','K1503','Contrôle et inspection des impôts',null,null,sysdate,sysdate,'89');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('513','K1504','Contrôle et inspection du Trésor Public',null,null,sysdate,sysdate,'89');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('514','K1505','Protection des consommateurs et contrôle des échanges commerciaux',null,null,sysdate,sysdate,'89');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('515','K1601','Gestion de l''information et de la documentation',null,null,sysdate,sysdate,'90');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('516','K1602','Gestion de patrimoine culturel',null,null,sysdate,sysdate,'90');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('517','K1701','Personnel de la Défense',null,null,sysdate,sysdate,'91');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('518','K1702','Direction de la sécurité civile et des secours',null,null,sysdate,sysdate,'91');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('519','K1703','Direction opérationnelle de la défense',null,null,sysdate,sysdate,'91');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('520','K1704','Management de la sécurité publique',null,null,sysdate,sysdate,'91');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('521','K1705','Sécurité civile et secours',null,null,sysdate,sysdate,'91');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('522','K1706','Sécurité publique',null,null,sysdate,sysdate,'91');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('523','K1707','Surveillance municipale',null,null,sysdate,sysdate,'91');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('524','K1801','Conseil en emploi et insertion socioprofessionnelle',null,null,sysdate,sysdate,'92');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('525','K1802','Développement local',null,null,sysdate,sysdate,'92');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('526','K1901','Aide et médiation judiciaire',null,null,sysdate,sysdate,'93');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('527','K1902','Collaboration juridique',null,null,sysdate,sysdate,'93');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('528','K1903','Défense et conseil juridique',null,null,sysdate,sysdate,'93');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('529','K1904','Magistrature',null,null,sysdate,sysdate,'93');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('530','K2101','Conseil en formation',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('531','K2102','Coordination pédagogique',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('532','K2103','Direction d''établissement et d''enseignement',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('533','K2104','Éducation et surveillance au sein d''établissements d''enseignement',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('534','K2105','Enseignement artistique',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('535','K2106','Enseignement des écoles',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('536','K2107','Enseignement général du second degré',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('537','K2108','Enseignement supérieur',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('538','K2109','Enseignement technique et professionnel',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('539','K2110','Formation en conduite de véhicules',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('540','K2111','Formation professionnelle',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('541','K2112','Orientation scolaire et professionnelle',null,null,sysdate,sysdate,'94');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('542','K2201','Blanchisserie industrielle',null,null,sysdate,sysdate,'95');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('543','K2202','Lavage de vitres',null,null,sysdate,sysdate,'95');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('544','K2203','Management et inspection en propreté de locaux',null,null,sysdate,sysdate,'95');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('545','K2204','Nettoyage de locaux',null,null,sysdate,sysdate,'95');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('546','K2301','Distribution et assainissement d''eau',null,null,sysdate,sysdate,'96');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('547','K2302','Management et inspection en environnement urbain',null,null,sysdate,sysdate,'96');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('548','K2303','Nettoyage des espaces urbains',null,null,sysdate,sysdate,'96');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('549','K2304','Revalorisation de produits industriels',null,null,sysdate,sysdate,'96');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('550','K2305','Salubrité et traitement de nuisibles',null,null,sysdate,sysdate,'96');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('551','K2306','Supervision d''exploitation éco-industrielle',null,null,sysdate,sysdate,'96');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('552','K2401','Recherche en sciences de l''homme et de la société',null,null,sysdate,sysdate,'97');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('553','K2402','Recherche en sciences de l''univers, de la matière et du vivant',null,null,sysdate,sysdate,'97');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('554','K2501','Gardiennage de locaux',null,null,sysdate,sysdate,'98');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('555','K2502','Management de sécurité privée',null,null,sysdate,sysdate,'98');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('556','K2503','Sécurité et surveillance privées',null,null,sysdate,sysdate,'98');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('557','K2601','Conduite d''opérations funéraires',null,null,sysdate,sysdate,'99');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('558','K2602','Conseil en services funéraires',null,null,sysdate,sysdate,'99');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('559','K2603','Thanatopraxie',null,null,sysdate,sysdate,'99');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('560','L1101','Animation musicale et scénique',null,null,sysdate,sysdate,'101');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('561','L1102','Mannequinat et pose artistique',null,null,sysdate,sysdate,'101');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('562','L1103','Présentation de spectacles ou d''émissions',null,null,sysdate,sysdate,'101');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('563','L1201','Danse',null,null,sysdate,sysdate,'102');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('564','L1202','Musique et chant',null,null,sysdate,sysdate,'102');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('565','L1203','Art dramatique',null,null,sysdate,sysdate,'102');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('566','L1204','Arts du cirque et arts visuels',null,null,sysdate,sysdate,'102');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('567','L1301','Mise en scène de spectacles vivants',null,null,sysdate,sysdate,'103');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('568','L1302','Production et administration spectacle, cinéma et audiovisuel',null,null,sysdate,sysdate,'103');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('569','L1303','Promotion d''artistes et de spectacles',null,null,sysdate,sysdate,'103');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('570','L1304','Réalisation cinématographique et audiovisuelle',null,null,sysdate,sysdate,'103');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('571','L1401','Sportif professionnel',null,null,sysdate,sysdate,'104');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('572','L1501','Coiffure et maquillage spectacle',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('573','L1502','Costume et Habillage spectacle',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('574','L1503','Décor et accessoires spectacle',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('575','L1504','Éclairage spectacle',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('576','L1505','Image cinématographique et télévisuelle',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('577','L1506','Machinerie spectacle',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('578','L1507','Montage et post-production',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('579','L1508','Prise de son et sonorisation',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('580','L1509','Régie générale',null,null,sysdate,sysdate,'105');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('581','M1101','Achats',null,null,sysdate,sysdate,'107');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('582','M1102','Direction des achats',null,null,sysdate,sysdate,'107');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('583','M1201','Analyse et ingénierie financière',null,null,sysdate,sysdate,'108');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('584','M1202','Audit et contrôle comptables et financiers',null,null,sysdate,sysdate,'108');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('585','M1203','Comptabilité',null,null,sysdate,sysdate,'108');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('586','M1204','Contrôle de gestion',null,null,sysdate,sysdate,'108');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('587','M1205','Direction administrative et financière',null,null,sysdate,sysdate,'108');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('588','M1206','Management de groupe ou de service comptable',null,null,sysdate,sysdate,'108');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('589','M1207','Trésorerie et financement',null,null,sysdate,sysdate,'108');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('590','M1301','Direction de grande entreprise ou d''établissement public',null,null,sysdate,sysdate,'109');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('591','M1302','Direction de petite ou moyenne entreprise',null,null,sysdate,sysdate,'109');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('592','M1401','Conduite d''enquêtes',null,null,sysdate,sysdate,'110');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('593','M1402','Conseil en organisation et management d''entreprise',null,null,sysdate,sysdate,'110');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('594','M1403','Études et prospectives socio-économiques',null,null,sysdate,sysdate,'110');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('595','M1404','Management et gestion d''enquêtes',null,null,sysdate,sysdate,'110');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('596','M1501','Assistanat en ressources humaines',null,null,sysdate,sysdate,'111');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('597','M1502','Développement des ressources humaines',null,null,sysdate,sysdate,'111');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('598','M1503','Management des ressources humaines',null,null,sysdate,sysdate,'111');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('599','M1601','Accueil et renseignements',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('600','M1602','Opérations administratives',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('601','M1603','Distribution de documents',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('602','M1604','Assistanat de direction',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('603','M1605','Assistanat technique et administratif',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('604','M1606','Saisie de données',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('605','M1607','Secrétariat',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('606','M1608','Secrétariat comptable',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('607','M1609','Secrétariat et assistanat médical ou médico-social',null,null,sysdate,sysdate,'112');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('608','M1701','Administration des ventes',null,null,sysdate,sysdate,'113');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('609','M1702','Analyse de tendance',null,null,sysdate,sysdate,'113');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('610','M1703','Management et gestion  de produit',null,null,sysdate,sysdate,'113');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('611','M1704','Management relation clientèle',null,null,sysdate,sysdate,'113');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('612','M1705','Marketing',null,null,sysdate,sysdate,'113');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('613','M1706','Promotion des ventes',null,null,sysdate,sysdate,'113');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('614','M1707','Stratégie commerciale',null,null,sysdate,sysdate,'113');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('615','M1801','Administration de systèmes d''information',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('616','M1802','Conseil et maîtrise d''ouvrage en systèmes d''information',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('617','M1803','Direction des systèmes d''information',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('618','M1804','Études et développement de réseaux de télécoms',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('619','M1805','Études et développement informatique',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('620','M1806','Expertise et support technique en systèmes d''information',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('621','M1807','Exploitation de systèmes de communication et de commandement',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('622','M1808','Information géographique',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('623','M1809','Information météorologique',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('624','M1810','Production et exploitation de systèmes d''information',null,null,sysdate,sysdate,'114');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('625','N1101','Conduite d''engins de déplacement des charges',null,null,sysdate,sysdate,'117');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('626','N1102','Déménagement',null,null,sysdate,sysdate,'117');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('627','N1103','Magasinage et préparation de commandes',null,null,sysdate,sysdate,'117');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('628','N1104','Manœuvre et conduite d''engins lourds de manutention',null,null,sysdate,sysdate,'117');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('629','N1105','Manutention manuelle de charges',null,null,sysdate,sysdate,'117');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('630','N1201','Affrètement transport',null,null,sysdate,sysdate,'118');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('631','N1202','Gestion des opérations de circulation internationale des marchandises',null,null,sysdate,sysdate,'118');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('632','N1301','Conception et organisation de la chaîne logistique',null,null,sysdate,sysdate,'119');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('633','N1302','Direction de site logistique',null,null,sysdate,sysdate,'119');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('634','N1303','Intervention technique d''exploitation logistique',null,null,sysdate,sysdate,'119');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('635','N2101','Navigation commerciale aérienne',null,null,sysdate,sysdate,'121');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('636','N2102','Pilotage et navigation technique aérienne',null,null,sysdate,sysdate,'121');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('637','N2201','Personnel d''escale aéroportuaire',null,null,sysdate,sysdate,'122');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('638','N2202','Contrôle de la navigation aérienne',null,null,sysdate,sysdate,'122');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('639','N2203','Exploitation des pistes aéroportuaires',null,null,sysdate,sysdate,'122');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('640','N2204','Préparation des vols',null,null,sysdate,sysdate,'122');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('641','N2205','Direction d''escale et exploitation aéroportuaire',null,null,sysdate,sysdate,'122');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('642','N3101','Encadrement de la navigation maritime',null,null,sysdate,sysdate,'124');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('643','N3102','Equipage de la navigation maritime',null,null,sysdate,sysdate,'124');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('644','N3103','Navigation fluviale',null,null,sysdate,sysdate,'124');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('645','N3201','Exploitation des opérations portuaires et du transport maritime',null,null,sysdate,sysdate,'125');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('646','N3202','Exploitation du transport fluvial',null,null,sysdate,sysdate,'125');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('647','N3203','Manutention portuaire',null,null,sysdate,sysdate,'125');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('648','N4101','Conduite de transport de marchandises sur longue distance',null,null,sysdate,sysdate,'127');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('649','N4102','Conduite de transport de particuliers',null,null,sysdate,sysdate,'127');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('650','N4103','Conduite de transport en commun sur route',null,null,sysdate,sysdate,'127');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('651','N4104','Courses et livraisons express',null,null,sysdate,sysdate,'127');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('652','N4105','Conduite et livraison par tournées sur courte distance',null,null,sysdate,sysdate,'127');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('653','N4201','Direction d''exploitation des transports routiers de marchandises',null,null,sysdate,sysdate,'128');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('654','N4202','Direction d''exploitation des transports routiers de personnes',null,null,sysdate,sysdate,'128');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('655','N4203','Intervention technique d''exploitation des transports routiers de marchandises',null,null,sysdate,sysdate,'128');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('656','N4204','Intervention technique d''exploitation des transports routiers de personnes',null,null,sysdate,sysdate,'128');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('657','N4301','Conduite sur rails',null,null,sysdate,sysdate,'129');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('658','N4302','Contrôle des transports en commun',null,null,sysdate,sysdate,'129');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('659','N4401','Circulation du réseau ferré',null,null,sysdate,sysdate,'130');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('660','N4402','Exploitation et manoeuvre des remontées mécaniques',null,null,sysdate,sysdate,'130');
Insert into ROME (ROM_ID,ROM_CODE,ROM_LIBELLE,ROM_D_OUVERTURE,ROM_D_FERMETURE,D_CREATION,D_MODIFICATION,ROM_ID_PERE) values ('661','N4403','Manœuvre du réseau ferré',null,null,sysdate,sysdate,'130');


--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.16.0',to_date('17/04/2014','DD/MM/YYYY'),sysdate,'MAJ TROUVER_LOGIN, RNE, ROME et TYPE_GROUPE');
COMMIT;
