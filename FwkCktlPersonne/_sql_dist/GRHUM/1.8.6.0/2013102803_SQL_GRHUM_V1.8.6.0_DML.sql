--
-- Patch DML de GRHUM du 28/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.6.0
-- Date de publication : 28/10/2013
-- Auteur(s) : Alain

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



------------------------------V20131028.133614__DML_MAJ_STRUCTURE.sql-------------------------------

DECLARE

cpt INTEGER;

BEGIN

/*******/
-- motif_position
select count(*) into cpt from grhum.motif_position where c_motif_position = 'GP';
if (cpt = 0) then
    INSERT INTO grhum.motif_position
    (C_MOTIF_POSITION, LC_MOTIF_POSITION, LL_MOTIF_POSITION, C_POSITION, RANG, REF_REGLEMENTAIRE, TEM_ENFANT, D_CREATION, D_MODIFICATION)
    values ('GP', 'Ent. publ. GIP', 'Entreprise publique ou GIP', 'DETA', '16', '14-04b', 'N', sysdate, sysdate);
end if;

select count(*) into cpt from grhum.motif_position where c_motif_position = 'DC';
if (cpt = 0) then
    INSERT INTO grhum.motif_position
    (C_MOTIF_POSITION, LC_MOTIF_POSITION, LL_MOTIF_POSITION, C_POSITION, RANG, REF_REGLEMENTAIRE, TEM_ENFANT, D_CREATION, D_MODIFICATION)
    values ('DC', 'Adm.CEE ou EEE', 'Adm. Etat membre CEE ou EEE', 'DETA', '17', '14-14', 'N', sysdate, sysdate);
end if;

select count(*) into cpt from grhum.motif_position where c_motif_position = 'AD';
if (cpt = 0) then
    INSERT INTO grhum.motif_position
    (C_MOTIF_POSITION, LC_MOTIF_POSITION, LL_MOTIF_POSITION, C_POSITION, REF_REGLEMENTAIRE, TEM_ENFANT, D_CREATION, D_MODIFICATION)
    values ('AD', 'Etranger adoption', 'Se rendre à l''étranger en vue d''adoption', 'DISP', 'Décret 85-986 art. 47', 'N', sysdate, sysdate);
end if;

select count(*) into cpt from grhum.motif_position where c_motif_position = 'MA';
if (cpt = 0) then
    INSERT INTO grhum.motif_position
    (C_MOTIF_POSITION, LC_MOTIF_POSITION, LL_MOTIF_POSITION, C_POSITION, REF_REGLEMENTAIRE, TEM_ENFANT, D_CREATION, D_MODIFICATION)
    values ('MA', 'Mandat d''élu local', 'Mandat d''Èlu local', 'DISP', 'Décret 85-986 art. 47', 'N', sysdate, sysdate);
end if;

select count(*) into cpt from grhum.motif_position where c_motif_position = 'AP';
if (cpt = 1) then
    UPDATE grhum.motif_position SET REF_REGLEMENTAIRE = '14-04a', d_modification = sysdate where c_motif_position = 'AP';
end if;

-- RNE 
select count(*) into cpt from grhum.rne where c_rne = '0920836J';
if (cpt = 0) then
    INSERT INTO grhum.rne
    (C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, D_DEB_VAL, D_CREATION, D_MODIFICATION, VILLE, SIRET)
 Values
   ('0920836J', 'CTRE INTERN ETUDES PEDAGOG', '1 RUE LEON JOURNAULT', '92310', TO_DATE('25/08/1967', 'DD/MM/YYYY'), sysdate, sysdate, 'SEVRES', '18004306900012');

end if;

end;
/

-- ---------------------------------------------------
-- MAJ de TYPE_ACCES
-- ---------------------------------------------------

INSERT INTO GRHUM.TYPE_ACCES
(C_TYPE_ACCES, LL_TYPE_ACCES, LC_TYPE_ACCES, TEM_PROMOTION, TYPE_PROMOTION, TEM_RECLASSEMENT, TEM_INTEGRATION, TEM_ANC_EQUIV,
D_OUVERTURE, D_FERMETURE, D_CREATION, D_MODIFICATION, TEM_ARRIVEE)
VALUES
(
'CS', 'EXAMEN PROFESSIONNALISÉ RÉSERVÉ (D 2012)', 'EX.PRR2012', 'N', NULL, NULL, NULL, NULL,
TO_DATE('01/09/2012', 'dd/mm/yyyy'), NULL, SYSDATE, SYSDATE, NULL
);


-- ---------------------------------------------------
-- MAJ de TYPE_ABSENCE
-- ---------------------------------------------------

DECLARE
cpt integer;
begin

    select count(*) into cpt from grhum.type_absence where c_type_absence = 'CGST';
    if (cpt = 0)
    then
        INSERT INTO GRHUM.TYPE_ABSENCE 
        (C_TYPE_ABSENCE, LC_TYPE_ABSENCE, LL_TYPE_ABSENCE, TYPE_ABS_IMPUTABLE, TYPE_ABS_NIVEAU, TYPE_ABS_JOURS_AUTORISES, TYPE_SENS_IMPUTATION, 
        D_CREATION, D_MODIFICATION, TYPE_VALIDATION, CONGE_LEGAL, JOURS_CONSECUTIFS, OBSERVATIONS, 
        C_TYPE_ABSENCE_HARPEGE, TEM_ENFANT, C_TYPE_ABSENCE_ONP, TEM_CIR, TEM_HCOMP)
        VALUES ('CGST', 'Sans Traitement', 'Congé sans traitement', 'N', 6, 0, '-', 
        SYSDATE, SYSDATE, 4, 'N', NULL, NULL, 
        NULL, 'N', NULL, 'N', 'N'
        );
    end if;

end;
/

-- ---------------------------------------------------
-- MAJ de PASSAGE_CHEVRON
-- ---------------------------------------------------
update grhum.passage_chevron set duree_chevron_annees = null where c_grade = '3013' and c_echelon = '06' and c_chevron = 'A-3';

-- ---------------------------------------------------
-- MAJ de RIBFOUR_ULR 
-- ---------------------------------------------------
-- Suppression de l'etat 'N' dans RIBFOUR_ULR
UPDATE grhum.ribfour_ulr set rib_valide='A', d_modification=sysdate where rib_valide='N';

-- ---------------------------------------------------
-- MAJ des communes de Corse
-- ---------------------------------------------------

select * from departement where c_academie = '027';
select count(*) from commune where c_dep = '020' and ( d_fin_val > to_date('31/12/1975') or d_fin_val is null );
select count(*) from commune where c_dep = '020' and c_insee like '2A%' and ( d_fin_val > to_date('31/12/1975') or d_fin_val is null );
update grhum.commune set c_dep = '02A', d_modification = sysdate where c_dep = '020' and c_insee like '2A%' and ( d_fin_val > to_date('31/12/1975') or d_fin_val is null );
select count(*) from commune where c_dep = '020' and c_insee like '2B%' and ( d_fin_val > to_date('31/12/1975') or d_fin_val is null );
update grhum.commune set c_dep = '02B', d_modification = sysdate where c_dep = '020' and c_insee like '2B%' and ( d_fin_val > to_date('31/12/1975') or d_fin_val is null );


-- ---------------------------------------------------
-- série de codes RNE trouvés par l'UPPA
-- ---------------------------------------------------

DECLARE
    cpt INTEGER;
    
BEGIN

    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0400105X';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'COLLEGE JACQUES PREVERT', adresse = '3 RUE DU LYCEE', code_postal = '40201', d_modification = sysdate, acad_code = '004', ville = 'MIMIZAN'
        WHERE C_RNE = '0400105X';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0400105X', 'COLLEGE JACQUES PREVERT', '3 RUE DU LYCEE', '40201', null, to_date('03/02/1971','DD/MM/YYYY'), null, to_date('03/02/1971','DD/MM/YYYY'), to_date('29/10/2012','DD/MM/YYYY'),
                 'CLG JACQUES PREVERT', '004', null, null, 'PU', 'MIMIZAN', null, '19400105300017');


    END IF;
    
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0400025K';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'COLLEGE RENE SOUBAIGNE', adresse = 'AVENUE CARNOT', code_postal = '40250', d_modification = sysdate, acad_code = '004', ville = 'MUGRON'
        WHERE C_RNE = '0400025K';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0400025K', 'COLLEGE RENE SOUBAIGNE', 'AVENUE CARNOT', '40250', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('05/07/2005','DD/MM/YYYY'), to_date('25/09/2009','DD/MM/YYYY'),
                 'CLG RENE SOUBAIGNE', '004', null, null, 'PU', 'MUGRON', null, '19400025300014');


    END IF;
    
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0640064X';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'COLLEGE JEAN PUJO', adresse = 'BORCIRIETTE', code_postal = '64430', d_modification = sysdate, acad_code = '004', ville = 'SAINT ETIENNE DE BAIGORRY'
        WHERE C_RNE = '0640064X';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0640064X', 'COLLEGE JEAN PUJO', 'BORCIRIETTE', '64430', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('05/07/2005','DD/MM/YYYY'), to_date('25/09/2009','DD/MM/YYYY'),
                 'CLG JEAN PUJO', '004', null, null, 'PU', 'SAINT ETIENNE DE BAIGORRY', null, '19640064200019');


    END IF;
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0400033U';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'COLLEGE MARIE CURIE', adresse = '155 RUE JEAN CHARLES DE BORDA', code_postal = '40370', d_modification = sysdate, acad_code = '004', ville = 'RION DES LANDES'
        WHERE C_RNE = '0400033U';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0400033U', 'COLLEGE MARIE CURIE', '155 RUE JEAN CHARLES DE BORDA', '40370', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('05/07/2005','DD/MM/YYYY'), to_date('14/09/2009','DD/MM/YYYY'),
                 'CLG MARIE CURIE', '004', null, null, 'PU', 'RION DES LANDES', null, '19400033700015');


    END IF;
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0640025E';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'COLLEGE JOSEPH PEYRE', adresse = '3 PLACE DES ECOLES', code_postal = '64330', d_modification = sysdate, acad_code = '004', ville = 'GARLIN'
        WHERE C_RNE = '0640025E';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0640025E', 'COLLEGE JOSEPH PEYRE', '3 PLACE DES ECOLES', '64330', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('05/07/2005','DD/MM/YYYY'), to_date('21/09/2009','DD/MM/YYYY'),
                 'CLG JOSEPH PEYRE', '004', null, null, 'PU', 'GARLIN', null, '19640025300015');


    END IF;
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0400042D';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'COLLEGE', adresse = '220 RUE DES CHARPENTIERS', code_postal = '40400', d_modification = sysdate, acad_code = '004', ville = 'TARTAS'
        WHERE C_RNE = '0400042D';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0400042D', 'COLLEGE', '220 RUE DES CHARPENTIERS', '40400', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('05/07/2005','DD/MM/YYYY'), to_date('22/07/2010','DD/MM/YYYY'),
                 'CLG JOSEPH PEYRE', '004', null, null, 'PU', 'TARTAS', null, '19400042800012');


    END IF;
    
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0640066Z';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'LYCEE PROFESSIONNEL RAMIRO ARRUE', adresse = '4 RUE RODOLPHE CAILLAUX', code_postal = '64500', d_modification = sysdate, acad_code = '004', ville = 'SAINT JEAN DE LUZ'
        WHERE C_RNE = '0640066Z';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0640066Z', 'LYCEE PROFESSIONNEL RAMIRO ARRUE', '4 RUE RODOLPHE CAILLAUX', '64500', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('05/07/2005','DD/MM/YYYY'), to_date('19/09/2013','DD/MM/YYYY'),
                 'LP RAMIRO ARRUE', '004', null, null, 'PU', 'SAINT JEAN DE LUZ', null, '19640066700016');


    END IF;
    
    
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0400023H';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'COLLEGE SERGE BARRANX', adresse = '14 ROUTE DE DAX', code_postal = '40380', d_modification = sysdate, acad_code = '004', ville = 'MONTFORT EN CHALOSSE'
        WHERE C_RNE = '0400023H';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0400023H', 'COLLEGE SERGE BARRANX', '14 ROUTE DE DAX', '40380', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('05/07/2005','DD/MM/YYYY'), to_date('14/09/2009','DD/MM/YYYY'),
                 'CLG SERGE BARRANX', '004', null, null, 'PU', 'MONTFORT EN CHALOSSE', null, '19400023800015');


    END IF;
END;
/

-- ---------------------------------------------------
-- série de codes RNE trouvés par l'IPB de Bordeaux
-- ---------------------------------------------------

DECLARE
    cpt INTEGER;
    
BEGIN

    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0330169E';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'EC NLE SUP DE CHIMIE PHYSIQUE', adresse = '16 AV PEY BERLAND', code_postal = '33607', d_modification = sysdate, acad_code = '004', ville = 'TALENCE'
        WHERE C_RNE = '0330169E';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0330169E', 'EC NLE SUP DE CHIMIE PHYSIQUE', '16 AV PEY BERLAND', '33607', null, to_date('02/05/1971','DD/MM/YYYY'), to_date('30/03/2009','DD/MM/YYYY'), to_date('28/09/2001','DD/MM/YYYY'), to_date('16/07/2009','DD/MM/YYYY'),
                 null, '004', null, null, 'PU', 'TALENCE', null, '19330169400025');


    END IF;
    
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0331766R';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'MICHEL DE MONTAIGNE', adresse = 'DOMAINE UNIVERSITAIRE', code_postal = '33607', d_modification = sysdate, acad_code = '004', ville = 'PESSAC'
        WHERE C_RNE = '0331766R';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0331766R', 'MICHEL DE MONTAIGNE', 'DOMAINE UNIVERSITAIRE', '33607', null, to_date('09/06/1970','DD/MM/YYYY'),null, to_date('28/09/2001','DD/MM/YYYY'), to_date('09/09/2010','DD/MM/YYYY'),
                 null, '004', null, null, 'PU', 'PESSAC', null, '19331766600017');


    END IF;
    
END;
/

-----------------------------V20131028.133652__DML_MAJ_USER_IMPORT.sql------------------------------
/**********************
    DML
**********************/


-- Suppression de l'etat 'N' dans RIBFOUR_ULR
UPDATE grhum.ribfour_ulr set rib_valide='A', d_modification=sysdate where rib_valide='N';

update grhum.passage_chevron set duree_chevron_annees = null where c_grade = '3013' and c_echelon = '06' and c_chevron = 'A-3';

DECLARE
cpt integer;
begin

    select count(*) into cpt from grhum.type_absence where c_type_absence = 'CGST';
    if (cpt = 0)
    then
        INSERT INTO GRHUM.TYPE_ABSENCE
        (C_TYPE_ABSENCE, LC_TYPE_ABSENCE, LL_TYPE_ABSENCE, TYPE_ABS_IMPUTABLE, TYPE_ABS_NIVEAU, TYPE_ABS_JOURS_AUTORISES, TYPE_SENS_IMPUTATION,
        D_CREATION, D_MODIFICATION, TYPE_VALIDATION, CONGE_LEGAL, JOURS_CONSECUTIFS, OBSERVATIONS,
        C_TYPE_ABSENCE_HARPEGE, TEM_ENFANT, C_TYPE_ABSENCE_ONP, TEM_CIR, TEM_HCOMP)
        VALUES ('CGST', 'Sans Traitement', 'Congé sans traitement', 'N', 6, 0, '-',
        SYSDATE, SYSDATE, 4, 'N', NULL, NULL,
        NULL, 'N', NULL, 'N', 'N'
        );
    end if;

end;
/

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.6.0',to_date('28/10/2013','DD/MM/YYYY'),sysdate,'MAJ TYPE_ABSENCE, TYPE_ACCES, RNE, STRUCTURE_ULR et le USER IMPORT');
COMMIT;
