--
-- Patch DDL de GRHUM du 28/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.6.0
-- Date de publication : 28/10/2013
-- Auteur(s) : Alain

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.5.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.6.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.6.0 a deja ete passe !');
    end if;

end;
/



------------------------------V20131028.133558__DDL_MAJ_STRUCTURE.sql-------------------------------

-- Suppression des procédures sur les 
declare
begin

GRHUM.DROP_OBJECT('MANGUE', 'MAJ_ENFANT', 'PROCEDURE');
GRHUM.DROP_OBJECT('MANGUE', 'DEL_ENFANT', 'PROCEDURE');

end;
/

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (TEM_SIRET_PROVISOIRE VARCHAR2(1));
COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.TEM_SIRET_PROVISOIRE IS 'Témoin de prise en compte du SIRET : P(rovisoire) ou R(éel)';

ALTER TABLE GRHUM.STRUCTURE_ULR MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX GRHUM.PK_STRUCTURE REBUILD TABLESPACE INDX_GRHUM;

-- ---------------------------------------------------
-- MAJ de la procédure Echanger_Personne
-- ---------------------------------------------------

create or replace
PROCEDURE Echanger_Personne
(
  oldid NUMBER,
  newid NUMBER
)
IS
  oldno NUMBER;
  newno NUMBER;
BEGIN
  -- Initialisations...
  SELECT no_individu
  INTO   oldno
  FROM   grhum.INDIVIDU_ULR
  WHERE  pers_id = oldid;

  SELECT no_individu
  INTO   newno
  FROM   grhum.INDIVIDU_ULR
  WHERE  pers_id = newid;
  
  -- **********
  -- ACCORDS...
  -- **********
   Echanger_Personne_Accords(oldid,newid,oldno,newno);

  -- ************
  -- ADMISSION...
  -- ************
   Echanger_Personne_Admission(oldid,newid,oldno,newno);

  -- **********
  -- CONGES...
  -- **********
  --Echanger_Personne_Conges(oldid,newid,oldno,newno);

  -- **********
  -- COURRIER...
  -- **********
  --Echanger_Personne_Courrier(oldid,newid,oldno,newno);

  -- **********
  -- CKTL_GED...
  -- **********
   Echanger_Personne_cktl_ged(oldid,newid,oldno,newno);

  -- **********
  -- DT...
  -- **********
  --Echanger_Personne_Dt(oldid,newid,oldno,newno);
  Echanger_Personne_Dt3(oldid,newid,oldno,newno);


  -- **********
  -- EDTSCOL
  -- **********
  Echanger_Personne_Edtscol(oldid,newid,oldno,newno);

  -- **********
  -- GED...
  -- **********
  Echanger_Personne_Ged(oldid,newid,oldno,newno);

  

  -- **********
  -- HCOMP2...
  -- **********
  Echanger_Personne_Hcomp2(oldid,newid,oldno,newno);

  -- **********
  -- INVENTAIRE...
  -- **********
  --Echanger_Personne_Inventaire(oldid,newid,oldno,newno);

  -- **********
  -- JEFY...
  -- **********
  --Echanger_Personne_Jefy(oldid,newid,oldno,newno);

  -- **********
  -- JEFY_ADMIN...
  -- **********
  Echanger_Personne_Jefy_Admin(oldid,newid,oldno,newno);

  -- **********
  -- JEFY_PAYE...
  -- **********
  Echanger_Personne_Jefy_Paye(oldid,newid,oldno,newno);


  -- **********
  -- MANGUE...
  -- **********
  Echanger_Personne_Mangue(oldid,newid,oldno,newno);

  -- **********
  -- MARACUJA...
  -- **********
  Echanger_Personne_Maracuja(oldid,newid,oldno,newno);

    -- **********
  -- PAPAYE
  -- **********
  -- Echanger_Personne_Papaye(oldid,newid,oldno,newno);

  -- **********
  -- PARC_MATOS...
  -- **********
   -- Echanger_Personne_Parc_Info(oldid,newid,oldno,newno);

  -- **********
  -- PEDA...
  -- **********
  Echanger_Personne_Peda(oldid,newid,oldno,newno);

  -- **********
  -- PRESTATION...
  -- **********
  Echanger_Personne_Prestation(oldid,newid,oldno,newno);

  -- **********
  -- SCOLARITE...
  -- **********
  Echanger_Personne_Scolarite(oldid,newid,oldno,newno);

  -- **********
  -- RESERVATION...
  -- **********
  Echanger_Personne_Reservation(oldid,newid,oldno,newno);

  -- **********
  -- SALLE...
  -- **********
  Echanger_Personne_Salle(oldid,newid,oldno,newno);

  -- **********
  -- BLOBS.PHOTO...
  -- **********
  --Echanger_Personne_Blobs(oldid,newid,oldno,newno);




  -- **********
  -- GRHUM...
  -- **********
  Echanger_Personne_Grhum(oldid,newid,oldno,newno);
  --

  -- Devalidation du trigger sur INDIVIDU_ULR
  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'ALTER TRIGGER grhum.trg_br_individu_ulr DISABLE;');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update GRHUM.INDIVIDU_ULR set tem_valide = ''N'' where no_individu = '||oldno||';');

  -- Re-validation du trigger sur INDIVIDU_ULR
  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'ALTER TRIGGER grhum.trg_br_individu_ulr ENABLE;');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'insert into GRHUM.ECHANGER_SAUVEGARDE select * from GRHUM.PERSONNE where pers_id  = '||oldid||';');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update GRHUM.PERSONNE set pers_libelle = ''PERSONNE INVALIDE - Contacter le CRI'' where pers_id  = '||oldid||';');
END;
/

-----------------------------V20131028.133638__DDL_MAJ_USER_IMPORT.sql------------------------------
/**********************
    DDL
**********************/

grant select on maracuja.bordereau to jefy_paf;
grant execute on maracuja.util to jefy_paf;

GRANT SELECT ON mangue.temps_partiel_seq TO import;
GRANT SELECT ON MANGUE.conge_maladie_seq TO import;
GRANT SELECT ON MANGUE.cgnt_maladie_seq TO import;
GRANT SELECT ON MANGUE.conge_maternite_seq TO import;
GRANT SELECT ON mangue.CLM_seq TO import;
GRANT SELECT ON mangue.CLD_seq TO import;
GRANT SELECT ON MANGUE.cgnt_cgm_seq TO import;
GRANT SELECT ON MANGUE.cgnt_accident_trav_seq TO import;
GRANT SELECT ON MANGUE.depart_seq TO import;
GRANT SELECT ON MANGUE.INDIVIDU_DIPLOMES_SEQ TO IMPORT;
GRANT SELECT ON MANGUE.PASSE_SEQ TO IMPORT;
GRANT SELECT ON GRHUM.PASSAGE_CHEVRON TO IMPORT;

ALTER TABLE IMPORT.EMPLOI ADD (NO_CNU NUMBER(3,0));
COMMENT ON COLUMN IMPORT.EMPLOI.NO_CNU IS 'Clef primaire de la table CNU';
ALTER TABLE IMPORT.CONGE_MALADIE ADD (TEM_JOUR_CARENCE VARCHAR2(1));
COMMENT ON COLUMN IMPORT.CONGE_MALADIE.TEM_JOUR_CARENCE IS 'Témoin du jour de carence';
ALTER TABLE IMPORT.CGNT_MALADIE ADD (TEM_JOUR_CARENCE VARCHAR2(1));
COMMENT ON COLUMN IMPORT.CGNT_MALADIE.TEM_JOUR_CARENCE IS 'Témoin du jour de carence';
ALTER TABLE IMPORT.CONTRAT ADD (D_FIN_ANTICIPEE DATE);
COMMENT ON COLUMN IMPORT.CONTRAT.D_FIN_ANTICIPEE IS 'Date de fin anticipée du contrat';
ALTER TABLE IMPORT.EMPLOI MODIFY (c_SOUS_SECTION_CNU varchar2(3));

ALTER TABLE IMPORT.CONTRAT_AVENANT MODIFY (c_SOUS_SECTION_CNU varchar2(3));

ALTER TABLE IMPORT.ELEMENT_CARRIERE MODIFY (c_SOUS_SECTION_CNU varchar2(3));

ALTER TABLE IMPORT.INDIVIDU_DIPLOMES ADD TEM_VALIDE VARCHAR2(1) DEFAULT 'O' NOT NULL;

ALTER TABLE IMPORT.INDIVIDU MODIFY VILLE_NAISSANCE VARCHAR2(60);

ALTER TABLE IMPORT.EMPLOI MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX IMPORT.PK_EMPLOI REBUILD TABLESPACE INDX_GRHUM;

ALTER TABLE IMPORT.CONGE_MALADIE MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX IMPORT.PK_CONGE_MALADIE REBUILD TABLESPACE INDX_GRHUM;

ALTER TABLE IMPORT.CGNT_MALADIE MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX IMPORT.PK_CGNT_MALADIE REBUILD TABLESPACE INDX_GRHUM;

ALTER TABLE IMPORT.CONTRAT MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX IMPORT.PK_CONTRAT REBUILD TABLESPACE INDX_GRHUM;

ALTER TABLE IMPORT.INDIVIDU_DIPLOMES MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX IMPORT.PK_INDIVIDU_DIPLOMES REBUILD TABLESPACE INDX_GRHUM;


