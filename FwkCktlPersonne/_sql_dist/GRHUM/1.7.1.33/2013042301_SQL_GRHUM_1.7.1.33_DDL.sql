--
-- Patch DML de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/8
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.33 
-- Date de publication : 23/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.32';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.33';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.33 a deja ete passe !');
    end if;

end;
/

--
-- Modification de la table GRHUM.COMPTE : agrandissement de la longueur des logins
--
ALTER TABLE GRHUM.COMPTE MODIFY cpt_login varchar2(50);


--
-- Modification de la table GRHUM.RG_RELEVE_ANOMALIE : ajout de la colonne des codes d'erreur
--
ALTER TABLE GRHUM.RG_RELEVE_ANOMALIE ADD RAN_CODE_ERREUR VARCHAR2(4);
COMMENT ON COLUMN GRHUM.RG_RELEVE_ANOMALIE.RAN_CODE_ERREUR IS 'Le code de catégorie de l''erreur.';


--
-- Modification de la table GRHUM.DOMAINE_SCIENTIFIQUE
--
ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_PARENT_ORDRE NUMBER);
ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_ANCIEN_ORDRE NUMBER);
ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_NOUVEAU_CODE VARCHAR2(20));
ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_VERSION NUMBER);

COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_PARENT_ORDRE IS 'Id du domaine parent';
COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_ANCIEN_ORDRE IS 'Id du domaine ancienne version';
COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_NOUVEAU_CODE IS 'Le code en version VARCHAR';
COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_VERSION IS 'Version de la nomenclature du domaine';





DROP TABLE GRHUM.ROME CASCADE CONSTRAINTS;
--
-- ROME
--
CREATE TABLE GRHUM.ROME
(
  ROM_ID           NUMBER                       NOT NULL,
  ROM_CODE         VARCHAR2(5)                  NOT NULL,
  ROM_LIBELLE      VARCHAR2(2000)               NOT NULL,
  ROM_D_OUVERTURE  DATE,
  ROM_D_FERMETURE  DATE,
  D_CREATION       DATE                         DEFAULT SYSDATE               NOT NULL,
  D_MODIFICATION   DATE                         DEFAULT SYSDATE               NOT NULL,
  ROM_ID_PERE      NUMBER                       
);

COMMENT ON TABLE GRHUM.ROME IS 'Nomenclature du Répertoire Opérationnel des Métiers et des Emplois';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID IS 'Clef primaire';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID IS 'Code ROME';

COMMENT ON COLUMN GRHUM.ROME.ROM_LIBELLE IS 'Libellé du ROME';

COMMENT ON COLUMN GRHUM.ROME.ROM_D_OUVERTURE IS 'Date de début de validité';

COMMENT ON COLUMN GRHUM.ROME.ROM_D_FERMETURE IS 'Date de fin de validité';

COMMENT ON COLUMN GRHUM.ROME.D_CREATION IS 'Date de création de l''enregistrement';

COMMENT ON COLUMN GRHUM.ROME.D_MODIFICATION IS 'Date de dernière modification de l''enregistrement';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID_PERE IS 'ROME père';

declare
tbs_def VARCHAR2(30);
tbs_idx VARCHAR2(30);
ord_sql VARCHAR2(255);
begin
-- recuperation des noms du tablespace par defaut et INDX
-- default
select default_tablespace into tbs_def from dba_users where username = 'GRHUM';
-- indexes
select max(tablespace_name) into tbs_idx from dba_segments where segment_type = 'INDEX' and owner = 'GRHUM' group by tablespace_name
having count(*) = (select max(count(*)) from dba_segments  where segment_type = 'INDEX' and owner = 'GRHUM' group by tablespace_name);

-- positionnement INDX comme tablespace par defaut
ord_sql := 'alter user GRHUM default tablespace '||tbs_idx;
execute immediate (ord_sql);

--
-- PK_ROME
--
execute immediate 'CREATE UNIQUE INDEX GRHUM.PK_ROME ON GRHUM.ROME(ROM_ID)';

--
-- Non Foreign Key Constraints for Table ROME
--
execute immediate 'ALTER TABLE GRHUM.ROME ADD (CONSTRAINT PK_ROME PRIMARY KEY (ROM_ID))';

--
-- Foreign Key Constraints for Table ROME
--
execute immediate '
ALTER TABLE GRHUM.ROME ADD (CONSTRAINT ROME_REF_ROME_PERE
 FOREIGN KEY (ROM_ID_PERE)
 REFERENCES GRHUM.ROME (ROM_ID))';

-- retour au tablespace DATA par defaut
ord_sql := 'alter user GRHUM default tablespace '||tbs_def;
execute immediate (ord_sql);
END;
/





--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.33', TO_DATE('23/04/2013', 'DD/MM/YYYY'),NULL,'Ajout de la nouvelle gestion des droits, Structure avec code ROME, nouveaux grades et échelons Mangue');

--
-- 
--






COMMIT;
