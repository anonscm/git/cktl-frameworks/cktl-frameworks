--
-- Patch DML de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8
--
-- Groupes dynamique

SET DEFINE OFF;

--
--
-- Fichier : 6/8
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.33
-- Date de publication : 23/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- Début du script
--

delete from GRHUM.REGLE_OPERATEUR;
delete from GRHUM.REGLE_NODE;
delete from GRHUM.REGLE_KEY;


INSERT INTO GRHUM.REGLE_OPERATEUR (RO_ID, RO_STR_ID, RO_LC, RO_DESCRIPTION, RO_VALEURS_POSSIBLES) VALUES (1 ,'membre' , 'Membre de', 'L''utilisateur doit être membre du groupe spécifié', null);
INSERT INTO GRHUM.REGLE_OPERATEUR (RO_ID, RO_STR_ID, RO_LC, RO_DESCRIPTION, RO_VALEURS_POSSIBLES) VALUES (2 , 'role','A comme rôle', 'L''utilisateur possède le rôle spécifié (indépendamment du groupe)', null);
INSERT INTO GRHUM.REGLE_OPERATEUR (RO_ID, RO_STR_ID, RO_LC, RO_DESCRIPTION, RO_VALEURS_POSSIBLES) VALUES (3 , 'roledansgroupe','A comme rôle au sein du groupe', 'L''utilisateur possède le rôle spécifié pour le groupe spécifié', null);

INSERT INTO GRHUM.REGLE_NODE (RN_ID, RN_NODE, RN_DESCRIPTION) VALUES (1, 'SIMPLE', 'Règle simple');
INSERT INTO GRHUM.REGLE_NODE (RN_ID, RN_NODE, RN_DESCRIPTION) VALUES (2, 'OR', 'Au moins une des règles doit être respectée');
INSERT INTO GRHUM.REGLE_NODE (RN_ID, RN_NODE, RN_DESCRIPTION) VALUES (3, 'AND', 'Toutes les règles doivent être respectées');
INSERT INTO GRHUM.REGLE_NODE (RN_ID, RN_NODE, RN_DESCRIPTION) VALUES (4, 'NOT', 'Tout sauf la règle exprimée');

INSERT INTO GRHUM.REGLE_KEY (RK_ID, RK_STR_ID, RK_LC, RK_DESCRIPTION) VALUES (1 ,'user' , 'Utilisateur' , 'Utilisateur de l''application');

COMMIT;
