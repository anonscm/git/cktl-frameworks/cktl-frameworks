--
-- Patch DML de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 4/8
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.33
-- Date de publication : 23/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- Mise a jour des passage_echelon et chevron pour les Professeurs des universites CL. EX. Ech1 et Ech2.
DELETE FROM GRHUM.PASSAGE_CHEVRON WHERE C_GRADE = '3003' and C_ECHELON = '02';
DELETE FROM GRHUM.PASSAGE_CHEVRON WHERE C_GRADE = '3004' and C_ECHELON = '01';

DELETE FROM GRHUM.PASSAGE_ECHELON WHERE C_GRADE = '3003' and C_ECHELON = '02';
DELETE FROM GRHUM.PASSAGE_ECHELON WHERE C_GRADE = '3004' and C_ECHELON = '01';


-- AJOUT D UN NOUVEAU TYPE D ACCES
Insert into GRHUM.TYPE_ACCES
   (C_TYPE_ACCES, LL_TYPE_ACCES, LC_TYPE_ACCES, TEM_PROMOTION, TYPE_PROMOTION, 
    TEM_RECLASSEMENT, TEM_INTEGRATION, TEM_ANC_EQUIV, D_OUVERTURE, D_FERMETURE, 
    D_CREATION, D_MODIFICATION, TEM_ARRIVEE)
 Values
   ('102', 'CONCOURS INTERNE RESERVE (LOI 96-1093)', 'CONCOURS INT. RESERV', 'N', 'C', 
    'N', 'N', 'N', TO_DATE('16/12/1996', 'DD/MM/YYYY'), NULL, 
    SYSDATE, SYSDATE, 'O');

 
--
--

UPDATE GRHUM.CORPS set tem_delegation = 'O' where c_type_corps = 'DA';

UPDATE type_contrat_travail set tem_ah_cu_ao = 'N', tem_partiel = 'O'  where  c_type_contrat_trav = 'OM';

UPDATE type_contrat_travail set tem_ah_cu_ao = 'N', tem_partiel = 'N'  where  c_type_contrat_trav = 'OT';


-- SITUATION HANDICAP

--UPDATE GRHUM.SITUATION_HANDICAP SET shan_d_fin_val = to_date('10/02/2005', 'dd/mm/yyyy') WHERE shan_ordre = 1;

INSERT INTO GRHUM.SITUATION_HANDICAP VALUES (8, 'MDPH (Maison départementale des personnes handicapées)', TO_DATE('11/02/2005', 'dd/mm/yyyy'), NULL, SYSDATE, SYSDATE);


-- INDICES BRUTS CHEVRONS

UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'A-1' where c_chevron  = 'A-1';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'A-2' where c_chevron  = 'A-2';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'A-3' where c_chevron  = 'A-3';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'B-1' where c_chevron  = 'B-1';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'B-2' where c_chevron  = 'B-2';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'B-3' where c_chevron  = 'B-3';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'C-1' where c_chevron  = 'C-1';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'C-2' where c_chevron  = 'C-2';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'C-3' where c_chevron  = 'C-3';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'D-1' where c_chevron  = 'D-1';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'D-2' where c_chevron  = 'D-2';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'D-3' where c_chevron  = 'D-3';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'E-1' where c_chevron  = 'E-1';
UPDATE GRHUM.PASSAGE_CHEVRON SET C_INDICE_BRUT = 'E-2' where c_chevron  = 'E-2';


-- NBI

insert into GRHUM.NBI_TRANCHES VALUES (1, to_date('01/08/1990', 'dd/mm/yyyy'), sysdate, sysdate);

insert into GRHUM.NBI_TRANCHES VALUES (2, to_date('01/08/1991', 'dd/mm/yyyy'), sysdate, sysdate);


UPDATE GRHUM.NBI_TYPES SET LL_TYPE_NBI = 'Secrétaire général' WHERE c_TYPE_NBI = 'SGU';

UPDATE GRHUM.NBI_TYPES SET LL_TYPE_NBI = 'Agent comptable' WHERE c_TYPE_NBI = 'AC';

UPDATE GRHUM.NBI_TYPES SET LL_TYPE_NBI = 'Responsable administratif (Cat. A)' WHERE c_TYPE_NBI = 'RA';

UPDATE GRHUM.NBI_TYPES SET LL_TYPE_NBI = 'Resposable technique (Cat. A ou B)' WHERE c_TYPE_NBI = 'RTAB';

UPDATE GRHUM.NBI_TYPES SET LL_TYPE_NBI = 'Resposable technique ouvrier (Cat. B ou C)' WHERE c_TYPE_NBI = 'RTBC';

UPDATE GRHUM.NBI_TYPES SET LL_TYPE_NBI = 'Personnel SCIO' WHERE c_TYPE_NBI = 'SCIO';

UPDATE GRHUM.NBI_TYPES SET LL_TYPE_NBI = 'Personnel service intérieur' WHERE c_TYPE_NBI = 'SI';



-- CLD pour les IA
Insert into GRHUM.GEST_CGMOD_POP
   (C_GESTION_ARRETE, NOM_TABLE_CGMOD, C_TYPE_POPULATION, D_CREATION, D_MODIFICATION)
select 
   'R', 'CLD', 'IA', TO_DATE('04/10/2007 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('04/10/2007 00:00:00', 'MM/DD/YYYY HH24:MI:SS') FROM DUAL
where not exists (select * from gest_cgmod_pop where c_gestion_arrete  = 'R' and nom_table_cgmod = 'CLD' and c_type_population = 'IA');


--
--

INSERT INTO GRHUM.NATURE_BONIF_TERRITOIRE
VALUES (GRHUM.NATURE_BONIF_TERRITOIRE_SEQ.nextval, '10101', 'Algérie', TO_DATE('01/09/1970', 'dd/mm/yyyy'), null, sysdate, sysdate);


-- Ajout d'une type de contrat de travail pour les contractuels CNRS
--
Insert into GRHUM.TYPE_CONTRAT_TRAVAIL
   (C_TYPE_CONTRAT_TRAV, REGIME_URSSAF, LC_TYPE_CONTRAT_TRAV, LL_TYPE_CONTRAT_TRAV, DROIT_CONGES_CONTRAT,
    HORAIRE_HEBDOMADAIRE, TEM_RENOUV_CONTRAT, DUREE_INIT_CONTRAT, DUREE_MAX_CONTRAT, TEM_CAV_AUTOR_TITUL,
    TEM_MISSION, TEM_DELEGATION, TEM_CAV_CTRL_169H, TEM_INVITE_ASSOCIE, TEM_CDI,
    TEM_PARTIEL, TEM_SERVICE_PUBLIC, TEM_ENSEIGNANT, TEM_EQUIV_GRADE, TEM_REMUNERATION_PRINCIPALE,
    C_CATEGORIE, PERIODE_REF_RENOUV, D_DEB_VAL, D_FIN_VAL, REF_REGLEMENTAIRE,
    D_CREATION, D_MODIFICATION, TEM_TITULAIRE, TEM_REMUNERATION_ACCESSOIRE, TEM_AH_CU_AO,
    TEM_QUOTITE_REMUNERATION, TEM_LOCAL, TEM_VISIBLE)
 Values
   ('CN', NULL, 'CONTR. CNRS', 'Contractuels type CNRS', 'N',
    NULL, 'N', null, null, 'N',
    'N', 'N', 'N', 'N', 'N',
    'O', 'O', 'O', 'N', 'O',
    NULL, NULL, TO_DATE('01/01/1900 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), NULL, NULL,
    sysdate, sysdate, 'N', 'N', 'N',
    'N', 'N', 'O');

    
    
-- ACCES GRADE

DELETE FROM GRHUM.TYPE_ACCES_GRADE;

insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG01', 'Recrutement dans le corps', 'Recrutement dans le corps', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG02', 'Sur TA au choix', 'Sur tableau annuel d''avancement au choix', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG03', 'Sur TA par exa prof', 'Sur tableau annuel d''avancement après examen professionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG04', 'Sélection par conc pro', 'Sur l''élection par concours professionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG05', 'Rétrogradation', 'Rétrogradation', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG06', 'Promotion à titre exceptionnel', 'Promotion à titre exceptionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG07', 'A l''ancienneté', 'A l''ancienneté', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG08', 'Au feu', 'Au feu', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG09', 'Reclassement', 'Reclassement', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG10', 'Concours externe', 'Concours externe', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG11', 'Au choix', 'Au choix', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG12', 'Essai ou formation', 'Par essai ou formation qualifiante', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG13', 'Concours interne', 'Concours interne', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG14', 'Reconnaissance promotion détachement', 'Par reconnaissance mutuelle des promotions obtenues en détachement', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG15', 'Liste d''aptitude', 'Liste d''aptitude', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_GRADE 
values ('AG16', 'Par examen professionnel', 'Par examen professionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);




-- ACCES CORPS 

DELETE FROM GRHUM.TYPE_ACCES_CORPS;

insert into GRHUM.TYPE_ACCES_CORPS
values ('AC100', 'Corps de fonctionnaire', 'Corps de fonctionnaire', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC101', 'Concours externe', 'Concours externe', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC102', 'Concours externe except.', 'Recrutement sur concours externe exceptionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC103', 'Concours interne', 'Concours interne', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC104', 'Concours interne except.', 'Recrutement sur concours interne exceptionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC105', '3ème concours', '3ème concours', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC106', '3ème concours except.', 'Recrutement sur concours interne exceptionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC107', 'Concours unique', 'Concours unique', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC108', 'Liste d''aptitude', 'Liste d''aptitude', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC109', 'Examen professionnel', 'Examen professionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC110', 'Tour extérieur', 'Tour extérieur', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC111', 'Concours réservé', 'Concours réservé', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC112', 'Sans concours externe', 'Sans concours externe', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC113', 'Sans concours interne', 'Sans concours interne', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC114', 'Titularisation (contrat donnant lieu à)', 'Titutarisation (Contrat donnant lieu à titularisation)', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC115', 'Titularisation de contractuel', 'Titularisation de contractuel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC116', 'Intégré après détach.', 'Détachement donnant lieu à intégration', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC117', 'Refonte de corps', 'Reclassement, fusion, éclatement, création de corps', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC118', 'Emploi réservé anc. militaire', 'Sur emploi réservé ancien militaire', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC119', 'Emploi réservé handicapé', 'Sur emploi réservé handicapé', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC120', 'Emp Rés inval.veuve guer.', 'Sur emploi réservé invalide, veuve de guerre, ...', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC121', 'Détachement suivi ou non d''intégration', 'Détachement suivi ou non d''intégration', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC122', 'Nommé direct', 'Prise en charge de personnes n''ayant pas la qualité de fonctionnaire', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC123', 'Elevé à la dignité', 'Elévation à la dignité', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC124', 'Reclassement inaptitude physique', 'Reclassement des fonctionnaires de l''état inaptes physiquement', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC125', 'Intégration directe corps judiciaire', 'Intégration directe dans corps judiciaire', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC126', 'Intégration directe', 'Intégration directe d''un fonctionnaire dans le corps', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC127', 'Intégration suite à MAD', 'Intégration suite à mise à disposition', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC128', 'Nomination à titre posthume', 'Nomination ou promotion à titre posthume', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC129', 'Concours professionnel', 'Concours professionnel', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC130', 'A titre except. bravoure blessure', 'Nomination ou promotion à titre exceptionnel (bravoure, blessure)', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC131', 'Recrutement sur titre', 'Recrutement sur titre', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC132', 'Nomination directe hors hiérarchie', 'Nomination directe hors hiérarchie', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC133', 'Concours complémentaire', 'Concours complémentaire', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC201', 'Détaché sur emploi', 'Détachement sur emploi', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC300', 'Emplois à la décision du gouvernement', 'Emplois à la décision du gouvernement', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC301', 'Détaché emploi/décision gouvernement', 'Détachement de fonctionnaire ( à la décision du gouvernement)', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC302', 'Recruté contrat/décision gouvernement', 'Recrutement par contrat (à la décision du gouvernement)', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC400', 'Quasi statut de non titulaires', 'Quasi statut de non titulaires', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC401', 'Recruté sur contrat', 'Recruté sur contrat', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC402', 'Promu sur contrat', 'Promotion sur contrat', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC403', 'Détaché sur contrat', 'Détachement sur contrat', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC499', 'Statuts militaires', 'Statuts militaires', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC500', 'Militaire sur dossier', 'Statut militaire sur dossier', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC501', 'Concours', 'Concours', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC502', 'Militaire concours externe', 'Statut militaire sur concours externe', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC503', 'Militaire concours interne', 'Statut militaire sur concours interne', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC504', 'Militaire sur demande', 'Statut militaire sur demande', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC505', 'Militaire d''office', 'Statut militaire d''office', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC506', 'Militaire par permutation', 'Statut militaire par permutation', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC599', 'Ouvriers de l''Etat', 'Ouvriers de l''Etat', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC600', 'ODE par essai', 'Ouvrier de l''Etat par essai', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC601', 'ODE par stage', 'Ouvrier de l''Etat par stage', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);
insert into GRHUM.TYPE_ACCES_CORPS
values ('AC602', 'ODE sur concours', 'Ouvrier de l''Etat par concours', to_date('01/01/1900','dd/mm/yyyy'), null, sysdate, sysdate);


--
-- INSERTION DES GRADES ASSOCIES AUX TYPES DE CONTRAT
--

DELETE from grhum.type_contrat_grades;

-- Doctorants
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '6902' , 'DO', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '6903' , 'DO', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '6904' , 'DO', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '7511' , 'LT', sysdate, sysdate);

-- Sans Grade
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES
SELECT GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '9990', C_TYPE_CONTRAT_TRAV, SYSDATE, SYSDATE
FROM GRHUM.TYPE_CONTRAT_TRAVAIL WHERE tem_equiv_grade = 'N';

-- CNRS
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8431' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8432' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8433' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8434' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8440' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8441' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8442' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8443' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8444' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8445' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8446' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8447' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8448' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8449' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8451' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8452' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8453' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8454' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8455' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8456' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8457' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8458' , 'CN', sysdate, sysdate);

INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8401' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8402' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8403' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8404' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8410' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8411' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8412' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8413' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8414' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8415' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8416' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8417' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8418' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8419' , 'CN', sysdate, sysdate);


INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8421' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8422' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8423' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8424' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8425' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8426' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8427' , 'CN', sysdate, sysdate);
INSERT INTO GRHUM.TYPE_CONTRAT_GRADES 
VALUES( GRHUM.TYPE_CONTRAT_GRADES_SEQ.nextval, '8428' , 'CN', sysdate, sysdate);



insert into grhum.nature_bonif_taux
values (grhum.nature_bonif_taux_seq.nextval,'G5101', '1/2', 1, 2, sysdate, sysdate );
insert into grhum.nature_bonif_taux
values (grhum.nature_bonif_taux_seq.nextval,'G5200', '1/3', 1, 3, sysdate, sysdate );

--
-- DB_VERSION
--
--UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.33';

COMMIT;