--
-- Patch DDL de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8
--
-- Profil

SET DEFINE OFF;

--
--
-- Fichier : 7/8
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.33
-- Date de publication : 23/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- Début du script
--

-- Creation des tables pour la gestion des droits

--DROP TABLE GRHUM.GD_PROFIL_DROIT_DONNEE cascade constraints;
--DROP TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST cascade constraints;
--DROP TABLE GRHUM.GD_PROFIL_DROIT_FONCTION cascade constraints;
--DROP TABLE GRHUM.GD_PROFIL cascade constraints;
--DROP TABLE GRHUM.GD_FONCTION cascade constraints;
--DROP TABLE GRHUM.GD_DONNEE cascade constraints;
--DROP TABLE GRHUM.GD_DONNEE_STAT cascade constraints;
--DROP TABLE GRHUM.GD_APPLICATION cascade constraints;
--DROP TABLE GRHUM.GD_DOMAINE cascade constraints;
--DROP TABLE GRHUM.GD_TYPE_DROIT_DONNEE cascade constraints;
--DROP TABLE GRHUM.GD_TYPE_DROIT_FONCTION cascade constraints;
--
--DROP SEQUENCE GRHUM.GD_PROFIL_DROIT_DONNEE_SEQ;
--DROP SEQUENCE GRHUM.GD_PROFIL_DROIT_FONCTION_SEQ;
--DROP SEQUENCE GRHUM.GD_PROFIL_DROIT_DONNEE_ST_SEQ;
--DROP SEQUENCE GRHUM.GD_PROFIL_SEQ;
--DROP SEQUENCE GRHUM.GD_FONCTION_SEQ;
--DROP SEQUENCE GRHUM.GD_DONNEE_SEQ;
--DROP SEQUENCE GRHUM.GD_DONNEE_STAT_SEQ;
--DROP SEQUENCE GRHUM.GD_APPLICATION_SEQ;
--DROP SEQUENCE GRHUM.GD_DOMAINE_SEQ;

declare

   c int;

begin

select count(*) into c from user_tables where table_name = upper('GD_PROFIL_DROIT_DONNEE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_PROFIL_DROIT_DONNEE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_PROFIL_DROIT_DONNEE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_PROFIL_DROIT_DONNEE_SEQ
      ';
   end if;


select count(*) into c from user_tables where table_name = upper('GD_PROFIL_DROIT_DONNEE_ST');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_PROFIL_DROIT_DONNEE_ST_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_PROFIL_DROIT_DONNEE_ST_SEQ
      ';
   end if;


select count(*) into c from user_tables where table_name = upper('GD_PROFIL_DROIT_FONCTION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_PROFIL_DROIT_FONCTION CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_PROFIL_DROIT_FONCTION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_PROFIL_DROIT_FONCTION_SEQ
      ';
   end if;
   


select count(*) into c from user_tables where table_name = upper('GD_PROFIL');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_PROFIL CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_PROFIL_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_PROFIL_SEQ
      ';
   end if;
   


select count(*) into c from user_tables where table_name = upper('GD_FONCTION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_FONCTION CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_FONCTION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_FONCTION_SEQ
      ';
   end if;
   


select count(*) into c from user_tables where table_name = upper('GD_DONNEE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_DONNEE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_DONNEE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_DONNEE_SEQ
      ';
   end if;

select count(*) into c from user_tables where table_name = upper('GD_DONNEE_STAT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_DONNEE_STAT CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_DONNEE_STAT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_DONNEE_STAT_SEQ
      ';
   end if;


select count(*) into c from user_tables where table_name = upper('GD_APPLICATION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_APPLICATION CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_APPLICATION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_APPLICATION_SEQ
      ';
   end if;


select count(*) into c from user_tables where table_name = upper('GD_DOMAINE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_DOMAINE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_DOMAINE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_DOMAINE_SEQ
      ';
   end if;


select count(*) into c from user_tables where table_name = upper('GD_TYPE_DROIT_DONNEE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_TYPE_DROIT_DONNEE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_TYPE_DROIT_DONNEE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_TYPE_DROIT_DONNEE_SEQ
      ';
   end if;


select count(*) into c from user_tables where table_name = upper('GD_TYPE_DROIT_FONCTION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.GD_TYPE_DROIT_FONCTION CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('GD_TYPE_DROIT_FONCTION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.GD_TYPE_DROIT_FONCTION_SEQ
      ';
   end if;


end;
/


-- Creation des tables pour la gestion des droits
CREATE TABLE GRHUM.GD_DOMAINE (
        DOM_ID NUMBER(22 , 0) NOT NULL,
        DOM_LC VARCHAR2(50) NOT NULL
    );

ALTER TABLE GRHUM.GD_DOMAINE ADD CONSTRAINT PK_GD_DOMAINE PRIMARY KEY (DOM_ID) USING INDEX TABLESPACE INDX_GRHUM;

ALTER TABLE GRHUM.GD_DOMAINE ADD CONSTRAINT UNQ_GD_DOMAINE_DOM_LC UNIQUE (DOM_LC) USING INDEX TABLESPACE INDX_GRHUM;

COMMENT ON TABLE GRHUM.GD_DOMAINE IS 'Les domaines fonctionnels des applications (GFC, RH, etc.)';
COMMENT ON COLUMN GRHUM.GD_DOMAINE.DOM_ID IS 'Identifiant du domaine fonctionnel';
COMMENT ON COLUMN GRHUM.GD_DOMAINE.DOM_LC IS 'Libelle du domaine fonctionnel (Unique)';


-----------
    
CREATE TABLE GRHUM.GD_APPLICATION (
        APP_ID NUMBER(12 , 0) NOT NULL,
        DOM_ID NUMBER(22 , 0) NOT NULL,
        APP_LC VARCHAR2(50) NOT NULL,
        APP_STR_ID VARCHAR2(20) NOT NULL
    );


ALTER TABLE GRHUM.GD_APPLICATION ADD CONSTRAINT UNQ_GD_APP_STR_ID UNIQUE (APP_STR_ID) USING INDEX TABLESPACE INDX_GRHUM;

ALTER TABLE GRHUM.GD_APPLICATION ADD CONSTRAINT PK_GD_APPLICATION PRIMARY KEY (APP_ID) USING INDEX TABLESPACE INDX_GRHUM;

ALTER TABLE GRHUM.GD_APPLICATION ADD CONSTRAINT FK_GD_APP_DOM_ID FOREIGN KEY (DOM_ID)
    REFERENCES GRHUM.GD_DOMAINE (DOM_ID);
COMMENT ON TABLE GRHUM.GD_APPLICATION IS 'Les applications ou regroupements de fonctionnalités ou groupes de données';
COMMENT ON COLUMN GRHUM.GD_APPLICATION.APP_ID IS 'Identifiant de l''application';
COMMENT ON COLUMN GRHUM.GD_APPLICATION.DOM_ID IS 'Référence au domaine fonctionnel';
COMMENT ON COLUMN GRHUM.GD_APPLICATION.APP_LC IS 'Libellé de l''application';
COMMENT ON COLUMN GRHUM.GD_APPLICATION.APP_STR_ID IS 'Identifiant significatif de l''application. Doit être unique';

-----

CREATE TABLE GRHUM.GD_FONCTION
(
  FON_ID          NUMBER(22,0)             NOT NULL,
  APP_ID            NUMBER(22,0)           NOT NULL,
  FON_CATEGORIE      VARCHAR2(50)          NOT NULL,
  FON_ID_INTERNE     VARCHAR2(20)          NOT NULL,
  FON_LC            VARCHAR2(50)           NOT NULL,
  FON_DESCRIPTION    VARCHAR2(500)         NULL
);

COMMENT ON TABLE GRHUM.GD_FONCTION IS 'Les fonctions (actions utilisateurs) possibles des differentes applications, définis par le développeur';
COMMENT ON COLUMN GRHUM.GD_FONCTION.FON_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_FONCTION.APP_ID IS 'Reference a l''application dont dépend cette fonction';
COMMENT ON COLUMN GRHUM.GD_FONCTION.FON_ID_INTERNE IS 'Identifiant visible de la fonction (couple APP_ID/FON_ID_INTERNE unique dans la table)';
COMMENT ON COLUMN GRHUM.GD_FONCTION.FON_CATEGORIE IS 'Categorie de la fonction (peut correspondre à un menu dans l''application, par exemple Outils, Administration, etc.)';
COMMENT ON COLUMN GRHUM.GD_FONCTION.FON_DESCRIPTION IS 'Description de la fonction, peut etre affichee en tooltip dans l''application';
COMMENT ON COLUMN GRHUM.GD_FONCTION.FON_LC IS 'Libelle de la fonction. Peut apparaitre comme libelle de l''action (menu ou bouton)';


CREATE UNIQUE INDEX GRHUM.PK_GD_FONCTION ON GRHUM.GD_FONCTION (FON_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_FONCTION ADD (CONSTRAINT PK_GD_FONCTION  PRIMARY KEY  (FON_ID) USING INDEX);

CREATE UNIQUE INDEX GRHUM.UNQ_GD_APP_FON_ID ON GRHUM.GD_FONCTION (APP_ID, FON_ID_INTERNE) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_FONCTION ADD (CONSTRAINT UNQ_GD_APP_FON_ID UNIQUE (APP_ID, FON_ID_INTERNE) USING INDEX);

ALTER TABLE GRHUM.GD_FONCTION ADD (CONSTRAINT FK_GD_FONCTION_APP_ID  FOREIGN KEY (APP_ID) REFERENCES GRHUM.GD_APPLICATION (APP_ID)  DEFERRABLE INITIALLY DEFERRED);


----

CREATE TABLE GRHUM.GD_DONNEE
(
  DON_ID          NUMBER(22,0)             NOT NULL,
  APP_ID            NUMBER(22,0)           NOT NULL,
  DON_CATEGORIE      VARCHAR2(50)          NOT NULL,
  DON_ID_INTERNE     VARCHAR2(20)          NOT NULL,
  DON_LC            VARCHAR2(50)           NOT NULL,
  DON_DESCRIPTION    VARCHAR2(500)         NULL
);

COMMENT ON TABLE GRHUM.GD_DONNEE IS 'Les périmètres de données possibles pour les differentes applications, définis par le développeur';
COMMENT ON COLUMN GRHUM.GD_DONNEE.DON_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_DONNEE.APP_ID IS 'Reference a l''application dont dépend le périmètre de données';
COMMENT ON COLUMN GRHUM.GD_DONNEE.DON_ID_INTERNE IS 'Identifiant visible du périmètre de données (couple APP_ID/DON_ID_INTERNE unique dans la table)';
COMMENT ON COLUMN GRHUM.GD_DONNEE.DON_CATEGORIE IS 'Categorie du prérimètre de données';
COMMENT ON COLUMN GRHUM.GD_DONNEE.DON_DESCRIPTION IS 'Description du périmètre de données';
COMMENT ON COLUMN GRHUM.GD_DONNEE.DON_LC IS 'Libelle du périmètre de données.';


CREATE UNIQUE INDEX GRHUM.PK_GD_DONNEE ON GRHUM.GD_DONNEE (DON_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_DONNEE ADD (CONSTRAINT PK_GD_DONNEE  PRIMARY KEY  (DON_ID) USING INDEX);

CREATE UNIQUE INDEX GRHUM.UNQ_GD_APP_DON_ID ON GRHUM.GD_DONNEE (APP_ID, DON_ID_INTERNE) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_DONNEE ADD (CONSTRAINT UNQ_GD_APP_DON_ID UNIQUE (APP_ID, DON_ID_INTERNE)  USING INDEX);

ALTER TABLE GRHUM.GD_DONNEE ADD (CONSTRAINT FK_GD_DONNEE_APP_ID  FOREIGN KEY (APP_ID) REFERENCES GRHUM.GD_APPLICATION (APP_ID)  DEFERRABLE INITIALLY DEFERRED);

-----
----

CREATE TABLE GRHUM.GD_DONNEE_STAT
(
  DOS_ID          NUMBER(22,0)             NOT NULL,
  APP_ID            NUMBER(22,0)           NOT NULL,
  DOS_ENTITE        VARCHAR2(200)          NOT NULL,
  DOS_CLE           VARCHAR2(30)           NOT NULL,
  DOS_TYPE_CLE      VARCHAR2(20)           NOT NULL,
  DOS_LC        VARCHAR2(100)           NOT NULL,
  DOS_DESCRIPTION   VARCHAR2(500)          NOT NULL,
  DOS_ENTITE_LIBELLE_KEY VARCHAR2(30)           NOT NULL
  --,
  --DOS_SQL_FOR_LIST VARCHAR2(4000)           NULL,
  --DOS_SQL_FOR_TREE_ROOTS VARCHAR2(4000)           NULL, 
  --DOS_SQL_FOR_TREE_CHILDS VARCHAR2(4000)           NULL
);

COMMENT ON TABLE GRHUM.GD_DONNEE_STAT IS 'Les descriptions des données pour lesquelles il est possible de donner des droits de manière statiques aux profils, définies par le développeur (Exemple JEFY_ADMIN.ORGAN, ORG_ID, NUM, "Branche de l''organigramme budgétaire")';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.APP_ID IS 'Reference a l''application';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_ENTITE IS 'Nom de la table (USER.NOM_DE_TABLE)';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_TYPE_CLE IS 'Type de la clé (NUM, STR)';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_LC IS 'Libellé de la donnée';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_DESCRIPTION IS 'Description de la donnée';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_CLE IS 'Nom du champ correspondant à la clé de l''entité';
COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_ENTITE_LIBELLE_KEY IS 'Nom du champ pour l''affichage des données (par exemple org_libelle)';
--COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_SQL_FOR_LIST IS 'Requete sql a executer dans le cas d''un affichage sous forme de liste';
--COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_SQL_FOR_TREE_ROOTS IS 'Requete sql a executer pour obtenir la racine dans le cas d''un affichage sous forme d''arbre';
--COMMENT ON COLUMN GRHUM.GD_DONNEE_STAT.DOS_SQL_FOR_TREE_CHILDS IS 'Requete sql a executer pour obtenir les enfants d''un noeud dans le cas d''un affichage sous forme d''arbre';


CREATE UNIQUE INDEX GRHUM.PK_GD_DONNEE_STAT ON GRHUM.GD_DONNEE_STAT (DOS_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_DONNEE_STAT ADD (CONSTRAINT PK_GD_DONNEE_STAT  PRIMARY KEY  (DOS_ID) USING INDEX);

CREATE UNIQUE INDEX GRHUM.UNQ_GD_DOS_ENTITE ON GRHUM.GD_DONNEE_STAT (DOS_ENTITE) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_DONNEE_STAT ADD (CONSTRAINT UNQ_GD_DOS_ENTITE UNIQUE (DOS_ENTITE) USING INDEX);

ALTER TABLE GRHUM.GD_DONNEE_STAT ADD (CONSTRAINT FK_GD_DONNEE_STAT_APP_ID  FOREIGN KEY (APP_ID) REFERENCES GRHUM.GD_APPLICATION (APP_ID)  DEFERRABLE INITIALLY DEFERRED);

-----

CREATE TABLE GRHUM.GD_PROFIL
(
    PR_ID                   number(22,0)        not null,
    PR_PERE_ID              number(22,0)        null,
    GRPD_ID                 number(22,0)        null,
    DATE_CREATION           date                not null,
    DATE_MODIFICATION       date                not null,
    PERS_ID_CREATION        number(22,0)        not null,
    PERS_ID_MODIFICATION    number(22,0)        not null,   
    PR_LC                   VARCHAR2(100)       not null,
    PR_DESCRIPTION          varchar(500)        null,
    PR_REMARQUES            varchar(1000)       null
);

COMMENT ON TABLE GRHUM.GD_PROFIL IS 'Les profils d''utilisateurs';
COMMENT ON COLUMN GRHUM.GD_PROFIL.PR_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_PROFIL.PR_PERE_ID IS 'Identifiant du profil parent';
COMMENT ON COLUMN GRHUM.GD_PROFIL.GRPD_ID IS 'Identifiant du groupe dynamique correspondant au profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL.DATE_CREATION IS 'Date de création du profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL.DATE_MODIFICATION IS 'Date de modification du profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL.PERS_ID_CREATION IS 'Utilisateur qui a créé le profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL.PERS_ID_MODIFICATION IS 'Dernier utilisateur qui a modifié le profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL.PR_LC IS 'Libellé du profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL.PR_DESCRIPTION IS 'Description du profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL.PR_REMARQUES IS 'Remarques concernant le profil (pour gestion interne)';

CREATE UNIQUE INDEX GRHUM.PK_GD_PROFIL ON GRHUM.GD_PROFIL (PR_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_PROFIL ADD (CONSTRAINT PK_GD_PROFIL  PRIMARY KEY  (PR_ID) USING INDEX);

ALTER TABLE GRHUM.GD_PROFIL ADD (CONSTRAINT FK_GD_PROFIL_PERE_ID  FOREIGN KEY (PR_PERE_ID) REFERENCES GRHUM.GD_PROFIL (PR_ID)  DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.GD_PROFIL ADD (CONSTRAINT FK_GD_PROFIL_GRPD_ID  FOREIGN KEY (GRPD_ID) REFERENCES GRHUM.GROUPE_DYNAMIQUE (GRPD_ID)  DEFERRABLE INITIALLY DEFERRED);


-----
CREATE TABLE GRHUM.GD_TYPE_DROIT_FONCTION
(
    TDF_ID                  number(2,0)     not null,
    TDF_STR_ID              varchar2(1)     not null,
    TDF_LL                  varchar2(100)   not null
);

CREATE UNIQUE INDEX GRHUM.PK_GD_TYPE_DROIT_FONCTION ON GRHUM.GD_TYPE_DROIT_FONCTION (TDF_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_TYPE_DROIT_FONCTION ADD (CONSTRAINT PK_GD_TYPE_DROIT_FONCTION  PRIMARY KEY  (TDF_ID) USING INDEX);
ALTER TABLE GRHUM.GD_TYPE_DROIT_FONCTION ADD CONSTRAINT UNQ_GD_TDF_TDF_STR_ID UNIQUE (TDF_STR_ID) USING INDEX TABLESPACE INDX_GRHUM;

COMMENT ON TABLE GRHUM.GD_TYPE_DROIT_FONCTION IS 'Les types de droits applicables aux fonctions (U, K, N, ...)';
COMMENT ON COLUMN GRHUM.GD_TYPE_DROIT_FONCTION.TDF_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_TYPE_DROIT_FONCTION.TDF_STR_ID IS 'Identifiant signifiant du type de droit (U, K, N, ...)';
COMMENT ON COLUMN GRHUM.GD_TYPE_DROIT_FONCTION.TDF_LL IS 'Libellé du type de droit (Utilisation, visualisation, ...)';


-----
CREATE TABLE GRHUM.GD_TYPE_DROIT_DONNEE
(
    TDD_ID                  number(2,0)     not null,
    TDD_STR_ID              varchar2(1)     not null,
    TDD_LL                  varchar2(100)   not null
);

CREATE UNIQUE INDEX GRHUM.PK_GD_TYPE_DROIT_DONNEE ON GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_TYPE_DROIT_DONNEE ADD (CONSTRAINT PK_GD_TYPE_DROIT_DONNEE  PRIMARY KEY  (TDD_ID) USING INDEX);
ALTER TABLE GRHUM.GD_TYPE_DROIT_DONNEE ADD CONSTRAINT UNQ_GD_TDD_TDD_STR_ID UNIQUE (TDD_STR_ID) USING INDEX TABLESPACE INDX_GRHUM;

COMMENT ON TABLE GRHUM.GD_TYPE_DROIT_DONNEE IS 'Les types de droits applicables aux données (C,R,U,D,N)';
COMMENT ON COLUMN GRHUM.GD_TYPE_DROIT_DONNEE.TDD_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_TYPE_DROIT_DONNEE.TDD_STR_ID IS 'Identifiant signifiant du type de droit C,R,U,D,N)';
COMMENT ON COLUMN GRHUM.GD_TYPE_DROIT_DONNEE.TDD_LL IS 'Libellé du type de droit (Création, Lecture, ...)';


------

CREATE TABLE GRHUM.GD_PROFIL_DROIT_FONCTION
(
    PDF_ID                  number(22,0)        not null,
    PR_ID                   number(22,0)        not null,
    FON_ID                  number(22,0)        not null,
    TDF_ID                  number(2,0)         not null,
    DATE_CREATION           date                not null,
    DATE_MODIFICATION       date                not null,
    PERS_ID_CREATION        number(22,0)        not null            
);
CREATE UNIQUE INDEX GRHUM.PK_GD_PROFIL_DROIT_FONCTION ON GRHUM.GD_PROFIL_DROIT_FONCTION (PDF_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_PROFIL_DROIT_FONCTION ADD (CONSTRAINT PK_GD_PROFIL_DROIT_FONCTION  PRIMARY KEY  (PDF_ID) USING INDEX);

ALTER TABLE GRHUM.GD_PROFIL_DROIT_FONCTION ADD (CONSTRAINT FK_GDPDF_PR_ID  FOREIGN KEY (PR_ID) REFERENCES GRHUM.GD_PROFIL (PR_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.GD_PROFIL_DROIT_FONCTION ADD (CONSTRAINT FK_GDPDF_FON_ID  FOREIGN KEY (FON_ID) REFERENCES GRHUM.GD_FONCTION (FON_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.GD_PROFIL_DROIT_FONCTION ADD (CONSTRAINT FK_GDPDF_TDF_ID  FOREIGN KEY (TDF_ID) REFERENCES GRHUM.GD_TYPE_DROIT_FONCTION (TDF_ID) DEFERRABLE INITIALLY DEFERRED);

COMMENT ON TABLE GRHUM.GD_PROFIL_DROIT_FONCTION IS 'Répartition entre les profils et les fonctions (droits fonctionnels accordés aux profils)';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_FONCTION.PDF_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_FONCTION.PR_ID IS 'Référence au profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_FONCTION.FON_ID IS 'Référence à la fonction';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_FONCTION.TDF_ID IS 'Référence au type de droit';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_FONCTION.DATE_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_FONCTION.DATE_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_FONCTION.PERS_ID_CREATION IS 'Utilisateur qui a créé de l''enregistrement';

------

CREATE TABLE GRHUM.GD_PROFIL_DROIT_DONNEE
(
    PDD_ID                  number(22,0)        not null,
    PR_ID                   number(22,0)        not null,
    DON_ID                  number(22,0)        not null,
    TDD_ID                  number(2,0)         not null,
    DATE_CREATION           date                not null,
    DATE_MODIFICATION       date                not null,
    PERS_ID_CREATION        number(22,0)        not null
);
CREATE UNIQUE INDEX GRHUM.PK_GD_PROFIL_DROIT_DONNEE ON GRHUM.GD_PROFIL_DROIT_DONNEE (PDD_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE ADD (CONSTRAINT PK_GD_PROFIL_DROIT_DONNEE  PRIMARY KEY  (PDD_ID) USING INDEX);

ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE ADD (CONSTRAINT FK_GDPDD_PR_ID  FOREIGN KEY (PR_ID) REFERENCES GRHUM.GD_PROFIL (PR_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE ADD (CONSTRAINT FK_GDPDD_DON_ID  FOREIGN KEY (DON_ID) REFERENCES GRHUM.GD_DONNEE (DON_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE ADD (CONSTRAINT FK_GDPDD_TDD_ID  FOREIGN KEY (TDD_ID) REFERENCES GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID) DEFERRABLE INITIALLY DEFERRED);

COMMENT ON TABLE GRHUM.GD_PROFIL_DROIT_DONNEE IS 'Répartition entre les profils et les périmètres de données (droits sur les périmètres de données accordés aux profils)';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE.PDD_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE.PR_ID IS 'Référence au profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE.DON_ID IS 'Référence au périmètre de données';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE.TDD_ID IS 'Référence au type de droit';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE.DATE_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE.DATE_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE.PERS_ID_CREATION IS 'Utilisateur qui a créé de l''enregistrement';

------

CREATE TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST
(
    PDS_ID                  number(22,0)        not null,
    PR_ID                   number(22,0)        not null,
    DOS_ID                  number(22,0)        not null,
    TDD_ID                  number(2,0)         not null,
    PDS_VALEUR_NUM          NUMBER(22,0)        null,           
    PDS_VALEUR_STR          varchar2(200)       null,           
    DATE_CREATION           date                not null,
    DATE_MODIFICATION       date                not null,
    PERS_ID_CREATION        number(22,0)        not null
);
CREATE UNIQUE INDEX GRHUM.GD_PROFIL_DROIT_DONNEE_ST ON GRHUM.GD_PROFIL_DROIT_DONNEE_ST (PDS_ID) TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST ADD (CONSTRAINT PK_GD_PROFIL_DROIT_DONNEE_ST  PRIMARY KEY  (PDS_ID) USING INDEX);

ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST ADD (CONSTRAINT FK_GDPDS_PR_ID  FOREIGN KEY (PR_ID) REFERENCES GRHUM.GD_PROFIL (PR_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST ADD (CONSTRAINT FK_GDPDS_DOS_ID  FOREIGN KEY (DOS_ID) REFERENCES GRHUM.GD_DONNEE_STAT (DOS_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST ADD (CONSTRAINT FK_GDPDS_TDD_ID  FOREIGN KEY (TDD_ID) REFERENCES GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID) DEFERRABLE INITIALLY DEFERRED);

COMMENT ON TABLE GRHUM.GD_PROFIL_DROIT_DONNEE_ST IS 'Répartition entre les profils et les données statiques (droits sur les données statiques accordés aux profils)';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.PDS_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.PR_ID IS 'Référence au profil';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.DOS_ID IS 'Référence à la donnée statique';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.TDD_ID IS 'Référence au type de droit';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.PDS_VALEUR_NUM IS 'Valeur de la clé qui identifie la donnée (si numérique)';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.PDS_VALEUR_STR IS 'Valeur de la clé qui identifie la donnée (si textuelle)';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.DATE_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.DATE_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PROFIL_DROIT_DONNEE_ST.PERS_ID_CREATION IS 'Utilisateur qui a créé de l''enregistrement';
------

----
-- Sequences
----
CREATE SEQUENCE GRHUM.GD_PROFIL_DROIT_DONNEE_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_PROFIL_DROIT_FONCTION_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_PROFIL_DROIT_DONNEE_ST_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_PROFIL_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_FONCTION_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_DONNEE_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_DONNEE_STAT_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_APPLICATION_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.GD_DOMAINE_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
