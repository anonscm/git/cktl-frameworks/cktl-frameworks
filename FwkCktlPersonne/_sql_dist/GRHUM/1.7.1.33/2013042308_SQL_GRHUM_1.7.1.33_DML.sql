--
-- Patch DML de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8
--
-- Profil

SET DEFINE OFF;

--
--
-- Fichier : 8/8
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.33
-- Date de publication : 23/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- Début du script
--

CREATE OR REPLACE package GRHUM.GD_UTILS
is
procedure init_all;
  procedure init_constantes;
  procedure init_from_jefy_admin;
end GD_UTILS;

/

CREATE OR REPLACE PACKAGE BODY GRHUM.GD_UTILS
is


procedure init_constantes
is
    persid_createur integer;
    flag integer;
 begin
 -- Initialise les tables sur la gestion des droits avec les constantes
delete from GRHUM.GD_TYPE_DROIT_FONCTION;
delete from GRHUM.GD_TYPE_DROIT_DONNEE;
delete from GRHUM.GD_PROFIL;


-- recup d'un createur
persid_createur := null;
select count(*) into flag from (select param_value from (
select * from grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

if (flag = 0) then
    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
end if;

select c.pers_id into persid_createur from (select param_value from (
select * from grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

--
INSERT INTO GRHUM.GD_TYPE_DROIT_FONCTION (TDF_ID, TDF_STR_ID, TDF_LL) VALUES (0 , 'N','Interdiction'); 
INSERT INTO GRHUM.GD_TYPE_DROIT_FONCTION (TDF_ID, TDF_STR_ID, TDF_LL) VALUES (1 , 'K','Connaissance');
INSERT INTO GRHUM.GD_TYPE_DROIT_FONCTION (TDF_ID, TDF_STR_ID, TDF_LL) VALUES (2 , 'U','Utilisation'); 
 

INSERT INTO GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID, TDD_STR_ID, TDD_LL) VALUES (0 ,'N' , 'Interdiction');
INSERT INTO GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID, TDD_STR_ID, TDD_LL) VALUES (1 ,'R' , 'Lecture');
INSERT INTO GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID, TDD_STR_ID, TDD_LL) VALUES (2 ,'C' , 'Création');
INSERT INTO GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID, TDD_STR_ID, TDD_LL) VALUES (3 ,'U' , 'Modification');
INSERT INTO GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID, TDD_STR_ID, TDD_LL) VALUES (4 ,'D' , 'Suppression');


INSERT INTO GRHUM.GD_PROFIL (
   PR_ID, PR_PERE_ID, DATE_CREATION, 
   DATE_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION, 
   PR_LC, PR_DESCRIPTION, PR_REMARQUES) 
VALUES (GRHUM.GD_PROFIL_SEQ.nextval , 
        null, 
        sysdate,
        sysdate, 
        persid_createur, 
        persid_createur,
        'Utilisateur du SI', 
        'Profil racine, décrivant un utilisateur basique du SI (sans droit particulier)', 
        null);
end;


procedure init_from_jefy_admin
is
 begin
 -- Recupere les domaines, applications et fonctions depuis Jefy_admin
 delete from GRHUM.GD_FONCTION;
 delete from GRHUM.GD_APPLICATION;
delete from GRHUM.GD_DOMAINE;

    INSERT INTO GRHUM.GD_DOMAINE (DOM_ID, DOM_LC) select GRHUM.GD_DOMAINE_seq.nextval, dom_libelle from jefy_admin.domaine;
    
    INSERT INTO GRHUM.GD_APPLICATION (APP_ID, DOM_ID, APP_LC, APP_STR_ID) select GRHUM.GD_APPLICATION_seq.nextval, d2.dom_id, x.tyap_libelle, x.tyap_strid from jefy_admin.type_application x, jefy_admin.domaine d1, grhum.GD_DOMAINE d2 where x.dom_id=d1.dom_id and d1.dom_libelle=d2.dom_lc;
    
    INSERT INTO GRHUM.GD_FONCTION (   FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) select GRHUM.GD_FONCTION_SEQ.NEXTVAL, d2.app_id, FON_CATEGORIE, FON_ID_INTERNE, FON_Libelle, FON_DESCRIPTION from jefy_admin.fonction x, jefy_admin.type_application d1, grhum.GD_application d2 where x.tyap_id=d1.tyap_id and d1.tyap_strid=d2.app_str_id;
end;


procedure init_all
is
begin
init_constantes;
init_from_jefy_admin;
end;



end GD_UTILS;
/

BEGIN 
  GRHUM.GD_UTILS.INIT_ALL;
  
	
--  COMMIT; 
END;
/

--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.33';

COMMIT;
