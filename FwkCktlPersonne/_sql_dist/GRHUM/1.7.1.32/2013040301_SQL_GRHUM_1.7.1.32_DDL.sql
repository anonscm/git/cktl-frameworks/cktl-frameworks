--
-- Patch DML de GRHUM du 03/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 03/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.31';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.32';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.32 a deja ete passe !');
    end if;

end;
/



--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.32', TO_DATE('03/04/2013', 'DD/MM/YYYY'),NULL,'Suppression de tables de la famille ENQUETES dans GRHUM, MAJ de INS_STRUCTURE_ULR_SYNC et MAJ des procédures impactées par le nettoyage');

--
-- MAJ de Ins_Structure_Ulr_Sync
--

create or replace
PROCEDURE       Ins_Structure_Ulr_Sync(
  cstructure                OUT STRUCTURE_ULR.c_structure%TYPE,
  persid                    OUT STRUCTURE_ULR.pers_id%TYPE,
  INS_LL_STRUCTURE          STRUCTURE_ULR.ll_structure%TYPE,
  INS_LC_STRUCTURE          STRUCTURE_ULR.lc_structure%TYPE,
  INS_C_TYPE_STRUCTURE      STRUCTURE_ULR.c_type_structure%TYPE,
  INS_C_STRUCTURE_PERE      STRUCTURE_ULR.c_structure_pere%TYPE,
  INS_C_TYPE_ETABLISSEMEN   STRUCTURE_ULR.c_type_etablissemen%TYPE,
  INS_C_ACADEMIE            STRUCTURE_ULR.c_academie%TYPE,
  INS_C_STATUT_JURIDIQUE    STRUCTURE_ULR.c_statut_juridique%TYPE,
  INS_C_RNE                 STRUCTURE_ULR.c_rne%TYPE,
  INS_SIRET                 STRUCTURE_ULR.siret%TYPE,
  INS_SIREN                 STRUCTURE_ULR.siren%TYPE,
  INS_C_NAF                 STRUCTURE_ULR.c_naf%TYPE,
  INS_C_TYPE_DECISION_STR   STRUCTURE_ULR.c_type_decision_str%TYPE,
  INS_REF_EXT_ETAB          STRUCTURE_ULR.ref_ext_etab%TYPE,
  INS_REF_EXT_COMP          STRUCTURE_ULR.ref_ext_comp%TYPE,
  INS_REF_EXT_CR            STRUCTURE_ULR.ref_ext_cr%TYPE,
  INS_REF_DECISION          STRUCTURE_ULR.ref_decision%TYPE,
  INS_DATE_DECISION         STRUCTURE_ULR.date_decision%TYPE,
  INS_DATE_OUVERTURE        STRUCTURE_ULR.date_ouverture%TYPE,
  INS_DATE_FERMETURE        STRUCTURE_ULR.date_fermeture%TYPE,
  INS_STR_ORIGINE            STRUCTURE_ULR.str_origine%TYPE,
  INS_STR_PHOTO              STRUCTURE_ULR.str_photo%TYPE,
  INS_STR_ACTIVITE            STRUCTURE_ULR.str_activite%TYPE,
  INS_GRP_OWNER             STRUCTURE_ULR.grp_owner%TYPE,
  INS_GRP_RESPONSABLE        STRUCTURE_ULR.grp_responsable%TYPE,
  INS_GRP_FORME_JURIDIQUE     STRUCTURE_ULR.grp_forme_juridique%TYPE,
  INS_GRP_CAPITAL              STRUCTURE_ULR.grp_capital%TYPE,
  INS_GRP_CA                  STRUCTURE_ULR.grp_ca%TYPE,
  INS_GRP_EFFECTIFS         STRUCTURE_ULR.grp_effectifs%TYPE,
  INS_GRP_CENTRE_DECISION    STRUCTURE_ULR.grp_centre_decision%TYPE,
  INS_GRP_APE_CODE          STRUCTURE_ULR.grp_ape_code%TYPE,
  INS_GRP_APE_CODE_BIS      STRUCTURE_ULR.grp_ape_code_bis%TYPE,
  INS_GRP_APE_CODE_COMP      STRUCTURE_ULR.grp_ape_code_comp%TYPE,
  INS_GRP_ACCES             STRUCTURE_ULR.grp_acces%TYPE,
  INS_GRP_ALIAS             STRUCTURE_ULR.grp_alias%TYPE,
  INS_GRP_RESPONSABILITE     STRUCTURE_ULR.grp_responsabilite%TYPE,
  INS_GRP_TRADEMARQUE         STRUCTURE_ULR.grp_trademarque%TYPE,
  INS_GRP_WEBMESTRE         STRUCTURE_ULR.grp_webmestre%TYPE,
  INS_GRP_FONCTION1         STRUCTURE_ULR.grp_fonction1%TYPE,
  INS_GRP_FONCTION2         STRUCTURE_ULR.grp_fonction2%TYPE,
  INS_ORG_ORDRE             STRUCTURE_ULR.org_ordre%TYPE,
  INS_GRP_MOTS_CLEFS        STRUCTURE_ULR.grp_mots_clefs%TYPE
)
IS
-- ------------
-- DECLARATIONS
-- ------------
cpt INTEGER;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

/*SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
WHERE ll_structure=ins_ll_structure;
IF (cpt>0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_STRUCTURE_ULR : Libelle existant');
END IF;*/

IF (ins_c_structure_pere IS NOT NULL) THEN
    SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
    WHERE c_structure=ins_c_structure_pere;
    IF (cpt=0) THEN
       RAISE_APPLICATION_ERROR(-20001,'INS_STRUCTURE_ULR : C_structure_pere inconnu');
    END IF;
END IF;


--SELECT MAX(TO_NUMBER(c_structure))+1 INTO cstructure FROM STRUCTURE_ULR;--+1

SELECT SEQ_STRUCTURE_ULR_SYNC.NEXTVAL INTO cstructure FROM dual;

--SELECT MAX(pers_id)+1 INTO persid FROM v_personne;--+1

SELECT SEQ_PERSONNE.NEXTVAL INTO persid FROM dual;

INSERT INTO STRUCTURE_ULR VALUES (
  cstructure,
  persid,
  INS_LL_STRUCTURE,
  INS_LC_STRUCTURE,
  INS_C_TYPE_STRUCTURE,
  INS_C_STRUCTURE_PERE,
  INS_C_TYPE_ETABLISSEMEN,
  INS_C_ACADEMIE,
  INS_C_STATUT_JURIDIQUE,
  INS_C_RNE,
  INS_SIRET,
  INS_SIREN,
  INS_C_NAF,
  INS_C_TYPE_DECISION_STR,
  INS_REF_EXT_ETAB,
  INS_REF_EXT_COMP,
  INS_REF_EXT_CR,
  INS_REF_DECISION,
  INS_DATE_DECISION,
  INS_DATE_OUVERTURE,
  INS_DATE_FERMETURE,
  INS_STR_ORIGINE,
  INS_STR_PHOTO,
  INS_STR_ACTIVITE,
  INS_GRP_OWNER,
  INS_GRP_RESPONSABLE,
  INS_GRP_FORME_JURIDIQUE,
  INS_GRP_CAPITAL,
  INS_GRP_CA,
  INS_GRP_EFFECTIFS,
  INS_GRP_CENTRE_DECISION,
  INS_GRP_APE_CODE,
  INS_GRP_APE_CODE_BIS,
  INS_GRP_APE_CODE_COMP,
  INS_GRP_ACCES,
  INS_GRP_ALIAS,
  INS_GRP_RESPONSABILITE,
  INS_GRP_TRADEMARQUE,
  INS_GRP_WEBMESTRE,
  INS_GRP_FONCTION1,
  INS_GRP_FONCTION2,
  INS_ORG_ORDRE,
  INS_GRP_MOTS_CLEFS,
  SYSDATE,
  SYSDATE,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  'N',
  'N',
  'O',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,      -- tva_intracom
  NULL,      -- str_accueil
  NULL,   -- str_recherche
  NULL,      -- num_rafp
  INS_LC_STRUCTURE,     -- str_affichage
  NULL,      -- gencod
  NULL,   -- pers_id_creation
  NULL,    -- pers_id_modification
  NULL,   -- STR_STATUT
  NULL,   -- ROM_ID
  NULL    -- STR_DESCRIPTION
);

END; 
/

DROP PROCEDURE GRHUM.DEL_ENQUETE_QUESTION;
DROP PROCEDURE GRHUM.INS_ENQUETE;


create or replace
PROCEDURE Del_Groupe_Diplome
(
	DEL_c_structure		IN	CHAR
)
IS
    nb      INTEGER;
	local_pers_id	INTEGER;
	local_cpt_ordre INTEGER;
	ordre	VARCHAR2(10);
	local_enq_ordre INTEGER;
	
	cpt INTEGER;

	CURSOR grp_fils(numero CHAR) IS SELECT c_structure FROM STRUCTURE_ULR WHERE c_structure_pere = numero;


BEGIN

	 	OPEN grp_fils(DEL_c_structure);
		LOOP
			FETCH grp_fils INTO ordre;
 			EXIT WHEN grp_fils%NOTFOUND;
			Del_Groupe_Diplome(ordre);
		END LOOP;

-- POUR CHAQUE FILS

	-- Recuperation du persid de la structure a supprimer
		SELECT pers_id INTO local_pers_id FROM STRUCTURE_ULR WHERE c_structure = del_c_structure;

	-- On regarde si un compte est associe pour le supprimer
		SELECT COUNT(*) INTO local_cpt_ordre FROM REPART_COMPTE WHERE pers_id = local_pers_id;

		IF (cpt > 0)
		THEN
				SELECT cpt_ordre INTO local_cpt_ordre FROM REPART_COMPTE WHERE pers_id = local_pers_id;

				DELETE FROM REPART_COMPTE WHERE pers_id = local_pers_id;
				DELETE FROM COMPTE WHERE cpt_ordre = local_cpt_ordre;
	    END IF;

	-- On enleve les personnes appartenant au groupe a supprimer
		DELETE FROM REPART_STRUCTURE WHERE c_structure = del_c_structure;

	-- On enleve tous les types du groupe
		DELETE FROM REPART_TYPE_GROUPE WHERE c_structure = del_c_structure;


	-- Depositaires
		DELETE FROM DEPOSITAIRE WHERE c_structure = del_c_structure;

	-- DELETE FROM PERSONNE
	   	DELETE FROM PERSONNE WHERE pers_id = local_pers_id;

	--  Secretariat
		DELETE FROM SECRETARIAT WHERE c_structure = del_c_structure;

	-- Suppression de la structure
		DELETE FROM STRUCTURE_ULR WHERE c_structure = del_c_structure;
END;
/

create or replace
PROCEDURE       Detruire_Personne(persid NUMBER)
-- Auteur : Cocktail
-- creation : 01/01/2001
-- modification : 28/03/2011
IS
noindividu NUMBER;
cpt INTEGER;
nbr	INTEGER;
VALUE		grhum.GRHUM_PARAMETRES.param_value%TYPE;
local_fou_ordre NUMBER;
local_cpt_ordre NUMBER;
local_adr_ordre NUMBER;
local_etud_numero NUMBER;
local_c_structure VARCHAR2(10);
local_paie_numero grhum.PAIEMENT.paie_numero%TYPE;
local_pme_key grhum.QUOTA_PME.pme_key%TYPE;
local_no_ater_dossier grhum.ATER_DOSSIER.no_ater_dossier%TYPE;


CURSOR fournisseurs(numero NUMBER) IS SELECT fou_ordre FROM FOURNIS_ULR WHERE pers_id = numero;
CURSOR comptes(numero NUMBER) IS SELECT cpt_ordre FROM COMPTE WHERE pers_id = numero;
CURSOR adresses(numero NUMBER) IS SELECT adr_ordre FROM REPART_PERSONNE_ADRESSE WHERE pers_id = numero;
CURSOR scolarite(numero NUMBER) IS SELECT etud_numero FROM ETUDIANT WHERE no_individu = numero;
CURSOR paiement_grhum(numero NUMBER) IS SELECT paie_numero FROM PAIEMENT WHERE pers_id = numero;
CURSOR pme(numero NUMBER) IS SELECT pme_key FROM QUOTA_PME WHERE pers_id = numero;
CURSOR ater(numero NUMBER) IS SELECT no_ater_dossier FROM ATER_DOSSIER WHERE no_individu = numero;

BEGIN

noindividu := 0;

SELECT COUNT(*) INTO cpt FROM INDIVIDU_ULR WHERE pers_id = persid;
IF (cpt = 0)
THEN
	SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR WHERE pers_id = persid;

	IF (cpt = 0)
	THEN
		RAISE_APPLICATION_ERROR(-20001,'Aucun pers_id trouve pour cet individu ou cette structure ...');
	END IF;

	SELECT c_structure INTO local_c_structure FROM STRUCTURE_ULR WHERE pers_id = persid;
    DELETE FROM REPART_TYPE_GROUPE WHERE c_structure = local_c_structure;

ELSE
	SELECT no_individu INTO noindividu FROM INDIVIDU_ULR WHERE pers_id = persid;
END IF;

-- On dispose des deux cles noindividu et persid

   -- Delete de la partie fournisseur : tables valide_fournis_ulr et fournis_ulr
	OPEN fournisseurs(persid);
	LOOP
	FETCH fournisseurs INTO local_fou_ordre;
 	EXIT WHEN fournisseurs%NOTFOUND;

		DELETE FROM VALIDE_FOURNIS_ULR WHERE fou_ordre = local_fou_ordre;
		DELETE FROM RIBFOUR_ULR WHERE fou_ordre = local_fou_ordre;
    END LOOP;
	CLOSE fournisseurs;

	DELETE FROM FOURNIS_ULR WHERE pers_id = persid;

   -- Delete de  la partie compte
	OPEN comptes(persid);
	LOOP
	FETCH comptes INTO local_cpt_ordre;
 	EXIT WHEN comptes%NOTFOUND;

		DELETE FROM GRHUM.REPART_COMPTE WHERE pers_id = persid;
        DELETE FROM GRHUM.PASSWORD_HISTORY WHERE cpt_ordre = local_cpt_ordre;
    	DELETE FROM GRHUM.COMPTE_EMAIL WHERE cpt_ordre = local_cpt_ordre;   
        DELETE FROM GRHUM.COMPTE_VALIDITE WHERE cpt_ordre= local_cpt_ordre;
        DELETE FROM GRHUM.COMPTE_SUITE WHERE cpt_ordre = local_cpt_ordre;
        DELETE FROM GRHUM.COMPTE_SAMBA WHERE cpt_ordre = local_cpt_ordre;
		DELETE FROM GRHUM.COMPTE WHERE pers_id = persid;
        
    END LOOP;
	CLOSE comptes;

   -- Delete de la partie adresses
	OPEN adresses(persid);
	LOOP
	FETCH adresses INTO local_adr_ordre;
 	EXIT WHEN adresses%NOTFOUND;

		DELETE FROM REPART_PERSONNE_ADRESSE WHERE pers_id = persid;
		DELETE FROM ADRESSE WHERE adr_ordre = local_adr_ordre;
		DELETE FROM AGENT_ADRESSES WHERE no_individu = noindividu;

    END LOOP;
	CLOSE adresses;

	-- Delete de la partie Paiement de GRHUM
	OPEN paiement_grhum(persid);
	LOOP
	FETCH paiement_grhum INTO local_paie_numero;
 	EXIT WHEN paiement_grhum%NOTFOUND;

		DELETE FROM PAIEMENT_DETAIL WHERE paie_numero = local_paie_numero;

    END LOOP;
	CLOSE paiement_grhum;
	DELETE FROM PAIEMENT WHERE pers_id = persid;
	
  -- Delete de la partie structures
   DELETE FROM REPART_STRUCTURE WHERE pers_id = persid;
   DELETE FROM DEPOSITAIRE WHERE no_individu = noindividu;
   DELETE FROM PERSONNE_TELEPHONE WHERE pers_id = persid;
   DELETE FROM SECRETARIAT WHERE no_individu = noindividu;
   DELETE FROM REPART_EMPLOI WHERE c_structure = local_c_structure;

  -- Delete de la partie Scolarite...
   OPEN scolarite(noindividu);
   LOOP
	FETCH scolarite INTO local_etud_numero;
 	EXIT WHEN scolarite%NOTFOUND;

	garnuche.Del_Dossier(local_etud_numero);

   END LOOP;
   CLOSE scolarite;
   
   -- Delete de la Photo
   SELECT COUNT(*) INTO nbr FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_PHOTO';
   IF (nbr != 0) THEN
   	  SELECT param_value INTO VALUE FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_PHOTO';
	  IF (VALUE = 'OUI') THEN
   	  	 DELETE FROM GRHUM.photos_employes_grhum WHERE no_individu = noindividu;
		 DELETE FROM GRHUM.photos_etudiants_grhum WHERE no_individu = noindividu;
   		 DELETE FROM GRHUM.photos_etudiants_old_grhum WHERE no_individu = noindividu;
	  END IF;
   END IF;
   
   -- Delete des Porte monnaie electroniques
   OPEN pme(persid);
   LOOP
   FETCH pme INTO local_pme_key;
   EXIT WHEN pme%NOTFOUND;
   		DELETE FROM QUOTA_PME_HISTO WHERE pme_key = local_pme_key;
   END LOOP;
   CLOSE pme;
   DELETE FROM QUOTA_PME WHERE pers_id = persid;
   
   -- Delete des dossiers ATER
   OPEN ater(noindividu);
   LOOP
   FETCH ater INTO local_no_ater_dossier;
   EXIT WHEN ater%NOTFOUND;
   		DELETE FROM ATER_PIECE_FOURNIE WHERE no_ater_dossier = local_no_ater_dossier;
   END LOOP;
   CLOSE ater;
   DELETE FROM ATER_DOSSIER WHERE no_individu = noindividu;
   DELETE FROM ATER_DIPLOME WHERE no_individu = noindividu;
   DELETE FROM ATER_HISTORIQUE WHERE no_individu = noindividu;
   DELETE FROM ATER_INDIVIDU_SITUATION WHERE no_individu = noindividu;
   DELETE FROM ATER_CONFIRM_MAIL WHERE no_individu = noindividu;

   -- Delete de la partie ARTT : CONGES
   GRHUM.Detruire_Personne_Conges(noindividu);

   -- Delete de la partie Personnel : MANGUE
   GRHUM.Detruire_Personne_Mangue(noindividu);

   -- Delete partie SupAnn
   DELETE FROM INDIVIDU_PSEUDO WHERE no_individu=noindividu;
   DELETE FROM SUPANN_REPART_ROLE WHERE no_individu=noindividu;
   DELETE FROM SUPANN_REPART_CATEGORIE WHERE no_individu=noindividu;
   	  
  -- Delete des Autres...
   DELETE FROM ABSENCES WHERE no_individu = noindividu;
   
   
   DELETE FROM INDIVIDU_ULR WHERE pers_id = persid;
   DELETE FROM STRUCTURE_ULR WHERE pers_id = persid;
   DELETE FROM PERSONNE_ALIAS WHERE pers_id = persid;
   DELETE FROM INDIVIDU_PSEUDO WHERE no_individu=noindividu;
   DELETE FROM PERSONNE WHERE pers_id = persid;

END;
/



create or replace
PROCEDURE       Ins_Personne_Telephone
(
INS_PERS_ID         NUMBER,
INS_NO_TELEPHONE    VARCHAR2,
INS_TYPE_NO         VARCHAR2,
INS_TYPE_TEL        VARCHAR2,
INS_D_DEB_VAL       DATE,
INS_D_FIN_VAL       DATE
)
IS
-- ------------
-- DECLARATIONS
-- ------------

cpt INTEGER;
local_no_individu INTEGER;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

  SELECT COUNT(*) INTO cpt FROM v_personne WHERE pers_id=INS_PERS_ID;
  IF (cpt=0) THEN
    RAISE_APPLICATION_ERROR(-20000,'INS_PERSONNE_TELEHONE : PERS_ID inconnu');
  END IF;

  SELECT COUNT(*) INTO cpt FROM PERSONNE_TELEPHONE
  WHERE
  pers_id=INS_PERS_ID
  AND
  no_telephone=INS_NO_TELEPHONE
  AND
  type_no=INS_TYPE_NO
  AND
  TYPE_TEL=INS_TYPE_TEL;

  IF (cpt = 0 and INS_NO_TELEPHONE is not NULL)
  THEN

      INSERT INTO PERSONNE_TELEPHONE VALUES (
      INS_PERS_ID,
      INS_NO_TELEPHONE,
      INS_TYPE_NO,
      INS_TYPE_TEL,
      INS_D_DEB_VAL,
      INS_D_FIN_VAL,
      SYSDATE,
      SYSDATE,
      NULL,                     -- INDICATIF
      NULL,                   -- C_STRUCTURE
      'N',                   -- LISTE_ROUGE
      'N'                    -- TEL_PRINCIPAL
      );
  END IF;

 

END;
/


-- Suppression de plusieurs tables de GRHUM


--DROP TABLE GRHUM.ENQUETES
--DROP TABLE GRHUM.ENQUETES_DELEGATION
--DROP TABLE GRHUM.ENQUETES_GROUPES_SOURCE
--DROP TABLE GRHUM.ENQUETES_IMAGES
--DROP TABLE GRHUM.ENQUETES_INDIVIDU_CREATEUR
--DROP TABLE GRHUM.ENQUETES_INDIVIDU_STRUCTURE
--DROP TABLE GRHUM.ENQUETES_INSC_DELEGUEES


--DROP TABLE GRHUM.ENQUETES_PAIEMENT
--DROP TABLE GRHUM.ENQUETES_PAIEMENT_PAYBOX
--DROP TABLE GRHUM.ENQUETES_PAIEMENT_SAVE
--DROP TABLE GRHUM.ENQUETES_PAIEMENT_TYPE
--DROP TABLE GRHUM.ENQUETES_QCM
--DROP TABLE GRHUM.ENQUETES_QUESTIONS
--DROP TABLE GRHUM.ENQUETES_QUESTIONS_LIMITE
--DROP TABLE GRHUM.ENQUETES_QUESTIONS_RADIO
--DROP TABLE GRHUM.ENQUETES_REDUC_PAIEMENT
--DROP TABLE GRHUM.ENQUETES_REPART_ART_DROIT
--DROP TABLE GRHUM.ENQUETES_REPART_CAT_EVENT
--DROP TABLE GRHUM.ENQUETES_REPART_STRUCTURE
--DROP TABLE GRHUM.ENQUETES_REPONSES_DELEGUEE
--DROP TABLE GRHUM.ENQUETES_STAT
--DROP TABLE GRHUM.ENQUETES_TYPE_REPONSES
--DROP TABLE GRHUM.ENQUETES_TYPES


declare

   c int;

begin



   select count(*) into c from user_tables where table_name = upper('ENQUETES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES CASCADE CONSTRAINTS 
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_SEQ
      ';
   end if;



   select count(*) into c from user_tables where table_name = upper('ENQUETES_DELEGATION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_DELEGATION CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_DELEGATION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_DELEGATION_SEQ
      ';
   end if;



   select count(*) into c from user_tables where table_name = upper('ENQUETES_GROUPES_SOURCE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_GROUPES_SOURCE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_GROUPES_SOURCE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_GROUPES_SOURCE_SEQ
      ';
   end if;
   
   
   
   
   
   

   select count(*) into c from user_tables where table_name = upper('ENQUETES_IMAGES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_IMAGES CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_IMAGES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_IMAGES_SEQ
      ';
   end if;
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_INDIVIDU_CREATEUR');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_INDIVIDU_CREATEUR CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_INDIVIDU_CREATEUR_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_INDIVIDU_CREATEUR_SEQ
      ';
   end if;
   
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_INDIVIDU_STRUCTURE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_INDIVIDU_STRUCTURE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_INDIVIDU_STRUCTURE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_INDIVIDU_STRUCTURE_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_INSC_DELEGUEES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_INSC_DELEGUEES CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_INSC_DELEGUEES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_INSC_DELEGUEES_SEQ
      ';
   end if;
   
   
   
 -- -----------------------------------------------------------------------------------------------------  
 -- -----------------------------------------------------------------------------------------------------   

   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_PAIEMENT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_PAIEMENT CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_PAIEMENT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_PAIEMENT_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_PAIEMENT_PAYBOX');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_PAIEMENT_PAYBOX CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_PAIEMENT_PAYBOX_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_PAIEMENT_PAYBOX_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_PAIEMENT_SAVE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_PAIEMENT_SAVE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_PAIEMENT_SAVE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_PAIEMENT_SAVE_SEQ
      ';
   end if;
   
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_PAIEMENT_TYPE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_PAIEMENT_TYPE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_PAIEMENT_TYPE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_PAIEMENT_TYPE_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_QCM');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_QCM CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_QCM_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_QCM_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_QUESTIONS');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_QUESTIONS CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_QUESTIONS_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_QUESTIONS_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_QUESTIONS_LIMITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_QUESTIONS_LIMITE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_QUESTIONS_LIMITE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_QUESTIONS_LIMITE_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_QUESTIONS_RADIO');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_QUESTIONS_RADIO CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_QUESTIONS_RADIO_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_QUESTIONS_RADIO_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_REDUC_PAIEMENT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_REDUC_PAIEMENT CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_REDUC_PAIEMENT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_REDUC_PAIEMENT_SEQ
      ';
   end if;
   
   
 -- -----------------------------------------------------------------------------------------------------  
 -- -----------------------------------------------------------------------------------------------------   
  
--DROP TABLE GRHUM.ENQUETES_REPART_ART_DROIT
--DROP TABLE GRHUM.ENQUETES_REPART_CAT_EVENT
--DROP TABLE GRHUM.ENQUETES_REPART_STRUCTURE
--DROP TABLE GRHUM.ENQUETES_REPONSES_DELEGUEE
--DROP TABLE GRHUM.ENQUETES_STAT
--DROP TABLE GRHUM.ENQUETES_TYPE_REPONSES
--DROP TABLE GRHUM.ENQUETES_TYPES


   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_REPART_ART_DROIT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_REPART_ART_DROIT CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_REPART_ART_DROIT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_REPART_ART_DROIT_SEQ
      ';
   end if;
   
   
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_REPART_CAT_EVENT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_REPART_CAT_EVENT CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_REPART_CAT_EVENT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_REPART_CAT_EVENT_SEQ
      ';
   end if;
   
     
   select count(*) into c from user_tables where table_name = upper('ENQUETES_REPART_STRUCTURE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_REPART_STRUCTURE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_REPART_STRUCTURE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_REPART_STRUCTURE_SEQ
      ';
   end if;
   
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_REPONSES_DELEGUEE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_REPONSES_DELEGUEE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_REPONSES_DELEGUEE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_REPONSES_DELEGUEE_SEQ
      ';
   end if;
   
   
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_TYPES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_TYPES CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_TYPES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_TYPES_SEQ
      ';
   end if;
   
   
     
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_TYPE_REPONSES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_TYPE_REPONSES CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_TYPE_REPONSES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_TYPE_REPONSES_SEQ
      ';
   end if;
   
   
   
   
   select count(*) into c from user_tables where table_name = upper('ENQUETES_STAT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ENQUETES_STAT CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('ENQUETES_STAT_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.ENQUETES_STAT_SEQ
      ';
   end if;
  
end;
/







COMMIT;
