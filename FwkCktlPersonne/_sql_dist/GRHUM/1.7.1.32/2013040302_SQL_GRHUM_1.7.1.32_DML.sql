--
-- Patch DML de GRHUM du 03/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 03/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- Correction d'une Commune
--


EXECUTE GRHUM.MAJ_COMMUNE('84007','MONTFAVET','MONTFAVET','84140','MONTFAVET');




--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.32';




COMMIT;