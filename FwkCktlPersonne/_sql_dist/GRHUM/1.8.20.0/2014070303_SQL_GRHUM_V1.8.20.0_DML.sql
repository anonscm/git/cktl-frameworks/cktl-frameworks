--
-- Patch DML de GRHUM du 03/07/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.20.0
-- Date de publication : 03/07/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-------------------------V20140612.180733__DML_maj_date_ouverture_CASU.sql--------------------------

--------------------------------------------------------
-- Mise à jour de la date d'ouverture du corps des CASU
--------------------------------------------------------

UPDATE GRHUM.CORPS
SET GRHUM.CORPS.D_OUVERTURE_CORPS = to_date('01/01/1900', 'dd/mm/yyyy'), D_MODIFICATION = SYSDATE
WHERE C_CORPS = '031';

------------------------V20140613.170338__DML_Maj_conf_serveur_planning.sql-------------------------
UPDATE GRHUM.SP_MET_SERVEUR SET SER_URI = '/wa/DAAgenda/horairesPourPeriode?' WHERE MET_KEY=102;
UPDATE GRHUM.SP_MET_SERVEUR SET SER_URI = '/wa/DAAgenda/presencesDemiJourneesPourPeriode?' WHERE MET_KEY=105;
UPDATE GRHUM.SP_MET_SERVEUR SET SER_URI = '/wa/DAAgenda/occupationsPourPeriode?' WHERE MET_KEY=106;
------------------V20140702.165009__DML_AJOUT_PARAM_SWITCH_TRIG_REPART_COMPTE.sql-------------------

--Insertion du parametre definissant si l'on doit fonctionner comment auparavant ou non pour les dates de début de validité des comptes
declare 
    persIdCreateur  integer;
    nomParametre	VARCHAR2(512);   
    itemCount		integer;
    valueParamId	integer; 
begin
    -- persIdCreation
    SELECT PERS_ID into persIdCreateur
               FROM GRHUM.COMPTE
        WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES
                                                WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));
    -- paramTypeId
    SELECT TYPE_ID into valueParamId
               FROM GRHUM.GRHUM_PARAMETRES_TYPE
        WHERE TYPE_ID_INTERNE = 'CODE_ACTIVATION';                                            
    
    nomParametre := 'org.cocktail.grhum.comptes.tenircomptedesdatesdanstriggerrepartcompte';

    select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where  PARAM_KEY = nomParametre;
    if itemCount =0 then                                           
      INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) 
        values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,nomParametre,'N','Switch pour désactiver la validité si la date de début de validité est à null (Déafut à N)',persIdCreateur,persIdCreateur,sysdate,sysdate,valueParamId);
    end if;
end;
/
-------------------------------V20140702.170111__DML_AJOUT_UN_UAI.sql-------------------------------
Insert into RNE
(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE,
D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION, LC_RNE,
ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE,
ADR_ORDRE, SIRET)
Values
('3900005L', 'LYCEE DES MASCAREIGNES (L)', 'ILE MAURICE', NULL, NULL,
TO_DATE('07/01/2014 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), NULL, TO_DATE('07/01/2014 14:02:10', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('07/01/2014 11:22:09', 'MM/DD/YYYY HH24:MI:SS'), 'LFE',
NULL, 'EFE', NULL, 'PU', NULL,
NULL, NULL);

-------------------------------V20140702.170146__DML_AJOUT_UN_FLX.sql-------------------------------
-- Re-initialisation des tables fille et de la table mere

insert into GRHUM.FLX_FLUX values ('BDF','Flux Banque de France', null);

insert into GRHUM.FLX_TABLES values('BDF','GRHUM','BANQUE','Mise à jour');

----------------------V20140702.170435__DML_MAJ_DATE_STRUCTURES_PISTACHES.sql-----------------------
DECLARE
	datePatch	VARCHAR2(20);
	persIdCreateur INTEGER;

BEGIN
	SELECT dbv_install INTO datePatch
	FROM stages.db_version dbv
	WHERE dbv.dbv_libelle = '1.0.0.0';
	
	SELECT pers_id INTO persIdCreateur FROM grhum.individu_ulr WHERE pers_id =
     (SELECT PERS_ID FROM GRHUM.COMPTE WHERE UPPER(CPT_LOGIN) =
        (SELECT UPPER(PARAM_VALUE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')));
	
	UPDATE GRHUM.STRUCTURE_ULR s
	SET s.d_creation = to_date(datePatch,'DD/MM/YYYY'),
		s.d_modification = to_date(datePatch,'DD/MM/YYYY'),
		s.pers_id_creation = persIdCreateur,
		s.pers_id_modification = persIdCreateur
	WHERE s.lc_structure in ('ORG_ACC-ANNULES','ORG_ACC-A_VALIDER','ORG_ACC-VALIDES');
COMMIT;

END;
/
----------------------------V20140702.170447__DML_AJOUT_REST_REQUETE.sql----------------------------
DECLARE
    sqlReq VARCHAR2(32767);
BEGIN

    delete from GRHUM.REST_REQUETE;
    
    -------------------------------------------------------------------
    --  cocktail.gfc.filter.exercice.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.exercice.list', 'Liste des exercices', 'GFC', 'Liste des exercices existant en GFC classés par ordre décroissant', 
    'Liste des exercices existant', 'select exe_ordre as id, exe_exercice as value from jefy_admin.exercice where exe_exercice >= nvl($P_IUN{exercice.debut},0) order by exe_exercice desc');

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.convention.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.convention.list', 'Liste des conventions', 'GFC', 'Liste des conventions classées par référence externe', 
    'Liste des conventions existantes', 'select con_ordre as ID, con_reference_externe as VALUE from jefy_report.v_convention where exe_ordre = $P_IUO{exercice.id} order by con_reference_externe asc');

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.organigramme_etab.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_etab.list', 'Liste des établissements budgétaires', 'GFC', 'Liste des établissements budgétaires. Il faut spécifier l''exercice et l''utilisateur', 
    'Liste des établissements budgétaires', 'select DISTINCT etab.org_id_etab as id, o.org_etab as value
    from jefy_admin.v_etab_for_organ etab
    inner join jefy_admin.utilisateur_organ uo on (etab.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (etab.org_id_etab=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    order by o.org_etab asc');
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_ub.list', 'Liste des unités budgétaires', 'GFC', 'Liste des unités budgétaires. Il faut spécifier l''exercice, l''utilisateur et l''établissement budgétaire parent', 
    'Liste des unités budgétaires', 'select DISTINCT ub.org_id_ub as id, o.org_ub as value
    from jefy_admin.v_ub_for_organ ub
    inner join jefy_admin.utilisateur_organ uo on (ub.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (ub.org_id_ub=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    and o.org_pere=$P_IUO{organigramme_etab.id}
    order by o.org_ub asc');
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_cr.list', 'Liste des centres de responsabilité budgétaires', 'GFC', 'Liste des centres de responsabilité budgétaires. Il faut spécifier l''exercice, l''utilisateur et l''unité budgétaire parente', 
    'Liste des centres de responsabilité', 'select DISTINCT cr.org_id_cr as id, o.org_cr as value
    from jefy_admin.v_cr_for_organ cr
    inner join jefy_admin.utilisateur_organ uo on (cr.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (cr.org_id_cr=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    and o.org_pere=$P_IUO{organigramme_ub.id}
    order by o.org_cr asc');
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_souscr.list', 'Liste des sous-centres de responsabilité budgétaires', 'GFC', 'Liste des sous-centres de responsabilité budgétaires. Il faut spécifier l''exercice, l''utilisateur et le centre de responsabilité parent', 
    'Liste des centres de responsabilité', 'select DISTINCT cr.org_id as id, o.org_souscr as value
    from jefy_admin.organ cr
    inner join jefy_admin.utilisateur_organ uo on (cr.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (cr.org_id=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    and o.org_pere=$P_IUO{organigramme_cr.id}
    order by o.org_souscr asc');
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.depense.synthese', 'Situation budgétaire en dépense par masse de crédit', 'GFC', 'Renvoie les montants crédits ouverts, restes engagés, mandatés, disponibles par masse de crédit. Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.', 
    'Situation budgétaire en dépense par masse de crédit', 'select tcd_libelle LIBELLE_CREDITS, tcd_code CREDITS, tcd_ordre ID_CREDITS, sum(bdxc_ouverts) montant_credits_ouverts, sum(bdxc_engagements) montant_engage, sum(bdxc_mandats) montant_mandate, sum(bdxc_disponible) montant_disponible
       from (select o.org_id, tcd_libelle, tcd_code, vtc.tcd_ordre, bdxc_ouverts, bdxc_engagements, bdxc_mandats, bdxc_disponible
               from (
                --suivi avec et sans convention (con_ordre fixé à -1000)
                select vo.org_id,
                vo.exe_ordre,
                -1000 as con_ordre,
                vbec.tcd_ordre,
                bdxc_ouverts,
                bdxc_engagements,
                bdxc_mandats,
                bdxc_disponible
                from jefy_admin.v_organ vo
                inner join jefy_depense.v_budget_exec_credit vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
                union all
                -- suivi par convention
                select vo.org_id,
                vo.exe_ordre,
                vbec.con_ordre,
                vbec.tcd_ordre,
                nvl(vbec.bdxc_ouverts,0),
                nvl(eng.bdxc_engagements,0) bdxc_engagements,
                nvl(mand.bdxc_mandats,0) bdxc_mandats,
                nvl(vbec.bdxc_ouverts-(nvl(bdxc_engagements,0)+nvl(bdxc_mandats,0)),0) as bdxc_disponible
                from jefy_admin.v_organ vo
                inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
                left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(ECON_MONTANT_BUDGETAIRE_RESTE) as bdxc_engagements from accords.v_suivi_eng p group by org_id, con_ordre, exe_ordre, tcd_ordre) eng on (eng.con_ordre = vbec.con_ordre and eng.org_id=vbec.org_id and eng.exe_ordre=vbec.exe_ordre and eng.tcd_ordre=vbec.tcd_ordre)
                left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(DCON_MONTANT_BUDGETAIRE) as bdxc_mandats from accords.v_suivi_dep p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
            ) x
            inner join jefy_admin.organ o on (x.org_id=o.org_id)
            inner join jefy_depense.v_type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre)
            inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
            inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
            where u.pers_id=$P_IUO{utilisateur.persid}
              and x.exe_ordre=$P_IUO{exercice.id}
              and o.org_id in (
                select $P_IUO{organigramme.id} from dual
                union all
                select org.org_id from jefy_admin.organ org
                 start with org.org_pere in (
                    select org_id from jefy_admin.organ tmpO
                     where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                   connect by prior org.org_id = org.org_pere)
              and x.con_ordre=nvl($P_IUN{convention.id}, -1000)
            order by org_id) innerQuery
     group by tcd_code, tcd_libelle, tcd_ordre 
     order by tcd_code');
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.recette.details', 'Situation budgétaire en recette par masse de crédit (Détails)', 'GFC', 'Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.', 
    'Situation budgétaire en recette par masse de crédit (Détails)', 'select o.org_id ORGANIGRAMME_ID, o.org_etab ORGANIGRAMME_ETAB, o.org_ub ORGANIGRAMME_UB, o.org_cr ORGANIGRAMME_CR, o.org_souscr ORGANIGRAMME_SOUSCR, o.org_lib ORGANIGRAMME_LIBELLE, tcd_libelle LIBELLE_CREDITS, tcd_code CREDITS, bdxc_ouverts MONTANT_CREDITS_OUVERTS, bdxc_titres MONTANT_TITRE, nvl(bdxc_ouverts, 0) - nvl(bdxc_titres, 0) MONTANT_RESTE_A_REALISER
       from (
            --suivi avec et sans convention (con_ordre fixé à -1000)
            select vo.org_id,
            vo.exe_ordre,
            -1000 as con_ordre,
            bvg.tcd_ordre ,
            nvl(BDVN_OUVERTS,0) as bdxc_ouverts,
            nvl(bdxc_titres,0) as bdxc_titres
            from jefy_admin.v_organ vo
            inner join (select org_id, exe_ordre, tcd_ordre, sum(BDVN_OUVERTS) BDVN_OUVERTS from jefy_budget.budget_vote_nature group by org_id, exe_ordre, tcd_ordre) bvg on (bvg.org_id = vo.org_id and bvg.exe_ordre=vo.exe_ordre)
            inner join jefy_admin.type_credit vtc on (bvg.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'' and tcd_code<>''00'')
            full outer join (
            select typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre,  sum(recPlanco.RPCO_HT_SAISIE ) as bdxc_titres
                from jefy_recette.recette_ctrl_planco recPlanco
                join jefy_recette.recette rec on rec.rec_id = recPlanco.rec_id
                join jefy_recette.facture fac on rec.fac_id = fac.fac_id
                join maracuja.titre titre on titre.tit_id = recPlanco.tit_id
                join jefy_admin.type_credit typeCredit on typeCredit.tcd_ordre = fac.tcd_ordre
                group by typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre
            ) x on (x.org_id = vo.org_id and x.tcd_ordre=vtc.tcd_ordre and x.exe_ordre=vo.exe_ordre)
            union all
            -- suivi par convention
            select vo.org_id,
            vo.exe_ordre,
            vbec.con_ordre,
            vbec.tcd_ordre,
            nvl(bdxc_ouverts,0) as bdxc_ouverts,
            nvl(mand.bdxc_titres,0) bdxc_titres
            from jefy_admin.v_organ vo
            inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes_rec p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by  p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
            full outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(RCON_MONTANT_BUDGETAIRE) as bdxc_titres from accords.v_suivi_rec p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
       ) x
      inner join jefy_admin.organ o on (x.org_id = o.org_id)
      inner join jefy_admin.type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'')
      inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
      inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
      where u.pers_id = $P_IUO{utilisateur.persid}
        and x.exe_ordre = $P_IUO{exercice.id}
        and o.org_id in (
                select $P_IUO{organigramme.id} from dual
                 union all
                select org.org_id
                  from jefy_admin.organ org
                 start with org.org_pere in (
                    select org_id from jefy_admin.organ tmpO
                     where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
               connect by prior org.org_id = org.org_pere )
        and x.con_ordre = nvl($P_IUN{convention.id}, -1000)
      order by o.org_ub, o.org_cr,org_souscr, tcd_code');
      
      
      
      
sqlReq := 'select case when (innerMandat.mtttc < 0  and innerMandat.mtbud < 0)then ''ORV'' when (innerMandat.mtttc < 0  and innerMandat.mtbud = 0)then ''Mandat extourne'' else ''Mandat'' end TYPE_PIECE, mandat.man_numero NUMERO,
       (select rtrim(XMLAGG(XMLELEMENT("depNum", dpp.dpp_numero_facture, '', '').extract(''//text()'')), '', '')
          from jefy_depense.depense_papier dpp join jefy_depense.depense_budget dep on dpp.dpp_id = dep.dpp_id where dep_id = innerMandat.dep_id) LIBELLE,
       fournisseur.fou_nom FOURNISSEUR, nvl(innerMandat.mtht, 0) MONTANT_HT_LIQUIDE, innerMandat.mtttc MONTANT_TTC_LIQUIDE, innerMandat.mtbud MONTANT_BUDGETAIRE_LIQUIDE,
       case mandat.man_etat
        when ''ATTENTE'' then ''Mandaté le '' || bordereau.bor_date_creation
        when ''VISE'' then ''Visé le '' || bordereau.bor_date_visa
        when ''PAYE'' then ''Payé le '' || p.pai_date_creation
        when ''ANNULE'' then ''Annulé'' || bordereau.bor_date_visa
        else ''En cours''
       end ETAT
     from (
        select man.man_id, engagement.fou_ordre, depense.dep_id, man.pai_ordre, sum(depPlanco.dpco_ht_saisie) mtht, sum(depPlanco.dpco_ttc_saisie) mtttc, sum(depPlanco.dpco_montant_budgetaire) mtbud
          from
            (-- suivi par convention
             select dep.eng_id, dep.dep_id, dep.con_ordre, dep.tcd_ordre, dep.org_id
               from accords.v_suivi_dep dep
              where dep.exe_ordre = $P_IUO{exercice.id}

            union all

            --suivi avec et sans convention (con_ordre fixé à -1000)
            select eng.eng_id, dep.dep_id, -1000, eng.tcd_ordre, eng.org_id
              from jefy_depense.depense_budget dep
              join jefy_depense.engage_budget eng on eng.eng_id = dep.eng_id
             where dep.exe_ordre = $P_IUO{exercice.id}
            ) depense
          join jefy_depense.engage_budget engagement on engagement.eng_id = depense.eng_id
          join jefy_depense.depense_ctrl_planco depPlanco on depPlanco.dep_id = depense.dep_id
          left outer join maracuja.mandat man on man.man_id = depPlanco.man_id
          join jefy_admin.organ o on (o.org_id = depense.org_id)
          join jefy_depense.v_type_credit vtc on vtc.tcd_ordre = depense.tcd_ordre
          join jefy_admin.utilisateur_organ uo on uo.org_id = o.org_id
          join jefy_admin.utilisateur u on uo.utl_ordre = u.utl_ordre
          where u.pers_id = $P_IUO{utilisateur.persid}
            and depense.con_ordre = nvl($P_IUN{convention.id}, -1000)
            and vtc.tcd_ordre = $P_IUO{typeCredit.id}
            and o.org_id in (
                  select $P_IUO{organigramme.id} from dual
                  union all
                  select org.org_id from jefy_admin.organ org
                   start with org.org_pere in (
                      select org_id from jefy_admin.organ tmpO
                       where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                     connect by prior org.org_id = org.org_pere)
          group by man.man_id, engagement.fou_ordre, depense.dep_id, man.pai_ordre) innerMandat
     left outer join maracuja.mandat mandat on mandat.man_id = innerMandat.man_id
     left outer join maracuja.bordereau bordereau on bordereau.bor_id = mandat.bor_id
     join jefy_depense.v_fournisseur fournisseur on fournisseur.fou_ordre = InnerMandat.fou_ordre
     left outer join maracuja.paiement p on p.pai_ordre = innerMandat.pai_ordre
       order by  
       case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''FOURNISSEUR'' then FOURNISSEUR end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''FOURNISSEUR'' then FOURNISSEUR end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''TYPE_PIECE'' then TYPE_PIECE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''TYPE_PIECE'' then TYPE_PIECE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''NUMERO'' then NUMERO end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''NUMERO'' then NUMERO end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''LIBELLE'' then LIBELLE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''LIBELLE'' then LIBELLE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''ETAT'' then ETAT end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''ETAT'' then ETAT end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_HT_LIQUIDE'' then MONTANT_HT_LIQUIDE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_HT_LIQUIDE'' then MONTANT_HT_LIQUIDE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_TTC_LIQUIDE'' then MONTANT_TTC_LIQUIDE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_TTC_LIQUIDE'' then MONTANT_TTC_LIQUIDE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_BUDGETAIRE_LIQUIDE'' then MONTANT_BUDGETAIRE_LIQUIDE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_BUDGETAIRE_LIQUIDE'' then MONTANT_BUDGETAIRE_LIQUIDE end desc
';      
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.depense.mandats', 'Situation budgétaire en depense detaillée par mandat et par masse de crédit', 'GFC', 'Renvoie le montant HT liquidé, le montant TTC liquidé et le montant budgétaire liquidé, le status par mandat. Il faut spécifier l''utilisateur, l''exercice, la masse de crédit, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.', 
    'Situation budgétaire en depense detaillée par mandat et par masse de crédit', 
    sqlReq);
    
    
--    
  sqlReq := 'select cmd.comm_numero CMDE_NUMERO, eng.eng_libelle ENGAGE_LIBELLE, fournisseur.fou_nom FOURNISSEUR, sum(eca.mtHt) MTHT, sum(eca.mtTTC) MTTTC, sum(eca.mtBUD) MTBUD, eng.eng_montant_budgetaire_reste,
                       decode(eng.eng_montant_budgetaire - eng.eng_montant_budgetaire_reste, 0, ''ENGAGE'', eng.eng_montant_budgetaire, ''SOLDE'', ''PARTIELLEMENT SOLDE'') status
                  from jefy_depense.engage_budget eng 
                  inner join(-- suivi par convention
                             select ec.eng_id, ec.con_ordre
                               from accords.v_suivi_eng ec
                              where ec.exe_ordre = $P_IUO{exercice.id}

                            union all

                            --suivi avec et sans convention (con_ordre fixé à -1000)
                            select e.eng_id, -1000
                              from jefy_depense.engage_budget e
                             where e.exe_ordre = $P_IUO{exercice.id}
                            ) engageConvention on engageConvention.eng_id = eng.eng_id
                  left join (select depense.eng_id, depense.con_ordre, sum(dpco.dpco_ht_saisie) mtHt, sum(dpco.dpco_ttc_saisie) mtTTC, sum(dpco.dpco_montant_budgetaire) mtBUD
                               from (-- suivi par convention
                                     select dep.eng_id, dep.dep_id, dep.con_ordre, dep.tcd_ordre, dep.org_id
                                       from accords.v_suivi_dep dep
                                      where dep.exe_ordre = $P_IUO{exercice.id}

                                    union all

                                    --suivi avec et sans convention (con_ordre fixé à -1000)
                                    select eng.eng_id, dep.dep_id, -1000, eng.tcd_ordre, eng.org_id
                                      from jefy_depense.depense_budget dep
                                      join jefy_depense.engage_budget eng on eng.eng_id = dep.eng_id
                                     where dep.exe_ordre = $P_IUO{exercice.id}
                                    ) depense
                               join jefy_depense.depense_ctrl_planco dpco on dpco.dep_id = depense.dep_id
                               join jefy_depense.v_type_credit vtc on vtc.tcd_ordre = depense.tcd_ordre
                              where vtc.tcd_ordre = $P_IUO{typeCredit.id}
                                and depense.con_ordre = nvl($P_IUN{convention.id}, -1000)
                              group by depense.eng_id, depense.con_ordre) eca on eca.eng_id = eng.eng_id
                 inner join jefy_depense.v_fournisseur fournisseur on fournisseur.fou_ordre = eng.fou_ordre
                 inner join jefy_admin.organ o on (o.org_id = eng.org_id)
                 inner join jefy_depense.v_type_credit vtc on vtc.tcd_ordre = eng.tcd_ordre
                 inner join jefy_admin.utilisateur_organ uo on uo.org_id = o.org_id
                 inner join jefy_admin.utilisateur u on uo.utl_ordre = u.utl_ordre
                  left join jefy_depense.commande_engagement ce on ce.eng_id = eng.eng_id
                  left join jefy_depense.commande cmd on ce.comm_id = cmd.comm_id
                 where u.pers_id = $P_IUO{utilisateur.persid}
                   and eng.exe_ordre = $P_IUO{exercice.id}
                   and vtc.tcd_ordre = $P_IUO{typeCredit.id}
                   and engageConvention.con_ordre = nvl($P_IUN{convention.id}, -1000)
                   and o.org_id in (
                        select $P_IUO{organigramme.id} from dual
                        union all
                        select org.org_id from jefy_admin.organ org
                            start with org.org_pere in (
                                    select org_id from jefy_admin.organ tmpO
                                     where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                                   connect by prior org.org_id = org.org_pere)
                 group by cmd.comm_id, eng.eng_id, eng.eng_libelle, eng.eng_montant_budgetaire, eng.eng_montant_budgetaire_reste, fournisseur.fou_nom, cmd.comm_numero
                 order by 
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''CMDE_NUMERO'' then cmd.comm_numero end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''CMDE_NUMERO'' then cmd.comm_numero end desc,
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''ENGAGE_LIBELLE'' then eng.eng_libelle end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''ENGAGE_LIBELLE'' then eng.eng_libelle end desc,
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''FOURNISSEUR'' then fournisseur.fou_nom end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''FOURNISSEUR'' then fournisseur.fou_nom end desc,
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''STATUT'' then status end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''STATUT'' then status end desc,
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MTHT'' then MTHT end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MTHT'' then MTHT end desc,
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MTTTC'' then MTTTC end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MTTTC'' then MTTTC end desc,
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MTBUD'' then MTBUD end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MTBUD'' then MTBUD end desc,
                    case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''ENG_MONTANT_BUDGETAIRE_RESTE'' then eng.eng_montant_budgetaire_reste end,
                    case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''ENG_MONTANT_BUDGETAIRE_RESTE'' then eng.eng_montant_budgetaire_reste end desc,
                    case when UPPER($P_SUN{ordre.sens}) = '''' and UPPER($P_SUN{ordre.colonne}) = '''' then cmd.comm_id end
';  
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.details.resteengage', 'Situation budgétaire en depense par masse de crédit (Reste engager)', 'GFC', 'Renvoie le numéro de commande, le montant HT liquidé, le montant TTC liquidé et le montant budgétaire liquidé par commande / engagement. Il faut spécifier l''utilisateur, l''exercice, la masse de crédit, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié', 
    'Situation budgétaire en depense par masse de crédit (Reste engager)', sqlReq);
    
  --  
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.conventionpourorgan.list', 'Liste des conventions pour une ligne budgétaire', 'GFC', 'Liste des conventions  pour une ligne budgétaire classées par référence externe', 
    'Liste des conventions existantes  pour une ligne budgétaire', 'select CAST(a.con_ordre AS INT) as ID, con_reference_externe as VALUE , accords.utilitaire.get_date_fin(a.con_ordre ) as DAT
                  from ACCORDS.v_suivi_depense_posit a, jefy_report.v_convention b 
                  where a.exe_ordre = $P_IUO{exercice.id} and org_id = $P_IUO{organigramme.id} and a.con_ordre =b.con_ordre 
                union 
                  select CAST(a.con_ordre AS INT) as ID, con_reference_externe as VALUE , accords.utilitaire.get_date_fin(a.con_ordre ) as DAT
                  from ACCORDS.v_suivi_recette_posit a, jefy_report.v_convention b 
                  where a.exe_ordre = $P_IUO{exercice.id} and org_id = $P_IUO{organigramme.id} and a.con_ordre =b.con_ordre 
                order by VALUE');
                
                
sqlReq :=  'select case when innerTitre.mtttc < 0  then ''REDUCTION'' else ''TITRE'' end TYPE_PIECE,mr.tit_numero NUMERO,jefy_recette.recette.rec_lib LIBELLE,
         (fournisseur.adr_nom || DECODE(fournisseur.adr_prenom, NULL, '''','' ''||fournisseur.adr_prenom)) FOURNISSEUR
       , nvl(innerTitre.mtht, 0) MONTANT_HT, innerTitre.mtttc MONTANT_TTC, innerTitre.mtbud MONTANT_BUDGETAIRE,
       case mr.tit_etat
        when ''ATTENTE'' then ''Titré le '' || bordereau.bor_date_creation
        when ''VISE'' then ''Visé le '' || bordereau.bor_date_visa
         when ''ANNULE'' then ''Annulé'' || bordereau.bor_date_visa
        else ''En cours''
       end BOR_DATE_VISAELSE__END
from (
      select  mr.tit_id,encaisse.rpco_id,encaisse.rec_id,  sum(jefy_recette.recette.rec_ht_saisie) mtht, sum(jefy_recette.recette.rec_ttc_saisie) mtttc, sum(recPlanco.rpco_ttc_saisie) mtbud
        from
            (-- suivi par convention
             select  rpco_id,rec1.rec_id, rec1.con_ordre, rec1.tcd_ordre, rec1.org_id
               from accords.v_suivi_rec rec1, jefy_recette.recette_ctrl_planco 
              where rec1.exe_ordre = $P_IUO{exercice.id} and jefy_recette.recette_ctrl_planco.rec_id=rec1.rec_id

            union all

            --suivi avec et sans convention (con_ordre fixé à -1000)
            select rpco_id,rec2.rec_id, -1000, fac.tcd_ordre, fac.org_id
              from jefy_recette.recette rec2
              join jefy_recette.facture fac on fac.fac_id = rec2.fac_id
              join jefy_recette.recette_ctrl_planco on rec2.rec_id = jefy_recette.recette_ctrl_planco.rec_id
            where rec2.exe_ordre = $P_IUO{exercice.id}
            ) encaisse
          join jefy_recette.recette on jefy_recette.recette.rec_id = encaisse.rec_id  
          join jefy_recette.recette_ctrl_planco recPlanco on recPlanco.rec_id = encaisse.rec_id
          left outer join maracuja.recette  on maracuja.recette.rec_ordre = encaisse.rpco_id
          left outer join maracuja.titre mr on mr.tit_id = maracuja.recette.tit_id
          join jefy_admin.organ o on (o.org_id = encaisse.org_id)
          join jefy_admin.type_credit vtc on vtc.tcd_ordre = encaisse.tcd_ordre
          join jefy_admin.utilisateur_organ uo on uo.org_id = o.org_id
          join jefy_admin.utilisateur u on uo.utl_ordre = u.utl_ordre
              where u.pers_id = $P_IUO{utilisateur.persid}
            and encaisse.con_ordre = nvl($P_IUN{convention.id}, -1000)
            and vtc.tcd_ordre = $P_IUO{typeCredit.id}
            and o.org_id in (
                  select  $P_IUO{organigramme.id} from dual
                  union all
                  select org.org_id from jefy_admin.organ org
                   start with org.org_pere in (
                      select org_id from jefy_admin.organ tmpO
                       where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                     connect by prior org.org_id = org.org_pere)
                  group by  mr.tit_id,encaisse.rpco_id,encaisse.rec_id) innerTitre
       left outer join maracuja.recette  on maracuja.recette.rec_ordre = innerTitre.rpco_id
      left outer  join maracuja.titre mr on mr.tit_id = maracuja.recette.tit_id
      left outer join maracuja.bordereau bordereau on bordereau.bor_id = mr.bor_id
       join jefy_recette.recette on jefy_recette.recette.rec_id = innerTitre.rec_id
       join jefy_recette.facture  on jefy_recette.facture.fac_id = jefy_recette.recette.fac_id
       join grhum.v_fournis_grhum fournisseur on fournisseur.fou_ordre = jefy_recette.facture.fou_ordre
       ';               
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.recette.titres', 'Situation budgétaire en recette détaille des titres par masse', 'GFC', 'Renvoie le montant HT , le montant TTC é et le montant budgétaire , le status par titre. Il faut spécifier l''utilisateur, l''exercice, la masse de crédit, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.', 
    'Situation budgétaire en recette détaille des titres par masse', sqlReq);
--
sqlReq := 'select tcd_libelle LIBELLE_CREDITS, tcd_code CREDITS, sum(bdxc_ouverts) MONTANT_CREDITS_OUVERTS, sum(bdxc_titres) MONTANT_TITRE, sum(bdxc_ouverts) - sum(bdxc_titres) MONTANT_RESTE_A_REALISER
       from (select o.org_id, con_ordre, tcd_libelle, tcd_code, bdxc_ouverts, bdxc_titres, nvl(bdxc_ouverts, 0) - nvl(bdxc_titres,0)
               from (
                    --suivi avec et sans convention (con_ordre fixé à -1000)
                    select vo.org_id,
                    vo.exe_ordre,
                    -1000 as con_ordre,
                    bvg.tcd_ordre ,
                    nvl(BDVN_OUVERTS,0) as bdxc_ouverts,
                    nvl(bdxc_titres,0) as bdxc_titres
                    from jefy_admin.v_organ vo
                    inner join (select org_id, exe_ordre, tcd_ordre, sum(BDVN_OUVERTS) BDVN_OUVERTS from jefy_budget.budget_vote_nature group by org_id, exe_ordre, tcd_ordre) bvg on (bvg.org_id = vo.org_id and bvg.exe_ordre=vo.exe_ordre)
                    inner join jefy_admin.type_credit vtc on (bvg.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'' and tcd_code<>''00'')
                    full outer join (
                    select typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre,  sum(recPlanco.RPCO_HT_SAISIE ) as bdxc_titres
                        from jefy_recette.recette_ctrl_planco recPlanco
                        join jefy_recette.recette rec on rec.rec_id = recPlanco.rec_id
                        join jefy_recette.facture fac on rec.fac_id = fac.fac_id
                        join maracuja.titre titre on titre.tit_id = recPlanco.tit_id
                        join jefy_admin.type_credit typeCredit on typeCredit.tcd_ordre = fac.tcd_ordre
                        group by typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre
                    ) x on (x.org_id = vo.org_id and x.tcd_ordre=vtc.tcd_ordre and x.exe_ordre=vo.exe_ordre)
                    union all
                    -- suivi par convention
                    select vo.org_id,
                    vo.exe_ordre,
                    vbec.con_ordre,
                    vbec.tcd_ordre,
                    nvl(bdxc_ouverts,0) as bdxc_ouverts,
                    nvl(mand.bdxc_titres,0) bdxc_titres
                    from jefy_admin.v_organ vo
                    inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes_rec p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by  p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
                    full outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(RCON_MONTANT_BUDGETAIRE) as bdxc_titres from accords.v_suivi_rec p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
               ) x
              inner join jefy_admin.organ o on (x.org_id = o.org_id)
              inner join jefy_admin.type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'')
              inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
              inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
              where u.pers_id = $P_IUO{utilisateur.persid}
                and x.exe_ordre = $P_IUO{exercice.id}
                and o.org_id in (
                        select $P_IUO{organigramme.id} from dual
                         union all
                        select org.org_id
                          from jefy_admin.organ org
                         start with org.org_pere in (
                            select org_id from jefy_admin.organ tmpO
                             where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                       connect by prior org.org_id = org.org_pere )
                and x.con_ordre = nvl($P_IUN{convention.id}, -1000)
              order by org_id) innerQuery
     group by tcd_code, tcd_libelle
     order by tcd_code';    
    
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.recette.synthese', 'Situation budgétaire en recette par masse de crédit (Synthèse)', 'GFC', 'Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention  ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.', 
    'Situation budgétaire en recette par masse de crédit (Synthèse)', sqlReq);
--
sqlReq := 'select o.org_id ORGANIGRAMME_ID, o.org_etab ORGANIGRAMME_ETAB, o.org_ub ORGANIGRAMME_UB, o.org_cr ORGANIGRAMME_CR, o.org_souscr ORGANIGRAMME_SOUSCR, vtc.tcd_ordre TYPECREDIT_ID, vtc.tcd_libelle LIBELLE_CREDITS, vtc.tcd_code CREDITS, bdxc_ouverts MONTANT_CREDITS_OUVERTS, bdxc_engagements MONTANT_ENGAGE, bdxc_mandats MONTANT_MANDATE, bdxc_disponible MONTANT_DISPONIBLE
       from (
        --suivi avec et sans convention (con_ordre fixé à -1000)
        select vo.org_id,
        vo.exe_ordre,
        -1000 as con_ordre,
        vbec.tcd_ordre,
        bdxc_ouverts,
        bdxc_engagements,
        bdxc_mandats,
        bdxc_disponible
        from jefy_admin.v_organ vo
        inner join jefy_depense.v_budget_exec_credit vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
        union all
        -- suivi par convention
        select vo.org_id,
        vo.exe_ordre,
        vbec.con_ordre,
        vbec.tcd_ordre,
        vbec.bdxc_ouverts,
        nvl(eng.bdxc_engagements,0) bdxc_engagements,
        nvl(mand.bdxc_mandats,0) bdxc_mandats,
        nvl(vbec.bdxc_ouverts-(nvl(bdxc_engagements,0)+nvl(bdxc_mandats,0)),0) as bdxc_disponible
        from jefy_admin.v_organ vo
        inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
        left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(ECON_MONTANT_BUDGETAIRE_RESTE) as bdxc_engagements from accords.v_suivi_eng p group by org_id, con_ordre, exe_ordre, tcd_ordre) eng on (eng.con_ordre = vbec.con_ordre and eng.org_id=vbec.org_id and eng.exe_ordre=vbec.exe_ordre and eng.tcd_ordre=vbec.tcd_ordre)
        left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(DCON_MONTANT_BUDGETAIRE) as bdxc_mandats from accords.v_suivi_dep p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
    ) x
    inner join jefy_admin.organ o on (x.org_id = o.org_id)
    inner join jefy_depense.v_type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre)
    inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
    where u.pers_id = $P_IUO{utilisateur.persid}
      and x.exe_ordre=$P_IUO{exercice.id}
      and o.org_id in (
            select $P_IUO{organigramme.id} from dual
            union all
            select org.org_id from jefy_admin.organ org
             start with org.org_pere in (
                select org_id from jefy_admin.organ tmpO
                 where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
               connect by prior org.org_id = org.org_pere)
      and x.con_ordre = nvl($P_IUN{convention.id}, -1000)
    order by o.org_id, tcd_code';
    
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.depense.details', 'Situation budgétaire en dépense par ligne budgétaire et masse de crédit', 'GFC', 'Renvoie les montants crédits ouverts, restes engagés, mandatés, disponibles par ligne budgétaire et par masse de crédit. Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention  ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.', 
    'Situation budgétaire en dépense par ligne budgétaire et masse de crédit', sqlReq);
    
--
sqlReq := 'select type_piece, numero , libelle, fournisseur, sum(montant_ht_liquide) montant_ht_liquide,sum(montant_ttc_liquide) montant_ttc_liquide,sum(montant_budgetaire_liquide) montant_budgetaire_liquide,  ETAT from 
(select case when (innerMandat.mtttc < 0  and innerMandat.mtbud < 0)then ''ORV'' when (innerMandat.mtttc < 0  and innerMandat.mtbud = 0)then ''Mandat extourne'' else ''Mandat'' end TYPE_PIECE, mandat.man_numero NUMERO,
       (select rtrim(XMLAGG(XMLELEMENT("depNum", dpp.dpp_numero_facture, '', '').extract(''//text()'')), '', '')
          from jefy_depense.depense_papier dpp join jefy_depense.depense_budget dep on dpp.dpp_id = dep.dpp_id where dep_id = innerMandat.dep_id) LIBELLE,
       fournisseur.fou_nom FOURNISSEUR, nvl(innerMandat.mtht, 0) MONTANT_HT_LIQUIDE, innerMandat.mtttc MONTANT_TTC_LIQUIDE, innerMandat.mtbud MONTANT_BUDGETAIRE_LIQUIDE,
      ( case mandat.man_etat
        when ''ATTENTE'' then ''Mandaté le '' || bordereau.bor_date_creation 
        when ''VISE'' then ''Visé le '' || bordereau.bor_date_visa 
        when ''PAYE'' then ''Payé le '' || p.pai_date_creation 
        when ''ANNULE'' then ''Annulé'' || bordereau.bor_date_visa 
        else ''En cours''
       end) as ETAT
     from (
        select man.man_id, engagement.fou_ordre, depense.dep_id, man.pai_ordre, sum(depPlanco.dpco_ht_saisie) mtht, sum(depPlanco.dpco_ttc_saisie) mtttc, sum(depPlanco.dpco_montant_budgetaire) mtbud
          from
            (-- suivi par convention
             select dep.eng_id, dep.dep_id, dep.con_ordre, dep.tcd_ordre, dep.org_id
               from accords.v_suivi_dep dep
              where dep.exe_ordre = $P_IUO{exercice.id}

            union all

            --suivi avec et sans convention (con_ordre fixé à -1000)
            select eng.eng_id, dep.dep_id, -1000, eng.tcd_ordre, eng.org_id
              from jefy_depense.depense_budget dep
              join jefy_depense.engage_budget eng on eng.eng_id = dep.eng_id
             where dep.exe_ordre = $P_IUO{exercice.id}
            ) depense
          join jefy_depense.engage_budget engagement on engagement.eng_id = depense.eng_id
          join jefy_depense.depense_ctrl_planco depPlanco on depPlanco.dep_id = depense.dep_id
          left outer join maracuja.mandat man on man.man_id = depPlanco.man_id
          join jefy_admin.organ o on (o.org_id = depense.org_id)
          join jefy_depense.v_type_credit vtc on vtc.tcd_ordre = depense.tcd_ordre
          join jefy_admin.utilisateur_organ uo on uo.org_id = o.org_id
          join jefy_admin.utilisateur u on uo.utl_ordre = u.utl_ordre
          where u.pers_id = $P_IUO{utilisateur.persid}
            and depense.con_ordre = nvl($P_IUN{convention.id}, -1000)
            and vtc.tcd_ordre = $P_IUO{typeCredit.id}
            and o.org_id in (
                  select $P_IUO{organigramme.id} from dual
                  union all
                  select org.org_id from jefy_admin.organ org
                   start with org.org_pere in (
                      select org_id from jefy_admin.organ tmpO
                       where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                     connect by prior org.org_id = org.org_pere)
          group by man.man_id, engagement.fou_ordre, depense.dep_id, man.pai_ordre) innerMandat
     left outer join maracuja.mandat mandat on mandat.man_id = innerMandat.man_id
     left outer join maracuja.bordereau bordereau on bordereau.bor_id = mandat.bor_id
     join jefy_depense.v_fournisseur fournisseur on fournisseur.fou_ordre = InnerMandat.fou_ordre
     left outer join maracuja.paiement p on p.pai_ordre = innerMandat.pai_ordre) 
     totalmandat
     group by type_piece, numero , libelle, fournisseur,etat
order by  
       case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''FOURNISSEUR'' then FOURNISSEUR end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''FOURNISSEUR'' then FOURNISSEUR end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''TYPE_PIECE'' then TYPE_PIECE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''TYPE_PIECE'' then TYPE_PIECE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''NUMERO'' then NUMERO end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''NUMERO'' then NUMERO end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''LIBELLE'' then LIBELLE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''LIBELLE'' then LIBELLE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''ETAT'' then ETAT end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''ETAT'' then ETAT end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_HT_LIQUIDE'' then MONTANT_HT_LIQUIDE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_HT_LIQUIDE'' then MONTANT_HT_LIQUIDE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_TTC_LIQUIDE'' then MONTANT_TTC_LIQUIDE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_TTC_LIQUIDE'' then MONTANT_TTC_LIQUIDE end desc,
        case when UPPER($P_SUN{ordre.sens}) = ''ASC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_BUDGETAIRE_LIQUIDE'' then MONTANT_BUDGETAIRE_LIQUIDE end,
        case when UPPER($P_SUN{ordre.sens}) = ''DESC'' and UPPER($P_SUN{ordre.colonne}) = ''MONTANT_BUDGETAIRE_LIQUIDE'' then MONTANT_BUDGETAIRE_LIQUIDE end desc
';    
    
Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR, 
    COMMENTAIRE_UTILISATEUR, SQL_REQ)
 Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.depense.mandats.salaire', 'Situation budgétaire en depense detaillée par mandat et crédit masse salariale', 'GFC', 'Renvoie le montant HT liquidé, le montant TTC liquidé et le montant budgétaire liquidé, le status par mandat. Il faut spécifier l''utilisateur, l''exercice, la masse de crédit, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.', 
    'Situation budgétaire en depense detaillée par mandat ', sqlReq);
COMMIT;

end;
/


--------------------------------V20140702.174102__DML_UPDATE_RNE.sql--------------------------------
-- MAJ GRHUM.RNE.TETAB_CODE

update grhum.rne
set tetab_code='UNI'
where ll_rne like 'UNIVERSITE%'
and tetab_code is null;

update grhum.rne
set tetab_code='LTO'
where ll_rne like 'LYCEE TECHNOLOGIQUE%'
and tetab_code is null;

update grhum.rne
set tetab_code='LA'
where ll_rne like 'LYCEE AGRICOLE%'
and tetab_code is null;

update grhum.rne
set tetab_code='LPO'
where ll_rne like 'LYCEE POLYVALENT%'
and tetab_code is null;

update grhum.rne
set tetab_code='LG'
where ll_rne like 'LYCEE GENERAL%'
and ll_rne not like 'LYCEE GENERAL ET TECHNOLOGIQUE%'
and tetab_code is null;

update grhum.rne
set tetab_code='LGT'
where ll_rne like 'LYCEE GENERAL ET TECHNOLOGIQUE%'
and tetab_code is null;

update grhum.rne
set tetab_code='LP'
where ll_rne like 'LYCEE PROFESSIONNEL%'
and tetab_code is null;

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.20.0',to_date('03/07/2014','DD/MM/YYYY'),sysdate,'MAJ de procédures, de tables, de triggers');
COMMIT;
