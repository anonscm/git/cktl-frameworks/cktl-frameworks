--
-- Patch DDL de GRHUM du 03/07/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.20.0
-- Date de publication : 03/07/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------V20140612.103931__DDL_ajout_colonne_table_mention.sql------------------------
-- ---------------------------------------------------------
-- Modification de la table DISCIPINE
-- ---------------------------------------------------------

declare
begin
	GRHUM.ADM_ADD_COLUMN('GRHUM', 'MENTION', 'VALIDE', 'NUMBER(1)', 1, 'NOT NULL');
	
end;
/
ALTER TABLE GRHUM.MENTION MOVE;
ALTER INDEX GRHUM.PK_MENTION REBUILD TABLESPACE INDX_GRHUM;

COMMENT ON COLUMN GRHUM.MENTION.VALIDE IS 'Témoin de validité de la mention ; les valeurs possibles sont : 1 (OUI) et 0 (NON) ; par défaut on le positionne à 1.';


-----------------------V20140617.103811__DDL_supression_sequence_etudiant.sql-----------------------
-- ---------------------------------------------------------------------------
-- Suppression de la séquence GRHUM.ETUDIANT_SEQ 
-- 	
-- ---------------------------------------------------------------------------


DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur
	  FROM ALL_SEQUENCES
         WHERE SEQUENCE_OWNER = 'GRHUM'
           AND SEQUENCE_NAME = 'ETUDIANT_SEQ';  

  	IF (compteur > 0)
  	  THEN              
	 	EXECUTE IMMEDIATE  'DROP SEQUENCE GRHUM.ETUDIANT_SEQ';	
 
	END IF;
END;
/
------------------------------V20140702.094428__DDL_Ajout_id_rnsr.sql-------------------------------
-- ---------------------------------------------------------
-- Modification de la table STRUCTURE_ULR
-- ---------------------------------------------------------

declare
begin
	GRHUM.ADM_ADD_COLUMN('GRHUM', 'STRUCTURE_ULR', 'ID_RNSR', 'VARCHAR2(10)', NULL, NULL);
end;
/
ALTER TABLE GRHUM.STRUCTURE_ULR MOVE;
ALTER INDEX GRHUM.PK_STRUCTURE REBUILD TABLESPACE INDX_GRHUM;

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.ID_RNSR IS 'Identifiant RNSR (répertoire national des structures de recherche)';


----------------------V20140702.102456__DDL_Modif_proc_ins_struct_url_sync.sql----------------------
create or replace PROCEDURE             INS_STRUCTURE_ULR_SYNC
(
  cstructure                OUT STRUCTURE_ULR.c_structure%TYPE,
  persid                    OUT STRUCTURE_ULR.pers_id%TYPE,
  INS_LL_STRUCTURE          STRUCTURE_ULR.ll_structure%TYPE,
  INS_LC_STRUCTURE          STRUCTURE_ULR.lc_structure%TYPE,
  INS_C_TYPE_STRUCTURE      STRUCTURE_ULR.c_type_structure%TYPE,
  INS_C_STRUCTURE_PERE      STRUCTURE_ULR.c_structure_pere%TYPE,
  INS_C_TYPE_ETABLISSEMEN   STRUCTURE_ULR.c_type_etablissemen%TYPE,
  INS_C_ACADEMIE            STRUCTURE_ULR.c_academie%TYPE,
  INS_C_STATUT_JURIDIQUE    STRUCTURE_ULR.c_statut_juridique%TYPE,
  INS_C_RNE                 STRUCTURE_ULR.c_rne%TYPE,
  INS_SIRET                 STRUCTURE_ULR.siret%TYPE,
  INS_SIREN                 STRUCTURE_ULR.siren%TYPE,
  INS_C_NAF                 STRUCTURE_ULR.c_naf%TYPE,
  INS_C_TYPE_DECISION_STR   STRUCTURE_ULR.c_type_decision_str%TYPE,
  INS_REF_EXT_ETAB          STRUCTURE_ULR.ref_ext_etab%TYPE,
  INS_REF_EXT_COMP          STRUCTURE_ULR.ref_ext_comp%TYPE,
  INS_REF_EXT_CR            STRUCTURE_ULR.ref_ext_cr%TYPE,
  INS_REF_DECISION          STRUCTURE_ULR.ref_decision%TYPE,
  INS_DATE_DECISION         STRUCTURE_ULR.date_decision%TYPE,
  INS_DATE_OUVERTURE        STRUCTURE_ULR.date_ouverture%TYPE,
  INS_DATE_FERMETURE        STRUCTURE_ULR.date_fermeture%TYPE,
  INS_STR_ORIGINE            STRUCTURE_ULR.str_origine%TYPE,
  INS_STR_PHOTO              STRUCTURE_ULR.str_photo%TYPE,
  INS_STR_ACTIVITE            STRUCTURE_ULR.str_activite%TYPE,
  INS_GRP_OWNER             STRUCTURE_ULR.grp_owner%TYPE,
  INS_GRP_RESPONSABLE        STRUCTURE_ULR.grp_responsable%TYPE,
  INS_GRP_FORME_JURIDIQUE     STRUCTURE_ULR.grp_forme_juridique%TYPE,
  INS_GRP_CAPITAL              STRUCTURE_ULR.grp_capital%TYPE,
  INS_GRP_CA                  STRUCTURE_ULR.grp_ca%TYPE,
  INS_GRP_EFFECTIFS         STRUCTURE_ULR.grp_effectifs%TYPE,
  INS_GRP_CENTRE_DECISION    STRUCTURE_ULR.grp_centre_decision%TYPE,
  INS_GRP_APE_CODE          STRUCTURE_ULR.grp_ape_code%TYPE,
  INS_GRP_APE_CODE_BIS      STRUCTURE_ULR.grp_ape_code_bis%TYPE,
  INS_GRP_APE_CODE_COMP      STRUCTURE_ULR.grp_ape_code_comp%TYPE,
  INS_GRP_ACCES             STRUCTURE_ULR.grp_acces%TYPE,
  INS_GRP_ALIAS             STRUCTURE_ULR.grp_alias%TYPE,
  INS_GRP_RESPONSABILITE     STRUCTURE_ULR.grp_responsabilite%TYPE,
  INS_GRP_TRADEMARQUE         STRUCTURE_ULR.grp_trademarque%TYPE,
  INS_GRP_WEBMESTRE         STRUCTURE_ULR.grp_webmestre%TYPE,
  INS_GRP_FONCTION1         STRUCTURE_ULR.grp_fonction1%TYPE,
  INS_GRP_FONCTION2         STRUCTURE_ULR.grp_fonction2%TYPE,
  INS_ORG_ORDRE             STRUCTURE_ULR.org_ordre%TYPE,
  INS_GRP_MOTS_CLEFS        STRUCTURE_ULR.grp_mots_clefs%TYPE
)
IS
-- ------------
-- DECLARATIONS
-- ------------
cpt INTEGER;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

/*SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
WHERE ll_structure=ins_ll_structure;
IF (cpt>0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_STRUCTURE_ULR : Libelle existant');
END IF;*/

IF (ins_c_structure_pere IS NOT NULL) THEN
    SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
    WHERE c_structure=ins_c_structure_pere;
    IF (cpt=0) THEN
       RAISE_APPLICATION_ERROR(-20001,'INS_STRUCTURE_ULR : C_structure_pere inconnu');
    END IF;
END IF;


--SELECT MAX(TO_NUMBER(c_structure))+1 INTO cstructure FROM STRUCTURE_ULR;--+1

SELECT SEQ_STRUCTURE_ULR_SYNC.NEXTVAL INTO cstructure FROM dual;

--SELECT MAX(pers_id)+1 INTO persid FROM v_personne;--+1

SELECT SEQ_PERSONNE.NEXTVAL INTO persid FROM dual;

INSERT INTO STRUCTURE_ULR VALUES (
  cstructure,
  persid,
  INS_LL_STRUCTURE,
  INS_LC_STRUCTURE,
  INS_C_TYPE_STRUCTURE,
  INS_C_STRUCTURE_PERE,
  INS_C_TYPE_ETABLISSEMEN,
  INS_C_ACADEMIE,
  INS_C_STATUT_JURIDIQUE,
  INS_C_RNE,
  INS_SIRET,
  INS_SIREN,
  INS_C_NAF,
  INS_C_TYPE_DECISION_STR,
  INS_REF_EXT_ETAB,
  INS_REF_EXT_COMP,
  INS_REF_EXT_CR,
  INS_REF_DECISION,
  INS_DATE_DECISION,
  INS_DATE_OUVERTURE,
  INS_DATE_FERMETURE,
  INS_STR_ORIGINE,
  INS_STR_PHOTO,
  INS_STR_ACTIVITE,
  INS_GRP_OWNER,
  INS_GRP_RESPONSABLE,
  INS_GRP_FORME_JURIDIQUE,
  INS_GRP_CAPITAL,
  INS_GRP_CA,
  INS_GRP_EFFECTIFS,
  INS_GRP_CENTRE_DECISION,
  INS_GRP_APE_CODE,
  INS_GRP_APE_CODE_BIS,
  INS_GRP_APE_CODE_COMP,
  INS_GRP_ACCES,
  INS_GRP_ALIAS,
  INS_GRP_RESPONSABILITE,
  INS_GRP_TRADEMARQUE,
  INS_GRP_WEBMESTRE,
  INS_GRP_FONCTION1,
  INS_GRP_FONCTION2,
  INS_ORG_ORDRE,
  INS_GRP_MOTS_CLEFS,
  SYSDATE,
  SYSDATE,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  'N',
  'N',
  'O',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,   -- tva_intracom
  NULL,   -- str_accueil
  NULL,   -- str_recherche
  NULL,   -- num_rafp
  INS_LC_STRUCTURE,     -- str_affichage
  NULL,   -- gencod
  NULL,   -- pers_id_creation
  NULL,   -- pers_id_modification
  NULL,   -- STR_STATUT
  NULL,   -- ROM_ID
  NULL,   -- STR_DESCRIPTION
  null,   -- TEM_SIRET_PROVISOIRE
  null    -- ID_RNSR
);

END;
/
-----------------------V20140702.164246__DDL_MAJ_INS_FOURNIS_ULR_ANNUAIRE.sql-----------------------
create or replace
PROCEDURE Ins_Fournis_Ulr_Annuaire(
INS_PERS_ID         FOURNIS_ULR.pers_id%TYPE,
INS_ADR_ORDRE 		FOURNIS_ULR.adr_ordre%TYPE,
INS_FOU_DATE        FOURNIS_ULR.fou_date%TYPE,
INS_FOU_MARCHE      FOURNIS_ULR.fou_marche%TYPE,
INS_FOU_VALIDE      FOURNIS_ULR.fou_valide%TYPE,
INS_AGT_ORDRE       FOURNIS_ULR.AGT_ORDRE%TYPE,
INS_FOU_TYPE        FOURNIS_ULR.fou_type%TYPE,
INS_FOU_ETRANGER	FOURNIS_ULR.FOU_ETRANGER%TYPE,
fouordre			OUT FOURNIS_ULR.fou_ordre%TYPE
) IS
-- ------------
-- DECLARATIONS
-- ------------

cpt 		   INTEGER;
foucode 	   FOURNIS_ULR.fou_code%TYPE;
nom 		   v_personne.pers_libelle%TYPE;
l_cpt_ordre    NUMBER;
l_adr_ordre    INTEGER;
local_passwd   VARCHAR2(30);
local_domaine  VARCHAR2(50);

--fouordre integer;
CURSOR cCpt( apers_id INTEGER) IS SELECT cpt_ordre FROM REPART_COMPTE WHERE pers_id = apers_id;

ligne cCpt%ROWTYPE;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

SELECT COUNT(*) INTO cpt FROM v_personne WHERE pers_id=ins_pers_id;
IF (cpt=0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PERS_ID INCORRECT');
END IF;

/*
SELECT COUNT(*) INTO cpt FROM FOURNIS_ULR;
IF ( cpt = 0 ) THEN
  fouordre := 1;
ELSE
  SELECT MAX(fou_ordre)+1 INTO fouordre FROM FOURNIS_ULR;
END IF;
*/

SELECT SEQ_FOURNIS_ULR.NEXTVAL INTO fouordre FROM dual;

SELECT pers_libelle INTO nom FROM v_personne WHERE pers_id=ins_pers_id;

foucode:=Cons_Fou_Code(nom);




SELECT COUNT(*) INTO cpt FROM REPART_PERSONNE_ADRESSE a , FOURNIS_ULR b, ADRESSE c WHERE tadr_code = 'FACT' AND
a.pers_id = INS_PERS_ID
AND a.PERS_ID = b.PERS_ID
AND b.FOU_VALIDE !='A'
AND  b.ADR_ORDRE = c.ADR_ORDRE
AND a.ADR_ORDRE = c.ADR_ORDRE;
IF ( cpt = 0 )THEN

SELECT COUNT(*) INTO cpt FROM REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE
AND pers_id = INS_PERS_ID
AND tadr_code = 'FACT';


  IF( cpt = 0) THEN
  INSERT INTO REPART_PERSONNE_ADRESSE VALUES (
         	INS_PERS_ID,
         	INS_ADR_ORDRE,
         	NULL,
         	'N',
         	'O',
         	'FACT',
         	SYSDATE,
         	SYSDATE
         	);

   END IF;

END IF;


INSERT INTO FOURNIS_ULR VALUES (
fouordre,
INS_PERS_ID,
INS_ADR_ORDRE,
foucode,
INS_FOU_DATE,
INS_FOU_MARCHE,
INS_FOU_VALIDE,
INS_AGT_ORDRE,
INS_FOU_TYPE,
SYSDATE,
SYSDATE,
l_cpt_ordre,
INS_FOU_ETRANGER	--FOURNIS_ULR.FOU_ETRANGER%type
);


INSERT INTO VALIDE_FOURNIS_ULR VALUES (
fouordre,
INS_AGT_ORDRE,
SYSDATE,
INS_AGT_ORDRE,
SYSDATE
);


SELECT COUNT(*) INTO cpt FROM REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE AND Tadr_code = 'FACT';

IF cpt  = 0 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR :  PROBLEME SUR L''ADRESSE DE FACT');
END IF;

IF cpt != 1 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT MULTI');

END IF;


SELECT COUNT(*)  INTO cpt FROM FOURNIS_ULR f, REPART_PERSONNE_ADRESSE R, ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND R.tadr_code = 'FACT';

IF (cpt != 1) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME PLUSIEURS ADRESSES DE FACTURATION ');
END IF;

SELECT f.adr_ordre  INTO l_adr_ordre FROM FOURNIS_ULR f, REPART_PERSONNE_ADRESSE R, ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND tadr_code = 'FACT';

IF (l_adr_ordre != ins_adr_ordre) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT ADR_ORDRE');

END IF;


END;
/
---------------------------V20140702.164332__DDL_MAJ_INS_FOURNIS_ULR.sql----------------------------
create or replace
PROCEDURE Ins_Fournis_Ulr(
INS_PERS_ID         FOURNIS_ULR.pers_id%TYPE,
INS_ADR_ORDRE 		FOURNIS_ULR.adr_ordre%TYPE,
INS_FOU_DATE        FOURNIS_ULR.fou_date%TYPE,
INS_FOU_MARCHE      FOURNIS_ULR.fou_marche%TYPE,
INS_FOU_VALIDE      FOURNIS_ULR.fou_valide%TYPE,
INS_AGT_ORDRE       FOURNIS_ULR.AGT_ORDRE%TYPE,
INS_FOU_TYPE        FOURNIS_ULR.fou_type%TYPE
--fouordre			out fournis_ulr.fou_ordre%TYPE
) IS
-- ------------
-- DECLARATIONS
-- ------------

cpt 		   INTEGER;
foucode 	   FOURNIS_ULR.fou_code%TYPE;
nom 		   v_personne.pers_libelle%TYPE;
l_cpt_ordre    NUMBER;
l_adr_ordre    INTEGER;
fouordre 	   INTEGER;
CURSOR cCpt( apers_id INTEGER) IS SELECT cpt_ordre FROM REPART_COMPTE WHERE pers_id = apers_id;
local_passwd VARCHAR2(30);
local_domaine VARCHAR2(50);
ligne cCpt%ROWTYPE;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

SELECT COUNT(*) INTO cpt FROM v_personne WHERE pers_id=ins_pers_id;
IF (cpt=0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PERS_ID INCORRECT');
END IF;

/*
SELECT COUNT(*) INTO cpt FROM FOURNIS_ULR;
IF ( cpt = 0 ) THEN
  fouordre := 1;
ELSE
  SELECT MAX(fou_ordre)+1 INTO fouordre FROM FOURNIS_ULR;
END IF;
*/

SELECT SEQ_FOURNIS_ULR.NEXTVAL INTO fouordre FROM dual;

SELECT pers_libelle INTO nom FROM v_personne WHERE pers_id=ins_pers_id;

foucode:=Cons_Fou_Code(nom);




SELECT COUNT(*) INTO cpt FROM REPART_PERSONNE_ADRESSE a , FOURNIS_ULR b, ADRESSE c WHERE tadr_code = 'FACT' AND
a.pers_id = INS_PERS_ID
AND a.PERS_ID = b.PERS_ID
AND b.FOU_VALIDE !='A'
AND  b.ADR_ORDRE = c.ADR_ORDRE
AND a.ADR_ORDRE = c.ADR_ORDRE;
IF ( cpt = 0 )THEN

SELECT COUNT(*) INTO cpt FROM REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE
AND pers_id = INS_PERS_ID
AND tadr_code = 'FACT';


  IF( cpt = 0) THEN
  INSERT INTO REPART_PERSONNE_ADRESSE VALUES (
         	INS_PERS_ID,
         	INS_ADR_ORDRE,
         	NULL,
         	'N',
         	'O',
         	'FACT',
         	SYSDATE,
         	SYSDATE
         	);

   END IF;

END IF;


INSERT INTO FOURNIS_ULR VALUES (
fouordre,
INS_PERS_ID,
INS_ADR_ORDRE,
foucode,
INS_FOU_DATE,
INS_FOU_MARCHE,
INS_FOU_VALIDE,
INS_AGT_ORDRE,
INS_FOU_TYPE,
SYSDATE,
SYSDATE,
l_cpt_ordre,
'N'			-- FOURNIS_ULR.FOU_ETRANGER
);


INSERT INTO VALIDE_FOURNIS_ULR VALUES (
fouordre,
INS_AGT_ORDRE,
SYSDATE,
INS_AGT_ORDRE,
SYSDATE
);


SELECT COUNT(*) INTO cpt FROM REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE AND Tadr_code = 'FACT';

IF cpt  = 0 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR :  PROBLEME SUR L''ADRESSE DE FACT');
END IF;

IF cpt != 1 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT MULTI');

END IF;


SELECT COUNT(*)  INTO cpt FROM FOURNIS_ULR f, REPART_PERSONNE_ADRESSE R, ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND R.tadr_code = 'FACT';

IF (cpt = 0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : AUCUNE ADRESSE DE FACTURATION');
END IF;

IF (cpt > 1) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME PLUSIEURS ADRESSES DE FACTURATION');
END IF;

SELECT f.adr_ordre  INTO l_adr_ordre FROM FOURNIS_ULR f, REPART_PERSONNE_ADRESSE R, ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND tadr_code = 'FACT';

IF (l_adr_ordre != ins_adr_ordre) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT ADR_ORDRE');

END IF;


END;
/
-----------------------------V20140702.164423__DDL_MAJ_COMMUNE_BCN.sql------------------------------
create or replace
PROCEDURE      GRHUM.MAJ_COMMUNE_BCN (
   cinsee       			VARCHAR2,
   ccommune		    		VARCHAR2,
   lcommune				    VARCHAR2,
   cdep             		VARCHAR2,
   dateOuverture			VARCHAR2,
   dateFermeture			VARCHAR2
)
IS
   n                NUMBER(5);
BEGIN

 -- MAJ
    
   SELECT COUNT (*) INTO n FROM grhum.commune WHERE ll_com = lcommune AND c_insee = cinsee;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.commune(c_insee, ll_com, lc_com, c_postal, c_dep, d_creation, d_modification, d_deb_val, d_fin_val, particularite_commune, c_postal_principal, ligne_acheminement)
      VALUES( cinsee, lcommune, ccommune, null, cdep, SYSDATE, SYSDATE, dateOuverture, dateFermeture, null, null, null);
   ELSE
      -- mise à jour des communes issues du Fichier des Communes
      UPDATE grhum.commune
      SET ll_com = lcommune, d_modification = SYSDATE, lc_com = ccommune, c_insee = cinsee, c_dep=cdep,
      		d_deb_val = dateOuverture, d_fin_val = dateFermeture
      WHERE lc_com = lcommune AND c_insee = cinsee;
   END IF;
   COMMIT;
 
END;
/
-----------------------------V20140702.164600__DDL_MAJ_DDL_BAC_PAYS.sql-----------------------------
-- ---------------------------------------------------------
-- Modification de la table GRHUM.PAYS
-- ---------------------------------------------------------

declare
begin
	GRHUM.ADM_ADD_COLUMN('GRHUM', 'PAYS', 'COMMENTAIRE', 'VARCHAR2(2000)', NULL, NULL);
end;
/
ALTER TABLE GRHUM.PAYS MOVE;
ALTER INDEX GRHUM.PK_PAYS REBUILD TABLESPACE INDX_GRHUM;

COMMENT ON COLUMN GRHUM.PAYS.COMMENTAIRE IS 'Commentaires sur le pays';

-- ---------------------------------------------------------
-- Modification de la table GRHUM.BAC
-- ---------------------------------------------------------

declare
begin
	GRHUM.ADM_ADD_COLUMN('GRHUM', 'BAC', 'BAC_LIB_COURT', 'VARCHAR2(20)', NULL, NULL);
end;
/

ALTER TABLE GRHUM.BAC MODIFY (BAC_CODE VARCHAR2(5));

ALTER TABLE GRHUM.BAC MOVE;
ALTER INDEX GRHUM.PK_BAC REBUILD TABLESPACE INDX_GRHUM;

COMMENT ON COLUMN GRHUM.BAC.BAC_LIB_COURT IS 'Commentaires sur le pays';
-------------------------------V20140702.164632__DDL_MAJ_PAYS_BCN.sql-------------------------------
create or replace
PROCEDURE      GRHUM.MAJ_PAYS_BCN (
   numPays       				VARCHAR2,
   libelleLong		    		VARCHAR2,
   libelleCourt					VARCHAR2,
   dOuv							VARCHAR2,
   dFer							VARCHAR2,
   paysCommentaire				VARCHAR2,
   libelleAccentue70			VARCHAR2
)
IS
   n                NUMBER(5);
   chaine			VARCHAR2(250);
BEGIN


   SELECT COUNT (*) INTO n FROM grhum.pays WHERE ll_pays = libelleLong and c_pays = numPays;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.pays(c_pays, ll_pays, lc_pays, l_nationalite, d_deb_val, d_fin_val,
      		d_creation, d_modification, code_iso, l_edition, ll_pays_en, iso_3166_3, commentaire)
      VALUES(numPays, libelleLong, libelleCourt, null, dOuv, dFer, SYSDATE, SYSDATE, null, libelleAccentue70, null, null, paysCommentaire);
   ELSE
      -- mise à jour des libellé et des dates de validité des pays
      UPDATE grhum.pays
      SET c_pays = numPays, ll_pays = libelleLong, lc_pays = libelleCourt, d_deb_val = dOuv, d_fin_val = dFer,
      		d_modification = SYSDATE, l_edition = libelleAccentue70, commentaire = paysCommentaire
     WHERE ll_pays = libelleLong and c_pays = numPays;
   END IF;
   COMMIT;
 
END;
/

----------------------------V20140702.164651__DDL_MAJ_PAYS_CODE_ISO.sql-----------------------------
create or replace
PROCEDURE       GRHUM.MAJ_PAYS_CODE_ISO (
   libelleMajuscule       				VARCHAR2,
   iso3		    						VARCHAR2,
   iso2									VARCHAR2
)
IS
   n                NUMBER(5);
   chaine			VARCHAR2(250);
BEGIN

   SELECT COUNT (*) INTO n FROM grhum.pays WHERE ll_pays = libelleMajuscule;
   IF (n = 0) THEN
      -- pas de MAJ possible
      chaine := ltrim(rtrim('Le pays '));
      chaine := ltrim(rtrim(chaine))||ltrim(rtrim(libelleMajuscule))||' ne correspond à aucun enregistrement.';
      dbms_output.put_line(chaine);
   ELSE
      -- mise à jour des codes ISO des pays
      UPDATE grhum.pays
      SET code_iso = iso2, iso_3166_3 = iso3 , d_modification = SYSDATE
     WHERE ll_pays = libelleMajuscule;
   END IF;
   COMMIT;
 
END;
/

----------------------------V20140702.164717__DDL_MAJ_RNE_CONTRAINTE.sql----------------------------

declare 
begin 

    grhum.adm_add_column('GRHUM', 'RNE', 'CODE_INSEE', 'VARCHAR2(8)', NULL, NULL);

end;
/

ALTER TABLE GRHUM.RNE MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX GRHUM.PK_RNE REBUILD TABLESPACE INDX_GRHUM;

/*
ALTER TABLE GRHUM.RNE ADD (
  CONSTRAINT RNE_REF_COMMUNE 
 FOREIGN KEY (CODE_INSEE, CODE_POSTAL) 
 REFERENCES GRHUM.COMMUNE (C_INSEE, C_POSTAL));
 */
 
declare 
begin  
 grhum.adm_create_fk_ifnotexists ('GRHUM', 'RNE_REF_COMMUNE', 'RNE', 'CODE_INSEE, CODE_POSTAL','GRHUM', 'COMMUNE', 'C_INSEE, C_POSTAL');

end;
/
 
---------------------------------V20140702.164735__DDL_MAJ_UAI.sql----------------------------------
create or replace
PROCEDURE       GRHUM.MAJ_UAI (
   crne       				VARCHAR2,
   llrne		    		VARCHAR2,
   adresseUai				VARCHAR2,
   dateOuverture			VARCHAR2,
   dateFermeture			VARCHAR2,
   academie					VARCHAR2,
   secteurRne				VARCHAR2,
   localite					VARCHAR2,
   numSiret					VARCHAR2
)
IS
   n                NUMBER(5);
BEGIN
    
   SELECT COUNT (*) INTO n FROM grhum.rne WHERE ll_rne = llrne AND c_rne = crne;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.rne(c_rne, ll_rne, adresse, code_postal, c_rne_pere, d_deb_val, d_fin_val, d_creation, d_modification,
      		lc_rne,acad_code,tetab_code, etab_enquete, etab_statut, ville, adr_ordre, siret )
      VALUES( crne, llrne, adresseUai, null, null, dateOuverture, dateFermeture, SYSDATE, SYSDATE,
      		null, academie, null, null, secteurRne, localite, null, numSiret);
   ELSE
      -- mise à jour des UAI du MESR
      UPDATE grhum.rne
      SET c_rne = crne, ll_rne = llrne, adresse = adresseUai, d_deb_val = dateOuverture, d_fin_val = dateFermeture,
      		d_modification = SYSDATE, acad_code = academie, ETAB_STATUT = secteurRne, ville = localite, siret = numSiret
     WHERE ll_rne = llrne AND c_rne = crne;
   END IF;
   COMMIT;
 
END;
/

-------------------------V20140702.164840__DDL_MAJ_TRIGGER_RNE_COMMUNE.sql--------------------------
create or replace
TRIGGER GRHUM.TRG_RNE_COMMUNE
AFTER INSERT OR UPDATE OR DELETE ON GRHUM.RNE FOR EACH ROW
-- auteur : Cocktail
-- creation : 23/06/2011
-- modification : 10/10/2016
-- Mise à jour de l'attribut VILLE de RNE à partir de COMMUNE via la FK
DECLARE
    cpt         INTEGER;
    cinsee              VARCHAR2(5);
BEGIN
    cpt := 0;
    select count(*) into cpt from GRHUM.COMMUNE c where (c.ll_com like ':NEW.ville%' or c.lc_com like ':NEW.ville%') and c.c_postal = :NEW.code_postal;
    if (cpt = 1) then
        update GRHUM.RNE set code_insee = (select c.c_insee from GRHUM.COMMUNE c where (c.ll_com like ':NEW.ville%' or c.lc_com like ':NEW.ville%') and c.c_postal = :NEW.code_postal)
                where ville = :NEW.ville and code_postal = :NEW.code_postal;
    end if;
    if (cpt > 1) then
        select c_insee into cinsee from GRHUM.COMMUNE c
                where (c.ll_com like ':NEW.ville%' or c.lc_com like ':NEW.ville%') and c.c_postal = :NEW.code_postal
                                and c.particularite_commune is null and c.LIGNE_ACHEMINEMENT = :NEW.ville;
        update GRHUM.RNE set code_insee = cinsee where ville = :NEW.ville and code_postal = :NEW.code_postal;
    end if;
END;
/

------------------------V20140702.164859__DDL_MAJ_TRIGGER_REPART_COMPTE.sql-------------------------
create or replace
TRIGGER "GRHUM"."TRG_REPART_COMPTE"
AFTER INSERT OR UPDATE OR DELETE
ON GRHUM.REPART_COMPTE
-- auteur : Cocktail
-- creation : 28/06/2004
-- modification : 08/01/2010
-- modification : 30/06/2014
-- Trigger de mise a jour de la table COMPTE (anciennement COMPTE_NEW) par rapport a la table REPART_COMPTE de maniere a recuperer le pers_id du compte

REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

    nbr             INTEGER;
    date_actuelle   DATE;
    crypte          COMPTE.CPT_CRYPTE%TYPE;
    persid          COMPTE.PERS_ID%TYPE;
    persidCRI       COMPTE.CPT_CREATEUR%TYPE;
    login           COMPTE.CPT_LOGIN%TYPE;
    passwd          COMPTE.CPT_PASSWD%TYPE;
    passwdCrypte    COMPTE.CPT_PASSWD%TYPE;
    passwdClair     COMPTE.CPT_PASSWD_CLAIR%TYPE;
    vlan            COMPTE.CPT_VLAN%TYPE;
    charte          COMPTE.CPT_CHARTE%TYPE;
    home            COMPTE.CPT_HOME%TYPE;
    shell           COMPTE.CPT_SHELL%TYPE;
    connexion       COMPTE.CPT_CONNEXION%TYPE;
    valide          COMPTE.CPT_VALIDE%TYPE;
    debutValide     COMPTE.CPT_DEBUT_VALIDE%TYPE;
    finValide       COMPTE.CPT_FIN_VALIDE%TYPE;
    uidGid          COMPTE.CPT_UID_GID%TYPE;
    uidNew          COMPTE.CPT_UID%TYPE;
    gidNew          COMPTE.CPT_GID%TYPE;
    tvpnCode        COMPTE.TVPN_CODE%TYPE;
    tcryOrdre       COMPTE.TCRY_ORDRE%TYPE;
    cemKey          COMPTE_EMAIL.CEM_KEY%TYPE;
    email           COMPTE_EMAIL.CEM_EMAIL%TYPE;
    domaine         COMPTE_EMAIL.CEM_DOMAINE%TYPE;
    lmpasswd        VARCHAR2(32);
    ntpasswd        VARCHAR2(32);
    flag            VARCHAR2(18);
    paramSwitch		GRHUM.GRHUM_PARAMETRES.PARAM_VALUE%TYPE;

BEGIN

     -- Initialisation des variables
     crypte := '';

     -- Type de cryptage par defaut : UNIX
     tcryOrdre := 1;

     -- Par defaut le createur ou le modificateur des comptes : le CRI
     --persidCRI := 20;
     -- Recup du pers_id de GRHUM_CREATEUR
     SELECT PERS_ID INTO persidCRI FROM COMPTE WHERE CPT_LOGIN =
        (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));

	 -- paramètre de Switch 
	 SELECT PARAM_VALUE into paramSwitch
               		FROM GRHUM_PARAMETRES
        		WHERE PARAM_KEY='org.cocktail.grhum.comptes.switchdanstriggerrepartcompte';

     -- Sysdate
     SELECT TO_DATE(TO_CHAR(SYSDATE,'ddmmyyyy'),'ddmmyyyy') INTO date_actuelle FROM dual;

     --- INSERTION sur REPART_COMPTE
     IF INSERTING THEN

         -- Recuperation des donnees de COMPTE
         SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
            SELECT cpt_login,cpt_passwd,cpt_passwd_clair,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
            INTO   login,passwd,passwdClair,vlan,charte,connexion,uidGid,email,domaine
            FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;
         -- Recuperation des donnees de COMPTE_OLD
--         SELECT COUNT(*) INTO nbr FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--         IF (nbr != 0) THEN
--            SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
--            INTO   login,passwd,vlan,charte,connexion,uidGid,email,domaine
--            FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--            IF (uidGid = 0) THEN uidGid := NULL; END IF;
--         END IF;
         -- Recuperation des donnees de COMPTE_SUITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT    cpt_home,cpt_shell,cpt_gid INTO home,shell,gidNew FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
         -- Recuperation des donnees de COMPTE_VALIDITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT TO_DATE(date_debut,'dd/mm/yyyy'),TO_DATE(date_fin,'dd/mm/yyyy') INTO debutValide,finValide FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;

          -- Password crypte en fonction de la taille du champ password
          -- les mots de passe cryptes sont constitues de 13 caracteres.
          -- Tous les password sont cryptes sauf ceux qui ont une *
          crypte := 'O';
          passwdClair := NULL;
          passwdCrypte := NULL;

          IF (LENGTH(passwd) >= 13) THEN
                    passwdClair := NULL;
                    passwdCrypte := passwd;
          ELSE
                    passwdClair := passwd;
                    IF (passwd = '*') THEN
                        passwdCrypte := passwd;
                    ELSE
                        passwdCrypte := GRHUM.Crypt(passwd);
                    END IF;
          END IF;

          --RAISE_APPLICATION_ERROR(-20000,'TRG_REPART_COMPTE : passwdClair = '||passwdClair||' passwdCrypte = '||passwdCrypte);



        -- on récupère la valeur de la table COMPTE
        select cpt_valide into valide from grhum.compte where cpt_ordre = :NEW.cpt_ordre;
        
        IF ( paramSwitch = 'O' ) -- on tient compte des dates pour savoir si le compte est valide
        THEN

          IF (debutValide IS NOT NULL) THEN
                 --pour que le compte soit valide, la date actuelle doit etre comprise entre debut et fin de validite
                 IF ( (debutValide < date_actuelle) AND (finValide > date_actuelle OR finValide is NULL) ) THEN
                         valide := 'O';
                 ELSE
                         valide := 'N';
                 END IF;
          ELSE
--                  IF ('N' = paramSwitch) THEN
--                      valide := 'O';
                    IF (vlan = 'E') THEN
                        debutValide := TO_DATE('01/07/2009','dd/mm/yyyy');
                        finValide := TO_DATE('31/01/2010','dd/mm/yyyy');
                    END IF;
--                  ELSE
                    valide :='N';
--                  END IF;
          END IF;

        END IF;



        -- recuperation du pers_id
        persId := NULL;
        if (:NEW.pers_id is null) then
            select count(*) into nbr from GRHUM.REPART_COMPTE where cpt_ordre = :NEW.cpt_ordre;
            if (nbr !=0) then
                select pers_Id into persId from GRHUM.REPART_COMPTE where cpt_ordre = :NEW.cpt_ordre;
            end if;
        else
            persId := :NEW.pers_id;
        end if;

         -- COMPTE
        SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:NEW.cpt_ordre;
        IF (nbr = 0) THEN
           -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID
           IF (vlan='D' OR vlan='G') THEN
                      INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,
                                               cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,cpt_valide,cpt_debut_valide,cpt_fin_valide,
                                               cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,
                                               cpt_uid_gid,tvpn_code,tcry_ordre)
                      VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,NULL,uidGid,
                             home,shell,connexion,vlan,charte,valide,debutValide,finValide,
                             'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,'NO',tcryOrdre);
           ELSE
                   IF (vlan='P' OR vlan='R' OR vlan='E') THEN
                         tvpnCode := 'W3VPN';
                   ELSE
                         tvpnCode := 'NO';
                   END IF;
                   -- Pour les comptes Etudiants le GID par defaut est de 2001
                   if (vlan='E') THEN
                      gidNew := 2001;
                   END IF;
                   INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,
                                            cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,cpt_valide,cpt_debut_valide,cpt_fin_valide,
                                            cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                   VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,uidGid,gidNew,
                          home,shell,connexion,vlan,charte,valide,debutValide,finValide,
                          'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);
           END IF;
        ELSE
                   IF (vlan='P' OR vlan='R' OR vlan='E') THEN
                        tvpnCode := 'W3VPN';
                   ELSE
                        tvpnCode := 'NO';
                   END IF;
                   -- Pour les comptes Etudiants le GID par defaut est de 2001
                   if (vlan='E') THEN
                      gidNew := 2001;
                   END IF;

                   UPDATE GRHUM.COMPTE
                   SET pers_id=persId, cpt_login=login, cpt_passwd=passwdCrypte, cpt_passwd_clair=passwdClair, cpt_crypte=crypte , cpt_uid=uidGid, cpt_gid=gidNew,
                       cpt_home=home, cpt_shell=shell, cpt_connexion=connexion, cpt_vlan=vlan, cpt_charte=charte, cpt_valide=valide, cpt_debut_valide=debutValide, cpt_fin_valide=finValide,
                       cpt_principal='O',cpt_liste_rouge='N',d_creation=:NEW.d_creation,cpt_createur=persidCRI,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid=uidGid,tvpn_code=tvpnCode, tcry_ordre=tcryOrdre
                   where cpt_ordre = :NEW.cpt_ordre;

        END IF;

         -- INSERTION de l'email en fonction du VLAN
       IF (email IS NOT NULL) THEN
                  SELECT COUNT(*) INTO nbr FROM COMPTE_EMAIL WHERE cpt_ordre=:NEW.cpt_ordre;
               IF (nbr = 0) THEN
                     SELECT compte_email_seq.NEXTVAL INTO cemKey FROM dual;
                     INSERT INTO COMPTE_EMAIL(CEM_KEY, CPT_ORDRE, CEM_EMAIL, CEM_DOMAINE, CEM_PRIORITE, CEM_ALIAS, D_CREATION, D_MODIFICATION)
                     VALUES(cemKey,:NEW.cpt_ordre,email,domaine,NULL,'N',SYSDATE,SYSDATE);
               END IF;
       END IF;

       -- Configuration SAMBA en ajout
--      SELECT COUNT(*) INTO NBR FROM COMPTE_SAMBA WHERE CPT_ORDRE = :NEW.CPT_ORDRE;
--      IF (nbr = 0 ) THEN
--              lmpasswd := GRHUM.Crypt_Lm_Samba(passwd);
--              ntpasswd := GRHUM.Crypt_Nt_Samba(passwd);
--              flag := '[UX         ]';
--              INSERT INTO GRHUM.COMPTE_SAMBA VALUES(:NEW.cpt_ordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);
--      END IF;

    END IF;

    -- MISE a JOUR sur REPART_COMPTE
    IF UPDATING THEN

         -- Recuperation des donnees de COMPTE
         SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
            SELECT cpt_login,cpt_passwd,cpt_passwd_clair,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine,tvpn_code
            INTO   login,passwd,passwdClair,vlan,charte,connexion,uidGid,email,domaine,tvpnCode
            FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;
         -- Recuperation des donnees de COMPTE_OLD
--         SELECT COUNT(*) INTO nbr FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--         IF (nbr != 0) THEN
--             SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
--             INTO         login,passwd,vlan,charte,connexion,uidGid,email,domaine
--             FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--             IF (uidGid = 0) THEN uidGid := NULL; END IF;
--         END IF;
         -- Recuperation des donnees de COMPTE_SUITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT    cpt_home,cpt_shell,cpt_gid INTO home,shell,gidNew FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
         -- Recuperation des donnees de COMPTE_VALIDITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT TO_DATE(date_debut,'dd/mm/yyyy'),TO_DATE(date_fin,'dd/mm/yyyy') INTO debutValide,finValide FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
        END IF;

          -- Password crypte en fonction de la taille du champ password
          -- les mots de passe cryptes sont constitues de 13 caracteres.
          -- Tous les password sont cryptes sauf ceux qui ont une *
          crypte := 'O';
          --passwdClair := NULL;
          passwdCrypte := NULL;
--          if(passwdClair IS NOT NULL) then
--               IF (SUBSTR(passwd,1,1)='*') THEN
--                  passwdCrypte := '*'||GRHUM.Crypt(passwd);
--               ELSE
--                  passwdCrypte := GRHUM.Crypt(passwd);
--               END IF;
--          else
--               passwdCrypte := passwd;
--          end if;


          IF (LENGTH(passwd) >= 13) THEN
                    passwdClair := passwdClair;
                    passwdCrypte := passwd;
          ELSE
                    passwdClair := passwd;
                    IF (passwd = '*') THEN
                        passwdCrypte := passwd;
                    ELSE
                        -- PB 16/06/2006 : si le paswwd est desactive, on crypte le passwd en mettant l'etoile devant
                        IF (SUBSTR(passwd,1,1)='*') THEN
                           passwdCrypte := '*'||GRHUM.Crypt(passwd);
                        ELSE
                           passwdCrypte := GRHUM.Crypt(passwd);
                        END IF;
                    END IF;
          END IF;



        -- on récupère la valeur de la table COMPTE
        select cpt_valide into valide from grhum.compte where cpt_ordre = :NEW.cpt_ordre;
        
        IF ( paramSwitch = 'O' ) -- si on tient compte des dates pour savoir si le compte est valide
        THEN

          IF (debutValide IS NOT NULL) THEN
                 --pour que le compte soit valide, la date actuelle doit etre comprise entre debut et fin de validite
                 IF ( (debutValide < date_actuelle) AND (finValide > date_actuelle OR finValide IS NULL) ) THEN
                         valide := 'O';
                 ELSE
                         valide := 'N';
                 END IF;
          ELSE
--                IF ('N' = paramSwitch) THEN
--                  valide := 'O';
--                ELSE
                  valide := 'N';
                --END IF;
          END IF;
          
        END IF;


     -- Vieillerie incompréhensible ?? TODO
         --IF (vlan = 'E') THEN
         --   debutValide := TO_DATE('01/07/2009','dd/mm/yyyy');
         --   finValide := TO_DATE('31/01/2010','dd/mm/yyyy');
         -- -- Pour les comptes Etudiants le GID par defaut est de 2001
         --   gidNew := 2001;
         --END IF;



        -- recuperation du pers_id
        persId := NULL;
        if (:NEW.pers_id is null) then
            select count(*) into nbr from GRHUM.REPART_COMPTE where cpt_ordre = :NEW.cpt_ordre;
            if (nbr !=0) then
                select pers_Id into persId from GRHUM.REPART_COMPTE where cpt_ordre = :NEW.cpt_ordre;
            end if;
        else
            persId := :NEW.pers_id;
        end if;

        SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:NEW.cpt_ordre;
        IF (nbr != 0) THEN

            -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID
           IF (vlan='D' OR vlan='G') THEN
                  UPDATE GRHUM.COMPTE
                  SET      pers_id = persId, cpt_login = login,cpt_passwd=passwdCrypte,cpt_passwd_clair=passwdClair,cpt_crypte=crypte,cpt_uid = NULL, cpt_gid= uidGid,
                           cpt_valide = valide, cpt_debut_valide = debutValide, cpt_fin_valide = finValide,cpt_home = home,cpt_shell = shell, cpt_connexion = connexion,
                           cpt_vlan=vlan,cpt_charte=charte,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid = uidGid,tcry_ordre = tcryOrdre
                  WHERE    cpt_ordre = :NEW.cpt_ordre;
           ELSE

                  UPDATE GRHUM.COMPTE
                  SET      pers_id = persId, cpt_login = login,cpt_passwd=passwdCrypte,cpt_passwd_clair=passwdClair,cpt_crypte=crypte,cpt_uid = uidGid, cpt_gid = gidNew,
                           cpt_valide = valide, cpt_debut_valide = debutValide, cpt_fin_valide = finValide,cpt_home = home,cpt_shell = shell, cpt_connexion = connexion,
                           cpt_vlan=vlan,cpt_charte=charte,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid = uidGid,tcry_ordre = tcryOrdre
                  WHERE    cpt_ordre = :NEW.cpt_ordre;
           END IF;

        ELSE

           -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID
           IF (vlan='D' OR vlan='G') THEN
                  INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                           cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tcry_ordre)
                  VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,NULL,uidGid,home,shell,connexion,vlan,charte,
                         valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tcryOrdre);
           ELSE
               -- Modif. le 15/03/2005 pour ne plus mettre a jour le mot de passe crypte car ils le sont deja pour les comptes Administration et Recherche
               -- Modif. le 19/04/2005 : par contre les passwd pour les Etudiants et les eXterieurs sont a crypter
--               IF (vlan='P' OR vlan='R' OR vlan='E') THEN
--                     tvpnCode := 'W3VPN';
--               ELSE
--                     tvpnCode := 'NO';
--               END IF;
               IF (vlan='P' OR vlan = 'R') THEN
                    INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                             cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                    VALUES(:NEW.cpt_ordre,persId,login,passwdClair,crypte,uidGid,gidNew,home,shell,connexion,vlan,charte,
                           valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);
               ELSE
                    INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                             cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                    VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,uidGid,gidNew,home,shell,connexion,vlan,charte,
                           valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);
                END IF;
           END IF;

        END IF;

         -- COMPTE_EMAIL en UPDATING
       IF (email IS NOT NULL) THEN
          SELECT COUNT(*) INTO nbr FROM COMPTE_EMAIL WHERE cpt_ordre=:NEW.cpt_ordre;
          IF (nbr != 0) THEN
              UPDATE COMPTE_EMAIL
              SET cem_email = email, cem_domaine = domaine, d_modification = SYSDATE
              WHERE cpt_ordre = :NEW.cpt_ordre;
          ELSE
              SELECT compte_email_seq.NEXTVAL INTO cemKey FROM dual;
              INSERT INTO COMPTE_EMAIL(CEM_KEY, CPT_ORDRE, CEM_EMAIL, CEM_DOMAINE, CEM_PRIORITE, CEM_ALIAS, D_CREATION, D_MODIFICATION)
              VALUES(cemKey,:NEW.cpt_ordre,email,domaine,NULL,'N',SYSDATE,SYSDATE);
          END IF;
        END IF;

      -- Configuration SAMBA en modification
--      SELECT count(*) into nbr from COMPTE_SAMBA where cpt_ordre = :NEW.cpt_ordre;
--      lmpasswd := GRHUM.Crypt_Lm_Samba(passwdClair);
--      ntpasswd := GRHUM.Crypt_Nt_Samba(passwdClair);
--      flag := '[UX         ]';
--      IF (nbr = 0 ) THEN
--            INSERT INTO GRHUM.COMPTE_SAMBA VALUES(:NEW.cpt_ordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);
--      ELSE
--            UPDATE COMPTE_SAMBA set SMB_LM_PASSWD = lmpasswd, smb_nt_passwd = ntpasswd, d_modification = sysdate where cpt_ordre = :NEW.cpt_ordre;
--      END IF;

    END IF;

    -- SUPPRESSION sur REPART_COMPTE
    IF DELETING THEN

       --RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_REPART_COMPTE : DELETING "'||:OLD.cpt_ordre);

       SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:OLD.cpt_ordre;
       IF (nbr != 0) THEN

          DELETE FROM COMPTE_EMAIL WHERE cpt_ordre=:OLD.cpt_ordre;
          DELETE FROM CERTIFICAT WHERE cpt_ordre=:OLD.cpt_ordre;
          DELETE FROM COMPTE WHERE cpt_ordre=:OLD.cpt_ordre AND pers_id=:OLD.pers_id;
          DELETE FROM COMPTE_SAMBA WHERE cpt_ordre=:OLD.cpt_ordre;
          -- Le compte passe a l'etat Annule
          --UPDATE COMPTE SET cpt_valide='A' WHERE cpt_ordre=:OLD.cpt_ordre AND pers_id=:OLD.pers_id;

       END IF;

    END IF;

END ;
/
-------------------------------V20140702.164921__DDL_MAJ_FLX_DDL.sql--------------------------------

-- Suppression des tables fille et de la table mere
declare
begin

    GRHUM.DROP_OBJECT('GRHUM', 'FLX_TABLES', 'TABLE');
    GRHUM.DROP_OBJECT('GRHUM', 'FLX_HISTO', 'TABLE');
    GRHUM.DROP_OBJECT('GRHUM', 'FLX_FLUX', 'TABLE');

end;
/


declare
   ordre_sql long;

begin
-- Table GRHUM.FLX_FLUX
   ordre_sql := q'[ create table GRHUM.FLX_FLUX
   (code_flux varchar2(5),
    libelle varchar2(30),
    filer varchar2(80),
    constraint FLX_FLUX_PK primary key (code_flux) using index tablespace INDX_GRHUM)
   ]';

   grhum.adm_create_table_ifnotexists('GRHUM','FLX_FLUX',ordre_sql,'DATA_GRHUM');

-- Table GRHUM.FLX_TABLES
   ordre_sql := q'[ create table GRHUM.FLX_TABLES
   (code_flux varchar2(5),
    user_table varchar2(30),
    nom_table varchar2(80),
    filer varchar2(80),
    constraint FLX_TABLES_PK primary key (code_flux, user_table, nom_table) using index tablespace INDX_GRHUM)
   ]';

   grhum.adm_create_table_ifnotexists('GRHUM','FLX_TABLES',ordre_sql,'DATA_GRHUM');

   grhum.adm_create_fk_ifnotexists('GRHUM','FLX_TABLES_CTRL_FLX','FLX_TABLES','code_flux','GRHUM','FLX_FLUX','code_flux');

-- Table GRHUM.FLX_HISTO
   ordre_sql := q'[ create table GRHUM.FLX_HISTO
   (code_flux varchar2(5),
    version varchar2(20),
    date_publi date,
    date_instal date,
    user_instal varchar2(30),
    status_instal varchar2(8),
    filer varchar2(80),
    constraint FLX_HISTO_PK primary key (code_flux, version, status_instal) using index tablespace INDX_GRHUM)
   ]';

   grhum.adm_create_table_ifnotexists('GRHUM','FLX_HISTO',ordre_sql,'DATA_GRHUM');

   grhum.adm_create_fk_ifnotexists('GRHUM','FLX_HISTO_CTRL_FLX','FLX_HISTO','code_flux','GRHUM','FLX_FLUX','code_flux');

end;
/

comment on table GRHUM.FLX_FLUX is 'Liste des flux de données intégrés dans le référentiel';
comment on column GRHUM.FLX_FLUX.code_flux is 'Code du flux';
comment on column GRHUM.FLX_FLUX.libelle is 'Libellé du flux';
comment on column GRHUM.FLX_FLUX.filer is 'Champs libre';

comment on table GRHUM.FLX_TABLES is 'Tables impactées par un flux de données intégrés dans le référentiel';
comment on column GRHUM.FLX_TABLES.code_flux is 'Code du flux';
comment on column GRHUM.FLX_TABLES.user_table is 'Propriétaire de la table';
comment on column GRHUM.FLX_TABLES.nom_table is 'Nom de la table';
comment on column GRHUM.FLX_TABLES.filer is 'Champ libre';

comment on table GRHUM.FLX_HISTO is 'Historique des flux de données intégrés dans le référentiel';
comment on column GRHUM.FLX_HISTO.code_flux is 'Code du flux';
comment on column GRHUM.FLX_HISTO.version is 'Version du flux';
comment on column GRHUM.FLX_HISTO.date_publi is 'Date de la publication du flux';
comment on column GRHUM.FLX_HISTO.date_instal is 'Date de l''installation du flux';
comment on column GRHUM.FLX_HISTO.user_instal is 'Compte Oracle utilisé pour installer le flux';
comment on column GRHUM.FLX_HISTO.status_instal is 'Statut de l''installation du flux';
comment on column GRHUM.FLX_HISTO.filer is 'Champs libre';

create or replace function grhum.is_flux_version (nom_flux varchar2, numero_version varchar2)
return  integer
is
   nbr_lgs integer;
   is_flux_v integer;
begin
   select count(*) into nbr_lgs from grhum.flx_histo
    where code_flux = nom_flux
      and version = numero_version
      and status_instal = 'OK';
   if (nbr_lgs = 1) then
      is_flux_v := 1;
   else
      is_flux_v := 0;
   end if;
   return is_flux_v;
end;
/

create or replace procedure grhum.maj_flux_version(in_code_flux varchar2, in_version varchar2, in_date_publi varchar2, in_status_instal varchar2, in_filer varchar2)
is
   nbr_lgs integer;
   is_flux_v integer;
begin
   select count(*) into nbr_lgs from grhum.flx_histo
    where code_flux = in_code_flux
      and version = in_version;
   
   if nbr_lgs = 0 then -- version non historisée
      insert into grhum.flx_histo values (in_code_flux, in_version, to_date(in_date_publi,'DD/MM/YYYY'), sysdate, user, in_status_instal, in_filer);
      commit;
   else -- une installation a déjà été réalisée
      update grhum.flx_histo set
             date_instal = sysdate,
             user_instal = user,
             status_instal = in_status_instal,
             filer = in_filer
       where code_flux = in_code_flux
         and version = in_version;
      commit;
   end if;
end;
/




