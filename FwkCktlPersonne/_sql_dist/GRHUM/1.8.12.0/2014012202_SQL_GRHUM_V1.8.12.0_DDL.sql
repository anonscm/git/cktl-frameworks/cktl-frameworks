--
-- Patch DDL de GRHUM du 22/01/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.12.0
-- Date de publication : 22/01/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.11.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.12.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.12.0 a deja ete passe !');
    end if;

end;
/


-----------------------V20140122.144533__DDL_PROC_VERIF_DOMAINE_CORRECTE.sql------------------------
create or replace
PROCEDURE       GRHUM.VERIF_DOMAINE
(
  p_nomParametre 			VARCHAR2,
  
  p_cpt_vlan				VLANS.C_VLAN%type,
  p_cpt_domaine			VLANS.DOMAINE%TYPE,
  
  p_vlan_type       		VLANS.C_VLAN%type,
  p_vlan_domaine 			VLANS.DOMAINE%TYPE
)
IS
  nomParametre 			VARCHAR2(2000);
  cpt_vlan				VLANS.C_VLAN%type;
  cpt_domaine			VLANS.DOMAINE%TYPE; 
  vlan_type       		VLANS.C_VLAN%type;
  vlan_domaine 			VLANS.DOMAINE%TYPE;
  
  cpt_dom_second       INTEGER;
  
  str_domaine      VARCHAR2(2000);
  list_dom_sec     VARCHAR2(2000);
  chaine           VARCHAR2(2000);
  valeur_test      integer;
  nb               integer;

  
BEGIN


-- regle d'integrite entre le VLAN et le domaine associé
      SELECT COUNT(*) INTO cpt_dom_second
      				  FROM GRHUM_PARAMETRES
      				  WHERE param_key = nomParametre;
      
      IF (cpt_dom_second <> 0) THEN
        	SELECT param_value INTO list_dom_sec
        					   FROM GRHUM_PARAMETRES
        					   WHERE param_key = nomParametre;
      END IF;
      
      IF (cpt_vlan = vlan_type) THEN
        -- regle d'integrite entre le VLAN et la liste des domaines (principal + secondaires)
        IF (list_dom_sec <> 'univ.fr' AND cpt_dom_second <> 0) THEN
          IF SUBSTR(list_dom_sec,LENGTH(ltrim(rtrim(list_dom_sec)))-1,1) = ';' THEN
            chaine := ltrim(rtrim(list_dom_sec));
          ELSE
            chaine := ltrim(rtrim(list_dom_sec))||';';
          END IF;
          chaine := ltrim(rtrim(chaine))||ltrim(rtrim(vlan_domaine))||';';
          
          -- la valeur de 1 est pour lever une erreur
          valeur_test := 1;
          SELECT LENGTH(ltrim(rtrim(chaine)))-LENGTH(REPLACE(ltrim(rtrim(chaine)),';',''))+1
          		INTO nb
          		FROM dual;
          FOR j IN 0..nb-1
          LOOP
            IF j =0 THEN
              str_domaine := SUBSTR(chaine,1,instr(chaine,';',1,1)-1);
            ELSE
              str_domaine := SUBSTR(chaine,instr(chaine,';',1,j)+1, instr(chaine,';',1,j+1) - instr(chaine,';',1,j)-1);
            END IF;
            IF ( cpt_vlan = vlan_type AND cpt_domaine = str_domaine ) THEN
              valeur_test     := 0;
            END IF;
            --dbms_output.put_line(str_domaine);
          END LOOP;
          IF (valeur_test <> 0) THEN
            RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||cpt_vlan||'" et les domaines principal et secondaire(s) ');
          END IF;
        ELSE
          IF ( cpt_domaine <> vlan_domaine) THEN
            RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||cpt_vlan||'" et le domaine associe au VLAN '||vlan_domaine||'.');
          END IF;
        END IF;
      END IF;
      
END;
/
------------------------------V20140122.145000__DDL_TRIGGER_COMPTE.sql------------------------------
create or replace 
TRIGGER "GRHUM"."TRG_COMPTE" BEFORE
  INSERT OR
  UPDATE ON GRHUM.COMPTE FOR EACH ROW
    -- CRI
    -- creation : 20/01/2004
    -- modification : 13/05/2009
    -- modification : 01/07/2013 prise en compte des domaines secondaires pour le VLAN P
    -- modification : 10/01/2014 prise en compte des domaines secondaires pour le VLAN E
    -- modification : 22/01/2014 prise en compte des domaines secondaires pour le VLAN R
    -- 1er declencheur, de niveau ligne, qui n'interroge plus la table mutante
    -- a la place, il stocke dans la table temporaire les donnees inserees
    -- Creation d'une table temporaire vide de meme structure que COMPTE
    DECLARE 
  cpt  INTEGER;
  cpt_dom_second INTEGER;
  newlogin COMPTE.cpt_login%TYPE;
  newemail COMPTE.cpt_email%TYPE;
  newdomaine COMPTE.cpt_domaine%TYPE;
  domaine_princ COMPTE.CPT_DOMAINE%TYPE;
  vlan_admin VLANS.C_VLAN%type;
  vlan_recherche VLANS.C_VLAN%type;
  vlan_etudiant VLANS.C_VLAN%type;
  vlan_externe VLANS.C_VLAN%type;
  vlan_admin_domaine VLANS.DOMAINE%TYPE;
  vlan_recherche_domaine VLANS.DOMAINE%TYPE;
  vlan_etudiant_domaine VLANS.DOMAINE%TYPE;
  nb           INTEGER;
  str_domaine  VARCHAR2(2000);
  list_dom_sec VARCHAR2(2000);
  chaine       VARCHAR2(2000);
  valeur_test  NUMBER(1);
  BEGIN
    
    cpt        := 0;
    newlogin   := NULL;
    newemail   := NULL;
    newdomaine := NULL;
    
    -- 03/10/2005 pamametrage des codes des VLANS
    SELECT param_value INTO vlan_admin
    FROM grhum_parametres
    WHERE param_key = 'GRHUM_VLAN_ADMIN';
    
    SELECT param_value INTO vlan_recherche
    FROM grhum_parametres
    WHERE param_key = 'GRHUM_VLAN_RECHERCHE';
    
    SELECT param_value INTO vlan_etudiant
    FROM grhum_parametres
    WHERE param_key = 'GRHUM_VLAN_ETUD';
    
    SELECT param_value INTO vlan_externe
    FROM grhum_parametres
    WHERE param_key = 'GRHUM_VLAN_EXTERNE';
    
    -- 09/07/2013 pamametrage des domaines des VLANS internes
    SELECT domaine INTO vlan_admin_domaine
    FROM GRHUM.vlans
    WHERE c_vlan = vlan_admin;
    
    SELECT domaine INTO vlan_recherche_domaine
    FROM GRHUM.vlans
    WHERE c_vlan = vlan_recherche;
    
    SELECT domaine INTO vlan_etudiant_domaine
    FROM GRHUM.vlans
    WHERE c_vlan = vlan_etudiant;
    
    -- contraintes d'ingegrites entre le VLAN P (not null) et le domaine principal
    SELECT COUNT(*) INTO cpt
    FROM GRHUM_PARAMETRES
    WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';
    
    IF (cpt <> 0) THEN
    		SELECT param_value INTO domaine_princ
      						 FROM GRHUM_PARAMETRES
      						 WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';
    		IF (domaine_princ <> vlan_admin_domaine) THEN
            	RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le domaine du VLAN P et le paramètre GRHUM_DOMAINE_PRINCIPAL');
          	END IF;
    END IF;
    
    -- regle d'integrite entre le VLAN P et la liste des domaines (principal + secondaires)
    GRHUM.VERIF_DOMAINE('org.cocktail.grhum.compte.domainessecondaires', :NEW.cpt_vlan, :NEW.cpt_domaine, vlan_admin, vlan_admin_domaine);
   
      
    -- regle d'integrite entre le VLAN R et le domaine associé
    GRHUM.VERIF_DOMAINE('org.cocktail.grhum.compte.domainessecondairesVlanR', :NEW.cpt_vlan, :NEW.cpt_domaine, vlan_recherche, vlan_recherche_domaine);
      
      
    -- regle d'integrite entre le VLAN E et le domaine associé
    GRHUM.VERIF_DOMAINE('org.cocktail.grhum.compte.domainessecondairesVlanE', :NEW.cpt_vlan, :NEW.cpt_domaine, vlan_etudiant, vlan_etudiant_domaine);
    
 
      
      -- integrite des comptes ETUDIANTs seulement pour L.R.
      IF ( domaine_princ    = 'univ-lr.fr' ) THEN
        IF ( (:NEW.cpt_vlan = vlan_etudiant) AND (:NEW.cpt_domaine NOT IN ('etudiant.univ-lr.fr','etudiut.univ-lr.fr')) ) THEN
          RAISE_APPLICATION_ERROR(                                      -20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine etudiant ! ');
        END IF;
      END IF;
      
      -- integrite entre le VLAN eXterieur et le domaine
      IF ( (:NEW.cpt_vlan= vlan_externe) AND (:NEW.cpt_domaine = domaine_princ) ) THEN
        RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine exterieur ! ');
      END IF;

    -- fin contraintes VLAN et domaine
    
    -- Conversion du login sans les accents
    IF (:NEW.cpt_login IS NOT NULL) THEN
      newlogin         := Chaine_Sans_Accents(:NEW.cpt_login);
      :NEW.cpt_login   := newlogin;
    END IF;
    
    -- Conversion du mail sans les accents
    IF (:NEW.cpt_email IS NOT NULL) THEN
      newemail         := Chaine_Sans_Accents(:NEW.cpt_email);
      :NEW.cpt_email   := newemail;
    END IF;
    
    -- Conversion du domaine sans les accents
    IF (:NEW.cpt_domaine IS NOT NULL) THEN
      newdomaine         := Chaine_Sans_Accents(:NEW.cpt_domaine);
      :NEW.cpt_domaine   := newdomaine;
    END IF;
    
    -- insertion du nouvel enregistrement dans la table temporaire pour ne pas avoir l'erreur ORA-04091 : table mutante
    -- cas du trigger before insert on COMPTE qui fait un SELECT sur la table COMPTE dans le meme trigger
    INSERT
    INTO COMPTE_TEMP
      (
        CPT_ORDRE,
        CPT_UID_GID,
        CPT_LOGIN,
        CPT_PASSWD,
        CPT_CRYPTE,
        CPT_CONNEXION,
        CPT_VLAN,
        CPT_EMAIL,
        CPT_DOMAINE,
        CPT_CHARTE,
        CPT_VALIDE,
        CPT_PRINCIPAL,
        CPT_LISTE_ROUGE,
        D_CREATION,
        D_MODIFICATION,
        TVPN_CODE
      )
      VALUES
      (
        :NEW.CPT_ORDRE,
        :NEW.CPT_UID_GID,
        :NEW.CPT_LOGIN,
        :NEW.CPT_PASSWD,
        :NEW.CPT_CRYPTE,
        :NEW.CPT_CONNEXION,
        :NEW.CPT_VLAN,
        :NEW.CPT_EMAIL,
        :NEW.CPT_DOMAINE,
        :NEW.CPT_CHARTE,
        :NEW.CPT_VALIDE,
        :NEW.CPT_PRINCIPAL,
        :NEW.CPT_LISTE_ROUGE,
        SYSDATE,
        SYSDATE,
        :NEW.TVPN_CODE
      );
  END ;
/


