--
-- Patch DML de GRHUM du 22/01/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.12.0
-- Date de publication : 22/01/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



---------------------------------V20140115.101043__DML_MAJ_RNE.sql----------------------------------
--------------------------------------------------------
--  RNE fourni par l'INSA de Rennes
--------------------------------------------------------

-- lycée Victor Hugo à Besançon

-- http://www.education.gouv.fr/annuaire/25-doubs/besancon/lycee/lycee-victor-hugo.html
 UPDATE GRHUM.RNE
 	SET adresse = '1 rue Rembrandt-BP 2159',
 		code_postal = '25000',
 		ville = upper('Besançon')
 	WHERE c_rne = '0250007X';

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.12.0',to_date('22/01/2014','DD/MM/YYYY'),sysdate,'Ajout de domaines secondaires au Vlan R et E et Eclaircissement du trigger des comptes');
COMMIT;
