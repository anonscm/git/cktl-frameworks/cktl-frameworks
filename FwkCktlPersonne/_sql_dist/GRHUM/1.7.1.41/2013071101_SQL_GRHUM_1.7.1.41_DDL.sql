--
-- Patch DML de GRHUM du 11/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.41 
-- Date de publication : 11/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.40';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.41';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.41 a deja ete passe !');
    end if;

end;
/


-- -------------------------
-- MAJ de GRHUM.TRG_COMPTE
-- -------------------------
create or replace
TRIGGER "GRHUM"."TRG_COMPTE"
BEFORE INSERT OR UPDATE ON GRHUM.COMPTE FOR EACH ROW
-- CRI
-- creation : 20/01/2004
-- modification : 13/05/2009
-- modification : 01/07/2013 prise en compte des domaines secondaires pour le VLAN P
-- 1er declencheur, de niveau ligne, qui n'interroge plus la table mutante
-- a la place, il stocke dans la table temporaire les donnees inserees
-- Creation d'une table temporaire vide de meme structure que COMPTE
-- CREATE GLOBAL TEMPORARY TABLE COMPTE_TEMP AS SELECT * FROM COMPTE WHERE 0=1;
DECLARE
   cpt              		INTEGER;
   cpt_dom_second       INTEGER;
   newlogin         		COMPTE.cpt_login%TYPE;
   newemail         		COMPTE.cpt_email%TYPE;
   newdomaine       		COMPTE.cpt_domaine%TYPE;
   domaine_princ    		COMPTE.CPT_DOMAINE%TYPE;
   vlan_admin       		VLANS.C_VLAN%type;
   vlan_recherche   		VLANS.C_VLAN%type;
   vlan_etudiant    		VLANS.C_VLAN%type;
   vlan_externe     		VLANS.C_VLAN%type;
   
   vlan_admin_domaine 		VLANS.DOMAINE%TYPE;
   vlan_recherche_domaine 	VLANS.DOMAINE%TYPE;
   vlan_etudiant_domaine 	VLANS.DOMAINE%TYPE;

   nb               integer;
   str_domaine      varchar2(2000);
   list_dom_sec     varchar2(2000);
   chaine           varchar2(2000);

	 valeur_test		NUMBER(1);

BEGIN
   cpt := 0;
   newlogin := NULL;
   newemail := NULL;
   newdomaine := NULL;

   -- 03/10/2005 pamametrage des codes des VLANS
   select param_value into vlan_admin from grhum_parametres where param_key = 'GRHUM_VLAN_ADMIN';
   select param_value into vlan_recherche from grhum_parametres where param_key = 'GRHUM_VLAN_RECHERCHE';
   select param_value into vlan_etudiant from grhum_parametres where param_key = 'GRHUM_VLAN_ETUD';
   select param_value into vlan_externe from grhum_parametres where param_key = 'GRHUM_VLAN_EXTERNE';
    -- 09/07/2013 pamametrage des domaines des VLANS internes
   select domaine into vlan_admin_domaine from GRHUM.vlans where c_vlan = vlan_admin;
   select domaine into vlan_recherche_domaine from GRHUM.vlans where c_vlan = vlan_recherche;
   select domaine into vlan_etudiant_domaine from GRHUM.vlans where c_vlan = vlan_etudiant;

   -- contraintes d'ingegrites entre le VLAN (not null) et le domaine
   SELECT COUNT(*) INTO cpt
   FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';

   IF (cpt <> 0) THEN

      SELECT param_value INTO domaine_princ
        FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';

      Select count(*) into cpt_dom_second from GRHUM_PARAMETRES WHERE param_key='org.cocktail.grhum.compte.domainessecondaires';
      if (cpt_dom_second <> 0) then
        SELECT param_value INTO list_dom_sec
          FROM GRHUM_PARAMETRES WHERE param_key='org.cocktail.grhum.compte.domainessecondaires';
      end if;
  
	IF (:NEW.cpt_vlan = vlan_admin) THEN
      -- regle d'integrite entre le VLAN P et la liste des domaines (principal + secondaires)
      IF (list_dom_sec <> 'univ.fr' AND cpt_dom_second <> 0)
      THEN
         if substr(list_dom_sec,length(ltrim(rtrim(list_dom_sec)))-1,1) = ';'
         then
            chaine := ltrim(rtrim(list_dom_sec));
         else
            chaine := ltrim(rtrim(list_dom_sec))||';';
         end if;
         
         chaine := ltrim(rtrim(list_dom_sec))||ltrim(rtrim(domaine_princ))||';';
         -- la valeur de 1 est pour lever une erreur
		 valeur_test := 1;	
		
         select length(ltrim(rtrim(chaine)))-length(replace(ltrim(rtrim(chaine)),';',''))+1 into nb from dual;

         for j in 0..nb-1 loop
            if j=0 then
               str_domaine := substr(chaine,1,instr(chaine,';',1,1)-1);
            else
               str_domaine := substr(chaine,instr(chaine,';',1,j)+1, instr(chaine,';',1,j+1) - instr(chaine,';',1,j)-1);
            end if;
            if ( :NEW.cpt_vlan = vlan_admin AND :NEW.cpt_domaine = str_domaine ) then
            	valeur_test := 0;
            	/*
            	if ( str_domaine == domaine_princ ) then
           			RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine principal '||domaine_princ||'.');
           		else
           			RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine secondaire '||str_domaine||'.');
           		*/
           	end if;
           	
            --dbms_output.put_line(str_domaine);
         end loop;
         if (valeur_test <> 0) then
           		RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et les domaines principal et secondaire(s) ');
         end if;
      ELSE
      	if ( :NEW.cpt_vlan = vlan_admin AND :NEW.cpt_domaine <> domaine_princ ) then
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine principal '||domaine_princ||'.');
      	end if;
      END IF;
	END IF;

  

        -- regle d'integrite entre le VLAN R et le domaine associé
      IF ( :NEW.cpt_vlan = vlan_recherche AND :NEW.cpt_domaine <> vlan_recherche_domaine) THEN
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine associé au VLAN '||vlan_recherche_domaine||'.');
      END IF;
      
         -- regle d'integrite entre le VLAN E et le domaine associé
      IF ( :NEW.cpt_vlan = vlan_etudiant AND :NEW.cpt_domaine <> vlan_etudiant_domaine) THEN
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine associé au VLAN '||vlan_etudiant_domaine||'.');
      END IF;

     -- integrite des comptes ETUDIANTs seulement pour L.R.
        IF ( domaine_princ = 'univ-lr.fr' ) THEN
            IF ( (:NEW.cpt_vlan = vlan_etudiant) AND (:NEW.cpt_domaine NOT IN ('etudiant.univ-lr.fr','etudiut.univ-lr.fr')) ) THEN
                RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine etudiant ! ');
             END IF;
        END IF;

     -- integrite entre le VLAN eXterieur et le domaine
     IF ( (:NEW.cpt_vlan= vlan_externe) AND (:NEW.cpt_domaine = domaine_princ) ) THEN
            RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine exterieur ! ');
     END IF;

  END IF;
   -- fin contraintes VLAN et domaine

   -- Conversion du login sans les accents
   IF (:NEW.cpt_login IS NOT NULL) THEN
        newlogin := Chaine_Sans_Accents(:NEW.cpt_login);
     :NEW.cpt_login := newlogin;
   END IF;

   -- Conversion du mail sans les accents
   IF (:NEW.cpt_email IS NOT NULL) THEN
        newemail := Chaine_Sans_Accents(:NEW.cpt_email);
     :NEW.cpt_email := newemail;
   END IF;

   -- Conversion du domaine sans les accents
   IF (:NEW.cpt_domaine IS NOT NULL) THEN
        newdomaine := Chaine_Sans_Accents(:NEW.cpt_domaine);
     :NEW.cpt_domaine := newdomaine;
   END IF;

   -- insertion du nouvel enregistrement dans la table temporaire pour ne pas avoir l'erreur ORA-04091 : table mutante
   -- cas du trigger before insert on COMPTE qui fait un SELECT sur la table COMPTE dans le meme trigger
   INSERT INTO COMPTE_TEMP(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CRYPTE,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,CPT_VALIDE,CPT_PRINCIPAL,CPT_LISTE_ROUGE,D_CREATION,D_MODIFICATION,TVPN_CODE)
   VALUES(:NEW.CPT_ORDRE,:NEW.CPT_UID_GID,:NEW.CPT_LOGIN,:NEW.CPT_PASSWD,:NEW.CPT_CRYPTE,:NEW.CPT_CONNEXION,:NEW.CPT_VLAN,:NEW.CPT_EMAIL,:NEW.CPT_DOMAINE,:NEW.CPT_CHARTE,:NEW.CPT_VALIDE,:NEW.CPT_PRINCIPAL,:NEW.CPT_LISTE_ROUGE,SYSDATE,SYSDATE,:NEW.TVPN_CODE);

END ;
/



--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.41', TO_DATE('11/07/2013', 'DD/MM/YYYY'),NULL,'MAJ du TRG_COMPTE, Entrée de la Croatie dans la zone Euro ');



COMMIT;




