--
-- Patch DML de GRHUM du 11/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.41 
-- Date de publication : 11/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


Declare
cpt INTEGER;

begin
  -- MAJ du commentaire d'un paramètre
  Select count(*) into cpt from GRHUM.GRHUM_PARAMETRES WHERE param_key='org.cocktail.grhum.compte.domainessecondaires';
  if (cpt <> 0) then
      UPDATE GRHUM.GRHUM_PARAMETRES SET PARAM_COMMENTAIRES='Liste des domaines secondaires prévus pour le VLAN P séparés par un ; (Défaut : une chaîne  à changer qui est univ.fr si vous souhaitez un domaine secondaire).' 
        WHERE param_key='org.cocktail.grhum.compte.domainessecondaires';
  end if;
  
  -- MAJ suite à l'entrée de la Croatie dans l'UE
  select count(*) into cpt from GRHUM.REPART_PAYS_ZONE_MONDE where C_pays='119' and c_zone_monde='UE';
  if (cpt = 0) then
    Insert into GRHUM.REPART_PAYS_ZONE_MONDE(C_PAYS, C_ZONE_MONDE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION)
      VALUES ('119', 'UE', TO_DATE('01/07/2013', 'DD/MM/YYYY'), null, Sysdate, Sysdate);
  end if;
  
end;
/



--
-- 
--
--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.41';

COMMIT;




