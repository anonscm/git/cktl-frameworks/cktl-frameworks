--
-- Patch DDL de GRHUM du 05/03/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.30
-- Date de publication : 05/03/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.29';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.30';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.30 a deja ete passe !');
    end if;

end;
/


--
-- CREATION DE LA TABLE DES MINISTERES
--

DROP TABLE GRHUM.MINISTERES CASCADE CONSTRAINTS;

DROP SEQUENCE GRHUM.MINISTERES_SEQ;

 CREATE TABLE GRHUM.MINISTERES (
	ID_MINISTERE NUMBER(38) NOT NULL,
	CODE_MINISTERE VARCHAR2(10),
	LC_MINISTERE VARCHAR2(30), 
	LL_MINISTERE VARCHAR2(250),
	D_OUVERTURE  DATE  NOT NULL,
	D_FIN DATE,
	TEM_VISIBLE VARCHAR2(1) DEFAULT 'N' NOT NULL,
  
  	CONSTRAINT PK_MINISTERES PRIMARY KEY (ID_MINISTERE) USING INDEX TABLESPACE INDX_GRHUM 
);
CREATE SEQUENCE GRHUM.MINISTERES_SEQ START WITH 1;
  COMMENT ON TABLE GRHUM.MINISTERES  IS 'Liste des Ministères';
  COMMENT ON COLUMN GRHUM.MINISTERES.ID_MINISTERE IS 'id du ministère';
  COMMENT ON COLUMN GRHUM.MINISTERES.CODE_MINISTERE IS 'codification du ministère';
  COMMENT ON COLUMN GRHUM.MINISTERES.LC_MINISTERE IS 'Libellé court du ministère';
  COMMENT ON COLUMN GRHUM.MINISTERES.LL_MINISTERE IS 'Libellé long du ministère';
  COMMENT ON COLUMN GRHUM.MINISTERES.D_OUVERTURE IS 'Date de création du ministère';
  COMMENT ON COLUMN GRHUM.MINISTERES.D_FIN IS 'Date de fermeture du ministère';
  COMMENT ON COLUMN GRHUM.MINISTERES.TEM_VISIBLE IS 'Visibilite du ministere dans les listes de choix';




-- Pour ManGUE

UPDATE GRHUM.CORPS SET TEM_DELEGATION = 'N' WHERE TEM_DELEGATION IS NULL;

ALTER TABLE GRHUM.CORPS MODIFY TEM_DELEGATION DEFAULT 'N' NOT NULL;

ALTER TABLE GRHUM.DISC_SECOND_DEGRE MODIFY (LL_DISC_SECOND_DEGRE VARCHAR2(100));

ALTER TABLE GRHUM.TYPE_POPULATION ADD (TEM_VISIBLE VARCHAR2(1) DEFAULT 'O' NOT NULL, D_OUVERTURE DATE, D_FERMETURE DATE);

ALTER TABLE GRHUM.TYPE_CONTRAT_TRAVAIL ADD (TEM_VISIBLE VARCHAR2(1) DEFAULT 'O' NOT NULL);

-- Creation d'un nouveau type d'absence : CGRFP
INSERT INTO GRHUM.TYPE_ABSENCE 
(C_TYPE_ABSENCE, LC_TYPE_ABSENCE, LL_TYPE_ABSENCE, TYPE_ABS_IMPUTABLE, TYPE_ABS_NIVEAU, TYPE_ABS_JOURS_AUTORISES, TYPE_SENS_IMPUTATION, D_CREATION, D_MODIFICATION,
 TYPE_VALIDATION, CONGE_LEGAL, JOURS_CONSECUTIFS, OBSERVATIONS, C_TYPE_ABSENCE_HARPEGE, TEM_ENFANT, C_TYPE_ABSENCE_ONP, TEM_CIR)
VALUES ('CGRFP', 'Congé Fam. Personnel', 'Congé Non Rémunéré pour raisons familiales ou personnelles', 'N', '4', 0, '-', sysdate, sysdate,
4, 'O', null, null, null, 'N', null, 'N');

-- Creation de la table contenant les motifs du conge pour raison familiale ou personnelle
CREATE TABLE GRHUM.MOTIF_CGNT_RFP
(
  C_MOTIF_CG_RFP   VARCHAR2(5)            NOT NULL,
  LL_MOTIF_CG_RFP  VARCHAR2(100),
  D_CREATION             DATE                   DEFAULT SYSDATE,
  D_MODIFICATION         DATE                   DEFAULT SYSDATE
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX GRHUM.PK_MOTIF_CG_RFP ON GRHUM.MOTIF_CGNT_RFP
(C_MOTIF_CG_RFP)
LOGGING
TABLESPACE INDX_GRHUM
NOPARALLEL;


ALTER TABLE GRHUM.MOTIF_CGNT_RFP ADD (
  CONSTRAINT PK_MOTIF_CG_RFP
 PRIMARY KEY
 (C_MOTIF_CG_RFP)
    USING INDEX 
    TABLESPACE INDX_GRHUM);

GRANT REFERENCES, SELECT ON GRHUM.MOTIF_CGNT_RFP TO MANGUE;

/



--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.30', TO_DATE('06/03/2013', 'DD/MM/YYYY'),NULL,'Ajout table des MINISTERES et des informations Grade et Echelon etc pour MANGUE');
-- commit;


Commit;




