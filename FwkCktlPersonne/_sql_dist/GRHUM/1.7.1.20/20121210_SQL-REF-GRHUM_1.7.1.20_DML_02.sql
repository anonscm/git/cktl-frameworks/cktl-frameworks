--
-- Patch DML de GRHUM du 10/12/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 10/12/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.20', TO_DATE('10/12/2012', 'DD/MM/YYYY'),NULL,'Mise à jour de la vue V_DOUBLON_INDIVIDU, des tables RNE et DIPLOMES ');
commit;


--
-- Actualisation de GRHUM.DIPLOMES
--
update GRHUM.DIPLOMES set c_niveau_diplome='8' where ll_diplome='Docteur nouvelle thèse';
update GRHUM.DIPLOMES set c_niveau_diplome='F' where ll_diplome='Docteur Ingénieur';

--
-- Actualisation de GRHUM.RNE
--
update grhum.rne set ville=upper('Mitry-Mory') where c_rne='0771996B';




--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.20';


COMMIT;