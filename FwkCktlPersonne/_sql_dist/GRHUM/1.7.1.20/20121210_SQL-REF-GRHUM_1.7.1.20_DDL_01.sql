--
-- Patch DDL de GRHUM du 10/12/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.20
-- Date de publication : 10/12/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.19';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.20';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.20 a deja ete passe !');
    end if;

end;
/



create or replace procedure grhum.drop_constraint (ownerconstraint varchar2, tableconstraint varchar2, nameconstraint varchar2)
is
    -- Suppression d'une contrainte si elle existe
    
   flag   smallint;
begin
   flag := 0;

   select count (constraint_name)
   into   flag
   from   all_constraints
   where  upper (owner) = upper (ownerconstraint) and upper (table_name) = upper (tableconstraint) and upper (constraint_name) = upper (nameconstraint);

   if flag > 0 then
      execute immediate 'ALTER TABLE ' || ownerconstraint || '.' || tableconstraint || ' DROP CONSTRAINT ' || nameconstraint;
   end if;
end;
/


--
-- Mise en archive d'une vue qui n'est plus utilisée
-- La vue est : GRHUM.V_DOUBLON_INDIVIDU
--

create or replace force view grhum.zv_doublon_individu_old (nom_patronymique, prenom, nombre)
as
   select   nom_patronymique,
            prenom,
            count (*)
   from     individu_ulr
   group by nom_patronymique, prenom
   having   count (*) > 1;
   
drop view grhum.v_doublon_individu;

