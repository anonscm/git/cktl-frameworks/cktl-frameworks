--
-- Patch DDL de GRHUM du 21/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.7.0
-- Date de publication : 21/11/2013
-- Auteur(s) : Alain

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.6.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.7.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.7.0 a deja ete passe !');
    end if;

end;
/

----------------------V20131121.145652__DDL_Compilation_vues_et_procedures.sql----------------------

CREATE OR REPLACE PROCEDURE GRHUM.Echanger_Personne_Grhum
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
  ligne1 VARCHAR2(200);
  ligne2 VARCHAR2(200);
  cursor c1 (oldid number) is select c_structure from grhum.repart_structure where pers_id = oldid;
  cursor c2 (oldid number) is select no_telephone from grhum.personne_telephone where pers_id = oldid;
  cstruct varchar2(10);
  notel varchar2(20);
BEGIN
  ligne1 := NULL;
  ligne2 := NULL;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_individu_urgence = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNEL_ULR set no_individu_urgence = '||newno||' where no_individu_urgence = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0) then
    SELECT COUNT(*)
    INTO   nbenr1
    FROM   grhum.PERSONNEL_ULR
    WHERE  no_dossier_pers = newno;
    IF (nbenr1 = 0) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'create table TEMP00 as select * from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update TEMP00 set no_dossier_pers = '||newno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'insert into GRHUM.PERSONNEL_ULR select * from TEMP00;');

       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
       ligne2 := 'drop table TEMP00;';
    else
       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
    end if;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_PERSONNEL
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_PERSONNEL set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR_EDITION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR_EDITION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DIPLOME
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DIPLOME set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DOSSIER
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DOSSIER set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_HISTORIQUE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_HISTORIQUE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_INDIVIDU_SITUATION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_INDIVIDU_SITUATION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BAP_ELEMENT_CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BAP_ELEMENT_CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BLOC_NOTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BLOC_NOTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_ECHELON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_ECHELON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_INDICIAIRE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_INDICIAIRE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CARRIERE_OLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CARRIERE_OLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CGE_MOD_AGT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CGE_MOD_AGT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CHERCHEUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CHERCHEUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  --

  /*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE_QUOTA
  WHERE  par_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE_QUOTA set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DELEGATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DELEGATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DEPOSITAIRE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DEPOSITAIRE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;


  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ENQUETES_REPONSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ENQUETES_REPONSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ETUDIANT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ETUDIANT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.EVOLUTION_CHEVRON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.EVOLUTION_CHEVRON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.FOURNIS_ULR
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.FOURNIS_ULR set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_DISTINCTIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_DISTINCTIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_FORMATIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_FORMATIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_PSEUDO
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_PSEUDO set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_LA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_ELECTEUR set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_LA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_LA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_TA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_TA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_TA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_TA_STRUCTURE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MAD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MAD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MI_TPS_THERAP
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MI_TPS_THERAP set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_auteur = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_auteur = '||newno||' where no_auteur = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --
*/
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NOTES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NOTES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PAIEMENT
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PAIEMENT set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_ALIAS
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_ALIAS set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_TELEPHONE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) then
    open c2(oldid);
    loop
        fetch c2 into notel;
        exit when c2%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.PERSONNE_TELEPHONE WHERE PERS_ID = newid and no_telephone = notel;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_TELEPHONE set pers_id = '||newid||' where pers_id = '||oldid||' and no_telephone='''||notel||''';');
        end if;
    end loop;
    close c2;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_PERSONNEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_PERSONNEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME_HISTO
  WHERE  pers_id_createur = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME_HISTO set pers_id_createur = '||newid||' where pers_id_createur = '||oldid||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.RELIQUATS_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.RELIQUATS_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ASSOCIATION
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
       SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_ASSOCIATION WHERE PERS_ID = newid;
       IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ASSOCIATION set pers_id = '||newid||' where pers_id = '||oldid||'
               and c_structure not in (select c_structure from GRHUM.REPART_ASSOCIATION where pers_id = '||newid||');');
       END IF;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_BUREAU
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_BUREAU set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_EMPLOI
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldno||','||newno,SYSDATE,
               'update GRHUM.REPART_EMPLOI set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_PERSONNE_ADRESSE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_PERSONNE_ADRESSE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_STRUCTURE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
    open c1(oldid);
    loop
        fetch c1 into cstruct;
        exit when c1%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_STRUCTURE WHERE PERS_ID = newid and c_structure = cstruct;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_STRUCTURE set pers_id = '||newid||' where pers_id = '||oldid||' and c_structure = '||cstruct||';');
        END IF;
    end loop;
    close c1;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SECRETARIAT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SECRETARIAT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STAGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STAGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_owner = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_owner = '||newno||' where grp_owner = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_responsable = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_responsable = '||newno||' where grp_responsable = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_ROLE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_ROLE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_CATEGORIE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_CATEGORIE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.TRAVAUX_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.TRAVAUX_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  IF (ligne1 IS NOT NULL)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne1);
       IF (ligne2 IS NOT NULL) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne2);
       end if;
  END IF;
END;
/









CREATE OR REPLACE PROCEDURE GRHUM.Detruire_Personne_Mangue(noindividu NUMBER)
IS

nbr                 INTEGER;
absnumero           NUMBER;
daccordre           NUMBER;
dasordre            NUMBER;
dmatordre           NUMBER;
evakey              NUMBER;
noseqcarriere       NUMBER;
noseqcontrat        NUMBER;
noseqaffectation    NUMBER;
noseqarrete         NUMBER;
noenfant            NUMBER;
CURSOR affectation(numero NUMBER) IS SELECT no_seq_affectation FROM MANGUE.AFFECTATION WHERE no_dossier_pers = numero;
CURSOR carriere(numero NUMBER) IS SELECT no_seq_carriere FROM MANGUE.CARRIERE WHERE no_dossier_pers = numero;
CURSOR cgntacctrav(numero NUMBER) IS SELECT abs_numero,dacc_ordre FROM MANGUE.CGNT_ACCIDENT_TRAV WHERE no_dossier_pers = numero;
CURSOR cgntcgm(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CGNT_CGM WHERE no_dossier_pers = numero;
CURSOR cgntmaladie(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CGNT_MALADIE WHERE no_dossier_pers = numero;
CURSOR cld(numero NUMBER) IS SELECT abs_numero,no_seq_arrete FROM MANGUE.CLD WHERE no_dossier_pers = numero;
CURSOR clm(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CLM WHERE no_dossier_pers = numero;
CURSOR congeaccserv(numero NUMBER) IS SELECT abs_numero,dacc_ordre FROM MANGUE.CONGE_ACCIDENT_SERV WHERE no_dossier_pers = numero;
CURSOR congeadoption(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_ADOPTION WHERE no_dossier_pers = numero;
CURSOR congeal3(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_AL3 WHERE no_dossier_pers = numero;
CURSOR congeal4(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_AL4 WHERE no_dossier_pers = numero;
CURSOR congeal5(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_AL5 WHERE no_dossier_pers = numero;
CURSOR congeal6(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_AL6 WHERE no_dossier_pers = numero;
CURSOR congeformation(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_FORMATION WHERE no_dossier_pers = numero;
CURSOR congegardeenfant(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_GARDE_ENFANT WHERE no_dossier_pers = numero;
CURSOR congemaladie(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_MALADIE WHERE no_dossier_pers = numero;
CURSOR congematernite(numero NUMBER) IS SELECT abs_numero,dmat_ordre FROM MANGUE.CONGE_MATERNITE WHERE no_dossier_pers = numero;
CURSOR congepaternite(numero NUMBER) IS SELECT abs_numero FROM MANGUE.CONGE_PATERNITE WHERE no_dossier_pers = numero;
CURSOR contrat(numero NUMBER) IS SELECT no_seq_contrat FROM MANGUE.CONTRAT WHERE no_dossier_pers = numero;
CURSOR crct(numero NUMBER) IS SELECT abs_numero,no_seq_arrete FROM MANGUE.CRCT WHERE no_dossier_pers = numero;
CURSOR evaluation(numero NUMBER) IS SELECT eva_key FROM MANGUE.EVALUATION WHERE no_individu = numero;
CURSOR enfant(numero NUMBER) IS SELECT no_enfant FROM GRHUM.REPART_ENFANT WHERE no_dossier_pers = numero;

BEGIN

    SELECT COUNT(*) INTO nbr FROM GRHUM.PERSONNEL_ULR WHERE no_dossier_pers = noindividu;
    IF (nbr != 0) THEN

        SELECT COUNT(*) INTO nbr FROM MANGUE.AFFECTATION WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN affectation(noindividu);
            LOOP
            FETCH affectation INTO noseqaffectation;
            EXIT WHEN affectation%NOTFOUND;

                DELETE FROM MANGUE.AFFECTATION_DETAIL WHERE no_seq_affectation in (select no_seq_affectation from MANGUE.AFFECTATION where no_dossier_pers = noindividu);
        
            END LOOP;
            CLOSE affectation;
        
            DELETE FROM MANGUE.AFFECTATION WHERE no_dossier_pers = noindividu;
    
        END IF;

        DELETE FROM MANGUE.AGENT_DROITS_SERVICES WHERE no_individu = noindividu;
        DELETE FROM MANGUE.AGENT_PERSONNEL WHERE no_individu = noindividu;
        DELETE FROM MANGUE.ARRIVEE WHERE no_dossier_pers = noindividu;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CARRIERE WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN carriere(noindividu);
            LOOP
            FETCH carriere INTO noseqcarriere;
            EXIT WHEN carriere%NOTFOUND;

                DELETE FROM MANGUE.RELIQUATS_ANCIENNETE WHERE no_dossier_pers = noindividu and no_seq_carriere = noseqcarriere;
                DELETE FROM MANGUE.ELEMENT_CARRIERE WHERE no_dossier_pers = noindividu and no_seq_carriere = noseqcarriere;
            
            END LOOP;
            CLOSE carriere;
        
            DELETE FROM MANGUE.CHANGEMENT_POSITION WHERE no_dossier_pers = noindividu;
            DELETE FROM MANGUE.STAGE WHERE no_dossier_pers = noindividu;
            DELETE FROM MANGUE.CARRIERE WHERE no_dossier_pers = noindividu;
    
        END IF;

        DELETE FROM MANGUE.CFA WHERE no_dossier_pers = noindividu;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CGNT_ACCIDENT_TRAV WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN cgntacctrav(noindividu);
            LOOP
            FETCH cgntacctrav INTO absnumero,daccordre;
            EXIT WHEN cgntacctrav%NOTFOUND;

                DELETE FROM MANGUE.DECLARATION_ACCIDENT WHERE dacc_ordre = daccordre;
                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE cgntacctrav;
        
            DELETE FROM MANGUE.CGNT_ACCIDENT_TRAV WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CGNT_CGM WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN cgntcgm(noindividu);
            LOOP
            FETCH cgntcgm INTO absnumero;
            EXIT WHEN cgntcgm%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE cgntcgm;
        
            DELETE FROM MANGUE.CGNT_CGM WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CGNT_MALADIE WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN cgntmaladie(noindividu);
            LOOP
            FETCH cgntmaladie INTO absnumero;
            EXIT WHEN cgntmaladie%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE cgntmaladie;
        
            DELETE FROM MANGUE.CGNT_MALADIE WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CLD WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN cld(noindividu);
            LOOP
            FETCH cld INTO absnumero,noseqarrete;
            EXIT WHEN cld%NOTFOUND;

                DELETE FROM MANGUE.CLD_DETAIL WHERE no_seq_arrete = noseqarrete;
                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero;
        
            END LOOP;
            CLOSE cld;
        
            DELETE FROM MANGUE.CLD WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CLM WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN clm(noindividu);
            LOOP
            FETCH clm INTO absnumero;
            EXIT WHEN clm%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero;
        
            END LOOP;
            CLOSE clm;
        
            DELETE FROM MANGUE.CLM WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_ACCIDENT_SERV WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congeaccserv(noindividu);
            LOOP
            FETCH congeaccserv INTO absnumero,daccordre;
            EXIT WHEN congeaccserv%NOTFOUND;

                DELETE FROM MANGUE.DECLARATION_ACCIDENT WHERE dacc_ordre = daccordre;
                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congeaccserv;
        
            DELETE FROM MANGUE.CONGE_ACCIDENT_SERV WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_ADOPTION WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congeadoption(noindividu);
            LOOP
            FETCH congeadoption INTO absnumero;
            EXIT WHEN congeadoption%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congeadoption;
        
            DELETE FROM MANGUE.CONGE_ADOPTION WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_AL3 WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congeal3(noindividu);
            LOOP
            FETCH congeal3 INTO absnumero;
            EXIT WHEN congeal3%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congeal3;
        
            DELETE FROM MANGUE.CONGE_AL3 WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_AL4 WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congeal4(noindividu);
            LOOP
            FETCH congeal4 INTO absnumero;
            EXIT WHEN congeal4%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congeal4;
        
            DELETE FROM MANGUE.CONGE_AL4 WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_AL5 WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congeal5(noindividu);
            LOOP
            FETCH congeal5 INTO absnumero;
            EXIT WHEN congeal5%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congeal5;
        
            DELETE FROM MANGUE.CONGE_AL5 WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_AL6 WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congeal6(noindividu);
            LOOP
            FETCH congeal6 INTO absnumero;
            EXIT WHEN congeal6%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congeal6;
        
            DELETE FROM MANGUE.CONGE_AL6 WHERE no_dossier_pers = noindividu;
    
        END IF;
    
        DELETE FROM MANGUE.CONGE_BONIFIE WHERE no_dossier_pers = noindividu;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_FORMATION WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congeformation(noindividu);
            LOOP
            FETCH congeformation INTO absnumero;
            EXIT WHEN congeformation%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congeformation;
        
            DELETE FROM MANGUE.CONGE_FORMATION WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_GARDE_ENFANT WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congegardeenfant(noindividu);
            LOOP
            FETCH congegardeenfant INTO absnumero;
            EXIT WHEN congegardeenfant%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congegardeenfant;
        
            DELETE FROM MANGUE.CONGE_GARDE_ENFANT WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_MALADIE WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congemaladie(noindividu);
            LOOP
            FETCH congemaladie INTO absnumero;
            EXIT WHEN congemaladie%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congemaladie;
        
            DELETE FROM MANGUE.CONGE_MALADIE_DETAIL WHERE conge_id in (select no_seq_arrete from MANGUE.CONGE_MALADIE where abs_numero = absnumero);
            DELETE FROM MANGUE.CONGE_MALADIE WHERE no_dossier_pers = noindividu;
    
        END IF;
    
        --DELETE FROM MANGUE.CONGE_MALADIE_DETAIL WHERE no_dossier_pers = noindividu;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_MATERNITE WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congematernite(noindividu);
            LOOP
            FETCH congematernite INTO absnumero,dmatordre;
            EXIT WHEN congematernite%NOTFOUND;

                DELETE FROM MANGUE.DECLARATION_MATERNITE WHERE dmat_ordre = dmatordre;
                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congematernite;
        
            DELETE FROM MANGUE.CONGE_MATERNITE WHERE no_dossier_pers = noindividu;
    
        END IF;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CONGE_PATERNITE WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN congepaternite(noindividu);
            LOOP
            FETCH congepaternite INTO absnumero;
            EXIT WHEN congepaternite%NOTFOUND;

                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero; 
        
            END LOOP;
            CLOSE congepaternite;
        
            DELETE FROM MANGUE.CONGE_PATERNITE WHERE no_dossier_pers = noindividu;
    
        END IF;

        DELETE FROM MANGUE.CONSERVATION_ANCIENNETE WHERE no_dossier_pers = noindividu;
    
        OPEN contrat(noindividu);
        LOOP
        FETCH contrat INTO noseqcontrat;
        EXIT WHEN contrat%NOTFOUND;

            DELETE FROM MANGUE.CONTRAT_AVENANT WHERE no_seq_contrat IN (select no_seq_contrat from mangue.contrat where no_dossier_pers= noindividu);
            DELETE FROM MANGUE.CONTRAT_VACATAIRES WHERE no_seq_contrat IN (select no_seq_contrat from mangue.contrat where no_dossier_pers= noindividu);
            DELETE FROM MANGUE.CONTRAT_LBUDS WHERE ctra_ordre IN (SELECT CTRA_ORDRE FROM MANGUE.CONTRAT_AVENANT WHERE NO_SEQ_CONTRAT IN(SELECT NO_SEQ_CONTRAT FROM MANGUE.CONTRAT WHERE NO_DOSSIER_PERS=noindividu));
            DELETE FROM MANGUE.CONTRAT_LBUDS WHERE ctrv_ordre IN (SELECT CTRV_ORDRE FROM MANGUE.CONTRAT_VACATAIRES WHERE NO_SEQ_CONTRAT IN(SELECT NO_SEQ_CONTRAT FROM MANGUE.CONTRAT WHERE NO_DOSSIER_PERS=noindividu));
        
        END LOOP;
        CLOSE contrat;
        DELETE FROM MANGUE.CONTRAT WHERE no_dossier_pers = noindividu;

        DELETE FROM MANGUE.CPA WHERE no_dossier_pers = noindividu;

        SELECT COUNT(*) INTO nbr FROM MANGUE.CRCT WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN crct(noindividu);
            LOOP
            FETCH crct INTO absnumero,noseqarrete;
            EXIT WHEN crct%NOTFOUND;

                DELETE FROM MANGUE.CRCT_DETAIL WHERE no_seq_arrete = noseqarrete;
                DELETE FROM MANGUE.ABSENCES WHERE abs_numero = absnumero;
        
            END LOOP;
            CLOSE crct;
        
            DELETE FROM MANGUE.CRCT WHERE no_dossier_pers = noindividu;
    
        END IF;

        DELETE FROM MANGUE.DECHARGE WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.DELEGATION WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.DEPART WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.DROITS_GARDE_ENFANT WHERE no_dossier_pers = noindividu;

        SELECT COUNT(*) INTO nbr FROM MANGUE.EVALUATION WHERE no_individu = noindividu;
        IF (nbr != 0) THEN
            OPEN evaluation(noindividu);
            LOOP
            FETCH evaluation INTO evakey;
            EXIT WHEN evaluation%NOTFOUND;

                DELETE FROM MANGUE.REPART_NIVEAU_COMP_PRO WHERE eva_key = evakey;
                DELETE FROM MANGUE.REPART_NOUVELLE_COMP WHERE eva_key = evakey;
        
            END LOOP;
            CLOSE evaluation;
        
            DELETE FROM MANGUE.EVALUATION WHERE no_individu = noindividu;
    
        END IF;

        DELETE FROM MANGUE.FEV_DROIT WHERE no_individu = noindividu;
        DELETE FROM MANGUE.HIERARCHIE WHERE no_individu = noindividu;
        DELETE FROM MANGUE.HIERARCHIE WHERE no_individu_resp = noindividu;
        DELETE FROM MANGUE.INDIVIDU_DIPLOMES WHERE no_individu = noindividu;
        DELETE FROM MANGUE.INDIVIDU_DISTINCTIONS WHERE no_individu = noindividu;
        DELETE FROM MANGUE.INDIVIDU_FAMILIALE WHERE no_individu = noindividu;            
        DELETE FROM MANGUE.INDIVIDU_FORMATIONS WHERE no_individu = noindividu;    
        DELETE FROM MANGUE.LISTE_ELECTEUR WHERE no_individu = noindividu;     
        DELETE FROM MANGUE.MAD WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.MI_TPS_THERAP WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.NBI_IMPRESSIONS WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.NBI_OCCUPATION WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.NOTES WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.OCCUPATION WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.PASSE WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.PERIODE_HANDICAP WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.PERIODES_MILITAIRES WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.PERIODE_TEMPS_PARTIEL WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.PREF_PERSONNEL WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.PROLONGATION_ACTIVITE WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.RECUL_AGE WHERE no_dossier_pers = noindividu;

        -- Temps Partiel
        DELETE FROM MANGUE.TEMPS_PARTIEL WHERE no_dossier_pers = noindividu;
        DELETE FROM MANGUE.REPRISE_TEMPS_PLEIN WHERE no_dossier_pers = noindividu;
                            
        -- Tables communes dans GRHUM       
        SELECT COUNT(*) INTO nbr FROM GRHUM.REPART_ENFANT WHERE no_dossier_pers = noindividu;
        IF (nbr != 0) THEN
            OPEN enfant(noindividu);
            LOOP
            FETCH enfant INTO noenfant;
            EXIT WHEN enfant%NOTFOUND;

                DELETE FROM GRHUM.ENFANT WHERE no_enfant = noenfant;
        
            END LOOP;
            CLOSE enfant;
        
            DELETE FROM GRHUM.REPART_ENFANT WHERE no_dossier_pers = noindividu;
    
        END IF;
        
        DELETE FROM GRHUM.PERSONNEL_ULR CASCADE WHERE no_dossier_pers = noindividu;

    END IF;

END;
/



/* Formatted on 2013/11/20 10:00 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW grhum.occupation (no_occupation,
                                               d_deb_occupation,
                                               d_fin_occupation,
                                               no_poste,
                                               num_moyen_utilise,
                                               den_moyen_utilise,
                                               no_dossier_pers,
                                               d_creation,
                                               d_modification,
                                               motif_fin,
                                               observations
                                              )
AS
   SELECT no_occupation, d_deb_occupation, d_fin_occupation, no_emploi,
          num_moyen_utilise, den_moyen_utilise, no_dossier_pers, d_creation,
          d_modification, motif_fin, observations
     FROM mangue.occupation;






CREATE OR REPLACE PROCEDURE GRHUM.Echanger_Personne_Mangue
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS

  nbenr1 INTEGER;

BEGIN

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ABSENCES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ABSENCES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.AFFECTATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.AFFECTATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.AGENT_DROITS_SERVICES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.AGENT_DROITS_SERVICES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.AGENT_PERSONNEL
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.AGENT_PERSONNEL set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CFA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CFA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CGNT_ACCIDENT_TRAV
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CGNT_ACCIDENT_TRAV set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CGNT_CGM
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CGNT_CGM set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CGNT_MALADIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CGNT_MALADIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CHANGEMENT_POSITION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CHANGEMENT_POSITION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CLM
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CLM set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_ACCIDENT_SERV
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_ACCIDENT_SERV set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_ADOPTION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_ADOPTION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL3
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL3 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL4
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL4 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL5
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL5 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL6
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL6 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_BONIFIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_BONIFIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_FORMATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_FORMATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_GARDE_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_GARDE_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_MALADIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_MALADIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_MATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_MATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_PATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_PATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONSERVATION_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONSERVATION_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONTRAT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONTRAT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONTRAT_HEBERGES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONTRAT_HEBERGES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CPA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CPA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CRCT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CRCT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DECHARGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DECHARGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DECLARATION_MATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DECLARATION_MATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DELEGATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DELEGATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DEPART
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DEPART set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DIF
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DIF set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DROITS_GARDE_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DROITS_GARDE_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ELECTRA_FICHIER
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ELECTRA_FICHIER set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ELEMENT_CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ELEMENT_CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EMERITAT
  WHERE  no_dossier_pers = oldno and tem_valide ='O';

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EMERITAT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ENSEIGNEMENT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ENSEIGNEMENT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EVALUATION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EVALUATION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EVALUATION
  WHERE  no_individu_resp = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EVALUATION set no_individu_resp = '||newno||' where no_individu_resp = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EXAMEN_MEDICAL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EXAMEN_MEDICAL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DROIT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DROIT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DROIT
  WHERE  dro_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DROIT set dro_no_individu = '||newno||' where dro_no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DRO_NOUV_ENTRANT
  WHERE  no_individu_entrant = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DRO_NOUV_ENTRANT set no_individu_entrant = '||newno||' where no_individu_entrant = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DRO_NOUV_ENTRANT
  WHERE  no_individu_resp = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DRO_NOUV_ENTRANT set no_individu_resp = '||newno||' where no_individu_resp = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.HIERARCHIE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.HIERARCHIE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.HIERARCHIE
  WHERE  no_individu_resp = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.HIERARCHIE set no_individu_resp = '||newno||' where no_individu_resp = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.HISTO_PROMOTION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.HISTO_PROMOTION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.INDIVIDU_DIPLOMES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.INDIVIDU_DIPLOMES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.INDIVIDU_DISTINCTIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.INDIVIDU_DISTINCTIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.INDIVIDU_FORMATIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.INDIVIDU_FORMATIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.LISTE_ELECTEUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.LISTE_ELECTEUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.MAD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.MAD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PERS_BUDGET
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PERS_BUDGET set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PROLONGATION_ACTIVITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PROLONGATION_ACTIVITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.MI_TPS_THERAP
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.MI_TPS_THERAP set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.NBI_IMPRESSIONS
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.NBI_IMPRESSIONS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.NBI_OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.NBI_OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.NOTES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.NOTES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PASSE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PASSE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PERIODES_MILITAIRES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PERIODES_MILITAIRES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PERIODE_TEMPS_PARTIEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PERIODE_TEMPS_PARTIELS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PREF_PERSONNEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PREF_PERSONNEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.RDT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.RDT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.RECUL_AGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.RECUL_AGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.RELIQUATS_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.RELIQUATS_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.REPRISE_TEMPS_PLEIN
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.REPRISE_TEMPS_PLEIN set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.SITUATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.SITUATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.STAGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.STAGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.TEMPS_PARTIEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.TEMPS_PARTIEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.VACCIN
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.VACCIN set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

END;
/




CREATE OR REPLACE PROCEDURE GRHUM.Echanger_Personne
(
  oldid NUMBER,
  newid NUMBER
)
IS
  oldno NUMBER;
  newno NUMBER;
BEGIN
  -- Initialisations...
  SELECT no_individu
  INTO   oldno
  FROM   grhum.INDIVIDU_ULR
  WHERE  pers_id = oldid;

  SELECT no_individu
  INTO   newno
  FROM   grhum.INDIVIDU_ULR
  WHERE  pers_id = newid;
  
  -- **********
  -- ACCORDS...
  -- **********
   Echanger_Personne_Accords(oldid,newid,oldno,newno);

  -- ************
  -- ADMISSION...
  -- ************
   Echanger_Personne_Admission(oldid,newid,oldno,newno);

  -- **********
  -- CONGES...
  -- **********
  --Echanger_Personne_Conges(oldid,newid,oldno,newno);

  -- **********
  -- COURRIER...
  -- **********
  --Echanger_Personne_Courrier(oldid,newid,oldno,newno);

  -- **********
  -- CKTL_GED...
  -- **********
   Echanger_Personne_cktl_ged(oldid,newid,oldno,newno);

  -- **********
  -- DT...
  -- **********
  --Echanger_Personne_Dt(oldid,newid,oldno,newno);
  Echanger_Personne_Dt3(oldid,newid,oldno,newno);


  -- **********
  -- EDTSCOL
  -- **********
  Echanger_Personne_Edtscol(oldid,newid,oldno,newno);

  -- **********
  -- GED...
  -- **********
  Echanger_Personne_Ged(oldid,newid,oldno,newno);

  

  -- **********
  -- HCOMP2...
  -- **********
  Echanger_Personne_Hcomp2(oldid,newid,oldno,newno);

  -- **********
  -- INVENTAIRE...
  -- **********
  --Echanger_Personne_Inventaire(oldid,newid,oldno,newno);

  -- **********
  -- JEFY...
  -- **********
  --Echanger_Personne_Jefy(oldid,newid,oldno,newno);

  -- **********
  -- JEFY_ADMIN...
  -- **********
  Echanger_Personne_Jefy_Admin(oldid,newid,oldno,newno);

  -- **********
  -- JEFY_PAYE...
  -- **********
  Echanger_Personne_Jefy_Paye(oldid,newid,oldno,newno);


  -- **********
  -- MANGUE...
  -- **********
  Echanger_Personne_Mangue(oldid,newid,oldno,newno);

  -- **********
  -- MARACUJA...
  -- **********
  Echanger_Personne_Maracuja(oldid,newid,oldno,newno);

    -- **********
  -- PAPAYE
  -- **********
  -- Echanger_Personne_Papaye(oldid,newid,oldno,newno);

  -- **********
  -- PARC_MATOS...
  -- **********
   -- Echanger_Personne_Parc_Info(oldid,newid,oldno,newno);

  -- **********
  -- PEDA...
  -- **********
  Echanger_Personne_Peda(oldid,newid,oldno,newno);

  -- **********
  -- PRESTATION...
  -- **********
  Echanger_Personne_Prestation(oldid,newid,oldno,newno);

  -- **********
  -- SCOLARITE...
  -- **********
  Echanger_Personne_Scolarite(oldid,newid,oldno,newno);

  -- **********
  -- RESERVATION...
  -- **********
  Echanger_Personne_Reservation(oldid,newid,oldno,newno);

  -- **********
  -- SALLE...
  -- **********
  Echanger_Personne_Salle(oldid,newid,oldno,newno);

  -- **********
  -- PHOTO...
  -- **********
  Echanger_Personne_Photo(oldid,newid,oldno,newno);

  -- **********
  -- GRHUM...
  -- **********
  Echanger_Personne_Grhum(oldid,newid,oldno,newno);
  --

  -- Devalidation du trigger sur INDIVIDU_ULR
  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'ALTER TRIGGER grhum.trg_br_individu_ulr DISABLE;');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update GRHUM.INDIVIDU_ULR set tem_valide = ''N'' where no_individu = '||oldno||';');

  -- Re-validation du trigger sur INDIVIDU_ULR
  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'ALTER TRIGGER grhum.trg_br_individu_ulr ENABLE;');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'insert into GRHUM.ECHANGER_SAUVEGARDE select * from GRHUM.PERSONNE where pers_id  = '||oldid||';');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update GRHUM.PERSONNE set pers_libelle = ''PERSONNE INVALIDE - Contacter le CRI'' where pers_id  = '||oldid||';');
END;
/

-- *****************************************************************************************************************************
-- *****************************************************************************************************************************
-- *****************************************************************************************************************************

-- MAJ de la vue  grhum.v_personnel_non_ens

CREATE OR REPLACE FORCE VIEW grhum.v_personnel_non_ens (no_dossier_pers,
                                                        con_car,
                                                        d_debut,
                                                        d_fin
                                                       )
AS
   SELECT p.no_dossier_pers, 'CAR', c.d_effet_element, c.d_fin_element
     FROM personnel_ulr p, element_carriere c, corps co, type_population tp
    WHERE p.no_dossier_pers = c.no_dossier_pers
      AND c.c_corps = co.c_corps
      AND co.c_type_corps = tp.c_type_population
      AND (tp.tem_itarf = 'O' OR tp.tem_atos = 'O' OR tem_biblio = 'O')
      AND c.tem_valide = 'O'
   UNION
   SELECT c.no_dossier_pers, 'CON', c.d_deb_contrat_trav,
          c.d_fin_contrat_trav
     FROM contrat c, type_contrat_travail t
    WHERE c.c_type_contrat_trav = t.c_type_contrat_trav
      AND c.c_type_contrat_trav NOT IN ('VF', 'VN', 'EE')
      AND c.tem_annulation = 'N'
      AND t.tem_enseignant = 'N'
      AND t.tem_remuneration_accessoire != 'O'
      AND t.tem_titulaire != 'O'
      AND c.no_seq_contrat NOT IN (
             -- exclusion des contrats non enseignant typ√©s
             -- par le typepopulation enseignant
             SELECT c.no_seq_contrat
               FROM contrat c,
                    mangue.contrat_avenant ca,
                    grade g,
                    corps c,
                    type_population tp
              WHERE c.no_seq_contrat = ca.no_seq_contrat
                AND ca.c_grade = g.c_grade
                AND g.c_corps = c.c_corps
                AND c.c_type_corps = tp.c_type_population
                AND ca.tem_annulation = 'N'
                AND tp.tem_enseignant = 'O');
     
-- MAJ de l'indexe des alias pour les structures
-- Contribution de l'INSA de Strasbourg
ALTER INDEX GRHUM.IDX_STR_GRP_ALIAS REBUILD;           
                
-- ----------------------------------------------------------------------------------
-- Pour le Connecteur Harp√®ge-Mangue
-- ----------------------------------------------------------------------------------
declare
	cpt integer;
begin	
	SELECT COUNT(*) INTO cpt  from dba_tab_columns where owner = 'IMPORT' and table_name = 'ENFANT' and column_name = 'LFEN_CODE';
	if (cpt = 0 ) then
    	EXECUTE IMMEDIATE 'ALTER TABLE IMPORT.ENFANT ADD (LFEN_CODE VARCHAR2(4))';
	end if;
end;
/

COMMENT ON COLUMN IMPORT.ENFANT.LFEN_CODE IS 'Code du lien de filiation';

ALTER TABLE IMPORT.ENFANT MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX IMPORT.PK_ENFANT REBUILD TABLESPACE INDX_GRHUM;

grant select on Grhum.lien_filiation_enfant to import;
grant select on mangue.declaration_maternite_seq to import;

/* *******************************************************
-- MAJ d'une contrainte
Old
ALTER TABLE IMPORT.CLM_CORRESP ADD (
  CONSTRAINT CLM_CORRESP_REF_CLM_GRHUM 
  FOREIGN KEY (NO_SEQ_ARRETE) 
  REFERENCES MANGUE.CGNT_MALADIE (NO_SEQ_ARRETE)
  ENABLE VALIDATE);

New
ALTER TABLE IMPORT.CLM_CORRESP ADD (
  CONSTRAINT CLM_CORRESP_REF_CLM_GRHUM 
  FOREIGN KEY (NO_SEQ_ARRETE) 
  REFERENCES MANGUE.CLM (NO_SEQ_ARRETE)
  ENABLE VALIDATE);
************************************************* */
-- Suppression et recr√©ation de la contrainte CLM_CORRESP_REF_CLM_GRHUM 
declare
begin
drop_constraint ('IMPORT', 'CLM_CORRESP', 'CLM_CORRESP_REF_CLM_GRHUM');

end;
/


ALTER TABLE IMPORT.CLM_CORRESP ADD (
  CONSTRAINT CLM_CORRESP_REF_CLM_GRHUM 
  FOREIGN KEY (NO_SEQ_ARRETE) 
  REFERENCES MANGUE.CLM (NO_SEQ_ARRETE)
  ENABLE VALIDATE);







