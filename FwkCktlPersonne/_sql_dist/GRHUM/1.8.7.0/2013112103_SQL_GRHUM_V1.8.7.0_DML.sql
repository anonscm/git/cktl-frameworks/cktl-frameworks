--
-- Patch DML de GRHUM du 21/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.7.0
-- Date de publication : 21/11/2013
-- Auteur(s) : Alain

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--------------------------------V20131121.145724__DML_MAJ_corps.sql---------------------------------
UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '31/12/1987'
WHERE c_corps = '820';

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '01/09/1988'
WHERE c_corps = '477';

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '31/12/1992'
WHERE c_corps in ('471', '456', '473', '478', '476');

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '31/07/1993'
WHERE c_corps = '831';

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '31/07/1995'
WHERE c_corps = '071';

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '31/08/1997'
WHERE c_corps = '523';

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '29/12/2003'
WHERE c_corps in ('041', '600');

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '30/09/2011'
WHERE c_corps = '818';

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '31/08/2011'
WHERE c_corps in ('230', '088', '083', '838');

UPDATE GRHUM.CORPS
SET D_FERMETURE_CORPS = '30/09/2012'
WHERE c_corps = '990';


--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.7.0',to_date('21/11/2013','DD/MM/YYYY'),sysdate,'MAJ des corps et MAJ des vues et des procédures qui ne compilaient plus');
COMMIT;
