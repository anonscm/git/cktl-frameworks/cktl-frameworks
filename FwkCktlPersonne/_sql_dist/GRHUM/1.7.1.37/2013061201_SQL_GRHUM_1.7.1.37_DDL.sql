--
-- Patch DML de GRHUM du 12/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.37 
-- Date de publication : 12/06/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.36';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.37';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.37 a deja ete passe !');
    end if;

end;
/

-- Pour les besoins de la paie
ALTER TABLE GRHUM.RIBFOUR_ULR DROP CONSTRAINT FK_AS_RIBFOUR_ULR_FOURNIS_ULR;

ALTER TABLE GRHUM.RIBFOUR_ULR
ADD ( CONSTRAINT FK_AS_RIBFOUR_ULR_FOURNIS_ULR 
 FOREIGN KEY (FOU_ORDRE) 
 REFERENCES GRHUM.FOURNIS_ULR (FOU_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);


declare
  cpt integer;
begin  
  select count(*) into cpt from dba_tab_columns
    where owner = 'GRHUM' and table_name = 'MOTIF_TEMPS_PARTIEL' and column_name = 'TEM_QUOTITE_LIMITEE';
--  if cpt = 1 then
--    raise_application_error(-20001,'La colonne TEM_QUOTITE_LIMITEE existe déjà');
--  end if;
  
  if cpt = 0 then
    execute immediate '
    ALTER TABLE GRHUM.MOTIF_TEMPS_PARTIEL ADD TEM_QUOTITE_LIMITEE VARCHAR2(1) DEFAULT ''N'' NOT NULL
    ';
  end if;
end;
/


UPDATE GRHUM.MOTIF_TEMPS_PARTIEL set TEM_QUOTITE_LIMITEE = 'O' where C_MOTIF_TEMPS_PARTIEL IN ('SA', 'FH');

ALTER TABLE GRHUM.TYPE_CONTRAT_TRAVAIL MODIFY (LL_TYPE_CONTRAT_TRAV VARCHAR2(100));

UPDATE GRHUM.TYPE_CONTRAT_TRAVAIL SET LL_TYPE_CONTRAT_TRAV = 'Personnels enseignants associés à mi-temps' where c_type_contrat_trav = 'PA';





-- Mise à jour de la table des équipements pour ajouter un code barre
-- EDT_SCOL 
 --GRANT RESOURCE TO EDTSCOL;

 ALTER TABLE edtscol.resa_objet ADD ( ro_code_barre varchar2(256));
 
 ALTER USER EDTSCOL quota unlimited ON SCOL;
 ALTER USER EDTSCOL quota unlimited ON SCOL_INDX;
 
 alter table edtscol.resa_objet move tablespace scol;
 alter index edtscol.table1_pk rebuild tablespace scol_indx;
 comment ON COLUMN  edtscol.resa_objet.ro_code_barre is 'Code barre de l''équipement';
 
 insert into edtscol.db_version (db_version, db_date, db_comment)
values ('1.4.2.1', to_date('21/05/2013','dd/mm/yyyy'), 'Ajout du code barre - via patch GRHUM 1.7.1.37');

-- GRHUM
-- Mise à jour de la table salle pour un point sur le plan
ALTER TABLE grhum.salles ADD ( sal_coordonnees varchar2(256));
alter table grhum.salles move tablespace data_grhum;
alter index grhum.pk_salle rebuild tablespace indx_grhum;
alter index grhum.index_salles rebuild tablespace indx_grhum;
comment ON COLUMN grhum.salles.sal_coordonnees is 'Point sur le plan';

-- Suppression de la contrainte unique sur appellation
-- Creation d'un trigger qui vérifie l'unicité de la IMP_GEO/LOCAL 

DROP INDEX "GRHUM"."UK_LOCAL_APPELLATION"; 
CREATE INDEX "GRHUM"."UK_LOCAL_APPELLATION" ON "GRHUM"."LOCAL" ("APPELLATION") TABLESPACE "INDX_GRHUM";

-- Trigger qui vérifie l'unicité de Implémentation/nom du local

CREATE OR REPLACE TRIGGER TRG_BR_REPART_BAT_IMP_GEO 
BEFORE INSERT OR UPDATE ON REPART_BAT_IMP_GEO
FOR EACH ROW
DECLARE
  w_appellation varchar2(250);
  w_count number:=0;
BEGIN
-- Trigger qui vérifie que deux bâtiments ne porte pas le même nom dans la même implémentation géographique 
  
  select lo.appellation into w_appellation 
  from grhum.local lo
  where lo.c_local=:NEW.c_local;
  
  select count (*) into w_count from grhum.local lo, grhum.repart_bat_imp_geo rbi
  where rbi.imgeo_ordre = :NEW.imgeo_ordre
        and rbi.c_local=lo.c_local
        and upper(lo.appellation) = upper (w_appellation);
  
  if w_count>0 then
  RAISE_APPLICATION_ERROR(-20000,'TRIGGER_REPART_BAT_GEO : un bâtiment porte déjà le nom :'|| w_appellation); 
  end if;
  
END;
/

-- A destination de Physallis
DECLARE
	
  ass_id_recherche INTEGER;
	ass_id_types_membres_unite INTEGER;
	ass_id_types_membres_sr INTEGER;
	ass_id_membre_permanent INTEGER;
	ass_id_membre_non_permanent INTEGER;
	ass_id_doctorant INTEGER;	
  
BEGIN

  INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'DOCTORANT', SYSDATE,SYSDATE,'1','DOCT','N');
  INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'DIRECTION', TO_DATE('27/04/11', 'DD/MM/RR'), TO_DATE('27/04/11', 'DD/MM/RR'), '1', 'DIRECTION', 'N');
  INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'MEMBRE PERMANENT', sysdate, sysdate, '1', 'MEMBRE P', 'N');
  INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'MEMBRE NON PERMANENT', sysdate, sysdate, '1', 'MEMBRE NP', 'N');
  
  
  SELECT ass_id INTO ass_id_recherche FROM GRHUM.ASSOCIATION WHERE ass_libelle LIKE 'RECHERCHE';
  SELECT ass_id INTO ass_id_membre_permanent FROM GRHUM.ASSOCIATION WHERE ass_libelle LIKE 'MEMBRE PERMANENT';
  SELECT ass_id INTO ass_id_membre_non_permanent FROM GRHUM.ASSOCIATION WHERE ass_libelle LIKE 'MEMBRE NON PERMANENT';
  SELECT ass_id INTO ass_id_doctorant FROM GRHUM.ASSOCIATION WHERE ass_libelle LIKE 'DOCTORANT';
  
  INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, ASS_CODE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'TYPES MEMBRES UNITE', 'TYP_MEM_UNI', sysdate, sysdate, '1', 'N');
  INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, ASS_CODE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'TYPES MEMBRES STRUCTURE RECHERCHE', 'TYPE_MEM_SR', sysdate, sysdate, '1', 'N');
  
  SELECT ass_id INTO ass_id_types_membres_unite FROM association WHERE ass_libelle LIKE 'TYPES MEMBRES UNITE';
  SELECT ass_id INTO ass_id_types_membres_sr FROM association WHERE ass_libelle LIKE 'TYPES MEMBRES STRUCTURE RECHERCHE';
  
  INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, D_CREATION, D_MODIFICATION) VALUES (GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL, ass_id_recherche, ass_id_types_membres_unite, sysdate, sysdate);
  INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, D_CREATION, D_MODIFICATION) VALUES (GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL, ass_id_recherche, ass_id_types_membres_sr, sysdate, sysdate);
  INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, D_CREATION, D_MODIFICATION) VALUES (GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL, ass_id_types_membres_unite, ass_id_membre_permanent, sysdate, sysdate);
  INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, D_CREATION, D_MODIFICATION) VALUES (GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL, ass_id_types_membres_unite, ass_id_membre_non_permanent, sysdate, sysdate);
  INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, D_CREATION, D_MODIFICATION) VALUES (GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL, ass_id_types_membres_unite, ass_id_doctorant, sysdate, sysdate);
  INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, D_CREATION, D_MODIFICATION) VALUES (GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL, ass_id_types_membres_sr, ass_id_membre_permanent, sysdate, sysdate);
  INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, D_CREATION, D_MODIFICATION) VALUES (GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL, ass_id_types_membres_sr, ass_id_membre_non_permanent, sysdate, sysdate);

END;
/

-- MAJ procédure ECHANGER_PERSONNE_MANGUE
create or replace
PROCEDURE       Echanger_Personne_Mangue
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS

  nbenr1 INTEGER;

BEGIN

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ABSENCES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ABSENCES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.AFFECTATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.AFFECTATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.AGENT_DROITS_SERVICES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.AGENT_DROITS_SERVICES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.AGENT_PERSONNEL
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.AGENT_PERSONNEL set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CFA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CFA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CGNT_ACCIDENT_TRAV
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CGNT_ACCIDENT_TRAV set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CGNT_CGM
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CGNT_CGM set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CGNT_MALADIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CGNT_MALADIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CHANGEMENT_POSITION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CHANGEMENT_POSITION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CLM
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CLM set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_ACCIDENT_SERV
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_ACCIDENT_SERV set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_ADOPTION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_ADOPTION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL3
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL3 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL4
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL4 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL5
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL5 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_AL6
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_AL6 set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_BONIFIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_BONIFIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_FORMATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_FORMATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_GARDE_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_GARDE_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_MALADIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_MALADIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_MALADIE_DETAIL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_MALADIE_DETAIL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_MATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_MATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONGE_PATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONGE_PATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONSERVATION_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONSERVATION_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONTRAT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONTRAT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CONTRAT_HEBERGES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CONTRAT_HEBERGES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CPA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CPA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.CRCT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.CRCT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DECHARGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DECHARGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DECLARATION_MATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DECLARATION_MATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DELEGATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DELEGATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DEPART
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DEPART set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DIF
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DIF set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.DROITS_GARDE_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.DROITS_GARDE_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ELECTRA_FICHIER
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ELECTRA_FICHIER set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ELEMENT_CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ELEMENT_CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EMERITAT
  WHERE  no_dossier_pers = oldno and tem_valide ='O';

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EMERITAT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.ENSEIGNEMENT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.ENSEIGNEMENT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EVALUATION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EVALUATION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EVALUATION
  WHERE  no_individu_resp = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EVALUATION set no_individu_resp = '||newno||' where no_individu_resp = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.EXAMEN_MEDICAL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.EXAMEN_MEDICAL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DROIT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DROIT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DROIT
  WHERE  dro_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DROIT set dro_no_individu = '||newno||' where dro_no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DRO_NOUV_ENTRANT
  WHERE  no_individu_entrant = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DRO_NOUV_ENTRANT set no_individu_entrant = '||newno||' where no_individu_entrant = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.FEV_DRO_NOUV_ENTRANT
  WHERE  no_individu_resp = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.FEV_DRO_NOUV_ENTRANT set no_individu_resp = '||newno||' where no_individu_resp = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.HIERARCHIE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.HIERARCHIE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.HIERARCHIE
  WHERE  no_individu_resp = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.HIERARCHIE set no_individu_resp = '||newno||' where no_individu_resp = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.HISTO_PROMOTION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.HISTO_PROMOTION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.INDIVIDU_DIPLOMES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.INDIVIDU_DIPLOMES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.INDIVIDU_DISTINCTIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.INDIVIDU_DISTINCTIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.INDIVIDU_FORMATIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.INDIVIDU_FORMATIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.LISTE_ELECTEUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.LISTE_ELECTEUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.MAD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.MAD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PERS_BUDGET
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PERS_BUDGET set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PROLONGATION_ACTIVITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PROLONGATION_ACTIVITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.MI_TPS_THERAP
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.MI_TPS_THERAP set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.NBI_IMPRESSIONS
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.NBI_IMPRESSIONS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.NBI_OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.NBI_OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.NOTES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.NOTES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PASSE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PASSE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PERIODES_MILITAIRES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PERIODES_MILITAIRES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PERIODE_TEMPS_PARTIEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PERIODE_TEMPS_PARTIELS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.PREF_PERSONNEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.PREF_PERSONNEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.RDT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.RDT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.RECUL_AGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.RECUL_AGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.RELIQUATS_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.RELIQUATS_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.REPRISE_TEMPS_PLEIN
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.REPRISE_TEMPS_PLEIN set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.SITUATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.SITUATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.STAGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.STAGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.TEMPS_PARTIEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.TEMPS_PARTIEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   MANGUE.VACCIN
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update MANGUE.VACCIN set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

END;
/

--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.37', TO_DATE('12/06/2013', 'DD/MM/YYYY'),NULL,'Actualisation des tables des salles, Ajout d''un rôle, MAJ de valeurs dans les tables GRHUM.RNE, TYPE_CONTRAT_TRAVAIL et TYPE_DECHARGE_SERVICE, un ajout dans EDTSCOL.RESA_OBJETC ');

--
-- 
--


COMMIT;




