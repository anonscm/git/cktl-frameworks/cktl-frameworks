--
-- Patch DML de GRHUM du 12/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.37
-- Date de publication : 12/06/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- Ajout d'un rôle indispensable pour la future application Peche

DECLARE
    compteur NUMBER;
BEGIN
	SELECT COUNT(*) INTO compteur FROM GRHUM.ASSOCIATION WHERE ASS_CODE = 'REPARTITEUR';
	IF (compteur = 0) THEN
		--
		-- ASSOCIATION
		--
		INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, TAS_ID, ASS_CODE, D_OUVERTURE)
		     VALUES (ASSOCIATION_SEQ.NEXTVAL, 'RÉPARTITEUR', 1, 'REPARTITEUR', TO_DATE('21/06/2012', 'DD/MM/YYYY'));
        
		--
		-- ASSOCIATION_RESEAU
		--
		INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, ASR_RANG, ASR_COMMENTAIRE, D_CREATION, D_MODIFICATION)
		     SELECT ASSOCIATION_RESEAU_SEQ.NEXTVAL, A1.ASS_ID, A2.ASS_ID, NULL, NULL, SYSDATE, SYSDATE
		       FROM ASSOCIATION A1, ASSOCIATION A2
		      WHERE A1.ASS_CODE = 'FONCTION ADM' AND A2.ASS_CODE = 'REPARTITEUR';
	END IF;

	COMMIT;
END;
/

-- Table GRHUM.COMMUNE
UPDATE "GRHUM"."COMMUNE" SET D_FIN_VAL = TO_DATE('01/01/2013', 'DD/MM/YYYY') WHERE LL_COM='BEGLES' AND C_POSTAL='33015';



-- Table GRHUM.RNE :
UPDATE "GRHUM"."RNE" SET ville = 'BORDEAUX' WHERE c_rne = '0330027A';
UPDATE "GRHUM"."RNE" SET ville = 'BORDEAUX' WHERE c_rne = '0330029C';
UPDATE "GRHUM"."RNE" SET code_postal = '33800' WHERE c_rne = '0330029C';

insert into grhum.RNE (c_rne, ll_rne, lc_rne, acad_code, etab_statut, adresse, code_postal, ville, d_creation, d_modification)
values ( '0641784S','ECOLE ELEMENTAIRE PUBLIQUE JEAN SARRAILH', 'ECOLE PRIMAIRE PU','004','PU','1 AVENUE ROBERT SCHUMAN', 64000, 'PAU', SYSDATE, SYSDATE);

UPDATE grhum.rne set ville='NAVARRENX' WHERE c_rne = '0640045B';
UPDATE grhum.rne set ville='SALIES DE BEARN' WHERE c_rne = '0640071E';




-- Tables TYPE_CONTRAT_TRAVAIL & TYPE_DECHARGE_SERVICE
UPDATE GRHUM.type_contrat_travail set d_deb_val = to_date('06/06/1969', 'dd/mm/yyyy') where c_type_contrat_trav = 'OT';

UPDATE GRHUM.TYPE_DECHARGE_SERVICE SET TEM_HCOMP = 'O' where C_TYPE_DECHARGE IN('DR1', 'DE1', 'PR3', 'PR4');

--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.37';

COMMIT;