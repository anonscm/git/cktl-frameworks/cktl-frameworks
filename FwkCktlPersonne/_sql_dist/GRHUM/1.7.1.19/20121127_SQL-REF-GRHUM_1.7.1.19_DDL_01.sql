--
-- Patch DDL de GRHUM du 27/11/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.19
-- Date de publication : 27/11/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.18';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.19';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.19 a deja ete passe !');
    end if;

end;
/



-- Correction index quand BIC nul dans les tem_local ='N' + création meme banque avec tem_local='O' + BIC
execute grhum.drop_object('GRHUM','UK_BANQ_GUICHET','index');
--drop index GRHUM.UK_BANQ_GUICHET;


CREATE UNIQUE INDEX GRHUM.UK_BANQ_GUICHET ON GRHUM.BANQUE
(C_BANQUE, C_GUICHET, DECODE(TEM_LOCAL||substr( decode( bic,null,'XXXXFR',bic  ),5,2),'NFR',TEM_LOCAL,NULL))
 LOGGING
TABLESPACE INDX_GRHUM;



CREATE OR REPLACE PROCEDURE GRHUM.Ins_Ribfour_Ulr
(
INS_C_BANQUE        RIBFOUR_ULR.c_banque%TYPE,
INS_C_GUICHET       RIBFOUR_ULR.c_guichet%TYPE,
INS_CLE_RIB         RIBFOUR_ULR.cle_rib%TYPE,
INS_FOU_ORDRE       RIBFOUR_ULR.fou_ordre%TYPE,
INS_MOD_CODE        RIBFOUR_ULR.mod_code%TYPE,
INS_NO_COMPTE       RIBFOUR_ULR.no_compte%TYPE,
INS_RIB_TITCO       RIBFOUR_ULR.rib_titco%TYPE,
INS_RIB_VALIDE      RIBFOUR_ULR.rib_valide%TYPE,
INS_BIC				RIBFOUR_ULR.bic%TYPE,
INS_IBAN			RIBFOUR_ULR.iban%TYPE
)
IS

retourIban	   INTEGER;
cpt            INTEGER;
ribOrdre       NUMBER;
ibanNew        GRHUM.RIBFOUR_ULR.iban%TYPE;
banqOrdre	   GRHUM.BANQUE.BANQ_ORDRE%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RIBFOUR_ULR
WHERE fou_ordre=INS_FOU_ORDRE
AND c_banque=INS_C_BANQUE
AND c_guichet=INS_C_GUICHET
AND no_compte=INS_NO_COMPTE
AND rib_valide = 'O';

IF (cpt>0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_RIBFOUR_ULR : Référence bancaire existante');
END IF;

-- Clef primaire de BANQUE
--SELECT BANQ_ORDRE INTO banqOrdre FROM GRHUM.BANQUE WHERE C_BANQUE=INS_C_BANQUE AND C_GUICHET=INS_C_GUICHET;
-- Modification UBX 12/11/2012 pour prise en compte des banques fichier NDF ou locales 
select banq_ordre into banqOrdre from GRHUM.BANQUE WHERE C_BANQUE=INS_C_BANQUE AND C_GUICHET=INS_C_GUICHET and rownum = 1 order by tem_local;

-- Clef primaire de RIBFOUR
SELECT SEQ_RIBFOUR_ULR.NEXTVAL INTO ribOrdre FROM dual;

-- Vérification du RIB
GRHUM.Verif_Rib(INS_C_BANQUE, INS_C_GUICHET ,INS_NO_COMPTE, INS_CLE_RIB);

IF (INS_IBAN IS NOT NULL) THEN
   -- test de la clef du code IBAN
   retourIban := Verif_Iban(INS_IBAN);
   IF (retourIban = 0) THEN
      RAISE_APPLICATION_ERROR(-20001,'INS_RIBFOUR_ULR : Le code IBAN est incorrect.');
   END IF;
   ibanNew := INS_IBAN;
ELSE
   -- constitution automatique de l'IBAN
   ibanNew := 'FR'||GRHUM.CONS_CLEF_IBAN('FR',INS_C_BANQUE||INS_C_GUICHET||INS_NO_COMPTE||INS_CLE_RIB)||INS_C_BANQUE||INS_C_GUICHET||INS_NO_COMPTE||INS_CLE_RIB;
END IF;

INSERT INTO RIBFOUR_ULR(RIB_ORDRE,FOU_ORDRE,C_BANQUE,C_GUICHET,NO_COMPTE,CLE_RIB,RIB_TITCO,MOD_CODE,RIB_VALIDE,D_CREATION,D_MODIFICATION,TEM_PAYE_UTIL,BIC,IBAN,PERS_ID_CREATION, PERS_ID_MODIFICATION, BANQ_ORDRE)
VALUES (ribOrdre,INS_FOU_ORDRE,INS_C_BANQUE,INS_C_GUICHET,INS_NO_COMPTE,INS_CLE_RIB,INS_RIB_TITCO,INS_MOD_CODE,'O',SYSDATE,SYSDATE,'N',INS_BIC,ibanNew,NULL,NULL,banqOrdre);

END;
/


