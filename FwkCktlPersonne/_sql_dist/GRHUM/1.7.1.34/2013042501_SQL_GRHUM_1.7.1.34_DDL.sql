--
-- Patch DML de GRHUM du 25/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.34 
-- Date de publication : 25/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.33';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.34';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.34 a deja ete passe !');
    end if;

end;
/




declare

   c int;

begin

  select count(*) into c from user_tables where table_name = upper('NATURE_BONIF_TAUX');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.NATURE_BONIF_TAUX CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('NATURE_BONIF_TAUX_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.NATURE_BONIF_TAUX_SEQ
      ';
   end if;
   
   select count(*) into c from user_tables where table_name = upper('TYPE_CONTRAT_GRADES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.TYPE_CONTRAT_GRADES CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('TYPE_CONTRAT_GRADES_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.TYPE_CONTRAT_GRADES_SEQ
      ';
   end if;
   
   
   select count(*) into c from user_tables where table_name = upper('TYPE_ACCES_GRADE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.TYPE_ACCES_GRADE CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('TYPE_ACCES_GRADE_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.TYPE_ACCES_GRADE_SEQ
      ';
   end if;
   
   select count(*) into c from user_tables where table_name = upper('TYPE_ACCES_CORPS');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.TYPE_ACCES_CORPS CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('TYPE_ACCES_CORPS_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.TYPE_ACCES_CORPS_SEQ
      ';
   end if;

end;
/

-- Creation d'une table stockant les taux de bonifications possibles (ManGUE)
CREATE TABLE GRHUM.NATURE_BONIF_TAUX
(
  NBT_ORDRE       NUMBER                   NOT NULL,
  NBT_CODE     	  VARCHAR2(5)                   NOT NULL,
  NBT_LIBELLE     VARCHAR2(100)                 NOT NULL,
  NBT_NUM_TAUX    NUMBER                        NOT NULL,
  NBT_DEN_TAUX    NUMBER                        NOT NULL,
  D_CREATION      DATE                          NOT NULL,
  D_MODIFICATION  DATE                          NOT NULL
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX GRHUM.PK_NAT_TAUX ON GRHUM.NATURE_BONIF_TAUX
(NBT_ORDRE)
LOGGING
TABLESPACE INDX_GRHUM
NOPARALLEL;


ALTER TABLE GRHUM.NATURE_BONIF_TAUX ADD (
  CONSTRAINT PK_NAT_TAUX
 PRIMARY KEY
 (NBT_ORDRE)
    USING INDEX);

GRANT REFERENCES, SELECT ON GRHUM.NATURE_BONIF_TAUX TO MANGUE;

CREATE SEQUENCE GRHUM.NATURE_BONIF_TAUX_SEQ START WITH 1 NOCACHE;


--
-- Creation d'une table de repartition entre les contrats et les grades
--

CREATE TABLE GRHUM.TYPE_CONTRAT_GRADES
(
  ID_TCG            NUMBER,
  C_GRADE         VARCHAR2(4)                   NOT NULL,
  C_TYPE_CONTRAT_TRAV        VARCHAR2(2)                  NOT NULL,
  D_CREATION      DATE                          NOT NULL,
  D_MODIFICATION  DATE                          NOT NULL
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX GRHUM.PK_CONTRAT_GRADES ON GRHUM.type_contrat_grades
(ID_TCG)
LOGGING
TABLESPACE INDX_GRHUM
NOPARALLEL;

ALTER TABLE GRHUM.type_contrat_grades ADD (
  CONSTRAINT PK_CONTRAT_GRADEs 
 PRIMARY KEY
 (ID_TCG)
    USING INDEX);


ALTER TABLE GRHUM.TYPE_CONTRAT_GRADES ADD (
  CONSTRAINT FK_TCG_GRADE 
 FOREIGN KEY (C_GRADE) 
 REFERENCES GRHUM.GRADE (C_GRADE),
  CONSTRAINT FK_AS_TCD_TYPE_CTR
 FOREIGN KEY (C_TYPE_CONTRAT_TRAV) 
 REFERENCES GRHUM.TYPE_CONTRAT_TRAVAIL (C_TYPE_CONTRAT_TRAV));


CREATE SEQUENCE GRHUM.TYPE_CONTRAT_GRADES_SEQ start with 1 nocache;


--
--
--
 
UPDATE GRHUM.TYPE_CONTRAT_TRAVAIL SET TEM_EQUIV_GRADE = 'O' WHERE C_TYPE_CONTRAT_TRAV IN('LT','LB');


-- Les types d'acces vont maintenant etre repartis en deux groupes ACCES et GRADES

CREATE TABLE GRHUM.TYPE_ACCES_GRADE
(
  C_TYPE_ACCES      VARCHAR2(5)                 NOT NULL,
  LC_TYPE_ACCES     VARCHAR2(50),
  LL_TYPE_ACCES     VARCHAR2(100),
  D_OUVERTURE       DATE,
  D_FERMETURE       DATE,
  D_CREATION        DATE                        NOT NULL,
  D_MODIFICATION    DATE                        NOT NULL
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX GRHUM.PK_TYPE_ACCES_G ON GRHUM.TYPE_ACCES_GRADE
(C_TYPE_ACCES)
LOGGING
TABLESPACE INDX_GRHUM
NOPARALLEL;

ALTER TABLE GRHUM.TYPE_ACCES_GRADE ADD (
  CONSTRAINT PK_TYPE_ACCES_G
 PRIMARY KEY
 (C_TYPE_ACCES)
    USING INDEX);

GRANT REFERENCES, SELECT ON GRHUM.TYPE_ACCES_GRADE TO MANGUE;


CREATE TABLE GRHUM.TYPE_ACCES_CORPS
(
  C_TYPE_ACCES      VARCHAR2(5)                 NOT NULL,
  LC_TYPE_ACCES     VARCHAR2(50),
  LL_TYPE_ACCES     VARCHAR2(100),
  D_OUVERTURE       DATE,
  D_FERMETURE       DATE,
  D_CREATION        DATE                        NOT NULL,
  D_MODIFICATION    DATE                        NOT NULL
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX GRHUM.PK_TYPE_ACCES_C ON GRHUM.TYPE_ACCES_CORPS
(C_TYPE_ACCES)
LOGGING
TABLESPACE INDX_GRHUM
NOPARALLEL;

ALTER TABLE GRHUM.TYPE_ACCES_CORPS ADD (
  CONSTRAINT PK_TYPE_ACCES_C
 PRIMARY KEY
 (C_TYPE_ACCES)
    USING INDEX);

GRANT REFERENCES, SELECT ON GRHUM.TYPE_ACCES_CORPS TO MANGUE;



--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.34', TO_DATE('25/04/2013', 'DD/MM/YYYY'),NULL,'Ajout de nouveaux grades et échelons Mangue avec les bons indexes');

--
-- 
--


COMMIT;




