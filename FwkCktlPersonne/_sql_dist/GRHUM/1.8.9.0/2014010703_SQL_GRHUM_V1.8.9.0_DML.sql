--
-- Patch DML de GRHUM du 07/01/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.9.0
-- Date de publication : 07/01/2014
-- Auteur(s) : Alain Malaplate

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-------------------------------V20140106.151344__DML_NOUVEAUX_UAI.sql-------------------------------
--------------------------------------------------------
--  RNE fourni par l'ex-ENSI Bourges   
--------------------------------------------------------
-- Fermeture de l'ENSI de Bourges et de l'ENI du Val de Loire au 31/12/2013 suite à la fusion INSA Centre Val de Loire
update grhum.rne set D_FIN_VAL=to_date('31/12/13','DD/MM/RR'),D_MODIFICATION=to_date('25/10/13','DD/MM/RR') where c_rne in ('0180910S','0410979S');

-- Création de l'INSA Centre Val de Loire à compter du 01/01/2014
Insert into GRHUM.RNE (C_RNE,LL_RNE,ADRESSE,CODE_POSTAL,C_RNE_PERE,D_DEB_VAL,D_FIN_VAL,D_CREATION,D_MODIFICATION,LC_RNE,ACAD_CODE,ETAB_STATUT,VILLE,SIRET)
values ('0180974L','INSA CENTRE VAL DE LOIRE','88 BOULEVARD LAHITOLLE','18020',null,to_date('01/01/14','DD/MM/RR'),null,to_date('25/10/13','DD/MM/RR'),to_date('25/10/13','DD/MM/RR'),'INSA CVL','018','PU','BOURGES','13001833600011');

-- Création d'un second numéro pour les enquètes ministériels sur la localisation des emplois et des étudiants
Insert into GRHUM.RNE (C_RNE,LL_RNE,ADRESSE,CODE_POSTAL,C_RNE_PERE,D_DEB_VAL,D_FIN_VAL,D_CREATION,D_MODIFICATION,LC_RNE,ACAD_CODE,ETAB_STATUT,VILLE,SIRET)
values ('0411068N','CAMPUS BLOIS - INSA CENTRE VAL DE LOIRE','3 RUE DE LA CHOCOLATERIE','41034','0180974L',to_date('01/01/14','DD/MM/RR'),null,to_date('22/11/13','DD/MM/RR'),to_date('22/11/13','DD/MM/RR'),'CAMPUS BLOIS','041','PU','BLOIS','13001833600029');



--------------------------------------------------------
--  RNE fourni par l'UTBM 
--------------------------------------------------------
--Institut Supérieur de l'Electronique et du Numérique (ISEN)
update grhum.rne set ville='BREST',adresse='20 rue Cuirassé',code_postal='29228',acad_code='014' where c_rne='0292125C';


--------------------------------------------------------
--  RNE fourni par l'INSA Rennes 
--------------------------------------------------------
--lycée Marceau de CHARTRES qui n'a pas de ville dans la table grhum.rne
--http://www.education.gouv.fr/annuaire/28-eure-et-loir/chartres/lycee/lycee-marceau.html
update grhum.rne set adresse = '2 rue Pierre Mendès-France', code_postal='28000', ville=upper('Chartres') where c_rne='0280007F';

--------------------------------------------------------
--  RNE fourni par l'Université de Nîmes
--------------------------------------------------------
--Lycée Jacques Prévert dont voici les spécifications :

--http://www.education.gouv.fr/annuaire/30-gard/saint-christol-les-ales/lycee/lycee-jacques-prevert-(voie-generale-et-technologique).html
DECLARE

cpt INTEGER;

BEGIN

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0301778V';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Lycée Polyvalent Jacques Prévert', lc_rne = 'LPO J PREVERT', adresse = '1 PL LUCIE AUBRAC', code_postal = '30380', d_modification = sysdate, acad_code = '011', ville = 'SAINTT-CHRISTOL-LES-ALES'
        WHERE C_RNE = '0301778V';

     ELSE

  insert into grhum.RNE (c_rne, ll_rne, lc_rne, acad_code, etab_statut, adresse, code_postal, ville, d_creation, d_modification, siret)
    values ( '0301778V','Lycée Polyvalent Jacques Prévert', 'LPO J PREVERT','011','PU','1 PL LUCIE AUBRAC', '30380', 'SAINTT-CHRISTOL-LES-ALES', SYSDATE, SYSDATE, '20001994100018');
    
    end if;    
 

END;
/

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.9.0',to_date('07/01/2014','DD/MM/YYYY'),sysdate,'Nettoyage et actualisation de Procédures, MAJ de RNE ');
COMMIT;
