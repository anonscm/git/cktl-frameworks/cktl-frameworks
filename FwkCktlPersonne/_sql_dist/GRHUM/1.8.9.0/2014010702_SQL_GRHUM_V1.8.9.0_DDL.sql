--
-- Patch DDL de GRHUM du 07/01/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.9.0
-- Date de publication : 07/01/2014
-- Auteur(s) : Alain Malaplate

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.8.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.9.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.9.0 a deja ete passe !');
    end if;

end;
/


--------------------------V20140106.151135__DDL_INS_STRUCTURE_ULR_SYNC.sql--------------------------
create or replace
PROCEDURE       "GRHUM"."Ins_Structure_Ulr_Sync"
(
  cstructure                OUT STRUCTURE_ULR.c_structure%TYPE,
  persid                    OUT STRUCTURE_ULR.pers_id%TYPE,
  INS_LL_STRUCTURE          STRUCTURE_ULR.ll_structure%TYPE,
  INS_LC_STRUCTURE          STRUCTURE_ULR.lc_structure%TYPE,
  INS_C_TYPE_STRUCTURE      STRUCTURE_ULR.c_type_structure%TYPE,
  INS_C_STRUCTURE_PERE      STRUCTURE_ULR.c_structure_pere%TYPE,
  INS_C_TYPE_ETABLISSEMEN   STRUCTURE_ULR.c_type_etablissemen%TYPE,
  INS_C_ACADEMIE            STRUCTURE_ULR.c_academie%TYPE,
  INS_C_STATUT_JURIDIQUE    STRUCTURE_ULR.c_statut_juridique%TYPE,
  INS_C_RNE                 STRUCTURE_ULR.c_rne%TYPE,
  INS_SIRET                 STRUCTURE_ULR.siret%TYPE,
  INS_SIREN                 STRUCTURE_ULR.siren%TYPE,
  INS_C_NAF                 STRUCTURE_ULR.c_naf%TYPE,
  INS_C_TYPE_DECISION_STR   STRUCTURE_ULR.c_type_decision_str%TYPE,
  INS_REF_EXT_ETAB          STRUCTURE_ULR.ref_ext_etab%TYPE,
  INS_REF_EXT_COMP          STRUCTURE_ULR.ref_ext_comp%TYPE,
  INS_REF_EXT_CR            STRUCTURE_ULR.ref_ext_cr%TYPE,
  INS_REF_DECISION          STRUCTURE_ULR.ref_decision%TYPE,
  INS_DATE_DECISION         STRUCTURE_ULR.date_decision%TYPE,
  INS_DATE_OUVERTURE        STRUCTURE_ULR.date_ouverture%TYPE,
  INS_DATE_FERMETURE        STRUCTURE_ULR.date_fermeture%TYPE,
  INS_STR_ORIGINE            STRUCTURE_ULR.str_origine%TYPE,
  INS_STR_PHOTO              STRUCTURE_ULR.str_photo%TYPE,
  INS_STR_ACTIVITE            STRUCTURE_ULR.str_activite%TYPE,
  INS_GRP_OWNER             STRUCTURE_ULR.grp_owner%TYPE,
  INS_GRP_RESPONSABLE        STRUCTURE_ULR.grp_responsable%TYPE,
  INS_GRP_FORME_JURIDIQUE     STRUCTURE_ULR.grp_forme_juridique%TYPE,
  INS_GRP_CAPITAL              STRUCTURE_ULR.grp_capital%TYPE,
  INS_GRP_CA                  STRUCTURE_ULR.grp_ca%TYPE,
  INS_GRP_EFFECTIFS         STRUCTURE_ULR.grp_effectifs%TYPE,
  INS_GRP_CENTRE_DECISION    STRUCTURE_ULR.grp_centre_decision%TYPE,
  INS_GRP_APE_CODE          STRUCTURE_ULR.grp_ape_code%TYPE,
  INS_GRP_APE_CODE_BIS      STRUCTURE_ULR.grp_ape_code_bis%TYPE,
  INS_GRP_APE_CODE_COMP      STRUCTURE_ULR.grp_ape_code_comp%TYPE,
  INS_GRP_ACCES             STRUCTURE_ULR.grp_acces%TYPE,
  INS_GRP_ALIAS             STRUCTURE_ULR.grp_alias%TYPE,
  INS_GRP_RESPONSABILITE     STRUCTURE_ULR.grp_responsabilite%TYPE,
  INS_GRP_TRADEMARQUE         STRUCTURE_ULR.grp_trademarque%TYPE,
  INS_GRP_WEBMESTRE         STRUCTURE_ULR.grp_webmestre%TYPE,
  INS_GRP_FONCTION1         STRUCTURE_ULR.grp_fonction1%TYPE,
  INS_GRP_FONCTION2         STRUCTURE_ULR.grp_fonction2%TYPE,
  INS_ORG_ORDRE             STRUCTURE_ULR.org_ordre%TYPE,
  INS_GRP_MOTS_CLEFS        STRUCTURE_ULR.grp_mots_clefs%TYPE
)
IS
-- ------------
-- DECLARATIONS
-- ------------
cpt INTEGER;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

/*SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
WHERE ll_structure=ins_ll_structure;
IF (cpt>0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_STRUCTURE_ULR : Libelle existant');
END IF;*/

IF (ins_c_structure_pere IS NOT NULL) THEN
    SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
    WHERE c_structure=ins_c_structure_pere;
    IF (cpt=0) THEN
       RAISE_APPLICATION_ERROR(-20001,'INS_STRUCTURE_ULR : C_structure_pere inconnu');
    END IF;
END IF;


--SELECT MAX(TO_NUMBER(c_structure))+1 INTO cstructure FROM STRUCTURE_ULR;--+1

SELECT SEQ_STRUCTURE_ULR_SYNC.NEXTVAL INTO cstructure FROM dual;

--SELECT MAX(pers_id)+1 INTO persid FROM v_personne;--+1

SELECT SEQ_PERSONNE.NEXTVAL INTO persid FROM dual;

INSERT INTO STRUCTURE_ULR VALUES (
  cstructure,
  persid,
  INS_LL_STRUCTURE,
  INS_LC_STRUCTURE,
  INS_C_TYPE_STRUCTURE,
  INS_C_STRUCTURE_PERE,
  INS_C_TYPE_ETABLISSEMEN,
  INS_C_ACADEMIE,
  INS_C_STATUT_JURIDIQUE,
  INS_C_RNE,
  INS_SIRET,
  INS_SIREN,
  INS_C_NAF,
  INS_C_TYPE_DECISION_STR,
  INS_REF_EXT_ETAB,
  INS_REF_EXT_COMP,
  INS_REF_EXT_CR,
  INS_REF_DECISION,
  INS_DATE_DECISION,
  INS_DATE_OUVERTURE,
  INS_DATE_FERMETURE,
  INS_STR_ORIGINE,
  INS_STR_PHOTO,
  INS_STR_ACTIVITE,
  INS_GRP_OWNER,
  INS_GRP_RESPONSABLE,
  INS_GRP_FORME_JURIDIQUE,
  INS_GRP_CAPITAL,
  INS_GRP_CA,
  INS_GRP_EFFECTIFS,
  INS_GRP_CENTRE_DECISION,
  INS_GRP_APE_CODE,
  INS_GRP_APE_CODE_BIS,
  INS_GRP_APE_CODE_COMP,
  INS_GRP_ACCES,
  INS_GRP_ALIAS,
  INS_GRP_RESPONSABILITE,
  INS_GRP_TRADEMARQUE,
  INS_GRP_WEBMESTRE,
  INS_GRP_FONCTION1,
  INS_GRP_FONCTION2,
  INS_ORG_ORDRE,
  INS_GRP_MOTS_CLEFS,
  SYSDATE,
  SYSDATE,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  'N',
  'N',
  'O',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,   -- tva_intracom
  NULL,   -- str_accueil
  NULL,   -- str_recherche
  NULL,   -- num_rafp
  INS_LC_STRUCTURE,     -- str_affichage
  NULL,   -- gencod
  NULL,   -- pers_id_creation
  NULL,   -- pers_id_modification
  NULL,   -- STR_STATUT
  NULL,   -- ROM_ID
  NULL,   -- STR_DESCRIPTION
  null    -- TEM_SIRET_PROVISOIRE
);

END;
/
-----------------------------V20140106.151222__DDL_VERIF_CODE_INSEE.sql-----------------------------
--------------------------------------------------------
--  Fichier créé - mardi-novembre-26-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function VERIF_CODE_INSEE
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "GARNUCHE"."VERIF_CODE_INSEE" 
(
  indnoinsee    grhum.individu_ulr.ind_no_insee%TYPE,
  indcleinsee   grhum.individu_ulr.ind_cle_insee%TYPE,
  prisecptinsee grhum.individu_ulr.prise_cpt_insee%TYPE,
  adrcivilite   grhum.individu_ulr.c_civilite%TYPE,
  datenais      grhum.individu_ulr.d_naissance%TYPE,
  dptnais       grhum.individu_ulr.c_dept_naissance%TYPE,
  paysnais      grhum.individu_ulr.c_pays_naissance%TYPE
)
RETURN INTEGER
IS
  -- declaration des variables
  v_cpays       grhum.pays.c_pays%TYPE;
  v_noinsee     grhum.individu_ulr.ind_no_insee%TYPE;
  v_clecalculee INTEGER;
  i             INTEGER;
  v_civilite    CHAR(1);
  v_cleinsee    CHAR(2);
  v_anneeinsee  CHAR(2);
  v_anneenais   CHAR(2);
  v_moisinsee   CHAR(2);
  v_moisnais    CHAR(2);
  v_deptinsee   CHAR(3);
  v_deptnais    CHAR(3);
  erreur01 EXCEPTION; erreur02 EXCEPTION; erreur03 EXCEPTION; 
  erreur04 EXCEPTION; erreur05 EXCEPTION; erreur06 EXCEPTION;
  erreur07 EXCEPTION; erreur08 EXCEPTION; erreur09 EXCEPTION;
  erreur10 EXCEPTION; erreur11 EXCEPTION; erreur12 EXCEPTION;
  erreur13 EXCEPTION; erreur14 EXCEPTION; erreur15 EXCEPTION;
  erreur16 EXCEPTION; erreur17 EXCEPTION; erreur18 EXCEPTION;
  erreur19 EXCEPTION; erreur20 EXCEPTION; erreur21 EXCEPTION;
  erreur22 EXCEPTION; erreur23 EXCEPTION; erreur24 EXCEPTION;
  erreur25 EXCEPTION;
  -- Variables pour la recherche d'un traitement spécifique
  v_uneChaine   CLOB;
  v_laChaine    VARCHAR2(2000);
  v_paramobjet  VARCHAR2(2000);
  v_typeobjet   VARCHAR2(50);
  v_nomobjet    VARCHAR2(50);
  v_intRetour   INTEGER;
  v_position    INTEGER;
BEGIN
  -- ##################################################################################
  -- Recherche d'un traitement spécifique à prendre en compte                       ###
  -- ##################################################################################
  v_uneChaine := histotox.F_Recup_Objet_Histotox('FUNCTION','GARNUCHE','Verif_Code_Insee');

  IF (grhum.F_Verif_Nullite(v_uneChaine) = 0)
  THEN v_position := 1;
	   grhum.recuperation_donnee(v_uneChaine,1,1,'$',v_typeobjet,v_position);
	   grhum.recuperation_donnee(v_uneChaine,(v_position+1),v_position,'$',v_nomobjet,v_position);
	   grhum.recuperation_donnee(v_uneChaine,(v_position+1),v_position,'$',v_paramobjet,v_position);

	   v_typeobjet  := RTRIM(v_typeobjet);
	   v_nomobjet   := RTRIM(v_nomobjet);
	   v_paramobjet := RTRIM(v_paramobjet);
	   v_laChaine   := 'SELECT '||v_nomobjet||'('||v_paramobjet||') FROM DUAL';

	   EXECUTE IMMEDIATE v_laChaine INTO v_intRetour USING IN indnoinsee,indcleinsee,prisecptinsee,adrcivilite,datenais,dptnais;

	   RETURN v_intRetour;
  END IF;

  -- ##################################################################################
  -- Traitement générique                                                           ###
  -- ##################################################################################
  
  -- Initialisations...
  v_noinsee  := REPLACE(UPPER(indnoinsee),' ');
  v_cleinsee := LPAD(REPLACE(UPPER(TO_CHAR(indcleinsee,'99')),' '),2,'0');
  
  -- Vérification de la VALIDITE
  -- ***************************
  IF ((grhum.F_Verif_Nullite(v_noinsee) = 1) AND (grhum.F_Verif_Nullite(v_cleinsee) = 0))
  THEN RAISE erreur01;
  END IF;
  
  IF ((grhum.F_Verif_Nullite(v_noinsee) = 0) AND (grhum.F_Verif_Nullite(v_cleinsee) = 1))
  THEN RAISE erreur02;
  END IF;
  
  IF (LENGTH(v_noinsee) <> 13)
  THEN RAISE erreur03;
  END IF;
  
  IF (LENGTH(v_cleinsee) <> 2)
  THEN RAISE erreur04;
  END IF;
  
  v_clecalculee := Recuperer_Cle_Insee(v_noinsee);
  
  IF (v_clecalculee > 99)
  THEN RAISE erreur05;
  END IF;
  
  IF (v_clecalculee <> TO_NUMBER(v_cleinsee))
  THEN RAISE erreur06;
  END IF;
  
  -- Vérification de la COHERENCE
  -- ****************************
  -- CODE Provisoire...
  IF (prisecptinsee = 'P')
  THEN RETURN (0);
  END IF;
  
  -- CIVILITE...
  v_civilite := SUBSTR(v_noinsee,1,1);

  IF ((v_civilite <> '1') AND (v_civilite <> '2')) /* On ne traite pas les codes provisoires 7,8 ...*/
  THEN RETURN(0);
  END IF;

  IF ((adrcivilite = 'M.') AND (v_civilite <> '1'))
  THEN RAISE erreur07;
  END IF;

  IF (((adrcivilite = 'MME') OR (adrcivilite = 'MLLE')) AND (v_civilite <> '2'))
  THEN RAISE erreur08;
  END IF;

  -- DATE NAISSANCE...
  v_anneeinsee := SUBSTR(v_noinsee,2,2);
  v_anneenais  := SUBSTR(TO_CHAR(datenais,'dd/mm/yy'),7,2);

  IF (v_anneeinsee <> v_anneenais)
  THEN RAISE erreur09;
  END IF;

  v_moisinsee := SUBSTR(v_noinsee,4,2);
  v_moisnais  := SUBSTR(TO_CHAR(datenais,'DD/MM/YY'),4,2);

  IF ((v_moisinsee <> '99') AND (v_moisinsee <> v_moisnais))
  THEN RAISE erreur10;
  END IF;

  -- DEPARTEMENT...
  v_cpays     := grhum.F_Recup_Pvalue_String_For_User('GRHUM','GRHUM','param_key = ''GRHUM_C_PAYS_DEFAUT''');
  v_deptinsee := SUBSTR(v_noinsee,6,2);
  
  IF (v_deptinsee IN ('96','99'))
  THEN -- Etranger ou ancien protectorat
       IF (grhum.F_Verif_Nullite(paysnais) = 1)
       THEN RAISE erreur11;
       
       END IF;
       IF (paysnais = v_cpays)
       THEN RAISE erreur12;
       END IF;
       
       v_deptnais := SUBSTR(v_noinsee,8,3);
       
       FOR i IN 1..LENGTH(v_deptnais) 
       LOOP
         IF (grhum.Chiffre(SUBSTR(v_deptnais,i,1)) = 0) 
         THEN RAISE erreur13;                             
         END IF;
       END LOOP;

       IF (TO_NUMBER(v_deptnais) = 0)
       THEN RAISE erreur14;
       END IF;
       
       IF ((v_deptinsee = '96') AND (TO_NUMBER(TO_CHAR(datenais,'YYYY')) > 1967))
       THEN RAISE erreur15;
       END IF;
  ELSE v_deptnais := v_deptinsee;
  		/*
       IF (v_deptinsee IN ('97','98'))
       THEN -- DOM TOM
            v_deptinsee := SUBSTR(v_noinsee,6,3);
       
            IF (grhum.F_Verif_Nullite(dptnais) = 0)
            THEN v_deptnais := dptnais;
            END IF;
       ELSE -- Normal
            IF (grhum.F_Verif_Nullite(dptnais) = 0)
            THEN v_deptnais := SUBSTR(dptnais,2,2);
            END IF;
       END IF;
       */
       IF (v_deptinsee IN ('97','98'))
       THEN -- DOM TOM
            v_deptinsee := SUBSTR(v_noinsee,6,2);
       END IF;     
       IF (grhum.F_Verif_Nullite(dptnais) = 0)
       		THEN v_deptnais := SUBSTR(dptnais,2,2);
       END IF;
       
       IF (v_deptnais <> v_deptinsee)
       THEN IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) <= 1962) AND (v_deptinsee IN ('91','92','93','94','95')))
            THEN -- Nés sous protectorat
                 IF (grhum.F_Verif_Nullite(dptnais) = 0)
                 THEN RAISE erreur16;
                 END IF;
            ELSE IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) <= 1968) AND (v_deptinsee IN ('75','78')))
                 THEN -- Nés en Ile de France avant 1968
                      -- Département 75 peut avoir un département de naissance égal à 75, 92, 93 ou 94
                      IF ((v_deptinsee = '75') AND (v_deptnais NOT IN ('75','92','93','94')))
                      THEN RAISE erreur17;
                      END IF;
                      
                      -- Département 78 peut avoir un département de naissance égal à 91, 92, 93, 94 ou 95
                      IF ((v_deptinsee = '78') AND (v_deptnais NOT IN ('91','92','93','94','95')))
                      THEN RAISE erreur18;
                      END IF;
                 ELSE IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) < 1976) AND (v_deptinsee = '20'))
                      THEN IF (v_deptnais NOT IN ('2A','2B'))
                           THEN RAISE erreur19;
                           END IF;
                      ELSE RAISE erreur20;
                      END IF;
                 END IF;
            END IF;
       END IF;
       
       IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) <= 1962) AND (v_deptinsee IN ('91','92','93','94','95')))
       THEN -- Vérification avec le pays de naissance
            IF (v_deptinsee IN ('91','92','93','94'))
            THEN -- Algérie
                 IF ((grhum.F_Verif_Nullite(paysnais) = 1) OR (paysnais NOT IN ('352','999')))
                 THEN RAISE erreur21;
                 END IF;
            ELSE IF (v_deptinsee = '95')
                 THEN -- Maroc
                      IF ((grhum.F_Verif_Nullite(paysnais) = 1) OR (paysnais NOT IN ('350','999')))
                      THEN RAISE erreur22;
                      END IF;
                 END IF;
            END IF;
       ELSE IF (paysnais <> v_cpays)
            THEN RAISE erreur23;
            END IF;
       END IF;
       
       IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) < 1976) AND (v_deptinsee IN ('2A','2B')))
       THEN RAISE erreur24;
       END IF;
       
       IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) >= 1976) AND (v_deptinsee = '20'))
       THEN RAISE erreur25;
       END IF;
  END IF;

  RETURN(0);

  -- ERREURS...
  EXCEPTION
  WHEN erreur01 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE est obligatoire si vous renseignez la CLE');
  WHEN erreur02 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE est obligatoire si vous renseignez le CODE.');
  WHEN erreur03 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE doit comporter treize chiffres.');
  WHEN erreur04 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE doit comporter 2 chiffres.');
  WHEN erreur05 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE doit etre inferieure a 100.');
  WHEN erreur06 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE a une valeur erronnee.');
  WHEN erreur07 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour un homme, le CODE commence par 1.');
  WHEN erreur08 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour une femme, le CODE commence par 2.');
  WHEN erreur09 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : L''annee de naissance du CODE ne correspond pas a l''annee de naissance.');
  WHEN erreur10 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le mois de naissance du CODE ne correspond pas au mois de naissance');
  WHEN erreur11 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour une personne nee a l''etranger, vous devez fournir le pays de naissance.');
  WHEN erreur12 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE indique que la personne n''est pas nee en France.');
  WHEN erreur13 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le code pays du CODE pour les personnes nees a l''etranger ne doit comporter que des chiffres.');
  WHEN erreur14 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le code pays du CODE pour les personnes nees a l''etranger doit etre compris entre 000 et 999.');
  WHEN erreur15 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : A partir de 1967, Le code departement du CODE doit etre 99.');
  WHEN erreur16 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pas de departement pour les personnes nees en Algerie ou sous un protectorat.');
  WHEN erreur17 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Une personne nee avant 1968 avec 75 comme departement du CODE, ne peut avoir comme departement de naissance que : 75, 92, 93 ou 94.');
  WHEN erreur18 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Une personne nee avant 1968 avec 78 comme departement du CODE, ne peut avoir comme departement de naissance que : 91, 92, 93, 94 ou 95.');
  WHEN erreur19 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le departement de naissance selectionne ne correspond pas a la Corse.');
  WHEN erreur20 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le departement du CODE ne correspond pas au departement de naissance.');
  WHEN erreur21 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le pays de naissance de cette personne est l''Algerie.');
  WHEN erreur22 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le pays de naissance de cette personne est le Maroc.');
  WHEN erreur23 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE indique que la personne est nee en France.');
  WHEN erreur24 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour un corse ne avant 1976, le code departement du CODE ne peut etre 2A ou 2B.');
  WHEN erreur25 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour un corse ne apres 1976, le code departement du CODE doit etre 2A ou 2B.');
END;

/


---------------------------V20140106.151258__DDL_NETTOYAGE_PROCEDURES.sql---------------------------
declare
	cpt INTEGER;
begin


	GRHUM.DROP_OBJECT('GRHUM', 'INS_AFFECTATION', 'PROCEDURE');
	GRHUM.DROP_OBJECT('GRHUM', 'SYNC_NABUCO_CLIENT', 'PROCEDURE');

	SELECT count(*) INTO cpt
    				FROM GRHUM.OCCUPATION;
  	IF cpt <> 0 THEN
    	GRHUM.DROP_OBJECT('GRHUM', 'OCCUPATION', 'SYNONYM');
  	END IF;

	GRHUM.DROP_OBJECT('GRHUM', 'MI_TPS_THERAP', 'TABLE');

end;
/

ALTER INDEX GRHUM.UK_RNE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.UK_STR_PERS_ID REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_STR_GRP_ALIAS REBUILD;


