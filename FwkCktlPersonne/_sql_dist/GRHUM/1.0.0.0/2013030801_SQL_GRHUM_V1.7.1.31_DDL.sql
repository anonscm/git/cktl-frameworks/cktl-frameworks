--
-- Patch DDL de GRHUM du 08/03/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8
--
-- Groupes dynamique

SET DEFINE OFF;

--
--
-- Fichier : 1/4
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 08/03/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente (pour mise en prod uniquement, décommenter)
--declare
--cpt integer;
--begin
--	SELECT COUNT(*) INTO cpt FROM GRHUM.DB_VERSION WHERE DBV_LIBELLE = '1.7.1.30';
--    if cpt = 0 then
--        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
--    end if;
--       
--	SELECT COUNT(*) INTO cpt from GRHUM.DB_VERSION WHERE DBV_LIBELLE = '1.7.1.31';
--    if cpt > 0 then
--        raise_application_error(-20000,'Le patch 1.7.1.31 a deja ete passe !');
--    end if;
--end;
--/

--
-- Début du script
--

DROP TABLE GRHUM.REGLE cascade constraints;
DROP TABLE GRHUM.REGLE_KEY cascade constraints;
DROP TABLE GRHUM.REGLE_NODE cascade constraints;
DROP TABLE GRHUM.REGLE_OPERATEUR cascade constraints;
DROP TABLE GRHUM.GRP_DYNA_PERSONNE cascade constraints;
DROP TABLE GRHUM.GROUPE_DYNAMIQUE cascade constraints;
DROP SEQUENCE GRHUM.REGLE_SEQ ;
DROP SEQUENCE GRHUM.GROUPE_DYNAMIQUE_SEQ;


WHENEVER SQLERROR EXIT SQL.SQLCODE;


CREATE TABLE GRHUM.REGLE_NODE
(
    RN_ID                   number(2,0)     not null,
    RN_NODE                 varchar2(10)    not null,
    RN_DESCRIPTION          varchar2(200)   not null
);
CREATE UNIQUE INDEX GRHUM.PK_REGLE_NODE ON GRHUM.REGLE_NODE (RN_ID);
ALTER TABLE GRHUM.REGLE_NODE ADD (CONSTRAINT PK_REGLE_NODE  PRIMARY KEY  (RN_ID) USING INDEX);
ALTER TABLE GRHUM.REGLE_NODE ADD CONSTRAINT UNQ_RN_RN_NODE UNIQUE (RN_NODE);

COMMENT ON TABLE GRHUM.REGLE_NODE IS 'Noeuds possibles pour l''arborescence des règles (pour générer des clés composées)';
COMMENT ON COLUMN GRHUM.REGLE_NODE.RN_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.REGLE_NODE.RN_NODE IS 'Noeud (SIMPLE, AND, NOT, OR, ...)';
---

CREATE TABLE GRHUM.REGLE_KEY
(
    RK_ID                   number(2,0)     not null,
    RK_STR_ID               varchar2(20)    not null,
    RK_LC                   varchar2(50)    not null,
    RK_DESCRIPTION          varchar2(200)   null
);
CREATE UNIQUE INDEX GRHUM.PK_REGLE_KEY ON GRHUM.REGLE_KEY (RK_ID);
ALTER TABLE GRHUM.REGLE_KEY ADD (CONSTRAINT PK_REGLE_KEY  PRIMARY KEY  (RK_ID) USING INDEX);
ALTER TABLE GRHUM.REGLE_KEY ADD CONSTRAINT UNQ_RK_RK_STR_ID UNIQUE (RK_STR_ID);

COMMENT ON TABLE GRHUM.REGLE_KEY IS 'Clés possibles pour une règle (personne, utilisateur, établissement de l''utilisateur, etc.) - Partie gauche de la règle';
COMMENT ON COLUMN GRHUM.REGLE_KEY.RK_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.REGLE_KEY.RK_STR_ID IS 'Identifiant signifiant de la clé. Doit être unique';
COMMENT ON COLUMN GRHUM.REGLE_KEY.RK_LC IS 'Libellé de la clé possible pour une règle (personne, utilisateur, établissement de l''utilisateur, etc.) - Partie gauche de la règle';
COMMENT ON COLUMN GRHUM.REGLE_KEY.RK_DESCRIPTION IS 'Description de la clé';
---
CREATE TABLE GRHUM.REGLE_OPERATEUR
(
    RO_ID                   number(12,0)        not null,
    RO_STR_ID               varchar2(20)        not null,
    RO_LC                   varchar2(50)        not null,
    RO_DESCRIPTION          varchar2(500)       null,
    RO_VALEURS_POSSIBLES        varchar2(4000)      null
);
CREATE UNIQUE INDEX GRHUM.PK_REGLE_OPERATEUR ON GRHUM.REGLE_OPERATEUR (RO_ID);
ALTER TABLE GRHUM.REGLE_OPERATEUR ADD (CONSTRAINT PK_REGLE_OPERATEUR  PRIMARY KEY  (RO_ID) USING INDEX);
ALTER TABLE GRHUM.REGLE_OPERATEUR ADD CONSTRAINT UNQ_RO_RO_LC UNIQUE (RO_LC);
ALTER TABLE GRHUM.REGLE_OPERATEUR ADD CONSTRAINT UNQ_RO_STR_ID UNIQUE (RO_STR_ID);

COMMENT ON TABLE  GRHUM.REGLE_OPERATEUR IS 'Operateurs possibles pour une règle (égal, membre de, nom commence par, ...)';
COMMENT ON COLUMN GRHUM.REGLE_OPERATEUR.RO_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.REGLE_OPERATEUR.RO_ID IS 'Identifiant signifiant de l''opérateur';
COMMENT ON COLUMN GRHUM.REGLE_OPERATEUR.RO_LC IS 'Libellé de l''opérateur (a comme role, membre de, ...)';
COMMENT ON COLUMN GRHUM.REGLE_OPERATEUR.RO_DESCRIPTION IS 'Description de l''opérateur';
COMMENT ON COLUMN GRHUM.REGLE_OPERATEUR.RO_VALEURS_POSSIBLES IS 'Valeurs possibles à appliquer à la partie droite de l''opérateur (sous forme de SELECT). NULL si n/a';
---
CREATE TABLE GRHUM.REGLE
(
    R_ID                    number(22,0)        not null,
    R_PERE_ID               number(22,0)        null,
    RN_ID                   number(2,0)         not null,               
    RK_ID                   number(2,0)         null,           
    RO_ID                   number(12,0)        null,   
    DATE_CREATION           date                not null,
    DATE_MODIFICATION       date                not null,
    PERS_ID_CREATION        number(22,0)        not null,
    PERS_ID_MODIFICATION    number(22,0)        not null,       
    R_VALUE                 varchar2(500)       null,
    R_VALUE_2               varchar2(500)       null
);
CREATE UNIQUE INDEX GRHUM.PK_REGLE ON GRHUM.REGLE (R_ID);
ALTER TABLE GRHUM.REGLE ADD (CONSTRAINT PK_REGLE  PRIMARY KEY  (R_ID) USING INDEX);

ALTER TABLE GRHUM.REGLE ADD (CONSTRAINT FK_R_R_PERE_ID  FOREIGN KEY (R_PERE_ID) REFERENCES GRHUM.REGLE (R_ID)  DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE GRHUM.REGLE ADD (CONSTRAINT FK_R_RN_ID  FOREIGN KEY (RN_ID) REFERENCES GRHUM.REGLE_NODE (RN_ID)  );
ALTER TABLE GRHUM.REGLE ADD (CONSTRAINT FK_R_RK_ID  FOREIGN KEY (RK_ID) REFERENCES GRHUM.REGLE_KEY (RK_ID) );
ALTER TABLE GRHUM.REGLE ADD (CONSTRAINT FK_R_RO_ID  FOREIGN KEY (RO_ID) REFERENCES GRHUM.REGLE_OPERATEUR (RO_ID) );

COMMENT ON TABLE GRHUM.REGLE IS 'Arborescence des règles qui détermine l''éligibilité à un profil';
COMMENT ON COLUMN GRHUM.REGLE.R_ID IS 'Identifiant';
COMMENT ON COLUMN GRHUM.REGLE.R_PERE_ID IS 'Référence à l''enregistrement père';
COMMENT ON COLUMN GRHUM.REGLE.RN_ID IS 'Référence au noeud de la règle';
COMMENT ON COLUMN GRHUM.REGLE.RK_ID IS 'Référence à la clé (partie gauche) de la règle dans le cas d''un noeud simple';
COMMENT ON COLUMN GRHUM.REGLE.RO_ID IS 'Référence à l''opérateur de la règle dans le cas d''un noeud simple';
COMMENT ON COLUMN GRHUM.REGLE.R_VALUE IS 'Valeur (partie droite) de la règle dans le cas d''un noeud simple. En String, à caster suivant les cas';
COMMENT ON COLUMN GRHUM.REGLE.DATE_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRHUM.REGLE.DATE_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRHUM.REGLE.PERS_ID_CREATION IS 'Référence à la personne qui a créé l''enregistrement';
COMMENT ON COLUMN GRHUM.REGLE.PERS_ID_MODIFICATION IS 'Référence à la dernière personne qui a modifié l''enregistrement';
---
CREATE SEQUENCE GRHUM.REGLE_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

---
CREATE TABLE GRHUM.GROUPE_DYNAMIQUE (
  R_ID                 number(22) NOT NULL, 
  GRPD_ID              number(22) NOT NULL,
  GRPD_LC              varchar2(255) NOT NULL,
  GRPD_DESCRIPTION     varchar2(500) NOT NULL,
  PRIMARY KEY (GRPD_ID));
ALTER TABLE GRHUM.GROUPE_DYNAMIQUE ADD CONSTRAINT FK_GD_R_ID FOREIGN KEY (R_ID) REFERENCES GRHUM.REGLE (R_ID);

COMMENT ON COLUMN GRHUM.GROUPE_DYNAMIQUE.R_ID IS 'Référence à la règle';
COMMENT ON COLUMN GRHUM.GROUPE_DYNAMIQUE.GRPD_ID IS 'Identifiant du groupe dynamique';

CREATE SEQUENCE GRHUM.GROUPE_DYNAMIQUE_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

---
CREATE TABLE GRHUM.GRP_DYNA_PERSONNE (
  PERS_ID     number(22) NOT NULL, 
  GRPD_ID     number(22) NOT NULL, 
  PRIMARY KEY (PERS_ID,
  GRPD_ID));
ALTER TABLE GRHUM.GRP_DYNA_PERSONNE ADD CONSTRAINT FK_GDI_PERS_ID  FOREIGN KEY (PERS_ID) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRHUM.GRP_DYNA_PERSONNE ADD CONSTRAINT FK_GDI_GRPD_ID FOREIGN KEY (GRPD_ID) REFERENCES GRHUM.GROUPE_DYNAMIQUE (GRPD_ID);

COMMENT ON COLUMN GRHUM.GRP_DYNA_PERSONNE.PERS_ID IS 'Référence à la personne faisant partie du groupe dynamique';
COMMENT ON COLUMN GRHUM.GRP_DYNA_PERSONNE.GRPD_ID IS 'Référence au groupe dynamique';

--
-- DB_VERSION (pour mise en prod uniquement, décommenter)
--
--INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.31', TO_DATE('08/03/2013', 'DD/MM/YYYY'), NULL ,'Ajouts des tables et des données minimums pour la nouvelles gestion des droits.');
--
--COMMIT;
