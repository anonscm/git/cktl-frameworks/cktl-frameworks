--
-- Patch DDL de GRHUM du 23/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.24
-- Date de publication : 23/01/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.23';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.24';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.20 a deja ete passe !');
    end if;

end;
/


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.24', TO_DATE('23/01/2013', 'DD/MM/YYYY'),NULL,'Mise à jour de la table Profession, Suppression de vieilles tables inutilisées et création si besoin de la table Axes_Stratégiques ');
-- commit;

--
-- Actualisation de GRHUM.PROFESSION
--

ALTER TABLE GRHUM.PROFESSION
ADD (
	D_OUVERTURE 	DATE,
 	D_FERMETURE  	DATE
    );

--
-- CREATION de GRHUM.AXES_STRATEGIQUES
--
declare

   c int;

begin

   select count(*) into c from user_tables where table_name = upper('AXES_STRATEGIQUES');

   if c = 0 then

      execute immediate '

        CREATE TABLE GRHUM.AXES_STRATEGIQUES (

          ID NUMBER NOT NULL,

          LIBELLE VARCHAR(100) NOT NULL,

          PERS_ID_CREATION NUMBER NOT NULL, 

          PERS_ID_MODIFICATION NUMBER,

          D_CREATION DATE NOT NULL, 

          D_MODIFICATION DATE,

          CONSTRAINT PK_AXE_STRATEGIQUE PRIMARY KEY (ID) USING INDEX TABLESPACE INDX_GRHUM

        )

      ';

      execute immediate '

        CREATE SEQUENCE GRHUM.AXES_STRATEGIQUES_SEQ INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE

      ';

   end if;

end;

/



COMMIT;
