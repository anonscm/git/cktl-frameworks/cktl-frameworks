--
-- Patch DML de GRHUM du 23/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 23/01/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;




--
-- Ajout de commentaires sur GRHUM.PROFESSION
--


COMMENT ON COLUMN GRHUM.PROFESSION.D_OUVERTURE IS 'Date du début de validité du code de profession';
COMMENT ON COLUMN GRHUM.PROFESSION.D_FERMETURE IS 'Date de fin de validité du code de profession';


--
-- Ajout de commentaires sur GRHUM.AXES_STRATEGIQUES
--
COMMENT ON TABLE GRHUM.AXES_STRATEGIQUES IS 'Table des axes strategiques de l''etablissement';

COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.ID IS 'Identifiant de l''axe';

COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.LIBELLE IS 'Libelle de l''axe';

COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.PERS_ID_CREATION IS 'persid du createur';

COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.PERS_ID_MODIFICATION IS 'persid du modificateur.';

COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.D_CREATION IS 'date de creation';

COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.D_MODIFICATION IS 'date de modification';

--
-- Suppression des tables en doublon dans GRHUM alors qu elles sont dans MANGUE
--
-- Mise à jour d'une duree dans PASSAGE_ECHELON

UPDATE grhum.passage_echelon SET duree_passage_annees=3, d_modification=sysdate WHERE c_grade='3002' AND c_echelon='02' AND d_fermeture is null;

-- Suppression de plusieurs tables de GRHUM

-- MANGUE - CONTRATS  / AFFECTATIONS / CARRIERES
-- DROP TABLE GRHUM.AFFECTATION_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.OCCUPATION_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONTRAT_VACATAIRES CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONTRAT_LBUDS CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONSERVATION_ANCIENNETE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CESS_PROG_ACTIVITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONTRAT_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.NBI CASCADE CONSTRAINTS;

-- MANGUE / CONGES
-- DROP TABLE GRHUM.ABSENCES_OLD CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CGNT_ACCIDENT_TRAV CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CG_FIN_ACTIVITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_ACCIDENT_SERV CASCADE CONSTRAINT;
-- DROP TABLE GRHUM.CONGE_ADOPTION CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_FORMATION CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_GARDE_ENFANT CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.DETAIL_CG_MALADIE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_MALADIE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.DECLARATION_MATERNITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_MATERNITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CONGE_PATERNITE CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CLM CASCADE CONSTRAINTS;
-- DROP TABLE GRHUM.CLD CASCADE CONSTRAINTS;

declare

   c int;

begin

-- MANGUE - CONTRATS  / AFFECTATIONS / CARRIERES
   select count(*) into c from user_tables where table_name = upper('AFFECTATION_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.AFFECTATION_OLD CASCADE CONSTRAINTS
      ';
   end if;



   select count(*) into c from user_tables where table_name = upper('OCCUPATION_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.OCCUPATION_OLD CASCADE CONSTRAINTS 
      ';
   end if;



   select count(*) into c from user_tables where table_name = upper('CONTRAT_VACATAIRES');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONTRAT_VACATAIRES CASCADE CONSTRAINTS    
      ';
   end if;



   select count(*) into c from user_tables where table_name = upper('CONTRAT_LBUDS');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONTRAT_LBUDS CASCADE CONSTRAINTS    
      ';
   end if;
   
  select count(*) into c from user_tables where table_name = upper('CONSERVATION_ANCIENNETE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONSERVATION_ANCIENNETE CASCADE CONSTRAINTS    
      ';     
   end if;
   
   select count(*) into c from user_tables where table_name = upper('CESS_PROG_ACTIVITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CESS_PROG_ACTIVITE CASCADE CONSTRAINTS    
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONTRAT_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONTRAT_OLD CASCADE CONSTRAINTS   
      ';
   end if;
   
   select count(*) into c from user_tables where table_name = upper('NBI');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.NBI CASCADE CONSTRAINTS    
      ';
   end if;

-- MANGUE / CONGES   
   select count(*) into c from user_tables where table_name = upper('ABSENCES_OLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.ABSENCES_OLD CASCADE CONSTRAINTS    
      ';     
   end if;

   select count(*) into c from user_tables where table_name = upper('CGNT_ACCIDENT_TRAV');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CGNT_ACCIDENT_TRAV CASCADE CONSTRAINTS   
      ';  
   end if;

   select count(*) into c from user_tables where table_name = upper('CG_FIN_ACTIVITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CG_FIN_ACTIVITE CASCADE CONSTRAINTS   
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_ACCIDENT_SERV');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_ACCIDENT_SERV CASCADE CONSTRAINT  
      ';   
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_ADOPTION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_ADOPTION CASCADE CONSTRAINTS  
      '; 
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_FORMATION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_FORMATION CASCADE CONSTRAINTS 
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_GARDE_ENFANT');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_GARDE_ENFANT CASCADE CONSTRAINTS
      ';  
   end if;

   select count(*) into c from user_tables where table_name = upper('DETAIL_CG_MALADIE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.DETAIL_CG_MALADIE CASCADE CONSTRAINTS
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_MALADIE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_MALADIE CASCADE CONSTRAINTS
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('DECLARATION_MATERNITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.DECLARATION_MATERNITE CASCADE CONSTRAINTS
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_MATERNITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_MATERNITE CASCADE CONSTRAINTS
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CONGE_PATERNITE');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CONGE_PATERNITE CASCADE CONSTRAINTS
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CLM');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CLM CASCADE CONSTRAINTS
      ';
   end if;

   select count(*) into c from user_tables where table_name = upper('CLD');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.CLD CASCADE CONSTRAINTS
      ';
   end if;

end;
/

--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.24';


COMMIT;