--
-- Patch DML de GRHUM du 15/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 15/01/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- PERSONNE_TELEPHONE
--
-- Rem : harmonisation des numéros de téléphones (enregistrés sans caractères non numérique)
-- no_telephone is not null
update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone=' '; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='.'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='/'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='0'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='00'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='0000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='000000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='0000000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='00000000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='000000000'; 

update grhum.personne_telephone set no_telephone=replace(no_telephone,'/',''),d_modification=sysdate where no_telephone like '%/%';

update grhum.personne_telephone set no_telephone=replace(no_telephone,'-',''),d_modification=sysdate where no_telephone like '%-%';

update grhum.personne_telephone set no_telephone=replace(no_telephone,'O','0'),d_modification=sysdate where no_telephone like '%O%';

update grhum.personne_telephone set no_telephone=replace(no_telephone,'x','0'),d_modification=sysdate where no_telephone like '%x%';

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where ltrim(rtrim(no_telephone)) is null; 

-- suppression des numeros en double (pk_telephone)
delete from personne_telephone
where (pers_id,no_telephone,type_no,type_tel)
in
(
select pers_id,no_telephone,type_no,type_tel from
(
select pers_id,ltrim(rtrim(no_telephone)) as no_telephone,type_no,type_tel,count(*)
from personne_telephone
group by pers_id,ltrim(rtrim(no_telephone)),type_no,type_tel having count(*)>1
)
);

update grhum.personne_telephone set no_telephone=ltrim(rtrim(no_telephone)),d_modification=sysdate;

-- suppression des numeros en double (pk_telephone)
delete from personne_telephone
where (pers_id,no_telephone,type_no,type_tel)
in
(
select pers_id,no_telephone,type_no,type_tel from
(
select pers_id,replace(no_telephone,'.','') as no_telephone,type_no,type_tel,count(*)
from personne_telephone
group by pers_id,replace(no_telephone,'.',''),type_no,type_tel having count(*)>1
)
);

update grhum.personne_telephone set no_telephone=replace(no_telephone,'.',''),d_modification=sysdate where no_telephone like '%.%';

-- suppression des numeros en double (pk_telephone)
delete from personne_telephone
where (pers_id,no_telephone,type_no,type_tel)
in
(
select pers_id,no_telephone,type_no,type_tel from
(
select pers_id,replace(no_telephone,' ','') as no_telephone,type_no,type_tel,count(*)
from personne_telephone
group by pers_id,replace(no_telephone,' ',''),type_no,type_tel having count(*)>1
)
);

update grhum.personne_telephone set no_telephone=replace(no_telephone,' ',''),d_modification=sysdate where no_telephone like '% %';






--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.23';


COMMIT;