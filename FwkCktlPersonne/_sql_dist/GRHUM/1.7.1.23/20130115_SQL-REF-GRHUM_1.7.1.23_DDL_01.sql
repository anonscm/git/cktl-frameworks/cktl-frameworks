--
-- Patch DDL de GRHUM du 15/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.23
-- Date de publication : 15/01/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.22';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.23';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.23 a deja ete passe !');
    end if;

end;
/



CREATE OR REPLACE FUNCTION GRHUM.Normaliser_Telephone(indicatif NUMBER,numero VARCHAR2)
RETURN VARCHAR2
IS
-- -------------------------------------------------------------------------------------------
-- Projet : Renvoi un n° de téléphone normalisé selon la Rec E 123 : +33 5 46 45 82 14
-- Auteur : DSI PB
-- Date   : 11/12/2012
-- Modif  : 12/12/2012
-- Pre-requis : le n° de téléphone est enregistré sans caractéres non numériques ( 0546458214 ) 
-- -------------------------------------------------------------------------------------------

    i               INTEGER;
    digCount        INTEGER;
    temp            VARCHAR2(30);
    numeroFormate   VARCHAR2(30);

BEGIN

    temp := null;
    digCount := 0;
    numeroFormate := null;
    IF (numero is null or length(numero) = 0) then return(null); end if;
    -- suppression des espaces en début et fin de numéro
    numeroFormate := ltrim(rtrim(numero));
   
    -- Traitement du numéro
    FOR i IN 1..length(numeroFormate)
    LOOP
        IF ( digCount = 2 and i < length(numeroFormate) ) THEN     
            temp := temp ||'.';
            digCount := 0;
        END IF;
        temp := temp ||  substr(numeroFormate,i,1);
        digCount := digCount + 1;
    END LOOP;
    numeroFormate := temp;

    -- Traitement de l'indicatif
    IF (indicatif IS NOT NULL) THEN
         -- suppression du premier 0 du numéro de tél
         IF ( SUBSTR(numeroFormate,1,1)='0' ) THEN
            numeroFormate := '+'||TO_CHAR(indicatif)||' '||SUBSTR(numeroFormate,2,LENGTH(numeroFormate));
         ELSE
            numeroFormate := '+'||TO_CHAR(indicatif)||' '||numeroFormate;
        END IF;
    END IF;

    RETURN (numeroFormate);

END Normaliser_Telephone;
/


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.23', TO_DATE('15/01/2013', 'DD/MM/YYYY'),NULL,'Harmonisation des téléphones ');
commit;








