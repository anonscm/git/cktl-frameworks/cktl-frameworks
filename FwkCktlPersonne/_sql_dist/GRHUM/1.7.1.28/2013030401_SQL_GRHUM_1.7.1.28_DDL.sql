--
-- Patch DML de GRHUM du 04/03/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 04/03/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.27';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.28';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.28 a deja ete passe !');
    end if;

end;
/


--
-- Création d'un indexe d'accélération sur la table COMPTE
-- COMPTE
--
CREATE INDEX IDX_COMPTE_PERS_ID ON GRHUM.COMPTE(PERS_ID) TABLESPACE INDX_GRHUM;



--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.28', TO_DATE('04/03/2013', 'DD/MM/YYYY'),NULL,'Ajout d''un indexe d''accélération et de RNE');
-- commit;




COMMIT;
