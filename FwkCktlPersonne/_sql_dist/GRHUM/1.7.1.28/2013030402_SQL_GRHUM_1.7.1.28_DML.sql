--
-- Patch DML de GRHUM du 04/03/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 04/03/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- Ajout de valeurs trouvées par les adhérents
--
-- RNE
--

-- un établissement n'a pas le bon nom et la bonne adresse dans la table grhum.rne
-- voici le lien avec les coordonnées
-- http://www.education.gouv.fr/annuaire/35-ille-et-vilaine/saint-gregoire/lycee/lycee-jean-paul-ii.html

update grhum.RNE set ll_rne=upper('Lycee Jean-Paul II'), adresse=upper('2 rue Antoine de St Exupery'),
code_postal='35760', ville=upper('Saint-Gregoire')
where c_rne='0350801F';


-- Trouvés par des utilisatrices
update grhum.RNE set ville='SAINT VENANT' where c_rne ='0622091L';

update grhum.RNE set ville='REIMS' where c_rne ='0511802G';



--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.28';


COMMIT;