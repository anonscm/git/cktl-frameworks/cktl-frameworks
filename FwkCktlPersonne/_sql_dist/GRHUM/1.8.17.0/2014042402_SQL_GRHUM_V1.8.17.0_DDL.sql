--
-- Patch DDL de GRHUM du 24/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.17.0
-- Date de publication : 24/04/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.16.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.17.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.17.0 a deja ete passe !');
    end if;

end;
/






DECLARE
	is_table_exists integer;
BEGIN

select count(*) into is_table_exists from user_tables where table_name = upper('REST_REQUETE');

IF is_table_exists <> 0 THEN
	raise_application_error(-20000,'Les tables pour Muscade existent déjà !');
  
END IF;

select count(*) into is_table_exists from user_tables where table_name = upper('REST_REQUETE_LOCAL');

IF is_table_exists <> 0 THEN
	raise_application_error(-20000,'Les tables pour Muscade existent déjà !');
  
END IF;


END;
/


------------------------------V20140423.085806__DDL_Table_Muscade.sql-------------------------------
CREATE TABLE GRHUM.REST_REQUETE (
   ID_REST_REQUETE NUMBER(38,0) NOT NULL,
   STR_ID VARCHAR2(1000) NOT null,
   LIBELLE VARCHAR2(255) NOT NULL,
   CATEGORIE VARCHAR2(1000) NOT NULL,
   COMMENTAIRE_DEVELOPPEUR VARCHAR2(4000) NULL,
   COMMENTAIRE_UTILISATEUR VARCHAR2(4000) NULL,
   SQL_REQ clob NOT NULL
);


CREATE UNIQUE INDEX GRHUM.PK_REST_REQUETE ON GRHUM.REST_REQUETE(ID_REST_REQUETE) LOGGING TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.REST_REQUETE ADD ( CONSTRAINT PK_REST_REQUETE  PRIMARY KEY (ID_REST_REQUETE) USING INDEX GRHUM.PK_REST_REQUETE);
alter TABLE GRHUM.REST_REQUETE ADD CONSTRAINT UNIQ_REST_REQUETE UNIQUE (STR_ID) USING INDEX TABLESPACE INDX_GRHUM;

COMMENT ON TABLE GRHUM.REST_REQUETE IS 'Les requetes utilisees par les services REST (ces requetes ne doivent pas etre modifiees : utilisez la table GRHUM.REST_REQUETE_LOCAL pour crééer des nouvelles requetes ou surcharger celles-ci)';
COMMENT ON COLUMN GRHUM.REST_REQUETE.ID_REST_REQUETE IS 'Cle';
COMMENT ON COLUMN GRHUM.REST_REQUETE.STR_ID IS 'Identifiant signifiant de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE.LIBELLE IS 'Libellé de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE.CATEGORIE IS 'Categorie de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE.COMMENTAIRE_DEVELOPPEUR IS 'Description de la requete a destination du developpeur du client du service REST';
COMMENT ON COLUMN GRHUM.REST_REQUETE.COMMENTAIRE_UTILISATEUR IS 'Description de la requete a destination de l''utilisateur final de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE.SQL_REQ IS 'Requete sql';


CREATE TABLE GRHUM.REST_REQUETE_LOCAL (
   ID_REST_REQUETE_LOCAL NUMBER(38,0) NOT NULL,
   STR_ID VARCHAR2(1000) NOT null,
   LIBELLE VARCHAR2(255) NOT NULL,
   CATEGORIE VARCHAR2(1000) NOT NULL,
   COMMENTAIRE_DEVELOPPEUR VARCHAR2(4000) NULL,
   COMMENTAIRE_UTILISATEUR VARCHAR2(4000) NULL,
   SQL_REQ clob NOT NULL
);


CREATE UNIQUE INDEX GRHUM.PK_REST_REQUETE_LOCAL ON GRHUM.REST_REQUETE_LOCAL(ID_REST_REQUETE_LOCAL) LOGGING TABLESPACE INDX_GRHUM;
ALTER TABLE GRHUM.REST_REQUETE_LOCAL ADD ( CONSTRAINT PK_REST_REQUETE_LOCAL  PRIMARY KEY (ID_REST_REQUETE_LOCAL) USING INDEX GRHUM.PK_REST_REQUETE_LOCAL);
alter TABLE GRHUM.REST_REQUETE_LOCAL ADD CONSTRAINT UNIQ_REST_REQUETE_LOCAL UNIQUE (STR_ID) USING INDEX TABLESPACE INDX_GRHUM;

COMMENT ON TABLE GRHUM.REST_REQUETE_LOCAL IS 'Les requetes utilisees par les services REST (ces requetes sont propres à votre environnement et seront prioritaires sur les requetes de la table GRHUM.REST_REQUETE)';
COMMENT ON COLUMN GRHUM.REST_REQUETE_LOCAL.ID_REST_REQUETE_LOCAL IS 'Cle';
COMMENT ON COLUMN GRHUM.REST_REQUETE_LOCAL.STR_ID IS 'Identifiant signifiant de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE_LOCAL.LIBELLE IS 'Libellé de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE_LOCAL.CATEGORIE IS 'Categorie de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE_LOCAL.COMMENTAIRE_DEVELOPPEUR IS 'Description de la requete a destination du developpeur du client du service REST';
COMMENT ON COLUMN GRHUM.REST_REQUETE_LOCAL.COMMENTAIRE_UTILISATEUR IS 'Description de la requete a destination de l''utilisateur final de la requete';
COMMENT ON COLUMN GRHUM.REST_REQUETE_LOCAL.SQL_REQ IS 'Requete sql';



-- ALTER INDEX GRHUM.UNIQ_REST_REQUETE REBUILD TABLESPACE INDX_GRHUM;
-- ALTER INDEX GRHUM.UNIQ_REST_REQUETE_LOCAL REBUILD TABLESPACE INDX_GRHUM;
alter table GRHUM.REST_REQUETE move lob (SQL_REQ) store as lobsegment (tablespace INDX_GRHUM);
alter table GRHUM.REST_REQUETE_LOCAL move lob (SQL_REQ) store as lobsegment3 (tablespace INDX_GRHUM);



CREATE SEQUENCE GRHUM.REST_REQUETE_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE GRHUM.REST_REQUETE_LOCAL_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

------------------------------V20140424.170705__DDL_TROUVER_LOGIN.sql-------------------------------
create or replace FUNCTION TROUVER_LOGIN
( prenom CHAR,
  nom CHAR )
RETURN CHAR
IS
  login      COMPTE.cpt_login%TYPE;
  login_base COMPTE.cpt_login%TYPE;
  nbenr      INTEGER;
  nbmax      INTEGER;
  nbmin      INTEGER;
  i          INTEGER := 0;
BEGIN
-- recuperation des valeurs MAX et MIN, sinon par defaut : 8 et 4
  select nvl(max(decode(param_key,'ANNUAIRE_LOGIN_MAX', param_value, null)),8),
         nvl(max(decode(param_key,'ANNUAIRE_LOGIN_MIN', param_value, null)),4)
         into nbmax, nbmin
   from  grhum.GRHUM_PARAMETRES;

-- construction du login
  login_base := SUBSTR(LOWER(prenom),1,1) || SUBSTR(LOWER(nom),1,(nbmax-1));
  
-- si trop court il est complete par les caracteres du prenom
-- et, eventuellement, par un certain nombre des zeros
  if length(login_base) < nbmin
     then
        login_base := rpad(SUBSTR(LOWER(prenom),1,nbmin - length(SUBSTR(LOWER(nom),1,(nbmax-1)))) || SUBSTR(LOWER(nom),1,(nbmax-1)),nbmin,'0');
  end if;

  login := login_base;
    
-- gestion des doublons
  SELECT COUNT(*)
    INTO nbenr
    FROM grhum.COMPTE
   WHERE cpt_login = login;
  
  -- si un doublon est detecte on force deux places
  -- pour ajouter deux chiffres : de 01 au 99
    WHILE nbenr > 0
     LOOP
        i := i + 1;
        IF i < 100 THEN
           login := substr(login_base,1,nbmax-2)||ltrim(rtrim(to_char(i,'09')));
        ELSE
           IF i < 1000 THEN
              login := substr(login_base,1,nbmax-3)||ltrim(rtrim(to_char(i,'999')));
           ELSE
              RAISE_APPLICATION_ERROR(-20001,'Le nombre de logins identiques a atteint le maximum autorisé : 1000');
           END IF;
        END IF;
        SELECT COUNT(*) INTO nbenr
          FROM grhum.COMPTE
         WHERE cpt_login = login;
     END LOOP;
   RETURN login;
END;
/



