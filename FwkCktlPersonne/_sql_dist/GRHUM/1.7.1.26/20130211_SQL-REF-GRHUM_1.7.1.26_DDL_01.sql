--
-- Patch DML de GRHUM du 11/02/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 11/02/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.25';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.26';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.26 a deja ete passe !');
    end if;

end;
/

--
-- V_PERSONNEL_ACTUEL_NON_ENS & V_PERSONNEL_ACTUEL_VAC_ENS
--

UPDATE MANGUE.CONTRAT_VACATAIRES
SET tem_enseignant = 'N'
WHERE tem_enseignant IS NULL;

ALTER TABLE MANGUE.CONTRAT_VACATAIRES MODIFY tem_enseignant NOT NULL;

--V_PERSONNEL_ACTUEL_VAC_ENS :

  CREATE OR REPLACE FORCE VIEW "GRHUM"."V_PERSONNEL_ACTUEL_VAC_ENS" ("NO_DOSSIER_PERS") AS 
  SELECT c.no_dossier_pers
  FROM MANGUE.CONTRAT c,
  MANGUE.CONTRAT_VACATAIRES v,
  GRHUM.TYPE_CONTRAT_TRAVAIL tct
  WHERE (c.d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
  AND (c.d_fin_contrat_trav   IS NULL
  OR c.d_fin_contrat_trav     >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
  AND c.c_type_contrat_trav   IN ('VF','VN')
  AND c.tem_annulation         = 'N' 
  AND c.no_seq_contrat         = v.no_seq_contrat (+)
  AND v.tem_enseignant         = 'O'
  AND c.c_type_contrat_trav    = tct.c_type_contrat_trav
  AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
  AND tct.TEM_TITULAIRE               != 'O';
  


--V_PERSONNEL_ACTUEL_NON_ENS :

  CREATE OR REPLACE FORCE VIEW "GRHUM"."V_PERSONNEL_ACTUEL_VAC_NON_ENS" ("NO_DOSSIER_PERS") AS 
  SELECT c.no_dossier_pers
  FROM MANGUE.CONTRAT c,
  MANGUE.CONTRAT_VACATAIRES v,
  GRHUM.TYPE_CONTRAT_TRAVAIL tct
  WHERE (c.d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
  AND (c.d_fin_contrat_trav   IS NULL
  OR c.d_fin_contrat_trav     >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
  AND c.c_type_contrat_trav   IN ('VF','VN')
  AND c.tem_annulation         = 'N' 
  AND c.no_seq_contrat         = v.no_seq_contrat (+)
  AND v.tem_enseignant  != 'O'
  AND c.c_type_contrat_trav    = tct.c_type_contrat_trav
  AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
  AND tct.TEM_TITULAIRE               != 'O';

------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------- TABLES des Ministères -----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- CREATION DE LA TABLE DES MINISTERES
--
 CREATE TABLE GRHUM.MINISTERES (
	ID_MINISTERE NUMBER(38) NOT NULL,
	LC_MINISTERE VARCHAR2(10), 
	LL_MINISTERE VARCHAR2(100), 

  	PRIMARY KEY (ID_MINISTERE)  
);
	CREATE SEQUENCE GRHUM.MINISTERES_SEQ START WITH 1;
 	COMMENT ON TABLE GRHUM.MINISTERES  IS 'Liste des Ministères';
	COMMENT ON COLUMN GRHUM.MINISTERES.ID_MINISTERE IS 'id du ministère';
	COMMENT ON COLUMN GRHUM.MINISTERES.LC_MINISTERE IS 'Libellé court du ministère';
	COMMENT ON COLUMN GRHUM.MINISTERES.LL_MINISTERE IS 'Libellé long du ministère';


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.26', TO_DATE('11/02/2013', 'DD/MM/YYYY'),NULL,'Ajout d un RNE et Suppression de vieilles tables inutilisées et de séquences; Actualisation de ECHANGER_PERSONNE');
-- commit;




COMMIT;
