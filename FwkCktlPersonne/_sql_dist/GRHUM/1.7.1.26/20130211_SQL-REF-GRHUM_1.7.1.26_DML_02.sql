--
-- Patch DML de GRHUM du 11/02/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 11/02/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- Insertion de données dans la table GRHUM.MINISTERES
--
Insert into GRHUM.MINISTERES (ID_MINISTERE,LC_MINISTERE,LL_MINISTERE)
values (GRHUM.MINISTERES_SEQ.NEXTVAL,'MESR','Ministère de l Enseignement Supérieur et de la Recherche');
Insert into GRHUM.MINISTERES (ID_MINISTERE,LC_MINISTERE,LL_MINISTERE)
values (GRHUM.MINISTERES_SEQ.NEXTVAL,'MEN','Ministère de l Education Nationale');
Insert into GRHUM.MINISTERES (ID_MINISTERE,LC_MINISTERE,LL_MINISTERE)
values (GRHUM.MINISTERES_SEQ.NEXTVAL,'MD','Ministère de la Défense');
Insert into GRHUM.MINISTERES (ID_MINISTERE,LC_MINISTERE,LL_MINISTERE)
values (GRHUM.MINISTERES_SEQ.NEXTVAL,'MT','Ministère des Transports');
Insert into GRHUM.MINISTERES (ID_MINISTERE,LC_MINISTERE,LL_MINISTERE)
values (GRHUM.MINISTERES_SEQ.NEXTVAL,'MAAF','Ministère de l Agriculture, de l Agroalimentaire et de la Forêt');



--
-- INDICE
--
-- rem : Décret n° 2013-33 du 10 janvier 2013
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='189' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('189',254,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='190' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('1900',255,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='191' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('191',256,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='192' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('192',257,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='193' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('193',258,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='194' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('194',259,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='195' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('195',260,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='196' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('196',261,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='197' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('197',262,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='198' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('198',263,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='199' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('199',264,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='200' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('200',265,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='201' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('201',266,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='202' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('202',267,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='203' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('203',268,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='204' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('204',269,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='205' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('205',270,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='206' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('206',271,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='207' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('207',272,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='208' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('208',273,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='209' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('209',274,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='210' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('210',275,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='211' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('211',276,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='212' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('212',277,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='213' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('213',278,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='214' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('214',279,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='215' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('215',280,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='216' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('216',281,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='217' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('217',282,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='218' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('218',283,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='219' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('219',284,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='220' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('220',285,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='221' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('221',286,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='222' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('222',287,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='223' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('223',288,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='224' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('224',289,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='225' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('225',290,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='226' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('226',291,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='227' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('227',292,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='228' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('228',293,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='229' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('229',294,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='230' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('230',295,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='231' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('231',296,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='232' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('232',297,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='233' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('233',298,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='234' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('234',299,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='235' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('235',300,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='236' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('236',301,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='237' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('237',302,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='238' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('238',303,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='239' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('239',304,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='240' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('240',305,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='241' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('241',306,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='242' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('242',307,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='243' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('243',308,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='244' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('244',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='245' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('245',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='246' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('246',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='247' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('247',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='248' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('248',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='250' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('250',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='251' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('251',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='252' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('252',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='253' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('253',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='254' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('254',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='255' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('255',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='256' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('256',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='257' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('257',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='258' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('258',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='259' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('259',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='260' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('260',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='261' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('261',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='262' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('262',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='263' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('263',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='264' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('264',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='265' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('265',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='266' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('266',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='267' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('267',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='268' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('268',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='269' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('269',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='270' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('270',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='271' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('271',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='272' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('272',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='273' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('273',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='274' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('274',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='275' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('275',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='276' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('276',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='277' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('277',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='278' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('278',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='279' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('279',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='280' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('280',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='281' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('281',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='282' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('282',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='283' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('283',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='284' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('284',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='285' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('285',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='286' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('286',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='287' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('287',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='288' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('288',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='289' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('289',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='290' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('290',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='291' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('291',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='292' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('292',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='293' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('293',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='294' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('294',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='295' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('295',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='296' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('296',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='297' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('297',309,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='298' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('298',310,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='299' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('299',311,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='300' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('300',311,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='301' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('301',311,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='302' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('302',312,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='303' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('303',312,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='304' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('304',312,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='305' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('305',312,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='306' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('306',312,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='307' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('307',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='308' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('308',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='309' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('309',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='310' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('310',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='311' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('311',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='312' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('312',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='313' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('313',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='314' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('314',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='315' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('315',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='316' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('316',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='317' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('317',313,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='318' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('318',314,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='319' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('319',314,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='320' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('320',314,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');
update grhum.indice set d_fermeture=to_date('31/12/2012','dd/mm/yyyy'), d_modification=sysdate where c_indice_brut='321' and d_fermeture is null;
insert into grhum.indice(C_INDICE_BRUT, C_INDICE_MAJORE, D_MAJORATION, D_CREATION, D_MODIFICATION, TEM_LOCAL) values('321',314,to_date('01/01/2013','dd/mm/yyyy'),sysdate,sysdate,'N');


--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.26';


COMMIT;