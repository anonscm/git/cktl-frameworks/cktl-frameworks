--
-- Patch DDL de GRHUM du 09/05/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.8
-- Date de publication : 09/05/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- pré-requis
declare
cpt integer;
version grhum.db_version.dbv_libelle%type;
begin
    select count(*) into cpt from grhum.db_version where dbv_libelle='1.7.1.7';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas à jour pour passer ce patch !');
    end if;
    select max(dbv_libelle) into version from grhum.db_version;
    if version = '1.7.1.8' then
        raise_application_error(-20000,'Le nouveau patch du user GRHUM est déjà appliqué.');
    end if;    
end;
/



COMMIT;
