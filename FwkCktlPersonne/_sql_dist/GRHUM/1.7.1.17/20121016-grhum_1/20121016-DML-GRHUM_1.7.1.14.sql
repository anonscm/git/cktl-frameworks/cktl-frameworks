--
-- Patch DML de GRHUM du 17/10/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.14
-- Date de publication : 17/10/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.14', TO_DATE('17/10/2012', 'DD/MM/YYYY'),NULL,'Evolutions ManGUE (CIR), Mise a jour Server Planning pour Kiwi');

-- Insertion de donnees pour la prise en compte dans KIWI des contrôles sur les Congés (Hamac) et les Emplois du temps (Superplan)
INSERT into GRHUM.SP_METHODE
 (MET_KEY, MET_NOM, MET_DESCRIPTION, MET_BOOL_SERVEUR, D_CREATION,D_MODIFICATION)
 Values
 (10, 'clientKiwi', 'Methode cliente de Kiwi (Missions)', 0, TO_DATE('09/23/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),TO_DATE('09/23/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS'));

INSERT into GRHUM.SP_MET_CLIENT (MET_KEY, CLI_TRAITEMENT, VAR_KEY, D_CREATION, D_MODIFICATION)
 Values (10, 0, 31, TO_DATE('05/30/2011 13:56:08', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('05/30/2011 13:56:08', 'MM/DD/YYYY HH24:MI:SS'));

INSERT into GRHUM.SP_REPARTITION
  (MET_KEY_CLIENT, MET_KEY_SERV, ID_KEY, D_CREATION, D_MODIFICATION) Values
  (10, 106, 1, TO_DATE('09/23/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('09/23/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS'));

INSERT into GRHUM.SP_REPARTITION
  (MET_KEY_CLIENT, MET_KEY_SERV, ID_KEY, D_CREATION, D_MODIFICATION)
 Values
 (10, 109, 1, TO_DATE('09/23/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('09/23/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS'));


-- Mise a jour d'un code RNE (Ville et Libelle)
UPDATE GRHUM.RNE SET ll_rne = 'COLLEGE (DES DEUX SARRES)', ville = 'LORQUIN', d_modification = TO_DATE('04/04/2011', 'dd/mm/yyyy')  WHERE c_rne ='0570328X';

-- Le CRCT est a declarer dans la partie CONGE du Fichier CIR.
UPDATE GRHUM.TYPE_ABSENCE set tem_cir = 'O' where c_type_absence = 'CRCT';

-- LE CRCT doit etre condidere comme une position d'activite
UPDATE GRHUM.POSITION SET c_position_onp = 'ACI00' where C_POSITION = 'CRCT';

UPDATE GRHUM.POSITION SET c_position_onp = 'ACI00' where C_POSITION = 'CFP';


-- Mise a jour du libelle de certains de liens de filiations
UPDATE LIEN_FILIATION_ENFANT set LFEN_EDITION = 'Naturel (Né Hors Mariage)' where LFEN_LIBELLE = 'NATUREL'; 

UPDATE LIEN_FILIATION_ENFANT set LFEN_EDITION = 'Légitime (Né pendant le Mariage)' where LFEN_LIBELLE = 'LEGITIME'; 


-- 
-- RIBFOUR_ULR
-- 
-- Rem : MaJ de banq_ordre vers une banque officielle de la BdF pour les banques dupliquées
update grhum.ribfour_ulr R
set banq_ordre=(select banq_ordre from grhum.banque where c_banque=R.c_banque and c_guichet=R.c_guichet and tem_local='N'),
d_modification=sysdate
where banq_ordre in
(
    select banq_ordre from grhum.banque
    where (c_banque,c_guichet) in
    (
        select c_banque,c_guichet
        from (select c_banque,c_guichet,bic,count(*) from grhum.banque
        where c_banque is not null
        group by c_banque,c_guichet,bic
        having count(*) > 1)
    )
    and tem_local='O'
);

-- Rem MaJ de BIC de RIBFOUR_ULR à partir du BIC de la BANQUE référencé par le banq_ordre
update grhum.ribfour_ulr R
set bic=(select bic from grhum.banque B where R.BANQ_ORDRE = B.BANQ_ORDRE), d_modification=sysdate
where rib_ordre in (select rib_ordre from grhum.ribfour_ulr where iban is not null and bic is null and rib_valide='O');



--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.14';


COMMIT;