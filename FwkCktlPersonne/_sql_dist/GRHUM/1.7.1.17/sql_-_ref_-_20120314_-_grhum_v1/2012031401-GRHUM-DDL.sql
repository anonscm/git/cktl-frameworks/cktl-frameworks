--
-- Patch DDL de GRHUM du 14/03/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.5
-- Date de publication : 14/03/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version précédente
declare
cpt integer;
begin
	select count(*) into cpt from grhum.db_version where dbv_libelle='1.7.1.4';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas à jour pour passer ce patch !');
    end if;
end;
/



--
-- BANQUE
--
DROP INDEX GRHUM.UK_BANQ_GUICHET;

CREATE UNIQUE INDEX GRHUM.UK_BANQ_GUICHET ON GRHUM.BANQUE(C_BANQUE,C_GUICHET,decode(tem_local,'O',null,tem_local));



--
-- MAJ_BANQUE_FGD
--
CREATE OR REPLACE PROCEDURE GRHUM.MAJ_BANQUE_FGD (
   cbanque          VARCHAR2,
   cguichet         VARCHAR2,
   cdomiciliation   VARCHAR2,
   cbic             VARCHAR2,
   cadresse         VARCHAR2,
   ccp              VARCHAR2,
   cville           VARCHAR2,
   douverture       DATE,
   dfermeture       DATE
)
IS

   n                NUMBER;
   persIdCreateur   GRHUM.COMPTE.PERS_ID%TYPE;

BEGIN

   -- persIdCreation
   SELECT PERS_ID into persIdCreateur FROM COMPTE WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));

   SELECT COUNT (*) INTO n FROM grhum.banque WHERE c_banque = cbanque AND c_guichet = cguichet;
   
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.banque(c_banque, c_guichet, domiciliation, d_creation,d_modification, bic, banq_ordre, adresse, code_postal,ville, d_ouverture, d_fermeture, tem_local, pers_id_creation, pers_id_modification)
      SELECT cbanque, cguichet, cdomiciliation, SYSDATE, SYSDATE, cbic, grhum.banque_seq.NEXTVAL, cadresse, ccp, cville, douverture, dfermeture, 'N', persIdCreateur, persIdCreateur FROM DUAL;
   ELSE
      -- mise à jour des banques issus du Fichier des Guichets (tem_local = 'N')
      UPDATE grhum.banque SET domiciliation = cdomiciliation, d_modification = SYSDATE, bic = cbic, adresse = cadresse, code_postal = ccp, ville = cville, d_ouverture = douverture, d_fermeture = dfermeture, pers_id_creation = persIdCreateur, pers_id_modification = persIdCreateur 
      WHERE c_banque = cbanque AND c_guichet = cguichet AND tem_local='N';
   END IF;
   
END;
/



COMMIT;
