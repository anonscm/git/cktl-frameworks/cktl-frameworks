--
-- Patch DML de GRHUM du 07/09/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.12
-- Date de publication : 07/09/2012
-- Auteur(s) : Alain MALAPLATE
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.12', TO_DATE('07/09/2012', 'DD/MM/YYYY'),NULL,'Mise à jour de la table RNE (ajout de 2 entrées) et modification d''un type de paramètre');


--
-- RNE (c_rne, ll_rne, adresse, code_postal, d_creation, d_modification, acad_code,etab_statut, ville)
--
insert into GRHUM.RNE
(c_rne, ll_rne, adresse, code_postal, d_creation, d_modification, acad_code,etab_statut, ville)
values ('0763409T','LYCEE PRIVE JEAN PAUL II','39 RUE DE L AVALASSE','76000', sysdate,sysdate,'021','PU','ROUEN');

insert into GRHUM.RNE
(c_rne, ll_rne, adresse, code_postal, d_creation, d_modification, acad_code,etab_statut, ville)
values ('9730423X','LYCEE POLYVALENT LAMA-PREVOT','QUARTIER DE MOULIN A VENT','97354', sysdate,sysdate,'033','PU','REMIRE-MONTJOLY');

--
-- GRHUM_PARAMETRES_TYPE
--
UPDATE GRHUM.GRHUM_PARAMETRES_TYPE SET TYPE_DESCRIPTION='Valeur numérique sur 4 ou 5 digits pour paramétrer les accès'
WHERE TYPE_ID_INTERNE='TCPIP';


--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.12';


COMMIT;