--
-- Patch DDL de GRHUM du 08/11/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.17
-- Date de publication : 08/10/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



create or replace
FUNCTION       grhum.Verif_bic(bic_a_tester RIBFOUR_ULR.BIC%TYPE)
RETURN INTEGER
IS
nb_pays integer;

BEGIN

	-- le bic ne doit pas ere null ou contenir des blancs
	if (bic_a_tester is null or length(trim(bic_a_tester)) = 0) then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> Erreur bic null : '||bic_a_tester);
		return 0;
	end if;
	-- si la fin du bic est 1 , on ne peut pas le verifier et on le considere comme bon
	if (substr(bic_a_tester,-1) = '1') then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> bic non SWIFT car se termine par 1 : '||bic_a_tester);
		return 1;
	end if;
	 
	if (length(trim(bic_a_tester)) < 8 ) then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> Le BIC est compose d''au moins 8 caracteres : '||bic_a_tester);
		return 0;
	end if;
	
	select count(*) into nb_pays from pays where upper(code_iso) = upper(substr(bic_a_tester,5,2));
	
	if (nb_pays = 0) then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> le code pays du bic (' || upper(substr(bic_a_tester,5,2)) || ') n''existe pas : '||bic_a_tester);
		return 0;
	end if;
	
	return 1;
END;

/


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.17', TO_DATE('08/11/2012', 'DD/MM/YYYY'),NULL,'Correction de Grhum.Verif_bic & Ajout de Saint-Pierre et Miquelon');



Insert into GRHUM.PAYS (C_PAYS,LL_PAYS,LC_PAYS,L_NATIONALITE,D_DEB_VAL,D_FIN_VAL,D_CREATION,D_MODIFICATION,CODE_ISO,L_EDITION,LL_PAYS_EN,ISO_3166_3) values ('975','SAINT-PIERRE-ET-MIQUELON','ST-PI-MIQ.','Français',to_date('31/03/96','DD/MM/RR'),null,SYSDATE,SYSDATE,'pm','Saint Pierre et Miquelon','Saint Pierre and Miquelon','SPM');


--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.17';


COMMIT;
