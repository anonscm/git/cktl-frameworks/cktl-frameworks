--
-- Patch DML de GRHUM du 07/12/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.11
-- Date de publication : 18/07/2012
-- Auteur(s) : Alain MALAPLATE
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.11', TO_DATE('18/07/2012', 'DD/MM/YYYY'),NULL,'Mise à jour de la table ADRESSE (allongement des champs adresse1 et adresse2)');


ALTER TABLE ADRESSE MODIFY ADR_ADRESSE1 VARCHAR2(100);
ALTER TABLE ADRESSE MODIFY ADR_ADRESSE2 VARCHAR2(300);
--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.11';


COMMIT;