--
-- Patch DDL de GRHUM du 08/11/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.16
-- Date de publication : 08/10/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- pre-requis
declare
cpt integer;
version grhum.db_version.dbv_libelle%type;
begin
    select count(*) into cpt from grhum.db_version where dbv_libelle='1.7.1.15';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas à jour pour passer ce patch !');
    end if;
    select max(dbv_libelle) into version from grhum.db_version;
    if version = '1.7.1.16' then
        raise_application_error(-20000,'Le nouveau patch du user GRHUM est déjà appliqué.');
    end if;    
end;
/






-- Pour ManGUE
ALTER TABLE GRHUM.TYPE_PERIODE_MILIT MODIFY (C_PERIODE_MILITAIRE VARCHAR2(3));


-- Suppression de procedures obsoletes

DROP PROCEDURE GRHUM.INS_CARRIERE;
DROP PROCEDURE GRHUM.MAJ_CARRIERE;
DROP PROCEDURE GRHUM.DEL_CARRIERE;

DROP PROCEDURE GRHUM.INS_ELEMENT_CARRIERE;
DROP PROCEDURE GRHUM.MAJ_ELEMENT_CARRIERE;
DROP PROCEDURE GRHUM.DEL_ELEMENT_CARRIERE;

DROP PROCEDURE GRHUM.INS_CHANGEMENT_POSITION;
DROP PROCEDURE GRHUM.MAJ_CHANGEMENT_POSITION;





--------------------------------------------------------------------
--
-- Mise à jour des procédures sur les IBAN, les BIC et le RIB
--
--------------------------------------------------------------------

create or replace
FUNCTION       grhum.Conversion_Table_Iban(lettre_a_convertir CHAR)
RETURN VARCHAR2

-- Conversion des lettres du code Pays de l'IBAN en chiffres à l'aide de la table de conversion ci-dessous.

IS

lettre	  CHAR(1);

BEGIN

	  lettre := UPPER(lettre_a_convertir);

	  CASE lettre

	  	   WHEN 'A' THEN RETURN '10';
		   WHEN 'B' THEN RETURN '11';
		   WHEN 'C' THEN RETURN '12';
		   WHEN 'D' THEN RETURN '13';
		   WHEN 'E' THEN RETURN '14';
		   WHEN 'F' THEN RETURN '15';
		   WHEN 'G' THEN RETURN '16';
		   WHEN 'H' THEN RETURN '17';
		   WHEN 'I' THEN RETURN '18';
		   WHEN 'J' THEN RETURN '19';
		   WHEN 'K' THEN RETURN '20';
		   WHEN 'L' THEN RETURN '21';
		   WHEN 'M' THEN RETURN '22';
		   WHEN 'N' THEN RETURN '23';
		   WHEN 'O' THEN RETURN '24';
		   WHEN 'P' THEN RETURN '25';
		   WHEN 'Q' THEN RETURN '26';
		   WHEN 'R' THEN RETURN '27';
		   WHEN 'S' THEN RETURN '28';
		   WHEN 'T' THEN RETURN '29';
		   WHEN 'U' THEN RETURN '30';
		   WHEN 'V' THEN RETURN '31';
		   WHEN 'W' THEN RETURN '32';
		   WHEN 'X' THEN RETURN '33';
		   WHEN 'Y' THEN RETURN '34';
		   WHEN 'Z' THEN RETURN '35';
		   -- ajout d'un else pour éviter les plantages
       ELSE return '--';

	   END CASE;

END;
 
/

create or replace
FUNCTION       grhum.Cons_Clef_Iban(codePays char,bban varchar)
RETURN varchar
-- Fonction qui calcule la clef de l'IBAN à partir du BBAN
-- codePays : code ISO de 2 caractéres
-- Pour la France, le BBAN est constitué des 4 éléments du RIB
-- BBAN = code_banque + code_guichet + no_compte + cle_rib
-- retourne la clef IBAN si OK sinon retourne XX

IS

i            INTEGER;
car          CHAR(1);
bbanConvert	 VARCHAR2(30);
clefIban	 VARCHAR2(2);
pays1		 VARCHAR2(2);
pays2		 VARCHAR2(2);
etape		 VARCHAR2(34);
clefCalculee NUMBER;
clefFinale	 VARCHAR2(2);

BEGIN

    clefFinale := 'XX';
    
    if ( bban is null ) then 
      return '--';
    end if;
    -- Conversion des lettres du BBAN en chiffre
    bbanConvert := upper(bban);
    FOR i IN 1..LENGTH(bban)
    LOOP
        car := SUBSTR(upper(bban),i,1);
        -- Pour chaque lettre du BBAN
        IF (  ASCII(car) BETWEEN 65 AND 90 ) THEN
           bbanConvert := REPLACE(bbanConvert,car,Conversion_Table_Iban(car));
        END IF;
    END LOOP;

    -- Conversion des lettres du code Pays en chiffre
    pays1 := Conversion_Table_Iban(SUBSTR(codePays,1,1));
    pays2 := Conversion_Table_Iban(SUBSTR(codePays,2,1));
    -- si il y a un soucis avec le code pays, renvoie un code bidon -- pour eviter les plantages
    if (pays1 = '--' or pays2 = '--') then 
      return '--';
    end if;

    etape := bbanConvert || pays1 || pays2 || '00';

    clefCalculee := 98 - ( MOD(TO_NUMBER(etape),97));

    IF (clefCalculee < 10) THEN
        clefFinale := '0' || TO_CHAR(clefCalculee);
	ELSE
		clefFinale := TO_CHAR(clefCalculee);
	END IF;

    RETURN(clefFinale);

END;
 
/

create or replace
FUNCTION       grhum.Verif_Iban(iban_a_tester RIBFOUR_ULR.IBAN%TYPE)
RETURN INTEGER
-- Fonction qui verifie si le code IBAN du RIB est correct
-- retourne 1 si le code IBAN est OK
-- retourne 0 si  le code IBAN est incorrect

/*
   Le code IBAN (International Bank Account Number) est un identifiant international pour les comptes clients des institutions financieres. Il est compose de trois parties :
   Le code pays : 2 lettres identifiant le pays dans lequel l'institution financiere reside. On utilise les codes specifies dans la norme ISO 3166.
   Une cle de controle composee de 2 chiffres.
   Le numero de compte (BBAN, Basic Bank Account Number) compose au maximum de 30 caracteres alphanumeriques (les 10 chiffres plus les lettres majuscules de "A" a "Z".
   ex : FR14 2004 1010 0505 0001 3M02 606
*/

IS

i  	  		 INTEGER;
car			 CHAR(1);
iban		 RIBFOUR_ULR.iban%TYPE;
codePays	 VARCHAR2(2);
clefIban	 VARCHAR2(2);
bban		 VARCHAR2(30);
pays1		 VARCHAR2(2);
pays2		 VARCHAR2(2);
etape1		 RIBFOUR_ULR.iban%TYPE;
etape2		 RIBFOUR_ULR.iban%TYPE;
clefCalculee NUMBER;
clefFinale	 VARCHAR2(2);
nb_car integer;

BEGIN

	 -- Conversion du code IBAN en Majuscule et sans les Espaces
	 iban := UPPER(REPLACE(iban_a_tester,' ',''));

	 codePays := SUBSTR(iban,1,2);
	 --DBMS_OUTPUT.PUT_LINE('codePays : '||codePays);

	 clefIban := SUBSTR(iban,3,2);
	 --DBMS_OUTPUT.PUT_LINE('clefIban : '||clefIban);

	 bban := SUBSTR(iban,5,LENGTH(iban));
	 --DBMS_OUTPUT.PUT_LINE('bban : '||bban);

	 etape1 := bban || codePays || '00';
	 --DBMS_OUTPUT.PUT_LINE('etape1 : '||etape1);

	-- Conversion des lettres du BBAN en chiffre
	 nb_car := LENGTH(bban);
	 i := 1;
	 while i <= nb_car
	 loop
		car := SUBSTR(bban,i,1);
		-- Pour chaque lettre du BBAN
		IF (  car >= CHR(65) AND car <= CHR(90) ) THEN
			bban := REPLACE(bban,car,Conversion_Table_Iban(car));
			nb_car := LENGTH(bban);
		END IF;
	   i := i+1;  
	 end loop;
  
	-- Conversion des lettres du code Pays en chiffre
	 pays1 := Conversion_Table_Iban(SUBSTR(codePays,1,1));
	 pays2 := Conversion_Table_Iban(SUBSTR(codePays,2,1));
   -- si le code pays est merdique alors iban non valide 
    if (pays1 = '--' or pays2 = '--') then 
	 	 RETURN (0);
    end if;

	 etape2 := bban || pays1 || pays2 || '00';

	 clefCalculee := 98 - ( MOD(TO_NUMBER(etape2),97));
	 --DBMS_OUTPUT.PUT_LINE('clefCalculee :  '||clefCalculee);

	 IF (clefCalculee < 10) THEN
	 	clefFinale := '0' || TO_CHAR(clefCalculee);
	 ELSE
	 	clefFinale := TO_CHAR(clefCalculee);
	 END IF;

	 -- test entre la clefFinale et la clef saisie
	 IF (clefFinale = clefIban) THEN
	 	 RETURN(1);
	 ELSE
	 	 RETURN (0);
	 END IF;

     EXCEPTION
     -- si on a une exception notamment sur un to_number, l'IBAN n''est pas correct
		WHEN others THEN
		RETURN (0);
END;

/


create or replace
FUNCTION       grhum.Verif_bic(bic_a_tester RIBFOUR_ULR.BIC%TYPE)
RETURN INTEGER
IS
nb_pays integer;

BEGIN

	-- le bic ne doit pas ere null ou contenir des blancs
	if (bic_a_tester is null or length(trim(bic_a_tester)) = 0) then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> Erreur bic null : '||bic_a_tester);
		return 0;
	end if;
	-- si la fin du bic est 1 , on ne peut pas le verifier et on le considere comme bon
	if (substr(bic_a_tester,-1) = '1') then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> bic non SWIFT car se termine par 1 : '||bic_a_tester);
		return 1;
	end if;
	 
	if (length(trim(bic_a_tester)) < 8 ) then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> Le BIC est compose d''au moins 8 caracteres : '||bic_a_tester);
		return 0;
	end if;
	
	select count(*) into nb_pays from pays where code_iso = upper(substr(bic_a_tester,5,2));
	
	if (nb_pays = 0) then
		DBMS_OUTPUT.PUT_LINE('Verif_bic -> le code pays du bic (' || upper(substr(bic_a_tester,5,2)) || ') n''existe pas : '||bic_a_tester);
		return 0;
	end if;
	
	return 1;
END;

/
 update ribfour_ulr set ribfour_ulr.bic = (select b.bic from banque b where b.banq_ordre = ribfour_ulr.banq_ordre) where ribfour_ulr.bic is null or length(trim (ribfour_ulr.bic)) = 0;
commit;

/

update grhum.ribfour_ulr set iban = upper(substr(bic,5,2))||GRHUM.CONS_CLEF_IBAN(substr(bic,5,2),c_banque||c_guichet||no_compte||cle_rib)||c_banque||c_guichet||no_compte||cle_rib
where c_banque is not null
and c_guichet is not null
and no_compte is not null
and cle_rib is not null
and iban is null
and grhum.verif_bic(bic) = 0
and rib_valide = 'O';
commit;

/

create or replace force view grhum.v_rib_probleme (rib_ordre, type_anomalie, anomalie, que_faire)
as

   select r.rib_ordre,
          rpad ('1-Attention (SEPA)', 20, ' ') as type_anomalie,
          rpad ('Agence bancaire fermee le ' || to_char (d_fermeture, 'dd/mm/yyyy'), 200, ' ') as anomalie,
          rpad ('Affecter la bonne banque au rib pour le fournisseur ' || p.pers_libelle || '(' || f.fou_code || ')', 200, ' ') as que_faire
   from   ribfour_ulr r inner join banque b on (r.banq_ordre = b.banq_ordre)
             inner join fournis_ulr f on (r.fou_ordre = f.fou_ordre and f.fou_valide ='O' and r.rib_valide = 'O')
               inner join personne p on (f.pers_id = p.pers_id)
   where  b.bic = r.bic and b.d_fermeture is not null
   union all
   select r.rib_ordre,
          rpad ('2-Bloquant (SEPA)', 20, ' ') as type_anomalie,
          rpad ('IBAN : format non valide : ' || r.iban, 100, ' ') as anomalie,
          rpad ('Corriger l''IBAN du rib du fournisseur ' || p.pers_libelle || '(' || f.fou_code || ')', 200, ' ') as que_faire
   from   ribfour_ulr r inner join banque b on (r.banq_ordre = b.banq_ordre)
             inner join fournis_ulr f on (r.fou_ordre = f.fou_ordre and f.fou_valide ='O' and r.rib_valide = 'O')
               inner join personne p on (f.pers_id = p.pers_id)
   where b.bic = r.bic and r.iban is not null and grhum.verif_iban (r.iban) = 0
   union all 
    select r.rib_ordre,
           rpad ('3-Attention (SEPA)', 20, ' ') as type_anomalie,
           rpad ('IBAN : ' || r.iban || ' different de l''iban calcule a partir du rib :' || upper(substr(r.bic,5,2))||GRHUM.CONS_CLEF_IBAN(substr(r.bic,5,2),r.c_banque||r.c_guichet||r.no_compte||r.cle_rib) || r.c_banque || r.c_guichet || r.no_compte || r.cle_rib, 100, ' ') as anomalie,
           rpad ('Verifier l''IBAN aupres du fournisseur ' || p.pers_libelle || '(' || f.fou_code || ') le corriger si besoin', 200, ' ') as que_faire

    from   ribfour_ulr r inner join banque b on (r.banq_ordre = b.banq_ordre)
             inner join fournis_ulr f on (r.fou_ordre = f.fou_ordre and f.fou_valide ='O' and r.rib_valide = 'O')
               inner join personne p on (f.pers_id = p.pers_id)
                 inner join pays p2 on (p2.code_iso = upper(substr(r.bic,5,2)) )
    where 1=1
    and r.iban != upper(substr(r.bic,5,2))||GRHUM.CONS_CLEF_IBAN(substr(r.bic,5,2),r.c_banque||r.c_guichet||r.no_compte||r.cle_rib)||r.c_banque||r.c_guichet||r.no_compte||r.cle_rib
     and substr(r.bic,-1) != '1'
     and length(trim(r.bic)) = length(r.bic)
     and length(trim(r.bic)) > 7
     and r.c_banque is not null
     and r.c_guichet is not null
     and r.no_compte is not null
     and r.cle_rib is not null
     and r.bic is not null
     and r.iban is not null
  union all 
     select rib_ordre,
          rpad ('4-Attention (SEPA)', 20, ' ') as type_anomalie,
          rpad ('IBAN : la generation du code iban (' || upper(substr(b.bic,5,2))||GRHUM.CONS_CLEF_IBAN(upper(substr(b.bic,5,2)),r.c_banque||r.c_guichet||no_compte||cle_rib)||r.c_banque||r.c_guichet||no_compte||cle_rib || ') d''apres les information du rib n''est pas valide ', 100, ' ') as anomalie,
          rpad ('Verifier le RIB aupres du fournisseur ' || pers_libelle || ' (' || fou_code || ') et le corriger si besoin', 200, ' ') as que_faire
      from ribfour_ulr r, fournis_ulr f, personne p,pays p2, banque b
      where r.fou_ordre = f.fou_ordre
      and f.pers_id = p.pers_id
      and r.banq_ordre = b.banq_ordre
      and verif_iban(upper(substr(b.bic,5,2))||GRHUM.CONS_CLEF_IBAN(upper(substr(b.bic,5,2)),r.c_banque||r.c_guichet||no_compte||cle_rib)||r.c_banque||r.c_guichet||no_compte||cle_rib) = 1
      and upper(substr(b.bic,5,2)) = p2.code_iso
      and f.fou_valide = 'O'
      and r.rib_valide = 'O'
      and substr(b.bic,-1) != '1'
      and length(trim(b.bic)) > 7
      and r.iban is null
  union all
     select r.rib_ordre,
          rpad ('5-Attention (SEPA)', 20, ' ') as type_anomalie,
          rpad ('BIC : (' || r.bic || ') non valide (pb de taille ou de pays )', 100, ' ') as anomalie,
          rpad ('Verifier le code BIC aupres du fournisseur ' || p.pers_libelle || ' (' || f.fou_code || ') et le corriger si besoin (la correction peut se faire au niveau de la banque)', 200, ' ') as que_faire
      from ribfour_ulr r, fournis_ulr f, personne p
      where r.fou_ordre = f.fou_ordre
      and f.pers_id = p.pers_id
      and f.fou_valide = 'O'
      and r.rib_valide = 'O'
      and verif_bic(r.bic) = 0;

/



