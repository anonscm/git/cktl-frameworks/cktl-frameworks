--
-- Patch DML de GRHUM du 07/11/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.16
-- Date de publication : 07/11/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.16', TO_DATE('07/11/2012', 'DD/MM/YYYY'),NULL,'Commentaires ...');


UPDATE GRHUM.TYPE_PERIODE_MILIT set C_TYPE_PERIODE_MILITAIRE_ONP = 'SN508' where  C_PERIODE_MILITAIRE = 'V';

INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L2', 'VSL', 'Volontariat Service Long (2 Mois)', 'O', sysdate, sysdate, 'SN141');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L3', 'VSL', 'Volontariat Service Long (3 Mois)', 'O', sysdate, sysdate, 'SN142');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L4', 'VSL', 'Volontariat Service Long (4 Mois)', 'O', sysdate, sysdate, 'SN143');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L5', 'VSL', 'Volontariat Service Long (5 Mois)', 'O', sysdate, sysdate, 'SN144');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L6', 'VSL', 'Volontariat Service Long (6 Mois)', 'O', sysdate, sysdate, 'SN145');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L7', 'VSL', 'Volontariat Service Long (7 Mois)', 'O', sysdate, sysdate, 'SN146');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L8', 'VSL', 'Volontariat Service Long (8 Mois)', 'O', sysdate, sysdate, 'SN147');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L9', 'VSL', 'Volontariat Service Long (9 Mois)', 'O', sysdate, sysdate, 'SN148');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L10', 'VSL', 'Volontariat Service Long (10 Mois)', 'O', sysdate, sysdate, 'SN149');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L11', 'VSL', 'Volontariat Service Long (11 Mois)', 'O', sysdate, sysdate, 'SN150');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L12', 'VSL', 'Volontariat Service Long (12 Mois)', 'O', sysdate, sysdate, 'SN151');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L13', 'VSL', 'Volontariat Service Long (13 Mois)', 'O', sysdate, sysdate, 'SN152');
INSERT INTO GRHUM.TYPE_PERIODE_MILIT VALUES ('L14', 'VSL', 'Volontariat Service Long (14 Mois)', 'O', sysdate, sysdate, 'SN153');

UPDATE GRHUM.MOTIF_POSITION set TEM_ENFANT  = 'N' where C_MOTIF_POSITION = 'CP';

-- 
-- PASSAGE_ECHELON
-- 
-- ADJ. ADM DE L'EN ET DE L'ENS SUP PR C1
update grhum.passage_echelon set DUREE_PT_CHOIX_ANNEES=3, DUREE_PASSAGE_ANNEES=4,d_modification=sysdate where c_grade='0564' and c_echelon='07' and d_fermeture is null;
Insert into GRHUM.PASSAGE_ECHELON(C_GRADE, C_ECHELON, C_INDICE_BRUT, D_CREATION, D_MODIFICATION, D_OUVERTURE, TEM_LOCAL) Values('0564', '08', '499', SYSDATE, SYSDATE, TO_DATE('01/01/2012', 'DD/MM/YYYY'), 'N');

-- ADJOINT TECHNIQUE RECH ET FORM  PR CL1
update grhum.passage_echelon set c_echelon='08',d_modification=sysdate where c_grade='8394' and c_echelon='SL' and d_fermeture is null;

-- MAGASINIER DES BIBLIOTHEQUES 1ERE CLASSE
update grhum.passage_echelon set DUREE_PT_CHOIX_ANNEES=null, DUREE_PASSAGE_ANNEES=null,d_modification=sysdate where c_grade='8202' and c_echelon='11' and d_fermeture is null;

-- MAGASINIER DES BIBLIOTHEQUES PR. 2EME CL
update grhum.passage_echelon set DUREE_PT_CHOIX_ANNEES=2, DUREE_PASSAGE_ANNEES=3,d_modification=sysdate where c_grade='8203' and c_echelon='06' and d_fermeture is null;

-- MAGASINIER DES BIBLIOTHEQUES PR. 1ERE CL
update grhum.passage_echelon set DUREE_PT_CHOIX_MOIS=6,DUREE_PASSAGE_ANNEES=2,d_modification=sysdate where c_grade='8204' and c_echelon='01' and d_fermeture is null;
update grhum.passage_echelon set DUREE_PT_CHOIX_ANNEES=2, DUREE_PASSAGE_ANNEES=3,d_modification=sysdate where c_grade='8204' and c_echelon='03' and d_fermeture is null;
Insert into GRHUM.PASSAGE_ECHELON(C_GRADE, C_ECHELON, C_INDICE_BRUT, D_CREATION, D_MODIFICATION, D_OUVERTURE, TEM_LOCAL) Values('8204', '08', '499', SYSDATE, SYSDATE, TO_DATE('01/01/2012', 'DD/MM/YYYY'), 'N');


COMMIT;