--
-- Patch DDL de GRHUM du 07/11/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.16
-- Date de publication : 0/11/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


-- Pour ManGUE
ALTER TABLE GRHUM.TYPE_PERIODE_MILIT MODIFY (C_PERIODE_MILITAIRE VARCHAR2(3));


-- Suppression de procedures obsoletes

drop procedure GRHUM.INS_CARRIERE;
drop procedure GRHUM.MAJ_CARRIERE;
drop procedure GRHUM.DEL_CARRIERE;

drop procedure GRHUM.INS_ELEMENT_CARRIERE;
drop procedure GRHUM.MAJ_ELEMENT_CARRIERE;
drop procedure GRHUM.DEL_ELEMENT_CARRIERE;

drop procedure GRHUM.INS_CHANGEMENT_POSITION;
drop procedure GRHUM.MAJ_CHANGEMENT_POSITION;





