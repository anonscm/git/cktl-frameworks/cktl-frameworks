--
-- Patch DDL de GRHUM du 04/06/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.9
-- Date de publication : 04/06/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- pré-requis
declare
cpt integer;
version grhum.db_version.dbv_libelle%type;
begin
    select count(*) into cpt from grhum.db_version where dbv_libelle='1.7.1.8';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas à jour pour passer ce patch !');
    end if;
    select max(dbv_libelle) into version from grhum.db_version;
    if version = '1.7.1.9' then
        raise_application_error(-20000,'Le nouveau patch du user GRHUM est déjà appliqué.');
    end if;    
end;
/



--
-- PERSONNE_TELEPHONE
--
UPDATE GRHUM.PERSONNE_TELEPHONE SET LISTE_ROUGE='N', D_MODIFICATION=SYSDATE WHERE LISTE_ROUGE IS NULL;

ALTER TABLE GRHUM.PERSONNE_TELEPHONE MODIFY LISTE_ROUGE DEFAULT 'N' NOT NULL;



--
-- EVOLUTION_CHEVRON
--
ALTER TABLE GRHUM.EVOLUTION_CHEVRON DROP CONSTRAINT FK_AS_EVOL_CHEVRON_ELT_CAR;



--
-- NATURE_BONIFICATION 
--
CREATE TABLE GRHUM.NATURE_BONIFICATION
(
  NBO_ORDRE      	NUMBER(2)               NOT NULL,
  NBO_CODE       	VARCHAR2(20)            NOT NULL,
  NBO_LIBELLE_COURT VARCHAR2(100)           NOT NULL,
  NBO_LIBELLE_LONG  VARCHAR2(250)           NOT NULL,
  D_OUVERTURE       DATE                    NOT NULL,
  D_FERMETURE       DATE,
  D_CREATION        DATE                    NOT NULL,
  D_MODIFICATION    DATE                    NOT NULL
)
TABLESPACE DATA_GRHUM;

COMMENT ON TABLE GRHUM.NATURE_BONIFICATION IS 'Nomenclature des types de bonifications de service';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.NBO_ORDRE IS 'Clef primaire';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.NBO_CODE IS 'Code du type de bonification';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.NBO_LIBELLE_COURT IS 'Libellé court du type de bonification';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.NBO_LIBELLE_LONG IS 'Libellé long du type de bonification';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.D_OUVERTURE IS 'Date d''ouverture de la bonification';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.D_FERMETURE IS 'Date de fermeture de la bonification';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.D_CREATION IS 'Date de création de l''enregistrement.';

COMMENT ON COLUMN GRHUM.NATURE_BONIFICATION.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement.';


CREATE UNIQUE INDEX GRHUM.PK_NATURE_BONIF ON GRHUM.NATURE_BONIFICATION(NBO_ORDRE) LOGGING TABLESPACE DATA_GRHUM NOPARALLEL;

ALTER TABLE GRHUM.NATURE_BONIFICATION ADD (
  CONSTRAINT PK_NATURE_BONIF
 PRIMARY KEY
 (NBO_ORDRE)
    USING INDEX 
    TABLESPACE DATA_GRHUM);

GRANT REFERENCES, SELECT ON GRHUM.NATURE_BONIFICATION TO MANGUE;

CREATE SEQUENCE GRHUM.NATURE_BONIFICATION_SEQ START WITH 1 NOCACHE;



--
-- NATURE_BONIF_TERRITOIRE 
--
CREATE TABLE GRHUM.NATURE_BONIF_TERRITOIRE
(
  NBOT_ORDRE      	NUMBER(2)                  NOT NULL,
  NBOT_CODE       	VARCHAR2(20)               NOT NULL,
  NBOT_LIBELLE    	VARCHAR2(100)              NOT NULL,
  D_OUVERTURE       DATE                       NOT NULL,
  D_FERMETURE       DATE,
  D_CREATION        DATE                       NOT NULL,
  D_MODIFICATION    DATE                       NOT NULL
)
TABLESPACE DATA_GRHUM;

COMMENT ON TABLE GRHUM.NATURE_BONIF_TERRITOIRE IS 'Nomenclature des territoires associes aux bonifications de service';

COMMENT ON COLUMN GRHUM.NATURE_BONIF_TERRITOIRE.NBOT_ORDRE IS 'Clef primaire';

COMMENT ON COLUMN GRHUM.NATURE_BONIF_TERRITOIRE.NBOT_CODE IS 'Code du territoire';

COMMENT ON COLUMN GRHUM.NATURE_BONIF_TERRITOIRE.NBOT_LIBELLE IS 'Libellé du territoire';

COMMENT ON COLUMN GRHUM.NATURE_BONIF_TERRITOIRE.D_OUVERTURE IS 'Date d''ouverture du territoire';

COMMENT ON COLUMN GRHUM.NATURE_BONIF_TERRITOIRE.D_FERMETURE IS 'Date de fermeture du territoire';

COMMENT ON COLUMN GRHUM.NATURE_BONIF_TERRITOIRE.D_CREATION IS 'Date de création de l''enregistrement.';

COMMENT ON COLUMN GRHUM.NATURE_BONIF_TERRITOIRE.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement.';


CREATE UNIQUE INDEX GRHUM.PK_NBOT ON GRHUM.NATURE_BONIF_TERRITOIRE(NBOT_ORDRE) LOGGING TABLESPACE DATA_GRHUM NOPARALLEL;

ALTER TABLE GRHUM.NATURE_BONIF_TERRITOIRE ADD (
  CONSTRAINT PK_NBOT
 PRIMARY KEY
 (NBOT_ORDRE)
    USING INDEX 
    TABLESPACE DATA_GRHUM);

GRANT REFERENCES, SELECT ON GRHUM.NATURE_BONIF_TERRITOIRE TO MANGUE;

CREATE SEQUENCE GRHUM.NATURE_BONIF_TERRITOIRE_SEQ START WITH 1 NOCACHE;



--
-- GRHUM.ECOLES_MILITAIRES
--
CREATE TABLE GRHUM.ECOLES_MILITAIRES
(
  EM_ORDRE      	NUMBER(2)                   NOT NULL,
  EM_CODE       	VARCHAR2(20)                NOT NULL,
  EM_LIBELLE    	VARCHAR2(100)               NOT NULL,
  D_OUVERTURE       DATE                        NOT NULL,
  D_FERMETURE       DATE,
  D_CREATION        DATE                        NOT NULL,
  D_MODIFICATION    DATE                        NOT NULL
)
TABLESPACE DATA_GRHUM;

COMMENT ON TABLE GRHUM.ECOLES_MILITAIRES IS 'Liste des écoles militaires (Utilisé pour le calcul des retraites)';

COMMENT ON COLUMN GRHUM.ECOLES_MILITAIRES.EM_ORDRE IS 'Clef primaire';

COMMENT ON COLUMN GRHUM.ECOLES_MILITAIRES.EM_CODE IS 'Code de l''école';

COMMENT ON COLUMN GRHUM.ECOLES_MILITAIRES.EM_LIBELLE IS 'Libellé de l''école';

COMMENT ON COLUMN GRHUM.ECOLES_MILITAIRES.D_OUVERTURE IS 'Date d''ouverture';

COMMENT ON COLUMN GRHUM.ECOLES_MILITAIRES.D_FERMETURE IS 'Date de fermeture';

COMMENT ON COLUMN GRHUM.ECOLES_MILITAIRES.D_CREATION IS 'Date de création de l''enregistrement.';

COMMENT ON COLUMN GRHUM.ECOLES_MILITAIRES.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement.';

CREATE UNIQUE INDEX GRHUM.PK_EM ON GRHUM.ECOLES_MILITAIRES(EM_ORDRE) LOGGING TABLESPACE DATA_GRHUM NOPARALLEL;

ALTER TABLE GRHUM.ECOLES_MILITAIRES ADD (
  CONSTRAINT PK_ECOLES_MILITAIRES
 PRIMARY KEY
 (EM_ORDRE)
    USING INDEX 
    TABLESPACE DATA_GRHUM);

GRANT REFERENCES, SELECT ON GRHUM.ECOLES_MILITAIRES TO MANGUE;

CREATE SEQUENCE GRHUM.ECOLES_MILITAIRES_SEQ START WITH 1 NOCACHE;




--
-- Echanger_Personne_Grhum
--
CREATE OR REPLACE PROCEDURE GRHUM.Echanger_Personne_Grhum
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
  ligne1 VARCHAR2(200);
  ligne2 VARCHAR2(200);
  cursor c1 (oldid number) is select c_structure from grhum.repart_structure where pers_id = oldid;
  cursor c2 (oldid number) is select no_telephone from grhum.personne_telephone where pers_id = oldid;
  cstruct varchar2(10);
  notel varchar2(20);
BEGIN
  ligne1 := NULL;
  ligne2 := NULL;

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ABSENCES_OLD
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ABSENCES_OLD set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_individu_urgence = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNEL_ULR set no_individu_urgence = '||newno||' where no_individu_urgence = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0) then
    SELECT COUNT(*)
    INTO   nbenr1
    FROM   grhum.PERSONNEL_ULR
    WHERE  no_dossier_pers = newno;
    IF (nbenr1 = 0) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'create table TEMP00 as select * from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update TEMP00 set no_dossier_pers = '||newno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'insert into GRHUM.PERSONNEL_ULR select * from TEMP00;');

       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
       ligne2 := 'drop table TEMP00;';
    else
       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
    end if;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AFFECTATION_OLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AFFECTATION_OLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_PERSONNEL
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_PERSONNEL set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR_EDITION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR_EDITION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DIPLOME
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DIPLOME set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DOSSIER
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DOSSIER set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_HISTORIQUE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_HISTORIQUE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_INDIVIDU_SITUATION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_INDIVIDU_SITUATION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BAP_ELEMENT_CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BAP_ELEMENT_CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BLOC_NOTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BLOC_NOTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_ECHELON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_ECHELON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_INDICIAIRE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_INDICIAIRE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CARRIERE_OLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CARRIERE_OLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CESS_PROG_ACTIVITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CESS_PROG_ACTIVITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CGE_MOD_AGT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CGE_MOD_AGT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CGNT_ACCIDENT_TRAV
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CGNT_ACCIDENT_TRAV set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CG_FIN_ACTIVITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CG_FIN_ACTIVITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CHANGEMENT_POSITION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CHANGEMENT_POSITION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CHERCHEUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CHERCHEUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CLM
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CLM set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  /*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE_QUOTA
  WHERE  par_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE_QUOTA set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CONGE_ACCIDENT_SERV
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CONGE_ACCIDENT_SERV set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CONGE_ADOPTION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CONGE_ADOPTION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CONGE_FORMATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CONGE_FORMATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CONGE_GARDE_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CONGE_GARDE_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CONGE_MALADIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CONGE_MALADIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CONGE_MATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CONGE_MATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CONTRAT_OLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CONTRAT_OLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DECLARATION_MATERNITE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DECLARATION_MATERNITE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DELEGATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DELEGATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DEPOSITAIRE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DEPOSITAIRE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DETAIL_CG_MALADIE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DETAIL_CG_MALADIE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ELEMENT_CARRIERE_OLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ELEMENT_CARRIERE_OLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ENQUETES_REPONSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ENQUETES_REPONSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ETUDIANT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ETUDIANT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.EVOLUTION_CHEVRON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.EVOLUTION_CHEVRON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.FOURNIS_ULR
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.FOURNIS_ULR set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_DISTINCTIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_DISTINCTIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_FORMATIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_FORMATIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_PSEUDO
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_PSEUDO set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_LA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_ELECTEUR set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_LA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_LA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_TA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_TA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_TA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_TA_STRUCTURE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MAD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MAD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MI_TPS_THERAP
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MI_TPS_THERAP set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_auteur = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_auteur = '||newno||' where no_auteur = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NOTES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NOTES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PAIEMENT
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PAIEMENT set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_ALIAS
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_ALIAS set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_TELEPHONE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) then
    open c2(oldid);
    loop
        fetch c2 into notel;
        exit when c2%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.PERSONNE_TELEPHONE WHERE PERS_ID = newid and no_telephone = notel;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_TELEPHONE set pers_id = '||newid||' where pers_id = '||oldid||' and no_telephone='''||notel||''';');
        end if;
    end loop;
    close c2;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_PERSONNEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_PERSONNEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME_HISTO
  WHERE  pers_id_createur = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME_HISTO set pers_id_createur = '||newid||' where pers_id_createur = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.RELIQUATS_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.RELIQUATS_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ASSOCIATION
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
       SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_ASSOCIATION WHERE PERS_ID = newid;
       IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ASSOCIATION set pers_id = '||newid||' where pers_id = '||oldid||'
               and c_structure not in (select c_structure from GRHUM.REPART_ASSOCIATION where pers_id = '||newid||');');
       END IF;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_BUREAU
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_BUREAU set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_EMPLOI
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldno||','||newno,SYSDATE,
               'update GRHUM.REPART_EMPLOI set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_PERSONNE_ADRESSE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_PERSONNE_ADRESSE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_STRUCTURE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
    open c1(oldid);
    loop
        fetch c1 into cstruct;
        exit when c1%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_STRUCTURE WHERE PERS_ID = newid and c_structure = cstruct;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_STRUCTURE set pers_id = '||newid||' where pers_id = '||oldid||' and c_structure = '||cstruct||';');
        END IF;
    end loop;
    close c1;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SECRETARIAT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SECRETARIAT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STAGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STAGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_owner = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_owner = '||newno||' where grp_owner = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_responsable = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_responsable = '||newno||' where grp_responsable = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_ROLE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_ROLE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_CATEGORIE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_CATEGORIE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.TRAVAUX_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.TRAVAUX_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  IF (ligne1 IS NOT NULL)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne1);
       IF (ligne2 IS NOT NULL) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne2);
       end if;
  END IF;
END;
/



--
-- Ins_Individu_Ulr
--
CREATE OR REPLACE PROCEDURE GRHUM.Ins_Individu_Ulr
(
INS_NOM_PATRONYMIQUE         INDIVIDU_ULR.nom_patronymique%TYPE,
INS_PRENOM                   INDIVIDU_ULR.prenom%TYPE,
INS_C_CIVILITE               INDIVIDU_ULR.c_civilite%TYPE,
INS_NOM_USUEL                INDIVIDU_ULR.nom_usuel%TYPE,
INS_PRENOM2                  INDIVIDU_ULR.prenom2%TYPE,
INS_D_NAISSANCE                 INDIVIDU_ULR.d_naissance%TYPE,
INS_VILLE_DE_NAISSANCE       INDIVIDU_ULR.ville_de_naissance%TYPE,
INS_C_DEPT_NAISSANCE         INDIVIDU_ULR.c_dept_naissance%TYPE,
INS_C_PAYS_NAISSANCE         INDIVIDU_ULR.c_pays_naissance%TYPE,
INS_C_PAYS_NATIONALITE       INDIVIDU_ULR.c_pays_nationalite%TYPE,
INS_D_NATURALISATION         INDIVIDU_ULR.d_naturalisation%TYPE,
INS_D_DECES                  INDIVIDU_ULR.d_deces%TYPE,
INS_IND_C_SIT_MILITAIRE      INDIVIDU_ULR.ind_c_sit_militaire%TYPE,
INS_IND_C_SITUATION_FAMILLE  INDIVIDU_ULR.ind_c_situation_famille%TYPE,
INS_IND_NO_INSEE             INDIVIDU_ULR.ind_no_insee%TYPE,
INS_IND_CLE_INSEE            INDIVIDU_ULR.ind_cle_insee%TYPE,
INS_IND_QUALITE              INDIVIDU_ULR.ind_qualite%TYPE,
INS_IND_ACTIVITE             INDIVIDU_ULR.ind_activite%TYPE,
INS_IND_ORIGINE              INDIVIDU_ULR.ind_origine%TYPE,
INS_IND_PHOTO                INDIVIDU_ULR.ind_photo%TYPE,
persid                         OUT NUMBER,
noindividu                     OUT NUMBER
)
-- Auteur : Cocktail
-- modification : 20/04/2012
-- prise en compte des attributs pers_id_creation et pers_id_modification
IS

cpt                 INTEGER;
new_INS_D_NAISSANCE DATE;
cpaysnaissance      INDIVIDU_ULR.c_pays_naissance%TYPE;
cpaysnationalite    INDIVIDU_ULR.c_pays_nationalite%TYPE;
photo               VARCHAR2(100);
persIdGRHUM         INTEGER;

BEGIN

-- recuperation d'un pers_id de GRHUM_CREATEUR
select count(*) into cpt FROM COMPTE WHERE CPT_LOGIN=(SELECT MIN(PARAM_VALUE) FROM GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');
if cpt=1 then
    SELECT PERS_ID into persIdGRHUM FROM COMPTE WHERE CPT_LOGIN=(SELECT MIN(PARAM_VALUE) FROM GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');
else
    persIdGRHUM := NULL;
end if;

-- vérification des chaines de caractères
IF ((INS_D_NAISSANCE IS NOT NULL) AND (LENGTH(TO_CHAR(INS_D_NAISSANCE,'DD/MM/YYYY')) <> 10)) THEN
   RAISE_APPLICATION_ERROR(-20000,'Ins_Individu_Ulr : INS_D_NAISSANCE ('||INS_D_NAISSANCE||') pas au format : JJ/MM/AAAA');
END IF;

IF (INS_D_NAISSANCE IS NULL) THEN
   new_INS_D_NAISSANCE:=TO_DATE('01/01/2001','dd/mm/yyyy');
ELSE
    new_INS_D_NAISSANCE:=INS_D_NAISSANCE;
END IF;

IF ((INS_C_DEPT_NAISSANCE IS NOT NULL) AND (INS_C_PAYS_NAISSANCE IS NULL))
THEN RAISE_APPLICATION_ERROR(-20000,'INS_INDIVIDU_ULR : Il faut un PAYS de NAISSANCE');
END IF;

-- Modif. le 29/10/04 : par défaut le pays de naissance est la France
IF (INS_C_PAYS_NAISSANCE IS NULL) THEN
   cpaysnaissance := '100';
ELSE
    cpaysnaissance := INS_C_PAYS_NAISSANCE;
END IF;

-- Idem pour le pays de nationalite
IF (INS_C_PAYS_NATIONALITE IS NULL) THEN
   cpaysnationalite := '100';
ELSE
    cpaysnationalite := INS_C_PAYS_NATIONALITE;
END IF;

-- Test HOMONYMIE faite par le trigger "trg_br_individu_ulr"

IF (INS_IND_PHOTO IS NULL) THEN
   photo:='N';
ELSE
    photo:=INS_IND_PHOTO;
END IF;

-- Clefs primaires
SELECT SEQ_INDIVIDU_ULR.NEXTVAL INTO noindividu FROM dual;

SELECT SEQ_PERSONNE.NEXTVAL INTO persid FROM dual;


INSERT INTO INDIVIDU_ULR
(
NO_INDIVIDU,
PERS_ID,
NOM_PATRONYMIQUE,
PRENOM,
C_CIVILITE,
NOM_USUEL,
PRENOM2,
D_NAISSANCE,
VILLE_DE_NAISSANCE,
C_DEPT_NAISSANCE,
C_PAYS_NAISSANCE,
C_PAYS_NATIONALITE,
D_NATURALISATION,
D_DECES,
IND_C_SIT_MILITAIRE,
IND_C_SITUATION_FAMILLE,
IND_NO_INSEE,
IND_CLE_INSEE,
IND_QUALITE,
IND_PHOTO,
IND_ACTIVITE,
IND_ORIGINE,
D_CREATION,
D_MODIFICATION,
TEM_SS_DIPLOME,
TEM_VALIDE,
IND_AGENDA,
LANGUE_PREF,
LISTE_ROUGE,
NOM_AFFICHAGE,
PRENOM_AFFICHAGE,
PERS_ID_CREATION,
PERS_ID_MODIFICATION
)
VALUES
(
noindividu,
persid,
UPPER(LTRIM(RTRIM(INS_NOM_PATRONYMIQUE))),
UPPER(LTRIM(RTRIM(INS_PRENOM))),
INS_C_CIVILITE,
UPPER(LTRIM(RTRIM(INS_NOM_USUEL))),
UPPER(LTRIM(RTRIM(INS_PRENOM2))),
new_INS_D_NAISSANCE,
INS_VILLE_DE_NAISSANCE,
INS_C_DEPT_NAISSANCE,
cpaysnaissance,
cpaysnationalite,
INS_D_NATURALISATION,
INS_D_DECES,
INS_IND_C_SIT_MILITAIRE,
INS_IND_C_SITUATION_FAMILLE,
INS_IND_NO_INSEE,
INS_IND_CLE_INSEE,
INS_IND_QUALITE,
photo,
INS_IND_ACTIVITE,
INS_IND_ORIGINE,
SYSDATE,
SYSDATE,
'O',
'O',
'N',
'fr',    -- langue_pref
'N',    -- liste_rouge
LTRIM(RTRIM(INS_NOM_USUEL)),       -- Nom d'affichage
LTRIM(RTRIM(INS_PRENOM)),            -- Prenom d'affichage
persIdGRHUM,
persIdGRHUM
);

END;
/



--
-- TRG_BR_INDIVIDU_ULR  (Trigger)
--
-- Rem : suppression du test d'homonymie effectué dans le FwkCktlPersonne
--
-- TRG_BR_INDIVIDU_ULR  (Trigger) 
--
CREATE OR REPLACE TRIGGER GRHUM.TRG_BR_INDIVIDU_ULR
BEFORE INSERT OR UPDATE ON GRHUM.INDIVIDU_ULR FOR EACH ROW
-- Trigger d'homonymie et de MàJ des attributs SupAnn v1
-- Auteur : Cocktail
-- creation : 04/10/2006
-- modification : 17/02/2012
-- suppression du test d'homonymie effectué dans le FwkCktlPersonne
-- test de l'existence de la paramKey='USE_PEDA'
-- si eduPersonAffiliation = student ET alum alors on garde juste student
-- prise en compte du parametre GRHUM_PEDA pour initialisation de l'année scolaire
-- prise en compte d'un nouveau code de résultat pour les étudiants (res_code = 'W')
-- nouvelle categorie de personnel : les invites ont eduPersonPrimaryAffiliation = affiliate
-- gestion de la période de transistion des inscriptions administratives des étudiants
-- Trigger de mise à jour des valeurs des attributs SupAnn du LDAP :
-- prise en compte des attributs orgaCode et orgaLibelle
-- prise en compte des Vacataires
-- eduPersonPrimaryAffiliation : INDIVIDU_ULR.categorie_princ
-- eduPersonAffiliation : enregistrement(s) de SUPANN_REPART_CATEGORIE par no_individu
DECLARE

cpt    	 	   	 	    INTEGER;
cpt2                    INTEGER;
localPersId 		    INTEGER;

anneeScol				NUMBER;
isEtudiant				NUMBER;
isAncienEtudiant		NUMBER;
isResearcher			NUMBER;
isPersonnel				NUMBER;
isEnseignant			NUMBER;
isVacataire				NUMBER;
isInvite				NUMBER;
isMember				NUMBER;

dDebPeriode             DATE;
dFinPeriode             DATE;

usePeda                 GRHUM_PARAMETRES.PARAM_VALUE%TYPE;
new_nom_clean		    INDIVIDU_ULR.nom_usuel%TYPE;
new_prenom_clean	    INDIVIDU_ULR.prenom%TYPE;
orgaLibelle             INDIVIDU_ULR.orga_libelle%TYPE;


BEGIN

     -- Remplissage des attributs d'affichage 
     IF (:NEW.NOM_AFFICHAGE IS NULL) THEN
        :NEW.NOM_AFFICHAGE := :NEW.NOM_USUEL;
     END IF;
     IF (:NEW.NOM_PATRONYMIQUE_AFFICHAGE IS NULL) THEN
         :NEW.NOM_PATRONYMIQUE_AFFICHAGE := :NEW.NOM_PATRONYMIQUE;
     END IF;
     IF (:NEW.PRENOM_AFFICHAGE IS NULL) THEN
         :NEW.PRENOM_AFFICHAGE := :NEW.PRENOM;
     END IF;

     -- Nettoyage des noms et prénom
     :NEW.NOM_USUEL := GRHUM.NETTOYER_LIBELLE(:NEW.NOM_USUEL);
     :NEW.NOM_PATRONYMIQUE := GRHUM.NETTOYER_LIBELLE(:NEW.NOM_PATRONYMIQUE);
     :NEW.PRENOM := GRHUM.NETTOYER_LIBELLE(:NEW.PRENOM);

	 -- ****************************************************************************
	 --  Mise à jour des attributs SupAnn en fonction de la catégorie de l'individu 
	 -- ****************************************************************************
     -- test si Connexion avec la Scolarité
     usePeda := 'NON';
     SELECT COUNT(*) INTO cpt FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_PEDA';
     IF (cpt != 0) THEN
        SELECT PARAM_VALUE INTO usePeda FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_PEDA';
        IF (usePeda = 'OUI') THEN
            -- recuperation de l'année scolaire de référence pour les Inscriptions Administratives
            SELECT TO_NUMBER(param_value) INTO anneeScol FROM GARNUCHE.GARNUCHE_PARAMETRES WHERE param_key='GARNUCHE_ANNEE_REFERENCE';
        ELSE
            anneeScol := 2000;
        END IF;
     END IF;
	 
     -- définition de la période de transition
     dDebPeriode := to_date('15/06/'||to_char(anneeScol),'dd/mm/yyyy');
     dFinPeriode := to_date('30/09/'||to_char(anneeScol),'dd/mm/yyyy');
	 
     -- Etudiant actuel
     -- étudiant inscrit administrativement pour l'année courante 
	 SELECT COUNT(*) INTO cpt FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE fann_key = anneeScol AND no_individu = :NEW.no_individu
     and (res_code is null or res_code not in ('D','E','Z','H','1')) and idipl_type_inscription != 9;
     -- OU étudiant inscrit administrativement l'année précédente jusqu'à la date de fin de la période de transition
     SELECT COUNT(*) INTO cpt2 FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE fann_key = anneeScol-1 AND no_individu = :NEW.no_individu
     and (res_code is null or res_code not in ('D','E','Z','H','1')) and idipl_type_inscription != 9
     and to_date(to_char(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy') between dDebPeriode and dFinPeriode;
	 IF (cpt = 0 AND cpt2 = 0) THEN
	 	isEtudiant := 0;
	 ELSE
		isEtudiant := 1;
	 END IF;

	 -- Ancien étudiant  
	 SELECT COUNT(*) INTO cpt FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE fann_key != anneeScol AND no_individu = :NEW.no_individu;
	 IF cpt = 0 THEN
		isAncienEtudiant := 0;
	 ELSE
		isAncienEtudiant := 1;
	 END IF;

     -- Pour le cas des étudiants actuels et anciens, on garde uniquement le statut 'student'
     IF (isAncienEtudiant = 1 and isEtudiant = 1) THEN
        isAncienEtudiant := 0;
     END IF;
     
	 -- Chercheurs
	 -- Les groupes des Doctorants (c_structure='1431') et des HDR (c_structure=''28576) sont mis à jour tous les soirs par GRHUM.Maj_Grp_Etudiants_Ecole_Doct() 
 	 SELECT COUNT(*) INTO cpt FROM REPART_STRUCTURE WHERE c_structure IN ('1431','28576') AND pers_id = :NEW.pers_id;
	 IF cpt = 0 THEN
		isResearcher := 0;
	 ELSE
		isResearcher := 1;
	 END IF;		
		
	 -- Non enseignant actuel 
	 SELECT COUNT(*) INTO cpt FROM GRHUM.V_PERSONNEL_ACTUEL_NON_ENS WHERE no_dossier_pers = :NEW.no_individu;
	 IF cpt = 0 THEN
		isPersonnel := 0;
	 ELSE
		isPersonnel := 1;
	 END IF;

	 -- Enseignant actuel 
	 SELECT COUNT(*) INTO cpt FROM GRHUM.V_PERSONNEL_ACTUEL_ENS WHERE no_dossier_pers = :NEW.no_individu;
	 IF cpt = 0 THEN
		isEnseignant := 0;
	 ELSE
		isEnseignant := 1;
	 END IF;

	 -- Vacataire actuel considéré comme Enseignant à cause de TEM_ENSEIGNANT de la table TYPE_CONTRAT_TRAVAIL
	 SELECT COUNT(*) INTO cpt FROM GRHUM.V_PERSONNEL_ACTUEL_VAC WHERE no_dossier_pers = :NEW.no_individu;
	 IF cpt = 0 THEN
		isVacataire := 0;
	 ELSE
		isVacataire := 1;
	 END IF;
     
	 -- Invité actuel considéré comme Enseignant
	 SELECT COUNT(*) INTO cpt FROM GRHUM.V_PERSONNEL_ACTUEL_INVITE WHERE no_dossier_pers = :NEW.no_individu;
	 IF cpt = 0 THEN
		isInvite := 0;
	 ELSE
		isInvite := 1;
	 END IF;     

	 -- Personne de l'établissement
	 IF ( isEnseignant = 1 OR isPersonnel = 1 OR isResearcher = 1 OR isEtudiant = 1 OR isVacataire = 1 OR isInvite = 1) THEN
		isMember := 1;
        :NEW.orga_code := 'EES';
        SELECT param_value into orgaLibelle FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DEFAULT_RNE';
        :NEW.orga_libelle := orgaLibelle;
	 ELSE
		isMember := 0;
        :NEW.orga_code := 'autre';
        :NEW.orga_libelle := 'A préciser';        
	 END IF;
		
	 -- definition de eduPersonPrimaryAffiliation
     -- affiliate
	 IF (isInvite = 1) THEN
		:NEW.categorie_princ := 8;
	 ELSE
    	 -- faculty
    	 IF (isEnseignant = 1) THEN
    		:NEW.categorie_princ := 1;
    	 ELSE
    		-- employee
    		IF (isPersonnel = 1 OR isVacataire = 1) THEN
    		    :NEW.categorie_princ := 6;
    		ELSE
    			--researcher
    			IF (isResearcher = 1) THEN
    			   :NEW.categorie_princ := 7;
    		    ELSE
    			   -- student
    			   IF (isEtudiant = 1) THEN
    			   	  :NEW.categorie_princ := 2;
    			   ELSE
    				  -- alum
    				  IF (isAncienEtudiant = 1) THEN
    				  	 :NEW.categorie_princ := 4;
    				  ELSE
    				  	  -- member
    					  IF (isMember = 1) THEN
    					  	 :NEW.categorie_princ := 5;
    					  ELSE
    					  	 :NEW.categorie_princ := NULL;
    					  END IF; 
    				  END IF;
    			   END IF;
    			END IF;
    		END IF;
    	 END IF;
     END IF;
		
	 -- definition de eduPersonAffiliation
	 DELETE FROM SUPANN_REPART_CATEGORIE WHERE no_individu = :NEW.no_individu;
		
	 IF (isEnseignant = 1) THEN
	 	INSERT INTO SUPANN_REPART_CATEGORIE(no_individu,cat_numero,d_creation,d_modification) VALUES(:NEW.no_individu,1,SYSDATE,SYSDATE);
	 END IF;  
		   
	 IF (isPersonnel = 1 or isVacataire = 1) THEN
		INSERT INTO SUPANN_REPART_CATEGORIE(no_individu,cat_numero,d_creation,d_modification) VALUES(:NEW.no_individu,6,SYSDATE,SYSDATE);
	 END IF;  

	 IF (isResearcher = 1) THEN
	  	INSERT INTO SUPANN_REPART_CATEGORIE(no_individu,cat_numero,d_creation,d_modification) VALUES(:NEW.no_individu,7,SYSDATE,SYSDATE);
	 END IF;  

	 IF (isEtudiant = 1) THEN
	 	INSERT INTO SUPANN_REPART_CATEGORIE(no_individu,cat_numero,d_creation,d_modification) VALUES(:NEW.no_individu,2,SYSDATE,SYSDATE);
	 END IF;  
		   
	 IF (isAncienEtudiant = 1) THEN
	 	INSERT INTO SUPANN_REPART_CATEGORIE(no_individu,cat_numero,d_creation,d_modification) VALUES(:NEW.no_individu,4,SYSDATE,SYSDATE);
	 END IF;
		   
	 IF (isMember = 1) THEN
	 	INSERT INTO SUPANN_REPART_CATEGORIE(no_individu,cat_numero,d_creation,d_modification) VALUES(:NEW.no_individu,5,SYSDATE,SYSDATE);		   
	 END IF;  

	 IF (isInvite = 1) THEN
	 	INSERT INTO SUPANN_REPART_CATEGORIE(no_individu,cat_numero,d_creation,d_modification) VALUES(:NEW.no_individu,8,SYSDATE,SYSDATE);		   
	 END IF;  

	 -- ****************************************************************************
	 --  				FIN attributs SupAnn  
	 -- ****************************************************************************	 

		   
	 -- Lors de l'ajout d'un nouvel INDIVIDU, on vérifie s'il n'existe pas déjà
	 IF INSERTING THEN

	 	 -- On épure les nouveaux noms et prénoms de tout caractère bizarre
	 	 new_nom_clean := Chaine_Claire(:NEW.nom_usuel);
	 	 new_prenom_clean := Chaine_Claire(:NEW.prenom);

		 IF (:NEW.tem_valide = 'N') THEN
		 	RAISE_APPLICATION_ERROR(-20000,'TRIGGER_INDIVIDU : Créer un individu non valide !!!');
		 END IF;
/*		 
		 IF (:NEW.d_naissance IS NULL) THEN

		 	-- TODO : verif new_nom_clean par rapport à Chaine_Claire(nom_usuel)
			-- Faire une vue qui contient les Chaine_Claire(noms) et Chaine_Claire(prénoms) des individus 
		 	-- Vérification sur le nom et le prénom sans les caractères bizarres
   	 	 	SELECT COUNT(*) INTO cpt
		 	FROM INDIVIDU_ULR
   		 	WHERE tem_valide = 'O'
		 	AND nom_usuel = new_nom_clean
		 	AND prenom = new_prenom_clean;

   		 	IF (cpt>0) THEN
	   	 	   RAISE_APPLICATION_ERROR(-20000,'TRIGGER_INDIVIDU : Homonymie pour ' ||new_nom_clean||' '||new_prenom_clean||' ( pers_id = '||:NEW.pers_id||' )');
   		 	END IF;

		 ELSE

		 	-- Vérification sur le nom, le prénom sans les caractères bizarres et la date de naissance
   	 	 	SELECT COUNT(*) INTO cpt
		 	FROM INDIVIDU_ULR
   		 	WHERE tem_valide = 'O'
		 	AND nom_usuel = new_nom_clean
		 	AND prenom = new_prenom_clean
			AND d_naissance = :NEW.d_naissance;
		
   		 	IF (cpt>0) THEN
	   	 	   RAISE_APPLICATION_ERROR(-20000,'TRIGGER_INDIVIDU : Homonymie pour ' ||new_nom_clean||' '||new_prenom_clean||' '||:NEW.d_naissance||' ( pers_id = '||:NEW.pers_id||' )');
   		 	END IF;

		 END IF;
*/
		 -- Bloc utile pour les saveChanges() dans les applis
		 IF (:NEW.pers_id IS NULL) THEN
		 	SELECT SEQ_PERSONNE.NEXTVAL INTO localPersId FROM dual;
		 ELSE
   	   	 	localPersId := :NEW.pers_id;
   		 END IF;
		 :NEW.pers_id := localPersId;

		 -- Ajout dans la table PERSONNE
		 INSERT INTO PERSONNE
		 VALUES (localPersId, :NEW.c_civilite, :NEW.no_individu, :NEW.nom_usuel, :NEW.prenom, :NEW.nom_patronymique);

	 ELSIF UPDATING THEN

 		 IF (:OLD.tem_valide = 'O' AND :NEW.tem_valide = 'N') THEN
		 	RAISE_APPLICATION_ERROR(-20000,'TRIGGER_INDIVIDU : Modifier la validité de l''individu pour le passer invalide !!!');
		 END IF;

	 	 UPDATE PERSONNE SET
		 pers_type=:NEW.c_civilite,
		 pers_ordre=:NEW.no_individu,
		 pers_libelle=:NEW.nom_usuel,
		 pers_lc=:NEW.prenom,
		 pers_nomPtr = :NEW.nom_patronymique
		 WHERE pers_id=:NEW.pers_id;

	 END IF;	 

END;
/



COMMIT;
