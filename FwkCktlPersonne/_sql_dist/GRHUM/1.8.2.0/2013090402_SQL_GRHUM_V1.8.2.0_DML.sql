--
-- Patch DML de GRHUM du 04/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.2.0
-- Date de publication : 04/09/2013
-- Auteur(s) : Alain

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



------------------V20130904.141948__DML_Groupes_Institutionnels_et_MAJ_de_RNE.sql-------------------
grant select on maracuja.bordereau to jefy_paf;
grant execute on maracuja.util to jefy_paf;

DECLARE
    cpt INTEGER;
    
BEGIN


--grant select on maracuja.bordereau to jefy_paf;
--grant execute on maracuja.util to jefy_paf;

select count(*) into cpt from grhum.bac where bac_code = 'TD2A';
if (cpt= 0)
then
	INSERT into grhum.bac values ('TD2A','SCIENCES ET TECHNOLOGIE DU DESIGN ET DES ARTS APPLIQUES', 'TD', 'BAC', 'STD', 2013, sysdate, sysdate, 'O', null);
	INSERT into grhum.bac values ('TI2D','SCIENCES ET TECHNOLOGIE INDUSTRIE ET DEVELOPPEMENT DURABLE', 'TI', 'BAC', 'STI', 2013, sysdate, sysdate, 'O', null);
end if;


-- ---------------------------------------------------
-- série de codes RNE trouvés par le PRES de Bordeaux
-- ---------------------------------------------------

    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '031032XV';
     
     IF cpt <> 0 THEN
        
        UPDATE RNE SET ll_rne = 'BIU TOULOUSE', adresse = '11 RUE DES PUITS CREUSES', code_postal = '31000', d_modification = sysdate, acad_code = '016', ville = 'TOULOUSE'
        WHERE C_RNE = '031032XV';
        
     ELSE
     
        INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('031032XV', 'BIU TOULOUSE', '11 RUE DES PUITS CREUSES', '31000', null, null, null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'BIU TOULOUSE', '016', null, null, 'PU', 'TOULOUSE', null, null);


    END IF;

    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0330401G';
     
     IF cpt <> 0 
     THEN
      
        UPDATE RNE SET ll_rne = 'Ecole primaire Marcel Sembat', adresse = '21 RUE NOUTARY', code_postal = '33130', d_modification = sysdate, acad_code = '004', ville = 'BEGLES'
        WHERE C_RNE = '0330401G';
     
     ELSE     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0330401G', 'Ecole primaire Marcel Sembat', '21 RUE NOUTARY', '33130', null, to_date('18/08/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'BEGLES', null, '21330039500158');

    END IF;
    
--0330280A    Ecole maternelle Louis Pasteur    IMPASSE DE LA MATERNELLE    33270        10/08/1965        18/01/2008    18/01/2008    21330167400114    4    FLOIRAC

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0330280A';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Ecole maternelle Louis Pasteur', lc_rne = 'Ecole maternelle', adresse = 'IMPASSE DE LA MATERNELLE', code_postal = '33270', d_modification = sysdate, acad_code = '004', ville = 'FLOIRAC'
        WHERE C_RNE = '0330280A';
     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0330280A', 'Ecole maternelle Louis Pasteur', 'IMPASSE DE LA MATERNELLE', '33270', null, to_date('10/08/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole maternelle', '004', null, null, 'PU', 'FLOIRAC', null, '21330167400114');

    END IF;
    
-- 0332271P    Ecole primaire Albert Camus    2, RUE VOLTAIRE    33270        13/05/1974        18/01/2008    18/01/2008    21330167400130    4    FLOIRAC

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0332271P';
     
     IF cpt <> 0 THEN

        UPDATE RNE SET ll_rne = 'Ecole primaire Albert Camus', adresse = '2 RUE VOLTAIRE', code_postal = '33270', d_modification = sysdate, acad_code = '004', ville = 'FLOIRAC'
        WHERE C_RNE = '0332271P';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0332271P', 'Ecole primaire Albert Camus', '2 RUE VOLTAIRE', '33270', null, to_date('13/05/1974','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'FLOIRAC', null, '21330167400130');
        
     END IF;        
        
        
-- 0332572S    Ecole primaire Louis Aragon    RUE ERIC SATIE    33270        01/09/1982        18/01/2008    18/01/2008    21330167400122    4    FLOIRAC
     
        
  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0332572S';
     
     IF cpt <> 0 THEN

        UPDATE RNE SET ll_rne = 'Ecole primaire Louis Aragon', adresse = 'RUE ERIC SATIE', code_postal = '33270', d_modification = sysdate, acad_code = '004', ville = 'FLOIRAC'
        WHERE C_RNE = '0332572S';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0332572S', 'Ecole primaire Louis Aragon', 'RUE ERIC SATIE', '33270', null, to_date('01/09/1982','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'FLOIRAC', null, '21330167400122');    
        
     END IF;
    
-- 0330237D    Ecole maternelle Le Point du Jour    2, RUE BARILLET DESCHAMPS    33300        10/08/1965        18/01/2008    18/01/2008    21330063500371    4    BORDEAUX
       
  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0330237D';
     
     IF cpt <> 0 THEN
     
        UPDATE RNE SET ll_rne = 'Ecole maternelle Le Point du Jour', lc_rne = 'Ecole maternelle', adresse = '2 RUE BARILLET DESCHAMPS', code_postal = '33300', d_modification = sysdate, acad_code = '004', ville = 'BORDEAUX'
        WHERE C_RNE = '0330237D';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0330237D', 'Ecole maternelle Le Point du Jour', '2 RUE BARILLET DESCHAMPS', '33300', null, to_date('10/08/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole maternelle', '004', null, null, 'PU', 'BORDEAUX', null, '21330063500371');         
    END IF;        
    
--  0330285F    Ecole maternelle    12, RUE DU BOURG    33360        10/08/1965        18/01/2008    18/01/2008    21330234200067    4    LATRESNE
  
    
   SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0330285F';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Ecole maternelle', lc_rne = 'Ecole maternelle', adresse = '12, RUE DU BOURG', code_postal = '33360', d_modification = sysdate, acad_code = '004', ville = 'LATRESNE'
        WHERE C_RNE = '0330285F';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0330285F', 'Ecole maternelle', '12, RUE DU BOURG', '33360', null, to_date('10/08/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole maternelle', '004', null, null, 'PU', 'LATRESNE', null, '21330234200067');          
    
    END IF;

-- 0332143A    Ecole primaire    25BIS, ROUTE DES ECOLES    33370        20/06/1972        18/01/2008    18/01/2008    21330165800026    4    FARGUES ST HILAIRE
  
   SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0332143A';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Ecole primaire', lc_rne = 'Ecole primaire', adresse = '25BIS ROUTE DES ECOLES', code_postal = '33370', d_modification = sysdate, acad_code = '004', ville = 'FARGUES ST HILAIRE'
        WHERE C_RNE = '0332143A';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0332143A', 'Ecole primaire', '25BIS ROUTE DES ECOLES', '33370', null, to_date('20/06/1972','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'FARGUES ST HILAIRE', null, '21330165800026'); 
    
    END IF;    
    
-- 0332660M    Ecole primaire    32, LE BOURG    33620        01/09/1984        18/01/2008    18/01/2008    21330123700045    4    CEZAC
    
    
    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0332660M';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Ecole primaire', lc_rne = 'Ecole primaire', adresse = '32, LE BOURG', code_postal = '33620', d_modification = sysdate, acad_code = '004', ville = 'CEZAC'
        WHERE C_RNE = '0332660M';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0332660M', 'Ecole primaire', '32, LE BOURG', '33620', null, to_date('01/09/1984','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'CEZAC', null, '21330123700045');
    
     END IF;
    
--  0331030R    Ecole primaire    AVENUE DU GENERAL LECLERC    33640        10/08/1965        18/01/2008    18/01/2008    21330334000045    4    PORTETS
  
   SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0331030R';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Ecole primaire', lc_rne = 'Ecole primaire', adresse = 'AVENUE DU GENERAL LECLERC', code_postal = '33640', d_modification = sysdate, acad_code = '004', ville = 'PORTETS'
        WHERE C_RNE = '0331030R';

     ELSE
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0331030R', 'Ecole primaire', 'AVENUE DU GENERAL LECLERC', '33640', null, to_date('10/08/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'PORTETS', null, '21330334000045'); 
    
    END IF;    
    
-- 0331243X    Ecole primaire         33710        10/08/1965        18/01/2008    18/01/2008    21330475100026    4    ST SEURIN DE BOURG

    SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0331243X';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Ecole élémentaire publique', lc_rne = 'Ecole primaire', adresse = 'MARCHAIS', code_postal = '33710', d_modification = sysdate, acad_code = '004', ville = 'ST SEURIN DE BOURG'
        WHERE C_RNE = '0331243X';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0331243X', 'Ecole élémentaire publique', 'MARCHAIS', '33710', null, to_date('10/08/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'ST SEURIN DE BOURG', null, '21330475100026');
    
    END IF; 
 
-- 0330323X    Ecole primaire Jacques Brel    RUE NUNGESSER ET COLI    33810        10/08/1965        18/01/2008    18/01/2008    21330004900037    4    AMBES
   
   SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0330323X';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Ecole primaire Jacques Brel', lc_rne = 'Ecole primaire', adresse = 'RUE NUNGESSER ET COLI', code_postal = '33640', d_modification = sysdate, acad_code = '004', ville = 'AMBES'
        WHERE C_RNE = '0330323X';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0330323X', 'Ecole primaire Jacques Brel', 'RUE NUNGESSER ET COLI', '33810', null, to_date('10/08/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '004', null, null, 'PU', 'AMBES', null, '21330004900037');
    
    END IF;
        
-- 0642031K    IUT ANGLET - Université de PAU    2 ALLEE DU PARC MONTAURY    64600        01/09/2007        18/01/2008    18/01/2008    19640251500189    4    ANGLET
   
   SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0642031K';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'IUT ANGLET - Université de PAU', lc_rne = 'IUT ANGLET', adresse = '2 ALLEE DU PARC MONTAURY', code_postal = '64600', d_modification = sysdate, acad_code = '004', ville = 'ANGLET'
        WHERE C_RNE = '0642031K';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0642031K', 'IUT ANGLET - Université de PAU', '2 ALLEE DU PARC MONTAURY', '64600', null, to_date('01/09/2007','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'IUT ANGLET', '004', null, null, 'PU', 'ANGLET', null, null);
    
    END IF;
-- 0673021V    Université de Strasbourg        67000        01/09/2008        12/09/2008    24/04/2009        15    STRASBOURG
-- http://www.lesecoles.net/etablissement/67-strasbourg/0673021v-universite-de-strasbourg
  
   SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0673021V';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'Université de Strasbourg', lc_rne = 'Univ Strasbourg', adresse = '4 Rue Blaise Pascal', code_postal = '67081', d_modification = sysdate, acad_code = '004', ville = 'STRASBOURG'
        WHERE C_RNE = '0673021V';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0673021V', 'Université de Strasbourg', '4 Rue Blaise Pascal', '67081', null, to_date('01/09/2008','DD/MM/YYYY'), null, to_date('12/09/2008','DD/MM/YYYY'), to_date('24/04/2009','DD/MM/YYYY'),
                 'Univ Strasbourg', '015', null, null, 'PU', 'STRASBOURG', null, '13000545700010'); 
    
    END IF;
     
-- 075307XV    INSTITUT DE RECHERCHE    213 RUE LA FAYETTE    75010                18/01/2008    18/01/2008        1    PARIS

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '075307XV';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'INSTITUT DE RECHERCHE', lc_rne = 'INST. DE RECHERCHE', adresse = '213 RUE LA FAYETTE', code_postal = '75010', d_modification = sysdate, acad_code = '001', ville = 'PARIS'
        WHERE C_RNE = '075307XV';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('075307XV', 'INSTITUT DE RECHERCHE', '213 RUE LA FAYETTE', '75010', null, null, null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'INST. DE RECHERCHE', '001', null, null, 'PU', 'PARIS', null, null);
    end if; 
 
-- 075325XB    INSTITUT D'ETUDES POLITIQUES    27 RUE SAINT GUILLAUME    75337                18/01/2008    18/01/2008        1    PARIS

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '075325XB';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'INSTITUT D''ETUDES POLITIQUES', lc_rne = 'IEP', adresse = '27 RUE SAINT GUILLAUME', code_postal = '75337', d_modification = sysdate, acad_code = '001', ville = 'PARIS'
        WHERE C_RNE = '075325XB';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('075325XB', 'INSTITUT D''ETUDES POLITIQUES', '27 RUE SAINT GUILLAUME', '75337', null, null, null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'IEP', '001', null, null, 'PU', 'PARIS', null, null);
    END IF; 
 
 
-- 075276XA    INSTITUT NATIONAL DE LA SANTE ET DE LA RECHERCHE MEDICALE (INSERM)    101 RUE DE TOLBIAC    75654                18/01/2008    18/01/2008        1    PARIS

 SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '075276XA';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'INSTITUT NATIONAL DE LA SANTE ET DE LA RECH MEDICALE (INSERM)', lc_rne = 'INSERM', adresse = '101 RUE DE TOLBIAC', code_postal = '75654', d_modification = sysdate, acad_code = '001', ville = 'PARIS'
        WHERE C_RNE = '075276XA';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('075276XA', 'INSTITUT NATIONAL DE LA SANTE ET DE LA RECH MEDICALE (INSERM)', '101 RUE DE TOLBIAC', '75654', null, null, null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'INSERM', '001', null, null, 'PU', 'PARIS', null, null);
 
    END IF; 
 
 
-- 0780486E    EC.REGIONALE DU PREMIER DEGRE    36 QUAI DE LA REPUBLIQUE    78700        01/05/1965        18/01/2008    18/01/2008    19780486700012    25    CONFLANS STE HONORINE

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0780486E';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'EC.REGIONALE DU PREMIER DEGRE', lc_rne = 'EC.REGIONALE', adresse = '36 QUAI DE LA REPUBLIQUE', code_postal = '78700', d_modification = sysdate, acad_code = '025', ville = 'CONFLANS STE HONORINE'
        WHERE C_RNE = '0780486E';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0780486E', 'EC.REGIONALE DU PREMIER DEGRE', '36 QUAI DE LA REPUBLIQUE', '78700', null, to_date('01/05/1965','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'EC.REGIONALE', '025', null, null, 'PU', 'CONFLANS STE HONORINE', null, '19780486700012');
    END IF; 
 
 
-- 0860077S    CENTRE PEDAGOGIQUE REGIONAL    6 RUE SAINTE CATHERINE    86034        08/10/1966    01/09/1991    18/01/2008    18/01/2008    17860430200111    13    POITIERS

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0860077S';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'CENTRE PEDAGOGIQUE REGIONAL', lc_rne = 'CENTRE PEDAGOGIQUE', adresse = '6 RUE SAINTE CATHERINE', code_postal = '86034', d_modification = sysdate, acad_code = '013', ville = 'POITIERS'
        WHERE C_RNE = '0860077S';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0860077S', 'CENTRE PEDAGOGIQUE REGIONAL', '6 RUE SAINTE CATHERINE', '86034', null, to_date('08/10/1966','DD/MM/YYYY'), to_date('01/09/1991','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'CENTRE PEDAGOGIQUE', '013', null, null, 'PU', 'POITIERS', null, '17860430200111');

    END IF;

 
-- 0922631K    CENTRE INTERNATIONAL D ETUDES    12 AVENUE LEONARD DE VINCI    92400        24/09/2007        18/01/2008    18/01/2008        25    COURBEVOIE

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0922631K';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'CENTRE INTERNATIONAL D''ETUDES', lc_rne = 'CIE', adresse = '12 AVENUE LEONARD DE VINCI', code_postal = '92400', d_modification = sysdate, acad_code = '025', ville = 'COURBEVOIE'
        WHERE C_RNE = '0922631K';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('0922631K', 'CENTRE INTERNATIONAL D''ETUDES', '12 AVENUE LEONARD DE VINCI', '92400', null, to_date('24/09/2007','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'CIE', '025', null, null, 'PU', 'COURBEVOIE', null, null);
    END IF; 
 
 
-- 9710126K    Ecole primaire Léon Feix    1 RUE RENE WACHTER    97110        23/01/1970        18/01/2008    18/01/2008    21971120700056    32    POINTE A PITRE
  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '9710126K';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'CENTRE INTERNATIONAL D''ETUDES', lc_rne = 'CIE', adresse = '1 RUE RENE WACHTER', code_postal = '97110', d_modification = sysdate, acad_code = '032', ville = 'POINTE A PITRE'
        WHERE C_RNE = '9710126K';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('9710126K', 'Ecole primaire Léon Feix', '1 RUE RENE WACHTER', '97110', null, to_date('23/01/1970','DD/MM/YYYY'), null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'Ecole primaire', '032', null, null, 'PU', 'POINTE A PITRE', null, '21971120700056');
    END IF;
 
-- 987001XU    MINISTERE DE LA CULTURE, DE        98714                18/01/2008    18/01/2008        41    PAPEETE

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '987001XU';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'MINISTERE DE LA CULTURE DE PAPEETE', lc_rne = 'MIN CULTURE PAPEETE', adresse = null, code_postal = '98714', d_modification = sysdate, acad_code = '041', ville = 'PAPEETE'
        WHERE C_RNE = '987001XU';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('987001XU', 'MINISTERE DE LA CULTURE DE PAPEETE', null, '98714', null, null, null, to_date('18/01/2008','DD/MM/YYYY'), to_date('18/01/2008','DD/MM/YYYY'),
                 'MIN CULTURE PAPEETE', '041', null, null, 'PU', 'PAPEETE', null, null);
    END if;        
        
-- 075307XV    INSTITUT DE RECHERCHE POUR LE DEVELOPPEMENT (IRD)    44 BVD DE DUNKERQUE CS 90009    13572                18/01/2008    26/08/2013        1    MARSEILLE
       
  
  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '075307XV';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'INSTITUT DE RECHERCHE POUR LE DEVELOPPEMENT (IRD)', lc_rne = 'IRD', adresse = '44 BVD DE DUNKERQUE CS 90009', code_postal = '13572', d_modification = sysdate, acad_code = '002', ville = 'MARSEILLE'
        WHERE C_RNE = '075307XV';

     ELSE
     
     INSERT INTO GRHUM.RNE(C_RNE, LL_RNE, ADRESSE, CODE_POSTAL, C_RNE_PERE, D_DEB_VAL, D_FIN_VAL, D_CREATION, D_MODIFICATION,
                                 LC_RNE, ACAD_CODE, TETAB_CODE, ETAB_ENQUETE, ETAB_STATUT, VILLE, ADR_ORDRE, SIRET)
         VALUES('075307XV', 'INSTITUT DE RECHERCHE POUR LE DEVELOPPEMENT (IRD)', '44 BVD DE DUNKERQUE CS 90009', '13572', null, null, null, to_date('18/01/2008','DD/MM/YYYY'), to_date('26/08/2013','DD/MM/YYYY'),
                 'IRD', '002', null, null, 'PU', 'MARSEILLE', null, null);
    END IF;
  
  
-- ---------------------------------------
--  Codes RNE fournis par l'UTBM
-- ---------------------------------------

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0841117H';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'LYCEE VAISON LA ROMAINE', lc_rne = 'LYCEE', adresse = 'Avenue Marcel Pagnol', code_postal = '84110', d_modification = sysdate, acad_code = '002', ville = 'VAISON-LA-ROMAINE'
        WHERE C_RNE = '0841117H';

     ELSE

  insert into grhum.RNE (c_rne, ll_rne, lc_rne, acad_code, etab_statut, adresse, code_postal, ville, d_creation, d_modification)
    values ( '0841117H','LYCEE VAISON LA ROMAINE', 'LYCEE','002','PU','Avenue Marcel Pagnol', '84110', 'VAISON-LA-ROMAINE', SYSDATE, SYSDATE);
    
    end if;    
    
  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0261260D';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'IUT DE VALENCE', lc_rne = 'IUT DE VALENCE', adresse = '51 rue Barthélemy de Laffemas', 
        code_postal = '26901', d_modification = sysdate, acad_code = '008', ville = 'VALENCE'
        WHERE C_RNE = '0261260D';

     ELSE
 
  insert into grhum.RNE (c_rne, ll_rne, lc_rne, acad_code, etab_statut, adresse, code_postal, ville, d_creation, d_modification)
    values ( '0261260D','IUT DE VALENCE', 'IUT DE VALENDE','008','PU','51 rue Barthélemy de Laffemas', '26901', 'VALENCE', SYSDATE, SYSDATE);

    end if;
    



-- ---------------------------------------
--  Codes RNE fournis par l'INSA Rennes
-- ---------------------------------------

-- Lycée Clemenceau
--http://www.education.gouv.fr/annuaire/51-marne/reims/lycee/lycee-clemenceau.html

update grhum.rne
	set adresse = '46 avenue Georges Clemenceau',
		code_postal = '51100',
		ville = 'REIMS'
  where c_rne='0510031G'; 
        
-- Lycée la martiniére Montplaisir
-- http://www.education.gouv.fr/annuaire/69-rhone/lyon-8e/lycee/lycee-la-martiniere-monplaisir.html

update grhum.rne
	set adresse = '41 rue Antoine Lumière',
		code_postal='69008',
		ville='LYON 8e'
  where c_rne='0692866R';
 
 

-- lycee jean perrin
-- http://www.education.gouv.fr/annuaire/95-val-d-oise/saint-ouen-l-aumone/lycee/lycee-technologique-jean-perrin.html

update grhum.rne
	set adresse = '2 rue des Egalisses',
		code_postal='95310',
		ville=upper('Saint-Ouen-l''Aumône')
  where c_rne='0951104J';
 
 
-- ---------------------------------------
--  Codes RNE fournis par l'UPPA
-- ---------------------------------------
 update grhum.rne set ville='LIBOURNE' where c_rne = '0332937N';
 
-- ------------------------------------------
--  Codes RNE fournis par l'ENSI de Bourges
-- ------------------------------------------
 
 update grhum.rne set ll_rne = 'LYCEE GENERAL ET TECHNOLOGIQUE PIERRE-GILLES DE GENNES - ENCPB', adresse = '11 RUE PIRANDELLO', code_postal = '75013', ville = 'PARIS XIII' where c_rne = '0750685M';
 
        
-- -------------------------------
-- Ajout d'une catégorie d'emploi
-- -------------------------------
  SELECT count(*) INTO cpt
      FROM GRHUM.CATEGORIE_EMPLOI
     WHERE c_categorie_emploi = 'BIB ASS';
     
     IF cpt <> 0 THEN
         DELETE 
           FROM GRHUM.CATEGORIE_EMPLOI
    WHERE c_categorie_emploi = 'BIB ASS';
     END IF;

  INSERT INTO GRHUM.CATEGORIE_EMPLOI (c_categorie_emploi, lc_categorie_emploi, ll_categorie_emploi, d_creation, d_modification, tem_budget_propre, tem_surnombre, tem_past)
    VALUES ('BIB ASS', 'Biblio. ass. spéc.', 'Bibliothécaire Assistant Spécialisé', SYSDATE, SYSDATE, 'N', 'N', 'N');
    
    
-- -------------------------------
-- Ajout d'une nature de budget
-- -------------------------------   
   SELECT count(*) INTO cpt
      FROM GRHUM.NATURE_BUDGET
     WHERE lc_budget = 'Titre 3 - Dot. Etab.' and ll_budget = 'Titre 3 - Dotation Etablissement';
     
     IF cpt <> 0 THEN
         DELETE 
           FROM GRHUM.NATURE_BUDGET
    WHERE lc_budget = 'Titre 3 - Dot. Etab.' and ll_budget = 'Titre 3 - Dotation Etablissement';
     END IF; 
   
   
   INSERT INTO GRHUM.NATURE_BUDGET (c_budget, lc_budget, ll_budget, tem_budget_etat, tem_flux_moyens, d_creation, d_modification)
    VALUES ('E', 'Titre 3 - Dot. Etab.', 'Titre 3 - Dotation Etablissement', 'N', null, to_date('11/07/2006','DD/MM/YYYY'), to_date('10/12/2010','DD/MM/YYYY'));






END;
/



--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.2.0',to_date('04/09/2013','DD/MM/YYYY'),sysdate,'MAJ de RNE, MAJ du Trigger Compte, Groupes Institutionnels, MAJ GARNUCHE.VERIF_CODE_INSEE');
COMMIT;
