--
-- Patch DDL de GRHUM du 04/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.2.0
-- Date de publication : 04/09/2013
-- Auteur(s) : Alain

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.1.1';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.2.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.2.0 a deja ete passe !');
    end if;

end;
/

------------------V20130904.141924__DDL_Groupes_Institutionnels_et_MAJ_de_RNE.sql-------------------
DECLARE
        cpt     INTEGER;
--        i       INTEGER;
        persIdCreateur  INTEGER;
        cStructure      VARCHAR2(10);
        one_structure   VARCHAR2(10);
  ----------
  -- DÈclaration du curseur pour balayer toutes les structures et les groupes cibles.
  -- il faudrait n'avoir que des structures !!!
  CURSOR curseur_structure IS
  SELECT s.c_structure FROM GRHUM.STRUCTURE_ULR s
 WHERE s.c_type_structure = 'E'
    OR s.c_type_structure = 'C'
    OR s.c_type_structure = 'CS'
    OR s.c_type_structure = 'ES'
UNION
SELECT tp.c_structure from GRHUM.REPART_TYPE_GROUPE tp
 WHERE tp.tgrp_code = 'S'
  --OR tp.tgrp_code = 'CS'
  --OR tp.tgrp_code = 'DE'
    OR tp.tgrp_code = 'ED'
    OR tp.tgrp_code = 'LA'
ORDER BY 1;
  ----------
BEGIN
  -- persIdCreation
   SELECT PERS_ID into persIdCreateur
                FROM GRHUM.COMPTE
        WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES
                                                WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));
--   i := 0;
  OPEN  curseur_structure;
  LOOP
                FETCH curseur_structure INTO one_structure;
                EXIT WHEN curseur_structure%NOTFOUND;
                -- i := i + 1;
                -- dbms_output.put_line('Iteration '||i||' pour c_structure = '||one_structure);
                SELECT count(*) into cpt
                        FROM GRHUM.REPART_TYPE_GROUPE tp
                  WHERE tp.c_structure = one_structure and tp.tgrp_code = 'G';
                IF cpt = 0 THEN
                -- dbms_output.put_line('Insert G');
                   INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, D_CREATION, PERS_ID_CREATION, PERS_ID_MODIFICATION, tgrp_code, D_MODIFICATION)
                              VALUES ( one_structure, SYSDATE, persIdCreateur, persIdCreateur, 'G', SYSDATE);
                END IF;
                SELECT count(*) into cpt
                                FROM GRHUM.REPART_TYPE_GROUPE tp
                  WHERE tp.c_structure = one_structure and tp.tgrp_code = 'GI';
                IF cpt = 0 THEN
                -- dbms_output.put_line('Insert GI');
                  INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, D_CREATION, PERS_ID_CREATION, PERS_ID_MODIFICATION, tgrp_code, D_MODIFICATION)
                              VALUES ( one_structure, SYSDATE, persIdCreateur, persIdCreateur, 'GI', SYSDATE);
                END IF;
  END LOOP;
END;
/



--------------------------------------------------------
--  Fichier créé - lundi-septembre-02-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger TRG_COMPTE
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER GRHUM.TRG_COMPTE 
BEFORE INSERT OR UPDATE ON GRHUM.COMPTE FOR EACH ROW
-- CRI
-- creation : 20/01/2004
-- modification : 13/05/2009
-- modification : 01/07/2013 prise en compte des domaines secondaires pour le VLAN P
-- 1er declencheur, de niveau ligne, qui n'interroge plus la table mutante
-- a la place, il stocke dans la table temporaire les donnees inserees
-- Creation d'une table temporaire vide de meme structure que COMPTE

DECLARE
   cpt              		INTEGER;
   cpt_dom_second       INTEGER;
   newlogin         		COMPTE.cpt_login%TYPE;
   newemail         		COMPTE.cpt_email%TYPE;
   newdomaine       		COMPTE.cpt_domaine%TYPE;
   domaine_princ    		COMPTE.CPT_DOMAINE%TYPE;
   vlan_admin       		VLANS.C_VLAN%type;
   vlan_recherche   		VLANS.C_VLAN%type;
   vlan_etudiant    		VLANS.C_VLAN%type;
   vlan_externe     		VLANS.C_VLAN%type;
   
   vlan_admin_domaine 		VLANS.DOMAINE%TYPE;
   vlan_recherche_domaine 	VLANS.DOMAINE%TYPE;
   vlan_etudiant_domaine 	VLANS.DOMAINE%TYPE;

   nb               integer;
   str_domaine      varchar2(2000);
   list_dom_sec     varchar2(2000);
   chaine           varchar2(2000);

	 valeur_test		NUMBER(1);

BEGIN
   cpt := 0;
   newlogin := NULL;
   newemail := NULL;
   newdomaine := NULL;

   -- 03/10/2005 pamametrage des codes des VLANS
   select param_value into vlan_admin from grhum_parametres where param_key = 'GRHUM_VLAN_ADMIN';
   select param_value into vlan_recherche from grhum_parametres where param_key = 'GRHUM_VLAN_RECHERCHE';
   select param_value into vlan_etudiant from grhum_parametres where param_key = 'GRHUM_VLAN_ETUD';
   select param_value into vlan_externe from grhum_parametres where param_key = 'GRHUM_VLAN_EXTERNE';
    -- 09/07/2013 pamametrage des domaines des VLANS internes
   select domaine into vlan_admin_domaine from GRHUM.vlans where c_vlan = vlan_admin;
   select domaine into vlan_recherche_domaine from GRHUM.vlans where c_vlan = vlan_recherche;
   select domaine into vlan_etudiant_domaine from GRHUM.vlans where c_vlan = vlan_etudiant;

   -- contraintes d'ingegrites entre le VLAN (not null) et le domaine
   SELECT COUNT(*) INTO cpt
   FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';

   IF (cpt <> 0) THEN

      SELECT param_value INTO domaine_princ
        FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';

      Select count(*) into cpt_dom_second from GRHUM_PARAMETRES WHERE param_key='org.cocktail.grhum.compte.domainessecondaires';
      if (cpt_dom_second <> 0) then
        SELECT param_value INTO list_dom_sec
          FROM GRHUM_PARAMETRES WHERE param_key='org.cocktail.grhum.compte.domainessecondaires';
      end if;
  
	IF (:NEW.cpt_vlan = vlan_admin) THEN
      -- regle d'integrite entre le VLAN P et la liste des domaines (principal + secondaires)
      IF (list_dom_sec <> 'univ.fr' AND cpt_dom_second <> 0)
  --  IF (list_dom_sec != 'univ.fr' AND cpt_dom_second <> 0)
      THEN
         if substr(list_dom_sec,length(ltrim(rtrim(list_dom_sec)))-1,1) = ';'
         then
            chaine := ltrim(rtrim(list_dom_sec));
         else
            chaine := ltrim(rtrim(list_dom_sec))||';';
         end if;
         
         chaine := ltrim(rtrim(chaine))||ltrim(rtrim(domaine_princ))||';';
         -- la valeur de 1 est pour lever une erreur
		 valeur_test := 1;	
		
         select length(ltrim(rtrim(chaine)))-length(replace(ltrim(rtrim(chaine)),';',''))+1 into nb from dual;

         for j in 0..nb-1 loop
            if j=0 then
               str_domaine := substr(chaine,1,instr(chaine,';',1,1)-1);
            else
               str_domaine := substr(chaine,instr(chaine,';',1,j)+1, instr(chaine,';',1,j+1) - instr(chaine,';',1,j)-1);
            end if;
            if ( :NEW.cpt_vlan = vlan_admin AND :NEW.cpt_domaine = str_domaine ) then
            	valeur_test := 0;
            	/*
            	if ( str_domaine == domaine_princ ) then
           			RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine principal '||domaine_princ||'.');
           		else
           			RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine secondaire '||str_domaine||'.');
           		*/
           	end if;
           	
            --dbms_output.put_line(str_domaine);
         end loop;
         if (valeur_test <> 0) then
           		RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et les domaines principal et secondaire(s) ');
         end if;
      ELSE
      	if ( :NEW.cpt_vlan = vlan_admin AND :NEW.cpt_domaine <> domaine_princ ) then
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine principal '||domaine_princ||'.');
      	end if;
      END IF;
	END IF;

  

        -- regle d'integrite entre le VLAN R et le domaine associé
      IF ( :NEW.cpt_vlan = vlan_recherche AND :NEW.cpt_domaine <> vlan_recherche_domaine) THEN
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine associe au VLAN '||vlan_recherche_domaine||'.');
      END IF;
      
         -- regle d'integrite entre le VLAN E et le domaine associé
      IF ( :NEW.cpt_vlan = vlan_etudiant AND :NEW.cpt_domaine <> vlan_etudiant_domaine) THEN
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine associe au VLAN '||vlan_etudiant_domaine||'.');
      END IF;

     -- integrite des comptes ETUDIANTs seulement pour L.R.
        IF ( domaine_princ = 'univ-lr.fr' ) THEN
            IF ( (:NEW.cpt_vlan = vlan_etudiant) AND (:NEW.cpt_domaine NOT IN ('etudiant.univ-lr.fr','etudiut.univ-lr.fr')) ) THEN
                RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine etudiant ! ');
             END IF;
        END IF;

     -- integrite entre le VLAN eXterieur et le domaine
     IF ( (:NEW.cpt_vlan= vlan_externe) AND (:NEW.cpt_domaine = domaine_princ) ) THEN
            RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine exterieur ! ');
     END IF;

  END IF;
   -- fin contraintes VLAN et domaine

   -- Conversion du login sans les accents
   IF (:NEW.cpt_login IS NOT NULL) THEN
        newlogin := Chaine_Sans_Accents(:NEW.cpt_login);
     :NEW.cpt_login := newlogin;
   END IF;

   -- Conversion du mail sans les accents
   IF (:NEW.cpt_email IS NOT NULL) THEN
        newemail := Chaine_Sans_Accents(:NEW.cpt_email);
     :NEW.cpt_email := newemail;
   END IF;

   -- Conversion du domaine sans les accents
   IF (:NEW.cpt_domaine IS NOT NULL) THEN
        newdomaine := Chaine_Sans_Accents(:NEW.cpt_domaine);
     :NEW.cpt_domaine := newdomaine;
   END IF;

   -- insertion du nouvel enregistrement dans la table temporaire pour ne pas avoir l'erreur ORA-04091 : table mutante
   -- cas du trigger before insert on COMPTE qui fait un SELECT sur la table COMPTE dans le meme trigger
   INSERT INTO COMPTE_TEMP(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CRYPTE,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,CPT_VALIDE,CPT_PRINCIPAL,CPT_LISTE_ROUGE,D_CREATION,D_MODIFICATION,TVPN_CODE)
   VALUES(:NEW.CPT_ORDRE,:NEW.CPT_UID_GID,:NEW.CPT_LOGIN,:NEW.CPT_PASSWD,:NEW.CPT_CRYPTE,:NEW.CPT_CONNEXION,:NEW.CPT_VLAN,:NEW.CPT_EMAIL,:NEW.CPT_DOMAINE,:NEW.CPT_CHARTE,:NEW.CPT_VALIDE,:NEW.CPT_PRINCIPAL,:NEW.CPT_LISTE_ROUGE,SYSDATE,SYSDATE,:NEW.TVPN_CODE);

END ;
/
ALTER TRIGGER GRHUM.TRG_COMPTE ENABLE;


---------------------------V20130904.142415__DDL_MAJ_VERIF_CODE_INSEE.sql---------------------------
--------------------------------------------------------
--  Fichier créé - vendredi-août-30-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function VERIF_CODE_INSEE
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "GARNUCHE"."VERIF_CODE_INSEE" 
(
  indnoinsee    grhum.individu_ulr.ind_no_insee%TYPE,
  indcleinsee   grhum.individu_ulr.ind_cle_insee%TYPE,
  prisecptinsee grhum.individu_ulr.prise_cpt_insee%TYPE,
  adrcivilite   grhum.individu_ulr.c_civilite%TYPE,
  datenais      grhum.individu_ulr.d_naissance%TYPE,
  dptnais       grhum.individu_ulr.c_dept_naissance%TYPE,
  paysnais      grhum.individu_ulr.c_pays_naissance%TYPE
)
RETURN INTEGER
IS
  -- declaration des variables
  v_cpays       grhum.pays.c_pays%TYPE;
  v_noinsee     grhum.individu_ulr.ind_no_insee%TYPE;
  v_clecalculee INTEGER;
  i             INTEGER;
  v_civilite    CHAR(1);
  v_cleinsee    CHAR(2);
  v_anneeinsee  CHAR(2);
  v_anneenais   CHAR(2);
  v_moisinsee   CHAR(2);
  v_moisnais    CHAR(2);
  v_deptinsee   CHAR(3);
  v_deptnais    CHAR(3);
  erreur01 EXCEPTION; erreur02 EXCEPTION; erreur03 EXCEPTION; 
  erreur04 EXCEPTION; erreur05 EXCEPTION; erreur06 EXCEPTION;
  erreur07 EXCEPTION; erreur08 EXCEPTION; erreur09 EXCEPTION;
  erreur10 EXCEPTION; erreur11 EXCEPTION; erreur12 EXCEPTION;
  erreur13 EXCEPTION; erreur14 EXCEPTION; erreur15 EXCEPTION;
  erreur16 EXCEPTION; erreur17 EXCEPTION; erreur18 EXCEPTION;
  erreur19 EXCEPTION; erreur20 EXCEPTION; erreur21 EXCEPTION;
  erreur22 EXCEPTION; erreur23 EXCEPTION; erreur24 EXCEPTION;
  erreur25 EXCEPTION;
  -- Variables pour la recherche d'un traitement spécifique
  v_uneChaine   CLOB;
  v_laChaine    VARCHAR2(2000);
  v_paramobjet  VARCHAR2(2000);
  v_typeobjet   VARCHAR2(50);
  v_nomobjet    VARCHAR2(50);
  v_intRetour   INTEGER;
  v_position    INTEGER;
BEGIN
  -- ##################################################################################
  -- Recherche d'un traitement spécifique à prendre en compte                       ###
  -- ##################################################################################
  v_uneChaine := histotox.F_Recup_Objet_Histotox('FUNCTION','GARNUCHE','Verif_Code_Insee');

  IF (grhum.F_Verif_Nullite(v_uneChaine) = 0)
  THEN v_position := 1;
	   grhum.recuperation_donnee(v_uneChaine,1,1,'$',v_typeobjet,v_position);
	   grhum.recuperation_donnee(v_uneChaine,(v_position+1),v_position,'$',v_nomobjet,v_position);
	   grhum.recuperation_donnee(v_uneChaine,(v_position+1),v_position,'$',v_paramobjet,v_position);

	   v_typeobjet  := RTRIM(v_typeobjet);
	   v_nomobjet   := RTRIM(v_nomobjet);
	   v_paramobjet := RTRIM(v_paramobjet);
	   v_laChaine   := 'SELECT '||v_nomobjet||'('||v_paramobjet||') FROM DUAL';

	   EXECUTE IMMEDIATE v_laChaine INTO v_intRetour USING IN indnoinsee,indcleinsee,prisecptinsee,adrcivilite,datenais,dptnais;

	   RETURN v_intRetour;
  END IF;

  -- ##################################################################################
  -- Traitement générique                                                           ###
  -- ##################################################################################
  
  -- Initialisations...
  v_noinsee  := REPLACE(UPPER(indnoinsee),' ');
  v_cleinsee := LPAD(REPLACE(UPPER(TO_CHAR(indcleinsee,'99')),' '),2,'0');
  
  -- Vérification de la VALIDITE
  -- ***************************
  IF ((grhum.F_Verif_Nullite(v_noinsee) = 1) AND (grhum.F_Verif_Nullite(v_cleinsee) = 0))
  THEN RAISE erreur01;
  END IF;
  
  IF ((grhum.F_Verif_Nullite(v_noinsee) = 0) AND (grhum.F_Verif_Nullite(v_cleinsee) = 1))
  THEN RAISE erreur02;
  END IF;
  
  IF (LENGTH(v_noinsee) <> 13)
  THEN RAISE erreur03;
  END IF;
  
  IF (LENGTH(v_cleinsee) <> 2)
  THEN RAISE erreur04;
  END IF;
  
  v_clecalculee := Recuperer_Cle_Insee(v_noinsee);
  
  IF (v_clecalculee > 99)
  THEN RAISE erreur05;
  END IF;
  
  IF (v_clecalculee <> TO_NUMBER(v_cleinsee))
  THEN RAISE erreur06;
  END IF;
  
  -- Vérification de la COHERENCE
  -- ****************************
  -- CODE Provisoire...
  IF (prisecptinsee = 'P')
  THEN RETURN (0);
  END IF;
  
  -- CIVILITE...
  v_civilite := SUBSTR(v_noinsee,1,1);

  IF ((v_civilite <> '1') AND (v_civilite <> '2')) /* On ne traite pas les codes provisoires 7,8 ...*/
  THEN RETURN(0);
  END IF;

  IF ((adrcivilite = 'M.') AND (v_civilite <> '1'))
  THEN RAISE erreur07;
  END IF;

  IF (((adrcivilite = 'MME') OR (adrcivilite = 'MLLE')) AND (v_civilite <> '2'))
  THEN RAISE erreur08;
  END IF;

  -- DATE NAISSANCE...
  v_anneeinsee := SUBSTR(v_noinsee,2,2);
  v_anneenais  := SUBSTR(TO_CHAR(datenais,'dd/mm/yy'),7,2);

  IF (v_anneeinsee <> v_anneenais)
  THEN RAISE erreur09;
  END IF;

  v_moisinsee := SUBSTR(v_noinsee,4,2);
  v_moisnais  := SUBSTR(TO_CHAR(datenais,'DD/MM/YY'),4,2);

  IF ((v_moisinsee <> '99') AND (v_moisinsee <> v_moisnais))
  THEN RAISE erreur10;
  END IF;

  -- DEPARTEMENT...
  v_cpays     := grhum.F_Recup_Pvalue_String_For_User('GRHUM','GRHUM','param_key = ''GRHUM_C_PAYS_DEFAUT''');
  v_deptinsee := SUBSTR(v_noinsee,6,2);
  
  IF (v_deptinsee IN ('96','99'))
  THEN -- Etranger ou ancien protectorat
       IF (grhum.F_Verif_Nullite(paysnais) = 1)
       THEN RAISE erreur11;
       
       END IF;
       IF (paysnais = v_cpays)
       THEN RAISE erreur12;
       END IF;
       
       v_deptnais := SUBSTR(v_noinsee,8,3);
       
       FOR i IN 1..LENGTH(v_deptnais) 
       LOOP
         IF (grhum.Chiffre(SUBSTR(v_deptnais,i,1)) = 0) 
         THEN RAISE erreur13;                             
         END IF;
       END LOOP;

       IF (TO_NUMBER(v_deptnais) = 0)
       THEN RAISE erreur14;
       END IF;
       
       IF ((v_deptinsee = '96') AND (TO_NUMBER(TO_CHAR(datenais,'YYYY')) > 1967))
       THEN RAISE erreur15;
       END IF;
  ELSE v_deptnais := v_deptinsee;
  		/*
       IF (v_deptinsee IN ('97','98'))
       THEN -- DOM TOM
            v_deptinsee := SUBSTR(v_noinsee,6,3);
       
            IF (grhum.F_Verif_Nullite(dptnais) = 0)
            THEN v_deptnais := dptnais;
            END IF;
       ELSE -- Normal
            IF (grhum.F_Verif_Nullite(dptnais) = 0)
            THEN v_deptnais := SUBSTR(dptnais,2,2);
            END IF;
       END IF;
       */
       IF (grhum.F_Verif_Nullite(dptnais) = 0)
       		THEN v_deptnais := SUBSTR(dptnais,2,2);
       END IF;
       
       IF (v_deptnais <> v_deptinsee)
       THEN IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) <= 1962) AND (v_deptinsee IN ('91','92','93','94','95')))
            THEN -- Nés sous protectorat
                 IF (grhum.F_Verif_Nullite(dptnais) = 0)
                 THEN RAISE erreur16;
                 END IF;
            ELSE IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) <= 1968) AND (v_deptinsee IN ('75','78')))
                 THEN -- Nés en Ile de France avant 1968
                      -- Département 75 peut avoir un département de naissance égal à 75, 92, 93 ou 94
                      IF ((v_deptinsee = '75') AND (v_deptnais NOT IN ('75','92','93','94')))
                      THEN RAISE erreur17;
                      END IF;
                      
                      -- Département 78 peut avoir un département de naissance égal à 91, 92, 93, 94 ou 95
                      IF ((v_deptinsee = '78') AND (v_deptnais NOT IN ('91','92','93','94','95')))
                      THEN RAISE erreur18;
                      END IF;
                 ELSE IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) < 1976) AND (v_deptinsee = '20'))
                      THEN IF (v_deptnais NOT IN ('2A','2B'))
                           THEN RAISE erreur19;
                           END IF;
                      ELSE RAISE erreur20;
                      END IF;
                 END IF;
            END IF;
       END IF;
       
       IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) <= 1962) AND (v_deptinsee IN ('91','92','93','94','95')))
       THEN -- Vérification avec le pays de naissance
            IF (v_deptinsee IN ('91','92','93','94'))
            THEN -- Algérie
                 IF ((grhum.F_Verif_Nullite(paysnais) = 1) OR (paysnais NOT IN ('352','999')))
                 THEN RAISE erreur21;
                 END IF;
            ELSE IF (v_deptinsee = '95')
                 THEN -- Maroc
                      IF ((grhum.F_Verif_Nullite(paysnais) = 1) OR (paysnais NOT IN ('350','999')))
                      THEN RAISE erreur22;
                      END IF;
                 END IF;
            END IF;
       ELSE IF (paysnais <> v_cpays)
            THEN RAISE erreur23;
            END IF;
       END IF;
       
       IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) < 1976) AND (v_deptinsee IN ('2A','2B')))
       THEN RAISE erreur24;
       END IF;
       
       IF ((TO_NUMBER(TO_CHAR(datenais,'YYYY')) >= 1976) AND (v_deptinsee = '20'))
       THEN RAISE erreur25;
       END IF;
  END IF;

  RETURN(0);

  -- ERREURS...
  EXCEPTION
  WHEN erreur01 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE est obligatoire si vous renseignez la CLE');
  WHEN erreur02 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE est obligatoire si vous renseignez le CODE.');
  WHEN erreur03 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE doit comporter treize chiffres.');
  WHEN erreur04 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE doit comporter 2 chiffres.');
  WHEN erreur05 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE doit etre inferieure a 100.');
  WHEN erreur06 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : La CLE a une valeur erronnee.');
  WHEN erreur07 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour un homme, le CODE commence par 1.');
  WHEN erreur08 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour une femme, le CODE commence par 2.');
  WHEN erreur09 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : L''annee de naissance du CODE ne correspond pas a l''annee de naissance.');
  WHEN erreur10 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le mois de naissance du CODE ne correspond pas au mois de naissance');
  WHEN erreur11 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour une personne nee a l''etranger, vous devez fournir le pays de naissance.');
  WHEN erreur12 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE indique que la personne n''est pas nee en France.');
  WHEN erreur13 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le code pays du CODE pour les personnes nees a l''etranger ne doit comporter que des chiffres.');
  WHEN erreur14 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le code pays du CODE pour les personnes nees a l''etranger doit etre compris entre 000 et 999.');
  WHEN erreur15 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : A partir de 1967, Le code departement du CODE doit etre 99.');
  WHEN erreur16 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pas de departement pour les personnes nees en Algerie ou sous un protectorat.');
  WHEN erreur17 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Une personne nee avant 1968 avec 75 comme departement du CODE, ne peut avoir comme departement de naissance que : 75, 92, 93 ou 94.');
  WHEN erreur18 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Une personne nee avant 1968 avec 78 comme departement du CODE, ne peut avoir comme departement de naissance que : 91, 92, 93, 94 ou 95.');
  WHEN erreur19 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le departement de naissance selectionne ne correspond pas a la Corse.');
  WHEN erreur20 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le departement du CODE ne correspond pas au departement de naissance.');
  WHEN erreur21 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le pays de naissance de cette personne est l''Algerie.');
  WHEN erreur22 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le pays de naissance de cette personne est le Maroc.');
  WHEN erreur23 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Le CODE indique que la personne est nee en France.');
  WHEN erreur24 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour un corse ne avant 1976, le code departement du CODE ne peut etre 2A ou 2B.');
  WHEN erreur25 THEN RAISE_APPLICATION_ERROR(-20241,'INSEE : Pour un corse ne apres 1976, le code departement du CODE doit etre 2A ou 2B.');
END;

/




