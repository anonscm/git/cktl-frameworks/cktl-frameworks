--
-- ROME 
--
CREATE TABLE GRHUM.ROME
(
  ROM_ID           NUMBER                       NOT NULL,
  ROM_CODE         VARCHAR2(5)                  NOT NULL,
  ROM_LIBELLE      VARCHAR2(2000)               NOT NULL,
  ROM_D_OUVERTURE  DATE,
  ROM_D_FERMETURE  DATE,
  D_CREATION       DATE                         DEFAULT SYSDATE               NOT NULL,
  D_MODIFICATION   DATE                         DEFAULT SYSDATE               NOT NULL,
  ROM_ID_PERE      NUMBER                       NOT NULL
);

COMMENT ON TABLE GRHUM.ROME IS 'Nomenclature du Répertoire Opérationnel des Métiers et des Emplois';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID IS 'Clef primaire';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID IS 'Code ROME';

COMMENT ON COLUMN GRHUM.ROME.ROM_LIBELLE IS 'Libellé du ROME';

COMMENT ON COLUMN GRHUM.ROME.ROM_D_OUVERTURE IS 'Date de début de validité';

COMMENT ON COLUMN GRHUM.ROME.ROM_D_FERMETURE IS 'Date de fin de validité';

COMMENT ON COLUMN GRHUM.ROME.D_CREATION IS 'Date de création de l''enregistrement';

COMMENT ON COLUMN GRHUM.ROME.D_MODIFICATION IS 'Date de dernière modification de l''enregistrement';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID_PERE IS 'ROME père';


--
-- PK_ROME 
--
CREATE UNIQUE INDEX GRHUM.PK_ROME ON GRHUM.ROME(ROM_ID);


-- 
-- Non Foreign Key Constraints for Table ROME
-- 
ALTER TABLE GRHUM.ROME ADD (CONSTRAINT PK_ROME PRIMARY KEY (ROM_ID));


-- 
-- Foreign Key Constraints for Table ROME 
-- 
ALTER TABLE GRHUM.ROME ADD (CONSTRAINT ROME_REF_ROME_PERE 
 FOREIGN KEY (ROM_ID_PERE) 
 REFERENCES GRHUM.ROME (ROM_ID));

 
 
--
-- STRUCTURE_ULR
--
ALTER TABLE GRHUM.STRUCTURE_ULR ADD STR_STATUT VARCHAR2(2);

ALTER TABLE GRHUM.STRUCTURE_ULR ADD ROM_ID NUMBER;

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.STR_STATUT IS 'Statut de la structure : PUblic, PRivé, AUtre';

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.ROM_ID IS 'Clef étrangère vers la nomenclature des codes ROME (Répertoire Opérationnel des Métiers et des Emplois)';

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (
  CONSTRAINT CHK_STR_STATUT
 CHECK (STR_STATUT in ('PU','PR','AU')));

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (CONSTRAINT STR_REF_ROME 
 FOREIGN KEY (ROM_ID) 
 REFERENCES GRHUM.ROME (ROM_ID));

 
