create or replace 
PROCEDURE       MAJ_COMMUNE (
   cinsee       			VARCHAR2,
   ccommune		    		VARCHAR2,
   lcommune				    VARCHAR2,
   ccp             		VARCHAR2,
   ligneacheminement	VARCHAR2
)
IS
   n              NUMBER(5);
--   ccpprincial		VARCHAR2(5);
   particularite	VARCHAR2(1);
   ddebval  		  DATE;
   dffinval    		DATE;
   cdep       		VARCHAR2(3);
   ccp2        		VARCHAR2(5);
   is_part        BOOLEAN;
BEGIN
  -- Rappel : EXECUTE GRHUM.MAJ_COMMUNES('69244','TASSIN LA DEMI LUNE','TASSIN-LA-DEMI-LUNE','69160','TASSIN LA DEMI LUNE')
  
  -- détermination du département
  -- il faut tester if (cp < 97000) then c_dep = (zero + 2 premiers chiffres du code postal) else c_dep = (3 premiers chiffres du code postal)
   IF ( to_number(ltrim(rtrim(ccp)),99999) < 97000 ) THEN
      cdep := '0'||substr(ltrim(rtrim(ccp)),1,2);
   ELSE
      cdep := substr(ltrim(rtrim(ccp)),1,3);
   END IF;
   
   -- détermination du code postal principal
   -- il faut tester le dernier numéro et si différent de zéro alors le remplacer par zéro
   IF ( substr(ltrim(rtrim(ccp)),5,1) = 0 ) THEN
      ccp2 := ltrim(rtrim(ccp));
   ELSE
      ccp2 := substr(ltrim(rtrim(ccp)),1,4)||'0';
   END IF;
   -- détermination de la particularité
   -- si acheminement différent du libellé court alors particularité = c sinon null
   IF ( upper(ltrim(rtrim(ligneacheminement))) = upper(ltrim(rtrim(ccommune))) ) THEN
      is_part := false;
 --     particularite := 'NON';
      particularite := null;
   ELSE
      is_part := true;
 --     particularite := 'OUI';
      particularite := 'C';
   END IF;

    -- MAJ
    
   SELECT COUNT (*) INTO n FROM grhum.commune WHERE c_postal = ccp AND c_insee = cinsee;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.commune(c_insee, ll_com, lc_com, c_postal, c_dep, d_creation, d_modification, d_deb_val, d_fin_val, particularite_commune, c_postal_principal, ligne_acheminement)
      SELECT cinsee, lcommune, ccommune, ccp, cdep, SYSDATE, SYSDATE, null, null, particularite, ccp2, ligneacheminement FROM DUAL;
   ELSE
      -- mise à jour des communes issues du Fichier des Communes
      UPDATE grhum.commune SET ll_com = lcommune, d_modification = SYSDATE, lc_com = ccommune, c_postal = ccp, c_postal_principal = ccp2, c_insee = cinsee, c_dep=cdep, ligne_acheminement = ligneacheminement
      WHERE c_postal = ccp AND c_insee = cinsee;
   END IF;
 

END;
/