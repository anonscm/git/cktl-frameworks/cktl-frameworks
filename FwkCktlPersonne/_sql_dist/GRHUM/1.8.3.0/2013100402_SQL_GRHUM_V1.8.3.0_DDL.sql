--
-- Patch DDL de GRHUM du 04/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.3.0
-- Date de publication : 04/10/2013
-- Auteur(s) : Alain

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.2.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.3.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.3.0 a deja ete passe !');
    end if;

end;
/




-----------------------V20131003.171122__DDL_MAJ_ECHELON_PASSAGE_ECHELON.sql------------------------

DECLARE

cpt integer;

BEGIN

select count(*) into cpt from all_tab_columns where column_name = 'LFEN_ORDRE' and table_name = 'REPART_ENFANT' and owner = 'GRHUM';
if (cpt = 0)
then
    EXECUTE IMMEDIATE 'ALTER TABLE GRHUM.REPART_ENFANT ADD (LFEN_ORDRE NUMBER)';
end if;

select count(*) into cpt from all_tab_columns where column_name = 'TEM_A_CHARGE' and table_name = 'REPART_ENFANT' and owner = 'GRHUM';
if (cpt = 0)
then
    EXECUTE IMMEDIATE 'ALTER TABLE GRHUM.REPART_ENFANT ADD (TEM_A_CHARGE VARCHAR2(1) default ''O'' NOT NULL)';
end if;

select count(*) into cpt from all_tab_columns where column_name = 'TEM_VALIDE' and table_name = 'POSITION' and owner = 'GRHUM';
if (cpt = 0)
then
    EXECUTE IMMEDIATE 'ALTER TABLE GRHUM.POSITION ADD (TEM_VALIDE VARCHAR2(1) default ''O'' NOT NULL)';
end if;

END;
/

CREATE OR REPLACE FUNCTION   grhum.format_BIC(bic varchar)
RETURN varchar
-- Fonction qui renvoie un bic formate
-- le parametre doit avoir une longueur de 8 ou 11 caracteres pour etre formate
is
    bicFormatte VARCHAR2(20);
begin
    if (length(bic) <> 8 and length(bic) <> 11) then
        return bic;
    end if;


    bicFormatte :=  substr(bic, 1,4) || ' ' ||
                    substr(bic, 5,2) || ' ' ||
                    substr(bic, 7,2);
    if (length(bic) = 11) then
        bicFormatte := bicFormatte || ' ( ' || substr(bic, 9,3) || ' ) ';
    end if;

    return bicFormatte;
end;
/


/* Formatted on 2013/09/27 14:39 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW grhum.v_pb_affectation (pers_id,
                                                     no_individu,
                                                     nom_usuel,
                                                     nom_patronymique,
                                                     prenom,
                                                     d_naissance
                                                    )
AS
   SELECT pers_id, no_individu, nom_usuel, nom_patronymique, prenom,
          d_naissance
     FROM individu_ulr
    WHERE no_individu IN (
             SELECT UNIQUE ec.no_dossier_pers
                      FROM element_carriere ec,
                           carriere c,
                           changement_position cp
                     WHERE (   ec.d_fin_element >=
                                  TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                           'dd/mm/YYYY'
                                          )
                            OR ec.d_fin_element IS NULL
                           )
                       AND ec.d_effet_element <=
                              TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                       'dd/mm/YYYY'
                                      )
                       AND ec.no_seq_carriere = c.no_seq_carriere
                       AND ec.no_dossier_pers = c.no_dossier_pers
                       AND c.no_dossier_pers = cp.no_dossier_pers
                       AND c.no_seq_carriere = cp.carriere_accueil
                       and ec.tem_valide = 'O'
                       and cp.tem_valide = 'O'
                       AND cp.c_position NOT IN ('CFA', 'DETA', 'DISP')
             UNION
             SELECT UNIQUE no_dossier_pers
                      FROM contrat c
                     WHERE (   c.d_fin_contrat_trav >=
                                  TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                           'dd/mm/YYYY'
                                          )
                            OR c.d_fin_contrat_trav IS NULL
                           )
                       AND c.d_deb_contrat_trav <=
                              TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                       'dd/mm/YYYY'
                                      )
                                      and c.tem_annulation = 'N'
             MINUS
             SELECT UNIQUE no_dossier_pers
                      FROM affectation a
                     WHERE (   a.d_fin_affectation >=
                                  TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                           'dd/mm/YYYY'
                                          )
                            OR a.d_fin_affectation IS NULL
                           )
                       AND a.d_deb_affectation <=
                              TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                       'dd/mm/YYYY'
                                      )
                       AND TEM_VALIDE = 'O'
             MINUS
             SELECT UNIQUE c.no_dossier_pers
                      FROM contrat c
                     WHERE c.c_type_contrat_trav IN ('VF', 'VN')
             MINUS
             SELECT UNIQUE no_dossier_pers
                      FROM affectation a
                     WHERE (   a.d_fin_affectation >=
                                  TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                           'dd/mm/YYYY'
                                          )
                            OR a.d_fin_affectation IS NULL
                           )
                       AND a.d_deb_affectation >=
                              TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                       'dd/mm/YYYY'
                                      ));




