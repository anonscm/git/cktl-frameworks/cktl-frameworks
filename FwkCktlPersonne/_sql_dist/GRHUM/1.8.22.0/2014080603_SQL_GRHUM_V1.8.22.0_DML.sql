--
-- Patch DML de GRHUM du 06/08/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.22.0
-- Date de publication : 06/08/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--------------------------V20140728.105623__DML_DML_TELE_ENSEIGNEMENT.sql---------------------------
DECLARE
	cpt INTEGER;
BEGIN

	SELECT count(*) INTO cpt FROM GARNUCHE.tele_enseignement;
	
	IF (cpt <> 5) THEN
		INSERT INTO GARNUCHE.tele_enseignement
			(HIST_TELEENS, TELE_CODE, TELE_LIBELLE, TELE_LC, DATE_OUVERTURE, DATE_FERMETURE, D_CREATION, D_MODIFICATION)
			VALUES(3, 3, 'ENSEIGT A DISTANCE SUIVI DEPUIS France', 'E DIST ETR', to_date('01/09/2013','DD/MM/YYYY'), NULL, SYSDATE, SYSDATE);
	
		INSERT INTO GARNUCHE.tele_enseignement
			(HIST_TELEENS, TELE_CODE, TELE_LIBELLE, TELE_LC, DATE_OUVERTURE, DATE_FERMETURE, D_CREATION, D_MODIFICATION)
			VALUES(4, 4, 'ENSEIGT A DISTANCE SUIVI DEPUIS ETRANGER', 'E DIST FR', to_date('01/09/2013','DD/MM/YYYY'), NULL, SYSDATE, SYSDATE);
	
		UPDATE GARNUCHE.tele_enseignement
			SET tele_lc = 'SANS OBJET', date_ouverture = to_date('02/08/1993','DD/MM/YYYY'), d_modification = SYSDATE
			WHERE tele_code = '0' and hist_teleens = 0;
	
		UPDATE GARNUCHE.tele_enseignement
			SET tele_lc = 'TELE HCNED', date_ouverture = to_date('02/08/1993','DD/MM/YYYY'), date_fermeture = to_date('31/08/2013','DD/MM/YYYY'), d_modification = SYSDATE
			WHERE tele_code = '1' and hist_teleens = 1;
	
	
		UPDATE GARNUCHE.tele_enseignement
			SET tele_lc = 'TELE CNED', date_fermeture = to_date('31/08/2013','DD/MM/YYYY'), d_modification = SYSDATE
			WHERE tele_code = '2' and hist_teleens = 2;
	END IF;

	
END;
/
-------------------------------V20140728.105724__DML_DML_MAJ_RNE.sql--------------------------------
DECLARE

BEGIN
-- http://www.education.gouv.fr/annuaire/19-correze/egletons/lycee/lycee-pierre-caraminot.html
	update grhum.rne set ville='EGLETONS' where c_rne = '0190018S';
	grhum.maj_cinsee_du_RNE('0190018S', 'EGLETONS', '19300');

-- http://www.education.gouv.fr/annuaire/78-yvelines/orgerus/college/college-georges-pompidou.html
	update grhum.rne set ville='ORGERUS' where c_rne = '0781864C';
	grhum.maj_cinsee_du_RNE('0781864C', 'ORGERUS', '78910');


END;
/


--------------------------------------------------------
--  RNE fourni par l'UTBM
--------------------------------------------------------
--ESME SUDRIA LYON :

DECLARE

cpt INTEGER;

BEGIN

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0693830E';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'ESME SUDRIA LYON', lc_rne = 'ESME SUDRIA LYON', adresse = '86 Boulevard Marius Vivier Merle', code_postal = '69003', d_modification = sysdate, acad_code = '010', ville = 'LYON 3E  ARRONDISSEMENT'
        WHERE C_RNE = '0693830E';

     ELSE

  insert into grhum.RNE (c_rne, ll_rne, lc_rne, acad_code, etab_statut, adresse, code_postal, ville, d_creation, d_modification, siret)
    values ( '0693830E','ESME SUDRIA LYON', 'ESME SUDRIA LYON','010','PU','86 Boulevard Marius Vivier Merle', '69003', 'LYON 3E  ARRONDISSEMENT', SYSDATE, SYSDATE, null);
    
    end if; 
       
 grhum.maj_cinsee_du_RNE('0693830E', 'LYON 3E  ARRONDISSEMENT', '69003');

END;
/
--------------------------V20140729.104224__DML_Ajout_types_crypt_sha.sql---------------------------
-- Insertion des types de cryptage SHA*

INSERT INTO GRHUM.TYPE_CRYPTAGE (TCRY_ORDRE,TCRY_LIBELLE,TCRY_JAVA_METHODE,D_DEB_VAL,D_FIN_VAL)
	VALUES (GRHUM.TYPE_CRYPTAGE_SEQ.NEXTVAL, 'SHA1', 'org.cocktail.crypto.sha.Sha1.crypt',TO_DATE('1980/01/01', 'yyyy/mm/dd'),null);

INSERT INTO GRHUM.TYPE_CRYPTAGE (TCRY_ORDRE,TCRY_LIBELLE,TCRY_JAVA_METHODE,D_DEB_VAL,D_FIN_VAL)
	VALUES (GRHUM.TYPE_CRYPTAGE_SEQ.NEXTVAL, 'SHA256', 'org.cocktail.crypto.sha.Sha256.crypt',TO_DATE('1980/01/01', 'yyyy/mm/dd'),null);

INSERT INTO GRHUM.TYPE_CRYPTAGE (TCRY_ORDRE,TCRY_LIBELLE,TCRY_JAVA_METHODE,D_DEB_VAL,D_FIN_VAL)
	VALUES (GRHUM.TYPE_CRYPTAGE_SEQ.NEXTVAL, 'SHA384', 'org.cocktail.crypto.sha.Sha384.crypt',TO_DATE('1980/01/01', 'yyyy/mm/dd'),null);

INSERT INTO GRHUM.TYPE_CRYPTAGE (TCRY_ORDRE,TCRY_LIBELLE,TCRY_JAVA_METHODE,D_DEB_VAL,D_FIN_VAL)
	VALUES (GRHUM.TYPE_CRYPTAGE_SEQ.NEXTVAL, 'SHA512', 'org.cocktail.crypto.sha.Sha512.crypt',TO_DATE('1980/01/01', 'yyyy/mm/dd'),null);		
------------------------V20140805.132341__DML_Maj_potentiel_brut_contrat.sql------------------------
---------------------------------------------------------------------
-- Mise à jour du potentiel brut des types de contrat enseignant
-- 	AF - AM - AT - AX - CI - DN - DO - LT - LB - MA - MC 
---------------------------------------------------------------------

UPDATE GRHUM.TYPE_CONTRAT_TRAVAIL
   SET POTENTIEL_BRUT = 192, D_MODIFICATION = SYSDATE
 WHERE C_TYPE_CONTRAT_TRAV IN ('AF', 'AM', 'AT');

UPDATE GRHUM.TYPE_CONTRAT_TRAVAIL
   SET POTENTIEL_BRUT = 96, D_MODIFICATION = SYSDATE
 WHERE C_TYPE_CONTRAT_TRAV IN ('AX', 'MA', 'MC');

UPDATE GRHUM.TYPE_CONTRAT_TRAVAIL
   SET POTENTIEL_BRUT = 250, D_MODIFICATION = SYSDATE
 WHERE C_TYPE_CONTRAT_TRAV IN ('LT', 'LB');


UPDATE GRHUM.TYPE_CONTRAT_TRAVAIL
   SET POTENTIEL_BRUT = 64, D_MODIFICATION = SYSDATE
 WHERE C_TYPE_CONTRAT_TRAV = 'DO';


UPDATE GRHUM.TYPE_CONTRAT_TRAVAIL
   SET POTENTIEL_BRUT = 384, D_MODIFICATION = SYSDATE
 WHERE C_TYPE_CONTRAT_TRAV = 'CI';


--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.22.0',to_date('06/08/2014','DD/MM/YYYY'),sysdate,'MAJ de triggers, tables, views et procédures stockées dans GRHUM, plus cryptage SHA et potentiels bruts sur contrat trav');
COMMIT;
