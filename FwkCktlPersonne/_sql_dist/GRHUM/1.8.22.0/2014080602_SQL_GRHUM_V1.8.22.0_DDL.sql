--
-- Patch DDL de GRHUM du 06/08/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.22.0
-- Date de publication : 06/08/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.21.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.22.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.22.0 a deja ete passe !');
    end if;

end;
/


--------------------------V20140728.104831__DDL_GRHUM_ADM_REORG_TABLE.sql---------------------------
create or replace PROCEDURE GRHUM.adm_reorg_table(ownerTable VARCHAR2, nameTable VARCHAR2, nameTablespaceIndx VARCHAR2)
IS
-- Procédure de réorganisation d'une table
-- ownerTable : le proprietaire (user) de la table
-- nameTable : nom de la table à réorganiser
-- nameTablespaceIndx : nom du tablespace des index
-- ex : grhum.adm_reorg_table('SCO_SCOLARITE', 'COMPOSANT', 'INDX_CKTL_SCOL')

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE ' || upper(ownerTable) || '.' || upper(nameTable) || ' MOVE';
    
    -- Rebuild des index de cette table
    FOR var_index IN (select * from all_indexes where table_owner = upper(ownerTable) and TABLE_NAME = upper(nameTable)) loop
        EXECUTE IMMEDIATE 'ALTER INDEX ' || var_index.owner || '.' || var_index.index_name || ' REBUILD TABLESPACE ' || nameTablespaceIndx;
    END LOOP;
END;
/
--------------------------V20140728.104858__DDL_DDL_TELE_ENSEIGNEMENT.sql---------------------------
declare
begin
	GRHUM.ADM_ADD_COLUMN('GARNUCHE', 'TELE_ENSEIGNEMENT', 'TELE_LC', 'VARCHAR2(15)', null, null);
	GRHUM.ADM_ADD_COLUMN('GARNUCHE', 'TELE_ENSEIGNEMENT', 'DATE_OUVERTURE', 'DATE', null, null);
	GRHUM.ADM_ADD_COLUMN('GARNUCHE', 'TELE_ENSEIGNEMENT', 'DATE_FERMETURE', 'DATE', null, null);
	GRHUM.ADM_ADD_COLUMN('GARNUCHE', 'TELE_ENSEIGNEMENT', 'D_CREATION', 'DATE', 'SYSDATE', 'NOT NULL');
	GRHUM.ADM_ADD_COLUMN('GARNUCHE', 'TELE_ENSEIGNEMENT', 'D_MODIFICATION', 'DATE', 'SYSDATE', 'NOT NULL');
	
	GRHUM.ADM_REORG_TABLE('GARNUCHE', 'TELE_ENSEIGNEMENT','SCOL_INDX');
	
end;
/

COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.HIST_TELEENS IS 'PK de la table Garnuche.Tele_enseignement';
COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.TELE_CODE IS 'Code du Télé_enseignement (Valeur de la BCN)';
COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.TELE_LIBELLE IS 'Libellé long du télé-enseignement';

COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.TELE_LC IS 'Libellé court du télé-enseignement';
COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.DATE_OUVERTURE IS 'Date d''ouverture';
COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.DATE_FERMETURE IS 'Date de fermeture';

COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GARNUCHE.TELE_ENSEIGNEMENT.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement.';
--------------------------V20140728.104935__DDL_GRHUM_SECTEUR_ACTIVITE.sql--------------------------
declare
begin

	GRHUM.DROP_CONSTRAINT('GRHUM', 'STRUCTURE_ULR', 'FK_STRUCTURE_SACT');

    GRHUM.DROP_OBJECT('GRHUM', 'SECTEUR_ACTIVITE', 'TABLE');
    
    GRHUM.DROP_OBJECT('GRHUM', 'SECTEUR_ACTIVITE_SEQ', 'SEQUENCE');
    
    COMMIT;

end;
/


CREATE TABLE GRHUM.SECTEUR_ACTIVITE 
(
   	SACT_ID NUMBER NOT NULL ENABLE, 
	SACT_CODE VARCHAR2(5) NOT NULL ENABLE, 
	SACT_LIBELLE  VARCHAR2(2000) NOT NULL ENABLE, 
	SACT_D_OUVERTURE DATE, 
	SACT_D_FERMETURE DATE, 
	D_CREATION DATE DEFAULT SYSDATE NOT NULL ENABLE, 
	D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL ENABLE, 
	CONSTRAINT PK_SECTEUR_ACTIVITE PRIMARY KEY (SACT_ID) USING INDEX TABLESPACE INDX_GRHUM
)
TABLESPACE DATA_GRHUM;
 
COMMENT ON COLUMN GRHUM.SECTEUR_ACTIVITE.SACT_ID IS 'Clef primaire de la table SECTEUR_ACTIVITE';

COMMENT ON COLUMN GRHUM.SECTEUR_ACTIVITE.SACT_CODE IS 'Code du secteur d''activité';
 
COMMENT ON COLUMN GRHUM.SECTEUR_ACTIVITE.SACT_LIBELLE IS 'Libellé du secteur d''activité';
 
COMMENT ON COLUMN GRHUM.SECTEUR_ACTIVITE.SACT_D_OUVERTURE IS 'Date de début de validité';
 
COMMENT ON COLUMN GRHUM.SECTEUR_ACTIVITE.SACT_D_FERMETURE IS 'Date de fin de validité';
 
COMMENT ON COLUMN GRHUM.SECTEUR_ACTIVITE.D_CREATION IS 'Date de création de l''enregistrement';
 
COMMENT ON COLUMN GRHUM.SECTEUR_ACTIVITE.D_MODIFICATION IS 'Date de dernière modification de l''enregistrement';
  
COMMENT ON TABLE GRHUM.SECTEUR_ACTIVITE  IS 'Nomenclature des secteurs d''activités liée aux personnes morales ou aux diplômes.';
 
CREATE SEQUENCE GRHUM.SECTEUR_ACTIVITE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCACHE NOCYCLE NOORDER;

-- DML
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'ADMPU','Administration publique, science politique',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'AGRAL','Agro-alimentaire, agriculture',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'BANAS','Banque, assurance',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'BIOTE','Biologie, biotechnologies',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'BTPAE','BTP, aménagement, énergie',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'COMDI','Commerce, distribution',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'COMME','Communication,médias',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'CULPA','Culture, patrimoine',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'DROJU','Droit, justice',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'ENSRE','Enseignement, recherche',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'ENVE','Environnement, écologie, littoral',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'GESMA','Gestion, management des entreprises, comptabilité',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'INFWE','Informatique, Web, images, télécommunications',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'MARKE','Marketing',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'PHYCH','Physique, chimie, matériaux',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'RELIN','Relations internationales',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'SANPA','Santé, paramédical',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'TOURI','Tourisme',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);
insert into GRHUM.SECTEUR_ACTIVITE values(GRHUM.SECTEUR_ACTIVITE_SEQ.nextval,'TRAIN','Traduction, interprétariat',to_date('01/01/2014','DD/MM/YYYY'),null,sysdate,sysdate);

-------------------------------V20140728.105026__DDL_MAJ_BAC_BCN.sql--------------------------------
create or replace
PROCEDURE       GRHUM.MAJ_BAC_BCN (
   bacCode       			GRHUM.BAC.BAC_CODE%TYPE,
   libelleCourt		    	GRHUM.BAC.BAC_LIB_COURT%TYPE,
   libelleLong				GRHUM.BAC.BAC_LIBELLE%TYPE,
   dateOuverture			GRHUM.BAC.BAC_DATE_VALIDITE%TYPE,
   dateFermeture			GRHUM.BAC.BAC_DATE_INVALIDITE%TYPE
)
IS
   n                NUMBER(5);
   bacValidite		GRHUM.BAC.BAC_VALIDITE%TYPE;
   bacCodeSise		GRHUM.BAC.BAC_CODE_SISE%TYPE;
   bacType			GRHUM.BAC.BAC_TYPE%TYPE;
BEGIN
    
   SELECT COUNT (*) INTO n FROM grhum.bac WHERE bac_code = bacCode;
   
   IF (n is not null) THEN
   		-- Positionnement de codes
   	  bacCodeSise := bacCode;
   	  
   	  if (bacCode like '003%' or bacCode like 'EQ%') then
   	  		bacType := 'EQV';
   	  else if (libelleLong like '%BACS PRO%') then
   	  			bacType := 'PRO';
   	  		else
   	  			bacType := 'BAC';
   	  		end if;
   	  end if;
   	  
   	  if (dateFermeture is not null) then
   	  		bacValidite := 'N';
   	  else
   	  		bacValidite := 'O';
   	  end if;
   END IF;
   
   IF (n = 0) THEN
   	  			
      -- insertion d'un bac
      INSERT INTO grhum.bac(bac_code, bac_libelle, bac_code_national, bac_type, bac_code_sise, bac_date_validite, d_creation, d_modification,
      		bac_validite, bac_date_invalidite, bac_lib_court )
      VALUES( bacCode, libelleLong, null, bacType, bacCodeSise, dateOuverture, SYSDATE, SYSDATE,
      		bacValidite, dateFermeture, libelleCourt);
   ELSE
      -- mise à jour d'un Bac
      UPDATE grhum.bac
      SET bac_code = bacCode, bac_libelle = libelleLong, bac_type = bacType, bac_code_sise = bacCodeSise, bac_date_validite = dateOuverture, d_modification = SYSDATE,
      		bac_validite = bacValidite, bac_date_invalidite = dateFermeture, bac_lib_court = libelleCourt
     WHERE bac_code = bacCode;
   END IF;
   COMMIT;
 
END;
/

---------------------------V20140728.105044__DDL_MAJ_BAC_SERIE_GT_BCN.sql---------------------------
create or replace
PROCEDURE       GRHUM.MAJ_BAC_SERIE_GT_BCN (
   bacCode       			GRHUM.BAC.BAC_CODE%TYPE,
   libelleCourt		    	GRHUM.BAC.BAC_LIB_COURT%TYPE,
   libelleLong				GRHUM.BAC.BAC_LIBELLE%TYPE,
   codeNational				GRHUM.BAC.BAC_CODE_NATIONAL%TYPE,
   dateOuverture			GRHUM.BAC.BAC_DATE_VALIDITE%TYPE,
   dateFermeture			GRHUM.BAC.BAC_DATE_INVALIDITE%TYPE
)
IS
   n                NUMBER(5);
   bacValidite		GRHUM.BAC.BAC_VALIDITE%TYPE;
   bacCodeSise		GRHUM.BAC.BAC_CODE_SISE%TYPE;
   bacType			GRHUM.BAC.BAC_TYPE%TYPE;
BEGIN
    
   SELECT COUNT (*) INTO n FROM grhum.bac WHERE bac_code = bacCode;
   
   IF (n is not null) THEN
   		-- Positionnement de codes
   	  bacCodeSise := bacCode;
   	  bacType := 'BAC';
   	  
   	  
   	  if (dateFermeture is not null) then
   	  		bacValidite := 'N';
   	  else
   	  		bacValidite := 'O';
   	  end if;
   END IF;
   
   IF (n = 0) THEN
   	  			
      -- insertion d'un bac
      INSERT INTO grhum.bac(bac_code, bac_libelle, bac_code_national, bac_type, bac_code_sise, bac_date_validite, d_creation, d_modification,
      		bac_validite, bac_date_invalidite, bac_lib_court )
      VALUES( bacCode, libelleLong, codeNational, bacType, bacCodeSise, dateOuverture, SYSDATE, SYSDATE,
      		bacValidite, dateFermeture, libelleCourt);
   ELSE
      -- mise à jour d'un Bac
      UPDATE grhum.bac
      SET bac_code = bacCode, bac_libelle = libelleLong, bac_code_national = codeNational, bac_type = bacType, bac_code_sise = bacCodeSise, bac_date_validite = dateOuverture, d_modification = SYSDATE,
      		bac_validite = bacValidite, bac_date_invalidite = dateFermeture, bac_lib_court = libelleCourt
     WHERE bac_code = bacCode;
   END IF;
   COMMIT;
 
END;
/

-----------------------------V20140728.105104__DDL_MAJ_COMMUNE_BCN.sql------------------------------
create or replace
PROCEDURE      GRHUM.MAJ_COMMUNE_BCN (
   cinsee       			GRHUM.COMMUNE.C_INSEE%TYPE,
   ccommune		    		GRHUM.COMMUNE.LC_COM%TYPE,
   lcommune				    GRHUM.COMMUNE.LL_COM%TYPE,
   cdep             		GRHUM.COMMUNE.C_DEP%TYPE,
   dateOuverture			GRHUM.COMMUNE.D_DEB_VAL%TYPE,
   dateFermeture			GRHUM.COMMUNE.D_FIN_VAL%TYPE
)
IS
   n                NUMBER(5);
   cpostal			GRHUM.COMMUNE.C_POSTAL%TYPE;
BEGIN

 -- MAJ
    
   SELECT COUNT (*) INTO n FROM grhum.commune WHERE (ll_com = lcommune or lc_com = lcommune) AND c_insee = cinsee;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.commune(c_insee, ll_com, lc_com, c_postal, c_dep, d_creation, d_modification, d_deb_val, d_fin_val, particularite_commune, c_postal_principal, ligne_acheminement)
      VALUES( cinsee, lcommune, ccommune, '00000', cdep, SYSDATE, SYSDATE, dateOuverture, dateFermeture, null, null, null);
      
      
   ELSE
      -- mise à jour des communes issues du Fichier des Communes
      UPDATE grhum.commune
      SET ll_com = lcommune, d_modification = SYSDATE, lc_com = ccommune, c_insee = cinsee, c_dep=cdep,
      		d_deb_val = dateOuverture, d_fin_val = dateFermeture
      WHERE ll_com = lcommune AND c_insee = cinsee;
   END IF;
   COMMIT;
   
 
END;
/


-------------------------------V20140728.105140__DDL_MAJ_PAYS_BCN.sql-------------------------------
create or replace
PROCEDURE      GRHUM.MAJ_PAYS_BCN (
   numPays       				GRHUM.PAYS.C_PAYS%TYPE,
   libelleLong		    		GRHUM.PAYS.LL_PAYS%TYPE,
   libelleCourt					GRHUM.PAYS.LC_PAYS%TYPE,
   dOuv							GRHUM.PAYS.D_DEB_VAL%TYPE,
   dFer							GRHUM.PAYS.D_FIN_VAL%TYPE,
   paysCommentaire				GRHUM.PAYS.COMMENTAIRE%TYPE,
   libelleAccentue70			GRHUM.PAYS.L_EDITION%TYPE
)
IS
   n                NUMBER(5);
   chaine			VARCHAR2(250);
BEGIN


   SELECT COUNT (*) INTO n FROM grhum.pays WHERE ll_pays = libelleLong and c_pays = numPays;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.pays(c_pays, ll_pays, lc_pays, l_nationalite, d_deb_val, d_fin_val,
      		d_creation, d_modification, code_iso, l_edition, ll_pays_en, iso_3166_3, commentaire)
      VALUES(numPays, libelleLong, libelleCourt, null, dOuv, dFer, SYSDATE, SYSDATE, null, libelleAccentue70, null, null, paysCommentaire);
   ELSE
      -- mise à jour des libellé et des dates de validité des pays
      UPDATE grhum.pays
      SET c_pays = numPays, ll_pays = libelleLong, lc_pays = libelleCourt, d_deb_val = dOuv, d_fin_val = dFer,
      		d_modification = SYSDATE, l_edition = libelleAccentue70, commentaire = paysCommentaire
     WHERE ll_pays = libelleLong and c_pays = numPays;
   END IF;
   COMMIT;
 
END;
/

----------------------------V20140728.105157__DDL_MAJ_PAYS_CODE_ISO.sql-----------------------------
create or replace
PROCEDURE       GRHUM.MAJ_PAYS_CODE_ISO (
   libelleMajuscule       				GRHUM.PAYS.LL_PAYS%TYPE,
   iso3		    						GRHUM.PAYS.ISO_3166_3%TYPE,
   iso2									GRHUM.PAYS.CODE_ISO%TYPE
)
IS
   n                NUMBER(5);
   chaine			VARCHAR2(250);
BEGIN

   SELECT COUNT (*) INTO n FROM grhum.pays WHERE ll_pays = libelleMajuscule;
   IF (n = 0) THEN
      -- pas de MAJ possible
      chaine := ltrim(rtrim('Le pays '));
      chaine := ltrim(rtrim(chaine))||ltrim(rtrim(libelleMajuscule))||' ne correspond à aucun enregistrement.';
      dbms_output.put_line(chaine);
   ELSE
      -- mise à jour des codes ISO des pays
      UPDATE grhum.pays
      SET code_iso = iso2, iso_3166_3 = iso3 , d_modification = SYSDATE
     WHERE ll_pays = libelleMajuscule;
   END IF;
   COMMIT;
 
END;
/

---------------------------------V20140728.105215__DDL_MAJ_UAI.sql----------------------------------
create or replace
PROCEDURE       GRHUM.MAJ_CINSEE_DU_RNE
(
  uai			GRHUM.RNE.C_RNE%TYPE,
  ville			GRHUM.RNE.VILLE%TYPE,
  cp			GRHUM.RNE.CODE_POSTAL%TYPE
  
)
IS
-- ------------
-- DECLARATIONS
-- ------------
	cpt			INTEGER;
	cinsee      VARCHAR2(5);
	
	
-- ---------
-- PRINCIPAL
-- ---------
BEGIN
    cpt := 0;
    select count(*) into cpt from GRHUM.COMMUNE c where (c.ll_com like ville or c.lc_com like ville or ligne_acheminement like ville) and c.c_postal = cp;
    if (cpt = 1) then
        update GRHUM.RNE set code_insee = (select c.c_insee from GRHUM.COMMUNE c 
        									where (c.ll_com like ville or c.lc_com like ville or ligne_acheminement like ville) and c.c_postal = cp)
                where c_rne = uai;
    end if;
    if (cpt > 1) then
        select max(c_insee) into cinsee from GRHUM.COMMUNE c
                where (c.ll_com like ville or c.lc_com like ville or ligne_acheminement like ville) and c.c_postal = cp
                                and c.particularite_commune is null;
        update GRHUM.RNE set code_insee = cinsee where c_rne = uai;
    end if;
    commit;
END;
/

create or replace
PROCEDURE       GRHUM.MAJ_UAI (
   crne       				GRHUM.RNE.C_RNE%TYPE,
   llrne		    		GRHUM.RNE.LL_RNE%TYPE,
   adresseUai				GRHUM.RNE.ADRESSE%TYPE,
   cpostal					GRHUM.RNE.CODE_POSTAL%TYPE,
   dateOuverture			GRHUM.RNE.D_DEB_VAL%TYPE,
   dateFermeture			GRHUM.RNE.D_FIN_VAL%TYPE,
   academie					GRHUM.RNE.ACAD_CODE%TYPE,
   secteurRne				GRHUM.RNE.ETAB_STATUT%TYPE,
   localite					GRHUM.RNE.VILLE%TYPE,
   numSiret					GRHUM.RNE.SIRET%TYPE
)
IS
   n                NUMBER(5);
BEGIN
    
   SELECT COUNT (*) INTO n FROM grhum.rne WHERE c_rne = crne;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.rne(c_rne, ll_rne, adresse, code_postal, c_rne_pere, d_deb_val, d_fin_val, d_creation, d_modification,
      		lc_rne,acad_code,tetab_code, etab_enquete, etab_statut, ville, adr_ordre, siret )
      VALUES( crne, llrne, adresseUai, cpostal, null, dateOuverture, dateFermeture, SYSDATE, SYSDATE,
      		null, academie, null, null, secteurRne, localite, null, numSiret);
   ELSE
      -- mise à jour des UAI du MESR
      UPDATE grhum.rne
      SET c_rne = crne, ll_rne = llrne, adresse = adresseUai, code_postal = cpostal, d_deb_val = dateOuverture, d_fin_val = dateFermeture,
      		d_modification = SYSDATE, acad_code = academie, ETAB_STATUT = secteurRne, ville = localite, siret = numSiret
     WHERE c_rne = crne;
   END IF;
   COMMIT;
   
   GRHUM.maj_cinsee_du_rne(crne, localite, cpostal);
   
   COMMIT;
 
END;
/

---------------------V20140728.105300__DDL_GARNUCHE_v_dossier_inscriptions.sql----------------------

/* Formatted on 2014/07/16 09:28 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW GARNUCHE.v_dossier_inscriptions (hist_numero,
                                                     idipl_type_inscription,
                                                     idipl_passage_conditionnel,
                                                     fgra_code,
                                                     idipl_annee_suivie,
                                                     fdip_libelle,
                                                     res_libelle
                                                    )
AS
   SELECT UNIQUE idi.hist_numero, idi.idipl_type_inscription,
                 idi.idipl_passage_conditionnel, fdi.fgra_code,
                 idi.idipl_annee_suivie,
                    fdi.fdip_libelle
                 || DECODE (fsp.fspn_libelle,
                            NULL, '',
                            ' ' || fsp.fspn_libelle
                           ),
                 res_libelle
            FROM garnuche.insc_dipl idi,
                 scolarite.scol_formation_specialisation fsp,
                 scolarite.scol_formation_diplome fdi,
                 garnuche.resultat r
           WHERE fsp.fdip_code = fdi.fdip_code
             AND idi.dspe_code = fsp.fspn_key
             AND r.res_code(+) = idi.res_code;



-------------------------V20140728.105341__DDL_MAJ_TRIGGERS_SECRETARIAT.sql-------------------------
CREATE OR REPLACE FORCE VIEW "GRHUM"."SECRETARIAT" ("C_STRUCTURE", "NO_INDIVIDU", "D_CREATION", "D_MODIFICATION") AS 
  SELECT  ra.c_structure, i.no_individu, ra.d_creation, ra.d_modification from grhum.repart_association ra
      inner join grhum.individu_ulr i on ra.pers_id = i.pers_id
        inner join grhum.association a on ra.ass_id = a.ass_id
          where upper(convert(a.ass_code,'US7ASCII')) = 'SECRETAIRE A'
            and ra.ras_d_ouverture <= sysdate
            and NVL(ra.ras_d_fermeture,to_date('31122199','DDMMYYYY')) >= sysdate;

CREATE OR REPLACE TRIGGER "GRHUM"."SECRETARIAT_INSERT" 
INSTEAD OF INSERT ON GRHUM.SECRETARIAT
REFERENCING NEW AS n

FOR EACH ROW
DECLARE
   pers_id_createur number;
   ass_id_secretaire number;
   pers_id_secretaire number;
   cpt number;
BEGIN

-- CONTROLE DES PARAMETRES

if(:n.c_structure is NULL) then
  RAISE_APPLICATION_ERROR(-20001,'SECRETARIAT : c_structure ne peut pas etre null');
end if;

if(:n.no_individu is NULL) then
  RAISE_APPLICATION_ERROR(-20002,'SECRETARIAT : no_individu ne peut pas etre null');
end if;

if(:n.d_creation is NULL) then
  RAISE_APPLICATION_ERROR(-20003,'SECRETARIAT : d_creation ne peut pas etre null');
end if;

if(:n.d_modification is NULL) then
  RAISE_APPLICATION_ERROR(-20004,'SECRETARIAT : d_modification ne peut pas etre null');
end if;

-- VERIFICATION DE LA VALIDITE DU C_STRUCTURE
select count(*) into cpt from structure_ulr where c_structure = :n.c_structure;
if(cpt=0) then
  RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : aucune structure correspondante au code fourni');
end if;

-- Recup du pers_id de GRHUM_CREATEUR
SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM GRHUM.COMPTE WHERE CPT_LOGIN IN
  (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');

-- RECUP DE L'ASS_ID SECRETAIRE
select min(ass_id) into ass_id_secretaire from grhum.association where upper(convert(ass_code,'US7ASCII')) = 'SECRETAIRE A';

-- RECUP DU PERS_ID DU SECRETAIRE
select count(*) into cpt from grhum.individu_ulr where no_individu = :n.no_individu;
if(cpt=0) then
  RAISE_APPLICATION_ERROR(-20005,'SECRETARIAT : aucun individu correspondant au numero fourni');
else
  select pers_id into pers_id_secretaire from grhum.individu_ulr where no_individu = :n.no_individu;
end if;

-- ON VERIFIE QUE L'ENREGISTREMENT N'EXISTE PAS DEJA
SELECT count(*) into cpt from grhum.repart_association
  where ass_id = ass_id_secretaire
    and c_structure = :n.c_structure
    and pers_id = pers_id_secretaire
    and ras_d_ouverture <= sysdate
    and (
      ras_d_fermeture is null
      or ras_d_fermeture >= sysdate
    );

if(cpt = 0) then
  -- RAISE_APPLICATION_ERROR(-20006,'SECRETARIAT : l''enregistrement existe deja');
  INSERT INTO GRHUM.REPART_ASSOCIATION
  (
    RAS_ID,
    PERS_ID,
    C_STRUCTURE,
    ASS_ID,
    RAS_RANG,
    RAS_COMMENTAIRE,
    RAS_D_OUVERTURE,
    RAS_D_FERMETURE,
    D_CREATION,
    D_MODIFICATION,
    RAS_QUOTITE,
    PERS_ID_CREATION,
    PERS_ID_MODIFICATION
  )
  VALUES
  (
    repart_association_seq.nextval,
    pers_id_secretaire,
    :n.c_structure,
    ass_id_secretaire,
    1,
    null,
    sysdate,
    null,
    :n.d_creation,
    :n.d_modification,
    null,
    pers_id_createur,
    pers_id_createur
  );
end if;

END;
/
ALTER TRIGGER "GRHUM"."SECRETARIAT_INSERT" ENABLE;

CREATE OR REPLACE TRIGGER "GRHUM"."SECRETARIAT_UPDATE" 
INSTEAD OF UPDATE ON GRHUM.SECRETARIAT
REFERENCING NEW AS n old as o
BEGIN
  -- on fait rien car on ne fait jamais d'update sur cette table
  null;
END;
/
ALTER TRIGGER "GRHUM"."SECRETARIAT_UPDATE" ENABLE;

CREATE OR REPLACE TRIGGER "GRHUM"."SECRETARIAT_DELETE" 
INSTEAD OF DELETE ON GRHUM.SECRETARIAT
REFERENCING old as o
FOR EACH ROW
DECLARE
   pers_id_createur number;
   ass_id_secretaire number;
   pers_id_secretaire number;
   cpt number;
BEGIN

  -- Recup du pers_id de GRHUM_CREATEUR
  SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM GRHUM.COMPTE WHERE CPT_LOGIN IN
    (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');

  -- RECUP DE L'ASS_ID SECRETAIRE
  select ass_id into ass_id_secretaire from grhum.association where upper(convert(ass_code,'US7ASCII')) = 'SECRETAIRE A';

  -- RECUP DU PERS_ID DU SECRETAIRE
  select pers_id into pers_id_secretaire from grhum.individu_ulr where no_individu = :o.no_individu;

  update grhum.repart_association set ras_d_fermeture = sysdate
  where ass_id = ass_id_secretaire
    and c_structure = :o.c_structure
    and pers_id = pers_id_secretaire
    and ras_d_ouverture <= sysdate
    and (
      ras_d_fermeture is null
      or ras_d_fermeture >= sysdate
    );

END;
/
ALTER TRIGGER "GRHUM"."SECRETARIAT_DELETE" ENABLE;

--------------------V20140728.105420__DDL_GRHUM_VIEWS_PERSONNEL_PROLONGATION.sql--------------------
 CREATE OR REPLACE FORCE VIEW "GRHUM"."V_PERSONNEL_ACTUEL_PROLONG" ("NO_DOSSIER_PERS", "D_FIN_PROLONG") AS 
-- Emeritat
  select E.no_dossier_pers,E.D_FIN_EMERITAT from MANGUE.EMERITAT E
    WHERE E.D_DEB_EMERITAT <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
    AND (E.D_FIN_EMERITAT IS NULL OR E.D_FIN_EMERITAT >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY') )
    AND E.NO_ARRETE_EMERITAT is not null
    AND E.TEM_VALIDE = 'O'
UNION
-- Recul d'age
    select R.no_dossier_pers,nvl(R.D_FIN_EXECUTION_RECUL_AGE,R.D_FIN_RECUL_AGE) from MANGUE.RECUL_AGE R
    WHERE R.D_DEB_RECUL_AGE <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
    AND (R.D_FIN_RECUL_AGE IS NULL OR R.D_FIN_RECUL_AGE >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY') )
    AND R.NO_ARR_RECUL_AGE is not null
    AND R.TEM_VALIDE = 'O'
UNION
-- Prologation d'activité
    select P.no_dossier_pers,nvl(P.D_FIN_EXEC_PROLONGATION,P.D_FIN_PROLONGATION) from MANGUE.PROLONGATION_ACTIVITE P
    WHERE P.D_DEB_PROLONGATION <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
    AND (P.D_FIN_PROLONGATION IS NULL OR P.D_FIN_PROLONGATION >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY') )
    AND P.NO_ARR_PROLONGATION is not null
    AND P.TEM_VALIDE = 'O';
 

  CREATE OR REPLACE FORCE VIEW "GRHUM"."V_PERSONNEL_ANCIEN_PROLONG" ("NO_DOSSIER_PERS", "D_FIN_PROLONG") AS
-- Emeritat 
  select E.no_dossier_pers,E.D_FIN_EMERITAT from MANGUE.EMERITAT E
    WHERE E.NO_ARRETE_EMERITAT is not null
    AND E.TEM_VALIDE = 'O'
    AND E.no_dossier_pers not in(select no_dossier_pers from GRHUM.V_PERSONNEL_ACTUEL_PROLONG)
UNION
-- Recul d'age
    select R.no_dossier_pers,nvl(R.D_FIN_EXECUTION_RECUL_AGE,R.D_FIN_RECUL_AGE) from MANGUE.RECUL_AGE R
    WHERE R.NO_ARR_RECUL_AGE is not null
    AND R.TEM_VALIDE = 'O'
    AND R.no_dossier_pers not in (select no_dossier_pers from GRHUM.V_PERSONNEL_ACTUEL_PROLONG)
UNION
-- Prologation d'activité
    select P.no_dossier_pers,nvl(P.D_FIN_EXEC_PROLONGATION,P.D_FIN_PROLONGATION) from MANGUE.PROLONGATION_ACTIVITE P
    WHERE P.NO_ARR_PROLONGATION is not null
    AND P.TEM_VALIDE = 'O'
    AND P.no_dossier_pers not in (select no_dossier_pers from GRHUM.V_PERSONNEL_ACTUEL_PROLONG);
----------------------------V20140728.105455__DDL_MAJ_STRUCTURE_ULR.sql-----------------------------
-- ---------------------------------------------------------
-- Modification de la table STRUCTURE_ULR
-- ---------------------------------------------------------

declare
begin

	GRHUM.DROP_OBJECT('GRHUM', 'TRG_RNE_COMMUNE', 'TRIGGER');
	
	GRHUM.ADM_ADD_COLUMN('GRHUM', 'STRUCTURE_ULR', 'SACT_ID', 'NUMBER', NULL, NULL);
	
	GRHUM.ADM_CREATE_FK_IFNOTEXISTS('GRHUM', 'FK_STRUCTURE_SACT', 'STRUCTURE_ULR', 'SACT_ID', 'GRHUM', 'SECTEUR_ACTIVITE', 'SACT_ID');
		
	GRHUM.ADM_REORG_TABLE('GRHUM', 'STRUCTURE_ULR','INDX_GRHUM');
end;
/


COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.SACT_ID IS 'FK vers la table GRHUM.SECTEUR_ACTIVITE';

ALTER TABLE GRHUM.RNE MODIFY (CODE_INSEE VARCHAR2(5));

COMMIT;

------------------------V20140728.105520__DDL_MAJ_INS_STRUCTURE_ULR_SYNC.sql------------------------
create or replace PROCEDURE             GRHUM.INS_STRUCTURE_ULR_SYNC
(
  cstructure                OUT STRUCTURE_ULR.c_structure%TYPE,
  persid                    OUT STRUCTURE_ULR.pers_id%TYPE,
  INS_LL_STRUCTURE          STRUCTURE_ULR.ll_structure%TYPE,
  INS_LC_STRUCTURE          STRUCTURE_ULR.lc_structure%TYPE,
  INS_C_TYPE_STRUCTURE      STRUCTURE_ULR.c_type_structure%TYPE,
  INS_C_STRUCTURE_PERE      STRUCTURE_ULR.c_structure_pere%TYPE,
  INS_C_TYPE_ETABLISSEMEN   STRUCTURE_ULR.c_type_etablissemen%TYPE,
  INS_C_ACADEMIE            STRUCTURE_ULR.c_academie%TYPE,
  INS_C_STATUT_JURIDIQUE    STRUCTURE_ULR.c_statut_juridique%TYPE,
  INS_C_RNE                 STRUCTURE_ULR.c_rne%TYPE,
  INS_SIRET                 STRUCTURE_ULR.siret%TYPE,
  INS_SIREN                 STRUCTURE_ULR.siren%TYPE,
  INS_C_NAF                 STRUCTURE_ULR.c_naf%TYPE,
  INS_C_TYPE_DECISION_STR   STRUCTURE_ULR.c_type_decision_str%TYPE,
  INS_REF_EXT_ETAB          STRUCTURE_ULR.ref_ext_etab%TYPE,
  INS_REF_EXT_COMP          STRUCTURE_ULR.ref_ext_comp%TYPE,
  INS_REF_EXT_CR            STRUCTURE_ULR.ref_ext_cr%TYPE,
  INS_REF_DECISION          STRUCTURE_ULR.ref_decision%TYPE,
  INS_DATE_DECISION         STRUCTURE_ULR.date_decision%TYPE,
  INS_DATE_OUVERTURE        STRUCTURE_ULR.date_ouverture%TYPE,
  INS_DATE_FERMETURE        STRUCTURE_ULR.date_fermeture%TYPE,
  INS_STR_ORIGINE            STRUCTURE_ULR.str_origine%TYPE,
  INS_STR_PHOTO              STRUCTURE_ULR.str_photo%TYPE,
  INS_STR_ACTIVITE            STRUCTURE_ULR.str_activite%TYPE,
  INS_GRP_OWNER             STRUCTURE_ULR.grp_owner%TYPE,
  INS_GRP_RESPONSABLE        STRUCTURE_ULR.grp_responsable%TYPE,
  INS_GRP_FORME_JURIDIQUE     STRUCTURE_ULR.grp_forme_juridique%TYPE,
  INS_GRP_CAPITAL              STRUCTURE_ULR.grp_capital%TYPE,
  INS_GRP_CA                  STRUCTURE_ULR.grp_ca%TYPE,
  INS_GRP_EFFECTIFS         STRUCTURE_ULR.grp_effectifs%TYPE,
  INS_GRP_CENTRE_DECISION    STRUCTURE_ULR.grp_centre_decision%TYPE,
  INS_GRP_APE_CODE          STRUCTURE_ULR.grp_ape_code%TYPE,
  INS_GRP_APE_CODE_BIS      STRUCTURE_ULR.grp_ape_code_bis%TYPE,
  INS_GRP_APE_CODE_COMP      STRUCTURE_ULR.grp_ape_code_comp%TYPE,
  INS_GRP_ACCES             STRUCTURE_ULR.grp_acces%TYPE,
  INS_GRP_ALIAS             STRUCTURE_ULR.grp_alias%TYPE,
  INS_GRP_RESPONSABILITE     STRUCTURE_ULR.grp_responsabilite%TYPE,
  INS_GRP_TRADEMARQUE         STRUCTURE_ULR.grp_trademarque%TYPE,
  INS_GRP_WEBMESTRE         STRUCTURE_ULR.grp_webmestre%TYPE,
  INS_GRP_FONCTION1         STRUCTURE_ULR.grp_fonction1%TYPE,
  INS_GRP_FONCTION2         STRUCTURE_ULR.grp_fonction2%TYPE,
  INS_ORG_ORDRE             STRUCTURE_ULR.org_ordre%TYPE,
  INS_GRP_MOTS_CLEFS        STRUCTURE_ULR.grp_mots_clefs%TYPE
)
IS
-- ------------
-- DECLARATIONS
-- ------------
cpt INTEGER;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

/*SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
WHERE ll_structure=ins_ll_structure;
IF (cpt>0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_STRUCTURE_ULR : Libelle existant');
END IF;*/

IF (ins_c_structure_pere IS NOT NULL) THEN
    SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
    WHERE c_structure=ins_c_structure_pere;
    IF (cpt=0) THEN
       RAISE_APPLICATION_ERROR(-20001,'INS_STRUCTURE_ULR : C_structure_pere inconnu');
    END IF;
END IF;


--SELECT MAX(TO_NUMBER(c_structure))+1 INTO cstructure FROM STRUCTURE_ULR;--+1

SELECT SEQ_STRUCTURE_ULR_SYNC.NEXTVAL INTO cstructure FROM dual;

--SELECT MAX(pers_id)+1 INTO persid FROM v_personne;--+1

SELECT SEQ_PERSONNE.NEXTVAL INTO persid FROM dual;

INSERT INTO STRUCTURE_ULR VALUES (
  cstructure,
  persid,
  INS_LL_STRUCTURE,
  INS_LC_STRUCTURE,
  INS_C_TYPE_STRUCTURE,
  INS_C_STRUCTURE_PERE,
  INS_C_TYPE_ETABLISSEMEN,
  INS_C_ACADEMIE,
  INS_C_STATUT_JURIDIQUE,
  INS_C_RNE,
  INS_SIRET,
  INS_SIREN,
  INS_C_NAF,
  INS_C_TYPE_DECISION_STR,
  INS_REF_EXT_ETAB,
  INS_REF_EXT_COMP,
  INS_REF_EXT_CR,
  INS_REF_DECISION,
  INS_DATE_DECISION,
  INS_DATE_OUVERTURE,
  INS_DATE_FERMETURE,
  INS_STR_ORIGINE,
  INS_STR_PHOTO,
  INS_STR_ACTIVITE,
  INS_GRP_OWNER,
  INS_GRP_RESPONSABLE,
  INS_GRP_FORME_JURIDIQUE,
  INS_GRP_CAPITAL,
  INS_GRP_CA,
  INS_GRP_EFFECTIFS,
  INS_GRP_CENTRE_DECISION,
  INS_GRP_APE_CODE,
  INS_GRP_APE_CODE_BIS,
  INS_GRP_APE_CODE_COMP,
  INS_GRP_ACCES,
  INS_GRP_ALIAS,
  INS_GRP_RESPONSABILITE,
  INS_GRP_TRADEMARQUE,
  INS_GRP_WEBMESTRE,
  INS_GRP_FONCTION1,
  INS_GRP_FONCTION2,
  INS_ORG_ORDRE,
  INS_GRP_MOTS_CLEFS,
  SYSDATE,
  SYSDATE,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  'N',
  'N',
  'O',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,   -- tva_intracom
  NULL,   -- str_accueil
  NULL,   -- str_recherche
  NULL,   -- num_rafp
  INS_LC_STRUCTURE,     -- str_affichage
  NULL,   -- gencod
  NULL,   -- pers_id_creation
  NULL,   -- pers_id_modification
  NULL,   -- STR_STATUT
  NULL,   -- ROM_ID
  NULL,   -- STR_DESCRIPTION
  null,   -- TEM_SIRET_PROVISOIRE
  null,   -- ID_RNSR
  null	  -- SACT_ID	
);
COMMIT;

END;
/
-----------------------V20140729.133834__DDL_Taille_cpt_pawwsd_pr_sha512.sql------------------------
alter table
   grhum.compte
modify (
   CPT_PASSWD  VARCHAR2(200),
   CPT_PASSWD_CLAIR  VARCHAR2(200)
);


-------------------------V20140729.142023__DDL_Taille_pwdh_pour_sha512.sql--------------------------


alter table
   GRHUM.PASSWORD_HISTORY
modify (
   PWDH_PASSWD  VARCHAR2(200)
);


----------------------V20140729.142922__DDL_Taille_compte_temp_pour_sha512.sql----------------------
alter table
   grhum.compte_temp
modify (
   CPT_PASSWD  VARCHAR2(200),
   CPT_PASSWD_CLAIR  VARCHAR2(200)
);


----------------------V20140805.120916__DDL_UPDATE_TABLE_CPTE_PASSWDHSITO.sql-----------------------
DECLARE
BEGIN
	GRHUM.ADM_REORG_TABLE('GRHUM', 'COMPTE','INDX_GRHUM');

	GRHUM.ADM_REORG_TABLE('GRHUM', 'PASSWORD_HISTORY','INDX_GRHUM');
END;
/
-------------------V20140805.122139__DDL_Ajout_colonne_potentiel_brut_contrat.sql-------------------
declare
begin
	GRHUM.ADM_ADD_COLUMN('GRHUM', 'TYPE_CONTRAT_TRAVAIL', 'POTENTIEL_BRUT', 'NUMBER', 0, 'NOT NULL');
	GRHUM.ADM_REORG_TABLE('GRHUM', 'TYPE_CONTRAT_TRAVAIL','INDX_GRHUM');
end;
/


COMMENT ON COLUMN GRHUM.TYPE_CONTRAT_TRAVAIL.POTENTIEL_BRUT IS 'Potentiel brut (en heures) associé aux types de contrat enseignant.';





