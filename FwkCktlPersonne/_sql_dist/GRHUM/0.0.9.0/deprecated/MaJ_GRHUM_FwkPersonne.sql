--
-- Patch de GRHUM du 03/09/2009 � executer depuis le user GRHUM
--
-- Description : nouveau schema de GRHUM pour le framework Personne
-- ATTENTION : la table COMPTE_NEW remplace la table COMPTE
--             les emails sont dans la table COMPTE_EMAIL (cpt_email, cpt_domaine et cpt_uid_gid restent pour le moment pour compatibilit�)

SET DEFINE OFF

INSERT INTO GRHUM.DB_VERSION VALUES(16, '1.6.0.0', TO_DATE('03/09/2009', 'DD/MM/YYYY'),SYSDATE,'Mise � jour de importante de GRHUM pour le framework Personne');


--
-- GRHUM_PARAMETRES
--
INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE, PARAM_KEY, PARAM_VALUE,PARAM_COMMENTAIRES) 
SELECT  130, 'ANNUAIRE_FOU_COMPTE_AUTO', 'N', 'Indique si un compte d''acces doit etre cr�� en automatique lors de la cr�ation d''un fournisseur (O/N).' FROM DUAL 
WHERE NOT EXISTS(SELECT * FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_COMPTE_AUTO');



--
-- BANQUE
--
UPDATE GRHUM.BANQUE SET DOMICILIATION=UPPER(DOMICILIATION);


--
-- PAYS_INDICATIF
--
-- nettoyer pays_indicatif (c_pays=327  mettre 229 au lieu de 0)
UPDATE GRHUM.PAYS_INDICATIF SET INDICATIF = 229 WHERE C_PAYS=327 AND INDICATIF=0;



--
-- TYPE_GROUPE
--
INSERT INTO GRHUM.TYPE_GROUPE VALUES('RE','R�f�rentiel',SYSDATE,SYSDATE,'N');



--
-- REPART_TYPE_GROUPE
--
-- rem : les groupes de type 'R�f�rentiel' sont issus des groupes institutionnels (Services, Laboratoires) de l'�tablissement
INSERT INTO GRHUM.REPART_TYPE_GROUPE
SELECT UNIQUE C_STRUCTURE,'RE',SYSDATE,SYSDATE FROM GRHUM.REPART_TYPE_GROUPE WHERE TGRP_CODE IN ('S','LA');

-- rem : plus le groupe 'Divers'
INSERT INTO GRHUM.REPART_TYPE_GROUPE
SELECT PARAM_VALUE,'RE',SYSDATE,SYSDATE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_STRUCTURE_DEFAUT';

-- ajouter le type de groupe FO pour les groupes des fournisseurs : param_key like 'ANNUAIRE_FOU%'
INSERT INTO GRHUM.REPART_TYPE_GROUPE
(SELECT C_STRUCTURE, 'FO', SYSDATE, SYSDATE FROM STRUCTURE_ULR WHERE C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_PHYSIQUE')
AND NOT EXISTS (SELECT * FROM REPART_TYPE_GROUPE WHERE TGRP_CODE='FO' AND C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_PHYSIQUE')));
  
INSERT INTO GRHUM.REPART_TYPE_GROUPE (
SELECT C_STRUCTURE, 'FO', SYSDATE, SYSDATE FROM STRUCTURE_ULR WHERE C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_MORALE')
AND NOT EXISTS (SELECT * FROM REPART_TYPE_GROUPE WHERE TGRP_CODE='FO' AND C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_MORALE')));

INSERT INTO GRHUM.REPART_TYPE_GROUPE (
SELECT C_STRUCTURE, 'FO', SYSDATE, SYSDATE FROM STRUCTURE_ULR WHERE C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_ENCOURS_VALIDE')
AND NOT EXISTS (SELECT * FROM REPART_TYPE_GROUPE WHERE TGRP_CODE='FO' AND C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_ENCOURS_VALIDE')));

INSERT INTO GRHUM.REPART_TYPE_GROUPE
(SELECT C_STRUCTURE, 'FO', SYSDATE, SYSDATE FROM STRUCTURE_ULR WHERE C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_ARCHIVES')
AND NOT EXISTS (SELECT * FROM REPART_TYPE_GROUPE WHERE TGRP_CODE='FO' AND C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_ARCHIVES')));

INSERT INTO GRHUM.REPART_TYPE_GROUPE (
SELECT C_STRUCTURE, 'FO', SYSDATE, SYSDATE FROM STRUCTURE_ULR WHERE C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_EXTERNE')
AND NOT EXISTS (SELECT * FROM REPART_TYPE_GROUPE WHERE TGRP_CODE='FO' AND C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_EXTERNE')));

INSERT INTO GRHUM.REPART_TYPE_GROUPE (
SELECT C_STRUCTURE, 'FO', SYSDATE, SYSDATE FROM STRUCTURE_ULR WHERE C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_INTERNE')
AND NOT EXISTS (SELECT * FROM REPART_TYPE_GROUPE WHERE TGRP_CODE='FO' AND C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_INTERNE')));                   

-- le groupe racine des  Fournisseur
INSERT INTO GRHUM.REPART_TYPE_GROUPE (SELECT C_STRUCTURE_PERE, 'FO', SYSDATE, SYSDATE FROM STRUCTURE_ULR WHERE C_STRUCTURE =(SELECT C_STRUCTURE_PERE FROM STRUCTURE_ULR WHERE C_STRUCTURE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_FOU_VALIDE_PHYSIQUE')));

 
 
--
-- TYPE_CRYPTAGE
--
CREATE TABLE GRHUM.TYPE_CRYPTAGE
(
TCRY_ORDRE            NUMBER(2)        NOT NULL,
TCRY_LIBELLE          VARCHAR2(200)    NOT NULL,
TCRY_JAVA_METHODE     VARCHAR2(2000),
D_DEB_VAL             DATE             NOT NULL,
D_FIN_VAL             DATE            
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

COMMENT ON TABLE GRHUM.TYPE_CRYPTAGE IS 'Nomenclature des types de cryptage disponible.';

COMMENT ON COLUMN GRHUM.TYPE_CRYPTAGE.TCRY_ORDRE IS 'Clef primaire du type de cryptage.';

COMMENT ON COLUMN GRHUM.TYPE_CRYPTAGE.TCRY_LIBELLE IS 'Libell� du type de cryptage.';

COMMENT ON COLUMN GRHUM.TYPE_CRYPTAGE.TCRY_JAVA_METHODE IS 'M�thode Java utilis�e pour le type de cryptage.';

COMMENT ON COLUMN GRHUM.TYPE_CRYPTAGE.D_DEB_VAL IS 'Date de d�but de validit� de l''occurrence.';

COMMENT ON COLUMN GRHUM.TYPE_CRYPTAGE.D_FIN_VAL IS 'Date de d�but de validit� de l''occurrence.';


CREATE UNIQUE INDEX GRHUM.PK_TYPE_CRYPTAGE ON GRHUM.TYPE_CRYPTAGE
(TCRY_ORDRE)
LOGGING
TABLESPACE INDX_GRHUM
NOPARALLEL;

CREATE UNIQUE INDEX GRHUM.UK_TCRY_LIBELLE ON GRHUM.TYPE_CRYPTAGE
(TCRY_LIBELLE)
LOGGING
TABLESPACE INDX_GRHUM
NOPARALLEL;

ALTER TABLE GRHUM.TYPE_CRYPTAGE ADD (
  CONSTRAINT PK_TYPE_CRYPTAGE
 PRIMARY KEY
 (TCRY_ORDRE));
 
 
-- Sequence
CREATE SEQUENCE GRHUM.TYPE_CRYPTAGE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCACHE NOCYCLE NOORDER;

-- Data
INSERT INTO GRHUM.TYPE_CRYPTAGE SELECT GRHUM.TYPE_CRYPTAGE_SEQ.NEXTVAL,'UNIX','org.cocktail.crypto.jcrypt.Jcrypt.crypt',TO_DATE('01/01/1980','DD/MM/YYYY'),NULL FROM DUAL;
INSERT INTO GRHUM.TYPE_CRYPTAGE SELECT GRHUM.TYPE_CRYPTAGE_SEQ.NEXTVAL,'MD5','org.cocktail.crypto.md5.MD5Crypt.crypt',TO_DATE('01/01/1980','DD/MM/YYYY'),NULL FROM DUAL;



--
-- VLANS
--
ALTER TABLE GRHUM.VLANS ADD TCRY_ORDRE NUMBER(2);

COMMENT ON COLUMN GRHUM.VLANS.TCRY_ORDRE IS 'Clef �trang�re vers le type de cryptage.';

ALTER TABLE GRHUM.VLANS ADD (
  CONSTRAINT VLANS_REF_TYPE_CRYPTAGE 
 FOREIGN KEY (TCRY_ORDRE) 
 REFERENCES TYPE_CRYPTAGE (TCRY_ORDRE));

ALTER TABLE GRHUM.VLANS ADD TVPN_CODE VARCHAR2(5);

COMMENT ON COLUMN GRHUM.VLANS.TVPN_CODE IS 'Clef �trang�re vers le type de VPN.';

ALTER TABLE GRHUM.VLANS ADD DOMAINE VARCHAR2(200);

COMMENT ON COLUMN GRHUM.VLANS.DOMAINE IS 'Domaine associ� au VLAN (ex : cocktail.org).';

COMMENT ON COLUMN GRHUM.VLANS.D_CREATION IS 'Date de cr�ation de l''enregistrement.';

COMMENT ON COLUMN GRHUM.VLANS.D_MODIFICATION IS 'Date de modification de l''enregistrement.';

ALTER TABLE GRHUM.VLANS ADD (
  CONSTRAINT VLANS_REF_TYPE_VPN 
 FOREIGN KEY (TVPN_CODE) 
 REFERENCES GRHUM.TYPE_VPN (TVPN_CODE));
 
-- mettre a jour vlans avec type_cryptage
UPDATE GRHUM.VLANS SET TVPN_CODE='NO' WHERE C_VLAN='X';
-- charge aux �tablissements de mettre le bon type de cryptage en fonction du VLAN
UPDATE GRHUM.VLANS SET TCRY_ORDRE = (SELECT TCRY_ORDRE FROM TYPE_CRYPTAGE WHERE TCRY_LIBELLE='UNIX') WHERE C_VLAN in ('P','R','E','X');       

-- M�J domaine
UPDATE GRHUM.VLANS SET DOMAINE = (SELECT PARAM_VALUE FROM GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_DOMAINE_PRINCIPAL') WHERE C_VLAN IN ('P','R');

UPDATE GRHUM.VLANS SET DOMAINE = 'etudiant.univ-lr.fr' WHERE C_VLAN = 'E';



--
-- COMPTE_NEW et COMPTE_EMAIL
--
-- Mise � niveau du sch�ma de ces 2 tables

CREATE OR REPLACE FUNCTION GRHUM.CreateCompteNew RETURN VARCHAR2
AS
BEGIN
     RETURN 'CREATE TABLE GRHUM.COMPTE_NEW(CPT_ORDRE NUMBER NOT NULL,PERS_ID NUMBER NOT NULL,CPT_LOGIN VARCHAR2(20) NOT NULL,CPT_PASSWD VARCHAR2(50) NOT NULL,CPT_PASSWD_CLAIR VARCHAR2(50),CPT_CRYPTE CHAR(1) DEFAULT ''N'' NOT NULL,CPT_UID NUMBER,CPT_GID NUMBER,CPT_HOME VARCHAR2(50),CPT_SHELL VARCHAR2(50),CPT_CONNEXION CHAR(1) DEFAULT ''N'' NOT NULL,CPT_TYPE_CONNEXION VARCHAR2(3),CPT_VLAN VARCHAR2(5) NOT NULL,CPT_CHARTE CHAR(1) DEFAULT ''N'' NOT NULL,CPT_VALIDE CHAR(1) DEFAULT ''N'' NOT NULL,CPT_DEBUT_VALIDE DATE DEFAULT SYSDATE,CPT_FIN_VALIDE DATE,CPT_COMMENTAIRE VARCHAR2(200),CPT_TYPE VARCHAR2(3),CPT_PRINCIPAL CHAR(1) DEFAULT ''O'' NOT NULL,CPT_LISTE_ROUGE CHAR(1) DEFAULT ''N'' NOT NULL,D_CREATION DATE DEFAULT SYSDATE NOT NULL,CPT_CREATEUR NUMBER,D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL,CPT_MODIFICATEUR NUMBER,CPT_UID_GID NUMBER,TVPN_CODE VARCHAR2(5) DEFAULT (''W3VPN'') NOT NULL,TCRY_ORDRE NUMBER(2),CPT_EMAIL VARCHAR2(40),CPT_DOMAINE VARCHAR2(40)) TABLESPACE DATA_GRHUM LOGGING NOCACHE NOPARALLEL';     
END;
/

CREATE OR REPLACE FUNCTION GRHUM.AlterCompteNew RETURN VARCHAR2
AS
BEGIN
	 RETURN 'ALTER TABLE GRHUM.COMPTE_NEW ADD (TCRY_ORDRE NUMBER(2),CPT_EMAIL VARCHAR2(40),CPT_DOMAINE VARCHAR2(40))';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.CreateCompteEmail RETURN VARCHAR2
AS
BEGIN
     RETURN 'CREATE TABLE GRHUM.COMPTE_EMAIL(CEM_KEY NUMBER NOT NULL,CPT_ORDRE NUMBER NOT NULL,CEM_EMAIL VARCHAR2(60),CEM_DOMAINE VARCHAR2(40) DEFAULT NULL,CEM_PRIORITE NUMBER(1),CEM_ALIAS CHAR(1) DEFAULT ''N'' NOT NULL,D_CREATION DATE DEFAULT SYSDATE NOT NULL,D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL) LOGGING NOCOMPRESS NOCACHE NOPARALLEL NOMONITORING';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.CreateCompteEmailSeq RETURN VARCHAR2
AS
BEGIN
     RETURN 'CREATE SEQUENCE GRHUM.COMPTE_EMAIL_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCACHE NOCYCLE NOORDER';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.AlterCompteEmail RETURN VARCHAR2
AS
BEGIN
	 RETURN 'ALTER TABLE GRHUM.COMPTE_EMAIL MODIFY CEM_DOMAINE DEFAULT NULL';
END;
/

CREATE OR REPLACE PROCEDURE GRHUM.MaJTablesCompte
AS
nbr INTEGER;
BEGIN 

      --
      -- COMPTE_NEW (Table) 
      --
      SELECT COUNT(*) INTO nbr FROM user_tables WHERE table_name = 'COMPTE_NEW'; 
      IF (nbr = 0) THEN
           EXECUTE IMMEDIATE GRHUM.CreateCompteNew;
      ELSE
           EXECUTE IMMEDIATE GRHUM.AlterCompteNew;
      END IF;  

      --
      -- COMPTE_EMAIL (Table) 
      --
      SELECT COUNT(*) INTO nbr FROM user_tables WHERE table_name = 'COMPTE_EMAIL'; 
      IF (nbr = 0) THEN
           EXECUTE IMMEDIATE GRHUM.CreateCompteEmail;
           EXECUTE IMMEDIATE GRHUM.CreateCompteEmailSeq;
      ELSE
           EXECUTE IMMEDIATE GRHUM.AlterCompteEmail;           
      END IF;  
      
END;
/

-- Execution de la proc�dure de mise � jour du sch�ma des tables COMPTE_NEW et COMPTE_EMAIL
BEGIN
	 GRHUM.MaJTablesCompte;
END;
/
COMMIT;

-- Suppression des fonctions et proc�dures de MaJ de la table COMPTE
DROP FUNCTION GRHUM.CreateCompteNew;
DROP FUNCTION GRHUM.AlterCompteNew;
DROP FUNCTION GRHUM.CreateCompteEmail;
DROP FUNCTION GRHUM.CreateCompteEmailSeq;
DROP PROCEDURE GRHUM.MaJTablesCompte;

-- Commentaires
-- COMPTE_NEW
COMMENT ON TABLE GRHUM.COMPTE_NEW IS 'COMPTES informatiques de l''�tablissement.';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_ORDRE IS 'Clef de la table COMPTE';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.PERS_ID IS 'Clef �trang�re de r�f�rence � la personne';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_LOGIN IS 'Login de connexion';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_PASSWD IS 'Mot de passe de connexion au compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_PASSWD_CLAIR IS 'Mot de passe en clair pour les comptes ext�rieurs';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_CRYPTE IS 'Le mot de passe est crypt� (Oui ou Non)';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_UID IS 'UID du compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_GID IS 'GID compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_HOME IS 'R�pertoire du compte (Home Directory)';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_SHELL IS 'Shell du compte (csh, ksh, zsh)';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_CONNEXION IS 'Droit de Connexion Oui ou Non via l''ext�rieur';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_TYPE_CONNEXION IS 'Code du TYPE de CONNEXION';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_VLAN IS 'Code du VLAN du compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_CHARTE IS 'Signature Oui ou Non de la charte d''utilisation du compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_VALIDE IS 'Etat de validit� du compte : Oui, Non, Annul�, en Instance de cr�ation';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_DEBUT_VALIDE IS 'Date de d�but de validit� du compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_FIN_VALIDE IS 'Date de fin de validit� du compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_COMMENTAIRE IS 'Commentaire rattach� au compte';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_TYPE IS 'TYPE de COMPTE (Projet, G�n�rique)';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_PRINCIPAL IS 'S''agit-il du compte principal (Oui ou Non) ?';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_LISTE_ROUGE IS 'Les informations du compte sont-elles priv�es ? (Oui ou Non) ?';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.D_CREATION IS 'Date de cr�ation de l''enregistrement';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_CREATEUR IS 'Pers_Id du createur du compte (Personne physique ou morale)';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.D_MODIFICATION IS 'Date de modification de l''enregistrement';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_MODIFICATEUR IS 'Pers_Id du modificateur du compte (Personne physique ou morale)';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.TVPN_CODE IS 'Code du type de Virtual Private Network';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.TCRY_ORDRE IS 'Clef �trang�re vers le type de cryptage.';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_UID_GID IS 'Attribut conserv� pour historique de l''ancienne table COMPTE.';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_EMAIL IS 'Attribut conserv� pour historique de l''ancienne table COMPTE.';

COMMENT ON COLUMN GRHUM.COMPTE_NEW.CPT_DOMAINE IS 'Attribut conserv� pour historique de l''ancienne table COMPTE.';


-- COMPTE_EMAIL
COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.CEM_KEY IS 'Clef de la table COMPTE_EMAIL';

COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.CPT_ORDRE IS 'Clef �trang�re de r�f�rence au Compte';

COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.CEM_EMAIL IS 'Email rattach� au compte (sans le domaine)';

COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.CEM_DOMAINE IS 'Domaine de l''email';

COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.CEM_PRIORITE IS 'Ordre de prise en compte des diff�rents emails';

COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.CEM_ALIAS IS 'Email enregistr� est un alias ? (O ou N)';

COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.D_CREATION IS 'Date de cr�ation de l''enregistrement';

COMMENT ON COLUMN GRHUM.COMPTE_EMAIL.D_MODIFICATION IS 'Date de modification de l''enregistrement';


--ALTER TABLE GRHUM.COMPTE_NEW ADD TCRY_ORDRE NUMBER(2);

--COMMENT ON COLUMN GRHUM.COMPTE_NEW.TCRY_ORDRE IS 'Clef �trang�re vers le type de cryptage.';

ALTER TABLE GRHUM.COMPTE_NEW ADD (
  CONSTRAINT COMPTE_REF_TYPE_CRYPTAGE 
 FOREIGN KEY (TCRY_ORDRE) 
 REFERENCES TYPE_CRYPTAGE (TCRY_ORDRE));

--ALTER TABLE GRHUM.COMPTE_NEW ADD CONSTRAINT UK_COMPTE_NEW UNIQUE (CPT_LOGIN, CPT_UID, CPT_VLAN, CPT_DEBUT_VALIDE); 

ALTER TABLE GRHUM.COMPTE_NEW MODIFY PERS_ID NULL;

--ALTER TABLE GRHUM.COMPTE_NEW ADD (CONSTRAINT CPT_PERS_ID_NOT_NULL CHECK (pers_id IS NOT NULL) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.COMPTE_NEW MODIFY CPT_CREATEUR NULL;

--ALTER TABLE GRHUM.COMPTE_NEW ADD (CONSTRAINT CPT_CREATEUR_NOT_NULL CHECK (CPT_CREATEUR IS NOT NULL) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.COMPTE_NEW MODIFY CPT_MODIFICATEUR NULL;

--ALTER TABLE GRHUM.COMPTE_NEW ADD (CONSTRAINT CPT_MODIFICATEUR_NOT_NULL CHECK (CPT_MODIFICATEUR IS NOT NULL) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.COMPTE MODIFY CPT_PASSWD VARCHAR2(100);

ALTER TABLE GRHUM.COMPTE_NEW MODIFY CPT_PASSWD VARCHAR2(100);

ALTER TABLE GRHUM.COMPTE_NEW MODIFY CPT_PASSWD_CLAIR VARCHAR2(100);

-- changer cpt_login et cpt_passwd sur compte_new (not null)
ALTER TABLE GRHUM.COMPTE_NEW MODIFY CPT_LOGIN NOT NULL;

ALTER TABLE GRHUM.COMPTE_NEW MODIFY CPT_PASSWD NOT NULL;

-- Sequence
CREATE SYNONYM GRHUM.COMPTE_NEW_SEQ FOR GRHUM.COMPTE_SEQ;


--
-- COMPTE_EMAIL
--

-- contrainte de compte_email vers compte en deferred 
ALTER TABLE GRHUM.COMPTE_EMAIL DROP CONSTRAINT FK_CEM_COMPTE;

ALTER TABLE GRHUM.COMPTE_EMAIL ADD (CONSTRAINT CPT_EMAIL_REF_COMPTE 
 FOREIGN KEY (CPT_ORDRE) 
 REFERENCES GRHUM.COMPTE_NEW (CPT_ORDRE) 
 DEFERRABLE INITIALLY DEFERRED);

--ALTER TABLE GRHUM.COMPTE_EMAIL MODIFY CEM_DOMAINE DEFAULT NULL; 



-- Alimentation de COMPTE_NEW et COMPTE_EMAIL (si elles n'existent pas) � partir des donn�es de COMPTE*
CREATE OR REPLACE PROCEDURE GRHUM.InsertCompteNew
AS

CURSOR c1 IS SELECT * FROM GRHUM.COMPTE;

nbr         INTEGER;
l1          c1%ROWTYPE;
persId      GRHUM.REPART_COMPTE.PERS_ID%TYPE;
cptCrypte   GRHUM.COMPTE_NEW.CPT_CRYPTE%TYPE;
tvpnCode    GRHUM.COMPTE_NEW.TVPN_CODE%TYPE;
persIdGrhum GRHUM.PERSONNE.PERS_ID%TYPE;
cemKey      GRHUM.COMPTE_EMAIL.CEM_KEY%TYPE;
home        GRHUM.COMPTE_NEW.CPT_HOME%TYPE;
shell        GRHUM.COMPTE_NEW.CPT_SHELL%TYPE;
gidNew        GRHUM.COMPTE_NEW.CPT_GID%TYPE;
debutValide    GRHUM.COMPTE_NEW.CPT_DEBUT_VALIDE%TYPE;
finValide    GRHUM.COMPTE_NEW.CPT_FIN_VALIDE%TYPE;

BEGIN 

    SELECT COUNT(*) INTO NBR FROM GRHUM.COMPTE_NEW;
    IF (NBR = 0) THEN

     SELECT PERS_ID INTO persIdGrhum FROM REPART_COMPTE RC, COMPTE C
     WHERE C.CPT_ORDRE = RC.CPT_ORDRE AND C.CPT_LOGIN = (SELECT MIN(PARAM_VALUE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');
    
     OPEN c1;
     LOOP

         -- Curseur Groupe
        FETCH c1 INTO l1;
        EXIT WHEN c1%NOTFOUND;
    
    
        SELECT PERS_ID INTO persId FROM GRHUM.REPART_COMPTE WHERE CPT_ORDRE = l1.cpt_ordre;
    
        IF (L1.CPT_VLAN = 'P' OR L1.CPT_VLAN = 'R') THEN
            cptCrypte := 'O';
        ELSE
            cptCrypte := 'N';
        END IF;

        IF (l1.cpt_vlan='P' OR l1.cpt_vlan='R' OR l1.cpt_vlan='E') THEN
            tvpnCode := 'W3VPN';
        ELSE
            tvpnCode := 'NO';
        END IF;        

        -- R�cup�ration des donn�es de COMPTE_SUITE
        SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre = l1.cpt_ordre;
        IF (nbr != 0) THEN
             SELECT    cpt_home,cpt_shell,cpt_gid INTO home,shell,gidNew FROM COMPTE_SUITE WHERE cpt_ordre = l1.cpt_ordre;
        ELSE
            home := NULL; shell := NULL; gidNew := NULL;
        END IF;
        
        -- R�cup�ration des donn�es de COMPTE_VALIDITE
        SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre = l1.cpt_ordre;
        IF (nbr != 0) THEN         
             SELECT TO_DATE(date_debut,'dd/mm/yyyy'),TO_DATE(date_fin,'dd/mm/yyyy') INTO debutValide,finValide FROM COMPTE_VALIDITE WHERE cpt_ordre = l1.cpt_ordre;
        ELSE
            debutValide := NULL; finValide := NULL;
        END IF;

        
        INSERT INTO GRHUM.COMPTE_NEW(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,tvpn_code)
        VALUES(l1.cpt_ordre,persId,l1.cpt_login,l1.cpt_passwd,cptCrypte,l1.cpt_uid_gid,gidNew,home,shell,l1.cpt_connexion,l1.cpt_vlan,l1.cpt_charte,'O',debutValide,finValide,'O','N',sysdate,persIdGrhum,sysdate,persIdGrhum,tvpnCode);

        IF (l1.cpt_email IS NOT NULL) THEN
            SELECT GRHUM.COMPTE_EMAIL_SEQ.NEXTVAL INTO cemKey FROM dual;
            INSERT INTO COMPTE_EMAIL VALUES(cemKey,l1.cpt_ordre,l1.cpt_email,l1.cpt_domaine,NULL,'N',SYSDATE,SYSDATE);
        END IF;
        
     END LOOP;
     CLOSE c1;
            
    END IF;
      
END;
/

-- Execution de la proc�dure d'insertion des donn�es
BEGIN
     GRHUM.InsertCompteNew;
END;
/
COMMIT;

-- Suppression de la proc�dure 
DROP PROCEDURE GRHUM.InsertCompteNew;

-- M�J des attributs restant pour compatibilit�
UPDATE GRHUM.COMPTE_NEW CN SET
CPT_UID_GID=(SELECT C.CPT_UID_GID FROM GRHUM.COMPTE C WHERE C.CPT_ORDRE = CN.CPT_ORDRE),
CPT_EMAIL=(SELECT C.CPT_EMAIL FROM GRHUM.COMPTE C WHERE C.CPT_ORDRE = CN.CPT_ORDRE),
CPT_DOMAINE=(SELECT C.CPT_DOMAINE FROM GRHUM.COMPTE C WHERE C.CPT_ORDRE = CN.CPT_ORDRE)
WHERE CN.CPT_ORDRE IN (SELECT C.CPT_ORDRE FROM GRHUM.COMPTE C WHERE C.CPT_ORDRE = CN.CPT_ORDRE);

 
-- Substitution des tables COMPTE et COMPTE_NEW
RENAME COMPTE TO COMPTE_OLD;

RENAME COMPTE_NEW TO COMPTE;

ALTER TABLE GRHUM.COMPTE RENAME CONSTRAINT PK_COMPTE_NEW TO PK_COMPTE;


-- Rollback
/*
RENAME COMPTE TO COMPTE_NEW;

RENAME COMPTE_OLD TO COMPTE;
*/

-- Synonymes � cr�er pour COMPTE_NEW
CREATE SYNONYM GRHUM.COMPTE_NEW FOR GRHUM.COMPTE;

-- GRANT de COMPTE vers les autres users Oracle
CREATE OR REPLACE FUNCTION GRHUM.GrantAccords RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO ACCORDS';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantColloque RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT INSERT, UPDATE ON GRHUM.COMPTE TO COLLOQUE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantConvention RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON  GRHUM.COMPTE TO CONVENTION';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantCourrier RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO COURRIER';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantDt3 RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO DT3';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantForums RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO FORUMS';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantGarnuche RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE ON GRHUM.COMPTE TO GARNUCHE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantGed RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO GED';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyAdmin RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO JEFY_ADMIN WITH GRANT OPTION';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyBudget RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO JEFY_BUDGET';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyDepense RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO JEFY_DEPENSE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyMission RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO JEFY_MISSION';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyPaf RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO JEFY_PAF';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyPaye RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO JEFY_PAYE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyRecette RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO JEFY_RECETTE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefyTelephonie RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO JEFY_TELEPHONIE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantJefy02 RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO JEFY02';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantMangue RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO MANGUE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantMatos RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO MATOS';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantOffreFormation RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO OFFRE_FORMATION';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantOpenReports RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO OPENREPORTS';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantPeda RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO PEDA';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantSaphari RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO SAPHARI';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantSaut RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO SAUT';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantScolarite RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT REFERENCES, SELECT ON GRHUM.COMPTE TO SCOLARITE';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantScolIut RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO SCOLIUT';
END;
/

CREATE OR REPLACE FUNCTION GRHUM.GrantStock RETURN VARCHAR2
AS
BEGIN
     RETURN 'GRANT SELECT ON GRHUM.COMPTE TO STOCK';
END;
/


CREATE OR REPLACE PROCEDURE GRHUM.GrantAllUsers
AS
nbr INTEGER;
BEGIN 

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'ACCORDS'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantAccords;
    END IF;  

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'COLLOQUE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantColloque;
    END IF;  

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'CONVENTION'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantConvention;
    END IF;  

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'COURRIER'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantCourrier;
    END IF;  

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'DT3'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantDt3;
    END IF;  

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'FORUMS'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantForums;
    END IF;  

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'GARNUCHE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantGarnuche;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'GED'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantGed;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_ADMIN'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyAdmin;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_BUDGET'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyBudget;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_DEPENSE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyDepense;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_MISSION'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyMission;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_PAF'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyPaf;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_PAYE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyPaye;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_RECETTE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyRecette;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY_TELEPHONIE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefyTelephonie;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'JEFY02'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantJefy02;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'MANGUE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantMangue;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'MATOS'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantMatos;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'OFFRE_FORMATION'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantOffreFormation;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'OPENREPORTS'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantOpenReports;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'PEDA'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantPeda;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'SAPHARI'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantSaphari;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'SAUT'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantSaut;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'SCOLARITE'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantScolarite;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'SCOLIUT'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantScolIut;
    END IF;

    SELECT COUNT(*) INTO nbr FROM all_users WHERE username = 'STOCK'; 
    IF (nbr = 1) THEN
        EXECUTE IMMEDIATE GRHUM.GrantStock;
    END IF;
     
END;
/

BEGIN
	 GRHUM.GrantAllUsers;
END;
/
COMMIT;

-- Suppression des fonctions et proc�dure des GRANT
DROP FUNCTION GRHUM.GrantAccords;
DROP FUNCTION GRHUM.GrantColloque;
DROP FUNCTION GRHUM.GrantConvention;
DROP FUNCTION GRHUM.GrantCourrier;
DROP FUNCTION GRHUM.GrantDt3;
DROP FUNCTION GRHUM.GrantForums;
DROP FUNCTION GRHUM.GrantGarnuche;
DROP FUNCTION GRHUM.GrantGed;
DROP FUNCTION GRHUM.GrantJefyAdmin;
DROP FUNCTION GRHUM.GrantJefyBudget;
DROP FUNCTION GRHUM.GrantJefyDepense;
DROP FUNCTION GRHUM.GrantJefyMission;
DROP FUNCTION GRHUM.GrantJefyPaf;
DROP FUNCTION GRHUM.GrantJefyPaye;
DROP FUNCTION GRHUM.GrantJefyRecette;
DROP FUNCTION GRHUM.GrantJefyTelephonie;
DROP FUNCTION GRHUM.GrantJefy02;
DROP FUNCTION GRHUM.GrantMangue;
DROP FUNCTION GRHUM.GrantMatos;
DROP FUNCTION GRHUM.GrantOffreFormation;
DROP FUNCTION GRHUM.GrantOpenReports;
DROP FUNCTION GRHUM.GrantPeda;
DROP FUNCTION GRHUM.GrantSaphari;
DROP FUNCTION GRHUM.GrantSaut;
DROP FUNCTION GRHUM.GrantScolarite;
DROP FUNCTION GRHUM.GrantScolIut;
DROP FUNCTION GRHUM.GrantStock;
DROP PROCEDURE GRHUM.GrantAllUsers;



--
-- REPART_COMPTE
--
-- Rem : on garde la relation REPART_COMPTE vers COMPTE_OLD pour "historique"
-- a terme, l'actuel COMPTE (anciennement COMPTE_NEW) sera la table de r�f�rence

ALTER TABLE GRHUM.REPART_COMPTE DROP CONSTRAINT FK_AS_COMPTE_REPART_COMPTE;

--ALTER TABLE GRHUM.REPART_COMPTE ADD (CONSTRAINT REPART_COMPTE_REF_COMPTE FOREIGN KEY (CPT_ORDRE) REFERENCES GRHUM.COMPTE (CPT_ORDRE));



--
-- COMPTE_OLD
--
ALTER TABLE SCOLARITE.SCOL_INSCRIPTION_ETUDIANT DROP CONSTRAINT IETUD_REF_CPT;

ALTER TABLE SCOLARITE.SCOL_INSCRIPTION_ETUDIANT ADD (CONSTRAINT IETUD_REF_CPT FOREIGN KEY (CPT_ORDRE) REFERENCES GRHUM.COMPTE (CPT_ORDRE) DISABLE);


--
-- COMPTE_TEMP
--
DROP TABLE GRHUM.COMPTE_TEMP;

CREATE GLOBAL TEMPORARY TABLE COMPTE_TEMP AS SELECT * FROM COMPTE WHERE 0=1;



-- ******************************************************************************************
--                              TRIGGERS (debut)
-- ******************************************************************************************
--DROP TRIGGER GRHUM.TRG_COMPTE;
ALTER TRIGGER TRG_COMPTE RENAME TO TRG_COMPTE_OLD;

CREATE OR REPLACE TRIGGER GRHUM.TRG_COMPTE
BEFORE INSERT OR UPDATE ON GRHUM.COMPTE FOR EACH ROW 
-- CRI 
-- creation : 20/01/2004
-- modification : 13/05/2009
-- 1er d�clencheur, de niveau ligne, qui n'interroge plus la table mutante
-- � la place, il stocke dans la table temporaire les donn�es ins�r�es
-- Cr�ation d'une table temporaire vide de m�me structure que COMPTE
-- CREATE GLOBAL TEMPORARY TABLE COMPTE_TEMP AS SELECT * FROM COMPTE WHERE 0=1;
DECLARE

   cpt              INTEGER;
   newlogin         COMPTE.cpt_login%TYPE;
   newemail         COMPTE.cpt_email%TYPE;
   newdomaine       COMPTE.cpt_domaine%TYPE;
   domaine_princ    COMPTE.CPT_DOMAINE%TYPE;
   vlan_admin       VLANS.C_VLAN%type;
   vlan_recherche   VLANS.C_VLAN%type;
   vlan_etudiant    VLANS.C_VLAN%type;
   vlan_externe     VLANS.C_VLAN%type;
   
BEGIN 

   cpt := 0;
   newlogin := NULL;
   newemail := NULL;
   newdomaine := NULL;

   -- 03/10/2005 pamametrage des codes des VLANS
   select param_value into vlan_admin from grhum_parametres where param_key = 'GRHUM_VLAN_ADMIN';
   select param_value into vlan_recherche from grhum_parametres where param_key = 'GRHUM_VLAN_RECHERCHE';
   select param_value into vlan_etudiant from grhum_parametres where param_key = 'GRHUM_VLAN_ETUD';
   select param_value into vlan_externe from grhum_parametres where param_key = 'GRHUM_VLAN_EXTERNE';
   
   -- contraintes d'ing�grit�s entre le VLAN (not null) et le domaine
   SELECT COUNT(*) INTO cpt
   FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';
     
   IF (cpt != 0) THEN
   
         SELECT param_value INTO domaine_princ
      FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';
      
      -- r�gle d'int�grit� entre le VLAN et le domaine principal
      IF ( ((:NEW.cpt_vlan = vlan_admin) OR (:NEW.cpt_vlan = vlan_recherche)) AND :NEW.cpt_domaine != domaine_princ ) THEN
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilit� entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine principal '||domaine_princ||'.');
      END IF;

     -- int�grit� des comptes ETUDIANTs seulement pour L.R.
        IF ( domaine_princ = 'univ-lr.fr' ) THEN
            IF ( (:NEW.cpt_vlan = vlan_etudiant) AND (:NEW.cpt_domaine NOT IN ('etudiant.univ-lr.fr','etudiut.univ-lr.fr')) ) THEN 
                RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilit� entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine �tudiant ! ');   
             END IF;
        END IF;

     -- int�grit� entre le VLAN eXt�rieur et le domaine
     IF ( (:NEW.cpt_vlan= vlan_externe) AND (:NEW.cpt_domaine = domaine_princ) ) THEN
            RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilit� entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine ext�rieur ! ');        
     END IF;
     
   END IF;
   -- fin contraintes VLAN et domaine

   -- Conversion du login sans les accents
   IF (:NEW.cpt_login IS NOT NULL) THEN
        newlogin := Chaine_Sans_Accents(:NEW.cpt_login);
     :NEW.cpt_login := newlogin;
   END IF;

   -- Conversion du mail sans les accents
   IF (:NEW.cpt_email IS NOT NULL) THEN
        newemail := Chaine_Sans_Accents(:NEW.cpt_email);
     :NEW.cpt_email := newemail;
   END IF;
   
   -- Conversion du domaine sans les accents
   IF (:NEW.cpt_domaine IS NOT NULL) THEN
        newdomaine := Chaine_Sans_Accents(:NEW.cpt_domaine);
     :NEW.cpt_domaine := newdomaine;
   END IF;
     
   -- insertion du nouvel enregistrement dans la table temporaire pour ne pas avoir l'erreur ORA-04091 : table mutante
   -- cas du trigger before insert on COMPTE qui fait un SELECT sur la table COMPTE dans le meme trigger
   INSERT INTO COMPTE_TEMP(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CRYPTE,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,CPT_VALIDE,CPT_PRINCIPAL,CPT_LISTE_ROUGE,D_CREATION,D_MODIFICATION,TVPN_CODE)
   VALUES(:NEW.CPT_ORDRE,:NEW.CPT_UID_GID,:NEW.CPT_LOGIN,:NEW.CPT_PASSWD,:NEW.CPT_CRYPTE,:NEW.CPT_CONNEXION,:NEW.CPT_VLAN,:NEW.CPT_EMAIL,:NEW.CPT_DOMAINE,:NEW.CPT_CHARTE,:NEW.CPT_VALIDE,:NEW.CPT_PRINCIPAL,:NEW.CPT_LISTE_ROUGE,SYSDATE,SYSDATE,:NEW.TVPN_CODE);
                                     
END ;
/

--DROP TRIGGER GRHUM.TRG_COMPTE2;
ALTER TRIGGER TRG_COMPTE2 RENAME TO TRG_COMPTE2_OLD;

CREATE OR REPLACE TRIGGER GRHUM.TRG_COMPTE2 
AFTER INSERT OR UPDATE ON GRHUM.COMPTE
-- auteur : CRI 
-- creation : 20/01/2004
-- modification : 13/05/2009
-- second d�clencheur, de niveau instruction, qui v�rifie qu'il y a pas d�j� de login identique
-- il s'ex�cute une seule fois, apr�s le traitement de tous les enregistrements touch�s par l'INSERT sous-jacent
DECLARE 

    CURSOR c_compte_temp IS SELECT * FROM COMPTE_TEMP;
    c1     c_compte_temp%ROWTYPE;

    nbr    INTEGER;
        
BEGIN 
    
    OPEN c_compte_temp;
    LOOP
        FETCH c_compte_temp INTO c1;
        EXIT WHEN  c_compte_temp%NOTFOUND;
  
            nbr := 0;

          -- Dans le cas d'un ajout on v�rifie tout sinon si c'est une modif., on exclu le compte courant
          IF INSERTING THEN          
               SELECT COUNT (*) INTO nbr FROM COMPTE
             WHERE cpt_login = c1.cpt_login
             AND cpt_vlan NOT IN ('X',c1.cpt_vlan);
          ELSE
               SELECT COUNT (*) INTO nbr FROM COMPTE
             WHERE cpt_login = c1.cpt_login AND cpt_ordre != c1.cpt_ordre
             AND cpt_vlan NOT IN ('X',c1.cpt_vlan);
          END IF;                
        
          IF nbr > 1 THEN 
            RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE2 : ce login "'||c1.cpt_login||'" existe d�j� ! ');
          END IF;
    
    END LOOP ;
    CLOSE c_compte_temp;
    
    -- tout � la fin, on remet � z�ro la table temporaire.
    DELETE FROM COMPTE_TEMP;
    
END ;
/


--DROP TRIGGER GRHUM.TRG_REPART_COMPTE;
ALTER TRIGGER TRG_REPART_COMPTE RENAME TO TRG_REPART_COMPTE_OLD;

CREATE OR REPLACE TRIGGER GRHUM.TRG_REPART_COMPTE
AFTER INSERT OR UPDATE OR DELETE
ON GRHUM.REPART_COMPTE
-- auteur : CRI ULR PB
-- creation : 28/06/2004
-- modification : 13/05/2009
-- Trigger de mise � jour de la table COMPTE (anciennement COMPTE_NEW) par rapport � la table REPART_COMPTE de mani�re � r�cup�rer le pers_id du compte

REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

    nbr             INTEGER;
    date_actuelle   DATE;
    crypte          COMPTE.CPT_CRYPTE%TYPE;
    persidCRI       COMPTE.CPT_CREATEUR%TYPE;
    login           COMPTE.CPT_LOGIN%TYPE;
    passwd          COMPTE.CPT_PASSWD%TYPE;
    passwdCrypte    COMPTE.CPT_PASSWD%TYPE;
    passwdClair     COMPTE.CPT_PASSWD_CLAIR%TYPE;       
    vlan            COMPTE.CPT_VLAN%TYPE; 
    charte          COMPTE.CPT_CHARTE%TYPE;
    home            COMPTE.CPT_HOME%TYPE;
    shell           COMPTE.CPT_SHELL%TYPE;
    connexion       COMPTE.CPT_CONNEXION%TYPE;
    valide          COMPTE.CPT_VALIDE%TYPE;
    debutValide     COMPTE.CPT_DEBUT_VALIDE%TYPE;
    finValide       COMPTE.CPT_FIN_VALIDE%TYPE;
    uidGid          COMPTE.CPT_UID_GID%TYPE;
    uidNew          COMPTE.CPT_UID%TYPE;
    gidNew          COMPTE.CPT_GID%TYPE;
    tvpnCode        COMPTE.TVPN_CODE%TYPE;
    tcryOrdre       COMPTE.TCRY_ORDRE%TYPE;
    cemKey          COMPTE_EMAIL.CEM_KEY%TYPE;
    email           COMPTE_EMAIL.CEM_EMAIL%TYPE;
    domaine         COMPTE_EMAIL.CEM_DOMAINE%TYPE;
    lmpasswd        VARCHAR2(32);
    ntpasswd        VARCHAR2(32);
    flag            VARCHAR2(18);
                       
BEGIN

     -- Initialisation des variables
     crypte := '';
     
     -- Type de cryptage par d�faut : UNIX
     tcryOrdre := 1;
     
     -- Par d�faut le cr�ateur ou le modificateur des comptes : le CRI
     persidCRI := 20;
     -- Recup du pers_id de GRHUM_CREATEUR
--     SELECT PERS_ID INTO createur FROM REPART_COMPTE WHERE CPT_ORDRE =
--        (SELECT CPT_ORDRE FROM COMPTE WHERE CPT_LOGIN =
--            (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
--                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')));    

     
    SELECT TO_DATE(TO_CHAR(SYSDATE,'ddmmyyyy'),'ddmmyyyy') INTO date_actuelle FROM dual;
    
     --- INSERTION sur REPART_COMPTE
     IF INSERTING THEN

         -- R�cup�ration des donn�es de COMPTE
         SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
            SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
            INTO   login,passwd,vlan,charte,connexion,uidGid,email,domaine
            FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;         
         -- R�cup�ration des donn�es de COMPTE_OLD
         SELECT COUNT(*) INTO nbr FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
            SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
            INTO   login,passwd,vlan,charte,connexion,uidGid,email,domaine
            FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;
         -- R�cup�ration des donn�es de COMPTE_SUITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT    cpt_home,cpt_shell,cpt_gid INTO home,shell,gidNew FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
         -- R�cup�ration des donn�es de COMPTE_VALIDITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN         
             SELECT TO_DATE(date_debut,'dd/mm/yyyy'),TO_DATE(date_fin,'dd/mm/yyyy') INTO debutValide,finValide FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
        
          -- Password crypte en fonction de la taille du champ password
          -- les mots de passe crypt�s sont constitu�s de 13 caract�res.
          -- Tous les password sont crypt�s sauf ceux qui ont une *
          crypte := 'O';
          passwdClair := NULL;
          passwdCrypte := NULL;
          IF (LENGTH(passwd) >= 13) THEN
                    passwdClair := NULL;
                    passwdCrypte := passwd;
          ELSE
                    passwdClair := passwd;
                    IF (passwd = '*') THEN
                        passwdCrypte := passwd;
                    ELSE
                        passwdCrypte := GRHUM.Crypt(passwd);
                    END IF;
          END IF;
          
        IF (debutValide IS NOT NULL) THEN
               --pour que le compte soit valide, la date actuelle doit etre comprise entre debut et fin de validite
               IF ( (debutValide < date_actuelle) AND (finValide > date_actuelle OR finValide is NULL) ) THEN
                       valide := 'O';
               ELSE
                       valide := 'N';
               END IF;
        ELSE
                valide := 'O';
                IF (vlan = 'E') THEN
                    debutValide := TO_DATE('01/07/2008','dd/mm/yyyy');
                   finValide := TO_DATE('31/01/2010','dd/mm/yyyy');
                 END IF;
        END IF;
            
         -- COMPTE
        SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:NEW.cpt_ordre;    
        IF (nbr = 0) THEN
           -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID
           IF (vlan='D' OR vlan='G') THEN
                      INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,
                                               cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,cpt_valide,cpt_debut_valide,cpt_fin_valide,
                                               cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,
                                               cpt_uid_gid,tvpn_code,tcry_ordre)
                      VALUES(:NEW.cpt_ordre,:NEW.pers_id,login,passwdCrypte,passwdClair,crypte,NULL,uidGid,
                             home,shell,connexion,vlan,charte,valide,debutValide,finValide,
                             'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,'NO',tcryOrdre);           
           ELSE
                   IF (vlan='P' OR vlan='R' OR vlan='E') THEN
                         tvpnCode := 'W3VPN';
                   ELSE
                         tvpnCode := 'NO';
                   END IF;
                   -- Pour les comptes Etudiants le GID par d�faut est de 2001
                   if (vlan='E') THEN
                      gidNew := 2001;
                   END IF;                   
                   INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,
                                            cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,cpt_valide,cpt_debut_valide,cpt_fin_valide,
                                            cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                   VALUES(:NEW.cpt_ordre,:NEW.pers_id,login,passwdCrypte,passwdClair,crypte,uidGid,gidNew,
                          home,shell,connexion,vlan,charte,valide,debutValide,finValide,
                          'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);
           END IF;           
        END IF;
    
         -- INSERTION de l'email en fonction du VLAN
       IF (email IS NOT NULL) THEN
                  SELECT COUNT(*) INTO nbr FROM COMPTE_EMAIL WHERE cpt_ordre=:NEW.cpt_ordre;    
               IF (nbr = 0) THEN
                        SELECT compte_email_seq.NEXTVAL INTO cemKey FROM dual;
                     INSERT INTO COMPTE_EMAIL VALUES(cemKey,:NEW.cpt_ordre,email,domaine,NULL,'N',SYSDATE,SYSDATE);
                  END IF;
       END IF;
       
       -- Configuration SAMBA en ajout
--      SELECT COUNT(*) INTO NBR FROM COMPTE_SAMBA WHERE CPT_ORDRE = :NEW.CPT_ORDRE;
--      IF (nbr = 0 ) THEN
--              lmpasswd := GRHUM.Crypt_Lm_Samba(passwd);
--              ntpasswd := GRHUM.Crypt_Nt_Samba(passwd);
--              flag := '[UX         ]';
--              INSERT INTO GRHUM.COMPTE_SAMBA VALUES(:NEW.cpt_ordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);
--      END IF;
    
    END IF;
    
    -- MISE � JOUR sur REPART_COMPTE
    IF UPDATING THEN

         -- R�cup�ration des donn�es de COMPTE
         SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
            SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
            INTO   login,passwd,vlan,charte,connexion,uidGid,email,domaine
            FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;         
         -- R�cup�ration des donn�es de COMPTE_OLD
         SELECT COUNT(*) INTO nbr FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
             INTO         login,passwd,vlan,charte,connexion,uidGid,email,domaine
             FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
             IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;
         -- R�cup�ration des donn�es de COMPTE_SUITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT    cpt_home,cpt_shell,cpt_gid INTO home,shell,gidNew FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
         -- R�cup�ration des donn�es de COMPTE_VALIDITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN         
             SELECT TO_DATE(date_debut,'dd/mm/yyyy'),TO_DATE(date_fin,'dd/mm/yyyy') INTO debutValide,finValide FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
        END IF;

          -- Password crypte en fonction de la taille du champ password
          -- les mots de passe crypt�s sont constitu�s de 13 caract�res.
          -- Tous les password sont crypt�s sauf ceux qui ont une *
          crypte := 'O';
          passwdClair := NULL;
          passwdCrypte := NULL;
          IF (LENGTH(passwd) >= 13) THEN
                    passwdClair := NULL;
                    passwdCrypte := passwd;
          ELSE
                    passwdClair := passwd;
                    IF (passwd = '*') THEN
                        passwdCrypte := passwd;
                    ELSE
                        -- PB 16/06/2006 : si le paswwd est d�sactiv�, on crypte le passwd en mettant l'�toile devant
                        IF (SUBSTR(passwd,1,1)='*') THEN
                           passwdCrypte := '*'||GRHUM.Crypt(passwd);
                        ELSE
                           passwdCrypte := GRHUM.Crypt(passwd);
                        END IF;
                    END IF;
          END IF;
              
        IF (debutValide IS NOT NULL) THEN
               --pour que le compte soit valide, la date actuelle doit etre comprise entre debut et fin de validite
               IF ( (debutValide < date_actuelle) AND (finValide > date_actuelle OR finValide IS NULL) ) THEN
                       valide := 'O';
               ELSE
                       valide := 'N';
               END IF;
            ELSE
               valide := 'O';
            END IF;

         IF (vlan = 'E') THEN
            debutValide := TO_DATE('01/07/2008');
            finValide := TO_DATE('31/01/2010');
            -- Pour les comptes Etudiants le GID par d�faut est de 2001
            gidNew := 2001;            
         END IF;
         
        SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:NEW.cpt_ordre;                
        IF (nbr != 0) THEN        

            -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID
           IF (vlan='D' OR vlan='G') THEN
                  UPDATE GRHUM.COMPTE
                  SET      cpt_login = login,cpt_passwd=passwdCrypte,cpt_passwd_clair=passwdClair,cpt_crypte=crypte,cpt_uid = NULL, cpt_gid= uidGid,
                           cpt_valide = valide, cpt_debut_valide = debutValide, cpt_fin_valide = finValide,cpt_home = home,cpt_shell = shell, cpt_connexion = connexion,
                           cpt_vlan=vlan,cpt_charte=charte,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid = uidGid,tcry_ordre = tcryOrdre
                  WHERE    cpt_ordre = :NEW.cpt_ordre;
           ELSE
              
                  UPDATE GRHUM.COMPTE
                  SET      cpt_login = login,cpt_passwd=passwdCrypte,cpt_passwd_clair=passwdClair,cpt_crypte=crypte,cpt_uid = uidGid, cpt_gid = gidNew,
                           cpt_valide = valide, cpt_debut_valide = debutValide, cpt_fin_valide = finValide,cpt_home = home,cpt_shell = shell, cpt_connexion = connexion,
                           cpt_vlan=vlan,cpt_charte=charte,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid = uidGid,tcry_ordre = tcryOrdre
                  WHERE    cpt_ordre = :NEW.cpt_ordre;
           END IF;

        ELSE

           -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID        
           IF (vlan='D' OR vlan='G') THEN
                  INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                           cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tcry_ordre)
                  VALUES(:NEW.cpt_ordre,:NEW.pers_id,login,passwdCrypte,passwdClair,crypte,NULL,uidGid,home,shell,connexion,vlan,charte,
                         valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tcryOrdre);        
           ELSE
               -- Modif. le 15/03/2005 pour ne plus mettre � jour le mot de passe crypt� car ils le sont d�j� pour les comptes Administration et Recherche
               -- Modif. le 19/04/2005 : par contre les passwd pour les Etudiants et les eXterieurs sont � crypter
               IF (vlan='P' OR vlan='R' OR vlan='E') THEN
                     tvpnCode := 'W3VPN';
               ELSE
                     tvpnCode := 'NO';
               END IF;           
               IF (vlan='P' OR vlan = 'R') THEN
                    INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                             cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                    VALUES(:NEW.cpt_ordre,:NEW.pers_id,login,passwdClair,crypte,uidGid,gidNew,home,shell,connexion,vlan,charte,
                           valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);
               ELSE
                    INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                             cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                    VALUES(:NEW.cpt_ordre,:NEW.pers_id,login,passwdCrypte,passwdClair,crypte,uidGid,gidNew,home,shell,connexion,vlan,charte,
                           valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);                
                END IF;
           END IF;
                                                                                     
        END IF;

         -- COMPTE_EMAIL en UPDATING
       IF (email IS NOT NULL) THEN
          SELECT COUNT(*) INTO nbr FROM COMPTE_EMAIL WHERE cpt_ordre=:NEW.cpt_ordre;    
          IF (nbr != 0) THEN
                 UPDATE COMPTE_EMAIL 
              SET cem_email = email, cem_domaine = domaine, d_modification = SYSDATE
              WHERE cpt_ordre = :NEW.cpt_ordre;
          ELSE
              SELECT compte_email_seq.NEXTVAL INTO cemKey FROM dual;
              INSERT INTO COMPTE_EMAIL VALUES(cemKey,:NEW.cpt_ordre,email,domaine,NULL,'N',SYSDATE,SYSDATE);
          END IF;
        END IF;

      -- Configuration SAMBA en modification
--      SELECT count(*) into nbr from COMPTE_SAMBA where cpt_ordre = :NEW.cpt_ordre;
--      lmpasswd := GRHUM.Crypt_Lm_Samba(passwd);
--      ntpasswd := GRHUM.Crypt_Nt_Samba(passwd);
--      flag := '[UX         ]';
--      IF (nbr = 0 ) THEN
--            INSERT INTO GRHUM.COMPTE_SAMBA VALUES(:NEW.cpt_ordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);
--      ELSE
--            UPDATE COMPTE_SAMBA set SMB_LM_PASSWD = lmpasswd, smb_nt_passwd = ntpasswd, d_modification = sysdate where cpt_ordre = :NEW.cpt_ordre;
--      END IF;
                
    END IF;
    
    -- SUPPRESSION sur REPART_COMPTE
    IF DELETING THEN
    
       --RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_REPART_COMPTE : DELETING "'||:OLD.cpt_ordre);
    
       SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:OLD.cpt_ordre;
       IF (nbr != 0) THEN
             
          DELETE FROM COMPTE_EMAIL WHERE cpt_ordre=:OLD.cpt_ordre;
          DELETE FROM CERTIFICAT WHERE cpt_ordre=:OLD.cpt_ordre;          
          DELETE FROM COMPTE WHERE cpt_ordre=:OLD.cpt_ordre AND pers_id=:OLD.pers_id;
          DELETE FROM COMPTE_SAMBA WHERE cpt_ordre=:OLD.cpt_ordre;
          -- Le compte passe � l'�tat Annul�
          --UPDATE COMPTE SET cpt_valide='A' WHERE cpt_ordre=:OLD.cpt_ordre AND pers_id=:OLD.pers_id;
           
       END IF;
       
    END IF;
    
END ;
/


-- ******************************************************************************************
--                              TRIGGERS (fin)
-- ******************************************************************************************

 
--
-- REPART_PERSONNE_ADRESSE
--
-- contrainte de repart_persone_adresse vers adresse en deferred 
ALTER TABLE GRHUM.REPART_PERSONNE_ADRESSE drop constraint RPA_ADRESSE ;

ALTER TABLE GRHUM.REPART_PERSONNE_ADRESSE ADD (CONSTRAINT RPA_ADRESSE 
 FOREIGN KEY (ADR_ORDRE) 
 REFERENCES GRHUM.ADRESSE (ADR_ORDRE)
 DEFERRABLE INITIALLY DEFERRED);



--
-- Nettoyer_Libelle
--
CREATE OR REPLACE FUNCTION GRHUM.Nettoyer_Libelle(texte VARCHAR2)
RETURN VARCHAR2
IS
-- --------------------------------------
-- Projet : Renvoi une chaine majuscule sans les caract�res accentu�s
-- Auteur : CRI
-- Date : 17/10/2008
-- Modif : 17/10/2008
-- Tous les caracteres sont possibles sauf :
-- l'etoile
--
-- --------------------------------------

-- ------------
-- DECLARATIONS
-- ------------

txt VARCHAR2(500);
taille NUMBER;
car VARCHAR2(500);
i    INTEGER;

BEGIN

txt:=trim(LOWER(texte));

-- Le caract�re * est remplac� par un soulign�
txt := REPLACE(txt,'*',' ');

taille := LENGTH(txt);
IF ( taille > 0 ) THEN
    FOR i IN 1..taille
    LOOP
        car := SUBSTR(txt,i,1);

        IF ( (car >= CHR(224)) AND (car <= CHR(229)) ) THEN
           txt:=REPLACE(txt,car,'a');
        END IF;

        IF ( car = CHR(231) ) THEN
           txt:=REPLACE(txt,car,'c');
        END IF;

        IF ( (car >= CHR(232)) AND (car <= CHR(235)) ) THEN
           txt:=REPLACE(txt,car,'e');
        END IF;

        IF ( (car >= CHR(236)) AND (car <= CHR(239)) ) THEN
           txt:=REPLACE(txt,car,'i');
        END IF;

        IF ( (car = CHR(241)) ) THEN
           txt:=REPLACE(txt,car,'n');
        END IF;

        IF ( (car >= CHR(242)) AND (car <= CHR(246)) ) THEN
           txt:=REPLACE(txt,car,'o');
        END IF;

        IF ( (car >= CHR(249)) AND (car <= CHR(252)) ) THEN
           txt:=REPLACE(txt,car,'u');
        END IF;

        --prise en compte du nouveau caract�re
        car := SUBSTR(txt,i,1);

    END LOOP;
END IF;

--DBMS_OUTPUT.PUT_LINE('Chaine sans accents : '||txt);
RETURN upper(trim(txt));

END;
/


--
-- STRUCTURE_ULR
--
--
UPDATE STRUCTURE_ULR SET GRP_FORME_JURIDIQUE=NULL WHERE C_STRUCTURE IN (SELECT C_STRUCTURE FROM STRUCTURE_ULR WHERE GRP_FORME_JURIDIQUE NOT IN (SELECT C_FJD FROM FORMES_JURIDIQUES_DETAILS));

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (CONSTRAINT STRUCTURE_REF_FORME_JURI_DET FOREIGN KEY (GRP_FORME_JURIDIQUE) REFERENCES GRHUM.FORMES_JURIDIQUES_DETAILS (C_FJD));
 

-- M�J Data 
-- update de SIREN � partir de SIRET
ALTER TABLE GRHUM.STRUCTURE_ULR DISABLE CONSTRAINT UK_SIREN;

UPDATE GRHUM.STRUCTURE_ULR SET SIREN = SUBSTR(SIRET,1,9) WHERE SIREN IS NULL AND SIRET IS NOT NULL; 
COMMIT;

-- remplir str_Affichage avec ll_structure si pas rempli
UPDATE GRHUM.STRUCTURE_ULR SET STR_AFFICHAGE=LL_STRUCTURE WHERE STR_AFFICHAGE IS NULL OR TRIM(STR_AFFICHAGE)='';
COMMIT;

-- NETTOYER LES LL_STRUCTURE
UPDATE GRHUM.STRUCTURE_ULR SET LL_STRUCTURE=GRHUM.NETTOYER_LIBELLE(LL_STRUCTURE) WHERE LL_STRUCTURE<>GRHUM.NETTOYER_LIBELLE(LL_STRUCTURE);
COMMIT;

-- REMPLIR NOM_AFFICHAGE SI PAS REMPLI 
UPDATE GRHUM.INDIVIDU_ULR SET NOM_AFFICHAGE=NOM_USUEL WHERE NOM_AFFICHAGE IS NULL OR TRIM(NOM_AFFICHAGE)='';
COMMIT;

UPDATE GRHUM.INDIVIDU_ULR SET PRENOM_AFFICHAGE=PRENOM WHERE PRENOM_AFFICHAGE IS NULL OR TRIM(PRENOM_AFFICHAGE)='';
COMMIT;

UPDATE GRHUM.INDIVIDU_ULR SET NOM_PATRONYMIQUE_AFFICHAGE=NOM_PATRONYMIQUE WHERE NOM_PATRONYMIQUE_AFFICHAGE IS NULL OR TRIM(NOM_PATRONYMIQUE_AFFICHAGE)='';
COMMIT;

-- NETTOYER
UPDATE GRHUM.INDIVIDU_ULR SET NOM_USUEL=GRHUM.NETTOYER_LIBELLE(NOM_USUEL) WHERE NOM_USUEL<>GRHUM.NETTOYER_LIBELLE(NOM_USUEL);
COMMIT;

UPDATE GRHUM.INDIVIDU_ULR SET NOM_PATRONYMIQUE=GRHUM.NETTOYER_LIBELLE(NOM_PATRONYMIQUE) WHERE NOM_PATRONYMIQUE IS NOT NULL AND NOM_PATRONYMIQUE<>GRHUM.NETTOYER_LIBELLE(NOM_PATRONYMIQUE);
COMMIT;

UPDATE GRHUM.INDIVIDU_ULR SET PRENOM=GRHUM.NETTOYER_LIBELLE(PRENOM) WHERE PRENOM IS NOT NULL AND PRENOM<>GRHUM.NETTOYER_LIBELLE(PRENOM);
COMMIT;


--
-- FOURNIS_ULR
--
UPDATE GRHUM.FOURNIS_ULR SET CPT_ORDRE=NULL WHERE CPT_ORDRE NOT IN (SELECT CPT_ORDRE FROM GRHUM.COMPTE);
COMMIT;

ALTER TABLE GRHUM.FOURNIS_ULR ADD (CONSTRAINT FOURNIS_REF_COMPTE 
 FOREIGN KEY (CPT_ORDRE) REFERENCES GRHUM.COMPTE (CPT_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

--
-- TODO : GRHUM.Purge_Fournisseurs
--

-- Remise � niveau des groupes des fournisseurs vis � vis de leur �tat.
BEGIN
    GRHUM.Maj_Groupes_Founisseurs;
END;
/



-- *****************************************************************************************************
--                 Ajout des attributs PERS_ID_CREATION et PERS_ID_MODIFICATION
-- *****************************************************************************************************

--
-- REPART_STRUCTURE
--
ALTER TABLE GRHUM.REPART_STRUCTURE ADD PERS_ID_CREATION NUMBER;

COMMENT ON COLUMN GRHUM.REPART_STRUCTURE.PERS_ID_CREATION IS 'Pers_id de la personne � l''origine de la cr�ation de l''enregistrement';

ALTER TABLE GRHUM.REPART_STRUCTURE ADD (CONSTRAINT REPART_STR_REF_PERS_CREAT FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.REPART_STRUCTURE ADD PERS_ID_MODIFICATION NUMBER;

COMMENT ON COLUMN GRHUM.REPART_STRUCTURE.PERS_ID_MODIFICATION IS 'Pers_id de la personne � l''origine de la modification de l''enregistrement';

ALTER TABLE GRHUM.REPART_STRUCTURE ADD (CONSTRAINT REPART_STR_REF_PERS_MODIF FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);



--
-- REPART_ASSOCIATION
--
ALTER TABLE GRHUM.REPART_ASSOCIATION RENAME COLUMN AGT_PERS_ID TO PERS_ID_CREATION;

COMMENT ON COLUMN GRHUM.REPART_ASSOCIATION.PERS_ID_CREATION IS 'Pers_id de la personne � l''origine de la cr�ation de l''enregistrement';

ALTER TABLE GRHUM.REPART_ASSOCIATION  RENAME CONSTRAINT RASS_REF_AGT_PERSONNE TO REPART_ASS_REF_PERS_CREAT;

ALTER TABLE GRHUM.REPART_ASSOCIATION ADD PERS_ID_MODIFICATION NUMBER;

COMMENT ON COLUMN GRHUM.REPART_ASSOCIATION.PERS_ID_MODIFICATION IS 'Pers_id de la personne � l''origine de la modification de l''enregistrement';

ALTER TABLE GRHUM.REPART_ASSOCIATION ADD (CONSTRAINT REPART_ASS_REF_PERS_MODIF FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);



--
-- ASSOCIATION
--
ALTER TABLE GRHUM.ASSOCIATION ADD ASS_CODE VARCHAR2(5);

COMMENT ON COLUMN GRHUM.ASSOCIATION.ASS_CODE IS 'Code de l''association';

UPDATE GRHUM.ASSOCIATION SET ASS_CODE = SUBSTR(REPLACE(ASS_LIBELLE,' ',''),1,5);

ALTER TABLE GRHUM.ASSOCIATION ADD ASS_ALIAS VARCHAR2(200);

COMMENT ON COLUMN GRHUM.ASSOCIATION.ASS_ALIAS IS 'Alias mail rattach� � l''association';

ALTER TABLE GRHUM.ASSOCIATION ADD ASS_LOCALE CHAR(1) DEFAULT 'N' NOT NULL;

COMMENT ON COLUMN GRHUM.ASSOCIATION.ASS_LOCALE IS 'Association locale ? (Oui ou Non)';



--
-- RIBFOUR_ULR
--
ALTER TABLE GRHUM.RIBFOUR_ULR ADD PERS_ID_CREATION NUMBER;

ALTER TABLE GRHUM.RIBFOUR_ULR_SAV ADD PERS_ID_CREATION NUMBER;

COMMENT ON COLUMN GRHUM.RIBFOUR_ULR.PERS_ID_CREATION IS 'Pers_id de la personne � l''origine de la cr�ation de l''enregistrement';

ALTER TABLE GRHUM.RIBFOUR_ULR ADD (CONSTRAINT RIBFOUR_REF_PERS_CREAT FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.RIBFOUR_ULR ADD PERS_ID_MODIFICATION NUMBER;

ALTER TABLE GRHUM.RIBFOUR_ULR_SAV ADD PERS_ID_MODIFICATION NUMBER;

COMMENT ON COLUMN GRHUM.RIBFOUR_ULR.PERS_ID_MODIFICATION IS 'Pers_id de la personne � l''origine de la modification de l''enregistrement';

ALTER TABLE GRHUM.RIBFOUR_ULR ADD (CONSTRAINT RIBFOUR_REF_PERS_MODIF FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);



--
-- INDIVIDU_ULR
--
ALTER TABLE GRHUM.INDIVIDU_ULR ADD PERS_ID_CREATION NUMBER;

COMMENT ON COLUMN GRHUM.INDIVIDU_ULR.PERS_ID_CREATION IS 'Pers_id de la personne � l''origine de la cr�ation de l''enregistrement';

ALTER TABLE GRHUM.INDIVIDU_ULR ADD (CONSTRAINT INDIVIDU_REF_PERS_CREAT FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.INDIVIDU_ULR ADD PERS_ID_MODIFICATION NUMBER;

COMMENT ON COLUMN GRHUM.INDIVIDU_ULR.PERS_ID_MODIFICATION IS 'Pers_id de la personne � l''origine de la modification de l''enregistrement';

ALTER TABLE GRHUM.INDIVIDU_ULR ADD (CONSTRAINT INDIVIDU_REF_PERS_MODIF FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);



--
-- STRUCTURE_ULR
--
ALTER TABLE GRHUM.STRUCTURE_ULR ADD PERS_ID_CREATION NUMBER;

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.PERS_ID_CREATION IS 'Pers_id de la personne � l''origine de la cr�ation de l''enregistrement';

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (CONSTRAINT STRUCTURE_REF_PERS_CREAT FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.STRUCTURE_ULR ADD PERS_ID_MODIFICATION NUMBER;

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.PERS_ID_MODIFICATION IS 'Pers_id de la personne � l''origine de la modification de l''enregistrement';

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (CONSTRAINT STRUCTURE_REF_PERS_MODIF FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED);





-- Recompiler tous les objets invalides
--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('GRHUM');
--END;
--/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('GARNUCHE');
--END;
--/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('SCOLARITE');
--END;
--/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('JEFY_ADMIN');
--END;
--/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('JEFY_DEPENSE');
--END;
--/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('JEFY_RECETTE');
--END;
--/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('JEFY_TELEPHONIE');
--END;
--/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('MARACUJA');
--END;
--/


COMMIT;
