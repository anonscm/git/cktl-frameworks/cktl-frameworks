--
-- Objets � recompiler suite � la modification du sch�ma des tables :
-- REPART_STRUCTURE
-- REPART_ASSOCIATION
-- ...
--
-- qui ont 2 attributs suppl�mentaires :
-- PERS_ID_CREATION
-- PRES_ID_MODIFICATION
--
--
-- Pr�-requis : patch 1.6.0.0 et 1.6.0.1 de GRHUM
--

--
-- Proc�dures li�es � COMPTE
--
CREATE OR REPLACE PROCEDURE GRHUM.INS_COMPTE(
INS_PERS_ID         REPART_COMPTE.pers_id%TYPE,
INS_CPT_UID_GID     COMPTE.cpt_uid_gid%TYPE,
INS_CPT_LOGIN       COMPTE.cpt_login%TYPE,
INS_CPT_PASSWD      COMPTE.cpt_passwd%TYPE,
INS_CPT_CONNEXION   COMPTE.cpt_connexion%TYPE,
INS_CPT_VLAN        COMPTE.cpt_VLAN%TYPE,
INS_CPT_EMAIL       COMPTE.cpt_email%TYPE,
INS_CPT_DOMAINE     COMPTE.cpt_DOMAINE%TYPE,
INS_CPT_CHARTE      COMPTE.cpt_CHARTE%TYPE,
INS_CPT_ORDRE OUT    NUMBER
) IS
-- ------------
-- DECLARATIONS
-- ------------

cpt INTEGER;
cptordre INTEGER;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN
--DBMS_OUTPUT.PUT_LINE('-----INS_COMPTE-----');

/*select count(*) into cpt from compte where (cpt_email=ins_cpt_email and cpt_domaine = ins_cpt_domaine);
if (cpt>0) then
   RAISE_APPLICATION_ERROR(-20001,'INS_COMPTE : Compte existant');
end if;*/

--select max(cpt_ordre)+1 into cptordre from compte;
SELECT COMPTE_SEQ.NEXTVAL INTO cptordre FROM dual;

INSERT INTO COMPTE(CPT_ORDRE,PERS_ID,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,D_CREATION,D_MODIFICATION)
VALUES (cptordre,INS_PERS_ID,INS_CPT_UID_GID,INS_CPT_LOGIN,INS_CPT_PASSWD,INS_CPT_CONNEXION,INS_CPT_VLAN,INS_CPT_EMAIL,INS_CPT_DOMAINE,INS_CPT_CHARTE,SYSDATE,SYSDATE);

INSERT INTO REPART_COMPTE VALUES (cptordre,INS_PERS_ID,SYSDATE,SYSDATE);

INS_CPT_ORDRE := cptordre;
END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Del_Compte_Etud(DEL_LOGIN COMPTE.cpt_login%TYPE)
IS
/*
  Auteur : CRI PB
  Date de cr�ation : 19/09/2007
  Date de modification : 06/03/2008
  Objectif : proc�dure utilis�e lors de la suppression du compte syst�me pour �viter de passer dans l'Annuaire.
*/


-- ------------
-- DECLARATIONS
-- ------------
DEL_CPT_ORDRE      NUMBER;
DEL_PERS_ID        NUMBER;

noIndividu      INDIVIDU_ULR.NO_INDIVIDU%TYPE;
nbr                INTEGER;
anneeScol        NUMBER;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

    -- V�rification de l'existence du compte
    SELECT COUNT(*) INTO nbr FROM COMPTE
    WHERE cpt_login = LOWER(DEL_LOGIN)
    AND cpt_vlan = 'E';
    IF (nbr = 0) THEN
       RAISE_APPLICATION_ERROR(-20001,'DEL_COMPTE_ETUD : Le login '||del_login||' '||' est inconnu !');
    END IF;

    -- Recup�ration des clefs
    SELECT COUNT(*) INTO nbr
    FROM COMPTE C, REPART_COMPTE RC
    WHERE C.cpt_ordre = RC.cpt_ordre
    AND cpt_login = LOWER(DEL_LOGIN)
    AND cpt_vlan = 'E';
    IF (nbr != 1) THEN
       RAISE_APPLICATION_ERROR(-20002,'DEL_COMPTE_ETUD : Probl�me sur les clefs primaires du login '||del_login||' !');
    END IF;

    SELECT C.cpt_ordre,RC.pers_id INTO DEL_CPT_ORDRE,DEL_PERS_ID
    FROM COMPTE C, REPART_COMPTE RC
    WHERE C.cpt_ordre = RC.cpt_ordre
    AND cpt_login = LOWER(DEL_LOGIN)
    AND cpt_vlan = 'E';

    DELETE FROM REPART_COMPTE WHERE cpt_ordre=DEL_CPT_ORDRE AND pers_id=DEL_PERS_ID;
    DELETE FROM COMPTE WHERE cpt_ordre=DEL_CPT_ORDRE;

    SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    IF (nbr != 0) THEN
       DELETE FROM COMPTE_VALIDITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    END IF;

    SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    IF (nbr != 0) THEN
       DELETE FROM COMPTE_SUITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    END IF;

    SELECT COUNT(*) INTO nbr FROM COMPTE_SAMBA WHERE cpt_ordre=DEL_CPT_ORDRE;
    IF (nbr != 0) THEN
       DELETE FROM COMPTE_SAMBA WHERE cpt_ordre=DEL_CPT_ORDRE;
    END IF;

    -- PB 06/03/2008 : v�rification si toujours employ�
    -- Personnel actuel 
    SELECT no_individu into noIndividu from GRHUM.INDIVIDU_ULR where pers_id = DEL_PERS_ID;
   
    SELECT count(*) into nbr from GRHUM.V_PERSONNEL_ACTUEL where no_dossier_pers = noIndividu;

     -- Pas personnel on supprime tout sauf les groupes des FOURNISSEURS
     if nbr = 0 then

        DELETE FROM REPART_STRUCTURE
        WHERE PERS_ID=DEL_PERS_ID
        AND C_STRUCTURE NOT IN (select param_value from grhum_parametres
                                where param_key='ANNUAIRE_FOU_ARCHIVES'
                                or param_key='ANNUAIRE_FOU_ENCOURS_VALIDE'
                                or param_key='ANNUAIRE_FOU_VALIDE_EXTERNE'
                                or param_key='ANNUAIRE_FOU_VALIDE_PHYSIQUE');
     
     -- Sinon on supprime l'appartenance aux groupes de type 'D'iplome
     else

        DELETE FROM REPART_STRUCTURE
        WHERE PERS_ID=DEL_PERS_ID
        AND C_STRUCTURE in (select S.C_STRUCTURE
                                from structure_ulr S, repart_structure RS, repart_type_groupe RTG
                                where RS.C_STRUCTURE = S.C_STRUCTURE
                                and RTG.C_STRUCTURE = S.C_STRUCTURE
                                and RTG.TGRP_CODE = 'D'
                                and RS.PERS_ID = DEL_PERS_ID);
     
     end if;

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Del_Compte_Prof(DEL_LOGIN    COMPTE.cpt_login%TYPE)
IS
/*
  Auteur : CRI PB
  Date de cr�ation : 01/02/2005
  Date de modification : 06/03/2008
  Objectif : proc�dure utilis�e lors de la suppression du compte syst�me pour �viter de passer dans l'Annuaire.
*/


-- ------------
-- DECLARATIONS
-- ------------
DEL_CPT_ORDRE    NUMBER;
DEL_PERS_ID     NUMBER;

noIndividu      INDIVIDU_ULR.NO_INDIVIDU%TYPE;
nbr                INTEGER;
anneeScol        NUMBER;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

    -- V�rification de l'existance du compte
    SELECT COUNT(*) INTO nbr FROM COMPTE
    WHERE cpt_login = LOWER(DEL_LOGIN)
    AND cpt_vlan IN ('P','R');
    IF (nbr = 0) THEN
       RAISE_APPLICATION_ERROR(-20001,'DEL_COMPTE_PROF : Le login '||del_login||' '||' est inconnu !');
    END IF;

    -- Recup�ration des clefs
    SELECT COUNT(*) INTO nbr
    FROM COMPTE C, REPART_COMPTE RC
    WHERE C.cpt_ordre = RC.cpt_ordre
    AND cpt_login = LOWER(DEL_LOGIN)
    AND cpt_vlan IN ('P','R');
    IF (nbr != 1) THEN
       RAISE_APPLICATION_ERROR(-20002,'DEL_COMPTE_PROF : Probl�me sur les clefs primaires du login '||del_login||' !');
    END IF;

    SELECT C.cpt_ordre,RC.pers_id INTO DEL_CPT_ORDRE,DEL_PERS_ID
    FROM COMPTE C, REPART_COMPTE RC
    WHERE C.cpt_ordre = RC.cpt_ordre
    AND cpt_login = LOWER(DEL_LOGIN)
    AND cpt_vlan IN ('P','R');

    DELETE FROM REPART_COMPTE WHERE cpt_ordre=DEL_CPT_ORDRE AND pers_id=DEL_PERS_ID;
    DELETE FROM COMPTE WHERE cpt_ordre=DEL_CPT_ORDRE;

    SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    IF (nbr != 0) THEN
       DELETE FROM COMPTE_VALIDITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    END IF;

    SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    IF (nbr != 0) THEN
       DELETE FROM COMPTE_SUITE WHERE cpt_ordre=DEL_CPT_ORDRE;
    END IF;

    SELECT COUNT(*) INTO nbr FROM COMPTE_SAMBA WHERE cpt_ordre=DEL_CPT_ORDRE;
    IF (nbr != 0) THEN
       DELETE FROM COMPTE_SAMBA WHERE cpt_ordre=DEL_CPT_ORDRE;
    END IF;

     -- PB 06/03/2008 : verification si toujours �tudiant
     -- Etudiant actuel
     SELECT TO_NUMBER(param_value) INTO anneeScol FROM GARNUCHE.GARNUCHE_PARAMETRES WHERE param_key='GARNUCHE_ANNEE_REFERENCE';
     
     SELECT no_individu into noIndividu from GRHUM.INDIVIDU_ULR where pers_id = DEL_PERS_ID;
     
     SELECT COUNT(*) INTO nbr FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE fann_key = anneeScol AND no_individu = noIndividu
     and (res_code is null or res_code not in ('D','E','Z','H','1')) and idipl_type_inscription != 9;

     -- Pas �tudiant on supprime tout sauf les groupes des FOURNISSEURS
     if nbr = 0 then

        DELETE FROM REPART_STRUCTURE
        WHERE PERS_ID=DEL_PERS_ID
        AND C_STRUCTURE NOT IN (select param_value from grhum_parametres
                                where param_key='ANNUAIRE_FOU_ARCHIVES'
                                or param_key='ANNUAIRE_FOU_ENCOURS_VALIDE'
                                or param_key='ANNUAIRE_FOU_VALIDE_EXTERNE'
                                or param_key='ANNUAIRE_FOU_VALIDE_PHYSIQUE');
     
     -- Sinon on garde l'appartenance aux groupes de type 'D'iplome
     else

        DELETE FROM REPART_STRUCTURE
        WHERE PERS_ID=DEL_PERS_ID
        AND C_STRUCTURE NOT IN (select S.C_STRUCTURE
                                from structure_ulr S, repart_structure RS, repart_type_groupe RTG
                                where RS.C_STRUCTURE = S.C_STRUCTURE
                                and RTG.C_STRUCTURE = S.C_STRUCTURE
                                and RTG.TGRP_CODE = 'D'
                                and RS.PERS_ID = DEL_PERS_ID
                                union
                                select param_value from grhum_parametres
                                where param_key='ANNUAIRE_FOU_ARCHIVES'
                                or param_key='ANNUAIRE_FOU_ENCOURS_VALIDE'
                                or param_key='ANNUAIRE_FOU_VALIDE_EXTERNE'
                                or param_key='ANNUAIRE_FOU_VALIDE_PHYSIQUE');
     
     end if;
     

END;
/




CREATE OR REPLACE PROCEDURE GRHUM.Ins_Structure_Ulr
(
  cstructure				OUT STRUCTURE_ULR.c_structure%TYPE,
  persid					OUT STRUCTURE_ULR.pers_id%TYPE,
  INS_LL_STRUCTURE          STRUCTURE_ULR.ll_structure%TYPE,
  INS_LC_STRUCTURE          STRUCTURE_ULR.lc_structure%TYPE,
  INS_C_TYPE_STRUCTURE      STRUCTURE_ULR.c_type_structure%TYPE,
  INS_C_STRUCTURE_PERE      STRUCTURE_ULR.c_structure_pere%TYPE,
  INS_C_TYPE_ETABLISSEMEN   STRUCTURE_ULR.c_type_etablissemen%TYPE,
  INS_C_ACADEMIE            STRUCTURE_ULR.c_academie%TYPE,
  INS_C_STATUT_JURIDIQUE    STRUCTURE_ULR.c_statut_juridique%TYPE,
  INS_C_RNE                 STRUCTURE_ULR.c_rne%TYPE,
  INS_SIRET                 STRUCTURE_ULR.siret%TYPE,
  INS_SIREN                 STRUCTURE_ULR.siren%TYPE,
  INS_C_NAF                 STRUCTURE_ULR.c_naf%TYPE,
  INS_C_TYPE_DECISION_STR   STRUCTURE_ULR.c_type_decision_str%TYPE,
  INS_REF_EXT_ETAB          STRUCTURE_ULR.ref_ext_etab%TYPE,
  INS_REF_EXT_COMP          STRUCTURE_ULR.ref_ext_comp%TYPE,
  INS_REF_EXT_CR            STRUCTURE_ULR.ref_ext_cr%TYPE,
  INS_REF_DECISION          STRUCTURE_ULR.ref_decision%TYPE,
  INS_DATE_DECISION         STRUCTURE_ULR.date_decision%TYPE,
  INS_DATE_OUVERTURE        STRUCTURE_ULR.date_ouverture%TYPE,
  INS_DATE_FERMETURE        STRUCTURE_ULR.date_fermeture%TYPE,
  INS_STR_ORIGINE			STRUCTURE_ULR.str_origine%TYPE,
  INS_STR_PHOTO  			STRUCTURE_ULR.str_photo%TYPE,
  INS_STR_ACTIVITE			STRUCTURE_ULR.str_activite%TYPE,
  INS_GRP_OWNER 			STRUCTURE_ULR.grp_owner%TYPE,
  INS_GRP_RESPONSABLE		STRUCTURE_ULR.grp_responsable%TYPE,
  INS_GRP_FORME_JURIDIQUE 	STRUCTURE_ULR.grp_forme_juridique%TYPE,
  INS_GRP_CAPITAL  			STRUCTURE_ULR.grp_capital%TYPE,
  INS_GRP_CA  				STRUCTURE_ULR.grp_ca%TYPE,
  INS_GRP_EFFECTIFS 		STRUCTURE_ULR.grp_effectifs%TYPE,
  INS_GRP_CENTRE_DECISION	STRUCTURE_ULR.grp_centre_decision%TYPE,
  INS_GRP_APE_CODE  		STRUCTURE_ULR.grp_ape_code%TYPE,
  INS_GRP_APE_CODE_BIS  	STRUCTURE_ULR.grp_ape_code_bis%TYPE,
  INS_GRP_APE_CODE_COMP  	STRUCTURE_ULR.grp_ape_code_comp%TYPE,
  INS_GRP_ACCES 			STRUCTURE_ULR.grp_acces%TYPE,
  INS_GRP_ALIAS 			STRUCTURE_ULR.grp_alias%TYPE,
  INS_GRP_RESPONSABILITE 	STRUCTURE_ULR.grp_responsabilite%TYPE,
  INS_GRP_TRADEMARQUE 		STRUCTURE_ULR.grp_trademarque%TYPE,
  INS_GRP_WEBMESTRE 		STRUCTURE_ULR.grp_webmestre%TYPE,
  INS_GRP_FONCTION1 		STRUCTURE_ULR.grp_fonction1%TYPE,
  INS_GRP_FONCTION2 		STRUCTURE_ULR.grp_fonction2%TYPE,
  INS_ORG_ORDRE 			STRUCTURE_ULR.org_ordre%TYPE,
  INS_GRP_MOTS_CLEFS		STRUCTURE_ULR.grp_mots_clefs%TYPE,
  INS_TVA_INTRACOM		STRUCTURE_ULR.tva_intracom%TYPE
)
IS
-- ------------
-- DECLARATIONS
-- ------------
cpt INTEGER;
structure_pere VARCHAR2(10);
llStructure    STRUCTURE_ULR.LL_STRUCTURE%TYPE;
strAffichage   STRUCTURE_ULR.STR_AFFICHAGE%TYPE;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

IF (ins_c_structure_pere IS NOT NULL)
THEN
	SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
	WHERE c_structure=ins_c_structure_pere;
	IF (cpt=0) THEN
	   RAISE_APPLICATION_ERROR(-20001,'INS_STRUCTURE_ULR : C_structure_pere inconnu');
	END IF;
END IF;

SELECT SEQ_STRUCTURE_ULR.NEXTVAL INTO cstructure FROM dual;

SELECT SEQ_PERSONNE.NEXTVAL INTO persid FROM dual;

-- Le libell� long de la structure ne contient pas de caract�res accentu�s
llStructure := GRHUM.NETTOYER_LIBELLE(INS_LL_STRUCTURE);
-- Le libell� d'affichage peut contenir des caract�res accentu�s
strAffichage := INS_LL_STRUCTURE;
 
INSERT INTO STRUCTURE_ULR (
   C_STRUCTURE, PERS_ID, LL_STRUCTURE,
   LC_STRUCTURE, C_TYPE_STRUCTURE,  C_STRUCTURE_PERE,
   C_TYPE_ETABLISSEMEN, C_ACADEMIE, C_STATUT_JURIDIQUE,
   C_RNE, SIRET, SIREN,
   C_NAF, C_TYPE_DECISION_STR, REF_EXT_ETAB,
   REF_EXT_COMP, REF_EXT_CR, REF_DECISION,
   DATE_DECISION, DATE_OUVERTURE, DATE_FERMETURE,
   STR_ORIGINE, STR_PHOTO, STR_ACTIVITE,
   GRP_OWNER, GRP_RESPONSABLE, GRP_FORME_JURIDIQUE,
   GRP_CAPITAL, GRP_CA, GRP_EFFECTIFS,
   GRP_CENTRE_DECISION, GRP_APE_CODE, GRP_APE_CODE_BIS,
   GRP_APE_CODE_COMP, GRP_ACCES, GRP_ALIAS,
   GRP_RESPONSABILITE, GRP_TRADEMARQUE, GRP_WEBMESTRE,
   GRP_FONCTION1, GRP_FONCTION2, ORG_ORDRE,
   GRP_MOTS_CLEFS, D_CREATION, D_MODIFICATION,
   ETAB_CODURSSAF, NUM_ASSEDIC, NUM_IRCANTEC,
   ETAB_NUMURSSAF, PSEC_ORDRE, TEM_SECTORISE,
   TEM_DADS, TEM_SOUMIS_TVA, TEM_VALIDE,
   C_NIC, TAUX_ACC_TRAV, RISQUE_ACC_TRAV,
   TAUX_TRANSPORT, TAUX_IR, TEM_COTIS_ASSEDIC,
   MOYENNE_AGE, EXPORT, TAUX_EXONERATION_TVA,
   TVA_INTRACOM,STR_AFFICHAGE)
VALUES (
  cstructure,
  persid,
  llStructure,
  INS_LC_STRUCTURE,
  INS_C_TYPE_STRUCTURE,
  INS_C_STRUCTURE_PERE,
  INS_C_TYPE_ETABLISSEMEN,
  INS_C_ACADEMIE,
  INS_C_STATUT_JURIDIQUE,
  INS_C_RNE,
  INS_SIRET,
  INS_SIREN,
  INS_C_NAF,
  INS_C_TYPE_DECISION_STR,
  INS_REF_EXT_ETAB,
  INS_REF_EXT_COMP,
  INS_REF_EXT_CR,
  INS_REF_DECISION,
  INS_DATE_DECISION,
  INS_DATE_OUVERTURE,
  INS_DATE_FERMETURE,
  INS_STR_ORIGINE,
  INS_STR_PHOTO,
  INS_STR_ACTIVITE,
  INS_GRP_OWNER,
  INS_GRP_RESPONSABLE,
  INS_GRP_FORME_JURIDIQUE,
  INS_GRP_CAPITAL,
  INS_GRP_CA,
  INS_GRP_EFFECTIFS,
  INS_GRP_CENTRE_DECISION,
  INS_GRP_APE_CODE,
  INS_GRP_APE_CODE_BIS,
  INS_GRP_APE_CODE_COMP,
  INS_GRP_ACCES,
  INS_GRP_ALIAS,
  INS_GRP_RESPONSABILITE,
  INS_GRP_TRADEMARQUE,
  INS_GRP_WEBMESTRE,
  INS_GRP_FONCTION1,
  INS_GRP_FONCTION2,
  INS_ORG_ORDRE,
  INS_GRP_MOTS_CLEFS,
  SYSDATE,
  SYSDATE,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  'N',
  'N',
  'O',
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  'N',
  NULL,	  -- Moyenne d'age
  NULL,	  -- % Export
  NULL,	  -- Taux exo TVA
  INS_TVA_INTRACOM,
  strAffichage
  );

END;
/

-- REPART_TYPE_GROUPE
CREATE OR REPLACE PROCEDURE GRHUM.Maj_Repart_Type_Groupe(
  MAJ_C_STRUCTURE          REPART_TYPE_GROUPE.c_structure%TYPE,
  MAJ_TGRP_CODE			   REPART_TYPE_GROUPE.tgrp_code%TYPE,
  maj_tgrp_code_valide	   CHAR
) IS
-- ------------
-- DECLARATIONS
-- ------------
cpt INTEGER;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

IF (maj_tgrp_code_valide='O')
THEN
	SELECT COUNT(*) INTO cpt FROM REPART_TYPE_GROUPE
	WHERE c_structure=maj_c_structure AND tgrp_code=maj_tgrp_code;
	IF (cpt=0) THEN
	   INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(maj_c_structure,maj_tgrp_code,SYSDATE,SYSDATE);
	END IF;
ELSE
	DELETE FROM REPART_TYPE_GROUPE WHERE c_structure=maj_c_structure AND tgrp_code=maj_tgrp_code;
END IF;

END;
/


CREATE OR REPLACE PROCEDURE GRHUM.Maj_Repart_Type_Groupe_Bis_Old
(
cstructure REPART_TYPE_GROUPE.c_structure%TYPE,
annuaire   REPART_TYPE_GROUPE.TGRP_CODE%TYPE,
service    REPART_TYPE_GROUPE.TGRP_CODE%TYPE,
forum 	   REPART_TYPE_GROUPE.TGRP_CODE%TYPE,
organigramme REPART_TYPE_GROUPE.TGRP_CODE%TYPE,
ged 	   REPART_TYPE_GROUPE.TGRP_CODE%TYPE,
ldiff 	   REPART_TYPE_GROUPE.TGRP_CODE%TYPE,
grpcompte  REPART_TYPE_GROUPE.TGRP_CODE%TYPE,
unix 	   REPART_TYPE_GROUPE.TGRP_CODE%TYPE
)
IS
BEGIN

DELETE FROM REPART_TYPE_GROUPE WHERE (c_structure=c_structure
AND tgrp_code IN ('S','A','F','O','C','U','G','L'));

IF (annuaire = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'A',SYSDATE,SYSDATE);
END IF;

IF (service = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'S',SYSDATE,SYSDATE);
END IF;

IF (organigramme = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'O',SYSDATE,SYSDATE);
END IF;

IF (forum = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'F',SYSDATE,SYSDATE);
END IF;

IF (ged = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'G',SYSDATE,SYSDATE);
END IF;

IF (ldiff = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'L',SYSDATE,SYSDATE);
END IF;

IF (grpcompte = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'C',SYSDATE,SYSDATE);
END IF;

IF (unix = 'O')
THEN INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES(cstructure,'U',SYSDATE,SYSDATE);
END IF;

END;
/

--DROP PROCEDURE GRHUM.Maj_Repart_Type_Groupe_Bis;
BEGIN 
  GRHUM.DROP_OBJECT ( 'GRHUM', 'MAJ_REPART_TYPE_GROUPE_BIS', 'PROCEDURE' );
END; 


CREATE OR REPLACE PROCEDURE GRHUM.Ins_Groupe(
  cstructure				OUT STRUCTURE_ULR.c_structure%TYPE,
  persid					OUT STRUCTURE_ULR.pers_id%TYPE,
  INS_TGRP_CODE				CHAR,
  INS_LL_STRUCTURE          STRUCTURE_ULR.ll_structure%TYPE,
  INS_LC_STRUCTURE          STRUCTURE_ULR.lc_structure%TYPE,
  INS_C_STRUCTURE_PERE      STRUCTURE_ULR.c_structure_pere%TYPE,
  INS_STR_ORIGINE            STRUCTURE_ULR.str_origine%TYPE,
  INS_STR_PHOTO              STRUCTURE_ULR.str_photo%TYPE,
  INS_STR_ACTIVITE            STRUCTURE_ULR.str_activite%TYPE,
  INS_GRP_OWNER             STRUCTURE_ULR.grp_owner%TYPE,
  INS_GRP_RESPONSABLE        STRUCTURE_ULR.grp_responsable%TYPE,
  INS_GRP_ACCES             STRUCTURE_ULR.grp_acces%TYPE,
  INS_GRP_ALIAS             STRUCTURE_ULR.grp_alias%TYPE,
  INS_GRP_RESPONSABILITE     STRUCTURE_ULR.grp_responsabilite%TYPE,
  INS_GRP_WEBMESTRE         STRUCTURE_ULR.grp_webmestre%TYPE,
  INS_GRP_FONCTION1         STRUCTURE_ULR.grp_fonction1%TYPE,
  INS_GRP_FONCTION2         STRUCTURE_ULR.grp_fonction2%TYPE,
  INS_GRP_MOTS_CLEFS         STRUCTURE_ULR.grp_mots_clefs%TYPE
)
IS
-- ------------
-- DECLARATIONS
-- ------------
cpt INTEGER;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

  Ins_Structure_Ulr (
  cstructure,
  persid,
  INS_LL_STRUCTURE,
  INS_LC_STRUCTURE,
  'A',--INS_C_TYPE_STRUCTURE,
  INS_C_STRUCTURE_PERE,
  NULL,--INS_C_TYPE_ETABLISSEMEN,
  NULL,--INS_C_ACADEMIE,
  NULL,--INS_C_STATUT_JURIDIQUE,
  NULL,--INS_C_RNE,
  NULL,--INS_SIRET,
  NULL,--INS_SIREN,
  NULL,--INS_C_NAF,
  NULL,--INS_C_TYPE_DECISION_STR,
  NULL,--INS_REF_EXT_ETAB,
  NULL,--INS_REF_EXT_COMP,
  NULL,--INS_REF_EXT_CR,
  NULL,--INS_REF_DECISION,
  NULL,--INS_DATE_DECISION,
  NULL,--INS_DATE_OUVERTURE,
  NULL,--INS_DATE_FERMETURE,
  INS_STR_ORIGINE,
  INS_STR_PHOTO,
  INS_STR_ACTIVITE,
  INS_GRP_OWNER,
  INS_GRP_RESPONSABLE,
  NULL,--INS_GRP_FORME_JURIDIQUE,
  NULL,--INS_GRP_CAPITAL,
  NULL,--INS_GRP_CA,
  NULL,--INS_GRP_EFFECTIFS,
  NULL,--INS_GRP_CENTRE_DECISION,
  NULL,--INS_GRP_APE_CODE,
  NULL,--INS_GRP_APE_CODE_BIS,
  NULL,--INS_GRP_APE_CODE_COMP,
  INS_GRP_ACCES,
  INS_GRP_ALIAS,
  INS_GRP_RESPONSABILITE,
  NULL,--INS_GRP_TRADEMARQUE,
  INS_GRP_WEBMESTRE,
  INS_GRP_FONCTION1,
  INS_GRP_FONCTION2,
  NULL,--INS_ORG_ORDRE,
  INS_GRP_MOTS_CLEFS,
  NULL -- TVA Intracom
   );

   --DBMS_OUTPUT.PUT_LINE(cstructure);

   INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification)
   VALUES (cstructure,ins_tgrp_code,SYSDATE,SYSDATE);

END;
/


CREATE OR REPLACE PROCEDURE GRHUM.Ins_Fournis_Interne
-- Proc. de cr�ation des fournisseurs internes pour les structures internes
-- cr�e (si besoin) et rempli le groupe des fournisseurs valides internes de l etablissement
-- a partir de l annuaire: prend tous les services types "budget" (LB)
--
-- cette procedure peut etre lancee autant que l on veut, aucun fournisseur interne ne sera
-- cree en double ni ajoute 2 fois dans le groupe (normalement :-) )

IS
  -- Curseur de r�cup�ration des structures de type "Budget"
  CURSOR c_struct_internes IS
    SELECT DISTINCT S.pers_id
      FROM STRUCTURE_ULR S, REPART_TYPE_GROUPE R
      WHERE S.C_STRUCTURE = R.C_STRUCTURE
      AND R.TGRP_CODE = 'LB';

  my_pers_id        	         grhum.PERSONNE.pers_id%TYPE;
  my_c_structure        		 grhum.STRUCTURE_ULR.c_structure%TYPE;
  my_c_structure_pere   		 grhum.STRUCTURE_ULR.c_structure%TYPE;
  my_grp_owner					 grhum.STRUCTURE_ULR.grp_owner%TYPE;
  my_param_ordre		         grhum.GRHUM_PARAMETRES.param_ordre%TYPE;
  my_annuaire_fou_valide_interne grhum.GRHUM_PARAMETRES.PARAM_VALUE%TYPE;
  my_adr_ordre					 grhum.REPART_PERSONNE_ADRESSE.adr_ordre%TYPE;
  my_fou_ordre					 grhum.FOURNIS_ULR.fou_ordre%TYPE;
  my_nb			   				 NUMBER;
BEGIN

  -- verif parametre ANNUAIRE_FOU_VALIDE_INTERNE existe
  SELECT COUNT(*) INTO my_nb FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_INTERNE';
  IF my_nb = 0 THEN
	-- n existe pas, on va le creer...
    SELECT COUNT(*) INTO my_nb FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_PHYSIQUE';
    IF my_nb = 0 THEN
	   RAISE_APPLICATION_ERROR(-20000, 'Parametre ANNUAIRE_FOU_VALIDE_PHYSIQUE manquant dans grhum.grhum_parametres, impossible de d�terminer o� chercher/cr�er le groupe des fournisseurs valides internes !');
    END IF;
    SELECT PARAM_VALUE INTO my_c_structure FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_PHYSIQUE';
	SELECT C_STRUCTURE_PERE, grp_owner INTO my_c_structure_pere, my_grp_owner FROM GRHUM.STRUCTURE_ULR WHERE c_structure = my_c_structure;
	SELECT COUNT(*) INTO my_nb FROM GRHUM.STRUCTURE_ULR WHERE LOWER(ll_structure) LIKE '%interne%' AND c_structure_pere = my_c_structure_pere;
	IF my_nb = 0 THEN
	   -- on cree le groupe qui va bien
	   SELECT SEQ_STRUCTURE_ULR.NEXTVAL INTO my_c_structure FROM dual;
	   SELECT SEQ_PERSONNE.NEXTVAL INTO my_pers_id FROM dual;
	   INSERT INTO STRUCTURE_ULR (C_STRUCTURE, PERS_ID, LL_STRUCTURE, LC_STRUCTURE, C_TYPE_STRUCTURE,  C_STRUCTURE_PERE, GRP_OWNER, GRP_ACCES)
	   		  VALUES (my_c_structure, my_pers_id, '04 - FOURNISSEURS VALIDES (INTERNES)', 'FOU-INT', 'A', my_c_structure_pere, my_grp_owner, '-');
	   INSERT INTO REPART_TYPE_GROUPE(c_structure,tgrp_code,d_creation,d_modification) VALUES (my_c_structure, 'G', SYSDATE, SYSDATE);
    ELSE
        -- on recupere le c_structure du groupe qui va bien
        SELECT c_structure INTO my_c_structure FROM GRHUM.STRUCTURE_ULR WHERE LOWER(ll_structure) LIKE '%interne%' AND c_structure_pere = my_c_structure_pere AND ROWNUM=1;
	END IF;

    -- recherche d un param_ordre pour ajouter le parametre...
	SELECT MAX(PARAM_ORDRE) + 1 INTO my_param_ordre FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY LIKE 'ANNUAIRE_%';
	SELECT COUNT(*) INTO my_nb FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE = my_param_ordre;
	IF my_nb > 0 THEN
	   SELECT MAX(PARAM_ORDRE) + 1 INTO my_param_ordre FROM GRHUM.GRHUM_PARAMETRES;
	END IF;

	-- ajout du parametre
	INSERT INTO GRHUM.GRHUM_PARAMETRES VALUES(my_param_ordre, 'ANNUAIRE_FOU_VALIDE_INTERNE', TO_CHAR(my_c_structure), 'C_structure du groupe des fournisseurs valides (Internes)');
  END IF;

  SELECT param_value INTO my_annuaire_fou_valide_interne FROM grhum.GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_VALIDE_INTERNE';

  OPEN c_struct_internes;
  LOOP
	FETCH c_struct_internes INTO my_pers_id;
	EXIT WHEN c_struct_internes %NOTFOUND;

	my_fou_ordre := NULL;
	SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = my_pers_id AND fou_valide = 'O';
	IF my_nb > 0 THEN
	  SELECT fou_ordre INTO my_fou_ordre FROM grhum.FOURNIS_ULR WHERE pers_id = my_pers_id AND fou_valide = 'O' AND ROWNUM = 1;
	ELSE
	  SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = my_pers_id AND fou_valide = 'A';
	  IF my_nb > 0 THEN
	    SELECT fou_ordre INTO my_fou_ordre FROM grhum.FOURNIS_ULR WHERE pers_id = my_pers_id AND fou_valide = 'A' AND ROWNUM = 1;
		UPDATE grhum.FOURNIS_ULR SET fou_valide = 'O', d_modification = SYSDATE WHERE fou_ordre = my_fou_ordre;
		UPDATE grhum.VALIDE_FOURNIS_ULR SET val_date_val = SYSDATE WHERE fou_ordre = my_fou_ordre;
		DELETE FROM grhum.REPART_STRUCTURE WHERE pers_id = my_pers_id
		  AND c_structure = (SELECT param_value FROM grhum.GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_ARCHIVES');
	  ELSE
	    SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = my_pers_id AND fou_valide = 'N';
	    IF my_nb > 0 THEN
	      SELECT fou_ordre INTO my_fou_ordre FROM grhum.FOURNIS_ULR WHERE pers_id = my_pers_id AND fou_valide = 'N' AND ROWNUM = 1;
	    END IF;
	  END IF;
	END IF;

	IF my_fou_ordre IS NULL THEN
	  -- on cree le fournis...
	  SELECT COUNT(*) INTO my_nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
	    WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O' AND RPA.tadr_code = 'FACT' AND RPA.rpa_principal = 'O';
	  IF my_nb > 0 THEN
	    -- une ou plusieurs adresses type FACT principales ==> on prend la premiere
    	SELECT RPA.adr_ordre INTO my_adr_ordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
          WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O' AND RPA.tadr_code = 'FACT' AND RPA.rpa_principal = 'O' AND ROWNUM = 1;
      ELSE
    	SELECT COUNT(*) INTO my_nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
          WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O' AND RPA.tadr_code = 'FACT';
    	IF my_nb > 0 THEN
		    -- une ou plusieurs adresses type FACT ==> on prend la premiere
            SELECT RPA.adr_ordre INTO my_adr_ordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
              WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O' AND RPA.tadr_code = 'FACT' AND ROWNUM = 1;
		ELSE
		    -- aucune adresse de type FACT, on cherche les autres (principale en priorit�)
			SELECT COUNT(*) INTO my_nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
			  WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O' AND RPA.rpa_principal = 'O';
			IF my_nb > 0 THEN
			    -- une ou plusieurs adresses principales ==> on prend la premi�re
				SELECT RPA.adr_ordre INTO my_adr_ordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
				  WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O' AND RPA.rpa_principal = 'O' AND ROWNUM = 1;
			ELSE
				SELECT COUNT(*) INTO my_nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
				  WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O';
				IF my_nb > 0 THEN
				    -- premi�re adresse valide
					SELECT RPA.adr_ordre INTO my_adr_ordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
					  WHERE RPA.pers_id = my_pers_id AND RPA.rpa_valide = 'O' AND ROWNUM = 1;
				ELSE
					RAISE_APPLICATION_ERROR(-20002, 'Aucune adresse valide trouv�e pour le pers_id ' || my_pers_id);
				END IF;
			END IF;
		END IF;
	  END IF;

	  -- creation fournis
	  grhum.Ins_Fournis_Ulr_Annuaire(
				my_pers_id,		  			  --INS_PERS_ID         FOURNIS_ULR.pers_id%TYPE,
				my_adr_ordre,				  --INS_ADR_ORDRE 		FOURNIS_ULR.adr_ordre%TYPE,
				SYSDATE,			  		  --INS_FOU_DATE        FOURNIS_ULR.fou_date%TYPE,
				'0',				  		  --INS_FOU_MARCHE      FOURNIS_ULR.fou_marche%TYPE,
				'O',				  		  --INS_FOU_VALIDE      FOURNIS_ULR.fou_valide%TYPE,
				NULL,			  		  	  --INS_AGT_ORDRE       FOURNIS_ULR.AGT_ORDRE%TYPE,
				'F',				  		  --INS_FOU_TYPE        FOURNIS_ULR.fou_type%TYPE
				'N',				  		  --INS_FOU_ETRANGER    FOURNIS_ULR.fou_etranger%TYPE
				my_fou_ordre		  		  --FOU_ORDRE           FOURNIS_ULR.fou_ordre%TYPE
				);
    END IF; -- fin my_fou_ordre is null

	Ins_Repart_Structure(my_pers_id, my_annuaire_fou_valide_interne);

  END LOOP;
  CLOSE c_struct_internes;

END;
/


CREATE OR REPLACE PROCEDURE GRHUM.Install_Params_Annuaire
(
 ins_nom         INDIVIDU_ULR.nom_usuel%TYPE,
 ins_prenom      INDIVIDU_ULR.prenom%TYPE,
 ins_login      COMPTE.cpt_login%TYPE
)

IS

localPersIdStructure INTEGER;
localCStructure         VARCHAR2(10);

localPersIdIndividu     INTEGER;
localNoIndividu         INTEGER;

localPersIdStructureDivers   INTEGER;
localCStructureDivers         VARCHAR2(10);

localPersIdSousStructure INTEGER;
localCSousStructure         VARCHAR2(10);

localCptOrdre         INTEGER;

BEGIN

     -- Mise a jour des tables de constantes

    /* insert into grhum_parametres select * from grhum_parametres@gest_ulr;
     insert into type_groupe select * from type_groupe@gest_ulr;
     insert into vlans select * from vlans@gest_ulr;
     insert into civilite select * from civilite@gest_ulr;
     insert into type_structure select * from type_structure@gest_ulr;
     insert into type_adresse select * from type_adresse@gest_ulr;
     insert into pays select * from pays@gest_ulr;
     insert into situation_familiale select * from situation_familiale@gest_ulr;
     insert into activites select * from activites@gest_ulr;
     insert into titre_ape select * from titre_ape@gest_ulr;
     insert into ape select * from ape@gest_ulr;
     insert into banque select * from banque@gest_ulr;
     insert into academie select * from academie@gest_ulr;
     insert into region select * from region@gest_ulr;
     insert into departement select * from departement@gest_ulr;
     insert into commune select * from commune@gest_ulr;
     insert into rne select * from rne@gest_ulr;
     insert into naf select * from naf@gest_ulr;
     insert into formes_juridiques_details select * from formes_juridiques_details@gest_ulr;
     insert into formes_juridiques_categories select * from formes_juridiques_categories@gest_ulr;
     insert into formes_juridiques_familles select * from formes_juridiques_familles@gest_ulr;
    */

     -- Insertion d'une structure de d�part : Ce sera la racine du navigateur de l'application Annuaire
     -- Le libelle de cette structure sera modifiable par l'application annuaire . Il est mis par d�faut � 'UNIVERSITE' .
       GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'UNIVERSITE', NULL, 'E', NULL, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL );

     -- La racine de l'etablissement : c_structure = c_structure_pere and c_type_structure = 'E'
     UPDATE GRHUM.STRUCTURE_ULR SET c_structure_pere = localCStructure WHERE c_structure = localCStructure;

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

     -- Un individu doit etre cree au depart. Ce sera la personne qui installe l'applications.
     -- Cet individu est cree en fonction des parametres passes a la procedure.
     GRHUM.Ins_Individu_Ulr ( ins_nom, ins_prenom, 'M.', ins_nom, NULL, NULL, NULL, NULL, NULL,
                             NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, localPersIdIndividu, localNoIndividu );

     -- Un compte est cree et associe � la personne creee.
     --Cela permettra de faire le lien entre le login windows et la personne qui utilise l'application.
       GRHUM.Ins_Compte ( localPersIdIndividu, 999999, ins_login, NULL, 'N', 'P', NULL, NULL, 'N', localCptOrdre );

     -- On cree une structure par defaut qui permettra d'afficher les fiches des personnes qui n'ont pas de groupes d'affectation.
       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'DIVERS', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- Mise a jour de la table GRHUM_PARAMETRES. On definit un super_user, et on renseigne la structure par defaut
     -- ainsi que le nom du groupe qui contiendra les fournisseurs
     UPDATE GRHUM_PARAMETRES SET param_value = ins_login WHERE param_key = 'GRHUM_CREATEUR';
     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_STRUCTURE_DEFAUT';
     UPDATE GRHUM_PARAMETRES SET param_value = 'NON' WHERE param_key = 'GRHUM_FORUMS';
     UPDATE GRHUM_PARAMETRES SET param_value = 'NON' WHERE param_key = 'GRHUM_PHOTO';
     UPDATE GRHUM_PARAMETRES SET param_value = 'FOURNISSEUR' WHERE param_key = 'ANNUAIRE_LIBELLE_FOURNISSEURS';





     -- Insertion du groupe pere des SERVICE SCOLARITE
       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'SERVICE SCOLARITE', 'SCOL', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'S','O');

     -- Insertion du groupe pere des DOCTORANTS
       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'DOCTORANTS', 'DOCT', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- Insertion du groupe pere des DIPLOMES
       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'DIPLOMES', 'DIPL', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- Insertion du groupe pere des PARCOURS CLASSIQUES
       GRHUM.Ins_Structure_Ulr ( localCSousStructure, localPersIdSousStructure, 'PARCOURS CLASSIQUES', 'CLASSIQUES', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCSousStructure, 'G','O');

     -- Insertion du groupe pere des PARCOURS LMD
       GRHUM.Ins_Structure_Ulr ( localCSousStructure, localPersIdSousStructure, 'PARCOURS LMD', 'LMD', 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCSousStructure, 'G','O');





     -- Insertion du groupe pere des fournisseurs
       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEUR', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

     -- Fournisseurs annul�s
       GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'FOURNISSEURS ANNULES', NULL, 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructure WHERE param_key = 'ANNUAIRE_FOU_ARCHIVES';

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

     -- Fournisseurs en cours de validation
     GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'FOURNISSEURS EN COURS DE VALIDATION', NULL, 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructure WHERE param_key = 'ANNUAIRE_FOU_ENCOURS_VALIDE';

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

     -- Fournisseurs valides
       GRHUM.Ins_Structure_Ulr ( localCStructure, localPersIdStructure, 'FOURNISSEURS VALIDES', NULL, 'A', localCStructureDivers, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

        -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');

     -- Insertion du groupe pere des fournisseurs
       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (INDIVIDUS)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

       UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_PHYSIQUE';

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (STRUCTURES)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_MORALE';

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     
       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (INTERNES)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_INTERNE';

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');

       GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'FOURNISSEURS VALIDES (EXTERNES)', NULL, 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );

     UPDATE GRHUM_PARAMETRES SET param_value = localCStructureDivers WHERE param_key = 'ANNUAIRE_FOU_VALIDE_EXTERNE';

     -- Tous les groupes affich�s dans le navigateur d'Annuaire ont un type groupe = 'G'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructureDivers, 'G','O');
     
     
     -- cr�ation de la structure 'PARTENARIAT'
     GRHUM.Ins_Structure_Ulr ( localCStructureDivers, localPersIdStructureDivers, 'PARTENARIAT', 'PARTENARIAT', 'A', localCStructure, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
	 
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'G','O');
     -- ce groupe est typ� 'Partyenariat'.
     GRHUM.Maj_Repart_Type_Groupe(localCStructure, 'PN','N');
     
END;
/



CREATE OR REPLACE PROCEDURE GRHUM.Ins_Cpt_Temporaire
(
 	   ins_pers_id   		STRUCTURE_ULR.pers_id%TYPE,
	   ins_cpt_ordre 	    COMPTE.cpt_ordre%TYPE,
	   ins_c_structure 	    STRUCTURE_ULR.c_structure%TYPE,		   -- groupe d'appartenance du gars qui a cree le compte
	   ins_type_groupe 		STRUCTURE_ULR.c_structure%TYPE,	   -- groupe correspondant au type de compte cree
	   ins_date_debut       VARCHAR2,
	   ins_date_fin 	    VARCHAR2,
	   ins_c_civilite 		INDIVIDU_ULR.c_civilite%TYPE,
	   ins_nom				INDIVIDU_ULR.nom_usuel%TYPE,
	   ins_prenom			INDIVIDU_ULR.prenom%TYPE,
	   newlogin   			OUT   VARCHAR2,
	   newpasswd  			OUT   VARCHAR2,
	   newuid	  			OUT	  INTEGER
)
IS

cpt    				INTEGER;

newpersid			INTEGER;
newnoindividu 		INTEGER;
newcptordre 		INTEGER;

logincree 			VARCHAR2(20);
passwdcree 			VARCHAR2(15);
uidcree 			INTEGER;
gidGroupe			COMPTE_SUITE.cpt_gid%TYPE;
lmpasswd 			VARCHAR2(32);
ntpasswd 			VARCHAR2(32);
flag	   			VARCHAR2(18);

BEGIN

IF (ins_pers_id IS NOT NULL)		  -- L'individu est connu dans la base adresse
THEN
	 -- insertion groupe_adresses - Groupe d'appartenance du gars (ex : BU) et type de groupe (ex : Personnel ULR )
	 SELECT COUNT(*) INTO cpt FROM REPART_STRUCTURE WHERE pers_id = ins_pers_id AND c_structure = ins_c_structure;
	 IF (cpt = 0)
	 THEN
	 	 INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(ins_pers_id,ins_c_structure,SYSDATE,SYSDATE);
	 END IF;

	 SELECT COUNT(*) INTO cpt FROM REPART_STRUCTURE WHERE pers_id = ins_pers_id AND c_structure = ins_type_groupe;

	 IF (cpt = 0)
	 THEN
	 	 INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(ins_pers_id,ins_type_groupe,SYSDATE,SYSDATE);
	 END IF;

	 /*	 IF (ins_cpt_ordre > 0)	   Il a deja un compte -
		 THEN
		 	 SELECT cpt_passwd,cpt_login,cpt_uid_gid INTO newpasswd,newlogin,newuid FROM COMPTE WHERE cpt_ordre=ins_cpt_ordre;

			 logincree := newlogin;
			 passwdcree := newpasswd;
			 uidcree := newuid;
		 ELSE*/
		  	 logincree := Cons_Login(ins_nom,ins_prenom);
			 passwdcree := Cons_Passwd;
	      	 uidcree:= garnuche.uid_etudiant;
		 --END IF;

	   	 newlogin:=logincree;
		 newpasswd:=passwdcree;
		 newuid := uidcree;

		 -- insertion compte
	     SELECT COMPTE_SEQ.NEXTVAL INTO newcptordre FROM dual;
	    --select max(cpt_ordre + 1) into newcptordre from compte;

    	 INSERT INTO COMPTE(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,D_CREATION,D_MODIFICATION)
         VALUES(newcptordre,uidcree,logincree,passwdcree,'N','E',LOWER(ins_prenom)||'.'||LOWER(ins_nom),'etudiant.univ-lr.fr','N',SYSDATE,SYSDATE);

		 -- insertion dans compte validite
	     INSERT INTO COMPTE_VALIDITE VALUES(newcptordre,ins_date_debut,ins_date_fin);

		 -- 05/07/05 ajout du GID
		 gidGroupe := NULL;
		 -- recuperation du GID du groupe d'appartenance
		 SELECT COUNT(*) INTO cpt FROM COMPTE WHERE cpt_ordre =
		 (SELECT cpt_ordre FROM REPART_COMPTE WHERE pers_id =
		 (SELECT pers_id FROM STRUCTURE_ULR WHERE c_structure=ins_type_groupe));
		 IF ( cpt = 1 ) THEN
			 SELECT cpt_uid_gid  INTO gidGroupe FROM COMPTE WHERE cpt_ordre =
			 (SELECT cpt_ordre FROM REPART_COMPTE WHERE pers_id =
			 (SELECT pers_id FROM STRUCTURE_ULR WHERE c_structure=ins_type_groupe));
		 END IF;

		 INSERT INTO COMPTE_SUITE(CPT_ORDRE,CPT_HOME,CPT_SHELL,D_CREATION,D_MODIFICATION,CPT_GID)
		 VALUES(newcptordre,'/Utilisateurs/'||logincree,'/bin/bash',SYSDATE,SYSDATE,gidGroupe);

		 -- Configuration Samba
		 lmpasswd := GRHUM.Crypt_Lm_Samba(passwdcree);
		 ntpasswd := GRHUM.Crypt_Nt_Samba(passwdcree);
		 flag := '[UX         ]';
		 INSERT INTO COMPTE_SAMBA VALUES(newcptordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);

	     -- insertion repart_compte
	     INSERT INTO REPART_COMPTE VALUES(newcptordre,ins_pers_id,SYSDATE,SYSDATE);


ELSE 		-- Pas d'adrordre --> on construit une nouvelle adresse
	 logincree := Cons_Login(ins_nom,ins_prenom);
     passwdcree := Cons_Passwd;
     uidcree:=garnuche.uid_etudiant;

   	 newlogin:=logincree;
	 newpasswd:=passwdcree;
	 newuid:=uidcree;

	 SELECT COUNT(*) INTO cpt  FROM INDIVIDU_ULR WHERE nom_usuel= ins_nom AND prenom= ins_prenom;

	 IF(cpt = 1)  THEN -- c'est gagn� on insert avec le pers_id

		 SELECT pers_id INTO newpersid FROM INDIVIDU_ULR WHERE nom_usuel= ins_nom AND prenom= ins_prenom;
	ELSE
		 IF(cpt = 0) THEN
		    Ins_Individu_Ulr(ins_nom,ins_prenom,ins_c_civilite,ins_nom,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'X',NULL,NULL,NULL,NULL,NULL,'N',newpersid,newnoindividu);
		  END IF;
 	END IF;
	 -- insertion compte

	 SELECT COMPTE_SEQ.NEXTVAL INTO newcptordre FROM dual;
	 --select max(cpt_ordre + 1) into newcptordre from compte;

   	 INSERT INTO COMPTE(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,D_CREATION,D_MODIFICATION)
     VALUES(newcptordre,uidcree,logincree,passwdcree,'N','E',LOWER(ins_prenom)||'.'||LOWER(ins_nom),'etudiant.univ-lr.fr','N',SYSDATE,SYSDATE);

	 	 -- insertion compte validite
	 INSERT INTO COMPTE_VALIDITE VALUES(newcptordre,ins_date_debut,ins_date_fin);

	 -- 05/07/05 ajout du GID
	 gidGroupe := NULL;
		 -- recuperation du GID du groupe d'appartenance
		 SELECT COUNT(*) INTO cpt FROM COMPTE WHERE cpt_ordre =
		 (SELECT cpt_ordre FROM REPART_COMPTE WHERE pers_id =
		 (SELECT pers_id FROM STRUCTURE_ULR WHERE c_structure=ins_type_groupe));
		 IF ( cpt = 1 ) THEN
			 SELECT cpt_uid_gid  INTO gidGroupe FROM COMPTE WHERE cpt_ordre =
			 (SELECT cpt_ordre FROM REPART_COMPTE WHERE pers_id =
			 (SELECT pers_id FROM STRUCTURE_ULR WHERE c_structure=ins_type_groupe));
		 END IF;

	 INSERT INTO COMPTE_SUITE(CPT_ORDRE,CPT_HOME,CPT_SHELL,D_CREATION,D_MODIFICATION,CPT_GID)
	 VALUES(newcptordre,'/Utilisateurs/'||logincree,'/bin/bash',SYSDATE,SYSDATE,gidGroupe);

	 -- Configuration Samba
	 lmpasswd := GRHUM.Crypt_Lm_Samba(passwdcree);
	 ntpasswd := GRHUM.Crypt_Nt_Samba(passwdcree);
	 flag := '[UX         ]';
	 INSERT INTO GRHUM.COMPTE_SAMBA VALUES(newcptordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);

	 -- insertion repart_compte
	 INSERT INTO REPART_COMPTE VALUES(newcptordre,newpersid,SYSDATE,SYSDATE);

	 -- insertion repart_structure
	 SELECT COUNT(*) INTO cpt FROM REPART_STRUCTURE WHERE pers_id = newpersid AND c_structure = ins_c_structure;
	 IF (cpt =0 ) THEN
	 	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(newpersid,ins_c_structure,SYSDATE,SYSDATE);
	 END IF;

	 SELECT COUNT(*) INTO cpt FROM REPART_STRUCTURE WHERE pers_id = newpersid AND c_structure = ins_type_groupe;
	 IF (cpt =0 ) THEN
	  	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(newpersid,ins_type_groupe,SYSDATE,SYSDATE);
	 END IF;

END IF;

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Ins_Compte_Prof(
INS_CPT_LOGIN       COMPTE.cpt_login%TYPE,
INS_CPT_PASSWD      COMPTE.cpt_passwd%TYPE,
INS_CPT_UID     	COMPTE.cpt_uid_gid%TYPE,
INS_CPT_GID     	COMPTE_SUITE.cpt_gid%TYPE,
INS_CPT_SHELL       COMPTE_SUITE.cpt_shell%TYPE,
INS_CPT_HOME      	COMPTE_SUITE.cpt_home%TYPE,
INS_NOM				INDIVIDU_ULR.nom_usuel%TYPE,
INS_PRENOM			INDIVIDU_ULR.prenom%TYPE,
INS_DATE_NAISSANCE	VARCHAR2
) IS

/*
  Auteur : CRI PB
  Date de cr�ation : 01/02/2005
  Date de modification : 16/01/2008
  Objectif : proc�dure utilis�e lors de la cr�ation du compte syst�me pour �viter de saisir ces m�mes informations dans l'Annuaire.
  		   	 		Par d�faut, le VLAN est le 10.4, Recherche. Donc on suppose qu'une v�rification au niveau de l'Annuaire sera faite apr�s la cr�ation.
*/

-- ------------
-- DECLARATIONS
-- ------------

nbr 		   		INTEGER;
cptordre 			INTEGER;
cStructUnix			STRUCTURE_ULR.c_structure%TYPE;
INS_PERS_ID         REPART_COMPTE.pers_id%TYPE;
INS_CPT_VLAN     	COMPTE.cpt_VLAN%TYPE;
INS_CPT_DOMAINE     COMPTE.cpt_DOMAINE%TYPE;
INS_CPT_EMAIL       COMPTE.cpt_email%TYPE;
INS_CPT_CONNEXION   COMPTE.cpt_connexion%TYPE;
INS_CPT_CHARTE      COMPTE.cpt_CHARTE%TYPE;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

-- Verif. de l'existence de l'individu
IF (INS_DATE_NAISSANCE IS NOT NULL AND LENGTH(INS_DATE_NAISSANCE) = 10) THEN
   SELECT COUNT(*) INTO nbr FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom)
   AND TO_CHAR(d_naissance,'dd/mm/yyyy') = INS_DATE_NAISSANCE;
ELSE
   SELECT COUNT(*) INTO nbr FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom);
END IF;
IF (nbr = 0) THEN
   RAISE_APPLICATION_ERROR(-20001,'INS_COMPTE_PROF : L''individu '||ins_prenom||' '||ins_nom||' est inconnu !');
END IF;
IF (nbr >1) THEN
   RAISE_APPLICATION_ERROR(-20002,'INS_COMPTE_PROF : Plusieurs individus connus dans l''Annuaire sous l''identit� : '||ins_prenom||' '||ins_nom||' !');
END IF;

-- Recup�ration du pers_id de l'individu
IF (INS_DATE_NAISSANCE IS NOT NULL AND LENGTH(INS_DATE_NAISSANCE) = 10) THEN
   SELECT pers_id INTO INS_PERS_ID FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom)
   AND TO_CHAR(d_naissance,'dd/mm/yyyy') = INS_DATE_NAISSANCE;
ELSE
   SELECT pers_id INTO INS_PERS_ID FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom);
END IF;

--ins_pers_id :=125439;

-- Clef de la table COMPTE
SELECT COMPTE_SEQ.NEXTVAL INTO cptordre FROM dual;

-- Valeurs par d�faut
INS_CPT_VLAN := 'R';
INS_CPT_DOMAINE := 'univ-lr.fr';
INS_CPT_EMAIL := LOWER(REPLACE(ins_prenom,' ','_')) || '.' || LOWER(REPLACE(ins_nom,' ','_'));
INS_CPT_CONNEXION := 'N';
INS_CPT_CHARTE := 'N';

-- Table des COMPTES
INSERT INTO COMPTE(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,D_CREATION,D_MODIFICATION)
VALUES (cptordre,INS_CPT_UID,INS_CPT_LOGIN,INS_CPT_PASSWD,INS_CPT_CONNEXION,INS_CPT_VLAN,INS_CPT_EMAIL,INS_CPT_DOMAINE,INS_CPT_CHARTE,SYSDATE,SYSDATE);

INSERT INTO REPART_COMPTE VALUES (cptordre,INS_PERS_ID,SYSDATE,SYSDATE);

GRHUM.Ins_Compte_Suite(cptordre,INS_CPT_GID ,INS_CPT_SHELL,INS_CPT_HOME);

-- Ajout de l'appartenance au groupe Unix en fonction du GID
SELECT COUNT(*) INTO nbr
FROM COMPTE C, REPART_COMPTE RC, STRUCTURE_ULR S
WHERE cpt_uid_gid = INS_CPT_GID
AND cpt_vlan = 'G'
AND C.cpt_ordre = RC.cpt_ordre
AND RC.pers_id = S.pers_id;

IF (nbr = 1) THEN
   -- r�cup�ration du cStructure du groupe Unix
   SELECT S.c_structure INTO cStructUnix
   FROM COMPTE C, REPART_COMPTE RC, STRUCTURE_ULR S
   WHERE cpt_uid_gid = INS_CPT_GID
   AND cpt_vlan = 'G'
   AND C.cpt_ordre = RC.cpt_ordre
   AND RC.pers_id = S.pers_id;

   -- Individu d�j� dans le groupe Unix ?
   SELECT COUNT(*) INTO nbr FROM REPART_STRUCTURE
   WHERE c_structure = cStructUnix AND pers_id = INS_PERS_ID;
   IF (nbr = 0) THEN
      INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(INS_PERS_ID,cStructUnix,SYSDATE,SYSDATE);
	END IF;

END IF;

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Ins_Compte_Etud(
INS_CPT_LOGIN           COMPTE.cpt_login%TYPE,
INS_CPT_PASSWD      	COMPTE.cpt_passwd%TYPE,
INS_CPT_UID     	 	COMPTE.cpt_uid_gid%TYPE,
INS_CPT_GID     		COMPTE_SUITE.cpt_gid%TYPE,
INS_CPT_SHELL       	COMPTE_SUITE.cpt_shell%TYPE,
INS_CPT_HOME      		COMPTE_SUITE.cpt_home%TYPE,
INS_NOM					INDIVIDU_ULR.nom_usuel%TYPE,
INS_PRENOM				INDIVIDU_ULR.prenom%TYPE,
INS_DATE_NAISSANCE		VARCHAR2, -- format de la date : DD/MM/YYYY
INS_CODE_GROUPE			STRUCTURE_ULR.c_structure%TYPE
) IS

/*
  Auteur : CRI PB
  Date de cr�ation : 03/03/2005
  Date de modification : 16/01/2008
  Objectif : proc�dure utilis�e lors de la cr�ation du compte syst�me pour �viter de saisir ces m�mes informations dans l'Annuaire.
  		   	 		Par d�faut, le VLAN est le 10.2, Etudiant.
					Le code du groupe pass� en parametres indique le c_structure dans lequel on affecte l'individu (ex : "ULR - Personnels" sous Comptes Tempo)
*/

-- ------------
-- DECLARATIONS
-- ------------

nbr                 INTEGER;
nbr2				INTEGER;
cptordre 			INTEGER;
cStructUnix			STRUCTURE_ULR.c_structure%TYPE;
INS_PERS_ID         REPART_COMPTE.pers_id%TYPE;
INS_CPT_VLAN     	COMPTE.cpt_VLAN%TYPE;
INS_CPT_DOMAINE     COMPTE.cpt_DOMAINE%TYPE;
INS_CPT_EMAIL       COMPTE.cpt_email%TYPE;
INS_CPT_CONNEXION   COMPTE.cpt_connexion%TYPE;
INS_CPT_CHARTE      COMPTE.cpt_CHARTE%TYPE;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

-- Verif. de l'existence de l'individu
IF (LENGTH(INS_DATE_NAISSANCE) = 10) THEN
   SELECT COUNT(*) INTO nbr FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom)
   AND TO_CHAR(d_naissance,'dd/mm/yyyy') = INS_DATE_NAISSANCE;
ELSE
   SELECT COUNT(*) INTO nbr FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom);
END IF;
IF (nbr = 0) THEN
   RAISE_APPLICATION_ERROR(-20001,'INS_COMPTE_ETUD : L''individu '||ins_prenom||' '||ins_nom||' est inconnu !');
END IF;
IF (nbr >1) THEN
   RAISE_APPLICATION_ERROR(-20002,'INS_COMPTE_ETUD : Plusieurs individus connus dans l''Annuaire sous l''identit� : '||ins_prenom||' '||ins_nom||' !');
END IF;

-- Recup�ration du pers_id de l'individu
IF (LENGTH(INS_DATE_NAISSANCE) = 10) THEN
   SELECT pers_id INTO INS_PERS_ID FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom)
   AND TO_CHAR(d_naissance,'dd/mm/yyyy') = INS_DATE_NAISSANCE;
ELSE
   SELECT pers_id INTO INS_PERS_ID FROM INDIVIDU_ULR
   WHERE tem_valide = 'O'
   AND nom_usuel = UPPER(ins_nom)
   AND prenom = UPPER(ins_prenom);
END IF;

-- Clef de la table COMPTE
SELECT COMPTE_SEQ.NEXTVAL INTO cptordre FROM dual;

-- Valeurs par d�faut
INS_CPT_VLAN := 'E';
INS_CPT_DOMAINE := 'etudiant.univ-lr.fr';
INS_CPT_EMAIL := LOWER(REPLACE(ins_prenom,' ','_')) || '.' || LOWER(REPLACE(ins_nom,' ','_'));
INS_CPT_CONNEXION := 'N';
INS_CPT_CHARTE := 'N';

-- Table des COMPTES
INSERT INTO COMPTE(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,D_CREATION,D_MODIFICATION)
VALUES (cptordre,INS_CPT_UID,INS_CPT_LOGIN,INS_CPT_PASSWD,INS_CPT_CONNEXION,INS_CPT_VLAN,INS_CPT_EMAIL,INS_CPT_DOMAINE,INS_CPT_CHARTE,SYSDATE,SYSDATE);

INSERT INTO REPART_COMPTE VALUES (cptordre,INS_PERS_ID,SYSDATE,SYSDATE);

GRHUM.Ins_Compte_Suite(cptordre,INS_CPT_GID ,INS_CPT_SHELL,INS_CPT_HOME);

-- Ajout de l'appartenance au groupe Unix en fonction du GID
SELECT COUNT(*) INTO nbr
FROM COMPTE C, REPART_COMPTE RC, STRUCTURE_ULR S
WHERE cpt_uid_gid = INS_CPT_GID
AND cpt_vlan = 'G'
AND C.cpt_ordre = RC.cpt_ordre
AND RC.pers_id = S.pers_id;

IF (nbr = 1) THEN
   SELECT S.c_structure INTO cStructUnix
   FROM COMPTE C, REPART_COMPTE RC, STRUCTURE_ULR S
   WHERE cpt_uid_gid = INS_CPT_GID
   AND cpt_vlan = 'G'
   AND C.cpt_ordre = RC.cpt_ordre
   AND RC.pers_id = S.pers_id;

   -- Individu d�j� dans le groupe Unix ?
   SELECT COUNT(*) INTO nbr FROM REPART_STRUCTURE
   WHERE c_structure = INS_CODE_GROUPE AND pers_id = INS_PERS_ID;
   IF (nbr = 0) THEN
      INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(INS_PERS_ID,cStructUnix,SYSDATE,SYSDATE);
	END IF;

END IF;

-- Ajout de l'appartenance au sous-groupe des "Comptes Temporaires" en fonction du code_groupe (parametre de la proc.)
-- Le groupe existe ?
SELECT COUNT(*) INTO nbr
FROM STRUCTURE_ULR WHERE c_structure=INS_CODE_GROUPE;

IF (nbr !=0) THEN

   -- Individu d�j� dans le groupe ?
   SELECT COUNT(*) INTO nbr2 FROM REPART_STRUCTURE
   WHERE c_structure = INS_CODE_GROUPE AND pers_id = INS_PERS_ID;

   IF (nbr2 = 0) THEN
   	  INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(INS_PERS_ID,INS_CODE_GROUPE,SYSDATE,SYSDATE);
   END IF;

END IF;

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Ins_Repart_Structure(
INS_PERS_ID        REPART_STRUCTURE.pers_id%TYPE,
INS_C_STRUCTURE    REPART_STRUCTURE.c_structure%TYPE
) IS
-- ------------
-- DECLARATIONS
-- ------------

cpt INTEGER;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

SELECT COUNT(*) INTO cpt FROM REPART_STRUCTURE WHERE pers_id=INS_PERS_ID AND c_structure=INS_C_STRUCTURE;
IF (cpt = 0)
THEN

	SELECT COUNT(*) INTO cpt FROM STRUCTURE_ULR
	WHERE c_structure=ins_c_structure;
	IF (cpt=0) THEN
	   RAISE_APPLICATION_ERROR(-20000,'INS_REPART_STRUCTURE : C_structure inconnu');
	END IF;

	SELECT COUNT(*) INTO cpt FROM v_personne
	WHERE pers_id=ins_pers_id;
	IF (cpt=0) THEN
   	   RAISE_APPLICATION_ERROR(-20000,'INS_REPART_STRUCTURE : Pers_id inconnu');
	END IF;

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
    VALUES (INS_PERS_ID,INS_C_STRUCTURE,SYSDATE,SYSDATE);
    
END IF;

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Maj_Grp_Etudiants_Ecole_Doct
IS
-- CRI ULR
-- creation : 15/03/2005
-- modification : 19/01/2009
-- objectif : mise � jour des groupes des �coles doctorales et du goupe des doctorants
-- prise en compte de l'�clatement de l'Ecole Doctorale (utilisation du type de groupe : tgrp_code = 'ED')
-- remarque : les groupes des �coles doctorales sont remis � 0 tandis que celui des doctorants est juste mis � jour vis � vis des nouveaus inscrits (pas de RAZ de ce groupe)
 
-- C_structure des �coles doctorales rattach�es au groupe des DOCTORANTS (racine d�finie dans GARNUCHE_PARAMETRES)
CURSOR c_struct_edoc IS
    select S.c_structure from structure_ulr S, repart_type_groupe R
    where S.C_STRUCTURE = R.C_STRUCTURE
    and R.TGRP_CODE = 'ED'
    and S.c_structure_pere = (SELECT param_commentaires FROM garnuche.garnuche_parametres WHERE param_key='GARNUCHE_GROUPE' AND param_value='DOCT')
	order by ll_structure;

-- C_structure des dipomes rattach�s � une �cole doctorales
CURSOR c_struct_diplomes(cStructPere VARCHAR) IS
    select c_structure from structure_ulr where c_structure_pere=cStructPere;

-- Pers_id des membres d'un groupe
CURSOR c_persid(cstructure VARCHAR) IS
   select pers_id from REPART_STRUCTURE where c_structure = cstructure;

persId      PERSONNE.PERS_ID%TYPE;
cStructDoct STRUCTURE_ULR.c_structure%TYPE;
cStructEdoc STRUCTURE_ULR.c_structure%TYPE;
cStructDipl STRUCTURE_ULR.c_structure%TYPE;
nbr	        INTEGER;

BEGIN

    SELECT param_commentaires INTO cStructDoct FROM garnuche.garnuche_parametres WHERE param_key='GARNUCHE_GROUPE' AND param_value='DOCT';
    
    -- Les �coles doctorales
	OPEN c_struct_edoc;
	LOOP
		FETCH c_struct_edoc INTO cStructEdoc;
 		EXIT WHEN c_struct_edoc%NOTFOUND;

        DELETE FROM REPART_STRUCTURE WHERE c_structure = cStructEdoc;

        -- Les dipl�mes de l'�cole doctorale
		OPEN c_struct_diplomes(cStructEdoc);
		LOOP
			FETCH c_struct_diplomes INTO cStructDipl;
			EXIT WHEN c_struct_diplomes%NOTFOUND;

            -- Les �tudiants du dipl�mes
    		OPEN c_persid(cStructDipl);
    		LOOP
    			FETCH c_persid INTO persId;
    			EXIT WHEN c_persid%NOTFOUND;

    			-- V�rification si l'�tudiant n'est pas d�j� dans le groupe Doctorants
    			SELECT COUNT(*) INTO nbr FROM REPART_STRUCTURE WHERE c_structure = cStructDoct AND pers_id = persId;
    			IF ( nbr = 0 ) THEN
    			   INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(persId,cStructDoct,SYSDATE,SYSDATE);
    			END IF;
            
       			-- V�rification si l'�tudiant n'est pas d�j� dans le groupe de l'�cole doctorale
    			SELECT COUNT(*) INTO nbr FROM REPART_STRUCTURE WHERE c_structure = cStructEdoc AND pers_id = persId;
    			IF ( nbr = 0 ) THEN
    			   INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(persId,cStructEdoc,SYSDATE,SYSDATE);
    			END IF;

      		END LOOP;
    		CLOSE c_persid;

  		END LOOP;
		CLOSE c_struct_diplomes;

  	END LOOP;
	CLOSE c_struct_edoc;

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Maj_Groupes_Founisseurs
-- Ce script va permettre d'affecter tous les fournisseurs de la base dans les groupes correspondant
-- (Fournisseurs annul�s, fournisseurs en cours de validation, fournisseurs valides (individus et structures).
-- A faire uniquement lorsque la table GRHUM_PARAMETRES sera remplie avec les bons code de structures.
IS

nbr	 INTEGER;

BEGIN

-- On vide tous les groupes correspondant aux fournisseurs
DELETE FROM REPART_STRUCTURE WHERE c_structure IN (SELECT param_value FROM GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_ARCHIVES');
DELETE FROM REPART_STRUCTURE WHERE c_structure IN (SELECT param_value FROM GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_ENCOURS_VALIDE');
DELETE FROM REPART_STRUCTURE WHERE c_structure IN (SELECT param_value FROM GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_VALIDE_PHYSIQUE');
DELETE FROM REPART_STRUCTURE WHERE c_structure IN (SELECT param_value FROM GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_VALIDE_MORALE');
DELETE FROM REPART_STRUCTURE WHERE c_structure IN (SELECT param_value FROM GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_ETUDIANT');
DELETE FROM REPART_STRUCTURE WHERE c_structure IN (SELECT param_value FROM GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_VALIDE_INTERNE');

-- Insertion des fournisseurs annul�s (type Individu)
INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
SELECT DISTINCT i.pers_id,param_value,SYSDATE,SYSDATE
FROM FOURNIS_ULR f,GRHUM_PARAMETRES,INDIVIDU_ULR i
WHERE i.pers_id = f.pers_id
AND fou_valide = 'A' AND param_key = 'ANNUAIRE_FOU_ARCHIVES'
AND i.tem_valide = 'O';

-- Insertion des fournisseurs annul�s (type Structure)
INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
SELECT DISTINCT u.pers_id,param_value,SYSDATE,SYSDATE
FROM FOURNIS_ULR f,GRHUM_PARAMETRES,STRUCTURE_ULR u
WHERE u.pers_id = f.pers_id
AND fou_valide = 'A' AND param_key = 'ANNUAIRE_FOU_ARCHIVES';

-- Insertion des fournisseurs en cours de validation (type Individu)
INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
SELECT DISTINCT f.pers_id,param_value,SYSDATE,SYSDATE
FROM FOURNIS_ULR f, GRHUM_PARAMETRES g, INDIVIDU_ULR i
WHERE f.pers_id = i.pers_id
AND f.fou_valide = 'N'
AND (i.ind_qualite != 'ETUDIANT' OR i.ind_qualite IS NULL)
AND param_key = 'ANNUAIRE_FOU_ENCOURS_VALIDE'
AND i.tem_valide = 'O';

-- Insertion des fournisseurs en cours de validation (type Structure)
INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
SELECT DISTINCT f.pers_id,param_value,SYSDATE,SYSDATE
FROM FOURNIS_ULR f, GRHUM_PARAMETRES g, STRUCTURE_ULR s
WHERE f.pers_id = s.pers_id
AND f.fou_valide = 'N'
AND param_key = 'ANNUAIRE_FOU_ENCOURS_VALIDE';


-- Insertion des fournisseurs valides de type structure (Personne morale)
INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
SELECT DISTINCT f.pers_id,param_value,SYSDATE,SYSDATE
FROM FOURNIS_ULR f, GRHUM_PARAMETRES g, STRUCTURE_ULR s
WHERE f.fou_valide = 'O'
AND f.pers_id = s.pers_id
AND param_key = 'ANNUAIRE_FOU_VALIDE_MORALE';

-- Insertion des fournisseurs valides de type individu (Personne physique)
INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
SELECT DISTINCT f.pers_id,param_value,SYSDATE,SYSDATE
FROM FOURNIS_ULR f, GRHUM_PARAMETRES g, INDIVIDU_ULR i
WHERE f.fou_valide = 'O'
AND f.pers_id = i.pers_id
AND param_key = 'ANNUAIRE_FOU_VALIDE_PHYSIQUE'
AND i.tem_valide = 'O';

SELECT COUNT(*) INTO nbr FROM GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_ETUDIANT';
IF ( nbr != 0 ) THEN
   -- Insertion des fournisseurs valides Etudiant
   INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
   SELECT DISTINCT f.pers_id,param_value,SYSDATE,SYSDATE
   FROM FOURNIS_ULR f, GRHUM_PARAMETRES g, ETUDIANT e , INDIVIDU_ULR i
   WHERE f.fou_valide = 'N'
   AND	i.no_individu = e.no_individu
   AND f.pers_id = i.pers_id
   AND param_key = 'ANNUAIRE_FOU_ETUDIANT'
   AND i.tem_valide = 'O';
END IF;

-- Insertion des fournisseurs internes
INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
SELECT DISTINCT f.pers_id,param_value,SYSDATE,SYSDATE
FROM FOURNIS_ULR f, GRHUM_PARAMETRES g, STRUCTURE_ULR s, REPART_TYPE_GROUPE r
WHERE s.C_STRUCTURE = r.C_STRUCTURE
AND r.TGRP_CODE = 'LB'
AND f.fou_valide = 'O'
AND f.pers_id = s.pers_id
AND param_key = 'ANNUAIRE_FOU_VALIDE_INTERNE';

-- MAJ_GROUPES_FOURNISSEURS
-- Procedure permettant d'affecter tous les fournisseurs de la base dans les groupes correspondant
-- (Fournisseurs annul�s, fournisseurs en cours de validation, fournisseurs valides (individus et structures).

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Maj_Groupes_Employes
IS
-- Auteur : CRI - PB
-- creation : 25/11/2005
-- modification : 24/06/2008

-- PB : 23/04/07 : ajout des doctorants dans le forum ULR : DT n� 47994

CURSOR c1 IS
	   -- Les employ�s de l'�tablissement
	   SELECT I.pers_id
	   FROM v_personnel_actuel_bis V, INDIVIDU_ULR I
	   WHERE V.no_dossier_pers = I.no_individu
	   UNION
	   -- Les doctorants
	   SELECT pers_id FROM REPART_STRUCTURE WHERE c_structure = '1431';

nbr	   		 				 INTEGER;
persId					  INDIVIDU_ULR.pers_id%TYPE;
email					  COMPTE.cpt_email%TYPE;

BEGIN

	-- GROUPE VACATAIRES
    DELETE FROM REPART_STRUCTURE WHERE c_structure = '1424';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT i.pers_id,'1424',SYSDATE,SYSDATE
	FROM v_personnel_actuel_vac v,INDIVIDU_ULR i
	WHERE v.no_dossier_pers = i.no_individu;

	 -- GROUPE EMPLOYES (61)
	DELETE FROM REPART_STRUCTURE WHERE c_structure='61';
	-- utilisation de la vue v_personnel_actuel_bis pour ne pas avoir les CFA dans le groupe
	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	   SELECT DISTINCT i.pers_id,'61',SYSDATE,SYSDATE FROM v_personnel_actuel_bis v,INDIVIDU_ULR i WHERE v.no_dossier_pers = i.no_individu;

	-- GROUPE "FORUM de L'UNIVERSITE de LA ROCHELLE" (27360)
	-- PB 05/10/2006 : le groupe du Forum ULR est l'image du groupe des Employes.
	DELETE FROM REPART_STRUCTURE WHERE c_structure='27360';
	-- PB 08/01/2007 : ajout des membres de l'ULR VALOR
	-- Valerie FONTELLE
	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(97906,'27360',SYSDATE,SYSDATE);
	-- Jean-Marc WALLET
	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(1156,'27360',SYSDATE,SYSDATE);
	-- Thomas THIMONIER (VP ETUDIANT)
	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(69675,'27360',SYSDATE,SYSDATE);
    -- Infos DIRECT : de mani�re � pouvoir poster de la par de directinfos su le forum ULR
	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(121815,'27360',SYSDATE,SYSDATE);
    
	-- M�J du groupe de "Forum de l'Universit� de La Rochelle" � partir des agents en poste : le groupe des EMPLOYES
	-- 23/04/07 : plus les doctorants
	OPEN c1;
	LOOP

	 	 FETCH c1 INTO persId;
		 EXIT WHEN c1%NOTFOUND;

		 --SELECT COUNT(*) INTO nbr FROM REPART_STRUCTURE WHERE c_structure='27360' AND pers_id = persId;

		 --IF ( nbr = 0 ) THEN
		 	-- L'agent poss�de-t-il un COMPTE Prof. avec un Email ?
			SELECT COUNT(*) INTO nbr FROM REPART_COMPTE RC, COMPTE C
			WHERE RC.cpt_ordre = C.cpt_ordre AND RC.pers_id = persId AND C.cpt_vlan IN ('P','R') AND C.cpt_email IS NOT NULL AND C.cpt_domaine IS NOT NULL;
			-- si OUI, on l'inscrit au Forum
			IF (nbr != 0) THEN
			   -- recup. de l'Email du COMPTE le plus prioritaire
			   SELECT C.cpt_email INTO email
			   FROM REPART_COMPTE  RC, COMPTE C, VLANS V
			   WHERE RC.cpt_ordre = C.cpt_ordre AND C.cpt_vlan = V.c_vlan AND RC.pers_id=persId
			   AND V.priorite = (SELECT MIN(priorite) FROM REPART_COMPTE  RC, COMPTE C, VLANS V
			   	   			  					WHERE RC.cpt_ordre = C.cpt_ordre AND C.cpt_vlan = V.c_vlan AND RC.pers_id=persId);
			   IF (email IS NOT NULL) THEN
                  SELECT COUNT(*) INTO nbr FROM REPART_STRUCTURE WHERE c_structure='27360' AND pers_id = persId;
                  IF (nbr = 0) then
		 	   	    INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES(persId,'27360',SYSDATE,SYSDATE);
                  END IF;
			   -- un COMPTE sans eMail
			   ELSE
			   	   DELETE FROM REPART_STRUCTURE WHERE pers_id=persId AND c_structure='27360';
			   END IF;
			-- pas de COMPTE
			--ELSE
			   --DELETE FROM REPART_STRUCTURE WHERE pers_id=persId AND c_structure='27360';
			END IF;
	     --END IF;

	 END LOOP;
	 CLOSE c1;

	-- GROUPE PERSONNEL IATOS
	DELETE FROM REPART_STRUCTURE WHERE c_structure='1414';
	-- utilisation de la vue v_personnel_actuel_non_ens_bis pour ne pas avoir les CFA dans le groupe
	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	   SELECT DISTINCT i.pers_id,'1414',SYSDATE,SYSDATE FROM v_personnel_actuel_non_ens_bis v,INDIVIDU_ULR i
	   	WHERE v.no_dossier_pers = i.no_individu;

   -- GROUPE PERSONNEL ENSEIGNANT
	DELETE FROM REPART_STRUCTURE WHERE c_structure='17008';
	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	   SELECT DISTINCT i.pers_id,'17008',SYSDATE,SYSDATE FROM v_personnel_actuel_ens v,INDIVIDU_ULR i
	   WHERE v.no_dossier_pers = i.no_individu;

	-- GROUPE CONTRACTUELS
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '1419';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT pers_id,'1419',SYSDATE,SYSDATE
	FROM CONTRAT c,INDIVIDU_ULR i
	WHERE i.no_individu = c.no_dossier_pers
		  AND c.c_type_contrat_trav NOT IN ('VF','VN')
		  AND c.d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
		  AND (c.d_fin_contrat_trav IS NULL OR c.d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY'));


 	-- GROUPE CONTRACTUELS  ENSEIGNANTS : on ajoute tous les contractuels et on ne garde que les enseignants
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '1416';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) SELECT DISTINCT pers_id,'1416',SYSDATE,SYSDATE FROM REPART_STRUCTURE WHERE c_structure = '1419';
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '1416' AND pers_id NOT IN (SELECT pers_id FROM REPART_STRUCTURE WHERE c_structure = '17008');

	-- GROUPE CONTRACTUELS IATOSS
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '23182';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT pers_id,'23182',SYSDATE,SYSDATE
	FROM CONTRAT c, TYPE_CONTRAT_TRAVAIL T, v_personnel_actuel_non_ens V, INDIVIDU_ULR i
	WHERE V.no_dossier_pers = c.no_dossier_pers
		  AND V.no_dossier_pers = I.no_individu
		  AND (c.c_type_contrat_trav NOT IN ('VF','VN'))
		  AND c.d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
		  AND (c.d_fin_contrat_trav IS NULL OR c.d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY'))
		  AND c.TEM_ANNULATION != 'O'
		  AND c.C_TYPE_CONTRAT_TRAV = T.C_TYPE_CONTRAT_TRAV
		  AND T.tem_titulaire != 'O' AND T.tem_Remuneration_Accessoire != 'O';

 	-- GROUPE IATOSS TITULAIRES : on ajoute tous les IATOSS et on enleve tous les contractuels
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '23183';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) SELECT DISTINCT pers_id,'23183',SYSDATE,SYSDATE FROM REPART_STRUCTURE WHERE c_structure = '1414';
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '23183' AND pers_id IN (SELECT pers_id FROM REPART_STRUCTURE WHERE c_structure = '23182');

	-- GROUPE EMPLOI JEUNES
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '1425';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT pers_id,'1425',SYSDATE,SYSDATE
	FROM CONTRAT c,INDIVIDU_ULR i
	WHERE i.no_individu = c.no_dossier_pers
		  AND (c.c_type_contrat_trav IN ('EJ'))
		  AND d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
		  AND (d_fin_contrat_trav IS NULL OR d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY'));

	-- GROUPE CES
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '1417';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT pers_id,'1417',SYSDATE,SYSDATE
	FROM CONTRAT c,INDIVIDU_ULR i
	WHERE i.no_individu = c.no_dossier_pers
		  AND c.c_type_contrat_trav IN ('CE')
		  AND c.d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
		  AND (c.d_fin_contrat_trav IS NULL OR c.d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY'));


	-- GROUPE CEC
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '17007';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT pers_id,'17007',SYSDATE,SYSDATE
	FROM CONTRAT c,INDIVIDU_ULR i
	WHERE i.no_individu = c.no_dossier_pers
		  AND c.c_type_contrat_trav IN ('CC')
		  AND c.d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
		  AND (c.d_fin_contrat_trav IS NULL OR c.d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY'));


    -- GROUPE ENSEIGNANTS CHERCHEURS
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '575';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT pers_id,'575',SYSDATE,SYSDATE
	FROM ELEMENT_CARRIERE ec,INDIVIDU_ULR i,CORPS c
	WHERE i.no_individu = ec.no_dossier_pers
		  AND ec.c_corps = c.c_corps
		  AND c.c_type_corps = 'SA'
		  AND d_effet_element <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
		  AND (d_fin_element IS NULL OR d_fin_element >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY'));

    -- GROUPE ENSEIGNANTS 2ND DEGRE
	DELETE FROM REPART_STRUCTURE WHERE c_structure = '1423';

	INSERT INTO REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION)
	SELECT DISTINCT pers_id,'1423',SYSDATE,SYSDATE
	FROM ELEMENT_CARRIERE ec,INDIVIDU_ULR i,CORPS c
	WHERE i.no_individu = ec.no_dossier_pers
		  AND ec.c_corps = c.c_corps
		  AND c.c_type_corps = 'DA'
		  AND d_effet_element <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
		  AND (d_fin_element IS NULL OR d_fin_element >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY'));


END;
/

CREATE OR REPLACE PROCEDURE JEFY_RECETTE.Ins_Client (
    a_pers_id                IN OUT GRHUM.PERSONNE.PERS_ID%TYPE,
    a_c_structure            IN OUT GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE,
    a_no_individu            IN OUT GRHUM.INDIVIDU_ULR.NO_INDIVIDU%TYPE,
    a_fou_ordre                IN OUT GRHUM.FOURNIS_ULR.fou_ordre%TYPE,
    a_type_client            VARCHAR2,
    a_c_civilite            VARCHAR2,
    a_nom                    VARCHAR2,
    a_prenom                VARCHAR2,
    a_adresse1                VARCHAR2,
    a_adresse2                VARCHAR2,
    a_bp                    VARCHAR2,
    a_cp                    VARCHAR2,
    a_cp_etranger            VARCHAR2,
    a_ville                    VARCHAR2,
    a_c_pays                VARCHAR2,
    a_tel                    VARCHAR2,
    a_fax                    VARCHAR2,
    a_login                    VARCHAR2,
    a_password                VARCHAR2,
    a_email                    VARCHAR2,
    a_rib_pays                VARCHAR2,
    a_rib_banque            VARCHAR2,
    a_rib_guichet            VARCHAR2,
    a_rib_compte            VARCHAR2,
    a_rib_cle                VARCHAR2,
    a_adr_ordre                GRHUM.ADRESSE.ADR_ORDRE%TYPE,
    a_utl_ordre             jefy_admin.utilisateur.UTL_ORDRE%type
    )
IS
  my_c_structure    grhum.STRUCTURE_ULR.c_structure%TYPE;
  my_adr_ordre        grhum.ADRESSE.adr_ordre%TYPE;
  my_cpt_ordre        grhum.COMPTE.cpt_ordre%TYPE;
  my_nom            VARCHAR2(500);
  my_prenom            VARCHAR2(500);
  my_nb                INTEGER;
BEGIN

  -- les 4 params IN OUT peuvent etre null --> tout est cree par la proc
  -- sinon on peut avoir un pers_id, et les 3 autres null --> un fournisseur client est cree a partir de ce pers_id (si existe pas deja)
  -- sinon on peut avoir les 4 cl�s remplies --> rien n est cree, sauf un rib associe au fournis (s il y en a un (de rib))
  -- si un adr_ordre est pass�, il sera affect� au fournisseur (pass� ou cr��)
  -- a_type_client : OBLIGATOIRE : '1' --> personne morale ; '2' --> personne physique

  -- passage en majuscules toujours
  my_nom := UPPER(a_nom);
  my_prenom := UPPER(a_prenom);

  my_adr_ordre := a_adr_ordre;

  -- recup du groupe des fournisseurs valides externes
  SELECT COUNT(*) INTO my_nb FROM grhum.GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_VALIDE_EXTERNE';
  IF my_nb <> 1 THEN
       RAISE_APPLICATION_ERROR(-20001, 'La parametre ANNUAIRE_FOU_VALIDE_EXTERNE est manquant dans la table grhum.grhum_parametres, impossible de cr�er un client externe.');
  END IF;
  SELECT param_value INTO my_c_structure FROM grhum.GRHUM_PARAMETRES WHERE param_key = 'ANNUAIRE_FOU_VALIDE_EXTERNE';

  -- si pas de personne, on la cr�e
  IF a_pers_id IS NULL THEN

    IF a_type_client = '1' THEN
      -- type personne MORALE

      -- cree la personne morale dans la bonne structure...
      GRHUM.Ins_Structure_Ulr(a_c_structure, a_pers_id, my_nom, SUBSTR(my_nom, 1, 10), 'A', my_c_structure, NULL, NULL,
           NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
           NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
           NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

      -- ajout des tels...
      IF a_tel IS NOT NULL THEN
             grhum.Ins_Personne_Telephone_New(a_pers_id, a_tel, 'TEL', 'PRF', NULL, NULL, NULL,'N');
      END IF;
      IF a_fax IS NOT NULL THEN
             grhum.Ins_Personne_Telephone_New(a_pers_id, a_fax, 'FAX', 'PRF', NULL, NULL, NULL,'N');
      END IF;

      -- plus le compte
      IF a_login IS NOT NULL THEN
           grhum.Ins_Compte(
            a_pers_id, 999999, a_login, a_password, 'N', 'X', SUBSTR(a_email, 1, INSTR(a_email, '@')-1),
            SUBSTR(a_email, INSTR(a_email, '@')+1), 'N', my_cpt_ordre);
        END IF;

    ELSE
      -- type personne PHYSIQUE

      -- cree la personne physique dans la bonne structure, avec ses tels et son compte...
      grhum.Ins_Individu_Web_New(
              a_c_civilite, my_c_structure, SUBSTR(a_email, INSTR(a_email, '@')+1),
              SUBSTR(a_email, 1, INSTR(a_email, '@')-1), a_login, a_password, NULL, a_fax, NULL,
              my_nom, my_prenom, a_tel, a_no_individu, a_pers_id);

    END IF;

    -- ajout de l adresse qui n est pas cree par les procs precedentes
    grhum.Ins_Adresse(
          a_pers_id, a_email, 'FACT', NULL, NULL, NULL, NULL, NULL, NULL, a_cp, a_ville, a_c_pays, a_cp_etranger,
          NULL, SYSDATE, NULL, a_adresse1, a_adresse2, a_bp, NULL, NULL, NULL, 'N', 'O', my_adr_ordre);

  ELSE -- a_pers_is not null

    -- recup des cl�s
    IF a_type_client = '1' THEN
      IF a_c_structure IS NULL THEN
        SELECT COUNT(*) INTO my_nb FROM grhum.STRUCTURE_ULR WHERE pers_id = a_pers_id;
        IF my_nb > 0 THEN
          SELECT c_structure INTO a_c_structure FROM grhum.STRUCTURE_ULR WHERE pers_id = a_pers_id AND ROWNUM = 1;
        END IF;
      END IF;
    ELSE
      IF a_no_individu IS NULL THEN
        SELECT COUNT(*) INTO my_nb FROM grhum.INDIVIDU_ULR WHERE pers_id = a_pers_id;
        IF my_nb > 0 THEN
          SELECT no_individu INTO a_no_individu FROM grhum.INDIVIDU_ULR WHERE pers_id = a_pers_id AND ROWNUM = 1;
        END IF;
      END IF;
    END IF;

    IF my_adr_ordre IS NULL THEN
      -- recup du adr_ordre avec creation si besoin
      SELECT COUNT(*) INTO my_nb FROM grhum.REPART_PERSONNE_ADRESSE
        WHERE pers_id = a_pers_id AND rpa_principal = 'O';
      IF my_nb > 0 THEN
        SELECT adr_ordre INTO my_adr_ordre FROM grhum.REPART_PERSONNE_ADRESSE
          WHERE pers_id = a_pers_id AND rpa_principal = 'O' AND ROWNUM=1;
      ELSE
        SELECT COUNT(*) INTO my_nb FROM grhum.REPART_PERSONNE_ADRESSE
          WHERE pers_id = a_pers_id AND tadr_code = 'FACT' AND rpa_valide = 'O';
        IF my_nb > 0 THEN
          SELECT adr_ordre INTO my_adr_ordre FROM grhum.REPART_PERSONNE_ADRESSE
            WHERE pers_id = a_pers_id AND tadr_code = 'FACT' AND rpa_valide = 'O' AND ROWNUM = 1;
        ELSE
          SELECT COUNT(*) INTO my_nb FROM grhum.REPART_PERSONNE_ADRESSE
            WHERE pers_id = a_pers_id AND tadr_code = 'PRO' AND rpa_valide = 'O';
          IF my_nb > 0 THEN
            SELECT adr_ordre INTO my_adr_ordre FROM grhum.REPART_PERSONNE_ADRESSE
              WHERE pers_id = a_pers_id AND tadr_code = 'PRO' AND rpa_valide = 'O' AND ROWNUM = 1;
          ELSE
            SELECT COUNT(*) INTO my_nb FROM grhum.REPART_PERSONNE_ADRESSE
              WHERE pers_id = a_pers_id AND rpa_valide = 'O';
            IF my_nb > 0 THEN
              SELECT adr_ordre INTO my_adr_ordre FROM grhum.REPART_PERSONNE_ADRESSE
                WHERE pers_id = a_pers_id AND rpa_valide = 'O' AND ROWNUM = 1;
            ELSE
              SELECT COUNT(*) INTO my_nb FROM grhum.REPART_PERSONNE_ADRESSE
                WHERE pers_id = a_pers_id;
              IF my_nb > 0 THEN
                SELECT adr_ordre INTO my_adr_ordre FROM grhum.REPART_PERSONNE_ADRESSE
                  WHERE pers_id = a_pers_id AND ROWNUM = 1;
              ELSE
                grhum.Ins_Adresse(
                    a_pers_id, a_email, 'FACT', NULL, NULL, NULL, NULL, NULL, NULL, a_cp, a_ville, a_c_pays, a_cp_etranger,
                    NULL, SYSDATE, NULL, a_adresse1, a_adresse2, a_bp, NULL, NULL, NULL, 'N', 'O', my_adr_ordre);
              END IF;
            END IF;
          END IF;
        END IF;
      END IF;
    END IF;
  END IF; -- fin if a_pers_id is null

  -- insertion dans le groupe des fournisseurs valides externes
  SELECT COUNT(*) INTO my_nb FROM grhum.REPART_STRUCTURE WHERE pers_id = a_pers_id AND c_structure = my_c_structure;
  IF my_nb = 0 THEN
       INSERT INTO grhum.REPART_STRUCTURE(PERS_ID,C_STRUCTURE,D_CREATION,D_MODIFICATION) VALUES (a_pers_id, my_c_structure, SYSDATE, SYSDATE);
  END IF;

  -- fournisseur ! :-(
  IF a_fou_ordre IS NULL THEN
    SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id;
    -- si aucun fournis avec ce pers_id...
    IF my_nb = 0 THEN
      grhum.Ins_Fournis_Ulr_Annuaire(a_pers_id, my_adr_ordre, SYSDATE, '0', 'O', a_utl_ordre, 'C', 'N', a_fou_ordre);
    ELSE
      -- y'en a (j y trouve un gout de pomme...)
      SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'O';
      IF my_nb > 0 THEN
        -- y a du valide
        SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'O' AND fou_type IN ('C', 'T');
        IF my_nb > 0 THEN
          -- y a du valide de type client ou tous... good !
          SELECT fou_ordre INTO a_fou_ordre FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'O' AND fou_type IN ('C', 'T') AND ROWNUM = 1;
        ELSE
          -- y a du valide de type F --> devient type T
          SELECT fou_ordre INTO a_fou_ordre FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'O' AND fou_type = 'F' AND ROWNUM = 1;
          UPDATE grhum.FOURNIS_ULR SET fou_type = 'T', d_modification = SYSDATE WHERE fou_ordre = a_fou_ordre;
        END IF;
      ELSE
        -- y a pas de valide... y aurait pas du annule par hasard...
          SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'A';
          IF my_nb > 0 THEN
          -- y a du annule --> on va le revalider...
            SELECT COUNT(*) INTO my_nb FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'A' AND fou_type IN ('T', 'C');
            IF my_nb > 0 THEN
            -- y a du annule de type client ou tous... on revalide ca...
            SELECT fou_ordre INTO a_fou_ordre FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'A' AND fou_type IN ('T', 'C') AND ROWNUM = 1;
            UPDATE grhum.FOURNIS_ULR SET fou_valide = 'O', d_modification = SYSDATE WHERE fou_ordre = a_fou_ordre;
          ELSE
            -- y a du annule de type F --> passe type T et revalide...
            SELECT fou_ordre INTO a_fou_ordre FROM grhum.FOURNIS_ULR WHERE pers_id = a_pers_id AND fou_valide = 'A' AND ROWNUM = 1;
            UPDATE grhum.FOURNIS_ULR SET fou_type = 'T', fou_valide = 'O', d_modification = SYSDATE WHERE fou_ordre = a_fou_ordre;
          END IF;
          UPDATE grhum.VALIDE_FOURNIS_ULR SET val_date_val = SYSDATE WHERE fou_ordre = a_fou_ordre;
          IF a_utl_ordre IS NOT NULL THEN
            UPDATE grhum.VALIDE_FOURNIS_ULR SET val_validation = a_utl_ordre WHERE fou_ordre = a_fou_ordre;
          END IF;
          DELETE FROM grhum.REPART_STRUCTURE WHERE pers_id = a_pers_id AND c_structure = (SELECT param_value FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_ARCHIVES');
        ELSE
            RAISE_APPLICATION_ERROR(-20001, 'Un fournisseur existe pour ce client, mais il est en instance de validation... il faut valider le fournisseur avant de pouvoir l''utiliser !');
          END IF;
      END IF;
    END IF;
  END IF;

  -- mise a jour de l adresse a utiliser...
  IF a_fou_ordre IS NOT NULL AND my_adr_ordre IS NOT NULL THEN
    UPDATE grhum.FOURNIS_ULR SET d_modification = SYSDATE, adr_ordre = my_adr_ordre WHERE fou_ordre = a_fou_ordre;
  END IF;

  -- rib s'il y en a un
  IF a_fou_ordre IS NOT NULL AND a_rib_banque IS NOT NULL THEN
    Ins_Client_Rib(a_fou_ordre, a_rib_pays, a_rib_banque, a_rib_guichet, a_rib_compte, a_rib_cle);
  END IF;

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Ins_Ribfour_Ulr(
INS_C_BANQUE        RIBFOUR_ULR.c_banque%TYPE,
INS_C_GUICHET       RIBFOUR_ULR.c_guichet%TYPE,
INS_CLE_RIB         RIBFOUR_ULR.cle_rib%TYPE,
INS_FOU_ORDRE       RIBFOUR_ULR.fou_ordre%TYPE,
INS_MOD_CODE        RIBFOUR_ULR.mod_code%TYPE,
INS_NO_COMPTE       RIBFOUR_ULR.no_compte%TYPE,
INS_RIB_TITCO       RIBFOUR_ULR.rib_titco%TYPE,
INS_RIB_VALIDE      RIBFOUR_ULR.rib_valide%TYPE,
INS_BIC				RIBFOUR_ULR.bic%TYPE,
INS_IBAN			RIBFOUR_ULR.iban%TYPE
) IS
-- ------------
-- DECLARATIONS
-- ------------

retourIban	   INTEGER;
cpt INTEGER;
rib_ordre NUMBER;
TitcoClair		 RIBFOUR_ULR.rib_titco%TYPE;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

SELECT COUNT(*) INTO cpt FROM RIBFOUR_ULR
WHERE fou_ordre=INS_FOU_ORDRE
AND c_banque=INS_C_BANQUE
AND c_guichet=INS_C_GUICHET
AND no_compte=INS_NO_COMPTE
AND rib_valide = 'O';

IF (cpt>0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_RIBFOUR_ULR : R�f�rence bancaire existante');
END IF;

/*
SELECT COUNT(*) INTO cpt
FROM RIBFOUR_ULR;

IF ( cpt = 0 )
THEN
  rib_ordre :=1;
ELSE
  SELECT MAX(rib_ordre)+1 INTO rib_ordre FROM RIBFOUR_ULR;
END IF;
*/

SELECT SEQ_RIBFOUR_ULR.NEXTVAL INTO rib_ordre FROM dual;

--rib_ordre sert uniquement � identifier le quadruplet (fou_ordre/c_banque/c_guichet/no_compte)
--et pour assurer une compatibilit� avec les donn�es li�es � l'ancienne base GASPARD

-- Modif. de Philippe le 26/01/04 pour avoir la clef du rib sur 2 caract�res
Verif_Rib(INS_C_BANQUE, INS_C_GUICHET ,INS_NO_COMPTE, INS_CLE_RIB);
-- pour Paris 2 car cle_rib est de type NUMBER
--Verif_Rib(INS_C_BANQUE, INS_C_GUICHET ,INS_NO_COMPTE,LPAD(TO_CHAR(INS_CLE_RIB),2,'0'));

-- Conversion en Majuscule du TITULAIRE du compte sans les caract�res accentu�s.
-- Rem : la fonction supprime aussi les espaces
--TitcoClair := Chaine_Claire(INS_RIB_TITCO);

-- Test de la clef du code IBAN
IF (INS_IBAN IS NOT NULL) THEN
   retourIban := Verif_Iban(INS_IBAN);
   IF (retourIban = 0) THEN
      RAISE_APPLICATION_ERROR(-20001,'INS_RIBFOUR_ULR : Le code IBAN est incorrect.');
   END IF;
END IF;

INSERT INTO RIBFOUR_ULR(RIB_ORDRE,FOU_ORDRE,C_BANQUE,C_GUICHET,NO_COMPTE,CLE_RIB,RIB_TITCO,MOD_CODE,RIB_VALIDE,D_CREATION,D_MODIFICATION,TEM_PAYE_UTIL,BIC,IBAN)
VALUES (
rib_ordre,
INS_FOU_ORDRE,
INS_C_BANQUE,
INS_C_GUICHET,
INS_NO_COMPTE,
INS_CLE_RIB,
INS_RIB_TITCO,			--TitcoClair
INS_MOD_CODE,
'O',
SYSDATE,
SYSDATE,
'N',
INS_BIC,
INS_IBAN
);

END;
/

CREATE OR REPLACE PROCEDURE GRHUM.Ins_Ribfour_Ulr_New(
INS_FOU_ORDRE       NUMBER,
INS_C_BANQUE        VARCHAR2,
INS_C_GUICHET       VARCHAR2,
INS_NO_COMPTE       VARCHAR2,
INS_CLE_RIB         VARCHAR2,
INS_RIB_TITCO       VARCHAR2,
INS_MOD_CODE        VARCHAR2,
INS_RIB_VALIDE      VARCHAR2
) IS
-- ------------
-- DECLARATIONS
-- ------------

cpt INTEGER;
rib_ordre NUMBER;
-- ---------
-- PRINCIPAL
-- ---------
BEGIN

SELECT COUNT(*) INTO cpt FROM RIBFOUR_ULR
WHERE
fou_ordre=INS_FOU_ORDRE
AND
c_banque=INS_C_BANQUE
AND
c_guichet=INS_C_GUICHET
AND
no_compte=INS_NO_COMPTE
AND rib_valide = 'O';

IF (cpt>0) THEN
   RAISE_APPLICATION_ERROR(-20000,'Ins_Ribfour_Ulr : R�f�rence bancaire existante');
END IF;

/*
SELECT COUNT(*) INTO cpt
FROM RIBFOUR_ULR;

IF ( cpt = 0 )
THEN
  rib_ordre :=1;
ELSE
  SELECT MAX(rib_ordre)+1 INTO rib_ordre FROM RIBFOUR_ULR;
END IF;
*/

SELECT SEQ_RIBFOUR_ULR.NEXTVAL INTO rib_ordre FROM dual;

--rib_ordre sert uniquement � identifier le quadruplet (fou_ordre/c_banque/c_guichet/no_compte)
--et pour assurer une compatibilit� avec les donn�es li�es � l'ancienne base GASPARD

INSERT INTO RIBFOUR_ULR(RIB_ORDRE,FOU_ORDRE,C_BANQUE,C_GUICHET,NO_COMPTE,CLE_RIB,RIB_TITCO,MOD_CODE,RIB_VALIDE,D_CREATION,D_MODIFICATION,TEM_PAYE_UTIL,BIC,IBAN)
VALUES (
rib_ordre,
INS_FOU_ORDRE,
INS_C_BANQUE,
INS_C_GUICHET,
INS_NO_COMPTE,
INS_CLE_RIB,
INS_RIB_TITCO,
INS_MOD_CODE,
'O',
SYSDATE,
SYSDATE,
'N',
NULL	,	 -- BIC
NULL		 -- IBAN
);

END;
/

CREATE OR REPLACE FUNCTION GRHUM.Chercher_Affectation_Individu
(
  noindividu INTEGER
)
RETURN CHAR
IS
  -- Action : AFFECTATION
  CURSOR c1
  IS SELECT aff.num_quot_affectation,aff.d_deb_affectation,str.ll_structure
     FROM   mangue.AFFECTATION aff,
	        grhum.STRUCTURE_ULR str
     WHERE  str.c_structure = aff.c_structure
	 AND    ((aff.d_fin_affectation IS NULL) OR (aff.d_fin_affectation >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
     AND    aff.d_deb_affectation <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
     AND    aff.no_dossier_pers = noindividu
     AND    aff.tem_valide= 'O'
	 ORDER BY aff.num_quot_affectation DESC, aff.d_deb_affectation DESC;
  -- variables
  r_affectation c1%ROWTYPE;
  lAffectation  VARCHAR2(500);
BEGIN
  lAffectation := '';

  -- Action 1
  OPEN c1;
  LOOP
     FETCH c1 INTO r_affectation;
     EXIT WHEN c1%NOTFOUND;

	 lAffectation := lAffectation||r_affectation.ll_structure||' ; ';

  END LOOP;
  CLOSE c1;

  IF ((lAffectation = '') OR (lAffectation IS NULL))
  THEN lAffectation := 'Affectation inconnue...';
  END IF;

  RETURN lAffectation;
END;
/





CREATE OR REPLACE FUNCTION GRHUM.Chercher_Occupation_Individu
(
  noindividu INTEGER
)
RETURN CHAR
IS
  -- Action : OCCUPATIONH
  CURSOR c1
  IS SELECT P.NUMERO_LOCAL,O.NUM_MOYEN_UTILISE
     FROM   mangue.OCCUPATION O, mangue.EMPLOI P
	 WHERE	O.NO_EMPLOI = P.NO_EMPLOI
	 AND  ((O.d_fin_occupation IS NULL) OR (O.d_fin_occupation >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
     AND    O.d_deb_occupation <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
     AND    O.no_dossier_pers = noindividu
     AND    O.tem_valide = 'O'
	 ORDER BY O.NUM_MOYEN_UTILISE DESC, O.d_deb_occupation DESC;
  -- variables
  r_occupation c1%ROWTYPE;
  lOccupation  VARCHAR2(500);
BEGIN
  lOccupation := '';

  -- Action 1
  OPEN c1;
  LOOP
     FETCH c1 INTO r_occupation;
     EXIT WHEN c1%NOTFOUND;

	 -- si c1=1 enr. -> Emploi budg�taire sinon Rompus de temps partiel
	 IF (c1%ROWCOUNT = 1) THEN
	 	lOccupation := 'Emploi budg�taire ';
	 ELSE
	 	lOccupation := 'Rompus de temps partiel ';
	 END IF;

	 lOccupation := lOccupation||r_occupation.numero_local||' ('||r_occupation.num_moyen_utilise||' '||CHR(37)||')'||' ; ';

  END LOOP;
  CLOSE c1;

  IF ((lOccupation = '') OR (lOccupation IS NULL))
  THEN lOccupation := 'Occupation inconnue...';
  END IF;

  RETURN lOccupation;
END;
/
CREATE OR REPLACE FUNCTION GRHUM.Chercher_Position_Individu
(
  noindividu INTEGER
)
RETURN CHAR
IS
  -- Action 1 : CONTRAT
  CURSOR c1
  IS SELECT con.*
     FROM   mangue.CONTRAT con,
	        mangue.contrat_avenant ave,
	        grhum.TYPE_CONTRAT_TRAVAIL typ
     WHERE  typ.tem_remuneration_accessoire != 'O'
	 AND    typ.c_type_contrat_trav = con.c_type_contrat_trav
	 AND    ave.CTRA_ORDRE = (SELECT MAX(a.CTRA_ORDRE) FROM mangue.contrat_avenant a WHERE a.no_seq_contrat = con.no_seq_contrat)
	 AND    ave.no_seq_contrat = con.no_seq_contrat
	 AND    ((con.d_fin_contrat_trav IS NULL) OR (con.d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
     AND    con.d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
--	 AND    c_type_contrat_trav NOT IN ('AR')
     AND    con.no_dossier_pers = noindividu
     AND    con.tem_annulation = 'N'
	 ORDER BY ave.num_quot_recrutement DESC, con.d_deb_contrat_trav DESC;
  -- Action 2 : CARRIERE
  CURSOR c2
  IS SELECT *
     FROM   mangue.ELEMENT_CARRIERE
     WHERE  ((d_fin_element IS NULL) OR (d_fin_element >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
     AND    d_effet_element <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
     AND    no_dossier_pers = noindividu
     AND    tem_valide = 'O'
	 ORDER BY quotite_element DESC, d_effet_element DESC;
  -- variables
  r_contrat    mangue.CONTRAT%ROWTYPE;
  r_element    mangue.ELEMENT_CARRIERE%ROWTYPE;
  r_occupation mangue.OCCUPATION%ROWTYPE;
  laPosition   VARCHAR2(500);
  leCorps      VARCHAR2(500);
  leLieu       VARCHAR2(500);
  nbenr        INTEGER;
BEGIN
  /*
  1 - on cherche un CONTRAT
  2 - On cherche une CARRIERE
  */

  laPosition := '';

  -- Action 1
  OPEN c1;
  LOOP
     FETCH c1 INTO r_contrat;
     EXIT WHEN c1%NOTFOUND;

	 IF (r_contrat.c_rne IS NOT NULL)
	 THEN SELECT lc_rne
	      INTO   leLieu
	      FROM   grhum.RNE
	      WHERE  c_rne = r_contrat.c_rne;
	 ELSE leLieu := '';
	 END IF;

	 IF (r_contrat.c_type_contrat_trav IN ('VN','VF'))
	 THEN -- Vacataire - OUI
	      SELECT DECODE(REPLACE(c.ll_corps,' '),NULL,'Vacataire',c.ll_corps||' - Vacataire')
		  INTO   leCorps
		  FROM   mangue.CONTRAT_VACATAIRES v,
		         grhum.CORPS c
		  WHERE  v.c_corps        = c.c_corps(+)
		  AND    v.no_seq_contrat = r_contrat.no_seq_contrat;
	 ELSE -- Vacataire - NON
	      SELECT ll_type_contrat_trav
		  INTO   leCorps
		  FROM   grhum.TYPE_CONTRAT_TRAVAIL
		  WHERE  c_type_contrat_trav = r_contrat.c_type_contrat_trav;
	 END IF;

	 laPosition := laPosition||leCorps||' ('||leLieu||') ; ';

  END LOOP;
  CLOSE c1;

  -- Action 2
  SELECT COUNT(*)
  INTO   nbenr
  FROM   mangue.OCCUPATION
  WHERE  ((d_fin_occupation IS NULL) OR (d_fin_occupation >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
  AND    d_deb_occupation <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
  AND    no_dossier_pers = noindividu;

  IF (nbenr > 0)
  THEN SELECT *
       INTO   r_occupation
       FROM   mangue.OCCUPATION
       WHERE  ((d_fin_occupation IS NULL) OR (d_fin_occupation >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
       AND    d_deb_occupation <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
       AND    no_dossier_pers = noindividu
       AND    ROWNUM = 1
       ORDER BY num_moyen_utilise DESC, d_deb_occupation DESC;

       OPEN c2;
       LOOP
          FETCH c2 INTO r_element;
          EXIT WHEN c2%NOTFOUND;

	      SELECT r.lc_rne
	      INTO   leLieu
	      FROM   mangue.EMPLOI p,
		         grhum.RNE r
	      WHERE  r.c_rne    = p.c_rne(+)
		  AND    p.no_emploi = r_occupation.no_emploi;

	      IF (r_element.c_corps IS NOT NULL)
	      THEN SELECT ll_corps
	           INTO   leCorps
	           FROM   grhum.CORPS
	           WHERE  c_corps = r_element.c_corps;
	      ELSE leCorps := '';
	      END IF;

	      laPosition := laPosition||leCorps||' ('||leLieu||') ; ';

       END LOOP;
       CLOSE c2;
  END IF;

  IF ((laPosition = '') OR (laPosition IS NULL))
  THEN laPosition := 'Dossier incomplet...';
  END IF;

  RETURN laPosition;
END;
/

CREATE OR REPLACE FUNCTION GRHUM.Chercher_Specia_Individu
(
  noindividu INTEGER
)
RETURN CHAR
IS
  -- POSTE
  CURSOR c1
  IS SELECT P.*
     FROM   mangue.OCCUPATION O, mangue.EMPLOI P
     WHERE  ((d_fin_occupation IS NULL) OR (d_fin_occupation >= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')))
     AND    d_deb_occupation <= TO_DATE(TO_CHAR(SYSDATE,'dd/mm/YYYY'),'dd/mm/YYYY')
	 AND	O.no_emploi = P.no_emploi
     AND    no_dossier_pers = noindividu
     AND    O.tem_valide = 'O'
	 ORDER BY d_deb_occupation DESC;

  -- variables
  r_poste         mangue.emploi%ROWTYPE;
  lSpecialisation VARCHAR2(500);
  codeCnu         grhum.CNU.c_section_cnu%TYPE;
  libelleCnu      grhum.CNU.ll_section_cnu%TYPE;
  code2degre      grhum.DISC_SECOND_DEGRE.C_DISC_SECOND_DEGRE%TYPE;
  libelle2degre   grhum.DISC_SECOND_DEGRE.ll_DISC_SECOND_DEGRE%TYPE;

BEGIN
  /*
  1 - on cherche la sp�cialisation du POSTE de l'OCCUPATION courante
  */

  lSpecialisation := '';

  OPEN c1;
  LOOP
     FETCH c1 INTO r_poste;
     EXIT WHEN c1%NOTFOUND;

	 -- Section CNU
	 IF (r_poste.no_cnu IS NOT NULL) THEN

	 	SELECT c_section_cnu,ll_section_cnu INTO codeCnu,libelleCnu
		FROM grhum.CNU
	 	WHERE NO_CNU = r_poste.no_cnu;

	 	lSpecialisation := '(' || codeCnu || ') ' || libelleCnu;

	 END IF;

	 -- Discipline Second Degr�
	 IF (r_poste.c_disc_sd_degre IS NOT NULL) THEN

	 	SELECT c_disc_second_degre,ll_disc_second_degre INTO code2degre,libelle2degre
		FROM grhum.DISC_SECOND_DEGRE
	 	WHERE c_disc_second_degre = r_poste.c_disc_sd_degre;

	 	lSpecialisation := '(' || code2degre || ') ' || libelle2degre;

	 END IF;

  END LOOP;
  CLOSE c1;

  IF ((lSpecialisation = '') OR (lSpecialisation IS NULL))
  THEN lSpecialisation := 'Sans sp�cialisation';
  END IF;

  RETURN lSpecialisation;
END;
/

--BEGIN
--	 DBMS_UTILITY.COMPILE_SCHEMA ('GRHUM');
--END;
--/

COMMIT;
