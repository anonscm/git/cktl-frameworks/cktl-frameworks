--
-- Patch DML de GRHUM du 05/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.38
-- Date de publication : 05/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.37';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.38';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.38 a deja ete passe !');
    end if;

end;
/


-- MAJ d'un champs NULL dans la table GRHUM.DOMAINE_SCIENTIFIQUE
UPDATE GRHUM.DOMAINE_SCIENTIFIQUE SET DS_VERSION = 1, DS_NOUVEAU_CODE = DS_CODE;


INSERT INTO GRHUM.ASSOCIATION (ASS_ID,ASS_LIBELLE,D_CREATION,D_MODIFICATION,TAS_ID,ASS_RACINE,ASS_CODE,ASS_ALIAS,ASS_LOCALE,D_OUVERTURE,D_FERMETURE) 
VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL,'AFFECTATION',sysdate,sysdate,'1',null,'AFFECT',null,'N',null,null);


INSERT INTO GRHUM.ASSOCIATION_RESEAU
  (
    ASR_ID,
    ASS_ID_PERE,
    ASS_ID_FILS,
    ASR_RANG,
    ASR_COMMENTAIRE,
    D_CREATION,
    D_MODIFICATION
  )
  VALUES
  (
    GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL,
    (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'FONCTION' AND ASS_RACINE = 'O'),
    (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'AFFECT'),
    null,
    'Association permettant de faire la correspondance avec les affectations de mangue',
    sysdate,
    sysdate
  );

INSERT INTO GRHUM.ASSOCIATION (ASS_ID,ASS_LIBELLE,D_CREATION,D_MODIFICATION,TAS_ID,ASS_RACINE,ASS_CODE,ASS_ALIAS,ASS_LOCALE,D_OUVERTURE,D_FERMETURE) 
VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL,'POLE DE COMPETITIVITE',sysdate,sysdate,'1',null,'POLECOMPET',null,'N',null,null);

INSERT INTO GRHUM.ASSOCIATION_RESEAU
  (
    ASR_ID,
    ASS_ID_PERE,
    ASS_ID_FILS,
    ASR_RANG,
    ASR_COMMENTAIRE,
    D_CREATION,
    D_MODIFICATION
  )
  VALUES
  (
    GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL,
    (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'RECHERCHE'),
    (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'POLECOMPET'),
    null,
    'Role de pole de compet',
    sysdate,
    sysdate
  );

INSERT INTO GRHUM.EVT_EVENEMENT_TYPE (EVTT_ID, EVTT_CAT_ID, EVTT_ID_INTERNE, EVTT_ORDRE_AFFICHAGE, EVTT_LC, EVTT_LL) 
VALUES (GRHUM.EVT_EVENEMENT_TYPE_SEQ.nextval, (select EVTT_CAT_ID from GRHUM.EVT_EVENEMENT_TYPE_CAT where EVTT_CAT_ID_INTERNE = 'ADMIN'), 'NOTE', 1, 'Note', 'Note');

--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.38', TO_DATE('05/07/2013', 'DD/MM/YYYY'),NULL,'MAJ DS_NOUVEAU_CODE et insertion AFFECTATION');

UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.38';

COMMIT;
