--
-- Patch DDL de GRHUM du 27/10/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.24.0
-- Date de publication : 27/10/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.23.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.24.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.24.0 a deja ete passe !');
    end if;

end;
/

--------------------------V20140915.122301__DDL_Maj_Ins_Repart_Bureau.sql---------------------------
create or replace
PROCEDURE   GRHUM.Ins_Repart_Bureau
(
 INS_BUR_ORDRE 	  GRHUM.REPART_BUREAU.bur_ordre%TYPE,
 INS_NO_INDIVIDU  GRHUM.REPART_BUREAU.no_individu%TYPE
)
IS

cpt INTEGER;

BEGIN

SELECT COUNT(*) INTO cpt FROM GRHUM.REPART_BUREAU WHERE bur_ordre=ins_bur_ordre AND no_individu=ins_no_individu;
IF (cpt = 0)
THEN

	INSERT INTO GRHUM.REPART_BUREAU VALUES (INS_BUR_ORDRE,INS_NO_INDIVIDU,SYSDATE,SYSDATE,NULL,NULL);

END IF;

END;
/
-------------------------V20140922.162129__DDL_Modif_longueur_LL_de_RNE.sql-------------------------
ALTER TABLE GRHUM.RNE MODIFY (ll_rne VARCHAR2(200));
----------------------------V20140924.141424__DDL_Maj_RNE_INSA_CVdL.sql-----------------------------
DECLARE
BEGIN
	GRHUM.MAJ_UAI('3500055H','GROUPE SCOLAIRE LA RESIDENCE DE CASABLANCA','89 AVENUE DU 2 MARS',null, TO_DATE('04/09/2014','DD/MM/YYYY'), TO_DATE('04/09/2014','DD/MM/YYYY'),null,'PU','CASABLANCA',null);
	GRHUM.MAJ_UAI('0694069Y','LYCEE GENERAL GERMAINE TILLION','500 ALLEE DE GRANDS CHAMPS','69210', TO_DATE('04/09/2014','DD/MM/YYYY'), TO_DATE('04/09/2014','DD/MM/YYYY'),null,'PU','SAIN-BEL',null);
END;
/
--------------------------V20141006.123113__DDL_Maj_Type_Voie_Adresse.sql---------------------------
ALTER TABLE GRHUM.ADRESSE MODIFY (c_voie VARCHAR2(4));
------------------------------V20141007.160602__DDL_Maj_Type_Etab.sql-------------------------------
ALTER TABLE GRHUM.type_etablissement MODIFY ll_type_etablissemen VARCHAR2(50);
ALTER TABLE GRHUM.type_etablissement MODIFY lc_type_etablissemen VARCHAR2(30);
---------------------------V20141007.161336__DDL_Maj_Referens_Emplois.sql---------------------------
DECLARE
BEGIN
	GRHUM.adm_add_column('GRHUM', 'REFERENS_EMPLOIS', 'DATE_OUVERTURE', 'DATE', null, null);
	GRHUM.adm_add_column('GRHUM', 'REFERENS_EMPLOIS', 'DATE_FERMETURE', 'DATE', null, null);
	GRHUM.adm_reorg_table('GRHUM', 'REFERENS_EMPLOIS', 'INDX_GRHUM');
END;
/

COMMENT ON COLUMN GRHUM.REFERENS_EMPLOIS.DATE_OUVERTURE IS 'Date d''ouverture de l''emploi type';
COMMENT ON COLUMN GRHUM.REFERENS_EMPLOIS.DATE_FERMETURE IS 'Date de fermeture de l''emploi type';
--------------------V20141015.153257__DDL_Ajout_procedure_renommage_colonne.sql---------------------
create or replace
PROCEDURE	GRHUM.adm_rename_column(ownerObject VARCHAR2, tableName VARCHAR2, oldColumnName VARCHAR2, newColumnName VARCHAR2, newCommentaire varchar2)
/** Permet de modifier une colonne dans une table si celle-ci existe. Aucune erreur n'est renvoyÈe si la colonne n'existe pas.
Le commentaire passé en paramètre remplacera le commentaire en cours si le paramètre n'est pas NULL
Usage :
grhum.adm_rename_column('GRH_PECHE', 'SERVICE_DETAIL', 'TEM_REPART_CROISEE', 'ETAT', 'Commentaire de la table');

grhum.adm_rename_column('GRH_PECHE', 'SERVICE_DETAIL', 'TEM_REPART_CROISEE', 'ETAT', NULL);

*/
IS
  flag integer;
  vsql varchar2(2000);
begin

 select count(*) into flag from all_tab_columns where owner = upper(ownerObject) and table_name = upper(tableName) and COLUMN_NAME = upper(oldColumnName);
 if (flag < 1)
 then
  SYS.DBMS_OUTPUT.PUT_LINE('La colonne ' || ownerObject ||'.'||tableName ||'.'|| oldColumnName || ' n''existe pas.');
 else
    vsql := 'ALTER TABLE '|| ownerObject ||'.'||tableName ||' RENAME COLUMN '|| oldColumnName ||' TO '|| newColumnName ||'';
  
  execute immediate vsql;
  SYS.DBMS_OUTPUT.PUT_LINE('La colonne ' || ownerObject ||'.'||tableName ||'.'|| oldColumnName || ' a ÈtÈ renomÈe.');
  
  if newCommentaire is not null
  then
    execute immediate 'COMMENT ON COLUMN '|| ownerObject ||'.'||tableName ||'.'||newColumnName|| '  IS '''|| newCommentaire ||''' ';
  end if;
  
 end if;

 
end;
/
---------------------------V20141015.165656__DDL_Maj_Ins_fournis_ULR.sql----------------------------
create or replace
PROCEDURE GRHUM.Ins_Fournis_Ulr(
INS_PERS_ID         FOURNIS_ULR.pers_id%TYPE,
INS_ADR_ORDRE 		FOURNIS_ULR.adr_ordre%TYPE,
INS_FOU_DATE        FOURNIS_ULR.fou_date%TYPE,
INS_FOU_MARCHE      FOURNIS_ULR.fou_marche%TYPE,
INS_FOU_VALIDE      FOURNIS_ULR.fou_valide%TYPE,
INS_AGT_ORDRE       FOURNIS_ULR.AGT_ORDRE%TYPE,
INS_FOU_TYPE        FOURNIS_ULR.fou_type%TYPE
--fouordre			out fournis_ulr.fou_ordre%TYPE
) IS
-- ------------
-- DECLARATIONS
-- ------------

cpt 		   INTEGER;
foucode 	   FOURNIS_ULR.fou_code%TYPE;
nom 		   v_personne.pers_libelle%TYPE;
l_cpt_ordre    NUMBER;
l_adr_ordre    INTEGER;
fouordre 	   INTEGER;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

SELECT COUNT(*) INTO cpt FROM GRHUM.v_personne WHERE pers_id=ins_pers_id;
IF (cpt=0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PERS_ID INCORRECT');
END IF;

/*
SELECT COUNT(*) INTO cpt FROM FOURNIS_ULR;
IF ( cpt = 0 ) THEN
  fouordre := 1;
ELSE
  SELECT MAX(fou_ordre)+1 INTO fouordre FROM FOURNIS_ULR;
END IF;
*/

SELECT GRHUM.SEQ_FOURNIS_ULR.NEXTVAL INTO fouordre FROM dual;

SELECT pers_libelle INTO nom FROM v_personne WHERE pers_id=ins_pers_id;

foucode:=Cons_Fou_Code(nom);




SELECT COUNT(*) INTO cpt FROM GRHUM.REPART_PERSONNE_ADRESSE a , GRHUM.FOURNIS_ULR b, GRHUM.ADRESSE c WHERE tadr_code = 'FACT' AND
a.pers_id = INS_PERS_ID
AND a.PERS_ID = b.PERS_ID
AND b.FOU_VALIDE !='A'
AND  b.ADR_ORDRE = c.ADR_ORDRE
AND a.ADR_ORDRE = c.ADR_ORDRE;
IF ( cpt = 0 )THEN

SELECT COUNT(*) INTO cpt FROM GRHUM.REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE
AND pers_id = INS_PERS_ID
AND tadr_code = 'FACT';


  IF( cpt = 0) THEN
  INSERT INTO GRHUM.REPART_PERSONNE_ADRESSE VALUES (
         	INS_PERS_ID,
         	INS_ADR_ORDRE,
         	NULL,
         	'N',
         	'O',
         	'FACT',
         	SYSDATE,
         	SYSDATE
         	);

   END IF;

END IF;


INSERT INTO GRHUM.FOURNIS_ULR VALUES (
fouordre,
INS_PERS_ID,
INS_ADR_ORDRE,
foucode,
INS_FOU_DATE,
INS_FOU_MARCHE,
INS_FOU_VALIDE,
INS_AGT_ORDRE,
INS_FOU_TYPE,
SYSDATE,
SYSDATE,
l_cpt_ordre,
'N'			-- FOURNIS_ULR.FOU_ETRANGER
);


INSERT INTO GRHUM.VALIDE_FOURNIS_ULR VALUES (
fouordre,
INS_AGT_ORDRE,
SYSDATE,
INS_AGT_ORDRE,
SYSDATE
);


SELECT COUNT(*) INTO cpt FROM GRHUM.REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE AND Tadr_code = 'FACT';

IF cpt  = 0 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR :  PROBLEME SUR L''ADRESSE DE FACT');
END IF;

IF cpt != 1 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT MULTI');

END IF;


SELECT COUNT(*)  INTO cpt FROM GRHUM.FOURNIS_ULR f, GRHUM.REPART_PERSONNE_ADRESSE R, GRHUM.ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND R.tadr_code = 'FACT';

IF (cpt = 0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : AUCUNE ADRESSE DE FACTURATION');
END IF;

IF (cpt > 1) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME PLUSIEURS ADRESSES DE FACTURATION');
END IF;

SELECT f.adr_ordre  INTO l_adr_ordre FROM GRHUM.FOURNIS_ULR f, GRHUM.REPART_PERSONNE_ADRESSE R, GRHUM.ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND tadr_code = 'FACT';

IF (l_adr_ordre != ins_adr_ordre) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT ADR_ORDRE');

END IF;


END;
/
-----------------------V20141015.165828__DDL_Maj_Ins_fournis_ULR_Annuaire.sql-----------------------
create or replace
PROCEDURE GRHUM.Ins_Fournis_Ulr_Annuaire(
INS_PERS_ID         GRHUM.FOURNIS_ULR.pers_id%TYPE,
INS_ADR_ORDRE 		GRHUM.FOURNIS_ULR.adr_ordre%TYPE,
INS_FOU_DATE        GRHUM.FOURNIS_ULR.fou_date%TYPE,
INS_FOU_MARCHE      GRHUM.FOURNIS_ULR.fou_marche%TYPE,
INS_FOU_VALIDE      GRHUM.FOURNIS_ULR.fou_valide%TYPE,
INS_AGT_ORDRE       GRHUM.FOURNIS_ULR.AGT_ORDRE%TYPE,
INS_FOU_TYPE        GRHUM.FOURNIS_ULR.fou_type%TYPE,
INS_FOU_ETRANGER	GRHUM.FOURNIS_ULR.FOU_ETRANGER%TYPE,
fouordre			OUT GRHUM.FOURNIS_ULR.fou_ordre%TYPE
) IS
-- ------------
-- DECLARATIONS
-- ------------

cpt 		   INTEGER;
foucode 	   GRHUM.FOURNIS_ULR.fou_code%TYPE;
nom 		   GRHUM.v_personne.pers_libelle%TYPE;
l_cpt_ordre    NUMBER;
l_adr_ordre    INTEGER;
local_passwd   VARCHAR2(30);
local_domaine  VARCHAR2(50);

--fouordre integer;

-- ---------
-- PRINCIPAL
-- ---------
BEGIN

SELECT COUNT(*) INTO cpt FROM GRHUM.v_personne WHERE pers_id=ins_pers_id;
IF (cpt=0) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PERS_ID INCORRECT');
END IF;

/*
SELECT COUNT(*) INTO cpt FROM FOURNIS_ULR;
IF ( cpt = 0 ) THEN
  fouordre := 1;
ELSE
  SELECT MAX(fou_ordre)+1 INTO fouordre FROM FOURNIS_ULR;
END IF;
*/

SELECT GRHUM.SEQ_FOURNIS_ULR.NEXTVAL INTO fouordre FROM dual;

SELECT pers_libelle INTO nom FROM GRHUM.v_personne WHERE pers_id=ins_pers_id;

foucode:=Cons_Fou_Code(nom);




SELECT COUNT(*) INTO cpt FROM GRHUM.REPART_PERSONNE_ADRESSE a , GRHUM.FOURNIS_ULR b, GRHUM.ADRESSE c WHERE tadr_code = 'FACT' AND
a.pers_id = INS_PERS_ID
AND a.PERS_ID = b.PERS_ID
AND b.FOU_VALIDE !='A'
AND  b.ADR_ORDRE = c.ADR_ORDRE
AND a.ADR_ORDRE = c.ADR_ORDRE;
IF ( cpt = 0 )THEN

SELECT COUNT(*) INTO cpt FROM GRHUM.REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE
AND pers_id = INS_PERS_ID
AND tadr_code = 'FACT';


  IF( cpt = 0) THEN
  INSERT INTO GRHUM.REPART_PERSONNE_ADRESSE VALUES (
         	INS_PERS_ID,
         	INS_ADR_ORDRE,
         	NULL,
         	'N',
         	'O',
         	'FACT',
         	SYSDATE,
         	SYSDATE
         	);

   END IF;

END IF;


INSERT INTO GRHUM.FOURNIS_ULR VALUES (
fouordre,
INS_PERS_ID,
INS_ADR_ORDRE,
foucode,
INS_FOU_DATE,
INS_FOU_MARCHE,
INS_FOU_VALIDE,
INS_AGT_ORDRE,
INS_FOU_TYPE,
SYSDATE,
SYSDATE,
l_cpt_ordre,
INS_FOU_ETRANGER	--FOURNIS_ULR.FOU_ETRANGER%type
);


INSERT INTO GRHUM.VALIDE_FOURNIS_ULR VALUES (
fouordre,
INS_AGT_ORDRE,
SYSDATE,
INS_AGT_ORDRE,
SYSDATE
);


SELECT COUNT(*) INTO cpt FROM GRHUM.REPART_PERSONNE_ADRESSE WHERE adr_ordre = INS_ADR_ORDRE AND Tadr_code = 'FACT';

IF cpt  = 0 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR :  PROBLEME SUR L''ADRESSE DE FACT');
END IF;

IF cpt != 1 THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT MULTI');

END IF;


SELECT COUNT(*)  INTO cpt FROM GRHUM.FOURNIS_ULR f, GRHUM.REPART_PERSONNE_ADRESSE R, GRHUM.ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND R.tadr_code = 'FACT';

IF (cpt != 1) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME PLUSIEURS ADRESSES DE FACTURATION ');
END IF;

SELECT f.adr_ordre  INTO l_adr_ordre FROM GRHUM.FOURNIS_ULR f, GRHUM.REPART_PERSONNE_ADRESSE R, GRHUM.ADRESSE a
WHERE f.pers_id = R.pers_id
AND R.adr_ordre  = a.adr_ordre
AND f.pers_id = ins_pers_id
AND tadr_code = 'FACT';

IF (l_adr_ordre != ins_adr_ordre) THEN
   RAISE_APPLICATION_ERROR(-20000,'INS_FOURNIS_ULR : PROBLEME SUR L''ADRESSE DE FACT ADR_ORDRE');

END IF;


END;
/
---------------------------V20141017.080845__DDL_Maj_Adm_Drop_Object.sql----------------------------
create or replace PROCEDURE    GRHUM.adm_drop_object(ownerObject VARCHAR2,nameObject VARCHAR2,typeObject VARCHAR2)
IS
-- Procedure de suppression d'un objet Oracle sans message d'erreur sur l'existence de ce dernier
-- ownerObject : le proprietaire (user) de l'objet
-- nameObject : nom de l'objet a supprimer
-- typeObject : type de l'objet (TABLE, PROCEDURE, FUNCTION, etc.)
-- ex : DROP FUNCTION GRHUM.CONS_LOGIN

fullObject  VARCHAR2(100);
instruction varchar2(500);

BEGIN

    SELECT owner||'.'||object_name INTO fullObject FROM all_objects WHERE owner = upper(ownerObject) AND object_name = upper(nameObject) AND object_type = upper(typeObject);
    instruction := 'DROP '||upper(typeObject)||' '||fullObject||'';

    if upper(typeObject)='TABLE' then
      instruction := instruction || ' cascade constraints';
    end if;
    EXECUTE IMMEDIATE instruction;
    EXCEPTION
        WHEN no_data_found THEN
    NULL;

END;
/
-----------------------V20141021.123341__DDL_Suppression_tables_obsoletes.sql-----------------------
DECLARE
BEGIN
	GRHUM.ADM_DROP_OBJECT('GRHUM', 'BAP_ELEMENT_CARRIERE', 'TABLE');
	GRHUM.ADM_DROP_OBJECT('GRHUM', 'COMPAT_CORPS_BAP_SPE', 'TABLE');
	GRHUM.ADM_DROP_OBJECT('GRHUM', 'POSTE_ULR', 'TABLE');
END;
/
-------------------V20141021.133620__DDL_Ajout_Procedure_Renommage_Contrainte.sql-------------------
create or replace
PROCEDURE   GRHUM.adm_rename_constraint(ownerObject VARCHAR2, tableName VARCHAR2, oldConstraintName VARCHAR2, newConstraintName VARCHAR2)
/** Permet de modifier le nom d'une contrainte dans une table si celle-ci existe. Aucune erreur n'est renvoyÈe si la contrainte n'existe pas.
Le commentaire passé en paramètre remplacera le commentaire en cours si le paramètre n'est pas NULL
Usage :
grhum.adm_rename_constraint('GRH_PECHE', 'SERVICE_DETAIL', 'CHK_SD_TEM_REPART_CROISEE', 'CHK_SD_ETAT');
-- TODO prendre en compte la modification sur le Search_condition 
*/
IS
  flag integer;
  vsql varchar2(2000);
begin

 select count(*) into flag from all_constraints where owner = upper(ownerObject) and table_name = upper(tableName) and constraint_name = upper(oldConstraintName);
 if (flag < 1)
 then
  SYS.DBMS_OUTPUT.PUT_LINE('La contrainte '|| oldConstraintName || ' de la table ' || ownerObject ||'.'||tableName ||'  n''existe pas.');
 else
    vsql := 'ALTER TABLE '|| ownerObject ||'.'||tableName ||' RENAME CONSTRAINT '|| oldConstraintName ||' TO '|| newConstraintName ||'';
  
  execute immediate vsql;
  SYS.DBMS_OUTPUT.PUT_LINE('La contrainte '|| oldConstraintName || ' de la table ' || ownerObject ||'.'||tableName ||' a ÈtÈ renommÈe.');
  
 end if;

 
end;
/

---------------------------V20141023.112141__DDL_MAJ_VUES_PERSONNELS.sql----------------------------

/* Formatted on 2014/10/15 11:58 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW grhum.v_personnel_actuel (no_dossier_pers)
AS
   SELECT c.no_dossier_pers
     FROM mangue.contrat c, type_contrat_travail tct
    WHERE (    c.d_deb_contrat_trav <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_contrat_trav IS NULL
                OR c.d_fin_contrat_trav >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_annulation = 'N'
      AND c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND tct.tem_remuneration_accessoire != 'O'
      AND tct.tem_titulaire != 'O'
      AND tct.tem_invite_associe != 'O'
   UNION
-- Les hÈbergÈs (ManGUE 1.4.0.0)
   SELECT UNIQUE ch.no_dossier_pers
            FROM mangue.contrat_heberges ch
           WHERE (    ch.d_deb_contrat_inv <=
                         TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                  'dd/mm/YYYY'
                                 )
                  AND (   ch.d_fin_contrat_inv IS NULL
                       OR ch.d_fin_contrat_inv >=
                             TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                      'dd/mm/YYYY'
                                     )
                      )
                 )
             AND ch.tem_valide = 'O'
   UNION
-- Les titulaires
   SELECT c.no_dossier_pers
     FROM mangue.element_carriere c
    WHERE c.d_effet_element <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      AND (   c.d_fin_element IS NULL
           OR c.d_fin_element >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
          )
      AND c.tem_valide = 'O'
      AND c.tem_provisoire = 'N'
      AND c.d_annulation IS NULL
      AND c.no_dossier_pers NOT IN 
-- Soustraire les positions qui ne sont pas considÈrÈes comme de l'activitÈ
          (
             SELECT cp.no_dossier_pers
               FROM mangue.changement_position cp, POSITION p
              WHERE cp.c_position = p.c_position
                AND p.tem_activite = 'N'
                AND cp.d_deb_position <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
                AND (   cp.d_fin_position IS NULL
                     OR cp.d_fin_position >=
                           TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                    'dd/mm/YYYY'
                                   )
                    ))
   UNION
-- Prise en compte des Professeurs ÈmÈrites
   SELECT no_dossier_pers
     FROM mangue.emeritat e
    WHERE e.d_deb_emeritat <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      AND (   e.d_fin_emeritat IS NULL
           OR e.d_fin_emeritat >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
          )
      AND no_arrete_emeritat IS NOT NULL;

	  
/* Formatted on 2014/10/15 11:56 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW grhum.v_personnel_ens (no_dossier_pers,
                                                    con_car,
                                                    d_debut,
                                                    d_fin
                                                   )
AS
   SELECT c.no_dossier_pers, 'CAR', c.d_effet_element, c.d_fin_element
     FROM element_carriere c, corps co, type_population tp
    WHERE c.c_corps = co.c_corps
      AND co.c_type_corps = tp.c_type_population
      AND (   tp.tem_enseignant = 'O'
           OR tp.tem_2degre = 'O'
           OR tp.tem_ens_sup = 'O'
          )
      AND c.tem_valide = 'O'
	  AND c.tem_provisoire = 'N'
	  AND c.d_annulation is null
   UNION
   SELECT c.no_dossier_pers, 'CON', c.d_deb_contrat_trav,
          c.d_fin_contrat_trav
     FROM contrat c, type_contrat_travail tct
    WHERE c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND c.tem_annulation = 'N'
      AND tct.tem_enseignant = 'O'
      AND c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND tct.tem_remuneration_accessoire != 'O'
      AND tct.tem_titulaire != 'O'
      AND tct.tem_invite_associe != 'O'
   UNION
   -- inclusion des contrats non enseignant typÈs par letypepopulation enseignant
   SELECT no_dossier_pers, 'CON', ca.d_deb_contrat_av, ca.d_fin_contrat_av
     FROM contrat c,
          type_contrat_travail tct,
          mangue.contrat_avenant ca,
          grade g,
          corps c,
          type_population tp
    WHERE c.tem_annulation = 'N'
      AND tct.tem_enseignant = 'N'
      AND c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND tct.tem_remuneration_accessoire != 'O'
      AND tct.tem_titulaire != 'O'
      AND c.no_seq_contrat = ca.no_seq_contrat
      AND ca.c_grade = g.c_grade
      AND g.c_corps = c.c_corps
      AND c.c_type_corps = tp.c_type_population
      AND ca.tem_annulation = 'N'
      AND tp.tem_enseignant = 'O';


/* Formatted on 2014/10/15 11:55 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW grhum.v_personnel_non_ens (no_dossier_pers,
                                                        con_car,
                                                        d_debut,
                                                        d_fin
                                                       )
AS
   SELECT p.no_dossier_pers, 'CAR', c.d_effet_element, c.d_fin_element
     FROM personnel_ulr p, element_carriere c, corps co, type_population tp
    WHERE p.no_dossier_pers = c.no_dossier_pers
      AND c.c_corps = co.c_corps
      AND co.c_type_corps = tp.c_type_population
      AND (tp.tem_itarf = 'O' OR tp.tem_atos = 'O' OR tem_biblio = 'O')
      AND c.tem_valide = 'O'
	  AND c.tem_provisoire = 'N'
	  AND c.d_annulation is NULL
   UNION
   SELECT c.no_dossier_pers, 'CON', c.d_deb_contrat_trav,
          c.d_fin_contrat_trav
     FROM contrat c, type_contrat_travail t
    WHERE c.c_type_contrat_trav = t.c_type_contrat_trav
      AND c.tem_annulation = 'N'
      AND t.tem_enseignant = 'N'
      AND t.tem_remuneration_accessoire != 'O'
      AND t.tem_titulaire != 'O'
      AND c.no_seq_contrat NOT IN (
             -- exclusion des contrats non enseignant typv©s
             -- par le typepopulation enseignant
             SELECT c.no_seq_contrat
               FROM contrat c,
                    mangue.contrat_avenant ca,
                    grade g,
                    corps c,
                    type_population tp
              WHERE c.no_seq_contrat = ca.no_seq_contrat
                AND ca.c_grade = g.c_grade
                AND g.c_corps = c.c_corps
                AND c.c_type_corps = tp.c_type_population
                AND ca.tem_annulation = 'N'
                AND tp.tem_enseignant = 'O');



				

/* Formatted on 2014/10/15 11:41 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW grhum.v_personnel_actuel_non_ens (no_dossier_pers)
AS
   SELECT c.no_dossier_pers
     FROM element_carriere c, corps co, type_population tp
    WHERE (    c.d_effet_element <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_element IS NULL
                OR c.d_fin_element >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_valide = 'O'
      AND TEM_VALIDE = 'O'
      AND tem_provisoire ='N'
      AND d_annulation is null
      AND c.c_corps = co.c_corps
      AND co.c_type_corps = tp.c_type_population
      AND (tp.tem_enseignant = 'N')
   UNION
   SELECT DISTINCT c.no_dossier_pers
              FROM contrat c, type_contrat_travail tct
             WHERE (    c.d_deb_contrat_trav <=
                           TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                    'dd/mm/YYYY'
                                   )
                    AND (   c.d_fin_contrat_trav IS NULL
                         OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >=
                               TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                        'dd/mm/YYYY'
                                       )
                        )
                   )
               AND c.tem_annulation = 'N'
               AND tct.tem_enseignant = 'N'
               AND c.c_type_contrat_trav = tct.c_type_contrat_trav
               AND tct.tem_remuneration_accessoire != 'O'
               AND tct.tem_titulaire != 'O'
               AND c.no_seq_contrat NOT IN (
                      -- exclusion des contrats non enseignant typÈs
                      -- par le typepopulation enseignant
                      SELECT c.no_seq_contrat
                        FROM contrat c,
                             mangue.contrat_avenant ca,
                             grade g,
                             corps c,
                             type_population tp
                       WHERE c.no_seq_contrat = ca.no_seq_contrat
                         AND ca.c_grade = g.c_grade
                         AND g.c_corps = c.c_corps
                         AND c.c_type_corps = tp.c_type_population
                         AND ca.tem_annulation = 'N'
                         AND tp.tem_enseignant = 'O'
                         AND ca.d_deb_contrat_av <=
                                TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                         'dd/mm/YYYY'
                                        )
                         AND (   ca.d_fin_contrat_av IS NULL
                              OR ADD_MONTHS (ca.d_fin_contrat_av, 1) >=
                                    TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
                                             'dd/mm/YYYY'
                                            )
                             ));
							 
							 
							 

/* Formatted on 2014/10/15 12:01 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW grhum.v_personnel_actuel_ens (no_dossier_pers)
AS
   SELECT c.no_dossier_pers
     FROM element_carriere c, corps co, type_population tp
    WHERE (    c.d_effet_element <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_element IS NULL
                OR c.d_fin_element >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_valide = 'O'
	  AND c.tem_provisoire = 'N'
	  AND c.d_annulation IS NULL
      AND c.c_corps = co.c_corps
      AND co.c_type_corps = tp.c_type_population
      AND (   tp.tem_enseignant = 'O'
           OR tp.tem_2degre = 'O'
           OR tp.tem_ens_sup = 'O'
          )
   UNION
   SELECT c.no_dossier_pers
     FROM contrat c, type_contrat_travail tct
    WHERE (    c.d_deb_contrat_trav <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_contrat_trav IS NULL
                OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_annulation = 'N'
      AND tct.tem_enseignant = 'O'
      AND c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND tct.tem_remuneration_accessoire != 'O'
      AND tct.tem_titulaire != 'O'
      AND tct.tem_invite_associe != 'O'
   UNION
   SELECT c.no_dossier_pers
     FROM element_carriere c, corps co
    WHERE c.c_corps = co.c_corps
      AND co.lc_corps IN ('M.A.', 'DCIO ET CONS.ORIENT.')
      AND (    c.d_effet_element <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_element IS NULL
                OR c.d_fin_element >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_valide = 'O'
   UNION
   -- inclusion des contrats non enseignant typÈs par le
   -- typepopulation enseignant
   SELECT no_dossier_pers
     FROM contrat c,
          type_contrat_travail tct,
          mangue.contrat_avenant ca,
          grade g,
          corps c,
          type_population tp
    WHERE (    c.d_deb_contrat_trav <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
           AND (   c.d_fin_contrat_trav IS NULL
                OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
               )
          )
      AND c.tem_annulation = 'N'
      AND tct.tem_enseignant = 'N'
      AND c.c_type_contrat_trav = tct.c_type_contrat_trav
      AND tct.tem_remuneration_accessoire != 'O'
      AND tct.tem_titulaire != 'O'
      AND c.no_seq_contrat = ca.no_seq_contrat
      AND ca.c_grade = g.c_grade
      AND g.c_corps = c.c_corps
      AND c.c_type_corps = tp.c_type_population
      AND tp.tem_enseignant = 'O'
      AND ca.tem_annulation = 'N'
      AND ca.d_deb_contrat_av <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      AND (   ca.d_fin_contrat_av IS NULL
           OR ADD_MONTHS (ca.d_fin_contrat_av, 1) >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
          )
   UNION
   -- Prise en compte des Professeurs ÈmÈrites
   SELECT no_dossier_pers
     FROM mangue.emeritat e
    WHERE e.d_deb_emeritat <=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      AND (   e.d_fin_emeritat IS NULL
           OR e.d_fin_emeritat >=
                       TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
          )
      AND no_arrete_emeritat IS NOT NULL;
							 


-----------------------V20141023.113801__DDL_MAJ_Echanger_Personne_Grhum.sql------------------------
create or replace
PROCEDURE    GRHUM.Echanger_Personne_Grhum
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
  ligne1 VARCHAR2(200);
  ligne2 VARCHAR2(200);
  cursor c1 (oldid number) is select c_structure from grhum.repart_structure where pers_id = oldid;
  cursor c2 (oldid number) is select no_telephone from grhum.personne_telephone where pers_id = oldid;
  cstruct varchar2(10);
  notel varchar2(20);
BEGIN
  ligne1 := NULL;
  ligne2 := NULL;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_individu_urgence = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNEL_ULR set no_individu_urgence = '||newno||' where no_individu_urgence = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNEL_ULR
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0) then
    SELECT COUNT(*)
    INTO   nbenr1
    FROM   grhum.PERSONNEL_ULR
    WHERE  no_dossier_pers = newno;
    IF (nbenr1 = 0) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'create table TEMP00 as select * from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update TEMP00 set no_dossier_pers = '||newno||';');

         INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'insert into GRHUM.PERSONNEL_ULR select * from TEMP00;');

       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
       ligne2 := 'drop table TEMP00;';
    else
       ligne1 := 'delete from GRHUM.PERSONNEL_ULR where no_dossier_pers = '||oldno||';';
    end if;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.AGENT_PERSONNEL
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.AGENT_PERSONNEL set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.APUR_EDITION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.APUR_EDITION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DIPLOME
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DIPLOME set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_DOSSIER
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_DOSSIER set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_HISTORIQUE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_HISTORIQUE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ATER_INDIVIDU_SITUATION
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ATER_INDIVIDU_SITUATION set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  -- Modification du 23/10/2014 suite √† la suppression de la table
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BAP_ELEMENT_CARRIERE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BAP_ELEMENT_CARRIERE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BLOC_NOTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BLOC_NOTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_ECHELON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_ECHELON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.BONIF_INDICIAIRE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.BONIF_INDICIAIRE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CARRIERE_OLD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CARRIERE_OLD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CGE_MOD_AGT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CGE_MOD_AGT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.CHERCHEUR
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.CHERCHEUR set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  --

  /*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE_QUOTA
  WHERE  par_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE_QUOTA set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DELEGATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DELEGATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.DEPOSITAIRE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.DEPOSITAIRE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;


  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ENFANT
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ENFANT set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ENQUETES_REPONSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ENQUETES_REPONSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.ETUDIANT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.ETUDIANT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.EVOLUTION_CHEVRON
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.EVOLUTION_CHEVRON set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.FOURNIS_ULR
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.FOURNIS_ULR set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_DISTINCTIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_DISTINCTIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_FORMATIONS
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_FORMATIONS set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.INDIVIDU_PSEUDO
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.INDIVIDU_PSEUDO set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_LA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_ELECTEUR set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_LA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_LA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_PROMOUVABLES_TA
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_PROMOUVABLES_TA set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.LISTE_TA_STRUCTURE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.LISTE_TA_STRUCTURE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MAD
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MAD set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --
/*
  -- Modification (mise en commentaire le 07/01/2014)
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.MI_TPS_THERAP
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.MI_TPS_THERAP set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_IMPRESSIONS
  WHERE  no_auteur = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_IMPRESSIONS set no_auteur = '||newno||' where no_auteur = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NBI_OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NBI_OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

  --
*/
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.NOTES
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.NOTES set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.OCCUPATION
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.OCCUPATION set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;

*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PAIEMENT
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PAIEMENT set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_ALIAS
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_ALIAS set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PERSONNE_TELEPHONE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) then
    open c2(oldid);
    loop
        fetch c2 into notel;
        exit when c2%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.PERSONNE_TELEPHONE WHERE PERS_ID = newid and no_telephone = notel;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PERSONNE_TELEPHONE set pers_id = '||newid||' where pers_id = '||oldid||' and no_telephone='''||notel||''';');
        end if;
    end loop;
    close c2;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_ADRESSES
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_ADRESSES set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.PREF_PERSONNEL
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.PREF_PERSONNEL set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.QUOTA_PME_HISTO
  WHERE  pers_id_createur = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.QUOTA_PME_HISTO set pers_id_createur = '||newid||' where pers_id_createur = '||oldid||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.RELIQUATS_ANCIENNETE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.RELIQUATS_ANCIENNETE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_ASSOCIATION
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
       SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_ASSOCIATION WHERE PERS_ID = newid;
       IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_ASSOCIATION set pers_id = '||newid||' where pers_id = '||oldid||'
               and c_structure not in (select c_structure from GRHUM.REPART_ASSOCIATION where pers_id = '||newid||');');
       END IF;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_BUREAU
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_BUREAU set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_COMPTE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_COMPTE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_EMPLOI
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldno||','||newno,SYSDATE,
               'update GRHUM.REPART_EMPLOI set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_PERSONNE_ADRESSE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_PERSONNE_ADRESSE set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.REPART_STRUCTURE
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0) THEN
    open c1(oldid);
    loop
        fetch c1 into cstruct;
        exit when c1%notfound;
        SELECT COUNT(*) INTO nbenr1 FROM GRHUM.REPART_STRUCTURE WHERE PERS_ID = newid and c_structure = cstruct;
        IF (nbenr1 = 0) THEN
            INSERT INTO ECHANGER_COMPTE_RENDU
            VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.REPART_STRUCTURE set pers_id = '||newid||' where pers_id = '||oldid||' and c_structure = '||cstruct||';');
        END IF;
    end loop;
    close c1;
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SECRETARIAT
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SECRETARIAT set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --
/*
  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STAGE
  WHERE  no_dossier_pers = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STAGE set no_dossier_pers = '||newno||' where no_dossier_pers = '||oldno||';');
  END IF;
*/
  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_owner = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_owner = '||newno||' where grp_owner = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.STRUCTURE_ULR
  WHERE  grp_responsable = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.STRUCTURE_ULR set grp_responsable = '||newno||' where grp_responsable = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_ROLE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_ROLE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.SUPANN_REPART_CATEGORIE
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.SUPANN_REPART_CATEGORIE set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   grhum.TRAVAUX_CARTES
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               'update GRHUM.TRAVAUX_CARTES set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  IF (ligne1 IS NOT NULL)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne1);
       IF (ligne2 IS NOT NULL) then
       INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
               ligne2);
       end if;
  END IF;
END;
/
-------------------------V20141023.115546__DDL_MAJ_Ins_Fournis_Interne.sql--------------------------
create or replace
PROCEDURE GRHUM.Ins_Fournis_Interne_temp(agtordre grhum.FOURNIS_ULR.agt_ordre%TYPE)
-- Proc. de creation des fournisseurs pour les structure internes
IS

-- Curseur de recuperation des structures de type "Budget" (qui ne sont pas Fournisseurs) avec leurs adresses princ. pro.
CURSOR c_struct_internes_pas_four IS
SELECT DISTINCT S.pers_id
FROM STRUCTURE_ULR S, REPART_TYPE_GROUPE R, REPART_PERSONNE_ADRESSE RPA
WHERE S.C_STRUCTURE = R.C_STRUCTURE
AND S.pers_id = RPA.pers_id
AND S.pers_id NOT IN (SELECT DISTINCT pers_id FROM grhum.FOURNIS_ULR)
AND RPA.rpa_valide='O'
AND RPA.tadr_code = 'PRO'
AND R.TGRP_CODE = 'LB';

-- Curseur de recuperation des structures de type "Budget" (qui sont Fournisseurs) avec leurs adresses princ. pro.
CURSOR c_struct_internes IS
SELECT DISTINCT S.pers_id
FROM STRUCTURE_ULR S, REPART_TYPE_GROUPE R, REPART_PERSONNE_ADRESSE RPA
WHERE S.C_STRUCTURE = R.C_STRUCTURE
AND S.pers_id = RPA.pers_id
AND S.pers_id IN (SELECT DISTINCT pers_id FROM grhum.FOURNIS_ULR)
AND RPA.rpa_valide='O'
AND RPA.tadr_code = 'PRO'
AND R.TGRP_CODE = 'LB';

structpasfour c_struct_internes_pas_four%ROWTYPE;
struct c_struct_internes%ROWTYPE;

annuaire_fou_valide_interne GRHUM.GRHUM_PARAMETRES.PARAM_VALUE%TYPE;
cstruct NUMBER;
owner NUMBER;
paramordre NUMBER;
nb NUMBER;
insadrordre grhum.REPART_PERSONNE_ADRESSE.adr_ordre%TYPE;

BEGIN

-- verif parametre ANNUAIRE_FOU_VALIDE_INTERNE existe
SELECT COUNT(*) INTO nb FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_INTERNE';
IF (nb=0)
THEN
	--RAISE_APPLICATION_ERROR(-20000, 'Parametre ANNUAIRE_FOU_VALIDE_INTERNE manquant dans grhum.grhum_parametres !');
	-- n'existe pas, on va le creer...
    SELECT COUNT(*) INTO nb FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_PHYSIQUE';
    IF (nb=0)
    THEN
	   RAISE_APPLICATION_ERROR(-20000, 'Parametre ANNUAIRE_FOU_VALIDE_PHYSIQUE manquant dans grhum.grhum_parametres, impossible de determiner ou chercher/creer le groupe des fournisseurs valides internes !');
    END IF;
    SELECT PARAM_VALUE INTO nb FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_PHYSIQUE';
	SELECT C_STRUCTURE_PERE INTO cstruct FROM GRHUM.STRUCTURE_ULR WHERE c_structure=nb;
	SELECT GRP_OWNER INTO owner FROM GRHUM.STRUCTURE_ULR WHERE c_structure=nb;
	SELECT COUNT(*) INTO nb FROM GRHUM.STRUCTURE_ULR WHERE LOWER(ll_structure) LIKE '%interne%' AND c_structure_pere=cstruct;
	IF (nb=0)
	THEN
	   -- on cree le groupe qui va bien
	   SELECT SEQ_STRUCTURE_ULR.NEXTVAL INTO cstruct FROM dual;
	   SELECT SEQ_PERSONNE.NEXTVAL INTO nb FROM dual;
	   INSERT INTO STRUCTURE_ULR (C_STRUCTURE, PERS_ID, LL_STRUCTURE, LC_STRUCTURE, C_TYPE_STRUCTURE,  C_STRUCTURE_PERE, GRP_OWNER, GRP_ACCES)
	   		  VALUES (cstruct,nb,'04 - FOURNISSEURS VALIDES (INTERNES)','FOU-INT','A',cstruct,owner,'-');
    ELSE
        -- on recupere le c_structure du groupe qui va bien
        SELECT c_structure INTO cstruct FROM GRHUM.STRUCTURE_ULR WHERE LOWER(ll_structure) LIKE '%interne%' AND c_structure_pere=cstruct AND ROWNUM=1;
	END IF;

    -- recherche d'un param_ordre pour ajouter le parametre...
	SELECT MAX(PARAM_ORDRE)+1 INTO paramordre FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY LIKE 'ANNUAIRE_%';
	SELECT COUNT(*) INTO nb FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE=paramordre;
	IF (nb > 0)
	THEN
	   SELECT MAX(PARAM_ORDRE)+1 INTO paramordre FROM GRHUM.GRHUM_PARAMETRES;
	END IF;

	-- Ajout du parametre
	INSERT INTO GRHUM.GRHUM_PARAMETRES VALUES(paramordre, 'ANNUAIRE_FOU_VALIDE_INTERNE', TO_CHAR(cstruct), 'C_structure du groupe des fournisseurs valides (Internes)',
          (SELECT PERS_ID FROM COMPTE WHERE CPT_LOGIN=(SELECT MIN(PARAM_VALUE) FROM GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')),
          (SELECT PERS_ID FROM COMPTE WHERE CPT_LOGIN=(SELECT MIN(PARAM_VALUE) FROM GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')),
           SYSDATE, SYSDATE, (SELECT TYPE_ID FROM GRHUM_PARAMETRES_TYPE WHERE TYPE_ID_INTERNE='C_STRUCTURE'));
END IF;
SELECT param_value INTO annuaire_fou_valide_interne FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_INTERNE';



OPEN c_struct_internes_pas_four;
LOOP
	FETCH c_struct_internes_pas_four INTO structpasfour;
	EXIT WHEN c_struct_internes_pas_four %NOTFOUND;

	SELECT COUNT(*) INTO nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
	WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O' AND RPA.tadr_code='FACT' AND RPA.rpa_principal='O';

	IF (nb>=1) -- une ou plusieurs adresses type FACT principales ==> on prend la premiere
	THEN
    	SELECT RPA.adr_ordre INTO insadrordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
        WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O' AND RPA.tadr_code='FACT' AND RPA.rpa_principal='O' AND ROWNUM=1;
    ELSE
    	SELECT COUNT(*) INTO nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
        WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O' AND RPA.tadr_code='FACT';
    	IF (nb>=1) -- une ou plusieurs adresses type FACT ==> on prend la premiere
        THEN
            SELECT RPA.adr_ordre INTO insadrordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
            WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O' AND RPA.tadr_code='FACT' AND ROWNUM=1;
		ELSE -- aucune adresse de type FACT, on cherche les autres (principale en priorite)
			SELECT COUNT(*) INTO nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
			WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O' AND RPA.rpa_principal='O';
			IF (nb>=1)
			THEN -- une ou plusieurs adresses principales ==> on prend la premiere
				SELECT RPA.adr_ordre INTO insadrordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
				WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O' AND RPA.rpa_principal='O' AND ROWNUM=1;
			ELSE
				SELECT COUNT(*) INTO nb FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
				WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O';
				IF (nb>=1)
				THEN -- premiere adresse valide
					SELECT RPA.adr_ordre INTO insadrordre FROM GRHUM.REPART_PERSONNE_ADRESSE RPA
					WHERE RPA.pers_id=structpasfour.pers_id AND RPA.rpa_valide='O' AND ROWNUM=1;
				ELSE
					RAISE_APPLICATION_ERROR(-20002, 'Aucune adresse valide trouvee pour le pers_id ' || structpasfour.pers_id);
				END IF;
			END IF;
		END IF;
	END IF;

	-- creation fournis
	Ins_Fournis_Ulr(
				structpasfour.pers_id,		  --INS_PERS_ID         FOURNIS_ULR.pers_id%TYPE,
				insadrordre,				  --INS_ADR_ORDRE 		FOURNIS_ULR.adr_ordre%TYPE,
				SYSDATE,			  		  --INS_FOU_DATE        FOURNIS_ULR.fou_date%TYPE,
				'0',				  		  --INS_FOU_MARCHE      FOURNIS_ULR.fou_marche%TYPE,
				'O',				  		  --INS_FOU_VALIDE      FOURNIS_ULR.fou_valide%TYPE,
				agtordre,			  		  --INS_AGT_ORDRE       FOURNIS_ULR.AGT_ORDRE%TYPE,
				'F'					  		  --INS_FOU_TYPE        FOURNIS_ULR.fou_type%TYPE
				);

END LOOP;
CLOSE c_struct_internes_pas_four;

-- on degage et remet dans le groupe
DELETE FROM GRHUM.REPART_STRUCTURE WHERE c_structure=annuaire_fou_valide_interne;

OPEN c_struct_internes;
LOOP
	FETCH c_struct_internes INTO struct;
	EXIT WHEN c_struct_internes %NOTFOUND;

	Ins_Repart_Structure(struct.pers_id, annuaire_fou_valide_interne);

END LOOP;
CLOSE c_struct_internes;

END;
/
------------------------V20141023.120629__DDL_Suppr_Procedures_Obsoletes.sql------------------------
DECLARE
BEGIN
	GRHUM.adm_drop_object('GRHUM', 'MAJ_BAP_ELEMENT_CARRIERE', 'PROCEDURE');
	GRHUM.adm_drop_object('GRHUM', 'CALCULER_AFF_MANGUE_HAMAC', 'PROCEDURE');
END;
/


