--
-- Patch DML de GRHUM du 27/10/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.24.0
-- Date de publication : 27/10/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------------V20140915.172636__DML_maj_grhum_param.sql------------------------------
create or replace
PROCEDURE      GRHUM.ADM_MAJ_PARAM (
   nomParam       				GRHUM.GRHUM_PARAMETRES.PARAM_KEY%Type,
   valueParam		    		GRHUM.GRHUM_PARAMETRES.PARAM_VALUE%Type,
   descriptionParam				GRHUM.GRHUM_PARAMETRES.PARAM_COMMENTAIRES%Type,
   typeParam				GRHUM.GRHUM_PARAMETRES_TYPE.TYPE_ID_INTERNE%Type
   
)
-- Procédure d'insertion d'un paramètre
-- dans la table GRHUM.GRHUM_PARAMETRES
-- nomParam : nom du paramètre
-- valueParam : valeur du paramètre
-- descriptionParam : description de ce que fait le paramètre
-- typeParam : type de paramètre que l'on veut créer
IS
   flag integer;
   typeId integer;
   persIdCreateur integer;
BEGIN

	-- persIdCreation
   SELECT PERS_ID into persIdCreateur
                FROM GRHUM.COMPTE
        WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES
                                                WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));

	select count(*) into flag from grhum_parametres where param_key = trim(nomParam);
	select type_id into typeId from GRHUM.GRHUM_PARAMETRES_TYPE where type_id_interne = trim(typeParam);
	
 if (flag > 0)
 then
		UPDATE GRHUM.GRHUM_PARAMETRES
		SET PARAM_KEY = trim(nomParam), PARAM_VALUE = valueParam, PARAM_COMMENTAIRES = descriptionParam,
			PERS_ID_MODIFICATION = persIdCreateur, D_MODIFICATION = SYSDATE, PARAM_TYPE_ID = typeId
		WHERE param_key = trim(nomParam);
 else
    	INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, D_CREATION, PERS_ID_MODIFICATION, D_MODIFICATION, PARAM_TYPE_ID)
			values (grhum.grhum_parametres_seq.nextval, trim(nomParam), valueParam, descriptionParam, persIdCreateur, SYSDATE, persIdCreateur, SYSDATE, typeId);
end if;

END;
/

--------------------------V20140915.172655__DML_maj_type_param_mangue.sql---------------------------
---------------------------------------------------------------------------
-- Mise à jour du paramètre 'org.cocktail.mangue.SIGNATURE_ARRETES'
---------------------------------------------------------------------------

declare
begin

    GRHUM.ADM_MAJ_PARAM('org.cocktail.mangue.SIGNATURE_ARRETES', 'Le Président', 'Signataire des arrêtés de congés dans Mangue', 
	'FREE_TEXT');

end;
/

-------------------V20140918.112703__DML_maj_param_utilisation_numero_emploi.sql--------------------
---------------------------------------------------------------------------
-- Ajout du paramètre 'org.cocktail.mangue.gestion_numero_local'
---------------------------------------------------------------------------

declare
begin

    GRHUM.ADM_MAJ_PARAM('org.cocktail.mangue.gestion_numero_local', 0, 'Utilisation du numéro local pour les emplois => 0 : pas d''utilisation, 1 : Numéro formatté', 'VALEUR_NUMERIQUE');

end;
/

------------------------------V20141007.160621__DML_Maj_Type_Etab.sql-------------------------------
DECLARE
	cpt INTEGER;
BEGIN
	SELECT count(*) INTO cpt FROM GRHUM.type_etablissement
							 WHERE c_type_etablissemen = 'EPCS';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.type_etablissement(c_type_etablissemen, lc_type_etablissemen, ll_type_etablissemen, d_deb_val, d_fin_val, d_creation, d_modification)
			VALUES('EPCS', 'Et. public coop. scient.', 'Etab. public de coopération scientifique', null, null, SYSDATE, SYSDATE);
	ELSE
		UPDATE GRHUM.type_etablissement
		SET c_type_etablissemen = 'EPCS',
			lc_type_etablissemen = 'Et. public coop. scient.',
			ll_type_etablissemen = 'Etab. public de coopération scientifique',
			d_modification = SYSDATE
		WHERE c_type_etablissemen = 'EPCS';
	END IF;

END;
/
--------------------------------V20141008.100716__DML_Ajout_RNE.sql---------------------------------
DECLARE
BEGIN
	-- http://www.education.gouv.fr/annuaire/40-landes/biscarrosse/college/college-nelson-mandela.html
	GRHUM.MAJ_UAI('0401048X','COLLEGE NELSON MANDELA','230 route des Lacs','40600',null,null,'004','PU','BISCARROSSE',null);
	
	-- http://www.education.gouv.fr/annuaire/40-landes/saint-geours-de-maremne/college/college-aime-cesaire.html
	GRHUM.MAJ_UAI('0401070W','COLLEGE AIME CESAIRE','450 avenue George Sand','40230', to_date('01/09/2012','dd/mm/yyyy'), null,'004','PU','SAINT-GEOURS-DE-MAREMNE', null);
END;
/


-- http://www.education.gouv.fr/annuaire/40-landes/biscarrosse/college/college-nelson-mandela.html
--insert into grhum.rne (c_rne, ll_rne, adresse, code_postal, d_creation, d_modification, acad_code, tetab_code, etab_statut, ville)
--values('0401048X','COLLEGE NELSON MANDELA','230 route des Lacs','40600',SYSDATE,SYSDATE,'004','CLG','PU','BISCARROSSE');

-- http://www.education.gouv.fr/annuaire/40-landes/saint-geours-de-maremne/college/college-aime-cesaire.html
--insert into grhum.rne (c_rne, ll_rne, adresse, code_postal, d_deb_val, d_creation, d_modification, acad_code, tetab_code, etab_statut, ville)
--values('0401070W','COLLEGE AIME CESAIRE','450 avenue George Sand','40230', to_date('01/09/2012','dd/mm/yyyy'), SYSDATE,SYSDATE,'004','CLG','PU','SAINT-GEOURS-DE-MAREMNE');


update grhum.rne set acad_code ='004' where c_rne = '0640302A';
update grhum.rne set acad_code ='004' where c_rne = '0640057P';

update grhum.rne
set tetab_code='CLG'
where ll_rne like 'COLLEGE%'
and tetab_code is null;
------------------------------V20141010.121507__DML_Maj_Indiv_Dipl.sql------------------------------
UPDATE MANGUE.INDIVIDU_DIPLOMES SET c_diplome = '0000038' WHERE c_diplome = '0321000';
UPDATE MANGUE.INDIVIDU_DIPLOMES SET c_diplome = '0679999' WHERE c_diplome = '0000056';

UPDATE GRHUM.INCLUSION_DIPLOME SET c_diplome = '0000038' WHERE c_diplome = '0321000';
UPDATE GRHUM.INCLUSION_DIPLOME SET c_diplome = '0679999' WHERE c_diplome = '0000056';

UPDATE GRHUM.ATER_DIPLOME SET c_diplome = '0000038' WHERE c_diplome = '0321000';
UPDATE GRHUM.ATER_DIPLOME SET c_diplome = '0679999' WHERE c_diplome = '0000056';

UPDATE IMPORT.INDIVIDU_DIPLOMES SET c_diplome = '0000038' WHERE c_diplome = '0321000';
UPDATE IMPORT.INDIVIDU_DIPLOMES SET c_diplome = '0679999' WHERE c_diplome = '0000056';

DELETE FROM GRHUM.DIPLOMES WHERE c_diplome = '0321000' or c_diplome = '0000056';
--------------------------V20141010.121657__DML_Ajout_categorie_supAnn.sql--------------------------

DECLARE
	cpt integer;
BEGIN
	SELECT count(*) INTO cpt FROM GRHUM.SUPANN_CATEGORIE
							 WHERE cat_libelle = 'retired';
	
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.SUPANN_CATEGORIE (CAT_NUMERO, CAT_LIBELLE, CAT_LIBELLE2, CAT_LISTE_ROUGE)
		     VALUES (9, 'retired', 'retraité lié à l''établissements', 'N');
		     
		INSERT INTO GRHUM.SUPANN_CATEGORIE (CAT_NUMERO, CAT_LIBELLE, CAT_LIBELLE2, CAT_LISTE_ROUGE)
		     VALUES (10, 'emeritus', 'professeur ayant l''éméritat', 'N');

		INSERT INTO GRHUM.SUPANN_CATEGORIE (CAT_NUMERO, CAT_LIBELLE, CAT_LIBELLE2, CAT_LISTE_ROUGE)
		     VALUES (11, 'teacher', 'enseignant', 'N');
		     
		INSERT INTO GRHUM.SUPANN_CATEGORIE (CAT_NUMERO, CAT_LIBELLE, CAT_LIBELLE2, CAT_LISTE_ROUGE)
		     VALUES (12, 'register_reader', 'lecteur de bibilothèque', 'N');
	END IF;
END;
/
-------------------------V20141015.154233__DML_Ajout_Motif_Tps_Partiel.sql--------------------------
DECLARE
	cpt integer;
BEGIN

	SELECT count(*) INTO cpt FROM GRHUM.MOTIF_TEMPS_PARTIEL
							 WHERE c_motif_temps_partiel = 'EN';
	IF (cpt = 0) THEN
		-- Ajout du motif de création/ reprise d'une entreprise
		INSERT INTO GRHUM.MOTIF_TEMPS_PARTIEL(C_MOTIF_TEMPS_PARTIEL, LC_MOTIF_TEMPS_PARTIEL, LL_MOTIF_TEMPS_PARTIEL, TEM_CTRL_DUREE_TPS, D_CREATION, D_MODIFICATION, C_MOTIF_TEMPS_PARTIEL_ONP, TEM_QUOTITE_LIMITEE)
			VALUES('EN', 'Entreprise', 'Pour créer ou reprendre une entreprise', 'N', TO_DATE('25/02/2010', 'dd/mm/yyyy'), TO_DATE('25/02/2010', 'dd/mm/yyyy'), 'MS214', 'O');
	END IF;
	
END;
/	
------------------------------V20141015.154317__DML_Maj_Ministeres.sql------------------------------
-- Suppression de Ministères non présent dans la BCN
DELETE FROM GRHUM.MINISTERES WHERE code_ministere in ('MI290', 'MI300', 'MI310', 'MI320', 'MI340');
DELETE FROM GRHUM.MINISTERES WHERE code_ministere in ('AI001', 'AI002', 'AI003', 'AI004', 'AI006');
DELETE FROM GRHUM.MINISTERES WHERE code_ministere in ('AI008', 'AI009', 'AI011', 'AI013', 'AI014');
DELETE FROM GRHUM.MINISTERES WHERE code_ministere in ('AI015', 'AI0116', 'AI017', 'AI018', 'AI019');
DELETE FROM GRHUM.MINISTERES WHERE code_ministere in ('AI020', 'AI021', 'AI022', 'AI023', 'AI024');
DELETE FROM GRHUM.MINISTERES WHERE code_ministere in ('AI025', 'AI026', 'AI027', 'AI028', 'AI029');
DELETE FROM GRHUM.MINISTERES WHERE code_ministere in ('AI030', 'AI031', 'AI032');

--MAJ de ministères à partir de la BCN
UPDATE GRHUM.MINISTERES
	SET lc_ministere = 'MAEE',
		ll_ministere = 'Affaires étrangères et européennes'
	WHERE code_ministere = 'MI110';

UPDATE GRHUM.MINISTERES
	SET lc_ministere = 'MTRSFSV',
		ll_ministere = 'Travail, relations sociales, famille, solidarité et ville'
	WHERE code_ministere = 'MI120';

UPDATE GRHUM.MINISTERES
	SET lc_ministere = 'MAAP',
		ll_ministere = 'Alimentation, agriculture et pêche'
	WHERE code_ministere = 'MI130';

UPDATE GRHUM.MINISTERES
	SET lc_ministere = 'MEEDDM',
		ll_ministere = 'Ecologie, énergie, développement durable et mer'
	WHERE code_ministere = 'MI190';

UPDATE GRHUM.MINISTERES
	SET lc_ministere = 'OUTRE-MER',
		d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy')
	WHERE code_ministere = 'MI230';

UPDATE GRHUM.MINISTERES
	SET lc_ministere = 'MEIE',
		ll_ministere = 'Economie, industrie, emploi'
	WHERE code_ministere = 'MI250';


-- Ajout de ministères fermés ou actuels
DECLARE
	cpt INTEGER;
BEGIN
	-- Ministère de l'emploi
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI121';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI121', 'EMPLOI', 'Emploi', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI121',
			lc_ministere = 'EMPLOI',
			ll_ministere = 'Emploi',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'O'
		WHERE code_ministere = 'MI121';
	END IF;
	
	
	-- Ministère de la solidarité
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI122';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI122', 'SOLIDARITE', 'Solidarité', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI122',
			lc_ministere = 'SOLIDARITE',
			ll_ministere = 'Solidarité',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI122';
	END IF;
	
	-- Ministère de l'écologie
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI160';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI160', 'ECOLOGIE', 'Ecologie', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI160',
			lc_ministere = 'ECOLOGIE',
			ll_ministere = 'Solidarité',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI160';
	END IF;
	
	-- Ministère de l'économie, finances et industrie
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI170';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI170', 'MINEFI', 'Economie, finances et industrie', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI170',
			lc_ministere = 'MINEFI',
			ll_ministere = 'Economie, finances et industrie',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI170';
	END IF;
	
	-- Ministère de l'économie, finances et industrie (hors Monnaies et médailles)
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI171';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI171', 'MINEFI (hors mon et méd)', 'Economie, finances et industrie (hors Monnaies et médailles)', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI171',
			lc_ministere = 'MINEFI (hors mon et méd)',
			ll_ministere = 'Economie, finances et industrie (hors Monnaies et médailles)',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI171';
	END IF;
	
	-- Ministère des monnaies et médailles
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI172';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI172', 'MONNAIES ET MEDAILLES', 'Monnaies et médailles', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI172',
			lc_ministere = 'MONNAIES ET MEDAILLES',
			ll_ministere = 'Monnaies et médailles',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI172';
	END IF;
	
	-- Ministère de l'équipement (hors aviation civile)
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI191';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI191', 'EQUIPEMENT (hors av civ)', 'Equipement (hors aviation civile)', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI191',
			lc_ministere = 'EQUIPEMENT (hors av civ)',
			ll_ministere = 'Equipement (hors aviation civile)',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI191';
	END IF;
	
	-- Ministère de l'intérieur (hors police)
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI201';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI201', 'INTERIEUR (hors police)', 'Intérieur (hors police)', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI201',
			lc_ministere = 'INTERIEUR (hors police)',
			ll_ministere = 'Intérieur (hors police)',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI201';
	END IF;
	
	-- Ministère de la police nationale
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI202';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI202', 'POLICE NATIONALE', 'Police nationale', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI202',
			lc_ministere = 'POLICE NATIONALE',
			ll_ministere = 'Police nationale',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI202';
	END IF;
	
	-- Ministère de la santé et des sports
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI210';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI210', 'MSS', 'Santé et sports', TO_DATE('31/12/1899', 'dd/mm/yyyy'), null, 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI210',
			lc_ministere = 'MSS',
			ll_ministere = 'Santé et sports',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = null,
			tem_visible = 'N'
		WHERE code_ministere = 'MI210';
	END IF;
	
	-- Ministère du premier minsitre (hors journaux officiels)
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI241';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI241', 'SPM (hors JO)', 'Premier ministre (hors journaux officiels)', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI241',
			lc_ministere = 'SPM (hors JO)',
			ll_ministere = 'Premier ministre (hors journaux officiels)',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI241';
	END IF;
	
	-- Journaux officiels
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI242';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI242', 'JO', 'Journaux officiels', TO_DATE('31/12/1899', 'dd/mm/yyyy'), TO_DATE('31/12/2009', 'dd/mm/yyyy'), 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI242',
			lc_ministere = 'JO',
			ll_ministere = 'Journaux officiels',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = TO_DATE('31/12/2009', 'dd/mm/yyyy'),
			tem_visible = 'N'
		WHERE code_ministere = 'MI242';
	END IF;
	
	-- Ministère du budget, comptes publics, fonction publique et réforme de l'Etat
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI260';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI260', 'MBCPFPRE', 'Budget, comptes publics, fonction publique et réforme de l''Etat', TO_DATE('31/12/1899', 'dd/mm/yyyy'), null, 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI260',
			lc_ministere = 'MBCPFPRE',
			ll_ministere = 'Budget, comptes publics, fonction publique et réforme de l''Etat',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = null,
			tem_visible = 'N'
		WHERE code_ministere = 'MI260';
	END IF;
	
	-- Ministère de l'immigration, intégration, identité nationale et développement solidaire
	SELECT count(*) INTO cpt FROM GRHUM.ministeres
							 WHERE code_ministere = 'MI270';
							 
	IF (cpt = 0) THEN
		INSERT INTO GRHUM.ministeres(ID_MINISTERE, CODE_MINISTERE, LC_MINISTERE, LL_MINISTERE, D_OUVERTURE, D_FIN, TEM_VISIBLE)
			VALUES(grhum.ministeres_seq.nextval, 'MI270', 'MIIINDS', 'Immigration, intégration, identité nationale et développement solidaire', TO_DATE('31/12/1899', 'dd/mm/yyyy'), null, 'N');
	ELSE
		UPDATE GRHUM.ministeres
		SET code_ministere = 'MI270',
			lc_ministere = 'MIIINDS',
			ll_ministere = 'Immigration, intégration, identité nationale et développement solidaire',
			d_ouverture = TO_DATE('31/12/1899', 'dd/mm/yyyy'),
			d_fin = null,
			tem_visible = 'N'
		WHERE code_ministere = 'MI270';
	END IF;
END;
/

--------------------V20141017.095943__DML_Suppression_parametre_non_utilise.sql---------------------
declare
	cpt integer;
begin
	select count(*) into cpt from GRHUM.grhum_parametres where param_key like 'org.cocktail.grhum.compte..domainessecondairesVVlanE';
	
	if (cpt = 1) then
		delete from GRHUM.grhum_parametres where param_key like 'org.cocktail.grhum.compte..domainessecondairesVVlanE';
	end if;
end;
/
--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.24.0',to_date('27/10/2014','DD/MM/YYYY'),sysdate,'Maj de procédures, de vues et suppression de tables obsolètes');
COMMIT;
