--
-- Patch DML de GRHUM du 24/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 24/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--


WHENEVER SQLERROR EXIT SQL.SQLCODE;


declare
  is_appIdGirofle_exists int;
  appIdGirofle int;
begin
  SELECT count(*) into is_appIdGirofle_exists from GRHUM.GD_APPLICATION WHERE APP_STR_ID='GIROFLE';

  IF(is_appIdGirofle_exists>0) then
	SELECT APP_ID into appIdGirofle from GRHUM.GD_APPLICATION WHERE APP_STR_ID='GIROFLE';
  
  	DELETE FROM GRHUM.GD_FONCTION WHERE APP_ID=appIdGirofle;
  
  	-- insertion des differents droits (les fonctions)
  	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Girofle','GIROFLE','Accès a Girofle','Accès a Girofle');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Maquettage','MAQUETTAGE_DIPLOME','Accès aux Diplômes','Accès aux Diplômes');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Maquettage','MAQUETTAGE_RGPT','Accès aux Regroupements','Accès aux Regroupements');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Maquettage','MAQUETTAGE_UE','Accès aux UEs','Accès aux UEs');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Maquettage','MAQUETTAGE_EC','Acces aux ECs','Acces aux ECs');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Maquettage','MAQUETTAGE_TYPE_AP','Accès aux types d''APs','Accès aux types d''APs');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Maquettage','MAQUETTAGE_TYPE_AE','Accès aux types d''AEs','Accès aux types d''AEs');

	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Maquettage','MAQUETTAGE_MAQUETTE','Accès à la Maquette de formation','Accès à la Maquette de formation');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Modelisation','MODELISATION_MODEL','Accès à la modélisation d''un diplôme','Accès à la modélisation d''un diplôme');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Modelisation','MODELISATION_SYL_DIP','Accès au syllabus d''un diplôme','Accès au syllabus d''un diplôme');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Modelisation','MODELISATION_SYL_CMP','Accès au syllabus d''un composant','Accès au syllabus d''un composant');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Administration','ADMIN_AUTORISER_ASSO','Autoriser l''association de composants','Accès à l''administration GIROFLE - Autoriser l''association de composants');
	
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,appIdGirofle,'Administration','ADMIN_INTERDIRE_ASSO','Interdire  l''association de composants','Accès à l''administration GIROFLE - Interdire  l''association de composants');
  END IF;

end;
/

UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.8.1.0';
COMMIT;
