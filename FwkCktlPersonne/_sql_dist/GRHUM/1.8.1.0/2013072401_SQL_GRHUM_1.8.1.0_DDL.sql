--
-- Patch DML de GRHUM du 16/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.0.0 
-- Date de publication : 16/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.0.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.1.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.1.0 a deja ete passe !');
    end if;

end;
/
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.8.1.0', TO_DATE('16/07/2013', 'DD/MM/YYYY'),NULL,'Ajout des fonctions de Girofle dans la table GRHUM.GDFONCTIONS ');

--
-- 
--

COMMIT;
