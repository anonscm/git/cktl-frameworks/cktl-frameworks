--
-- Patch DDL de GRHUM du 21/05/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.18.0
-- Date de publication : 21/05/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.17.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.18.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.18.0 a deja ete passe !');
    end if;

end;
/

-------------------V20140521.141025__DDL_DDL_creation_sequence_grhum_etudiant.sql-------------------
-- ---------------------------------------------------------------------------
-- Création de la séquence GRHUM.ETUDIANT_SEQ si non créée avant
-- 	
-- ---------------------------------------------------------------------------


DECLARE
   compteur integer;
   startvalue integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur
	  FROM ALL_SEQUENCES
         WHERE SEQUENCE_OWNER = 'GRHUM'
           AND SEQUENCE_NAME = 'ETUDIANT_SEQ';  

	select COALESCE(MAX(ETUD_NUMERO)+1, 1) into startvalue from GRHUM.ETUDIANT;

  	IF (compteur = 0)
  	  THEN              
    		EXECUTE IMMEDIATE 'CREATE SEQUENCE GRHUM.ETUDIANT_SEQ START WITH ' || startvalue ||' INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';
  	
	  ELSE
	 	EXECUTE IMMEDIATE  'DROP SEQUENCE GRHUM.ETUDIANT_SEQ';	
   		EXECUTE IMMEDIATE 'CREATE SEQUENCE GRHUM.ETUDIANT_SEQ START WITH ' || startvalue ||' INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';
 
	END IF;
COMMIT;
END;
/
---------------------------V20140521.145400__DDL_DDL_fonctions_grhum.sql----------------------------

create or replace FUNCTION grhum.adm_is_object_exists (aObjectType varchar2, aOwner varchar2, aObjectName varchar2 )
/**
  Verifie l'existence d'un objet dans la base
  Renvoie 0 si l'objet n'existe pas, renvoie 1 sinon
*/

RETURN smallint
IS
 flag smallint;
BEGIN

  if (upper(aObjectType) = 'CONSTRAINT') then
  	select count(*) into flag from all_constraints where constraint_name = upper(aObjectName) and OWNER = upper(aOwner); 
  else
  	select count(*) into flag from all_objects where object_type = upper(aObjectType) and  object_name = upper(aObjectName) and OWNER = upper(aOwner); 
  end if;
  RETURN flag;
END;
/


create or replace procedure grhum.adm_create_table_ifnotexists (aOwner varchar2, aName varchar2, aColumns long, aTableSpace varchar2 default null) is
/*
  cree une table si celle ci n'existe pas deja.
  ex.1 : adm_create_table_ifnotexists('MARACUJA', 'SEPA_SDD_ECHEANCIER_BOB', 'CREATE TABLE "MARACUJA"."SEPA_SDD_ECHEANCIER_BOB"  ("SEB_ID" NUMBER NOT NULL ENABLE, 
	"ID_SEPA_SDD_ECHEANCIER" NUMBER NOT NULL ENABLE, 
	"BOB_ORDRE" NUMBER NOT NULL ENABLE' ,'GFC', );
  --------------------------
  ex.2:
  declare
   crt_ordre long;
begin
crt_ordre := q'[ create table mangue.test
   (    "EFIC_ORDRE" NUMBER(8,0) NOT NULL ENABLE,
        "EFIC_ANNEE" NUMBER(4,0),
        "NO_DOSSIER_PERS" NUMBER(8,0) NOT NULL ENABLE,
        "EFIC_UAI" VARCHAR2(8 BYTE) NOT NULL ENABLE,
        "EFIC_NUMEN" VARCHAR2(13 BYTE),
        "EFIC_CIVILITE" CHAR(1 BYTE) NOT NULL ENABLE,
        "EFIC_NOM_PATRONYMIQUE" VARCHAR2(40 BYTE) NOT NULL ENABLE,
        "EFIC_PRENOM" VARCHAR2(15 BYTE) NOT NULL ENABLE,
        "EFIC_NOM_USUEL" VARCHAR2(20 BYTE),
        "EFIC_D_NAISSANCE" DATE NOT NULL ENABLE,
        "EFIC_C_PAYS_NATIONALITE" VARCHAR2(3 BYTE) NOT NULL ENABLE,
        "EFIC_C_POSITION" VARCHAR2(5 BYTE),
        "EFIC_D_DEB_POSTION" DATE NOT NULL ENABLE,
        "EFIC_D_FIN_POSTION" DATE,
        "EFIC_C_SECTION_CNU" VARCHAR2(5 BYTE),
        "EFIC_C_CORPS" VARCHAR2(3 BYTE),
        "EFIC_D_NOMINATION_CORPS" DATE,
        "EFIC_D_TITULARISATION" DATE,
        "EFIC_C_GRADE" VARCHAR2(4 BYTE),
        "EFIC_D_GRADE" DATE,
        "EFIC_C_ECHELON" VARCHAR2(2 BYTE),
        "EFIC_C_CHEVRON" VARCHAR2(1 BYTE),
        "EFIC_D_ECHELON" DATE,
        "EFIC_ANC_CONSERVEE" VARCHAR2(6 BYTE),
        "EFIC_TEM_VALIDE" CHAR(1 BYTE) DEFAULT 'N' NOT NULL ENABLE,
        "D_CREATION" DATE DEFAULT SYSDATE NOT NULL ENABLE,
        "D_MODIFICATION" DATE DEFAULT SYSDATE NOT NULL ENABLE,
        "PARP_ORDRE" NUMBER(8,0),
        "EFIC_ETAT" CHAR(1 BYTE),
        "EFIC_NO_ARRETE" VARCHAR2(20 BYTE),
        "EFIC_D_ARRETE" DATE,
        "EFIC_D_PROMOTION" DATE,
        "EFIC_D_EFFET_DEPART" DATE,
        "EFIC_C_MOTIF_DEPART" VARCHAR2(5 BYTE),
        "EFIC_QUOTITE" NUMBER(3,0),
        "EFIC_C_MOTIF_TEMPS_PARTIEL" VARCHAR2(5 BYTE),
        "EFIC_LIEU_POSITION" VARCHAR2(50 BYTE),
        "EFIC_D_EFFET_ARRIVEE" DATE,
        "EFIC_D_ENTREE_CATEGORIE" DATE,
        "EFIC_C_TYPE_ACCES_CORPS" VARCHAR2(2 BYTE),
        "EFIC_C_TYPE_ACCES_GRADE" VARCHAR2(2 BYTE),
        "EFIC_C_GRADE_PREV" VARCHAR2(4 BYTE),
        "EFIC_C_TYPE_ABSENCE" VARCHAR2(5 BYTE),
        "EFIC_D_DEB_ABSENCE" DATE,
        "EFIC_D_FIN_ABSENCE" DATE,
        "EFIC_D_OBSERVATION" DATE,
        "EFIC_OBJET" VARCHAR2(10 BYTE),
        "EFIC_OBSERVATIONS" VARCHAR2(500 BYTE),
        "EFIC_STATUT" VARCHAR2(10 BYTE),
        "EFIC_C_SECTION_CNU_EC" VARCHAR2(5 BYTE),
        "SUPF_ID" NUMBER,
        "EFIC_TEM_CRCT" VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL ENABLE,
         CONSTRAINT "TCHK_EFIC_CIVILITE" CHECK ((EFIC_CIVILITE IN ('1','2'))) ENABLE,
         CONSTRAINT "TCHK_EFIC_TEM_VALIDE" CHECK ((EFIC_TEM_VALIDE IN ('O','N'))) ENABLE,
         CONSTRAINT "TPK_ELECTRA_FICHIER" PRIMARY KEY ("EFIC_ORDRE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE,
         CONSTRAINT "TCHK_EFIC_ETAT" CHECK (EFIC_ETAT IN ('P','T','V','N')) ENABLE,
         CONSTRAINT "TELECTRA_FICHIER_REF_PERSONNEL" FOREIGN KEY ("NO_DOSSIER_PERS")
          REFERENCES "GRHUM"."PERSONNEL_ULR" ("NO_DOSSIER_PERS") ENABLE,
         CONSTRAINT "TELECTRA_FIC_REF_PARAM_PROMO" FOREIGN KEY ("PARP_ORDRE")
          REFERENCES "MANGUE"."PARAM_PROMOTION" ("PARP_ORDRE") ENABLE,
         CONSTRAINT "TFK_SUPINFO_FICHIER" FOREIGN KEY ("SUPF_ID")
          REFERENCES "MANGUE"."SUPINFO_FICHIER" ("SUPF_ID") DEFERRABLE INITIALLY DEFERRED ENABLE,
         CONSTRAINT "TFK_SUPINFO_CORPS" FOREIGN KEY ("EFIC_C_CORPS")
          REFERENCES "GRHUM"."CORPS" ("C_CORPS") ENABLE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT)
  -- TABLESPACE "GRH"
]';

adm_create_table_ifnotexists('MANGUE','TEST',crt_ordre, 'GRH');
end;
*/
  v_sql LONG;
  flag smallint;
  aSpecTbs varchar2(48) default null;
  v_sqlstarts varchar2(100);
begin
  if aTableSpace is not null then
     aSpecTbs := ' tablespace '||aTableSpace;
  end if;

  DBMS_OUTPUT.PUT_LINE('Creation de la table ' || aOwner ||'.'||aName);
  if (grhum.adm_is_object_exists('table', aOwner, aName) = 0) then
       v_sqlstarts := 'create table '||aOwner||'.'||aName||' ';
       if (replace(upper(trim(aColumns)), ' ', '') like replace(upper(v_sqlstarts), ' ', '')||'%' ) then
        v_sqlstarts :='';
       end if;
       v_sql:= v_sqlstarts || aColumns||aSpecTbs;
       DBMS_OUTPUT.PUT('    --> execution de '||v_sql);
       execute immediate v_sql;
       DBMS_OUTPUT.PUT_LINE(' ... OK');
  else
      DBMS_OUTPUT.PUT_LINE('    --> deja presente');
  end if;
  DBMS_OUTPUT.PUT_LINE('');
END;
/


create or replace procedure adm_create_seq_ifnotexists (aOwner varchar2, aName varchar2, aStartIndex integer, aMinValue integer) is
/**
  cree une sequence si celle ci n'existe pas deja. 
  ex. : adm_create_seq_ifnotexists('MARACUJA', 'SEPA_SDD_ECHEANCIER_BOB_SEQ', 1, 1);
*/
  v_sql LONG;
  flag smallint;
begin
  DBMS_OUTPUT.PUT_LINE('Creation de la sequence ' || aOwner ||'.'||aName);
  if (grhum.adm_is_object_exists('sequence', aOwner, aName) = 0) then
       v_sql:='create sequence '|| aOwner ||'.'||aName || ' START WITH '|| aStartIndex ||' MAXVALUE 999999999999999999999999999 MINVALUE '||  aMinValue ||' NOCYCLE NOCACHE NOORDER';
       DBMS_OUTPUT.PUT('    --> execution de '||v_sql);
       execute immediate v_sql;
       DBMS_OUTPUT.PUT_LINE(' ... OK');
  else
      DBMS_OUTPUT.PUT_LINE('    --> deja presente');
  end if;
  DBMS_OUTPUT.PUT_LINE('');
END; 
/


create or replace procedure adm_create_pk_ifnotexists (aOwner varchar2, aPkName varchar2, aTableName varchar2, aColumns varchar2, aTableSpaceIndex varchar2) is
/**
  - cree une cle primaire si celle ci n'existe pas deja. 
  - genere une commande du type ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER_BOB ADD constraint  PK_SEPA_SDD_ECHEANCIER_BOB PRIMARY KEY(SEB_ID) USING INDEX TABLESPACE GFC_INDX
  ex. : adm_create_pk_ifnotexists('MARACUJA', 'PK_SEPA_SDD_ECHEANCIER_BOB', 'SEPA_SDD_ECHEANCIER_BOB', 'SEB_ID', 'GFC_INDX');
*/
  v_sql LONG;
  flag smallint;
begin
  DBMS_OUTPUT.PUT_LINE('Creation de la cle primaire ' || aPkName || ' sur la table '|| aOwner ||'.'||aTableName);
  if (grhum.adm_is_object_exists('index', aOwner, aPkName) = 0) then
       v_sql:='ALTER TABLE '|| aOwner ||'.'||aTableName || ' ADD constraint '|| aPkName ||'  PRIMARY KEY( '||  aColumns ||') USING INDEX TABLESPACE '|| aTableSpaceIndex;
       DBMS_OUTPUT.PUT('    --> execution de '||v_sql);
       execute immediate v_sql;
       DBMS_OUTPUT.PUT_LINE(' ... OK');
  else
      DBMS_OUTPUT.PUT_LINE('    --> deja presente');
  end if;
  DBMS_OUTPUT.PUT_LINE('');
END; 
/


create or replace procedure adm_create_fk_ifnotexists (aOwner varchar2, aFkName varchar2, aTableName varchar2, aColumns varchar2, rOwner varchar2, rTableName varchar2, rColumns varchar2) is
/**
  - cree une cle etrangere si celle ci n'existe pas deja. 
  - genere une commande du type ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER_BOB ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_BOB_BOB  foreign key (BOB_ORDRE) references maracuja.bordereau_brouillard(BOB_ORDRE))
  ex. :  adm_create_fk_ifnotexists('MARACUJA', 'FK_SEPA_SDD_ECHEANCIER_BOB_BOB','SEPA_SDD_ECHEANCIER_BOB', 'BOB_ORDRE', 'MARACUJA', 'bordereau_brouillard', 'BOB_ORDRE');
*/
  v_sql LONG;
  flag smallint;
begin
  DBMS_OUTPUT.PUT_LINE('Creation de la cle etrangere ' || aFkName || ' sur la table '|| aOwner ||'.'||aTableName);
  if (grhum.adm_is_object_exists('constraint', aOwner, aFkName) = 0) then
       v_sql:='ALTER TABLE '|| aOwner ||'.'||aTableName || ' ADD (constraint '|| aFkName ||'  FOREIGN KEY ('||  aColumns ||') references '||   rOwner ||'.'||rTableName ||'( '||  rColumns ||')) ';
       DBMS_OUTPUT.PUT('    --> execution de '||v_sql);
       execute immediate v_sql;
       DBMS_OUTPUT.PUT_LINE(' ... OK');
  else
      DBMS_OUTPUT.PUT_LINE('    --> deja presente');
  end if;
  DBMS_OUTPUT.PUT_LINE('');
END; 
/


create or replace procedure adm_create_unq_ifnotexists (aOwner varchar2, aUnqName varchar2, aTableName varchar2, aColumns varchar2, aTableSpaceIndex varchar2) is
/**
  - cree une cle unique si celle ci n'existe pas deja. 
  - genere une commande du type   CREATE UNIQUE INDEX "MARACUJA"."UNQ_SEPA_SDD_ECHEANCIER_BOB" ON "MARACUJA"."SEPA_SDD_ECHEANCIER_BOB" ("ID_SEPA_SDD_ECHEANCIER", "BOB_ORDRE") TABLESPACE "GFC_INDX" 
  ex. :  adm_create_unq_ifnotexists ('MARACUJA', 'UNQ_SEPA_SDD_ECHEANCIER_BOB', 'SEPA_SDD_ECHEANCIER_BOB','ID_SEPA_SDD_ECHEANCIER, BOB_ORDRE', 'GFC_INDX');
*/
  v_sql LONG;
  flag smallint;
begin
  DBMS_OUTPUT.PUT_LINE('Creation de la contrainte d''unicite ' || aUnqName || ' sur la table '|| aOwner ||'.'||aTableName);
  if (grhum.adm_is_object_exists('constraint', aOwner, aUnqName) = 0) then
       v_sql:='ALTER TABLE '|| aOwner ||'.'||aTableName || ' ADD constraint '|| aUnqName ||'  UNIQUE ('||  aColumns ||') using index tablespace '|| aTableSpaceIndex;
       DBMS_OUTPUT.PUT('    --> execution de '||v_sql);
       execute immediate v_sql;
       DBMS_OUTPUT.PUT_LINE(' ... OK');
  else
      DBMS_OUTPUT.PUT_LINE('    --> deja presente');
  end if;
  DBMS_OUTPUT.PUT_LINE('');
END; 
/




