--
-- Patch DML de GRHUM du 21/05/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.18.0
-- Date de publication : 21/05/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



------------------V20140519.135537__DML_Maj_Passage_echelon_Maitre_Auxiliaire.sql-------------------

-- Passage d'échelons pour les grades du corps 496 MAITRE AUXILIAIRE

DECLARE
   compteur NUMBER;
BEGIN

	-- On recherche l'existence des correspondances du grade 4961 avec la TG
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.LIEN_GRADE_MEN_TG 
	 WHERE C_GRADE = '4961';

	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.LIEN_GRADE_MEN_TG (C_GRADE, C_GRADE_TG, DATE_OUVERTURE, DATE_FERMETURE, D_CREATION, D_MODIFICATION, LIBELLE)
		     VALUES ('4961', '0625040000', NULL, NULL, SYSDATE, SYSDATE, 'ECR MAITRE AUXILIAIRE 1ERE CATEGORIE');
	END IF;		


	-- On recherche l'existence des échelons du grade 4961
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.PASSAGE_ECHELON 
	 WHERE C_GRADE = '4961';
	    IF (compteur = 0) 
	  THEN
		-- Insertion des échelons correspondants
		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '01', 379, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '02', 423, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '03', 450, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '04', 480, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '05', 510, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '06', 541, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '07', 573, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4961', '08', 603, NULL, NULL, NULL, NULL, NULL, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');
	END IF;


	-- On recherche l'existence des correspondances du grade 4962 avec la TG
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.LIEN_GRADE_MEN_TG 
	 WHERE C_GRADE = '4962';

	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.LIEN_GRADE_MEN_TG (C_GRADE, C_GRADE_TG, DATE_OUVERTURE, DATE_FERMETURE, D_CREATION, D_MODIFICATION, LIBELLE)
		     VALUES ('4962', '0625030000', NULL, NULL, SYSDATE, SYSDATE, 'ECR MAITRE AUXILIAIRE 2EME CATEGORIE');
	END IF;		

	-- On recherche l'existence des échelons du grade 4962
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.PASSAGE_ECHELON 
	 WHERE C_GRADE = '4962';
	    IF (compteur = 0) 
	  THEN
		-- Insertion des échelons correspondants
		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '01', 340, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '02', 361, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '03', 381, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '04', 412, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '05', 436, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '06', 450, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '07', 479, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4962', '08', 521, NULL, NULL, NULL, NULL, NULL, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');
	END IF;


	-- On recherche l'existence des correspondances du grade 4963 avec la TG
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.LIEN_GRADE_MEN_TG 
	 WHERE C_GRADE = '4963';
	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.LIEN_GRADE_MEN_TG (C_GRADE, C_GRADE_TG, DATE_OUVERTURE, DATE_FERMETURE, D_CREATION, D_MODIFICATION, LIBELLE)
		     VALUES ('4963', '0625020000', NULL, NULL, SYSDATE, SYSDATE, 'ECR MAITRE AUXILIAIRE 3EME CATEGORIE');
	END IF;		

	-- On recherche l'existence des échelons du grade 4963
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.PASSAGE_ECHELON 
	 WHERE C_GRADE = '4963';
	    IF (compteur = 0) 
	  THEN
		-- Insertion des échelons correspondants
		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '01', 267, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '02', 302, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '03', 321, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '04', 340, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '05', 363, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '06', 389, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '07', 421, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4963', '08', 444, NULL, NULL, NULL, NULL, NULL, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');
	END IF;


	-- On recherche l'existence des correspondances du grade 4964 avec la TG
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.LIEN_GRADE_MEN_TG 
	 WHERE C_GRADE = '4964';
	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.LIEN_GRADE_MEN_TG (C_GRADE, C_GRADE_TG, DATE_OUVERTURE, DATE_FERMETURE, D_CREATION, D_MODIFICATION, LIBELLE)
		     VALUES ('4964', '0625010000', NULL, NULL, SYSDATE, SYSDATE, 'ECR MAITRE AUXILIAIRE 4EME CATEGORIE');
	END IF;		

	-- On recherche l'existence des échelons du grade 4964
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.PASSAGE_ECHELON 
	 WHERE C_GRADE = '4964';
	    IF (compteur = 0) 
	  THEN
		-- Insertion des échelons correspondants
		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '01', 267, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '02', 288, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '03', 312, NULL, NULL, 2, 6, 3, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '04', 320, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '05', 332, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '06', 351, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '07', 370, NULL, NULL, 3, NULL, 4, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');

		INSERT INTO GRHUM.PASSAGE_ECHELON (C_GRADE, C_ECHELON, C_INDICE_BRUT, DUREE_GR_CHOIX_ANNEES, DUREE_GR_CHOIX_MOIS, DUREE_PT_CHOIX_ANNEES, DUREE_PT_CHOIX_MOIS, DUREE_PASSAGE_ANNEES, DUREE_PASSAGE_MOIS, D_CREATION, D_MODIFICATION, D_FERMETURE, D_OUVERTURE, TEM_LOCAL)
		     VALUES ('4964', '08', 384, NULL, NULL, NULL, NULL, NULL, NULL, SYSDATE, SYSDATE, NULL, TO_DATE('01/01/1900', 'dd/mm/yyyy'), 'N');
	END IF;

END;
/

----------------------V20140519.135630__DML_Maj_Passage_echelon_Professeur.sql----------------------

--On rattache le corps des élèves-professeur au type de population 'DA'
UPDATE GRHUM.CORPS
SET c_type_corps = 'DA'
WHERE c_corps = '590';


DECLARE
   compteur NUMBER;
BEGIN

-- On met à jour l'échelon 01 du grade 3002
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.PASSAGE_ECHELON 
	 WHERE C_GRADE = '3002'
	   AND C_ECHELON = '01'
	   AND D_OUVERTURE = TO_DATE('01/09/2009','dd/mm/yyyy');
	    IF (compteur = 1) 
	  THEN
		UPDATE GRHUM.PASSAGE_ECHELON 
   		   SET D_FERMETURE = NULL, D_MODIFICATION = SYSDATE 
 		 WHERE C_GRADE = '3002'
	   	   AND C_ECHELON = '01'
 		   AND D_OUVERTURE = TO_DATE('01/09/2009','dd/mm/yyyy');
	END IF;

END;
/

----------------------------V20140521.141357__DML_MAJ_PERSONNEL_ULR.sql-----------------------------
COMMENT ON COLUMN GRHUM.PERSONNEL_ULR.CIR_D_VERIFICATION
IS
  'Date de dernière mise à jour du dossier de l''agent';
COMMENT ON COLUMN GRHUM.PERSONNEL_ULR.CIR_D_CERTIFICATION
IS
  'Date de certification de l''identité de l''agent';
COMMENT ON COLUMN GRHUM.PERSONNEL_ULR.CIR_D_COMPLETUDE
IS
  'Date à partir de laquelle le dossier de l''agent est à jour';
COMMENT ON COLUMN GRHUM.PERSONNEL_ULR.ID_MINISTERE
IS
  'Clef de la table MINISTERE pour le ministère de tutelle de l''agent';
COMMENT ON COLUMN GRHUM.PERSONNEL_ULR.NO_EPICEA
IS
  'Numéro d''immatriculation des agents du ministère de l''Agriculture';
-----------------------------V20140521.142347__DML_MAJ_GROUPES_REF.sql------------------------------
DECLARE
	cStructureRacine VARCHAR2(10);
BEGIN
	SELECT c_structure INTO cStructureRacine FROM GRHUM.STRUCTURE_ULR s
		WHERE s.c_type_structure = 'E'
		AND s.c_structure = s.c_structure_pere;


	UPDATE GRHUM.STRUCTURE_ULR
		SET c_structure_pere = cStructureRacine
		WHERE lc_structure = 'REFERENTIEL_APP';
	
	UPDATE GRHUM.STRUCTURE_ULR
		SET c_structure_pere = cStructureRacine
		WHERE lc_structure = 'WORKFLOW';
		
	UPDATE GRHUM.STRUCTURE_ULR
		SET c_structure_pere = cStructureRacine
		WHERE lc_structure = 'CONTACTS';
		
	UPDATE GRHUM.STRUCTURE_ULR
		SET c_structure_pere = cStructureRacine
		WHERE lc_structure = 'ARCHIVES';

END;
/

---------------------V20140521.142455__DML_MAJ_TauxBonifications_MiTpsThera.sql---------------------
--------------------------------------------------
-- Taux de bonifications
--------------------------------------------------
update grhum.nature_bonif_taux set nbt_code = 'D' where nbt_ordre = 1;

update grhum.nature_bonif_taux set nbt_code = 'T' where nbt_ordre = 2;

update grhum.nature_bonif_territoire set nbot_code = '10120' where nbot_libelle = 'Algérie';

--------------------------------------------------
-- Mi Temps Thérapeutique ==> Temps Partiel Thérapeutique
--------------------------------------------------
update grhum.type_absence set lc_type_absence = 'Temps partiel thérapeutique', ll_type_absence = 'Temps partiel thérapeutique' where c_type_absence = 'MTT';

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.18.0',to_date('21/05/2014','DD/MM/YYYY'),sysdate,'MAJ Passage Echelon, Groupes, Taux Bonification et Mi-Tps Théra ');
COMMIT;
