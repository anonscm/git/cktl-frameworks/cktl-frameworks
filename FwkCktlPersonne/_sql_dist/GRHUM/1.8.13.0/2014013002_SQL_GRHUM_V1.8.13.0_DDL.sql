--
-- Patch DDL de GRHUM du 30/01/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.13.0
-- Date de publication : 30/01/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.12.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.13.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.13.0 a deja ete passe !');
    end if;

end;
/


-------------------------V20140130.105334__DDL_TROUVE_LC_STRUCTURE_PERE.sql-------------------------
CREATE OR REPLACE FUNCTION GRHUM.Trouve_lc_structure_pere(c_structure_fille VARCHAR2)
-- Procedure de recupÈration de la structure pere d'un service
RETURN VARCHAR2

IS
  lc_structure_pere structure_ulr.lc_structure%type;
BEGIN

SELECT sp.lc_structure INTO lc_structure_pere
FROM STRUCTURE_ULR s, STRUCTURE_ULR sp
WHERE c_structure_fille = s.c_structure
AND s.c_structure_pere = sp.c_structure ;


	RETURN lc_structure_pere;


END;
/
-----------------------------V20140130.105818__DDL_MAJ_SUR_LES_AGES.sql-----------------------------

/*****************************
Procedure de test de suppression d'une colonne
******************************/
create or replace PROCEDURE  grhum.adm_drop_column(ownerObject VARCHAR2,tableName VARCHAR2, columnName VARCHAR2)
/** Permet de supprimer une colonne sans qu'une erreur soit levÈe si la colonne n'existe pas */
IS
  flag integer;
  vsql varchar2(2000);
begin

 select count(*) into flag from all_tab_columns where owner = upper(ownerObject) and table_name = upper(tableName) and COLUMN_NAME = upper(columnName);
 if (flag = 0)
 then
  SYS.DBMS_OUTPUT.PUT_LINE('La colonne ' || ownerObject ||'.'||tableName ||'.'|| columnName || ' n''existe pas.');
 else
    vsql := 'ALTER TABLE '|| ownerObject ||'.'||tableName ||' drop column '|| columnName;
 
  execute immediate vsql;
  SYS.DBMS_OUTPUT.PUT_LINE('La colonne ' || ownerObject ||'.'||tableName ||'.'|| columnName || ' a ÈtÈ supprimÈe.');
 end if;

end;
/

/*****************************
Procedure de test de suppression d'une colonne
******************************/
create or replace PROCEDURE  grhum.adm_add_column(ownerObject VARCHAR2,tableName VARCHAR2, columnName VARCHAR2, typeObject VARCHAR2, defaultValue varchar2, nullable varchar2)
/** Permet d'ajouter une colonne dans une table si celle-ci n'existe pas deja. Aucune erreur n'est renvoyÈe si la colonne existe.
Usage :
grhum.adm_add_column('comptefi', 'bilan_actif', 'bp_str_id', 'number', null, 'not null');

grhum.adm_add_column('comptefi', 'bilan_actif', 'libelle', 'varchar2(30)', '''pas de libelle''', null);

*/
IS
  flag integer;
  vsql varchar2(2000);
begin

 select count(*) into flag from all_tab_columns where owner = upper(ownerObject) and table_name = upper(tableName) and COLUMN_NAME = upper(columnName);
 if (flag > 0)
 then
  SYS.DBMS_OUTPUT.PUT_LINE('La colonne ' || ownerObject ||'.'||tableName ||'.'|| columnName || ' existe deja.');
 else
    vsql := 'ALTER TABLE '|| ownerObject ||'.'||tableName ||' ADD ('|| columnName ||' ' || typeObject;
 
    if (defaultValue is not null) then
      vsql := vsql || ' default ' || defaultValue;
    end if;
    
    if (nullable is not null) then
      vsql := vsql || ' ' || nullable;
    end if;

    vsql := vsql || ')';
 
  execute immediate vsql;
  SYS.DBMS_OUTPUT.PUT_LINE('La colonne ' || ownerObject ||'.'||tableName ||'.'|| columnName || ' a ÈtÈ crÈÈe.');
 end if;

 
end;
/

/***************************

***************************/
CREATE OR REPLACE PROCEDURE GRHUM.adm_drop_object(ownerObject VARCHAR2,nameObject VARCHAR2,typeObject VARCHAR2)
IS
-- Procedure de suppression d'un objet Oracle sans message d'erreur sur l'existence de ce dernier
-- ownerObject : le proprietaire (user) de l'objet
-- nameObject : nom de l'objet a supprimer
-- typeObject : type de l'objet (TABLE, PROCEDURE, FUNCTION, etc.)
-- ex : DROP FUNCTION GRHUM.CONS_LOGIN

fullObject  VARCHAR2(100);

BEGIN
       
    SELECT owner||'.'||object_name INTO fullObject FROM all_objects WHERE owner = upper(ownerObject) AND object_name = upper(nameObject) AND object_type = upper(typeObject);
    EXECUTE IMMEDIATE 'DROP '||upper(typeObject)||' '||fullObject||'';
    EXCEPTION
        WHEN no_data_found THEN
    NULL;

END;
/

/*****************************
Procedure de test de suppression d'une contrainte
******************************/

CREATE OR REPLACE procedure GRHUM.adm_drop_constraint (ownerconstraint varchar2, tableconstraint varchar2, nameconstraint varchar2)
is
    -- Suppression d'une contrainte si elle existe
    
   flag   smallint;
begin
   flag := 0;

   select count (constraint_name)
   into   flag
   from   all_constraints
   where  upper (owner) = upper (ownerconstraint) and upper (table_name) = upper (tableconstraint) and upper (constraint_name) = upper (nameconstraint);

   if flag > 0 then
      execute immediate 'ALTER TABLE ' || ownerconstraint || '.' || tableconstraint || ' DROP CONSTRAINT ' || nameconstraint;
   end if;
end;
/

/*****************************
Procedure de test de suppression d'un index
******************************/

CREATE OR REPLACE procedure GRHUM.adm_drop_index (ownerindex varchar2, nameindex varchar2)
is
    -- Suppression d'un index si il existe
    
   flag   smallint;
begin
   flag := 0;

   select count (*)
   into   flag
   from   all_indexes
   where  upper (owner) = upper (ownerindex) and upper (index_name) = upper (nameindex);

   if flag > 0 then
      execute immediate 'DROP INDEX ' || ownerindex || '.' || nameindex;
   end if;
end;
/


/******************************

Motifs de conges sans traitement

*****************************/

declare 
cpt integer;
begin

select count(*) into cpt from grhum.motif_cg_stagiaire;

if (cpt = 0)
then
	Insert into GRHUM.MOTIF_CG_STAGIAIRE
	   (C_MOTIF_CG_STAGIAIRE, LL_MOTIF_CG_STAGIAIRE, D_CREATION, D_MODIFICATION)
	 Values
	   ('SO', 'Pour donner des soins au conjoint ou au partenaire', SYSDATE, SYSDATE);
	Insert into GRHUM.MOTIF_CG_STAGIAIRE
	   (C_MOTIF_CG_STAGIAIRE, LL_MOTIF_CG_STAGIAIRE, D_CREATION, D_MODIFICATION)
	 Values
	   ('EN', 'Pour Èlever un enfant de moins de huit ans ou pour donner des soins ‡ un enfant ‡ charge', SYSDATE, SYSDATE);
	Insert into GRHUM.MOTIF_CG_STAGIAIRE
	   (C_MOTIF_CG_STAGIAIRE, LL_MOTIF_CG_STAGIAIRE, D_CREATION, D_MODIFICATION)
	 Values
	   ('SU', 'Pour suivre son conjoint ou le partenaire',SYSDATE, SYSDATE);
	Insert into GRHUM.MOTIF_CG_STAGIAIRE
	   (C_MOTIF_CG_STAGIAIRE, LL_MOTIF_CG_STAGIAIRE, D_CREATION, D_MODIFICATION)
	 Values
	   ('SA', 'Pour raison de santÈ', SYSDATE, SYSDATE);
	Insert into GRHUM.MOTIF_CG_STAGIAIRE
	   (C_MOTIF_CG_STAGIAIRE, LL_MOTIF_CG_STAGIAIRE, D_CREATION, D_MODIFICATION)
	 Values
	   ('SN', 'Pour obligation de Service National', SYSDATE, SYSDATE);

end if;
	   
end;
/

/******************************

	Limites d'age

***************************/

declare 
begin 

	GRHUM.ADM_ADD_COLUMN('GRHUM', 'LIMITE_AGE', 'COMMENTAIRES', 'VARCHAR2(500)', NULL, NULL);

	GRHUM.ADM_ADD_COLUMN('GRHUM', 'LIMITE_AGE', 'TEM_VALIDE', 'VARCHAR2(1)', '''O''', 'NOT NULL');

end;
/

ALTER TABLE GRHUM.LIMITE_AGE MOVE TABLESPACE DATA_GRHUM;
ALTER INDEX GRHUM.PK_LIMITE_AGE REBUILD TABLESPACE INDX_GRHUM;


declare
cpt integer;
begin

	select count(*) into cpt from grhum.limite_age where c_limite_age = '6504';
	if (cpt = 0)
	then
		INSERT INTO GRHUM.LIMITE_AGE VALUES ('6504', '65 ans 4 mois', SYSDATE, SYSDATE, NULL, 'O');
	end if;

		select count(*) into cpt from grhum.limite_age where c_limite_age = '6509';
	if (cpt = 0)
	then
		INSERT INTO GRHUM.LIMITE_AGE VALUES ('6509', '65 ans 9 mois', SYSDATE, SYSDATE, NULL, 'O');
	end if;

	select count(*) into cpt from grhum.limite_age where c_limite_age = '6602';
	if (cpt = 0)
	then
		INSERT INTO GRHUM.LIMITE_AGE VALUES ('6602', '66 ans 2 mois', SYSDATE, SYSDATE, NULL, 'O');
	end if;

	select count(*) into cpt from grhum.limite_age where c_limite_age = '6607';
	if (cpt = 0)
	then
		INSERT INTO GRHUM.LIMITE_AGE VALUES ('6607', '66 ans 7 mois', SYSDATE, SYSDATE, NULL, 'O');
	end if;

	select count(*) into cpt from grhum.limite_age where c_limite_age = '6700';
	if (cpt = 0)
	then
		INSERT INTO GRHUM.LIMITE_AGE VALUES ('6700', '67 ans', SYSDATE, SYSDATE, NULL, 'O');
	end if;
	
	
	
end;
/

UPDATE GRHUM.LIMITE_AGE SET commentaires = 'Agents nés avant le 1er Juillet 1951' where c_limite_age = '6500';
UPDATE GRHUM.LIMITE_AGE SET commentaires = 'Agents nés entre le 01/07/1951 et le 31/12/1951' where c_limite_age = '6504';
UPDATE GRHUM.LIMITE_AGE SET commentaires = 'Agents nés en 1952' where c_limite_age = '6509';
UPDATE GRHUM.LIMITE_AGE SET commentaires = 'Agents nés en 1953' where c_limite_age = '6602';
UPDATE GRHUM.LIMITE_AGE SET commentaires = 'Agents nés en 1954' where c_limite_age = '6607';
UPDATE GRHUM.LIMITE_AGE SET commentaires = 'Agents nés aprËs 1954' where c_limite_age = '6700';

UPDATE GRHUM.LIMITE_AGE SET tem_valide = 'N' where c_limite_age not in ('6500', '6504', '6509', '6602', '6607', '6700');

-- Mise a jour automatique des limites d age non renseignees

declare

cursor c_personnels
is select i.no_individu, d_naissance, extract(year from d_naissance) from grhum.personnel_ulr p, grhum.individu_ulr i , grhum.v_personnel_actuel v
where p.no_dossier_pers = i.no_individu and i.d_naissance is not null and i.tem_valide = 'O' 
and extract(year from i.d_naissance) >= 1940 and p.c_limite_age is null
and p.no_dossier_pers = v.no_dossier_pers;

date_naissance date;
annee_naissance integer;
noindividu integer;
cpt integer;

begin
    -- Anonymisation des donnÈes de ENFANT
    OPEN c_personnels;
    LOOP
    FETCH c_personnels INTO noindividu, date_naissance, annee_naissance;
    EXIT WHEN c_personnels%NOTFOUND;

        if (date_naissance < to_date ('01/07/1951', 'dd/mm/yyyy') )
        then
               update grhum.personnel_ulr set c_limite_age = '6500' where no_dossier_pers = noindividu;
        end if;

        if (date_naissance < to_date ('31/12/1951', 'dd/mm/yyyy') and date_naissance > to_date ('01/07/1951', 'dd/mm/yyyy') )
        then
               update grhum.personnel_ulr set c_limite_age = '6504' where no_dossier_pers = noindividu;
        end if;

        if (annee_naissance = 1952 )
        then
               update grhum.personnel_ulr set c_limite_age = '6509' where no_dossier_pers = noindividu;
        end if;

        if (annee_naissance = 1953 )
        then
               update grhum.personnel_ulr set c_limite_age = '6602' where no_dossier_pers = noindividu;
        end if;

        if (annee_naissance = 1954 )
        then
               update grhum.personnel_ulr set c_limite_age = '6607' where no_dossier_pers = noindividu;
        end if;

        if (annee_naissance > 1954 )
        then
               update grhum.personnel_ulr set c_limite_age = '6700' where no_dossier_pers = noindividu;
        end if;


    END LOOP;
    CLOSE c_personnels;
    

end;
/

/****************************

	TYPE CONTRAT ==> Passage du c_type_contrat_trav de 2 a 6 caracteres

***************************/

ALTER TABLE GOYAVE.PRIME_POPULATION MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.ACTIVITES_TYPE_CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.COMPAT_CATEMP_TYPCTR MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.COMPAT_CTR_NAT_BUD MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.CONDITION_RECRUTEMENT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.GEST_CGMOD_CON MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.GEST_CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.INCLUSION_CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.PARAM_TYPE_CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE GRHUM.TYPE_CONTRAT_GRADES MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE IMPORT.CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE MANGUE.CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE MANGUE.CONTRAT_HEBERGES MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE MANGUE.CONTRAT_IMPRESSION MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE MANGUE.VISA_CGMOD_CON MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE MANGUE.VISA_CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);

ALTER TABLE JEFY_PAYE.PAYE_CONTRAT MODIFY  C_TYPE_CONTRAT_TRAV VARCHAR2(6);


/****************************

	ECHELONS ==> Passage du c_echelon de 2 a 5 caracteres

***************************/

ALTER TABLE GRHUM.ECHELON MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE GRHUM.ECHELON MODIFY C_ECHELON_PREC VARCHAR2(5);

ALTER TABLE GRHUM.ECHELON MODIFY C_ECHELON_SUIV VARCHAR2(5);

ALTER TABLE GRHUM.PASSAGE_ECHELON MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE GRHUM.PASSAGE_CHEVRON MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.CONTRAT_AVENANT MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.ELEMENT_CARRIERE MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.ARRIVEE MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_ECHELON_ARRIVEE VARCHAR2(5);


/****************************

	CORPS ==> Passage du c_corps de 4 a 10 caracteres

***************************/

ALTER TABLE GRHUM.CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.GRADE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.BAP_CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.CORPS_CATEG_EMPLOI MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.CORRESP_CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.CRITERES_LA MODIFY (C_CORPS_PROMOTION VARCHAR2(10));

ALTER TABLE GRHUM.CRIT_LA_CORPS_DEP MODIFY (C_CORPS_DEPART VARCHAR2(10));

ALTER TABLE GRHUM.HIERARCHIE_CORPS MODIFY (C_CORPS_SUIV VARCHAR2(10));

ALTER TABLE GRHUM.HIERARCHIE_CORPS MODIFY (C_CORPS_INIT VARCHAR2(10));

ALTER TABLE GRHUM.INCLUSION_CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.LISTe_APTITUDE MODIFY (C_CORPS_PROMOTION VARCHAR2(10));

ALTER TABLE IMPORT.ELEMENT_CARRIERE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.ARRIVEE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CONTRAT_HEBERGES MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CONTRAT_VACATAIRES MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CORPS_EMERITE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CORPS_PROMOUVABLE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.ELEMENT_CARRIERE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.LISTE_ELECTEUR MODIFY (C_CORPS VARCHAR2(10));

/****************************

	GRADE ==> Passage du c_grade de 4 a 10 caracteres

***************************/

ALTER TABLE GRHUM.GRADE MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.CRITERES_TA MODIFY C_GRADE_PROMOTION VARCHAR2(10);

ALTER TABLE GRHUM.CRIT_TA_GRADE_DEP MODIFY C_GRADE_DEPART VARCHAR2(10);

ALTER TABLE GRHUM.EQUIV_ANC_CORPS MODIFY C_GRADE_EQUIV VARCHAR2(10);

ALTER TABLE GRHUM.EQUIV_ANC_GRADE MODIFY C_GRADE_DEPART VARCHAR2(10);

ALTER TABLE GRHUM.EQUIV_ANC_GRADE MODIFY C_GRADE_EQUIV VARCHAR2(10);

ALTER TABLE GRHUM.HIERARCHIE_GRADE MODIFY C_GRADE_INIT VARCHAR2(10);

ALTER TABLE GRHUM.HIERARCHIE_GRADE MODIFY C_GRADE_SUIV VARCHAR2(10);

ALTER TABLE GRHUM.LIEN_GRADE_MEN_TG MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.PASSAGE_ECHELON MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.PASSAGE_CHEVRON MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.TABLEAU_AVANCEMENT MODIFY C_GRADE_PROMOTION VARCHAR2(10);

ALTER TABLE IMPORT.CONTRAT_AVENANT MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.ARRIVEE MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.CONTRAT_AVENANT MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.CONTRAT_HEBERGES MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.CONTRAT_VACATAIRES MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.ELEMENT_CARRIERE MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.LISTE_ELECTEUR MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.NOTES MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_GRADE_ARRIVEE VARCHAR2(10);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_GRADE_DEPART VARCHAR2(10);

ALTER TABLE GOYAVE.PRIME_PARAM MODIFY PPAR_GRADE VARCHAR2(10);

ALTER TABLE GOYAVE.PRIME_POPULATION MODIFY C_GRADE VARCHAR2(10);



/****************************

	ECHELONS ==> Passage du c_echelon de 2 a 5 caracteres

***************************/

ALTER TABLE GRHUM.ECHELON MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE GRHUM.ECHELON MODIFY C_ECHELON_PREC VARCHAR2(5);

ALTER TABLE GRHUM.ECHELON MODIFY C_ECHELON_SUIV VARCHAR2(5);

ALTER TABLE GRHUM.PASSAGE_ECHELON MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE GRHUM.PASSAGE_CHEVRON MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.CONTRAT_AVENANT MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.ELEMENT_CARRIERE MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.ARRIVEE MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_ECHELON VARCHAR2(5);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_ECHELON_ARRIVEE VARCHAR2(5);


/****************************

	CORPS ==> Passage du c_corps de 4 a 10 caracteres

***************************/

ALTER TABLE GRHUM.CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.GRADE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.BAP_CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.CORPS_CATEG_EMPLOI MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.CORRESP_CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.CRITERES_LA MODIFY (C_CORPS_PROMOTION VARCHAR2(10));

ALTER TABLE GRHUM.CRIT_LA_CORPS_DEP MODIFY (C_CORPS_DEPART VARCHAR2(10));

ALTER TABLE GRHUM.HIERARCHIE_CORPS MODIFY (C_CORPS_SUIV VARCHAR2(10));

ALTER TABLE GRHUM.HIERARCHIE_CORPS MODIFY (C_CORPS_INIT VARCHAR2(10));

ALTER TABLE GRHUM.INCLUSION_CORPS MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE GRHUM.LISTe_APTITUDE MODIFY (C_CORPS_PROMOTION VARCHAR2(10));

ALTER TABLE IMPORT.ELEMENT_CARRIERE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.ARRIVEE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CONTRAT_HEBERGES MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CONTRAT_VACATAIRES MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CORPS_EMERITE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.CORPS_PROMOUVABLE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.ELEMENT_CARRIERE MODIFY (C_CORPS VARCHAR2(10));

ALTER TABLE MANGUE.LISTE_ELECTEUR MODIFY (C_CORPS VARCHAR2(10));

/****************************

	GRADE ==> Passage du c_grade de 4 a 10 caracteres

***************************/

ALTER TABLE GRHUM.GRADE MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.CRITERES_TA MODIFY C_GRADE_PROMOTION VARCHAR2(10);

ALTER TABLE GRHUM.CRIT_TA_GRADE_DEP MODIFY C_GRADE_DEPART VARCHAR2(10);

ALTER TABLE GRHUM.EQUIV_ANC_CORPS MODIFY C_GRADE_EQUIV VARCHAR2(10);

ALTER TABLE GRHUM.EQUIV_ANC_GRADE MODIFY C_GRADE_DEPART VARCHAR2(10);

ALTER TABLE GRHUM.EQUIV_ANC_GRADE MODIFY C_GRADE_EQUIV VARCHAR2(10);

ALTER TABLE GRHUM.HIERARCHIE_GRADE MODIFY C_GRADE_INIT VARCHAR2(10);

ALTER TABLE GRHUM.HIERARCHIE_GRADE MODIFY C_GRADE_SUIV VARCHAR2(10);

ALTER TABLE GRHUM.LIEN_GRADE_MEN_TG MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.PASSAGE_ECHELON MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.PASSAGE_CHEVRON MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE GRHUM.TABLEAU_AVANCEMENT MODIFY C_GRADE_PROMOTION VARCHAR2(10);

ALTER TABLE IMPORT.CONTRAT_AVENANT MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.ARRIVEE MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.CONTRAT_AVENANT MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.CONTRAT_HEBERGES MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.CONTRAT_VACATAIRES MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.ELEMENT_CARRIERE MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.LISTE_ELECTEUR MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.NOTES MODIFY C_GRADE VARCHAR2(10);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_GRADE_ARRIVEE VARCHAR2(10);

ALTER TABLE MANGUE.PARAM_PROMOTION MODIFY C_GRADE_DEPART VARCHAR2(10);

ALTER TABLE GOYAVE.PRIME_PARAM MODIFY PPAR_GRADE VARCHAR2(10);

ALTER TABLE GOYAVE.PRIME_POPULATION MODIFY C_GRADE VARCHAR2(10);




