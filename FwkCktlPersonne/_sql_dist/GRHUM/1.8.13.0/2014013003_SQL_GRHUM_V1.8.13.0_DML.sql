--
-- Patch DML de GRHUM du 30/01/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.13.0
-- Date de publication : 30/01/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



---------------------------V20140130.111442__DML_MAJ_ADRESSES_DE_RNE.sql----------------------------

--------------------------------------------------------
--  RNE fourni par l'INSA de Rennes
--------------------------------------------------------

-- http://www.education.gouv.fr/annuaire/57-moselle/metz/lycee/lycee-fabert.html
update grhum.rne
set
	adresse = '12 rue Saint-Vincent',
	code_postal='57000',
	ville=upper('Metz')
where c_rne='0570054Z';


-- http://www.education.gouv.fr/annuaire/68-haut-rhin/mulhouse/lycee/lycee-albert-schweitzer.html
update grhum.rne
set
	adresse = '8 boulevard de la Marne BP 2269',
	code_postal='68100',
	ville=upper('Mulhouse')
where c_rne='0680031P';


-- http://www.education.gouv.fr/annuaire/54-meurthe-et-moselle/nancy/lycee/lycee-henri-loritz.html
update grhum.rne
set
	adresse = '29 rue des Jardiniers - BP 4218',
	code_postal='54000',
	ville=upper('Nancy')
where c_rne='0540042C';


-- http://www.education.gouv.fr/annuaire/06-alpes-maritimes/nice/lycee/lgt-honore-d-estienne-d-orves.html
update grhum.rne
set
	ll_rne='Lycée Honoré d''Estienne d''Orves',
	adresse = '13 avenue d''Estienne d''Orves',
	code_postal='06000',
	ville=upper('Nice')
where c_rne='0060033D';

-- http://www.education.gouv.fr/annuaire/06-alpes-maritimes/nice/lycee/lycee-des-sciences-appliquees-aux-metiers-de-l-industrie-les-eucalyptus.html
update grhum.rne
set
	adresse = '7 avenue des Eucalyptus',
	code_postal='06000',
	ville=upper('Nice')
where c_rne='0060075Z';


-- http://www.education.gouv.fr/annuaire/66-pyrenees-orientales/perpignan/lycee/lycee-francois-arago.html
update grhum.rne
set
	ll_rne='Lycée François Arago',
	adresse = '22 av. Président Doumer-BP 60119',
	code_postal='66000',
	ville=upper('Perpignan')
where c_rne='0660010C';


-- http://www.education.gouv.fr/annuaire/63-puy-de-dome/thiers/lycee/lycee-jean-zay.html
update grhum.rne
set
	ll_rne='Lycée Jean Zay',
	adresse = '21 rue Jean Zay',
	code_postal='63300',
	ville=upper('Thiers')
where c_rne='0630069H';

-- http://www.education.gouv.fr/annuaire/59-nord/valenciennes/lycee/lycee-henri-wallon.html
update grhum.rne
set
	adresse = '16 place de la République-BP 435',
	code_postal='59300',
	ville=upper('Valenciennes')
where c_rne='0590221V';

-- http://www.education.gouv.fr/annuaire/78-yvelines/versailles/lycee/lycee-jules-ferry.html
update grhum.rne
set
	ll_rne='Lycée Jules Ferry ',
	adresse = '29 rue du Maréchal Joffre',
	code_postal='78000',
	ville=upper('Versailles')
where c_rne='0782565P';


--------------------------V20140130.113956__DML_RATTACHEMENT_CORPS_AC.sql---------------------------
-- rattacher ces corps au type de population ATOS (comme dans la BCN)
update grhum.corps set c_type_corps = 'AC' where c_corps in ( '284','285','286');

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.13.0',to_date('30/01/2014','DD/MM/YYYY'),sysdate,'MAJ des limites d''âge, MAJ de RNE et de Corps');
COMMIT;
