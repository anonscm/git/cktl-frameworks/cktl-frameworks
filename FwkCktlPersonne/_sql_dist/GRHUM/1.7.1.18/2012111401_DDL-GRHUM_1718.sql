--
-- Patch DDL de GRHUM du 14/11/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.18
-- Date de publication : 14/11/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--
-- Ajout d'une procedure pour la creation des banques etrangeres.
-- Modification de l'index unique des banques sur code banque/code guichet/tem_local pour prendre en compte des banques étrangères.


WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.18', TO_DATE('14/11/2012', 'DD/MM/YYYY'),NULL,'Mise à jour de la table BANQUE à partir du fichier de novembre de la Banque de France + banques étrangères récupérées sur http://www.europeanpaymentscouncil.eu/');
commit;



CREATE OR REPLACE PROCEDURE GRHUM.MAJ_BANQUE_ETRANGERE (
 
   cdomiciliation   VARCHAR2,
   cbic             VARCHAR2,
   cadresse         VARCHAR2,
   cville           VARCHAR2,
   cpays            varchar2,
   douverture       DATE,
   dfermeture       DATE
)
IS
    adomicil grhum.banque.DOMICILIATION%type;
    aadress grhum.banque.ADRESSE%type;
    aville grhum.banque.VILLE%type;
   n                NUMBER;
   persIdCreateur   GRHUM.COMPTE.PERS_ID%TYPE;

BEGIN

   -- persIdCreation
   SELECT PERS_ID into persIdCreateur FROM COMPTE WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));

   SELECT COUNT (*) INTO n FROM grhum.banque WHERE bic = cbic AND c_guichet is null and c_banque is null and tem_local='N';
   
   -- remplacement des ' par des espaces 
   adomicil := replace(cdomiciliation, chr(39),chr(32));
   aadress := replace(cadresse, chr(39),chr(32));
   aville := replace(cville, chr(39),chr(32));
   
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.banque(c_banque, c_guichet, domiciliation, d_creation,d_modification, bic, banq_ordre, adresse, code_postal,ville, d_ouverture, d_fermeture, tem_local, pers_id_creation, pers_id_modification)
      SELECT null, null, trim(adomicil), SYSDATE, SYSDATE, cbic, grhum.banque_seq.NEXTVAL, trim(aadress), null,trim(aville), douverture, dfermeture, 'N', persIdCreateur, persIdCreateur FROM DUAL;
   ELSE
      -- mise à jour
      UPDATE grhum.banque SET domiciliation = trim(adomicil), d_modification = SYSDATE, bic = cbic, adresse = trim(aadress), ville = trim(aville), d_ouverture = douverture, d_fermeture = dfermeture, pers_id_creation = persIdCreateur, pers_id_modification = persIdCreateur 
      WHERE bic = cbic AND tem_local='N';
   END IF;
   
END;
/


-- changement de l'index unique pour autoriser les banques etrangeres en tem_local='N'
execute grhum.drop_object('GRHUM','UK_BANQ_GUICHET','index');
--drop index GRHUM.UK_BANQ_GUICHET;

CREATE UNIQUE INDEX GRHUM.UK_BANQ_GUICHET ON GRHUM.BANQUE
(C_BANQUE, C_GUICHET, DECODE(TEM_LOCAL||substr(bic,5,2),'NFR',"TEM_LOCAL",NULL))
LOGGING
TABLESPACE INDX_GRHUM;

