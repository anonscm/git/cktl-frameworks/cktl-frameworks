--
-- Patch DML de GRHUM du 16/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.0.0 
-- Date de publication : 16/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.41';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.0.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.0.0 a deja ete passe !');
    end if;

    select count(*) into cpt from grhum.association where ass_code = 'SECRÉTAIRE A';
    if cpt = 0 then
        raise_application_error(-20000,'L''association avec le code ''SECRÉTAIRE A'' n''est pas présente dans la table GRHUM.ASSOCIATION');
    end if;
end;
/


-- Renommage de secretariat
ALTER TABLE GRHUM.SECRETARIAT  RENAME TO SECRETARIAT_OLD;

-- Creation de la vue secretariat
CREATE OR REPLACE VIEW GRHUM.SECRETARIAT AS
    SELECT  ra.c_structure, i.no_individu, ra.d_creation, ra.d_modification from repart_association ra
      inner join individu_ulr i on ra.pers_id = i.pers_id
        inner join association a on ra.ass_id = a.ass_id
          where a.ass_code = 'SECRÉTAIRE A'
            and ra.ras_d_ouverture <= sysdate
            and NVL(ra.ras_d_fermeture,to_date('31122199','DDMMYYYY')) >= sysdate; 
/

-- Création des triggers associés a la vue 
CREATE OR REPLACE TRIGGER GRHUM.SECRETARIAT_INSERT
INSTEAD OF INSERT ON GRHUM.SECRETARIAT
REFERENCING NEW AS n               

FOR EACH ROW
DECLARE
   pers_id_createur number;
   ass_id_secretaire number;
   pers_id_secretaire number;
   cpt number;
BEGIN

-- CONTROLE DES PARAMETRES

if(:n.c_structure is NULL) then
  RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : c_structure ne peut pas etre null');
end if;

if(:n.no_individu is NULL) then
  RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : no_individu ne peut pas etre null');
end if;

if(:n.d_creation is NULL) then
  RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : d_creation ne peut pas etre null');
end if;

if(:n.d_modification is NULL) then
  RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : d_modification ne peut pas etre null');
end if;

-- VERIFICATION DE LA VALIDITE DU C_STRUCTURE
select count(*) into cpt from structure_ulr where c_structure = :n.c_structure;
if(cpt=0) then
  RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : aucune structure correspondante au code fourni');
end if;

-- Recup du pers_id de GRHUM_CREATEUR
SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM COMPTE WHERE CPT_LOGIN IN 
  (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');

-- RECUP DE L'ASS_ID SECRETAIRE
select min(ass_id) into ass_id_secretaire from association where ass_code = 'SECRÉTAIRE A';

-- RECUP DU PERS_ID DU SECRETAIRE
select count(*) into cpt from individu_ulr where no_individu = :n.no_individu;
if(cpt=0) then
  RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : aucun individu correspondant au numero fourni');
else
  select pers_id into pers_id_secretaire from individu_ulr where no_individu = :n.no_individu;
end if;

-- ON VERIFIE QUE L'ENREGISTREMENT N'EXISTE PAS DEJA
SELECT count(*) into cpt from repart_association
  where ass_id = ass_id_secretaire
    and c_structure = :n.c_structure
    and pers_id = pers_id_secretaire
    and ras_d_ouverture <= sysdate
    and (
      ras_d_fermeture is null
      or ras_d_fermeture >= sysdate
    ); 

if(cpt = 0) then 
  -- RAISE_APPLICATION_ERROR(-20000,'SECRETARIAT : l''enregistrement existe deja');
  INSERT INTO REPART_ASSOCIATION
  (
    RAS_ID,
    PERS_ID,
    C_STRUCTURE,
    ASS_ID,
    RAS_RANG,
    RAS_COMMENTAIRE,
    RAS_D_OUVERTURE,
    RAS_D_FERMETURE,
    D_CREATION,
    D_MODIFICATION,
    RAS_QUOTITE,
    PERS_ID_CREATION,
    PERS_ID_MODIFICATION
  )
  VALUES
  (
    repart_association_seq.nextval,
    pers_id_secretaire,
    :n.c_structure,
    ass_id_secretaire,
    1,
    null,
    sysdate,
    null,
    :n.d_creation,
    :n.d_modification,
    null,
    pers_id_createur,
    pers_id_createur
  );
end if;




END;
/

CREATE OR REPLACE TRIGGER GRHUM.SECRETARIAT_UPDATE
INSTEAD OF UPDATE ON GRHUM.SECRETARIAT
REFERENCING NEW AS n old as o
BEGIN
  -- on fait rien car on ne fait jamais d'update sur cette table
  null;
END;
/

CREATE OR REPLACE TRIGGER GRHUM.SECRETARIAT_DELETE
INSTEAD OF DELETE ON GRHUM.SECRETARIAT
REFERENCING old as o
FOR EACH ROW
DECLARE
   pers_id_createur number;
   ass_id_secretaire number;
   pers_id_secretaire number;
   cpt number;
BEGIN

  -- Recup du pers_id de GRHUM_CREATEUR
  SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM COMPTE WHERE CPT_LOGIN IN 
    (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');
  
  -- RECUP DE L'ASS_ID SECRETAIRE
  select ass_id into ass_id_secretaire from association where ass_code = 'SECRÉTAIRE A';
  
  -- RECUP DU PERS_ID DU SECRETAIRE
  select pers_id into pers_id_secretaire from individu_ulr where no_individu = :o.no_individu;
  
  update repart_association set ras_d_fermeture = sysdate
  where ass_id = ass_id_secretaire
    and c_structure = :o.c_structure
    and pers_id = pers_id_secretaire
    and ras_d_ouverture <= sysdate
    and (
      ras_d_fermeture is null
      or ras_d_fermeture >= sysdate
    ); 

END;
/

-- REMISE EN PLACE DES GRANTS                                                                                                 
GRANT REFERENCES ON GRHUM.SECRETARIAT TO ACCORDS;                                                                                                  
GRANT UPDATE ON GRHUM.SECRETARIAT TO MANGUE;                                                                                                       
GRANT SELECT ON GRHUM.SECRETARIAT TO FORUMS;                                                                                                       
GRANT SELECT ON GRHUM.SECRETARIAT TO DT3;                                                                                                          
GRANT SELECT ON GRHUM.SECRETARIAT TO MANGUE;                                                                                                       
GRANT SELECT ON GRHUM.SECRETARIAT TO JEFY_RECETTE;                                                                                                 
GRANT SELECT ON GRHUM.SECRETARIAT TO ACCORDS;                                                                                                      
GRANT INSERT ON GRHUM.SECRETARIAT TO MANGUE;                                                                                                       

-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.8.0.0', TO_DATE('16/07/2013', 'DD/MM/YYYY'),NULL,'Transformation en vue de la table GRHUM.SECRETARIAT ');

--
-- 
--


COMMIT;
