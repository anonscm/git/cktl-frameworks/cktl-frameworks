--
-- Patch DML de GRHUM du 16/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.0.0
-- Date de publication : 16/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- Migration des données
INSERT INTO grhum.secretariat (c_structure, no_individu, d_creation, d_modification)
    SELECT * FROM grhum.secretariat_old;


--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.8.0.0';

COMMIT;
