--
-- Patch DML de GRHUM du 26/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.1.1
-- Date de publication : 26/07/2013
-- Auteur(s) : ldrouot

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------------V20130726.110309__DML_gestionDesDroits.sql-----------------------------
declare
  is_appIdGirofle_exists int;
  appIdGirofle int;
  domaineId int;
begin
  SELECT count(*) into is_appIdGirofle_exists from GRHUM.GD_APPLICATION WHERE APP_STR_ID='GIROFLE';

  --Suppression des fonctions associees a Girofle si il y en a
  IF(is_appIdGirofle_exists>0) then
	SELECT APP_ID into appIdGirofle from GRHUM.GD_APPLICATION WHERE APP_STR_ID='GIROFLE';
  
  	DELETE FROM GRHUM.GD_FONCTION WHERE APP_ID=appIdGirofle;
  end if;
  
  SELECT DOM_ID INTO domaineId FROM GRHUM.GD_DOMAINE WHERE DOM_LC='SCOLARITE';
  DELETE FROM GRHUM.GD_APPLICATION WHERE APP_STR_ID='GIROFLE';
  -- insertion de l'application Girofle
  INSERT INTO GRHUM.GD_APPLICATION(APP_ID, DOM_ID, APP_LC, APP_STR_ID) VALUES(GRHUM.GD_APPLICATION_seq.nextval,domaineId,'GIROFLE', 'GIROFLE');

  
  -- insertion des differents droits (les fonctions)
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Girofle','GIROFLE','Accès a Girofle','Accès a Girofle');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Maquettage','MAQUETTAGE_DIPLOME','Accès aux Diplômes','Accès aux Diplômes');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Maquettage','MAQUETTAGE_RGPT','Accès aux Regroupements','Accès aux Regroupements');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Maquettage','MAQUETTAGE_UE','Accès aux UEs','Accès aux UEs');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Maquettage','MAQUETTAGE_EC','Accès aux ECs','Accès aux ECs');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Maquettage','MAQUETTAGE_TYPE_AP','Accès aux types d''APs','Accès aux types d''APs');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Maquettage','MAQUETTAGE_TYPE_AE','Accès aux types d''AEs','Accès aux types d''AEs');

  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Maquettage','MAQUETTAGE_MAQUETTE','Accès à la Maquette de formation','Accès à la Maquette de formation');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Modélisation','MODELISATION_MODEL','Accès à la modélisation d''un diplôme','Accès à la modélisation d''un diplôme');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Modélisation','MODELISATION_SYL_DIP','Accès au syllabus d''un diplôme','Accès au syllabus d''un diplôme');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Modélisation','MODELISATION_SYL_CMP','Accès au syllabus d''un composant','Accès au syllabus d''un composant');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Administration','ADMIN_AUTORISER_ASSO','Autoriser l''association de composants','Accès à l''administration GIROFLE - Autoriser l''association de composants');
	
  INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
  VALUES(grhum.GD_FONCTION_SEQ.NEXTVAL,GRHUM.GD_APPLICATION_seq.CURRVAL,'Administration','ADMIN_INTERDIRE_ASSO','Interdire  l''association de composants','Accès à l''administration GIROFLE - Interdire  l''association de composants');
end;
/

COMMIT;

----------------------------V20130726.110309__DML_gestionDesDroits.sql~-----------------------------


--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.1.1',to_date('26/07/2013','DD/MM/YYYY'),sysdate,'Gestion des droits');
COMMIT;
