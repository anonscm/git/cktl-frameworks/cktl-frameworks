--
-- Patch DDL de GRHUM du 26/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.1.1
-- Date de publication : 26/07/2013
-- Auteur(s) : ldrouot

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.1.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.1.1';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.1.1 a deja ete passe !');
    end if;

end;
/

-----------------------------V20130726.105952__DDL_gestionDesDroits.sql-----------------------------

-- Test pour vérifier que la table GD_PERIMETRE n'existe pas déjà
declare 
    is_table_exists int;
    is_seq_exists int;
begin
    select count(*) into is_table_exists from user_tables where table_name = upper('GD_PERIMETRE');

        if (is_table_exists <> 0) then
      execute immediate('DROP TABLE GRHUM.GD_PERIMETRE CASCADE CONSTRAINTS');
   end if;
   
   select count(*) into is_seq_exists from all_sequences where sequence_name= 'GD_PERIMETRE_SEQ';
        if (is_seq_exists = 1) then
        execute immediate('DROP SEQUENCE GRHUM.GD_PERIMETRE_SEQ');
    end if;
end;
/   

--Création de la table GD_PERIMETRE
CREATE TABLE GRHUM.GD_PERIMETRE (
    ID_GD_PERIMETRE NUMBER(38) NOT NULL,
    APP_ID NUMBER(12) NOT NULL,
    TDD_ID NUMBER(2) DEFAULT 0,
    PERIMETRE_PARENT_ID NUMBER(38),
    LIBELLE VARCHAR2(255) NOT NULL,
    D_CREATION DATE NOT NULL, 
	D_MODIFICATION DATE, 
  	PERS_ID_CREATION NUMBER NOT NULL, 
	PERS_ID_MODIFICATION NUMBER,
    CONSTRAINT PK_ID_GD_PERIMETRE PRIMARY KEY (ID_GD_PERIMETRE) USING INDEX TABLESPACE INDX_GRHUM,
    CONSTRAINT FK_GD_PERI_APP_ID FOREIGN KEY (APP_ID) REFERENCES GRHUM.GD_APPLICATION (APP_ID) DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT FK_GD_PERI_TDD_ID FOREIGN KEY (TDD_ID) REFERENCES GRHUM.GD_TYPE_DROIT_DONNEE (TDD_ID) DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT FK_GD_PERI_PARENT_ID FOREIGN KEY (PERIMETRE_PARENT_ID) REFERENCES GRHUM.GD_PERIMETRE (ID_GD_PERIMETRE) DEFERRABLE INITIALLY DEFERRED
);
CREATE SEQUENCE GRHUM.GD_PERIMETRE_SEQ START WITH 1 NOCACHE;
COMMENT ON TABLE GRHUM.GD_PERIMETRE IS 'Table des périmètres de données pour les droits';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.APP_ID IS 'Référence a l''application dont dépend ce périmètre de données';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.TDD_ID IS 'Identifiant du type de droits sur le périmètre de données';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.PERIMETRE_PARENT_ID IS 'Identifiant du périmètre parent ';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.LIBELLE IS 'Libellé du périmètre de données';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.D_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.PERS_ID_CREATION IS 'Pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRHUM.GD_PERIMETRE.PERS_ID_MODIFICATION IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';

-- Test pour vérifier que la table GD_PERIMETRE_PROFIL n'existe pas déjà
declare 
    is_table_exists int;
begin
    select count(*) into is_table_exists from user_tables where table_name = upper('GD_PROFIL_PERIMETRE');

        if (is_table_exists <> 0) then
      execute immediate('DROP TABLE GRHUM.GD_PROFIL_PERIMETRE CASCADE CONSTRAINTS');
   end if;
   
end;
/
   
CREATE TABLE GRHUM.GD_PROFIL_PERIMETRE(
    PROFIL_ID NUMBER(38) NOT NULL,
    PERIMETRE_ID NUMBER(38) NOT NULL,
    CONSTRAINT PK_PROFIL_PERIMETRE PRIMARY KEY (PROFIL_ID,  PERIMETRE_ID) USING INDEX TABLESPACE INDX_GRHUM,
    CONSTRAINT FK_PROFIL_PERIMETRE_PROFIL FOREIGN KEY (PROFIL_ID) REFERENCES GRHUM.GD_PROFIL (PR_ID) DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT FK_PROFIL_PERIMETRE_PERIM  FOREIGN KEY (PERIMETRE_ID) REFERENCES GRHUM.GD_PERIMETRE (ID_GD_PERIMETRE) DEFERRABLE INITIALLY DEFERRED
);
COMMENT ON TABLE GRHUM.GD_PROFIL_PERIMETRE IS 'Table de jointure entre un profil et un périmètre';
COMMENT ON COLUMN GRHUM.GD_PROFIL_PERIMETRE.PROFIL_ID IS 'Profil (lien avec la table GD_PROFIL)';
COMMENT ON COLUMN GRHUM.GD_PROFIL_PERIMETRE.PERIMETRE_ID IS 'Perimetre (lien avec la table GD_PERIMETRE)';


----------------------------V20130726.105952__DDL_gestionDesDroits.sql~-----------------------------



