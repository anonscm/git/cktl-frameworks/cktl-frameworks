--
-- Patch DML de GRHUM du 11/07/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.21.0
-- Date de publication : 11/07/2014
-- Auteur(s) : Cocktail

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------------V20140711.102858__DML_AJOUT_PARAMETRES.sql-----------------------------
--Insertion du parametre definissant si l'on doit fonctionner comment auparavant ou non pour les dates de début de validité des comptes
declare 
    persIdCreateur  integer;
    nomParametre	VARCHAR2(512);   
    itemCount		integer;
    valueParamId	integer; 
begin
	
	nomParametre := 'org.cocktail.grhum.comptes.switchdanstriggerrepartcompte';
	select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where PARAM_KEY = nomParametre;
    if (itemCount = 1) then                                           
      delete from GRHUM.GRHUM_PARAMETRES where PARAM_KEY = nomParametre;
    end if;
    
    nomParametre := 'org.cocktail.grhum.comptes.tenircomptedesdatesdanstriggerrepartcompte';
    select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where PARAM_KEY = nomParametre;
    if (itemCount = 1) then                                           
      delete from GRHUM.GRHUM_PARAMETRES where PARAM_KEY = nomParametre;
    end if;
	
	commit;


	-- Ajout du paramètre pour activer ou désactiver le contrôle des dates de validité

    -- persIdCreation
    SELECT PERS_ID into persIdCreateur
               FROM GRHUM.COMPTE
        WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES
                                                WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));
    -- paramTypeId
    SELECT TYPE_ID into valueParamId
               FROM GRHUM.GRHUM_PARAMETRES_TYPE
        WHERE TYPE_ID_INTERNE = 'CODE_ACTIVATION';                                            
    
    nomParametre := 'org.cocktail.grhum.comptes.tenircomptedesdatesdanstriggerrepartcompte';

    select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where  PARAM_KEY = nomParametre;
    if itemCount =0 then                                           
      INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) 
        values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,nomParametre,'O','Switch pour prendre en compte les dates de validité (Défaut à O)',persIdCreateur,persIdCreateur,sysdate,sysdate,valueParamId);
    end if;
    
    commit;
    
    
    -- Ajout d'un paramètre pour la valeur par défaut du GID des comptes étudiants
    
     -- paramTypeId
    SELECT TYPE_ID into valueParamId
               FROM GRHUM.GRHUM_PARAMETRES_TYPE
        WHERE TYPE_ID_INTERNE = 'VALEUR_NUMERIQUE';                                            
    
    nomParametre := 'org.cocktail.grhum.comptes.default_GID_etudiant';

    select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where  PARAM_KEY = nomParametre;
    if itemCount =0 then                                           
      INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) 
        values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,nomParametre,2001,'Valeur par défaut du GID des comptes étudiant (Défaut à 2001)',persIdCreateur,persIdCreateur,sysdate,sysdate,valueParamId);
    end if;
    
    commit;
    
    
end;
/

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.21.0',to_date('11/07/2014','DD/MM/YYYY'),sysdate,'MAJ du TRG_REPART_COMPTE avec paramètres');
COMMIT;
