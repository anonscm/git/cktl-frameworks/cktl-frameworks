--
-- Patch DDL de GRHUM du 11/07/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.21.0
-- Date de publication : 11/07/2014
-- Auteur(s) : Cocktail

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



---------------------------V20140711.102747__DDL_MAJ_STRUCTURE_ET_BAC.sql---------------------------
-- Reconstruction des indexes
ALTER TABLE GRHUM.STRUCTURE_ULR MOVE;
ALTER INDEX GRHUM.PK_STRUCTURE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.UK_RNE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.UK_STR_PERS_ID REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_STR_PERE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_GRP_OWNER REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_STR_GRP_ACCES REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_STR_GRP_ALIAS REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.INDX_STRUCT_VALIDE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_TYPE_STRUCTURE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_ACADEMIE_STRUCT REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_STRUCT_JURIDIQUE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_STRUCT_TYPE_ETAB REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_STR_GRP_RESPONSABLE REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.IDX_AS_STR_TYPE_DECISION REBUILD TABLESPACE INDX_GRHUM;


ALTER TABLE GRHUM.BAC MODIFY (BAC_CODE VARCHAR2(5));
ALTER TABLE GRHUM.BAC MODIFY (BAC_CODE_SISE VARCHAR2(5));

ALTER TABLE GRHUM.BAC MOVE;
ALTER INDEX GRHUM.PK_BAC REBUILD TABLESPACE INDX_GRHUM;
ALTER INDEX GRHUM.BAC_DAT REBUILD TABLESPACE INDX_GRHUM;

-- Rectification d'un commentaire sur la table GRHUM.BAC
COMMENT ON COLUMN GRHUM.BAC.BAC_CODE IS 'Code du BAC';
COMMENT ON COLUMN GRHUM.BAC.BAC_LIBELLE IS 'Libellé long du BAC';
COMMENT ON COLUMN GRHUM.BAC.BAC_TYPE IS 'Type de BAC (BAC, PRO ou EQV)';
COMMENT ON COLUMN GRHUM.BAC.BAC_CODE_SISE IS 'Code SISE du BAC';
COMMENT ON COLUMN GRHUM.BAC.BAC_DATE_VALIDITE IS 'Date d''ouverture de ce Bac';
COMMENT ON COLUMN GRHUM.BAC.D_CREATION IS 'Date de creation de l''enregistrement.';
COMMENT ON COLUMN GRHUM.BAC.D_MODIFICATION IS 'Date de la dernière modification de l''enregistrement.';
COMMENT ON COLUMN GRHUM.BAC.BAC_VALIDITE IS 'Témoin de validité.';
COMMENT ON COLUMN GRHUM.BAC.BAC_DATE_VALIDITE IS 'Date de fermeture de ce Bac';
COMMENT ON COLUMN GRHUM.BAC.BAC_LIB_COURT IS 'Libellé court du BAC';
COMMENT ON TABLE GRHUM.BAC  IS 'Nomenclature des bacs.';

--------------------------V20140711.102816__DDL_MAJ_TRG_REPART_COMPTE.sql---------------------------
create or replace TRIGGER GRHUM.TRG_REPART_COMPTE
AFTER INSERT OR UPDATE OR DELETE
ON GRHUM.REPART_COMPTE
-- auteur : Cocktail
-- creation : 28/06/2004
-- modification : 04/07/2013
-- modification : 10/07/2014
-- Paramétrage de la période scolaire pour les dates de début et fin de validité du compte
-- Prise en compte de la table PASSWORD_HISTORY (uniquement pour les VLAN Administration et Recherche)
-- Trigger de mise à jour de la table COMPTE (anciennement COMPTE_NEW) par rapport à la table REPART_COMPTE de manière à récupérer le pers_id du compte

REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

    nbr             INTEGER;
    date_actuelle   DATE;
    crypte          COMPTE.CPT_CRYPTE%TYPE;
    persid          COMPTE.PERS_ID%TYPE;    
    persidCRI       COMPTE.CPT_CREATEUR%TYPE;
    login           COMPTE.CPT_LOGIN%TYPE;
    passwd          COMPTE.CPT_PASSWD%TYPE;
    passwdCrypte    COMPTE.CPT_PASSWD%TYPE;
    passwdClair     COMPTE.CPT_PASSWD_CLAIR%TYPE;       
    vlan            COMPTE.CPT_VLAN%TYPE; 
    charte          COMPTE.CPT_CHARTE%TYPE;
    home            COMPTE.CPT_HOME%TYPE;
    shell           COMPTE.CPT_SHELL%TYPE;
    connexion       COMPTE.CPT_CONNEXION%TYPE;
    valide          COMPTE.CPT_VALIDE%TYPE;
    debutValide     COMPTE.CPT_DEBUT_VALIDE%TYPE;
    finValide       COMPTE.CPT_FIN_VALIDE%TYPE;
    uidGid          COMPTE.CPT_UID_GID%TYPE;
    uidNew          COMPTE.CPT_UID%TYPE;
    gidNew          COMPTE.CPT_GID%TYPE;
    tvpnCode        COMPTE.TVPN_CODE%TYPE;
    tcryOrdre       COMPTE.TCRY_ORDRE%TYPE;
    cemKey          COMPTE_EMAIL.CEM_KEY%TYPE;
    email           COMPTE_EMAIL.CEM_EMAIL%TYPE;
    domaine         COMPTE_EMAIL.CEM_DOMAINE%TYPE;
    pwdhOrdre       GRHUM.PASSWORD_HISTORY.PWDH_ORDRE%TYPE;
    lmpasswd        VARCHAR2(32);
    ntpasswd        VARCHAR2(32);
    flag            VARCHAR2(18);
    cpt             integer;
    usePeda         GRHUM.GRHUM_PARAMETRES.PARAM_VALUE%TYPE;
    anneeScol       NUMBER;
    debutAnneeScol  GRHUM.COMPTE.CPT_DEBUT_VALIDE%TYPE;
    finAnneeScol    GRHUM.COMPTE.CPT_FIN_VALIDE%TYPE;
    paramSwitch		GRHUM.GRHUM_PARAMETRES.PARAM_VALUE%TYPE;
    paramDefaultGid NUMBER;
                       
BEGIN

     -- Initialisation des variables
     crypte := '';
     
     -- Type de cryptage par défaut : UNIX
     tcryOrdre := 1;
     
     -- Période scolaire des Inscriptions Administratives
     anneeScol := 2000;
     -- test si Connexion avec la Scolarité
     usePeda := 'NON';
     SELECT COUNT(*) INTO cpt FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_PEDA';
     IF (cpt != 0) THEN
        SELECT PARAM_VALUE INTO usePeda FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_PEDA';
        IF (usePeda = 'OUI') THEN
            -- recuperation de l'année scolaire de référence pour les Inscriptions Administratives
            SELECT TO_NUMBER(param_value) INTO anneeScol FROM GARNUCHE.GARNUCHE_PARAMETRES WHERE param_key='GARNUCHE_ANNEE_REFERENCE';
        END IF;
     END IF;     
     debutAnneeScol := to_date('01/07/'||to_char(anneeScol),'dd/mm/yyyy');
     finAnneeScol := to_date('30/09/'||to_char(anneeScol+1),'dd/mm/yyyy');
     
     -- Recup du pers_id de GRHUM_CREATEUR     
     select  count(*) into cpt 
     FROM COMPTE WHERE CPT_LOGIN =
        (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));
                
    if (cpt != 1)
    then
        raise_application_error(-20001, '0 ou plusieurs comptes définis pour le GRHUM_CREATEUR !');
    end if;                
                
     SELECT PERS_ID INTO persidCRI FROM COMPTE WHERE CPT_LOGIN =
        (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));    
	 
-- ************************************************************************************************************************************
 -- ************************************************************************************************************************************

	 -- paramètre de Switch des dates de validité
	 SELECT PARAM_VALUE into paramSwitch
               		FROM GRHUM_PARAMETRES
        		WHERE PARAM_KEY='org.cocktail.grhum.comptes.tenircomptedesdatesdanstriggerrepartcompte';
        		
    IF (paramSwitch is null) THEN
    		paramSwitch := 'O';
    END IF;




	 -- paramètre de Switch 
	 SELECT PARAM_VALUE into paramDefaultGid
               		FROM GRHUM_PARAMETRES
        		WHERE PARAM_KEY='org.cocktail.grhum.comptes.default_GID_etudiant';
        		
    IF (paramDefaultGid is null) THEN
    		paramSwitch := 2001;
    END IF;

-- ************************************************************************************************************************************
-- ************************************************************************************************************************************
	 
     -- Sysdate
     SELECT TO_DATE(TO_CHAR(SYSDATE,'ddmmyyyy'),'ddmmyyyy') INTO date_actuelle FROM dual;
    
     --- INSERTION sur REPART_COMPTE
     IF INSERTING THEN

         -- Récupération des données de COMPTE
         SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
            SELECT cpt_login,cpt_passwd,cpt_passwd_clair,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
            INTO   login,passwd,passwdClair,vlan,charte,connexion,uidGid,email,domaine
            FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;         
         -- Récupération des données de COMPTE_OLD
--         SELECT COUNT(*) INTO nbr FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--         IF (nbr != 0) THEN
--            SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
--            INTO   login,passwd,vlan,charte,connexion,uidGid,email,domaine
--            FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--            IF (uidGid = 0) THEN uidGid := NULL; END IF;
--         END IF;
         -- Récupération des données de COMPTE_SUITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT    cpt_home,cpt_shell,cpt_gid INTO home,shell,gidNew FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
         -- Récupération des données de COMPTE_VALIDITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN         
             SELECT TO_DATE(date_debut,'dd/mm/yyyy'),TO_DATE(date_fin,'dd/mm/yyyy') INTO debutValide,finValide FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
        
          -- Password crypte en fonction de la taille du champ password
          -- les mots de passe cryptés sont constitués de 13 caractères.
          -- Tous les password sont cryptés sauf ceux qui ont une *
          crypte := 'O';
          passwdClair := NULL;
          passwdCrypte := NULL;

          IF (LENGTH(passwd) >= 13) THEN
                    passwdClair := NULL;
                    passwdCrypte := passwd;
          ELSE
                    passwdClair := passwd;
                    IF (passwd = '*') THEN
                        passwdCrypte := passwd;
                    ELSE
                        passwdCrypte := GRHUM.Crypt(passwd);
                    END IF;
          END IF;
                  
          --RAISE_APPLICATION_ERROR(-20000,'TRG_REPART_COMPTE : passwdClair = '||passwdClair||' passwdCrypte = '||passwdCrypte);                      
 
 
 -- ************************************************************************************************************************************
 -- ************************************************************************************************************************************

 -- on récupère la valeur de la table COMPTE
        select cpt_valide into valide from grhum.compte where cpt_ordre = :NEW.cpt_ordre;
        
        IF ( paramSwitch = 'O' ) -- on tient compte des dates pour savoir si le compte est valide
        THEN
          
        	IF (debutValide IS NOT NULL) THEN
            	   --pour que le compte soit valide, la date actuelle doit etre comprise entre debut et fin de validite
             	  IF ( (debutValide <= date_actuelle) AND (finValide >= date_actuelle OR finValide IS NULL) ) THEN
             	          valide := 'O';
             	  ELSE
             	          valide := 'N';
             	  END IF;
        	ELSE
               	 valide := 'O';
               	 IF (vlan = 'E') THEN
               	     debutValide := debutAnneeScol;
               	     finValide := finAnneeScol;
               	  END IF;
        	END IF;
        END IF;


 -- ************************************************************************************************************************************
 -- ************************************************************************************************************************************



        -- recupération du pers_id
        persId := NULL;
        IF (:NEW.pers_id IS NULL) THEN        
            SELECT COUNT(*) INTO nbr FROM GRHUM.REPART_COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (nbr !=0) THEN
                SELECT pers_Id INTO persId FROM GRHUM.REPART_COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            END IF;
        ELSE
            persId := :NEW.pers_id;        
        END IF;            
        
         -- COMPTE
        SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:NEW.cpt_ordre;    
        IF (nbr = 0) THEN
           -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID
           IF (vlan='D' OR vlan='G') THEN
                      INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,
                                               cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,cpt_valide,cpt_debut_valide,cpt_fin_valide,
                                               cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,
                                               cpt_uid_gid,tvpn_code,tcry_ordre)
                      VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,NULL,uidGid,
                             home,shell,connexion,vlan,charte,valide,debutValide,finValide,
                             'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,'NO',tcryOrdre);           
           ELSE
                   IF (vlan='P' OR vlan='R' OR vlan='E') THEN
                         tvpnCode := 'W3VPN';
                   ELSE
                         tvpnCode := 'NO';
                   END IF;
                   -- Pour les comptes Etudiants le GID par défaut est de 2001
                   IF (vlan='E') THEN
                      gidNew := 2001;
                   END IF;                                      
                   INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,
                                            cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,cpt_valide,cpt_debut_valide,cpt_fin_valide,
                                            cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                   VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,uidGid,gidNew,
                          home,shell,connexion,vlan,charte,valide,debutValide,finValide,
                          'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);
           END IF;
        ELSE
                   IF (vlan='P' OR vlan='R' OR vlan='E') THEN
                        tvpnCode := 'W3VPN';
                   ELSE
                        tvpnCode := 'NO';
                   END IF;
                   -- Pour les comptes Etudiants le GID par défaut est de 2001
                   IF (vlan='E') THEN
                      gidNew := 2001;
                   END IF;                                      

                   UPDATE GRHUM.COMPTE
                   SET pers_id=persId, cpt_login=login, cpt_passwd=passwdCrypte, cpt_passwd_clair=passwdClair, cpt_crypte=crypte , cpt_uid=uidGid, cpt_gid=gidNew,
                       cpt_home=home, cpt_shell=shell, cpt_connexion=connexion, cpt_vlan=vlan, cpt_charte=charte, cpt_valide=valide, cpt_debut_valide=debutValide, cpt_fin_valide=finValide,
                       cpt_principal='O',cpt_liste_rouge='N',d_creation=:NEW.d_creation,cpt_createur=persidCRI,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid=uidGid,tvpn_code=tvpnCode, tcry_ordre=tcryOrdre
                   WHERE cpt_ordre = :NEW.cpt_ordre;
                  
        END IF;
    
         -- INSERTION de l'email en fonction du VLAN
       IF (email IS NOT NULL) THEN
                  SELECT COUNT(*) INTO nbr FROM COMPTE_EMAIL WHERE cpt_ordre=:NEW.cpt_ordre;    
               IF (nbr = 0) THEN
                     SELECT compte_email_seq.NEXTVAL INTO cemKey FROM dual;
                     INSERT INTO COMPTE_EMAIL(CEM_KEY, CPT_ORDRE, CEM_EMAIL, CEM_DOMAINE, CEM_PRIORITE, CEM_ALIAS, D_CREATION, D_MODIFICATION)
                     VALUES(cemKey,:NEW.cpt_ordre,email,domaine,NULL,'N',SYSDATE,SYSDATE);
               END IF;
       END IF;
       
       -- Configuration SAMBA en ajout
--      SELECT COUNT(*) INTO NBR FROM COMPTE_SAMBA WHERE CPT_ORDRE = :NEW.CPT_ORDRE;
--      IF (nbr = 0 ) THEN
--              lmpasswd := GRHUM.Crypt_Lm_Samba(passwd);
--              ntpasswd := GRHUM.Crypt_Nt_Samba(passwd);
--              flag := '[UX         ]';
--              INSERT INTO GRHUM.COMPTE_SAMBA VALUES(:NEW.cpt_ordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);
--      END IF;
    
        -- PASSWORD_HISTORY en ajout
        -- uniquement pour les VLAN Administration et Recherche
        IF (vlan='P' OR vlan='R') THEN
            SELECT COUNT(*) INTO nbr FROM GRHUM.PASSWORD_HISTORY WHERE cpt_ordre=:NEW.cpt_ordre;
            IF (NBR = 0) THEN
                SELECT GRHUM.PASSWORD_HISTORY_SEQ.NEXTVAL INTO pwdhOrdre FROM DUAL;
                -- Duree de validite du mot de passe lors de la creation du compte (en jours)
                SELECT SYSDATE + TO_NUMBER(param_value) INTO finValide FROM grhum.grhum_parametres WHERE param_key='PASSWORD_VALID_PERIOD_CREATE';
                INSERT INTO GRHUM.PASSWORD_HISTORY(PWDH_ORDRE, CPT_ORDRE, PWDH_PASSWD, TCRY_ORDRE, D_CREATION, D_FIN_VALIDITE)
                VALUES(pwdhOrdre,:NEW.cpt_ordre,passwdCrypte,tcryOrdre,SYSDATE,finValide);
            END IF;    
        END IF;
    END IF;
    
    -- MISE à JOUR sur REPART_COMPTE
    IF UPDATING THEN
    
         -- Récupération des données de COMPTE
         SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
            SELECT cpt_login,cpt_passwd,cpt_passwd_clair,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine,tvpn_code
            INTO   login,passwd,passwdClair,vlan,charte,connexion,uidGid,email,domaine,tvpnCode
            FROM GRHUM.COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (uidGid = 0) THEN uidGid := NULL; END IF;
         END IF;         
         -- Récupération des données de COMPTE_OLD
--         SELECT COUNT(*) INTO nbr FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--         IF (nbr != 0) THEN
--             SELECT cpt_login,cpt_passwd,cpt_vlan,cpt_charte,cpt_connexion,cpt_uid_gid,cpt_email,cpt_domaine
--             INTO         login,passwd,vlan,charte,connexion,uidGid,email,domaine
--             FROM COMPTE_OLD WHERE cpt_ordre = :NEW.cpt_ordre;
--             IF (uidGid = 0) THEN uidGid := NULL; END IF;
--         END IF;
         -- Récupération des données de COMPTE_SUITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN
             SELECT    cpt_home,cpt_shell,cpt_gid INTO home,shell,gidNew FROM COMPTE_SUITE WHERE cpt_ordre = :NEW.cpt_ordre;
         END IF;
         -- Récupération des données de COMPTE_VALIDITE
         SELECT COUNT(*) INTO nbr FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
         IF (nbr != 0) THEN         
             SELECT TO_DATE(date_debut,'dd/mm/yyyy'),TO_DATE(date_fin,'dd/mm/yyyy') INTO debutValide,finValide FROM COMPTE_VALIDITE WHERE cpt_ordre = :NEW.cpt_ordre;
        END IF;

          -- Password crypte en fonction de la taille du champ password
          -- les mots de passe cryptés sont constitués de 13 caractères.
          -- Tous les password sont cryptés sauf ceux qui ont une *
          crypte := 'O';
          --passwdClair := NULL;
          passwdCrypte := NULL;
--          if(passwdClair IS NOT NULL) then
--               IF (SUBSTR(passwd,1,1)='*') THEN
--                  passwdCrypte := '*'||GRHUM.Crypt(passwd);
--               ELSE
--                  passwdCrypte := GRHUM.Crypt(passwd);
--               END IF;          
--          else
--               passwdCrypte := passwd;        
--          end if;
          
       
          IF (LENGTH(passwd) >= 13) THEN
                    passwdClair := passwdClair;
                    passwdCrypte := passwd;
          ELSE
                    passwdClair := passwd;
                    IF (passwd = '*') THEN
                        passwdCrypte := passwd;
                    ELSE
                        -- PB 16/06/2006 : si le paswwd est désactivé, on crypte le passwd en mettant l'étoile devant
                        IF (SUBSTR(passwd,1,1)='*') THEN
                           passwdCrypte := '*'||GRHUM.Crypt(passwd);
                        ELSE
                           passwdCrypte := GRHUM.Crypt(passwd);
                        END IF;
                    END IF;
          END IF;
          
 -- ************************************************************************************************************************************
 -- ************************************************************************************************************************************
         
          
         -- on récupère la valeur de la table COMPTE
        select cpt_valide into valide from grhum.compte where cpt_ordre = :NEW.cpt_ordre;
        
        IF ( paramSwitch = 'O' ) -- si on tient compte des dates pour savoir si le compte est valide
        THEN  
            
            IF (vlan = 'E') THEN
            	debutValide := debutAnneeScol;
            	finValide := finAnneeScol;
            	-- Pour les comptes Etudiants le GID par défaut est de 2001
            gidNew := paramDefaultGid;            
         	END IF;
              
        	IF (debutValide IS NOT NULL) THEN
            	   --pour que le compte soit valide, la date actuelle doit etre comprise entre debut et fin de validite
            	   IF ( (debutValide <= date_actuelle) AND (finValide >= date_actuelle OR finValide IS NULL) ) THEN
            	           valide := 'O';
            	   ELSE
            	           valide := 'N';
            	   END IF;
            ELSE
            	   valide := 'O';
            END IF;
            
        END IF;
        
        
 -- ************************************************************************************************************************************
 -- ************************************************************************************************************************************
       
        
            
--            IF (:NEW.pers_id = 82429)
--            THEN raise_application_error (-20001,'debutValide : '||TO_CHAR(debutValide,'DD/MM/YYYY')||' ; date_actuelle : '||TO_CHAR(date_actuelle,'DD/MM/YYYY')||' ; ');
--            END IF;

-- Zone déplacer plus haut pour faire les étapes de manière logique MAJ des dates et ensuite contrôle de la validité

--         IF (vlan = 'E') THEN
--            debutValide := debutAnneeScol;
--            finValide := finAnneeScol;
--            -- Pour les comptes Etudiants le GID par défaut est de 2001
--            gidNew := 2001;            
--         END IF;

        -- recupération du pers_id
        persId := NULL;
        IF (:NEW.pers_id IS NULL) THEN        
            SELECT COUNT(*) INTO nbr FROM GRHUM.REPART_COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            IF (nbr !=0) THEN
                SELECT pers_Id INTO persId FROM GRHUM.REPART_COMPTE WHERE cpt_ordre = :NEW.cpt_ordre;
            END IF;
        ELSE
            persId := :NEW.pers_id;        
        END IF;            
         
        SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:NEW.cpt_ordre;                
        IF (nbr != 0) THEN        

            -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID
           IF (vlan='D' OR vlan='G') THEN
                  UPDATE GRHUM.COMPTE
                  SET      pers_id = persId, cpt_login = login,cpt_passwd=passwdCrypte,cpt_passwd_clair=passwdClair,cpt_crypte=crypte,cpt_uid = NULL, cpt_gid= uidGid,
                           cpt_valide = valide, cpt_debut_valide = debutValide, cpt_fin_valide = finValide,cpt_home = home,cpt_shell = shell, cpt_connexion = connexion,
                           cpt_vlan=vlan,cpt_charte=charte,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid = uidGid,tcry_ordre = tcryOrdre
                  WHERE    cpt_ordre = :NEW.cpt_ordre;
           ELSE
              
                  UPDATE GRHUM.COMPTE
                  SET      pers_id = persId, cpt_login = login,cpt_passwd=passwdCrypte,cpt_passwd_clair=passwdClair,cpt_crypte=crypte,cpt_uid = uidGid, cpt_gid = gidNew,
                           cpt_valide = valide, cpt_debut_valide = debutValide, cpt_fin_valide = finValide,cpt_home = home,cpt_shell = shell, cpt_connexion = connexion,
                           cpt_vlan=vlan,cpt_charte=charte,d_modification=:NEW.d_modification,cpt_modificateur=persidCRI,cpt_uid_gid = uidGid,tcry_ordre = tcryOrdre
                  WHERE    cpt_ordre = :NEW.cpt_ordre;
           END IF;

        ELSE

           -- Compte de type Diplome ou Groupe : le champ cpt_uid_gid contient le GID        
           IF (vlan='D' OR vlan='G') THEN
                  INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                           cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tcry_ordre)
                  VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,NULL,uidGid,home,shell,connexion,vlan,charte,
                         valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tcryOrdre);        
           ELSE
               -- Modif. le 15/03/2005 pour ne plus mettre à jour le mot de passe crypté car ils le sont déjà pour les comptes Administration et Recherche
               -- Modif. le 19/04/2005 : par contre les passwd pour les Etudiants et les eXterieurs sont à crypter
--               IF (vlan='P' OR vlan='R' OR vlan='E') THEN
--                     tvpnCode := 'W3VPN';
--               ELSE
--                     tvpnCode := 'NO';
--               END IF;           
               IF (vlan='P' OR vlan = 'R') THEN
                    INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                             cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                    VALUES(:NEW.cpt_ordre,persId,login,passwdClair,crypte,uidGid,gidNew,home,shell,connexion,vlan,charte,
                           valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);
               ELSE
                    INSERT INTO GRHUM.COMPTE(cpt_ordre,pers_id,cpt_login,cpt_passwd,cpt_passwd_clair,cpt_crypte,cpt_uid,cpt_gid,cpt_home,cpt_shell,cpt_connexion,cpt_vlan,cpt_charte,
                                             cpt_valide,cpt_debut_valide,cpt_fin_valide,cpt_principal,cpt_liste_rouge,d_creation,cpt_createur,d_modification,cpt_modificateur,cpt_uid_gid,tvpn_code,tcry_ordre)
                    VALUES(:NEW.cpt_ordre,persId,login,passwdCrypte,passwdClair,crypte,uidGid,gidNew,home,shell,connexion,vlan,charte,
                           valide,debutValide,finValide,'O','N',:NEW.d_creation,persidCRI,:NEW.d_modification,persidCRI,uidGid,tvpnCode,tcryOrdre);                
                END IF;
           END IF;
                                                                                     
        END IF;

         -- COMPTE_EMAIL en UPDATING
       IF (email IS NOT NULL) THEN
          SELECT COUNT(*) INTO nbr FROM COMPTE_EMAIL WHERE cpt_ordre=:NEW.cpt_ordre;    
          IF (nbr != 0) THEN
              UPDATE COMPTE_EMAIL 
              SET cem_email = email, cem_domaine = domaine, d_modification = SYSDATE
              WHERE cpt_ordre = :NEW.cpt_ordre;
          ELSE
              SELECT compte_email_seq.NEXTVAL INTO cemKey FROM dual;
              INSERT INTO COMPTE_EMAIL(CEM_KEY, CPT_ORDRE, CEM_EMAIL, CEM_DOMAINE, CEM_PRIORITE, CEM_ALIAS, D_CREATION, D_MODIFICATION)
              VALUES(cemKey,:NEW.cpt_ordre,email,domaine,NULL,'N',SYSDATE,SYSDATE);
          END IF;
        END IF;

      -- Configuration SAMBA en modification
--      SELECT count(*) into nbr from COMPTE_SAMBA where cpt_ordre = :NEW.cpt_ordre;
--      lmpasswd := GRHUM.Crypt_Lm_Samba(passwdClair);
--      ntpasswd := GRHUM.Crypt_Nt_Samba(passwdClair);
--      flag := '[UX         ]';
--      IF (nbr = 0 ) THEN
--            INSERT INTO GRHUM.COMPTE_SAMBA VALUES(:NEW.cpt_ordre,lmpasswd,ntpasswd,flag,SYSDATE,SYSDATE);
--      ELSE
--            UPDATE COMPTE_SAMBA set SMB_LM_PASSWD = lmpasswd, smb_nt_passwd = ntpasswd, d_modification = sysdate where cpt_ordre = :NEW.cpt_ordre;
--      END IF;

       -- PASSWORD_HISTORY en UPDATING
       -- Rem : fait par l'application de changement de mot de passe
--       if (finValide is null) then
--        select sysdate + to_number(param_value) into finValide from grhum.grhum_parametres where param_key='PASSWORD_VALID_PERIOD_MODIFY';
--       end if;    
--        SELECT COUNT(*) INTO nbr FROM GRHUM.PASSWORD_HISTORY WHERE cpt_ordre=:NEW.cpt_ordre;
--        IF (nbr != 0) THEN
--            UPDATE GRHUM.PASSWORD_HISTORY
--            SET pwdh_passwd = passwdCrypte, tcry_ordre = tcryOrdre, d_creation = sysdate, d_fin_validite = finValide
--            WHERE cpt_ordre = :NEW.cpt_ordre;
--        ELSE
--            SELECT GRHUM.PASSWORD_HISTORY_SEQ.NEXTVAL INTO pwdhOrdre FROM DUAL;
--             Duree de validite du mot de passe lors de la creation du compte (en jours)
--            INSERT INTO GRHUM.PASSWORD_HISTORY(PWDH_ORDRE, CPT_ORDRE, PWDH_PASSWD, TCRY_ORDRE, D_CREATION, D_FIN_VALIDITE)
--            VALUES(pwdhOrdre,:NEW.cpt_ordre,passwdCrypte,tcryOrdre,SYSDATE,finValide);
--        END IF;    
                
    END IF;
    
    -- SUPPRESSION sur REPART_COMPTE
    IF DELETING THEN
    
       --RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_REPART_COMPTE : DELETING "'||:OLD.cpt_ordre);
    
       SELECT COUNT(*) INTO nbr FROM GRHUM.COMPTE WHERE cpt_ordre=:OLD.cpt_ordre;
       IF (nbr != 0) THEN

          DELETE FROM PASSWORD_HISTORY WHERE cpt_ordre=:OLD.cpt_ordre;             
          DELETE FROM COMPTE_EMAIL WHERE cpt_ordre=:OLD.cpt_ordre;
          DELETE FROM CERTIFICAT WHERE cpt_ordre=:OLD.cpt_ordre;          
          DELETE FROM COMPTE WHERE cpt_ordre=:OLD.cpt_ordre AND pers_id=:OLD.pers_id;
          --DELETE FROM COMPTE_SAMBA WHERE cpt_ordre=:OLD.cpt_ordre;
          -- Le compte passe à l'état Annulé
          --UPDATE COMPTE SET cpt_valide='A' WHERE cpt_ordre=:OLD.cpt_ordre AND pers_id=:OLD.pers_id;
           
       END IF;
       
    END IF;
    
END ; 
/

