--
-- Patch DDL de GRHUM du 10/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.4.0
-- Date de publication : 10/10/2013
-- Auteur(s) : Julien Lafourcade

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------------------V20131004.153506__DDL_MAJ.sql------------------------------------
declare 
    persIdCreateur  integer;
    keyHabilitationAnneeDebut VARCHAR2(512);
    keyHabilitationDuree VARCHAR2(512);   
    itemCount integer;
    paramTypeId integer;
begin
    -- persIdCreation
    SELECT PERS_ID into persIdCreateur
               FROM GRHUM.COMPTE
        WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES
                                                WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));
                                                
    keyHabilitationAnneeDebut := 'org.cocktail.fwkcktlscolpeda.habilitation.anneedebut';
    keyHabilitationDuree := 'org.cocktail.fwkcktlscolpeda.habilitation.duree';
    
        
    select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where  PARAM_KEY = keyHabilitationAnneeDebut;
    if itemCount <> 0 then
      select type_id into paramTypeId 
      from GRHUM.grhum_parametres_type
      where type_id_interne = 'SCHOOL_YEAR';
      
      UPDATE GRHUM.GRHUM_PARAMETRES
      SET param_type_id = paramTypeId
      WHERE PARAM_KEY = keyHabilitationAnneeDebut;
 
    end if;

    select count(*) into itemCount from  GRHUM.GRHUM_PARAMETRES where  PARAM_KEY = keyHabilitationDuree;
    if itemCount <> 0 then
      select type_id into paramTypeId 
      from GRHUM.grhum_parametres_type
      where type_id_interne = 'VALEUR_NUMERIQUE';
      
      UPDATE GRHUM.GRHUM_PARAMETRES
      SET param_type_id = paramTypeId
      WHERE PARAM_KEY = keyHabilitationDuree;
 
    end if;
end;
/



