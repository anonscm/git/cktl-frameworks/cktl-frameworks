--
-- Patch DML de GRHUM du 25/02/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.15.0
-- Date de publication : 25/02/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



----------------------------V20140225.150155__DML_3emeUAI_INSA_CVdL.sql-----------------------------
-- Création d'un 3ème UAI pour l'INSA Centre Val de Loire à compter du 01/01/2014
Insert into GRHUM.RNE (C_RNE,LL_RNE,ADRESSE,CODE_POSTAL,C_RNE_PERE,D_DEB_VAL,D_FIN_VAL,D_CREATION,D_MODIFICATION,LC_RNE,ACAD_CODE,ETAB_STATUT,VILLE,SIRET)
	values ('0180975M','CAMPUS BOURGES - INSA CENTRE VAL DE LOIRE','88 BOULEVARD LAHITOLLE - CS 60013','18022',null,
			to_date('01/01/14','DD/MM/RR'),null,to_date('29/01/14','DD/MM/RR'),to_date('29/01/14','DD/MM/RR'),'CAMPUS BOURGES','018','PU','BOURGES','13001833600011');
			
			
-- Mise à jour de l'adresse du campus Blois de l'INSA Centre Val de Loire
update grhum.rne set adresse='3 RUE DE LA CHOCOLATERIE - CS23410' where c_rne='0411068N';


-------------------------------V20140225.150219__DML_RNE_ESPEMEN.sql--------------------------------
--------------------------------------------------------
--  RNE fourni par l'INSA de Rouen
--------------------------------------------------------

-- http://www.education.gouv.fr/bce/avancee/rech_fran.php?ACTION=VOIR&p_INDEX=1&c_NUMERO_UAI=0861316N

DECLARE

cpt INTEGER;

BEGIN

  SELECT count(*) INTO cpt
      FROM GRHUM.RNE
     WHERE c_rne = '0861316N';
     
     IF cpt <> 0 THEN
        UPDATE RNE SET ll_rne = 'ECOLE SUPERIEURE DU PERSONNEL D ENCADREMENT DU MEN', lc_rne = 'DPATE ESPEMEN', adresse = 'TELEPORT 2 BD 2', code_postal = '86963', d_modification = sysdate, c_rne_pere = '075080XW', ville = 'CHASSENEUIL-DU-POITOU'
        WHERE C_RNE = '0861316N';

     ELSE

  insert into grhum.RNE (c_rne, ll_rne, lc_rne, c_rne_pere, etab_statut, adresse, code_postal, ville, d_creation, d_modification, siret,d_deb_val)
    values ( '0861316N','ECOLE SUPERIEURE DU PERSONNEL D ENCADREMENT DU MEN', 'DPATE ESPEMEN','075080XW','PU','TELEPORT 2 BD 2', '86963', 'CHASSENEUIL-DU-POITOU', SYSDATE, SYSDATE, '12000002100028', to_date('01/09/1997','DD/MM/YYYY'));
    
    end if;    
 

END;
/
--------------------------V20140225.150238__DML_MAJ_RNE_recu_20140219.sql---------------------------
--------------------------------------------------------
--  RNE fourni par l'UPPA
--------------------------------------------------------

update grhum.rne set ville ='BELLEU' where c_rne = '0021659T';
update grhum.rne set ville ='MONTLUCON' where c_rne = '0030893E';
update grhum.rne set ville ='CHALETTE SUR LOING' where c_rne = '0451148D';
--------------------------V20140225.150259__DML_MAJ_RNE_recu_20140225.sql---------------------------
--------------------------------------------------------
--  RNE fourni par l'UPPA
--------------------------------------------------------

update grhum.rne set ville ='DIVES SUR MER' where c_rne = '0141599M';
update grhum.rne set ville ='CAEN' where c_rne = '0140013N';

--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.15.0',to_date('25/02/2014','DD/MM/YYYY'),sysdate,' 2 Champs plus grands, ajout d''un indexe suppl sur Grhum_parametres et Ajout et MAJ de RNE');
COMMIT;
