--
-- Patch DDL de GRHUM du 25/02/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.15.0
-- Date de publication : 25/02/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.14.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.15.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.15.0 a deja ete passe !');
    end if;

end;
/

------------------------V20140225.150037__DDL_MAJ_INDEX_GRHUM_PARAMETRES.sql------------------------
-- DT 5329
Declare
Begin
	GRHUM.DROP_OBJECT('GRHUM', 'IDX_GRHUM_PARAM_KEY_VALUE', 'INDEX');
End;
/

CREATE UNIQUE INDEX IDX_GRHUM_PARAM_KEY_VALUE ON GRHUM.GRHUM_PARAMETRES(PARAM_KEY,PARAM_VALUE) TABLESPACE INDX_GRHUM;


----------------------V20140225.150109__DDL_Alter_GRHUM_TYPE_CONTRAT_TRAV.sql-----------------------
-- Harmonisation de la taille de ce champ
ALTER TABLE GRHUM.TYPE_CONTRAT_TRAVAIL MODIFY C_TYPE_CONTRAT_TRAV VARCHAR2(6);

-- Agrandissement de la taille de l'adresse des RNE
ALTER TABLE GRHUM.RNE MODIFY ADRESSE VARCHAR2(50);


