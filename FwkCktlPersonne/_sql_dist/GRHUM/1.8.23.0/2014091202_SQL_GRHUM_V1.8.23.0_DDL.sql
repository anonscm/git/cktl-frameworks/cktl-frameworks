--
-- Patch DDL de GRHUM du 12/09/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.8.23.0
-- Date de publication : 12/09/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

declare
    cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.22.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.8.23.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.8.23.0 a deja ete passe !');
    end if;

end;
/

----------------------V20140820.141001__DDL_Ajout_colonne_bureau_principal.sql----------------------
declare
begin
	GRHUM.ADM_ADD_COLUMN('GRHUM', 'REPART_BUREAU', 'BUR_PRINCIPAL', 'NUMBER(1)', '0', NULL);
end;
/

COMMENT ON COLUMN GRHUM.REPART_BUREAU.BUR_PRINCIPAL IS 'Bureau principal (1: oui, 0: non)';


----------------------------V20140912.152257__DDL_Modif_tables_flux.sql-----------------------------
-- mise à jour des colonnes FILER dans les tables FLX_xxx
DECLARE
  is_column_exists int;
BEGIN
   SELECT count(*) INTO is_column_exists FROM all_tab_cols
    WHERE owner = 'GRHUM' and table_name = 'FLX_HISTO' and column_name = 'FILER';
   IF (is_column_exists  = 1) THEN
      execute immediate 'ALTER TABLE GRHUM.FLX_HISTO MODIFY (FILER VARCHAR2(255))';
   END IF;

   SELECT count(*) INTO is_column_exists FROM all_tab_cols
    WHERE owner = 'GRHUM' and table_name = 'FLX_FLUX' and column_name = 'FILER';
   IF (is_column_exists  = 1) THEN
      execute immediate 'ALTER TABLE GRHUM.FLX_FLUX MODIFY (FILER VARCHAR2(255))';
   END IF;

   SELECT count(*) INTO is_column_exists FROM all_tab_cols
    WHERE owner = 'GRHUM' and table_name = 'FLX_TABLES' and column_name = 'FILER';
   IF (is_column_exists  = 1) THEN
      execute immediate 'ALTER TABLE GRHUM.FLX_TABLES MODIFY (FILER VARCHAR2(255))';
   END IF;
END;
/
---------------------------V20140912.152526__DDL_Maj_Departement_BCN.sql----------------------------
create or replace
PROCEDURE      GRHUM.MAJ_DEPARTEMENT_BCN (
   codeDept       				GRHUM.DEPARTEMENT.C_DEPARTEMENT%Type,
   libelleCourtDept		    	GRHUM.DEPARTEMENT.LC_DEPARTEMENT%Type,
   libelleLongDept				GRHUM.DEPARTEMENT.LL_DEPARTEMENT%Type,
   dOuv							VARCHAR2,
   dFer							VARCHAR2,
   region						VARCHAR2,
   acad							VARCHAR2
)
IS
   n                NUMBER(5);
   regionDept		GRHUM.DEPARTEMENT.C_REGION%Type;
   acadDept			GRHUM.DEPARTEMENT.C_ACADEMIE%Type;
BEGIN

	-- Détermination de la région
	if ( region = '11') then
		select c_region into regionDept from GRHUM.region where ll_region = 'ILE DE FRANCE';
	end if;
	if ( region = '21') then
		select c_region into regionDept from GRHUM.region where ll_region = 'CHAMPAGNE ARDENNES';
	end if;
	if ( region = '22') then
		select c_region into regionDept from GRHUM.region where ll_region = 'PICARDIE';
	end if;
	if ( region = '23') then
		select c_region into regionDept from GRHUM.region where ll_region = 'HAUTE NORMANDIE';
	end if;
	if ( region = '24') then
		select c_region into regionDept from GRHUM.region where ll_region = 'CENTRE';
	end if;
	if ( region = '25') then
		select c_region into regionDept from GRHUM.region where ll_region = 'BASSE NORMANDIE';
	end if;
	if ( region = '26') then
		select c_region into regionDept from GRHUM.region where ll_region = 'BOURGOGNE';
	end if;
	if ( region = '31') then
		select c_region into regionDept from GRHUM.region where ll_region = 'NORD PAS DE CALAIS';
	end if;
	if ( region = '41') then
		select c_region into regionDept from GRHUM.region where ll_region = 'LORRAINE';
	end if;
	if ( region = '42') then
		select c_region into regionDept from GRHUM.region where ll_region = 'ALSACE';
	end if;
	if ( region = '43') then
		select c_region into regionDept from GRHUM.region where ll_region = 'FRANCHE COMTE';
	end if;
	if ( region = '52') then
		select c_region into regionDept from GRHUM.region where ll_region = 'PAYS DE LOIRE';
	end if;
	if ( region = '53') then
		select c_region into regionDept from GRHUM.region where ll_region = 'BRETAGNE';
	end if;
	if ( region = '54') then
		select c_region into regionDept from GRHUM.region where ll_region = 'POITOU CHARENTES';
	end if;
	if ( region = '72') then
		select c_region into regionDept from GRHUM.region where ll_region = 'AQUITAINE';
	end if;
	if ( region = '73') then
		select c_region into regionDept from GRHUM.region where ll_region = 'MIDI PYRENEES';
	end if;
	if ( region = '74') then
		select c_region into regionDept from GRHUM.region where ll_region = 'LIMOUSIN';
	end if;
	if ( region = '82') then
		select c_region into regionDept from GRHUM.region where ll_region = 'RHONE ALPES';
	end if;
	if ( region = '83') then
		select c_region into regionDept from GRHUM.region where ll_region = 'AUVERGNE';
	end if;
	if ( region = '91') then
		select c_region into regionDept from GRHUM.region where ll_region = 'LANGUEDOC ROUSSILLON';
	end if;
	if ( region = '93') then
		select c_region into regionDept from GRHUM.region where ll_region = 'PROVENCE COTE D''AZUR';
	end if;
	if ( region = '94') then
		select c_region into regionDept from GRHUM.region where ll_region = 'CORSE';
	end if;
	if ( region in ('01', '02', '03', '04', '06')) then
		regionDept := 'DOM';
	end if;
	if ( region = '00') then
		regionDept := 'TOM';
	end if;
	
	-- Détermination de l'académie
	acadDept := '0'||ltrim(rtrim(acad));

   SELECT COUNT (*) INTO n FROM grhum.departement WHERE c_departement = codeDept;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.departement(c_departement, lc_departement, ll_departement, d_deb_val, d_fin_val,
      		d_creation, d_modification, c_region, c_academie)
      VALUES(codeDept, libelleCourtDept, libelleLongDept, dOuv, dFer, SYSDATE, SYSDATE, regionDept, acadDept);
   ELSE
      -- mise à jour des libellé et des dates de validité des pays
      UPDATE grhum.departement
      SET c_departement = codeDept, lc_departement = libelleCourtDept, ll_departement = libelleLongDept, d_deb_val = dOuv, d_fin_val = dFer,
      		d_modification = SYSDATE, c_region = regionDept, c_academie = acadDept
     WHERE c_departement = codeDept;
   END IF;
   
   dbms_output.put_line('Departement :'||codeDept||' '||libelleCourtDept||' '||libelleLongDept||' '||dOuv||' '||dFer||' '||regionDept||' '||acadDept);
   
   COMMIT;
 
END;
/

-------------------------------V20140912.152636__DDL_Maj_Pays_BCN.sql-------------------------------
create or replace
PROCEDURE      GRHUM.MAJ_PAYS_BCN (
   numPays       				VARCHAR2,
   libelleLong		    		VARCHAR2,
   libelleCourt					VARCHAR2,
   dOuv							VARCHAR2,
   dFer							VARCHAR2,
   paysCommentaire				VARCHAR2,
   libelleAccentue70			VARCHAR2
)
IS
   n                NUMBER(5);
   chaine			VARCHAR2(250);
BEGIN


   SELECT COUNT (*) INTO n FROM grhum.pays WHERE c_pays = numPays;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.pays(c_pays, ll_pays, lc_pays, l_nationalite, d_deb_val, d_fin_val,
      		d_creation, d_modification, code_iso, l_edition, ll_pays_en, iso_3166_3, commentaire)
      VALUES(numPays, libelleLong, libelleCourt, null, dOuv, dFer, SYSDATE, SYSDATE, null, libelleAccentue70, null, null, paysCommentaire);
   ELSE
      -- mise à jour des libellé et des dates de validité des pays
      UPDATE grhum.pays
      SET c_pays = numPays, ll_pays = libelleLong, lc_pays = libelleCourt, d_deb_val = dOuv, d_fin_val = dFer,
      		d_modification = SYSDATE, l_edition = libelleAccentue70, commentaire = paysCommentaire
     WHERE c_pays = numPays;
   END IF;
   COMMIT;
 
END;
/


---------------------V20140912.153507__DDL_Ajout_table_gestion_type_service.sql---------------------
-- CREATION D'UNE TABLE pour la gestion des types de services (onglet PASSE / Services validés)

declare
begin

    grhum.adm_drop_object('GRHUM', 'TYPE_SERVICE', 'TABLE');

    grhum.adm_drop_object('GRHUM', 'TYPE_FONCTION_PUBLIQUE', 'TABLE');

    grhum.adm_drop_constraint ('GRHUM', 'EXCLUSION', 'EXCLU_REF_GRP_EXC');

    grhum.adm_drop_constraint ('GRHUM', 'EXCLUSION', 'FK_EXCL_GRP_EXCL');

    grhum.adm_drop_constraint ('GRHUM', 'INCLUSION_CONTRAT', 'INC_CON_REF_GRP_EXCLU');

    grhum.adm_drop_constraint ('GRHUM', 'INCLUSION_CONTRAT', 'FK_INC_CON_GRP_EXCL');

    grhum.adm_drop_constraint ('GRHUM', 'INCLUSION_CORPS', 'INC_COR_REF_GRP_EXCLU');

    grhum.adm_drop_constraint ('GRHUM', 'INCLUSION_CORPS', 'FK_INC_COR_GRP_EXCL');

    grhum.adm_drop_constraint ('GRHUM', 'COMPTE_EMAIL', 'CPT_EMAIL_REF_CPT');
    

end;
/

CREATE TABLE GRHUM.TYPE_SERVICE
(
  C_TYPE_SERVICE    VARCHAR2(3)                   NOT NULL,
  LC_TYPE_SERVICE   VARCHAR2(20)                  NOT NULL,
  LL_TYPE_SERVICE   VARCHAR2(70)                  NOT NULL,
  D_CREATION        DATE                          NOT NULL,
  D_MODIFICATION    DATE                          NOT NULL
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE GRHUM.TYPE_SERVICE ADD CONSTRAINT PK_TYPE_SERVICE PRIMARY KEY(C_TYPE_SERVICE) USING INDEX TABLESPACE INDX_GRHUM;

GRANT REFERENCES, SELECT ON GRHUM.TYPE_SERVICE TO MANGUE;


CREATE TABLE GRHUM.TYPE_FONCTION_PUBLIQUE
(
  C_TYPE_FP    VARCHAR2(3)                   NOT NULL,
  LC_TYPE_FP   VARCHAR2(20)                  NOT NULL,
  LL_TYPE_FP   VARCHAR2(70)                  NOT NULL,
  D_CREATION        DATE                          NOT NULL,
  D_MODIFICATION    DATE                          NOT NULL
)
TABLESPACE DATA_GRHUM
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

ALTER TABLE GRHUM.TYPE_FONCTION_PUBLIQUE ADD CONSTRAINT PK_TYPE_FP PRIMARY KEY(C_TYPE_FP) USING INDEX TABLESPACE INDX_GRHUM;

GRANT REFERENCES, SELECT ON GRHUM.TYPE_FONCTION_PUBLIQUE TO MANGUE;

-- COMPTE_EMAIL
ALTER TABLE grhum.compte_email ADD (
  CONSTRAINT CPT_EMAIL_REF_CPT 
 FOREIGN KEY (CPT_ORDRE) 
 REFERENCES COMPTE (CPT_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);


-- Motifs de depart (Fonction publique Hospitaliere et Territoriale)
ALTER TABLE GRHUM.MOTIF_DEPART MODIFY (LC_MOTIF_DEPART VARCHAR2(50));


ALTER TABLE GRHUM.EXCLUSION ADD (
  CONSTRAINT FK_EXCL_GRP_EXCL
 FOREIGN KEY (C_GROUPE_EXCLUSION)
 REFERENCES GRHUM.GROUPE_EXCLUSION (C_GROUPE_EXCLUSION)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE GRHUM.INCLUSION_CONTRAT ADD (
  CONSTRAINT FK_INC_CON_GRP_EXCL
 FOREIGN KEY (C_GROUPE_EXCLUSION)
 REFERENCES GRHUM.GROUPE_EXCLUSION (C_GROUPE_EXCLUSION)
    DEFERRABLE INITIALLY DEFERRED);


ALTER TABLE GRHUM.INCLUSION_CORPS ADD (
  CONSTRAINT FK_INC_COR_GRP_EXCL
 FOREIGN KEY (C_GROUPE_EXCLUSION)
 REFERENCES GRHUM.GROUPE_EXCLUSION (C_GROUPE_EXCLUSION)
    DEFERRABLE INITIALLY DEFERRED);





