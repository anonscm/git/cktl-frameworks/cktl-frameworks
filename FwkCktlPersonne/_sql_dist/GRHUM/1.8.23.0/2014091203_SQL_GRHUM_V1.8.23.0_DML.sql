--
-- Patch DML de GRHUM du 12/09/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.8.23.0
-- Date de publication : 12/09/2014
-- Auteur(s) : Alain

--
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--------------------------------V20140820.170920__DML_maj_budget.sql--------------------------------
--------------------------------------------------
-- Mise à jour de la table GRHUM.NATURE_BUDGET
--------------------------------------------------


DECLARE
   compteur NUMBER;
BEGIN

	-- On recherche l'existence du budget "Rompus"
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.NATURE_BUDGET 
	 WHERE C_BUDGET = 'R';

	    IF (compteur = 1) 
	  THEN
		UPDATE GRHUM.NATURE_BUDGET
   		   SET LC_BUDGET = 'Rompu TP état', LL_BUDGET = 'Rompu TP budget Etat', D_MODIFICATION = SYSDATE
 		 WHERE C_BUDGET = 'R';
	END IF;	

	-- On recherche l'existence du budget "Rompus TP propre"
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.NATURE_BUDGET 
	 WHERE C_BUDGET = 'T';

	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.NATURE_BUDGET (C_BUDGET, LC_BUDGET, LL_BUDGET, TEM_BUDGET_ETAT, TEM_FLUX_MOYENS, D_CREATION, D_MODIFICATION)
		     VALUES ('T', 'Rompu TP propre', 'Rompu TP budget Propre', 'N', 'R', SYSDATE, SYSDATE);
	END IF;	
END;
/

---------------------------V20140821.103233__DML_maj_Categorie_emploi.sql---------------------------
--------------------------------------------------
-- Mise à jour de la table GRHUM.CATEGORIE_EMPLOI
-- Ajout des catégories d'emploi : ASOCAE, CONTET, CTSSAE, MCFMG, 
--------------------------------------------------


DECLARE
   compteur NUMBER;
BEGIN

	-- On recherche l'existence de la catégorie d'emploi 'ASOCAE'
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.CATEGORIE_EMPLOI 
	 WHERE C_CATEGORIE_EMPLOI = 'ASOCAE';

	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.CATEGORIE_EMPLOI (C_CATEGORIE_EMPLOI, LC_CATEGORIE_EMPLOI, LL_CATEGORIE_EMPLOI, D_CREATION, D_MODIFICATION, TEM_BUDGET_PROPRE, TEM_SURNOMBRE, TEM_PAST)
		     VALUES ('ASOCAE', 'Assist. sociale AE', 'Assistante Sociale des administrations de l''état', SYSDATE, SYSDATE, 'N', 'N', 'N');
	END IF;	

	-- On recherche l'existence de la catégorie d'emploi 'CONTET'
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.CATEGORIE_EMPLOI 
	 WHERE C_CATEGORIE_EMPLOI = 'CONTET';

	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.CATEGORIE_EMPLOI (C_CATEGORIE_EMPLOI, LC_CATEGORIE_EMPLOI, LL_CATEGORIE_EMPLOI, D_CREATION, D_MODIFICATION, TEM_BUDGET_PROPRE, TEM_SURNOMBRE, TEM_PAST)
		     VALUES ('CONTET', 'Etudiant', 'Contractuel étudiant', SYSDATE, SYSDATE, 'N', 'N', 'N');
	END IF;	

	-- On recherche l'existence de la catégorie d'emploi 'CTSSAE'
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.CATEGORIE_EMPLOI 
	 WHERE C_CATEGORIE_EMPLOI = 'CTSSAE';

	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.CATEGORIE_EMPLOI (C_CATEGORIE_EMPLOI, LC_CATEGORIE_EMPLOI, LL_CATEGORIE_EMPLOI, D_CREATION, D_MODIFICATION, TEM_BUDGET_PROPRE, TEM_SURNOMBRE, TEM_PAST)
		     VALUES ('CTSSAE', 'Cons. tech. SSAE', 'Conseiller Technique de Service Social des admin. de l''état', SYSDATE, SYSDATE, 'N', 'N', 'N');
	END IF;	

	-- On recherche l'existence de la catégorie d'emploi 'MCFMG'
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.CATEGORIE_EMPLOI 
	 WHERE C_CATEGORIE_EMPLOI = 'MCFMG';

	    IF (compteur = 0) 
	  THEN
		INSERT INTO GRHUM.CATEGORIE_EMPLOI (C_CATEGORIE_EMPLOI, LC_CATEGORIE_EMPLOI, LL_CATEGORIE_EMPLOI, D_CREATION, D_MODIFICATION, TEM_BUDGET_PROPRE, TEM_SURNOMBRE, TEM_PAST)
		     VALUES ('MCFMG', 'Maitre de conf  MG', 'Maitre de conférences des universités de médecine générale', SYSDATE, SYSDATE, 'N', 'N', 'N');
	END IF;	
	
END;
/

----------------------------V20140901.162533__DML_Maj_libelle_corps.sql-----------------------------
--------------------------------------------------------
-- Mise à jour des corps 759 et 758
--------------------------------------------------------

UPDATE GRHUM.CORPS
SET LC_CORPS = 'CEV', LL_CORPS = 'Chargé d''enseignement vacataire', D_OUVERTURE_CORPS = to_date('29/10/1987', 'dd/mm/yyyy'), POTENTIEL_BRUT = 187, D_MODIFICATION = SYSDATE
WHERE C_CORPS = '758';

UPDATE GRHUM.CORPS
SET LC_CORPS = 'ATV', LL_CORPS = 'Agent temporaire vacataire d''enseignement', D_OUVERTURE_CORPS = to_date('29/10/1987', 'dd/mm/yyyy'), POTENTIEL_BRUT = 96, D_MODIFICATION = SYSDATE
WHERE C_CORPS = '759';



-----------------------------V20140912.085727__DML_Suppr_quotite_86.sql-----------------------------
DELETE FROM GRHUM.QUOTITE quot WHERE quot.num_quotite = '86';
----------------------------V20140912.090714__DML_MAJ_RNE_A_MAURICE.sql-----------------------------
DECLARE

BEGIN
	GRHUM.MAJ_UAI ('3900005L','LYCEE DES MASCAREIGNES','HELVETIA',null,'01/09/2005',null,'028','PR','MAURICE',null);
END;
/
----------------------------V20140912.154600__DML_Merge_tables_flux.sql-----------------------------
-- table FLX_FLUX
merge into GRHUM.FLX_FLUX ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'RNE')
when matched then update set LIBELLE = 'Flux RNE', FILER = null
when not matched then insert values ('RNE','Flux RNE', null);

merge into GRHUM.FLX_FLUX ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'BDF')
when matched then update set LIBELLE = 'Flux Banque de France', FILER = null
when not matched then insert values ('BDF','Flux Banque de France', null);

merge into GRHUM.FLX_FLUX ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'COM')
when matched then update set LIBELLE = 'Flux des communes', FILER = null
when not matched then insert values ('COM','Flux des communes', null);

merge into GRHUM.FLX_FLUX ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'UAI')
when matched then update set LIBELLE = 'Flux des UAI de MESR', FILER = null
when not matched then insert values ('UAI','Flux des UAI de MESR', null);

merge into GRHUM.FLX_FLUX ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'PAY')
when matched then update set LIBELLE = 'Flux des pays', FILER = null
when not matched then insert values ('PAY','Flux des pays', null);

merge into GRHUM.FLX_FLUX ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'BAC')
when matched then update set LIBELLE = 'Flux des baccalauérats', FILER = null
when not matched then insert values ('BAC','Flux des baccalauréats', null);

merge into GRHUM.FLX_FLUX ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'DEP')
when matched then update set LIBELLE = 'Flux des départements', FILER = null
when not matched then insert values ('DEP','Flux des départements', null);

-- table FLX_TABLES
merge into GRHUM.FLX_TABLES ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'RNE' and ff.USER_TABLE = 'GRHUM' and ff.NOM_TABLE = 'RNE')
when matched then update set FILER = 'Mise à jour, recherche de lien potentiel avec COMMUNE'
when not matched then insert values ('RNE','GRHUM','RNE','Mise à jour, recherche de lien potentiel avec COMMUNE');

merge into GRHUM.FLX_TABLES ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'BDF' and ff.USER_TABLE = 'GRHUM' and ff.NOM_TABLE = 'BANQUE')
when matched then update set FILER = 'Mise à jour'
when not matched then insert values ('BDF','GRHUM','BANQUE','Mise à jour');

merge into GRHUM.FLX_TABLES ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'COM' and ff.USER_TABLE = 'GRHUM' and ff.NOM_TABLE = 'COMMUNE')
when matched then update set FILER = 'Mise à jour'
when not matched then insert values ('COM','GRHUM','COMMUNE','Mise à jour');

merge into GRHUM.FLX_TABLES ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'UAI' and ff.USER_TABLE = 'GRHUM' and ff.NOM_TABLE = 'UAI')
when matched then update set FILER = 'Mise à jour'
when not matched then insert values ('UAI','GRHUM','UAI','Mise à jour');

merge into GRHUM.FLX_TABLES ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'PAY' and ff.USER_TABLE = 'GRHUM' and ff.NOM_TABLE = 'PAYS')
when matched then update set FILER = 'Mise à jour'
when not matched then insert values ('PAY','GRHUM','PAYS','Mise à jour');

merge into GRHUM.FLX_TABLES ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'BAC' and ff.USER_TABLE = 'GRHUM' and ff.NOM_TABLE = 'BAC')
when matched then update set FILER = 'Mise à jour'
when not matched then insert values ('BAC','GRHUM','BAC','Mise à jour');

merge into GRHUM.FLX_TABLES ff using (select 1 from dual) sf on (ff.CODE_FLUX = 'DEP' and ff.USER_TABLE = 'GRHUM' and ff.NOM_TABLE = 'DEPARTEMENT')
when matched then update set FILER = 'Mise à jour'
when not matched then insert values ('DEP','GRHUM','DEPARTEMENT','Mise à jour');

-- correction d'une eventuelle erreur historique
delete GRHUM.FLX_TABLES where CODE_FLUX = 'UAI' and USER_TABLE = 'GRHUM' and NOM_TABLE = 'RNE';

commit;
------------------------V20140912.154658__DML_parametre_signature_arrete.sql------------------------
-- Insertion d'un parametre pour la signature des arretes de conge

declare
cpt integer;
begin


select count(*) into cpt from grhum_parametres where param_key = 'org.cocktail.mangue.SIGNATURE_ARRETES';

if (cpt = 0)
then

    execute immediate 
    'INSERT INTO GRHUM.GRHUM_PARAMETRES
    values (grhum.grhum_parametres_seq.nextval, ''org.cocktail.mangue.SIGNATURE_ARRETES'', ''Le Président'', ''Signataire des arrêtés de congés dans Mangue'', NULL, NULL, SYSDATE, SYSDATE, 10)';

end if;

end;
/

-----------------------------V20140912.154719__DML_maj_motif_depart.sql-----------------------------
-- Motifs de depart (Fonction publique Hospitaliere et Territoriale)

DELETE FROM GRHUM.MOTIF_DEPART where c_motif_depart in ('IFPT', 'IFPH');

INSERT INTO GRHUM.MOTIF_DEPART
(C_MOTIF_DEPART, LC_MOTIF_DEPART, TEM_RNE_LIEU, TEM_FIN_CARRIERE, D_CREATION, D_MODIFICATION, TEM_ENS, C_MOTIF_DEPART_ONP)
VALUES
('IFPT','Intégration Fonction Publique Territoriale', 'N', 'N', SYSDATE, SYSDATE, 'N', 'MC126');

INSERT INTO GRHUM.MOTIF_DEPART
(C_MOTIF_DEPART, LC_MOTIF_DEPART, TEM_RNE_LIEU, TEM_FIN_CARRIERE, D_CREATION, D_MODIFICATION, TEM_ENS, C_MOTIF_DEPART_ONP)
VALUES
('IFPH','Intégration Fonction Publique Hospitalière', 'N', 'N', SYSDATE, SYSDATE, 'N', 'MC126');



--------------------------V20140912.154737__DML_insertion_type_service.sql--------------------------
-- Insertion des types de service

DELETE FROM GRHUM.TYPE_SERVICE;

Insert into GRHUM.TYPE_SERVICE
   (C_TYPE_SERVICE, LC_TYPE_SERVICE, LL_TYPE_SERVICE, D_CREATION, D_MODIFICATION)
 Values
   ('SV', 'Services validés', 'Services de Non Titulaires validés', SYSDATE, SYSDATE);

Insert into GRHUM.TYPE_SERVICE
   (C_TYPE_SERVICE, LC_TYPE_SERVICE, LL_TYPE_SERVICE, D_CREATION, D_MODIFICATION)
 Values
   ('MI', 'Militaire', 'Carrière militaire (>=15 ans)', SYSDATE, SYSDATE);
   
Insert into GRHUM.TYPE_SERVICE
   (C_TYPE_SERVICE, LC_TYPE_SERVICE, LL_TYPE_SERVICE, D_CREATION, D_MODIFICATION)
 Values
   ('EN', 'Engagé', 'Engagé (<15 ans)', SYSDATE, SYSDATE);

Insert into GRHUM.TYPE_SERVICE
   (C_TYPE_SERVICE, LC_TYPE_SERVICE, LL_TYPE_SERVICE, D_CREATION, D_MODIFICATION)
 Values
   ('EA', 'EAS', 'EAS (stagiaire ou titulaire hors MEN/MENSR)', SYSDATE, SYSDATE);
   
Insert into GRHUM.TYPE_SERVICE
   (C_TYPE_SERVICE, LC_TYPE_SERVICE, LL_TYPE_SERVICE, D_CREATION, D_MODIFICATION)
 Values
   ('EL', 'Elève', 'Elève avant titularisation', SYSDATE, SYSDATE);


---------------------V20140912.154752__DML_insertion_type_fonction_publique.sql---------------------
-- Insertion des types de fonction_publique

DELETE FROM GRHUM.TYPE_FONCTION_PUBLIQUE;

Insert into GRHUM.TYPE_FONCTION_PUBLIQUE
   (C_TYPE_FP, LC_TYPE_FP, LL_TYPE_FP, D_CREATION, D_MODIFICATION)
 Values
   ('FPE', 'Etat', 'Fonction publique d''Etat', SYSDATE, SYSDATE);

Insert into GRHUM.TYPE_FONCTION_PUBLIQUE
   (C_TYPE_FP, LC_TYPE_FP, LL_TYPE_FP, D_CREATION, D_MODIFICATION)
 Values
   ('FPH', 'Hospitalière', 'Fonction publique hospitalière', SYSDATE, SYSDATE);
   
Insert into GRHUM.TYPE_FONCTION_PUBLIQUE
   (C_TYPE_FP, LC_TYPE_FP, LL_TYPE_FP, D_CREATION, D_MODIFICATION)
 Values
   ('FPT', 'Territoriale', 'Fonction publique territoriale', SYSDATE, SYSDATE);



--DB_VERSION 
INSERT INTO GRHUM.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL,'1.8.23.0',to_date('12/09/2014','DD/MM/YYYY'),sysdate,'Ajout des tables GRHUM.TYPE_SERVICE et TYPE_FONCTION_PUBLIQUE, MAJ des tables de Flux et les procédures insertion de pays et de département');
COMMIT;
