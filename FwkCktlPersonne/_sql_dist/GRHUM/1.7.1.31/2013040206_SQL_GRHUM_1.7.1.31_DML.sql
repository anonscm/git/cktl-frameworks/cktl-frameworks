--
-- Patch DML de GRHUM du 02/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 6/6
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 02/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--



WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- Ajout ou modification de RNE
--
update grhum.rne set ville ='OLORON STE MARIE' where c_rne = '0640050G';
update grhum.rne set ville='VIC-EN-BIGORRE' where c_rne in ('0650139Y','0650035K','0650838H','0650038N','0650075D','0650863K');
update grhum.rne set ville='MONLEON MAGNOAC' where c_rne in ('0650971C','0650878B','0650885J');
update grhum.rne set ville='LOURES BAROUSSE' where c_rne in ('0650017R');
update grhum.rne set ville='SAINT-PALAIS' where c_rne in ('0640168K');
update grhum.rne set ville='SAINT-JUST_IBARRE' where c_rne in ('0640166H','0641520E','0641927X');
update grhum.rne set ville='LARCEVEAU-ARROS-CIBITS' where c_rne in ('0642006H');
update grhum.rne set ville='IGON' where c_rne in ('0641654A','0641665M');
update grhum.rne set ville='NAY' where c_rne in ('0640125N','0641657D','0641534V');
update grhum.rne set ville='LESTELLE-BETHARRAM' where c_rne in ('0640122K','0641655B');
update grhum.rne set ville='BIARRITZ' where c_rne in ('064007XJ','064014XV');
update grhum.rne set ville='BIARRITZ',code_postal=64201 where c_rne ='0641386J';
update grhum.rne set ville='MONEIN' where c_rne in ('0640041X');
update grhum.RNE set ville='SAINT VENANT' where c_rne ='0622091L';
update grhum.RNE set ville='REIMS' where c_rne ='0511802G';
update grhum.RNE set ville='BIDACHE' where c_rne ='0640019Y';
update grhum.RNE set ville='SANTENY' where c_rne ='0941782B';
update grhum.RNE set ville='PLOEUC SUR LIE' where c_rne='0221568K';
update grhum.RNE set ville='SAUVETERRE DE BEARN' where c_rne='0640073G';






--
-- PERSONNE_TELEPHONE
--
-- Rem : harmonisation des numéros de téléphones (enregistrés sans caractères non numérique)
-- no_telephone is not null
update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone=' '; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='.'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='/'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='0'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='00'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='0000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='000000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='0000000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='00000000'; 

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where no_telephone='000000000'; 

update grhum.personne_telephone set no_telephone=replace(no_telephone,'/',''),d_modification=sysdate where no_telephone like '%/%';

update grhum.personne_telephone set no_telephone=replace(no_telephone,'-',''),d_modification=sysdate where no_telephone like '%-%';

update grhum.personne_telephone set no_telephone=replace(no_telephone,'O','0'),d_modification=sysdate where no_telephone like '%O%';

update grhum.personne_telephone set no_telephone=replace(no_telephone,'x','0'),d_modification=sysdate where no_telephone like '%x%';

update grhum.personne_telephone set no_telephone='0000000000',d_modification=sysdate where ltrim(rtrim(no_telephone)) is null; 

-- suppression des numeros en double (pk_telephone)
delete from personne_telephone
where (pers_id,no_telephone,type_no,type_tel)
in
(
select pers_id,no_telephone,type_no,type_tel from
(
select pers_id,ltrim(rtrim(no_telephone)) as no_telephone,type_no,type_tel,count(*)
from personne_telephone
group by pers_id,ltrim(rtrim(no_telephone)),type_no,type_tel having count(*)>1
)
);

update grhum.personne_telephone set no_telephone=ltrim(rtrim(no_telephone)),d_modification=sysdate;

-- suppression des numeros en double (pk_telephone)
delete from personne_telephone
where (pers_id,no_telephone,type_no,type_tel)
in
(
select pers_id,no_telephone,type_no,type_tel from
(
select pers_id,replace(no_telephone,'.','') as no_telephone,type_no,type_tel,count(*)
from personne_telephone
group by pers_id,replace(no_telephone,'.',''),type_no,type_tel having count(*)>1
)
);

update grhum.personne_telephone set no_telephone=replace(no_telephone,'.',''),d_modification=sysdate where no_telephone like '%.%';

-- suppression des numeros en double (pk_telephone)
delete from personne_telephone
where (pers_id,no_telephone,type_no,type_tel)
in
(
select pers_id,no_telephone,type_no,type_tel from
(
select pers_id,replace(no_telephone,' ','') as no_telephone,type_no,type_tel,count(*)
from personne_telephone
group by pers_id,replace(no_telephone,' ',''),type_no,type_tel having count(*)>1
)
);

update grhum.personne_telephone set no_telephone=replace(no_telephone,' ',''),d_modification=sysdate where no_telephone like '% %';



--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.31';




COMMIT;