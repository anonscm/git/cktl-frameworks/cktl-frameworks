--
-- Patch DML de GRHUM du 02/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8
-- JS : ajout du placement des indexes dans le tablespace destine (a priori) aux indexes

SET DEFINE OFF;

--
--
-- Fichier : 4/6
-- Type : DDL
-- Schema : GRHUM
-- Numero de version :
-- Date de publication : 02/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- ROME
--
CREATE TABLE GRHUM.ROME
(
  ROM_ID           NUMBER                       NOT NULL,
  ROM_CODE         VARCHAR2(5)                  NOT NULL,
  ROM_LIBELLE      VARCHAR2(2000)               NOT NULL,
  ROM_D_OUVERTURE  DATE,
  ROM_D_FERMETURE  DATE,
  D_CREATION       DATE                         DEFAULT SYSDATE               NOT NULL,
  D_MODIFICATION   DATE                         DEFAULT SYSDATE               NOT NULL,
  ROM_ID_PERE      NUMBER                       NOT NULL
);

COMMENT ON TABLE GRHUM.ROME IS 'Nomenclature du Répertoire Opérationnel des Métiers et des Emplois';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID IS 'Clef primaire';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID IS 'Code ROME';

COMMENT ON COLUMN GRHUM.ROME.ROM_LIBELLE IS 'Libellé du ROME';

COMMENT ON COLUMN GRHUM.ROME.ROM_D_OUVERTURE IS 'Date de début de validité';

COMMENT ON COLUMN GRHUM.ROME.ROM_D_FERMETURE IS 'Date de fin de validité';

COMMENT ON COLUMN GRHUM.ROME.D_CREATION IS 'Date de création de l''enregistrement';

COMMENT ON COLUMN GRHUM.ROME.D_MODIFICATION IS 'Date de dernière modification de l''enregistrement';

COMMENT ON COLUMN GRHUM.ROME.ROM_ID_PERE IS 'ROME père';

declare
tbs_def VARCHAR2(30);
tbs_idx VARCHAR2(30);
ord_sql VARCHAR2(255);
begin
-- recuperation des noms du tablespace par defaut et INDX
-- default
select default_tablespace into tbs_def from dba_users where username = 'GRHUM';
-- indexes
select max(tablespace_name) into tbs_idx from dba_segments where segment_type = 'INDEX' and owner = 'GRHUM' group by tablespace_name
having count(*) = (select max(count(*)) from dba_segments  where segment_type = 'INDEX' and owner = 'GRHUM' group by tablespace_name);

-- positionnement INDX comme tablespace par defaut
ord_sql := 'alter user GRHUM default tablespace '||tbs_idx;
execute immediate (ord_sql);

--
-- PK_ROME
--
execute immediate 'CREATE UNIQUE INDEX GRHUM.PK_ROME ON GRHUM.ROME(ROM_ID)';

--
-- Non Foreign Key Constraints for Table ROME
--
execute immediate 'ALTER TABLE GRHUM.ROME ADD (CONSTRAINT PK_ROME PRIMARY KEY (ROM_ID))';

--
-- Foreign Key Constraints for Table ROME
--
execute immediate '
ALTER TABLE GRHUM.ROME ADD (CONSTRAINT ROME_REF_ROME_PERE
 FOREIGN KEY (ROM_ID_PERE)
 REFERENCES GRHUM.ROME (ROM_ID))';

-- retour au tablespace DATA par defaut
ord_sql := 'alter user GRHUM default tablespace '||tbs_def;
execute immediate (ord_sql);
END;
/

--
-- STRUCTURE_ULR
--
ALTER TABLE GRHUM.STRUCTURE_ULR ADD STR_STATUT VARCHAR2(2);

ALTER TABLE GRHUM.STRUCTURE_ULR ADD ROM_ID NUMBER;

ALTER TABLE GRHUM.STRUCTURE_ULR ADD STR_DESCRIPTION VARCHAR2(1000);

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.STR_STATUT IS 'Statut de la structure : PUblic, PRivé, AUtre';

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.ROM_ID IS 'Clef étrangère vers la nomenclature des codes ROME (Répertoire Opérationnel des Métiers et des Emplois)';

COMMENT ON COLUMN GRHUM.STRUCTURE_ULR.STR_DESCRIPTION IS 'Description de la structure : description pure ou fonction';

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (
  CONSTRAINT CHK_STR_STATUT
 CHECK (STR_STATUT in ('PU','PR','AU')));

ALTER TABLE GRHUM.STRUCTURE_ULR ADD (CONSTRAINT STR_REF_ROME
 FOREIGN KEY (ROM_ID)
 REFERENCES GRHUM.ROME (ROM_ID));

CREATE OR REPLACE PROCEDURE GRHUM.MAJ_COMPTE_PRINCIPAL
IS
-- Procedure de mise à jour de l'attribut cpt_principal de la table COMPTE
-- regle : un seul compte principal pour un pers_id défini par rapport à la priorité du VLAN
-- Auteur : ULR PB
-- creation : 21/03/2013
-- modification : 21/03/2013

-- curseurs des multi-comptes
CURSOR c1 IS
    select pers_id from GRHUM.compte group by pers_id having count(*) > 1;

-- comptes d'une personne par ordre de priorité
CURSOR c2(persid NUMBER) Is
    select cpt_ordre from GRHUM.compte C
    inner join vlans V on C.CPT_VLAN = V.C_VLAN
    where pers_id = persid
    order by V.PRIORITE;

persId      GRHUM.COMPTE.PERS_ID%TYPE;
cptOrdre    GRHUM.COMPTE.CPT_ORDRE%TYPE;

BEGIN
     OPEN c1;
     LOOP
        FETCH c1 INTO persId;
        EXIT WHEN c1%NOTFOUND;
        OPEN c2(persId);
        LOOP
            FETCH c2 INTO cptOrdre;
            EXIT WHEN c2%NOTFOUND;

            IF c2%rowcount = 1 then
                update grhum.compte set cpt_principal = 'O', d_modification = sysdate where cpt_ordre = cptOrdre;
            else
                update grhum.compte set cpt_principal = 'N', d_modification = sysdate where cpt_ordre = cptOrdre;
            end if;
        END LOOP;
        CLOSE c2;
     END LOOP;
     CLOSE c1;

END;
/

