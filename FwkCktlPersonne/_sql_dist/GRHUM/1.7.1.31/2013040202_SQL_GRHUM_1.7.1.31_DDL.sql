--
-- Patch DML de GRHUM du 02/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8
-- JS : regroupement des traitements DEPT dans CASE
-- JS : suppression de is_part
-- JS : ajout du commit explicite

SET DEFINE OFF;

--
--
-- Fichier : 2/6
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 
-- Date de publication : 02/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

create or replace 
PROCEDURE       MAJ_COMMUNE (
   cinsee       			VARCHAR2,
   ccommune		    		VARCHAR2,
   lcommune				    VARCHAR2,
   ccp             		VARCHAR2,
   ligneacheminement	VARCHAR2
)
IS
   n                NUMBER(5);
   particularite	VARCHAR2(1);
   ddebval  		DATE;
   dffinval    		DATE;
   cdep       		VARCHAR2(3);
   ccp2        		VARCHAR2(5);
BEGIN
  -- détermination du département
  CASE cinsee
        WHEN '97701' THEN cdep := '971'; -- cas de Saint-Barthélémy
        WHEN '97801' THEN cdep := '971'; -- cas de Saint-Martin
        WHEN '98901' THEN cdep := '987'; -- cas de l'Ile de Clipperton
        ELSE
           BEGIN
                IF ( to_number(ltrim(rtrim(ccp)),99999) < 97000 ) THEN
                   cdep := '0'||substr(ltrim(rtrim(ccp)),1,2);
                ELSE
                   cdep := substr(ltrim(rtrim(cinsee)),1,3);
                END IF;
           END;
 END CASE;
   
   -- détermination du code postal principal
   -- il faut tester le dernier numéro et si différent de zéro alors le remplacer par zéro
   IF ( substr(ltrim(rtrim(ccp)),5,1) = 0 ) THEN
      ccp2 := ltrim(rtrim(ccp));
   ELSE
      ccp2 := substr(ltrim(rtrim(ccp)),1,4)||'0';
   END IF;
   -- détermination de la particularité
   -- si acheminement différent du libellé court alors particularité = c sinon null
   IF ( upper(ltrim(rtrim(ligneacheminement))) = upper(ltrim(rtrim(ccommune))) ) THEN
 --     particularite := 'NON';
      particularite := null;
   ELSE
 --     particularite := 'OUI';
      particularite := 'C';
   END IF;

 -- MAJ
    
   SELECT COUNT (*) INTO n FROM grhum.commune WHERE c_postal = ccp AND c_insee = cinsee;
   IF (n = 0) THEN
      -- insertion
      INSERT INTO grhum.commune(c_insee, ll_com, lc_com, c_postal, c_dep, d_creation, d_modification, d_deb_val, d_fin_val, particularite_commune, c_postal_principal, ligne_acheminement)
      SELECT cinsee, lcommune, ccommune, ccp, cdep, SYSDATE, SYSDATE, null, null, particularite, ccp2, ligneacheminement FROM DUAL;
   ELSE
      -- mise à jour des communes issues du Fichier des Communes
      UPDATE grhum.commune SET ll_com = lcommune, d_modification = SYSDATE, lc_com = ccommune, c_postal = ccp, c_postal_principal = ccp2, c_insee = cinsee, c_dep=cdep, ligne_acheminement = ligneacheminement
      WHERE c_postal = ccp AND c_insee = cinsee;
   END IF;
   COMMIT;
 
END;
/

