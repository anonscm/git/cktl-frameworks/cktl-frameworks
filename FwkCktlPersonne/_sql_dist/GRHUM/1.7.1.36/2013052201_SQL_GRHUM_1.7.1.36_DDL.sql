--
-- Patch DML de GRHUM du 22/05/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.36 
-- Date de publication : 22/05/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.35';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.36';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.36 a deja ete passe !');
    end if;

end;
/

-- Modification pour que le trigger sur les comptes ne plantent pas.
ALTER TABLE GRHUM.compte_temp MODIFY (cpt_login VARCHAR2(50));



-- SB UPPA le 16/05/2013
-- Procédures echanger_personne_* manquantes

-- Ajout de 

  -- **********
  -- ACCORDS...
  -- **********
  -- Echanger_Personne_Accords(oldid,newid,oldno,newno);

  -- ************
  -- ADMISSION...
  -- ************
  -- Echanger_Personne_Admission(oldid,newid,oldno,newno);

  -- **********
  -- CKTL_GED...
  -- **********
  -- Echanger_Personne_cktl_ged(oldid,newid,oldno,newno);

  -- **********
  -- SALLE...
  -- **********
  -- Echanger_Personne_salle(oldid,newid,oldno,newno);

  -- **************
  -- RESERVATION...
  -- **************
  --  Echanger_Personne_reservation(oldid,newid,oldno,newno);



create or replace
PROCEDURE Echanger_Personne
(
  oldid NUMBER,
  newid NUMBER
)
IS
  oldno NUMBER;
  newno NUMBER;
BEGIN
  -- Initialisations...
  SELECT no_individu
  INTO   oldno
  FROM   grhum.INDIVIDU_ULR
  WHERE  pers_id = oldid;

  SELECT no_individu
  INTO   newno
  FROM   grhum.INDIVIDU_ULR
  WHERE  pers_id = newid;
  
  -- **********
  -- ACCORDS...
  -- **********
   Echanger_Personne_Accords(oldid,newid,oldno,newno);

  -- ************
  -- ADMISSION...
  -- ************
   Echanger_Personne_Admission(oldid,newid,oldno,newno);

  -- **********
  -- CONGES...
  -- **********
  --Echanger_Personne_Conges(oldid,newid,oldno,newno);

  -- **********
  -- COURRIER...
  -- **********
  --Echanger_Personne_Courrier(oldid,newid,oldno,newno);

  -- **********
  -- CKTL_GED...
  -- **********
   Echanger_Personne_cktl_ged(oldid,newid,oldno,newno);

  -- **********
  -- DT...
  -- **********
  --Echanger_Personne_Dt(oldid,newid,oldno,newno);
  Echanger_Personne_Dt3(oldid,newid,oldno,newno);


  -- **********
  -- EDTSCOL
  -- **********
  Echanger_Personne_Edtscol(oldid,newid,oldno,newno);

  -- **********
  -- GED...
  -- **********
  Echanger_Personne_Ged(oldid,newid,oldno,newno);

  -- **********
  -- GRHUM...
  -- **********
  Echanger_Personne_Grhum(oldid,newid,oldno,newno);

  -- **********
  -- HCOMP2...
  -- **********
  Echanger_Personne_Hcomp2(oldid,newid,oldno,newno);

  -- **********
  -- INVENTAIRE...
  -- **********
  --Echanger_Personne_Inventaire(oldid,newid,oldno,newno);

  -- **********
  -- JEFY...
  -- **********
  --Echanger_Personne_Jefy(oldid,newid,oldno,newno);

  -- **********
  -- JEFY_ADMIN...
  -- **********
  Echanger_Personne_Jefy_Admin(oldid,newid,oldno,newno);

  -- **********
  -- JEFY_PAYE...
  -- **********
  Echanger_Personne_Jefy_Paye(oldid,newid,oldno,newno);


  -- **********
  -- MANGUE...
  -- **********
  Echanger_Personne_Mangue(oldid,newid,oldno,newno);

  -- **********
  -- MARACUJA...
  -- **********
  Echanger_Personne_Maracuja(oldid,newid,oldno,newno);

    -- **********
  -- PAPAYE
  -- **********
  -- Echanger_Personne_Papaye(oldid,newid,oldno,newno);

  -- **********
  -- PARC_MATOS...
  -- **********
   -- Echanger_Personne_Parc_Info(oldid,newid,oldno,newno);

  -- **********
  -- PEDA...
  -- **********
  Echanger_Personne_Peda(oldid,newid,oldno,newno);

  -- **********
  -- PRESTATION...
  -- **********
  Echanger_Personne_Prestation(oldid,newid,oldno,newno);

  -- **********
  -- SCOLARITE...
  -- **********
  Echanger_Personne_Scolarite(oldid,newid,oldno,newno);

  -- **********
  -- RESERVATION...
  -- **********
  Echanger_Personne_Reservation(oldid,newid,oldno,newno);

  -- **********
  -- SALLE...
  -- **********
  Echanger_Personne_Salle(oldid,newid,oldno,newno);

  -- **********
  -- BLOBS.PHOTO...
  -- **********
  --Echanger_Personne_Blobs(oldid,newid,oldno,newno);

  --

  -- Devalidation du trigger sur INDIVIDU_ULR
  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'ALTER TRIGGER grhum.trg_br_individu_ulr DISABLE;');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update GRHUM.INDIVIDU_ULR set tem_valide = ''N'' where no_individu = '||oldno||';');

  -- Re-validation du trigger sur INDIVIDU_ULR
  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'ALTER TRIGGER grhum.trg_br_individu_ulr ENABLE;');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'insert into GRHUM.ECHANGER_SAUVEGARDE select * from GRHUM.PERSONNE where pers_id  = '||oldid||';');

  INSERT INTO ECHANGER_COMPTE_RENDU
  VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update GRHUM.PERSONNE set pers_libelle = ''PERSONNE INVALIDE - Contacter le CRI'' where pers_id  = '||oldid||';');
END;
/


-- les procédures manquantes par user

create or replace PROCEDURE echanger_personne_cktl_ged
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
BEGIN

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   cktl_ged.DOCUMENT
  WHERE  PERS_ID = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update cktl_ged.DOCUMENT set PERS_ID = '||newid||' where PERS_ID = '||oldid||';');
  END IF;

END; 
/

create or replace PROCEDURE echanger_personne_admission
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
BEGIN

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ADMISSION.CANDIDAT_GRHUM
  WHERE  NO_INDIVIDU = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ADMISSION.CANDIDAT_GRHUM set NO_INDIVIDU = '||newno||' where NO_INDIVIDU = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ADMISSION.CANDIDAT_GRHUM
  WHERE  PERS_ID = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ADMISSION.CANDIDAT_GRHUM set PERS_ID = '||newid||' where PERS_ID = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ADMISSION.REPART_CANDIDAT_INDIVIDU
  WHERE  NO_INDIVIDU = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ADMISSION.REPART_CANDIDAT_INDIVIDU set NO_INDIVIDU = '||newno||' where NO_INDIVIDU = '||oldno||';');
  END IF;

END; 
/

create or replace PROCEDURE echanger_personne_accords
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
BEGIN

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.AVENANT_PART_CONTACT
  WHERE  NO_INDIVIDU = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.CONTRAT_PART_CONTACT set NO_INDIVIDU = '||newno||' where NO_INDIVIDU = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.CONTRAT_PART_CONTACT
  WHERE  PERS_ID = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.CONTRAT_PART_CONTACT set PERS_ID = '||newid||' where PERS_ID = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.CONTRAT_PART_CONTACT
  WHERE  PERS_ID_CONTACT = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.CONTRAT_PART_CONTACT set PERS_ID_CONTACT = '||newid||' where PERS_ID_CONTACT = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.AVENANT_PARTENAIRE
  WHERE  PERS_ID = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.AVENANT_PARTENAIRE set PERS_ID = '||newid||' where PERS_ID = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.CONTRAT_PART_CONTACT
  WHERE  NO_INDIVIDU = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.CONTRAT_PART_CONTACT set NO_INDIVIDU = '||newno||' where NO_INDIVIDU = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.AVENANT_PART_CONTACT
  WHERE  PERS_ID = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.AVENANT_PART_CONTACT set PERS_ID = '||newid||' where PERS_ID = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.AVENANT_PART_CONTACT
  WHERE  PERS_ID_CONTACT = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.AVENANT_PART_CONTACT set PERS_ID_CONTACT = '||newid||' where PERS_ID_CONTACT = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.CONTRAT_PARTENAIRE
  WHERE  PERS_ID = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.CONTRAT_PARTENAIRE set PERS_ID = '||newid||' where PERS_ID = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.PROJET
  WHERE  PERS_ID_RESPONSABLE = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.PROJET set PERS_ID_RESPONSABLE = '||newid||' where PERS_ID_RESPONSABLE = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   ACCORDS.REPART_INDIVIDU_EVT
  WHERE  NO_INDIVIDU = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update ACCORDS.REPART_INDIVIDU_EVT set NO_INDIVIDU = '||newno||' where NO_INDIVIDU = '||oldno||';');
  END IF;

  --

END; 
/

create or replace PROCEDURE echanger_personne_salle
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
BEGIN

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.hcomp_recup
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.hcomp_recup set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.indisponibilite_harp
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.indisponibilite_harp set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.occupant_harp
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.occupant_harp set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.occupant_harp_save
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.occupant_harp_save set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.periodicite_harp
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.periodicite_harp set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.periodicite_harp_save
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.periodicite_harp_save set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.pref_enseignement
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.pref_enseignement set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.reservation_harp
  WHERE  no_individu_client = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.reservation_harp set no_individu_client = '||newno||' where no_individu_client = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   salle.reservation_harp_save
  WHERE  no_individu_client = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update salle.reservation_harp_save set no_individu_client = '||newno||' where no_individu_client = '||oldno||';');
  END IF;

END; 
/

create or replace PROCEDURE echanger_personne_reservation
(
  oldid NUMBER,
  newid NUMBER,
  oldno NUMBER,
  newno NUMBER
)
IS
  nbenr1 INTEGER;
BEGIN

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.kiosque_black_list
  WHERE  kbl_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.kiosque_black_list set kbl_no_individu = '||newno||' where kbl_no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.kiosque_black_list
  WHERE  kbl_no_individu_fait_exclu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.kiosque_black_list set kbl_no_individu_fait_exclu = '||newno||' where kbl_no_individu_fait_exclu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.kiosque_black_list
  WHERE  kbl_no_individu_modif_fin = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.kiosque_black_list set kbl_no_individu_modif_fin = '||newno||' where kbl_no_individu_modif_fin = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.kiosque_droit_gestion
  WHERE  pers_id = oldid;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.kiosque_droit_gestion set pers_id = '||newid||' where pers_id = '||oldid||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.kiosque_list_attente
  WHERE  kla_no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.kiosque_liste_attente set kla_no_individu = '||newno||' where kla_no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.resa_depositaire_harp
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.resa_depositaire_harp set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.resa_occupant_harp
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.resa_occupant_harp set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.resa_planning_harp
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.resa_planning_harp set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

  --

  SELECT COUNT(*)
  INTO   nbenr1
  FROM   reservation.resp_groupe_agenda
  WHERE  no_individu = oldno;

  IF (nbenr1 > 0)
  THEN INSERT INTO ECHANGER_COMPTE_RENDU
       VALUES (seq_echanger.NEXTVAL,'Echanger '||oldid||','||newid,SYSDATE,
          'update reservation.resp_groupe_agenda set no_individu = '||newno||' where no_individu = '||oldno||';');
  END IF;

END; 
/


-- Modifictaion pour MANGUE
ALTER TABLE GRHUM.TYPE_DECHARGE_SERVICE ADD (TEM_HCOMP VARCHAR2(1) DEFAULT 'N' NOT NULL);

ALTER TABLE GRHUM.TYPE_DECHARGE_SERVICE ADD (D_OUVERTURE DATE DEFAULT TO_DATE('01/01/1900', 'dd/mm/yyyy') NOT NULL);

ALTER TABLE GRHUM.TYPE_DECHARGE_SERVICE ADD (D_FERMETURE DATE);

ALTER TABLE GRHUM.TYPE_DECHARGE_SERVICE ADD (TEM_VALIDE VARCHAR2(1) DEFAULT 'O' NOT NULL);

COMMENT ON COLUMN GRHUM.TYPE_DECHARGE_SERVICE.TEM_HCOMP IS 'La décharge autorise t elle la saisie d''heures complémentaires (O / N)';

COMMENT ON COLUMN GRHUM.TYPE_DECHARGE_SERVICE.D_OUVERTURE IS 'Début de validité du type de décharge';

COMMENT ON COLUMN GRHUM.TYPE_DECHARGE_SERVICE.D_FERMETURE IS 'Fin de validité du type de décharge';

COMMENT ON COLUMN GRHUM.TYPE_DECHARGE_SERVICE.TEM_VALIDE IS 'Validité du type de décharge';

ALTER TABLE GRHUM.TYPE_ABSENCE ADD (TEM_HCOMP VARCHAR2(1) default 'N' NOT NULL);

COMMENT ON COLUMN GRHUM.TYPE_ABSENCE.TEM_HCOMP IS 'Le type d''absence autorise t il la saisie d''heures complémentaires (O / N)';


--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.36', TO_DATE('22/05/2013', 'DD/MM/YYYY'),NULL,'MAJ de procédures echanger_personne_* manquantes, modification de la table GRHUM.COMPTE_TEMP, ajout de témoins pour MANGUE ');

--
-- 
--


COMMIT;




