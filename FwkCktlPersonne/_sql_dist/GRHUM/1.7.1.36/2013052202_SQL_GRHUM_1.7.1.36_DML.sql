--
-- Patch DML de GRHUM du 22/05/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.7.1.36
-- Date de publication : 22/05/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


update GRHUM.type_absence
SET tem_hcomp = 'O'
WHERE c_type_absence in ('MAL', 'CMATER', 'CGP', 'CGPA', 'CPATER', 'DEPA', 'DETA');

update GRHUM.type_decharge_service
SET tem_hcomp = 'O'
WHERE c_type_decharge in ('SY', 'DE2');

--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.36';

COMMIT;