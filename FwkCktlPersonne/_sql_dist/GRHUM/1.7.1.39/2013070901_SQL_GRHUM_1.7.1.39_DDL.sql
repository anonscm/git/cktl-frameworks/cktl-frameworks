--
-- Patch DML de GRHUM du 09/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.7.1.39 
-- Date de publication : 09/07/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
begin
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.38';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRHUM.db_version where dbv_libelle='1.7.1.39';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.7.1.39 a deja ete passe !');
    end if;

end;
/


-- --------------------------------------------------------------
-- Désactivation de contraintes sur la table GRHUM.STRUCTURE_ULR
-- --------------------------------------------------------------

-- Désactivation de contraintes sur la table Structure_ULR
ALTER TABLE GRHUM.structure_ulr disable CONSTRAINT UK_SIREN;
-- ALTER TABLE GRHUM.structure_ulr DROP CONSTRAINT UK_SIREN;
ALTER TABLE GRHUM.structure_ulr disable CONSTRAINT UK_SIRET;


-- ------------------------------------------------
-- Suppression en douceur de GRHUM.NBI_OCCUPATION
-- ------------------------------------------------

/*
  Si des soucis surviennent après la suppression de la table dans GRHUM, c'est que vous avez des vues ou autres qui pointaient sur cette table.
  Vous devez alors rediriger les liens vers la table MANGUE.NBI_OCCUPATION
*/
declare

   c integer;

begin

  select count(*) into c from user_tables where table_name = upper('NBI_OCCUPATION');

   if c <> 0 then
      execute immediate '
      DROP TABLE GRHUM.NBI_OCCUPATION CASCADE CONSTRAINTS    
      ';
   end if;
   select count(*) into c from user_sequences where sequence_name = upper('NBI_OCCUPATION_SEQ');

   if c <> 0 then
      execute immediate '
      DROP SEQUENCE GRHUM.NBI_OCCUPATION_SEQ
      ';
   end if;
   
end;
/

-- -------------------------
-- MAJ de GRHUM.TRG_COMPTE
-- -------------------------
create or replace
TRIGGER "GRHUM"."TRG_COMPTE"
BEFORE INSERT OR UPDATE ON GRHUM.COMPTE FOR EACH ROW
-- CRI
-- creation : 20/01/2004
-- modification : 13/05/2009
-- modification : 01/07/2013 prise en compte des domaines secondaires pour le VLAN P
-- 1er declencheur, de niveau ligne, qui n'interroge plus la table mutante
-- a la place, il stocke dans la table temporaire les donnees inserees
-- Creation d'une table temporaire vide de meme structure que COMPTE
-- CREATE GLOBAL TEMPORARY TABLE COMPTE_TEMP AS SELECT * FROM COMPTE WHERE 0=1;
DECLARE
   cpt              INTEGER;
   newlogin         COMPTE.cpt_login%TYPE;
   newemail         COMPTE.cpt_email%TYPE;
   newdomaine       COMPTE.cpt_domaine%TYPE;
   domaine_princ    COMPTE.CPT_DOMAINE%TYPE;
   vlan_admin       VLANS.C_VLAN%type;
   vlan_recherche   VLANS.C_VLAN%type;
   vlan_etudiant    VLANS.C_VLAN%type;
   vlan_externe     VLANS.C_VLAN%type;

   nb               integer;
   str_domaine      varchar2(2000);
   list_dom_sec     varchar2(2000);
   chaine           varchar2(2000);

	valeur_test		NUMBER(1);

BEGIN
   cpt := 0;
   newlogin := NULL;
   newemail := NULL;
   newdomaine := NULL;

   -- 03/10/2005 pamametrage des codes des VLANS
   select param_value into vlan_admin from grhum_parametres where param_key = 'GRHUM_VLAN_ADMIN';
   select param_value into vlan_recherche from grhum_parametres where param_key = 'GRHUM_VLAN_RECHERCHE';
   select param_value into vlan_etudiant from grhum_parametres where param_key = 'GRHUM_VLAN_ETUD';
   select param_value into vlan_externe from grhum_parametres where param_key = 'GRHUM_VLAN_EXTERNE';

   -- contraintes d'ingegrites entre le VLAN (not null) et le domaine
   SELECT COUNT(*) INTO cpt
   FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';

   IF (cpt != 0) THEN

         SELECT param_value INTO domaine_princ
      FROM GRHUM_PARAMETRES WHERE param_key='GRHUM_DOMAINE_PRINCIPAL';

       SELECT param_value INTO list_dom_sec
      FROM GRHUM_PARAMETRES WHERE param_key='org.cocktail.grhum.compte.domainessecondaires';

      -- regle d'integrite entre le VLAN P et la liste des domaines (principal + secondaires)
      IF list_dom_sec != 'univ.fr;'
      THEN
         if substr(list_dom_sec,length(ltrim(rtrim(list_dom_sec)))-1,1) = ';'
         then
            chaine := ltrim(rtrim(list_dom_sec));
         else
            chaine := ltrim(rtrim(list_dom_sec))||';';
         end if;
         
         chaine := ltrim(rtrim(chaine))||ltrim(rtrim(domaine_princ))||';';
         -- la valeur de 1 est pour lever une erreur
		 valeur_test := 1;	
		
         select length(ltrim(rtrim(chaine)))-length(replace(ltrim(rtrim(chaine)),';',''))+1 into nb from dual;

         for j in 0..nb-1 loop
            if j=0 then
               str_domaine := substr(chaine,1,instr(chaine,';',1,1)-1);
            else
               str_domaine := substr(chaine,instr(chaine,';',1,j)+1, instr(chaine,';',1,j+1) - instr(chaine,';',1,j)-1);
            end if;
            if ( :NEW.cpt_vlan = vlan_admin AND :NEW.cpt_domaine = str_domaine ) then
            	valeur_test := 0;
           	end if;
            --dbms_output.put_line(str_domaine);
         end loop;
         if (valeur_test != 0) then
           		RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et les domaines principal et secondaire(s) ');
           	end if;	
      ELSE
      	if ( :NEW.cpt_vlan = vlan_admin AND :NEW.cpt_domaine != domaine_princ ) then
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine principal '||domaine_princ||'.');
        end if;   
      END IF;



        -- regle d'integrite entre le VLAN R et le domaine principal
      IF ( :NEW.cpt_vlan = vlan_recherche AND :NEW.cpt_domaine != domaine_princ ) THEN
           RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine principal '||domaine_princ||'.');
      END IF;

     -- integrite des comptes ETUDIANTs seulement pour L.R.
        IF ( domaine_princ = 'univ-lr.fr' ) THEN
            IF ( (:NEW.cpt_vlan = vlan_etudiant) AND (:NEW.cpt_domaine NOT IN ('etudiant.univ-lr.fr','etudiut.univ-lr.fr')) ) THEN
                RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine etudiant ! ');
             END IF;
        END IF;

     -- integrite entre le VLAN eXterieur et le domaine
     IF ( (:NEW.cpt_vlan= vlan_externe) AND (:NEW.cpt_domaine = domaine_princ) ) THEN
            RAISE_APPLICATION_ERROR(-20000,'Trigger TRG_COMPTE : incompatibilite entre le VLAN "'||:NEW.cpt_vlan||'" et le domaine exterieur ! ');
     END IF;

   END IF;
   -- fin contraintes VLAN et domaine

   -- Conversion du login sans les accents
   IF (:NEW.cpt_login IS NOT NULL) THEN
        newlogin := Chaine_Sans_Accents(:NEW.cpt_login);
     :NEW.cpt_login := newlogin;
   END IF;

   -- Conversion du mail sans les accents
   IF (:NEW.cpt_email IS NOT NULL) THEN
        newemail := Chaine_Sans_Accents(:NEW.cpt_email);
     :NEW.cpt_email := newemail;
   END IF;

   -- Conversion du domaine sans les accents
   IF (:NEW.cpt_domaine IS NOT NULL) THEN
        newdomaine := Chaine_Sans_Accents(:NEW.cpt_domaine);
     :NEW.cpt_domaine := newdomaine;
   END IF;

   -- insertion du nouvel enregistrement dans la table temporaire pour ne pas avoir l'erreur ORA-04091 : table mutante
   -- cas du trigger before insert on COMPTE qui fait un SELECT sur la table COMPTE dans le meme trigger
   INSERT INTO COMPTE_TEMP(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CRYPTE,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,CPT_VALIDE,CPT_PRINCIPAL,CPT_LISTE_ROUGE,D_CREATION,D_MODIFICATION,TVPN_CODE)
   VALUES(:NEW.CPT_ORDRE,:NEW.CPT_UID_GID,:NEW.CPT_LOGIN,:NEW.CPT_PASSWD,:NEW.CPT_CRYPTE,:NEW.CPT_CONNEXION,:NEW.CPT_VLAN,:NEW.CPT_EMAIL,:NEW.CPT_DOMAINE,:NEW.CPT_CHARTE,:NEW.CPT_VALIDE,:NEW.CPT_PRINCIPAL,:NEW.CPT_LISTE_ROUGE,SYSDATE,SYSDATE,:NEW.TVPN_CODE);

END ;
/



-- ---------------------------------------
-- A destination de la Gestion des Droits
-- ---------------------------------------

DECLARE

	dom_id_application INTEGER;
	persIdCreateur   INTEGER;
	profilPereNum INTEGER;
	application_num INTEGER;
	numRegleSeq INTEGER;
	numGrpDyn INTEGER;
  numRegleOp INTEGER;
  numRegleNode INTEGER;
  numRegleClef INTEGER;
  
  pdfId INTEGER;
  prId INTEGER;
  fonId INTEGER;
  tdfId INTEGER;
  
  cStructure INTEGER;
  persId INTEGER;
  numIndividu INTEGER;
  cStructurePere INTEGER;
  
  cpt INTEGER;

BEGIN

	-- persIdCreation
   SELECT PERS_ID into persIdCreateur FROM GRHUM.COMPTE WHERE CPT_LOGIN = (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE = (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR'));


	-- Création du Groupe qui sera utilisé
	
	SELECT GRHUM.SEQ_STRUCTURE_ULR.NEXTVAL INTO cStructure FROM dual;
	SELECT GRHUM.SEQ_PERSONNE.NEXTVAL INTO persId FROM dual;
	SELECT NO_INDIVIDU INTO numIndividu FROM GRHUM.INDIVIDU_ULR WHERE PERS_ID=persIdCreateur;
	SELECT C_STRUCTURE INTO cStructurePere FROM GRHUM.STRUCTURE_ULR WHERE C_TYPE_STRUCTURE='E';

  select count(*) into cpt from GRHUM.STRUCTURE_ULR where lc_structure like 'GESTIONPROFILS';
   if cpt = 0 then
      
      INSERT INTO GRHUM.STRUCTURE_ULR(STR_PHOTO, C_STATUT_JURIDIQUE, SIREN, C_NAF, GRP_ACCES, ROM_ID, C_ACADEMIE, REF_DECISION, STR_DESCRIPTION, d_Creation, SIRET, d_Modification, STR_AFFICHAGE, TVA_INTRACOM, GRP_CA, TEM_VALIDE, DATE_DECISION, PERS_ID_CREATION, PERS_ID, C_TYPE_STRUCTURE, GRP_RESPONSABILITE, C_TYPE_DECISION_STR, GRP_FONCTION1, GRP_FONCTION2, LC_STRUCTURE, GRP_FORME_JURIDIQUE, GRP_CAPITAL, STR_STATUT, C_RNE, STR_RECHERCHE, STR_ACTIVITE, GRP_OWNER, C_STRUCTURE, DATE_FERMETURE, LL_STRUCTURE, DATE_OUVERTURE, C_STRUCTURE_PERE, GRP_RESPONSABLE, C_TYPE_ETABLISSEMEN, GRP_EFFECTIFS, GRP_MOTS_CLEFS, GRP_ALIAS, PERS_ID_MODIFICATION, GRP_APE_CODE)
        VALUES (NULL, NULL, NULL, NULL, '+', NULL, NULL, NULL, NULL, SYSDATE, NULL, SYSDATE, 'GestionProfils', NULL, NULL, 'O', NULL, persIdCreateur, persId, 'A', NULL, NULL, NULL, NULL, 'GESTIONPROFILS', NULL, NULL, NULL, NULL, NULL, NULL, numIndividu, cStructure, NULL, 'GESTIONPROFILS', NULL, cStructurePere, numIndividu, NULL, NULL, NULL, NULL, persIdCreateur, NULL);

      INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, D_CREATION, PERS_ID_CREATION, PERS_ID_MODIFICATION, tgrp_code, D_MODIFICATION)
        VALUES ( cStructure, SYSDATE, persIdCreateur, persIdCreateur, 'G', SYSDATE);
	end if;

	-- Définition d'un DOMAINE de Droits
  select count(*) into cpt from GRHUM.GD_DOMAINE where DOM_LC like 'REFERENTIEL';
   if cpt = 0 then
      INSERT INTO GRHUM.GD_DOMAINE(DOM_ID,DOM_LC) VALUES (GRHUM.GD_DOMAINE_SEQ.NEXTVAL, 'REFERENTIEL');
   end if;

	-- Définition de l'application utilisant ce domaine de Droits
  select count(*) into cpt from GRHUM.GD_APPLICATION where APP_STR_ID like 'GESTION DES DROITS';
   if cpt = 0 then
      SELECT dom_id INTO dom_id_application FROM GRHUM.GD_DOMAINE WHERE dom_lc LIKE 'REFERENTIEL';
	
      INSERT INTO GRHUM.GD_APPLICATION(APP_ID, DOM_ID, APP_LC, APP_STR_ID)
      	VALUES(GRHUM.GD_APPLICATION_SEQ.NEXTVAL, dom_id_application, 'GESTION DES DROITS', 'GESTION DES DROITS');
	 end if;	

	-- Définition de la Fonction lié à ce profil
	-- Cette fonction permettra d'accéder à la Gestion des droits et de gérer les profils
  select count(*) into cpt from GRHUM.GD_FONCTION where FON_ID_INTERNE like 'GESTDROIT';
   if cpt = 0 then
      SELECT app_id INTO application_num FROM GRHUM.GD_APPLICATION WHERE app_lc LIKE 'GESTION DES DROITS';
	
      INSERT INTO GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        VALUES(GRHUM.GD_FONCTION_SEQ.NEXTVAL, application_num, 'Droits','GESTDROIT', 'Droit à la gestion des profils', 'Droit d''accès à la gestion des droits');
		end if;
	
	-- Définition du Profil
  
  
	SELECT PR_ID INTO profilPereNum FROM GRHUM.GD_PROFIL WHERE PR_LC = 'Utilisateur du SI';
	SELECT GRHUM.REGLE_SEQ.NEXTVAL INTO numRegleSeq  FROM DUAL;
	SELECT GRHUM.GROUPE_DYNAMIQUE_SEQ.NEXTVAL INTO numGrpDyn FROM DUAL;
	  
  select count(*) into cpt from GRHUM.GD_PROFIL where PR_LC like 'Profil de gestionnaire de droits';
   if cpt = 0 then
      INSERT INTO GRHUM.GD_PROFIL(PR_ID, PR_PERE_ID, GRPD_ID, DATE_CREATION, DATE_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION, PR_LC, PR_DESCRIPTION, PR_REMARQUES)
        VALUES(GRHUM.GD_PROFIL_SEQ.NEXTVAL, profilPereNum, numGrpDyn, SYSDATE, SYSDATE, persIdCreateur, persIdCreateur, 'Profil de gestionnaire de droits', 'Profils des personnes à accéder à la gestion des droits', 'Profil sans restriction de domaine ou d''application');	
	 end if;
  
  
  
  
  	SELECT RO_ID INTO numregleop FROM GRHUM.regle_operateur WHERE ro_str_id = 'membre';
  	SELECT RK_ID INTO numregleclef FROM GRHUM.regle_key WHERE rk_str_id='user';
  	SELECT RN_ID INTO numreglenode FROM GRHUM.regle_node WHERE rn_node='SIMPLE';
    
  select count(*) into cpt from GRHUM.REGLE where R_VALUE=cStructure and R_VALUE_2=null ;
   if cpt = 0 then
      INSERT INTO GRHUM.REGLE(R_ID, R_PERE_ID, RN_ID, RK_ID, RO_ID, DATE_CREATION, DATE_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION, R_VALUE, R_VALUE_2)
        VALUES (numRegleSeq, null, numRegleNode, numRegleClef, numRegleOp, SYSDATE, SYSDATE, persIdCreateur, persIdCreateur, cStructure , null);
		
      INSERT INTO GRHUM.GROUPE_DYNAMIQUE(R_ID, GRPD_ID, GRPD_LC, GRPD_DESCRIPTION)
        VALUES (numRegleSeq, numGrpDyn, 'Groupe dynamique pour profils', 'Groupe dynamique pour profils');
	 end if;
	
	SELECT GRHUM.GD_PROFIL_DROIT_FONCTION_SEQ.NEXTVAL INTO pdfId FROM DUAL;
	SELECT PR_ID INTO prId FROM GRHUM.GD_PROFIL WHERE PR_LC='Profil de gestionnaire de droits';
	SELECT FON_ID INTO fonId FROM GRHUM.GD_FONCTION WHERE FON_ID_INTERNE='GESTDROIT';
	SELECT TDF_ID INTO tdfId FROM GRHUM.GD_TYPE_DROIT_FONCTION WHERE TDF_LL='Utilisation';
	
	INSERT INTO GRHUM.GD_PROFIL_DROIT_FONCTION(PDF_ID, PR_ID, FON_ID, TDF_ID, DATE_CREATION, DATE_MODIFICATION, PERS_ID_CREATION)
		VALUES (pdfId, prId, fonId, tdfId, SYSDATE, SYSDATE, persIdCreateur);

END;
/





--
-- DB_VERSION
--
INSERT INTO GRHUM.DB_VERSION VALUES(GRHUM.DB_VERSION_SEQ.NEXTVAL, '1.7.1.39', TO_DATE('09/07/2013', 'DD/MM/YYYY'),NULL,'MAJ du TRG_COMPTE, désactivation de contraintes sur Structure_ULR, suppression NBI_OCCUPATION, ajout de profils ');

--
-- 
--
--
-- DB_VERSION
--
UPDATE GRHUM.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='1.7.1.39';

COMMIT;




