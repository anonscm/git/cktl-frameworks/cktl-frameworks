create or replace
function        mangue.get_c_grade_Agent
(

  noindividu INTEGER)
  return VARCHAR2
/*
  Fonction qui renvoie le code grade actuel (contractuel ou titulaire) d'un agent ou null si inexistant
  Auteur : Cocktail
  creation : 14/12/2011
*/

IS
  --  CONTRAT
  CURSOR c1
  IS SELECT *
     FROM   MANGUE.CONTRAT
     WHERE  ((d_fin_contrat_trav IS NULL) OR (d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')))
     AND    d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')
     AND    tem_annulation = 'N'
     AND    no_dossier_pers = noindividu;

  -- CARRIERE
  CURSOR c2
  IS SELECT *
     FROM   MANGUE.ELEMENT_CARRIERE
     WHERE  ((d_fin_element IS NULL) OR (d_fin_element >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')))
     AND    d_effet_element <= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')
     AND    tem_valide = 'O'
     AND    no_dossier_pers = noindividu;

  cursor c3
  is select * 
     from mangue.contrat_heberges
     where ((D_FIN_CONTRAT_INV IS NULL) OR (D_FIN_CONTRAT_INV >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')))
     AND    D_DEB_CONTRAT_INV <= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')
     AND    tem_valide = 'O'
     AND    no_dossier_pers = noindividu;

  -- variables
  res           INTEGER;
  l_contrat     MANGUE.CONTRAT%ROWTYPE;
  l_element     MANGUE.ELEMENT_CARRIERE%ROWTYPE;
  l_contrat_heberges     MANGUE.contrat_heberges%ROWTYPE;
  sexe          GRHUM.CIVILITE.SEXE%TYPE;
  laSituation   VARCHAR2(2000);
  llTypeContrat GRHUM.TYPE_CONTRAT_TRAVAIL.LL_TYPE_CONTRAT_TRAV%TYPE;
  cGrade        GRHUM.GRADE.C_GRADE%TYPE;
  llGrade       GRHUM.GRADE.LL_GRADE%TYPE;
  cEchelon      MANGUE.CONTRAT_AVENANT.C_ECHELON%TYPE;
  ECHELON       VARCHAR2(30);
  le_code_grade        GRHUM.GRADE.C_GRADE%TYPE;

BEGIN

  le_code_grade := '';

  -- partie CONTRAT
  OPEN c1;
  LOOP
     FETCH c1 INTO l_contrat;
     EXIT WHEN c1%NOTFOUND;


     -- Grade 
     SELECT count(*) into res FROM MANGUE.CONTRAT
     WHERE  ((d_fin_contrat_trav IS NULL) OR (d_fin_contrat_trav >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')))
     AND    d_deb_contrat_trav <= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')
      AND     NO_SEQ_CONTRAT = l_contrat.NO_SEQ_CONTRAT
     AND    tem_annulation = 'N';
     
     if (res != 0) then
     
         SELECT C_GRADE INTO cGrade FROM MANGUE.CONTRAT_AVENANT
         WHERE  ((d_fin_contrat_av IS NULL) OR (d_fin_contrat_av >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')))
         AND    d_deb_contrat_av <= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')
          AND     NO_SEQ_CONTRAT = l_contrat.NO_SEQ_CONTRAT
         AND    tem_annulation = 'N';
    else

        laSituation := 'Le dossier actuel du contrat n''est pas jour.';
    
    end if;

  END LOOP;
  CLOSE c1;

  if ((cGrade is not null ) or (cGrade >0) )
  then return cGrade;
  end if;
  
  -- partie CARRIERE
  OPEN c2;
  LOOP
        FETCH c2 INTO l_element;
      EXIT WHEN c2%NOTFOUND;

      cGrade := l_element.c_grade;
    
   END LOOP;
   CLOSE c2;

  if ((cGrade is not null ) or (cGrade >0) )
  then return cGrade;
  end if;

  OPEN c3;
  LOOP
        FETCH c3 INTO l_contrat_heberges;
      EXIT WHEN c3%NOTFOUND;

      cGrade := l_contrat_heberges.c_grade;
      --SELECT LL_GRADE INTO llGrade FROM GRHUM.GRADE WHERE C_GRADE = l_element.c_grade;
    
   END LOOP;
   CLOSE c3;

  RETURN cGrade;

END;