/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORegle;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSSet;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

/**
 * Représente un utilisateur d'application pour le frmk personne.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class PersonneApplicationUser extends ApplicationUser {

	private static final String OUI = "O";
	public static final String TYAP_STR_ID_ANNUAIRE = "ANNUAIRE";

	public static final String FON_ID_CREATION_FOURNISSEUR = "ANFOUCR";
	public static final String FON_ID_VALIDATION_FOURNISSEUR = "ANFOUVAL";
	public static final String FON_ID_VISUALISATION_FOURNISSEUR = "ANFOUCS";

	public static final String PERS_ID_CURRENT_USER_STORAGE_KEY = "persIdCurrentUser";
	public static final String LOGIN_CURRENT_USER_STORAGE_KEY = "loginCurrentUser";
	
	private IPersonne iPersonne = null;
	private Boolean hasDroitGrhumCreateur = null;

	private Logger logger = Logger.getLogger(PersonneApplicationUser.class);

	private static final String HAS_DROIT_CREATION_EOFOURNIS_KEY = "hasDroitCreationEOFournis";
	private static final String HAS_DROIT_VALIDATION_EOFOURNIS_KEY = "hasDroitValidationEOFournis";
	private static final String HAS_DROIT_VISUALISATION_EOFOURNIS_KEY = "hasDroitVisualisationEOFournis";
	private static final String AGENT_ADRESSE_KEY = "agentAdresse";
	
	/**
	 * Liste des c_structure des groupes dont les membres ont le droit d'accéder à une application ou une fonctionnalité.
	 */
	private NSSet<String> cStructuresAcces;
	
	
	

	//Cache simpliste
	private Map<String, Boolean> _cacheDroits = new HashMap<String, Boolean>();
	private Map<String, Object> _cacheDonnees = new HashMap<String, Object>();

	public PersonneApplicationUser(EOEditingContext ec, EOUtilisateur utilisateur) {
		super(ec, utilisateur);
	}

	public PersonneApplicationUser(EOEditingContext ec, Integer persId) {
		super(ec, persId);
	}

	public PersonneApplicationUser(EOEditingContext ec, String tyapStrId, EOUtilisateur utilisateur) {
		super(ec, tyapStrId, utilisateur);
	}

	public PersonneApplicationUser(EOEditingContext ec, String tyapStrId, Integer persId) {
		super(ec, tyapStrId, persId);
	}

	/**
	 * @return L'objet IPersonne identifiant l'utilisateur.
	 */
	public IPersonne iPersonne() {
		if (iPersonne == null) {
			if (getPersonne() != null) {
				if (getPersonne().isStructure()) {
					iPersonne = EOStructure.structurePourCode(getEditingContext(), String.valueOf(getPersonne().persOrdre()));
				}
				else {
					iPersonne = EOIndividu.individuPourNumero(getEditingContext(), getPersonne().persOrdre());
				}
			}
		}
		return iPersonne;
	}

	/**
	 * @return L'objet agentAdresse associé a l'utilisateur en cours. Permet d'avoir des autorisations concernant l'annuaire.
	 */
	public EOAgentAdresse getAgentAdresse() {
		if (!_cacheDonnees.containsKey(AGENT_ADRESSE_KEY)) {
			_cacheDonnees.put(AGENT_ADRESSE_KEY, EOAgentAdresse.fetchByKeyValue(getEditingContext(), EOAgentAdresse.NO_INDIVIDU_KEY, getPersonne().persOrdre()));
		}
		return (EOAgentAdresse) _cacheDonnees.get(AGENT_ADRESSE_KEY);
	}

	/**
	 * @return true si l'utilisateur a le droit de viualiser des comptes (différents du vlan X)
	 */
	public boolean hasDroitCompteVisualisation() {
		if (getPersonne() == null || EOPersonne.PERS_TYPE_STR.equals(getPersonne().persType())) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtCompte())));
	}

	/**
	 * @return true si l'utilisateur a le droit de modifier des comptes (différents du vlan X)
	 */
	public boolean hasDroitCompteModification() {
		if (getPersonne() == null || EOPersonne.PERS_TYPE_STR.equals(getPersonne().persType())) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtCompteModifs())));
	}
	
	/**
	 * @return true si l'utilisateur a le droit de supprimer des comptes (différents du vlan X)
	 */
	public boolean hasDroitCompteSuppression() {
		if (getPersonne() == null || EOPersonne.PERS_TYPE_STR.equals(getPersonne().persType())) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtCompteSuppression())));
	}

	/**
	 * @deprecated
	 */
	public boolean hasDroitCompteModifiation() {
		return hasDroitCompteModification();
	}

	/**
	 * @return true si l'utilisateur a tous les droits sur l'annuaire (n'inclue pas ceux concernant la gestion des fournisseurs).
	 */
	public boolean hasDroitTous() {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		return (hasDroitGrhumCreateur() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtTout())));
	}

	/**
	 * @return true si l'utilisateur a le droit GRHUM_CREATEUR (super admin).
	 */
	public boolean hasDroitGrhumCreateur() {
		if (hasDroitGrhumCreateur == null) {
			NSArray res = EOGrhumParametres.parametresPourCle(getEditingContext(), EOGrhumParametres.PARAM_GRHUM_CREATEUR);
			NSArray logins = getLogins();
			hasDroitGrhumCreateur = Boolean.valueOf(NSArrayCtrl.intersectionOfNSArray(res, logins).count() > 0);
		}
		return hasDroitGrhumCreateur;
	}

	/**
	 * @return true si l'utilisateur a le droit d'ajouter une personne. IL s'agit de la gestion des droits ancienne mode (qui se base sur la table
	 *         agentAdresse).
	 */
	public boolean hasDroitAjoutPersonne() {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtAjout())));
	}

	/**
	 * @return true si l'utilisateur a le droit de voir la photo d'une personne. IL s'agit de la gestion des droits ancienne mode (qui se base sur la
	 *         table agentAdresse).
	 */
	public boolean hasDroitPhotoVisualisation() {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtPhoto())));
	}

	/**
	 * @return true si l'utilisateur a le droit de voir les informations personnelles d'une personne. IL s'agit de la gestion des droits ancienne mode
	 *         (qui se base sur la table agentAdresse).
	 */
	public boolean hasDroitVoirInfosPerso(IPersonne personne) {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		if (hasDroitTous()) {
			return true;
		}
		if (ERXEOControlUtilities.eoEquals(this.iPersonne(), personne) == true) {
			return true;
		}
		if (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtVoirInfosPerso())) {
			NSArray<EOStructure> groupesAdministres = this.iPersonne().getPersonneDelegate().getGroupesAdministres();
			if (personne.toRepartStructures().isEmpty()) {
				return true;
			}
			for (EOStructure groupe : groupesAdministres) {
				if (EOStructureForGroupeSpec.isPersonneInGroupe(personne, groupe)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return true si l'utilisateur a le droit de visualiser toutes les infos sur les compte du vlan X.
	 */
	public boolean hasDroitCompteTempoVisualisation() {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtCptTempo())));
	}

	public boolean hasDroitPersonneInvalideVisualisation() {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtTemValide())));
	}

	public boolean hasDroitCivilitesModification() {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		return (hasDroitTous() || (getAgentAdresse() != null && OUI.equals(getAgentAdresse().agtCiviliteModifs())));
	}

	/**
	 * @return true si l'utilisateur a le droit de visualiser tous les groupes privés (actuellement = tous les droits)
	 */
	public boolean hasDroitAllGroupesPrivesVisualisation() {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		return (hasDroitTous());
	}

	/**
	 * @return true si l'utilisateur a le droit de voir tous les types d'adresses (dont perso) :
	 *         <ul>
	 *         <li>s'il a tous les droits</li>
	 *         <li>s'il a le droit de creer des fournisseurs (non non non et non !)</li>
	 *         <li>s'il est lui-même</li>
	 *         </ul>
	 */
	public boolean hasDroitAllTypeAdresseVisualisation(IPersonne personne) {
		if (getPersonne() == null || !getPersonne().isIndividu()) {
			return false;
		}
		//return (hasDroitTous() || (personne != null && personne.persId().equals(getPersonne().persId())) || hasDroitCreationEOFournis(null, AUtils.currentExercice()));
		return (hasDroitTous() || (personne != null && personne.persId().equals(getPersonne().persId())));
	}

	public boolean hasDroitTypesAdressePersoVisualisation(IPersonne personne) {
		return hasDroitAllTypeAdresseVisualisation(personne) || hasDroitVoirInfosPerso(personne);
	}

	public boolean hasDroitTypesAdresseFacturationVisualisation(IPersonne personne) {
		return hasDroitAllTypeAdresseVisualisation(personne)
				|| getUtilisateur() != null
				|| hasDroitCreationEOFournis(null, AUtils.currentExercice());
	}

	public boolean hasDroitTypesAdresseProVisualisation(IPersonne personne) {
		return true;
		//return hasDroitAllTypeAdresseVisualisation(personne);
	}

	public boolean hasDroitTypesAdresseEtudiantVisualisation(IPersonne personne) {
		//FIXME gerer les droits pour les adresses de type etudiant
		return hasDroitAllTypeAdresseVisualisation(personne) || hasDroitCreationEOFournis(null, AUtils.currentExercice()) || hasDroitVoirInfosPerso(personne);
	}

	public boolean hasDroitTypesAdresseParentVisualisation(IPersonne personne) {
		//FIXME gerer les droits pour les adresses de type parent
		return hasDroitAllTypeAdresseVisualisation(personne) || hasDroitCreationEOFournis(null, AUtils.currentExercice()) || hasDroitVoirInfosPerso(personne);
	}

	/**
	 * @param typeAdresse Type d'adresse que l'utilisateur veut creer
	 * @param personne Personne pour laquelle l'utilisateur veut crééer l'adresse
	 * @return
	 */
	public boolean hasDroitCreationEOAdresseDeType(String typeAdresse, IPersonne personne) {
		//Si type adresse gere par Scolarité 
		//		if (EOTypeAdresse.TADR_CODE_PAR.equals(typeAdresse) || EOTypeAdresse.TADR_CODE_ETUD.equals(typeAdresse)) {
		//			return false;
		//		}
		//		else {
		//		return true;
		return hasDroitModificationIPersonne(personne);
		//		}
	}

	public boolean hasDroitModificationEOAdresseDeType(String typeAdresse, IPersonne personne) {
		return hasDroitCreationEOAdresseDeType(typeAdresse, personne);
	}

	public boolean hasDroitCreationEOAdresse(EOAdresse adresse) {
		//		boolean hasDroit = false;
		//		if (adresse != null && !adresse.toRepartPersonneAdresses().isEmpty()) {
		//			EORepartPersonneAdresse rpa = (EORepartPersonneAdresse) ERXArrayUtilities.firstObject(adresse.toRepartPersonneAdresses());
		//			hasDroit = hasDroitModificationIPersonne(rpa.toPersonne());
		//		}
		//		return hasDroit;
		//Les adresses peuvent etre reliees à aure chose que des personnes (ex. batiments)
		return true;
	}

	public boolean hasDroitModificationEOAdresse(EOAdresse adresse) {
		//		return true;
		return hasDroitCreationEOAdresse(adresse);
	}

	public boolean hasDroitCreationEOPersonneTelephone(IPersonne personne) {
		return hasDroitModificationIPersonne(personne);
	}

	public boolean hasDroitModificationEOPersonneTelephone(EOPersonneTelephone personneTelephone) {
		boolean hasDroit = false;
		if (personneTelephone != null && personneTelephone.toPersonne() != null) {
			hasDroit = hasDroitModificationIPersonne(personneTelephone.toPersonne());
		}
		return hasDroit;
	}

	public boolean hasDroitCreationEOFournis(IFournis fournis, String exercice) {
		if (_cacheDroits.get(HAS_DROIT_CREATION_EOFOURNIS_KEY) == null) {
			_cacheDroits.put(HAS_DROIT_CREATION_EOFOURNIS_KEY, (isFonctionAutoriseeByFonID(TYAP_STR_ID_ANNUAIRE, FON_ID_CREATION_FOURNISSEUR, exercice) || hasDroitValidationEOFournis(fournis, exercice)));
		}
		return _cacheDroits.get(HAS_DROIT_CREATION_EOFOURNIS_KEY);
	}

	public boolean hasDroitModificationEOFournis(IFournis fournis, String exercice) {
		if (fournis == null) {
			return false;
		}
		if (fournis.isFouValideValide()) {
			return hasDroitValidationEOFournis(fournis, exercice);
		}
		return hasDroitCreationEOFournis(fournis, exercice);
	}

	public boolean hasDroitValidationEOFournis(IFournis fournis, String exercice) {
		if (_cacheDroits.get(HAS_DROIT_VALIDATION_EOFOURNIS_KEY) == null) {
			_cacheDroits.put(HAS_DROIT_VALIDATION_EOFOURNIS_KEY, (isFonctionAutoriseeByFonID(TYAP_STR_ID_ANNUAIRE, FON_ID_VALIDATION_FOURNISSEUR, exercice)));
		}
		return _cacheDroits.get(HAS_DROIT_VALIDATION_EOFOURNIS_KEY);
	}

	public boolean hasDroitVisualisationEOFournis(EOFournis fournis, String exercice) {
		if (_cacheDroits.get(HAS_DROIT_VISUALISATION_EOFOURNIS_KEY) == null) {
			_cacheDroits.put(HAS_DROIT_VISUALISATION_EOFOURNIS_KEY, (isFonctionAutoriseeByFonID(TYAP_STR_ID_ANNUAIRE, FON_ID_VISUALISATION_FOURNISSEUR, exercice) || hasDroitCreationEOFournis(fournis, exercice) || hasDroitValidationEOFournis(fournis, exercice)));
		}
		return _cacheDroits.get(HAS_DROIT_VISUALISATION_EOFOURNIS_KEY);
		//		return isFonctionAutoriseeByFonID(TYAP_STR_ID_ANNUAIRE, FON_ID_VISUALISATION_FOURNISSEUR, exercice) || hasDroitCreationEOFournis(fournis, exercice) || hasDroitValidationEOFournis(fournis, exercice);
	}

	/**
	 * @param groupe
	 * @return true si l'utilisateur a le droit de modifier les elements du groupe.
	 */
	public boolean hasDroitGererGroupe(EOStructure groupe) {
		if (EOStructureForGroupeSpec.isGroupeReserve(groupe)) {
			return hasDroitGrhumCreateur();
		}
		//return (hasDroitTous() || this.iPersonne().getPersonneDelegate().getGroupesAdministres().indexOfObject(groupe) != NSArray.NotFound || (EOStructureForGroupeSpec.isGroupeGereEnAuto(groupe)));
		return (hasDroitTous() || CktlEOUtilities.isListContainsEo(this.iPersonne().getPersonneDelegate().getGroupesAdministres(), groupe));
	}

	/**
	 * @param groupe
	 * @return true si l'utilisateur a le droit de visualiser le groupe sélectionné.
	 */
	public boolean hasDroitVisualiserGroupe(EOStructure groupe) {

		if (EOStructureForGroupeSpec.isGroupeReserve(groupe)) {
			if (EOStructureForGroupeSpec.isGroupeOfType(groupe, EOTypeGroupe.TGRP_CODE_FO)) {
				return hasDroitGrhumCreateur() || hasDroitVisualisationEOFournis(null, AUtils.currentExercice());
			}
			return hasDroitGrhumCreateur();
		}
		return hasDroitGererGroupe(groupe);
	}
	/**
	 * Droit qui permet de voir les photos même celles marquées privée
	 * @return true si l'individu appartient à ce groupe
	 */
	public boolean hasDroitRhTrombi() {

		String cStructuresListe = null;
		if (FwkCktlPersonneParamManager.PARAM_RH_ADMIN != null && !MyStringCtrl.isEmpty(FwkCktlPersonneParamManager.PARAM_RH_ADMIN)) {
			cStructuresListe = FwkCktlPersonneParamManager.PARAM_RH_ADMIN;
		} else {
			return false;
		}
		
		if ("0".equals(cStructuresListe)) {
			// Cas par défaut dans lequel il n'y a pas de Groupe (ou Structure) sur lequel on pointe
			return false;
		}
		
		return isInGroupesAccess(cStructuresListe);

	}
	
	/**
	 * @return the cStructuresAcces : liste des cStructures dont les membres ont accés à une application ou une fonctionnalité
	 */
	private NSSet<String> getCStructuresAcces(String cStructureListe) {
		if (cStructuresAcces == null) {
			
			if (!MyStringCtrl.isEmpty(cStructureListe)) {
				cStructuresAcces = new NSSet<String>(cStructureListe.split(","));
			}
		}
		return cStructuresAcces;
	}
	
	/**
	 * 
	 * @return true si est un membre d'une des structures passées via la liste des c_structures
	 */
	public boolean isInGroupesAccess(String cStructureListe) {
		NSArray<String> cStructureAGrhumUsers = getCStructuresAcces(cStructureListe).allObjects();
		EOQualifier qualifier = ERXQ.is(EORepartStructure.PERS_ID_KEY,
				getPersId()).and(
				ERXQ.in(EORepartStructure.C_STRUCTURE_KEY,
						cStructureAGrhumUsers));

		return (EORepartStructure.fetchAll(getEditingContext(), qualifier)
				.count() > 0);
	}
	
	/**
	 * Droit de consultation des RIB d'une autre personne
	 * @param persId PersId de la personne qui veut consulter le RIB
	 * @return true si la personne a le droit Jefy_Admin
	 */
	public boolean hasPersonneUtilisateurDroitRibConsultation(Integer persId) {
		PersonneApplicationUser appUser = new PersonneApplicationUser(personne.editingContext(), persId);
		return appUser.hasDroitVisualisationEOFournis(null, AUtils.currentExercice());
	}
	
	/**
	 * @return true si l'utilisateur as des droits d'administration sur au moins un groupe.
	 */
	public boolean hasDroitGererAuMoinsUnGroupe() {
		return (hasDroitTous() || iPersonne().getPersonneDelegate().getGroupesAdministres().count() > 0);
	}

	/**
	 * @param tgrpCode
	 * @return true si l'utilisateur as des droits d'administration sur au moins un groupe du type specifie.
	 */
	public boolean hasDroitGererAuMoinsUnGroupeOfType(String tgrpCode) {
		return (hasDroitTous() || iPersonne().getPersonneDelegate().getGroupesAdministresOfType(tgrpCode).count() > 0);
	}

	/**
	 * Verifie si l'utilisateur le droit de modifier la personne (structure ou individu)).<br>
	 * Si la personne est l'utilisateur lui-même il a le droit. <br>
	 * Si la personne est une structure/groupe et si l'utilisateur est administrateur du groupe, il a le droit de modifier (si la structure est un
	 * sous-groupe d'un des groupes administrés par la personne ca marche aussi).<br>
	 * Sinon on part du principe qu'une personne est systématiquement affectée à au moins un groupe de type référentiel.
	 * <ul>
	 * <li>Si l'utilisateur est la personne elle-meme, elle a le droit (attention dans le cas des applications RH)</li>
	 * <li>Si le groupe est le groupe par defaut (divers), l'utilisateur a le droit de modifier (obligatoire pour toutes les affectations auto)</li>
	 * <li>si le seul groupe de type réferentiel affecté à la personne est le groupe referentiel par defaut, alors l'utilisateur doit être
	 * administrateur d'au moins un groupe de type référentiel.</li>
	 * <li>Sinon (la personne est affectée a plusieurs groupes de type référentiel), l'utilisateur doit être administrateur dans un des groupes
	 * referentiels affectes à la personne.</li>
	 * </ul>
	 * 
	 * @param personne
	 * @return true si l'utilisateur a le droit de modifier la personne
	 */
	public boolean hasDroitModificationIPersonne(IPersonne personne) {
		//try {
		if (personne == null) {
			logger.error("La personne est nulle");
			return false;
		}
		if (personne.editingContext() == null) {
			logger.error(personne.getNomEtId() + " : editingContext est null");
			return false;
		}
		if (personne.hasTemporaryGlobalID()) {
			return true;
		}
		if (this.iPersonne().persId().equals(personne.persId()) || hasDroitTous()) {
			return true;
		}
		if (EOStructureForGroupeSpec.getGroupeDefaut(personne.editingContext()).globalID().equals(personne.globalID())) {
			return true;
		}
		//Si la personne est une structure/groupe et si l'utilisateur est administrateur du groupe ou de ses peres, 
		//il a le droit
		if (personne.isStructure()) {
			if (this.iPersonne().getPersonneDelegate().isGroupeOrPeresInGroupesAdministres((EOStructure) personne)) {
				return true;
			}
		}

		// Si la personne est un individu et que si l'utilisateur est administrateur d'un groupe (ou un de ses peres)
		// auquel l'utilisateur est affecté, il a le droit sauf si on laisse la priorité au service RH

		if (personne.isIndividu()) {
			if (this.iPersonne().getPersonneDelegate().isGroupesAffectesPourIndividusOrPeresInGroupesAdministres((EOIndividu) personne)) {
				return true;
			}
		}

		//si la personne en cours est affectee uniquement au groupe referentiel "defaut", l'utilisateur doit etre administrateur d'au moins un groupe de type referentiel.
		boolean res = false;
		NSArray groupesReferentielsAffectes = personne.getPersonneDelegate().getAllGroupesAffectesOfType(EOTypeGroupe.TGRP_CODE_RE);
		if (groupesReferentielsAffectes.count() == 1 && EOStructureForGroupeSpec.getGroupeDefaut(personne.editingContext()).equals(groupesReferentielsAffectes.objectAtIndex(0))) {
			if (this.iPersonne().getPersonneDelegate().getGroupesAdministresOfType(EOTypeGroupe.TGRP_CODE_RE).count() > 0) {
				res = true;
			}
		}

		//Sinon, l'utilisateur doit etre administrateur dans un des groupes Referentiels affectes à la personne (
		//l'utilisateur peut etre administrateur dans l'arborescence (un des peres d'un groupe affecte)
		//FIXME : Rod 24/11/2011 une fois le patch qui s'assure que chaque personne (indiv ou structure) est passé dans au moins un groupe RE a été diffusé, virer la recherce dans l'arobrescence. Sinon certaines personnes ont trop de droits suivant les cas...
		else {
			NSArray groupesAdministres = this.iPersonne().getPersonneDelegate().getGroupesAdministresOfType(EOTypeGroupe.TGRP_CODE_RE);
			//res = (MyNSNSArrayCtrl.intersectionOfNSArray(groupesReferentielsAffectes, groupesAdministres).count() > 0);
			NSMutableArray grpAffectesWork = ((NSArray) personne.toRepartStructures().valueForKey(EORepartStructure.TO_STRUCTURE_GROUPE_KEY)).mutableClone();//groupesReferentielsAffectes.mutableClone();
			NSMutableArray grpReferentielsAffectesWork = groupesReferentielsAffectes.mutableClone();
			EOQualifier qualifier = new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_RE);
			for (Object strOccur : grpAffectesWork) {
				grpReferentielsAffectesWork = addPereToArray(grpReferentielsAffectesWork, (EOStructure) strOccur, qualifier);
			}
			res = (NSArrayCtrl.intersectionOfNSArray(grpReferentielsAffectesWork, groupesAdministres).count() > 0);
		}

		//si toujours pas trouvé de droit, il reste la possibilité des fonctions /groupes réservés
		if (!res) {
			EOFournis fournis = (EOFournis) personne.toFournis();
			if (fournis != null) {
				if (hasDroitModificationEOFournis(fournis, AUtils.currentExercice())) {
					res = true;
				}
			}
		}

		return res;
		/*
		 * } catch (Exception e) { e.printStackTrace(); return false; }
		 */
	}

	/**
	 * Verifie que l'utilisateur a le droit de creer une personne.
	 * 
	 * @param personne La personne a creer si elle existe deja comme objet en memoire. Facultatif.
	 * @return true si l'utilisateur a le droit de creer une personne.
	 */
	public boolean hasDroitCreationIPersonne(IPersonne personne) {
		if (hasDroitTous() || hasDroitAjoutPersonne()) {
			return true;
		}

		if (hasDroitGererAuMoinsUnGroupeOfType(EOTypeGroupe.TGRP_CODE_RE)) {
			return true;
		}
		//Si droit de creer un fournisseur, c'est ok
		if (this.hasDroitCreationEOFournis((personne != null ? personne.toFournis() : null), MyDateCtrl.getAnnee(MyDateCtrl.getDateJour()))) {
			return true;
		}

		//FIXME si personne particuliere, verifier qu'elle est dans un groupe administre par l'utilisateur

		return false;
	}

	/**
	 * @return true si l'utilisateur a le droit de creer une structure (= au moins un droit d'administration sur un groupe de type REF ou le droit
	 *         hasDroitAjoutPersonne ou le droit de creer des fournisseurs).
	 * @see PersonneApplicationUser#hasDroitCreationIPersonne(IPersonne)
	 */
	public boolean hasDroitCreerStructure(EOStructure personne) {
		// Pour l'instant pas de difference entre les droit de creation entre structures et individus.
		return hasDroitCreationIPersonne(personne);
		//return hasDroitAjoutPersonne() || hasDroitCreationIPersonne(personne) || hasDroitCreationEOFournis(null, MyDateCtrl.getAnnee(MyDateCtrl.getDateJour())) || hasDroitGererAuMoinsUnGroupeOfType(EOTypeGroupe.TGRP_CODE_RE);
	}

	/**
	 * @return true si l'utilisateur a le droit de creer un individu (= au moins un droit d'administration sur un groupe de type REF ou le droit
	 *         hasDroitAjoutPersonne ou le droit de creer des fournisseurs).
	 * @see PersonneApplicationUser#hasDroitCreationIPersonne(IPersonne)
	 */
	public boolean hasDroitCreerIndividu(EOIndividu personne) {
		//Pour l'instant pas de difference entre les droit de creation entre structures et individus.
		return hasDroitCreationIPersonne(personne);
		//return hasDroitAjoutPersonne() || hasDroitCreationIPersonne(personne) || hasDroitCreationEOFournis(null, MyDateCtrl.getAnnee(MyDateCtrl.getDateJour())) || hasDroitGererAuMoinsUnGroupeOfType(EOTypeGroupe.TGRP_CODE_RE);
	}

	/**
	 * @param eoRib
	 * @return true si l'utilisateur a le droit de modifier le rib.
	 */
	public boolean hasDroitModificationEORib(EORib eoRib) {
		// verifier si droit modifier fournisseur associe.
		if (eoRib == null) {
			return false;
		}
		if (eoRib.toFournis() == null) {
			return false;
		}
		if (eoRib.toFournis().isFouValideValide()) {
			return hasDroitValidationEOFournis(eoRib.toFournis(), MyDateCtrl.getAnnee(MyDateCtrl.getDateJour()));
		}
		return hasDroitModificationEOFournis(eoRib.toFournis(), MyDateCtrl.getAnnee(MyDateCtrl.getDateJour()));
	}

	public boolean hasDroitCreationEORib(EORib eoRib) {
		// verifier si droit modifier fournisseur associe.
		if (eoRib == null) {
			return hasDroitCreationEOFournis(null, MyDateCtrl.getAnnee(MyDateCtrl.getDateJour()));
		}
		if (eoRib.toFournis() == null) {
			return false;
		}
		if (eoRib.toFournis().isFouValideValide()) {
			return hasDroitValidationEOFournis(eoRib.toFournis(), MyDateCtrl.getAnnee(MyDateCtrl.getDateJour()));
		}
		return hasDroitModificationEOFournis(eoRib.toFournis(), MyDateCtrl.getAnnee(MyDateCtrl.getDateJour()));
	}

	/**
	 * @return Les services dont depend l'utilisateur. (Tableau de EOStructure). L'utilisateur doit etre un individu.
	 */
	public NSArray getServices() {
		NSArray res = NSArray.EmptyArray;
		if (iPersonne() != null && (iPersonne() instanceof EOIndividu)) {
			res = ((EOIndividu) iPersonne()).getServices();
		}
		return res;
	}

	/**
	 * @return Les établissements affectes à l'utilisateur. Tableau de EOStructure). L'utilisateur doit etre un individu.
	 */
	public NSArray getEtablissementsAffectation() {
		NSArray res = NSArray.EmptyArray;
		if (iPersonne() != null && (iPersonne() instanceof EOIndividu)) {
			res = ((EOIndividu) iPersonne()).getEtablissementsAffectation(null);
		}
		return res;
	}

	private NSMutableArray addPereToArray(NSArray tbStr, EOStructure str, EOQualifier qualifier) {
		NSMutableArray retour = tbStr.mutableClone();
		while ((str.toStructurePere() != null) && (!str.equals(str.toStructurePere()))) {
			str = str.toStructurePere();
			if ((str.toRepartTypeGroupes(qualifier).count() > 0) && (!retour.containsObject(str)))
				retour.addObject(str);
		}
		return retour;
	}

	public boolean hasDroitCreationProfil(EOGdProfil eoGdProfil) {
		return true;
	}

	public boolean hasDroitModificationProfil(EOGdProfil eoGdProfil) {
		return true;
	}

	public boolean hasDroitSuppressionProfil(EOGdProfil eoGdProfil) {
		return true;
	}

	public boolean hasDroitSuppressionRegle(EORegle eoGdProfilRegle) {
		return false;
	}

	public boolean hasDroitCreationRegle(EORegle eoGdProfilRegle) {
		return true;
	}

	public boolean hasDroitModificationRegle(EORegle eoGdProfilRegle) {
		return false;
	}

	public boolean isSamePersonne(IPersonne personne) {
		return ((personne != null) && (this.iPersonne().persId().equals(personne.persId())));
	}
}
