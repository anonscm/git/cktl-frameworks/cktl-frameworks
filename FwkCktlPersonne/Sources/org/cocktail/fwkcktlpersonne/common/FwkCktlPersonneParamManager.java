/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common;

import org.cocktail.fwkcktldroitsutils.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.foundation.ERXThreadStorage;

public class FwkCktlPersonneParamManager extends CktlParamManager {

	public static final String PERSONNE_OBJECTHASCHANGED_DESACTIVE = "org.cocktail.fwkcktlpersonne.personne.objecthaschanged.disabled";
	public static final String INDIVIDU_CHECK_COHERENCE_INSEE_DISABLED = "org.cocktail.fwkcktlpersonne.individu.checkcoherenceinsee.disabled";
	public static final String FOURNISSEUR_DEMANDEVALIDATION_MAIL_DISABLED = "org.cocktail.fwkcktlpersonne.fournisseur.demandevalidation.mail.disabled";
	public static final String CLIENT_DEMANDEVALIDATION_MAIL_DISABLED = "org.cocktail.fwkcktlpersonne.client.demandevalidation.mail.disabled";
	public static final String APPLICATION_MANGUE_INSTALLED = "org.cocktail.fwkcktlpersonne.grhumparametres.checkmangue.installed";
	public static final String APPLICATION_SCOLARIX_INSTALLED = "org.cocktail.fwkcktlpersonne.grhumparametres.checkscolarix.installed";
	public static final String APPLICATION_PROPAGATE_TO_SAMBA_ENABLED = "org.cocktail.fwkcktlpersonne.compte.propagatetosamba.enabled";
	public static final String COMPTE_PASSWORD_EN_CLAIR_ACTIVE = "org.cocktail.fwkcktlpersonne.compte.stockageenclair.enabled";
	public static final String NETTOYER_VILLE_NAISSANCE_DISABLED = "org.cocktail.fwkcktlpersonne.individu.nettoyervillenaissance.disabled";
	public static final String COMPTE_AFFICHAGE_RESTREINT_ACTIVE = "org.cocktail.fwkcktlpersonne.compte.affichagerestreint.enabled";
	
	public static final String COMPTE_PASSWORD_HISTORY_VLAN_X = "org.cocktail.fwkcktlpersonne.compte.pwdhistory.vlanX.enabled";
	public static final String COMPTE_PASSWORD_HISTORY_VLAN_P = "org.cocktail.fwkcktlpersonne.compte.pwdhistory.vlanP.enabled";
	public static final String COMPTE_PASSWORD_HISTORY_VLAN_E = "org.cocktail.fwkcktlpersonne.compte.pwdhistory.vlanE.enabled";
	public static final String COMPTE_PASSWORD_HISTORY_VLAN_R = "org.cocktail.fwkcktlpersonne.compte.pwdhistory.vlanR.enabled";
	public static final String COMPTE_PASSWORD_HISTORY_VLAN_G = "org.cocktail.fwkcktlpersonne.compte.pwdhistory.vlanG.enabled";
	public static final String COMPTE_PASSWORD_HISTORY_VLAN_D = "org.cocktail.fwkcktlpersonne.compte.pwdhistory.vlanD.enabled";
	
	public static final String COMPTE_GERE_PAR_GRHUM = "org.cocktail.fwkcktlpersonne.compte.issudegrhum.enabled";
	
	public static final String COMPTE_PASSWORD_GENERATOR_SWITCH = "org.cocktail.fwkcktlpersonne.compte.pwdgeneratorswitch.enabled";

	public static String PARAM_RH_ADMIN = "org.cocktail.personne.trombi.groupe.autorise";
	
	public static final String PARAM_GRHUM_DOMAINES_SECONDAIRES = "org.cocktail.grhum.compte.domainessecondaires";
	public static final String PARAM_GRHUM_COMPTE_HOME = "org.cocktail.grhum.compte.home";
	public static final String PARAM_GRHUM_COMPTE_SHELL = "org.cocktail.grhum.compte.shell";
	public static final String PARAM_GRHUM_DOMAINES_SECONDAIRES_SCOL = "org.cocktail.grhum.compte.domainessecondairesVlanE";
	public static final String PARAM_GRHUM_DOMAINES_SECONDAIRES_RECHERCHE = "org.cocktail.grhum.compte.domainessecondairesVlanR";
	
	
	public static final String PARAM_PHOTO_ONLYPUBLICATED_ACTIVE = "org.cocktail.fwkcktlpersonne.photo.onlypublicated.enabled";
	
	public static final String PARAM_FECHT_LIMIT_MAX = "org.cocktail.fwkcktlpersonne.recherche.max";
	
	public static final String PARAM_NOM_APPLICATION = "org.cocktail.configuration.nomapplication";
	public static final String PARAM_LOGIN_PATTERN = "ANNUAIRE_LOGIN_PATTERN";
	
	public static final String PARAM_ADRESSE_SUPPRESSION_ENABLED = "org.cocktail.fwkcktlpersonne.adresse.suppression.enabled";
	public static final String PARAM_ADRESSE_AJOUT_ENABLED = "org.cocktail.fwkcktlpersonne.adresse.ajout.enabled";
	
	public static final String PARAM_ANNEE_UNIVERSITAIRE = "GRHUM_ANNEE_UNIVERSITAIRE";
	public static final String PARAM_SCOL_INFOS_PERS_VISIBLE = "org.cocktail.inscription.personnesrch.infosperso.visible";
	
	public static final String PARAM_GRHUM_RH = "org.cocktail.fwkcktlpersonne.grhum.rh";
	public static final String PARAM_GRHUM_PEDA = "org.cocktail.fwkcktlpersonne.grhum.peda";
	public static final String PARAM_GRHUM_SATUT_FONCTION = "org.cocktail.fwkcktlpersonne.grhum.statutfonction";

	
	public static final String ANNUAIRE_FOU_ENCOURS_VALIDE_KEY = "ANNUAIRE_FOU_ENCOURS_VALIDE";
	public static final String ANNUAIRE_FOU_VALIDE_PHYSIQUE_KEY = "ANNUAIRE_FOU_VALIDE_PHYSIQUE";
	public static final String ANNUAIRE_FOU_VALIDE_MORALE_KEY = "ANNUAIRE_FOU_VALIDE_MORALE";
	public static final String ANNUAIRE_FOU_ARCHIVES_KEY = "ANNUAIRE_FOU_ARCHIVES";
	public static final String ANNUAIRE_FOU_VALIDE_EXTERNE_KEY = "ANNUAIRE_FOU_VALIDE_EXTERNE";
	public static final String ANNUAIRE_FOU_VALIDE_INTERNE_KEY = "ANNUAIRE_FOU_VALIDE_INTERNE";
	public static final String ANNUAIRE_STRUCTURE_DEFAUT_KEY = "ANNUAIRE_STRUCTURE_DEFAUT";
	public static final String ANNUAIRE_FOU_ENTREPRISE_KEY = "ANNUAIRE_FOU_ENTREPRISE";
	public static final String ANNUAIRE_ENTREPRISE_KEY = "ANNUAIRE_ENTREPRISE";
	
	private EOEditingContext ec = ERXEC.newEditingContext();

	public FwkCktlPersonneParamManager() {
		// NE PAS SUPPRIMER C'EST A GERER PLUS TARD AU CAS OÙ LE ObjectHasChanged soit utilisé en plus de RGRHUM

		//		getParamList().add(PERSONNE_OBJECTHASCHANGED_DESACTIVE);
		//		getParamComments().put(PERSONNE_OBJECTHASCHANGED_DESACTIVE, "Forcer à valider les objets métier");
		//		getParamDefault().put(PERSONNE_OBJECTHASCHANGED_DESACTIVE, "N");
		//		getParamTypes().put(PERSONNE_OBJECTHASCHANGED_DESACTIVE, EOGrhumParametresType.codeActivation);
		getParamList().add(FOURNISSEUR_DEMANDEVALIDATION_MAIL_DISABLED);
		getParamComments().put(FOURNISSEUR_DEMANDEVALIDATION_MAIL_DISABLED, "Désactiver l'envoi d'un mail de demande de validation lors de la création d'un fournisseur");
		getParamDefault().put(FOURNISSEUR_DEMANDEVALIDATION_MAIL_DISABLED, "N");
		getParamTypes().put(FOURNISSEUR_DEMANDEVALIDATION_MAIL_DISABLED, EOGrhumParametresType.codeActivation);

		getParamList().add(CLIENT_DEMANDEVALIDATION_MAIL_DISABLED);
		getParamComments().put(CLIENT_DEMANDEVALIDATION_MAIL_DISABLED, "Désactiver l'envoi d'un mail de demande de validation lors de la création d'un client");
		getParamDefault().put(CLIENT_DEMANDEVALIDATION_MAIL_DISABLED, "N");
		getParamTypes().put(CLIENT_DEMANDEVALIDATION_MAIL_DISABLED, EOGrhumParametresType.codeActivation);
		
//		getParamList().add(PARAM_RH_ADMIN);
//		getParamComments().put(PARAM_RH_ADMIN, "C_Structure du groupe RH autorisé à accéder");
//		getParamDefault().put(PARAM_RH_ADMIN, "0");
//		getParamTypes().put(PARAM_RH_ADMIN, EOGrhumParametresType.cStructure);
		
		
//		getParamList().add(APPLICATION_MANGUE_INSTALLED);
//		getParamComments().put(APPLICATION_MANGUE_INSTALLED, "L'application de RH est elle utilisée ? (OUI/NON) (Défaut : N). ");
//		getParamDefault().put(APPLICATION_MANGUE_INSTALLED, "N");
//		getParamTypes().put(APPLICATION_MANGUE_INSTALLED, EOGrhumParametresType.codeActivation);
//		
//		getParamList().add(APPLICATION_SCOLARIX_INSTALLED);
//		getParamComments().put(APPLICATION_SCOLARIX_INSTALLED, "L'application de scolarité est elle utilisée ? (OUI/NON) (Défaut : N). ");
//		getParamDefault().put(APPLICATION_SCOLARIX_INSTALLED, "N");
//		getParamTypes().put(APPLICATION_SCOLARIX_INSTALLED, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(COMPTE_PASSWORD_HISTORY_VLAN_X);
		getParamComments().put(COMPTE_PASSWORD_HISTORY_VLAN_X, "Activer/Désactiver l'historisation sur le Vlan X (Défaut : O). ");
		getParamDefault().put(COMPTE_PASSWORD_HISTORY_VLAN_X, "O");
		getParamTypes().put(COMPTE_PASSWORD_HISTORY_VLAN_X, EOGrhumParametresType.codeActivation);
		
		getParamList().add(COMPTE_PASSWORD_HISTORY_VLAN_E);
		getParamComments().put(COMPTE_PASSWORD_HISTORY_VLAN_E, "Activer/Désactiver l'historisation sur le Vlan E (Défaut : O). ");
		getParamDefault().put(COMPTE_PASSWORD_HISTORY_VLAN_E, "O");
		getParamTypes().put(COMPTE_PASSWORD_HISTORY_VLAN_E, EOGrhumParametresType.codeActivation);
		
		getParamList().add(COMPTE_PASSWORD_HISTORY_VLAN_P);
		getParamComments().put(COMPTE_PASSWORD_HISTORY_VLAN_P, "Activer/Désactiver l'historisation sur le Vlan P (Défaut : O). ");
		getParamDefault().put(COMPTE_PASSWORD_HISTORY_VLAN_P, "O");
		getParamTypes().put(COMPTE_PASSWORD_HISTORY_VLAN_P, EOGrhumParametresType.codeActivation);
		
		getParamList().add(COMPTE_PASSWORD_HISTORY_VLAN_R);
		getParamComments().put(COMPTE_PASSWORD_HISTORY_VLAN_R, "Activer/Désactiver l'historisation sur le Vlan R (Défaut : O). ");
		getParamDefault().put(COMPTE_PASSWORD_HISTORY_VLAN_R, "O");
		getParamTypes().put(COMPTE_PASSWORD_HISTORY_VLAN_R, EOGrhumParametresType.codeActivation);
		
		getParamList().add(COMPTE_PASSWORD_HISTORY_VLAN_G);
		getParamComments().put(COMPTE_PASSWORD_HISTORY_VLAN_G, "Activer/Désactiver l'historisation sur le Vlan G (Défaut : N). ");
		getParamDefault().put(COMPTE_PASSWORD_HISTORY_VLAN_G, "N");
		getParamTypes().put(COMPTE_PASSWORD_HISTORY_VLAN_G, EOGrhumParametresType.codeActivation);
		
		getParamList().add(COMPTE_PASSWORD_HISTORY_VLAN_D);
		getParamComments().put(COMPTE_PASSWORD_HISTORY_VLAN_D, "Activer/Désactiver l'historisation sur le Vlan D (Défaut : N). ");
		getParamDefault().put(COMPTE_PASSWORD_HISTORY_VLAN_D, "N");
		getParamTypes().put(COMPTE_PASSWORD_HISTORY_VLAN_D, EOGrhumParametresType.codeActivation);
		
		getParamList().add(EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY);
		getParamComments().put(EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY, "Valeur minimale du GID attribuée à un compte (Défaut : 1000). ");
		getParamDefault().put(EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY, "1000");
		getParamTypes().put(EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY, EOGrhumParametresType.valeurNumerique);
		
		getParamList().add(EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY);
		getParamComments().put(EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY, "Valeur maximale du GID attribuée à un compte (Défaut : 65000). ");
		getParamDefault().put(EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY, "65000");
		getParamTypes().put(EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY, EOGrhumParametresType.valeurNumerique);
		
		getParamList().add(EOGrhumParametres.PARAM_GRHUM_DEFAULT_UID_KEY);
		getParamComments().put(EOGrhumParametres.PARAM_GRHUM_DEFAULT_UID_KEY, "Valeur par défaut de l'UID (Défaut : 1000). ");
		getParamDefault().put(EOGrhumParametres.PARAM_GRHUM_DEFAULT_UID_KEY, "1000");
		getParamTypes().put(EOGrhumParametres.PARAM_GRHUM_DEFAULT_UID_KEY, EOGrhumParametresType.valeurNumerique);
		
		getParamList().add(NETTOYER_VILLE_NAISSANCE_DISABLED);
		getParamComments().put(NETTOYER_VILLE_NAISSANCE_DISABLED, "Activer/Désactiver le nettoyage des caratères accentués pour la ville de naissance(Défaut: N)");
		getParamDefault().put(NETTOYER_VILLE_NAISSANCE_DISABLED, "N");
		getParamTypes().put(NETTOYER_VILLE_NAISSANCE_DISABLED, EOGrhumParametresType.codeActivation);
		

		getParamList().add(COMPTE_AFFICHAGE_RESTREINT_ACTIVE);
		getParamComments().put(COMPTE_AFFICHAGE_RESTREINT_ACTIVE, "Activer/Désactiver l'affichage restreint du compte (Défaut : N).");
		getParamDefault().put(COMPTE_AFFICHAGE_RESTREINT_ACTIVE, "N");
		getParamTypes().put(COMPTE_AFFICHAGE_RESTREINT_ACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PARAM_PHOTO_ONLYPUBLICATED_ACTIVE);
		getParamComments().put(PARAM_PHOTO_ONLYPUBLICATED_ACTIVE, "Activer/Désactiver seulement les boutons de publication (Défaut : N).");
		getParamDefault().put(PARAM_PHOTO_ONLYPUBLICATED_ACTIVE, "N");
		getParamTypes().put(PARAM_PHOTO_ONLYPUBLICATED_ACTIVE, EOGrhumParametresType.codeActivation);
		

		getParamList().add(PARAM_GRHUM_DOMAINES_SECONDAIRES);
		getParamComments().put(PARAM_GRHUM_DOMAINES_SECONDAIRES, "Liste des domaines secondaires prévus pour le VLAN P séparés par un ; (Défaut : une chaîne  à changer qui est univ.fr si vous souhaitez un domaine secondaire).");
		getParamDefault().put(PARAM_GRHUM_DOMAINES_SECONDAIRES, "univ.fr");
		getParamTypes().put(PARAM_GRHUM_DOMAINES_SECONDAIRES, EOGrhumParametresType.texteLibre);
		
		getParamList().add(PARAM_GRHUM_DOMAINES_SECONDAIRES_SCOL);
		getParamComments().put(PARAM_GRHUM_DOMAINES_SECONDAIRES_SCOL, "Liste des domaines secondaires prévus pour le VLAN E séparés par un ; (Défaut : une chaîne  à changer qui est univ.fr si vous souhaitez un domaine secondaire).");
		getParamDefault().put(PARAM_GRHUM_DOMAINES_SECONDAIRES_SCOL, "univ.fr");
		getParamTypes().put(PARAM_GRHUM_DOMAINES_SECONDAIRES_SCOL, EOGrhumParametresType.texteLibre);
		
		getParamList().add(PARAM_GRHUM_DOMAINES_SECONDAIRES_RECHERCHE);
		getParamComments().put(PARAM_GRHUM_DOMAINES_SECONDAIRES_RECHERCHE, "Liste des domaines secondaires prévus pour le VLAN R séparés par un ; (Défaut : une chaîne  à changer qui est univ.fr si vous souhaitez un domaine secondaire).");
		getParamDefault().put(PARAM_GRHUM_DOMAINES_SECONDAIRES_RECHERCHE, "univ.fr");
		getParamTypes().put(PARAM_GRHUM_DOMAINES_SECONDAIRES_RECHERCHE, EOGrhumParametresType.texteLibre);
		
		
		getParamList().add(PARAM_FECHT_LIMIT_MAX);
		getParamComments().put(PARAM_FECHT_LIMIT_MAX, "Nombre maximal de recherche effectuée (Défaut : 40).");
		getParamDefault().put(PARAM_FECHT_LIMIT_MAX, "40");
		getParamTypes().put(PARAM_FECHT_LIMIT_MAX, EOGrhumParametresType.valeurNumerique);
		
		
		getParamList().add(PARAM_LOGIN_PATTERN);
		getParamComments().put(PARAM_LOGIN_PATTERN, "Caractères autorisés pour le login.");
		getParamDefault().put(PARAM_LOGIN_PATTERN, "^[a-z0-9._-]+");
		getParamTypes().put(PARAM_LOGIN_PATTERN, EOGrhumParametresType.listeCaracteres);

		getParamList().add(COMPTE_PASSWORD_GENERATOR_SWITCH);
		getParamComments().put(COMPTE_PASSWORD_GENERATOR_SWITCH, "Activer/Désactiver le switch sur le générateur de MdP avec début en lettre majuscule (Défaut: N)");
		getParamDefault().put(COMPTE_PASSWORD_GENERATOR_SWITCH, "N");
		getParamTypes().put(COMPTE_PASSWORD_GENERATOR_SWITCH, EOGrhumParametresType.codeActivation);

		
		getParamList().add(PARAM_SCOL_INFOS_PERS_VISIBLE);
		getParamComments().put(PARAM_SCOL_INFOS_PERS_VISIBLE, "Activer/Désactiver des informations d'un individu dans la recherche d'un individu (Défaut: O)");
		getParamDefault().put(PARAM_SCOL_INFOS_PERS_VISIBLE, "O");
		getParamTypes().put(PARAM_SCOL_INFOS_PERS_VISIBLE, EOGrhumParametresType.codeActivation);
		
		

		getParamList().add(COMPTE_GERE_PAR_GRHUM);
		getParamComments().put(COMPTE_GERE_PAR_GRHUM, "Activer/Désactiver le contrôle de tous les champs de COMPTE car le Référentiel est GRHUM (Défaut : O). ");
		getParamDefault().put(COMPTE_GERE_PAR_GRHUM, "O");
		getParamTypes().put(COMPTE_GERE_PAR_GRHUM, EOGrhumParametresType.codeActivation);
	}

	@Override
	public void checkAndInitParamsWithDefault() {
		//Recuperer un grhum_createur
		String cptLogin = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CREATEUR);
		if (cptLogin != null) {
			EOCompte cpt = EOCompte.compteForLogin(ec, cptLogin);
			if (cpt != null) {
				ERXThreadStorage.takeValueForKey(cpt.persId(), PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
			}
		}
		super.checkAndInitParamsWithDefault();
	}

	/**
	 * Cree un nouveau parametre de type EOGrhumParametresType.codeActivation
	 */
	@Override
	public void createNewParam(String key, String value, String comment) {
		createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);

	}

	@Override
	public void createNewParam(String key, String value, String comment,
			String type) {
		EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
		newParametre.setParamKey(key);
		newParametre.setParamValue(value);
		newParametre.setParamCommentaires(comment);
		newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
		if (ec.hasChanges()) {
			EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
			try {

				// Avant de sauvegarder les données, nous modifions le modèle
				// pour que l'on puisse avoir accès aussi en écriture sur les données
				entityParameter.setReadOnly(false);
				ec.saveChanges();

			} catch (Exception e) {
				log.warn("Erreur lors de l'enregistrement des parametres.");
				e.printStackTrace();
			} finally {
				entityParameter.setReadOnly(true);
			}
		}

	}
	
	public boolean enregistrementPersIdCreationModificationActif() {
		return !isCodeActivationActif(PERSONNE_OBJECTHASCHANGED_DESACTIVE);
		//return !"O".equals(getParam(PERSONNE_OBJECTHASCHANGED_DESACTIVE));
	}


	@Override
	public String getParam(String key) {
		String res = getApplication().config().stringForKey(key);
		return res;
	}

}
