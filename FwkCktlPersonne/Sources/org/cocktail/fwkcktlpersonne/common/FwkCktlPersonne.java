/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleKey;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleNode;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.ERXFrameworkPrincipal;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

/**
 * 
 * Framework principal du nouveau fwk de gestion des droits.
 * Pour des raisons de rétro compatibilité, un paramètre ENABLE_NEW_GD est introduit pour activier ou non
 * la nouvelle gestion des droits.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class FwkCktlPersonne extends ERXFrameworkPrincipal {

    public static final Logger LOG = Logger.getLogger(FwkCktlPersonne.class);
    
    public static FwkCktlPersonneParamManager paramManager = new FwkCktlPersonneParamManager();

    // Registers the class as the framework principal
    static {
        setUpFrameworkPrincipalClass(FwkCktlPersonne.class);
    }

    @Override
    public void didFinishInitialization() {
        super.didFinishInitialization();
        if (ERXProperties.booleanForKeyWithDefault("ENABLE_NEW_GD", false)) {
            LOG.info(" Mise en cache des objets pour la gestion des droits... ");
            EORegleKey.getRegleKeys();
            EORegleNode.getRegleNodes();
            EORegleOperateur.getRegleOperateurs();
            EOGdFonction.getFonctions();
            EOGdApplication.getApplications();
            EOGdDomaine.getDomaines();
            EOGdTypeDroitFonction.getTypesDroitFonction();
            EOGdDonnee.getDonnees();
            EOGdDonneeStat.getDonnees();
            EOGdTypeDroitDonnee.getTypesDroitDonnee();
        }
        PersonneLegacyWorkAroundObserver.initializeNotifications();
        paramManager.checkAndInitParamsWithDefault();
    }

    @Override
    public void finishInitialization() {
        // TODO Auto-generated method stub

    }
    
    public static void launchCalculGroupesDynamiquesTask() {
        // Lancement du cron de calcul des groupes dynamiques (20 mins si non renseigné)
        long period = ERXProperties.longForKeyWithDefault("GRP_DYNAMIQUE_CALCUL_DELAY", 20*60) * 1000;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new CalculGroupesDynamiquesTask(), new NSTimestamp(), period);
    }
    
    /**
     * Task lancée à intervalles réguliers pour recalculer tous les groupes dynamiques
     *
     */
    private static class CalculGroupesDynamiquesTask extends TimerTask {

        private EOObjectStoreCoordinator osc;
        
        public CalculGroupesDynamiquesTask() {
            osc = new EOObjectStoreCoordinator();
        }
        
        @Override
        public void run() {
            calculerGroupes();
        }
        
        private void calculerGroupes() {
            // On va faire les traitements dans une stack eof à part que l'on va dropper, ceci pour éviter de bouffer
            // de la mémoire par de trop gros fetch
            EOEditingContext ec = ERXEC.newEditingContext(osc);
            NSArray<EOGroupeDynamique> groupes = EOGroupeDynamique.fetchAll(ec);
            LOG.info("Passage du daemon recalculant les groupes dynamiques... " + groupes.count() + " groupes trouvés.");
            for (EOGroupeDynamique groupe : groupes) {
                LOG.info("Calcul du groupe " + groupe.primaryKey());
                groupe.calculerGroupeDynamique();
            }
            // On invalide tout ce pti monde
            ec.invalidateAllObjects();
            ec.dispose();
            ec = null;
        }
        
    }
    
}