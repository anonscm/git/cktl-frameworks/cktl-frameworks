/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;

import er.extensions.foundation.ERXProperties;
import er.extensions.foundation.ERXSelectorUtilities;

/**
 * Classe Observer permettant de prendre en charge des traitements de compatibilités historiques, par le biais de notifications.
 * 
 * Les solutions de contournement disponibles sont :
 * - synchronisation de la table GRHUM.REPART_COMPTE à chaque insertion d'un nouveau EOCompte
 * - sychronisation des colonnes cpt_email et cpt_domain de la table GRHUM.COMPTE à chaque mise à jour d'un EOCompteEmail
 * 
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 *
 */
public class PersonneLegacyWorkAroundObserver {
    public static final Logger LOG = Logger.getLogger(PersonneLegacyWorkAroundObserver.class);
	private static PersonneLegacyWorkAroundObserver observerInstance;
	
	public static final String REPARTCPT_WORKAROUND_PROPERTY_NAME = "org.cocktail.fwkcktlpersonne.PersonneLegacyWorkAroundObserver.repartCompte.enabled";
	public static final String COMPTE_EMAIL_WORKAROUND_PROPERTY_NAME = "org.cocktail.fwkcktlpersonne.PersonneLegacyWorkAroundObserver.compteEmail.enabled";
	
	
	
	public static PersonneLegacyWorkAroundObserver getInstance() {
		if (observerInstance==null) {
			observerInstance = new PersonneLegacyWorkAroundObserver();
		}
		return observerInstance;
	}
	
	public static void initializeNotifications() {
		NSNotificationCenter center = NSNotificationCenter.defaultCenter();
		if (ERXProperties.booleanForKeyWithDefault(REPARTCPT_WORKAROUND_PROPERTY_NAME, false)) {
			center.addObserver(getInstance(),
					//new NSSelector("willInsertRepartComptes",  ERXConstant.NotificationClassArray),
					ERXSelectorUtilities.notificationSelector("willInsertRepartComptes"),
					EOCompte.EOCompteDidInsertNotification,
					null);
			LOG.info("Workaround repartCompte sur la notification : "+EOCompte.EOCompteDidInsertNotification+" activé.");
		}
		if (ERXProperties.booleanForKeyWithDefault(COMPTE_EMAIL_WORKAROUND_PROPERTY_NAME, false)) {
			center.addObserver(getInstance(),
					//new NSSelector("willUpdateEOCompteDomainMailAttributs",  ERXConstant.NotificationClassArray),
					ERXSelectorUtilities.notificationSelector("willUpdateEOCompteDomainMailAttributs"),
					EOCompteEmail.EOCompteEmailDidInsertNotification,
					null);
			LOG.info("Workaround compteEmail sur la notification : "+EOCompteEmail.EOCompteEmailDidInsertNotification+" activé.");
		}
	}
	
	/*
	 * observer invoked methods
	 */
	
	/**
	 * Méthode appelée par la notification EOCompteDidInsert, après chaque enregistrement d'un nouvel EOCompte et mettant 
	 * à jour la table : GRHUM.REPART_COMPTE par une requête en RawSQL.
	 */
	public void willInsertRepartComptes(NSNotification n) {
		LOG.debug("PersonneLegacyWorkAroundObserver.willInsertRepartComptes() fired.");
        CktlDataBus databus = ((CktlWebApplication) CktlWebApplication.application()).dataBus();
        String query = "insert into grhum.repart_compte ( select cpt_ordre, pers_id, sysdate, sysdate " +
        		"from grhum.compte  where cpt_ordre not in (select cpt_ordre from grhum.repart_compte))";
        try {
        	String exp = "ALTER TRIGGER GRHUM.TRG_REPART_COMPTE DISABLE";
            databus.executeSQLQuery(exp, NSArray.EmptyArray);
            
        	databus.executeSQLQuery(query, NSArray.EmptyArray);
        	
        	exp = "ALTER TRIGGER GRHUM.TRG_REPART_COMPTE ENABLE";
            databus.executeSQLQuery(exp, NSArray.EmptyArray);
        } catch (Throwable t) {
        	databus.rollbackTransaction();
			LOG.error("PersonneLegacyWorkAroundObserver.willInsertRepartComptes() : Erreur de traitement", t);
		} 
	}

	/**
	 * Méthode appelée par la notification {@link EOCompteEmail#EOCompteEmailDidInsertNotification}, après chaque 
	 * enregistrement d'un nouvel EOCompteEmail et mettant à jour les colonnes cpt_email et cpt_domain de la table : 
	 * GRHUM.COMPTE par une requête en RawSQL.
	 */
	public void willUpdateEOCompteDomainMailAttributs(NSNotification n) {
		LOG.debug("PersonneLegacyWorkAroundObserver.willUpdateEOCompteDomainMailAttributs() fired.");
		CktlDataBus databus = ((CktlWebApplication) CktlWebApplication.application()).dataBus();
        String query = "UPDATE grhum.compte T1 SET T1.cpt_email= (select cem_email from grhum.compte_email T2 where T1.cpt_ordre = T2.cpt_ordre) ," +
        		"T1.cpt_domaine = (select cem_domaine from grhum.compte_email T2 where T1.cpt_ordre = T2.cpt_ordre) " +
        		"WHERE  exists ( select * from grhum.compte_email T2 where T1.cpt_ordre = T2.cpt_ordre " +
        		"and ( cpt_email IS NULL or cpt_domaine IS NULL or T1.cpt_email!= T2.cem_email or T1.cpt_domaine!=T2.cem_domaine))";
        try {
        	databus.executeSQLQuery(query, NSArray.EmptyArray);
        } catch (Throwable t) {
        	databus.rollbackTransaction();
			LOG.error("PersonneLegacyWorkAroundObserver.willUpdateEOCompteDomainMailAttributs() : Erreur de traitement", t);
		} 
	}
	
}
