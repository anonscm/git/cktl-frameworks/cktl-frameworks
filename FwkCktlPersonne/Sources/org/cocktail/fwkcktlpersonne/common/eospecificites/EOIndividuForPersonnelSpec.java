/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;

import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Les specificites pour un individu de type Personnel.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOIndividuForPersonnelSpec implements ISpecificite {

	private static EOIndividuForPersonnelSpec sharedInstance = new EOIndividuForPersonnelSpec();

	public static EOIndividuForPersonnelSpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOIndividuForPersonnelSpec() {
	}

	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		//((EOIndividu)afwkPersRecord)

	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	/**
	 * Contrôles :
	 * <ul>
	 * <li>{@link EOIndividuForPersonnelSpec#checkLieuNaissance(EOIndividu)}</li>
	 * <ul>
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
		if (!isSpecificite(afwkPersRecord)) {
			throw new NSValidation.ValidationException("L'objet ne peut être considéré comme un personnel.");
		}
		EOIndividu individu = (EOIndividu) afwkPersRecord;
		checkLieuNaissance(individu);
		//FIXME ajouter prise en compte et controle du param GRHUM_CONTROLE_INSEE

	}

	/**
	 * @param afwkPersRecord
	 * @return true si afwkPersRecord est de la classe EOIndividu et que toPersonnel() n'est pas vide.
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		if (afwkPersRecord instanceof EOIndividu) {
			EOIndividu individu = (EOIndividu) afwkPersRecord;
			try {
				if (individu.toPersonnel() != null && !individu.toPersonnel().equals(EOKeyValueCoding.NullValue) && individu.toPersonnel().dCreation() != null) {
					return true;
				}
			} catch (Exception e) {
				//si exception, on fait rien, c'est les controles Wonder dispo dans ERXDatabaseContextDelegate#databaseContextFailedToFetchObject
				//pb lie au modele a priori (cle de eoindividu = cle de no dosier pers), exception quand pas de personel lié à l'individu
				if (!e.getClass().getName().endsWith("ObjectNotAvailableException")) {
					throw new RuntimeException(e);
				}
			}
		}
		return false;
	}

	/**
	 * Verifie la coherence de saisi concernant le pays et le departement de naissance.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkLieuNaissance(EOIndividu individu) throws NSValidation.ValidationException {
		if (individu.toPaysNaissance() != null && EOPays.CODE_PAYS_FRANCE.equals(individu.toPaysNaissance().cPays()) && individu.toDepartement() == null) {
			throw new NSValidation.ValidationException("Pour une personne née en France, vous devez fournir le département");
		}
		if (individu.toDepartement() != null && individu.toDepartement().cDepartement().equals("000") == false && (individu.toPaysNaissance() == null || !individu.toPaysNaissance().cPays().equals(EOPays.CODE_PAYS_FRANCE))) {
			throw new NSValidation.ValidationException("Le pays de naissance est incohérent avec le département de naissance. Soit la personne est née en France, soit vous ne devez pas fournir de département");
		}
	}

}
