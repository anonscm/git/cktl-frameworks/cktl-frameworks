/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Les specificites pour une structure de type Service.
 * <ul>
 * <li>Règles d'initialisation : {@link EOStructureForServiceSpec#onAwakeFromInsertion(AfwkPersRecord)}</li>
 * <li>Règles de validation : {@link EOStructureForServiceSpec#onValidateObjectMetier(AfwkPersRecord)}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOStructureForServiceSpec extends EOStructureForGroupeSpec {
	public static final Logger logger = Logger.getLogger(EOStructureForServiceSpec.class);
	private static EOStructureForServiceSpec sharedInstance = new EOStructureForServiceSpec();

	public static EOStructureForServiceSpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOStructureForServiceSpec() {

	}

	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		super.onAwakeFromInsertion(afwkPersRecord);
	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateBeforeTransactionSave(afwkPersRecord);
	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForDelete(afwkPersRecord);
	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForInsert(afwkPersRecord);
	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForUpdate(afwkPersRecord);
	}

	/**
	 * Appelé lors de la validation d'une structure de type groupe. Controles : <br/>
	 * <ul>
	 * <li>L'appartenance au type de groupe S est obligatoire (via toRepartTypeGroupes)</li>
	 * <li>Le responsable est obligatoire</li>
	 * <li></li>
	 * </ul>
	 * 
	 * @see EOStructureForGroupeSpec#onValidateObjectMetier(AfwkPersRecord)
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateObjectMetier(afwkPersRecord);
		//responsable obligatoire
		EOStructure structure = (EOStructure) afwkPersRecord;

		if (!isSpecificite(afwkPersRecord)) {
			logger.error("Une structure de type Service doit être affecté au type de groupe S. " + structure.getNomEtId());
			throw new NSValidation.ValidationException("Une structure de type Service doit être affecté au type de groupe S.");
		}
		if (structure.grpResponsable() == null) {
			logger.error("Le responsable du service est obligatoire. " + structure.getNomEtId());
			throw new NSValidation.ValidationException("Le responsable du service est obligatoire.");
		}
	}

	/**
	 * @return true si l'objet est une structure affectée au type de groupe S.
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		if (afwkPersRecord instanceof EOStructure) {
			final EOStructure structure = (EOStructure) afwkPersRecord;
			return structure.toRepartTypeGroupes(new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_S)).count() > 0;
		}
		return false;
	}

}
