package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;

import com.webobjects.foundation.NSArray;

/**
 * Services sur les structures de type fournisseur internes
 */
public class FournisseurInterneServices {
	private String cStructureFouValideInterne;
	private String cStructureFouValideMorale;
	private EOStructureForFournisseurSpec structureForFournisseurSpec;

	/**
	 * @return un fournisseur de structure
	 */
	public EOStructureForFournisseurSpec getStructureForFournisseurSpec() {
		if (structureForFournisseurSpec == null) {
			structureForFournisseurSpec = EOStructureForFournisseurSpec.sharedInstance();
		}
		return structureForFournisseurSpec;
	}

	public void setStructureForFournisseurSpec(EOStructureForFournisseurSpec structureForFournisseurSpec) {
		this.structureForFournisseurSpec = structureForFournisseurSpec;
	}

	/**
	 * vrai si la structure est un fournisseur , si elle est associée à un groupe de type fou_valide_interne et si elle est associée à un groupe de type
	 * fou_valide_morale
	 * @param structure structure testée
	 * @return <code> true </code> si la structure est un fournisseur interne
	 */
	public Boolean isStructureFournisseurInterne(EOStructure structure) {

		IFournis fournisseur = structure.toFournis();
		if (fournisseur == null || fournisseur.toValideFournis() == null) {
			return false;
		}

		if (!estDansGroupe(structure, getCStructureFouValideInterne()) || !estDansGroupe(structure, getCStructureFouValideMorale())) {
			return false;
		}

		return true;
	}

	/**
	 * @param structure déclare la structure comme étant un fournisseur interne
	 * @param applicationUser le persId de l'utilisateur qui fait l'appel à la fonction
	 * @throws Exception exception lancée en cas d'erreur
	 */
	public void declareFournisseurInterne(EOStructure structure, PersonneApplicationUser applicationUser) throws Exception {
		IFournis fournisseur = structure.toFournis();
		if (fournisseur == null) {
			fournisseur = creer(structure, applicationUser.getPersId());
		}
		((EOFournis) fournisseur).validerFournis(applicationUser);
		ajouterFournisseurAuGroupe(structure, getCStructureFouValideInterne(), applicationUser.getPersId());
	}

	private void ajouterFournisseurAuGroupe(EOStructure structure, String cgroupe, Integer persIdUser) {
		EOStructure structureGroupe = EOStructure.fetchByQualifier(structure.editingContext(), EOStructure.C_STRUCTURE.eq(cgroupe));
		EORepartStructure.creerInstanceSiNecessaire(structure.editingContext(), structure, structureGroupe, persIdUser);
	}

	/**
	 * @param structure la structure que l'on teste
	 * @param cStruct le code structure auquel la structure doit appartenir
	 * @return <code> true </code> si la structure appartient bien à la structure cStruct
	 */
	Boolean estDansGroupe(EOStructure structure, String cStruct) {
		NSArray<EORepartStructure> repartStructures = structure.toRepartStructures(EORepartStructure.PERS_ID.eq(structure.persId()).and(
		    EORepartStructure.C_STRUCTURE.eq(cStruct)));
		return repartStructures.size() > 0;
	}

	String getCStructureFouValideInterne() {
		if (cStructureFouValideInterne == null) {
			cStructureFouValideInterne = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_INTERNE_KEY);
		}
		return cStructureFouValideInterne;
	}

	String getCStructureFouValideMorale() {
		if (cStructureFouValideMorale == null) {
			cStructureFouValideMorale = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_MORALE_KEY);
		}
		return cStructureFouValideMorale;
	}

	IFournis creer(EOStructure structure, Integer persIdUser) throws Exception {
		EOFournis fournis = null;

		if (structure == null) {
			throw new Exception(String.format("Impossible de creer un fournisseur pour une structure nulle"));
		}

		IAdresse adresseFacturation = getAdresseFacturation(structure);
		if (adresseFacturation == null) {
			throw new Exception(String.format("Impossible de creer un fournisseur pour une adresse nulle"));
		}

		fournis = EOFournis.creerInstance(structure.editingContext());
		fournis.setToStructure(structure);
		fournis.setFouValide(EOFournis.FOU_VALIDE_OUI);
		structure.addToToFournissRelationship(fournis);

		try {
			fournis.initialise(persIdUser, (EOAdresse) adresseFacturation, false, EOFournis.FOU_TYPE_FOURNISSEUR);
		} catch (Exception e) {
			throw new Exception("Une erreur est survenue lors de l'initialisation du fournisseur pour la structure " + structure.lcStructure(), e);
		}

		return fournis;
	}

	private IAdresse getAdresseFacturation(EOStructure structure) {
		return getStructureForFournisseurSpec().getAdresseFacturationPrincipale(structure);
	}

}
