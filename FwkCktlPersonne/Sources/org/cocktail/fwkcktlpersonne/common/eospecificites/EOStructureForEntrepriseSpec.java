/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Les specificites metiers pour une structure de type Entreprise.
 * <ul>
 * <li>Règles d'initialisation : {@link EOStructureForEntrepriseSpec#onAwakeFromInsertion(AfwkPersRecord)}</li>
 * <li>Règles de validation : {@link EOStructureForEntrepriseSpec#onValidateObjectMetier(AfwkPersRecord)}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOStructureForEntrepriseSpec extends EOStructureForPersonneMoraleSpec implements ISpecificite {

	private static EOStructureForEntrepriseSpec sharedInstance = new EOStructureForEntrepriseSpec();

	public static EOStructureForEntrepriseSpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOStructureForEntrepriseSpec() {
		super();
	}

	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		super.onAwakeFromInsertion(afwkPersRecord);
	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateBeforeTransactionSave(afwkPersRecord);
	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForDelete(afwkPersRecord);
	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForInsert(afwkPersRecord);
	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForUpdate(afwkPersRecord);
	}

	/**
	 * Ajoute des validations pour une structure de type entreprise.
	 * <ul>
	 * <li>si la structure est affectée au groupe des entreprises {@link EOStructureForGroupeSpec#ANNUAIRE_ENTREPRISE_KEY}</li>
	 * <li>Le code NAF est obligatoire.</li>
	 * </ul>
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
		EOStructure structure = (EOStructure) afwkPersRecord;

		checkGroupeEntreprise(structure);

		//Code NAF obligatoire
		if (MyStringCtrl.isEmpty(structure.cNaf())) {
			throw new NSValidation.ValidationException("Le code NAF est obligatoire pour une structure de type Entreprise.");
		}

	}

	/**
	 * @return true si l'objet passé en parametre est une structure de type entreprise.
	 * @see org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForPersonneMoraleSpec#isSpecificite(org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord)
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		if (afwkPersRecord instanceof EOStructure) {
			if (super.isSpecificite(afwkPersRecord)) {
				final EOStructure structure = (EOStructure) afwkPersRecord;

				//verifier si structure relie a un type de groupe entreprise 
				return isInGroupeEntreprise(structure);
			}
		}
		return false;
	}

	/**
	 * Verifier si la structure est affectée au groupe des entreprises.
	 * 
	 * @param structure
	 */
	public void checkGroupeEntreprise(EOStructure structure) {
		try {
			EOQualifier qual1 = new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, EOStructureForGroupeSpec.getGroupeEntreprise(structure.editingContext()));
			EOQualifier qual = new EOOrQualifier(new NSArray(new Object[] {
					qual1
			}));
			if (structure.toRepartStructures(qual).count() == 0) {
				throw new NSValidation.ValidationException("La personne morale de type entreprise doit être affectée au groupe des entreprises.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException(e.getMessage());
		}
	}

	public boolean isInGroupeEntreprise(EOStructure structure) {
		try {
			EOQualifier qual1 = new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, EOStructureForGroupeSpec.getGroupeEntreprise(structure.editingContext()));
			EOQualifier qual = new EOOrQualifier(new NSArray(new Object[] {
					qual1
			}));
			if (structure.toRepartStructures(qual).count() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return false;

	}

}
