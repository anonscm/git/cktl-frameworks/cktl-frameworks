/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.HashMap;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Les specificites pour une structure de type groupe.
 * <ul>
 * <li>Règles d'initialisation : {@link EOStructureForGroupeSpec#onAwakeFromInsertion(AfwkPersRecord)}</li>
 * <li>Règles de validation : {@link EOStructureForGroupeSpec#onValidateObjectMetier(AfwkPersRecord)}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOStructureForGroupeSpec implements ISpecificite {

	private static EOStructureForGroupeSpec sharedInstance = new EOStructureForGroupeSpec();
	public static final String GRP_ACCES_PUBLIC = "+";
	public static final String GRP_ACCES_PRIVE = "-";
	public static final String LC_GRP_ACCES_PUBLIC = "public";
	public static final String LC_GRP_ACCES_PRIVE = "privé";
	public static final NSDictionary GRP_ACCESS = new NSDictionary(
			new String[] {
					LC_GRP_ACCES_PUBLIC, LC_GRP_ACCES_PRIVE
			},
			new String[] {
					GRP_ACCES_PUBLIC, GRP_ACCES_PRIVE
			});

	private static NSMutableArray CSTRUCTURE_FOR_GROUPES_RESERVES = null;

	/** Structure de type groupe */
	public static final EOQualifier QUAL_IS_GROUPE = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorNotEqual, EOEnterpriseObject.NullValue);

	/** Groupes de type G */
	public static final EOQualifier QUAL_GROUPES_GROUPES = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_G);

	/** Groupes de type Services */
	public static final EOQualifier QUAL_GROUPES_SERVICES = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorContains, EOTypeGroupe.TGRP_CODE_S);

	/** Groupes de type Laboratoires */
	public static final EOQualifier QUAL_GROUPES_LABORATOIRES = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorContains, EOTypeGroupe.TGRP_CODE_LA);

	/** Groupes de type Structures de recherche */
	public static final EOQualifier QUAL_GROUPES_STRUCTURES_RECHERCHE = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_SR);
	/** Groupes de type Fournisseurs */
	public static final EOQualifier QUAL_GROUPE_FOURNISSEUR = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_FO);

	/** Groupes de type entreprises */
	public static final EOQualifier QUAL_GROUPE_ENTREPRISES = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorContains, EOTypeGroupe.TGRP_CODE_EN);

	/** Groupes de type Forum */
	public static final EOQualifier QUAL_GROUPE_FORUMS = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_F);

	/** Groupes de type Groupe système */
	public static final EOQualifier QUAL_GROUPE_SYSTEME = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_U);

	public static final String ANNUAIRE_CREATION_ENQUETES_KEY = "ANNUAIRE_CREATION_ENQUETES";
	public static final String ANNUAIRE_CREATION_FORUMS_KEY = "ANNUAIRE_CREATION_FORUMS";
	public static final String ANNUAIRE_EMPLOYEUR_KEY = "ANNUAIRE_EMPLOYEUR";
	public static final String ANNUAIRE_FOU_ETUDIANT_KEY = "ANNUAIRE_FOU_ETUDIANT";
	public static final String ANNUAIRE_INVITE_WIFI_KEY = "ANNUAIRE_INVITE_WIFI";
	public static final String ANNUAIRE_TICKET_WIFI_KEY = "ANNUAIRE_TICKET_WIFI";
	public static final String ANNUAIRE_PARTENARIAT_KEY = "ANNUAIRE_PARTENARIAT";
	public static final String ANNUAIRE_CONTACTS_KEY = "ANNUAIRE_CONTACTS";
	public static final String ANNUAIRE_GROUPE_UNIX_KEY = "ANNUAIRE_GROUPE_UNIX";
	public static final String ANNUAIRE_REFAPP_KEY = "ANNUAIRE_REFAPP";

	public static final String ANNUAIRE_VALID_TICKET_WIFI_KEY = "ANNUAIRE_VALID_TICKET_WIFI";

	public static final String ANNUAIRE_ARCHIVES = EOGrhumParametres.PARAM_ANNUAIRE_ARCHIVES;

	private static final String GRHUM_CREATEUR_KEY = "GRHUM_CREATEUR";

	/**
	 * Cache des structures referencees dans les parametres.
	 */
	private static final HashMap<String, EOStructure> PARAM_STRUCTURES_CACHE = new HashMap<String, EOStructure>();

	public static EOStructure getGroupeFournisseurEnCours(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ENCOURS_VALIDE_KEY);
	}

	public static EOStructure getGroupeFournisseurValideMorale(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_MORALE_KEY);
	}

	public static EOStructure getGroupeFournisseurValidePhysique(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_PHYSIQUE_KEY);
	}

	public static EOStructure getGroupeFournisseurValideExterne(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_EXTERNE_KEY);
	}

	public static EOStructure getGroupeFournisseurAnnules(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ARCHIVES_KEY);
	}

	public static EOStructure getGroupeEntreprise(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_ENTREPRISE_KEY);
	}

	public static EOStructure getGroupeFouEntreprise(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ENTREPRISE_KEY);
	}

	/**
	 * @param ec : editingContexte à utiliser pour récupérer ce groupe.
	 * @return le groupe pour l'alias de Validation des Tickets
	 * @throws Exception
	 */
	public static EOStructure getGroupeValidTicketWifi(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, ANNUAIRE_VALID_TICKET_WIFI_KEY);
	}

	public static EOStructure getGroupeReferentielApplicatifs(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, ANNUAIRE_REFAPP_KEY);
	}

	/**
	 * @param ec
	 * @return la structure d'archivage ou exception.
	 * @throws Exception si le paramètre GRHUM: ANNUAIRE_ARCHIVES n'est pas defini ou si la structure correspondante n'est pas trouvée.
	 */
	public static EOStructure getStructurePourArchivage(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, ANNUAIRE_ARCHIVES);
	}

	/**
	 * @param ec
	 * @return Le groupe (structure) correspondant au parametre ANNUAIRE_STRUCTURE_DEFAUT. Ce groupe doit etre typé Référentiel (RE).
	 */
	public static EOStructure getGroupeDefaut(EOEditingContext ec) {
		try {
			return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_STRUCTURE_DEFAUT_KEY);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param ec
	 * @return Les racines du referentiel (c_structure_pere = c_structure et Type structure = E ou ES).
	 */
	public static NSArray getGroupesRacine(EOEditingContext ec) {
		
		EOQualifier qualifier = 
			EOStructure.C_STRUCTURE.eq(EOStructure.C_STRUCTURE_PERE)
				.and(
					EOStructure.C_TYPE_STRUCTURE.in(new NSArray<String>(EOTypeStructure.TYPE_STRUCTURE_E, EOTypeStructure.TYPE_STRUCTURE_ES))
				);
		
		return EOStructure.fetchAll(
				ec,
				qualifier,
				EOStructure.LL_STRUCTURE.ascs()
			);
	}

	public static boolean isGroupeRacine(EOStructure groupe) {
		//(c_structure_pere = c_structure et Type structure = E ou ES).
		return (groupe != null
				&& groupe.toStructurePere().equals(groupe) && (groupe.cTypeStructure().equalsIgnoreCase((String) EOTypeStructure.TYPE_STRUCTURE_E) || groupe.cTypeStructure().equalsIgnoreCase((String) EOTypeStructure.TYPE_STRUCTURE_ES)));
	}

	public static boolean isGroupeArchive(EOStructure groupe) {
		EOStructure orphelinat;
		try {
			orphelinat = EOStructureForGroupeSpec.getStructurePourArchivage(groupe.editingContext());
		} catch (Exception e) {
			return false;
		}
		return (groupe.equals(orphelinat));
	}

	/**
	 * Détermination si un groupe est un groupe réservé
	 * @param groupe à tester
	 * @return true si c'est un groupe réservé
	 */
	public static boolean isGroupeReserve(EOStructure groupe) {
		return getCStructureForGroupesReserves(groupe.editingContext()).contains(groupe.cStructure());
	}

	/**
	 * Détermination si un groupe est de type institutionnel
	 * @param groupe structure à tester
	 * @return true si le groupe est de type GI
	 */
	public static boolean isGroupeInstitutionnel(EOStructure groupe) {
		return isGroupeOfType(groupe, EOTypeGroupe.TGRP_CODE_GI);
	}

	/**
	 * @param ec
	 * @param paramKey La clé du parametre (de la table grhum.grhum_parametres)
	 * @param La valeur est récupérée depuis le cache si présente. Pour nettoyer le cache et forcer le fetch, appeler
	 *            {@linkplain EOStructureForGroupeSpec#cleanParamStructureCache}
	 * @return La structure représentant le groupe.
	 * @throws Exception
	 */
	public static EOStructure getGroupeForParamKey(EOEditingContext ec, String paramKey) throws Exception {
		return getGroupeForParamKey(ec, paramKey, false);
	}

	/**
	 * @param ec
	 * @param paramKey La clé du parametre (de la table grhum.grhum_parametres)
	 * @param fetch si true, le fetch est systématqiuement effectué, sinon la valeur est récupérée depuis le cache si présente.
	 * @return La structure représentant le groupe.
	 * @throws Exception
	 */
	public static EOStructure getGroupeForParamKey(EOEditingContext ec, String paramKey, boolean fetch) throws Exception {
		if (fetch || PARAM_STRUCTURES_CACHE.get(paramKey) == null || PARAM_STRUCTURES_CACHE.get(paramKey).editingContext() == null || PARAM_STRUCTURES_CACHE.get(paramKey).editingContextOrParentIsNull()) {
			String cStruct = EOGrhumParametres.parametrePourCle(ec, paramKey);
			if (MyStringCtrl.isEmpty(cStruct)) {
				throw new Exception("Le parametre GRHUM " + paramKey + " n'est pas defini.");
			}
			EOQualifier qual = new EOKeyValueQualifier(EOStructure.C_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, cStruct);
			@SuppressWarnings("rawtypes")
			NSArray res = rechercherGroupes(ec, qual, 1, true);
			if (res.count() == 0) {
				throw new Exception("Le parametre GRHUM " + paramKey + " (=" + cStruct + ") ne reference pas une structure valide.");
			}
			PARAM_STRUCTURES_CACHE.put(paramKey, ((EOStructure) res.objectAtIndex(0)).localInstanceIn(ec));
		}
		EOStructure result = PARAM_STRUCTURES_CACHE.get(paramKey);
		return result != null ? result.localInstanceIn(ec) : null;
	}

	/**
	 * @param ec
	 * @param paramKey
	 * @return La structure représentant le groupe. Si le groupe n'est pas trouvé un msg est ecrit dans le log des erreurs.
	 */
	public static EOStructure getGroupeForParamKeySilent(EOEditingContext ec, String paramKey) {
		try {
			return getGroupeForParamKey(ec, paramKey);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	public static EOStructure getGroupeForGrpAlias(EOEditingContext ec, String alias) {
		EOQualifier qual = new EOKeyValueQualifier(
				EOStructure.GRP_ALIAS_KEY,
				EOQualifier.QualifierOperatorCaseInsensitiveLike,
				alias);
		EOStructure struct = EOStructure.fetchFirstByQualifier(ec, qual);
		return struct;
	}

	/**
	 * @deprecated Utilisez {@link #getGroupeForParamKey(EOEditingContext, String)} à l aplace.
	 */
	@Deprecated
	public static EOStructure getStructureForGroupe(EOEditingContext ec, String groupeKey) throws Exception {
		return getGroupeForParamKey(ec, groupeKey);
	}

	/**
	 * @param ec
	 * @param qual
	 * @param fetchLimit pris en compte si >0
	 * @return Les structures valides de type groupe dependant du qualifier specifie.
	 */
	public static NSArray<EOStructure> rechercherGroupes(EOEditingContext ec, EOQualifier qual, int fetchLimit, boolean isOnlyValid) {
		NSMutableArray array = new NSMutableArray();
		if (isOnlyValid) {
			// Mise en commentaire de la ligne suivante le 10/02/2014 pour que les structures invalides ne disparaissent pas de l'arnorescence dans AGrhum.
			array.addObject(EOStructure.QUAL_STRUCTURES_VALIDE);
		}
		//array.addObject(QUAL_GROUPES_GROUPES);
		array.addObject(QUAL_IS_GROUPE);
		if (qual != null) {
			array.addObject(qual);
		}
		EOFetchSpecification spec = new EOFetchSpecification(EOStructure.ENTITY_NAME, new EOAndQualifier(array), new NSArray(EOStructure.SORT_LL_STRUCTURE_ASC));
		if (fetchLimit > 0) {
			spec.setFetchLimit(fetchLimit);
		}
		spec.setUsesDistinct(true);
		NSArray res = ec.objectsWithFetchSpecification(spec);
		return res;
	}

	public static EOStructureForGroupeSpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOStructureForGroupeSpec() {
	}

	/**
	 * Appelé lors de l'insertion (si la specificite a été passée en parametre de {@link EOStructure#creerInstance(EOEditingContext, NSArray)}).
	 * <ul>
	 * <li>Par defaut, le type structure est A</li>
	 * <li>Par defaut, le grpAcces est a PUBLIC (+)</li>
	 * </ul>
	 */
	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		EOStructure structure = ((EOStructure) afwkPersRecord);
		EOTypeStructure typeStructure = EOTypeStructure.fetchByKeyValue(structure.editingContext(), EOTypeStructure.C_TYPE_STRUCTURE_KEY, EOTypeStructure.TYPE_STRUCTURE_A);
		structure.setToTypeStructureRelationship(typeStructure);
		structure.setCTypeStructure(EOTypeStructure.TYPE_STRUCTURE_A);
		structure.setGrpAcces(GRP_ACCES_PUBLIC);
	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	/**
	 * Appelé lors de la validation d'une structure de type groupe. Modifications : <br>
	 * <ul>
	 * <li>{@link EOStructureForGroupeSpec#fixGrpOwner(EOStructure)}</li>
	 * </ul>
	 * Controles :<br/>
	 * <ul>
	 * <li>L'appartenance au type de groupe G est obligatoire (via toRepartTypeGroupes)</li>
	 * <li>Le grpOwner est obligatoire (si non saisi, l'initialiser avec l'individu defini comme grhum_createur dans les parametres)</li>
	 * <li>Le grpAcces est obligatoire et doit etre egal a + ou -</li>
	 * <li>Une structure pere (de type groupe) est obligatoire</li>
	 * <li>si tgrpCode='EN' et structure est fournisseur externe, verifier que on est dans le groupe ANNUAIRE_FOU_ENTREPRISE, sinon verifier que on
	 * est dans groupe ANNUAIRE_ENTREPRISE</li>
	 * </ul>
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
		if (!isSpecificite(afwkPersRecord)) {
			throw new NSValidation.ValidationException("La structure doit etre associee au type de groupe G.");
		}

		final EOStructure structure = (EOStructure) afwkPersRecord;

		//owner obligatoire
		fixGrpOwner(structure);
		checkGrpOwner(structure);
		fixGrpAcces(structure);

		//grpAcces obligatoire (+ ou -)
		if (MyStringCtrl.isEmpty(structure.grpAcces()) || (!GRP_ACCES_PUBLIC.equals(structure.grpAcces()) && !GRP_ACCES_PRIVE.equals(structure.grpAcces()))) {
			throw new NSValidation.ValidationException("Dans le cas d'un groupe, le champ grpAcces doit être rempli avec + ou -.");
		}
		fixStructurePere(structure);
		checkStructurePere(structure);
		if (!isGroupeOfType(structure.toStructurePere(), EOTypeGroupe.TGRP_CODE_G)) {
			throw new NSValidation.ValidationException("La structure pere doit etre un groupe.");
		}
		// Vérification de l'homonymie dans la même branche
		checkNomWithPeers(structure);

		//si tgrpCode='EN' et structure est fournisseur externe, verifier que on est dans le groupe ANNUAIRE_FOU_ENTREPRISE, sinon verifier que on est dans groupe ANNUAIRE_ENTREPRISE
		fixGroupeEntreprises(structure);

	}

	/**
	 * Verifie et corrige eventuellement l'affectation aux groupes ANNUAIRE_FOU_ENTREPRISE et ANNUAIRE_ENTREPRISEen fonction du tgrpCode affecte.
	 * 
	 * @param structure
	 */
	public void fixGroupeEntreprises(EOStructure structure) throws NSValidation.ValidationException {
		try {
			boolean isInGroupeOfTypeEN = isGroupeOfType(structure, EOTypeGroupe.TGRP_CODE_EN);
			boolean isExterne = !structure.isInterneEtablissement().booleanValue();
			boolean isFournisseur = EOStructureForFournisseurSpec.sharedInstance().isSpecificite(structure);
			EOEditingContext ec = structure.editingContext();

			EOStructure groupeEntreprise = getGroupeEntreprise(ec);
			EOStructure groupeFouEntreprise = getGroupeFouEntreprise(ec);

			if (isInGroupeOfTypeEN && isExterne) {
				if (isFournisseur) {
					if (!isPersonneInGroupe(structure, groupeFouEntreprise)) {
						affecterPersonneDansGroupe(ec, structure, groupeFouEntreprise, structure.persIdModification());
					}
				}
				else {
					if (!isPersonneInGroupe(structure, groupeEntreprise)) {
						affecterPersonneDansGroupe(ec, structure, groupeEntreprise, structure.persIdModification());
					}
				}
			}
		} catch (Exception e) {
			NSValidation.ValidationException x = new NSValidation.ValidationException(e.getMessage());
			x.setStackTrace(e.getStackTrace());
			throw x;
		}

	}

	/**
	 * @return true si l'objet passé en parametre est une structure de type de groupe G (tgrpCode=G).
	 * @see org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite#isSpecificite(org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord)
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		if (afwkPersRecord instanceof EOStructure) {
			final EOStructure structure = (EOStructure) afwkPersRecord;
			return isGroupeOfType(structure, EOTypeGroupe.TGRP_CODE_G);
		}
		return false;
	}

	/**
	 * Affecte un grpOwner par defaut si celui-ci n'est pas defini. (On le recupere à partir du parametre GRHUM_CREATEUR.
	 * 
	 * @param structure
	 */
	public void fixGrpOwner(EOStructure structure) {
		if (structure.grpOwner() == null) {

			NSArray<String> val = EOGrhumParametres.parametresPourCle(structure.editingContext(), GRHUM_CREATEUR_KEY);

			if (!val.isEmpty()) {
				EOCompte res = null;

				for (int i = 0; i < val.count(); i++) {
					EOQualifier qual = new EOKeyValueQualifier(EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, val.objectAtIndex(i));
					NSMutableArray quals = new NSMutableArray();
					quals.addObject(qual);
					quals.addObject(EOCompte.QUAL_CPT_VALIDE_OUI);
					res = EOCompte.fetchFirstByQualifier(structure.editingContext(), new EOAndQualifier(quals));

					if (res != null) {
						EOIndividu ind = res.toIndividu();
						if (ind != null) {
							structure.setGrpOwner(ind.noIndividu());
							break;
						} 
					}
				}

			}
		} 
	}

	private void fixGrpAcces(EOStructure structure) {
		if (structure.grpAcces() == null) {
			structure.setGrpAcces(GRP_ACCES_PUBLIC);
		}
	}

	private void fixStructurePere(EOStructure structure) {
		if (structure.toStructurePere() == null) {
			EOStructure groupeDefaut = EOStructureForGroupeSpec.getGroupeDefaut(structure.editingContext());
			structure.setToStructurePereRelationship(groupeDefaut);
		}
	}

	public void checkGrpOwner(EOStructure structure) {
		if (structure.grpOwner() == null) {
			throw new NSValidation.ValidationException("Le champ grpOwner (createur du groupe) doit être rempli avec un n° d'individu.");
		}
	}

	public void checkStructurePere(EOStructure structure) {
		if (structure.toStructurePere() == null) {
			throw new NSValidation.ValidationException("Pour un groupe la structure pere doit etre definie.");
		}
	}

	/**
	 * @param structure la structure à valider
	 * @throws ValidationException si un groupe de type service a le même nom et le même père que <code>structure</code>
	 */
	private void checkNomWithPeers(EOStructure structure) {
		if (EOStructureForServiceSpec.sharedInstance().isSpecificite(structure) && structure.toStructurePere() != null) {
			EOQualifier qual = new EOKeyValueQualifier(
					EOStructure.LL_STRUCTURE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, structure.llStructure());
			EOQualifier qual2 = new EOKeyValueQualifier(EOStructure.C_STRUCTURE_KEY, EOQualifier.QualifierOperatorNotEqual, structure.cStructure());
			qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					qual, qual2
			}));
			NSArray<EOStructure> peers = structure.toStructurePere().structuresFils(qual);
			if (peers.count() > 0)
				throw new NSValidation.ValidationException(
						"Le groupe " + structure.toStructurePere().llStructure()
						+ " contient déjà une structure nommée : " + structure.llStructure());
		}
	}

	/**
	 * @param tgrpCode
	 * @param personne
	 * @return true si la personne est affectee a un groupe de type tgrpCode.
	 */
	public static boolean isPersonneInGroupeOfType(IPersonne personne, String tgrpCode) {
		if (personne == null || tgrpCode == null) {
			return false;
		}
		//boolean res = false;
		return (personne.getPersonneDelegate().getAllGroupesAffectesOfType(tgrpCode).count() > 0);
		//		
		//		
		//		for (int i = 0; i < personne.toRepartStructures().count(); i++) {
		//			EORepartStructure repartStructure = (EORepartStructure) personne.toRepartStructures().objectAtIndex(i);
		//			NSArray repartTypeGroupes = repartStructure.toStructureGroupe().toRepartTypeGroupes();
		//			for (int j = 0; j < repartTypeGroupes.count(); j++) {
		//				if (tgrpCode.equals( ((EORepartTypeGroupe)repartTypeGroupes.objectAtIndex(j)).tgrpCode())) {
		//					res = true;
		//				}
		//			}
		//		}	
		//		return res;
	}

	/**
	 * @param structure
	 * @param tgrpCode
	 * @return True si la structure est un groupe du type specifie.
	 */
	public static boolean isGroupeOfType(EOStructure structure, String tgrpCode) {
		return structure.toRepartTypeGroupes(new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, tgrpCode)).count() > 0;
	}

	public static boolean isGroupePrive(EOStructure structure) {
		return GRP_ACCES_PRIVE.equals(structure.grpAcces());
	}

	/**
	 * @param personne
	 * @param groupe
	 * @return true si la personne est affectee au groupe.
	 */
	public static boolean isPersonneInGroupe(IPersonne personne, EOStructure groupe) {
		if (personne == null || groupe == null) {
			return false;
		}
		EOQualifier qual2;
		try {
			qual2 = new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, groupe);
			return (EOQualifier.filteredArrayWithQualifier(personne.toRepartStructures(), qual2).count() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static EOStructure getGroupeFournisseurValideInterne(EOEditingContext ec) throws Exception {
		return getGroupeForParamKey(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_INTERNE_KEY);
	}

	/**
	 * @param edc
	 * @return Tous les groupes fournisseurs (avec un repartTypeGroupe a FO).
	 */
	public static NSArray getGroupesFournisseurs(EOEditingContext edc) {
		return EOStructure.fetchAll(edc, QUAL_GROUPE_FOURNISSEUR, new NSArray(new Object[] {
				EOStructure.SORT_LL_STRUCTURE_ASC
		}));
	}

	/**
	 * Définie la structure comme etant un groupe (repartTypeGroupe a G si necessaire) + initialise grpAcces. Le persIdCreation et le
	 * persIdModification sont initialisés avec le persIdModification de la structure.
	 * 
	 * @param structure
	 */
	public static void setStructureAsGroupe(EOStructure structure) {
		if (!isGroupeOfType(structure, EOTypeGroupe.TGRP_CODE_G)) {
			EORepartTypeGroupe repartTypeGroupe = EORepartTypeGroupe.creerInstance(structure.editingContext());
			repartTypeGroupe.setCStructure(structure.cStructure());
			repartTypeGroupe.setTgrpCode(EOTypeGroupe.TGRP_CODE_G);
			repartTypeGroupe.setTypeGroupeRelationship(EOTypeGroupe.fetchRequiredByKeyValue(structure.editingContext(), EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_G));
			repartTypeGroupe.setPersIdCreation(structure.persIdModification());
			repartTypeGroupe.setPersIdModification(structure.persIdModification());
			structure.addToToRepartTypeGroupesRelationship(repartTypeGroupe);
		}
	}

	/**
	 * @param structure
	 * @param qualifier
	 * @return Remonte l'arbre jusqu'à trouver la structure racine ou une structure qui reponde au qualifier passé en parametre. Si qualifier est
	 *         different de nul, que la recherche remonte à la racine et que la racine n'est pas compatible avec qualifier, la methode renvoie null.
	 */
	public static EOStructure getPremiereStructurePereWithQualifier(EOStructure structure, EOQualifier qualifier) {
		boolean found = false;
		EOStructure struct = structure;
		while (!found && !(struct.cStructure().equals(struct.cStructurePere()))) {
			if (qualifier != null && qualifier.evaluateWithObject(struct)) {
				found = true;
			}
			struct = struct.toStructurePere();
		}
		if (qualifier != null && qualifier.evaluateWithObject(struct)) {
			found = true;
		}
		if (!found) {
			return null;
		}
		else {
			return struct;
		}
	}

	/**
	 * @param structure
	 * @param ancetre
	 * @return true si la structure (groupe) est un descendant de ancetre (dans l'arborescence des groupes)
	 */
	public static boolean isGroupeDescendantOf(EOStructure structure, EOStructure ancetre) {
		if (structure != null && ancetre != null) {
			if (structure.editingContext() == null) {
				throw new RuntimeException("l'objet structure " + structure.toString() + " n'est pas dans un editing context");
			}
			if (ancetre.editingContext() == null) {
				throw new RuntimeException("l'objet ancetre " + ancetre.toString() + " n'est pas dans un editing context");
			}
			EOStructure pere = structure.toStructurePere();
			while (pere != null && (pere.toStructurePere() != null && !pere.globalID().equals(pere.toStructurePere().globalID()))) {
				if (pere.globalID().equals(ancetre.globalID())) {
					return true;
				}

				pere = pere.toStructurePere();
				//structure = pere;
			}
		}
		return false;
	}

	/**
	 * @param qual Qualifier pour identifier un etablissement. La determination d'un etablissement peut varier suivant le contexte. Par defaut, le
	 *            qualifier est un EOOrQualifier entre {@link EOStructure#QUAL_STRUCTURES_TYPE_ETABLISSEMENT} et
	 *            {@link EOStructure#QUAL_STRUCTURES_TYPE_ETABLISSEMENT_SECONDAIRE} , ce qui veut dire que par défaut la premiere structure trouvée en
	 *            remontant l'arbre qui répond à ce qualifier sera renvoyée. Si vous ne voulez pas qu'un établissement secondaire soit renvoyé,
	 *            indiquez EOStructure.QUAL_STRUCTURES_TYPE_ETABLISSEMENT ici.
	 * @return L'etablisement dont depend la structure
	 */
	public static EOStructure getEtablissementAffectation(EOStructure structure, EOQualifier qual) {
		if (qual == null) {
			qual = new EOOrQualifier(new NSArray(new Object[] {
					EOStructure.QUAL_STRUCTURES_TYPE_ETABLISSEMENT, EOStructure.QUAL_STRUCTURES_TYPE_ETABLISSEMENT_SECONDAIRE
			}));
		}
		NSMutableArray res = new NSMutableArray();
		EOStructure etab = EOStructureForGroupeSpec.getPremiereStructurePereWithQualifier(structure, qual);
		return etab;
	}

	/**
	 * Methode pour faciliter la creation d'une structure "groupe".
	 */
	public static EOStructure creerGroupe(EOEditingContext ec, Integer utilisateurPersId, String strAffichage, String lcStructure, NSArray typeGroupes, EOStructure groupePere) {
		EOStructure structure = EOStructure.creerInstance(ec, new NSArray(EOStructureForGroupeSpec.sharedInstance()));
		structure.setPersIdCreation(utilisateurPersId);
		structure.setPersIdModification(utilisateurPersId);
		structure.setStrAffichage(strAffichage);
		if (!MyStringCtrl.isEmpty(lcStructure)) {
			structure.setLcStructure(lcStructure);
		} else {
			structure.setLcStructure(MyStringCtrl.cut(strAffichage, 29));
		}
		structure.setToStructurePereRelationship(groupePere);
		if (groupePere != null) {
			structure.setCStructurePere(groupePere.cStructure());
			// Par defaut reprendre le responsable du groupe pere
			if (groupePere.toResponsable() != null) {
				structure.setToResponsableRelationship(groupePere.toResponsable().localInstanceIn(ec));
			}
			// Par defaut reprendre les secretariats du groupe pere
			if (groupePere.toSecretariats() != null && groupePere.toSecretariats().count() > 0) {
				for (Object secretariat : groupePere.toSecretariats()) {
					// le Cstructure dans secretatriat fait partie de la PK => donc clonnage des secrétariats
					EOSecretariat newSecretariat = EOSecretariat.creerInstance(ec);
					newSecretariat.setToIndividuRelationship(((EOSecretariat) secretariat).toIndividu().localInstanceIn(ec));
					newSecretariat.setDCreation(MyDateCtrl.now());
					newSecretariat.setDModification(MyDateCtrl.now());
					newSecretariat.setCStructure(structure.cStructure());
					structure.addToToSecretariatsRelationship(newSecretariat);
				}
			}
		}

		EOTypeGroupe typeGroupeG = EOTypeGroupe.fetchByKeyValue(ec, EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_G);
		setTypeOfGroupe(ec, utilisateurPersId, structure, typeGroupeG);
		//setTypeOfGroupe(structure.editingContext(), utilisateurPersId, structure, EOTypeGroupe.TGRP_CODE_G);

		if (typeGroupes != null) {
			for (int i = 0; i < typeGroupes.count(); i++) {
				EOTypeGroupe typeGroupe = (EOTypeGroupe) typeGroupes.objectAtIndex(i);
				setTypeOfGroupe(structure.editingContext(), utilisateurPersId, structure, typeGroupe);
				//setTypeOfGroupe(structure.editingContext(), utilisateurPersId, structure, typeGroupe.tgrpCode());
			}
		}
		return structure;
	}

	/**
	 * Affecte une personne dans un groupe en creant une instance de repartStructure si elle n'existe pas deja. Cette methode ne fonctionne pas dans
	 * le cas d'un groupe reserve.
	 * 
	 * @param ec
	 * @param personne La personne (structure ou individu) a inserer dans le groupe
	 * @param structureGroupe Le groupe dans lequel inserer la personne.
	 * @param persId Le persId de l'utilisateur
	 * @return L'objet existant trouvé ou bien un nouvel objet EORepartStructure ou null si lr groupe est reserve.
	 */
	public static EORepartStructure affecterPersonneDansGroupe(EOEditingContext ec, IPersonne personne, EOStructure structureGroupe, Integer persId) {
		return affecterPersonneDansGroupe(ec, personne, structureGroupe, persId, false);
	}

	/**
	 * Affecte une personne dans un groupe en creant une instance de repartStructure si elle n'existe pas deja. Cette methode ne fonctionne pas dans
	 * le cas d'un groupe reserve si autoriserGroupeReserve est false
	 * 
	 * @param ec
	 * @param personne La personne (structure ou individu) a inserer dans le groupe
	 * @param structureGroupe Le groupe dans lequel inserer la personne.
	 * @param persId Le persId de l'utilisateur
	 * @param autoriserGroupeReserve si true, le repartStructure est cree meme si le groupe est reserve.
	 * @return L'objet existant trouvé ou bien un nouvel objet EORepartStructure .
	 */
	public static EORepartStructure affecterPersonneDansGroupe(EOEditingContext ec, IPersonne personne, EOStructure structureGroupe, Integer persId, boolean autoriserGroupeReserve) {
		if (!autoriserGroupeReserve && isGroupeGereEnAuto(structureGroupe)) {
			return null;
		}
		return EORepartStructure.creerInstanceSiNecessaire(ec, personne, structureGroupe, persId);
	}

	/**
	 * Affecte une personne dans un groupe en creant une instance de repartStructure si elle n'existe pas deja. Cette methode cree l'affectation meme
	 * sur un groupe reserve.
	 * 
	 * @param ec
	 * @param personne La personne (structure ou individu) a inserer dans le groupe
	 * @param structureGroupe Le groupe dans lequel inserer la personne.
	 * @param persId Le persId de l'utilisateur
	 * @return L'objet existant trouvé ou bien un nouvel objet EORepartStructure ou null si lr groupe est reserve.
	 */
	public static EORepartStructure affecterPersonneDansGroupeForce(EOEditingContext ec, IPersonne personne, EOStructure structureGroupe, Integer persId) {
		return EORepartStructure.creerInstanceSiNecessaire(ec, personne, structureGroupe, persId);
	}

	/**
	 * Affecte une personne dans un groupe en creant une instance de repartStructure si elle n'existe pas deja, et lui affecte une association (en
	 * creant un EORepartAssociation).
	 * 
	 * @see EOStructureForGroupeSpec#affecterPersonneDansGroupe(EOEditingContext, IPersonne, EOStructure, Integer)
	 * @param ec
	 * @param personne La personne (structure ou individu) a inserer dans le groupe
	 * @param structureGroupe Le groupe dans lequel inserer la personne.
	 * @param persId Le persId de l'utilisateur
	 * @param association
	 * @return L'objet existant trouvé ou bien un nouvel objet EORepartStructure.
	 * @deprecated : ne plus utiliser cette methode, gerer d'abord l'affectation au groupe puis les roles (association).
	 */
	public static EORepartStructure affecterPersonneDansGroupe(EOEditingContext ec, IPersonne personne, EOStructure structureGroupe, Integer persId, EOAssociation association) {

		EORepartStructure repartStructure = EORepartStructure.creerInstanceSiNecessaire(ec, personne, structureGroupe, persId);

		if (association != null) {
			EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(ec);
			repartAssociation.setToAssociationRelationship(association);
			repartAssociation.setPersIdModification(persId);
			repartStructure.addToToRepartAssociationsRelationship(repartAssociation);
		}
		return repartStructure;
	}

	/**
	 * Définit un role pour une personne au sein d'un groupe. Crée un objet repartAssociation ou bien renvoie un objet repartAssociation qui existait
	 * deja avec meme groupe/personne/Association/ date debut (contrainte d'unicité sur ces attributs). L'objet repartStructure lié est créé
	 * automatiquement si necessaire. Utilisez cette methode plutot dans le cadre d'ajout de role sans interface. Ne fonctionne pas pour les groupes
	 * réservés.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @return L'objet EORepartAssociation
	 */
	public EORepartAssociation definitUnRole(EOEditingContext ec, IPersonne personne, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang) {
		return definitUnRole(ec, personne, association, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite, rasRang, false);
	}

	/**
	 * Définit un role pour une personne au sein d'un groupe. Crée un objet repartAssociation ou bien renvoie un objet repartAssociation qui existait
	 * deja avec meme groupe/personne/Association/ date debut (contrainte d'unicité sur ces attributs). L'objet repartStructure lié est créé
	 * automatiquement si necessaire. Utilisez cette methode plutot dans le cadre d'ajout de role sans interface.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @param autoriserGroupeReserve indique s'il la methode peut affecter un membre à un groupe réservé
	 * @return L'objet EORepartAssociation
	 */
	public static EORepartAssociation definitUnRole(EOEditingContext ec, IPersonne personne, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang,
			boolean autoriserGroupeReserve) {
		if (rasRang == null) {
			rasRang = EORepartAssociation.DEFAULT_RAS_RANG;
		}

		//Verifier s'il y a deja une association identique entre la structure et la personne
		EOQualifier qual1 = new EOKeyValueQualifier(EORepartAssociation.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, personne.persId());
		EOQualifier qual2 = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual, association);
		EOQualifier qual3 = new EOKeyValueQualifier(EORepartAssociation.RAS_D_OUVERTURE_KEY, EOQualifier.QualifierOperatorEqual, rasDOuverture);

		NSArray res1 = EOQualifier.filteredArrayWithQualifier(groupe.toRepartStructuresElts(), qual1);
		NSMutableArray array = new NSMutableArray();
		for (int i = 0; i < res1.count(); i++) {
			EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
			array.addObjectsFromArray(array_element.toRepartAssociations(new EOAndQualifier(new NSArray(new Object[] {
					qual2, qual3
			}))));
		}
		EORepartAssociation res = null;
		if (array.count() > 0) {
			res = (EORepartAssociation) array.objectAtIndex(0);
		}
		if (res == null) {
			EORepartStructure repartStructure = affecterPersonneDansGroupe(ec, personne, groupe, persIdUtilisateur, autoriserGroupeReserve);
			//EORepartStructure repartStructure = EORepartStructure.creerInstanceSiNecessaire(ec, personne, groupe, persIdUtilisateur);
			EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(ec);
			repartAssociation.setToAssociationRelationship(association);
			repartAssociation.setPersIdModification(persIdUtilisateur);
			repartStructure.addToToRepartAssociationsRelationship(repartAssociation);
			repartAssociation.setRasDOuverture(rasDOuverture);
			repartAssociation.setRasDFermeture(rasDFermeture);
			repartAssociation.setRasCommentaire(rasCommentaire);
			repartAssociation.setRasQuotite(rasQuotite);
			repartAssociation.setRasRang(rasRang);
			res = repartAssociation;
		}
		return res;
	}

	/**
	 * Définit les roles pour une personne au sein d'un groupe. Elimine les repartAssociations precedemment existantes Retourne un tableau
	 * d'EORepartAssociation nouvelles L'objet repartStructure lié est créé automatiquement si necessaire. Utilisez cette methode plutot dans le cadre
	 * d'ajout de roles sans interface.
	 * 
	 * @param ec
	 * @param associations
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @param autoriserGroupeReserve
	 * @return NSArray des EORepartAssociation
	 */

	public static NSArray definitLesRoles(EOEditingContext ec, IPersonne personne, NSArray associations, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang, boolean autoriserGroupeReserve) {
		NSMutableArray lesRepartAssociations = new NSMutableArray();

		if (rasRang == null) {
			rasRang = EORepartAssociation.DEFAULT_RAS_RANG;
		}
		EORepartStructure repartStructure = affecterPersonneDansGroupe(ec, personne, groupe, persIdUtilisateur, autoriserGroupeReserve);
		if (associations != null) {
			repartStructure.deleteAllToRepartAssociationsRelationships();
			Enumeration<EOAssociation> enumAssociations = associations.objectEnumerator();
			while (enumAssociations.hasMoreElements()) {
				EOAssociation eoAssociation = (EOAssociation) enumAssociations.nextElement();
				EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(ec);
				repartAssociation.setToAssociationRelationship(eoAssociation);
				repartAssociation.setPersIdModification(persIdUtilisateur);
				repartStructure.addToToRepartAssociationsRelationship(repartAssociation);
				repartAssociation.setRasDOuverture(rasDOuverture);
				repartAssociation.setRasDFermeture(rasDFermeture);
				repartAssociation.setRasCommentaire(rasCommentaire);
				repartAssociation.setRasQuotite(rasQuotite);
				repartAssociation.setRasRang(rasRang);
				lesRepartAssociations.add(repartAssociation);
			}
		}
		return lesRepartAssociations.immutableClone();
	}


	/**
	 * Définit les roles pour une personne au sein d'un groupe. Elimine les repartAssociations precedemment existantes Retourne un tableau
	 * d'EORepartAssociation nouvelles L'objet repartStructure lié est créé automatiquement si necessaire. Utilisez cette methode plutot dans le cadre
	 * d'ajout de roles sans interface.
	 * 
	 * @param ec
	 * @param associations
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @return NSArray des EORepartAssociation
	 */
	public static NSArray definitLesRoles(EOEditingContext ec, IPersonne personne, NSArray associations, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang) {
		return definitLesRoles(ec, personne, associations, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite, rasRang, false);
	}

	/**
	 * Ajout un role pour une personne au sein d'un groupe. L'objet repartStructure lié est créé automatiquement si necessaire. 
	 * La repartAssociation est aussi créée si nécessaire. Si elle est trouvée avant, elle est retournée
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @return EORepartAssociation
	 */	 
	public static EORepartAssociation ajouterUnRole(EOEditingContext ec, IPersonne personne, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang, boolean autoriserGroupeReserve) {
		EORepartAssociation repartAssociation = null;
		if (rasRang == null) {
			rasRang = EORepartAssociation.DEFAULT_RAS_RANG;
		}
		EORepartStructure repartStructure = affecterPersonneDansGroupe(ec, personne, groupe, persIdUtilisateur, autoriserGroupeReserve);
		if(repartStructure != null) {
			if (association != null) {
				EOQualifier qualifier = EORepartAssociation.TO_ASSOCIATION.eq(association).and(EORepartAssociation.RAS_D_OUVERTURE.eq(rasDOuverture));
				EORepartAssociation repartAsso = ERXArrayUtilities.firstObject(repartStructure.toRepartAssociations(qualifier));
				if(repartAsso == null) {
					repartAssociation = EORepartAssociation.creerInstance(ec);
					repartAssociation.setToAssociationRelationship(association);
					repartAssociation.setPersIdModification(persIdUtilisateur);
					repartAssociation.setToStructureRelationship(groupe);
					repartAssociation.setToPersonne(personne);
					repartStructure.addToToRepartAssociationsRelationship(repartAssociation);
					repartAssociation.setRasDOuverture(rasDOuverture);
					repartAssociation.setRasDFermeture(rasDFermeture);
					repartAssociation.setRasCommentaire(rasCommentaire);
					repartAssociation.setRasQuotite(rasQuotite);
					repartAssociation.setRasRang(rasRang);
				}
			}
		}
		return repartAssociation;
	}
	/**
	 * Supprime le repartStructure pour une personne et un groupe donnés.
	 * 
	 * @param editingContext editingContext
	 * @param personne personne que l'on supprime du groupe
	 * @param groupe goupe dans lequel la personne est supprimée
	 * @param persIdUtilisateur persId de la personne faisant la modification
	 */
	public static void supprimerPersonneDuGroupe(EOEditingContext editingContext, IPersonne personne, EOStructure groupe, Integer persIdUtilisateur) {
		personne.supprimerAffectationAUnGroupe(editingContext, persIdUtilisateur, groupe);
		//EORepartStructure.supprimerRepartPourPersonneEtGroupe(editingContext, personne, groupe);
	}

	/**
	 * Supprime les roles specifiques (repartAssociations) de la personne au sein d'un groupe.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur
	 */
	public void supprimerLesRoles(EOEditingContext ec, IPersonne personne, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur) {
		EOQualifier qual1 = new EOKeyValueQualifier(EORepartAssociation.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, personne.persId());
		EOQualifier qual2 = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual, association);

		NSArray res1 = EOQualifier.filteredArrayWithQualifier(groupe.toRepartStructuresElts(), qual1);
		NSMutableArray array = new NSMutableArray();
		for (int i = 0; i < res1.count(); i++) {
			EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
			NSArray assos = array_element.toRepartAssociations(new EOAndQualifier(new NSArray(new Object[] {
					qual2
			})));
			array.addObjectsFromArray(assos);
			for (int j = 0; j < assos.count(); j++) {
				array_element.removeFromToRepartAssociationsRelationship((EORepartAssociation) assos.get(j));
			}
		}
		if (array.count() > 0) {
			for (int i = 0; i < array.count(); i++) {
				EORepartAssociation res = (EORepartAssociation) array.objectAtIndex(i);
				res.setToAssociationRelationship(null);
				res.setToPersonne(null);
				res.setToStructureRelationship(null);
				ec.deleteObject(res);
			}
		}

	}

	/*
	 * public void supprimerLesRoles(EOEditingContext ec, IPersonne personne, EOAssociation association, EOStructure groupe, Integer
	 * persIdUtilisateur) { EOQualifier qual1 = new EOKeyValueQualifier(EORepartAssociation.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual,
	 * personne.persId()); EOQualifier qual2 = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual,
	 * association);
	 * 
	 * NSArray res1 = EOQualifier.filteredArrayWithQualifier(groupe.toRepartStructuresElts(), qual1); NSMutableArray array = new NSMutableArray(); for
	 * (int i = 0; i < res1.count(); i++) { EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
	 * array.addObjectsFromArray(array_element.toRepartAssociations(new EOAndQualifier(new NSArray(new Object[] { qual2 })))); } if (array.count() >
	 * 0) { for (int i = 0; i < array.count(); i++) { EORepartAssociation res = (EORepartAssociation) array.objectAtIndex(i);
	 * res.setToAssociationRelationship(null); res.setToPersonne(null); res.setToStructureRelationship(null); ec.deleteObject(res); } }
	 * 
	 * }
	 */

	/**
	 * Supprime un role specifique (repartAssociation) de la personne au sein d'un groupe commencant a une date donnee. Le repartStructure n'est pas
	 * supprime.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur
	 * @param rasDOuverture
	 */
	public void supprimerUnRole(EOEditingContext ec, IPersonne personne, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture) {
		//Verifier s'il y a deja une association identique entre la structure et la personne

		EOQualifier qual1 = EORepartAssociation.PERS_ID.eq(personne.persId());
		EOQualifier qual2 = EORepartAssociation.TO_ASSOCIATION.eq(association);
		EOQualifier qual3 = EORepartAssociation.RAS_D_OUVERTURE.eq(rasDOuverture);

		NSArray<EORepartStructure> repartsFiltrees = EOQualifier.filteredArrayWithQualifier(groupe.toRepartStructuresElts(), qual1);
		if (!repartsFiltrees.isEmpty()) {
			EORepartStructure repartStructure = ERXArrayUtilities.firstObject(repartsFiltrees);
			EORepartAssociation repartAsso = repartStructure.toRepartAssociations(ERXQ.and(qual2, qual3)).lastObject();
			if (repartAsso != null) {
				repartStructure.deleteToRepartAssociationsRelationship(repartAsso);
				repartAsso.setToAssociationRelationship(null);
				repartAsso.setToPersonne(null);
				repartAsso.setToStructureRelationship(null);
				ec.deleteObject(repartAsso);
			}
		}

	}

	/**
	 * Supprime le role d'une personne au sein d'un groupe. Si une personne a plusieurs fois le même role pour le groupe ils sont tous supprimés.
	 * 
	 * @param editingContext
	 * @param personne
	 * @param groupe
	 * @param association Rôle de la personne au sein du groupe
	 * @deprecated Utilisez {@link EOStructureForGroupeSpec#supprimerLesRoles(EOEditingContext, IPersonne, EOAssociation, EOStructure, Integer)}
	 */
	public static void supprimerRoleDePersonneDuGroupe(EOEditingContext editingContext, IPersonne personne, EOStructure groupe, EOAssociation association) {
		personne.supprimerLesRoles(editingContext, association, groupe, personne.persIdModification());

		//		EOQualifier qual2 = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual, association);
		//		EOQualifier qual1 = new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, groupe);
		//		NSArray res = personne.toRepartStructures(qual1);
		//		for (int i = 0; i < res.count(); i++) {
		//			EORepartStructure repart = (EORepartStructure) res.objectAtIndex(i);
		//			NSArray ras = repart.toRepartAssociations(qual2);
		//			for (int j = 0; j < ras.count(); j++) {
		//				repart.deleteToRepartAssociationsRelationship((EORepartAssociation) ras.objectAtIndex(j));
		//			}
		//		}
	}

	/**
	 * Définit le type de groupe (ajoute un EORepartTypeGroupe) si pas deja defini.
	 * 
	 * @param ec
	 * @param structure
	 * @param tgrpCode
	 * @deprecated
	 */
	public static void setTypeOfGroupe(EOEditingContext ec, Integer utilisateurPersId, EOStructure structure, String tgrpCode) {
		if (tgrpCode == null) {
			return;
		}
		if (!isGroupeOfType(structure, tgrpCode)) {
			EORepartTypeGroupe repartTypeGroupe = EORepartTypeGroupe.creerInstance(ec);
			repartTypeGroupe.setCStructure(structure.cStructure());
			repartTypeGroupe.setTgrpCode(tgrpCode);
			repartTypeGroupe.setPersIdCreation(utilisateurPersId);
			repartTypeGroupe.setPersIdModification(utilisateurPersId);
			structure.addToToRepartTypeGroupesRelationship(repartTypeGroupe);
		}
	}

	/**
	 * Définit le type de groupe (ajoute un EORepartTypeGroupe) si pas deja defini.
	 * 
	 * @param ec
	 * @param utilisateurPersId
	 * @param structure
	 * @param typeGroupe
	 */
	public static void setTypeOfGroupe(EOEditingContext ec, Integer utilisateurPersId, EOStructure structure, EOTypeGroupe typeGroupe) {
		if (typeGroupe == null) {
			return;
		}
		if (!isGroupeOfType(structure, typeGroupe.tgrpCode())) {
			EORepartTypeGroupe repartTypeGroupe = EORepartTypeGroupe.creerInstance(ec);
			repartTypeGroupe.setCStructure(structure.cStructure());
			repartTypeGroupe.setTypeGroupeRelationship(typeGroupe);
			repartTypeGroupe.setTgrpCode(typeGroupe.tgrpCode());
			repartTypeGroupe.setPersIdCreation(utilisateurPersId);
			repartTypeGroupe.setPersIdModification(utilisateurPersId);
			structure.addToToRepartTypeGroupesRelationship(repartTypeGroupe);
		}
	}

	/**
	 * Enleve le type au groupe.
	 * 
	 * @param ec
	 * @param utilisateurPersId
	 * @param structure
	 * @param tgrpCode
	 */
	public static void removeTypeOfGroupe(EOEditingContext ec, Integer utilisateurPersId, EOStructure structure, String tgrpCode) {
		if (tgrpCode == null) {
			return;
		}
		NSArray res = structure.toRepartTypeGroupes(new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, tgrpCode));
		for (int i = res.count() - 1; i >= 0; i--) {
			EORepartTypeGroupe repartTypeGroupe = (EORepartTypeGroupe) res.objectAtIndex(i);
			repartTypeGroupe.setPersIdModification(utilisateurPersId);
			structure.removeFromToRepartTypeGroupesRelationship(repartTypeGroupe);
			ec.deleteObject(repartTypeGroupe);
		}
	}

	/**
	 * @param groupe
	 * @return Les EOTypeGroupe affectes au groupe (via toRepartTypeGroupe).
	 */
	public static NSArray getTypesGroupe(EOStructure groupe) {
		NSArray reparts = groupe.toRepartTypeGroupes();
		if (reparts.count() > 0) {
			NSMutableArray quals = new NSMutableArray();
			for (int i = 0; i < reparts.count(); i++) {
				EORepartTypeGroupe rtg = (EORepartTypeGroupe) reparts.objectAtIndex(i);
				quals.addObject(new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, rtg.tgrpCode()));
			}
			return EOTypeGroupe.fetchAll(groupe.editingContext(), new EOOrQualifier(quals));
		}
		else {
			return NSArray.EmptyArray;
		}

	}

	/**
	 * @param groupe
	 * @return Tous les membres du groupe (via repartStructureElts)
	 */
	public static NSArray<IPersonne> getPersonnesInGroupe(EOStructure groupe) {
		return getPersonnesInGroupe(groupe, (EOQualifier) null);
	}
	
	/**
	 * @param groupe
	 * @return Tous les membres du groupe (via repartStructureElts)
	 */
	public static NSArray<IPersonne> getPersonnesInGroupe(EOStructure groupe, EOQualifier qual) {
		NSArray reparts = groupe.toRepartStructuresElts(qual, true);
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < reparts.count(); i++) {
			res.addObject(((EORepartStructure) reparts.objectAtIndex(i)).toPersonneElt());
		}
		return res.immutableClone();
	}

	/**
	 * @param groupe
	 * @param association Le role sur le lequel filtrer
	 * @return Les personnes membres du groupe qui ont comme role association
	 * @see EOStructureForGroupeSpec#getRepartStructureEltsInGroupe(EOStructure, EOAssociation)
	 */
	public static NSArray getPersonnesInGroupe(EOStructure groupe, EOAssociation association) {
		NSArray reparts = getRepartStructureEltsInGroupe(groupe, association);
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < reparts.count(); i++) {
			res.addObject(((EORepartStructure) reparts.objectAtIndex(i)).toPersonneElt());
		}
		return res.immutableClone();
	}

	/**
	 * @param groupe
	 * @param association Le role sur le lequel filtrer
	 * @return Les repartStructuresElts (membres) du groupe qui ont comme role association
	 * @see EOStructureForGroupeSpec#getPersonnesInGroupe(EOStructure, EOAssociation)
	 */
	public static NSArray getRepartStructureEltsInGroupe(EOStructure groupe, EOAssociation association) {
		NSArray reparts = groupe.toRepartStructuresElts();
		NSMutableArray res = new NSMutableArray();
		EOKeyValueQualifier qual = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual, association);

		for (int i = 0; i < reparts.count(); i++) {
			EORepartStructure repart = ((EORepartStructure) reparts.objectAtIndex(i));
			if (repart.toRepartAssociations(qual).count() > 0) {
				res.addObject(repart);
			}
		}
		return res.immutableClone();
	}

	/**
	 * @param groupe
	 * @return True si le groupe est un groupe integre a GRHUM (fournisseurs, entreprises, etc.), c'es a dire si l'affectation au groupe depend de
	 *         diverses regles metiers controlees par GRHUM. Egalement true si le groupe est un descendant du groupe REFERENTIEL APPLICATIFS.
	 */
	public static boolean isGroupeGereEnAuto(EOStructure groupe) {
		if (groupe != null) {
			String cStructure = groupe.cStructure();
			if (getCStructureForGroupesReserves(groupe.editingContext()).containsObject(cStructure)) {
				return true;
			}
			try {
				EOStructure groupeRefApp = EOStructureForGroupeSpec.getGroupeReferentielApplicatifs(groupe.editingContext());
				if (isGroupeDescendantOf(groupe, groupeRefApp)) {
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * @param ec
	 * @return Les groupes dont la gestion est interne.
	 */
	public static NSArray getGroupesReserves(EOEditingContext ec) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(QUAL_GROUPE_FOURNISSEUR);
		NSArray res = getCStructureForGroupesReserves(ec);
		Enumeration ens = res.objectEnumerator();
		while (ens.hasMoreElements()) {
			quals.addObject(new EOKeyValueQualifier(EOStructure.C_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, ens.nextElement()));
		}
		return EOStructure.fetchAll(ec, new EOOrQualifier(quals));
	}

	/**
	 * @param ec
	 * @return Les cStructure des groupes reserves (geres en auto). Recupere d'apres les parametre de grhum. Le controle n'est pas effectue sur
	 *         l'existance de la structure identifiee.
	 */
	public static NSArray getCStructureForGroupesReserves(EOEditingContext ec) {
		if (CSTRUCTURE_FOR_GROUPES_RESERVES == null || CSTRUCTURE_FOR_GROUPES_RESERVES.count() == 0) {
			CSTRUCTURE_FOR_GROUPES_RESERVES = new NSMutableArray();
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_CREATION_ENQUETES_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_CREATION_ENQUETES_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_CREATION_FORUMS_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_CREATION_FORUMS_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_EMPLOYEUR_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_EMPLOYEUR_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_ENTREPRISE_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_ENTREPRISE_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ARCHIVES_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ARCHIVES_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ENCOURS_VALIDE_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ENCOURS_VALIDE_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ENTREPRISE_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_ENTREPRISE_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_FOU_ETUDIANT_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_FOU_ETUDIANT_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_EXTERNE_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_EXTERNE_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_INTERNE_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_INTERNE_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_MORALE_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_MORALE_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_PHYSIQUE_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_PHYSIQUE_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_GROUPE_UNIX_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_GROUPE_UNIX_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_INVITE_WIFI_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_INVITE_WIFI_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_PARTENARIAT_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_PARTENARIAT_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_TICKET_WIFI_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_TICKET_WIFI_KEY));
			}
			if (EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_REFAPP_KEY) != null) {
				CSTRUCTURE_FOR_GROUPES_RESERVES.addObject(EOGrhumParametres.parametrePourCle(ec, ANNUAIRE_REFAPP_KEY));
			}
		}
		return CSTRUCTURE_FOR_GROUPES_RESERVES;

	}

	/**
	 * Supprime l'affectation à un groupe (repartStructure + repartAssociations) même s'il s'agit d'un groupe réservé.
	 * 
	 * @param ec
	 * @param persIdUtilisateur
	 * @param repartStructure
	 */
	public void supprimerAffectationAUnGroupeForce(EOEditingContext ec, Integer persIdUtilisateur, EORepartStructure repartStructure) {
		supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repartStructure, true);

	}

	/**
	 * Supprime l'affectation à un groupe (repartStructure + repartAssociations) sauf s'il s'agit d'un groupe réservé.
	 * 
	 * @param ec
	 * @param persIdUtilisateur
	 * @param repartStructure
	 */
	public void supprimerAffectationAUnGroupe(EOEditingContext ec, Integer persIdUtilisateur, EORepartStructure repartStructure) {
		supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repartStructure, false);
	}

	/**
	 * Supprime l'affectation à un groupe (repartStructure + repartAssociations).
	 * 
	 * @param ec
	 * @param persIdUtilisateur
	 * @param repartStructure
	 * @param autoriserGroupeReserve Si true la suppression est effectuée même si le groupe est reservé.
	 */
	public void supprimerAffectationAUnGroupe(EOEditingContext ec, Integer persIdUtilisateur, EORepartStructure repartStructure, boolean autoriserGroupeReserve) {
		if (repartStructure != null) {
			//Verifier qu'on n'est pas dans un groupe gere en auto
			if (repartStructure.toStructureGroupe() != null) {
				if (EOStructureForGroupeSpec.isGroupeGereEnAuto(repartStructure.toStructureGroupe())) {
					if (!autoriserGroupeReserve) {
						return;
					}
				}
			}
			
			//Met à jour la personne et date de modification
			repartStructure.majModificationPourRepartStructureEtAssociations(ec, persIdUtilisateur);
			
			repartStructure.deleteAllToRepartAssociationsRelationships();
			
			if (repartStructure.toPersonneElt() != null) {
				repartStructure.toPersonneElt().getPersonneDelegate().fixPersIdModificationEtCreation(persIdUtilisateur);
			}
			if (repartStructure.toStructureGroupe() != null) {
				repartStructure.toStructureGroupe().getPersonneDelegate().fixPersIdModificationEtCreation(persIdUtilisateur);
			}
			if (repartStructure.toPersonneElt() != null) {
				repartStructure.toPersonneElt().removeFromToRepartStructuresRelationship(repartStructure);
			}
			if (repartStructure.toStructureGroupe() != null) {
				repartStructure.toStructureGroupe().removeFromToRepartStructuresEltsRelationship(repartStructure);
			}
			
			repartStructure.setCStructure(null);
			ec.deleteObject(repartStructure);
		}

	}

	public void supprimerAffectationAUnGroupe(EOEditingContext ec, IPersonne personne, Integer persIdUtilisateur, EOStructure groupe) {
		if (groupe != null) {
			NSArray reparts = personne.toRepartStructures(new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, groupe));
			Enumeration e = reparts.objectEnumerator();
			while (e.hasMoreElements()) {
				EORepartStructure repart = (EORepartStructure) e.nextElement();
				supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repart);
			}
		}
	}

	public void supprimerAffectationAUnGroupeForce(EOEditingContext ec, IPersonne personne, Integer persIdUtilisateur, EOStructure groupe) {
		if (groupe != null) {
			NSArray reparts = personne.toRepartStructures(new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, groupe));
			Enumeration e = reparts.objectEnumerator();
			while (e.hasMoreElements()) {
				EORepartStructure repart = (EORepartStructure) e.nextElement();
				supprimerAffectationAUnGroupeForce(ec, persIdUtilisateur, repart);
			}
		}
	}

	public void supprimerAffectationATousLesGroupes(EOEditingContext ec, IPersonne personne, Integer persIdUtilisateur) {
		NSArray reparts = personne.toRepartStructures();
		Enumeration e = reparts.objectEnumerator();
		while (e.hasMoreElements()) {
			EORepartStructure repart = (EORepartStructure) e.nextElement();
			supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repart);
		}
	}

	/**
	 * @param groupe Un groupe
	 * @param qualifier optionnel
	 * @return Les repartAssociations affectees a la personne au sein du groupe
	 */
	public NSArray getRepartAssociationsInGroupe(IPersonne personne, EOStructure groupe, EOQualifier qualifier) {
		EOKeyValueQualifier qualGroupe = new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOKeyValueQualifier.QualifierOperatorEqual, groupe);
		NSArray res1 = personne.toRepartStructures(qualGroupe);
		NSMutableArray array = new NSMutableArray();
		for (int i = 0; i < res1.count(); i++) {
			EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
			array.addObjectsFromArray(array_element.toRepartAssociations(qualifier));
		}
		return array.immutableClone();
	}

	/**
	 * Affecte la personne au groupe par defaut si celle-ci n'est pas deja dans un groupe de type REFERENTIEL.
	 * 
	 * @param persId PersId de l'utilisateur
	 * @throws NSValidation.ValidationException
	 */
	public void fixGroupeDefaut(IPersonne personne, Integer persId) throws NSValidation.ValidationException {
		if (personne.toRepartStructures().count() == 0) {
			if (!EOStructureForGroupeSpec.isPersonneInGroupeOfType(personne, EOTypeGroupe.TGRP_CODE_RE)) {
				try {
					EOStructure groupe = EOStructureForGroupeSpec.getGroupeDefaut(personne.editingContext());
					EORepartStructure repart = affecterPersonneDansGroupeForce(personne.editingContext(), personne, groupe, persId);
					//					EORepartStructure repart = EORepartStructure.creerInstanceSiNecessaire(personne.editingContext(), personne, groupe, persId);
					if (repart.hasTemporaryGlobalID()) {
						repart.setPersIdCreation(personne.persIdModification());
					}
					repart.setPersIdModification(personne.persIdModification());

				} catch (Exception e) {
					e.printStackTrace();
					throw new NSValidation.ValidationException(e.getMessage());
				}
			}
		}
	}

	/**
	 * @param groupe
	 * @param qualifier
	 * @return Les sous-groupes d'un groupe (via relation structurePere) de maniere NON recursive (seulement un niveau).
	 */
	public NSArray getSousGroupes(EOEditingContext ec, EOStructure groupe, EOQualifier qualifier) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOStructure.TO_STRUCTURE_PERE_KEY, EOQualifier.QualifierOperatorEqual, groupe));
		if (qualifier != null) {
			quals.addObject(qualifier);
		}

		NSArray ssGroupes = EOStructure.fetchAll(ec, new EOAndQualifier(quals), new NSArray(new Object[] {
				EOStructure.SORT_LL_STRUCTURE_ASC
		}));
		ssGroupes.remove(groupe);
		return ssGroupes;
	}

	/**
	 * @param ec
	 * @param groupe
	 * @param qualifier
	 * @return Les sous-groupes d'un groupe (via relation structurePere) de maniere recursive.
	 */
	public NSArray getSousGroupesRecursif(EOEditingContext ec, EOStructure groupe, EOQualifier qualifier) {
		NSMutableArray res = new NSMutableArray();
		NSArray groupes = getSousGroupes(ec, groupe, qualifier);
		for (int i = 0; i < groupes.count(); i++) {
			EOStructure unGroupe = (EOStructure) groupes.objectAtIndex(i);
			res.addObjectsFromArray(getSousGroupesRecursif(ec, unGroupe, qualifier));
		}
		res.addObjectsFromArray(groupes);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				EOStructure.SORT_LL_STRUCTURE_ASC
		})).immutableClone();
	}

	/**
	 * Nettoie le cache des structures référencees dans les parametres.
	 */
	public static void cleanParamStructureCache() {
		PARAM_STRUCTURES_CACHE.clear();
	}

}
