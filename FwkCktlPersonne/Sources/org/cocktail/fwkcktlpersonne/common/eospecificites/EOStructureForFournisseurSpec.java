/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXQ;

/**
 * Les specificites metiers pour une structure de type fournisseur.
 * <ul>
 * <li>Règles d'initialisation : {@link EOStructureForFournisseurSpec#onAwakeFromInsertion(AfwkPersRecord)}</li>
 * <li>Règles de validation : {@link EOStructureForFournisseurSpec#onValidateObjectMetier(AfwkPersRecord)}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOStructureForFournisseurSpec extends EOStructureForPersonneMoraleSpec {

	private static EOStructureForFournisseurSpec sharedInstance = new EOStructureForFournisseurSpec();

	public static final EOQualifier QUAL_STRUCTURES_IN_GROUPE_TYPE_FOURNISSEUR = new EOKeyValueQualifier(EOStructure.TO_REPART_STRUCTURES_KEY + "."
	    + EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY,
	    EOQualifier.QualifierOperatorContains, EOTypeGroupe.TGRP_CODE_FO);

	public static EOStructureForFournisseurSpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOStructureForFournisseurSpec() {
		super();
	}

	@Override
	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		super.onAwakeFromInsertion(afwkPersRecord);
	}

	@Override
	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateBeforeTransactionSave(afwkPersRecord);
	}

	@Override
	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForDelete(afwkPersRecord);
	}

	@Override
	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForInsert(afwkPersRecord);
	}

	@Override
	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateForUpdate(afwkPersRecord);
	}

	/**
	 * Appelé lors de la validation d'une structure de type fournisseur. Controles : <br/>
	 * <ul>
	 * <li>La structure doit être reliée à un fournisseur.</li>
	 * <li>La structure doit faire partie d'un groupe Fournisseur</li>
	 * <li>Il doit y avoir une coherence entre le champ fou_valide et l'affectation de la structure a un groupe</li>
	 * <li>La structure doit avoir un repart_personne_adresse de type FACT affecté</li>
	 * <li>Si le parametre ANNUAIRE_FOU_COMPTE_AUTO est à O, et que le fournisseur est valide, la structure associee doit avoir un compte de connexion (Ne verifie
	 * pas si le compte est valide car le compte peut être desactive pour une raison ou une autre.).</li>
	 * <li>Si parametre GRHUM_CONTROLE_SIRET est a OUI, la presence du SIRET est obligatoire pour les fournisseurs externes non étrangers (sauf les fournisseurs
	 * qui ne sont que clients). Si la forme juridique de la structure est renseigné, selon la valeur du champ CONTROLE_SIRET_FJD, le contrôle peut ne pas être
	 * effectué.</li>
	 * </ul>
	 * 
	 * @see org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForPersonneMoraleSpec#onValidateObjectMetier(org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord)
	 */
	@Override
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
		super.onValidateObjectMetier(afwkPersRecord);
		EOStructure structure = (EOStructure) afwkPersRecord;
		if (structure.toFournis() == null) {
			throw new NSValidation.ValidationException("le lien vers le fournisseur est obligatoire.");
		}

		if (afwkPersRecord.isObjectInValidationEditingContext()) {
			// Verifier coherence fou_valide / groupe
			checkGroupes(structure);

			// verifier adresse FACT saisie
			getListRepartionAdresseFacturation(structure);

			// verifier que le compte est cree (selon parametre)
			if (EOFournis.FOU_VALIDE_OUI.equals(structure.toFournis().fouValide())) {
				Boolean creerCompteAuto = Boolean.valueOf(EOGrhumParametres.OUI.equals(EOGrhumParametres.parametrePourCle(structure.editingContext(),
				    EOGrhumParametres.PARAM_ANNUAIRE_FOU_COMPTE_AUTO)));
				if (creerCompteAuto.booleanValue()) {
					if (structure.toComptes().count() == 0) {
						throw new NSValidation.ValidationException(
						    "Un compte de connexion devrait être affecté a la structure (car le parametre ANNUAIRE_FOU_COMPTE_AUTO est à O).");
					}
				}
			}

			// Si fournisseur externe, on verifie le SIRET si besoin
			if (!structure.isInterneEtablissement().booleanValue() && !structure.toFournis().isTypeClient()) {
				String verifierSiret = EOGrhumParametres.parametrePourCle(structure.editingContext(), EOGrhumParametres.PARAM_GRHUM_CONTROLE_SIRET);
				if (verifierSiret != null && verifierSiret.startsWith(AfwkPersRecord.OUI)) {
					// Si la forme juridique ne demande pas de SIRET, on ne fait pas le controle.
					if (structure.toFormesJuridiquesDetails() == null
					    || !EOFormesJuridiquesDetails.CONTROLE_SIRET_NON.equals(structure.toFormesJuridiquesDetails().controleSiretFjd())) {
						if (structure.siret() == null && !structure.toFournis().isEtranger().booleanValue()) {
							throw new NSValidation.ValidationException("La saisie du SIRET est obligatoire pour un fournisseur non étranger "
							    + (structure.toFormesJuridiquesDetails() != null ? " pour la forme juridque sélectionnée." : "") + " (parametre GRHUM_CONTROLE_SIRET=OUI) ");
						}
					}
				}
			}
		}

	}

	public NSArray<EORepartPersonneAdresse> getListRepartionAdresseFacturation(EOStructure structure) {
		NSArray<EORepartPersonneAdresse> res = structure.toRepartPersonneAdresses(new EOAndQualifier(new NSArray(new Object[] {
		    EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE, EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_FACT })));
		if (res.count() == 0) {
			throw new NSValidation.ValidationException("Une adresse de type facturation est obligatoire pour une personne morale de type fournisseur.");
		}
		return res;
	}

	/**
	 * @param structure structure pour laquelle on recherche une adresse de facturation
	 * @return la première adresse de facturation valide
	 */
	public IAdresse getAdresseFacturationPrincipale(EOStructure structure) {
		NSArray<EORepartPersonneAdresse> res = getListRepartionAdresseFacturation(structure);
		if (res.count() >= 0) {
			return res.get(0).toAdresse();
		}
		return null;
	}

	/**
	 * Verifie la coherence des groupes affectes a la structure.
	 * <ul>
	 * <li>La structure doit être dans un groupe de type fournisseur (repartTypeGroupe FO)</li>
	 * <li>Si le fournisseur est valide, la structure doit etre dans le groupe des fournisseurs valides</li>
	 * <li>Si le fournisseur est en cours de validation, la structure doit etre dans le groupe des fournisseurs en cours de creation</li>
	 * <li>Si le fournisseur est annule, la structure doit etre dans le groupe des fournisseurs archives</li>
	 * <li>Si le fournisseur est valide et est un client, la structure doit etre dans le groupe des fournisseurs valides externes (07/04/2010 : regle desactivee
	 * (initialement servait à régler les pbs de PIEWEB (creation auto) mais pose pb ailleurs).)</li>
	 * </ul>
	 * 
	 * @param structure
	 */
	public static void checkGroupes(EOStructure structure) {
		try {
			// lastructure doit etre dans un groupe de fournisseur
			if (!EOStructureForGroupeSpec.isPersonneInGroupeOfType(structure, EOTypeGroupe.TGRP_CODE_FO)) {
				throw new NSValidation.ValidationException("La personne de type fournisseur doit être affectée a un groupe de fournisseur (tgrp_code=FO).");
			}

			if (EOFournis.FOU_VALIDE_NON.equals(structure.toFournis().fouValide())) {
				if (!EOStructureForGroupeSpec.isPersonneInGroupe(structure, EOStructureForGroupeSpec.getGroupeFournisseurEnCours(structure.editingContext()))) {
					throw new NSValidation.ValidationException(
					    "Un fournisseur en cours de validation doit être affectée au groupe des fournisseurs en cours de validation.");
				}
			}
			if (EOFournis.FOU_VALIDE_ANNULE.equals(structure.toFournis().fouValide())) {
				if (!EOStructureForGroupeSpec.isPersonneInGroupe(structure, EOStructureForGroupeSpec.getGroupeFournisseurAnnules(structure.editingContext()))) {
					throw new NSValidation.ValidationException("Un fournisseur annulé doit être affectée au groupe des fournisseurs annulés.");
				}
			}

			if (structure.toFournis().isTypeFournisseur() || structure.toFournis().isTypeTiers()) {
				if (EOFournis.FOU_VALIDE_OUI.equals(structure.toFournis().fouValide())) {
					if (!EOStructureForGroupeSpec.isPersonneInGroupe(structure, EOStructureForGroupeSpec.getGroupeFournisseurValideMorale(structure.editingContext()))) {
						throw new NSValidation.ValidationException("Un fournisseur valide doit être affectée à un groupe de fournisseurs valide.");
					}
				}
			}

			// si le fournisseur est client et valide, verifier qu'il est dans le groupe des fournisseurs externes valides
			// regle desactivee le 07/04/2010 (initialement servait à régler les pbs de PIEWEB (creation auto) mais pose pb ailleurs).
			/*
			 * if (structure.toFournis().isTypeClient() || structure.toFournis().isTypeTiers()) { if
			 * (EOFournis.FOU_VALIDE_OUI.equals(structure.toFournis().fouValide())) { if (!EOStructureForGroupeSpec.isPersonneInGroupe(structure,
			 * EOStructureForGroupeSpec.getGroupeFournisseurValideExterne(structure.editingContext()))) { throw new
			 * NSValidation.ValidationException("Un client valide doit être affecté au groupe des fournisseurs valides externes."); } } }
			 */
		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException(structure.libelleEtId() + " : " + e.getMessage());
		}
	}

	/**
	 * @param afwkPersRecord
	 * @return true si l'objet est considéré comme un fournisseur ( l'objet est de la classe EOStructure et toFournis() != null)
	 */
	@Override
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		boolean res = false;
		if (super.isSpecificite(afwkPersRecord)) {
			if (afwkPersRecord instanceof EOStructure) {
				EOStructure structure = (EOStructure) afwkPersRecord;
				if (structure.toFournis() != null) {
					res = true;
				}
			}
		}
		return res;
	}

	/**
	 * Les structures qui sont dans le groupe des fournisseurs valides externes = clients
	 * 
	 * @param ec
	 */
	public static final EOQualifier QUAL_STRUCTURES_IN_GROUPE_FOURNISSEUR_VALIDE_EXTERNE(EOEditingContext ec) {
		return new EOKeyValueQualifier(EOStructure.TO_REPART_STRUCTURES_KEY + "." + EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual,
		    EOStructureForGroupeSpec.getGroupeForParamKeySilent(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_EXTERNE_KEY));
	}

	public static final EOQualifier QUAL_STRUCTURES_IN_GROUPE_FOURNISSEUR_VALIDE_INTERNE(EOEditingContext ec) {
		return new EOKeyValueQualifier(EOStructure.TO_REPART_STRUCTURES_KEY + "." + EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual,
		    EOStructureForGroupeSpec.getGroupeForParamKeySilent(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_INTERNE_KEY));
	}

	public static final EOQualifier QUAL_STRUCTURES_IN_GROUPE_FOURNISSEUR_VALIDE_MORALE(EOEditingContext ec) {
		return new EOKeyValueQualifier(EOStructure.TO_REPART_STRUCTURES_KEY + "." + EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual,
		    EOStructureForGroupeSpec.getGroupeForParamKeySilent(ec, FwkCktlPersonneParamManager.ANNUAIRE_FOU_VALIDE_MORALE_KEY));
	}

	public static NSArray fetchStructuresForFouCode(EOEditingContext myContext, String fouCode, EOQualifier qualOptionnel) {
		EOQualifier qual = new EOKeyValueQualifier(EOStructure.TO_FOURNISS_KEY + "." + EOFournis.FOU_CODE_KEY, EOQualifier.QualifierOperatorEqual, fouCode);
		if (qualOptionnel != null) {
			qual = new EOAndQualifier(new NSArray(new Object[] { qual, qualOptionnel }));
		}
		EOFetchSpecification spec = new EOFetchSpecification(EOStructure.ENTITY_NAME, qual, new NSArray(new Object[] { EOStructure.SORT_LL_STRUCTURE_ASC }));
		// EOFetchSpecification spec = new EOFetchSpecification(EOFournis.ENTITY_NAME, qual, new NSArray(new Object[] {
		// EOFournis.SORT_FOU_CODE_DESC
		// }));

		spec.setRefreshesRefetchedObjects(true);
		spec.setUsesDistinct(true);
		NSArray res = myContext.objectsWithFetchSpecification(spec);
		return res;

		//
		// if (res.count() > 0) {
		// return EOQualifier.filteredArrayWithQualifier((NSArray) res.valueForKeyPath(EOFournis.TO_STRUCTURE_KEY), qualOptionnel);
		// }
		// return NSArray.EmptyArray;
	}

	/**
	 * 
	 * @param structure structure testée comme fournisseur interne
	 * @return <code> true </code> si la structure est fournisseur interne
	 */
	public static boolean isStructureFournisseurInterne(EOStructure structure) {
		NSArray res = structure.toRepartAssociations(ERXQ.is(EORepartAssociation.TO_ASSOCIATION_KEY + "." + EOAssociation.ASS_CODE_KEY,
		    EOAssociation.ASS_CODE_FINANCIEREPIE));
		return (res != null && res.count() > 0);
	}
}
