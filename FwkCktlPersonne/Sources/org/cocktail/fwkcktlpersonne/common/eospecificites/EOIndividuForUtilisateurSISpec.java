/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Les specificites pour un individu de type Utilisateur du SI.
 * <ul>
 * <li>Règles d'initialisation : {@link EOIndividuForUtilisateurSISpec#onAwakeFromInsertion(AfwkPersRecord)}</li>
 * <li>Règles de validation : {@link EOIndividuForUtilisateurSISpec#onValidateObjectMetier(AfwkPersRecord)}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOIndividuForUtilisateurSISpec implements ISpecificite {

	private static EOIndividuForUtilisateurSISpec sharedInstance = new EOIndividuForUtilisateurSISpec();

	/** Individus qui ont un compte valide */
	public static EOQualifier QUAL_UTILISATEUR_SI_VALIDE = new EOKeyValueQualifier(EOIndividu.TO_COMPTES_KEY + "." + EOCompte.CPT_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EOCompte.CPT_VALIDE_OUI);

	/** Individus ayant un compte d'acces exterieur (VLAN X) */
	public static EOQualifier QUAL_UTILISATEUR_SI_VLAN_X = new EOKeyValueQualifier(EOIndividu.TO_COMPTES_KEY + "." + EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorContains, EOVlans.VLAN_X);

	/** Individus ayant un compte d'acces Administration */
	public static EOQualifier QUAL_UTILISATEUR_SI_VLAN_P = new EOKeyValueQualifier(EOIndividu.TO_COMPTES_KEY + "." + EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorContains, EOVlans.VLAN_P);

	/** Individus ayant un compte d'acces Etudiant */
	public static EOQualifier QUAL_UTILISATEUR_SI_VLAN_E = new EOKeyValueQualifier(EOIndividu.TO_COMPTES_KEY + "." + EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorContains, EOVlans.VLAN_E);

	/** Individus ayant un compte d'acces Recherche */
	public static EOQualifier QUAL_UTILISATEUR_SI_VLAN_R = new EOKeyValueQualifier(EOIndividu.TO_COMPTES_KEY + "." + EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorContains, EOVlans.VLAN_R);

	/** Individus ayant un compte d'acces interne (VLAN P, E ou R) */
	public static EOQualifier QUAL_UTILISATEUR_SI_VLAN_INTERNE = new EOOrQualifier(new NSArray(new Object[] {
			QUAL_UTILISATEUR_SI_VLAN_P, QUAL_UTILISATEUR_SI_VLAN_E, QUAL_UTILISATEUR_SI_VLAN_R
	}));

	public static EOIndividuForUtilisateurSISpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOIndividuForUtilisateurSISpec() {
	}

	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		//((EOIndividu)afwkPersRecord)

	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	/**
	 * Validation pour un individu utilisateur du systeme d'information.
	 * <ul>
	 * <li>L'individu doit posséder un compte.</li>
	 * </ul>
	 * 
	 * @see org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite#onValidateObjectMetier(org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord)
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
//		EOIndividu individu = (EOIndividu) afwkPersRecord;
//
//		if (individu.toComptes().count() == 0) {
//			throw new NSValidation.ValidationException("Un utilisateur du système d'information doit disposer d'un compte de connexion.");
//		}
	}

	/**
	 * @param afwkPersRecord
	 * @return true si l'objet est considéré comme utilisateur du SI ( l'objet est de la classe EOIndividu et a au moins un {@link EOCompte} associé.)
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		if (afwkPersRecord instanceof EOIndividu) {
			EOIndividu individu = (EOIndividu) afwkPersRecord;
			return (individu.toComptes().count() > 0);
		}
		return false;
	}

}
