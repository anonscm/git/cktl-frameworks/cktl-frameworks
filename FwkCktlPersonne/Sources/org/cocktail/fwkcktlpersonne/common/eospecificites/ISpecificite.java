/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/**
 * Interface a respecter pour ajouter des specificites métier à l'objet. Pour chaque objet déclaré comme spécificité, les méthodes seront appelées
 * lors des étapes de la vie de l'objet, que ce soit lors de la création ou lors des étapes de validation. Ceci permet par exemple d'ajouter du code
 * pour définir les règles métier d'un partenaire (qui est une spécificité d'un objet EOStructure). <br/>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public interface ISpecificite {

	/**
	 * @param afwkPersRecord
	 * @return true si l'objet afwkPersRecord peut être considéré comme une spécificité.
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord);

	/**
	 * Appele lors de l'insertion dans l'editing context.<br/>
	 * NB : Cette methode ne peut etre appelee que si l'instance de l'objet a été créée en indiquant la spécificité dans les arguments de la methode (
	 * {@link AfwkPersRecord#createAndInsertInstance(EOEditingContext, String, NSArray)}.
	 * 
	 * @param afwkPersRecord
	 */
	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord);

	/**
	 * Appele par le saveChanges lors de l'insertion d'un objet (par validateForUpdate)
	 * 
	 * @param afwkPersRecord L'objet sur lequel porte la methode.
	 */
	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws NSValidation.ValidationException;

	/**
	 * Appele par le saveChanges lors de la mise a jour d'un objet (par validateForUpdate)
	 * 
	 * @param afwkPersRecord L'objet sur lequel porte la methode.
	 */
	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws NSValidation.ValidationException;

	/**
	 * Appele par le saveChanges lors lors de la suppression d'un objet (par validateForDelete)
	 * 
	 * @param afwkPersRecord L'objet sur lequel porte la methode.
	 */
	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws NSValidation.ValidationException;

	/**
	 * Appele avant l'enregistrement d'un objet (par validateObjectMetier)
	 * 
	 * @param afwkPersRecord L'objet sur lequel porte la methode.
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws NSValidation.ValidationException;

	/**
	 * Appele avant l'enregistrement d'un objet (par ValidateBeforeTransactionSave)
	 * 
	 * @param afwkPersRecord L'objet sur lequel porte la methode.
	 */
	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws NSValidation.ValidationException;

}
