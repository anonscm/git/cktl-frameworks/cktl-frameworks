/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXQ;

/**
 * Les specificites pour un individu de type Etudiant (indQualite=ETUDIANT). Règles
 * <ul>
 * <li>L'Etudiant doit posséder une et une seule adresse typée PARENT</li>
 * <li>L'Etudiant doit posséder une et une seule adresse typée ETUDIANT</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOIndividuForEtudiantSpec implements ISpecificite {

	private static EOIndividuForEtudiantSpec sharedInstance = new EOIndividuForEtudiantSpec();

	public static EOIndividuForEtudiantSpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOIndividuForEtudiantSpec() {
	}

	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		//((EOIndividu)afwkPersRecord)

	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
		if (afwkPersRecord.isObjectInValidationEditingContext()) {
			checkEtudiantHasAdresseEtudiant(afwkPersRecord);
			checkEtudiantHasAdresseParent(afwkPersRecord);
		}
	}

	/**
	 * @return true si l'objet est un etudiant (indQualite='ETUDIANT').
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		boolean isSpecifite = false;
		//FIXME changer le fonctionnement pour individu a un enregistrement lié dans la table Etudiant ?
		if (afwkPersRecord instanceof EOIndividu) {
			final EOIndividu individu = (EOIndividu) afwkPersRecord;
//			isSpecifite = EOIndividu.QUALITE_ETUDIANT.equalsIgnoreCase(individu.indQualite());
			if (EOIndividu.QUALITE_ETUDIANT.equalsIgnoreCase(individu.indQualite())){
				isSpecifite = true;
			}
			if ( individu.indQualite() != null && individu.indQualite().startsWith("Etudiant")){
				isSpecifite = true;
			}
//			isSpecifite = individu.indQualite().startsWith("Etudiant");
		}
		return isSpecifite;
	}

	public void checkEtudiantHasAdresseEtudiant(AfwkPersRecord afwkPersRecord) throws ValidationException {
		EOIndividu individu = (EOIndividu) afwkPersRecord;
		NSArray<EORepartPersonneAdresse> res = individu.toRepartPersonneAdresses(ERXQ.and(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_ETUD, EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE));
		if (res == null || res.count() != 1) {
			throw new NSValidation.ValidationException("Un étudiant doit obligatoirement disposer d'une et d'une seule adresse typée ETUDIANT");
		}
	}

	public void checkEtudiantHasAdresseParent(AfwkPersRecord afwkPersRecord) {
		EOIndividu individu = (EOIndividu) afwkPersRecord;
		NSArray<EORepartPersonneAdresse> res = individu.toRepartPersonneAdresses(ERXQ.and(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_PAR, EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE));
		if (res == null || res.count() != 1) {
			throw new NSValidation.ValidationException("Un étudiant doit obligatoirement disposer d'une et d'une seule adresse typée PARENTS");
		}
	}

}
