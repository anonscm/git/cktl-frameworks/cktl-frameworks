/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Les specificites metiers pour une structure qui peut se connecter au SI.
 * <ul>
 * <li>Règles d'initialisation : {@link EOStructureForUtilisateurSISpec#onAwakeFromInsertion(AfwkPersRecord)}</li>
 * <li>Règles de validation : {@link EOStructureForUtilisateurSISpec#onValidateObjectMetier(AfwkPersRecord)}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOStructureForUtilisateurSISpec implements ISpecificite {

	private static EOStructureForUtilisateurSISpec sharedInstance = new EOStructureForUtilisateurSISpec();

	public static EOStructureForUtilisateurSISpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOStructureForUtilisateurSISpec() {
		super();
	}

	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {

	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {
	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {
	}

	/**
	 * Appelé lors de la validation d'une structure de type utilisateur SI. Controles : <br/>
	 * <ul>
	 * <li>.</li>
	 * </ul>
	 * 
	 * @see org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForPersonneMoraleSpec#onValidateObjectMetier(org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord)
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {

		if (!isSpecificite(afwkPersRecord)) {
			throw new NSValidation.ValidationException("La structure doit avoir au moins un compte valide associé pour être considéré comme utilisateur du système d'information.");
		}

		EOStructure structure = (EOStructure) afwkPersRecord;

	}

	/**
	 * @param afwkPersRecord
	 * @return true si l'objet est considéré comme utilisateur du SI ( l'objet est de la classe EOStructure et a au moins un {@link EOCompte}
	 *         associé.)
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		if (afwkPersRecord instanceof EOStructure) {
			EOStructure structure = (EOStructure) afwkPersRecord;
			return (structure.toComptes(EOCompte.QUAL_CPT_VALIDE_OUI).count() > 0);
		}
		return false;
	}

}
