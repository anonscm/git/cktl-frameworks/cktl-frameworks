/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.eospecificites;

import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Les specificites pour un individu de type Fournisseur.
 * <ul>
 * <li>Règles d'initialisation : {@link EOIndividuForFournisseurSpec#onAwakeFromInsertion(AfwkPersRecord)}</li>
 * <li>Règles de validation : {@link EOIndividuForFournisseurSpec#onValidateObjectMetier(AfwkPersRecord)}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class EOIndividuForFournisseurSpec implements ISpecificite {
	public static final EOQualifier QUAL_INDIVIDUS_IN_GROUPE_TYPE_FOURNISSEUR = new EOKeyValueQualifier(EOIndividu.TO_REPART_STRUCTURES_KEY + "." + EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY,
			EOQualifier.QualifierOperatorEqual,
			EOTypeGroupe.TGRP_CODE_FO);

	private static EOIndividuForFournisseurSpec sharedInstance = new EOIndividuForFournisseurSpec();

	public static EOIndividuForFournisseurSpec sharedInstance() {
		return sharedInstance;
	}

	/**
	 * Utilisez sharedInstance() a la place.
	 */
	protected EOIndividuForFournisseurSpec() {
	}

	public void onAwakeFromInsertion(AfwkPersRecord afwkPersRecord) {
		//((EOIndividu)afwkPersRecord)

	}

	public void onValidateBeforeTransactionSave(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForDelete(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForInsert(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	public void onValidateForUpdate(AfwkPersRecord afwkPersRecord) throws ValidationException {

	}

	/**
	 * Ajoute des validations pour un individu de type fournisseur.
	 * <ul>
	 * <li>L'individu doit être relié à un fournisseur.</li>
	 * <li>L'individu doit faire partie d'un groupe Fournisseur</li>
	 * <li>Il doit y avoir une coherence entre le champ fou_valide et l'affectation de l'individu a un groupe</li>
	 * <li>L'individu doit avoir un repart_personne_adresse de type FACT affecté</li>
	 * <li>Si le parametre ANNUAIRE_FOU_COMPTE_AUTO est à O, et que le fournisseur est valide, l'individu associe doit avoir un compte de connexion
	 * (Ne verifie pas si le compte est valide car le compte peut être desactive pour une raison ou une autre.).</li>
	 * </ul>
	 */
	public void onValidateObjectMetier(AfwkPersRecord afwkPersRecord) throws ValidationException {
		EOIndividu individu = (EOIndividu) afwkPersRecord;

		if (afwkPersRecord.isObjectInValidationEditingContext()) {

			//Verifier coherence fou_valide / groupe
			checkGroupes(individu);

			// verifier adresse FACT saisie
			NSArray res = individu.toRepartPersonneAdresses(new EOAndQualifier(new NSArray(new Object[] {
					EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE, EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_FACT
			})));
			if (res.count() == 0) {
				throw new NSValidation.ValidationException("Une adresse de type facturation est obligatoire pour une personne fournisseur.");
			}

			//verifier que le compte est cree (selon parametre)
			if (EOFournis.FOU_VALIDE_OUI.equals(individu.toFournis().fouValide())) {
				Boolean creerCompteAuto = Boolean.valueOf(EOGrhumParametres.OUI.equals(EOGrhumParametres.parametrePourCle(individu.editingContext(), EOGrhumParametres.PARAM_ANNUAIRE_FOU_COMPTE_AUTO)));
				if (creerCompteAuto.booleanValue()) {
					if (individu.toComptes().count() == 0) {
						throw new NSValidation.ValidationException("Un compte de connexion devrait être affecté a l'individu (car le parametre ANNUAIRE_FOU_COMPTE_AUTO est à O).");
					}
				}
			}
		}

	}

	/**
	 * @param afwkPersRecord
	 * @return true si l'objet est considéré comme un fournisseur ( l'objet est de la classe EOIndividu et toFournis() != null)
	 */
	public boolean isSpecificite(AfwkPersRecord afwkPersRecord) {
		if (afwkPersRecord instanceof EOIndividu) {
			EOIndividu individu = (EOIndividu) afwkPersRecord;
			if (individu.toFournis() != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifie la coherence des groupes affectes a l'individu.
	 * <ul>
	 * <li>L'individu doit être dans un groupe de type fournisseur (repartTypeGroupe FO)</li>
	 * <li>Si le fournisseur est valide, l'individu doit etre dans le groupe des fournisseurs valides</li>
	 * <li>Si le fournisseur est en cours de validation, l'individu doit etre dans le groupe des fournisseurs en cours de creation</li>
	 * <li>Si le fournisseur est annule, l'individu doit etre dans le groupe des fournisseurs archives</li>
	 * <li>Si le fournisseur est valide et est un client, l'individu doit etre dans le groupe des fournisseurs valides externes (07/04/2010 : regle
	 * desactivee (initialement servait à régler les pbs de PIEWEB (creation auto) mais pose pb ailleurs).)</li>
	 * </ul>
	 * 
	 * @param individu
	 */
	public static void checkGroupes(EOIndividu individu) {
		try {
			//l'individu doit etre dans un groupe de fournisseur
			if (!EOStructureForGroupeSpec.isPersonneInGroupeOfType(individu, EOTypeGroupe.TGRP_CODE_FO)) {
				throw new NSValidation.ValidationException("La personne de type fournisseur doit être affectée a un groupe de fournisseur (tgrp_code=FO).");
			}

			if (EOFournis.FOU_VALIDE_NON.equals(individu.toFournis().fouValide())) {
				if (!EOStructureForGroupeSpec.isPersonneInGroupe(individu, EOStructureForGroupeSpec.getGroupeFournisseurEnCours(individu.editingContext()))) {
					throw new NSValidation.ValidationException("Un fournisseur en cours de validation doit être affectée au groupe des fournisseurs en cours de validation.");
				}
			}
			if (EOFournis.FOU_VALIDE_ANNULE.equals(individu.toFournis().fouValide())) {
				if (!EOStructureForGroupeSpec.isPersonneInGroupe(individu, EOStructureForGroupeSpec.getGroupeFournisseurAnnules(individu.editingContext()))) {
					throw new NSValidation.ValidationException("Un fournisseur annulé doit être affectée au groupe des fournisseurs annulés.");
				}
			}

			if (individu.toFournis().isTypeFournisseur() || individu.toFournis().isTypeTiers()) {
				if (EOFournis.FOU_VALIDE_OUI.equals(individu.toFournis().fouValide())) {
					if (!EOStructureForGroupeSpec.isPersonneInGroupe(individu, EOStructureForGroupeSpec.getGroupeFournisseurValidePhysique(individu.editingContext()))) {
						throw new NSValidation.ValidationException("Un fournisseur valide doit être affectée à un groupe de fournisseurs valide.");
					}
				}
			}

			/*
			 * //si le fournisseur est client et valide, verifier qu'il est dans le groupe des fournisseurs externes valides if
			 * (individu.toFournis().isTypeClient() || individu.toFournis().isTypeTiers()) { if
			 * (EOFournis.FOU_VALIDE_OUI.equals(individu.toFournis().fouValide())) { if (!EOStructureForGroupeSpec.isPersonneInGroupe(individu,
			 * EOStructureForGroupeSpec.getGroupeFournisseurValideExterne(individu.editingContext()))) { throw new
			 * NSValidation.ValidationException("Un fournisseur valide doit être affectée à un groupe de fournisseurs valide."); } } }
			 */

		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException(e.getMessage());
		}

	}

}
