/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONatureBonifTerritoire.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EONatureBonifTerritoire extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EONatureBonifTerritoire.class);

	public static final String ENTITY_NAME = "Fwkpers_NatureBonifTerritoire";
	public static final String ENTITY_TABLE_NAME = "GRHUM.NATURE_BONIF_TERRITOIRE";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_FERMETURE = new ERXKey<NSTimestamp>("dateFermeture");
  public static final ERXKey<NSTimestamp> DATE_OUVERTURE = new ERXKey<NSTimestamp>("dateOuverture");
  public static final ERXKey<String> NBOT_CODE = new ERXKey<String>("nbotCode");
  public static final ERXKey<String> NBOT_LIBELLE = new ERXKey<String>("nbotLibelle");
  public static final ERXKey<Integer> NBT_ORDRE = new ERXKey<Integer>("nbtOrdre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTaux> TO_TAUX = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTaux>("toTaux");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "nbotOrdre";

	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String NBOT_CODE_KEY = "nbotCode";
	public static final String NBOT_LIBELLE_KEY = "nbotLibelle";
	public static final String NBT_ORDRE_KEY = "nbtOrdre";

// Attributs non visibles
	public static final String NBOT_ORDRE_KEY = "nbotOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String DATE_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String NBOT_CODE_COLKEY = "NBOT_CODE";
	public static final String NBOT_LIBELLE_COLKEY = "NBOT_LIBELLE";
	public static final String NBT_ORDRE_COLKEY = "NBT_ORDRE";

	public static final String NBOT_ORDRE_COLKEY = "NBOT_ORDRE";


	// Relationships
	public static final String TO_TAUX_KEY = "toTaux";



	// Accessors methods
  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
  }

  public String nbotCode() {
    return (String) storedValueForKey(NBOT_CODE_KEY);
  }

  public void setNbotCode(String value) {
    takeStoredValueForKey(value, NBOT_CODE_KEY);
  }

  public String nbotLibelle() {
    return (String) storedValueForKey(NBOT_LIBELLE_KEY);
  }

  public void setNbotLibelle(String value) {
    takeStoredValueForKey(value, NBOT_LIBELLE_KEY);
  }

  public Integer nbtOrdre() {
    return (Integer) storedValueForKey(NBT_ORDRE_KEY);
  }

  public void setNbtOrdre(Integer value) {
    takeStoredValueForKey(value, NBT_ORDRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTaux toTaux() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTaux)storedValueForKey(TO_TAUX_KEY);
  }

  public void setToTauxRelationship(org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTaux value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTaux oldValue = toTaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TAUX_KEY);
    }
  }
  

/**
 * Créer une instance de EONatureBonifTerritoire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONatureBonifTerritoire createEONatureBonifTerritoire(EOEditingContext editingContext			) {
    EONatureBonifTerritoire eo = (EONatureBonifTerritoire) createAndInsertInstance(editingContext, _EONatureBonifTerritoire.ENTITY_NAME);    
    return eo;
  }

  
	  public EONatureBonifTerritoire localInstanceIn(EOEditingContext editingContext) {
	  		return (EONatureBonifTerritoire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONatureBonifTerritoire creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONatureBonifTerritoire creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EONatureBonifTerritoire object = (EONatureBonifTerritoire)createAndInsertInstance(editingContext, _EONatureBonifTerritoire.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EONatureBonifTerritoire localInstanceIn(EOEditingContext editingContext, EONatureBonifTerritoire eo) {
    EONatureBonifTerritoire localInstance = (eo == null) ? null : (EONatureBonifTerritoire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONatureBonifTerritoire#localInstanceIn a la place.
   */
	public static EONatureBonifTerritoire localInstanceOf(EOEditingContext editingContext, EONatureBonifTerritoire eo) {
		return EONatureBonifTerritoire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EONatureBonifTerritoire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONatureBonifTerritoire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EONatureBonifTerritoire> eoObjects = fetchAll(editingContext, qualifier, null);
	    EONatureBonifTerritoire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONatureBonifTerritoire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONatureBonifTerritoire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONatureBonifTerritoire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EONatureBonifTerritoire> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONatureBonifTerritoire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONatureBonifTerritoire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONatureBonifTerritoire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONatureBonifTerritoire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONatureBonifTerritoire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONatureBonifTerritoire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
