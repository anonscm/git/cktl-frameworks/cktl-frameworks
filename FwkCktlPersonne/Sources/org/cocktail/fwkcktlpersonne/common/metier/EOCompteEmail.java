/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Enumeration;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICompteEmail;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.qualifiers.ERXAndQualifier;

public class EOCompteEmail extends _EOCompteEmail implements ICompteEmail {

	public static final String EOCompteEmailDidInsertNotification = "EOCompteEmailDidInsertNotification";

	public static final EOSortOrdering SORT_PRIORITE = EOSortOrdering.sortOrderingWithKey(CEM_PRIORITE_KEY, EOSortOrdering.CompareAscending);
	
	private static String nomSecondDomain = null;

	public EOCompteEmail() {
		super();
	}

	@Override
	public void didInsert() {
		if (editingContext() != null && editingContext().parentObjectStore() instanceof EOObjectStoreCoordinator) {
			NSNotificationCenter.defaultCenter().postNotification(new NSNotification(EOCompteEmail.EOCompteEmailDidInsertNotification, this));
		}
		super.didInsert();
	}

	/**
	 * @param editingContext
	 * @param ident
	 * @param domain
	 * @return Le premier objet renvoyé pour email et domaine ou null.
	 */
	public static EOCompteEmail fetchByIdentAndDomain(EOEditingContext editingContext, String ident, String domain) {
		EOQualifier qual = new EOAndQualifier(new NSArray(new EOQualifier[] {
				new EOKeyValueQualifier(CEM_EMAIL_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, ident), new EOKeyValueQualifier(CEM_DOMAINE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, domain)
		}));
		return fetchFirstByQualifier(editingContext, qual, null);
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		trimAllString();
		checkEmailValide(getEmailFormatte());
		if (cemAlias() == null) {
			setCemAlias(NON);
		}

		if (MyStringCtrl.isEmpty(cemAlias())) {
			throw new NSValidation.ValidationException("Le champ cemAlias est obligatoire");
		}
		if (MyStringCtrl.isEmpty(cemDomaine())) {
			throw new NSValidation.ValidationException("Le champ cemDomaine est obligatoire");
		}
		if (MyStringCtrl.isEmpty(cemEmail())) {
			throw new NSValidation.ValidationException("Le champ cemEmail est obligatoire");
		}

		// Verifier si le domaine exemple est encore présent dans cemDomaine apres le parsing
		String domaineARemplacer = EOGrhumParametres.parametrePourCle(this.editingContext(),
				EOGrhumParametres.PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER);
		if (!MyStringCtrl.isEmpty(domaineARemplacer) && cemDomaine().contains(domaineARemplacer)) {
			//TODO : éventuellement faire un match plus poussé sur les parties du domaine exemple
			throw new NSValidation.ValidationException("Le domaine '" + domaineARemplacer + "' est un exemple et "
					+ "doit être remplacé par un nom de domaine valide.");
		}

		checkDoublons();

		//		//Verifier les doublons (meme email affecte a un compte valide d'une autre personne)
		checkCompteEmailDoublonAutre(this.editingContext(), this.toCompte().persId(), getEmailFormatte());

		//Verifier qu'un alias n'existe pas deja avec la même partie gauche de l'email (pour emails sur des domaines internes).
		NSArray domainesInternes = EOVlans.getDomainesDistinct(editingContext());
		if (domainesInternes.indexOfObject(this.cemDomaine()) != NSArray.NotFound) {
			//sur grpAlias
			EOStructure struct = EOStructureForGroupeSpec.getGroupeForGrpAlias(editingContext(), cemEmail());
			if (struct != null) {
				throw new NSValidation.ValidationException(
						"La structure " + struct.libelleEtId() + " " +
								"comporte déjà l'alias " + struct.grpAlias());
			}

			// Vérification de l'unicité de l'alias par rapport aux personneAlias
			EOPersonneAlias aliasDouble = EOPersonneAlias.fetchForAlias(editingContext(), cemEmail());
			if (aliasDouble != null)
				throw new NSValidation.ValidationException(
						"L'individu ou structure d'id " + aliasDouble.toPersonne().libelleEtId() +
								" comporte déjà l'alias " + aliasDouble.alias());
			// Vérification de l'unicité de l'alias par rapport aux login
			EOCompte compte = EOCompte.compteForLogin(editingContext(), cemEmail());
			if (compte != null)
				throw new NSValidation.ValidationException("Un compte avec le login " + compte.cptLogin() + " existe déjà dans le système d'information");
		}
		super.validateObjectMetier();
	}

	/**
	 * Verifie si un email est deja affecte a un compte dont le persid est different de celui specifie en parametre.
	 * 
	 * @param ec
	 * @param persId
	 * @param email Adresse email valide.
	 */
	public static void checkCompteEmailDoublonAutre(EOEditingContext ec, Number persId, String email) {
		NSMutableArray quals = new NSMutableArray();
		String ident = email.substring(0, email.indexOf("@"));
		String domaine = email.substring(email.indexOf("@") + 1);

		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompteEmail.TO_COMPTE_KEY + "." + EOCompte.PERS_ID_KEY + "<>%@", new NSArray(new Object[] {
				persId
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompteEmail.TO_COMPTE_KEY + "." + EOCompte.CPT_VALIDE_KEY + "=%@", new NSArray(new Object[] {
				EOCompte.CPT_VALIDE_OUI
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompteEmail.CEM_EMAIL_KEY + "=%@ and " + EOCompteEmail.CEM_DOMAINE_KEY + "=%@", new NSArray(new Object[] {
				ident, domaine
		})));

		NSArray res = EOCompteEmail.fetchAll(ec, new EOAndQualifier(quals), null);
		Enumeration en = res.objectEnumerator();
		while (en.hasMoreElements()) {
			EOCompteEmail obj = (EOCompteEmail) en.nextElement();
			IPersonne pers = PersonneDelegate.fetchPersonneByPersId(ec, obj.toCompte().persId());
			throw new NSValidation.ValidationException("Cet adresse email (" + email + ") est deja affectee a un compte pour " + pers.getNomPrenomAffichage() + " (N° " + pers.getNumero() + ").");
		}
	}

	/**
	 * Vérifier si l'adresse email est deja affectée au compte specifié.
	 */
	public static boolean isEmailAffecte(EOCompte compte, String email) {
		NSMutableArray quals = new NSMutableArray();

		String ident = email.substring(0, email.indexOf("@"));
		String domaine = email.substring(email.indexOf("@") + 1);
		//		
		//		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompteEmail.TO_COMPTE_KEY + "." + EOCompte.PERS_ID_KEY +"<>%@" , new NSArray(new Object[]{persId})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompteEmail.TO_COMPTE_KEY + "=%@", new NSArray(new Object[] {
				compte
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompteEmail.CEM_EMAIL_KEY + "=%@ and " + EOCompteEmail.CEM_DOMAINE_KEY + "=%@", new NSArray(new Object[] {
				ident, domaine
		})));

		NSArray res = compte.toCompteEmails(new EOAndQualifier(quals));
		return (res.count() > 0);
	}

	/**
	 * Vérifier si l'adresse email est deja affectée au compte specifié.
	 */
	public static boolean isCompteEmailAlreadyExistInCompte(EOCompte compte, EOCompteEmail newCompteEmail) {
		String email = newCompteEmail.cemEmail();
		String domaine = newCompteEmail.cemDomaine();
		ERXAndQualifier qual = ERXQ.is(EOCompteEmail.CEM_EMAIL_KEY, email).and(ERXQ.is(EOCompteEmail.CEM_DOMAINE_KEY, domaine));
		//		NSArray res = compte.toCompteEmails(new EOAndQualifier(quals));
		NSArray res = ERXArrayUtilities.arrayMinusObject(compte.toCompteEmails(qual), newCompteEmail);
		return (res.count() > 0);
	}

	/**
	 * Verifie les doublons pour la meme personne.
	 */
	private void checkDoublons() {
		NSArray res = this.toCompte().toCompteEmails(EOQualifier.qualifierWithQualifierFormat(EOCompteEmail.CEM_EMAIL_KEY + "=%@ and " + EOCompteEmail.CEM_DOMAINE_KEY + "=%@", new NSArray(new Object[] {
				cemEmail(), cemDomaine()
		})));
		Enumeration en = res.objectEnumerator();
		while (en.hasMoreElements()) {
			EOCompteEmail obj = (EOCompteEmail) en.nextElement();
			if (!this.editingContext().globalIDForObject(obj).equals(this.editingContext().globalIDForObject(this))) {
				throw new NSValidation.ValidationException("L'adresse email est deja affectée au compte " + this.toCompte().affichage());
			}
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		setDModification(AUtils.now());
		super.validateBeforeTransactionSave();
	}

	/**
	 * @return l'adrese email formattee (sous la forme toto@titi).
	 */
	public String getEmailFormatte() {
		return (cemEmail() == null ? null : cemEmail() + "@" + cemDomaine());
	}

	//	public static EOCompteEmail creerInstance(EOEditingContext ec) {
	//		EOCompteEmail object = (EOCompteEmail) AUtils.instanceForEntity(ec, EOCompteEmail.ENTITY_NAME);
	//		ec.insertObject(object);
	//		object.onCreerInstance();
	//		return object;
	//	}

	/**
	 * Rempli cemEmail et cemDomaine a partir de l'adresse forunie en parametre.
	 * 
	 * @param adresseEmail
	 * @throws Exception
	 */
	public void parseEmailAndDomaine(String adresseEmail) throws Exception {
		if (!MyStringCtrl.isEmpty(adresseEmail)) {
//			MyStringCtrl.replace(adresseEmail, " ", "_");
			adresseEmail.replaceAll(" ", "_");
			checkEmailValide(adresseEmail);
			setCemEmail(adresseEmail.substring(0, adresseEmail.indexOf("@")).toLowerCase());
			setCemDomaine(adresseEmail.substring(adresseEmail.indexOf("@") + 1).toLowerCase());
		}
		else {
			setCemEmail(null);
			setCemDomaine(null);
		}
	}

	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setDCreation(AUtils.now());

	}

	public static void checkEmailValide(String adresseMail) throws NSValidation.ValidationException {
		if (adresseMail != null && !MyStringCtrl.isEmailValid(adresseMail)) {
			throw new NSValidation.ValidationException("L'adresse email specifiee (" + adresseMail + ") n'est pas valide.");
		}
	}

	public void setEmailFormatte(String email) {
		try {
			parseEmailAndDomaine(email);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * @param ec
	 * @param leftEmail
	 * @return Le compteEmail dont la partie gauche de l'email est égale à leftEmail et dont le domaine est un domaine interne (un de ceux définis
	 *         dans VLANS).
	 */
	public static EOCompteEmail fetchCompteEmailInterne(EOEditingContext ec, String leftEmail) {
		String email = leftEmail;
		NSArray res = EOVlans.fetchAll(ec);
		NSArray domainesInternes = EOVlans.getDomainesDistinct(ec);
		NSMutableArray qualsDomaine = new NSMutableArray();
		for (int i = 0; i < domainesInternes.count(); i++) {
			qualsDomaine.addObject(new EOKeyValueQualifier(EOCompteEmail.CEM_DOMAINE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, domainesInternes.objectAtIndex(i)));
		}
		EOQualifier qualDomaine = new EOOrQualifier(qualsDomaine);
		EOCompteEmail compteEmail = EOCompteEmail.fetchFirstByQualifier(ec, new EOAndQualifier(new NSArray(new Object[] {
				new EOKeyValueQualifier(EOCompteEmail.CEM_EMAIL_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, email), qualDomaine
		})));
		return compteEmail;
	}

	//FIXME jouter initialisation email (se baser sur fonctions plsql)<

	/**
	 * Construit une proposition d'adresse email canonique (prenoms.nom) pour la personne (individu) donnée avec comme domaine la valeur du paramètre
	 * : {@link EOGrhumParametres#PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER}. Les prénoms et noms composés sont séparés par des '-' et le prénom du nom
	 * par un '.', les caractères accentués sont remplacés. Exemple pour la personne : Pierre yves Marie cela revéra : 'pierre-yves.marie@domain.fr'.
	 * 
	 * @param personne : la personne pour laquelle il faut construire l'adresse email
	 * @param edc : EOEditingContext pour faire la requete du paramètre
	 * @return l'adresse canonique correspondant au prenom.nom@domain.fr de la personne donnée.
	 */
	public static String computeCanonicalEmailAdresseFromPersonne(IPersonne personne, EOEditingContext edc) {
		return EORepartPersonneAdresse.computeCanonicalEmailAdresseFromPersonne(personne, edc);
	}

	public static String computeCanonicalEmailAdresseAvecDomaineFromPersonne(IPersonne personne, EOEditingContext edc, EOVlans unVlan) {
		return EORepartPersonneAdresse.computeCanonicalEmailAdresseAvecDomaineFromPersonne(personne, edc, unVlan );
	}
	
	public String getAnonymizedEmail() {
		String anonymizedEmail = null;
		if (!MyStringCtrl.isEmpty(cemEmail()) && !MyStringCtrl.isEmpty(cemDomaine())) {
			String emailString = Character.toString(cemEmail().charAt(0));
			char star = "*".charAt(0);
			emailString = ERXStringUtilities.rightPad(emailString, star, cemEmail().length() - 1);
			anonymizedEmail = emailString + "@" + cemDomaine();
		}
		return anonymizedEmail;
	}

	public static String getNomSecondDomain() {
		return nomSecondDomain;
	}

	public static void setNomSecondDomain(String newSecondDomain) {
		nomSecondDomain = newSecondDomain;
	}
	
	
}
