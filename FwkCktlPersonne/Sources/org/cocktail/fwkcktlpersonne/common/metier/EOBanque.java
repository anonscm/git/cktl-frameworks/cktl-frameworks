/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOBanque.java
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Date;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.controles.ControleBIC;
import org.cocktail.fwkcktlpersonne.common.controles.ControleIban;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXStringUtilities;

public class EOBanque extends _EOBanque {
	private static final String TEM_LOCAL_OUI = "O";
	private static final String TEM_LOCAL_NON = "N";
	public static final String EXCEPTION_BANQUE = "Le code etablissement est obligatoire et doit comporter 5 caractères pour une banque de la zone FR";
	public static final String EXCEPTION_GUICHET = "Le code guichet est obligatoire et doit comporter 5 caractères  pour une banque de la zone FR";
	public static final String EXCEPTION_DOMICILIATION = "La domiciliation est obligatoire.";
	public static final String EXCEPTION_BIC = "Le BIC est obligatoire.";

	public static final String CBANQUE_ETRANGER = "99999";
	public static final String CGUICHET_ETRANGER = "99999";
	public static final String DOMICILIATION_ETRANGER = "ETRANGER";
	public static final String CODE_KEY = "code";
	public static final String LIBELLE_KEY = "libelle";
	public static final EOSortOrdering SORT_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(EOBanque.LIBELLE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_CODE_ASC = EOSortOrdering.sortOrderingWithKey(EOBanque.CODE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_BIC_ASC = EOSortOrdering.sortOrderingWithKey(EOBanque.BIC_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_CBANQUE_ASC = EOSortOrdering.sortOrderingWithKey(EOBanque.C_BANQUE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_CGUICHET_ASC = EOSortOrdering.sortOrderingWithKey(EOBanque.C_GUICHET_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_DOMICILIATION = EOSortOrdering.sortOrderingWithKey(EOBanque.DOMICILIATION_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_D_FERMETURE_DESC = EOSortOrdering.sortOrderingWithKey(EOBanque.D_FERMETURE_KEY, EOSortOrdering.CompareDescending);
	public static final EOQualifier QUAL_TEM_LOCAL_O = new EOKeyValueQualifier(EOBanque.TEM_LOCAL_KEY, EOQualifier.QualifierOperatorEqual, TEM_LOCAL_OUI);

	public EOBanque() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		try {

			//FIXME interdire la création de banques avec un BIC FR ? pour l'instant non (30/09/2011)

			trimAllString();
			if (bic() == null || bic().length() == 0) {
				throw new NSValidation.ValidationException(EXCEPTION_BIC);
			}
			checkBICValide();
			if (ControleBIC.isInZoneFR(bic(), editingContext())) {
				if (cBanque() == null || cBanque().length() == 0) {
					throw new NSValidation.ValidationException(EXCEPTION_BANQUE);
				}
				if (cBanque().length() > 5 || cBanque().length() < 5) {
					throw new NSValidation.ValidationException(EXCEPTION_BANQUE);
				}
				if (cGuichet() == null || cGuichet().length() == 0) {
					throw new NSValidation.ValidationException(EXCEPTION_GUICHET);
				}
				if (cGuichet().length() > 5 || cGuichet().length() < 5) {
					throw new NSValidation.ValidationException(EXCEPTION_GUICHET);
				}
				EOBanque doublon = findDoublonCodeBanqueEtGuichet(editingContext(), this);
				if (doublon != null) {
					throw new NSValidation.ValidationException("La banque " + doublon.toPresentableString() + " existe déjà avec ces même codes banques et guichet");
				}
			}

			if (domiciliation() == null || domiciliation().length() == 0) {
				throw new NSValidation.ValidationException(EXCEPTION_DOMICILIATION);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException("Banque " + toPresentableString() + " : " + e.getLocalizedMessage());
		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		setDModification(AUtils.now());
	}

	/**
	 * Cherche et renvoie un objet EOBanque qui correspond aux parametres.
	 * 
	 * @param ed
	 * @param ribEtab
	 * @param ribGuich
	 * @return L'objet banque correspondant
	 * @throws NSValidation.ValidationException
	 */
	public static EOBanque findBanque(EOEditingContext ed, String ribEtab, String ribGuich) throws NSValidation.ValidationException {
		EOQualifier qual = qualifierForCodeBanqueEtCodeGuichet(ribEtab, ribGuich);
		return EOBanque.fetchFirstByQualifier(ed, qual);
	}

	public static EOQualifier qualifierForCodeBanqueEtCodeGuichet(String cBanque, String cGuichet) {
		if (cBanque == null || cBanque.trim().length() == 0) {
			throw new NSValidation.ValidationException(EOBanque.EXCEPTION_BANQUE);
		}
		if (cGuichet == null || cGuichet.trim().length() == 0) {
			throw new NSValidation.ValidationException(EOBanque.EXCEPTION_GUICHET);
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOBanque.C_BANQUE_KEY + "=%@ and " + EOBanque.C_GUICHET_KEY + "=%@", new NSArray<String>(new String[] {
				cBanque, cGuichet
		}));
		return qual;
	}

	/**
	 * @param ed
	 * @param bic
	 * @return Toutes les banques dont le bic est celui passé en parametre.
	 * @throws NSValidation.ValidationException
	 */
	public static NSArray<EOBanque> findBanquesWithBic(EOEditingContext ed, String bic) throws NSValidation.ValidationException {
		if (bic == null || bic.trim().length() == 0) {
			return NSArray.emptyArray();
		}
		EOQualifier qual = new EOKeyValueQualifier(EOBanque.BIC_KEY, EOQualifier.QualifierOperatorEqual, bic);
		NSArray<EOBanque> res = EOBanque.fetchAll(ed, qual, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				EOBanque.SORT_D_FERMETURE_DESC, EOBanque.SORT_DOMICILIATION
		}));
		return res;
	}

	/**
	 * Recherche un doublon de banque basé sur code banque et code guichet, uniquement dans les banques "locales" (créées dans l'établissement).
	 * 
	 * @param ed
	 * @param banque
	 * @return
	 * @throws Exception
	 */
	public static EOBanque findDoublonCodeBanqueEtGuichet(EOEditingContext ed, EOBanque banque) throws Exception {
		if (banque == null) {
			throw new NSValidation.ValidationException("La banque est obligatoire pour chercher des doublons.");
		}
		if (MyStringCtrl.isEmpty(banque.cBanque()) || MyStringCtrl.isEmpty(banque.cGuichet())) {
			return null;
		}
		EOQualifier qual = qualifierForCodeBanqueEtCodeGuichet(banque.cBanque(), banque.cGuichet());
		qual = ERXQ.and(qual, QUAL_TEM_LOCAL_O);
		EOBanque res = fetchFirstByQualifier(ed, qual);
		if (res != null && !res.globalID().equals(banque.globalID())) {
			return res;
		}
		return null;

	}

	public static EOBanque creerInstance(EOEditingContext ed) {
		EOBanque object = (EOBanque) AUtils.instanceForEntity(ed, EOBanque.ENTITY_NAME);
		ed.insertObject(object);
		return object;
	}

	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setDCreation(AUtils.now());
		setTemLocal(TEM_LOCAL_OUI);
	}

	public void setDomiciliation(String value) {
		super.setDomiciliation((value == null ? value : MyStringCtrl.chaineSansAccents(value.trim()).toUpperCase()));
	}

	/**
	 * Cheche l'enregistrement generique de BANQUE qui represente une banque etrangere. Elle est cree si elle n'est pas trouvee.
	 * 
	 * @param editingContext
	 * @return La banque correspondant a une banque etrangere.
	 */
	public static EOBanque getBanqueEtranger(EOEditingContext editingContext) {
		EOQualifier qual1 = new EOKeyValueQualifier(EOBanque.C_BANQUE_KEY, EOQualifier.QualifierOperatorEqual, EOBanque.CBANQUE_ETRANGER);
		EOQualifier qual2 = new EOKeyValueQualifier(EOBanque.C_GUICHET_KEY, EOQualifier.QualifierOperatorEqual, EOBanque.CGUICHET_ETRANGER);
		EOBanque res = fetchByQualifier(editingContext, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				qual1, qual2
		})));
		if (res == null) {
			res = EOBanque.creerInstance(editingContext);
			res.setCBanque(EOBanque.CBANQUE_ETRANGER);
			res.setCGuichet(EOBanque.CGUICHET_ETRANGER);
			res.setDomiciliation(DOMICILIATION_ETRANGER);
		}
		return res;
	}

	public String toPresentableString() {
		return cBanque() + " " + cGuichet() + " " + domiciliation() + " " + bic();
	}

	/**
	 * Vérifie la validité du code BIC.
	 */
	private void checkBICValide() throws NSValidation.ValidationException {
		if (bic() != null) {
			ControleBIC cb = new ControleBIC(bic());
			cb.checkValide(editingContext());
		}
	}

	/**
	 * @param edc
	 * @param iban
	 * @return Cherche une banque à partir du code banque FR / code guichet FR déduit à partir de l'IBAN.
	 */
	public static EOBanque findBanqueWithIban(EOEditingContext edc, String iban) {
		if (ERXStringUtilities.stringIsNullOrEmpty(iban)) {
			return null;
		}
		EOBanque banque = null;
		if (iban.startsWith("FR")) {
			String cbanque = ControleIban.getCodeBanqueFR(iban);
			String cguichet = ControleIban.getCodeGuichetFR(iban);
			if (cbanque != null && cguichet != null) {
				banque = findBanque(edc, cbanque, cguichet);
			}
		}
		return banque;
	}

	//	public static EOBanque findBanqueWithBic(EOEditingContext edc, String bic) {
	//		if (ERXStringUtilities.stringIsNullOrEmpty(bic)) {
	//			return null;
	//		}
	//		NSArray<EOBanque> res = findBanquesWithBic(edc, bic);
	//		if (res == null || res.count() != 1) {
	//			return null;
	//		}
	//		//EOBanque banque = EOBanque.fetchByKeyValue(edc, EOBanque.BIC_KEY, bic);
	//		return res.objectAtIndex(0);
	//	}

	/**
	 * @param edc
	 * @param filtBanque
	 * @return Les banques cherchées à partir du code banque, de la domiciliation ou du code BIC.
	 */
	public static NSArray<EOBanque> getFilteredBanques(EOEditingContext edc, String filtBanque) {
		NSMutableArray<EOBanque> resFinal = new NSMutableArray<EOBanque>();
		if (filtBanque != null) {
			filtBanque = filtBanque.trim();
		}

		if (filtBanque != null && filtBanque.length() > 0) {
			NSArray<EOBanque> res1 = EOBanque.fetchAll(edc, getQualifierForFiltre(filtBanque), new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOSortOrdering.sortOrderingWithKey(EOBanque.C_BANQUE_KEY, EOSortOrdering.CompareAscending)
			}));
			resFinal.addObjectsFromArray(res1);
		}
		return resFinal;
	}

	/**
	 * @return un qualifier portant sur la date de fermeture pour recuperer les banques valides à ce jour.
	 */
	public static EOQualifier getQualifierForBanquesValides() {
		return ERXQ.or(ERXQ.isNull(EOBanque.D_FERMETURE_KEY), ERXQ.greaterThan(EOBanque.D_FERMETURE_KEY, new NSTimestamp()));
	}

	/**
	 * @return un qualifier portant sur la date de fermeture pour recuperer les banques fermées à ce jour.
	 */
	public static EOQualifier getQualifierForBanquesNonValides() {
		return ERXQ.not(getQualifierForBanquesValides());
	}

	/**
	 * @param filtBanque
	 * @return Un qualifier construit à partir du filtre. Recherche à partir du code banque, de la domiciliation ou du code BIC. Seules les banque
	 *         sans date de fin ou avec une date de fin >date du jour sont recuperees.
	 */
	public static EOQualifier getQualifierForFiltre(String filtBanque) {
		NSMutableArray<EOQualifier> quals1 = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> quals2 = new NSMutableArray<EOQualifier>();
		if (filtBanque != null && filtBanque.trim().length() > 0) {
			filtBanque = filtBanque.trim();
			filtBanque = MyStringCtrl.chaineSansAccents(filtBanque, "?");
			NSArray<String> lesMots = NSArray.componentsSeparatedByString(filtBanque, " ");
			NSMutableArray<String> motsBanque = new NSMutableArray<String>();
			NSMutableArray<String> motsGuichet = new NSMutableArray<String>();
			NSMutableArray<String> motsDomiciliation = new NSMutableArray<String>();
			for (int i = 0; i < lesMots.count(); i++) {
				String mot = (String) lesMots.objectAtIndex(i);
				if (MyStringCtrl.containsDigits(mot)) {
					motsBanque.addObject(mot);
					motsGuichet.addObject(mot);
					quals1.addObject(EOQualifier.qualifierWithQualifierFormat(EOBanque.C_BANQUE_KEY + " like '" + mot + "*' or " + EOBanque.C_GUICHET_KEY + " like '" + mot + "*'   ", null));
				}
				else {
					motsDomiciliation.addObject(mot);
					NSMutableArray<EOQualifier> quals3 = new NSMutableArray<EOQualifier>();
					quals3.addObject(new EOKeyValueQualifier(EOBanque.DOMICILIATION_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + mot + "*"));
					quals3.addObject(new EOKeyValueQualifier(EOBanque.VILLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, mot));
					quals3.addObject(new EOKeyValueQualifier(EOBanque.ADRESSE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + mot + "*"));
					quals1.addObject(new EOOrQualifier(quals3));
				}
				quals2.addObject(ERXQ.likeInsensitive(EOBanque.BIC_KEY, mot + "*"));
			}
			return ERXQ.or(ERXQ.or(quals2), ERXQ.and(quals1));
		}
		return null;
	}

	/**
	 * @param filtBanque
	 * @return Un qualifier construit à partir du filtre. Recherche à partir du code banque, de la domiciliation ou du code BIC. Seules les banque
	 *         sans date de fin ou avec une date de fin >date du jour sont recuperees.
	 */
	public static EOQualifier getQualifierForFiltre(String filtBanque, boolean fetchValide) {

		EOQualifier res;
		if (fetchValide) {
			res = getQualifierForBanquesValides();
		}
		else {
			res = getQualifierForBanquesNonValides();
		}
		NSMutableArray<EOQualifier> quals1 = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> quals2 = new NSMutableArray<EOQualifier>();
		if (filtBanque != null && filtBanque.trim().length() > 0) {
			filtBanque = filtBanque.trim();
			filtBanque = MyStringCtrl.chaineSansAccents(filtBanque, "?");
			NSArray<String> lesMots = NSArray.componentsSeparatedByString(filtBanque, " ");
			NSMutableArray<String> motsBanque = new NSMutableArray<String>();
			NSMutableArray<String> motsGuichet = new NSMutableArray<String>();
			NSMutableArray<String> motsDomiciliation = new NSMutableArray<String>();
			for (int i = 0; i < lesMots.count(); i++) {
				String mot = (String) lesMots.objectAtIndex(i);
				if (MyStringCtrl.containsDigits(mot)) {
					motsBanque.addObject(mot);
					motsGuichet.addObject(mot);
					quals1.addObject(EOQualifier.qualifierWithQualifierFormat(EOBanque.C_BANQUE_KEY + " like '" + mot + "*' or " + EOBanque.C_GUICHET_KEY + " like '" + mot + "*'   ", null));
				}
				else {
					motsDomiciliation.addObject(mot);
					NSMutableArray<EOQualifier> quals3 = new NSMutableArray<EOQualifier>();
					quals3.addObject(new EOKeyValueQualifier(EOBanque.DOMICILIATION_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + mot + "*"));
					quals3.addObject(new EOKeyValueQualifier(EOBanque.VILLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, mot));
					quals3.addObject(new EOKeyValueQualifier(EOBanque.ADRESSE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + mot + "*"));
					quals1.addObject(new EOOrQualifier(quals3));
				}
				quals2.addObject(ERXQ.likeInsensitive(EOBanque.BIC_KEY, mot + "*"));
			}
			res = ERXQ.and(res, ERXQ.or(ERXQ.or(quals2), ERXQ.and(quals1)));
		}
		return res;
	}

	/**
	 * @return Le BIC s'il existe, sinon le code banque + code guichet
	 */
	public String getCode() {
		return (!ERXStringUtilities.stringIsNullOrEmpty(bic()) ? bic() : this.cBanque() + " " + this.cGuichet());
	}

	public String libelle() {
		return this.domiciliation().concat(cBanque() != null ? " (" + cBanque() + "-" + cGuichet() + ")" : "").concat(adresse() != null ? " - " + adresse() : "")
				.concat(codePostal() != null ? " - " + codePostal() : "")
				.concat(ville() != null ? " - " + ville() : "");
	}

	/**
	 * @return Le libellé de l'agence avec mention local et fermeture.
	 */

	public String libelleEtendu() {
		return (isLocal().booleanValue() ? "(Local) " : "").concat(isClosed().booleanValue() ? "(Fermée) " : "").concat(libelle());
	}

	@Override
	public void setBic(String value) {
		if (value != null) {
			value = value.toUpperCase();
		}
		super.setBic(value);
	}

	public Boolean isClosed() {
		return Boolean.valueOf((dFermeture() != null && dFermeture().before(new Date())));
	}

	public Boolean isLocal() {
		return TEM_LOCAL_OUI.equals(temLocal());
	}

}
