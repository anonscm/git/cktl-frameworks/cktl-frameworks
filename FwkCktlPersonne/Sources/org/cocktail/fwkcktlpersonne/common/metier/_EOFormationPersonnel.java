/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFormationPersonnel.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOFormationPersonnel extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOFormationPersonnel.class);

	public static final String ENTITY_NAME = "Fwkpers_FormationPersonnel";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FORMATION_PERSONNEL";


// Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> FOR_COMMENTAIRE = new ERXKey<String>("forCommentaire");
  public static final ERXKey<NSTimestamp> FOR_D_FERMETURE = new ERXKey<NSTimestamp>("forDFermeture");
  public static final ERXKey<NSTimestamp> FOR_D_OUVERTURE = new ERXKey<NSTimestamp>("forDOuverture");
  public static final ERXKey<Integer> FOR_ID = new ERXKey<Integer>("forId");
  public static final ERXKey<Integer> FOR_ID_PERE = new ERXKey<Integer>("forIdPere");
  public static final ERXKey<String> FOR_LIBELLE = new ERXKey<String>("forLibelle");
  public static final ERXKey<String> FOR_LIBELLE_DESCENDANCE = new ERXKey<String>("forLibelleDescendance");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> TO_FORMATION_PERE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>("toFormationPere");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> TOS_FORMATION_FILLE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>("tosFormationFille");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "forId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FOR_COMMENTAIRE_KEY = "forCommentaire";
	public static final String FOR_D_FERMETURE_KEY = "forDFermeture";
	public static final String FOR_D_OUVERTURE_KEY = "forDOuverture";
	public static final String FOR_ID_KEY = "forId";
	public static final String FOR_ID_PERE_KEY = "forIdPere";
	public static final String FOR_LIBELLE_KEY = "forLibelle";
	public static final String FOR_LIBELLE_DESCENDANCE_KEY = "forLibelleDescendance";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FOR_COMMENTAIRE_COLKEY = "FOR_COMMENTAIRE";
	public static final String FOR_D_FERMETURE_COLKEY = "FOR_D_FERMETURE";
	public static final String FOR_D_OUVERTURE_COLKEY = "FOR_D_OUVERTURE";
	public static final String FOR_ID_COLKEY = "FOR_ID";
	public static final String FOR_ID_PERE_COLKEY = "FOR_ID_PERE";
	public static final String FOR_LIBELLE_COLKEY = "FOR_LIBELLE";
	public static final String FOR_LIBELLE_DESCENDANCE_COLKEY = "newAttribute1";



	// Relationships
	public static final String TO_FORMATION_PERE_KEY = "toFormationPere";
	public static final String TOS_FORMATION_FILLE_KEY = "tosFormationFille";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String forCommentaire() {
    return (String) storedValueForKey(FOR_COMMENTAIRE_KEY);
  }

  public void setForCommentaire(String value) {
    takeStoredValueForKey(value, FOR_COMMENTAIRE_KEY);
  }

  public NSTimestamp forDFermeture() {
    return (NSTimestamp) storedValueForKey(FOR_D_FERMETURE_KEY);
  }

  public void setForDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, FOR_D_FERMETURE_KEY);
  }

  public NSTimestamp forDOuverture() {
    return (NSTimestamp) storedValueForKey(FOR_D_OUVERTURE_KEY);
  }

  public void setForDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, FOR_D_OUVERTURE_KEY);
  }

  public Integer forId() {
    return (Integer) storedValueForKey(FOR_ID_KEY);
  }

  public void setForId(Integer value) {
    takeStoredValueForKey(value, FOR_ID_KEY);
  }

  public Integer forIdPere() {
    return (Integer) storedValueForKey(FOR_ID_PERE_KEY);
  }

  public void setForIdPere(Integer value) {
    takeStoredValueForKey(value, FOR_ID_PERE_KEY);
  }

  public String forLibelle() {
    return (String) storedValueForKey(FOR_LIBELLE_KEY);
  }

  public void setForLibelle(String value) {
    takeStoredValueForKey(value, FOR_LIBELLE_KEY);
  }

  public String forLibelleDescendance() {
    return (String) storedValueForKey(FOR_LIBELLE_DESCENDANCE_KEY);
  }

  public void setForLibelleDescendance(String value) {
    takeStoredValueForKey(value, FOR_LIBELLE_DESCENDANCE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel toFormationPere() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel)storedValueForKey(TO_FORMATION_PERE_KEY);
  }

  public void setToFormationPereRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel oldValue = toFormationPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FORMATION_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FORMATION_PERE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> tosFormationFille() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>)storedValueForKey(TOS_FORMATION_FILLE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> tosFormationFille(EOQualifier qualifier) {
    return tosFormationFille(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> tosFormationFille(EOQualifier qualifier, boolean fetch) {
    return tosFormationFille(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> tosFormationFille(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel.TO_FORMATION_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosFormationFille();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosFormationFilleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_FORMATION_FILLE_KEY);
  }

  public void removeFromTosFormationFilleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_FORMATION_FILLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel createTosFormationFilleRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_FormationPersonnel");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_FORMATION_FILLE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel) eo;
  }

  public void deleteTosFormationFilleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_FORMATION_FILLE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosFormationFilleRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> objects = tosFormationFille().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosFormationFilleRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOFormationPersonnel avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFormationPersonnel createEOFormationPersonnel(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer forId
, Integer forIdPere
, String forLibelle
, String forLibelleDescendance
, org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel toFormationPere			) {
    EOFormationPersonnel eo = (EOFormationPersonnel) createAndInsertInstance(editingContext, _EOFormationPersonnel.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setForId(forId);
		eo.setForIdPere(forIdPere);
		eo.setForLibelle(forLibelle);
		eo.setForLibelleDescendance(forLibelleDescendance);
    eo.setToFormationPereRelationship(toFormationPere);
    return eo;
  }

  
	  public EOFormationPersonnel localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFormationPersonnel)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFormationPersonnel creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFormationPersonnel creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOFormationPersonnel object = (EOFormationPersonnel)createAndInsertInstance(editingContext, _EOFormationPersonnel.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFormationPersonnel localInstanceIn(EOEditingContext editingContext, EOFormationPersonnel eo) {
    EOFormationPersonnel localInstance = (eo == null) ? null : (EOFormationPersonnel)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFormationPersonnel#localInstanceIn a la place.
   */
	public static EOFormationPersonnel localInstanceOf(EOEditingContext editingContext, EOFormationPersonnel eo) {
		return EOFormationPersonnel.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFormationPersonnel fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFormationPersonnel fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFormationPersonnel> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFormationPersonnel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFormationPersonnel)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFormationPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFormationPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFormationPersonnel> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFormationPersonnel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFormationPersonnel)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFormationPersonnel fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFormationPersonnel eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFormationPersonnel ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFormationPersonnel fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
