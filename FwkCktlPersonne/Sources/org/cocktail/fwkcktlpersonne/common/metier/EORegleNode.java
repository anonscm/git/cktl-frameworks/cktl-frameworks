/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EORegleNode extends _EORegleNode {

    public static final String STR_ID_SIMPLE = "SIMPLE";
    public static final String STR_ID_OR = "OR";
    public static final String STR_ID_AND = "AND";
    public static final String STR_ID_NOT = "NOT";
    private static EORegleNode NodeSimple;
    private static EORegleNode NodeAnd;
    private static EORegleNode NodeOr;
    private static EORegleNode NodeNot;
    private static NSArray<EORegleNode> RegleNodes;

    public EORegleNode() {
        super();
    }

    public boolean isNodeSimple() {
        return STR_ID_SIMPLE.equals(rnNode());
    }
    
    public boolean isNodeAnd() {
        return STR_ID_AND.equals(rnNode());
    }
    
    public boolean isNodeOr() {
        return STR_ID_OR.equals(rnNode());
    }
    
    public boolean isNodeNot() {
        return STR_ID_NOT.equals(rnNode());
    }
    
    /**
     * @return
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EORegleNode> getRegleNodes() {
        if (RegleNodes == null)
            RegleNodes = EORegleNode.fetchAll(EOSharedEditingContext.defaultSharedEditingContext(), ERXS.ascs(RN_NODE_KEY));
        return RegleNodes;
    }
    
    /**
     * @return
     */
    public static EORegleNode getRegleNodeSimple() {
        if (NodeSimple == null)
            NodeSimple = ERXQ.first(getRegleNodes(), ERXQ.equals(RN_NODE_KEY, STR_ID_SIMPLE));
        return NodeSimple;
    }
    
    /**
     * @return
     */
    public static EORegleNode getRegleNodeAnd() {
        if (NodeAnd == null)
            NodeAnd = ERXQ.first(getRegleNodes(), ERXQ.equals(RN_NODE_KEY, STR_ID_AND));
        return NodeAnd;
    }
    
    /**
     * @return
     */
    public static EORegleNode getRegleNodeOr() {
        if (NodeOr == null)
            NodeOr = ERXQ.first(getRegleNodes(), ERXQ.equals(RN_NODE_KEY, STR_ID_OR));
        return NodeOr;
    }
    
    /**
     * @return
     */
    public static EORegleNode getRegleNodeNot() {
        if (NodeNot == null)
            NodeNot = ERXQ.first(getRegleNodes(), ERXQ.equals(RN_NODE_KEY, STR_ID_NOT));
        return NodeNot;
    }

}
