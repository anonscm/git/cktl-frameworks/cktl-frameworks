/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartEcoleDoctorale.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EORepartEcoleDoctorale extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EORepartEcoleDoctorale.class);

	public static final String ENTITY_NAME = "Fwkpers_RepartEcoleDoctorale";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REPART_ECOLE_DOCTORALE";


// Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> TO_ECOLE_DOCTORALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale>("toEcoleDoctorale");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORoleEtabEcoleDoctorale> TO_ROLE_ETAB_ECOLE_DOCTORALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORoleEtabEcoleDoctorale>("toRoleEtabEcoleDoctorale");

	// Attributes



// Attributs non visibles
	public static final String C_RNE_KEY = "cRne";
	public static final String ID_ECOLE_DOCTORALE_KEY = "idEcoleDoctorale";
	public static final String ID_ROLE_ETAB_ECOLE_DOCTORALE_KEY = "idRoleEtabEcoleDoctorale";

//Colonnes dans la base de donnees

	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String ID_ECOLE_DOCTORALE_COLKEY = "ID_ECOLE_DOCTORALE";
	public static final String ID_ROLE_ETAB_ECOLE_DOCTORALE_COLKEY = "ID_ROLE_ETAB_ECOLE_DOCTORALE";


	// Relationships
	public static final String TO_ECOLE_DOCTORALE_KEY = "toEcoleDoctorale";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_ROLE_ETAB_ECOLE_DOCTORALE_KEY = "toRoleEtabEcoleDoctorale";



	// Accessors methods
  public org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale toEcoleDoctorale() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale)storedValueForKey(TO_ECOLE_DOCTORALE_KEY);
  }

  public void setToEcoleDoctoraleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale oldValue = toEcoleDoctorale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECOLE_DOCTORALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECOLE_DOCTORALE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORoleEtabEcoleDoctorale toRoleEtabEcoleDoctorale() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORoleEtabEcoleDoctorale)storedValueForKey(TO_ROLE_ETAB_ECOLE_DOCTORALE_KEY);
  }

  public void setToRoleEtabEcoleDoctoraleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORoleEtabEcoleDoctorale value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORoleEtabEcoleDoctorale oldValue = toRoleEtabEcoleDoctorale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ROLE_ETAB_ECOLE_DOCTORALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ROLE_ETAB_ECOLE_DOCTORALE_KEY);
    }
  }
  

/**
 * Créer une instance de EORepartEcoleDoctorale avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORepartEcoleDoctorale createEORepartEcoleDoctorale(EOEditingContext editingContext, org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale toEcoleDoctorale, org.cocktail.fwkcktlpersonne.common.metier.EORne toRne, org.cocktail.fwkcktlpersonne.common.metier.EORoleEtabEcoleDoctorale toRoleEtabEcoleDoctorale			) {
    EORepartEcoleDoctorale eo = (EORepartEcoleDoctorale) createAndInsertInstance(editingContext, _EORepartEcoleDoctorale.ENTITY_NAME);    
    eo.setToEcoleDoctoraleRelationship(toEcoleDoctorale);
    eo.setToRneRelationship(toRne);
    eo.setToRoleEtabEcoleDoctoraleRelationship(toRoleEtabEcoleDoctorale);
    return eo;
  }

  
	  public EORepartEcoleDoctorale localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartEcoleDoctorale)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartEcoleDoctorale creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartEcoleDoctorale creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORepartEcoleDoctorale object = (EORepartEcoleDoctorale)createAndInsertInstance(editingContext, _EORepartEcoleDoctorale.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartEcoleDoctorale localInstanceIn(EOEditingContext editingContext, EORepartEcoleDoctorale eo) {
    EORepartEcoleDoctorale localInstance = (eo == null) ? null : (EORepartEcoleDoctorale)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORepartEcoleDoctorale#localInstanceIn a la place.
   */
	public static EORepartEcoleDoctorale localInstanceOf(EOEditingContext editingContext, EORepartEcoleDoctorale eo) {
		return EORepartEcoleDoctorale.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORepartEcoleDoctorale fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartEcoleDoctorale fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORepartEcoleDoctorale> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartEcoleDoctorale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartEcoleDoctorale)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartEcoleDoctorale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartEcoleDoctorale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORepartEcoleDoctorale> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartEcoleDoctorale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartEcoleDoctorale)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORepartEcoleDoctorale fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartEcoleDoctorale eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartEcoleDoctorale ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartEcoleDoctorale fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
