/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOTypeAdresse.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOTypeAdresse extends _EOTypeAdresse implements ITypeAdresse {
	public static final String TADR_CODE_FACT = "FACT";
	public static final String TADR_CODE_PERSO = "PERSO";
	public static final String TADR_CODE_PRO = "PRO";
	public static final String TADR_CODE_PAR = "PAR";
	public static final String TADR_CODE_ETUD = "ETUD";
	public static final String TADR_CODE_INVAL = "INVAL";
	public static final String TADR_CODE_STAGE = "STAGE";
	public static final String TADR_CODE_LIVR = "LIVR";

	public static final EOQualifier QUAL_TADR_CODE_FACT = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_FACT);
	public static final EOQualifier QUAL_TADR_CODE_PERSO = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_PERSO);
	public static final EOQualifier QUAL_TADR_CODE_PRO = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_PRO);
	public static final EOQualifier QUAL_TADR_CODE_PAR = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_PAR);
	public static final EOQualifier QUAL_TADR_CODE_ETUD = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_ETUD);
	public static final EOQualifier QUAL_TADR_CODE_STAGE = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_STAGE);
	public static final EOQualifier QUAL_TADR_CODE_LIVR = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_LIVR);
	public static final EOQualifier QUAL_TADR_CODE_INVAL = new EOKeyValueQualifier(EOTypeAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, TADR_CODE_INVAL);

	/** ORQualifier sur les adresses de type professionnel */
	public static final EOQualifier QUAL_TADR_PROS = new EOOrQualifier(new NSArray(new Object[] {
			QUAL_TADR_CODE_FACT, QUAL_TADR_CODE_PRO, QUAL_TADR_CODE_LIVR
	}));

	public EOTypeAdresse() {
		super();
	}
	
	public static NSArray fetchTypeadressesNonPerso(EOEditingContext edc) {
		EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
				new EONotQualifier(QUAL_TADR_CODE_PERSO), new EONotQualifier(QUAL_TADR_CODE_INVAL)
		}));
		return fetchAll(edc, qual);
	}

	public static EOQualifier qualifierVisualisablesForApplicationUser(PersonneApplicationUser appUser, IPersonne personne) {
		NSMutableArray notAllowed = new NSMutableArray();
		notAllowed.addObject(new EONotQualifier(QUAL_TADR_CODE_INVAL));

		if (!appUser.hasDroitTypesAdresseEtudiantVisualisation(personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_ETUD));
		}
		if (!appUser.hasDroitTypesAdresseParentVisualisation(personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PAR));
		}
		if (!appUser.hasDroitTypesAdressePersoVisualisation(personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PERSO));
		} 
		// PYM : Ajouter la verification pour la visualisation des adresses Facturation pour les utilisateurs qui ne sont pas dans jefy_admin
		if (!appUser.hasDroitTypesAdresseFacturationVisualisation(personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_FACT));
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_LIVR));
		}
		// PYM : et pour les adresse PRO ?
		if (!appUser.hasDroitTypesAdresseProVisualisation(personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_FACT));
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_LIVR));
		}
		return new EOAndQualifier(notAllowed);
	}

	public static EOQualifier qualifierModifiablesForApplicationUser(PersonneApplicationUser appUser, IPersonne personne) {
		NSMutableArray notAllowed = new NSMutableArray();
		notAllowed.addObject(new EONotQualifier(QUAL_TADR_CODE_INVAL));

		if (!appUser.hasDroitModificationEOAdresseDeType(EOTypeAdresse.TADR_CODE_ETUD, personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_ETUD));
		}

		if (!appUser.hasDroitModificationEOAdresseDeType(EOTypeAdresse.TADR_CODE_PAR, personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PAR));
		}

		if (!appUser.hasDroitModificationEOAdresseDeType(EOTypeAdresse.TADR_CODE_PERSO, personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PERSO));
		}
		
		// PYM : Ajouter la verification pour la modification des adresses Facturation pour les utilisateurs qui ne sont pas dans jefy_admin
		if (!appUser.hasDroitModificationEOAdresseDeType(EOTypeAdresse.TADR_CODE_FACT, personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_FACT));
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_LIVR));
		}
		
		if (!appUser.hasDroitModificationEOAdresseDeType(EOTypeAdresse.TADR_CODE_PRO, personne)) {
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PRO));
			notAllowed.addObject(new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_STAGE));
		}

		return new EOAndQualifier(notAllowed);
	}

}
