/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

import er.extensions.localization.ERXLocalizer;

public class InseeDepartementNaissanceFrance implements IIndividuControle {
	public static final String PREFIX = InseeDepartementNaissanceFrance.class.getSimpleName() + ".";
    public static final String REGLE_DEPARTEMENT_FRANCE = PREFIX  + "REGLE_DEPARTEMENT_FRANCE";
	private final int DOM_TOM = 970;
	private final int CORSE = 20;

    public boolean checkable(final IIndividu individu) {
        
        if (individu.getCodeInsee() == null || individu.getDepartement() == null || individu.getDepartement().getDepartementFrance() == null) {
        	return false;
        }
        
        CodeInsee insee = individu.getCodeInsee();
        return !(insee.isHorsFrance() || insee.isSeine() || insee.isSeineEtOise() || insee.isCorseAvantDivision());
    }

    public ResultatControle check(IIndividu individu) {
        if (!checkable(individu)) {
        	return ResultatControle.NON_CHECKABLE;
        }
        ERXLocalizer localizer = ERXLocalizer.currentLocalizer();
        CodeInsee insee = individu.getCodeInsee();
        Integer departement;
        if (!individu.getDepartement().getDepartementFrance().equals("2A") && !individu.getDepartement().getDepartementFrance().equals("2B")) {
        	departement = Integer.parseInt(individu.getDepartement().getDepartementFrance());
        } else {
        	departement = CORSE;
        }
        
        
   		if (departement > DOM_TOM
        		&& (!individu.getDepartement().getDepartementFrance().equals("2A") || !individu.getDepartement().getDepartementFrance().equals("2B"))) {
        	if (!individu.getDepartement().getDepartementFrance().substring(0, 2).equals(insee.getDepartement().substring(0, 2))) {
        		if (!individu.getDepartement().getDepartementFrance().equals(insee.getDepartement())) {
        			return new ResultatControle(false, REGLE_DEPARTEMENT_FRANCE, localizer.localizedStringForKey(REGLE_DEPARTEMENT_FRANCE));
        		}
            }  
        } else {
        	if (!individu.getDepartement().getDepartementFrance().equals(insee.getDepartement())) {
                return new ResultatControle(false, REGLE_DEPARTEMENT_FRANCE, localizer.localizedStringForKey(REGLE_DEPARTEMENT_FRANCE));
            }  
        }
           
        return ResultatControle.RESULTAT_OK;
        
    }
}

