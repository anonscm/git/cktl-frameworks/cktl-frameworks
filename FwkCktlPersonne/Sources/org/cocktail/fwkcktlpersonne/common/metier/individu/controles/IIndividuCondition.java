package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

public interface IIndividuCondition {

	/**
	 * Détermine si une condition est exacte pour un individu
	 * @param individu
	 * @return
	 */
	public boolean isValide(IIndividu individu);
}
