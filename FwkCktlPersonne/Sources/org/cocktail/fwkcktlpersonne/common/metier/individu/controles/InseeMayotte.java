package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

import er.extensions.localization.ERXLocalizer;

public class InseeMayotte implements IIndividuControle {
	
	public static final String PREFIX = InseeMayotte.class.getSimpleName() + ".";
	public static final String REGLE_NAISSANCE_MAYOTTE_A_PARTIR_AVRIL_2009 = PREFIX + "REGLE_NAISSANCE_MAYOTTE_A_PARTIR_AVRIL_2009";
    public static final String REGLE_NAISSANCE_MAYOTTE_AVANT_AVRIL_2009 = PREFIX + "REGLE_NAISSANCE_MAYOTTE_AVANT_AVRIL_2009";
    public static final String REGLE_COHERENCE_MAYOTTE = PREFIX + "REGLE_COHERENCE_MAYOTTE";
    
    public static final String DATE_DE_BASCULE = "2009-03-30";
    public Date dt;

public boolean checkable(IIndividu individu) {
        
        if (individu.getCodeInsee() == null || individu.getDtNaissance() == null || individu.getDepartement() == null) {
            return false;
        }
        CodeInsee insee = individu.getCodeInsee();
        
        return insee.isMayotte();
    }

    public ResultatControle check(IIndividu individu) {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	int comparaisonDate;
    	try {
			dt = sdf.parse(DATE_DE_BASCULE);
		} catch (ParseException e) {
			e.getMessage();
		}
        ERXLocalizer localizer = ERXLocalizer.currentLocalizer();
    	
        if (!checkable(individu)) { return ResultatControle.NON_CHECKABLE; }

        CodeInsee insee = individu.getCodeInsee();
//        Date dateNaissance = individu.getDtNaissance();
        Date dateNaissance = individu.getDtNaissance();
        System.out.println("Date de naissance : " + dateNaissance.toString());
        comparaisonDate = dateNaissance.compareTo(dt);
        System.out.println("Date de bascule : " + dt.toString());
        System.out.println("Comparaison de date : " + comparaisonDate);
        
        String departement =  individu.getDepartement().getCode();
        
        if (comparaisonDate > 0 && departement.equals("985")) {
//        if (dateNaissance.before(dt) && departement.equals("976")) {
            return new ResultatControle(false, REGLE_NAISSANCE_MAYOTTE_A_PARTIR_AVRIL_2009, localizer.localizedStringForKey(REGLE_NAISSANCE_MAYOTTE_A_PARTIR_AVRIL_2009));
        }
//        if (dateNaissance.after(dt)
//        		&& departement.equals("985")) {
//            return new ResultatControle(false, REGLE_NAISSANCE_MAYOTTE_AVANT_AVRIL_2009);
//        }
//        if (dateNaissance.equals(dt)
//        		&& departement.equals("985")) {
//        	return new ResultatControle(false, REGLE_NAISSANCE_MAYOTTE_AVANT_AVRIL_2009);
//        }
        if (comparaisonDate <= 0
        		&& departement.equals("976")) {
        	return new ResultatControle(false, REGLE_NAISSANCE_MAYOTTE_AVANT_AVRIL_2009, localizer.localizedStringForKey(REGLE_NAISSANCE_MAYOTTE_AVANT_AVRIL_2009));
        }

        if (!departement.equals("985") && !departement.equals("976")) {
        	return new ResultatControle(false, REGLE_COHERENCE_MAYOTTE, localizer.localizedStringForKey(REGLE_COHERENCE_MAYOTTE));
        }
        
        
        return ResultatControle.RESULTAT_OK;
    }

	
}
