/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

import er.extensions.localization.ERXLocalizer;

public class InseeProtectoratTunisien implements IIndividuControle {
	public static final String PREFIX = InseeProtectoratTunisien.class.getSimpleName() + ".";
    public static final String REGLE_PAYS_DIFFERENT_FRANCE_POUR_PROTECTORAT = PREFIX + "REGLE_PAYS_DIFFERENT_FRANCE_POUR_PROTECTORAT";
    public static final String REGLE_CODE_PAYS_INSEE_TUNISIE = PREFIX + "REGLE_CODE_PAYS_INSEE_TUNISIE";
    public static final String REGLE_ANNEE_NAISSANCE_INF_68 = PREFIX + "REGLE_ANNEE_NAISSANCE_INF_68";
    
    public static final String CODE_INSEE_TUNISIE="351";

    public boolean checkable(IIndividu individu) {
        if(individu.getCodeInsee() == null || individu.getPaysNaissance() == null || individu.getDtNaissance() == null)
            return false;
        return individu.getCodeInsee().isProtectorat();
    }

    public ResultatControle check(IIndividu individu) {
        if(!checkable(individu)) return ResultatControle.NON_CHECKABLE;
        ERXLocalizer localizer = ERXLocalizer.currentLocalizer();
        CodeInsee insee = individu.getCodeInsee();
        
        if (individu.getPaysNaissance().getCode().equals(EOPays.CODE_PAYS_FRANCE))
            return new ResultatControle(false, REGLE_PAYS_DIFFERENT_FRANCE_POUR_PROTECTORAT, localizer.localizedStringForKey(REGLE_PAYS_DIFFERENT_FRANCE_POUR_PROTECTORAT));
        if(!CODE_INSEE_TUNISIE.equals(insee.getPays()))
            return new ResultatControle(false, REGLE_CODE_PAYS_INSEE_TUNISIE, localizer.localizedStringForKey(REGLE_CODE_PAYS_INSEE_TUNISIE));

        if(individu.getDtNaissance() != null && MyDateCtrl.getYear(individu.getDtNaissance()) > 1967)
            return new ResultatControle(false, REGLE_ANNEE_NAISSANCE_INF_68, localizer.localizedStringForKey(REGLE_ANNEE_NAISSANCE_INF_68));

        return ResultatControle.RESULTAT_OK;
    }

}
