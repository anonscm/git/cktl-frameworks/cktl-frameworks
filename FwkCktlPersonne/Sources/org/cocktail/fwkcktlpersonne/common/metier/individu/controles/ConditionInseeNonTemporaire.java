package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

public class ConditionInseeNonTemporaire implements IIndividuCondition {

	public boolean isValide(IIndividu individu) {
		
		String premierChiffre = individu.indNoInsee().substring(0, 1); 
		
		if ("8".equals(premierChiffre) || "9".equals(premierChiffre)) {
			return true;
		}
		
		return false;
	}
	
	

}
