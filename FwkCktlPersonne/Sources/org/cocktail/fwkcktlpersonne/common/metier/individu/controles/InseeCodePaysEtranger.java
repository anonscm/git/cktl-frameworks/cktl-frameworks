/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;

import er.extensions.localization.ERXLocalizer;

public class InseeCodePaysEtranger implements IIndividuControle {
	public static final String PREFIX = InseeCodePaysEtranger.class.getSimpleName() + ".";
    public static final String REGLE_PAYS_DIFFERENT_FRANCE_POUR_ETRANGER = PREFIX + "REGLE_PAYS_DIFFERENT_FRANCE_POUR_ETRANGER";
    public static final String REGLE_CODE_PAYS_INSEE_INVALIDE = PREFIX + "REGLE_CODE_PAYS_INSEE_INVALIDE";
    public static final String REGLE_CODE_PAYS_INSEE_FRANCE_INVALIDE = PREFIX + "REGLE_CODE_PAYS_INSEE_FRANCE_INVALIDE";
    public static final String REGLE_ETRANGER_PAYS_NAISSANCE_001_999 = PREFIX + "REGLE_ETRANGER_PAYS_NAISSANCE_001_999";

    public boolean checkable(IIndividu individu) {
        return  individu.getCodeInsee() != null
                && individu.getPaysNaissance() != null
                && individu.getCodeInsee().isEtranger();
    }

    public ResultatControle check(IIndividu individu) {
        if (!checkable(individu)) {
        	return ResultatControle.NON_CHECKABLE;
        }
        ERXLocalizer localizer = ERXLocalizer.currentLocalizer();
        CodeInsee insee = individu.getCodeInsee();
        if (individu.getPaysNaissance().getCode().equals(IPays.CODE_PAYS_FRANCE)) {
            return new ResultatControle(false, REGLE_PAYS_DIFFERENT_FRANCE_POUR_ETRANGER, localizer.localizedStringForKey(REGLE_PAYS_DIFFERENT_FRANCE_POUR_ETRANGER));
        }    
     
        if (insee.getPays().equals(IPays.CODE_PAYS_FRANCE)) {
            return new ResultatControle(false, REGLE_CODE_PAYS_INSEE_FRANCE_INVALIDE, localizer.localizedStringForKey(REGLE_CODE_PAYS_INSEE_FRANCE_INVALIDE));
        }
            
        int valeur = 0;
        try {
            valeur = new Integer(insee.getPays()).intValue();
        } catch (Exception e) {
            return new ResultatControle(false, REGLE_CODE_PAYS_INSEE_INVALIDE, localizer.localizedStringForKey(REGLE_CODE_PAYS_INSEE_INVALIDE));
        }
        if (valeur == 0) {
            return new ResultatControle(false, REGLE_ETRANGER_PAYS_NAISSANCE_001_999, localizer.localizedStringForKey(REGLE_ETRANGER_PAYS_NAISSANCE_001_999));
        }
        return ResultatControle.RESULTAT_OK;
    }

}
