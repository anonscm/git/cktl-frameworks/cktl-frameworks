/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

import er.extensions.localization.ERXLocalizer;

public class InseeAlgerieFrancaiseEtProtectoratMarocain implements IIndividuControle {

	public static final String PREFIX = InseeAlgerieFrancaiseEtProtectoratMarocain.class.getSimpleName() + ".";
    public static final String REGLE_PROTECTORAT_ALGERIE_PAS_DEPARTEMENT = PREFIX + "REGLE_PROTECTORAT_ALGERIE_PAS_DEPARTEMENT";
    public static final String REGLE_PAYS_NAISSANCE_MAROC_KO = PREFIX + "REGLE_PAYS_NAISSANCE_MAROC_KO";
    public static final String REGLE_PAYS_NAISSANCE_ALGERIE_KO = PREFIX + "REGLE_PAYS_NAISSANCE_ALGERIE_KO";
    
    public static final String CODE_INSEE_ALGERIE = "352";
    public static final String CODE_INSEE_MAROC = "350";
    public static final String PAYS_INCONNU = "999";

    public boolean checkable(IIndividu individu) {
        
        if (individu.getCodeInsee() == null || individu.getDtNaissance() == null) {
        	return false;
        }

        return MyDateCtrl.getYear(individu.getDtNaissance()) <= 1962
        			&& individu.getCodeInsee().isAlgerieFrancaiseEtProtectoratMarocain();
    }

    public ResultatControle check(IIndividu individu) {
        if (!checkable(individu)) {
        	return ResultatControle.NON_CHECKABLE;
        }
        
        ERXLocalizer localizer = ERXLocalizer.currentLocalizer();
                        
        CodeInsee insee = individu.getCodeInsee();
        
        if (individu.getDepartement() != null) {
            return new ResultatControle(false, REGLE_PROTECTORAT_ALGERIE_PAS_DEPARTEMENT, localizer.localizedStringForKey(REGLE_PROTECTORAT_ALGERIE_PAS_DEPARTEMENT));
        }
        if (insee.isAlgerieFrancaise()) {
            if (!estNeeDansLePaysCible(individu, CODE_INSEE_ALGERIE)) {
                return new ResultatControle(false, REGLE_PAYS_NAISSANCE_ALGERIE_KO, localizer.localizedStringForKey(REGLE_PAYS_NAISSANCE_ALGERIE_KO));
            }
        }
        if (insee.isProtectoratMarocain()) {
            if (!estNeeDansLePaysCible(individu, CODE_INSEE_MAROC)) {
                return new ResultatControle(false, REGLE_PAYS_NAISSANCE_MAROC_KO, localizer.localizedStringForKey(REGLE_PAYS_NAISSANCE_MAROC_KO));
            }
        }
        return ResultatControle.RESULTAT_OK;
    }

	private boolean estNeeDansLePaysCible(IIndividu individu, String codePays) {
		return individu.getPaysNaissance() != null
				&& (individu.getPaysNaissance().getCode().equals(codePays)
						|| individu.getPaysNaissance().getCode().equals(PAYS_INCONNU));
	}

}
