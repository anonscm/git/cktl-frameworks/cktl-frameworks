/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.individu.controles;

import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

import er.extensions.localization.ERXLocalizer;

public class InseeSexe implements IIndividuControle {
	public static final String PREFIX = InseeSexe.class.getSimpleName() + ".";
    public static final String REGLE_FEMME = PREFIX + "REGLE_FEMME";
    public static final String REGLE_HOMME = PREFIX + "REGLE_HOMME";
    
    public boolean checkable(final IIndividu individu) {
        
        if (individu.getCivilite() == null || individu.getCodeInsee() == null) { return false; }
        
        return true;
    }
    
    public ResultatControle check(final IIndividu individu) {
        if (!checkable(individu)) { return ResultatControle.NON_CHECKABLE; }
        ERXLocalizer localizer = ERXLocalizer.currentLocalizer();
        CodeInsee insee = individu.getCodeInsee();
        if (individu.estHomme()) {
            if (!insee.isHomme()) {
                return new ResultatControle(false, REGLE_HOMME, localizer.localizedStringForKey(REGLE_HOMME));
            }
        } else {
            if (!insee.isFemme()) {
                return new ResultatControle(false, REGLE_FEMME, localizer.localizedStringForKey(REGLE_FEMME));
            }
        }
        return ResultatControle.RESULTAT_OK;

   }
}
