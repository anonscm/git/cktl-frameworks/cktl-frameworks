/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCompteEmail.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOCompteEmail extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOCompteEmail.class);

	public static final String ENTITY_NAME = "Fwkpers_CompteEmail";
	public static final String ENTITY_TABLE_NAME = "GRHUM.COMPTE_EMAIL";


// Attribute Keys
  public static final ERXKey<String> CEM_ALIAS = new ERXKey<String>("cemAlias");
  public static final ERXKey<String> CEM_DOMAINE = new ERXKey<String>("cemDomaine");
  public static final ERXKey<String> CEM_EMAIL = new ERXKey<String>("cemEmail");
  public static final ERXKey<Long> CEM_PRIORITE = new ERXKey<Long>("cemPriorite");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> TO_COMPTE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>("toCompte");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cemKey";

	public static final String CEM_ALIAS_KEY = "cemAlias";
	public static final String CEM_DOMAINE_KEY = "cemDomaine";
	public static final String CEM_EMAIL_KEY = "cemEmail";
	public static final String CEM_PRIORITE_KEY = "cemPriorite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

// Attributs non visibles
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String CEM_KEY_KEY = "cemKey";

//Colonnes dans la base de donnees
	public static final String CEM_ALIAS_COLKEY = "CEM_ALIAS";
	public static final String CEM_DOMAINE_COLKEY = "CEM_DOMAINE";
	public static final String CEM_EMAIL_COLKEY = "CEM_EMAIL";
	public static final String CEM_PRIORITE_COLKEY = "CEM_PRIORITE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String CEM_KEY_COLKEY = "CEM_KEY";


	// Relationships
	public static final String TO_COMPTE_KEY = "toCompte";



	// Accessors methods
  public String cemAlias() {
    return (String) storedValueForKey(CEM_ALIAS_KEY);
  }

  public void setCemAlias(String value) {
    takeStoredValueForKey(value, CEM_ALIAS_KEY);
  }

  public String cemDomaine() {
    return (String) storedValueForKey(CEM_DOMAINE_KEY);
  }

  public void setCemDomaine(String value) {
    takeStoredValueForKey(value, CEM_DOMAINE_KEY);
  }

  public String cemEmail() {
    return (String) storedValueForKey(CEM_EMAIL_KEY);
  }

  public void setCemEmail(String value) {
    takeStoredValueForKey(value, CEM_EMAIL_KEY);
  }

  public Long cemPriorite() {
    return (Long) storedValueForKey(CEM_PRIORITE_KEY);
  }

  public void setCemPriorite(Long value) {
    takeStoredValueForKey(value, CEM_PRIORITE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompte toCompte() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompte)storedValueForKey(TO_COMPTE_KEY);
  }

  public void setToCompteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCompte oldValue = toCompte();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTE_KEY);
    }
  }
  

/**
 * Créer une instance de EOCompteEmail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCompteEmail createEOCompteEmail(EOEditingContext editingContext, String cemAlias
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlpersonne.common.metier.EOCompte toCompte			) {
    EOCompteEmail eo = (EOCompteEmail) createAndInsertInstance(editingContext, _EOCompteEmail.ENTITY_NAME);    
		eo.setCemAlias(cemAlias);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToCompteRelationship(toCompte);
    return eo;
  }

  
	  public EOCompteEmail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCompteEmail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompteEmail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompteEmail creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCompteEmail object = (EOCompteEmail)createAndInsertInstance(editingContext, _EOCompteEmail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCompteEmail localInstanceIn(EOEditingContext editingContext, EOCompteEmail eo) {
    EOCompteEmail localInstance = (eo == null) ? null : (EOCompteEmail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCompteEmail#localInstanceIn a la place.
   */
	public static EOCompteEmail localInstanceOf(EOEditingContext editingContext, EOCompteEmail eo) {
		return EOCompteEmail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCompteEmail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCompteEmail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCompteEmail> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCompteEmail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCompteEmail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCompteEmail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCompteEmail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCompteEmail> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCompteEmail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCompteEmail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCompteEmail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCompteEmail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCompteEmail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCompteEmail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
