/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EOTypeTel.java
// 

package org.cocktail.fwkcktlpersonne.common.metier;


import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOTypeTel extends _EOTypeTel implements ITypeTel {
	public static final String C_TYPE_TEL_PRV = "PRV";
	public static final String C_TYPE_TEL_INT = "INT";
    public static final String C_TYPE_TEL_PRF = "PRF";
    public static final String C_TYPE_TEL_PAR = "PAR";
    public static final String C_TYPE_TEL_ETUD = "ETUD";

    public static final EOQualifier QUAL_C_TYPE_TEL_PRV = new EOKeyValueQualifier(EOTypeTel.C_TYPE_TEL_KEY, EOQualifier.QualifierOperatorEqual, C_TYPE_TEL_PRV);
    public static final EOQualifier QUAL_C_TYPE_TEL_INT = new EOKeyValueQualifier(EOTypeTel.C_TYPE_TEL_KEY, EOQualifier.QualifierOperatorEqual, C_TYPE_TEL_INT);
    public static final EOQualifier QUAL_C_TYPE_TEL_PRF = new EOKeyValueQualifier(EOTypeTel.C_TYPE_TEL_KEY, EOQualifier.QualifierOperatorEqual, C_TYPE_TEL_PRF);
    public static final EOQualifier QUAL_C_TYPE_TEL_PAR = new EOKeyValueQualifier(EOTypeTel.C_TYPE_TEL_KEY, EOQualifier.QualifierOperatorEqual, C_TYPE_TEL_PAR);
	public static final EOQualifier QUAL_C_TYPE_TEL_ETUD = new EOKeyValueQualifier(EOTypeTel.C_TYPE_TEL_KEY, EOQualifier.QualifierOperatorEqual, C_TYPE_TEL_ETUD);
    
    private static ERXEnterpriseObjectCache<EOTypeTel> typeTelCache;

	public EOTypeTel() {
        super();
    }
	
	public boolean isTypeInterne() {
		return EOTypeTel.C_TYPE_TEL_INT.equals(this.cTypeTel());
	}
	
	 /**
     * @return le cache des {@link EOTypeFormation} par leur type
     */
    public static ERXEnterpriseObjectCache<EOTypeTel> getTypeTelCache() {
    	if (typeTelCache == null) {
    		typeTelCache = new ERXEnterpriseObjectCache<EOTypeTel>(EOTypeTel.class, C_TYPE_TEL_KEY);
    	}
    	return typeTelCache;
    }
    
    /**
     * @param typeFormationCache le cache a setter
     */
    public static void setTypeTelCache(ERXEnterpriseObjectCache<EOTypeTel> typeTelCache) {
    	EOTypeTel.typeTelCache = typeTelCache;
    }
    
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeNoTel} "Fixe"
	 */
	public static EOTypeTel typeParent(EOEditingContext edc) {
		return getTypeTelCache().objectForKey(edc,C_TYPE_TEL_PAR);
	}
    
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeNoTel} "Fixe"
	 */
	public static EOTypeTel typeEtudiant(EOEditingContext edc) {
		return getTypeTelCache().objectForKey(edc,C_TYPE_TEL_ETUD);
	}
    

}
