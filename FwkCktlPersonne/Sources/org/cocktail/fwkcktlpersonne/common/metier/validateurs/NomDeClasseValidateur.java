/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NomDeClasseValidateur implements Validateur {
	
	public NomDeClasseValidateur() {
		// Constructeur par défaut de la classe
	}

	/**
	 * 
	 * @author Alain Malaplate
	 * 
	 * Implémentation de la méthode héritée de l'interface : Valider(String);
	 * C'est équivalent à ValiderNomDeClasse();
	 * Vérification que c'est un nom de classe valide.
	 */
	
	// Modification du 07/06/2011 avec définition d'une méthode statique
	// utilisant les expressions régilières
	/**
	 * @param s
	 * @return true si s est un nom de classe construit correctement (une URL composée en sens inverse).
	 */
	private boolean isClassNameValid(String s) {
		Pattern p = Pattern.compile("[a-z]{2,3}[.][a-zA-Z_-]{1,}[.][a-zA-Z_-]{1,}[.][a-zA-Z_-]{1,}[.][a-zA-Z_-]{1,}$");
		Matcher m = p.matcher(s);
		return (m.matches());
	}
	
	public void valider(String valeurParam) throws TypageException {
		
		if ( valeurParam == null) {
			throw new TypageException(TypageException.CLASSNAME_MESSAGE);
		}
		
		if ( false == isClassNameValid(valeurParam) ){
			throw new TypageException(TypageException.CLASSNAME_MESSAGE);
		}
	}
	
	
}
