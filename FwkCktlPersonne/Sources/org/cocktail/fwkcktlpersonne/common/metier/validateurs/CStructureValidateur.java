/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEC;


public class CStructureValidateur implements Validateur {
	
	private EOEditingContext editingContext;
	
	public CStructureValidateur() {
		// Constructeur par défaut
	}
	
	/**
	 * 
	 * @author Alain Malaplate
	 * 
	 * Implémentation de la méthode héritée de l'interface : Valider(String);
	 * C'est équivalent à ValiderC_Structure();
	 * Rechercher que l'on a bien une valeur numérique car c'est une clef de table.
	 * 
	 */
	
/*
* *********************************** Beginning of Editing Contexte Area ********************************************
*/

			/**
			 * @return the editingContext
			 */
			public EOEditingContext editingContext() {
				if (editingContext == null) {
					//editingContext = new EOEditingContext();
					editingContext = ERXEC.newEditingContext();
				}
				return editingContext;
			}
/*
* *********************************** End of Editing Contexte Area ********************************************
*/	

	private boolean isCStructureValid(String s) {
		
		 if (s == null || s.length() <= 0){
			 return false;
		 }
		 
		
		boolean validite = false;
		
		if ( MyStringCtrl.isDigits(s) && Integer.parseInt(s) == 0){
			validite = true;
	    	return validite;
		}
		
		EOEditingContext ec = null;
		ec = editingContext();
		

	       //String conditionStr = EOStructure.LL_STRUCTURE_KEY + " = %@ ";
	       String conditionStr = EOStructure.C_STRUCTURE_KEY + " = %@ ";

	       NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
	       tbKeys.add(s);

	       EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys);

	       EOFetchSpecification fetchSpec = new EOFetchSpecification(EOStructure.ENTITY_NAME, qual, null);
	       
	       NSArray<EOStructure> res = ec.objectsWithFetchSpecification(fetchSpec, ec);

	       if (res.count() == 0){
	    	   validite = false;
	    	   return validite;
	       } else if ( res.count() == 1){
	    	   validite = true;
	    	   return validite;
	       }

	       //validite = true;
	       return validite;

	}
	
	public void valider(String valeurParam) throws TypageException {
		// Test de nullité du paramètre
		if ( valeurParam == null ) {
			throw new TypageException(TypageException.C_STRUCTURE_MESSAGE);
		}
		// Test de validité du paramètre
		if ( false == isCStructureValid(valeurParam) ) {
			throw new TypageException(TypageException.C_STRUCTURE_MESSAGE);
		}
	}

}
