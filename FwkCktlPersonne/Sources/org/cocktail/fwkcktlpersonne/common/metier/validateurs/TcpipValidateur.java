/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

public class TcpipValidateur implements Validateur {
	
	public TcpipValidateur() {
		
	}
	
	/**
	 * 
	 * @author Alain Malaplate
	 * 
	 * Implémentation de la méthode héritée de l'interface : Valider(String);
	 * C'est équivalent à ValiderTcpip();
	 * Vérification que c'est une valeur numérique de 4 digits
	 */
	public void valider(String valeurParam) throws TypageException {
		boolean isValid = false;
		
		// Modification du 07/06/2011 qui teste d'abord que le paramètre est bien numérique
		// Modification avec les validations déjà faites par R. Prin
		// Modification faite le 07/06/2011 et le 08/06/2011 après mise à plat avec PYM
		
		if ( valeurParam == null ) {
			throw new TypageException(TypageException.TCPIP_MESSAGE);
		}
		
		if ( MyStringCtrl.isDigits(valeurParam) == true ){
			int valeurNumerique;// valeur numérique du paramètre
			
			if ( (valeurParam.length() == 4) && (valeurParam != null) ) {
				valeurNumerique = Integer.parseInt(valeurParam);
				if ( (valeurNumerique > 999) && (valeurNumerique < 10000) ) {
					isValid = true;
				}
			}
			if ((valeurParam != null) && (valeurParam.length() == 5)) {
				valeurNumerique = Integer.parseInt(valeurParam);
				if ( (valeurNumerique > 9999) && (valeurNumerique < 100000) ) {
					isValid = true;
				}
			}
			
			if (false == isValid){
				throw new TypageException(TypageException.TCPIP_MESSAGE);
			}
		} else {
			throw new TypageException(TypageException.TCPIP_MESSAGE);
		}

	}

}
