/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import com.webobjects.foundation.NSValidation;

public class TypageException extends NSValidation.ValidationException {
	
	/**
	 * 
	 * @author Alain Malaplate
	 * 
	 * Ajout d'une classe contenant en fait le message d'erreur
	 * relatif au choix d'un type de paramètres 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String VALEUR_NUMERIQUE_MESSAGE = "Erreur : pas un format numérique";
	public static final String CODE_ACTIVATION_MESSAGE = "Erreur : pas une chaîne du type oui ou non";
	public static final String DOMAINE_MESSAGE = "Erreur : pas un format de nom de domaine";
	public static final String URL_MESSAGE = "Erreur : pas une chaîne du type URL";
	public static final String HTML_MESSAGE = "Erreur : pas une balise HTML";
	public static final String CLASSNAME_MESSAGE = "Erreur : pas un nom d'URL inversé Exemple valide avec bonne longueur :fr.univlr.cri.print.SIXPrinter";
	public static final String CHARACTERS_LIST_MESSAGE = "Erreur : suite de caractères mais sûrement avec un blanc";
	public static final String PATH_MESSAGE = "Erreur : écriture non correct pour un chemin d'accès";
	public static final String FREE_TEXT_MESSAGE = "Erreur : chaîne vide ou null";
	public static final String SCHOOL_YEAR_MESSAGE = "Erreur : soit pas une valeur numérique, soit année universitaire non valide";
	public static final String TCPIP_MESSAGE = "Erreur : doit être une valeur numérique sur 4 ou 5 digits";
	public static final String VALIDITY_PERIODS_MESSAGE = "Erreur : nombre de jours non égal à 7, 90 ou 365";
	public static final String TIMEZONE_MESSAGE = "Erreur : pas une valeur dans knowTimeZoneNames()";
	public static final String PERS_ID_MESSAGE = "Erreur : pas une valeur numérique ou ID inconnu";
	public static final String C_STRUCTURE_MESSAGE = "Erreur : pas une clé de la table structure ULR";
	public static final String LIST_MESSAGE = "Erreur : pas une valeur de la liste {P, R, E, X} pour le type de Vlan";
	public static final String LOGIN_MESSAGE = "Erreur : pas le login d'un superAgent ";
	public static final String EMAIL_MESSAGE = "Erreur : pas d'arobase ou de nom de domaine dans l'email";
	public static final String DATE_MESSAGE = "Erreur : pas le format dd/mm/YYYY";
	public static final String ID_MESSAGE = "Erreur : pas une clé de table";
	public static final String SERVEUR_MESSAGE = "Erreur : ceci n'est pas une adresse IP";
	public static final String IPV4_MESSAGE = "Erreur : ceci n'est pas une adresse internet de type IPv4";



	
	public TypageException(String message) {
		super(message);
	}

}
