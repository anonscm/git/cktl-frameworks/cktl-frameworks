/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

public class PeriodeValiditeValidateur implements Validateur {
	
	private int[] periodes = {7, 90, 365};
	private int nombrePeriodes = periodes.length;
	
	

	public PeriodeValiditeValidateur() {
		
	}
	
	/**
	 * 
	 * @author Alain Malaplate
	 * 
	 * Implémentation de la méthode héritée de l'interface : Valider(String);
	 * C'est équivalent à ValiderPeriodesValidite();
	 * Comparaison avec les durées pré-établies.
	 * Les durées pré-établies sont inscrites en dur dans le code.
	 * 
	 */
	public void valider(String valeurParam) throws TypageException {
		boolean isValid = false;
		int valeurNumerique; // valeur numérique du paramètre
		int i; // variable de compteur
		
		// Modification du 07/06/2011 avec une vérification supplémentaire
		// Modification avec les validations déjà faites par R. Prin
		// Modification faite le 07/06/2011 et le 08/06/2011 après mise à plat avec PYM
	
		if ( valeurParam == null ) {
			throw new TypageException(TypageException.VALIDITY_PERIODS_MESSAGE);
		}
		
		if ( MyStringCtrl.isDigits(valeurParam) == true) {
			valeurNumerique = Integer.parseInt(valeurParam);
			for (i=0; i<nombrePeriodes;i++) {
				if (periodes[i] == valeurNumerique)
					isValid = true;
			}
			
			if (false == isValid){
				throw new TypageException(TypageException.VALIDITY_PERIODS_MESSAGE);
			}
		} else {
			throw new TypageException(TypageException.VALIDITY_PERIODS_MESSAGE);
		}
		

	}
}
