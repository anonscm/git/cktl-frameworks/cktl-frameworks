/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

/**
 * <p><b>InetAddress</b> validation and conversion routines (<code>java.net.InetAddress</code>).</p>
 *
 * <p>This class provides methods to validate a candidate IP address.
 *
 */
public class InetAddressValidator implements Serializable {
        private static final long serialVersionUID = 788293706350447118L;


    /**
     * Checks if the specified string is a valid IP address.
     * @param inetAddress the string to validate
     * @return true if the string validates as an IP address
     */
   

    /**
     * Validates an IPv4 address. Returns true if valid.
     * @param inet4Address the IPv4 address to validate
     * @return true if the argument contains a valid IPv4 address
     */
    public static boolean isValidInet4Address(String inet4Address) {
    	
    	if ( inet4Address == null ) {
    		return false;
    	} else {
    		// L'expression régulière est dans la méthode statique cette fois
    		Pattern p = Pattern.compile("^[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}$");
    		Matcher m = p.matcher(inet4Address);
            
    		String[] groups = inet4Address.split("[.]");
    		//System.out.println(groups.length);
    		if ( groups.length != 4) {
    			return false;
    		}
    		for (int i = 0; i< groups.length; i++) {
    			//System.out.println(groups[i]);
    			if ( MyStringCtrl.hasOnlyDigits(groups[i]) == false){
    				return false;
    			}
    		}
    		
         // verify that address conforms to generic IPv4 format
            if ( m.matches() == true){
            	 // verify that address subgroups are legal
                for (int i = 0; i <= 3; i++) {
                    String ipSegment = groups[i];
                    if (ipSegment == null || ipSegment.length() <= 0) {
                    	System.out.println("un segment est nul");
                        return false;
                    }

                    int iIpSegment = 0;

                    try {
                        iIpSegment = Integer.parseInt(ipSegment);
                        //System.out.println(iIpSegment);
                    } catch(NumberFormatException e) {
                        return false;
                    }

                    if ( (iIpSegment < 0)||(iIpSegment > 255) ) {
                    	System.out.println("valeur : " + iIpSegment);
                        return false;
                    }
                }
            }     
    	}    

        return true;
    }
}
