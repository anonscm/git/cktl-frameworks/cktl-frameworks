/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.validateurs;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEC;

public class PersIdValidateur implements Validateur {
	
	private EOEditingContext editingContext;
	
	public PersIdValidateur() {
		// Constructeur par défaut de la classe
	}
	
	
	
	/**
	 * 
	 * @author Alain Malaplate
	 * Création en date du 14/06/2011
	 * 
	 * Implémentation de la méthode héritée de l'interface : Valider(String);
	 * C'est équivalent à ValiderID();
	 * Vérification que c'est une valeur déjà dans la table non modélisée Personne
	 */
	
	
	/**
	 * @return the editingContext
	 */
	public EOEditingContext editingContext() {
		if (editingContext == null) {
			editingContext = ERXEC.newEditingContext();
		}
		return editingContext;
	}

	protected boolean isStructureKey(String valeurParam, EOEditingContext ec) {
		if (valeurParam == null || valeurParam.length() <= 0){
			return false;
		}
		
		String conditionStr_structure = EOStructure.C_STRUCTURE_KEY + " = %@ ";
		
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		tbKeys.add(valeurParam);
		
		EOQualifier qual_structure = EOQualifier.qualifierWithQualifierFormat(conditionStr_structure, tbKeys);
		
		EOFetchSpecification fetchSpec_structure = new EOFetchSpecification(EOStructure.ENTITY_NAME, qual_structure, null);
		
		NSArray<EOStructure> res_structure = ec.objectsWithFetchSpecification(fetchSpec_structure, ec);
		
		if ( res_structure.count() == 0 ){
			return false;
		} else if ( res_structure.count() == 1 ){
			return true;
		}
		
		return false;
		
	}
		
	protected boolean isIndividuPersId(String valeurParam, EOEditingContext ec) {
		if (valeurParam == null || valeurParam.length() <= 0){
			return false;
		}
		try {
			String conditionStr_Individu = EOIndividu.PERS_ID_KEY + " = %@ ";

			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			tbKeys.add(Integer.parseInt(valeurParam));

			EOQualifier qual_Individu = EOQualifier.qualifierWithQualifierFormat(conditionStr_Individu, tbKeys);

			EOFetchSpecification fetchSpec_Individu = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual_Individu, null);

			NSArray<EOIndividu> res_Individu = ec.objectsWithFetchSpecification(fetchSpec_Individu, ec);

			if ( res_Individu.count() == 0 ){
				return false;
			} else if ( res_Individu.count() == 1 ){
				return true;
			}

		} catch ( NumberFormatException e) {
			throw new TypageException(TypageException.PERS_ID_MESSAGE);
		}
		
		
		return false;
		
	}
	
	
	private boolean ifExists(String valeurAComparer) {
		
		if (valeurAComparer == null || valeurAComparer.length() <= 0){
			return false;
		}
		
		boolean validite = false;
		
		EOEditingContext ec = null;
		ec = editingContext();
		

		if ( (true == isIndividuPersId(valeurAComparer, ec)) || (true == isStructureKey(valeurAComparer, ec)) ){
			validite = true;
		} 

		return validite;
	}	
	

	public void valider(String valeurParam) throws TypageException {

		if ( valeurParam == null ) {
			throw new TypageException(TypageException.PERS_ID_MESSAGE);
		}

		if ( false == ifExists(valeurParam) ) {
			throw new TypageException(TypageException.PERS_ID_MESSAGE);
		}

	}

}
