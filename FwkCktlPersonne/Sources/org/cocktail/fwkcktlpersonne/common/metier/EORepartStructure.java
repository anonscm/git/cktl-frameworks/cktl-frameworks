/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EORepartStructure.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Enumeration;
import java.util.Iterator;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRepartStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class EORepartStructure extends _EORepartStructure implements
		IRepartStructure {

	public static final String TO_PERSONNE_ELT_KEY = "toPersonneElt";

	// public static final EOQualifier QUAL_TO_STRUCTURE_VALIDE =
	// EOQualifier.qualifierWithQualifierFormat(EORepartStructure.TO_STRUCTURE_GROUPE_KEY
	// + "." + EOStructure.TEM_VALIDE_KEY + "=%s", new
	// NSArray(EOStructure.TEM_VALIDE_OUI));
	public static final EOQualifier QUAL_TO_STRUCTURE_VALIDE = new EOKeyValueQualifier(
			EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "."
					+ EOStructure.TEM_VALIDE_KEY,
			EOQualifier.QualifierOperatorEqual, EOStructure.TEM_VALIDE_OUI);
	public static final EOQualifier QUAL_TO_STRUCTURE_GROUPE_PUBLIC = new EOKeyValueQualifier(
			EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "."
					+ EOStructure.GRP_ACCES_KEY,
			EOQualifier.QualifierOperatorEqual,
			EOStructureForGroupeSpec.GRP_ACCES_PUBLIC);
	public static final EOQualifier QUAL_TO_STRUCTURE_GROUPE_PRIVE = new EOKeyValueQualifier(
			EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "."
					+ EOStructure.GRP_ACCES_KEY,
			EOQualifier.QualifierOperatorEqual,
			EOStructureForGroupeSpec.GRP_ACCES_PRIVE);

	public static final EOSortOrdering SORT_LL_STRUCTURE_ASC = EOSortOrdering
			.sortOrderingWithKey(EORepartStructure.TO_STRUCTURE_GROUPE_KEY
					+ "." + EOStructure.LL_STRUCTURE_KEY,
					EOSortOrdering.CompareAscending);

	public static final String ROLE_KEY = "roles";
	public static final ERXKey<String> ROLE = new ERXKey<String>(ROLE_KEY);

	private static boolean ACTIVER_CACHE = false;

	private IPersonne personneEltCache;

	// private EOStructure personneEltCache;

	public EORepartStructure() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		// ajouter la verification du droit sur les groupes lorsque
		// l'utilisateur sera memorisé ici
		try {
			setDModification(AUtils.now());
			if (persId() == null) {
				throw new NSValidation.ValidationException(
						"Vous devez fournir le persId d'un individu ou d'une structure");
			}
			if (toStructureGroupe() == null) {
				throw new NSValidation.ValidationException(
						"Vous devez fournir une structure");
			}

			checkUsers();
			checkDoublons();

			// Verifier que la structureGroupe a bien un repartTypeGroupe a G
			EOStructureForGroupeSpec.setStructureAsGroupe(toStructureGroupe());
			super.validateObjectMetier();
		} catch (Exception e) {
			e.printStackTrace();
			NSValidation.ValidationException e1 = new ValidationException(
					"Erreur sur RepartStructure " + libelleEtId() + " : "
							+ e.getMessage());
			e1.initCause(e);
			throw e1;
		}
	}

	public void checkUsers() throws NSValidation.ValidationException {
		if (persIdModification() == null) {
			throw new NSValidation.ValidationException(
					"La référence au modificateur (persIdModification) est obligatoire.");
		}
		if (persIdCreation() == null) {
			setPersIdCreation(persIdModification());
		}

		PersonneApplicationUser appUser;
		if (hasTemporaryGlobalID()) {
			appUser = getCreateur();
		} else {
			appUser = getModificateur();
		}

		EOStructure groupeDefaut = EOStructureForGroupeSpec
				.getGroupeDefaut(editingContext());
		if (!(appUser.hasDroitGererGroupe(toStructureGroupe())
				|| (groupeDefaut != null && groupeDefaut
						.equals(toStructureGroupe())) || EOStructureForGroupeSpec
					.isGroupeGereEnAuto(toStructureGroupe()))) {
			throw new NSValidation.ValidationException(
					"Vous n'avez pas le droit de gerer les elements du groupe "
							+ toStructureGroupe().llStructure());
		}

	}

	public void init(IPersonne personne, EOStructure structureGroupe) {
		init(personne.persId(), structureGroupe);
		personne.addToToRepartStructuresRelationship(this);
	}

	//
	// /** Initialise une repart avec un individu et une structure */
	// public void init(EOIndividu individu,EOStructure structureGroupe) {
	// init(individu.persId(), structureGroupe);
	// individu.addToToRepartStructuresRelationship(this);
	// }
	//
	// public void init(EOStructure structure, EOStructure structureGroupe) {
	// init(structure.persId(), structureGroupe);
	// structure.addToToRepartStructuresRelationship(this);
	// }

	public void init(Integer persId, EOStructure structureGroupe) {
		setPersId(persId);
		setCStructure(structureGroupe.cStructure());
		setToStructureGroupeRelationship(structureGroupe);
	}

	/**
	 * recherche les repart structures liés &agrave; un individu et une
	 * structure
	 * 
	 * @param editingContext
	 * @param individu
	 * @return repart structure
	 */
	public static NSArray rechercherRepartStructuresPourIndividu(
			EOEditingContext editingContext, EOIndividu individu) {
		return rechercherRepartStructuresPourIndividuEtStructure(
				editingContext, individu, null);
	}

	/**
	 * recherche les repart structures liés &agrave; un individu et une
	 * structure
	 * 
	 * @param editingContext
	 * @param individu
	 * @param structure
	 *            (peut &ecirc;tre nul)
	 * @return repart structure
	 */
	public static NSArray rechercherRepartStructuresPourIndividuEtStructure(
			EOEditingContext editingContext, EOIndividu individu,
			EOStructure structure) {
		return rechercherRepartStructuresPourPersidEtStructure(editingContext,
				individu.persId(), structure);
		// String stringQualifier = "persId = %@";
		// if (structure != null) {
		// args.addObject(structure);
		// stringQualifier = stringQualifier + " AND structure  = %@";
		// }
		// EOQualifier qualifier =
		// EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		// EOFetchSpecification fs = new
		// EOFetchSpecification("RepartStructure",qualifier, null);
		// return editingContext.objectsWithFetchSpecification(fs);
	}

	public static NSArray rechercherRepartStructuresPourPersidEtStructure(
			EOEditingContext editingContext, Number persId,
			EOStructure structure) {
		NSMutableArray args = new NSMutableArray(persId);
		String stringQualifier = EORepartStructure.PERS_ID_KEY + " = %@";
		if (structure != null) {
			args.addObject(structure);
			stringQualifier = stringQualifier + " AND "
					+ EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "  = %@";
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
				stringQualifier, args);
		EOFetchSpecification fs = new EOFetchSpecification(
				EORepartStructure.ENTITY_NAME, qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	/**
	 * supprime les repart structure pour un individu
	 * 
	 * @deprecated Utilisez
	 *             {@link IPersonne#supprimerAffectationATousLesGroupes(EOEditingContext, Integer)}
	 *             a la place.
	 */
	public static void supprimerRepartPourIndividu(
			EOEditingContext editingContext, EOIndividu individu) {
		NSArray args = new NSArray(individu.persId());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
				"persId = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(
				EORepartStructure.ENTITY_NAME, qualifier, null);
		NSArray reparts = editingContext.objectsWithFetchSpecification(fs);
		supprimerSiNecessaire(editingContext, reparts);
	}

	/**
	 * supprime une repart structure pour un individu et une structure
	 * 
	 * @deprecated Utilisez
	 *             {@link IPersonne#supprimerAffectationAUnGroupe(EOEditingContext, Integer, EOStructure)}
	 *             a la place.
	 */
	public static void supprimerRepartPourIndividuEtStructure(
			EOEditingContext editingContext, EOIndividu individu,
			EOStructure structure) {
		NSMutableArray args = new NSMutableArray(individu.persId());
		args.addObject(structure);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
				EORepartStructure.PERS_ID_KEY + " = %@ AND "
						+ EORepartStructure.TO_STRUCTURE_GROUPE_KEY + " = %@",
				args);
		EOFetchSpecification fs = new EOFetchSpecification(
				EORepartStructure.ENTITY_NAME, qualifier, null);
		NSArray reparts = editingContext.objectsWithFetchSpecification(fs);
		supprimerSiNecessaire(editingContext, reparts);
	}

	// /**
	// * Supprime obligatoirement l'afectation d'une personne a un groupe
	// (sructure).
	// *
	// * @param editingContext
	// * @param persId
	// * @param groupe
	// */
	// public static void
	// supprimerForceRepartPourPersIdEtGroupe(EOEditingContext
	// editingContext,Number persId,EOStructure groupe) {
	// NSMutableArray args = new NSMutableArray();
	// args.addObject(persId);
	// args.addObject(groupe);
	// EOQualifier qualifier =
	// EOQualifier.qualifierWithQualifierFormat(EORepartStructure.PERS_ID_KEY +
	// " = %@ AND "+ EORepartStructure.TO_STRUCTURE_GROUPE_KEY +" = %@",args);
	// EOFetchSpecification fs = new
	// EOFetchSpecification(EORepartStructure.ENTITY_NAME,qualifier, null);
	// NSArray reparts = editingContext.objectsWithFetchSpecification(fs);
	// Enumeration e = reparts.objectEnumerator();
	// while (e.hasMoreElements()) {
	// EORepartStructure repart = (EORepartStructure)e.nextElement();
	// //
	// repart.removeObjectFromBothSidesOfRelationshipWithKey(repart.toStructure(),
	// "structure");
	// repart.setToStructureRelationship(null);
	// editingContext.deleteObject(repart);
	// }
	//
	// }

	/**
	 * Supprime le repartStructure pour une personne et un groupe donnés.
	 * 
	 * @param editingContext
	 * @param personne
	 * @param groupe
	 * @deprecated Utilisez
	 *             {@link IPersonne#supprimerAffectationAUnGroupe(EOEditingContext, Integer, EOStructure)}
	 *             a la place.
	 */
	public static void supprimerRepartPourPersonneEtGroupe(
			EOEditingContext editingContext, IPersonne personne,
			EOStructure groupe) {
		EOQualifier qual = new EOKeyValueQualifier(
				EORepartStructure.TO_STRUCTURE_GROUPE_KEY,
				EOQualifier.QualifierOperatorEqual, groupe);
		NSArray reparts = EOQualifier.filteredArrayWithQualifier(
				personne.toRepartStructures(), qual);
		Enumeration e = reparts.objectEnumerator();
		while (e.hasMoreElements()) {
			EORepartStructure repart = (EORepartStructure) e.nextElement();
			repart.deleteAllToRepartAssociationsRelationships();
			repart.setToStructureGroupeRelationship(null);
			personne.removeFromToRepartStructuresRelationship(repart);
			editingContext.deleteObject(repart);
		}
	}

	/**
	 * Ne pas utiliser cette methode sauf si vous etes sur que vous pouvez
	 * supprimer la repartition (cad pas de repartition sur un groupe gere en
	 * auto). Dans le cas normal, utilisez Utilisez
	 * {@link IPersonne#supprimerAffectationAUnGroupe(EOEditingContext, Integer, EORepartStructure)}
	 * a la place.
	 * 
	 * @param editingContext
	 * @param repart
	 */
	protected static void supprimerForce(EOEditingContext editingContext,
			EORepartStructure repart) {
		repart.deleteAllToRepartAssociationsRelationships();
		repart.toPersonneElt().removeFromToRepartStructuresRelationship(repart);
		repart.setToStructureGroupeRelationship(null);
		editingContext.deleteObject(repart);
	}

	/**
	 * Ne pas utiliser cette methode sauf si vous etes sur que vous pouvez
	 * supprimer la repartition (cad pas de repartition sur un groupe gere en
	 * auto). Dans le cas normal, utilisez Utilisez
	 * {@link IPersonne#supprimerAffectationAUnGroupe(EOEditingContext, Integer, EORepartStructure)}
	 * a la place.
	 * 
	 * @param editingContext
	 * @param repart
	 */
	protected static void supprimerForce(EOEditingContext editingContext,
			IPersonne personne, EOStructure groupe) {
		if (groupe != null) {
			NSArray reparts = personne
					.toRepartStructures(new EOKeyValueQualifier(
							EORepartStructure.TO_STRUCTURE_GROUPE_KEY,
							EOQualifier.QualifierOperatorEqual, groupe));
			Enumeration e = reparts.objectEnumerator();
			while (e.hasMoreElements()) {
				EORepartStructure repart = (EORepartStructure) e.nextElement();
				supprimerForce(editingContext, repart);
			}
		}
	}

	private static void supprimerSiNecessaire(EOEditingContext editingContext,
			NSArray reparts) {
		// rechercher les repart structures à maintenir
		String structureFouArchives = EOGrhumParametres.parametrePourCle(
				editingContext, "ANNUAIRE_FOU_ARCHIVES");
		String structureFouEnCours = EOGrhumParametres.parametrePourCle(
				editingContext, "ANNUAIRE_FOU_ENCOURS_VALIDE");
		String structureFouValide = EOGrhumParametres.parametrePourCle(
				editingContext, "ANNUAIRE_FOU_VALIDE_PHYSIQUE");
		java.util.Enumeration e = reparts.objectEnumerator();
		while (e.hasMoreElements()) {
			EORepartStructure repart = (EORepartStructure) e.nextElement();
			boolean supprimer = true;
			if ((structureFouArchives != null && repart.toStructureGroupe()
					.cStructure().equals(structureFouArchives))
					|| (structureFouEnCours != null && repart
							.toStructureGroupe().cStructure()
							.equals(structureFouEnCours))
					|| (structureFouValide != null && repart
							.toStructureGroupe().cStructure()
							.equals(structureFouValide))) {
				supprimer = false;
			}
			if (supprimer) {
				// repart.removeObjectFromBothSidesOfRelationshipWithKey(repart.toStructure(),
				// "structure");
				repart.deleteAllToRepartAssociationsRelationships();
				repart.setToStructureGroupeRelationship(null);
				editingContext.deleteObject(repart);
			}
		}
	}

	public static EORepartStructure creerInstance(EOEditingContext ec) {
		EORepartStructure object = (EORepartStructure) AUtils
				.instanceForEntity(ec, EORepartStructure.ENTITY_NAME);
		ec.insertObject(object);
		return object;
	}

	/**
	 * @param ec
	 * @param personne
	 *            La personne (structure ou individu) a inserer dans le groupe
	 * @param structureGroupe
	 *            Le groupe dans lequel inserer la personne.
	 * @return L'objet existant trouvé ou bien un nouvel objet
	 *         EORepartStructure.
	 * @deprecated Utilisez
	 *             {@link EORepartStructure#creerInstanceSiNecessaire(EOEditingContext, IPersonne, EOStructure, Integer)}
	 *             a la place.
	 */
	public static EORepartStructure creerInstanceSiNecessaire(
			EOEditingContext ec, IPersonne personne, EOStructure structureGroupe) {
		EORepartStructure repartStructure = null;
		// si la structure est deja affectee au groupe on ne l'affecte pas une
		// nouvelle fois
		EOQualifier qual = new EOKeyValueQualifier(
				EORepartStructure.TO_STRUCTURE_GROUPE_KEY,
				EOQualifier.QualifierOperatorEqual, structureGroupe);
		NSArray res = EOQualifier.filteredArrayWithQualifier(
				personne.toRepartStructures(), qual);
		if (res.count() == 0) {
			repartStructure = EORepartStructure.creerInstance(ec);
			repartStructure.init(personne, structureGroupe);
		} else {
			repartStructure = (EORepartStructure) res.objectAtIndex(0);
		}
		return repartStructure;
	}

	/**
	 * Cree une instance de repartStructure si elle n'existe pas deja.
	 * 
	 * @param ec
	 * @param personne
	 *            La personne (structure ou individu) a inserer dans le groupe
	 * @param structureGroupe
	 *            Le groupe dans lequel inserer la personne.
	 * @param persId
	 *            Le persId de l'utilisateur
	 * @return L'objet existant trouvé ou bien un nouvel objet
	 *         EORepartStructure.
	 */
	public static EORepartStructure creerInstanceSiNecessaire(
			EOEditingContext ec, IPersonne personne,
			EOStructure structureGroupe, Integer persId) {
		EORepartStructure repartStructure = null;
		// si la structure est deja affectee au groupe on ne l'affecte pas une
		// nouvelle fois
		EOQualifier qual = new EOKeyValueQualifier(
				EORepartStructure.TO_STRUCTURE_GROUPE_KEY,
				EOQualifier.QualifierOperatorEqual, structureGroupe);
		NSArray res = EOQualifier.filteredArrayWithQualifier(
				personne.toRepartStructures(), qual);
		if (res.count() == 0) {
			repartStructure = EORepartStructure.creerInstance(ec);
			repartStructure.init(personne, structureGroupe);
		} else {
			repartStructure = (EORepartStructure) res.objectAtIndex(0);
		}
		repartStructure.setPersIdModification(persId);
		repartStructure.toPersonneElt().getPersonneDelegate()
				.fixPersIdModificationEtCreation(persId);
		repartStructure.toStructureGroupe().getPersonneDelegate()
				.fixPersIdModificationEtCreation(persId);
		return repartStructure;
	}

	/**
	 * Affecte la personne au repartStructure.
	 * 
	 * @param personne
	 */
	public void setToPersonneElt(IPersonne personne) {
		// Nettoyer
		cleanPersonneEltCache();
		while (toPersonneElt() != null) {
			if (toPersonneElt() instanceof EOIndividu) {
				removeFromToIndividuEltsRelationship((EOIndividu) toPersonneElt());
			}
			if (toPersonneElt() instanceof EOStructure) {
				removeFromToStructureEltsRelationship((EOStructure) toPersonneElt());
			}
		}

		IPersonne _personne = personne;
		if (_personne == null) {
			setPersId(null);
		} else {
			if (!this.editingContext().equals(personne.editingContext())) {
				_personne = personne.localInstanceIn(this.editingContext());
			}
			setPersId(_personne.persId());
			_personne.addToToRepartStructuresRelationship(this);

		}
		if (toRepartAssociations() != null) {
			for (int i = 0; i < toRepartAssociations().count(); i++) {
				((EORepartAssociation) toRepartAssociations().objectAtIndex(i))
						.setToPersonne(_personne);
			}
		}

	}

	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setDCreation(AUtils.now());
	}

	@Override
	public void setToStructureGroupeRelationship(EOStructure value) {
		super.setToStructureGroupeRelationship(value);
		if (value == null) {
			setCStructure(null);
		} else {
			setCStructure(value.cStructure());
		}

		// Fixer les pb de persIdModification absents
		if (toStructureGroupe() != null
				&& toStructureGroupe().persIdModification() == null) {
			if (toPersonneElt() != null
					&& toPersonneElt().persIdModification() != null) {
				toStructureGroupe().getPersonneDelegate()
						.fixPersIdModificationEtCreation(
								toPersonneElt().persIdModification());
			}
		}
		if (toPersonneElt() != null
				&& toPersonneElt().persIdModification() == null) {
			if (toStructureGroupe() != null
					&& toStructureGroupe().persIdModification() != null) {
				toPersonneElt().getPersonneDelegate()
						.fixPersIdModificationEtCreation(
								toStructureGroupe().persIdModification());
			}
		}

		// if (toRepartAssociation() != null) {
		// toRepartAssociation().setToStructureRelationship(value);
		// }

		if (toRepartAssociations() != null) {
			for (int i = 0; i < toRepartAssociations().count(); i++) {
				((EORepartAssociation) toRepartAssociations().objectAtIndex(i))
						.setToStructureRelationship(value);
			}
		}
	}

	/**
	 * @return La personne pour la partie "element" de repartStructure.
	 */
	public IPersonne toPersonneElt() {
		if (ACTIVER_CACHE) {
			if (personneEltCache != null) {
				return personneEltCache;
			}
		}

		if (toStructureElts() != null && toStructureElts().count() > 0) {
			personneEltCache = (IPersonne) toStructureElts().objectAtIndex(0);
		} else if (toIndividuElts() != null && toIndividuElts().count() > 0) {
			personneEltCache = (IPersonne) toIndividuElts().objectAtIndex(0);
		}
		return personneEltCache;
	}

	private void checkDoublons() throws NSValidation.ValidationException {
		EOStructure groupe = this.toStructureGroupe();
		EOQualifier qual1 = new EOKeyValueQualifier(PERS_ID_KEY,
				EOQualifier.QualifierOperatorEqual, this.toPersonneElt()
						.persId());

		NSArray res = EOQualifier.filteredArrayWithQualifier(
				groupe.toRepartStructuresElts(), qual1);
		for (int i = 0; i < res.count(); i++) {
			EORepartStructure array_element = (EORepartStructure) res
					.objectAtIndex(i);
			if (!array_element.globalID().equals(this.globalID())) {
				throw new NSValidation.ValidationException("La personne "
						+ this.toPersonneElt().persLibelle()
						+ " est deja affectee au groupe " + groupe.libelle());
			}
		}
	}

	@Override
	public void removeFromToRepartAssociationsRelationship(
			EORepartAssociation object) {
		// object.setPersId(null);
		// object.setToStructureRelationship(null);
		super.removeFromToRepartAssociationsRelationship(object);
	}

	public void addToToRepartAssociationsRelationship(EORepartAssociation object) {
		super.addToToRepartAssociationsRelationship(object);
		object.setPersId(persId());
		object.setToStructureRelationship(toStructureGroupe());

		object.setToPersonne(toPersonneElt());
	}

	public void nettoieRepartAssociationVides() {
		NSArray res = toRepartAssociations();
		for (int i = res.count() - 1; i >= 0; i--) {
			EORepartAssociation rep = (EORepartAssociation) res
					.objectAtIndex(i);
			if (rep.isEmpty()) {
				this.removeFromToRepartAssociationsRelationship(rep);
				rep.setToAssociationRelationship(null);
				rep.setToPersonne(null);
				rep.setToStructureRelationship(null);
				this.editingContext().deleteObject(rep);
			}
		}

	}

	public void fixPersIdCreation(Integer persId) {
		if (persIdCreation() == null) {
			setPersIdCreation(persId);
		}
	}

	public void setPersIdModification(Integer value) {
		if (persIdCreation() == null) {
			setPersIdCreation(value);
		}
		super.setPersIdModification(value);

		if (toStructureGroupe() != null) {
			toStructureGroupe().getPersonneDelegate()
					.fixPersIdModificationEtCreation(value);
		}

		if (toPersonneElt() != null) {
			toPersonneElt().getPersonneDelegate()
					.fixPersIdModificationEtCreation(value);
		}

	}

	public void cleanPersonneEltCache() {
		personneEltCache = null;
	}

	public String libelleEtId() {
		return (toStructureGroupe() != null ? toStructureGroupe().libelleEtId()
				: "")
				+ "<-->"
				+ (toStructureElts().count() > 0 ? ((IPersonne) toStructureElts()
						.objectAtIndex(0)).libelleEtId() : "");
	}

	public static NSArray<EORepartStructure> getRepartStructureWithIndividusValidesMembresDe(
			EOEditingContext editingContext, EOStructure structure) {

		EOQualifier qualifier = ERXQ.and(ERXQ.equals(
				EORepartStructure.TO_STRUCTURE_GROUPE_KEY, structure), ERXQ
				.equals(EORepartStructure.TO_INDIVIDU_ELTS_KEY + "."
						+ EOIndividu.TEM_VALIDE_KEY, EOIndividu.TEM_VALIDE_O));
		ERXFetchSpecification<EORepartStructure> fetchSpecification = new ERXFetchSpecification<EORepartStructure>(
				EORepartStructure.ENTITY_NAME, qualifier, null);
		NSArray<String> prefetchPaths = new NSMutableArray<String>();
		prefetchPaths.add(EORepartStructure.TO_INDIVIDU_ELTS.dot(
				EOIndividu.TO_PERSONNELS).key());
		prefetchPaths.add(EORepartStructure.TO_INDIVIDU_ELTS.dot(
				EOIndividu.TO_V_PERSONNEL_ACTUELS).key());
		NSArray<EORepartStructure> repartStructures = fetchSpecification
				.fetchObjects(editingContext);
		NSArray<EORepartStructure> resultats = new NSMutableArray<EORepartStructure>();
		for (EORepartStructure repartStructure : repartStructures) {
			EOIndividu individu = ERXArrayUtilities.firstObject(repartStructure
					.toIndividuElts());
			if (individu.toPersonnels().isEmpty()) {
				resultats.add(repartStructure);
			} else if (individu.toVPersonnelActuels().isEmpty() == false) {
				resultats.add(repartStructure);
			}
		}
		return resultats;
	}

	// methodes rajoutees après le passage de Feve sous les Frameworks

	public static final String TO_INDIVIDU_KEY = "toIndividu";

	/**
	 * L'individu associé
	 * 
	 * @return l'individu associé à une structure
	 */
	public EOIndividu toIndividu() {
		EOIndividu toIndividu = null;

		toIndividu = EOIndividu.fetchByKeyValue(editingContext(),
				EOIndividu.PERS_ID_KEY, persId());

		return toIndividu;
	}

	/**
	 * Creation d'un enregistrement dans le context
	 * 
	 * @param ec
	 *            Editing Context de la création de la RepartStructure
	 * @param cStructure
	 *            le CStructure de la structure de rattachement
	 * @param persId
	 *            de la personne rattachée à la structure
	 * @return un objet de type EORepartStructure
	 */
	public static EORepartStructure createGrhumRepartStructure(
			EOEditingContext ec, EOStructure structure, EOIndividu individu,
			Integer persIdConnected) {
		EORepartStructure eo = EORepartStructure.creerInstance(ec);
		eo.setCStructure(structure.cStructure());
		eo.setToStructureGroupeRelationship(structure);
		eo.setToPersonneElt(individu);
		eo.setPersIdCreation(persIdConnected);
		eo.setPersIdModification(persIdConnected);
		eo.setDCreation(DateCtrl.now());
		eo.setDModification(DateCtrl.now());
		return eo;
	}

	/**
	 * Permet de mettre à jour la personne et la date de modification (cf DG
	 * 6654) => met à jour les associations et la répartition de structure
	 * 
	 * @param ec
	 *            : Editing context
	 * @param persIdConnected
	 *            : personne à l'origine de la suppression
	 */
	public void majModificationPourRepartStructureEtAssociations(
			EOEditingContext ec, Integer persIdConnected) {
		// Met à jour toutes les associations correspondantes
		for (EORepartAssociation uneRepartAssociation : this
				.toRepartAssociations()) {
			uneRepartAssociation.setDModification(DateCtrl.now());
			uneRepartAssociation.setPersIdModification(persIdConnected);
		}

		this.setDModification(DateCtrl.now());
		this.setPersIdModification(persIdConnected);
		ec.saveChanges();
	}

	/**
	 * Retourne une chaine contenant les rôles de l'individu dans la structure
	 * @return roles de l'individus pour la structure. Les rôles sont séparés par des ","
	 */
	public String roles() {
		NSArray<EORepartAssociation> repartAssociations = toRepartAssociations();
		Boolean premierRole=true;

		String result = "";
		if (repartAssociations != null) {
			Iterator<EORepartAssociation> iterRepartAsso = repartAssociations
					.iterator();
			while (iterRepartAsso.hasNext()) {
				EORepartAssociation repartAssociation = iterRepartAsso.next();
				if (repartAssociation != null
						&& repartAssociation.toAssociation() != null) {
					EOAssociation association = repartAssociation
							.toAssociation();
					if (premierRole)
					{
						result += association.assLibelle();
						premierRole=false;
					}
					else
						result += ", "+association.assLibelle();
				}
			}
		}
		return result;
	}

}
