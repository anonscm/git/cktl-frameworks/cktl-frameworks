/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReferensEmplois.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOReferensEmplois extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOReferensEmplois.class);

	public static final String ENTITY_NAME = "Fwkpers_ReferensEmplois";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REFERENS_EMPLOIS";


// Attribute Keys
  public static final ERXKey<String> CODE_MEN = new ERXKey<String>("codeMen");
  public static final ERXKey<String> DEFINITION = new ERXKey<String>("definition");
  public static final ERXKey<String> INTITUL_EMPLOI = new ERXKey<String>("intitulEmploi");
  public static final ERXKey<String> LETTRE_BAP = new ERXKey<String>("lettreBap");
  public static final ERXKey<String> NUM_BAP = new ERXKey<String>("numBap");
  public static final ERXKey<String> NUM_DCP = new ERXKey<String>("numDcp");
  public static final ERXKey<String> NUM_EMPLOI = new ERXKey<String>("numEmploi");
  public static final ERXKey<String> NUM_FP = new ERXKey<String>("numFp");
  public static final ERXKey<String> OUVERT_CONCOURS = new ERXKey<String>("ouvertConcours");
  public static final ERXKey<String> SIGLE_CORPS = new ERXKey<String>("sigleCorps");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> TO_REFERENS_CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps>("toReferensCorps");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> TO_REFERENS_FP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp>("toReferensFp");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> TOS_REFERENS_ACTIVITES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites>("tosReferensActivites");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> TOS_REFERENS_COMPETENCES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>("tosReferensCompetences");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "codeEmploi";

	public static final String CODE_MEN_KEY = "codeMen";
	public static final String DEFINITION_KEY = "definition";
	public static final String INTITUL_EMPLOI_KEY = "intitulEmploi";
	public static final String LETTRE_BAP_KEY = "lettreBap";
	public static final String NUM_BAP_KEY = "numBap";
	public static final String NUM_DCP_KEY = "numDcp";
	public static final String NUM_EMPLOI_KEY = "numEmploi";
	public static final String NUM_FP_KEY = "numFp";
	public static final String OUVERT_CONCOURS_KEY = "ouvertConcours";
	public static final String SIGLE_CORPS_KEY = "sigleCorps";

// Attributs non visibles
	public static final String CODE_EMPLOI_KEY = "codeEmploi";

//Colonnes dans la base de donnees
	public static final String CODE_MEN_COLKEY = "CODEMEN";
	public static final String DEFINITION_COLKEY = "DEFINTION_CLEAN";
	public static final String INTITUL_EMPLOI_COLKEY = "INTITULEMPLOI";
	public static final String LETTRE_BAP_COLKEY = "LETTREBAP";
	public static final String NUM_BAP_COLKEY = "NUMBAP";
	public static final String NUM_DCP_COLKEY = "NUMDCP";
	public static final String NUM_EMPLOI_COLKEY = "NUMEMPLOI";
	public static final String NUM_FP_COLKEY = "NUMFP";
	public static final String OUVERT_CONCOURS_COLKEY = "OUVERTCONCOURS";
	public static final String SIGLE_CORPS_COLKEY = "SIGLECORPS";

	public static final String CODE_EMPLOI_COLKEY = "CODEEMPLOI";


	// Relationships
	public static final String TO_REFERENS_CORPS_KEY = "toReferensCorps";
	public static final String TO_REFERENS_FP_KEY = "toReferensFp";
	public static final String TOS_REFERENS_ACTIVITES_KEY = "tosReferensActivites";
	public static final String TOS_REFERENS_COMPETENCES_KEY = "tosReferensCompetences";



	// Accessors methods
  public String codeMen() {
    return (String) storedValueForKey(CODE_MEN_KEY);
  }

  public void setCodeMen(String value) {
    takeStoredValueForKey(value, CODE_MEN_KEY);
  }

  public String definition() {
    return (String) storedValueForKey(DEFINITION_KEY);
  }

  public void setDefinition(String value) {
    takeStoredValueForKey(value, DEFINITION_KEY);
  }

  public String intitulEmploi() {
    return (String) storedValueForKey(INTITUL_EMPLOI_KEY);
  }

  public void setIntitulEmploi(String value) {
    takeStoredValueForKey(value, INTITUL_EMPLOI_KEY);
  }

  public String lettreBap() {
    return (String) storedValueForKey(LETTRE_BAP_KEY);
  }

  public void setLettreBap(String value) {
    takeStoredValueForKey(value, LETTRE_BAP_KEY);
  }

  public String numBap() {
    return (String) storedValueForKey(NUM_BAP_KEY);
  }

  public void setNumBap(String value) {
    takeStoredValueForKey(value, NUM_BAP_KEY);
  }

  public String numDcp() {
    return (String) storedValueForKey(NUM_DCP_KEY);
  }

  public void setNumDcp(String value) {
    takeStoredValueForKey(value, NUM_DCP_KEY);
  }

  public String numEmploi() {
    return (String) storedValueForKey(NUM_EMPLOI_KEY);
  }

  public void setNumEmploi(String value) {
    takeStoredValueForKey(value, NUM_EMPLOI_KEY);
  }

  public String numFp() {
    return (String) storedValueForKey(NUM_FP_KEY);
  }

  public void setNumFp(String value) {
    takeStoredValueForKey(value, NUM_FP_KEY);
  }

  public String ouvertConcours() {
    return (String) storedValueForKey(OUVERT_CONCOURS_KEY);
  }

  public void setOuvertConcours(String value) {
    takeStoredValueForKey(value, OUVERT_CONCOURS_KEY);
  }

  public String sigleCorps() {
    return (String) storedValueForKey(SIGLE_CORPS_KEY);
  }

  public void setSigleCorps(String value) {
    takeStoredValueForKey(value, SIGLE_CORPS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps toReferensCorps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps)storedValueForKey(TO_REFERENS_CORPS_KEY);
  }

  public void setToReferensCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps oldValue = toReferensCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_CORPS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp toReferensFp() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp)storedValueForKey(TO_REFERENS_FP_KEY);
  }

  public void setToReferensFpRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp oldValue = toReferensFp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_FP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_FP_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> tosReferensActivites() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites>)storedValueForKey(TOS_REFERENS_ACTIVITES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> tosReferensActivites(EOQualifier qualifier) {
    return tosReferensActivites(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> tosReferensActivites(EOQualifier qualifier, boolean fetch) {
    return tosReferensActivites(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> tosReferensActivites(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites.TO_REFERENS_EMPLOIS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosReferensActivites();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosReferensActivitesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REFERENS_ACTIVITES_KEY);
  }

  public void removeFromTosReferensActivitesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_ACTIVITES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites createTosReferensActivitesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_ReferensActivites");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REFERENS_ACTIVITES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites) eo;
  }

  public void deleteTosReferensActivitesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_ACTIVITES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosReferensActivitesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> objects = tosReferensActivites().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosReferensActivitesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> tosReferensCompetences() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>)storedValueForKey(TOS_REFERENS_COMPETENCES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> tosReferensCompetences(EOQualifier qualifier) {
    return tosReferensCompetences(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> tosReferensCompetences(EOQualifier qualifier, boolean fetch) {
    return tosReferensCompetences(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> tosReferensCompetences(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences.TO_REFERENS_EMPLOIS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosReferensCompetences();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosReferensCompetencesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REFERENS_COMPETENCES_KEY);
  }

  public void removeFromTosReferensCompetencesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_COMPETENCES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences createTosReferensCompetencesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_ReferensCompetences");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REFERENS_COMPETENCES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences) eo;
  }

  public void deleteTosReferensCompetencesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_COMPETENCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosReferensCompetencesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> objects = tosReferensCompetences().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosReferensCompetencesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOReferensEmplois avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOReferensEmplois createEOReferensEmplois(EOEditingContext editingContext, String definition
			) {
    EOReferensEmplois eo = (EOReferensEmplois) createAndInsertInstance(editingContext, _EOReferensEmplois.ENTITY_NAME);    
		eo.setDefinition(definition);
    return eo;
  }

  
	  public EOReferensEmplois localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReferensEmplois)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensEmplois creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensEmplois creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOReferensEmplois object = (EOReferensEmplois)createAndInsertInstance(editingContext, _EOReferensEmplois.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOReferensEmplois localInstanceIn(EOEditingContext editingContext, EOReferensEmplois eo) {
    EOReferensEmplois localInstance = (eo == null) ? null : (EOReferensEmplois)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOReferensEmplois#localInstanceIn a la place.
   */
	public static EOReferensEmplois localInstanceOf(EOEditingContext editingContext, EOReferensEmplois eo) {
		return EOReferensEmplois.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReferensEmplois fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReferensEmplois fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReferensEmplois> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReferensEmplois eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReferensEmplois)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReferensEmplois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReferensEmplois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReferensEmplois> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReferensEmplois eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReferensEmplois)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReferensEmplois fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReferensEmplois eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReferensEmplois ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReferensEmplois fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
