/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartAssociation.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EORepartAssociation extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EORepartAssociation.class);

	public static final String ENTITY_NAME = "Fwkpers_RepartAssociation";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REPART_ASSOCIATION";


// Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> RAS_COMMENTAIRE = new ERXKey<String>("rasCommentaire");
  public static final ERXKey<NSTimestamp> RAS_D_FERMETURE = new ERXKey<NSTimestamp>("rasDFermeture");
  public static final ERXKey<NSTimestamp> RAS_D_OUVERTURE = new ERXKey<NSTimestamp>("rasDOuverture");
  public static final ERXKey<java.math.BigDecimal> RAS_QUOTITE = new ERXKey<java.math.BigDecimal>("rasQuotite");
  public static final ERXKey<Integer> RAS_RANG = new ERXKey<Integer>("rasRang");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> TO_ASSOCIATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation>("toAssociation");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDUS_ASSOCIES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividusAssocies");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURES_ASSOCIEES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructuresAssociees");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rasId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String RAS_COMMENTAIRE_KEY = "rasCommentaire";
	public static final String RAS_D_FERMETURE_KEY = "rasDFermeture";
	public static final String RAS_D_OUVERTURE_KEY = "rasDOuverture";
	public static final String RAS_QUOTITE_KEY = "rasQuotite";
	public static final String RAS_RANG_KEY = "rasRang";

// Attributs non visibles
	public static final String RAS_ID_KEY = "rasId";
	public static final String ASS_ID_KEY = "assId";
	public static final String C_STRUCTURE_KEY = "cStructure";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String RAS_COMMENTAIRE_COLKEY = "RAS_COMMENTAIRE";
	public static final String RAS_D_FERMETURE_COLKEY = "RAS_D_FERMETURE";
	public static final String RAS_D_OUVERTURE_COLKEY = "RAS_D_OUVERTURE";
	public static final String RAS_QUOTITE_COLKEY = "RAS_QUOTITE";
	public static final String RAS_RANG_COLKEY = "RAS_RANG";

	public static final String RAS_ID_COLKEY = "RAS_ID";
	public static final String ASS_ID_COLKEY = "ASS_ID";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";


	// Relationships
	public static final String TO_ASSOCIATION_KEY = "toAssociation";
	public static final String TO_INDIVIDUS_ASSOCIES_KEY = "toIndividusAssocies";
	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TO_STRUCTURES_ASSOCIEES_KEY = "toStructuresAssociees";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String rasCommentaire() {
    return (String) storedValueForKey(RAS_COMMENTAIRE_KEY);
  }

  public void setRasCommentaire(String value) {
    takeStoredValueForKey(value, RAS_COMMENTAIRE_KEY);
  }

  public NSTimestamp rasDFermeture() {
    return (NSTimestamp) storedValueForKey(RAS_D_FERMETURE_KEY);
  }

  public void setRasDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, RAS_D_FERMETURE_KEY);
  }

  public NSTimestamp rasDOuverture() {
    return (NSTimestamp) storedValueForKey(RAS_D_OUVERTURE_KEY);
  }

  public void setRasDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, RAS_D_OUVERTURE_KEY);
  }

  public java.math.BigDecimal rasQuotite() {
    return (java.math.BigDecimal) storedValueForKey(RAS_QUOTITE_KEY);
  }

  public void setRasQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RAS_QUOTITE_KEY);
  }

  public Integer rasRang() {
    return (Integer) storedValueForKey(RAS_RANG_KEY);
  }

  public void setRasRang(Integer value) {
    takeStoredValueForKey(value, RAS_RANG_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAssociation toAssociation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAssociation)storedValueForKey(TO_ASSOCIATION_KEY);
  }

  public void setToAssociationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAssociation value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAssociation oldValue = toAssociation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ASSOCIATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ASSOCIATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividusAssocies() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)storedValueForKey(TO_INDIVIDUS_ASSOCIES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividusAssocies(EOQualifier qualifier) {
    return toIndividusAssocies(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividusAssocies(EOQualifier qualifier, boolean fetch) {
    return toIndividusAssocies(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividusAssocies(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu.TO_REPART_ASSOCIATIONS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOIndividu.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toIndividusAssocies();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToIndividusAssociesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_ASSOCIES_KEY);
  }

  public void removeFromToIndividusAssociesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_ASSOCIES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createToIndividusAssociesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_INDIVIDUS_ASSOCIES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteToIndividusAssociesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_ASSOCIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToIndividusAssociesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> objects = toIndividusAssocies().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToIndividusAssociesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructuresAssociees() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)storedValueForKey(TO_STRUCTURES_ASSOCIEES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructuresAssociees(EOQualifier qualifier) {
    return toStructuresAssociees(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructuresAssociees(EOQualifier qualifier, boolean fetch) {
    return toStructuresAssociees(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructuresAssociees(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOStructure.TO_REPART_ASSOCIATIONS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toStructuresAssociees();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToStructuresAssocieesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_ASSOCIEES_KEY);
  }

  public void removeFromToStructuresAssocieesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_ASSOCIEES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createToStructuresAssocieesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_STRUCTURES_ASSOCIEES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteToStructuresAssocieesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_ASSOCIEES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToStructuresAssocieesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> objects = toStructuresAssociees().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToStructuresAssocieesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORepartAssociation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORepartAssociation createEORepartAssociation(EOEditingContext editingContext, Integer persId
, Integer rasRang
, org.cocktail.fwkcktlpersonne.common.metier.EOAssociation toAssociation, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure			) {
    EORepartAssociation eo = (EORepartAssociation) createAndInsertInstance(editingContext, _EORepartAssociation.ENTITY_NAME);    
		eo.setPersId(persId);
		eo.setRasRang(rasRang);
    eo.setToAssociationRelationship(toAssociation);
    eo.setToStructureRelationship(toStructure);
    return eo;
  }

  
	  public EORepartAssociation localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartAssociation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartAssociation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartAssociation creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORepartAssociation object = (EORepartAssociation)createAndInsertInstance(editingContext, _EORepartAssociation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartAssociation localInstanceIn(EOEditingContext editingContext, EORepartAssociation eo) {
    EORepartAssociation localInstance = (eo == null) ? null : (EORepartAssociation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORepartAssociation#localInstanceIn a la place.
   */
	public static EORepartAssociation localInstanceOf(EOEditingContext editingContext, EORepartAssociation eo) {
		return EORepartAssociation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORepartAssociation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartAssociation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORepartAssociation> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartAssociation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartAssociation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartAssociation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartAssociation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORepartAssociation> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartAssociation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartAssociation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORepartAssociation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartAssociation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartAssociation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartAssociation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
