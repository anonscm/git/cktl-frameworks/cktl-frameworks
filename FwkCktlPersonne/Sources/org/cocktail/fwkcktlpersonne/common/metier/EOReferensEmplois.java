/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IReferensEmplois;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOReferensEmplois extends _EOReferensEmplois implements IReferensEmplois {

    public EOReferensEmplois() {
        super();
    }

    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     * @throws une exception de validation
     */
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
    
 // METHODES RAJOUTEES

	public static final String DISPLAY_KEY = "display";

	/**
	 * 
	 * @return true si ce code MEN est archivé
	 */
	public boolean isArchive() {
		boolean isArchive = false;

		if (toReferensFp() == null
				|| toReferensFp().toReferensDcp() == null
				|| toReferensFp().toReferensDcp().isArchive()) {
			isArchive = true;
		}

		return isArchive;
	}

	/**
     * Affichage du code MEN avec prise en compte de l'archivage
     * @return une String avec un libellé du code MEN
     */
	public String display() {
		String display = StringCtrl.capitalizeWords(intitulEmploi());
		if (codeMen() != null) {
			display += " (" + codeMen() + ")";
		}
		if (isArchive()) {
			display = "___" + display + " (ancienne nomenclature)";
		}

		return display;
	}

	/**
     * 
     * @return une String pour l'affichage du libellé long
     */
	public String displayLong() {
		return display();
	}

	@Override
	public NSArray tosReferensActivites() {
		// classement alphabetique
		NSArray tosReferensActivites = super.tosReferensActivites();
		return CktlSort.sortedArray(tosReferensActivites, EOReferensActivites.INTITUL_ACTIVITE_KEY);
	}

	@Override
	public NSArray tosReferensCompetences() {
		// classement alphabetique
		NSArray tosReferensCompetences = super.tosReferensCompetences();
		return CktlSort.sortedArray(tosReferensCompetences, EOReferensCompetences.INTITUL_COMP_KEY);
	}
}
