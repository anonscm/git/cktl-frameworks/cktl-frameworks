/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Un groupe dynamique permet de référencer un ensemble de personnes (en fait de personnes pour l'instant)
 * selon des règles.
 * Exemple : Un groupe dynamique référençant l'ensemble des personnes ayant pour rôle "CONTACT"...
 * Pour des raisons de performance, ces groupes sont calculés et enregistrés dans une table de jointure.
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class EOGroupeDynamique extends _EOGroupeDynamique implements HasRegle {

	public static final Logger LOG = Logger.getLogger(EOGroupeDynamique.class);

	public EOGroupeDynamique() {
		super();
	}

	/**
	 * @return la liste des persId correspondant à la règle.
	 */
	@SuppressWarnings("unchecked")
	public NSArray<Integer> calculerPersIds() {
		if (toRegle() != null) {
			EOQualifier qualifier = toRegle().qualifier();
			EOFetchSpecification fspec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qualifier, null);
			fspec.setFetchesRawRows(true);
			NSArray<NSDictionary<String, Object>> rawRows = editingContext().objectsWithFetchSpecification(fspec);
			NSArray<Integer> persIds = (NSArray<Integer>) rawRows.valueForKey(EOIndividu.PERS_ID_KEY);
			return ERXArrayUtilities.distinct(persIds);
		}
		return NSArray.EmptyArray;
	}

	/**
	 * Suppression des enregistrements de la table de jointure
	 */
	private void deleteGrpDynaIndividuRows() {
		Object grpIdPk = ERXEOControlUtilities.primaryKeyObjectForObject(this);
		EOQualifier qual = ERXQ.equals(EOGroupeDynamiquePersonne.GRPD_ID_KEY, grpIdPk);
		ERXEOAccessUtilities.deleteRowsDescribedByQualifier(
		        editingContext(), EOGroupeDynamiquePersonne.ENTITY_NAME, qual);
	}

	/**
	 * Insertion des enregistrements de la table de jointure
	 */
	private void insertGrpDynaIndividuRows() {
		NSArray<Integer> persIds = calculerPersIds();
		NSMutableArray<NSDictionary<String, Object>> rows = new NSMutableArray<NSDictionary<String, Object>>();
		Object grpIdPk = ERXEOControlUtilities.primaryKeyObjectForObject(this);
		for (Integer i : persIds) {
			NSMutableDictionary<String, Object> row = new NSMutableDictionary<String, Object>();
			row.setObjectForKey(i, EOGroupeDynamiquePersonne.PERS_ID_KEY);
			row.setObjectForKey(grpIdPk, EOGroupeDynamiquePersonne.GRPD_ID_KEY);
			rows.addObject(row);
		}
		ERXEOAccessUtilities.insertRows(editingContext(), EOGroupeDynamiquePersonne.ENTITY_NAME, rows);
	}

	/**
	 * (Re)Calcul du groupe dynamique : suppression et insertion des enregistrements de la table de jointure
	 */
	public void calculerGroupeDynamique() {
		if (ERXEOControlUtilities.isNewObject(this)) {
			throw new IllegalStateException("Le groupe dynamique doit être sauvegardé");
		}
		deleteGrpDynaIndividuRows();
		insertGrpDynaIndividuRows();
	}

	@Override
	public NSArray toGroupeDynamiquePersonnes() {
		LOG.warn("Pour des raisons de performance, vous ne devriez pas manipuler cette relation");
		return super.toGroupeDynamiquePersonnes();
	}

	@Override
	public NSArray toGroupeDynamiquePersonnes(EOQualifier qualifier) {
		LOG.warn("Pour des raisons de performance, vous ne devriez pas manipuler cette relation");
		return super.toGroupeDynamiquePersonnes(qualifier);
	}

	@Override
	public NSArray toGroupeDynamiquePersonnes(EOQualifier qualifier, NSArray sortOrderings) {
		LOG.warn("Pour des raisons de performance, vous ne devriez pas manipuler cette relation");
		return super.toGroupeDynamiquePersonnes(qualifier, sortOrderings);
	}

	@Override
	public void willDelete() throws ValidationException {
		super.willDelete();
		deleteGrpDynaIndividuRows();
	}

}
