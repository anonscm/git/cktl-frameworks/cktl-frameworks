/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReferensCorps.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOReferensCorps extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOReferensCorps.class);

	public static final String ENTITY_NAME = "Fwkpers_ReferensCorps";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REFERENS_CORPS";


// Attribute Keys
  public static final ERXKey<String> CATEGORIE = new ERXKey<String>("categorie");
  public static final ERXKey<String> CODE_CORPS = new ERXKey<String>("codeCorps");
  public static final ERXKey<String> COD_GRADE_E_CORPS = new ERXKey<String>("codGradeECorps");
  public static final ERXKey<String> FILIERE = new ERXKey<String>("filiere");
  public static final ERXKey<String> INTITUL_CORPS = new ERXKey<String>("intitulCorps");
  public static final ERXKey<String> SIGLE_CORPS = new ERXKey<String>("sigleCorps");
  public static final ERXKey<String> STATUT = new ERXKey<String>("statut");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TOS_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("tosReferensEmplois");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "sigleCorps";

	public static final String CATEGORIE_KEY = "categorie";
	public static final String CODE_CORPS_KEY = "codeCorps";
	public static final String COD_GRADE_E_CORPS_KEY = "codGradeECorps";
	public static final String FILIERE_KEY = "filiere";
	public static final String INTITUL_CORPS_KEY = "intitulCorps";
	public static final String SIGLE_CORPS_KEY = "sigleCorps";
	public static final String STATUT_KEY = "statut";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String CATEGORIE_COLKEY = "CATEGORIE";
	public static final String CODE_CORPS_COLKEY = "CODECORPS";
	public static final String COD_GRADE_E_CORPS_COLKEY = "CODGRADEECORPS";
	public static final String FILIERE_COLKEY = "FILIERE";
	public static final String INTITUL_CORPS_COLKEY = "INTITULCORPS";
	public static final String SIGLE_CORPS_COLKEY = "SIGLECORPS";
	public static final String STATUT_COLKEY = "STATUT";



	// Relationships
	public static final String TOS_REFERENS_EMPLOIS_KEY = "tosReferensEmplois";



	// Accessors methods
  public String categorie() {
    return (String) storedValueForKey(CATEGORIE_KEY);
  }

  public void setCategorie(String value) {
    takeStoredValueForKey(value, CATEGORIE_KEY);
  }

  public String codeCorps() {
    return (String) storedValueForKey(CODE_CORPS_KEY);
  }

  public void setCodeCorps(String value) {
    takeStoredValueForKey(value, CODE_CORPS_KEY);
  }

  public String codGradeECorps() {
    return (String) storedValueForKey(COD_GRADE_E_CORPS_KEY);
  }

  public void setCodGradeECorps(String value) {
    takeStoredValueForKey(value, COD_GRADE_E_CORPS_KEY);
  }

  public String filiere() {
    return (String) storedValueForKey(FILIERE_KEY);
  }

  public void setFiliere(String value) {
    takeStoredValueForKey(value, FILIERE_KEY);
  }

  public String intitulCorps() {
    return (String) storedValueForKey(INTITUL_CORPS_KEY);
  }

  public void setIntitulCorps(String value) {
    takeStoredValueForKey(value, INTITUL_CORPS_KEY);
  }

  public String sigleCorps() {
    return (String) storedValueForKey(SIGLE_CORPS_KEY);
  }

  public void setSigleCorps(String value) {
    takeStoredValueForKey(value, SIGLE_CORPS_KEY);
  }

  public String statut() {
    return (String) storedValueForKey(STATUT_KEY);
  }

  public void setStatut(String value) {
    takeStoredValueForKey(value, STATUT_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> tosReferensEmplois() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>)storedValueForKey(TOS_REFERENS_EMPLOIS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> tosReferensEmplois(EOQualifier qualifier) {
    return tosReferensEmplois(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> tosReferensEmplois(EOQualifier qualifier, boolean fetch) {
    return tosReferensEmplois(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> tosReferensEmplois(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois.TO_REFERENS_CORPS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosReferensEmplois();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REFERENS_EMPLOIS_KEY);
  }

  public void removeFromTosReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_EMPLOIS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois createTosReferensEmploisRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_ReferensEmplois");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REFERENS_EMPLOIS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois) eo;
  }

  public void deleteTosReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_EMPLOIS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosReferensEmploisRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> objects = tosReferensEmplois().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosReferensEmploisRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOReferensCorps avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOReferensCorps createEOReferensCorps(EOEditingContext editingContext, String sigleCorps
			) {
    EOReferensCorps eo = (EOReferensCorps) createAndInsertInstance(editingContext, _EOReferensCorps.ENTITY_NAME);    
		eo.setSigleCorps(sigleCorps);
    return eo;
  }

  
	  public EOReferensCorps localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReferensCorps)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensCorps creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensCorps creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOReferensCorps object = (EOReferensCorps)createAndInsertInstance(editingContext, _EOReferensCorps.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOReferensCorps localInstanceIn(EOEditingContext editingContext, EOReferensCorps eo) {
    EOReferensCorps localInstance = (eo == null) ? null : (EOReferensCorps)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOReferensCorps#localInstanceIn a la place.
   */
	public static EOReferensCorps localInstanceOf(EOEditingContext editingContext, EOReferensCorps eo) {
		return EOReferensCorps.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCorps>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReferensCorps fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReferensCorps fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReferensCorps> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReferensCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReferensCorps)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReferensCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReferensCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReferensCorps> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReferensCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReferensCorps)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReferensCorps fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReferensCorps eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReferensCorps ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReferensCorps fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
