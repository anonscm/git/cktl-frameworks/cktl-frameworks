/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcoleDoctorale.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOEcoleDoctorale extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOEcoleDoctorale.class);

	public static final String ENTITY_NAME = "Fwkpers_EcoleDoctorale";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ECOLE_DOCTORALE";


// Attribute Keys
  public static final ERXKey<String> LL_ECOLE_DOCTORALE = new ERXKey<String>("llEcoleDoctorale");
  public static final ERXKey<Integer> NO_ECOLE_DOCTORALE = new ERXKey<Integer>("noEcoleDoctorale");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> TO_REPART_ECOLE_DOCTORALES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale>("toRepartEcoleDoctorales");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idEcoleDoctorale";

	public static final String LL_ECOLE_DOCTORALE_KEY = "llEcoleDoctorale";
	public static final String NO_ECOLE_DOCTORALE_KEY = "noEcoleDoctorale";

// Attributs non visibles
	public static final String ID_ECOLE_DOCTORALE_KEY = "idEcoleDoctorale";

//Colonnes dans la base de donnees
	public static final String LL_ECOLE_DOCTORALE_COLKEY = "LL_ECOLE_DOCTORALE";
	public static final String NO_ECOLE_DOCTORALE_COLKEY = "NO_ECOLE_DOCTORALE";

	public static final String ID_ECOLE_DOCTORALE_COLKEY = "ID_ECOLE_DOCTORALE";


	// Relationships
	public static final String TO_REPART_ECOLE_DOCTORALES_KEY = "toRepartEcoleDoctorales";



	// Accessors methods
  public String llEcoleDoctorale() {
    return (String) storedValueForKey(LL_ECOLE_DOCTORALE_KEY);
  }

  public void setLlEcoleDoctorale(String value) {
    takeStoredValueForKey(value, LL_ECOLE_DOCTORALE_KEY);
  }

  public Integer noEcoleDoctorale() {
    return (Integer) storedValueForKey(NO_ECOLE_DOCTORALE_KEY);
  }

  public void setNoEcoleDoctorale(Integer value) {
    takeStoredValueForKey(value, NO_ECOLE_DOCTORALE_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> toRepartEcoleDoctorales() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale>)storedValueForKey(TO_REPART_ECOLE_DOCTORALES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> toRepartEcoleDoctorales(EOQualifier qualifier) {
    return toRepartEcoleDoctorales(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> toRepartEcoleDoctorales(EOQualifier qualifier, boolean fetch) {
    return toRepartEcoleDoctorales(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> toRepartEcoleDoctorales(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale.TO_ECOLE_DOCTORALE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartEcoleDoctorales();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartEcoleDoctoralesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_ECOLE_DOCTORALES_KEY);
  }

  public void removeFromToRepartEcoleDoctoralesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ECOLE_DOCTORALES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale createToRepartEcoleDoctoralesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartEcoleDoctorale");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_ECOLE_DOCTORALES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale) eo;
  }

  public void deleteToRepartEcoleDoctoralesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ECOLE_DOCTORALES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartEcoleDoctoralesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartEcoleDoctorale> objects = toRepartEcoleDoctorales().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartEcoleDoctoralesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEcoleDoctorale avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEcoleDoctorale createEOEcoleDoctorale(EOEditingContext editingContext, String llEcoleDoctorale
, Integer noEcoleDoctorale
			) {
    EOEcoleDoctorale eo = (EOEcoleDoctorale) createAndInsertInstance(editingContext, _EOEcoleDoctorale.ENTITY_NAME);    
		eo.setLlEcoleDoctorale(llEcoleDoctorale);
		eo.setNoEcoleDoctorale(noEcoleDoctorale);
    return eo;
  }

  
	  public EOEcoleDoctorale localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEcoleDoctorale)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcoleDoctorale creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcoleDoctorale creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOEcoleDoctorale object = (EOEcoleDoctorale)createAndInsertInstance(editingContext, _EOEcoleDoctorale.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEcoleDoctorale localInstanceIn(EOEditingContext editingContext, EOEcoleDoctorale eo) {
    EOEcoleDoctorale localInstance = (eo == null) ? null : (EOEcoleDoctorale)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEcoleDoctorale#localInstanceIn a la place.
   */
	public static EOEcoleDoctorale localInstanceOf(EOEditingContext editingContext, EOEcoleDoctorale eo) {
		return EOEcoleDoctorale.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEcoleDoctorale fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEcoleDoctorale fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEcoleDoctorale> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcoleDoctorale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcoleDoctorale)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcoleDoctorale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcoleDoctorale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEcoleDoctorale> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcoleDoctorale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcoleDoctorale)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEcoleDoctorale fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcoleDoctorale eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcoleDoctorale ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcoleDoctorale fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
