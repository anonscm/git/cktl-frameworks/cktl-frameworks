package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IVlans;

/**
 *  compte d'un utilisateur
 */
public interface ICompte {
	
	/**
	 * @return La date de fin de validité du compte (ou <code>null</code> si pas de date de finde validité)
	 */
	Date cptFinValide();

	/**
	 * @param value Une date de fin de validité du compte (ou <code>null</code> pour "sans fin de validité")
	 */
	void setCptFinValide(Date value);

	/**
	 * @return Vrai si le temoin de validite du compte informatique est vrai
	 * @see EOCompte#cptValide()
	 */
	boolean isCompteValide();
	
	/**
	 * @param value O/N pour compte valide/invalide
	 */
	void setCptValide(String value);

	/**
	 * @return vlan associé à ce compte
	 */
	IVlans toVlans();

	/**
	 * @return comptes email associé au compte de l'utilisateur
	 */
	List<? extends ICompteEmail> toCompteEmails();
}
