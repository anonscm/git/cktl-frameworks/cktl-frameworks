/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEnfant.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOEnfant extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOEnfant.class);

	public static final String ENTITY_NAME = "Fwkpers_Enfant";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ENFANT";


// Attribute Keys
  public static final ERXKey<NSTimestamp> D_ARRIVEE_FOYER = new ERXKey<NSTimestamp>("dArriveeFoyer");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_DEB_SFT = new ERXKey<NSTimestamp>("dDebSft");
  public static final ERXKey<NSTimestamp> D_DECES = new ERXKey<NSTimestamp>("dDeces");
  public static final ERXKey<NSTimestamp> D_FIN_SFT = new ERXKey<NSTimestamp>("dFinSft");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_NAISSANCE = new ERXKey<NSTimestamp>("dNaissance");
  public static final ERXKey<NSTimestamp> D_SEIZE_ANS = new ERXKey<NSTimestamp>("dSeizeAns");
  public static final ERXKey<NSTimestamp> D_VINGT_ANS = new ERXKey<NSTimestamp>("dVingtAns");
  public static final ERXKey<String> LIEU_NAISSANCE = new ERXKey<String>("lieuNaissance");
  public static final ERXKey<String> NOM = new ERXKey<String>("nom");
  public static final ERXKey<Long> NO_ORDRE_NAISSANCE = new ERXKey<Long>("noOrdreNaissance");
  public static final ERXKey<Long> POURCENT_HANDICAP = new ERXKey<Long>("pourcentHandicap");
  public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
  public static final ERXKey<String> SEXE = new ERXKey<String>("sexe");
  public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant> TO_REPART_ENFANTS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant>("toRepartEnfants");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noEnfant";

	public static final String D_ARRIVEE_FOYER_KEY = "dArriveeFoyer";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_SFT_KEY = "dDebSft";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_FIN_SFT_KEY = "dFinSft";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String D_SEIZE_ANS_KEY = "dSeizeAns";
	public static final String D_VINGT_ANS_KEY = "dVingtAns";
	public static final String LIEU_NAISSANCE_KEY = "lieuNaissance";
	public static final String NOM_KEY = "nom";
	public static final String NO_ORDRE_NAISSANCE_KEY = "noOrdreNaissance";
	public static final String POURCENT_HANDICAP_KEY = "pourcentHandicap";
	public static final String PRENOM_KEY = "prenom";
	public static final String SEXE_KEY = "sexe";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String NO_ENFANT_KEY = "noEnfant";

//Colonnes dans la base de donnees
	public static final String D_ARRIVEE_FOYER_COLKEY = "D_ARRIVEE_FOYER";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_SFT_COLKEY = "D_DEB_SFT";
	public static final String D_DECES_COLKEY = "D_DECES";
	public static final String D_FIN_SFT_COLKEY = "D_FIN_SFT";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String D_SEIZE_ANS_COLKEY = "D_SEIZE_ANS";
	public static final String D_VINGT_ANS_COLKEY = "D_VINGT_ANS";
	public static final String LIEU_NAISSANCE_COLKEY = "LIEU_NAISSANCE";
	public static final String NOM_COLKEY = "NOM";
	public static final String NO_ORDRE_NAISSANCE_COLKEY = "NO_ORDRE_NAISSANCE";
	public static final String POURCENT_HANDICAP_COLKEY = "POURCENT_HANDICAP";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String SEXE_COLKEY = "SEXE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String NO_ENFANT_COLKEY = "NO_ENFANT";


	// Relationships
	public static final String TO_REPART_ENFANTS_KEY = "toRepartEnfants";



	// Accessors methods
  public NSTimestamp dArriveeFoyer() {
    return (NSTimestamp) storedValueForKey(D_ARRIVEE_FOYER_KEY);
  }

  public void setDArriveeFoyer(NSTimestamp value) {
    takeStoredValueForKey(value, D_ARRIVEE_FOYER_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebSft() {
    return (NSTimestamp) storedValueForKey(D_DEB_SFT_KEY);
  }

  public void setDDebSft(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_SFT_KEY);
  }

  public NSTimestamp dDeces() {
    return (NSTimestamp) storedValueForKey(D_DECES_KEY);
  }

  public void setDDeces(NSTimestamp value) {
    takeStoredValueForKey(value, D_DECES_KEY);
  }

  public NSTimestamp dFinSft() {
    return (NSTimestamp) storedValueForKey(D_FIN_SFT_KEY);
  }

  public void setDFinSft(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_SFT_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey(D_NAISSANCE_KEY);
  }

  public void setDNaissance(NSTimestamp value) {
    takeStoredValueForKey(value, D_NAISSANCE_KEY);
  }

  public NSTimestamp dSeizeAns() {
    return (NSTimestamp) storedValueForKey(D_SEIZE_ANS_KEY);
  }

  public void setDSeizeAns(NSTimestamp value) {
    takeStoredValueForKey(value, D_SEIZE_ANS_KEY);
  }

  public NSTimestamp dVingtAns() {
    return (NSTimestamp) storedValueForKey(D_VINGT_ANS_KEY);
  }

  public void setDVingtAns(NSTimestamp value) {
    takeStoredValueForKey(value, D_VINGT_ANS_KEY);
  }

  public String lieuNaissance() {
    return (String) storedValueForKey(LIEU_NAISSANCE_KEY);
  }

  public void setLieuNaissance(String value) {
    takeStoredValueForKey(value, LIEU_NAISSANCE_KEY);
  }

  public String nom() {
    return (String) storedValueForKey(NOM_KEY);
  }

  public void setNom(String value) {
    takeStoredValueForKey(value, NOM_KEY);
  }

  public Long noOrdreNaissance() {
    return (Long) storedValueForKey(NO_ORDRE_NAISSANCE_KEY);
  }

  public void setNoOrdreNaissance(Long value) {
    takeStoredValueForKey(value, NO_ORDRE_NAISSANCE_KEY);
  }

  public Long pourcentHandicap() {
    return (Long) storedValueForKey(POURCENT_HANDICAP_KEY);
  }

  public void setPourcentHandicap(Long value) {
    takeStoredValueForKey(value, POURCENT_HANDICAP_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String sexe() {
    return (String) storedValueForKey(SEXE_KEY);
  }

  public void setSexe(String value) {
    takeStoredValueForKey(value, SEXE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant> toRepartEnfants() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant>)storedValueForKey(TO_REPART_ENFANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant> toRepartEnfants(EOQualifier qualifier) {
    return toRepartEnfants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant> toRepartEnfants(EOQualifier qualifier, boolean fetch) {
    return toRepartEnfants(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant> toRepartEnfants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant.TO_ENFANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartEnfants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_ENFANTS_KEY);
  }

  public void removeFromToRepartEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ENFANTS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant createToRepartEnfantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartEnfant");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_ENFANTS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant) eo;
  }

  public void deleteToRepartEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ENFANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartEnfantsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant> objects = toRepartEnfants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartEnfantsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEnfant avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEnfant createEOEnfant(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String nom
, Long noOrdreNaissance
, String prenom
, String temValide
			) {
    EOEnfant eo = (EOEnfant) createAndInsertInstance(editingContext, _EOEnfant.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNom(nom);
		eo.setNoOrdreNaissance(noOrdreNaissance);
		eo.setPrenom(prenom);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOEnfant localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEnfant)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEnfant creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEnfant creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOEnfant object = (EOEnfant)createAndInsertInstance(editingContext, _EOEnfant.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEnfant localInstanceIn(EOEditingContext editingContext, EOEnfant eo) {
    EOEnfant localInstance = (eo == null) ? null : (EOEnfant)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEnfant#localInstanceIn a la place.
   */
	public static EOEnfant localInstanceOf(EOEditingContext editingContext, EOEnfant eo) {
		return EOEnfant.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEnfant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEnfant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEnfant> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEnfant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEnfant)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEnfant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEnfant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEnfant> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEnfant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEnfant)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEnfant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEnfant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEnfant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEnfant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
