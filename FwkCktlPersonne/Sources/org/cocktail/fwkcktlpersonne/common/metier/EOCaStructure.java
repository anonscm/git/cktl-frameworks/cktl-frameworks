/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Enumeration;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOCaStructure extends _EOCaStructure {
	public static final EOSortOrdering SORT_CAST_ANNEE_DESC = EOSortOrdering.sortOrderingWithKey(EOCaStructure.CAST_ANNEE_KEY, EOSortOrdering.CompareDescending);

	public EOCaStructure() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		setDModification(MyDateCtrl.now());

		try {
			trimAllString();
			checkContraintesObligatoires();
			checkContraintesLongueursMax();

			//Verifier que la meme annee n'est pas saisie en double
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOCaStructure.CAST_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, castAnnee()));
			EOQualifier qual = new EOAndQualifier(quals);
			NSArray res = toStructure().toCaStructures(qual, false);
			if (res.count() > 0) {
				Enumeration en = res.objectEnumerator();
				while (en.hasMoreElements()) {
					EOCaStructure object = (EOCaStructure) en.nextElement();
					if (!object.globalID().equals(globalID())) {
						throw new NSValidation.ValidationException("Vous ne pouvez pas definir plusieurs fois le CA pour l'annee " + castAnnee());
					}
				}
			}
			super.validateObjectMetier();
		} catch (NSValidation.ValidationException e) {
			System.err.println(this);
			throw e;
		}
		super.validateObjectMetier();
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDCreation(MyDateCtrl.now());
	}
}
