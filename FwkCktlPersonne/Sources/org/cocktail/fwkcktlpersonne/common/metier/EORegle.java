/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.validation.ERXValidationFactory;

public class EORegle extends _EORegle {

    public EORegle() {
        super();
    }

    @Override
    public String toString() {
        // Si de type simple on affiche 
        if (isRegleSimple()) {
            String value = rValue() == null ? "-" : rValue();
            if (hasOperateurRoleDansGroupe()) {
                value = value + (rValue2() == null ? ", -" : "," + rValue2());
            }
            return toRegleKey().rkLc() + " " + toRegleOperateur().roLc() + " \"" + value + "\""; 
        } else {
            if (isRegleNot()) {
                return toRegleNode().rnNode() + "(" + toReglesEnfantsOrdered().lastObject() +")";
            }
            return "(" + toReglesEnfantsOrdered().componentsJoinedByString(") " + toRegleNode().rnNode() + " (") + ")";
        }
    }

    /**
     * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
     * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {

        trimAllString();
        setDateModification(MyDateCtrl.now());

        try {
            checkContraintesObligatoires();
            checkContraintesLongueursMax();

            super.validateObjectMetier();
        } catch (Exception e) {
            NSValidation.ValidationException e1 = new ValidationException("Erreur lors de la validation de la règle '" + toDisplayString() + "' : " + e.getMessage());
            e1.initCause(e);
            throw e1;
        }
    }

    /**
     * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
     * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();
        if (isRegleSimple() && rValue() == null)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "SimpleRegleRValueNull");
        if (hasOperateurRoleDansGroupe() && rValue2() == null)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "SimpleRegleRValue2Null");
    }

    @Override
    public void awakeFromInsertion(EOEditingContext ec) {
        super.awakeFromInsertion(ec);
        setDateCreation(MyDateCtrl.now());
    }

    public String toDisplayString() {
        return toRegleKey() + " " + toRegleOperateur() + " " + rValue();
    }

    /**
     * Verifie si les utilisateurs affectes a persIdCreation et persIdModification ont bien le droits de creer/Modifier l'entité.
     * 
     * @throws NSValidation.ValidationException
     */
    public void checkUsers() throws NSValidation.ValidationException {
        try {
            if (persIdModification() != null && persIdCreation() == null) {
                setPersIdCreation(persIdModification());
            }

            if (getCreateur() == null) {
                throw new NSValidation.ValidationException("La reference au createur (persIdCreation) est obligatoire.");
            }
            if (getModificateur() == null) {
                throw new NSValidation.ValidationException("La reference au modificateur (persIdModification) est obligatoire.");
            }

        } catch (Exception e) {
            NSValidation.ValidationException e1 = new NSValidation.ValidationException(this.getClass().getSimpleName() + " : " + toDisplayString() + ":" + e.getLocalizedMessage());
            throw e1;
        }
    }
    
    public EOGroupeDynamique groupeDynamique() {
        return (EOGroupeDynamique) toGroupeDynamiques().lastObject();
    }
    
    /**
     * @return la liste des règles filles ordonnées par date de création ascendante
     */
    @SuppressWarnings("unchecked")
    public NSArray<EORegle> toReglesEnfantsOrdered() {
        return toReglesEnfants(null, ERXS.asc(DATE_CREATION_KEY).array(), false);
    }
    
    /**
     * @return true si la règle est une règle simple et a un opérateur de type "Role dans groupe"
     */
    public boolean hasOperateurRoleDansGroupe() {
        return isRegleSimple() && toRegleOperateur() != null && toRegleOperateur().isOperateurRoleDansGroupe();
    }
    
    /**
     * @return true si la règle est une règle simple et a un opérateur de type "Membre de "
     */
    public boolean hasOperateurMembre() {
        return isRegleSimple() && toRegleOperateur() != null && toRegleOperateur().isOperateurMembre();
    }
    
    /**
     * @return true si la règle est une règle simple et a un opérateur de type "Role "
     */
    public boolean hasOperateurRole() {
        return isRegleSimple() && toRegleOperateur() != null && toRegleOperateur().isOperateurRole();
    }
    
    /**
     * @return true si la règle est une règle simple (ie pas destiné à avoir des règles filles)
     */
    public boolean isRegleSimple() {
        return toRegleNode().isNodeSimple();
    }
    
    /**
     * @return true si la règle a une node de type AND
     */
    public boolean isRegleAnd() {
        return toRegleNode().isNodeAnd();
    }
    
    /**
     * @return true si la règle a une node de type OR
     */
    public boolean isRegleOr() {
        return toRegleNode().isNodeOr();
    }
    
    /**
     * @return true si la règle a une node de type NOT
     */
    public boolean isRegleNot() {
        return toRegleNode().isNodeNot();
    }
    
    public void setGroupeDynamiqueIfRoot(EOGroupeDynamique groupe) {
        if (toReglePere() == null && groupe != null)
            addToToGroupeDynamiquesRelationship(groupe);
    }
    
    /**
     * @param regle la règle pourlaquelle on rajoute un AND
     * @param persId le persId de la personne éditant la règle
     * @return la règle AND reliant <code>regle</code> à this
     */
    public EORegle and(EORegle regle, Integer persId) {
        // on regarde d'abord si this ne comporte pas déjà une règle AND en parent.
        // si oui on l'utilise plutôt qu'en créer une autre pour éviter les AND(AND(..))
        if (this.toReglePere() != null && this.toReglePere().isRegleAnd()) {
            this.toReglePere().addToToReglesEnfantsRelationship(regle);
            return toReglePere();
        } else {
            EORegle regleAnd = EORegle.creerInstance(regle.editingContext());
            regleAnd.setToRegleNodeRelationship(
                    ERXEOControlUtilities.localInstanceOfObject(editingContext(), EORegleNode.getRegleNodeAnd()));
            regleAnd.setPersIdCreation(persId);
            regleAnd.setPersIdModification(persId);
            regleAnd.setToReglePereRelationship(this.toReglePere());
            regle.setToReglePereRelationship(regleAnd);
            this.setToReglePereRelationship(regleAnd);
            regleAnd.setGroupeDynamiqueIfRoot(this.groupeDynamique());
            return regleAnd;
        }
    }

    /**
     * @param persId le persId de la personne éditant la règle
     * @return la règle AND reliant this à une nouvelle règle simple
     */
    public EORegle andRegleSimple(Integer persId) {
        EORegle regleSimple = creerRegleSimple(editingContext(), persId);
        return this.and(regleSimple, persId);
    }
       
    /**
     * @param regle la règle pourlaquelle on rajoute un OR
     * @param persId le persId de la personne éditant la règle
     * @return la règle OR reliant <code>regle</code> à this
     */
    public EORegle or(EORegle regle, Integer persId) {
        // on regarde d'abord si this ne comporte pas déjà une règle OR en parent.
        // si oui on l'utilise plutôt qu'en créer une autre pour éviter les OR(OR(..))
        if (this.toReglePere() != null && this.toReglePere().isRegleOr()) {
            this.toReglePere().addToToReglesEnfantsRelationship(regle);
            return toReglePere();
        } else {
            EORegle regleOr = EORegle.creerInstance(regle.editingContext());
            regleOr.setToRegleNodeRelationship(
                    ERXEOControlUtilities.localInstanceOfObject(editingContext(),EORegleNode.getRegleNodeOr()));
            regleOr.setPersIdCreation(persId);
            regleOr.setPersIdModification(persId);
            regleOr.setToReglePereRelationship(this.toReglePere());
            regle.setToReglePereRelationship(regleOr);
            this.setToReglePereRelationship(regleOr);
            regleOr.setGroupeDynamiqueIfRoot(this.groupeDynamique());
            return regleOr;
        }
    }
    
    /**
     * @param persId le persId de la personne éditant la règle
     * @return la règle OR reliant this à une nouvelle règle simple
     */
    public EORegle orRegleSimple(Integer persId) {
        EORegle regleSimple = creerRegleSimple(editingContext(), persId);
        return this.or(regleSimple, persId);
    }
    
    /**
     * @param persId le persId de la personne éditant la règle
     * @return la règle NOT encapsulant this
     */
    public EORegle not(Integer persId) {
        EORegle regleNot = EORegle.creerInstance(editingContext());
        regleNot.setToRegleNodeRelationship(
                ERXEOControlUtilities.localInstanceOfObject(editingContext(),EORegleNode.getRegleNodeNot()));
        regleNot.setPersIdCreation(persId);
        regleNot.setPersIdModification(persId);
        regleNot.setToReglePereRelationship(this.toReglePere());
        this.setToReglePereRelationship(regleNot);
        regleNot.setGroupeDynamiqueIfRoot(this.groupeDynamique());
        return regleNot;
    }
    
    @SuppressWarnings("unchecked")
    public EOQualifier qualifier() {
        // Si c'est une règle simple
        if (isRegleSimple()) {
            return toRegleOperateur().qualifier(this);
        } else if (isRegleAnd()) {
            NSArray<EOQualifier> qual = (NSArray<EOQualifier>)toReglesEnfants().valueForKey("qualifier");
            return ERXQ.and(qual);
        } else if (isRegleOr()) {
            NSArray<EOQualifier> qual = (NSArray<EOQualifier>)toReglesEnfants().valueForKey("qualifier");
            return ERXQ.or(qual);
        } else if (isRegleNot()) {
            NSArray<EOQualifier> qual = (NSArray<EOQualifier>)toReglesEnfants().valueForKey("qualifier");
            return ERXQ.not(qual.lastObject());
        }
        return null;
    }
    
    /**
     * @param profil le profil pourlequel on crée une règle simple
     * @param persIdCreation persId le persId de la personne créant la règle
     * @return une nouvelle règle simple sans règle parente, destiné à être renseigné par la suite.
     */
    public static EORegle creerRegleSimple(EOEditingContext ec, Integer persIdCreation) {
        EORegle regle = EORegle.creerInstance(ec);
        regle.setPersIdCreation(persIdCreation);
        regle.setPersIdModification(persIdCreation);
        regle.setToRegleNodeRelationship(
                ERXEOControlUtilities.localInstanceOfObject(ec,EORegleNode.getRegleNodeSimple()));
        regle.setToRegleKeyRelationship(
                ERXEOControlUtilities.localInstanceOfObject(ec,EORegleKey.getRegleKeyUtilisateur()));
        regle.setToRegleOperateurRelationship(
                ERXEOControlUtilities.localInstanceOfObject(ec,EORegleOperateur.getRegleOperateurMembre()));
        return regle;
    }
    
    /**
     * Si la regle est complexe (comporte des fillles ou fils) on parcourt les fils pour les faire pointer
     * si possible vers le parent de la règle à supprimer. Cependant on n'applique pas cela si plus d'une règle simple 
     * va se retrouver sans règle père : une seule règle racine est autorisée.
     * 
     * @param regle la règle à supprimer
     */
    public static void supprimerRegle(EORegle regle) {
        NSArray<EORegle> reglesFilles = regle.toReglesEnfants().immutableClone();
        // Si on a plusieurs filles et que notre regle pere est null on fait rien (pas autorisé d'avoir plus d'une racine)
        if (reglesFilles.count() > 1 && regle.toReglePere() == null) {
            return;
        } else if (reglesFilles.count() == 1 && regle.toReglePere() == null) {
            reglesFilles.lastObject().setToReglePereRelationship(null);
        } else if (reglesFilles.count() > 1 && regle.toReglePere() != null) {
            for (EORegle regleFille : reglesFilles) {
                regleFille.setToReglePereRelationship(regle.toReglePere());
            }
        } 
        regle.setToReglePereRelationship(null);
        regle.editingContext().deleteObject(regle);
    }
    
}
