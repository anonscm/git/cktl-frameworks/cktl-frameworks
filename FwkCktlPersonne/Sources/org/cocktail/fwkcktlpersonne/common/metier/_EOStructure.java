/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStructure.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOStructure extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOStructure.class);

	public static final String ENTITY_NAME = "Fwkpers_Structure";
	public static final String ENTITY_TABLE_NAME = "GRHUM.STRUCTURE_ULR";


// Attribute Keys
  public static final ERXKey<String> C_ACADEMIE = new ERXKey<String>("cAcademie");
  public static final ERXKey<String> C_NAF = new ERXKey<String>("cNaf");
  public static final ERXKey<String> C_RNE = new ERXKey<String>("cRne");
  public static final ERXKey<String> C_STATUT_JURIDIQUE = new ERXKey<String>("cStatutJuridique");
  public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
  public static final ERXKey<String> C_STRUCTURE_PERE = new ERXKey<String>("cStructurePere");
  public static final ERXKey<String> C_TYPE_DECISION_STR = new ERXKey<String>("cTypeDecisionStr");
  public static final ERXKey<String> C_TYPE_ETABLISSEMEN = new ERXKey<String>("cTypeEtablissemen");
  public static final ERXKey<String> C_TYPE_STRUCTURE = new ERXKey<String>("cTypeStructure");
  public static final ERXKey<NSTimestamp> DATE_DECISION = new ERXKey<NSTimestamp>("dateDecision");
  public static final ERXKey<NSTimestamp> DATE_FERMETURE = new ERXKey<NSTimestamp>("dateFermeture");
  public static final ERXKey<NSTimestamp> DATE_OUVERTURE = new ERXKey<NSTimestamp>("dateOuverture");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> GRP_ACCES = new ERXKey<String>("grpAcces");
  public static final ERXKey<String> GRP_ALIAS = new ERXKey<String>("grpAlias");
  public static final ERXKey<String> GRP_APE_CODE = new ERXKey<String>("grpApeCode");
  public static final ERXKey<Integer> GRP_CA = new ERXKey<Integer>("grpCa");
  public static final ERXKey<Integer> GRP_CAPITAL = new ERXKey<Integer>("grpCapital");
  public static final ERXKey<Integer> GRP_EFFECTIFS = new ERXKey<Integer>("grpEffectifs");
  public static final ERXKey<String> GRP_FONCTION1 = new ERXKey<String>("grpFonction1");
  public static final ERXKey<String> GRP_FONCTION2 = new ERXKey<String>("grpFonction2");
  public static final ERXKey<String> GRP_FORME_JURIDIQUE = new ERXKey<String>("grpFormeJuridique");
  public static final ERXKey<String> GRP_MOTS_CLEFS = new ERXKey<String>("grpMotsClefs");
  public static final ERXKey<Integer> GRP_OWNER = new ERXKey<Integer>("grpOwner");
  public static final ERXKey<String> GRP_RESPONSABILITE = new ERXKey<String>("grpResponsabilite");
  public static final ERXKey<Integer> GRP_RESPONSABLE = new ERXKey<Integer>("grpResponsable");
  public static final ERXKey<String> ID_RNSR = new ERXKey<String>("idRnsr");
  public static final ERXKey<String> LC_STRUCTURE = new ERXKey<String>("lcStructure");
  public static final ERXKey<String> LL_STRUCTURE = new ERXKey<String>("llStructure");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> REF_DECISION = new ERXKey<String>("refDecision");
  public static final ERXKey<Integer> ROM_ID = new ERXKey<Integer>("romId");
  public static final ERXKey<Integer> SACT_ID = new ERXKey<Integer>("sactId");
  public static final ERXKey<String> SIREN = new ERXKey<String>("siren");
  public static final ERXKey<String> SIRET = new ERXKey<String>("siret");
  public static final ERXKey<String> STR_ACTIVITE = new ERXKey<String>("strActivite");
  public static final ERXKey<String> STR_AFFICHAGE = new ERXKey<String>("strAffichage");
  public static final ERXKey<String> STR_DESCRIPTION = new ERXKey<String>("strDescription");
  public static final ERXKey<String> STR_PHOTO = new ERXKey<String>("strPhoto");
  public static final ERXKey<String> STR_RECHERCHE = new ERXKey<String>("strRecherche");
  public static final ERXKey<String> STR_STATUT = new ERXKey<String>("strStatut");
  public static final ERXKey<String> TEM_SECTORISE = new ERXKey<String>("temSectorise");
  public static final ERXKey<String> TEM_SIRET_PROVISOIRE = new ERXKey<String>("temSiretProvisoire");
  public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
  public static final ERXKey<String> TVA_INTRACOM = new ERXKey<String>("tvaIntracom");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure> TO_CA_STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure>("toCaStructures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_COMPOSANTE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toComposante");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> TO_COMPTES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>("toComptes");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure> TO_EFFECTIF_STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure>("toEffectifStructures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> TO_FORMES_JURIDIQUES_DETAILS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails>("toFormesJuridiquesDetails");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> TO_FOURNISS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>("toFourniss");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONaf> TO_NAF = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONaf>("toNaf");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_OWNER = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toOwner");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> TO_PERSONNE_ALIASES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>("toPersonneAliases");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> TO_PERSONNE_TELEPHONES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>("toPersonneTelephones");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> TO_REPART_ASSOCIATIONS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>("toRepartAssociations");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> TO_REPART_ASSOCIATIONS_ELTS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>("toRepartAssociationsElts");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> TO_REPART_PERSONNE_ADRESSES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>("toRepartPersonneAdresses");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> TO_REPART_STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>("toRepartStructures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> TO_REPART_STRUCTURES_ELTS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>("toRepartStructuresElts");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe> TO_REPART_TYPE_GROUPES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe>("toRepartTypeGroupes");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_RESPONSABLE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toResponsable");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORome> TO_ROME = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORome>("toRome");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat> TO_SECRETARIATS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat>("toSecretariats");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSecteurActivite> TO_SECTEUR_ACTIVITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSecteurActivite>("toSecteurActivite");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TOS_STRUCTURES_FILLES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("tosStructuresFilles");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_PERE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructurePere");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure> TO_TYPE_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure>("toTypeStructure");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_UAI_HEBERGEUR = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toUaiHebergeur");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVService> TO_V_SERVICE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVService>("toVService");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String C_ACADEMIE_KEY = "cAcademie";
	public static final String C_NAF_KEY = "cNaf";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_STATUT_JURIDIQUE_KEY = "cStatutJuridique";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
	public static final String C_TYPE_DECISION_STR_KEY = "cTypeDecisionStr";
	public static final String C_TYPE_ETABLISSEMEN_KEY = "cTypeEtablissemen";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String DATE_DECISION_KEY = "dateDecision";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String GRP_ACCES_KEY = "grpAcces";
	public static final String GRP_ALIAS_KEY = "grpAlias";
	public static final String GRP_APE_CODE_KEY = "grpApeCode";
	public static final String GRP_CA_KEY = "grpCa";
	public static final String GRP_CAPITAL_KEY = "grpCapital";
	public static final String GRP_EFFECTIFS_KEY = "grpEffectifs";
	public static final String GRP_FONCTION1_KEY = "grpFonction1";
	public static final String GRP_FONCTION2_KEY = "grpFonction2";
	public static final String GRP_FORME_JURIDIQUE_KEY = "grpFormeJuridique";
	public static final String GRP_MOTS_CLEFS_KEY = "grpMotsClefs";
	public static final String GRP_OWNER_KEY = "grpOwner";
	public static final String GRP_RESPONSABILITE_KEY = "grpResponsabilite";
	public static final String GRP_RESPONSABLE_KEY = "grpResponsable";
	public static final String ID_RNSR_KEY = "idRnsr";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String REF_DECISION_KEY = "refDecision";
	public static final String ROM_ID_KEY = "romId";
	public static final String SACT_ID_KEY = "sactId";
	public static final String SIREN_KEY = "siren";
	public static final String SIRET_KEY = "siret";
	public static final String STR_ACTIVITE_KEY = "strActivite";
	public static final String STR_AFFICHAGE_KEY = "strAffichage";
	public static final String STR_DESCRIPTION_KEY = "strDescription";
	public static final String STR_PHOTO_KEY = "strPhoto";
	public static final String STR_RECHERCHE_KEY = "strRecherche";
	public static final String STR_STATUT_KEY = "strStatut";
	public static final String TEM_SECTORISE_KEY = "temSectorise";
	public static final String TEM_SIRET_PROVISOIRE_KEY = "temSiretProvisoire";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String TVA_INTRACOM_KEY = "tvaIntracom";

// Attributs non visibles
	public static final String UAI_HEBERGEUR_KEY = "uaiHebergeur";

//Colonnes dans la base de donnees
	public static final String C_ACADEMIE_COLKEY = "C_ACADEMIE";
	public static final String C_NAF_COLKEY = "C_NAF";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_STATUT_JURIDIQUE_COLKEY = "C_STATUT_JURIDIQUE";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
	public static final String C_TYPE_DECISION_STR_COLKEY = "C_TYPE_DECISION_STR";
	public static final String C_TYPE_ETABLISSEMEN_COLKEY = "C_TYPE_ETABLISSEMEN";
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String DATE_DECISION_COLKEY = "DATE_DECISION";
	public static final String DATE_FERMETURE_COLKEY = "DATE_FERMETURE";
	public static final String DATE_OUVERTURE_COLKEY = "DATE_OUVERTURE";
	public static final String D_CREATION_COLKEY = "d_Creation";
	public static final String D_MODIFICATION_COLKEY = "d_Modification";
	public static final String GRP_ACCES_COLKEY = "GRP_ACCES";
	public static final String GRP_ALIAS_COLKEY = "GRP_ALIAS";
	public static final String GRP_APE_CODE_COLKEY = "GRP_APE_CODE";
	public static final String GRP_CA_COLKEY = "GRP_CA";
	public static final String GRP_CAPITAL_COLKEY = "GRP_CAPITAL";
	public static final String GRP_EFFECTIFS_COLKEY = "GRP_EFFECTIFS";
	public static final String GRP_FONCTION1_COLKEY = "GRP_FONCTION1";
	public static final String GRP_FONCTION2_COLKEY = "GRP_FONCTION2";
	public static final String GRP_FORME_JURIDIQUE_COLKEY = "GRP_FORME_JURIDIQUE";
	public static final String GRP_MOTS_CLEFS_COLKEY = "GRP_MOTS_CLEFS";
	public static final String GRP_OWNER_COLKEY = "GRP_OWNER";
	public static final String GRP_RESPONSABILITE_COLKEY = "GRP_RESPONSABILITE";
	public static final String GRP_RESPONSABLE_COLKEY = "GRP_RESPONSABLE";
	public static final String ID_RNSR_COLKEY = "ID_RNSR";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String REF_DECISION_COLKEY = "REF_DECISION";
	public static final String ROM_ID_COLKEY = "ROM_ID";
	public static final String SACT_ID_COLKEY = "SACT_ID";
	public static final String SIREN_COLKEY = "SIREN";
	public static final String SIRET_COLKEY = "SIRET";
	public static final String STR_ACTIVITE_COLKEY = "STR_ACTIVITE";
	public static final String STR_AFFICHAGE_COLKEY = "STR_AFFICHAGE";
	public static final String STR_DESCRIPTION_COLKEY = "STR_DESCRIPTION";
	public static final String STR_PHOTO_COLKEY = "STR_PHOTO";
	public static final String STR_RECHERCHE_COLKEY = "STR_RECHERCHE";
	public static final String STR_STATUT_COLKEY = "STR_STATUT";
	public static final String TEM_SECTORISE_COLKEY = "TEM_SECTORISE";
	public static final String TEM_SIRET_PROVISOIRE_COLKEY = "TEM_SIRET_PROVISOIRE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String TVA_INTRACOM_COLKEY = "TVA_INTRACOM";

	public static final String UAI_HEBERGEUR_COLKEY = "UAI_HEBERGEUR";


	// Relationships
	public static final String TO_CA_STRUCTURES_KEY = "toCaStructures";
	public static final String TO_COMPOSANTE_KEY = "toComposante";
	public static final String TO_COMPTES_KEY = "toComptes";
	public static final String TO_EFFECTIF_STRUCTURES_KEY = "toEffectifStructures";
	public static final String TO_FORMES_JURIDIQUES_DETAILS_KEY = "toFormesJuridiquesDetails";
	public static final String TO_FOURNISS_KEY = "toFourniss";
	public static final String TO_NAF_KEY = "toNaf";
	public static final String TO_OWNER_KEY = "toOwner";
	public static final String TO_PERSONNE_ALIASES_KEY = "toPersonneAliases";
	public static final String TO_PERSONNE_TELEPHONES_KEY = "toPersonneTelephones";
	public static final String TO_REPART_ASSOCIATIONS_KEY = "toRepartAssociations";
	public static final String TO_REPART_ASSOCIATIONS_ELTS_KEY = "toRepartAssociationsElts";
	public static final String TO_REPART_PERSONNE_ADRESSES_KEY = "toRepartPersonneAdresses";
	public static final String TO_REPART_STRUCTURES_KEY = "toRepartStructures";
	public static final String TO_REPART_STRUCTURES_ELTS_KEY = "toRepartStructuresElts";
	public static final String TO_REPART_TYPE_GROUPES_KEY = "toRepartTypeGroupes";
	public static final String TO_RESPONSABLE_KEY = "toResponsable";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_ROME_KEY = "toRome";
	public static final String TO_SECRETARIATS_KEY = "toSecretariats";
	public static final String TO_SECTEUR_ACTIVITE_KEY = "toSecteurActivite";
	public static final String TOS_STRUCTURES_FILLES_KEY = "tosStructuresFilles";
	public static final String TO_STRUCTURE_PERE_KEY = "toStructurePere";
	public static final String TO_TYPE_STRUCTURE_KEY = "toTypeStructure";
	public static final String TO_UAI_HEBERGEUR_KEY = "toUaiHebergeur";
	public static final String TO_V_SERVICE_KEY = "toVService";



	// Accessors methods
  public String cAcademie() {
    return (String) storedValueForKey(C_ACADEMIE_KEY);
  }

  public void setCAcademie(String value) {
    takeStoredValueForKey(value, C_ACADEMIE_KEY);
  }

  public String cNaf() {
    return (String) storedValueForKey(C_NAF_KEY);
  }

  public void setCNaf(String value) {
    takeStoredValueForKey(value, C_NAF_KEY);
  }

  public String cRne() {
    return (String) storedValueForKey(C_RNE_KEY);
  }

  public void setCRne(String value) {
    takeStoredValueForKey(value, C_RNE_KEY);
  }

  public String cStatutJuridique() {
    return (String) storedValueForKey(C_STATUT_JURIDIQUE_KEY);
  }

  public void setCStatutJuridique(String value) {
    takeStoredValueForKey(value, C_STATUT_JURIDIQUE_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public String cStructurePere() {
    return (String) storedValueForKey(C_STRUCTURE_PERE_KEY);
  }

  public void setCStructurePere(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_PERE_KEY);
  }

  public String cTypeDecisionStr() {
    return (String) storedValueForKey(C_TYPE_DECISION_STR_KEY);
  }

  public void setCTypeDecisionStr(String value) {
    takeStoredValueForKey(value, C_TYPE_DECISION_STR_KEY);
  }

  public String cTypeEtablissemen() {
    return (String) storedValueForKey(C_TYPE_ETABLISSEMEN_KEY);
  }

  public void setCTypeEtablissemen(String value) {
    takeStoredValueForKey(value, C_TYPE_ETABLISSEMEN_KEY);
  }

  public String cTypeStructure() {
    return (String) storedValueForKey(C_TYPE_STRUCTURE_KEY);
  }

  public void setCTypeStructure(String value) {
    takeStoredValueForKey(value, C_TYPE_STRUCTURE_KEY);
  }

  public NSTimestamp dateDecision() {
    return (NSTimestamp) storedValueForKey(DATE_DECISION_KEY);
  }

  public void setDateDecision(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DECISION_KEY);
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String grpAcces() {
    return (String) storedValueForKey(GRP_ACCES_KEY);
  }

  public void setGrpAcces(String value) {
    takeStoredValueForKey(value, GRP_ACCES_KEY);
  }

  public String grpAlias() {
    return (String) storedValueForKey(GRP_ALIAS_KEY);
  }

  public void setGrpAlias(String value) {
    takeStoredValueForKey(value, GRP_ALIAS_KEY);
  }

  public String grpApeCode() {
    return (String) storedValueForKey(GRP_APE_CODE_KEY);
  }

  public void setGrpApeCode(String value) {
    takeStoredValueForKey(value, GRP_APE_CODE_KEY);
  }

  public Integer grpCa() {
    return (Integer) storedValueForKey(GRP_CA_KEY);
  }

  public void setGrpCa(Integer value) {
    takeStoredValueForKey(value, GRP_CA_KEY);
  }

  public Integer grpCapital() {
    return (Integer) storedValueForKey(GRP_CAPITAL_KEY);
  }

  public void setGrpCapital(Integer value) {
    takeStoredValueForKey(value, GRP_CAPITAL_KEY);
  }

  public Integer grpEffectifs() {
    return (Integer) storedValueForKey(GRP_EFFECTIFS_KEY);
  }

  public void setGrpEffectifs(Integer value) {
    takeStoredValueForKey(value, GRP_EFFECTIFS_KEY);
  }

  public String grpFonction1() {
    return (String) storedValueForKey(GRP_FONCTION1_KEY);
  }

  public void setGrpFonction1(String value) {
    takeStoredValueForKey(value, GRP_FONCTION1_KEY);
  }

  public String grpFonction2() {
    return (String) storedValueForKey(GRP_FONCTION2_KEY);
  }

  public void setGrpFonction2(String value) {
    takeStoredValueForKey(value, GRP_FONCTION2_KEY);
  }

  public String grpFormeJuridique() {
    return (String) storedValueForKey(GRP_FORME_JURIDIQUE_KEY);
  }

  public void setGrpFormeJuridique(String value) {
    takeStoredValueForKey(value, GRP_FORME_JURIDIQUE_KEY);
  }

  public String grpMotsClefs() {
    return (String) storedValueForKey(GRP_MOTS_CLEFS_KEY);
  }

  public void setGrpMotsClefs(String value) {
    takeStoredValueForKey(value, GRP_MOTS_CLEFS_KEY);
  }

  public Integer grpOwner() {
    return (Integer) storedValueForKey(GRP_OWNER_KEY);
  }

  public void setGrpOwner(Integer value) {
    takeStoredValueForKey(value, GRP_OWNER_KEY);
  }

  public String grpResponsabilite() {
    return (String) storedValueForKey(GRP_RESPONSABILITE_KEY);
  }

  public void setGrpResponsabilite(String value) {
    takeStoredValueForKey(value, GRP_RESPONSABILITE_KEY);
  }

  public Integer grpResponsable() {
    return (Integer) storedValueForKey(GRP_RESPONSABLE_KEY);
  }

  public void setGrpResponsable(Integer value) {
    takeStoredValueForKey(value, GRP_RESPONSABLE_KEY);
  }

  public String idRnsr() {
    return (String) storedValueForKey(ID_RNSR_KEY);
  }

  public void setIdRnsr(String value) {
    takeStoredValueForKey(value, ID_RNSR_KEY);
  }

  public String lcStructure() {
    return (String) storedValueForKey(LC_STRUCTURE_KEY);
  }

  public void setLcStructure(String value) {
    takeStoredValueForKey(value, LC_STRUCTURE_KEY);
  }

  public String llStructure() {
    return (String) storedValueForKey(LL_STRUCTURE_KEY);
  }

  public void setLlStructure(String value) {
    takeStoredValueForKey(value, LL_STRUCTURE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String refDecision() {
    return (String) storedValueForKey(REF_DECISION_KEY);
  }

  public void setRefDecision(String value) {
    takeStoredValueForKey(value, REF_DECISION_KEY);
  }

  public Integer romId() {
    return (Integer) storedValueForKey(ROM_ID_KEY);
  }

  public void setRomId(Integer value) {
    takeStoredValueForKey(value, ROM_ID_KEY);
  }

  public Integer sactId() {
    return (Integer) storedValueForKey(SACT_ID_KEY);
  }

  public void setSactId(Integer value) {
    takeStoredValueForKey(value, SACT_ID_KEY);
  }

  public String siren() {
    return (String) storedValueForKey(SIREN_KEY);
  }

  public void setSiren(String value) {
    takeStoredValueForKey(value, SIREN_KEY);
  }

  public String siret() {
    return (String) storedValueForKey(SIRET_KEY);
  }

  public void setSiret(String value) {
    takeStoredValueForKey(value, SIRET_KEY);
  }

  public String strActivite() {
    return (String) storedValueForKey(STR_ACTIVITE_KEY);
  }

  public void setStrActivite(String value) {
    takeStoredValueForKey(value, STR_ACTIVITE_KEY);
  }

  public String strAffichage() {
    return (String) storedValueForKey(STR_AFFICHAGE_KEY);
  }

  public void setStrAffichage(String value) {
    takeStoredValueForKey(value, STR_AFFICHAGE_KEY);
  }

  public String strDescription() {
    return (String) storedValueForKey(STR_DESCRIPTION_KEY);
  }

  public void setStrDescription(String value) {
    takeStoredValueForKey(value, STR_DESCRIPTION_KEY);
  }

  public String strPhoto() {
    return (String) storedValueForKey(STR_PHOTO_KEY);
  }

  public void setStrPhoto(String value) {
    takeStoredValueForKey(value, STR_PHOTO_KEY);
  }

  public String strRecherche() {
    return (String) storedValueForKey(STR_RECHERCHE_KEY);
  }

  public void setStrRecherche(String value) {
    takeStoredValueForKey(value, STR_RECHERCHE_KEY);
  }

  public String strStatut() {
    return (String) storedValueForKey(STR_STATUT_KEY);
  }

  public void setStrStatut(String value) {
    takeStoredValueForKey(value, STR_STATUT_KEY);
  }

  public String temSectorise() {
    return (String) storedValueForKey(TEM_SECTORISE_KEY);
  }

  public void setTemSectorise(String value) {
    takeStoredValueForKey(value, TEM_SECTORISE_KEY);
  }

  public String temSiretProvisoire() {
    return (String) storedValueForKey(TEM_SIRET_PROVISOIRE_KEY);
  }

  public void setTemSiretProvisoire(String value) {
    takeStoredValueForKey(value, TEM_SIRET_PROVISOIRE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public String tvaIntracom() {
    return (String) storedValueForKey(TVA_INTRACOM_KEY);
  }

  public void setTvaIntracom(String value) {
    takeStoredValueForKey(value, TVA_INTRACOM_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toComposante() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_COMPOSANTE_KEY);
  }

  public void setToComposanteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toComposante();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPOSANTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPOSANTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails toFormesJuridiquesDetails() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails)storedValueForKey(TO_FORMES_JURIDIQUES_DETAILS_KEY);
  }

  public void setToFormesJuridiquesDetailsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails oldValue = toFormesJuridiquesDetails();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FORMES_JURIDIQUES_DETAILS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FORMES_JURIDIQUES_DETAILS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EONaf toNaf() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EONaf)storedValueForKey(TO_NAF_KEY);
  }

  public void setToNafRelationship(org.cocktail.fwkcktlpersonne.common.metier.EONaf value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EONaf oldValue = toNaf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_NAF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_NAF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toOwner() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(TO_OWNER_KEY);
  }

  public void setToOwnerRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toOwner();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_OWNER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_OWNER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toResponsable() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(TO_RESPONSABLE_KEY);
  }

  public void setToResponsableRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toResponsable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RESPONSABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RESPONSABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORome toRome() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORome)storedValueForKey(TO_ROME_KEY);
  }

  public void setToRomeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORome value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORome oldValue = toRome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ROME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ROME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSecteurActivite toSecteurActivite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSecteurActivite)storedValueForKey(TO_SECTEUR_ACTIVITE_KEY);
  }

  public void setToSecteurActiviteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSecteurActivite value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSecteurActivite oldValue = toSecteurActivite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SECTEUR_ACTIVITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SECTEUR_ACTIVITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructurePere() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_STRUCTURE_PERE_KEY);
  }

  public void setToStructurePereRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructurePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure toTypeStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure)storedValueForKey(TO_TYPE_STRUCTURE_KEY);
  }

  public void setToTypeStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure oldValue = toTypeStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toUaiHebergeur() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_UAI_HEBERGEUR_KEY);
  }

  public void setToUaiHebergeurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toUaiHebergeur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UAI_HEBERGEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UAI_HEBERGEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOVService toVService() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVService)storedValueForKey(TO_V_SERVICE_KEY);
  }

  public void setToVServiceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVService value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOVService oldValue = toVService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_V_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_V_SERVICE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure> toCaStructures() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure>)storedValueForKey(TO_CA_STRUCTURES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure> toCaStructures(EOQualifier qualifier) {
    return toCaStructures(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure> toCaStructures(EOQualifier qualifier, boolean fetch) {
    return toCaStructures(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure> toCaStructures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure.TO_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toCaStructures();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToCaStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_CA_STRUCTURES_KEY);
  }

  public void removeFromToCaStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CA_STRUCTURES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure createToCaStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_CaStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_CA_STRUCTURES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure) eo;
  }

  public void deleteToCaStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CA_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToCaStructuresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure> objects = toCaStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToCaStructuresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>)storedValueForKey(TO_COMPTES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes(EOQualifier qualifier) {
    return toComptes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes(EOQualifier qualifier, boolean fetch) {
    return toComptes(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOCompte.TO_STRUCTURES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOCompte.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toComptes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToComptesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
  }

  public void removeFromToComptesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompte createToComptesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Compte");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_COMPTES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompte) eo;
  }

  public void deleteToComptesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToComptesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> objects = toComptes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToComptesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure> toEffectifStructures() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure>)storedValueForKey(TO_EFFECTIF_STRUCTURES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure> toEffectifStructures(EOQualifier qualifier) {
    return toEffectifStructures(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure> toEffectifStructures(EOQualifier qualifier, boolean fetch) {
    return toEffectifStructures(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure> toEffectifStructures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure.TO_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toEffectifStructures();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToEffectifStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_EFFECTIF_STRUCTURES_KEY);
  }

  public void removeFromToEffectifStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EFFECTIF_STRUCTURES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure createToEffectifStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_EffectifStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_EFFECTIF_STRUCTURES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure) eo;
  }

  public void deleteToEffectifStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EFFECTIF_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToEffectifStructuresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure> objects = toEffectifStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToEffectifStructuresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>)storedValueForKey(TO_FOURNISS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss(EOQualifier qualifier) {
    return toFourniss(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss(EOQualifier qualifier, boolean fetch) {
    return toFourniss(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOFournis.TO_STRUCTURES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOFournis.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toFourniss();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToFournissRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_FOURNISS_KEY);
  }

  public void removeFromToFournissRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FOURNISS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOFournis createToFournissRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Fournis");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_FOURNISS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis) eo;
  }

  public void deleteToFournissRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FOURNISS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToFournissRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> objects = toFourniss().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToFournissRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>)storedValueForKey(TO_PERSONNE_ALIASES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases(EOQualifier qualifier) {
    return toPersonneAliases(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases(EOQualifier qualifier, boolean fetch) {
    return toPersonneAliases(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias.TO_STRUCTURES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersonneAliases();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersonneAliasesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERSONNE_ALIASES_KEY);
  }

  public void removeFromToPersonneAliasesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_ALIASES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias createToPersonneAliasesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_PersonneAlias");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERSONNE_ALIASES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias) eo;
  }

  public void deleteToPersonneAliasesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_ALIASES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersonneAliasesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> objects = toPersonneAliases().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersonneAliasesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>)storedValueForKey(TO_PERSONNE_TELEPHONES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones(EOQualifier qualifier) {
    return toPersonneTelephones(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones(EOQualifier qualifier, boolean fetch) {
    return toPersonneTelephones(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone.TO_STRUCTURES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersonneTelephones();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersonneTelephonesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERSONNE_TELEPHONES_KEY);
  }

  public void removeFromToPersonneTelephonesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_TELEPHONES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone createToPersonneTelephonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_PersonneTelephone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERSONNE_TELEPHONES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone) eo;
  }

  public void deleteToPersonneTelephonesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_TELEPHONES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersonneTelephonesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> objects = toPersonneTelephones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersonneTelephonesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)storedValueForKey(TO_REPART_ASSOCIATIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations(EOQualifier qualifier) {
    return toRepartAssociations(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations(EOQualifier qualifier, boolean fetch) {
    return toRepartAssociations(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartAssociations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartAssociationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_KEY);
  }

  public void removeFromToRepartAssociationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation createToRepartAssociationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartAssociation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_ASSOCIATIONS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation) eo;
  }

  public void deleteToRepartAssociationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartAssociationsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> objects = toRepartAssociations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartAssociationsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociationsElts() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)storedValueForKey(TO_REPART_ASSOCIATIONS_ELTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociationsElts(EOQualifier qualifier) {
    return toRepartAssociationsElts(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociationsElts(EOQualifier qualifier, boolean fetch) {
    return toRepartAssociationsElts(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociationsElts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation.TO_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartAssociationsElts();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartAssociationsEltsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_ELTS_KEY);
  }

  public void removeFromToRepartAssociationsEltsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_ELTS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation createToRepartAssociationsEltsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartAssociation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_ASSOCIATIONS_ELTS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation) eo;
  }

  public void deleteToRepartAssociationsEltsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_ELTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartAssociationsEltsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> objects = toRepartAssociationsElts().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartAssociationsEltsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)storedValueForKey(TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier) {
    return toRepartPersonneAdresses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier, boolean fetch) {
    return toRepartPersonneAdresses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse.TO_STRUCTURES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse createToRepartPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse) eo;
  }

  public void deleteToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartPersonneAdressesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> objects = toRepartPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartPersonneAdressesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)storedValueForKey(TO_REPART_STRUCTURES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures(EOQualifier qualifier) {
    return toRepartStructures(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures(EOQualifier qualifier, boolean fetch) {
    return toRepartStructures(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure.TO_STRUCTURE_ELTS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartStructures();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_KEY);
  }

  public void removeFromToRepartStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure createToRepartStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_STRUCTURES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure) eo;
  }

  public void deleteToRepartStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartStructuresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> objects = toRepartStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartStructuresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructuresElts() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)storedValueForKey(TO_REPART_STRUCTURES_ELTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructuresElts(EOQualifier qualifier) {
    return toRepartStructuresElts(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructuresElts(EOQualifier qualifier, boolean fetch) {
    return toRepartStructuresElts(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructuresElts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartStructuresElts();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartStructuresEltsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_ELTS_KEY);
  }

  public void removeFromToRepartStructuresEltsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_ELTS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure createToRepartStructuresEltsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_STRUCTURES_ELTS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure) eo;
  }

  public void deleteToRepartStructuresEltsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_ELTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartStructuresEltsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> objects = toRepartStructuresElts().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartStructuresEltsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe> toRepartTypeGroupes() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe>)storedValueForKey(TO_REPART_TYPE_GROUPES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe> toRepartTypeGroupes(EOQualifier qualifier) {
    return toRepartTypeGroupes(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe> toRepartTypeGroupes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe> results;
      results = toRepartTypeGroupes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToRepartTypeGroupesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_GROUPES_KEY);
  }

  public void removeFromToRepartTypeGroupesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_GROUPES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe createToRepartTypeGroupesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartTypeGroupe");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_TYPE_GROUPES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe) eo;
  }

  public void deleteToRepartTypeGroupesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_GROUPES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartTypeGroupesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe> objects = toRepartTypeGroupes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartTypeGroupesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat> toSecretariats() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat>)storedValueForKey(TO_SECRETARIATS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat> toSecretariats(EOQualifier qualifier) {
    return toSecretariats(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat> toSecretariats(EOQualifier qualifier, boolean fetch) {
    return toSecretariats(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat> toSecretariats(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat.TO_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSecretariats();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSecretariatsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SECRETARIATS_KEY);
  }

  public void removeFromToSecretariatsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SECRETARIATS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat createToSecretariatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Secretariat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SECRETARIATS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat) eo;
  }

  public void deleteToSecretariatsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SECRETARIATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSecretariatsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat> objects = toSecretariats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSecretariatsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructuresFilles() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)storedValueForKey(TOS_STRUCTURES_FILLES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructuresFilles(EOQualifier qualifier) {
    return tosStructuresFilles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructuresFilles(EOQualifier qualifier, boolean fetch) {
    return tosStructuresFilles(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructuresFilles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOStructure.TO_STRUCTURE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosStructuresFilles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosStructuresFillesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURES_FILLES_KEY);
  }

  public void removeFromTosStructuresFillesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURES_FILLES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructuresFillesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURES_FILLES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructuresFillesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURES_FILLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructuresFillesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> objects = tosStructuresFilles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructuresFillesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOStructure avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOStructure createEOStructure(EOEditingContext editingContext, String cStructure
, String cTypeStructure
, String llStructure
, Integer persId
, String strAffichage
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure toTypeStructure			) {
    EOStructure eo = (EOStructure) createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setCTypeStructure(cTypeStructure);
		eo.setLlStructure(llStructure);
		eo.setPersId(persId);
		eo.setStrAffichage(strAffichage);
		eo.setTemValide(temValide);
    eo.setToTypeStructureRelationship(toTypeStructure);
    return eo;
  }

  
	  public EOStructure localInstanceIn(EOEditingContext editingContext) {
	  		return (EOStructure)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructure creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructure creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOStructure object = (EOStructure)createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOStructure localInstanceIn(EOEditingContext editingContext, EOStructure eo) {
    EOStructure localInstance = (eo == null) ? null : (EOStructure)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOStructure#localInstanceIn a la place.
   */
	public static EOStructure localInstanceOf(EOEditingContext editingContext, EOStructure eo) {
		return EOStructure.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOStructure fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStructure fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOStructure> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOStructure> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOStructure fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOStructure eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOStructure ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOStructure fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
