/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOTypeAbsence extends _EOTypeAbsence {

	public static final EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_TYPE_ABSENCE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
	public static final String[] TYPES_CONGE_COMMUNS =  {"CMATER", "CPATER", "ENFANT", "ADOPTION", "CSF", "CGRFP"};
	public static final String[] TYPES_CONGE_FONCTIONNAIRES =  {"MAL", "CLM", "CLD", "MALSSTRT", "ACCSERV", "CFP", "CRCT"};
	public static final String[] TYPES_CONGE_NORMALIENS =  {"CCP", "CIR", "RDT"};
	public static final String[] TYPES_CONGE_CONTRACTUELS_HORS_HU =  {"MALNT", "ACCTRAV", "CGM"};
	public static final String[] TYPES_CONGE_CONTRACTUELS =  {"MALNT", "ACCTRAV", "CGM", "CGRFP", "AHCUAO3", "AHCUAO4", "AHCUAO5", "AHCUAO6"};
	public static final String TYPE_CONGE_MATERNITE = "CMATER";
	public static final String TYPE_CONGE_PATERNITE = "CPATER";
	public static final String TYPE_CONGE_GARDE_ENFANT = "ENFANT";

	public static final String TYPE_CONGE_MALADIE = "MAL";
	public static final String TYPE_CONGE_MALADIE_NT = "MALNT";
	public static final String TYPE_CONGE_MALADIE_CGM = "CGM";
	public static final String TYPE_CONGE_RFP = "CGRFP";

	public static final String TYPE_CONGE_ACC_SERV = "ACCSERV";
	public static final String TYPE_CONGE_ACC_TRAV = "ACCTRAV";

	public static final String TYPE_CLD = "CLD";
	public static final String TYPE_CLM = "CLM";
	public static final String TYPE_CPA = "CPA";
	public static final String TYPE_CFP = "CFP";	// 24/11/2010
	private static String CONGE_FORMATION = "CFP";
	private static String HOSPITALO_UNIV = "AHCUAO";

	
	public static final String TYPE_DETACHEMENT = "DETA";
	public static final String TYPE_DISPONIBILITE = "DISP";
	public static final String TYPE_SERVICE_NATIONAL = "SNAT";
	public static final String TYPE_CONGE_PARENTAL = "CGPA";
	
	public static final String TYPE_TEMPS_PARTIEL = "TPAR";
	public static final String TYPE_TEMPS_PARTIEL_THERAP = "MTT";
	public static String TYPE_CRCT = "CRCT";
	public static final String TYPE_CST = "CGST";
	public static final String TYPE_DELEGATION = "DELEGATION";
	public static final String TYPE_CFA = "CFA";

	public static final String TYPE_DEPART = "DEPA";

	public EOTypeAbsence() {
		super();
	}
	
	public String toString() {
		return llTypeAbsence();
	}
	
	public static NSDictionary entitesConge() {
		
		NSMutableDictionary dicoEntites = new NSMutableDictionary();
		
		dicoEntites.setObjectForKey(TYPE_CONGE_MALADIE, "CongeMaladie");
		dicoEntites.setObjectForKey(TYPE_CONGE_MALADIE_NT, "CgntMaladie");

		
		return dicoEntites;
		
	}
	
	
	public boolean estCongeLegal() {
		return congeLegal() != null && congeLegal().equals(CocktailConstantes.VRAI);
	}
	public boolean estCongeCir() {
		return temCir() != null && temCir().equals(CocktailConstantes.VRAI);
	}
	public boolean requiertEnfant() {
		return temEnfant() != null && temEnfant().equals(CocktailConstantes.VRAI);
	}

	
	public boolean estCongeGardeEnfant() {
		return cTypeAbsence().equals(TYPE_CONGE_GARDE_ENFANT);
	}

	public boolean estCongeMaternite() {
		return cTypeAbsence().equals(TYPE_CONGE_MATERNITE);
	}

	public boolean estCongeRfp() {
		return cTypeAbsence().equals(TYPE_CONGE_RFP);
	}

	public boolean estCongeMaladieContractuel() {

		return cTypeAbsence().equals(TYPE_CONGE_MALADIE_NT);
		
	}

	public boolean estCongeMaladie() {
		return cTypeAbsence().equals(TYPE_CONGE_MALADIE) || cTypeAbsence().equals(TYPE_CONGE_MALADIE_NT);
	}
	public boolean estCongeLongueDuree() {
		return cTypeAbsence().equals(TYPE_CLD);
	}
	public boolean estCongeLongueMaladie() {
		return cTypeAbsence().equals(TYPE_CLM);
	}

	
	public boolean estDetachement() {
		return cTypeAbsence().equals(TYPE_DETACHEMENT);
	}
	public boolean estDisponibilite() {
		return cTypeAbsence().equals(TYPE_DISPONIBILITE);
	}
	public boolean estCongeParental() {
		return cTypeAbsence().equals(TYPE_CONGE_PARENTAL);
	}
	public boolean estServiceNational() {
		return cTypeAbsence().equals(TYPE_SERVICE_NATIONAL);
	}
	
	public boolean estCongePourFonctionnaire() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_FONCTIONNAIRES.length; i++) {
			if (TYPES_CONGE_FONCTIONNAIRES[i].equals(cTypeAbsence())) {
				return true;
			}
		}
		return false;
	}
	public boolean estCongePourContractuel() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_CONTRACTUELS.length; i++) {
			if (TYPES_CONGE_CONTRACTUELS[i].equals(cTypeAbsence())) {
				return true;
			}
		}
		return false;
	}
	public boolean estCongeCommun() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_COMMUNS.length; i++) {
			if (TYPES_CONGE_COMMUNS[i].equals(cTypeAbsence())) {
				return true;
			}
		}
		return false;
	}
	public boolean estCongeNormalien() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_NORMALIENS.length; i++) {
			if (TYPES_CONGE_NORMALIENS[i].equals(cTypeAbsence())) {
				return true;
			}
		}
		return false;
	}
	/** Return true si il s'agit d'un CLM ou d'un CLD */
	// 18/11/2010
	public boolean estCongePourLongueMaladie() {
		return cTypeAbsence() != null && (cTypeAbsence().equals(TYPE_CLD) || cTypeAbsence().equals(TYPE_CLM));
	}
	// Interface RecordAvecLibelle
	public String libelle() {
		return llTypeAbsence();
	}
	// méthodes ajoutées
	
	/** retourne le type absence pour le type passe en parametre ou null */
	public static EOTypeAbsence rechercherTypeAbsence(EOEditingContext editingContext,String typeAbsence) {

		NSMutableArray qualifiers = new NSMutableArray();
		try {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_ABSENCE_KEY + "=%@", new NSArray(typeAbsence)));
			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
		} catch (Exception e) {
			return null;
		}
	}

		/** retourne le type absence pour le nom de table et le type passes en parametre
	 * typeAbsence peut etre nulle si il y a une seule entree dans la table pour le nom de table  */
	public static EOTypeAbsence rechercherTypeAbsencePourTable(EOEditingContext editingContext,String nomTable,String typeAbsence) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_ABSENCE_HARPEGE_KEY+"=%@", new NSArray(nomTable)));
			if (typeAbsence != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_ABSENCE_KEY+"=%@", new NSArray(typeAbsence)));
			}

			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
		} catch (Exception e) {
			return null;
		}
	}
	
	

}
