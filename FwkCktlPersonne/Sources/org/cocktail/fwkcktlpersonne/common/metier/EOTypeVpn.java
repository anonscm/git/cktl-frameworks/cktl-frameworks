

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTypeVpn extends _EOTypeVpn
{
    public static final Object TVPN_CODE_NO = "NO";

	public EOTypeVpn() {
        super();
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

	public static String vlanPourCle(EOEditingContext editingContext, String cle) {
		return vlanPourCle(editingContext, cle, null);
	}

	public static String vlanPourCle(EOEditingContext editingContext, String cle, String defaultValue) {
		NSArray res = vlansPourCle(editingContext, cle);
		// gere les retour en keyValueCodingNull
		if (res.count() > 0 && (res.objectAtIndex(0) instanceof String)) {
			return (String) res.objectAtIndex(0);
		}
		return defaultValue;
	}
	
	/**
	 * @param editingContext
	 * @param cle
	 * @return Un tableau de String correspondant aux valeurs trouvées pour la
	 *         cle specifiee en parametre.
	 */
	public static NSArray vlansPourCle(EOEditingContext editingContext, String cle) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeVpn.TVPN_CODE_KEY + " = %@", new NSArray(cle));
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeVpn.ENTITY_NAME, qualifier, null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		return (NSArray) results.valueForKeyPath(EOTypeVpn.TVPN_CODE_KEY);
	}

}
