/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReferensDcp.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOReferensDcp extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOReferensDcp.class);

	public static final String ENTITY_NAME = "Fwkpers_ReferensDcp";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REFERENS_DCP";


// Attribute Keys
  public static final ERXKey<String> INFOS_DCP = new ERXKey<String>("infosDcp");
  public static final ERXKey<String> INTITUL_DCP = new ERXKey<String>("intitulDcp");
  public static final ERXKey<String> LETTRE_BAP = new ERXKey<String>("lettreBap");
  public static final ERXKey<String> NUM_DCP = new ERXKey<String>("numDcp");
  public static final ERXKey<String> SYGLE_DCP = new ERXKey<String>("sygleDcp");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> TO_REFERENS_FPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp>("toReferensFps");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "numDcp";

	public static final String INFOS_DCP_KEY = "infosDcp";
	public static final String INTITUL_DCP_KEY = "intitulDcp";
	public static final String LETTRE_BAP_KEY = "lettreBap";
	public static final String NUM_DCP_KEY = "numDcp";
	public static final String SYGLE_DCP_KEY = "sygleDcp";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String INFOS_DCP_COLKEY = "INFOSDCP";
	public static final String INTITUL_DCP_COLKEY = "INTITULDCP";
	public static final String LETTRE_BAP_COLKEY = "LETTREBAP";
	public static final String NUM_DCP_COLKEY = "NUMDCP";
	public static final String SYGLE_DCP_COLKEY = "SYGLEDCP";



	// Relationships
	public static final String TO_REFERENS_FPS_KEY = "toReferensFps";



	// Accessors methods
  public String infosDcp() {
    return (String) storedValueForKey(INFOS_DCP_KEY);
  }

  public void setInfosDcp(String value) {
    takeStoredValueForKey(value, INFOS_DCP_KEY);
  }

  public String intitulDcp() {
    return (String) storedValueForKey(INTITUL_DCP_KEY);
  }

  public void setIntitulDcp(String value) {
    takeStoredValueForKey(value, INTITUL_DCP_KEY);
  }

  public String lettreBap() {
    return (String) storedValueForKey(LETTRE_BAP_KEY);
  }

  public void setLettreBap(String value) {
    takeStoredValueForKey(value, LETTRE_BAP_KEY);
  }

  public String numDcp() {
    return (String) storedValueForKey(NUM_DCP_KEY);
  }

  public void setNumDcp(String value) {
    takeStoredValueForKey(value, NUM_DCP_KEY);
  }

  public String sygleDcp() {
    return (String) storedValueForKey(SYGLE_DCP_KEY);
  }

  public void setSygleDcp(String value) {
    takeStoredValueForKey(value, SYGLE_DCP_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> toReferensFps() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp>)storedValueForKey(TO_REFERENS_FPS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> toReferensFps(EOQualifier qualifier) {
    return toReferensFps(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> toReferensFps(EOQualifier qualifier, boolean fetch) {
    return toReferensFps(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> toReferensFps(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp.TO_REFERENS_DCP_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toReferensFps();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToReferensFpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REFERENS_FPS_KEY);
  }

  public void removeFromToReferensFpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REFERENS_FPS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp createToReferensFpsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_ReferensFp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REFERENS_FPS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp) eo;
  }

  public void deleteToReferensFpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REFERENS_FPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToReferensFpsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> objects = toReferensFps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToReferensFpsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOReferensDcp avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOReferensDcp createEOReferensDcp(EOEditingContext editingContext, String numDcp
			) {
    EOReferensDcp eo = (EOReferensDcp) createAndInsertInstance(editingContext, _EOReferensDcp.ENTITY_NAME);    
		eo.setNumDcp(numDcp);
    return eo;
  }

  
	  public EOReferensDcp localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReferensDcp)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensDcp creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensDcp creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOReferensDcp object = (EOReferensDcp)createAndInsertInstance(editingContext, _EOReferensDcp.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOReferensDcp localInstanceIn(EOEditingContext editingContext, EOReferensDcp eo) {
    EOReferensDcp localInstance = (eo == null) ? null : (EOReferensDcp)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOReferensDcp#localInstanceIn a la place.
   */
	public static EOReferensDcp localInstanceOf(EOEditingContext editingContext, EOReferensDcp eo) {
		return EOReferensDcp.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReferensDcp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReferensDcp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReferensDcp> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReferensDcp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReferensDcp)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReferensDcp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReferensDcp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReferensDcp> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReferensDcp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReferensDcp)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReferensDcp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReferensDcp eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReferensDcp ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReferensDcp fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
