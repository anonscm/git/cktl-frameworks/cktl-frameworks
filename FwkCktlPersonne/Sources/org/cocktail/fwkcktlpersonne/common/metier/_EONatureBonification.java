/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONatureBonification.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EONatureBonification extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EONatureBonification.class);

	public static final String ENTITY_NAME = "Fwkpers_NatureBonification";
	public static final String ENTITY_TABLE_NAME = "GRHUM.NATURE_BONIFICATION";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_FERMETURE = new ERXKey<NSTimestamp>("dateFermeture");
  public static final ERXKey<NSTimestamp> DATE_OUVERTURE = new ERXKey<NSTimestamp>("dateOuverture");
  public static final ERXKey<String> NBO_CODE = new ERXKey<String>("nboCode");
  public static final ERXKey<String> NBO_LIBELLE_COURT = new ERXKey<String>("nboLibelleCourt");
  public static final ERXKey<String> NBO_LIBELLE_LONG = new ERXKey<String>("nboLibelleLong");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "nboOrdre";

	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String NBO_CODE_KEY = "nboCode";
	public static final String NBO_LIBELLE_COURT_KEY = "nboLibelleCourt";
	public static final String NBO_LIBELLE_LONG_KEY = "nboLibelleLong";

// Attributs non visibles
	public static final String NBO_ORDRE_KEY = "nboOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String DATE_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String NBO_CODE_COLKEY = "NBO_CODE";
	public static final String NBO_LIBELLE_COURT_COLKEY = "NBO_LIBELLE_COURT";
	public static final String NBO_LIBELLE_LONG_COLKEY = "NBO_LIBELLE_LONG";

	public static final String NBO_ORDRE_COLKEY = "NBO_ORDRE";


	// Relationships



	// Accessors methods
  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
  }

  public String nboCode() {
    return (String) storedValueForKey(NBO_CODE_KEY);
  }

  public void setNboCode(String value) {
    takeStoredValueForKey(value, NBO_CODE_KEY);
  }

  public String nboLibelleCourt() {
    return (String) storedValueForKey(NBO_LIBELLE_COURT_KEY);
  }

  public void setNboLibelleCourt(String value) {
    takeStoredValueForKey(value, NBO_LIBELLE_COURT_KEY);
  }

  public String nboLibelleLong() {
    return (String) storedValueForKey(NBO_LIBELLE_LONG_KEY);
  }

  public void setNboLibelleLong(String value) {
    takeStoredValueForKey(value, NBO_LIBELLE_LONG_KEY);
  }


/**
 * Créer une instance de EONatureBonification avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONatureBonification createEONatureBonification(EOEditingContext editingContext			) {
    EONatureBonification eo = (EONatureBonification) createAndInsertInstance(editingContext, _EONatureBonification.ENTITY_NAME);    
    return eo;
  }

  
	  public EONatureBonification localInstanceIn(EOEditingContext editingContext) {
	  		return (EONatureBonification)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONatureBonification creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONatureBonification creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EONatureBonification object = (EONatureBonification)createAndInsertInstance(editingContext, _EONatureBonification.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EONatureBonification localInstanceIn(EOEditingContext editingContext, EONatureBonification eo) {
    EONatureBonification localInstance = (eo == null) ? null : (EONatureBonification)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONatureBonification#localInstanceIn a la place.
   */
	public static EONatureBonification localInstanceOf(EOEditingContext editingContext, EONatureBonification eo) {
		return EONatureBonification.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EONatureBonification fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONatureBonification fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EONatureBonification> eoObjects = fetchAll(editingContext, qualifier, null);
	    EONatureBonification eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONatureBonification)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONatureBonification fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONatureBonification fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EONatureBonification> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONatureBonification eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONatureBonification)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONatureBonification fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONatureBonification eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONatureBonification ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONatureBonification fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
