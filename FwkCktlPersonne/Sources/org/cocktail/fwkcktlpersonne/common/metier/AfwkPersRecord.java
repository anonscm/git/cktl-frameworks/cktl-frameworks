/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.CktlCallEOUtilities;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXGenericRecord;
import er.extensions.foundation.ERXThreadStorage;

/**
 * Classe abstraite pour les entités métier du framework FwkCktlPersonne.
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class AfwkPersRecord extends ERXGenericRecord {

	public static final String OUI = "O";
	public static final String NON = "N";
	public static final String VIDE = "";
	public static final String SPACE = " ";
	public static final String ETOILE = "*";

	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	private NSMutableArray<ISpecificite> specificites = new NSMutableArray<ISpecificite>();
	private NSArray attributes = null;
	private Map attributesTaillesMax;
	private NSArray attributesObligatoires;

	private EOEditingContext validationEditingContext;

	private PersonneApplicationUser createur;
	private PersonneApplicationUser modificateur;

	/**
	 * @return La date actuelle.
	 * @deprecated Utilisez {@link MyDateCtrl#now()} à la place.
	 */
	@Deprecated
	public NSTimestamp now() {
		return MyDateCtrl.now();
	}

	/**
	 * Verifie si la longueur d'une chaine est bien inferieure a un maximum.
	 *
	 * @param s
	 * @param l -1 si illimite.
	 * @return true si l=-1 ou si le nombre de caracteres de s est inferieur ou egal a l.
	 */
	protected boolean verifieLongueurMax(String s, int l) {
		return (l == -1 || s == null || s.trim().length() <= l);
	}

	/**
	 * Verifie si la longeur d'une chaine est bien superieure a un maximum.
	 *
	 * @param s
	 * @param l
	 * @return true si l=0 ou si le nombre de caracteres de s est superieur ou egal a l.
	 */
	protected boolean verifieLongueurMin(String s, int l) {
		return (s == null || s.trim().length() >= l);
	}

	/**
	 * Nettoie toutes les chaines de l'objet (en effectuant un trim + remplace chaine vide par null). A appeler en debut de ValidateObjectMetier.
	 */
	protected void trimAllString() {
		EOEnterpriseObject obj = this;
		NSArray atts = obj.attributeKeys();
		for (int i = 0; i < atts.count(); i++) {
			String key = (String) atts.objectAtIndex(i);
			if (obj.valueForKey(key) != null && hasKeyChangedFromCommittedSnapshot(key) && obj.valueForKey(key) instanceof String) {
				String value = ((String) obj.valueForKey(key)).trim();
				if (value.length() == 0) {
					value = null;
				}
				obj.takeValueForKey(value, key);
			}
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		callOnAwakeFromInsertionForSpecificites();
	}

	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		callOnValidateForInsertOnSpecificites();
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		callOnValidateForUpdateOnSpecificites();
		super.validateForUpdate();
	}

	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
		callOnValidateForDeleteOnSpecificites();
	}

	@Override
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * @throws NSValidation.ValidationException : exception remontée par la validation de l'objet métier
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		callOnValidateObjectMetierOnSpecificites();
	}

	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		callOnValidateBeforeTransactionSaveOnSpecificites();
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, AfwkPersRecord eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		} else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		AfwkPersRecord res = (AfwkPersRecord) eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

		if (res != null && eoenterpriseobject.getValidationEditingContext() != null) {
			res.setValidationEditingContext(eoenterpriseobject.getValidationEditingContext());
		}

		return res;

	}

	public static AfwkPersRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}

	/**
	 * @param eoeditingcontext
	 * @param s
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau est affecte a l'objet juste apres sa creation, avant l'insertion dans
	 *            l'editing context.
	 * @return l'objet créé.
	 */
	public static AfwkPersRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		} else {
			AfwkPersRecord eoenterpriseobject = (AfwkPersRecord) eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			if (specificites != null) {
				Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
				while (enumeration.hasMoreElements()) {
					ISpecificite specificite = enumeration.nextElement();
					eoenterpriseobject.registerSpecificite(specificite);
				}
			}
			eoeditingcontext.insertObject(eoenterpriseobject);
			eoenterpriseobject.setValidationEditingContext(eoeditingcontext);
			return eoenterpriseobject;
		}
	}

	/**
	 * Cree une instance sans l'inserer dans l'EC.
	 *
	 * @param eoeditingcontext
	 * @param s
	 * @param specificites
	 * @return
	 */
	public static AfwkPersRecord createInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		} else {
			AfwkPersRecord eoenterpriseobject = (AfwkPersRecord) eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			if (specificites != null) {
				Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
				while (enumeration.hasMoreElements()) {
					ISpecificite specificite = enumeration.nextElement();
					eoenterpriseobject.registerSpecificite(specificite);
				}
			}
			return eoenterpriseobject;
		}
	}

	/**
	 * Ajoute un specificite pour les operations de validation de l'objet metier.
	 *
	 * @param specificite
	 */
	public void registerSpecificite(ISpecificite specificite) {
		if (specificites == null) {
			specificites = new NSMutableArray();
		}
		if (!hasSpecificite(specificite)) {
			specificites.addObject(specificite);
		}
	}

	/**
	 * @param specificite
	 * @return true si la specificite fait déja partie de la liste des specificites.
	 */
	public boolean hasSpecificite(ISpecificite specificite) {
		return specificites.indexOfObject(specificite) != NSArray.NotFound;
	}

	/**
	 * Supprime une specificite la liste des specificites.
	 *
	 * @param specificite
	 */
	public void unregisterSpecificite(ISpecificite specificite) {
		if (specificites != null) {
			if (specificites.indexOfObject(specificite) == NSArray.NotFound) {
				specificites.removeObject(specificite);
			}
		}
	}

	private void callOnAwakeFromInsertionForSpecificites() {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onAwakeFromInsertion(this);
			}
		}
	}

	private void callOnValidateForDeleteOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForDelete(this);
			}
		}
	}

	private void callOnValidateForInsertOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForInsert(this);
			}
		}
	}

	private void callOnValidateForUpdateOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForUpdate(this);
			}
		}
	}

	private void callOnValidateObjectMetierOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateObjectMetier(this);
			}
		}
	}

	private void callOnValidateBeforeTransactionSaveOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateBeforeTransactionSave(this);
			}
		}
	}

	public NSMutableArray<ISpecificite> getSpecificites() {
		return specificites;
	}

	public void setSpecificites(NSMutableArray<ISpecificite> specificites) {
		this.specificites = specificites;
	}

	/**
	 * @return Une map avec en clé le nom de l'attribut et en valeur un integer contenant la taille max de l'attribut.
	 */
	public Map<String, Integer> attributesTaillesMax() {
		if (attributesTaillesMax == null) {
			attributesTaillesMax = CktlCallEOUtilities.attributeMaxSizesForEntityName(this.editingContext(), entityName());
		}
		return attributesTaillesMax;
	}

	public NSArray attributesObligatoires() {
		if (attributesObligatoires == null) {
			attributesObligatoires = CktlCallEOUtilities.requiredAttributeForEntityName(this.editingContext(), entityName());
		}
		return attributesObligatoires;
	}

	/**
	 * Verifie la longueur maximale des champs saisis (a partir de la taille indiquée dans le modèle).
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesLongueursMax() throws NSValidation.ValidationException {
		Iterator iterator = attributesTaillesMax().keySet().iterator();
		//System.out.println(attributesTaillesMax().keySet());
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			//System.out.println("verif :" + getDisplayName(key) + " : " + ((Integer) attributesTaillesMax().get(key)).intValue());
			if (valueForKey(key) != null && ((String) valueForKey(key)).length() > ((Integer) attributesTaillesMax().get(key)).intValue()) {
				throw new NSValidation.ValidationException("La taille du champ " + getDisplayName(key) + " ne doit pas dépasser " + ((Integer) attributesTaillesMax().get(key)).intValue() + " caractères.");
			}
		}
	}

	/**
	 * Vérife si les champs obligatoires sont bien saisis (en fonction de ce qui est indiqué dans le modèle)
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesObligatoires() throws NSValidation.ValidationException {
		Enumeration iterator = attributesObligatoires().objectEnumerator();
		while (iterator.hasMoreElements()) {
			String key = (String) iterator.nextElement();
			if (valueForKey(key) == null) {
				throw new NSValidation.ValidationException("Le champ " + getDisplayName(key) + " est obligatoire.");
			}
		}
	}

	/**
	 * @param propertyName nom de la propriété (attribut ou relation)
	 * @return le nom d'affichage de la propriété. Utilise {@link #displayNames()}.
	 */
	public String getDisplayName(String propertyName) {
		if (displayNames().get(propertyName) != null) {
			return (String) displayNames().get(propertyName);
		}
		return propertyName;
	}

	/**
	 * Renvoie une Map contenant en clé le nom de la propriété et en valeur le nom d'affichage (parlant) de cette propriété. Par exemple <CP, Code
	 * Postal>. Par défaut la Map est vide, il faut surcharger la méthode.
	 */
	public Map displayNames() {
		return new HashMap();
	}

	/**
	 * @param ec
	 * @return le globalID de l'objet à partir d'ec.
	 */
	public EOGlobalID globalID(EOEditingContext ec) {
		return ec.globalIDForObject(this);
	}

	/**
	 * @return le globalID de l'objet à partir de l'editingContext associé. Null si pas d'editingContext.
	 */
	public EOGlobalID globalID() {
		if (editingContext() == null) {
			return null;
		}
		return globalID(this.editingContext());
	}

	/**
	 * @return true si l'objet possede un globalID temporaire (n'existe pas encore dans la base de données).
	 */
	public boolean hasTemporaryGlobalID() {
		return (globalID() != null && globalID() instanceof EOTemporaryGlobalID);
	}

	/**
	 * @return l'EditingContext de validation. S'il n'a pas ete defini,, renvoie
	 *         {@link org.cocktail.fwkcktljavaclientsupport.common.calls.CommonCallCktlEOUtilities#getTopLevelEditingContext(EOEditingContext)}.
	 */
	public EOEditingContext getValidationEditingContext() {
		if (validationEditingContext == null) {
			return CktlCallEOUtilities.getTopLevelEditingContext(editingContext());
		}
		return validationEditingContext;
	}

	public void setValidationEditingContext(EOEditingContext validationEditingContext) {
		this.validationEditingContext = validationEditingContext;
	}

	/**
	 * @return true si l'objet se trouve dans l'editingContext de validation {@link AfwkPersRecord#getValidationEditingContext()}.
	 */
	public boolean isObjectInValidationEditingContext() {
		if (editingContext() == null) {
			return false;
		}
		return (editingContext().equals(getValidationEditingContext()));
	}

	/**
	 * A surcharger. Doit renvoyer true si l'utilisateur a le droit de creer cette instance de l'entite.
	 */
	public boolean hasDroitCreation(Integer persId) {
		return true;
	}

	/**
	 * A surcharger. Doit renvoyer true si l'utilisateur a le droit de modifier une instance de l'entite.
	 */
	public boolean hasDroitModification(Integer persId) {
		return true;
	}

	/**
	 * A surcharger. Doit renvoyer true si l'utilisateur a le droit de supprimer une instance de l'entite.
	 */
	public boolean hasDroitDeletion(Integer persId) {
		return true;
	}

	/**
	 * @return le createur de l'enregistrement (si le champ persIdCreation est renseigne).
	 */
	public PersonneApplicationUser getCreateur() {
		Integer persId = (Integer) valueForKey(PERS_ID_CREATION_KEY);
		if ((createur == null && persId != null) || (createur != null && !createur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				createur = null;
			} else {
				createur = new PersonneApplicationUser(editingContext(), persId);
			}
		}
		return createur;
	}

	/**
	 * @return le modificateur de l'enregistrement (si le champ persIdModification est renseigne).
	 */
	public PersonneApplicationUser getModificateur() {
		Integer persId = (Integer) valueForKey(PERS_ID_MODIFICATION_KEY);
		if ((modificateur == null && persId != null) || (modificateur != null && !modificateur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				modificateur = null;
			} else {
				modificateur = new PersonneApplicationUser(editingContext(), persId);
			}
		}
		return modificateur;
	}

	public void setPersIdModification(Integer persId) {
		takeValueForKey(persId, PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdCreation(Integer persId) {
		takeValueForKey(persId, PERS_ID_CREATION_KEY);
	}

	/**
	 * Recupere le persId depuis la valeur associée à la clé PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY dans ERXThreadStorage et
	 * l'affecte à persIdModification et à persIdCreation si celui-ci est null.
	 */
	public void fixPersIdCreationEtModification() {
		// Ajout des persIdModification à partir du user courant
		Integer persIdModif = (Integer) ERXThreadStorage.valueForKey(PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
		if (persIdModif != null) {
			setPersIdModification(persIdModif);
			if (valueForKey(PERS_ID_CREATION_KEY) == null) {
				setPersIdCreation(persIdModif);
			}
		}
	}

	//	/**
	//	 * @return La liste des valeurs modifiées de l'objet depuis le dernier saveChanges sous forme de NSDictionnary
	//	 * @see com.webobjects.eocontrol.EOCustomObject#changesFromSnapshot(NSDictionary)
	//	 */
	//	public NSDictionary changesFromCommittedSnapshot() {
	//		return super.changesFromCommittedSnapshot();
	//		//return this.changesFromSnapshot(this.editingContext().committedSnapshotForObject(this));
	//	}
	//
	//	/**
	//	 * @param key une clé indiquant un epropriété d'un EO.
	//	 * @return true si la clé passée en paramètre représente une propriété qui a été modifiée depuis le dernier saveChanges.
	//	 */
	//	public boolean hasKeyChangedFromCommittedSnapshot(String key) {
	//		return changesFromCommittedSnapshot().containsKey(key);
	//	}

	/**
	 * @return true si un l'editingContext (ou un des parents) de l'objet est null
	 */
	public boolean editingContextOrParentIsNull() {
		EOEditingContext ec = editingContext();
		if (ec == null) {
			return true;
		}
		while (ec != null && ec.parentObjectStore() instanceof EOEditingContext) {
			ec = (EOEditingContext) ec.parentObjectStore();
		}
		return (ec == null);

	}

	/**
	 * @return true si et seulement si un attribut a été modifié (hors relations) ; false sinon.
	 */
	public boolean hasDirtyAttributes() {
		NSArray<String> attributes = toOneRelationshipKeys().arrayByAddingObjectsFromArray(attributeKeys());
		return !Collections.disjoint(attributes, changesFromCommittedSnapshot().allKeys());
	}
}
