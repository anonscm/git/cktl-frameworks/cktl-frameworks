/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.insee;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CodeInsee {
    
    public static final String INSEE_HOMME = "1";
    public static final String INSEE_FEMME = "2";

    public static final String MOIS_INDETERMINE = "99";

    public static final String DEPARTEMENT_PROTECTORAT = "96";
    public static final String DEPARTEMENT_ETRANGER = "99";
    public static final String DEPARTEMENT_SEINE_ET_OISE = "78";
    public static final String DEPARTEMENT_SEINE = "75";
    public static final String DEPARTEMENT_CORSE = "20";
    public static final String DEPARTEMENT_HAUTE_CORSE = "2B";
    public static final String DEPARTEMENT_CORSE_DU_SUD = "2A";

    public static final String DEPARTEMENT_PARIS = "75";
    public static final String DEPARTEMENT_YVELINES = "78";
    public static final String DEPARTEMENT_ESSONE = "91";
    public static final String DEPARTEMENT_HAUTS_DE_SEINE = "92";
    public static final String DEPARTEMENT_VAL_DE_MARNE = "94";
    public static final String DEPARTEMENT_SEINE_ST_DENIS = "93";
    public static final String DEPARTEMENT_VAL_D_OISE = "95";

    
    public static final String DEPARTEMENT_ALGERIE_ALGER = "91";
    public static final String DEPARTEMENT_ALGERIE_ORAN = "92";
    public static final String DEPARTEMENT_ALGERIE_CONSTANTINE = "93";
    public static final String DEPARTEMENT_ALGERIE_SUD = "94";

    public static final String DEPARTEMENT_PROTECTORAT_MAROCAIN = "95";
    
    public static final String MAYOTTE_A_PARTIR_AVRIL_2009 = "976";
    public static final String MAYOTTE_AVANT_AVRIL_2009 = "985";
    public static final String MAYOTTE = "MAYOTTE";
    
    private static final int ANNEE_1976 = 1976;
    private static final int ANNEE_1969 = 1969;
    
    public static final String DOM = "97";
    public static final String TOM = "98";
    public static List<String> seineEtOise = new ArrayList<String>(); 
    public static List<String> seine = new ArrayList<String>(); 
    public static List<String> algerieFrancaise = new ArrayList<String>(); 
    public static List<String> corse = new ArrayList<String>(); 
    
    
    static {
        
        seineEtOise.add(DEPARTEMENT_YVELINES);
        seineEtOise.add(DEPARTEMENT_ESSONE);
        seineEtOise.add(DEPARTEMENT_HAUTS_DE_SEINE);
        seineEtOise.add(DEPARTEMENT_SEINE_ST_DENIS);
        seineEtOise.add(DEPARTEMENT_VAL_DE_MARNE);
        seineEtOise.add(DEPARTEMENT_VAL_D_OISE);
               
        seine.add(DEPARTEMENT_PARIS);
        seine.add(DEPARTEMENT_HAUTS_DE_SEINE);
        seine.add(DEPARTEMENT_SEINE_ST_DENIS);
        seine.add(DEPARTEMENT_VAL_DE_MARNE);

        algerieFrancaise.add(DEPARTEMENT_ALGERIE_ALGER);
        algerieFrancaise.add(DEPARTEMENT_ALGERIE_ORAN);
        algerieFrancaise.add(DEPARTEMENT_ALGERIE_CONSTANTINE);
        algerieFrancaise.add(DEPARTEMENT_ALGERIE_SUD);
        
        corse.add(DEPARTEMENT_HAUTE_CORSE);
        corse.add(DEPARTEMENT_CORSE_DU_SUD);
        
    }
    
    private String code = "";
    private String sexe = "";
    private String annee = "";
    private String mois =  "";
    private String departement = "";
    private String pays = "";
    
    private boolean domTom = false;
    
    public static boolean isDepartementSeineEtOise(int annee, String departement) {
//        if(annee >= 1969) { return false; }
        if (annee >= ANNEE_1969) { return false; }
        return seineEtOise.contains(departement);
    }

    public static boolean isDepartementSeine(int annee, String departement) {
//        if (annee >= 1969) { return false; }
        if (annee >= ANNEE_1969) { return false; }
        return seine.contains(departement);
    }

    public static boolean isDepartementCorseApresDivision(int annee, String departement) {
//        if (annee >= 1976) { return corse.contains(departement); }
        if (annee >= ANNEE_1976) { return corse.contains(departement); }
        
        return false;
    }

    public static boolean isDepartementCorseAvantDivision(int annee, String departement) {
//        if (annee < 1976) { return DEPARTEMENT_CORSE.equals(departement); }
        if (annee < ANNEE_1976) { return DEPARTEMENT_CORSE.equals(departement); }
        return false;
    }
    
    protected CodeInsee() { }

    public CodeInsee(String code) {
        this.code = code;
        sexe = code.substring(0, 1);
        annee = code.substring(1, 3);
        mois = code.substring(3, 5);
        departement = code.substring(5, 7);
//        if(DOM.equals(departement) ||  TOM.equals(departement)) {
//            departement=code.substring(5,7);
//            domTom = true;
//        }
        if (DOM.equals(departement) ||  TOM.equals(departement)) {
        	departement = code.substring(5, 8);
        	domTom = true;
        }
        pays = code.substring(7, 10);
    }
    
    /**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	public String getSexe() {
        return sexe;
    }
    
    public boolean isHomme() {
        return sexe.equals(INSEE_HOMME);
    }
    
    public boolean isFemme() {
        return sexe.equals(INSEE_FEMME);
    }
    
    public String getAnnee() {
        return annee;
    }

    public String getMois() {
        return mois;
    }

    public String getDepartement() {
        return departement;
    }

    public String getPays() {
        return pays;
    }

    public boolean isSeineEtOise() {
        if (Integer.valueOf(annee) > 68) { return false; }
        
        return DEPARTEMENT_SEINE_ET_OISE.equals(departement);
    }

    public boolean isSeine() {
        if (Integer.valueOf(annee) > 68) { return false; }
        
        return DEPARTEMENT_SEINE.equals(departement);
    }
    
    public boolean isHorsFrance() {
        return isProtectorat() || isEtranger() || isAlgerieFrancaiseEtProtectoratMarocain();
    }

    public boolean isProtectorat() {
        return DEPARTEMENT_PROTECTORAT.equals(departement);
    }

    public boolean isEtranger() {
        return DEPARTEMENT_ETRANGER.equals(departement);
    }

    public boolean isAlgerieFrancaiseEtProtectoratMarocain() {
        
        return isAlgerieFrancaise() || isProtectoratMarocain();
    }
    

    public boolean isMoisIndetermine() {
        return MOIS_INDETERMINE.equals(getMois());
    }

    public boolean isDomTom() {
        return domTom;
    }

    public boolean isAlgerieFrancaise() {
        return algerieFrancaise.contains(departement);
    }

    public boolean isProtectoratMarocain() {
        return DEPARTEMENT_PROTECTORAT_MAROCAIN.equals(departement);
    }

    public boolean isCorseApresDivision() {
        return corse.contains(departement);
    }

    public boolean isCorseAvantDivision() {
        return DEPARTEMENT_CORSE.equals(departement);
    }
    
    public boolean isMayotte() {
    	return MAYOTTE_A_PARTIR_AVRIL_2009.equals(departement) || MAYOTTE_AVANT_AVRIL_2009.equals(departement);
    }
    
    public static Number calculeClefinsee(String numero) {
        String nouveauNumero = numero.toUpperCase();
        String nombreASoustraire = null; // Pour la Corse, si c'est 2A ou 2B, il faut retirer des valeurs
        if (nouveauNumero.indexOf("A") > 0) {
            nombreASoustraire = "100";
        } else if (nouveauNumero.indexOf("B") > 0) {
                nombreASoustraire = "200";
            }
        nouveauNumero = nouveauNumero.replaceAll("B", "0");
        nouveauNumero = nouveauNumero.replaceAll("A", "0");
        BigInteger bigInt1;
        try {
        	bigInt1 = new BigInteger(nouveauNumero.substring(0, 9));
        } catch (NumberFormatException nfe) {
        	return -1;
        }
        if (nombreASoustraire != null) {
            bigInt1 = bigInt1.subtract(new BigInteger(nombreASoustraire));
        }
        String s = "" + ((bigInt1.mod(new BigInteger("97"))).intValue()) + (nouveauNumero.substring(9, 13));
        BigInteger bigInt2 = new BigInteger(s);

        return new Integer(97 - ((bigInt2.mod(new BigInteger("97"))).intValue()));
    }


}
