package org.cocktail.fwkcktlpersonne.common.metier.filtre;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.predicate.GdProfilPredicate;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

/**
 * 
 * Responsabilité de la classe
 * - Filtrer en mémoire des profils
 *
 */
public class GdProfilFiltre {

	private GdProfilPredicate profilPredicate = new GdProfilPredicate();
	
	public GdProfilFiltre() {
		super();
	}

	/**
	 * Filtre les profils selon les ids
	 * @param profils
	 * @param ids
	 * @return liste de profils filtrée
	 */
	public List<EOGdProfil> filtrerParIds(List<EOGdProfil> profils, List<Integer> ids) {
		return Lists.newArrayList(Collections2.filter(profils, profilPredicate.buildFiltreIds(ids)));
	}
	
	
}
