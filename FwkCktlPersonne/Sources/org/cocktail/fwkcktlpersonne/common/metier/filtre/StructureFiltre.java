package org.cocktail.fwkcktlpersonne.common.metier.filtre;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.predicate.StructurePredicate;

import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * 
 * Responsabilité de la classe :
 * - Filtrer une liste de IStructure en fonction de différents critères
 *
 */
public class StructureFiltre {

	private StructurePredicate structurePredicate = new StructurePredicate();
	
	/**
	 * Filtre selon le searchTerm (case insensitive)
	 * @param listeStructure liste en mémoire
	 * @param searchTerm texte recherché
	 * @return List<IStructure>
	 */
	public List<IStructure> filtrerParNomCompletAffichage(List<IStructure> listeStructure, String searchTerm) {
		return Lists.newArrayList(Collections2.filter(listeStructure, structurePredicate.buildContainsIgnoreCaseNomAffichage(searchTerm)));
	}
	
	/**
	 * Filtre selon le cStructure
	 * @param listeStructure liste en mémoire
	 * @param codeStructure cStructure
	 * @return IStructure
	 */
	public IStructure filtreParCodeStructure(List<IStructure> listeStructure, String codeStructure) {
		List<IStructure> listeFiltree = Lists.newArrayList(Collections2.filter(listeStructure, structurePredicate.buildFiltrerCodeStructure(codeStructure)));
		if (listeFiltree.size() > 0) {
			return (IStructure) Iterables.getOnlyElement(listeFiltree);
		} else {
			return null;
		}
	}
	
	/**
	 * Filtre les structures archivées
	 * @param listeStructure liste en mémoire
	 * @return List<IStructure>
	 */
	public List<IStructure> filtrerParIsArchivee(List<IStructure> listeStructure) {
		return Lists.newArrayList(Collections2.filter(listeStructure, structurePredicate.buildFiltrerArchive()));
	}
	
	
}
