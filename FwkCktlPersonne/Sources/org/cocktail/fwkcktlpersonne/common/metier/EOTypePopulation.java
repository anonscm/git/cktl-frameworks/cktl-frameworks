/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypePopulation;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOTypePopulation extends _EOTypePopulation implements ITypePopulation {

	public static final String TYPE_ENSEIGNANT_CHERCHEUR = "SA";
	public static final String TYPE_ITRF = "IA";
	
	public static final EOSortOrdering SORT_LIBELLE_LONG_ASC = new EOSortOrdering(LL_TYPE_POPULATION_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_LIBELLE_LONG_ASC = new NSArray(SORT_LIBELLE_LONG_ASC);

	public static final EOSortOrdering SORT_LIBELLE_COURT_ASC = new EOSortOrdering(LC_TYPE_POPULATION_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_LIBELLE_COURT_ASC = new NSArray(SORT_LIBELLE_COURT_ASC);

	public static final String ENSEIGNANT_CHERCHEUR = "SA";
	public static final String NORMALIEN = "N";

    public EOTypePopulation() {
        super();
    }
    
	public static EOTypePopulation rechercherTypePopulation(EOEditingContext ec,String codeType) {
		EOQualifier qualifier =  ERXQ.equals(C_TYPE_POPULATION_KEY, codeType);
		return fetchFirstByQualifier(ec, qualifier);
	}

public static NSArray fetch(EOEditingContext ec, NSTimestamp dateRef) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VISIBLE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));		
		
		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE_LONG_ASC);
	}
	
	public String toString() {
		return cTypePopulation() + "     \r- " + llTypePopulation();		
	}

	public boolean isLocal() {
		return false;
	}

	public boolean estVisible() {
		return temVisible().equals(CocktailConstantes.VRAI);
	}
	public void setEstVisible(Boolean yn) {
		if (yn)
			setTemVisible(CocktailConstantes.VRAI);
		else
			setTemVisible(CocktailConstantes.FAUX);
	}

	public boolean estItarf() {
		return temItarf() != null && temItarf().equals(CocktailConstantes.VRAI);
	}
	public boolean estBiblio() {
		return temBiblio() != null && temBiblio().equals(CocktailConstantes.VRAI);
	}
	public boolean est1erDegre() {
		return tem1Degre() != null && tem1Degre().equals(CocktailConstantes.VRAI);
	}
	public boolean est2Degre() {
		return tem2Degre() != null && tem2Degre().equals(CocktailConstantes.VRAI);
	}
	public boolean estAtos() {
		return temAtos() != null && temAtos().equals(CocktailConstantes.VRAI);
	}
	public boolean estEnseignant() {
		return temEnseignant() != null && temEnseignant().equals(CocktailConstantes.VRAI);
	}
	public boolean estFonctionnaire() {
		return temFonctionnaire() != null && temFonctionnaire().equals(CocktailConstantes.VRAI);
	}
	public boolean estHospitalier() {
		return temHospitalier() != null && temHospitalier().equals(CocktailConstantes.VRAI);
	}
	public boolean estNormalien() {
		return cTypePopulation().equals(NORMALIEN);
	}
	// interface RecordAvecLibelleEtCode
	public String libelle() {
		return lcTypePopulation();
	}
	public String code() {
		return cTypePopulation();
	}
	public String codeEtLibelle() {
		return cTypePopulation() + " - " + lcTypePopulation();
	}

	
	
	/** recherche le type de population avec le libelle fourni
	 * @param editingContext
	 * @param libelle
	 * @return EOTypePopulation
	 */
	public static EOTypePopulation rechercherTypePopulationAvecLibelle(EOEditingContext ec,String libelle) {
		try {
			NSMutableArray args = new NSMutableArray(libelle);
			args.addObject(libelle);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(LC_TYPE_POPULATION_KEY + " = %@ OR llTypePopulation =  = %@", args);
		
			return fetchFirstByQualifier(ec, myQualifier);
		} catch (Exception e) {
			return null;
		}
	}
	/** recherche les types de population avec les codes fournis. Si le tableau est nul ou vide, retourne tous les types de population
	 * @param editingContext
	 * @param codesPopulation liste de codes population
	 * @return tableau de EOTypePopulation tri&eacute; par ordre alphab&eacute;tique sur les lib&eacute;ll&eacute;s courts
	 */
	public static NSArray rechercherTypePopulationAvecCodes(EOEditingContext editingContext,NSArray codesPopulation) {

		EOQualifier qualifier = null;
		if (codesPopulation != null && codesPopulation.count() > 0) {
			NSMutableArray args = new NSMutableArray(codesPopulation.objectAtIndex(0));
			String stringQualifier = C_TYPE_POPULATION_KEY + " = %@";
			for (int i = 1; i < codesPopulation.count(); i++) {
				args.addObject(codesPopulation.objectAtIndex(i));
				stringQualifier = stringQualifier + " OR cTypePopulation = %@";
			}
			qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		}
		
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(LC_TYPE_POPULATION_KEY, EOSortOrdering.CompareAscending);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qualifier, new NSArray(sort));
		
		return editingContext.objectsWithFetchSpecification(myFetch);

	}
		
	

    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

//    /**
//     * Apparemment cette methode n'est pas appel̩e.
//     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
//     * @throws une exception de validation
//     */
//    @Override
//    public void validateForSave() throws NSValidation.ValidationException {
//        validateObjectMetier();
//        validateBeforeTransactionSave();
//        super.validateForSave();
//        
//    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
    
 // ajouts

    /**
     * @return true si c'est une population rattachée aux ATOS
     */
	public boolean isAenes() {
		boolean isAenes = false;

		if (!StringCtrl.isEmpty(temAtos())
				&& temAtos().equals(OUI)) {
			isAenes = true;
		}

		return isAenes;
	}

	/**
	 * 
	 * @return true si c'est une population rattachée aux ITARF
	 */
	public boolean isItrf() {
		boolean isItrf = false;

		if (!StringCtrl.isEmpty(temItarf())
				&& temItarf().equals(OUI)) {
			isItrf = true;
		}

		return isItrf;
	}

	/**
	 * 
	 * @return true si c'est une population rattachée aux bibliothèques
	 */
	public boolean isBu() {
		boolean isBu = false;

		if (!StringCtrl.isEmpty(temBiblio())
				&& temBiblio().equals(OUI)) {
			isBu = true;
		}

		return isBu;
	}
}
