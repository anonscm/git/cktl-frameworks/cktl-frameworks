/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOGrhumParametres.java
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Enumeration;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.CStructureValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.CharactersListValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.CodeActivationValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.DateValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.DomaineValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.EmailValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.FreeTextValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.HtmlValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.IdValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.ListeValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.LoginValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.NomDeClasseValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.PathValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.PeriodeValiditeValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.PersIdValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.SchoolYearValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.ServeurValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TcpipValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TimeZoneValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.URLValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.ValeurNumeriqueValidateur;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.Validateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOGrhumParametres extends _EOGrhumParametres {
	
	private static final long serialVersionUID = 8605036277271856623L;
	public static final String PARAM_ANNUAIRE_FOU_COMPTE_AUTO = "ANNUAIRE_FOU_COMPTE_AUTO";
	public static final String PARAM_GRHUM_CREATEUR = "GRHUM_CREATEUR";
	public static final String PARAM_GRHUM_CP_OBLIGATOIRE = "GRHUM_CP_OBLIGATOIRE";

	public static final String OUI = "OUI";
	public static final String PARAM_ANNUAIRE_LOGIN_MAX = "ANNUAIRE_LOGIN_MAX";
	public static final String PARAM_ANNUAIRE_LOGIN_MIN = "ANNUAIRE_LOGIN_MIN";
	public static final String PARAM_ANNUAIRE_LOGIN_PATTERN = "ANNUAIRE_LOGIN_PATTERN";
	public static final String PARAM_ANNUAIRE_LOGIN_PATTERN_NOT_MATCH_MSG = "ANNUAIRE_LOGIN_PATTERN_NOT_MATCH_MSG";
	public static final String PARAM_ANNUAIRE_EMPLOYEUR = "ANNUAIRE_EMPLOYEUR";
	public static final String PARAM_ANNUAIRE_ETABLISSEMENTS_ETRANGERS = "ANNUAIRE_ETABLISSEMENTS_ETRANGERS";

	public static final String PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER = "ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER";
	public static final String PARAM_ANNUAIRE_ARCHIVES = "ANNUAIRE_ARCHIVES";

	public static final String PARAM_GRHUM_DOMAINE_PRINCIPAL = "GRHUM_DOMAINE_PRINCIPAL";
	public static final String PARAM_GRHUM_VLAN_EXTERNE_KEY = "GRHUM_VLAN_EXTERNE";
	public static final String PARAM_GRHUM_VLAN_ETUD_KEY = "GRHUM_VLAN_ETUD";
	public static final String PARAM_GRHUM_VLAN_ADMIN_KEY = "GRHUM_VLAN_ADMIN";

	public static final String PARAM_GRHUM_CONTROLE_SIRET = "GRHUM_CONTROLE_SIRET";
	public static final String PARAM_GRHUM_RH = "GRHUM_RH";
	public static final String PARAM_GRHUM_PEDA = "GRHUM_PEDA";
	public static final String PARAM_GRHUM_PHOTO = "GRHUM_PHOTO";
//	public static final String PARAM_RH_TROMBI = "org.cocktail.personne.groupe.autorise";
	public static final String PARAM_GRHUM_ANNEE_UNIVERSITAIRE = "GRHUM_ANNEE_UNIVERSITAIRE";

	public static NSMutableDictionary<String, Class<? extends Validateur>> choixVerifications = new NSMutableDictionary<String, Class<? extends Validateur>>();
	public static final String PARAM_GRHUM_DEFAULT_GID_KEY = "GRHUM_DEFAULT_GID";
	public static final String PARAM_GRHUM_DEFAULT_UID_KEY = "GRHUM_DEFAULT_UID";
	public static final String PARAM_GRHUM_UID_MIN_KEY = "GRHUM_UID_MIN";
	public static final String PARAM_GRHUM_UID_MAX_KEY = "GRHUM_UID_MAX";
	public static final String PARAM_GRHUM_GID_MIN_KEY = "GRHUM_GID_MIN";
	public static final String PARAM_GRHUM_GID_MAX_KEY = "GRHUM_GID_MAX";

	public static final String PARAM_GRHUM_HOME = "org.cocktail.grhum.compte.home";
	public static final String PARAM_GRHUM_SHELL = "org.cocktail.grhum.compte.shell";
	
//	public static final String PARAM_GRHUM_DOMAINES_SECONDAIRES = "org.cocktail.grhum.compte.domainessecondaires";

	public static final String PARAM_GRHUM_ETAB_KEY = "GRHUM_ETAB";
	
	static {
		choixVerifications.setObjectForKey(ValeurNumeriqueValidateur.class, EOGrhumParametresType.valeurNumerique);
		choixVerifications.setObjectForKey(CodeActivationValidateur.class, EOGrhumParametresType.codeActivation);
		choixVerifications.setObjectForKey(DomaineValidateur.class, EOGrhumParametresType.nomDomaine);
		choixVerifications.setObjectForKey(URLValidateur.class, EOGrhumParametresType.uRL);
		choixVerifications.setObjectForKey(HtmlValidateur.class, EOGrhumParametresType.html);
		choixVerifications.setObjectForKey(NomDeClasseValidateur.class, EOGrhumParametresType.nomClasse);
		choixVerifications.setObjectForKey(CharactersListValidateur.class, EOGrhumParametresType.listeCaracteres);
		choixVerifications.setObjectForKey(PathValidateur.class, EOGrhumParametresType.chemin);
		choixVerifications.setObjectForKey(FreeTextValidateur.class, EOGrhumParametresType.texteLibre);
		choixVerifications.setObjectForKey(SchoolYearValidateur.class, EOGrhumParametresType.anneeUniversitaire);
		choixVerifications.setObjectForKey(TcpipValidateur.class, EOGrhumParametresType.tcpip);
		choixVerifications.setObjectForKey(PeriodeValiditeValidateur.class, EOGrhumParametresType.periodesValidite);
		choixVerifications.setObjectForKey(TimeZoneValidateur.class, EOGrhumParametresType.fuseauHoraire);
		choixVerifications.setObjectForKey(PersIdValidateur.class, EOGrhumParametresType.idPersonnel);
		choixVerifications.setObjectForKey(CStructureValidateur.class, EOGrhumParametresType.cStructure);
		choixVerifications.setObjectForKey(ListeValidateur.class, EOGrhumParametresType.liste);
		choixVerifications.setObjectForKey(LoginValidateur.class, EOGrhumParametresType.identifiant);
		choixVerifications.setObjectForKey(EmailValidateur.class, EOGrhumParametresType.messageElectronique);
		choixVerifications.setObjectForKey(DateValidateur.class, EOGrhumParametresType.dateFormatee);
		choixVerifications.setObjectForKey(IdValidateur.class, EOGrhumParametresType.clefTable);
		choixVerifications.setObjectForKey(ServeurValidateur.class, EOGrhumParametresType.serveur);
	}

	public EOGrhumParametres() {
		super();
	}
	
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		setDModification(MyDateCtrl.now());
		Validateur validateur;
		try {
			validateur = getValidateurForType(toParametresType().typeIdInterne());
			validateur.valider(paramValue());
			fixPersIdCreationEtModification();
			// Integer persIdModif = (Integer)
			// ERXThreadStorage.valueForKey(PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
			// if (persIdModif != null) {
			// setPersIdModification(persIdModif);
			// //setDModification(new NSTimestamp());
			// }
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setDCreation(MyDateCtrl.now());
	}

	private Validateur getValidateurForType(String typeIdInterne) throws InstantiationException, IllegalAccessException {
		Class<? extends Validateur> clazz = choixVerifications.objectForKey(typeIdInterne);
		return clazz.newInstance();
	}

	/** retourne la valeur dest param&egrave;tres GRHUM dans un dictionnaire */
	public static NSDictionary parametresGRhum(EOEditingContext editingContext) {
		EOFetchSpecification fs = new EOFetchSpecification(EOGrhumParametres.ENTITY_NAME, null, null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		NSMutableDictionary dict = new NSMutableDictionary();
		Enumeration e = results.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGrhumParametres param = (EOGrhumParametres) e.nextElement();
			dict.setObjectForKey(param.paramValue(), param.paramKey());
		}
		return dict;
	}

	public static String parametrePourCle(EOEditingContext editingContext, String cle) {
		return parametrePourCle(editingContext, cle, null);
	}

	public static String parametrePourCle(EOEditingContext editingContext, String cle, String defaultValue) {
		NSArray res = parametresPourCle(editingContext, cle);
		// gere les retour en keyValueCodingNull
		if (res.count() > 0 && (res.objectAtIndex(0) instanceof String)) {
			return (String) res.objectAtIndex(0);
		}
		return defaultValue;
	}

	/**
	 * @param editingContext
	 * @param cle
	 * @return Un tableau de String correspondant aux valeurs trouvées pour la
	 *         cle specifiee en parametre.
	 */
	public static NSArray parametresPourCle(EOEditingContext editingContext, String cle) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + " = %@", new NSArray(cle));
		EOFetchSpecification fs = new EOFetchSpecification(EOGrhumParametres.ENTITY_NAME, qualifier, null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		return (NSArray) results.valueForKeyPath(EOGrhumParametres.PARAM_VALUE_KEY);
	}

	/**
	 * @param ec
	 * @param paramKey
	 * @deprecated Utilisez
	 *             {@link EOGrhumParametres#parametrePourCle(EOEditingContext, String)}
	 *             à la place.
	 */
	@Deprecated
	public static String getParamValue(EOEditingContext ec, String paramKey) {
		return parametrePourCle(ec, paramKey);
	}
	
	/**
	 * Edite la valeur du parametre ainsi que la date de modification et le persId l'ayant modifie
	 * 
	 * @param paramValue la nouvelle valeur du parametre
	 * @param persId le persId de l'utilisateur faisant la modification
	 * @return EOGhrumParametres
	 */
	public EOGrhumParametres setParamValue(String paramValue, Integer persId) {
		
		setParamValue(paramValue);
		setPersIdModification(persId);
		setDModification(new NSTimestamp());
		
		return this;
	}
	
	/**
	 * Recupere le premier GrhumParametre qui est Grhum_Createur
	 * @param editingContext
	 * @return
	 */
	public static EOGrhumParametres getFirstGrhumCreateur(EOEditingContext editingContext) {
		return EOGrhumParametres.fetchFirstByQualifier(editingContext, EOGrhumParametres.PARAM_KEY.eq("GRHUM_CREATEUR"));
	}
	
	/**
	 * Insere les date de creation et de modification ainsi que le persId de creation et de modification
	 * @param persId
	 * @return
	 */
	public EOGrhumParametres setDefaultGrhumParametre(Integer persId) {
		setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		setPersIdCreation(persId);
		setPersIdModification(persId);
		return this;
	}
	
}
