/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.List;

import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOVReferens extends _EOVReferens {

    public EOVReferens() {
        super();
    }
   

    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     * @throws une exception de validation
     */
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
	// methodes rajoutees

	public final static String KEY_ROOT = "-1";

	public final static CktlSort SORT_LIBELLE = CktlSort.newSort(LIBELLE_KEY);

	public static final EOSortOrdering Z_SORT_LIBELLE = EOSortOrdering.sortOrderingWithKey(
			EOVReferens.LIBELLE_KEY, EOSortOrdering.CompareAscending);

	// ignorer la racine pour l'affichage des fils
	public final static EOQualifier QUAL_IGNORE_ROOT = CktlDataBus.newCondition("key <> '" + KEY_ROOT + "'");

	// ignorer les DCP archives
	public static EOQualifier QUAL_IGNORE_ARCHIVE = null;

	static {
		String strQualIgnoreArchive = "";
		for (int i = 0; i < EOReferensDcp.LIST_NUMDCP_ARCHIVE.count(); i++) {
			strQualIgnoreArchive += EOVReferens.KEY_KEY + "<>'" + EOReferensDcp.LIST_NUMDCP_ARCHIVE.objectAtIndex(i) + "'";
			if (i < EOReferensDcp.LIST_NUMDCP_ARCHIVE.count() - 1) {
				strQualIgnoreArchive += " and ";
			}
		}
		QUAL_IGNORE_ARCHIVE = CktlDataBus.newCondition(strQualIgnoreArchive);
	}

	// ignorer les objets provenant des DPC archives
	public static EOQualifier QUAL_IGNORE_FROM_DCP_ARCHIVE = null;

	static {
		String strQualIgnoreFromDcpArchive = "";
		for (int i = 0; i < EOReferensDcp.LIST_NUMDCP_ARCHIVE.count(); i++) {
			strQualIgnoreFromDcpArchive += EOVReferens.TO_REFERENS_DCP_KEY + "." + EOReferensDcp.NUM_DCP_KEY + "<>'" + EOReferensDcp.LIST_NUMDCP_ARCHIVE.objectAtIndex(i) + "'";
			if (i < EOReferensDcp.LIST_NUMDCP_ARCHIVE.count() - 1) {
				strQualIgnoreFromDcpArchive += " and ";
			}
		}
		QUAL_IGNORE_FROM_DCP_ARCHIVE = CktlDataBus.newCondition(strQualIgnoreFromDcpArchive);
	}

	public static final EOQualifier QUAL_IGNORE_ROOT_AND_ARCHIVE = new EOAndQualifier(new NSArray(
			new EOQualifier[] {QUAL_IGNORE_ROOT, QUAL_IGNORE_ARCHIVE }));

	public static final String PREFIX_AUTRES = "z__";

	public static final String MENTION_NOUVELLE_NOMENCLATURE = "(nouvelle nomenclature)";
	public static final String MENTION_NE_PLUS_UTILISER = "_NE_PLUS_UTILISER_";

	// niveau de la vue contenant les emplois types
	public static final int NIVEAU_DCP = 1;
	public static final int NIVEAU_FAMILLE = 2;
	public static final int NIVEAU_EMPLOI = 3;
	public static final int NIVEAU_ACTIVITE = 4;
	public static final int NIVEAU_COMPETENCE = 5;

	/**
	 * @param ec Editing Context pour rechercher la racine des données
	 * @return Donne la racine des donnees referens
	 */
	public static EOVReferens findRoot(EOEditingContext ec) {
		return EOVReferens.fetchByQualifier(ec,
				CktlDataBus.newCondition(KEY_KEY + "=" + KEY_PERE_KEY));
	}

	/**
	 * La recherche effectue une recherche "caseInsensitiveLike". L'enregistrement
	 * est selectionne dans la vue <i>VReferens</i>. On ne recherche pas sur la
	 * racine
	 * 
	 * @param ec Editing Context de la recherche
	 * @param libelle libellé de la recherche
	 * @param niveau niveau de la recherche
	 * @return une liste correspondant à la recherche
	 */
	public static NSArray findVReferensLibelleSeulLike(EOEditingContext ec, String libelle, int niveau) {
		// construire un qualifier avec tous les mots separes par des '_'
		NSArray motList = NSArray.componentsSeparatedByString(libelle, "_");
		String strQual = QUAL_IGNORE_FROM_DCP_ARCHIVE + " and " + NIVEAU_KEY + "=" + niveau;
		for (int i = 0; i < motList.count(); i++) {
			String mot = (String) motList.objectAtIndex(i);
			if (!StringCtrl.isEmpty(mot)) {
				if (i == 0) {
					strQual += " and (";
				}
				strQual += LIBELLE_SEUL_KEY + " caseInsensitiveLike '*" + mot + "*'";
				if (i < motList.count() - 1) {
					strQual += " and ";
				} else {
					strQual += ")";
				}
			}
		}
		return fetchAll(ec, CktlDataBus.newCondition(strQual), SORT_LIBELLE);
	}

	/**
	 * Retourne le chemin de l'activite representee dans l'enregistrement
	 * <code>record</code>. Le chemin est constitue des <code>EOVReferens</code>
	 * de l'entite <code>VReferens</code>.
	 * @param record enregistrement dont on veut connaître le chemin
	 * @return le chemin de l'activite representee dans l'enregistrement
	 */
	public static NSArray findReferensPath(EOVReferens record) {
		NSArray path = new NSArray();
		String clePere;
		EOVReferens rec;

		if (record != null && !record.key().equals(KEY_ROOT)) {
			clePere = record.keyPere();
			path = path.arrayByAddingObject(record);
		} else {
			clePere = KEY_ROOT;
		}
		while (!clePere.equals(KEY_ROOT)) {
			rec = fetchByQualifier(record.editingContext(), CktlDataBus.newCondition(KEY_KEY + " = '" + clePere + "'"));
			if (rec == null) {
				break;
			}
			clePere = rec.keyPere();
			path = path.arrayByAddingObject(rec);
		}
		// on inverse tout ca pour avoir le sens pere -> fils
		NSArray revertPath = new NSArray();
		for (int i = 0; i < path.count(); i++) {
			revertPath = revertPath.arrayByAddingObject(path.objectAtIndex(path.count() - i - 1));
		}	
		return revertPath;
	}

	/**
	 * @param record
	 * @return
	 */
	public static EOVReferens findVReferensForReferensEmploi(EOReferensEmplois record) {
		return EOVReferens.fetchByQualifier(record.editingContext(),
				CktlDataBus.newCondition(
						NIVEAU_KEY + " = " + NIVEAU_EMPLOI + " and " + EOVReferens.TO_REFERENS_EMPLOIS_KEY + "=%@",
						new NSArray(record)));
	}

	/**
	 * @param record enregistrement de type EOReferensActivites à trouver
	 * @return un objet de type EOREfrens en lien avec le paramètre
	 */
	public static EOVReferens findVReferensForReferensActivites(EOReferensActivites record) {
		return EOVReferens.fetchByQualifier(record.editingContext(),
				CktlDataBus.newCondition(
						NIVEAU_KEY + " = " + NIVEAU_ACTIVITE + " and " + EOVReferens.TO_REFERENS_ACTIVITES_KEY + "=%@",
						new NSArray(record)));
	}

	/**
	 * Méthode pour améliorer les perfs de Feve
	 * @param edc
	 * @param records
	 * @return
	 */
	public static NSArray<EOVReferens> findListVReferensForReferensActivites(EOEditingContext edc, NSArray<EOReferensActivites> records) {
		if (!NSArrayCtrl.isEmpty(records)) {
			NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			for (EOReferensActivites eoReferensActivites : records) {
				quals.add(CktlDataBus.newCondition(
						NIVEAU_KEY + " = " + NIVEAU_ACTIVITE + " and " + EOVReferens.TO_REFERENS_ACTIVITES_KEY + "=%@", new NSArray(eoReferensActivites)));
			}
			return EOVReferens.fetchAll(edc, ERXQ.or(quals), null);
		} else {
			return new NSArray<EOVReferens>();
		}
		
	}
	
	/**
	 * @param record enregistrement de type EOReferensCompetences à trouver
	 * @return un objet de type EOREfrens en lien avec le paramètre
	 */
	public static EOVReferens findVReferensForReferensCompetences(EOReferensCompetences record) {
		return EOVReferens.fetchByQualifier(record.editingContext(),
				CktlDataBus.newCondition(
						NIVEAU_KEY + " = " + NIVEAU_COMPETENCE + " and " + EOVReferens.TO_REFERENS_COMPETENCES_KEY + "=%@",
						new NSArray(record)));
	}

	/**
	 * Méthode pour améliorer les perfs de Feve
	 * @param edc
	 * @param records
	 * @return
	 */
	public static NSArray<EOVReferens> findListVReferensForReferensCompetences(EOEditingContext edc, NSArray<EOReferensCompetences> records) {
		if (!NSArrayCtrl.isEmpty(records)) {
			NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			for (EOReferensCompetences eoReferensCompetences : records) {
				quals.add(CktlDataBus.newCondition(
						NIVEAU_KEY + " = " + NIVEAU_COMPETENCE + " and " + EOVReferens.TO_REFERENS_COMPETENCES_KEY + "=%@", new NSArray(eoReferensCompetences)));
			}
			return EOVReferens.fetchAll(edc, ERXQ.or(quals), null);
		} else {
			return new NSArray<EOVReferens>();
		}
		
	}
	
	
	// ajouts pour les nodes

	public static final EOQualifier QUAL_RACINE = CktlDataBus.newCondition(
			KEY_PERE_KEY + "=\"" + KEY_ROOT + "\"");

	/**
	 * @param edc Editing context pour rechercher les racines
	 * @return Les objet EOVReferens definies comme racines de l'arbre des
	 *         associations.
	 */
	public static NSArray getRacines(EOEditingContext edc) {
		return fetchAll(edc, QUAL_RACINE, new NSArray(new Object[] {
				Z_SORT_LIBELLE
		}));
	}

	/**
	 * @return Les objets {@link EOVReferens} fils de l'objet en cours. on enleve
	 *         la racine elle meme
	 */
	public NSArray getFils() {
		NSMutableArray filsMutable = new NSMutableArray(tosVReferensFils());
		filsMutable.removeIdenticalObject(this);
		NSArray fils = CktlSort.sortedArray(filsMutable.immutableClone(), EOVReferens.LIBELLE_SEUL_KEY);
		return fils;
	}

	/**
	 * @param ec Editing Context de la recherche
	 * @param qual propriété discriminante de la recherche
	 * @param fetchLimit nombre limitant le nombre de résultat
	 *          pris en compte si >0
	 * @return Les formation en vigueur dependant du qualifier specifie.
	 */
	public static NSArray rechercher(EOEditingContext ec, EOQualifier qual, int fetchLimit) {
		NSMutableArray array = new NSMutableArray();
		NSTimestamp now = DateCtrl.now();
		array.add(QUAL_IGNORE_FROM_DCP_ARCHIVE);
		if (qual != null) {
			array.addObject(qual);
		}
		EOFetchSpecification spec = new EOFetchSpecification(
				EOVReferens.ENTITY_NAME,
				new EOAndQualifier(array),
				new NSArray(Z_SORT_LIBELLE));
		if (fetchLimit > 0) {
			spec.setFetchLimit(fetchLimit);
		}
		spec.setUsesDistinct(true);
		NSArray res = ec.objectsWithFetchSpecification(spec);
		return res;
	}

}
