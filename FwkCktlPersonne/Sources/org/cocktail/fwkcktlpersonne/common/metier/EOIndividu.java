/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// EOIndividuUlr.java
//
package org.cocktail.fwkcktlpersonne.common.metier;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForEtudiantSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForPersonnelSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForUtilisateurSISpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IDepartement;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPhoto;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.manager.FwkPersonneManagerImpl;
import org.cocktail.fwkcktlpersonne.common.metier.manager.IFwkPersonneManager;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.qualifiers.ERXAndQualifier;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Classe permettant la gestion des objets Individus.<br/>
 * <ul>
 * <li>Règles d'initialisation : {@link EOIndividu#awakeFromInsertion(EOEditingContext)}</li>
 * <li>Règles de validation : {@link EOIndividu#validateObjectMetier()}</li>
 * </ul>
 * Si des spécificités sont rattachées à un objet EOIndividu, d'autres règles doivent être respectées (
 * {@link EOIndividu#registerDetectedSpec()}). <b>Exemple 1 : Creation d'un individu simple</b><br/>
 * <code>
 *  <br/>
 *  EOIndividu individu = EOIndividu.creerInstance(myEditingContext);<br/>
 *  individu.setNomAffichage("Durand");<br/>
 *  individu.setPrenomAffichage("André");<br/>
 *   ...<br/>
 *   myEditingContext.saveChanges();<br/>
 *   <br/>
 * </code> <br/>
 * <b>Exemple 2 : Creation d'un individu qui doit à la spécificité d'être un fournisseur</b><br/>
 * <code>
 *  <br/>
 *  EOIndividu individu = EOIndividu.creerInstance(myEditingContext, new NSArray(EOIndividuForFournisseurSpec.sharedInstance()));<br/>
 *  individu.setNomAffichage("Durand");<br/>
 *  individu.setPrenomAffichage("André");<br/>
 *   ...<br/>
 *   myEditingContext.saveChanges();<br/>
 *   <br/>
 *
 * </code>
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @author egeze
 */
public class EOIndividu extends _EOIndividu implements IPersonne, IIndividu {

	public static final NSArray<String> IGNORE_CHANGES_IN_KEYS = new NSArray<String>(new String[] {TO_REPART_STRUCTURES_KEY,
			TO_REPART_ASSOCIATIONS_KEY });
	public static final String TEM_VALIDE_O = OUI;
	public static final String TEM_VALIDE_N = NON;
	public static final EOSortOrdering SORT_NOM_AFFICHAGE_ASC = EOIndividu.NOM_AFFICHAGE.asc();
	// public static final EOSortOrdering SORT_NOM_AFFICHAGE_ASC = EOSortOrdering.sortOrderingWithKey(EOIndividu.NOM_AFFICHAGE_KEY,
	// EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_NOM_ASC = EOIndividu.NOM_USUEL.asc();
	// public static final EOSortOrdering SORT_NOM_ASC = EOSortOrdering.sortOrderingWithKey(EOIndividu.NOM_USUEL_KEY,
	// EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_PRENOM_ASC = EOIndividu.PRENOM.asc();
	// public static final EOSortOrdering SORT_PRENOM_ASC = EOSortOrdering.sortOrderingWithKey(EOIndividu.PRENOM_KEY,
	// EOSortOrdering.CompareAscending);

	public static final EOQualifier QUAL_INDIVIDU_VALIDE = EOIndividu.TEM_VALIDE.eq(EOIndividu.TEM_VALIDE_O);
	public static final EOQualifier QUAL_INDIVIDU_INVALIDE = EOIndividu.TEM_VALIDE.eq(EOIndividu.TEM_VALIDE_N);
	public static final EOQualifier QUAL_INDIVIDU_TOUS = ERXQ.and(QUAL_INDIVIDU_VALIDE, QUAL_INDIVIDU_INVALIDE);

	// public static final EOQualifier QUAL_INDIVIDU_VALIDE = new EOKeyValueQualifier(EOIndividu.TEM_VALIDE_KEY,
	// EOQualifier.QualifierOperatorEqual, EOIndividu.TEM_VALIDE_O);
	public static final EOQualifier QUAL_INDIVIDU_SANS_COMPTE = EOIndividu.TO_COMPTES.dot(EOCompte.CPT_LOGIN).isNull();
	// public static final EOQualifier QUAL_INDIVIDU_SANS_COMPTE = EOQualifier.qualifierWithQualifierFormat(EOIndividu.TO_COMPTES_KEY + "."
	// + EOCompte.CPT_LOGIN_KEY + "=nil", null);

	public static String PRISE_CPT_INSEE_R = "R"; // reel
	public static String PRISE_CPT_INSEE_P = "P"; // provisoire
	public static final String LISTE_ROUGE_O = OUI;
	public static final String LISTE_ROUGE_N = NON;

	/**
	 * Individus consideres comme internes a l'etablissement. Notion d'interne basée sur
	 * {@link EOIndividuForUtilisateurSISpec#QUAL_UTILISATEUR_SI_VLAN_INTERNE}
	 */
	// FIXME Etendre la notion d'interne aux membres des personnes morales internes ?
	public static final EOQualifier QUAL_INDIVIDU_INTERNE = ERXQ.and(EOIndividuForUtilisateurSISpec.QUAL_UTILISATEUR_SI_VALIDE,
			EOIndividuForUtilisateurSISpec.QUAL_UTILISATEUR_SI_VLAN_INTERNE);

	// FIXME Etendre la notion d'interne aux membres des personnes morales internes ?
	public static final EOQualifier QUAL_INDIVIDU_INTERNE_GENERAL = EOIndividuForUtilisateurSISpec.QUAL_UTILISATEUR_SI_VLAN_INTERNE;

	/**
	 * Individus consideres commes externes a l'etablissement (pour l'instant ceux qui ne sont pas internes).
	 */
	// public static final EOQualifier QUAL_INDIVIDU_EXTERNE = new EONotQualifier(QUAL_INDIVIDU_INTERNE);
	public static final EOQualifier QUAL_INDIVIDU_EXTERNE = ERXQ.or(QUAL_INDIVIDU_SANS_COMPTE,
			EOIndividuForUtilisateurSISpec.QUAL_UTILISATEUR_SI_VLAN_X);

	/**
	 * Individus consideres comme etudiant
	 */
	public static final String QUALITE_ETUDIANT = "ETUDIANT";
	/**
	 * Individus consideres comme doctorant
	 */
	public static final String QUALITE_DOCTORANT = "DOCTORANT";

	/**
	 * Individus consideres commes etudiants.
	 */
	public static final EOQualifier QUAL_INDIVIDU_ETUDIANT = EOIndividu.IND_QUALITE.eq(QUALITE_ETUDIANT);
	// public static final EOQualifier QUAL_INDIVIDU_ETUDIANT = new EOKeyValueQualifier(EOIndividu.IND_QUALITE_KEY,
	// EOQualifier.QualifierOperatorEqual, QUALITE_ETUDIANT);

	// public static int FETCH_LIMIT = 20;

	private static int ANNEE_BASE_POUR_NAISSANCE = 1900;
	public static int LONGUEUR_NO_INSEE = 13;
	public static final String NO_INSEE_OBLIGATOIRE = "GRHUM_CONTROLE_INSEE";
	private static final String NO_INSEE_PROVISOIRE = "999999999999";

	public static final String SEQ_INDIVIDU_ENTITY_NAME = "Fwkpers_SeqIndividu";

	private PersonneDelegate personneDelegate = new PersonneDelegate(this);

	private static final Map _displayNames = new HashMap();

	static {
		_displayNames.put(EOIndividu.NOM_PATRONYMIQUE_KEY, "Nom patronymique");
		_displayNames.put(EOIndividu.PRENOM_KEY, "Prénom");
		_displayNames.put(EOIndividu.NOM_USUEL_KEY, "Nom usuel");
	}

	IFwkPersonneManager manager = new FwkPersonneManagerImpl();

	public EOIndividu() {
		super();
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to
	 * avoid unnecessary overhead (the properties will be // serialized for you in the superclass). private void
	 * writeObject(java.io.ObjectOutputStream out) throws java.io.IOException { }
	 *
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	/**
	 * Validation lors de la creation des EOIndividu.
	 * <ul>
	 * <li>L'attribut noIndividuCreateur doit correspondre à un individu valide</li>
	 * </ul>
	 */
	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		registerDetectedSpec();
		checkCreateur();
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		registerDetectedSpec();
		super.validateForUpdate();
	}

	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		registerDetectedSpec();
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Contrôles :
	 * <ul>
	 * <li>Si civilite=M, le nom patronymique affichage doit etre egal au nom usuel affichage</li>
	 * <li>L'attribut nomUsuel doit être égal au nom affichage sans accent et en UpperCase</li>
	 * <li>L'attribut prenom doit être égal au prenom affichage sans accent et en UpperCase</li>
	 * <li>L'attribut nom patronymique doit être égal au nom patronymique affichage sans accent et en UpperCase</li>
	 * <li>Si le n° INSEE est saisi, sa validité et la cohérence avec les autres champs sont controlés.</li>
	 * <li>Si le n° INSEE est saisi, Une recherche de doublons est effectuee.</li>
	 * <li>Si le flag PriseCptInsee est a R, le n° INSEE est obligatoire, s'il est a P pas de controles.</li>
	 * <li>Si le flag PriseCptInsee est a R, le n° INSEE provisoire ne doit pas etre supprimé s'il existe.</li>
	 * <li>Si le flag PriseCptInsee est a P, le n° INSEE est vidé.</li>
	 * <li>Si le parametre GRHUM_CONTROLE_INSEE est a OUI, le n° INSEE Réel est vérifié, sinon il ne l'est pas .</li>
	 * <li>Si la date de naissance est saisie, verification de la coherence des dates (naissance/deces)</li>
	 * <li>Si saisis, vérification sur la coherence pays/departement de naissance</li>
	 * </ul>
	 *
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {

		if (this.toComptes() != null  && !this.toComptes().isEmpty()) {
			this.toComptes().get(0).validateObjectMetier();
		}
		
		System.out.println("EOIndividu.validateObjectMetier()");
		if (objectHasChanged()) {
			try {
				trimAllString();
				setDModification(MyDateCtrl.now());

				if (fautIlEnregistrerLePersIDCreationModification()) {
					fixPersIdCreationEtModification();
				}
				getPersonneDelegate().checkUsers();

				if (isObjectInValidationEditingContext()) {
					CktlLog. trace("Validation de EOIndividu " + libelleEtId());
					checkContraintesObligatoires();
					checkContraintesLongueursMax();

					if (toCivilite() == null) {
						throw new NSValidation.ValidationException("La civivilité est obligatoire.");
					}

					if (!EOCivilite.C_CIVILITE_MADAME.equals(toCivilite().cCivilite())) {
						if (MyStringCtrl.isEmpty(nomPatronymiqueAffichage())) {
							//Modification du 27/06/2014 suite à la DT 6737
//							setNomPatronymiqueAffichage(nomAffichage());
							setNomPatronymiqueAffichage(nomPatronymique());
						}
					}

					fixNoms();
					checkNoms();
					checkDates();
					
					// FIXME Ajouter checkDoublonsHomonyme (sur nom, prenom + date naissance si saisie) (idem trigger)

					// checkLieuNaissance();

					// Si flag no insee reel, verifier qu'il est saisi
					if (PRISE_CPT_INSEE_R.equals(priseCptInsee())) {
						if (MyStringCtrl.isEmpty(indNoInsee())) {
							throw new NSValidation.ValidationException(
							"Vous devez renseigner le numero insee reel ou forcer l'utilisation d'un numero insee provisoire.");
						}
					}

					if (hasNoInseeProvisoire()) {
						if (this.toPaysNaissance() == null){
							this.setToPaysNaissanceRelationship(EOPays.getPaysDefaut(this.editingContext()));
						}

						if(this.toPaysNaissance().llPays().equals("FRANCE") && this.toDepartement()== null ){
							EODepartement selectedDep = EODepartement.fetchByQualifier(this.editingContext(), EODepartement.QUAL_SANS_DPT);
							this.setToDepartementRelationship(selectedDep);
						}
					}

					String verifierInsee = EOGrhumParametres.parametrePourCle(editingContext(), "GRHUM_CONTROLE_INSEE");
					if (verifierInsee != null && verifierInsee.startsWith(OUI)) {
						if (!MyStringCtrl.isEmpty(indNoInsee()) || indCleInsee() != null) {
							checkValiditeNoInsee();
							checkCoherenceNoInsee();
							checkDoublonsForNoInsee();
						}
					}
					super.validateObjectMetier();

					// si pas affecte a un groupe, l'affecter a un groupe par defaut (ANNUAIRE_STRUCTURE_DEFAUT").
					personneDelegate.fixGroupeDefaut(persIdModification());

					// si pas fournisseurspec, verifier que pas dans groupe repart FO
					if (!EOIndividuForFournisseurSpec.sharedInstance().isSpecificite(this)) {
						if (EOStructureForGroupeSpec.isPersonneInGroupeOfType(this, EOTypeGroupe.TGRP_CODE_FO)) {
							throw new NSValidation.ValidationException(
							"Un individu qui n'est pas fournisseur ne peut etre affectee a un groupe de type FO.");
						}
					}

				}
			}
			catch (NSValidation.ValidationException e) {
				e.printStackTrace();
				NSValidation.ValidationException e1 = new ValidationException(libelleEtId() + " : " + e.getMessage());
				throw e1;
			}
		}
	}

	private boolean fautIlEnregistrerLePersIDCreationModification() {
		return FwkCktlPersonne.paramManager.enregistrementPersIdCreationModificationActif() && hasDirtyAttributes();
	}

	public String persLc() {
		return prenom();
	}

	public String persLibelle() {
		return nomUsuel();
	}

	public String persNomptr() {
		return nomPatronymique();
	}

	public String persType() {
		return toCivilite().cCivilite();
	}

	public String getNomAndPrenom() {
		return nomAffichage() + " " + prenomAffichage();
	}

	private static EOQualifier qualForNomUsuelLike(String nom) {
		return EOIndividu.NOM_USUEL.likeInsensitive(nom);
	}

	private static EOQualifier qualForNomPatronymiqueLike(String nom) {
		return EOIndividu.NOM_PATRONYMIQUE.likeInsensitive(nom);
	}

	private static EOQualifier qualForPrenomLike(String prenom) {
		return EOIndividu.PRENOM.likeInsensitive(prenom);
	}

	private static EOQualifier qualForNomUsuelContains(String nom) {
		return EOIndividu.NOM_USUEL.contains(nom);
	}

	private static EOQualifier qualForNomPatronymiqueContains(String nom) {
		return EOIndividu.NOM_PATRONYMIQUE.contains(nom);
	}

	private static EOQualifier qualForPrenomContains(String prenom) {
		return EOIndividu.PRENOM.contains(prenom);
	}

	private static EOQualifier qualForEmailEquals(String email) {

		ERXKeyValueQualifier qualRPA = ERXQ.is(ERXQ.keyPath(EOIndividu.TO_REPART_PERSONNE_ADRESSES_KEY, EORepartPersonneAdresse.E_MAIL_KEY), email);
		EOQualifier qual = qualRPA;

		if (email.contains("@")) {
			ERXAndQualifier qualCEM = ERXQ.is(ERXQ.keyPath(EOIndividu.TO_COMPTES_KEY, EOCompte.TO_COMPTE_EMAILS_KEY, EOCompteEmail.CEM_EMAIL_KEY),
					email.substring(0, email.indexOf("@")).toLowerCase()).and(
							ERXQ.is(ERXQ.keyPath(EOIndividu.TO_COMPTES_KEY, EOCompte.TO_COMPTE_EMAILS_KEY, EOCompteEmail.CEM_DOMAINE_KEY),
									email.substring(email.indexOf("@") + 1).toLowerCase()));
			qual = qualRPA.or(qualCEM);
		} // TODO : Eventuellement gerer le cas d'un critère 'email' sans le '@' pour le CompteEmail

		return qual;
	}

	public static EOIndividu individuWithPersId(EOEditingContext ec, Integer persId) {
		return fetchFirstByQualifier(ec, ERXQ.equals(PERS_ID_KEY, persId));
	}

	/**
	 * @param myContext
	 * @param nom
	 *            Facultatif.
	 * @param prenom
	 *            Facultatif.
	 * @return Les individus dont le nom usuel et le prenom sont égaux à ceux fournis en parametre. Si le nom et le prenom sont vides, un
	 *         tableau vide est renvoyé. Le nom et le prenom sont transformé en majuscules sans accent avant de servir de filtre.
	 */
	public static NSArray<EOIndividu> individusWithNameEqualsAndFirstNameEqualsAndEmailEquals(EOEditingContext myContext, String nom, String prenom,
			String email, EOQualifier qualifierOptionnel, int fetchLimit) {
		NSMutableArray quals = new NSMutableArray();
		if (MyStringCtrl.isEmpty(nom) && MyStringCtrl.isEmpty(prenom) && MyStringCtrl.isEmpty(email)) {
			return NSArray.EmptyArray;
		}

		if (!MyStringCtrl.isEmpty(nom)) {
			quals.addObject(qualForNomUsuelLike(MyStringCtrl.chaineSansAccents(nom, "?").toUpperCase()));
		}
		if (!MyStringCtrl.isEmpty(prenom)) {
			quals.addObject(qualForPrenomLike(MyStringCtrl.chaineSansAccents(prenom, "?").toUpperCase()));
		}
		if (!MyStringCtrl.isEmpty(email)) {
			quals.addObject(qualForEmailEquals(MyStringCtrl.chaineSansAccents(email).toLowerCase()));
		}
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
		spec.setFetchLimit(fetchLimit);
		spec.setUsesDistinct(true);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	/**
	 * @param myContext
	 * @param nom
	 *            Facultatif.
	 * @param prenom
	 *            Facultatif.
	 * @return Les individus dont le nom usuel et le prenom sont égaux à ceux fournis en parametre. Si le nom et le prenom sont vides, un
	 *         tableau vide est renvoyé. Le nom et le prenom sont transformé en majuscules sans accent avant de servir de filtre.
	 */
	public static NSArray<EOIndividu> individusWithNameEqualsAndFirstNameEquals(EOEditingContext myContext, String nom, String prenom,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		NSMutableArray quals = new NSMutableArray();
		if (MyStringCtrl.isEmpty(nom) && MyStringCtrl.isEmpty(prenom)) {
			return NSArray.EmptyArray;
		}
		if (!MyStringCtrl.isEmpty(nom)) {
			quals.addObject(qualForNomUsuelLike(MyStringCtrl.chaineSansAccents(nom, "?").toUpperCase()));
		}
		if (!MyStringCtrl.isEmpty(prenom)) {
			quals.addObject(qualForPrenomLike(MyStringCtrl.chaineSansAccents(prenom, "?").toUpperCase()));
		}
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
		spec.setFetchLimit(fetchLimit);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	public static NSArray<EOIndividu> individusWithNameEqualsAndFirstNameEquals(EOEditingContext myContext, String nom, String prenom, int fetchLimit) {
		return individusWithNameEqualsAndFirstNameEquals(myContext, nom, prenom, null, fetchLimit);
	}

	/**
	 * @param myContext
	 * @param nom
	 * @param prenom
	 * @return Les individus dont le nom est égal à nom et le prenom commence par nom. Le nom et le prenom sont transformé en majuscules
	 *         sans accent avant de servir de filtre.
	 */
	public static NSArray<EOIndividu> individusWithNameEqualsAndFirstNameLike(EOEditingContext myContext, String nom, String prenom,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom)) {
			return NSArray.EmptyArray;
		}

		NSMutableArray quals = new NSMutableArray();
		if (!MyStringCtrl.isEmpty(nom)) {
			quals.addObject(qualForNomUsuelLike(MyStringCtrl.chaineSansAccents(nom, "?").toUpperCase()));
		}
		if (!MyStringCtrl.isEmpty(prenom)) {
			quals.addObject(qualForPrenomLike(MyStringCtrl.chaineSansAccents(prenom, "?").toUpperCase() + ETOILE));
		}
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
		spec.setFetchLimit(fetchLimit);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	public static NSArray<EOIndividu> individusWithNameEqualsAndFirstNameLike(EOEditingContext myContext, String nom, String prenom, int fetchLimit) {
		return individusWithNameEqualsAndFirstNameLike(myContext, nom, prenom, null, fetchLimit);
	}

	/**
	 * @param myContext
	 * @param nom
	 *            Obligatoire
	 * @param prenom
	 *            Facultatif
	 * @return Les individus dont le prenom contient le paramètre prénom. Le prenom est transformé en majuscules sans accent avant de servir
	 *         de filtre.
	 */
	public static NSArray<EOIndividu> individusWithFirstNameLike(EOEditingContext myContext, String prenom, EOQualifier qualifierOptionnel,
			int fetchLimit) {
		if (MyStringCtrl.isEmpty(prenom)) {
			return NSArray.emptyArray();
		}

		NSMutableArray quals = new NSMutableArray();

		if (!MyStringCtrl.isEmpty(prenom)) {
			quals.addObject(qualForPrenomContains(MyStringCtrl.chaineSansAccents(prenom, "?").toUpperCase()));
		}
		// if (!MyStringCtrl.isEmpty(prenom)) {
		// quals.addObject(qualForPrenomLike(MyStringCtrl.chaineSansAccents(prenom, "?").toUpperCase()));
		// }
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
		spec.setFetchLimit(fetchLimit);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	/**
	 * @param myContext
	 * @param nomPatronymique
	 * @param qualifierOptionnel
	 * @param fetchLimit
	 * @return Les invividus dont le nom patronymique contient le paramètre nomPatronymique. Le nom patronymique est transformé en
	 *         majuscules sans accent avant de servir de filtre.
	 */
	public static NSArray<EOIndividu> individusWithMiddleNameLike(EOEditingContext myContext, String nomPatronymique, EOQualifier qualifierOptionnel,
			int fetchLimit) {

		if (ERXStringUtilities.stringIsNullOrEmpty(nomPatronymique)) {
			return new NSArray<EOIndividu>();
		}

		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();

		quals.addObject(qualForNomPatronymiqueContains(MyStringCtrl.chaineSansAccents(nomPatronymique, "?").toUpperCase()));

		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));

		spec.setFetchLimit(fetchLimit);
		NSArray<EOIndividu> disp = myContext.objectsWithFetchSpecification(spec);

		return disp;
	}

	public static NSArray<EOIndividu> individusWithMiddleNameLike(EOEditingContext myContext, String nomPatronymique, int fetchLimit) {
		return individusWithMiddleNameLike(myContext, nomPatronymique, null, fetchLimit);
	}

	/**
	 * @param myContext
	 * @param login
	 * @param qualifierOptionnel
	 * @param fetchLimit
	 * @return Les invividus dont le login est egal au paramètre login. Le login est transformé en majuscules sans accent avant de servir de
	 *         filtre.
	 */
	public static NSArray<EOIndividu> individusWithLoginEquals(EOEditingContext myContext, String login, EOQualifier qualifierOptionnel,
			int fetchLimit) {

		if (ERXStringUtilities.stringIsNullOrEmpty(login)) {
			return new NSArray<EOIndividu>();
		}

		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();

		EOQualifier qualifierForLogin = EOIndividu.TO_COMPTES.dot(EOCompte.CPT_LOGIN).eq(MyStringCtrl.chaineSansAccents(login, "?"));

		quals.addObject(qualifierForLogin);

		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));

		spec.setFetchLimit(fetchLimit);
		NSArray<EOIndividu> disp = myContext.objectsWithFetchSpecification(spec);

		return disp;
	}

	/**
	 * @param myContext
	 * @param nom
	 *            Obligatoire
	 * @param prenom
	 *            Facultatif
	 * @return Les individus dont le nom usuel contient le paramètre nom et le prénom est égal au paramètre prenom. Le nom et le prenom sont
	 *         transformé en majuscules sans accent avant de servir de filtre.
	 */
	public static NSArray<EOIndividu> individusWithNameLikeAndFirstNameEquals(EOEditingContext myContext, String nom, String prenom,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom)) {
			return NSArray.EmptyArray;
		}

		NSMutableArray quals = new NSMutableArray();

		if (!MyStringCtrl.isEmpty(nom)) {
			quals.addObject(qualForNomUsuelContains(MyStringCtrl.chaineSansAccents(nom, "?").toUpperCase()));
		}
		if (!MyStringCtrl.isEmpty(prenom)) {
			quals.addObject(qualForPrenomLike(MyStringCtrl.chaineSansAccents(prenom, "?").toUpperCase()));
		}
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] { SORT_NOM_ASC, SORT_PRENOM_ASC }));
		spec.setFetchLimit(fetchLimit);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	public static NSArray<EOIndividu> individusWithNameLikeAndFirstNameEquals(EOEditingContext myContext, String nom, String prenom, int fetchLimit) {
		return individusWithNameLikeAndFirstNameEquals(myContext, nom, prenom, null, fetchLimit);
	}

	/**
	 * @param myContext
	 * @param nom
	 *            Obligatoire
	 * @param prenom
	 *            Facultatif
	 * @return Les individus dont le nom usuel contient le paramètre nom et le prénom contient le paramètre prenom. Le nom et le prenom sont
	 *         transformé en majuscules sans accent avant de servir de filtre.
	 */
	public static NSArray<EOIndividu> individusWithNameLikeAndFirstNameLike(EOEditingContext myContext, String nom, String prenom,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom)) {
			return NSArray.EmptyArray;
		}

		NSMutableArray quals = new NSMutableArray();

		if (!MyStringCtrl.isEmpty(nom)) {
			quals.addObject(qualForNomUsuelContains(MyStringCtrl.chaineSansAccents(nom, "?").toUpperCase()));
		}
		if (!MyStringCtrl.isEmpty(prenom)) {
			quals.addObject(qualForPrenomContains(MyStringCtrl.chaineSansAccents(prenom, "?").toUpperCase()));
		}
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
		spec.setFetchLimit(fetchLimit);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	public static NSArray<EOIndividu> individusWithNameLikeAndFirstNameLike(EOEditingContext myContext, String nom, String prenom, int fetchLimit) {
		return individusWithNameLikeAndFirstNameLike(myContext, nom, prenom, null, fetchLimit);
	}

	/**
	 * @param myContext
	 * @param nom
	 *            Obligatoire
	 * @return Les individus valides dont le nom usuel est égal au paramètre nom.
	 */
	public static NSArray<EOIndividu> individusWithNameEquals(EOEditingContext myContext, String nom, EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom)) {
			return NSArray.EmptyArray;
		}
		NSMutableArray quals = new NSMutableArray();

		if (!MyStringCtrl.isEmpty(nom)) {
			quals.addObject(qualForNomUsuelLike(MyStringCtrl.chaineSansAccents(nom, "?").toUpperCase()));
		}
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}
		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
				new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
		spec.setFetchLimit(fetchLimit);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	public static NSArray<EOIndividu> individusWithNameEquals(EOEditingContext myContext, String nom, int fetchLimit) {
		return individusWithNameEquals(myContext, nom, null, fetchLimit);
	}

	/**
	 * Effectue une recherche a partir d'un nom et d'un prénom. En fait plusieurs recherches sont effectuées et les résultats sont
	 * concaténés. Dans l'ordre :
	 * <ol>
	 * <li>
	 * {@link EOIndividu#individusWithNameEqualsAndFirstNameEquals(EOEditingContext, String, String)}</li>
	 * <li>
	 * {@link EOIndividu#individusWithNameEqualsAndFirstNameLike(EOEditingContext, String, String)}</li>
	 * <li> {@link EOIndividu#individusWithNameEquals(EOEditingContext, String)}</li>
	 * <li>
	 * {@link EOIndividu#individusWithNameLikeAndFirstNameEquals(EOEditingContext, String, String)}</li>
	 * </ol>
	 *
	 * @param myContext
	 * @param nom
	 * @param prenom
	 * @param qualifierOptionnel
	 *            qualifier optionnel rajouté aux critères précedents (nom, prénom).
	 * @param fetchLimit
	 *            limite du nombre de résultat.
	 * @return un tableau contenant les individus trouvés (les correspondances les plus fortes en premier).
	 */
	public static NSArray<EOIndividu> individusByNameAndFirstname(EOEditingContext myContext, String nom, String prenom, EOQualifier qualOptionnel,
			int fetchLimit) {
		return individusByNameAndFirstnameAndEmail(myContext, nom, prenom, null, qualOptionnel, fetchLimit);
	}

	/**
	 * Effectue une recherche a partir d'un nom, d'un prénom et d'une adresse email.<br>
	 * En fait plusieurs recherches sont effectuées et les résultats sont concaténés. Dans l'ordre :
	 * <ol>
	 * <li>
	 * {@link EOIndividu#individusWithNameEqualsAndFirstNameEqualsAndEmailEquals(EOEditingContext, String, String, String, EOQualifier, int)}
	 * </li>
	 * <li>
	 * {@link EOIndividu#individusWithNameEqualsAndFirstNameEquals(EOEditingContext, String, String)}</li>
	 * <li>
	 * {@link EOIndividu#individusWithNameEqualsAndFirstNameLike(EOEditingContext, String, String)}</li>
	 * <li> {@link EOIndividu#individusWithNameEquals(EOEditingContext, String)}</li>
	 * <li>
	 * {@link EOIndividu#individusWithNameLikeAndFirstNameEquals(EOEditingContext, String, String)}</li>
	 * </ol>
	 *
	 * @param myContext
	 *            EditingContext d'execution de la requete
	 * @param nom
	 *            critère sur le nom
	 * @param prenom
	 *            critère sur le prénom
	 * @param email
	 *            critère sur l'adresse Email
	 * @param qualifierOptionnel
	 *            qualifier optionnel rajouté aux critères précedents (nom, prénom ou email).
	 * @param fetchLimit
	 *            limite du nombre de résultat.
	 * @return un tableau contenant les individus trouvés (les correspondances les plus fortes en premier).
	 */
	public static NSArray<EOIndividu> individusByNameAndFirstnameAndEmail(EOEditingContext myContext, String nom, String prenom, String email,
			EOQualifier qualOptionnel, int fetchLimit) {
		NSMutableArray res = new NSMutableArray();
		res.addObjectsFromArray(individusWithNameEqualsAndFirstNameEqualsAndEmailEquals(myContext, nom, prenom, email, qualOptionnel, fetchLimit));
		res.addObjectsFromArray(individusWithNameEqualsAndFirstNameEquals(myContext, nom, prenom, qualOptionnel, fetchLimit));
		res.addObjectsFromArray(individusWithNameEqualsAndFirstNameLike(myContext, nom, prenom, qualOptionnel, fetchLimit));
		res.addObjectsFromArray(individusWithNameEquals(myContext, nom, qualOptionnel, fetchLimit));
		res.addObjectsFromArray(individusWithNameLikeAndFirstNameEquals(myContext, nom, prenom, qualOptionnel, fetchLimit));
		res.addObjectsFromArray(individusWithNameLikeAndFirstNameLike(myContext, nom, prenom, qualOptionnel, fetchLimit));
		if (MyStringCtrl.isEmpty(nom)) {
			res.addObjectsFromArray(individusWithFirstNameLike(myContext, prenom, qualOptionnel, fetchLimit));
		}
		AFinder.removeDuplicatesInNSArray(res);
		return res;
	}

	/**
	 * Renvoie l'ensemble des {@link EOIndividu} externes répondant aux critères donnés. Cette methode rajoute les critères propres aux
	 * individus externes.
	 *
	 * @param myContext
	 *            editingContext pour faire la requete.
	 * @param nom
	 *            critère sur le nom.
	 * @param prenom
	 *            critère sur le prénom.
	 * @param email
	 *            critère sur l'email.
	 * @param qualifierOptionnel
	 *            qualifier optionnel rajouté aux critères précedents (nom, prénom ou email).
	 * @param fetchLimit
	 *            limite du nombre de résultat.
	 * @return l'ensemble des {@link EOIndividu} externes répondant aux critères donnés.
	 */
	public static NSArray<EOIndividu> individusExternesByNameFirstnameAndEmail(EOEditingContext myContext, String nom, String prenom, String email,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		EOQualifier qual = (qualifierOptionnel == null ? EOIndividu.QUAL_INDIVIDU_EXTERNE : new EOAndQualifier(new NSArray(new Object[] {
				qualifierOptionnel, EOIndividu.QUAL_INDIVIDU_EXTERNE })));
		return individusByNameAndFirstnameAndEmail(myContext, nom, prenom, email, qual, fetchLimit);
	}

	public static NSArray<EOIndividu> individusExternesByNameAndFirstname(EOEditingContext myContext, String nom, String prenom,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		return individusExternesByNameFirstnameAndEmail(myContext, nom, prenom, null, qualifierOptionnel, fetchLimit);
	}

	/**
	 * Renvoie l'ensemble des {@link EOIndividu} internes répondant aux critères donnés. Cette methode rajoute les critères propres aux
	 * individus internes.
	 *
	 * @param myContext
	 *            editingContext pour faire la requete.
	 * @param nom
	 *            critère sur le nom.
	 * @param prenom
	 *            critère sur le prénom.
	 * @param email
	 *            critère sur l'email.
	 * @param qualifierOptionnel
	 *            qualifier optionnel rajouté aux critères précedents (nom, prénom ou email).
	 * @param fetchLimit
	 *            limite du nombre de résultat.
	 * @return l'ensemble des {@link EOIndividu} internes répondant aux critères donnés.
	 */
	public static NSArray<EOIndividu> individusInternesByNameFirstnameAndEmail(EOEditingContext myContext, String nom, String prenom, String email,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		EOQualifier qual = (qualifierOptionnel == null ? EOIndividu.QUAL_INDIVIDU_INTERNE_GENERAL : new EOAndQualifier(new NSArray(new Object[] {
				qualifierOptionnel, EOIndividu.QUAL_INDIVIDU_INTERNE_GENERAL })));
		return individusByNameAndFirstnameAndEmail(myContext, nom, prenom, email, qual, fetchLimit);
	}

	public static NSArray<EOIndividu> individusInternesByNameAndFirstname(EOEditingContext myContext, String nom, String prenom,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		return individusInternesByNameFirstnameAndEmail(myContext, nom, prenom, null, qualifierOptionnel, fetchLimit);
	}
	
	public static NSArray<EOIndividu> individusInternesWithNameLikeOrFirstNameLike(EOEditingContext editingContext, String chaine, int fetchLimit) {
	    return individusInternesWithNameLikeOrFirstNameLikeWithQualifier(editingContext, chaine, null, fetchLimit);
	}

	public static NSArray<EOIndividu> individusInternesWithNameLikeOrFirstNameLikeWithQualifier(EOEditingContext editingContext, 
			String chaine, EOQualifier qualifier ,int fetchLimit) {
    if (MyStringCtrl.isEmpty(chaine)) {
        return NSArray.EmptyArray;
    }
    EOQualifier qual = ERXQ.and(
            ERXQ.containsAny(new NSArray<String>(EOIndividu.NOM_USUEL_KEY,  EOIndividu.PRENOM_KEY), chaine),
            QUAL_INDIVIDU_VALIDE,
            QUAL_INDIVIDU_INTERNE_GENERAL);
    
    if(qualifier!=null){
    	qual = ERXQ.and(qual, qualifier);
    }
    
    EOFetchSpecification spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual,
            new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
    spec.setFetchLimit(fetchLimit);
    NSArray disp = editingContext.objectsWithFetchSpecification(spec);
    return disp;
}
	
	public static EOIndividu individuWithNameAndFirstNamesAndDateNaisLike(EOEditingContext myContext, String nom, String prenom, String prenom2,
			NSTimestamp naissance) {
		EOQualifier qualifier;
		EOFetchSpecification spec;

		//
		// on se passe des caracteres accentues
		//
		nom = MyStringCtrl.prepareStringForSqlSrch(nom);
		prenom = MyStringCtrl.prepareStringForSqlSrch(prenom);
		String critere = "nomUsuel caseInsensitiveLike %s and prenom caseInsensitiveLike %s and temValide = 'O'";
		NSMutableArray myArray = new NSMutableArray();
		myArray.addObject(nom);
		myArray.addObject(prenom);
		qualifier = EOQualifier.qualifierWithQualifierFormat(critere, myArray);

		//
		// si la date de naissance est remplie on l utilise aussi ds la recherche
		//
		if (naissance != null) {
			qualifier = new EOAndQualifier(new NSArray(new Object[] {
					EOQualifier.qualifierWithQualifierFormat("dNaissance = %@", new NSArray(naissance)), qualifier }));
		}

		if (prenom2 != null && prenom2.length() > 0) {
			qualifier = new EOAndQualifier(new NSArray(new Object[] {
					EOQualifier.qualifierWithQualifierFormat("prenom2 caseInsensitiveLike %s", new NSArray(prenom2)), qualifier }));
		}

		spec = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qualifier, new NSArray(new Object[] {SORT_NOM_ASC, SORT_PRENOM_ASC }));
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		if (disp != null && disp.count() > 0) {
			return (EOIndividu) disp.objectAtIndex(0);
		} else {
			return null;
		}

	}

	/**
	 * @param myContext
	 * @param studentNumber
	 * @return l'individu associé au numéro d'étudiant
	 */
	public static EOIndividu individuWithEtudNumeroEquals(EOEditingContext myContext, String studentNumber) {
		/*
		 *  Chercher l'étudiant
		 */
		if (!MyStringCtrl.isEmpty(studentNumber) && MyStringCtrl.hasOnlyDigits(studentNumber)) {
			Integer IStudentNumber = null;
			try {
				IStudentNumber = new Integer (studentNumber.trim());
			} catch (Exception e) {
				return null;
			}
			EOQualifier qual = ERXQ.equals(EOEtudiant.ETUD_NUMERO_KEY, IStudentNumber);
			@SuppressWarnings("unchecked")
			NSArray <EOEtudiant> res = EOEtudiant.fetchAll(myContext, qual);
			if (res.count() > 0) {
				return res.get(0).toIndividu();
			}
		}
		return  null;
	}


	/**
	 * {@inheritDoc}
	 */
	public IFournis toFournis() {
		if (toFourniss() == null || toFourniss().count() == 0) {
			return null;
		} else {
			NSArray res = EOQualifier.filteredArrayWithQualifier(toFourniss(), EOFournis.QUAL_FOU_VALIDE_OUI);
			if (res.count() > 0) {
				return (EOFournis) res.objectAtIndex(0);
			}
			NSArray res2 = EOQualifier.filteredArrayWithQualifier(toFourniss(), EOFournis.QUAL_FOU_VALIDE_INSTANCE);
			if (res2.count() > 0) {
				return (EOFournis) res2.objectAtIndex(0);
			}

			NSArray res3 = EOQualifier.filteredArrayWithQualifier(toFourniss(), EOFournis.QUAL_FOU_VALIDE_ANNULE);
			if (res3.count() > 0) {
				return (EOFournis) res3.objectAtIndex(0);
			}
			return null;
		}
	}

	/**
	 * Verifie la validite du n° INSEE s'il est saisi indépendamment des autres champs.
	 */
	private void checkValiditeNoInsee() throws NSValidation.ValidationException {
		if (!MyStringCtrl.isEmpty(indNoInsee())) {

			if (indCleInsee() == null) {
				throw new NSValidation.ValidationException("La clé INSEE est obligatoire si vous renseignez le numero INSEE.");
			}

			if (indNoInsee().length() != LONGUEUR_NO_INSEE) {
				throw new NSValidation.ValidationException("Le numéro Insee doit comporter treize chiffres");
			}

			int cleInsee = CodeInsee.calculeClefinsee(indNoInsee()).intValue();
			if (cleInsee != indCleInsee().intValue()) {
				throw new NSValidation.ValidationException("La clé Insee a une valeur erronnée");
			}
		}
	}

	/**
	 * Vérifie que le créateur de l'enregistrement est un individu valide.
	 *
	 * @throws NSValidation.ValidationException
	 */
	protected void checkCreateur() throws NSValidation.ValidationException {
		EOIndividu createur = EOIndividu.fetchByKeyValue(editingContext(), EOIndividu.NO_INDIVIDU_KEY, noIndividuCreateur());
		if (createur == null || !EOIndividu.TEM_VALIDE_O.equals(createur.temValide())) {
			throw new NSValidation.ValidationException("Le n° d'individu " + noIndividuCreateur() + " affecté à l'attribut "
					+ getDisplayName(EOIndividu.NO_INDIVIDU_CREATEUR_KEY) + " ne correspond pas à un n° d'individu valide.");
		}
	}

	/**
	 * Verifie la coherence des noms.
	 *
	 * @throws NSValidation.ValidationException
	 */
	private void checkNoms() throws NSValidation.ValidationException {

		if (!nomUsuel().equals(MyStringCtrl.chaineSansAccents(nomAffichage()).toUpperCase())) {
			throw new NSValidation.ValidationException("Le nom usuel doit être égal au nom d'affichage (en majuscules, sans accent).");
		}
		if (!prenom().equals(MyStringCtrl.chaineSansAccents(prenomAffichage()).toUpperCase())) {
			throw new NSValidation.ValidationException("Le prénom doit être égal au prénom d'affichage (en majuscules, sans accent).");
		}

		if (MyStringCtrl.isEmpty(nomPatronymique()) && !MyStringCtrl.isEmpty(nomPatronymiqueAffichage()) || !MyStringCtrl.isEmpty(nomPatronymique())
				&& MyStringCtrl.isEmpty(nomPatronymiqueAffichage())) {
			throw new NSValidation.ValidationException(
			"Le nom patronymique et le nom patronymique d'affichage doivent être remplis tous les deux ou vides tous les deux.)");
		}

		if (!MyStringCtrl.isEmpty(nomPatronymique())
				&& !nomPatronymique().equals(MyStringCtrl.chaineSansAccents(nomPatronymiqueAffichage()).toUpperCase())) {
			throw new NSValidation.ValidationException(
			"Le nom patronymique doit être égal au nom patronymique d'affichage (en majuscules, sans accent).");
		}

	}

	/**
	 * Verifie la coherence du n° INSEE avec les autres champs s'ils sont saisis.
	 */
	private void checkCoherenceNoInsee() {
		if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.INDIVIDU_CHECK_COHERENCE_INSEE_DISABLED)) {
			return;
		}

		List<ResultatControle> resultats = manager.getIndividuService()
		.appliqueControles(this);
		if (resultats.size() > 0) {
			throw new NSValidation.ValidationException(resultats.get(0)
					.getMessage());
		}
	}

	/**
	 * Verifier si le n° INSEE est deja affecté a un autre individu.
	 *
	 * @throws NSValidation.ValidationException
	 */
	private void checkDoublonsForNoInsee() throws NSValidation.ValidationException {
		if (indNoInsee() != null) {
			NSArray doublons = fetchAllValides(this.editingContext(), new EOKeyValueQualifier(EOIndividu.IND_NO_INSEE_KEY,
					EOQualifier.QualifierOperatorEqual, indNoInsee()), null);
			if (doublons.count() > 1 || (doublons.count() == 1 && doublons.objectAtIndex(0) != this)) {
				EOIndividu doublon = (EOIndividu) doublons.objectAtIndex(0);
				throw new NSValidation.ValidationException("L'individu n°" + doublon.noIndividu() + " (" + doublon.getNomAndPrenom()
						+ ") possède déjà ce n° INSEE.");
			}
		}
	}

	/**
	 * Verifie la coherence des dates naissances et deces.
	 */
	protected void checkDates() {
		checkDatesNaissance(this.dNaissance(), this.dDeces(), new NSTimestamp());
	}

	protected static void checkDatesNaissance(NSTimestamp dNaissance, NSTimestamp dDeces, NSTimestamp today) {
		if (dNaissance != null) {
			// vérifier la cohérence des dates par rapport à la date du jour
			if (MyDateCtrl.isAfter(dNaissance, today)) {
				throw new NSValidation.ValidationException("La date de naissance ne peut être postérieure à aujourd'hui");
			}
			if (dDeces != null && MyDateCtrl.isAfter(dDeces, today)) {
				throw new NSValidation.ValidationException("La date de décès ne peut être postérieure à aujourd'hui");
			}
			// vérifier que la date de décès n'est pas antérieure à la date de naissance
			if (dDeces != null && MyDateCtrl.isBefore(dDeces, dNaissance)) {
				throw new com.webobjects.foundation.NSValidation.ValidationException(
				"La date de décès ne peut être antérieure à la date de naissance");
			}
		}
	}

	public boolean estHomme() {
		return toCivilite() != null && toCivilite().cCivilite().equals(EOCivilite.C_CIVILITE_MONSIEUR);
	}

	@Deprecated // Déporté dans la classe CodeInsee
	public static Number getCleInseeCalculee(String numero) {
		return CodeInsee.calculeClefinsee(numero);
	}

	public boolean estNoInseeProvisoire() {
		return indNoInsee().substring(1).equals(NO_INSEE_PROVISOIRE);
	}

	public String getNumero() {
		return "" + noIndividu().intValue();
	}

	public Integer getNumeroInt() {
		return noIndividu();
	}

	
	/**
	 * {@inheritDoc}
	 */
	public String getNomPrenomAffichage() {
		return getNomAndPrenom();
	}

	/*
	 * Méthode pour avoir une chaîne de caractères sans accent. Utilisation du nom usuel qui est NOT NULL contrairement au nom patronymique
	 *
	 * @see org.cocktail.fwkcktlpersonne.common.metier.IPersonne#getNomPrenomRecherche() Implémentation d'une méthode abstraite de
	 * l'interface IPersonne.
	 */

	public String getNomPrenomRecherche() {
		return nomUsuel() + " " + prenom();
	}

	/**
	 * Initialisation
	 * <ul>
	 * <li>noIndividu avec construireNoIndividu()</li>
	 * <li>persId avec construirePersId()</li>
	 * <li>temValide à O</li>
	 * <li>indPhoto à O</li>
	 * <li>dCreation</li>
	 * </ul>
	 */
	@Override
	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setNoIndividu(construireNoIndividu());
		setPersId(construirePersId());
		setTemValide(TEM_VALIDE_O);
		setIndPhoto(NON);
		setListeRouge(NON);
		setPriseCptInsee(PRISE_CPT_INSEE_P);
		setDCreation(MyDateCtrl.now());
	}

	/**
	 * @return un nouveau noIndividu
	 */
	public Integer construireNoIndividu() {
		return AFinder.clePrimairePour(editingContext(), EOIndividu.ENTITY_NAME, EOIndividu.NO_INDIVIDU_KEY, SEQ_INDIVIDU_ENTITY_NAME, true);
	}

	/**
	 * @return un nouveau persId
	 */
	public Integer construirePersId() {
		return AFinder.construirePersId(editingContext());
	}

	@Override
	public void setNomUsuel(String value) {
		if (value != null) {
			value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
		}
		super.setNomUsuel(value);
	}

	@Override
	public void setNomPatronymique(String value) {
		if (value != null) {
			value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
		}
		super.setNomPatronymique(value);
	}

	@Override
	public void setPrenom(String value) {
		if (value != null) {
			value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
		}
		super.setPrenom(value);
	}

	@Override
	public void setPrenom2(String value) {
		if (value != null) {
			value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
		}
		super.setPrenom2(value);
	}

	@Override
	public void setNomAffichage(String value) {
		super.setNomAffichage(value);
		setNomUsuel(value);
	}

	@Override
	public void setPrenomAffichage(String value) {
		super.setPrenomAffichage(value);
		setPrenom(value);
	}

	@Override
	public void setNomPatronymiqueAffichage(String value) {
		super.setNomPatronymiqueAffichage(value);
		setNomPatronymique(value);
	}

	/** Recherche les distinctions d'un individu */
	public static NSArray rechercherDistinctionsPourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu));
		EOFetchSpecification myFetch = new EOFetchSpecification("IndividuDistinctions", myQualifier, null);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * Teste si l'objet doit respecter certaines specificites et les affecte à l'objet.<br/>
	 * Cette méthode est appelée par les méthodes de validation. <br/>
	 * Liste des specificites detectees
	 * <ul>
	 * <li>{@link EOIndividuForFournisseurSpec}</li>
	 * <li>{@link EOIndividuForPersonnelSpec}</li>
	 * <li>{@link EOIndividuForUtilisateurSISpec}</li>
	 * <li>{@link EOIndividuForEtudiantSpec}</li>
	 * </ul>
	 *
	 * @return true si des nouvelles specificites ont été ajoutées.
	 */
	public boolean registerDetectedSpec() {
		System.out.println("AfwkPersRecord.validateForSave() " + this.getClass().getName());
		boolean res = false;
		if (!hasSpecificite(EOIndividuForFournisseurSpec.sharedInstance()) && EOIndividuForFournisseurSpec.sharedInstance().isSpecificite(this)) {
			registerSpecificite(EOIndividuForFournisseurSpec.sharedInstance());
			res = true;
		}
		if (!hasSpecificite(EOIndividuForPersonnelSpec.sharedInstance()) && EOIndividuForPersonnelSpec.sharedInstance().isSpecificite(this)) {
			registerSpecificite(EOIndividuForPersonnelSpec.sharedInstance());
			res = true;
		}
		if (!hasSpecificite(EOIndividuForUtilisateurSISpec.sharedInstance()) && EOIndividuForUtilisateurSISpec.sharedInstance().isSpecificite(this)) {
			registerSpecificite(EOIndividuForUtilisateurSISpec.sharedInstance());
			res = true;
		}
		if (!hasSpecificite(EOIndividuForEtudiantSpec.sharedInstance()) && EOIndividuForEtudiantSpec.sharedInstance().isSpecificite(this)) {
			registerSpecificite(EOIndividuForEtudiantSpec.sharedInstance());
			res = true;
		}
		return res;
	}

	@Override
	public Map displayNames() {
		return EOIndividu._displayNames;
	}

	@Override
	public String nomAffichage() {
		return (MyStringCtrl.isEmpty(super.nomAffichage()) ? nomUsuel() : super.nomAffichage());
	}

	@Override
	public String prenomAffichage() {
		return (MyStringCtrl.isEmpty(super.prenomAffichage()) ? prenom() : super.prenomAffichage());
	}

	@Override
	public String nomPatronymiqueAffichage() {
		return (MyStringCtrl.isEmpty(super.nomPatronymiqueAffichage()) ? nomPatronymique() : super.nomPatronymiqueAffichage());
	}

	@Override
	public void setVilleDeNaissance(String value) {
		if (value != null) {
			if (EOGrhumParametres.parametrePourCle(editingContext(), FwkCktlPersonneParamManager.NETTOYER_VILLE_NAISSANCE_DISABLED).equalsIgnoreCase("N")) {
				value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
			}
		}
		super.setVilleDeNaissance(value);
	}

	/**
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Tous les EOIndividus valides qui repondent au qualifier.
	 */
	public static NSArray<EOIndividu> fetchAllValides(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(QUAL_INDIVIDU_VALIDE);
		if (qualifier != null) {
			quals.addObject(qualifier);
		}
		return fetchAll(editingContext, new EOAndQualifier(quals), sortOrderings, true);
	}

	/**
	 * Corrige les incohérences sur les noms (entre nom affichage/nom usuel et prenom affichage/prenom)
	 */
	private void fixNoms() {
		if (!MyStringCtrl.isEmpty(nomUsuel()) && MyStringCtrl.isEmpty(nomAffichage())) {
			setNomAffichage(nomUsuel());
		}
		if (!MyStringCtrl.isEmpty(nomUsuel())) {
			if (!nomUsuel().equals(MyStringCtrl.chaineSansAccents(nomAffichage()).toUpperCase())) {
				setNomAffichage(nomUsuel());
			}
		}

		if (!MyStringCtrl.isEmpty(prenom()) && MyStringCtrl.isEmpty(prenomAffichage())) {
			setPrenomAffichage(prenom());
		}
		if (!MyStringCtrl.isEmpty(prenom())) {
			if (!prenom().equals(MyStringCtrl.chaineSansAccents(prenomAffichage()).toUpperCase())) {
				setPrenomAffichage(prenom());
			}
		}
	}

	public String persLcAffichage() {
		return prenomAffichage();
	}

	public String persLibelleAffichage() {
		return nomAffichage();
	}

	public String persNomptrAffichage() {
		return nomPatronymiqueAffichage();
	}

	public String getNomCompletAffichage() {
		return persLibelleAffichage()
		+ (persNomptrAffichage() != null && !persNomptrAffichage().equals(persLibelleAffichage()) ? " (" + persNomptrAffichage() + ")" : "")
		+ (persLcAffichage() != null ? " " + persLcAffichage() : "");
	}

	@Override
	public NSArray toComptes(EOQualifier qualifier) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(super.toComptes(qualifier), new NSArray(EOCompte.SORT_VLANS_PRIORITE));
	}

	public PersonneDelegate getPersonneDelegate() {
		return personneDelegate;
	}

	public void setPersonneDelegate(PersonneDelegate personneDelegate) {
		this.personneDelegate = personneDelegate;
	}

	public NSArray getRepartStructuresAffectes(PersonneApplicationUser appUser) {
		return personneDelegate.getRepartStructuresAffectes(appUser, null);
	}

	public NSArray getRepartStructuresAffectes(Integer persIdForUser) {
		return personneDelegate.getRepartStructuresAffectes(persIdForUser, null);
	}

	public NSArray getRepartStructuresAffectes(PersonneApplicationUser appUser, EOQualifier qualifierForGroupe) {
		return personneDelegate.getRepartStructuresAffectes(appUser, qualifierForGroupe);
	}

	public String libelleEtId() {
		return persLibelle() + " (persid=" + persId() + ")";
	}

	public boolean isStructure() {
		return false;
	}

	public boolean isIndividu() {
		return true;
	}

	public String indNoInseeForDisplay() {
		if (MyStringCtrl.isEmpty(indNoInsee())) {
			return "Non renseigné";
		}
		return (indNoInsee().substring(0, 1) + MyStringCtrl.extendWithChars("", "x", indNoInsee().length() - 2, true) + indNoInsee().substring(
				indNoInsee().length() - 2, indNoInsee().length() - 1));

	}

	public static IPersonne individuPourNumero(EOEditingContext editingContext, Integer noIndividu) {
		if (noIndividu == null) {
			return null;
		}
		return fetchByKeyValue(editingContext, NO_INDIVIDU_KEY, noIndividu);
	}

	/**
	 * @param qual
	 *            Qualifier pour identifier un etablisssement. Cf.
	 *            {@link EOStructureForGroupeSpec#getEtablissementAffectation(EOStructure, EOQualifier)} .
	 * @return Les établissements affectés à l'individu (à partir de ses services affectés).
	 * @see EOStructureForGroupeSpec
	 */
	public NSArray getEtablissementsAffectation(EOQualifier qual) {
		NSArray services = getServices();

		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < services.count(); i++) {
			EOStructure struct = (EOStructure) services.objectAtIndex(i);
			EOStructure etab = EOStructureForGroupeSpec.getEtablissementAffectation(struct, qual);
			if (etab != null) {
				if (res.indexOfObject(etab) == NSArray.NotFound) {
					res.addObject(etab);
				}
			}
		}
		return res.immutableClone();
	}

	public NSArray getServices() {
		NSArray res = personneDelegate.getAllGroupesAffectesOfType(EOTypeGroupe.TGRP_CODE_S);
		return res;
	}

	public EORepartAssociation definitUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang) {
		return personneDelegate.definitUnRole(ec, association, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite,
				rasRang);
	}

	public EORepartAssociation definitUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang,
			boolean autoriserGroupeReserve) {
		return personneDelegate.definitUnRole(ec, association, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite,
				rasRang, autoriserGroupeReserve);
	}

	public NSArray definitLesRoles(EOEditingContext ec, NSArray associations, EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang,
			boolean autoriserGroupeReserve) {
		return personneDelegate.definitLesRoles(ec, associations, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire,
				rasQuotite, rasRang, autoriserGroupeReserve);
	}

	public NSArray definitLesRoles(EOEditingContext ec, NSArray associations, EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang) {
		return personneDelegate.definitLesRoles(ec, associations, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire,
				rasQuotite, rasRang);
	}

	/**
	 * @param qualifierOnCompte
	 *            un qualifier a appliquer sur les EOCompte (par exemple sur les vLans)
	 * @return Les emails (sous forme de String) issus de CompteEmail.
	 */
	public NSArray<String> getEmails(EOQualifier qualifierOnCompte) {
		NSMutableArray res = new NSMutableArray();
		EOQualifier qual = EOCompte.QUAL_CPT_VALIDE_OUI;
		if (qualifierOnCompte != null) {
			qual = new EOAndQualifier(new NSArray(new Object[] {qualifierOnCompte, qual }));
		}
		NSArray comptes = toComptes(qual, new NSArray(EOCompte.SORT_VLANS_PRIORITE), false);

		for (int i = 0; i < comptes.count(); i++) {
			org.cocktail.fwkcktlpersonne.common.metier.EOCompte compte = ((org.cocktail.fwkcktlpersonne.common.metier.EOCompte) comptes
					.objectAtIndex(i));
			NSArray cptEmails = compte.toCompteEmails();
			for (int j = 0; j < cptEmails.count(); j++) {
				String email = ((org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail) cptEmails.objectAtIndex(j)).getEmailFormatte();
				if (res.indexOfObject(email) == NSArray.NotFound) {
					res.addObject(email);
				}
			}
		}
		return res.immutableClone();
	}

	public NSArray getRepartAssociationsInGroupe(EOStructure groupe, EOQualifier qualifier) {
		return getPersonneDelegate().getRepartAssociationsInGroupe(groupe, qualifier);
	}

	public void supprimerAffectationAUnGroupe(EOEditingContext ec, Integer persIdUtilisateur, EORepartStructure repartStructure) {
		getPersonneDelegate().supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repartStructure);
	}

	public void supprimerAffectationAUnGroupe(EOEditingContext editingContext, Integer persIdUtilisateur, EOStructure groupe) {
		getPersonneDelegate().supprimerAffectationAUnGroupe(editingContext, persIdUtilisateur, groupe);
	}

	public void supprimerAffectationATousLesGroupes(EOEditingContext editingContext, Integer persIdUtilisateur) {
		getPersonneDelegate().supprimerAffectationATousLesGroupes(editingContext, persIdUtilisateur);
	}

	public void supprimerLesRoles(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur) {
		getPersonneDelegate().supprimerLesRoles(ec, association, groupe, persIdUtilisateur);

	}

	public void supprimerUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture) {
		getPersonneDelegate().supprimerUnRole(ec, association, groupe, persIdUtilisateur, rasDOuverture);
	}

	public EOAdresse getAdressePrincipale() {
		return getPersonneDelegate().getAdressePrincipale(editingContext());
	}

	/**
	 * {@inheritDoc}
	 */
	public IAdresse adresseFacturation() {
		return personneDelegate.adressesFacturation();
	}
	
	public IAdresse adresseEtudiant() {
		return getPersonneDelegate().adresseEtudiant();
	}
	
	public IAdresse adresseParent() {
		return getPersonneDelegate().adresseParent();
	}
	
	public EOPersonneTelephone telephone(String typeNoTel, String typeTel) {
		EOQualifier qual = ERXQ.and(
				EOPersonneTelephone.TO_TYPE_TEL.dot(EOTypeTel.C_TYPE_TEL).eq(typeTel),
				EOPersonneTelephone.TO_TYPE_NO_TEL.dot(EOTypeNoTel.C_TYPE_NO_TEL).eq(typeNoTel));
		List<EOPersonneTelephone> telephones = this.toPersonneTelephones(qual);
		
		if (telephones.size() > 0) {
			return telephones.get(0);
		}

		return null;
	}

	public Boolean hasCompteValide() {
		return Boolean.valueOf(EOQualifier.filteredArrayWithQualifier(this.toComptes(), EOCompte.QUAL_CPT_VALIDE_OUI).count() > 0);
	}

	public Boolean hasCompteInterne() {
		return Boolean.valueOf(EOQualifier.filteredArrayWithQualifier(this.toComptes(), EOCompte.QUAL_CPT_INTERNE).count() > 0);
	}

	public Boolean isInterneEtablissement() {
		return Boolean.valueOf(hasCompteValide().booleanValue() && hasCompteInterne());
	}

	public String getNomEtId() {
		return libelleEtId();
	}

	public EOPersonnel toPersonnel() {
		EOPersonnel res = null;
		if (toPersonnels() != null && toPersonnels().size() > 0) {
			res = (EOPersonnel) toPersonnels().lastObject();
		}
		return res;
	}

	public void setToPersonnel(EOPersonnel personnel) {
		if (personnel != null) {
			EOPersonnel existing = toPersonnel();
			if (existing != null) {
				removeFromToPersonnelsRelationship(existing);
			}	
			addToToPersonnelsRelationship(personnel);
		}
	}

	public boolean hasQualiteEtudiant(){
		if (QUALITE_ETUDIANT.equals(indQualite())) {
			return true;
		}
		if (indQualite() != null && indQualite().startsWith("Etudiant")) {
			return true;
		}
		return false;
	}

	public boolean hasQualiteDoctorant(){
		if (QUALITE_DOCTORANT.equalsIgnoreCase(indQualite())) {
			return true;
		}
		return false;
	}

	public IPhoto toPhoto() {
		NSArray photos = new NSMutableArray();

		if (hasQualiteEtudiant()) {
			photos = EOPhotosEtudiantsGrhum.fetchAll(editingContext(), ERXQ.equals(EOPhotosEtudiantsGrhum.NO_INDIVIDU_KEY, noIndividu()));
			if (photos == null || photos.isEmpty()) {
				photos = EOPhotosEtudiantsOldGrhum.fetchAll(editingContext(), ERXQ.equals(EOPhotosEtudiantsGrhum.NO_INDIVIDU_KEY, noIndividu()));
			}
		} else {
			photos = EOPhotosEmployes.fetchAll(editingContext(), ERXQ.equals(EOPhotosEmployes.NO_INDIVIDU_KEY, noIndividu()));
		}

		if (hasQualiteDoctorant() && (photos == null || photos.isEmpty())) {
			photos = EOPhotosEmployes.fetchAll(editingContext(), ERXQ.equals(EOPhotosEmployes.NO_INDIVIDU_KEY, noIndividu()));
			if (photos == null || photos.isEmpty()) {
				photos = EOPhotosEtudiantsGrhum.fetchAll(editingContext(), ERXQ.equals(EOPhotosEtudiantsGrhum.NO_INDIVIDU_KEY, noIndividu()));
				if (photos == null || photos.isEmpty()) {
					photos = EOPhotosEtudiantsOldGrhum.fetchAll(editingContext(), ERXQ.equals(EOPhotosEtudiantsGrhum.NO_INDIVIDU_KEY, noIndividu()));
				}
			}
		}

		//		if (ERXProperties.booleanForKeyWithDefault("SHOW_WATERMARK", false)) {
		//			
		//		}

		return (IPhoto) photos.lastObject();
	}

	public IPhoto setPhoto(NSData data) {
		IPhoto photo = toPhoto();
		if (photo == null) {
			//			if (QUALITE_ETUDIANT.equals(indQualite())) {
			if (hasQualiteEtudiant()) {
				photo = EOPhotosEtudiantsGrhum.creerInstance(editingContext());
			} else {
				photo = EOPhotosEmployes.creerInstance(editingContext());
			}
		}
		// Si on crée une photo, cet indice est vide donc à null.
		// On sette par défaut à "OUI".
		if (indPhoto() == null) {
			setIndPhoto(TEM_VALIDE_O);
		}
		photo.setDatasPhoto(data);
		photo.setDatePrise(new NSTimestamp());
		photo.setNoIndividu(noIndividu());
		return photo;
	}

	@Override
	public void setPersIdModification(Integer value) {
		super.setPersIdModification(value);
		if (persIdCreation() == null) {
			setPersIdCreation(value);
		}
	}

	public boolean estListeRouge() {
		return listeRouge().equals(LISTE_ROUGE_O);
	}

	/**
	 * @return True si l'objet a été modifié (en ignorant les modifications sur les clés de IGNORE_CHANGES_IN_KEYS)
	 */
	public boolean objectHasChanged() {
		// Modification faite pour que seul le fichier Properties de RGRHUM n'impacte cette méthode.
		if ("O".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PERSONNE_OBJECTHASCHANGED_DESACTIVE))) {
			return true;
		}
		if (this.hasTemporaryGlobalID()) {
			return true;
		}
		NSArray committedKeys = changesFromCommittedSnapshot().allKeys();
		return !ERXArrayUtilities.arrayContainsArray(IGNORE_CHANGES_IN_KEYS, committedKeys);
	}

	public String retourneCreateur() {
		String createur = "";
		if (getCreateur() != null) {
			createur = getCreateur().getNomAndPrenom();
		}
		return createur;
	}

	public String retourneModificateur() {
		String modificateur = "";
		if (getModificateur() != null) {
			modificateur = getModificateur().getNomAndPrenom();
		}
		return modificateur;
	}

	@Deprecated //utliser dCreation
	public NSTimestamp retourneDateCreation() {
		return dCreation();
	}

	@Deprecated //utliser dModification
	public NSTimestamp retourneDateModification() {
		return dModification();
	}

	public boolean isDoctorant(){
		boolean isSpecifite = false;
		//FIXME changer le fonctionnement pour individu a un enregistrement lié dans la table Etudiant ?
		if (isIndividu()) {

			if (hasQualiteDoctorant()) {
				isSpecifite = true;
			}
		}
		return isSpecifite;
	}

	public boolean isEtudiant() {
		return EOIndividuForEtudiantSpec.sharedInstance().isSpecificite(this);
	}

	public boolean isEnseignant() {
		boolean isEnseignant = false;

		EOQualifier qualifier = ERXQ.equals(EOVPersonnelActuelEns.NO_DOSSIER_PERS_KEY, noIndividu());		
		NSArray<EOVPersonnelActuelEns> enseignants = EOVPersonnelActuelEns.fetchAll(this.editingContext(), qualifier, null);		

		//NSArray<EOVPersonnelActuelEns> enseignants = EOVPersonnelActuelEns.fetchAll(this.editingContext(), EOVPersonnelActuelEns.NO_DOSSIER_PERS_KEY, this.NO_INDIVIDU_KEY, null);

		if (enseignants.size() > 0) {
			isEnseignant = true;
		}

		return isEnseignant;
	}

	public boolean isNonEnseignant() {
		return !isEnseignant();
	}

	public boolean isPersonnel() {
		return EOIndividuForPersonnelSpec.sharedInstance().isSpecificite(this);
	}

	public boolean isEtranger() {
		EOPays paysNationalite = toPaysNationalite();
		return paysNationalite != null && !paysNationalite.isPaysDefaut(); 
	}

	@Override
	public void setPriseCptInsee(String value) {
		super.setPriseCptInsee(value);
		if (EOIndividu.PRISE_CPT_INSEE_P.equals(value)) {
			setIndNoInsee(null);
			setIndCleInsee(null);
		}
	}

	public CodeInsee getCodeInsee() {
		if (StringCtrl.isEmpty(indNoInsee())) {
			return null;
		}

		return new CodeInsee(indNoInsee());
	}

	public CodeInsee getCodeInseeProv() {
		if (StringCtrl.isEmpty(indNoInseeProv())) {
			return null;
		}

		return new CodeInsee(indNoInseeProv());
	}

	public Integer getCleInsee() {
		return indCleInsee();
	}

	public Integer getCleInseeProv() {
		return indCleInseeProv();
	}

	@Deprecated
	public NSTimestamp dateNaissance() {
		return dNaissance();
	}

	public Date getDtNaissance() {
		return dNaissance();
	}

	public Date getDtNaturalisation() {
		return dNaturalisation();
	}

	public IPays getPaysNaissance() {
		return toPaysNaissance();
	}

	public IDepartement getDepartement() {
		return toDepartement();
	}

	public ICivilite getCivilite() {
		return toCivilite();
	}


	// METHODES RAJOUTEES

	public static final NSArray<EOSortOrdering> INDIVIDU_SORT = new NSArray<EOSortOrdering>(
			new EOSortOrdering[] {
					EOSortOrdering.sortOrderingWithKey(NOM_USUEL_KEY, EOSortOrdering.CompareAscending),
					EOSortOrdering.sortOrderingWithKey(PRENOM_KEY, EOSortOrdering.CompareAscending)
			}
	);

	public static final String NOM_PRENOM_KEY = "nomPrenom";

	public String display() {
		return nomPrenom();
	}

	private static CktlDataBus _criDataBusForDisplayLogin = new CktlDataBus(new EOEditingContext());

	/**
	 * Affichage du noIndividu et du login
	 * 
	 * @return
	 */
	public String noIndividuLogin() {
		String display = Integer.toString(noIndividu().intValue());
		CktlUserInfoDB ui = new CktlUserInfoDB(_criDataBusForDisplayLogin);
		ui.compteForPersId(persId(), false);
		if (ui != null) {
			display += " / " + ui.login();
		}
		return display;
	}

	public String nomPrenomQualite() {
		return nomUsuel() + " " + prenom()
		+ (!StringCtrl.isEmpty(indQualite()) ? " (" + indQualite() + ")" : "");
	}

	public String nomPrenom() {
		return nomUsuel() + " " + prenom();
	}


	// recherche

	public static final String NOM_USUEL_BASIC_KEY = NOM_USUEL_KEY + "Basic";
	public static final String PRENOM_BASIC_KEY = PRENOM_KEY + "Basic";

	public String nomUsuelBasic() {
		return StringCtrl.toBasicString(StringCtrl.chaineSansAccents(nomUsuel()));
	}

	public String prenomBasic() {
		return StringCtrl.toBasicString(StringCtrl.chaineSansAccents(prenom()));
	}

	/**
	 * Construire un qualifier qui permet une recherche d'un individu d'apres un
	 * nom et/ou un prenom. Le lien vers l'individu doit etre nommee
	 * <code>toIndividu</code> sur l'entite que l'on va fetcher avec ce qualifier
	 * 
	 * @param nomOuPrenom nom
	 * @return le qualifier
	 */
	public static EOQualifier getQualifierForNomOrPrenom(String nomOuPrenom) {
		NSArray<String> mots = NSArray.componentsSeparatedByString(nomOuPrenom, " ");
		String premierMot, deuxiemeMot;
		premierMot = deuxiemeMot = "";
		if (mots.count() > 0) {
			premierMot = (String) mots.objectAtIndex(0);
			if (mots.count() > 1) {
				deuxiemeMot = (String) mots.objectAtIndex(1);
			}
			NSArray<String> args = new NSArray<String>(new String[] {
					"*" + nomOuPrenom + "*",
					"*" + nomOuPrenom + "*",
					"*" + premierMot + "*",
					"*" + deuxiemeMot + "*",
					"*" + premierMot + "*",
					"*" + deuxiemeMot + "*" });
			String strQual = "toIndividu." + NOM_USUEL_KEY + " like %@ OR "
			+ "toIndividu." + PRENOM_KEY + " like %@ OR "
			+ "(toIndividu." + NOM_USUEL_KEY + " like %@ AND toIndividu." + PRENOM_KEY + " like %@) OR "
			+ "(toIndividu." + PRENOM_KEY + " like %@ AND toIndividu." + NOM_USUEL_KEY + " like %@)";
			return EOQualifier.qualifierWithQualifierFormat(strQual, args);
		}
		return null;
	}

	
	
	/**
	 * trouver un individu selon un nom et/ou un prenom
	 * 
	 * @param ec
	 * @param nomOuPrenom
	 * @param qualOptionnel
	 * @param onlyInPersonnelActuel
	 *          : <code>true</code> : restreindre la recherche aux personnels,
	 *          <code>false</code>
	 * @return
	 */
	public static NSArray<EOIndividu> findIndividuForNomOrPrenom(
			EOEditingContext ec, String nomOuPrenom, EOQualifier qualOptionnel, boolean onlyInPersonnelActuel) {
		// mise en caps
		nomOuPrenom = nomOuPrenom.toUpperCase();
		NSArray<EOIndividu> findIndividuForNomOrPrenom = new NSArray<EOIndividu>();
		NSArray<String> mots = NSArray.componentsSeparatedByString(nomOuPrenom, " ");
		String premierMot, deuxiemeMot;
		premierMot = deuxiemeMot = "";
		if (mots.count() > 0) {
			premierMot = mots.objectAtIndex(0);
			if (mots.count() > 1) {
				deuxiemeMot = mots.objectAtIndex(1);
			}
			NSArray<String> args = new NSArray<String>(new String[] {
					"*" + nomOuPrenom + "*",
					"*" + nomOuPrenom + "*",
					"*" + premierMot + "*",
					"*" + deuxiemeMot + "*",
					"*" + premierMot + "*",
					"*" + deuxiemeMot + "*" });

			String strQual =
				"(" + NOM_USUEL_KEY + "  like %@ OR "
				+ PRENOM_KEY + "  like %@ OR "
				+ "(" + NOM_USUEL_KEY + "  like %@ AND " + PRENOM_KEY + "  like %@) OR "
				+ "(" + PRENOM_KEY + "  like %@ AND " + NOM_USUEL_KEY + "  like %@)"
				+ ")";
			if (onlyInPersonnelActuel) {

				strQual += " and " + TO_V_PERSONNEL_ACTUELS_KEY + "."
				+ EOVPersonnelActuel.TO_INDIVIDU_KEY + "." 
				+ EOIndividu.PERS_ID_KEY + " <> nil";
			}
			EOQualifier qual = CktlDataBus.newCondition(strQual, args);
			if (qualOptionnel != null) {
				qual = ERXQ.and(qual, qualOptionnel);
			}
			findIndividuForNomOrPrenom = fetchAll(ec, qual, INDIVIDU_SORT);
			findIndividuForNomOrPrenom = NSArrayCtrl.removeDuplicate(findIndividuForNomOrPrenom);
		}
		return findIndividuForNomOrPrenom;
	}
	

	/**
	 * trouver un individu selon un nom et/ou un prenom
	 * 
	 * @param ec
	 * @param nomOuPrenom
	 * @param onlyInPersonnelActuel
	 *          : <code>true</code> : restreindre la recherche aux personnels,
	 *          <code>false</code>
	 * @return
	 */
	public static NSArray<EOIndividu> findIndividuForNomOrPrenom(
			EOEditingContext ec, String nomOuPrenom, boolean onlyInPersonnelActuel) {
		return findIndividuForNomOrPrenom(ec, nomOuPrenom, null, onlyInPersonnelActuel); 
	}

	/**
	 * Pour un service donne, detailler le niveau de responsabilite de la personne
	 * via GRP_RESPONSABLE. Les droits de lecture sont herites par les
	 * responsables de toutes structures peres => c'est ce qu'on affiche.
	 * 
	 * @param fromStructure
	 * @return
	 */
	public String tipHeritageAnnuaire(EOStructure fromStructure) {
		StringBuffer sb = new StringBuffer();

		boolean shouldContinue = true;
		EOStructure structure = fromStructure;
		while (shouldContinue) {
			if (structure == structure.toStructurePere()) {
				shouldContinue = false;
			}
			if (structure.toResponsable() == this) {
				if (sb.length() == 0) {
					sb.append("Responsable de ");
				} else {
					sb.append(" / ");
				}
				sb.append(structure.displayUltraCourt());
			}
			structure = structure.toStructurePere();
		}

		return sb.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToCiviliteRelationship(ICivilite civilite) {
		super.setToCiviliteRelationship((EOCivilite) civilite);
	}
	
	public boolean estTitulaire() {
		return this.toPersonnel() != null && this.toPersonnel().temTitulaire() != null && this.toPersonnel().temTitulaire().equals(CocktailConstantes.VRAI);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IAdresse> toAdresses() {
		return getPersonneDelegate().adresses();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ITelephone> toTelephones() {
		return new NSArray<ITelephone>(toPersonneTelephones());
	}

	/**
	 * {@inheritDoc}
	 */
	public void setCleInsee(Integer clef) {
		setIndCleInsee(clef);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToPaysNationaliteRelationship(IPays pays) {
		super.setToPaysNationaliteRelationship((EOPays) pays);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setDNaissance(Date dNaissance) {
		setDNaissance((NSTimestamp) dNaissance);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToPaysNaissanceRelationship(IPays pays) {
		setToPaysNaissanceRelationship((EOPays) pays);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToDepartementRelationship(IDepartement departement) {
		setToDepartementRelationship((EODepartement) departement);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToSituationFamilialeRelationship(
			ISituationFamiliale situation) {
		setToSituationFamilialeRelationship((EOSituationFamiliale) situation);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean hasNoInseeProvisoire() {
		return PRISE_CPT_INSEE_P.equals(priseCptInsee());
	}

	/**
	 * {@inheritDoc}
	 */
	public void setHasNoInseeProvisoire(boolean inseeProvisoire) {
		if (inseeProvisoire) {
			setPriseCptInsee(PRISE_CPT_INSEE_P);
		} else {
			setPriseCptInsee(PRISE_CPT_INSEE_R);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void supprimerAdresse(IAdresse adresse, ITypeAdresse type) {
		NSArray<EORepartPersonneAdresse> repartsAdresses = toRepartPersonneAdresses(
				EORepartPersonneAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE).eq(type.tadrCode())
				.and(EORepartPersonneAdresse.TO_ADRESSE.dot(EOAdresse.ADR_ORDRE).eq(adresse.adrOrdre())));
		for (EORepartPersonneAdresse repart : repartsAdresses.immutableClone()) {
			repart.supprimer();
		}
	}

}
