package org.cocktail.fwkcktlpersonne.common.metier;

public interface ISituationFamiliale {

	public static final String MARIE = "M";
	public static final String CONCUBINAGE = "U";
	public static final String PACS = "P";
	boolean isEnCouple();
	String lSituationFamille();
	
}
