/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.*;

public class EOPassageEchelon extends _EOPassageEchelon {

    public EOPassageEchelon() {
        super();
    }
   
    /** Recherche les passages echelon pour un grade et un echelon donnes */
	public static NSArray rechercherPassageEchelonOuvertPourGradeEtEchelon(EOEditingContext editingContext,String grade,String echelon,NSTimestamp dateReference,boolean enOrdreDecroissant) {
		NSMutableArray args = new NSMutableArray(grade);
		String qualifier = C_GRADE_KEY + "= %@";
		if (echelon != null) {
			args.addObject(echelon);
			qualifier = qualifier + " AND cEchelon = %@";
		}
		if (dateReference != null) {
			args.addObject(dateReference);
			qualifier = qualifier + " AND (dFermeture = nil OR dFermeture >= %@)";
		} else {
			qualifier = qualifier + " AND dFermeture = nil";
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifier , args);
		// on ne fait pas le tri par la base de données car il ne renvoie pas les classements selon l'ordre alphabétique
		// avec les numériques en tête correctement (du moins avec Oracle)
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, qual, null);
		NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
		NSArray sorts = null;
		if (enOrdreDecroissant) {
			sorts = new NSArray (new EOSortOrdering(C_ECHELON_KEY,EOSortOrdering.CompareDescending));
		} else {
			sorts = new NSArray (new EOSortOrdering(C_ECHELON_KEY,EOSortOrdering.CompareAscending));
		}
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sorts);
	}

}
