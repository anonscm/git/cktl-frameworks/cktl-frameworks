/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IVlans;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXS;

public class EOVlans extends _EOVlans implements IVlans{

	public static final String VLAN_P = "P";
	public static final String VLAN_R = "R";
	public static final String VLAN_E = "E";
	public static final String VLAN_X = "X";
	public static final String VLAN_G = "G";
	public static final String VLAN_D = "D";
	
	public static final String PARAM_GRHUM_VLAN_EXTERNE_KEY = EOGrhumParametres.PARAM_GRHUM_VLAN_EXTERNE_KEY;
	public static final String PARAM_GRHUM_VLAN_ETUD_KEY = EOGrhumParametres.PARAM_GRHUM_VLAN_ETUD_KEY;
	public static final String PARAM_GRHUM_VLAN_ADMIN_KEY= EOGrhumParametres.PARAM_GRHUM_VLAN_ADMIN_KEY;
	
	public static final EOQualifier QUAL_VLAN_D = new EOKeyValueQualifier(EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_D);
	public static final EOQualifier QUAL_VLAN_G = new EOKeyValueQualifier(EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_G);
	
	public static final EOQualifier QUAL_VLAN_P = new EOKeyValueQualifier(EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_P);
	public static final EOQualifier QUAL_VLAN_R = new EOKeyValueQualifier(EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_R);
	public static final EOQualifier QUAL_VLAN_E = new EOKeyValueQualifier(EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_E);
	public static final EOQualifier QUAL_VLAN_X = new EOKeyValueQualifier(EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_X);

	public static final EOQualifier QUAL_GROUPE_U_OU_D = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
			QUAL_VLAN_D, QUAL_VLAN_G
	}));
	
	public EOVlans() {
		super();
	}

	/**
	 * @param context
	 * @return Les différents domaines indiqués dans la table VLAN. Normalement les domaines "internes" à l'établissement.
	 */
	public static NSArray getDomainesDistinct(EOEditingContext context) {
		
		NSArray res = EOVlans.fetchAll(context, EOQualifier.qualifierWithQualifierFormat(EOVlans.DOMAINE_KEY + "<>nil", null));
		NSMutableArray domainesInternes = new NSMutableArray();
		
		domainesInternes.addObjectsFromArray((NSArray) res.valueForKeyPath(EOVlans.DOMAINE_KEY));
		NSArrayCtrl.removeDuplicatesInNSArray(domainesInternes);
		
		return domainesInternes.immutableClone();
	
	}
	
	public static NSArray<EOVlans> fetchAllVLansAPrendreEnCompte(EOEditingContext editingContext) {
        return EOVlans.fetchAll(editingContext, EOVlans.PRISE__COMPTE_KEY, "O", ERXS.asc(EOVlans.C_VLAN_KEY).array());
    }
	
	public static NSArray<EOVlans> fetchAllVLansAPrendreEnComptePourStructure(EOEditingContext context, IPersonne personne) {
		EOStructure structure = (EOStructure) personne;

		if(structure.laStructurePossedeUnTypeGroupeUnix()) {
			return EOVlans.fetchAll(context, QUAL_VLAN_G);
		}

		if(structure.laStructurePossedeUnTypeGroupeDiplome()) {
			return EOVlans.fetchAll(context, QUAL_VLAN_D);
		}
		
		NSArray<EOVlans> resultats = EOVlans.fetchAll(context, EOVlans.C_VLAN.eq(VLAN_X));
		if(EOStructureForFournisseurSpec.sharedInstance().isSpecificite(structure)) {
			return EOVlans.fetchAll(context, EOVlans.C_VLAN.eq(VLAN_X));
		}
		return resultats;
		
	}

	public static NSArray<EOVlans> fetchAllVlansPourPersonne(IPersonne personne, EOEditingContext editingContext) {
		
		if(personne.isIndividu()) {
			return fetchAllVLansAPrendreEnCompte(editingContext); 
		} else if(personne.isStructure()) {
			return fetchAllVLansAPrendreEnComptePourStructure(editingContext, personne);
		}
		
		return new NSArray<EOVlans>();
	}
	
}
