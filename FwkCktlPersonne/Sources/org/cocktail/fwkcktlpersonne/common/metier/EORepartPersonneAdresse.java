/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EORepartPersonneAdresse.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Enumeration;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForEtudiantSpec;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRepartPersonneAdresse;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 * Association entre une personne et une adresse.
 * 
 * <pre>
 * EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(editingContext, EOTypeAdresse.QUAL_TADR_CODE_FACT);
 * 
 * EORepartPersonneAdresse repartPersonneAdresse = EORepartPersonneAdresse.creerInstance(editingContext);
 * repartPersonneAdresse.initForPersonne(editingContext, structure, adresse, typeAdresse);
 * repartPersonneAdresse.setRpaPrincipal(EORepartPersonneAdresse.RPA_PRINCIPAL_OUI);
 * repartPersonneAdresse.setRpaValide(EORepartPersonneAdresse.RPA_VALIDE_OUI);
 * 
 * </pre>
 * 
 * @author rprin
 */
public class EORepartPersonneAdresse extends _EORepartPersonneAdresse implements IRepartPersonneAdresse {
	public static final String RPA_VALIDE_OUI = OUI;
	public static final String RPA_VALIDE_NON = NON;
	public static final String RPA_PRINCIPAL_OUI = OUI;
	public static final String RPA_PRINCIPAL_NON = NON;
	public static final EOQualifier QUAL_RPA_PRINCIPAL = EOQualifier.qualifierWithQualifierFormat(RPA_PRINCIPAL_KEY + "=%@", new NSArray(new Object[] { OUI }));

	public static final EOQualifier QUAL_PERSONNE_ADRESSE_INVAL = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY,
	    EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_INVAL);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_PERSO = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY,
	    EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_PERSO);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_PRO = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY,
	    EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_PRO);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_FACT = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY,
	    EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_FACT);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_LIVR = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY,
	    EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_LIVR);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_PAR = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY,
	    EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_PAR);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_ETUD = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY,
	    EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_ETUD);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_VALIDE = new EOKeyValueQualifier(EORepartPersonneAdresse.RPA_VALIDE_KEY,
	    EOQualifier.QualifierOperatorEqual, EORepartPersonneAdresse.RPA_VALIDE_OUI);
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_VALIDE_SANS_PERSO = new EOAndQualifier(new NSArray(new Object[] { QUAL_PERSONNE_ADRESSE_VALIDE,
	    new EONotQualifier(QUAL_PERSONNE_ADRESSE_INVAL), new EONotQualifier(QUAL_PERSONNE_ADRESSE_PERSO) }));
	public static final EOQualifier QUAL_PERSONNE_ADRESSE_VALIDE_PRO = new EOAndQualifier(new NSArray(new Object[] { QUAL_PERSONNE_ADRESSE_VALIDE,
	    QUAL_PERSONNE_ADRESSE_PRO }));

	public static final String DISPLAY_NON_DEFINIE = "Non définie";

	public EORepartPersonneAdresse() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories.<br/>
	 * Verifications :
	 * <ul>
	 * <li>Exception si plusieurs adresses de type facturation sont affectees</li>
	 * </ul>
	 * On ne verifie pas si l'adresse email est deja affectee a une autre personne car dans certains cas ca peut arriver (exemple membre d'une association avec un
	 * seul email).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		setDModification(AUtils.now());
		if (!MyStringCtrl.isEmpty(eMail())) {
			if (eMail().length() > 120) {
				throw new NSValidation.ValidationException("Une adresse email comporte au plus 120 caractères !");
			} else if (!MyStringCtrl.isEmailValid(eMail())) {
				throw new NSValidation.ValidationException("L'adresse email n'est pas valide !");
			}

			// Verifier si le domaine exemple est encore présent dans cemDomaine apres le parsing
			String domaineARemplacer = EOGrhumParametres.parametrePourCle(this.editingContext(), EOGrhumParametres.PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER);
			// if (!MyStringCtrl.isEmpty(domaineARemplacer) && eMail().contains(domaineARemplacer)) {
			// //TODO : éventuellement faire un match plus poussé sur les parties du domaine exemple
			// throw new NSValidation.ValidationException("Dans l'adresse email le domaine '" + domaineARemplacer + "' est un exemple et "
			// + "doit être remplacé par un nom de domaine valide.");
			// }
			if (!MyStringCtrl.isEmpty(domaineARemplacer) && eMail().contains(domaineARemplacer)) {
				setEMail(null);
			}
		}

		// verifier que adresse pas affectee au fournisseur si rpavalide=N
		if (!OUI.equals(rpaValide())) {
			if (toPersonne().toFournis() != null) {
				if (toPersonne().toFournis().toAdresse() != null && toPersonne().toFournis().toAdresse().equals(toAdresse())) {
					throw new NSValidation.ValidationException("L'adresse (adr_ordre=" + toPersonne().toFournis().toAdresse().adrOrdre()
					    + ") est associée a l'objet fournisseur, elle ne peut pas être définie comme invalide sauf si une autre adresse de facturation valide existe déjà.");
				}
			}
		}

		if (EOIndividuForEtudiantSpec.sharedInstance().isSpecificite(this)) {

		}

		// On ne verifie pas si l'adresse email est deja affectee a une autre personne car dans certains cas ca peut arriver (exemple membre d'une association avec
		// un seul email).
		// verifier si cette adresse est affectee a un compte d'une autre personne
		// EOCompteEmail.checkCompteEmailDoublon(this.editingContext(), this.persId(), eMail());

		super.validateObjectMetier();
	}

	/**
	 * @return toStructure() ou bien toIndiviodu() selon le cas.
	 */
	public IPersonne toPersonne() {
		return (toStructure() != null ? (IPersonne) toStructure() : (IPersonne) toIndividu());
	}

	public EOIndividu toIndividu() {
		return (EOIndividu) ((toIndividus() != null && toIndividus().count() > 0) ? toIndividus().objectAtIndex(0) : null);
	}

	public EOStructure toStructure() {
		return (EOStructure) ((toStructures() != null && toStructures().count() > 0) ? toStructures().objectAtIndex(0) : null);
	}

	/**
	 * Verifie si l'adresse email est deja affectee dans un repartPersonneAdresse pour une autre personne.
	 * 
	 * @param ec
	 * @param persId
	 * @param email
	 * @throws NSValidation.ValidationException si l'adresse est deja affectee.
	 */
	public static void checkEmailDoublon(EOEditingContext ec, Number persId, String email) throws NSValidation.ValidationException {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.PERS_ID_KEY + "<>%@", new NSArray(new Object[] { persId })));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY + "=%@", new NSArray(
		    new Object[] { EORepartPersonneAdresse.RPA_VALIDE_OUI })));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.E_MAIL_KEY + "=%@ ", new NSArray(new Object[] { email })));

		NSArray res = EORepartPersonneAdresse.fetchAll(ec, new EOAndQualifier(quals), null);
		Enumeration en = res.objectEnumerator();
		while (en.hasMoreElements()) {
			EORepartPersonneAdresse obj = (EORepartPersonneAdresse) en.nextElement();
			IPersonne doublon = null;
			doublon = obj.toIndividu();
			if (doublon == null) {
				doublon = obj.toStructure();
			}
			if (doublon != null) {
				throw new NSValidation.ValidationException("L'adresse email (" + email + ") est deja affectee a " + doublon.getNomPrenomAffichage() + " (N° "
				    + doublon.getNumero() + ").");
			}
		}
	}

	public void initForPersonne(EOEditingContext ec, IPersonne personne, EOAdresse adresse, EOTypeAdresse typeAdresse) {
		setToPersonneRelationship(personne);
		// this.setPersId(personne.persId());
		this.setToTypeAdresseRelationship(typeAdresse.localInstanceIn(adresse.editingContext()));
		// if (typeAdresse != null) {
		// this.setTadrCode(typeAdresse.tadrCode());
		// }
		this.setToAdresseRelationship(adresse);

		// this.setAdrOrdre(adresse.adrOrdre());
		if (EOQualifier.filteredArrayWithQualifier(personne.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE), QUAL_RPA_PRINCIPAL)
		    .count() == 0) {
			this.setRpaPrincipal(OUI);
		} else {
			this.setRpaPrincipal(NON);
		}
	}

	@Override public void setToAdresseRelationship(EOAdresse value) {
		super.setToAdresseRelationship(value);
		if (value == null) {
			setAdrOrdre(null);
		} else {
			setAdrOrdre(value.adrOrdre());
		}
	}

	@Override public void setToTypeAdresseRelationship(EOTypeAdresse value) {
		super.setToTypeAdresseRelationship(value);
		if (value == null) {
			this.setTadrCode(null);
		} else {
			this.setTadrCode(value.tadrCode());
		}

	}

	public void initAdresseEntreprise(Integer persId, EOAdresse adresse) {
		setRpaValide(OUI);
		setPersId(persId);
		setRpaPrincipal(OUI);
		setTadrCode("PRO");
		addObjectToBothSidesOfRelationshipWithKey(adresse, "toAdresse");
	}

	/**
	 * Initialise les champs de la repart courante avec ceux de la repart pass&eacute;e en param&egrave;tre sans prendre en compte l'adresse de cette
	 * derni&egrave;re
	 */
	public void initAvecIdEtRepartAdresse(Integer persId, EORepartPersonneAdresse repart) {
		setRpaValide(repart.rpaValide());
		setRpaPrincipal(repart.rpaPrincipal());
		setTadrCode(repart.tadrCode());
		setEMail(repart.eMail());
		setPersId(persId);
	}

	public boolean estValide() {
		return rpaValide().equals(OUI);
	}

	public boolean estAdressePrincipale() {
		return rpaPrincipal().equals(OUI);
	}

	public void setEstAdressePrincipale(boolean aBool) {
		if (aBool) {
			setRpaPrincipal(OUI);
		} else {
			setRpaPrincipal(NON);
		}
	}

	public boolean estUtiliseePaye() {
		return toAdresse() != null && toAdresse().temPayeUtil() != null && toAdresse().temPayeUtil().equals(OUI);
	}

	public String typeAdresse() {
		if (tadrCode() != null) {
			return ((EOTypeAdresse) AFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOTypeAdresse.ENTITY_NAME, EOTypeAdresse.TADR_CODE_KEY,
			    tadrCode())).tadrLibelleCourt();
		} else {
			return null;
		}
	}

	// méthodes statiques
	/**
	 * recherche les adresses de l'individu
	 * 
	 * @param editingContext
	 * @param individu
	 * @return repartPersoAdresses
	 */
	public static NSArray adresses(EOEditingContext editingContext, EOIndividu individu) {
		return adressesDeType(editingContext, individu, null, false);
	}

	/**
	 * recherche les adresses de l'individu
	 * 
	 * @param editingContext
	 * @param personne
	 * @param typeAdresse type adresse
	 * @return repartPersoAdresses
	 */
	public static NSArray adressesValidesDeType(EOEditingContext editingContext, IPersonne personne, String typeAdresse) {
		return adressesDeType(editingContext, personne, typeAdresse, true);
	}

	/**
	 * recherche les adresses de facturation de l'individu
	 * 
	 * @param editingContext
	 * @param individu
	 * @return repartPersoAdresses
	 */
	public static NSArray adressesFacturationsValides(EOEditingContext editingContext, EOIndividu individu) {
		return adressesDeType(editingContext, individu, EOTypeAdresse.TADR_CODE_FACT, true);
	}

	/**
	 * recherche les adresses personnelles de l'individu
	 * 
	 * @param editingContext
	 * @param personne
	 * @return repartPersoAdresses
	 */
	public static NSArray adressesPersoValides(EOEditingContext editingContext, IPersonne personne) {
		return adressesDeType(editingContext, personne, EOTypeAdresse.TADR_CODE_PERSO, true);
	}

	// méthodes privées
	// typeAdresse peut être nul
	// uniquementValide = true si on veut les repart adresses avec une rpa valide
	private static NSArray adressesDeType(EOEditingContext editingContext, IPersonne personne, String typeAdresse, boolean uniquementValide) {
		NSMutableArray args = new NSMutableArray(personne.persId());
		String stringQualifier = "persId = %@";
		if (typeAdresse != null) {
			args.addObject(typeAdresse);
			stringQualifier = stringQualifier + " AND " + EORepartPersonneAdresse.TADR_CODE_KEY + " = %@";
		}
		if (uniquementValide) {
			args.addObject(OUI);
			stringQualifier = stringQualifier + " AND " + EORepartPersonneAdresse.RPA_VALIDE_KEY + " = %@";
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		// EOFetchSpecification fs = new EOFetchSpecification("RepartPersonneAdresse", qualifier, null);
		EOFetchSpecification fs = new EOFetchSpecification(EORepartPersonneAdresse.ENTITY_NAME, qualifier, null);
		NSMutableArray prefetches = new NSMutableArray("toAdresse");
		fs.setPrefetchingRelationshipKeyPaths(prefetches);

		return editingContext.objectsWithFetchSpecification(fs);
	}

	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setRpaValide(RPA_VALIDE_OUI);
		setDCreation(AUtils.now());
	}

	/**
	 * Si value="O", les autres repartPersonneAdresse sont mis à rpaPrincipal N.
	 */
	public void setRpaPrincipal(String value) {
		super.setRpaPrincipal(value);
		// Mettre les autres adresses a N
		if (OUI.equals(value)) {
			NSArray autresTels = null;
			if (this.toIndividu() != null) {
				autresTels = this.toIndividu().toRepartPersonneAdresses();
			} else if (this.toStructure() != null) {
				autresTels = this.toStructure().toRepartPersonneAdresses();
			}
			if (autresTels != null) {
				for (int i = 0; i < autresTels.count(); i++) {
					EORepartPersonneAdresse repart = (EORepartPersonneAdresse) autresTels.objectAtIndex(i);
					if (!NON.equals(repart.rpaPrincipal())) {
						repart.setRpaPrincipal(NON);
						repart.setDModification(AUtils.now());
					}
				}
			}
		}
		super.setRpaPrincipal(value);
	}

	/**
	 * Methode de convenance pour utilisation avec des bindings.
	 * 
	 * @return Le libelle du type de l'adrese avec mention "(Principale)" si rpaPprincipal est a OUI. Par exemple : Adresse professionnelle (principale).
	 */
	public String getDisplayTypeEtPrincipale() {
		if (toTypeAdresse() == null) {
			return DISPLAY_NON_DEFINIE;
		}
		return toTypeAdresse().tadrLibelleCourt() + (OUI.equals(rpaPrincipal()) ? " (principale)" : "");
	}

	/**
	 * Suppression d'un objet repartPersonneAdresse : le rpaVAlide est mis à N si l'objet est deja dans la BD, sinon il est supprimé de l'editingContext. Le
	 * rpaPrincipal est mis à N. Si l'adresse associée n'est plus reliée, on la la laisse telle quelle.
	 */
	public void supprimer() {
		EOAdresse adresse = this.toAdresse();
		// metre rpavalide a N (laisser adresse telle quelle).
		EOQualifier qual = new EOKeyValueQualifier(EORepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, adresse);
		if (this.hasTemporaryGlobalID()) {
			NSArray reparts = toPersonne().toRepartPersonneAdresses(qual);
			setToAdresseRelationship(null);
			if (reparts.count() <= 1 && adresse.hasTemporaryGlobalID()) {
				editingContext().deleteObject(adresse);
			}
			toPersonne().removeFromToRepartPersonneAdressesRelationship(this);
			editingContext().deleteObject(this);
		} else if (toPersonne().toRepartPersonneAdresses(qual).count() == 1) {
			setRpaValide(RPA_VALIDE_NON);
			setRpaPrincipal(NON);
		} else {
			setToAdresseRelationship(null);
			toPersonne().removeFromToRepartPersonneAdressesRelationship(this);
			editingContext().deleteObject(this);
		}
	}

	public void setToPersonneRelationship(IPersonne personne) {
		if (personne == null) {
			while (toIndividus().count() > 0) {
				removeFromToIndividusRelationship((EOIndividu) toIndividus().objectAtIndex(0));
			}
			while (toStructures().count() > 0) {
				removeFromToStructuresRelationship((EOStructure) toStructures().objectAtIndex(0));
			}
			setPersId(null);
		} else {
			if (personne instanceof EOIndividu) {
				this.addToToIndividusRelationship((EOIndividu) personne);
			}
			if (personne instanceof EOStructure) {
				this.addToToStructuresRelationship((EOStructure) personne);
			}
			setPersId(personne.persId());
		}
	}

	/**
	 * Construit une proposition d'adresse email canonique (prenoms.nom) pour la personne (individu) donnée avec comme domaine la valeur du paramètre :
	 * {@link EOGrhumParametres#PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER}. Les prénoms et noms composés sont séparés par des '-' et le nom par un '.', les
	 * caractères accentués sont remplacés. Exemple pour la personne : Pierre yves Marie revéra : 'pierre-yves.marie@domain.fr'.
	 * 
	 * @param personne : la personne pour laquelle il faut construire l'adresse email
	 * @param edc : EOEditingContext pour faire la requete du paramètre
	 * @return l'adresse canonique correspondant au prenom.nom@domain.fr de la personne donnée.
	 */
	public static String computeCanonicalEmailAdresseFromPersonne(IPersonne personne, EOEditingContext edc) {
		String returnEmail = "";

		if (personne != null && personne.isIndividu()) {
			EOIndividu individu = (EOIndividu) personne;
			String domaineARemplacer = EOGrhumParametres.parametrePourCle(edc, EOGrhumParametres.PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER);
			if (!MyStringCtrl.isEmpty(domaineARemplacer)) {
				StringBuffer emailBuff = new StringBuffer(MyStringCtrl.chaineSansAccents(individu.prenomAffichage().trim()).toLowerCase().replace(" ", "-"))
				    .append(".").append(MyStringCtrl.chaineSansAccents(individu.nomAffichage().trim()).toLowerCase().replace(" ", "-")).append("@")
				    .append(domaineARemplacer);
				returnEmail = emailBuff.toString();
			}
		}
		return returnEmail;
	}

	// /**
	// * Construit une proposition d'adresse email canonique (prenoms.nom) pour la personne (individu) donnée avec comme domaine la valeur du paramètre
	// * : {@link EOGrhumParametres#PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER}. Les prénoms et noms composés sont séparés par des '-' et le nom par un
	// * '.', les caractères accentués sont remplacés. Exemple pour la personne : Pierre yves Marie revéra : 'pierre-yves.marie@domain.fr'.
	// *
	// * @param personne : la personne pour laquelle il faut construire l'adresse email
	// * @param edc : EOEditingContext pour faire la requete du paramètre
	// * @return l'adresse canonique correspondant au prenom.nom@domain.fr de la personne donnée.
	// */
	public static String computeCanonicalEmailAdresseAvecDomaineFromPersonne(IPersonne personne, EOEditingContext edc, EOVlans leVlan) {
		String returnEmail = "";

		if (personne != null && personne.isIndividu()) {
			EOIndividu individu = (EOIndividu) personne;
			String nomDomaine = null;
			String domaineARemplacer = EOGrhumParametres.parametrePourCle(edc, EOGrhumParametres.PARAM_ANNUAIRE_EMAIL_DOMAIN_A_REMPLACER);

			if (!MyStringCtrl.isEmpty(leVlan.domaine())) {
				if (EOCompteEmail.getNomSecondDomain() != null) {
					nomDomaine = EOCompteEmail.getNomSecondDomain();
				} else {
					nomDomaine = leVlan.domaine();
				}
				StringBuffer emailBuff = new StringBuffer(MyStringCtrl.chaineSansAccents(individu.prenomAffichage().trim()).toLowerCase().replace(" ", "-"))
				    .append(".").append(MyStringCtrl.chaineSansAccents(individu.nomAffichage().trim()).toLowerCase().replace(" ", "-")).append("@")
				    // .append(leVlan.domaine());
				    .append(nomDomaine);
				returnEmail = emailBuff.toString();
			} else if (!MyStringCtrl.isEmpty(domaineARemplacer)) {
				StringBuffer emailBuff = new StringBuffer(MyStringCtrl.chaineSansAccents(individu.prenomAffichage().trim()).toLowerCase().replace(" ", "-"))
				    .append(".").append(MyStringCtrl.chaineSansAccents(individu.nomAffichage().trim()).toLowerCase().replace(" ", "-")).append("@")
				    .append(domaineARemplacer);
				returnEmail = emailBuff.toString();
			}

		}
		return returnEmail;
	}

	@Override public void setEMail(String value) {
		if (value != null) {
			value = value.trim();
		}
		super.setEMail(value);
	}
}
