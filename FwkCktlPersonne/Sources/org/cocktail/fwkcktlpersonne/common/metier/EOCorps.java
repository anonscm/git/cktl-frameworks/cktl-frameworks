/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOCorps extends _EOCorps {

	public static final EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_CORPS_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
	public static String CORPS_POUR_COMPTABLE = "024";
	public static String CORPS_POUR_AGREFGE_2DEGRE = "551";
	public static final String CORPS_PR = "300";
	public static final String CORPS_MCF = "301";

	public EOCorps() {
		super();
	}

	public Boolean isLocal() {
		return true; //"O".equals(temLocal());
	}

	
	public boolean autoriseDelegation() {
		return temDelegation().equals(CocktailConstantes.VRAI);
	}
	public void setAutoriseDelegation(boolean yn) {
		if (yn)
			setTemDelegation(CocktailConstantes.VRAI);
		else
			setTemDelegation(CocktailConstantes.FAUX);
	}

	
	public static EOCorps creer(EOEditingContext ec, EOTypePopulation typePopulation) {

		EOCorps newObject = (EOCorps) createAndInsertInstance(ec, EOCorps.ENTITY_NAME);    

		newObject.setToTypePopulationRelationship(typePopulation);

		newObject.setDOuvertureCorps(new NSTimestamp());
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		newObject.setTemCfp("N");
		newObject.setTemDelegation("N");
		newObject.setTemLocal("O");
		newObject.setTemSurnombre("N");
		
		return newObject;
	}

	
	public boolean beneficieSurnombre() {
		return temSurnombre() != null && temSurnombre().equals(CocktailConstantes.VRAI);
	}
	public boolean beneficieCrct() {
		return temCrct() != null && temCrct().equals(CocktailConstantes.VRAI);
	}
	public boolean beneficieDelegation() {
		return temDelegation() != null && temDelegation().equals(CocktailConstantes.VRAI);
	}
	public boolean beneficieCfp() {
		return temCfp() != null && temCfp().equals(CocktailConstantes.VRAI);
	}

	public String libelle() {
		String libelle = llCorps();
		if (dOuvertureCorps() != null) {
			if (dFermetureCorps() == null) {
				libelle += " ouvert à partir du " + DateCtrl.dateToString(dOuvertureCorps());
			} else {
				libelle += " ouvert du " + DateCtrl.dateToString(dOuvertureCorps()) + " au " + DateCtrl.dateToString(dFermetureCorps());
			}
		} else if (dFermetureCorps() != null) {
			libelle += " fermé à partir du " + DateCtrl.dateToString(dFermetureCorps());
		}
		return libelle;
	}
	public String code() {
		return cCorps();
	}

	/** recherche les corps selon certains criteres
	 * @param editingContext
	 * @param qualifier
	 * @return tableau des corps
	 */
	public static NSArray rechercherCorpsAvecCriteres(EOEditingContext editingContext,EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier);
	}
	
	public static NSArray rechercherCorpsPourTypePopulation(EOEditingContext ec, EOTypePopulation typePopulation, NSTimestamp dateReference) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		if (typePopulation != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_POPULATION_KEY + "=%@", new NSArray(typePopulation)));
		
		NSMutableArray orQualifiers = new NSMutableArray();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_OUVERTURE_CORPS_KEY + "=nil", null));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_OUVERTURE_CORPS_KEY + "<=%@", new NSArray(dateReference)));
		qualifiers.addObject(new EOOrQualifier(orQualifiers));
		
		orQualifiers = new NSMutableArray();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_CORPS_KEY + "=nil", null));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_CORPS_KEY + ">=%@", new NSArray(dateReference)));
		qualifiers.addObject(new EOOrQualifier(orQualifiers));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);	
	}

	
	public static NSArray rechercherCorpsPourQualifier(EOEditingContext ec, EOQualifier qualifier) {		
		return fetchAll(ec, qualifier, null);	
	}

	
	public static NSArray rechercherCorpsPourTypePopulation(EOEditingContext ec, EOTypePopulation typePopulation) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_POPULATION_KEY + "=%@", new NSArray(typePopulation)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);
		
	}
	
	/** Recherche tous les corps d'un certains types
	 * @param editingContext
	 * @param typeCorps	peut etre nul
	 * @param estFerme	true si le corps est ferme
	 * @return corps trouves
	 */
	public static NSArray rechercherCorpsDeType(EOEditingContext editingContext,String typeCorps,boolean estFerme) {
		String qualifier;
		if (typeCorps != null) {
			if (estFerme) {
				qualifier = "cTypeCorps = '" + typeCorps + "' and dFermetureCorps != nil";
			} else {
				qualifier = "cTypeCorps = '" + typeCorps + "' and dFermetureCorps = nil";
			}			
		} else {
			if (estFerme) {
				qualifier = D_FERMETURE_CORPS_KEY + " != nil";
			} else {
				qualifier = D_FERMETURE_CORPS_KEY + " = nil";
			}
		}

		return rechercherCorpsAvecCriteres(editingContext,EOQualifier.qualifierWithQualifierFormat(qualifier,null));
	}
	/** Recherche tous les corps d'un certains types
	 * @param editingContext
	 * @param estFerme	true si le corps est ferme
	 * @return corps trouves
	 */
	public static NSArray rechercherCorps(EOEditingContext editingContext,boolean estFerme) {
		return rechercherCorpsDeType(editingContext,null,estFerme);
	}


    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     * @throws une exception de validation
     */
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
    
    
    
}
