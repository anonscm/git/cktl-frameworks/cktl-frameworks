/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartEmploi.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EORepartEmploi extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EORepartEmploi.class);

	public static final String ENTITY_NAME = "Fwkpers_RepartEmploi";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REPART_EMPLOI";


// Attribute Keys
  public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
  public static final ERXKey<NSTimestamp> DATE_ENTREE = new ERXKey<NSTimestamp>("dateEntree");
  public static final ERXKey<NSTimestamp> DATE_SORTIE = new ERXKey<NSTimestamp>("dateSortie");
  public static final ERXKey<String> INTERET_CONTACT = new ERXKey<String>("interetContact");
  public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
  public static final ERXKey<Integer> PRIORITE = new ERXKey<Integer>("priorite");
  public static final ERXKey<String> RESPONSABILITE = new ERXKey<String>("responsabilite");
  public static final ERXKey<String> SERVICE = new ERXKey<String>("service");
  public static final ERXKey<String> TITRE = new ERXKey<String>("titre");
  // Relationship Keys

	// Attributes


	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String DATE_ENTREE_KEY = "dateEntree";
	public static final String DATE_SORTIE_KEY = "dateSortie";
	public static final String INTERET_CONTACT_KEY = "interetContact";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PRIORITE_KEY = "priorite";
	public static final String RESPONSABILITE_KEY = "responsabilite";
	public static final String SERVICE_KEY = "service";
	public static final String TITRE_KEY = "titre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String DATE_ENTREE_COLKEY = "DATE_ENTREE";
	public static final String DATE_SORTIE_COLKEY = "DATE_SORTIE";
	public static final String INTERET_CONTACT_COLKEY = "INTERET_CONTACT";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PRIORITE_COLKEY = "PRIORITE";
	public static final String RESPONSABILITE_COLKEY = "RESPONSABILITE";
	public static final String SERVICE_COLKEY = "SERVICE";
	public static final String TITRE_COLKEY = "TITRE";



	// Relationships



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public NSTimestamp dateEntree() {
    return (NSTimestamp) storedValueForKey(DATE_ENTREE_KEY);
  }

  public void setDateEntree(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ENTREE_KEY);
  }

  public NSTimestamp dateSortie() {
    return (NSTimestamp) storedValueForKey(DATE_SORTIE_KEY);
  }

  public void setDateSortie(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_SORTIE_KEY);
  }

  public String interetContact() {
    return (String) storedValueForKey(INTERET_CONTACT_KEY);
  }

  public void setInteretContact(String value) {
    takeStoredValueForKey(value, INTERET_CONTACT_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public Integer priorite() {
    return (Integer) storedValueForKey(PRIORITE_KEY);
  }

  public void setPriorite(Integer value) {
    takeStoredValueForKey(value, PRIORITE_KEY);
  }

  public String responsabilite() {
    return (String) storedValueForKey(RESPONSABILITE_KEY);
  }

  public void setResponsabilite(String value) {
    takeStoredValueForKey(value, RESPONSABILITE_KEY);
  }

  public String service() {
    return (String) storedValueForKey(SERVICE_KEY);
  }

  public void setService(String value) {
    takeStoredValueForKey(value, SERVICE_KEY);
  }

  public String titre() {
    return (String) storedValueForKey(TITRE_KEY);
  }

  public void setTitre(String value) {
    takeStoredValueForKey(value, TITRE_KEY);
  }


/**
 * Créer une instance de EORepartEmploi avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORepartEmploi createEORepartEmploi(EOEditingContext editingContext, String cStructure
, Integer noIndividu
, Integer priorite
			) {
    EORepartEmploi eo = (EORepartEmploi) createAndInsertInstance(editingContext, _EORepartEmploi.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setNoIndividu(noIndividu);
		eo.setPriorite(priorite);
    return eo;
  }

  
	  public EORepartEmploi localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartEmploi)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartEmploi creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartEmploi creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORepartEmploi object = (EORepartEmploi)createAndInsertInstance(editingContext, _EORepartEmploi.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartEmploi localInstanceIn(EOEditingContext editingContext, EORepartEmploi eo) {
    EORepartEmploi localInstance = (eo == null) ? null : (EORepartEmploi)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORepartEmploi#localInstanceIn a la place.
   */
	public static EORepartEmploi localInstanceOf(EOEditingContext editingContext, EORepartEmploi eo) {
		return EORepartEmploi.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploi>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORepartEmploi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartEmploi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORepartEmploi> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartEmploi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartEmploi)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartEmploi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartEmploi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORepartEmploi> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartEmploi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartEmploi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORepartEmploi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartEmploi eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartEmploi ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartEmploi fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
