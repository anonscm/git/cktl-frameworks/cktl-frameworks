/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndividu.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOIndividu extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOIndividu.class);

	public static final String ENTITY_NAME = "Fwkpers_Individu";
	public static final String ENTITY_TABLE_NAME = "GRHUM.INDIVIDU_ULR";


// Attribute Keys
  public static final ERXKey<Integer> CATEGORIE_PRINC = new ERXKey<Integer>("categoriePrinc");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_DECES = new ERXKey<NSTimestamp>("dDeces");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_NAISSANCE = new ERXKey<NSTimestamp>("dNaissance");
  public static final ERXKey<NSTimestamp> D_NATURALISATION = new ERXKey<NSTimestamp>("dNaturalisation");
  public static final ERXKey<String> IND_ACTIVITE = new ERXKey<String>("indActivite");
  public static final ERXKey<String> IND_AGENDA = new ERXKey<String>("indAgenda");
  public static final ERXKey<Integer> IND_CLE_INSEE = new ERXKey<Integer>("indCleInsee");
  public static final ERXKey<Integer> IND_CLE_INSEE_PROV = new ERXKey<Integer>("indCleInseeProv");
  public static final ERXKey<String> IND_C_SIT_MILITAIRE = new ERXKey<String>("indCSitMilitaire");
  public static final ERXKey<String> IND_C_SITUATION_FAMILLE = new ERXKey<String>("indCSituationFamille");
  public static final ERXKey<String> IND_NO_INSEE = new ERXKey<String>("indNoInsee");
  public static final ERXKey<String> IND_NO_INSEE_PROV = new ERXKey<String>("indNoInseeProv");
  public static final ERXKey<String> IND_ORIGINE = new ERXKey<String>("indOrigine");
  public static final ERXKey<String> IND_PHOTO = new ERXKey<String>("indPhoto");
  public static final ERXKey<String> IND_QUALITE = new ERXKey<String>("indQualite");
  public static final ERXKey<String> LISTE_ROUGE = new ERXKey<String>("listeRouge");
  public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
  public static final ERXKey<Integer> NO_INDIVIDU_CREATEUR = new ERXKey<Integer>("noIndividuCreateur");
  public static final ERXKey<String> NOM_AFFICHAGE = new ERXKey<String>("nomAffichage");
  public static final ERXKey<String> NOM_PATRONYMIQUE = new ERXKey<String>("nomPatronymique");
  public static final ERXKey<String> NOM_PATRONYMIQUE_AFFICHAGE = new ERXKey<String>("nomPatronymiqueAffichage");
  public static final ERXKey<String> NOM_USUEL = new ERXKey<String>("nomUsuel");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
  public static final ERXKey<String> PRENOM2 = new ERXKey<String>("prenom2");
  public static final ERXKey<String> PRENOM_AFFICHAGE = new ERXKey<String>("prenomAffichage");
  public static final ERXKey<String> PRISE_CPT_INSEE = new ERXKey<String>("priseCptInsee");
  public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
  public static final ERXKey<String> VILLE_DE_NAISSANCE = new ERXKey<String>("villeDeNaissance");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite> TO_CIVILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite>("toCivilite");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> TO_COMPTES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>("toComptes");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_DEPARTEMENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toDepartement");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> TO_FOURNISS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>("toFourniss");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS_NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPaysNaissance");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS_NATIONALITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPaysNationalite");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> TO_PERSONNE_ALIASES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>("toPersonneAliases");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> TO_PERSONNELS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>("toPersonnels");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> TO_PERSONNE_TELEPHONES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>("toPersonneTelephones");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> TO_REPART_ASSOCIATIONS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>("toRepartAssociations");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> TO_REPART_PERSONNE_ADRESSES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>("toRepartPersonneAdresses");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> TO_REPART_STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>("toRepartStructures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale> TO_SITUATION_FAMILIALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale>("toSituationFamiliale");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire> TO_SITUATION_MILITAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire>("toSituationMilitaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns> TOS_V_PERSONNEL_ACTUEL_NON_ENSES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns>("tosVPersonnelActuelNonEnses");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns> TOS_V_PERSONNEL_NON_ENS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns>("tosVPersonnelNonEns");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns> TO_V_PERSONNEL_ACTUEL_ENSES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns>("toVPersonnelActuelEnses");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel> TO_V_PERSONNEL_ACTUELS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel>("toVPersonnelActuels");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns> TO_V_PERSONNEL_ENSES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns>("toVPersonnelEnses");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final String CATEGORIE_PRINC_KEY = "categoriePrinc";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String D_NATURALISATION_KEY = "dNaturalisation";
	public static final String IND_ACTIVITE_KEY = "indActivite";
	public static final String IND_AGENDA_KEY = "indAgenda";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_CLE_INSEE_PROV_KEY = "indCleInseeProv";
	public static final String IND_C_SIT_MILITAIRE_KEY = "indCSitMilitaire";
	public static final String IND_C_SITUATION_FAMILLE_KEY = "indCSituationFamille";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_NO_INSEE_PROV_KEY = "indNoInseeProv";
	public static final String IND_ORIGINE_KEY = "indOrigine";
	public static final String IND_PHOTO_KEY = "indPhoto";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String LISTE_ROUGE_KEY = "listeRouge";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NO_INDIVIDU_CREATEUR_KEY = "noIndividuCreateur";
	public static final String NOM_AFFICHAGE_KEY = "nomAffichage";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_KEY = "nomPatronymiqueAffichage";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRENOM2_KEY = "prenom2";
	public static final String PRENOM_AFFICHAGE_KEY = "prenomAffichage";
	public static final String PRISE_CPT_INSEE_KEY = "priseCptInsee";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String VILLE_DE_NAISSANCE_KEY = "villeDeNaissance";

// Attributs non visibles
	public static final String C_PAYS_NAISSANCE_KEY = "cPaysNaissance";
	public static final String C_PAYS_NATIONALITE_KEY = "cPaysNationalite";
	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String C_DEPT_NAISSANCE_KEY = "cDeptNaissance";

//Colonnes dans la base de donnees
	public static final String CATEGORIE_PRINC_COLKEY = "CATEGORIE_PRINC";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DECES_COLKEY = "D_DECES";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String D_NATURALISATION_COLKEY = "D_NATURALISATION";
	public static final String IND_ACTIVITE_COLKEY = "IND_ACTIVITE";
	public static final String IND_AGENDA_COLKEY = "IND_AGENDA";
	public static final String IND_CLE_INSEE_COLKEY = "IND_CLE_INSEE";
	public static final String IND_CLE_INSEE_PROV_COLKEY = "IND_CLE_INSEE_PROV";
	public static final String IND_C_SIT_MILITAIRE_COLKEY = "IND_C_SIT_MILITAIRE";
	public static final String IND_C_SITUATION_FAMILLE_COLKEY = "IND_C_SITUATION_FAMILLE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String IND_NO_INSEE_PROV_COLKEY = "IND_NO_INSEE_PROV";
	public static final String IND_ORIGINE_COLKEY = "IND_ORIGINE";
	public static final String IND_PHOTO_COLKEY = "IND_PHOTO";
	public static final String IND_QUALITE_COLKEY = "IND_QUALITE";
	public static final String LISTE_ROUGE_COLKEY = "LISTE_ROUGE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NO_INDIVIDU_CREATEUR_COLKEY = "NO_INDIVIDU_CREATEUR";
	public static final String NOM_AFFICHAGE_COLKEY = "NOM_AFFICHAGE";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_COLKEY = "NOM_PATRONYMIQUE_AFFICHAGE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String PRENOM2_COLKEY = "PRENOM2";
	public static final String PRENOM_AFFICHAGE_COLKEY = "PRENOM_AFFICHAGE";
	public static final String PRISE_CPT_INSEE_COLKEY = "PRISE_CPT_INSEE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String VILLE_DE_NAISSANCE_COLKEY = "VILLE_DE_NAISSANCE";

	public static final String C_PAYS_NAISSANCE_COLKEY = "C_PAYS_NAISSANCE";
	public static final String C_PAYS_NATIONALITE_COLKEY = "C_PAYS_NATIONALITE";
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String C_DEPT_NAISSANCE_COLKEY = "C_DEPT_NAISSANCE";


	// Relationships
	public static final String TO_CIVILITE_KEY = "toCivilite";
	public static final String TO_COMPTES_KEY = "toComptes";
	public static final String TO_DEPARTEMENT_KEY = "toDepartement";
	public static final String TO_FOURNISS_KEY = "toFourniss";
	public static final String TO_PAYS_NAISSANCE_KEY = "toPaysNaissance";
	public static final String TO_PAYS_NATIONALITE_KEY = "toPaysNationalite";
	public static final String TO_PERSONNE_ALIASES_KEY = "toPersonneAliases";
	public static final String TO_PERSONNELS_KEY = "toPersonnels";
	public static final String TO_PERSONNE_TELEPHONES_KEY = "toPersonneTelephones";
	public static final String TO_REPART_ASSOCIATIONS_KEY = "toRepartAssociations";
	public static final String TO_REPART_PERSONNE_ADRESSES_KEY = "toRepartPersonneAdresses";
	public static final String TO_REPART_STRUCTURES_KEY = "toRepartStructures";
	public static final String TO_SITUATION_FAMILIALE_KEY = "toSituationFamiliale";
	public static final String TO_SITUATION_MILITAIRE_KEY = "toSituationMilitaire";
	public static final String TOS_V_PERSONNEL_ACTUEL_NON_ENSES_KEY = "tosVPersonnelActuelNonEnses";
	public static final String TOS_V_PERSONNEL_NON_ENS_KEY = "tosVPersonnelNonEns";
	public static final String TO_V_PERSONNEL_ACTUEL_ENSES_KEY = "toVPersonnelActuelEnses";
	public static final String TO_V_PERSONNEL_ACTUELS_KEY = "toVPersonnelActuels";
	public static final String TO_V_PERSONNEL_ENSES_KEY = "toVPersonnelEnses";



	// Accessors methods
  public Integer categoriePrinc() {
    return (Integer) storedValueForKey(CATEGORIE_PRINC_KEY);
  }

  public void setCategoriePrinc(Integer value) {
    takeStoredValueForKey(value, CATEGORIE_PRINC_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDeces() {
    return (NSTimestamp) storedValueForKey(D_DECES_KEY);
  }

  public void setDDeces(NSTimestamp value) {
    takeStoredValueForKey(value, D_DECES_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey(D_NAISSANCE_KEY);
  }

  public void setDNaissance(NSTimestamp value) {
    takeStoredValueForKey(value, D_NAISSANCE_KEY);
  }

  public NSTimestamp dNaturalisation() {
    return (NSTimestamp) storedValueForKey(D_NATURALISATION_KEY);
  }

  public void setDNaturalisation(NSTimestamp value) {
    takeStoredValueForKey(value, D_NATURALISATION_KEY);
  }

  public String indActivite() {
    return (String) storedValueForKey(IND_ACTIVITE_KEY);
  }

  public void setIndActivite(String value) {
    takeStoredValueForKey(value, IND_ACTIVITE_KEY);
  }

  public String indAgenda() {
    return (String) storedValueForKey(IND_AGENDA_KEY);
  }

  public void setIndAgenda(String value) {
    takeStoredValueForKey(value, IND_AGENDA_KEY);
  }

  public Integer indCleInsee() {
    return (Integer) storedValueForKey(IND_CLE_INSEE_KEY);
  }

  public void setIndCleInsee(Integer value) {
    takeStoredValueForKey(value, IND_CLE_INSEE_KEY);
  }

  public Integer indCleInseeProv() {
    return (Integer) storedValueForKey(IND_CLE_INSEE_PROV_KEY);
  }

  public void setIndCleInseeProv(Integer value) {
    takeStoredValueForKey(value, IND_CLE_INSEE_PROV_KEY);
  }

  public String indCSitMilitaire() {
    return (String) storedValueForKey(IND_C_SIT_MILITAIRE_KEY);
  }

  public void setIndCSitMilitaire(String value) {
    takeStoredValueForKey(value, IND_C_SIT_MILITAIRE_KEY);
  }

  public String indCSituationFamille() {
    return (String) storedValueForKey(IND_C_SITUATION_FAMILLE_KEY);
  }

  public void setIndCSituationFamille(String value) {
    takeStoredValueForKey(value, IND_C_SITUATION_FAMILLE_KEY);
  }

  public String indNoInsee() {
    return (String) storedValueForKey(IND_NO_INSEE_KEY);
  }

  public void setIndNoInsee(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_KEY);
  }

  public String indNoInseeProv() {
    return (String) storedValueForKey(IND_NO_INSEE_PROV_KEY);
  }

  public void setIndNoInseeProv(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_PROV_KEY);
  }

  public String indOrigine() {
    return (String) storedValueForKey(IND_ORIGINE_KEY);
  }

  public void setIndOrigine(String value) {
    takeStoredValueForKey(value, IND_ORIGINE_KEY);
  }

  public String indPhoto() {
    return (String) storedValueForKey(IND_PHOTO_KEY);
  }

  public void setIndPhoto(String value) {
    takeStoredValueForKey(value, IND_PHOTO_KEY);
  }

  public String indQualite() {
    return (String) storedValueForKey(IND_QUALITE_KEY);
  }

  public void setIndQualite(String value) {
    takeStoredValueForKey(value, IND_QUALITE_KEY);
  }

  public String listeRouge() {
    return (String) storedValueForKey(LISTE_ROUGE_KEY);
  }

  public void setListeRouge(String value) {
    takeStoredValueForKey(value, LISTE_ROUGE_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public Integer noIndividuCreateur() {
    return (Integer) storedValueForKey(NO_INDIVIDU_CREATEUR_KEY);
  }

  public void setNoIndividuCreateur(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_CREATEUR_KEY);
  }

  public String nomAffichage() {
    return (String) storedValueForKey(NOM_AFFICHAGE_KEY);
  }

  public void setNomAffichage(String value) {
    takeStoredValueForKey(value, NOM_AFFICHAGE_KEY);
  }

  public String nomPatronymique() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
  }

  public void setNomPatronymique(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
  }

  public String nomPatronymiqueAffichage() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_AFFICHAGE_KEY);
  }

  public void setNomPatronymiqueAffichage(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_AFFICHAGE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String prenom2() {
    return (String) storedValueForKey(PRENOM2_KEY);
  }

  public void setPrenom2(String value) {
    takeStoredValueForKey(value, PRENOM2_KEY);
  }

  public String prenomAffichage() {
    return (String) storedValueForKey(PRENOM_AFFICHAGE_KEY);
  }

  public void setPrenomAffichage(String value) {
    takeStoredValueForKey(value, PRENOM_AFFICHAGE_KEY);
  }

  public String priseCptInsee() {
    return (String) storedValueForKey(PRISE_CPT_INSEE_KEY);
  }

  public void setPriseCptInsee(String value) {
    takeStoredValueForKey(value, PRISE_CPT_INSEE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public String villeDeNaissance() {
    return (String) storedValueForKey(VILLE_DE_NAISSANCE_KEY);
  }

  public void setVilleDeNaissance(String value) {
    takeStoredValueForKey(value, VILLE_DE_NAISSANCE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toCivilite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCivilite)storedValueForKey(TO_CIVILITE_KEY);
  }

  public void setToCiviliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCivilite value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCivilite oldValue = toCivilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CIVILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CIVILITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toDepartement() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey(TO_DEPARTEMENT_KEY);
  }

  public void setToDepartementRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toDepartement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPARTEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPARTEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysNaissance() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_NAISSANCE_KEY);
  }

  public void setToPaysNaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPaysNaissance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_NAISSANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_NAISSANCE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysNationalite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_NATIONALITE_KEY);
  }

  public void setToPaysNationaliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPaysNationalite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_NATIONALITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_NATIONALITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale toSituationFamiliale() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale)storedValueForKey(TO_SITUATION_FAMILIALE_KEY);
  }

  public void setToSituationFamilialeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale oldValue = toSituationFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_FAMILIALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_FAMILIALE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire toSituationMilitaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire)storedValueForKey(TO_SITUATION_MILITAIRE_KEY);
  }

  public void setToSituationMilitaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire oldValue = toSituationMilitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_MILITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_MILITAIRE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>)storedValueForKey(TO_COMPTES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes(EOQualifier qualifier) {
    return toComptes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes(EOQualifier qualifier, boolean fetch) {
    return toComptes(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> toComptes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOCompte.TO_INDIVIDUS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOCompte.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toComptes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToComptesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
  }

  public void removeFromToComptesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompte createToComptesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Compte");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_COMPTES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompte) eo;
  }

  public void deleteToComptesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToComptesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> objects = toComptes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToComptesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>)storedValueForKey(TO_FOURNISS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss(EOQualifier qualifier) {
    return toFourniss(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss(EOQualifier qualifier, boolean fetch) {
    return toFourniss(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> toFourniss(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOFournis.TO_INDIVIDUS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOFournis.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toFourniss();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToFournissRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_FOURNISS_KEY);
  }

  public void removeFromToFournissRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FOURNISS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOFournis createToFournissRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Fournis");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_FOURNISS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis) eo;
  }

  public void deleteToFournissRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FOURNISS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToFournissRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> objects = toFourniss().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToFournissRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>)storedValueForKey(TO_PERSONNE_ALIASES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases(EOQualifier qualifier) {
    return toPersonneAliases(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases(EOQualifier qualifier, boolean fetch) {
    return toPersonneAliases(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> toPersonneAliases(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias.TO_INDIVIDUS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersonneAliases();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersonneAliasesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERSONNE_ALIASES_KEY);
  }

  public void removeFromToPersonneAliasesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_ALIASES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias createToPersonneAliasesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_PersonneAlias");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERSONNE_ALIASES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias) eo;
  }

  public void deleteToPersonneAliasesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_ALIASES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersonneAliasesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias> objects = toPersonneAliases().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersonneAliasesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> toPersonnels() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>)storedValueForKey(TO_PERSONNELS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> toPersonnels(EOQualifier qualifier) {
    return toPersonnels(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> toPersonnels(EOQualifier qualifier, boolean fetch) {
    return toPersonnels(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> toPersonnels(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersonnels();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersonnelsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERSONNELS_KEY);
  }

  public void removeFromToPersonnelsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNELS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel createToPersonnelsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Personnel");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERSONNELS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel) eo;
  }

  public void deleteToPersonnelsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNELS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersonnelsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> objects = toPersonnels().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersonnelsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>)storedValueForKey(TO_PERSONNE_TELEPHONES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones(EOQualifier qualifier) {
    return toPersonneTelephones(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones(EOQualifier qualifier, boolean fetch) {
    return toPersonneTelephones(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> toPersonneTelephones(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone.TO_INDIVIDUS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersonneTelephones();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersonneTelephonesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERSONNE_TELEPHONES_KEY);
  }

  public void removeFromToPersonneTelephonesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_TELEPHONES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone createToPersonneTelephonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_PersonneTelephone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERSONNE_TELEPHONES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone) eo;
  }

  public void deleteToPersonneTelephonesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERSONNE_TELEPHONES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersonneTelephonesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone> objects = toPersonneTelephones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersonneTelephonesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)storedValueForKey(TO_REPART_ASSOCIATIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations(EOQualifier qualifier) {
    return toRepartAssociations(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations(EOQualifier qualifier, boolean fetch) {
    return toRepartAssociations(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> toRepartAssociations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation.TO_INDIVIDUS_ASSOCIES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartAssociations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartAssociationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_KEY);
  }

  public void removeFromToRepartAssociationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation createToRepartAssociationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartAssociation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_ASSOCIATIONS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation) eo;
  }

  public void deleteToRepartAssociationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_ASSOCIATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartAssociationsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> objects = toRepartAssociations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartAssociationsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)storedValueForKey(TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier) {
    return toRepartPersonneAdresses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier, boolean fetch) {
    return toRepartPersonneAdresses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse.TO_INDIVIDUS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse createToRepartPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse) eo;
  }

  public void deleteToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartPersonneAdressesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> objects = toRepartPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartPersonneAdressesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)storedValueForKey(TO_REPART_STRUCTURES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures(EOQualifier qualifier) {
    return toRepartStructures(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures(EOQualifier qualifier, boolean fetch) {
    return toRepartStructures(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> toRepartStructures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure.TO_INDIVIDU_ELTS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartStructures();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_KEY);
  }

  public void removeFromToRepartStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure createToRepartStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_STRUCTURES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure) eo;
  }

  public void deleteToRepartStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartStructuresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> objects = toRepartStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartStructuresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns> tosVPersonnelActuelNonEnses() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns>)storedValueForKey(TOS_V_PERSONNEL_ACTUEL_NON_ENSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns> tosVPersonnelActuelNonEnses(EOQualifier qualifier) {
    return tosVPersonnelActuelNonEnses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns> tosVPersonnelActuelNonEnses(EOQualifier qualifier, boolean fetch) {
    return tosVPersonnelActuelNonEnses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns> tosVPersonnelActuelNonEnses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosVPersonnelActuelNonEnses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosVPersonnelActuelNonEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNEL_ACTUEL_NON_ENSES_KEY);
  }

  public void removeFromTosVPersonnelActuelNonEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNEL_ACTUEL_NON_ENSES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns createTosVPersonnelActuelNonEnsesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_VPersonnelActuelNonEns");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_V_PERSONNEL_ACTUEL_NON_ENSES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns) eo;
  }

  public void deleteTosVPersonnelActuelNonEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNEL_ACTUEL_NON_ENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosVPersonnelActuelNonEnsesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns> objects = tosVPersonnelActuelNonEnses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosVPersonnelActuelNonEnsesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns> tosVPersonnelNonEns() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns>)storedValueForKey(TOS_V_PERSONNEL_NON_ENS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns> tosVPersonnelNonEns(EOQualifier qualifier) {
    return tosVPersonnelNonEns(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns> tosVPersonnelNonEns(EOQualifier qualifier, boolean fetch) {
    return tosVPersonnelNonEns(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns> tosVPersonnelNonEns(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosVPersonnelNonEns();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosVPersonnelNonEnsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNEL_NON_ENS_KEY);
  }

  public void removeFromTosVPersonnelNonEnsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNEL_NON_ENS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns createTosVPersonnelNonEnsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_VPersonnelNonEns");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_V_PERSONNEL_NON_ENS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns) eo;
  }

  public void deleteTosVPersonnelNonEnsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNEL_NON_ENS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosVPersonnelNonEnsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns> objects = tosVPersonnelNonEns().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosVPersonnelNonEnsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns> toVPersonnelActuelEnses() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns>)storedValueForKey(TO_V_PERSONNEL_ACTUEL_ENSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns> toVPersonnelActuelEnses(EOQualifier qualifier) {
    return toVPersonnelActuelEnses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns> toVPersonnelActuelEnses(EOQualifier qualifier, boolean fetch) {
    return toVPersonnelActuelEnses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns> toVPersonnelActuelEnses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVPersonnelActuelEnses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVPersonnelActuelEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ACTUEL_ENSES_KEY);
  }

  public void removeFromToVPersonnelActuelEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ACTUEL_ENSES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns createToVPersonnelActuelEnsesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_VPersonnelActuelEns");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_V_PERSONNEL_ACTUEL_ENSES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns) eo;
  }

  public void deleteToVPersonnelActuelEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ACTUEL_ENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToVPersonnelActuelEnsesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns> objects = toVPersonnelActuelEnses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVPersonnelActuelEnsesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel> toVPersonnelActuels() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel>)storedValueForKey(TO_V_PERSONNEL_ACTUELS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel> toVPersonnelActuels(EOQualifier qualifier) {
    return toVPersonnelActuels(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel> toVPersonnelActuels(EOQualifier qualifier, boolean fetch) {
    return toVPersonnelActuels(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel> toVPersonnelActuels(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVPersonnelActuels();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVPersonnelActuelsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ACTUELS_KEY);
  }

  public void removeFromToVPersonnelActuelsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ACTUELS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel createToVPersonnelActuelsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_VPersonnelActuel");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_V_PERSONNEL_ACTUELS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel) eo;
  }

  public void deleteToVPersonnelActuelsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ACTUELS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToVPersonnelActuelsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuel> objects = toVPersonnelActuels().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVPersonnelActuelsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns> toVPersonnelEnses() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns>)storedValueForKey(TO_V_PERSONNEL_ENSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns> toVPersonnelEnses(EOQualifier qualifier) {
    return toVPersonnelEnses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns> toVPersonnelEnses(EOQualifier qualifier, boolean fetch) {
    return toVPersonnelEnses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns> toVPersonnelEnses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVPersonnelEnses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVPersonnelEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ENSES_KEY);
  }

  public void removeFromToVPersonnelEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ENSES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns createToVPersonnelEnsesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_VPersonnelEns");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_V_PERSONNEL_ENSES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns) eo;
  }

  public void deleteToVPersonnelEnsesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_V_PERSONNEL_ENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToVPersonnelEnsesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelEns> objects = toVPersonnelEnses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVPersonnelEnsesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOIndividu avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOIndividu createEOIndividu(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String listeRouge
, Integer noIndividu
, String nomUsuel
, Integer persId
, String prenom
, org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toCivilite			) {
    EOIndividu eo = (EOIndividu) createAndInsertInstance(editingContext, _EOIndividu.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setListeRouge(listeRouge);
		eo.setNoIndividu(noIndividu);
		eo.setNomUsuel(nomUsuel);
		eo.setPersId(persId);
		eo.setPrenom(prenom);
    eo.setToCiviliteRelationship(toCivilite);
    return eo;
  }

  
	  public EOIndividu localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndividu)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividu creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividu creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOIndividu object = (EOIndividu)createAndInsertInstance(editingContext, _EOIndividu.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOIndividu localInstanceIn(EOEditingContext editingContext, EOIndividu eo) {
    EOIndividu localInstance = (eo == null) ? null : (EOIndividu)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOIndividu#localInstanceIn a la place.
   */
	public static EOIndividu localInstanceOf(EOEditingContext editingContext, EOIndividu eo) {
		return EOIndividu.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOIndividu fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndividu fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOIndividu> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndividu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndividu)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOIndividu> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividu)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOIndividu fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndividu eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndividu ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndividu fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
