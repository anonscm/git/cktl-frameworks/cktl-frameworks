/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.workflow;

/**
 * Abstraction d'un workflow de validation simple.
 * Peut-être implémenté par les hébergés, les fournisseurs...
 * 
 * Un workflow manipule des items. Il est capable de changer l'état d'un item : valider, annuler, passer en cours
 * de validation.
 * 
 * Le partie pris est de déléguer au workflow la responsabilité de savoir quel est
 * l'état de l'item, plutôt que de définir l'état de l'item au niveau de l'item...
 * En effet il peut arriver que l'état de l'item dépende de plusieurs facteurs 
 * (appartenance à un groupe...)
 * 
 * 
 * @see IWorkflow
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public interface IWorkflow {

    /**
     * @param item l'item à valider
     * @param persIdModificateur le persId de la personne validant
     */
    void valider(IWorkflowItem item, Integer persIdModificateur);
    
    /**
     * @param item l'item à annuler
     * @param persIdModificateur le persId de la personne annulant
     */
    void annuler(IWorkflowItem item, Integer persIdModificateur);
    
    /**
     * @param item l'item à passer en cours de validation
     * @param persIdModificateur le persId de la personne 
     */
    void passerEnCoursDeValidation(IWorkflowItem item, Integer persIdModificateur);
    
    /**
     * @param item l'item dont on veut connaître l'état
     * @return true si l'item est validé
     */
    boolean estValide(IWorkflowItem item);
    
    /**
     * @param item l'item dont on veut connaître l'état
     * @return true si l'item est annulé
     */
    boolean estAnnule(IWorkflowItem item);
    
    /**
     * @param item l'item dont on veut connaître l'état
     * @return true si l'item est en cours de validation
     */
    boolean estEnCoursDeValidation(IWorkflowItem item);
    
}
