/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.crypto.samba.SmbLMCrypt;
import org.cocktail.crypto.samba.SmbNTCrypt;
import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe métier pour les comptes samba. 1 {@link EOCompteSamba} est associé à un {@link EOCompte} et conserve le mot de passe crypté dans 2 formats
 * différents : NTUNICODEHash (SMB-compatible MD4 hashing) et LANMANHash (LANMAN-compatible DES hashing).
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 */
public class EOCompteSamba extends _EOCompteSamba {
	private static final long serialVersionUID = -5977675168067533808L;
	public final static Logger logger = Logger.getLogger(EOCompteSamba.class);

	public static final String APP_FLAG_PROPERTY_KEY = "APP_USE_SAMBA";

	public static final String LM_CRYPT_JAVAMETHODE = SmbLMCrypt.class.getName() + ".crypt";
	public static final String NT_CRYPT_JAVAMETHODE = SmbNTCrypt.class.getName() + ".crypt";

	public EOCompteSamba() {
		super();
	}
	
	/**
	 * Si Samba est activé(cf. EOCompteSamba.APP_FLAG_PROPERTY_KEY), effectue le changement de mot de passe dans le compte samba associé au compte
	 * passé en paramètre, en suivant les 2 méthodes de cryptage propres à samba. Si le compte n'as pas de compte samba associé, un nouveau compte
	 * samba est créé et lui est associé.
	 * 
	 * @param compte
	 * @param passwordClair
	 * @throws Exception (essentiellement due aux erreurs eventuelles de cryptage du mot de passe)
	 */
	public static void changePassword(EOCompte compte, String passwordClair) throws Exception {
		if (compte == null) {
			throw new Exception("Vous essayez de propager un password d'un compte inexistant.");
		}
		// Compte Samba
		creerSiNecessaireCompteSmbPourCompte(compte); // TODO : eventuellement ajouter un throw si le eocomptesamba est null
		// Cryptage Samba
		compte.toCompteSamba().setDModification(new NSTimestamp());
		compte.toCompteSamba().setSmbCryptePassword(passwordClair);
	}

	private static EOCompteSamba creerSiNecessaireCompteSmbPourCompte(EOCompte compte) {
		EOCompteSamba samba = null;
		if (compte != null) {
			if (compte.toCompteSamba() == null) {
				samba = EOCompteSamba.creerInstance(compte.editingContext());
				samba.setCptOrdre(compte._cptOrdreConsult());
				samba.setDCreation(new NSTimestamp());
				samba.setSmbAcctFlag("[UX         ]");
				compte.addToToCompteSambasRelationship(samba);
			}
			else {
				samba = compte.toCompteSamba();
			}
		}
		return samba;
	}

	/**
	 * Crypte le paramètre "passwordClair" et positionne les attributs smbLmPasswd et smbNTPasswd avec le résultat du cryptage samba.
	 * 
	 * @param passwordClair
	 * @see SmbLMCrypt
	 * @see SmbNTCrypt
	 */
	public void setSmbCryptePassword(String passwordClair) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Cryptage samba du mot de passe");
			logger.debug("Cryptage LM du mot de passe Samba avec la methode " + LM_CRYPT_JAVAMETHODE);
			logger.debug("Cryptage NT du mot de passe Samba avec la methode " + NT_CRYPT_JAVAMETHODE);
		}

		try {
			setSmbLmPasswd(CryptoCtrl.cryptPass(LM_CRYPT_JAVAMETHODE, passwordClair));
			setSmbNtPasswd(CryptoCtrl.cryptPass(NT_CRYPT_JAVAMETHODE, passwordClair));
			
		} catch (Exception e) {
			logger.error("erreur lors de l'encryptage", e);
			throw new Exception("Erreur lors de l'encryptage sur Samba", e);
		}
	}

}
