/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMotifPosition.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOMotifPosition extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOMotifPosition.class);

	public static final String ENTITY_NAME = "Fwkpers_MotifPosition";
	public static final String ENTITY_TABLE_NAME = "GRHUM.MOTIF_POSITION";


// Attribute Keys
  public static final ERXKey<String> C_MOTIF_POSITION = new ERXKey<String>("cMotifPosition");
  public static final ERXKey<String> C_POSITION = new ERXKey<String>("cPosition");
  public static final ERXKey<String> LC_MOTIF_POSITION = new ERXKey<String>("lcMotifPosition");
  public static final ERXKey<String> LL_MOTIF_POSITION = new ERXKey<String>("llMotifPosition");
  public static final ERXKey<String> TEM_ENFANT = new ERXKey<String>("temEnfant");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cMotifPosition";

	public static final String C_MOTIF_POSITION_KEY = "cMotifPosition";
	public static final String C_POSITION_KEY = "cPosition";
	public static final String LC_MOTIF_POSITION_KEY = "lcMotifPosition";
	public static final String LL_MOTIF_POSITION_KEY = "llMotifPosition";
	public static final String TEM_ENFANT_KEY = "temEnfant";

// Attributs non visibles
	public static final String REF_REGLEMENTAIRE_KEY = "refReglementaire";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String RANG_KEY = "rang";

//Colonnes dans la base de donnees
	public static final String C_MOTIF_POSITION_COLKEY = "C_MOTIF_POSITION";
	public static final String C_POSITION_COLKEY = "C_POSITION";
	public static final String LC_MOTIF_POSITION_COLKEY = "LC_MOTIF_POSITION";
	public static final String LL_MOTIF_POSITION_COLKEY = "LL_MOTIF_POSITION";
	public static final String TEM_ENFANT_COLKEY = "TEM_ENFANT";

	public static final String REF_REGLEMENTAIRE_COLKEY = "REF_REGLEMENTAIRE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String RANG_COLKEY = "RANG";


	// Relationships



	// Accessors methods
  public String cMotifPosition() {
    return (String) storedValueForKey(C_MOTIF_POSITION_KEY);
  }

  public void setCMotifPosition(String value) {
    takeStoredValueForKey(value, C_MOTIF_POSITION_KEY);
  }

  public String cPosition() {
    return (String) storedValueForKey(C_POSITION_KEY);
  }

  public void setCPosition(String value) {
    takeStoredValueForKey(value, C_POSITION_KEY);
  }

  public String lcMotifPosition() {
    return (String) storedValueForKey(LC_MOTIF_POSITION_KEY);
  }

  public void setLcMotifPosition(String value) {
    takeStoredValueForKey(value, LC_MOTIF_POSITION_KEY);
  }

  public String llMotifPosition() {
    return (String) storedValueForKey(LL_MOTIF_POSITION_KEY);
  }

  public void setLlMotifPosition(String value) {
    takeStoredValueForKey(value, LL_MOTIF_POSITION_KEY);
  }

  public String temEnfant() {
    return (String) storedValueForKey(TEM_ENFANT_KEY);
  }

  public void setTemEnfant(String value) {
    takeStoredValueForKey(value, TEM_ENFANT_KEY);
  }


/**
 * Créer une instance de EOMotifPosition avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMotifPosition createEOMotifPosition(EOEditingContext editingContext, String cMotifPosition
, String cPosition
			) {
    EOMotifPosition eo = (EOMotifPosition) createAndInsertInstance(editingContext, _EOMotifPosition.ENTITY_NAME);    
		eo.setCMotifPosition(cMotifPosition);
		eo.setCPosition(cPosition);
    return eo;
  }

  
	  public EOMotifPosition localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMotifPosition)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMotifPosition creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMotifPosition creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOMotifPosition object = (EOMotifPosition)createAndInsertInstance(editingContext, _EOMotifPosition.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMotifPosition localInstanceIn(EOEditingContext editingContext, EOMotifPosition eo) {
    EOMotifPosition localInstance = (eo == null) ? null : (EOMotifPosition)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMotifPosition#localInstanceIn a la place.
   */
	public static EOMotifPosition localInstanceOf(EOEditingContext editingContext, EOMotifPosition eo) {
		return EOMotifPosition.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMotifPosition fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMotifPosition fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOMotifPosition> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMotifPosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMotifPosition)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMotifPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMotifPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOMotifPosition> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMotifPosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMotifPosition)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMotifPosition fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMotifPosition eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMotifPosition ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMotifPosition fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
