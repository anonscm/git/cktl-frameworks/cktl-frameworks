/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCnu.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOCnu extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOCnu.class);

	public static final String ENTITY_NAME = "Fwkpers_Cnu";
	public static final String ENTITY_TABLE_NAME = "GRHUM.CNU";


// Attribute Keys
  public static final ERXKey<String> C_SECTION_CNU = new ERXKey<String>("cSectionCnu");
  public static final ERXKey<String> C_SOUS_SECTION_CNU = new ERXKey<String>("cSousSectionCnu");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> GROUPE_CNU = new ERXKey<String>("groupeCnu");
  public static final ERXKey<String> LC_SECTION_CNU = new ERXKey<String>("lcSectionCnu");
  public static final ERXKey<String> LL_SECTION_CNU = new ERXKey<String>("llSectionCnu");
  public static final ERXKey<String> TEM_COMPOSEE = new ERXKey<String>("temComposee");
  public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noCnu";

	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String GROUPE_CNU_KEY = "groupeCnu";
	public static final String LC_SECTION_CNU_KEY = "lcSectionCnu";
	public static final String LL_SECTION_CNU_KEY = "llSectionCnu";
	public static final String TEM_COMPOSEE_KEY = "temComposee";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String NO_CNU_KEY = "noCnu";

//Colonnes dans la base de donnees
	public static final String C_SECTION_CNU_COLKEY = "C_SECTION_CNU";
	public static final String C_SOUS_SECTION_CNU_COLKEY = "C_SOUS_SECTION_CNU";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String GROUPE_CNU_COLKEY = "GROUPE_CNU";
	public static final String LC_SECTION_CNU_COLKEY = "LC_SECTION_CNU";
	public static final String LL_SECTION_CNU_COLKEY = "LL_SECTION_CNU";
	public static final String TEM_COMPOSEE_COLKEY = "TEM_COMPOSEE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String NO_CNU_COLKEY = "NO_CNU";


	// Relationships



	// Accessors methods
  public String cSectionCnu() {
    return (String) storedValueForKey(C_SECTION_CNU_KEY);
  }

  public void setCSectionCnu(String value) {
    takeStoredValueForKey(value, C_SECTION_CNU_KEY);
  }

  public String cSousSectionCnu() {
    return (String) storedValueForKey(C_SOUS_SECTION_CNU_KEY);
  }

  public void setCSousSectionCnu(String value) {
    takeStoredValueForKey(value, C_SOUS_SECTION_CNU_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String groupeCnu() {
    return (String) storedValueForKey(GROUPE_CNU_KEY);
  }

  public void setGroupeCnu(String value) {
    takeStoredValueForKey(value, GROUPE_CNU_KEY);
  }

  public String lcSectionCnu() {
    return (String) storedValueForKey(LC_SECTION_CNU_KEY);
  }

  public void setLcSectionCnu(String value) {
    takeStoredValueForKey(value, LC_SECTION_CNU_KEY);
  }

  public String llSectionCnu() {
    return (String) storedValueForKey(LL_SECTION_CNU_KEY);
  }

  public void setLlSectionCnu(String value) {
    takeStoredValueForKey(value, LL_SECTION_CNU_KEY);
  }

  public String temComposee() {
    return (String) storedValueForKey(TEM_COMPOSEE_KEY);
  }

  public void setTemComposee(String value) {
    takeStoredValueForKey(value, TEM_COMPOSEE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }


/**
 * Créer une instance de EOCnu avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCnu createEOCnu(EOEditingContext editingContext, String cSectionCnu
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
			) {
    EOCnu eo = (EOCnu) createAndInsertInstance(editingContext, _EOCnu.ENTITY_NAME);    
		eo.setCSectionCnu(cSectionCnu);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOCnu localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCnu)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCnu creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCnu creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCnu object = (EOCnu)createAndInsertInstance(editingContext, _EOCnu.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCnu localInstanceIn(EOEditingContext editingContext, EOCnu eo) {
    EOCnu localInstance = (eo == null) ? null : (EOCnu)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCnu#localInstanceIn a la place.
   */
	public static EOCnu localInstanceOf(EOEditingContext editingContext, EOCnu eo) {
		return EOCnu.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCnu fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCnu fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCnu> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCnu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCnu)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCnu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCnu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCnu> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCnu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCnu)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCnu fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCnu eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCnu ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCnu fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
