/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EORne.java
// 

package org.cocktail.fwkcktlpersonne.common.metier;


import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXKey;

public class EORne extends _EORne implements IRne{
	
    private static final long serialVersionUID = -1L;
	public static final EOSortOrdering SORT_C_RNE_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EORne.C_RNE_KEY, EOSortOrdering.CompareAscending);	
	public static final EOSortOrdering SORT_LL_RNE_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EORne.LL_RNE_KEY, EOSortOrdering.CompareAscending);
	
	public static final String CODE_DEPARTEMENT_KEY = "codeDepartement";
	public static final ERXKey<String> CODE_DEPARTEMENT = new ERXKey<String>(CODE_DEPARTEMENT_KEY);
	
	
    public EORne() {
        super();
    }
    
    public static NSArray<EORne> getFilteredEtablissements(EOEditingContext editingContext, String filtEtablissement) {

		NSArray<EORne> res = new NSArray<EORne>();
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		
		if (filtEtablissement != null) {
			filtEtablissement = filtEtablissement.trim();
		}

		if (!MyStringCtrl.isEmpty(filtEtablissement)) {
			filtEtablissement = filtEtablissement.replaceAll(" ", "*");
			quals.addObject(new EOKeyValueQualifier(EORne.C_RNE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "" + filtEtablissement + "*"));
			quals.addObject(new EOKeyValueQualifier(EORne.LL_RNE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtEtablissement + "*"));
			res = EORne.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
					EORne.SORT_C_RNE_KEY_ASC, EORne.SORT_LL_RNE_KEY_ASC
			}));
			
			if (res.count() == 0) {
				quals.removeAllObjects();
				quals.addObject(new EOKeyValueQualifier(EORne.C_RNE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtEtablissement + "*"));
				quals.addObject(new EOKeyValueQualifier(EORne.LL_RNE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtEtablissement + "*"));
				res = EORne.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
						EORne.SORT_C_RNE_KEY_ASC, EORne.SORT_LL_RNE_KEY_ASC
				}));
				
			}
		}
			
		return res;
	}
    
    /**
     * Retourne le code du departement de l'etablissement
     * @return String : le code du departement
     */    
    public String getCodeDepartement() {
    	return cRne().substring(0, 3);
    }
    
    /**
     * Retourne l'objet departement de l'etablissement
     * @return EODepartement : le departement
     */
    public EODepartement getDepartement() {
    	EODepartement departement = null;
    	
    	if (getCodeDepartement() != null) {
    		departement = EODepartement.fetchFirstByQualifier(editingContext(), EODepartement.C_DEPARTEMENT.eq(getCodeDepartement()));
    	}
    	
    	return departement;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isUneUniversite() {
    	return "UNI".equals(tetabCode());
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isUnEtablissementFrancaisALEtranger() {
    	return "EFE".equals(tetabCode());
    }
}
