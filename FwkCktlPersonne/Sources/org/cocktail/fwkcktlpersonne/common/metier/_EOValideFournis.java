/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOValideFournis.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;


public abstract class _EOValideFournis extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOValideFournis.class);

	public static final String ENTITY_NAME = "Fwkpers_ValideFournis";
	public static final String ENTITY_TABLE_NAME = "GRHUM.VALIDE_FOURNIS_ULR";


// Attribute Keys
  public static final ERXKey<Integer> VAL_CREATION = new ERXKey<Integer>("valCreation");
  public static final ERXKey<NSTimestamp> VAL_DATE_CREATE = new ERXKey<NSTimestamp>("valDateCreate");
  public static final ERXKey<NSTimestamp> VAL_DATE_VAL = new ERXKey<NSTimestamp>("valDateVal");
  public static final ERXKey<Integer> VAL_VALIDATION = new ERXKey<Integer>("valValidation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> TO_FOURNIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>("toFournis");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fouOrdre";

	public static final String VAL_CREATION_KEY = "valCreation";
	public static final String VAL_DATE_CREATE_KEY = "valDateCreate";
	public static final String VAL_DATE_VAL_KEY = "valDateVal";
	public static final String VAL_VALIDATION_KEY = "valValidation";

// Attributs non visibles
	public static final String FOU_ORDRE_KEY = "fouOrdre";

//Colonnes dans la base de donnees
	public static final String VAL_CREATION_COLKEY = "VAL_CREATION";
	public static final String VAL_DATE_CREATE_COLKEY = "VAL_DATE_CREATE";
	public static final String VAL_DATE_VAL_COLKEY = "VAL_DATE_VAL";
	public static final String VAL_VALIDATION_COLKEY = "VAL_VALIDATION";

	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";


	// Relationships
	public static final String TO_FOURNIS_KEY = "toFournis";



	// Accessors methods
  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#valCreation()
 */
public Integer valCreation() {
    return (Integer) storedValueForKey(VAL_CREATION_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#setValCreation(java.lang.Integer)
 */
public void setValCreation(Integer value) {
    takeStoredValueForKey(value, VAL_CREATION_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#valDateCreate()
 */
public NSTimestamp valDateCreate() {
    return (NSTimestamp) storedValueForKey(VAL_DATE_CREATE_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#setValDateCreate(com.webobjects.foundation.NSTimestamp)
 */
public void setValDateCreate(NSTimestamp value) {
    takeStoredValueForKey(value, VAL_DATE_CREATE_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#valDateVal()
 */
public NSTimestamp valDateVal() {
    return (NSTimestamp) storedValueForKey(VAL_DATE_VAL_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#setValDateVal(com.webobjects.foundation.NSTimestamp)
 */
public void setValDateVal(NSTimestamp value) {
    takeStoredValueForKey(value, VAL_DATE_VAL_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#valValidation()
 */
public Integer valValidation() {
    return (Integer) storedValueForKey(VAL_VALIDATION_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#setValValidation(java.lang.Integer)
 */
public void setValValidation(Integer value) {
    takeStoredValueForKey(value, VAL_VALIDATION_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#toFournis()
 */
public org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis)storedValueForKey(TO_FOURNIS_KEY);
  }

  /* (non-Javadoc)
 * @see org.cocktail.fwkcktlpersonne.common.metier.IValideFournis#setToFournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis)
 */
public void setToFournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFournis oldValue = toFournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_KEY);
    }
  }
  

/**
 * Créer une instance de EOValideFournis avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOValideFournis createEOValideFournis(EOEditingContext editingContext, org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis			) {
    EOValideFournis eo = (EOValideFournis) createAndInsertInstance(editingContext, _EOValideFournis.ENTITY_NAME);    
    eo.setToFournisRelationship(toFournis);
    return eo;
  }

  
	  public EOValideFournis localInstanceIn(EOEditingContext editingContext) {
	  		return (EOValideFournis)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOValideFournis creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOValideFournis creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOValideFournis object = (EOValideFournis)createAndInsertInstance(editingContext, _EOValideFournis.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOValideFournis localInstanceIn(EOEditingContext editingContext, EOValideFournis eo) {
    EOValideFournis localInstance = (eo == null) ? null : (EOValideFournis)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOValideFournis#localInstanceIn a la place.
   */
	public static EOValideFournis localInstanceOf(EOEditingContext editingContext, EOValideFournis eo) {
		return EOValideFournis.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOValideFournis fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOValideFournis fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOValideFournis> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOValideFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOValideFournis)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOValideFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOValideFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOValideFournis> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOValideFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOValideFournis)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOValideFournis fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOValideFournis eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOValideFournis ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOValideFournis fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
