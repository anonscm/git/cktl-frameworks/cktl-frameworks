/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTypeGroupe extends _EOTypeGroupe {
	
	
	public static final String OUI = "O";
	public static final String NON = "N";
	
	
	/** Type de groupes "Groupe" (G) */
	public static final String TGRP_CODE_G = "G";

	/** Type de groupes "Groupe Institutionnel" (GI) */
	public static final String TGRP_CODE_GI = "GI";

	/** Type de groupes "Service" (S) */
	public static final String TGRP_CODE_S = "S";

	/** Type de groupes "Annuaire" (A) */
	public static final String TGRP_CODE_A = "A";

	/** Type de groupes "Composante scolarité" (CS) */
	public static final String TGRP_CODE_CS = "CS";

	/** Type de groupes "Fournisseur" (FO) */
	public static final String TGRP_CODE_FO = "FO";

	/** Type de groupes "Entreprise" (EN) */
	public static final String TGRP_CODE_EN = "EN";

	/** Type de groupes "Referentiel" (RE) */
	public static final String TGRP_CODE_RE = "RE";

	/** Type de groupes "Laboratoires" (LA) */
	public static final String TGRP_CODE_LA = "LA";
	
	/** Type de groupes "Ecole Doctorale" (ED) */
	public static final String TGRP_CODE_ED = "ED";

	/** Type de groupes "Federation de recherche" (FR) */
	public static final String TGRP_CODE_FR = "FR";
	
	 /** Type de groupes "Structure Recherche" (SR) */
	public static final String TGRP_CODE_SR = "SR";

	/** Type de groupes "Partenariat" (PN) */
	public static final String TGRP_CODE_PN = "PN";

	/** Type de groupes "Forum" (F) */
	public static final String TGRP_CODE_F = "F";

	/** Type de groupes "Groupe système" (U) */
	public static final String TGRP_CODE_U = "U";
	
	/** Type de groupes "Groupe diplome" (U) */
	public static final String TGRP_CODE_D = "D";
	
	/** Type de groupes "Departement" (DE) */
	public static final String TGRP_CODE_DE = "DE";
	
	/** Type de groupes "Ligne budgetaire" (LB) */
	public static final String TGRP_CODE_LB = "LB";
	

	public static final EOQualifier QUAL_TYPE_GROUPE_G = new EOKeyValueQualifier(TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_G);
	
	public static final EOQualifier QUAL_TEM_WEB = new EOKeyValueQualifier(TGRP_TEM_WEB_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.OUI);
	public static final EOQualifier QUAL_TEM_WEB_NON = new EOKeyValueQualifier(TGRP_TEM_WEB_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.NON);
	
	public static final EOQualifier QUAL_TEM_UTIL = new EOKeyValueQualifier(TGRP_TEM_UTIL_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.OUI);
	public static final EOQualifier QUAL_TEM_UTIL_NON = new EOKeyValueQualifier(TGRP_TEM_UTIL_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.NON);

	/**
	 * Tableau des codes des types de groupes dont l'affectation est reservee a des utilisateur qui ont le droit SuperADmin (GRHUM_CREATEUR).
	 */
	public static final NSArray TYPE_GROUPES_RESERVES_SUPERADMIN = new NSArray(new Object[] {
			TGRP_CODE_RE
	});

	/**
	 * Qualifier pour recuperer les types de groupe pour les utilisateurs qui ne sont pas Super Admin
	 * 
	 * en date 16/07/2014 retrait du type Service pour que les administrateurs RH (Mangue ou Hamac) puissent créer des groupes de type Service.
	 */
	public static final EOQualifier QUAL_TYPES_GROUPES_NON_SUPERADMIN = new EONotQualifier(new EOOrQualifier(new NSArray(new Object[] {
//			new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_S),
			new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_RE)
	})));
	
	/**
	 * Qualifier pour recuperer les types de groupe pour les utilisateurs qui ne sont pas Grhum Createur
	 */
	public static final EOQualifier QUAL_TYPES_GROUPES_NON_GRHUMCREATEUR = new EONotQualifier(new EOOrQualifier(new NSArray(new Object[] {
			new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_RE)
	})));

	public static final EOSortOrdering SORT_TGRP_CODE_ASC = EOSortOrdering.sortOrderingWithKey(EOTypeGroupe.TGRP_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_TGRP_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(EOTypeGroupe.TGRP_LIBELLE_KEY, EOSortOrdering.CompareAscending);

	public EOTypeGroupe() {
		super();
	}

	/**
	 * @return Les types de groupes selectionnables par l'utilisateur (en fonction de ses droits). Tries par grpLibelle.
	 */
	public static NSArray getTypesGroupeForUtilisateur(EOEditingContext edc, PersonneApplicationUser appUser) {
		NSArray res = new NSArray();
		res = fetchAll(edc, getQualifierForUtilisateur(appUser), new NSArray(new Object[] {
				SORT_TGRP_LIBELLE_ASC
		}));
		return res;
	}

	/**
	 * @param appUser
	 * @return le qualifier pour fetcher les types de groupe en fonction de l'utilisateur (de ses droits).
	 */
	public static EOQualifier getQualifierForUtilisateur(PersonneApplicationUser appUser) {
		if (appUser.hasDroitGrhumCreateur()) {
			return null;
		} else if (appUser.hasDroitTous()) { // GRHUM CREATEUR (deja teste ci-dessus) ou SUPER UTILISATEUR
			return QUAL_TYPES_GROUPES_NON_GRHUMCREATEUR;
		} else {
			return QUAL_TYPES_GROUPES_NON_SUPERADMIN;
		}
	}

	public String libelleEtCode() {
		return tgrpLibelle() + " (" + tgrpCode() + ")";
	}
}
