/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation.Liste;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * Permet de typer des personnes au sein d'un groupe. Les choses intéressantes a
 * savoir : <li>
 * <ul>
 * Les doublons ne sont pas autorises (un doublon definit par la combinaison
 * association + groupe + personne + date debut).
 * </ul>
 * </li>
 *
 * @see IPersonne#definitUnRole(EOEditingContext, EOAssociation, EOStructure,
 *      Integer, com.webobjects.foundation.NSTimestamp,
 *      com.webobjects.foundation.NSTimestamp, String, java.math.BigDecimal,
 *      Integer)
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class EORepartAssociation extends _EORepartAssociation {

	private static final long serialVersionUID = 8826340008322150639L;

	public static final Integer DEFAULT_RAS_RANG = Integer.valueOf(1);
	public static final String TO_PERSONNE_ELT_KEY = "toPersonneElt";
	public static final EOSortOrdering SORT_NOM_AFFICHAGE_ASC = EOSortOrdering.sortOrderingWithKey(TO_PERSONNE_ELT_KEY + ".getNomPrenomAffichage", EOSortOrdering.CompareAscending);



	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 *
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		setDModification(AUtils.now());
		fixPersIdCreationEtModification();
		checkUsers();
		checkReservees();
		checkContraintesObligatoires();
		checkDoublons();
		super.validateObjectMetier();
	}

	private void checkReservees() throws ValidationException {
		if (toAssociation().assCode().equals(Liste.AFFECT.code())) {
			throw new ValidationException("La modification des affectations est interdite en dehors de l'application Mangue");
		}
	}

	private void checkDoublons() throws NSValidation.ValidationException {
		EOStructure groupe = this.toStructure();
		EOQualifier qual1 = new EOKeyValueQualifier(PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, this.toPersonneElt().persId());
		EOQualifier qual2 = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual, this.toAssociation());
		EOQualifier qual3 = new EOKeyValueQualifier(EORepartAssociation.RAS_D_OUVERTURE_KEY, EOQualifier.QualifierOperatorEqual, this.rasDOuverture());

		NSArray res1 = EOQualifier.filteredArrayWithQualifier(groupe.toRepartStructuresElts(), qual1);
		NSMutableArray array = new NSMutableArray();
		for (int i = 0; i < res1.count(); i++) {
			EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
			array.addObjectsFromArray(array_element.toRepartAssociations(new EOAndQualifier(new NSArray(new Object[] { qual2, qual3 }))));
		}
		NSArray res = array;

		// NSArray res = EOQualifier.filteredArrayWithQualifier((NSArray)
		// groupe.toRepartStructuresElts().valueForKey(EORepartStructure.TO_REPART_ASSOCIATIONS_KEY),
		// new EOAndQualifier(new NSArray(new Object[]{qual1, qual2})));
		// res = (NSArray)
		// res.valueForKey(EORepartStructure.TO_REPART_ASSOCIATIONS_KEY);
		for (int i = 0; i < res.count(); i++) {
			EORepartAssociation array_element = (EORepartAssociation) res.objectAtIndex(i);
			if (!array_element.globalID().equals(this.globalID())) {
				throw new NSValidation.ValidationException("La personne " + this.toPersonneElt().persLibelle() + " est deja affectee au groupe " + groupe.libelle()
						+ " pour le role " + toAssociation().assLibelle()
						+ (this.rasDOuverture() == null ? "" : " pour cette date (" + MyDateCtrl.formatDateShort(this.rasDOuverture()) + ")") + ".");
			}
		}
	}

	/**
	 * @return La personne pour la partie "element" de repartStructure.
	 */
	public IPersonne toPersonneElt() {
		if (toStructuresAssociees() != null && toStructuresAssociees().count() > 0) {
			return (IPersonne) toStructuresAssociees().objectAtIndex(0);
		} else if (toIndividusAssocies() != null && toIndividusAssocies().count() > 0) {
			return (IPersonne) toIndividusAssocies().objectAtIndex(0);
		}
		return null;
	}

	/**
	 * Permet de coller a setToPersonne dans les cas de keyValueCoding (bindings notamment)
	 * @return
	 */

	public IPersonne toPersonne() {
		return toPersonneElt();
	}

	public void checkUsers() throws NSValidation.ValidationException {
		if (persIdModification() == null) {
			throw new NSValidation.ValidationException("La référence au modificateur (persIdModification) est obligatoire.");
		}
		if (persIdCreation() == null) {
			setPersIdCreation(persIdModification());
		}

		if (hasTemporaryGlobalID()) {
			checkDroitCreation(persIdCreation());
		} else {
			checkDroitModification(persIdModification());
		}
	}

	public void checkDroitModification(Integer persId) throws NSValidation.ValidationException {
		return;
	}

	public void checkDroitCreation(Integer persId) throws NSValidation.ValidationException {
		return;
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setRasRang(Integer.valueOf(1));
		setDCreation(AUtils.now());
	}

	public void setToPersonne(IPersonne personne) {
		removeAllFromToIndividusAssociesRelationship();
		removeAllFromToStructuresAssociesRelationship();
		if (personne == null) {
			setPersId(null);
		} else {
			if (personne.isIndividu()) {
				addToToIndividusAssociesRelationship((EOIndividu) personne);
			} else {
				addToToStructuresAssocieesRelationship((EOStructure) personne);
			}
			setPersId(personne.persId());
		}
	}

	@Override
	public void setToStructureRelationship(EOStructure value) {
		if (value != null) {
			super.setToStructureRelationship(value.localInstanceIn(this.editingContext()));
		} else {
			super.setToStructureRelationship(null);
		}
	}

	@Override
	public void setToAssociationRelationship(EOAssociation value) {
		if (value != null) {
			super.setToAssociationRelationship(value.localInstanceIn(this.editingContext()));
		} else {
			super.setToAssociationRelationship(null);
		}
	}

	/**
	 * @return true si l'objet n'est pas correctement rempli.
	 */
	public boolean isEmpty() {
		if (this.toAssociation() == null) {
			return true;
		}
		return false;
	}

	public void removeAllFromToIndividusAssociesRelationship() {
		for (int i = toIndividusAssocies().count() - 1; i >= 0; i--) {
			removeFromToIndividusAssociesRelationship((EOIndividu) toIndividusAssocies().objectAtIndex(i));
		}
	}

	public void removeAllFromToStructuresAssociesRelationship() {
		for (int i = toStructuresAssociees().count() - 1; i >= 0; i--) {
			removeFromToStructuresAssocieesRelationship((EOStructure) toStructuresAssociees().objectAtIndex(i));
		}
	}

	/**
	 * @param assCode
	 *            le code de l'association (role)
	 * @return les personnes ayant le role dans une structure quelconque
	 */
	public static NSArray getPersonnesWithAssociation(EOEditingContext ec, String assCode) {
		NSMutableSet res = new NSMutableSet();
		NSArray reparts = EORepartAssociation.fetchAll(ec, ERXQ.equals(ERXQ.keyPath(EORepartAssociation.TO_ASSOCIATION_KEY, EOAssociation.ASS_CODE_KEY), assCode));
		for (Object repartObj : reparts) {
			EORepartAssociation repart = (EORepartAssociation) repartObj;
			res.addObject(repart.toPersonneElt());
		}
		return res.allObjects();
	}

}
