/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORegle.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EORegle extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EORegle.class);

	public static final String ENTITY_NAME = "Fwkpers_Regle";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REGLE";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> R_VALUE = new ERXKey<String>("rValue");
  public static final ERXKey<String> R_VALUE2 = new ERXKey<String>("rValue2");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> TO_GROUPE_DYNAMIQUES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique>("toGroupeDynamiques");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegleKey> TO_REGLE_KEY = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegleKey>("toRegleKey");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegleNode> TO_REGLE_NODE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegleNode>("toRegleNode");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur> TO_REGLE_OPERATEUR = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur>("toRegleOperateur");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegle> TO_REGLE_PERE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegle>("toReglePere");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegle> TO_REGLES_ENFANTS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegle>("toReglesEnfants");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String R_VALUE_KEY = "rValue";
	public static final String R_VALUE2_KEY = "rValue2";

// Attributs non visibles
	public static final String R_PERE_ID_KEY = "rPereId";
	public static final String R_ID_KEY = "rId";
	public static final String RO_ID_KEY = "roId";
	public static final String RN_ID_KEY = "rnId";
	public static final String RK_ID_KEY = "rkId";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String R_VALUE_COLKEY = "R_VALUE";
	public static final String R_VALUE2_COLKEY = "R_VALUE_2";

	public static final String R_PERE_ID_COLKEY = "R_PERE_ID";
	public static final String R_ID_COLKEY = "R_ID";
	public static final String RO_ID_COLKEY = "RO_ID";
	public static final String RN_ID_COLKEY = "RN_ID";
	public static final String RK_ID_COLKEY = "RK_ID";


	// Relationships
	public static final String TO_GROUPE_DYNAMIQUES_KEY = "toGroupeDynamiques";
	public static final String TO_REGLE_KEY_KEY = "toRegleKey";
	public static final String TO_REGLE_NODE_KEY = "toRegleNode";
	public static final String TO_REGLE_OPERATEUR_KEY = "toRegleOperateur";
	public static final String TO_REGLE_PERE_KEY = "toReglePere";
	public static final String TO_REGLES_ENFANTS_KEY = "toReglesEnfants";



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String rValue() {
    return (String) storedValueForKey(R_VALUE_KEY);
  }

  public void setRValue(String value) {
    takeStoredValueForKey(value, R_VALUE_KEY);
  }

  public String rValue2() {
    return (String) storedValueForKey(R_VALUE2_KEY);
  }

  public void setRValue2(String value) {
    takeStoredValueForKey(value, R_VALUE2_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORegleKey toRegleKey() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORegleKey)storedValueForKey(TO_REGLE_KEY_KEY);
  }

  public void setToRegleKeyRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegleKey value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORegleKey oldValue = toRegleKey();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REGLE_KEY_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REGLE_KEY_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORegleNode toRegleNode() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORegleNode)storedValueForKey(TO_REGLE_NODE_KEY);
  }

  public void setToRegleNodeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegleNode value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORegleNode oldValue = toRegleNode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REGLE_NODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REGLE_NODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur toRegleOperateur() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur)storedValueForKey(TO_REGLE_OPERATEUR_KEY);
  }

  public void setToRegleOperateurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur oldValue = toRegleOperateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REGLE_OPERATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REGLE_OPERATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORegle toReglePere() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORegle)storedValueForKey(TO_REGLE_PERE_KEY);
  }

  public void setToReglePereRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegle value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORegle oldValue = toReglePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REGLE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REGLE_PERE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> toGroupeDynamiques() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique>)storedValueForKey(TO_GROUPE_DYNAMIQUES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> toGroupeDynamiques(EOQualifier qualifier) {
    return toGroupeDynamiques(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> toGroupeDynamiques(EOQualifier qualifier, boolean fetch) {
    return toGroupeDynamiques(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> toGroupeDynamiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique.TO_REGLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGroupeDynamiques();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGroupeDynamiquesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GROUPE_DYNAMIQUES_KEY);
  }

  public void removeFromToGroupeDynamiquesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GROUPE_DYNAMIQUES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique createToGroupeDynamiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_GroupeDynamique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GROUPE_DYNAMIQUES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique) eo;
  }

  public void deleteToGroupeDynamiquesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GROUPE_DYNAMIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGroupeDynamiquesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> objects = toGroupeDynamiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGroupeDynamiquesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> toReglesEnfants() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle>)storedValueForKey(TO_REGLES_ENFANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> toReglesEnfants(EOQualifier qualifier) {
    return toReglesEnfants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> toReglesEnfants(EOQualifier qualifier, boolean fetch) {
    return toReglesEnfants(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> toReglesEnfants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORegle.TO_REGLE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORegle.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toReglesEnfants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToReglesEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegle object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REGLES_ENFANTS_KEY);
  }

  public void removeFromToReglesEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REGLES_ENFANTS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORegle createToReglesEnfantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Regle");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REGLES_ENFANTS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORegle) eo;
  }

  public void deleteToReglesEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REGLES_ENFANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToReglesEnfantsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORegle> objects = toReglesEnfants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToReglesEnfantsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORegle avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORegle createEORegle(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, Integer persIdCreation
, Integer persIdModification
, org.cocktail.fwkcktlpersonne.common.metier.EORegleNode toRegleNode			) {
    EORegle eo = (EORegle) createAndInsertInstance(editingContext, _EORegle.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    eo.setToRegleNodeRelationship(toRegleNode);
    return eo;
  }

  
	  public EORegle localInstanceIn(EOEditingContext editingContext) {
	  		return (EORegle)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORegle creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORegle creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORegle object = (EORegle)createAndInsertInstance(editingContext, _EORegle.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORegle localInstanceIn(EOEditingContext editingContext, EORegle eo) {
    EORegle localInstance = (eo == null) ? null : (EORegle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORegle#localInstanceIn a la place.
   */
	public static EORegle localInstanceOf(EOEditingContext editingContext, EORegle eo) {
		return EORegle.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORegle>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORegle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORegle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORegle> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORegle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORegle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORegle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORegle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORegle> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORegle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORegle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORegle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORegle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORegle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORegle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
