/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXExistsQualifier;

/**
 * Prend en charge des méthodes partages entre les classes {@link EOIndividu} et {@link EOStructure}.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class PersonneDelegate {
	public final static Logger logger = Logger.getLogger(PersonneDelegate.class);

	private final IPersonne personne;
	private final PersonneDelegateCacheManager cacheManager;

	public PersonneDelegate(IPersonne personne) {
		this.personne = personne;
		cacheManager = new PersonneDelegateCacheManager(this);
	}

	/**
	 * <b>Rod : imposible d'utiliser la recherche en memoire de ERXFetchSpecification car le qualifier peut porter sur un toMany et les filtres en
	 * memoires ne fonctionnent pas sur des toMany (ex.toRepartStructureElts.persId = toto ) </b>
	 * 
	 * @param qualifierForGroupe (qualifier applicable a partir d'un objet EOStructure).
	 * @return tous les groupes affectés a la personne
	 */
	@SuppressWarnings("unchecked")
	protected NSArray getAllGroupesAffectes(EOQualifier qualifierForGroupe) {
		//		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOStructure.TEM_VALIDE_KEY + "= 'O' AND " + EOStructure.TO_REPART_STRUCTURES_ELTS_KEY + "." + EORepartStructure.PERS_ID_KEY + " = %@", new NSArray(personne.persId()));
		//		if (qualifierForGroupe != null) {
		//			qualifier = new EOAndQualifier(new NSArray(new Object[] {
		//					qualifier, qualifierForGroupe
		//			}));
		//		}
		//		//EOFetchSpecification eofs = new EOFetchSpecification(EOStructure.ENTITY_NAME, qualifier, new NSArray(EOStructure.SORT_LL_STRUCTURE_ASC));
		//		//Rod : imposible d'utiliser la recherche en memoire de ERXFetchSpecification car le qualifier peut porter sur un toMany (ex. 
		//
		//		ERXFetchSpecification eofs = new ERXFetchSpecification(EOStructure.ENTITY_NAME, qualifier, new NSArray(EOStructure.SORT_LL_STRUCTURE_ASC));
		//		eofs.setIncludeEditingContextChanges(true);
		//		eofs.setUsesDistinct(true);
		//
		//		ProgrammerTips.checkEc(personne.editingContext(), "PersonneDelegate.getAllGroupesAffectes");
		//		NSArray resultats = personne.editingContext().objectsWithFetchSpecification(eofs);
		//
		//		return resultats;

		//
		NSArray restmp = (NSArray) personne.toRepartStructures().valueForKey(EORepartStructure.TO_STRUCTURE_GROUPE_KEY);
		EOQualifier qualFinal = EOStructure.QUAL_STRUCTURES_VALIDE;
		if (qualifierForGroupe != null) {
			qualFinal = new EOAndQualifier(new NSArray(new Object[] {
					qualFinal, qualifierForGroupe
			}));
		}
		NSArray res2 = EOQualifier.filteredArrayWithQualifier(restmp, qualFinal);
		NSArray res3 = EOSortOrdering.sortedArrayUsingKeyOrderArray(res2, new NSArray(EOStructure.SORT_LL_STRUCTURE_ASC));
		return res3;
	}

	protected NSArray getAllRepartStructuresAffectes(EOQualifier qual) {
		EOQualifier qualFinal = EORepartStructure.QUAL_TO_STRUCTURE_VALIDE;
		if (qual != null) {
			qualFinal = new EOAndQualifier(new NSArray(new Object[] {
					qualFinal, qual
			}));
		}
		NSArray res = personne.toRepartStructures(qualFinal);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(EORepartStructure.SORT_LL_STRUCTURE_ASC));
	}

	protected NSArray getGroupesPublicsAffectes(EOQualifier qualifierForGroupe) {
		NSArray res = getAllGroupesAffectes(qualifierForGroupe);

		return EOQualifier.filteredArrayWithQualifier(res, EOStructure.QUAL_GROUPE_PUBLIC);
	}

	protected NSArray getGroupesPrivesAffectes(EOQualifier qualifierForGroupe) {
		NSArray res = getAllGroupesAffectes(qualifierForGroupe);
		return EOQualifier.filteredArrayWithQualifier(res, EOStructure.QUAL_GROUPE_PRIVE);
	}

	/**
	 * @param appUser
	 * @return Les groupes affectés à l'individu en fonction des droits de l'utilisateur.
	 */
	public NSArray getGroupesAffectes(PersonneApplicationUser appUser, EOQualifier qualifierForGroupe) {
		if (appUser.hasDroitAllGroupesPrivesVisualisation()) {
			return NSArrayCtrl.unionOfNSArrays(new NSArray[] {
					getGroupesPrivesAffectes(qualifierForGroupe), getGroupesPublicsAffectes(qualifierForGroupe)
			});
		}
		return getGroupesPublicsAffectes(qualifierForGroupe);
	}

	public NSArray getGroupesAffectes(Integer persIdForUser, EOQualifier qualifierForGroupe) {
		return getGroupesAffectes(new PersonneApplicationUser(personne.editingContext(), PersonneApplicationUser.TYAP_STR_ID_ANNUAIRE, persIdForUser), qualifierForGroupe);
	}

	protected NSArray getRepartStructuresPublicsAffectes(EOQualifier qual) {
		NSArray res = getAllRepartStructuresAffectes(qual);
		return EOQualifier.filteredArrayWithQualifier(res, EORepartStructure.QUAL_TO_STRUCTURE_GROUPE_PUBLIC);
	}

	protected NSArray getRepartStructuresPrivesAffectes(EOQualifier qual) {
		NSArray res = getAllRepartStructuresAffectes(qual);
		return EOQualifier.filteredArrayWithQualifier(res, EORepartStructure.QUAL_TO_STRUCTURE_GROUPE_PRIVE);
	}

	/**
	 * @param appUser
	 * @param qualifierForGroupe Qualifier facultatif pour filtrer les groupes (qualifier applicable a partir d'un objet EORepartStructure).
	 * @return Les groupes affectés à l'individu en fonction des droits de l'utilisateur.
	 */
	public NSArray getRepartStructuresAffectes(PersonneApplicationUser appUser, EOQualifier qualifierForGroupe) {
		if (appUser.hasDroitAllGroupesPrivesVisualisation()) {
			return NSArrayCtrl.unionOfNSArrays(new NSArray[] {
					getRepartStructuresPrivesAffectes(qualifierForGroupe), getRepartStructuresPublicsAffectes(qualifierForGroupe)
			});
		}
		return getRepartStructuresPublicsAffectes(qualifierForGroupe);
	}

	public NSArray getRepartStructuresAffectes(Integer persIdForUser, EOQualifier qualifierForGroupe) {
		return getRepartStructuresAffectes(new PersonneApplicationUser(personne.editingContext(), PersonneApplicationUser.TYAP_STR_ID_ANNUAIRE, persIdForUser), qualifierForGroupe);
	}

	//
	//	public void fixPersIdCreation() {
	//		//  a implementer quand champ dispo
	//	}
	//
	//	public void fixPersIdModification() {
	//		//  a implementer quand champ dispo		
	//	}

	/**
	 * Affecte la personne au groupe par defaut si celle-ci n'est pas deja dans un groupe de type REFERENTIEL.
	 * 
	 * @deprecated
	 */
	@Deprecated
	public void fixGroupeDefaut() throws NSValidation.ValidationException {
		if (personne.toRepartStructures().count() == 0) {
			if (!EOStructureForGroupeSpec.isPersonneInGroupeOfType(personne, EOTypeGroupe.TGRP_CODE_RE)) {
				try {
					EOStructure groupe = EOStructureForGroupeSpec.getGroupeDefaut(personne.editingContext());
					EORepartStructure repart = EORepartStructure.creerInstanceSiNecessaire(personne.editingContext(), personne, groupe);
					if (repart.hasTemporaryGlobalID()) {
						repart.setPersIdCreation(personne.persIdModification());
					}
					repart.setPersIdModification(personne.persIdModification());

				} catch (Exception e) {
					e.printStackTrace();
					throw new NSValidation.ValidationException(e.getMessage());
				}
			}
		}
	}

	/**
	 * Affecte la personne au groupe par defaut si celle-ci n'est pas deja dans un groupe de type REFERENTIEL.
	 * 
	 * @param persId PersId de l'utilisateur
	 * @throws NSValidation.ValidationException
	 */
	public void fixGroupeDefaut(Integer persId) throws NSValidation.ValidationException {
		EOStructureForGroupeSpec.sharedInstance().fixGroupeDefaut(personne, persId);
		//		if (personne.toRepartStructures().count() == 0) {
		//			if (!EOStructureForGroupeSpec.isPersonneInGroupeOfType(personne, EOTypeGroupe.TGRP_CODE_RE)) {
		//				try {
		//					EOStructure groupe = EOStructureForGroupeSpec.getGroupeDefaut(personne.editingContext());
		//					EORepartStructure repart = EORepartStructure.creerInstanceSiNecessaire(personne.editingContext(), personne, groupe, persId);
		//					if (repart.hasTemporaryGlobalID()) {
		//						repart.setPersIdCreation(personne.persIdModification());
		//					}
		//					repart.setPersIdModification(personne.persIdModification());
		//
		//				} catch (Exception e) {
		//					e.printStackTrace();
		//					throw new NSValidation.ValidationException(e.getMessage());
		//				}
		//			}
		//		}
	}

	/**
	 * @return Tous les groupes valides d'un certain type affectes a la personne
	 */
	public NSArray getAllGroupesAffectesOfType(String tgrpCode) {
		//		EOQualifier qualifier1 = null;
		//		if (!StringCtrl.isEmpty(tgrpCode)) {
		//			qualifier1 = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, tgrpCode);
		//		}
		//		NSArray tmp1 = getAllGroupesAffectes(qualifier1);
		//		return tmp1;

		EOQualifier qualifier = new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, tgrpCode);
		NSArray tmp = getAllGroupesAffectes(null);
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < tmp.count(); i++) {
			EOStructure structure = (EOStructure) tmp.objectAtIndex(i);
			if (structure.toRepartTypeGroupes(qualifier).count() > 0) {
				if (res.indexOfObject(structure) == NSArray.NotFound) {
					res.addObject(structure);
				}
			}
		}
		return res;
	}

	/**
	 * Verifie si l'utilisateur represente par le persId a le doit de modifier la personne en cours.
	 * 
	 * @see PersonneApplicationUser#hasDroitModificationIPersonne(IPersonne)
	 */
	public boolean hasPersonneUtilisateurDroitModification(Integer persId) {
		PersonneApplicationUser appUser = new PersonneApplicationUser(personne.editingContext(), persId);
		return appUser.hasDroitModificationIPersonne(personne);
	}

	public boolean hasPersonneUtilisateurDroitCreation(Integer persId) {
		PersonneApplicationUser appUser = new PersonneApplicationUser(personne.editingContext(), persId);
		return appUser.hasDroitCreationIPersonne(personne);
	}

//	public boolean hasPersonneUtilisateurDroitRibConsultation(Integer persId) {
//		PersonneApplicationUser appUser = new PersonneApplicationUser(personne.editingContext(), persId);
//		return appUser.hasDroitVisualisationEOFournis(null, AUtils.currentExercice());
//	}
	
	protected NSArray<EOStructure> fetchGroupesAdministres() {
		NSArray<EOStructure> res = NSArray.emptyArray();
		if (personne.isIndividu()) {
			EOQualifier qual = EOStructure.GRP_RESPONSABLE.eq(Integer.valueOf(personne.getNumero()))
					.or(new ERXExistsQualifier(EOSecretariat.NO_INDIVIDU.eq(Integer.valueOf(personne.getNumero())), EOStructure.TO_SECRETARIATS.key()));

			res = EOStructureForGroupeSpec.rechercherGroupes(personne.editingContext(), qual, 0, true);
			
		}
		return res;
	}

	/**
	 * @return Les groupes dont la personne est administrateur, cad si la personne est grpResponsable ou bien est dans la table secretariat. Ceci lui
	 *         donne le droit de modifier le groupe.
	 */
	public NSArray<EOStructure> getGroupesAdministres() {		
		NSArray<EOStructure> groupes = getCacheManager().getGroupesAdministres(personne.editingContext());
		if (groupes == null) {
			groupes = new NSArray<EOStructure>();
		}
		return groupes;
	}

	/**
	 * @return Les groupes dont la personne est administrateur, cad si la personne est grpResponsable.
	 */
	public NSArray getGroupesResponsable() {
		NSArray res = NSArray.EmptyArray;
		if (personne.isIndividu()) {
			EOQualifier qual = new EOKeyValueQualifier(EOStructure.GRP_RESPONSABLE_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(personne.getNumero()));
			res = EOStructureForGroupeSpec.rechercherGroupes(personne.editingContext(), qual, 0, true);

		}
		return res;
	}

	/**
	 * @return Les groupes dont la personne est administrateur, cad si la personne est grpResponsable.
	 */
	public NSArray getGroupesSecretariat() {
		NSArray res = NSArray.EmptyArray;
		if (personne.isIndividu()) {
			EOQualifier qual2 = new EOKeyValueQualifier(EOStructure.TO_SECRETARIATS_KEY + "." + EOSecretariat.NO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(personne.getNumero()));
			res = EOStructureForGroupeSpec.rechercherGroupes(personne.editingContext(), qual2, 0, true);

		}
		return res;
	}

	public NSArray getGroupesAdministresOfType(String tgrpCode) {
		//EOQualifier qualifier = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, tgrpCode);
		//return EOQualifier.filteredArrayWithQualifier(getGroupesAdministres(), qualifier);
		//EOQualifier qualifier = new EOKeyValueQualifier(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, tgrpCode);
		EOQualifier qualifier = new EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, tgrpCode);
		NSMutableArray retour = new NSMutableArray();
		for (Object occur : getGroupesAdministres()) {
			NSArray typesGroupes = ((EOStructure) occur).toRepartTypeGroupes(qualifier);
			if ((typesGroupes != null) && (typesGroupes.count() > 0))
				retour.addObject(occur);
		}
		//return EOQualifier.filteredArrayWithQualifier(getGroupesAdministres(), qualifier);
		return retour;
	}

	/**
	 * Verifie si l'utilisateur a le droit de modifier cette personne. Appelé lors de l'enregistrement.
	 * 
	 * @see PersonneDelegate#hasPersonneUtilisateurDroitModification(Integer persId)
	 */
	public void checkDroitModification(Integer persId) throws NSValidation.ValidationException {
		if (!hasPersonneUtilisateurDroitModification(persId)) {
			throw new NSValidation.ValidationException("Vous n'avez pas le droit de modifier cette personne.");
		}
	}

	public void checkDroitCreation(Integer persId) throws NSValidation.ValidationException {
		if (!hasPersonneUtilisateurDroitCreation(persId)) {
			throw new NSValidation.ValidationException("Vous n'avez pas le droit de creer cette personne.");
		}
	}

	/**
	 * Verifie si les utilisateurs affectes a persIdCreation et persIdModification ont bien le droits de creer/Modifier la personne.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkUsers() throws NSValidation.ValidationException {
		if (personne.persIdModification() == null) {
			throw new NSValidation.ValidationException("La référence au modificateur (persIdModification) est obligatoire pour la personne " + personne.persLibelle() + " (" + personne.persId() + ")");
		}
		if (personne.persIdCreation() == null) {
			personne.setPersIdCreation(personne.persIdModification());
		}
//		if (personne.persIdCreation() == null) {
//			throw new NSValidation.ValidationException("La reference au createur (persIdCreation) est obligatoire.");
//		}

		if (!FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PERSONNE_OBJECTHASCHANGED_DESACTIVE)) {
			try {
				if (personne.hasTemporaryGlobalID()) {
					checkDroitCreation(personne.persIdCreation());
				} else {
					checkDroitModification(personne.persIdModification());
				}
			} catch (NSValidation.ValidationException e) {
				throw new NSValidation.ValidationException(e.getMessage()
						+ " : " + personne.persLibelle() + " ("
						+ personne.persId() + ")");
			}
		}
	}

	/**
	 * Si le persIdModification ou le persIdCreation est nul, il sont affectes.
	 * 
	 * @param persId
	 */
	public void fixPersIdModificationEtCreation(Integer persId) {
		if (personne.persIdModification() == null) {
			personne.setPersIdModification(persId);
		}

		if (personne.persIdModification() == null) {
			personne.setPersIdCreation(persId);
		}
	}

	/**
	 * Définit un role pour une personne au sein d'un groupe. Crée un objet repartAssociation ou bien renvoie un objet repartAssociation qui existait
	 * deja avec meme groupe/personne/Association/ date debut (contrainte d'unicité sur ces attributs). L'objet repartStructure lié est créé
	 * automatiquement si necessaire. Utilisez cette methode plutot dans le cadre d'ajout de role sans interface.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @return L'objet EORepartAssociation
	 */
	public EORepartAssociation definitUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang) {
		return this.definitUnRole(ec, association, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite, rasRang, false);
	}

	/**
	 * Définit un role pour une personne au sein d'un groupe. Crée un objet repartAssociation ou bien renvoie un objet repartAssociation qui existait
	 * deja avec meme groupe/personne/Association/ date debut (contrainte d'unicité sur ces attributs). L'objet repartStructure lié est créé
	 * automatiquement si necessaire. Utilisez cette methode plutot dans le cadre d'ajout de role sans interface.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @param autoriserGroupeReserve autorise la définition d'un role dans un groupe reserve
	 * @return L'objet EORepartAssociation
	 */
	public EORepartAssociation definitUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang, boolean autoriserGroupeReserve) {
		return EOStructureForGroupeSpec.sharedInstance().definitUnRole(ec, personne, association, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite, rasRang, autoriserGroupeReserve);
	}

	/**
	 * Supprime un role specifique (repartAssociation) de la personne au sein d'un groupe commencant a une date donnee. Le repartStructure n'est pas
	 * supprime.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur
	 * @param rasDOuverture
	 */
	public void supprimerUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture) {
		EOStructureForGroupeSpec.sharedInstance().supprimerUnRole(ec, personne, association, groupe, persIdUtilisateur, rasDOuverture);

		//		//Verifier s'il y a deja une association identique entre la structure et la personne
		//		EOQualifier qual1 = new EOKeyValueQualifier(EORepartAssociation.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, this.personne.persId());
		//		EOQualifier qual2 = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual, association);
		//		EOQualifier qual3 = new EOKeyValueQualifier(EORepartAssociation.RAS_D_OUVERTURE_KEY, EOQualifier.QualifierOperatorEqual, rasDOuverture);
		//
		//		NSArray res1 = EOQualifier.filteredArrayWithQualifier(groupe.toRepartStructuresElts(), qual1);
		//		NSMutableArray array = new NSMutableArray();
		//		for (int i = 0; i < res1.count(); i++) {
		//			EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
		//			array.addObjectsFromArray(array_element.toRepartAssociations(new EOAndQualifier(new NSArray(new Object[] {
		//					qual2, qual3
		//			}))));
		//		}
		//		EORepartAssociation res = null;
		//		if (array.count() > 0) {
		//			res = (EORepartAssociation) array.objectAtIndex(0);
		//		}
		//		if (res != null) {
		//			res.setToAssociationRelationship(null);
		//			res.setToPersonne(null);
		//			res.setToStructureRelationship(null);
		//			ec.deleteObject(res);
		//		}
	}

	/**
	 * Supprime les roles specifiques (repartAssociations) de la personne au sein d'un groupe.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur
	 */
	public void supprimerLesRoles(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur) {
		EOStructureForGroupeSpec.sharedInstance().supprimerLesRoles(ec, personne, association, groupe, persIdUtilisateur);

		//		EOQualifier qual1 = new EOKeyValueQualifier(EORepartAssociation.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, this.personne.persId());
		//		EOQualifier qual2 = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY, EOQualifier.QualifierOperatorEqual, association);
		//
		//		NSArray res1 = EOQualifier.filteredArrayWithQualifier(groupe.toRepartStructuresElts(), qual1);
		//		NSMutableArray array = new NSMutableArray();
		//		for (int i = 0; i < res1.count(); i++) {
		//			EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
		//			array.addObjectsFromArray(array_element.toRepartAssociations(new EOAndQualifier(new NSArray(new Object[] {
		//				qual2
		//			}))));
		//		}
		//		if (array.count() > 0) {
		//			for (int i = 0; i < array.count(); i++) {
		//				EORepartAssociation res = (EORepartAssociation) array.objectAtIndex(i);
		//				res.setToAssociationRelationship(null);
		//				res.setToPersonne(null);
		//				res.setToStructureRelationship(null);
		//				ec.deleteObject(res);
		//			}
		//		}

	}
  /**
   * Définit les roles pour une personne au sein d'un groupe. Elimine les repartAssociations precedemment existantes Retourne un tableau
   * d'EORepartAssociation nouvelles L'objet repartStructure lié est créé automatiquement si necessaire. Utilisez cette methode plutot dans le cadre
   * d'ajout de roles sans interface.
   * 
   * @param ec
   * @param associations
   * @param groupe
   * @param persIdUtilisateur Le Persid de l'utilisateur
   * @param rasDOuverture facultatif
   * @param rasDFermeture facultatif
   * @param rasCommentaire facultatif
   * @param rasQuotite facultatif
   * @param rasRang facultatif
   * @return NSArray des EORepartAssociation
   */
  public NSArray definitLesRoles(EOEditingContext ec, NSArray associations, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang, boolean autoriserGroupeReserve) {
    return EOStructureForGroupeSpec.sharedInstance().definitLesRoles(ec, personne, associations, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite, rasRang, autoriserGroupeReserve);
  }
  
	/**
	 * Définit les roles pour une personne au sein d'un groupe. Elimine les repartAssociations precedemment existantes Retourne un tableau
	 * d'EORepartAssociation nouvelles L'objet repartStructure lié est créé automatiquement si necessaire. Utilisez cette methode plutot dans le cadre
	 * d'ajout de roles sans interface.
	 * 
	 * @param ec
	 * @param associations
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @return NSArray des EORepartAssociation
	 */
	public NSArray definitLesRoles(EOEditingContext ec, NSArray associations, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang) {
		return EOStructureForGroupeSpec.sharedInstance().definitLesRoles(ec, personne, associations, groupe, persIdUtilisateur, rasDOuverture, rasDFermeture, rasCommentaire, rasQuotite, rasRang);
		//		NSMutableArray lesRepartAssociations = new NSMutableArray();
		//
		//		if (rasRang == null) {
		//			rasRang = EORepartAssociation.DEFAULT_RAS_RANG;
		//		}
		//		EORepartStructure repartStructure = EORepartStructure.creerInstanceSiNecessaire(ec, personne, groupe, persIdUtilisateur);
		//		if (associations != null) {
		//			repartStructure.deleteAllToRepartAssociationsRelationships();
		//			Enumeration<EOAssociation> enumAssociations = associations.objectEnumerator();
		//			while (enumAssociations.hasMoreElements()) {
		//				EOAssociation eoAssociation = (EOAssociation) enumAssociations.nextElement();
		//				EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(ec);
		//				repartAssociation.setToAssociationRelationship(eoAssociation);
		//				repartAssociation.setPersIdModification(persIdUtilisateur);
		//				repartStructure.addToToRepartAssociationsRelationship(repartAssociation);
		//				repartAssociation.setRasDOuverture(rasDOuverture);
		//				repartAssociation.setRasDFermeture(rasDFermeture);
		//				repartAssociation.setRasCommentaire(rasCommentaire);
		//				repartAssociation.setRasQuotite(rasQuotite);
		//				repartAssociation.setRasRang(rasRang);
		//				lesRepartAssociations.add(repartAssociation);
		//			}
		//		}
		//		return lesRepartAssociations.immutableClone();
	}

	/**
	 * @param groupe Un groupe
	 * @param qualifier optionnel
	 * @return Les repartAssociations affectees a la personne au sein du groupe
	 */
	public NSArray getRepartAssociationsInGroupe(EOStructure groupe, EOQualifier qualifier) {
		return EOStructureForGroupeSpec.sharedInstance().getRepartAssociationsInGroupe(personne, groupe, qualifier);
		//
		//		EOKeyValueQualifier qualGroupe = new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOKeyValueQualifier.QualifierOperatorEqual, groupe);
		//		NSArray res1 = personne.toRepartStructures(qualGroupe);
		//		NSMutableArray array = new NSMutableArray();
		//		for (int i = 0; i < res1.count(); i++) {
		//			EORepartStructure array_element = (EORepartStructure) res1.objectAtIndex(i);
		//			array.addObjectsFromArray(array_element.toRepartAssociations(qualifier));
		//		}
		//		return array.immutableClone();
	}

	public void supprimerAffectationAUnGroupe(EOEditingContext ec, Integer persIdUtilisateur, EORepartStructure repartStructure) {
		EOStructureForGroupeSpec.sharedInstance().supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repartStructure);
		//		if (repartStructure != null) {
		//			//Verifier qu'on n'est pas dans un groupe gere en auto
		//			if (repartStructure.toStructureGroupe() != null) {
		//				if (EOStructureForGroupeSpec.isGroupeGereEnAuto(repartStructure.toStructureGroupe())) {
		//					return;
		//				}
		//			}
		//			repartStructure.deleteAllToRepartAssociationsRelationships();
		//
		//			if (repartStructure.toPersonneElt() != null) {
		//				repartStructure.toPersonneElt().getPersonneDelegate().fixPersIdModificationEtCreation(persIdUtilisateur);
		//			}
		//			if (repartStructure.toStructureGroupe() != null) {
		//				repartStructure.toStructureGroupe().getPersonneDelegate().fixPersIdModificationEtCreation(persIdUtilisateur);
		//			}
		//			if (repartStructure.toPersonneElt() != null) {
		//				repartStructure.toPersonneElt().removeFromToRepartStructuresRelationship(repartStructure);
		//			}
		//			if (repartStructure.toStructureGroupe() != null) {
		//				repartStructure.toStructureGroupe().removeFromToRepartStructuresRelationship(repartStructure);
		//			}
		//			repartStructure.setCStructure(null);
		//			ec.deleteObject(repartStructure);
		//		}

	}

	public void supprimerAffectationAUnGroupe(EOEditingContext ec, Integer persIdUtilisateur, EOStructure groupe) {
		EOStructureForGroupeSpec.sharedInstance().supprimerAffectationAUnGroupe(ec, personne, persIdUtilisateur, groupe);
		//		if (groupe != null) {
		//			NSArray reparts = personne.toRepartStructures(new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, groupe));
		//			Enumeration e = reparts.objectEnumerator();
		//			while (e.hasMoreElements()) {
		//				EORepartStructure repart = (EORepartStructure) e.nextElement();
		//				supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repart);
		//			}
		//		}
	}

	public void supprimerAffectationATousLesGroupes(EOEditingContext ec, Integer persIdUtilisateur) {
		EOStructureForGroupeSpec.sharedInstance().supprimerAffectationATousLesGroupes(ec, personne, persIdUtilisateur);
		//		NSArray reparts = personne.toRepartStructures();
		//		Enumeration e = reparts.objectEnumerator();
		//		while (e.hasMoreElements()) {
		//			EORepartStructure repart = (EORepartStructure) e.nextElement();
		//			supprimerAffectationAUnGroupe(ec, persIdUtilisateur, repart);
		//		}
	}

	public EOAdresse getAdressePrincipale(EOEditingContext ec) {
		NSArray res = personne.toRepartPersonneAdresses(new EOAndQualifier(new NSArray(new Object[] {
				EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE_SANS_PERSO, EORepartPersonneAdresse.QUAL_RPA_PRINCIPAL
		})));
		if (res.count() > 0) {
			return ((EORepartPersonneAdresse) res.objectAtIndex(0)).toAdresse();
		}
		return null;
	}

	/**
	 * Recherche une personne par son persId. Effectue d'abord une recherche sur les individus. Si pas trouve, recherche sur les structures.
	 */
	public static IPersonne fetchPersonneByPersId(EOEditingContext ec, Number persId) {
		if (persId == null) {
			return null;
		}
		EOIndividu ind = EOIndividu.fetchByKeyValue(ec, EOIndividu.PERS_ID_KEY, persId);
		if (ind != null) {
			return ind;
		}
		EOStructure struct = EOStructure.fetchByKeyValue(ec, EOIndividu.PERS_ID_KEY, persId);
		if (struct != null) {
			return struct;
		}

		return null;

	}

	public static IPersonne fetchPersonneForLoginPass(EOEditingContext ec, String login, String pass) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, login));
		quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_PASSWD_KEY, EOQualifier.QualifierOperatorEqual, pass));

		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
		NSArray res = EOCompte.fetchAll(ec, new EOAndQualifier(quals), new NSArray(new Object[] {
				sort1
		}));
		if (res.count() > 0) {
			return ((EOCompte) res.objectAtIndex(0)).toPersonne();
		}
		return null;
	}

	public static IPersonne fetchPersonneForLogin(EOEditingContext ec, String login) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, login));
		//    	quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_PASSWD_KEY, EOQualifier.QualifierOperatorEqual, pass));              

		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
		NSArray res = EOCompte.fetchAll(ec, new EOAndQualifier(quals), new NSArray(new Object[] {
				sort1
		}));
		if (res.count() > 0) {
			return ((EOCompte) res.objectAtIndex(0)).toPersonne();
		}
		return null;
	}

	/**
	 * @param groupe
	 * @return true si la personne administre le groupe passe en parametre.
	 */
	public boolean isGroupeInGroupesAdministres(EOStructure groupe) {
		NSArray groupesAdministres1 = getGroupesAdministres();
		NSArray<EOGlobalID> ids = (NSArray<EOGlobalID>) groupesAdministres1.valueForKey("globalID");
		return (ids.indexOfObject(groupe.globalID()) != NSArray.NotFound);
	}

	/**
	 * @param groupe
	 * @return true si la personne administre le groupe ou un de ses peres (recursif jusqu'à la racine des groupes).
	 */
	public boolean isGroupeOrPeresInGroupesAdministres(EOStructure groupe) {
		
		NSArray<NSDictionary<String, Object>> resultats = EOUtilities.rawRowsForSQL(groupe.editingContext(), "FwkCktlPersonne", isGroupeOrPeresInGroupesAdministresSql(groupe), null);
		
		boolean resultat = Boolean.parseBoolean((String) resultats.get(0).valueForKey("RESULTAT"));
		return resultat;
		
		/*
		boolean res = isGroupeInGroupesAdministres(groupe);
		if (res) {
			if (logger.isDebugEnabled()) {
				logger.debug(personne.libelleEtId() + " est administrateur du groupe " + groupe.libelleEtId());
			}
		}
		if (!res && groupe.toStructurePere() != null && !groupe.toStructurePere().equals(groupe)) {
			return isGroupeOrPeresInGroupesAdministres(groupe.toStructurePere());
		}
		return res;
		*/
	}
	
	public boolean isGroupesAffectesPourIndividusOrPeresInGroupesAdministres(EOIndividu individu) {
		
		NSArray<NSDictionary<String, Object>> resultats = EOUtilities.rawRowsForSQL(personne.editingContext(), "FwkCktlPersonne", 
				isGroupesAffectesPourIndividusOrPeresInGroupesAdministresSql(individu), null);
		
		boolean resultat = Boolean.parseBoolean((String) resultats.get(0).valueForKey("RESULTAT"));
		return resultat;
	}
	
	public String requeteCStructuresGroupesAdministresSql() {
		
		String requeteGroupesAdministresSecretariatSelect = " select c_structure";
		String requeteGroupesAdministresSecretariatFrom = " from grhum.secretariat";
		String requeteGroupesAdministresSecretariatWhere = " where no_individu = " + personne.getNumero();
		String requeteGroupesAdministresResponsableSelect = " select c_structure";
		String requeteGroupesAdministresResponsableFrom = " from GRHUM.structure_ulr";
		String requeteGroupesAdministresResponsable =  " where grp_responsable = " + personne.getNumero();
		
		String requeteGroupesAdministresComplete = 
				requeteGroupesAdministresSecretariatSelect + requeteGroupesAdministresSecretariatFrom + requeteGroupesAdministresSecretariatWhere +
				" UNION " + 
				requeteGroupesAdministresResponsableSelect + requeteGroupesAdministresResponsableFrom + requeteGroupesAdministresResponsable;

		return requeteGroupesAdministresComplete;
		
	}
	
	
	public String requeteCStructuresGroupesEtAncetresSql(String requetePourGroupes) {
		
		String requeteLigneeGroupeSelect = " SELECT t1.c_structure";
		String requeteLigneeGroupeFrom = " FROM grhum.structure_ulr t1";
		String requeteLigneeGroupeStartWith = " START WITH t1.c_structure in ( " + requetePourGroupes + ")"; 
		String requeteLigneeGroupeConnectBy = " CONNECT BY NOCYCLE t1.c_structure = PRIOR t1.c_structure_pere";
		
		String union = " UNION ";
		
		String requeteGroupeRacineSelect =" SELECT c_structure";
		String requeteGroupeRacineFrom = " FROM grhum.structure_ulr";
		String requeteGroupeRacineWhere = " where c_structure = c_structure_pere";
		
		String requeteLigneeComplete = 
				requeteLigneeGroupeSelect + requeteLigneeGroupeFrom + requeteLigneeGroupeStartWith + requeteLigneeGroupeConnectBy + 
				union +
				requeteGroupeRacineSelect + requeteGroupeRacineFrom + requeteGroupeRacineWhere;

		return requeteLigneeComplete;
		
	}
	
	public String requeteCStructuresGroupesEtAncetresSql(NSArray<EOStructure> groupes) {
		NSArray<String> listeDesGroupes = (NSArray<String>) groupes.valueForKey(EOStructure.C_STRUCTURE_KEY);
		return requeteCStructuresGroupesEtAncetresSql(listeDesGroupes.componentsJoinedByString(","));
	}
	
	public String requeteCStructuresGroupesEtAncetresSql(EOStructure groupe) {
		return requeteCStructuresGroupesEtAncetresSql(new NSArray<EOStructure>(groupe));
	}
	
	public String isGroupeOrPeresInGroupesAdministresSql(EOStructure groupe) {
		
		String requeteFinaleSelect  = " select to_char(decode(count(c_structure),0,'false','true')) as resultat"; 
		String requeteFinaleFrom = " from (" + requeteCStructuresGroupesEtAncetresSql(groupe) + ")";
		String requeteFinaleWhere = " where c_structure in (" + requeteCStructuresGroupesAdministresSql() + ")";
		
		return requeteFinaleSelect + requeteFinaleFrom + requeteFinaleWhere;
				
	}
	
	public String requeteCStructureGroupesAffectesSql(EOIndividu individu) {
		
		String requeteSelect  = " SELECT rs.c_structure ";
		String requeteFrom = " from grhum.repart_structure rs";
		String requeteInnerJoin = " inner join structure_ulr s on rs.c_structure = s.c_structure";
		String requeteWhere = " where rs.pers_id = " + individu.persId();
		
		return requeteSelect + requeteFrom + requeteInnerJoin + requeteWhere;
		
	}
	
	public String isGroupesAffectesPourIndividusOrPeresInGroupesAdministresSql(EOIndividu individu) {
		
		String requeteFinaleSelect  = " select to_char(decode(count(c_structure),0,'false','true')) as resultat"; 
		String requeteFinaleFrom = " from (" + requeteCStructuresGroupesEtAncetresSql(requeteCStructureGroupesAffectesSql(individu)) + ")";
		String requeteFinaleWhere = " where c_structure in (" + requeteCStructuresGroupesAdministresSql() + ")";
		
		return requeteFinaleSelect + requeteFinaleFrom + requeteFinaleWhere;

	}
	
	protected PersonneDelegateCacheManager getCacheManager() {
		return cacheManager;
	}

	/**
	 * @return l'adresse de facturation
	 */
	public IAdresse adressesFacturation() {
		EOQualifier qual = ERXQ.and(
				EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE, 
				EORepartPersonneAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE).eq(EOTypeAdresse.TADR_CODE_FACT));
		NSArray<EORepartPersonneAdresse> repartsAdressesFacturation = personne.toRepartPersonneAdresses(qual);
		NSArray<EOAdresse> adressesFacturation = EORepartPersonneAdresse.TO_ADRESSE.arrayValueInObject(repartsAdressesFacturation);
		return adressesFacturation.lastObject();
	}
	
	/**
	 * @return l'adresse de l'etudiant
	 */
	public IAdresse adresseEtudiant() {
		return getAdresse(EOTypeAdresse.TADR_CODE_ETUD);
	}
	
	/**
	 * @return l'adresse des parents
	 */
	public IAdresse adresseParent() {
		return getAdresse(EOTypeAdresse.TADR_CODE_PAR);
	}
	
	private IAdresse getAdresse(String codeTypeAdresse) {
		EOQualifier qual = ERXQ.and(
				EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE, 
				EORepartPersonneAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE).eq(codeTypeAdresse));
		NSArray<EORepartPersonneAdresse> repartsAdresses = personne.toRepartPersonneAdresses(qual);
		NSArray<EOAdresse> adresses = EORepartPersonneAdresse.TO_ADRESSE.arrayValueInObject(repartsAdresses);
		return adresses.lastObject();
	}
	
	public List<IAdresse> adresses() {
		NSArray<EORepartPersonneAdresse> repartsAdresses = personne.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE);
		List<EOAdresse> adresses = EORepartPersonneAdresse.TO_ADRESSE.arrayValueInObject(repartsAdresses);
		return new ArrayList<IAdresse>(adresses);
	}

}
