

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EONaf extends _EONaf
{

	public static final String C_NAF_ET_LIB_KEY = "cNafEtLib";
	
    public EONaf() {
        super();
    }

	public static final EOSortOrdering SORT_C_NAF = EOSortOrdering.sortOrderingWithKey(EONaf.C_NAF_KEY, EOSortOrdering.CompareAscending);
	

	/**
	 * 
	 * @param ec
	 * @param date
	 * @return les codes NAF valide a la date specifiee. 
	 */
	public static NSArray getAllNaf(EOEditingContext ec, NSTimestamp date) {
		EOQualifier qualValide = EOQualifier.qualifierWithQualifierFormat( "("+EONaf.D_DEB_VAL_KEY +" = nil or " + EONaf.D_DEB_VAL_KEY + "<= %@) and " + 
								"("+EONaf.D_FIN_VAL_KEY +" = nil or " + EONaf.D_FIN_VAL_KEY + ">= %@)", new NSArray(new Object[]{date, date   }));
		return fetchAll(ec, qualValide, new NSArray(new Object[]{SORT_C_NAF}));
	}
	
	/**
	 * @param ec
	 * @param date
	 * @param codeNaf
	 * @return un objet EONaf a partir de son code.
	 */
	public static EONaf getNaf(EOEditingContext ec, NSTimestamp date, String codeNaf) {
		EOQualifier qualCode = EOQualifier.qualifierWithQualifierFormat( "("+EONaf.C_NAF_KEY +" =%@)", new NSArray(new Object[]{codeNaf   }));
		return fetchFirstByQualifier(ec, qualCode);
	}

	
	public String cNafEtLib() {
		return cNaf()+ " " + llNaf();
	}
	

}
