/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IReferensCompetences;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

public class EOReferensCompetences extends _EOReferensCompetences implements IReferensCompetences {

    public EOReferensCompetences() {
        super();
    }

    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     * @throws une exception de validation
     */
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
    
    
// METHODES RAJOUTEES
    
    private static final int LIBELLE_TAILLE_MAX = 120;
    private static final int TAILLE_RABOTAGE = 6;

    /**
     * 
     * @return une String avec un libellé court
     */
    public String display() {
        return displayCourt();
    }
    
    /**
     * affichage des LIBELLE_TAILLE_MAX premiers caracteres
     * @return une String avec une taille adaptée
     */
    public String displayCourt() {
       String display = intitulComp();
       if (libelleTailleMaxDepassee()) {
           display = display.substring(0, LIBELLE_TAILLE_MAX - TAILLE_RABOTAGE) + " (...)";
       }
       return display;
    }
    
    /**
     * 
     * @return une String pour l'affichage du libellé long
     */
    public String displayLong() {
        return intitulComp();
    }
    
    /**
     * 
     * @return true si la taille de l'intitulé est supérieure à la taille max (fixée ici à 120)
     */
    public boolean libelleTailleMaxDepassee() {
        return intitulComp().length() > LIBELLE_TAILLE_MAX;
    }
    
    /**
     * 
     * @return une String pour l'affichage dans les listes déroulantes
     */
    public String popup() {
        return "poplink('" + StringCtrl.replace(displayLong(), "'", " ") + "');";
    }
    
    /**
     * 
     * @return une String pour l'affichage dans les "bulles d'aide"
     */
    public String popupWs() {
      return "Tip('" + StringCtrl.replace(displayLong(), "'", " ") + "')";
    }
    
    /**
     * Le composé de la clef primaire
     * @return le nom de l'entité correspondant au Code Emploi
     */
    public String id() {
    	NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
    	return ENTITY_NAME + "-" + dico.valueForKey("codeemploi") + "-_-" + dico.valueForKey("ordre");
    }

}
